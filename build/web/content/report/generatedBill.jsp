
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    
    <body>
        
        <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
            <!-- copy there from end -->
            <div id="print" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                    
                </div>
            </div>
            
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
                      <%
            int c = 0;
                            %>
            <c:if test = "${jcList != null}" >
                <c:forEach items="${jcList}" var="list"> 	
                    
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="border">
                        <tbody><tr>
                                <td class="contenthead" colspan="6" height="30"><div class="contenthead">Bill Report</div></td>
                            </tr>
                            
                            <tr>
                                
                                <td class="text1" height="30">JobCardNo</td>
                                <td class="text1" height="30"><c:out value="${list.jobCardId}"/></td>
                                <td class="text1" height="30">Vehicle No</td>
                                <td class="text1" height="30"><c:out value="${list.vehicleNo}"/></td>
                                <td class="text1" height="30">Customer</td>
                                <td class="text1" height="30"><c:out value="${list.customerName}"/></td>
                            </tr>
                    </tbody></table>
                    <input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>'/>
                    <input type="hidden" name="sparesPercent" value='<c:out value="${list.sparesPercentage}"/>'/>
                    <input type="hidden" name="labourPercent" value='<c:out value="${list.labourPercentage}"/>'/>
                    
                </c:forEach >
                
            </c:if> 
            <br>
            <%

            String classText = "";
            int oddEven = 0;

            int index = 0;%>    
                                    
            <c:if test = "${billDetails != null}" >                                            		
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="border">		                 
                    <tr>
                        <td class="text2" colspan="8" align="center" height="30"><strong>Spare Particulars</strong></td>
                    </tr>
                    
                    <tr>
                        <td class="contentsub" height="30">SNo</td>
                        <td class="contentsub" height="30"><div class="contentsub">Item Code</div></td>
                        <td class="contentsub" width="100" height="30">Item Name</td>
                        <td class="contentsub" height="30">Quantity</td>
                        <td class="contentsub" height="30">Tax(%)</td>
                        <td class="contentsub" height="30">Price(SAR.)</td>
                        <td class="contentsub" height="30">Amount(SAR.)</td>
                    </tr>
                    <c:forEach items="${billDetails}" var="itemList">                                       	
                        <%

            classText = "";
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>                                                                       
                            <td class="<%=classText %>" width="100" height="30"><c:out value="${itemList.itemId}"/></td>
                            <td class="<%=classText %>" width="100" height="30"><c:out value="${itemList.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><input type="text" size='10' class="form-control" readonly name="quantity" value='<c:out value="${itemList.itemQty}"/>'/></td>
                            <td class="<%=classText %>" height="30"><input type="text"  size='10'  class="form-control" readonly name="tax" value='<c:out value="${itemList.tax}"/>'/></td>
                            <td class="<%=classText %>" height="30"><input type="text" size='10'   class="form-control" readonly name="price" value='<c:out value="${itemList.itemPrice}"/>'/> </td>
                            <td class="<%=classText %>" height="30"><input type="text"  size='10' class="form-control"  readonly name="lineItemAmount" value='<c:out value="${itemList.itemAmount}"/>'/> </td>
                            
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
                
            </c:if> 
            
            
            
            <br>
            <%  index = 0;%>   
            
                                                    
                         
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="border">
                
                <tr>
                    <td class="text2" align="center" colspan="8" height="30"><strong>Labour Particulars</strong></td>
                </tr>
                
                <tr>
                    
                    <td class="contentsub" height="30"><div class="contentsub">SNo</div></td>
                     <td class="contentsub" height="30"><div class="contentsub">Bill No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Vendor</div></td>                   
                    <td class="contentsub" height="30"><div class="contentsub">Activities</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Charge</div></td>
                    <td></td>
                </tr>
                
                
                
                <c:if test = "${bodyBillDetails != null}" >
                    <c:forEach items="${bodyBillDetails}" var="bodyBill"> 	
                        <%

            classText = "";
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }

                        %>
                        
                        
                        <tr height="30">                            
                            
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bodyBill.billNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bodyBill.vendorName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bodyBill.activity}"/></td>                            
                                <td class="<%=classText %>" height="30"><input type="text" class="form-control" size='10'   readonly name="activityAmount" value='<c:out value="${bodyBill.totalAmount}"/>'/>
                                <input type="hidden" readonly name="OriginalActivityAmount" value='<c:out value="${bodyBill.totalAmount}"/>'/></td>                            
                            
                            
                        </tr>
                        <%
            index++;
                        %>
                        
                    </c:forEach >
                </c:if> 
                <c:if test = "${ActivityList != null}" >
                    <c:forEach items="${ActivityList}" var="activity"> 
                        <%

            classText = "";
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }

                        %>
                        
                        
                        <tr height="30">                            
                            
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                             <td class="<%=classText %>" height="30">PAPL</td>
                              <td class="<%=classText %>" height="30">PAPL</td>
                            <td class="<%=classText %>" height="30"><c:out value="${activity.activity}"/></td>
                            
                            
                                
                                <td class="<%=classText %>" height="30"><input type="text" class="form-control" size='10'   readonly name="activityAmount" value='<c:out value="${activity.activityAmount}"/>'/>
                                <input type="hidden" readonly name="OriginalActivityAmount" value='<c:out value="${activity.activityAmount}"/>'/></td>                            
                            
                            
                        </tr>
                        <%
            index++;
                        %>                      
                        
                    </c:forEach >
                </c:if> 
            </table>
            
            
            
            
            
            <br> 
           
            <c:if test = "${totalPrices != null}" > 
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="381" class="border">
                    <tbody>
                    <c:forEach items="${totalPrices}" var="itemList">    
                       
                        <tr>
                            <td class="text1" height="30" width="190">Spares Amount</td>
                            <td class="text1" height="30" width="189"><input name="spares"  class="form-control" readonly="readonly"  value="<c:out value='${itemList.spares}'/>"></td>
                        </tr>
                        
                        
                        <tr>
                            <td class="text2" height="30" width="190">Labour Amount</td>
                            <td class="text2" height="30" width="189"><input name="labour"  class="form-control" readonly="readonly"  value="<c:out value='${itemList.labour}'/>"/></td>
                        </tr>
                        
                        
                        
                        <tr>
                            <td class="text1" height="30" width="190">Total Amount</td>
                            <td class="text1" height="30" width="189"><input  name="total" class="form-control" readonly="readonly"  value="<c:out value='${itemList.totalAmount}'/>" ></td>
                        </tr>
                        
                        <tr>
                            <td class="text2" height="30" width="190">Discount(in %age)</td>
                            <td class="text2" height="30" width="189"><input name="discount" class="form-control" readonly type="text" value="<c:out value='${itemList.discount}'/>" /></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30" width="190">Nett Amount</td>
                            <td class="text2" height="30" width="189"><input class="form-control" readonly="readonly" name="nett" value="<c:out value='${itemList.netAmount}'/>" /></td>
                        </tr>   
  
                             
                    </c:forEach >
                    
                </table>
            </c:if>           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
