<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<html lang="en">
<head>

        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="prettify/prettify.js"></script>
        <script type="text/javascript" src="ui/js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:100%;
}
	.column { width: 480px; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>

    <%
String purchase = "<chart palette='2' caption='' showValues='0' numVDivLines='10' drawAnchors='0' numberPrefix='SAR' divLineAlpha='30' alternateHGridAlpha='20'  setAdaptiveYMin='1' >	<categories><category label='Apr' /> <category label='May' /><category label='Jun' /> <category label='Jul' /><category label='Aug' /> 	<category label='Sep' /><category label='Oct' /> <category label='Nov' /><category label='Dec' /> 	<category label='Jan' /><category label='Feb' /> <category label='Mar' /></categories>	<dataset seriesName='Current Year' color='#4AA02C'><set value='1127654' /> <set value='1226234' />	<set value='1299456' /> <set value='1311565' /> <set value='1324454' /> <set value='1357654' />	<set value='1296234' /> <set value='1359456' /> <set value='1391565' /> <set value='1414454' />	<set value='1671565' /> <set value='1134454' /> </dataset>	<dataset seriesName='Previous Year' color='#6698FF'><set value='927654' /><set value='1126234' /><set value='999456' /><set value='1111565' /><set value='1124454' /><set value='1257654' /><set value='1196234' /><set value='1259456' /><set value='1191565' /><set value='1214454' /><set value='1371565' /><set value='1434454' /> </dataset>	<styles><definition><style name='XScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_xScale' /><style name='YScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='XAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='AlphaAnim' type='ANIMATION' duration='0.5' start='0' param='_alpha' /></definition>	<application><apply toObject='CANVAS' styles='XScaleAnim, YScaleAnim,AlphaAnim' /><apply toObject='DIVLINES' styles='XScaleAnim,AlphaAnim' /><apply toObject='VDIVLINES' styles='YScaleAnim,AlphaAnim' /> <apply toObject='HGRID' styles='YScaleAnim,AlphaAnim' /></application></styles></chart>";

String issues = "<chart palette='2' caption='' showValues='0' numVDivLines='10' drawAnchors='0' numberPrefix='SAR' divLineAlpha='30' alternateHGridAlpha='20'  setAdaptiveYMin='1' >	<categories><category label='Apr' /> <category label='May' /><category label='Jun' /> <category label='Jul' /><category label='Aug' /> 	<category label='Sep' /><category label='Oct' /> <category label='Nov' /><category label='Dec' /> 	<category label='Jan' /><category label='Feb' /> <category label='Mar' /></categories>	<dataset seriesName='Current Year' color='#4AA02C'><set value='127654' /> <set value='205784' />	<set value='129456' /> <set value='111565' /> <set value='124454' /> <set value='137654' />	<set value='196234' /> <set value='159456' /> <set value='191565' /> <set value='174454' />	<set value='171565' /> <set value='134454' /> </dataset>	<dataset seriesName='Previous Year' color='#6698FF'><set value='127654' /><set value='126234' /><set value='199456' /><set value='111565' /><set value='124454' /><set value='127654' /><set value='119234' /><set value='129456' /><set value='191565' /><set value='114454' /><set value='171565' /><set value='144454' /> </dataset>	<styles><definition><style name='XScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_xScale' /><style name='YScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='XAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='AlphaAnim' type='ANIMATION' duration='0.5' start='0' param='_alpha' /></definition>	<application><apply toObject='CANVAS' styles='XScaleAnim, YScaleAnim,AlphaAnim' /><apply toObject='DIVLINES' styles='XScaleAnim,AlphaAnim' /><apply toObject='VDIVLINES' styles='YScaleAnim,AlphaAnim' /> <apply toObject='HGRID' styles='YScaleAnim,AlphaAnim' /></application></styles></chart>";

String transfer = "<chart palette='2' caption='' showValues='0' numVDivLines='10' drawAnchors='0' numberPrefix='SAR' divLineAlpha='30' alternateHGridAlpha='20'  setAdaptiveYMin='1' >	<categories><category label='Apr' /> <category label='May' /><category label='Jun' /> <category label='Jul' /><category label='Aug' /> 	<category label='Sep' /><category label='Oct' /> <category label='Nov' /><category label='Dec' /> 	<category label='Jan' /><category label='Feb' /> <category label='Mar' /></categories>	<dataset seriesName='Current Year' color='#4AA02C'><set value='27654' /> <set value='25784' />	<set value='19456' /> <set value='11565' /> <set value='14454' /> <set value='17654' />	<set value='16234' /> <set value='19456' /> <set value='11565' /> <set value='14454' />	<set value='17565' /> <set value='13454' /> </dataset>	<dataset seriesName='Previous Year' color='#6698FF'><set value='27654' /><set value='16234' /><set value='9956' /><set value='11565' /><set value='12454' /><set value='17654' /><set value='11934' /><set value='12456' /><set value='19165' /><set value='1454' /><set value='17165' /><set value='14454' /> </dataset>	<styles><definition><style name='XScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_xScale' /><style name='YScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='XAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='AlphaAnim' type='ANIMATION' duration='0.5' start='0' param='_alpha' /></definition>	<application><apply toObject='CANVAS' styles='XScaleAnim, YScaleAnim,AlphaAnim' /><apply toObject='DIVLINES' styles='XScaleAnim,AlphaAnim' /><apply toObject='VDIVLINES' styles='YScaleAnim,AlphaAnim' /> <apply toObject='HGRID' styles='YScaleAnim,AlphaAnim' /></application></styles></chart>";


String worth = "<chart palette='2' caption='' showValues='0' numVDivLines='10' drawAnchors='0' numberPrefix='SAR' divLineAlpha='30' alternateHGridAlpha='20'  setAdaptiveYMin='1' >	<categories><category label='Apr' /> <category label='May' /><category label='Jun' /> <category label='Jul' /><category label='Aug' /> 	<category label='Sep' /><category label='Oct' /> <category label='Nov' /><category label='Dec' /> 	<category label='Jan' /><category label='Feb' /> <category label='Mar' /></categories>	<dataset seriesName='Current Year' color='#4AA02C'><set value='21127654' /> <set value='22205784' />	<set value='27129456' /> <set value='2111565' /> <set value='22124454' /> <set value='22137654' />	<set value='21196234' /> <set value='21159456' /> <set value='25191565' /> <set value='26174454' />	<set value='25171565' /> <set value='22134454' /> </dataset>	<dataset seriesName='Previous Year' color='#6698FF'><set value='21927654' /><set value='22126234' /><set value='22999456' /><set value='2111565' /><set value='24124454' /><set value='26127654' /><set value='25119234' /><set value='25129456' /><set value='21191565' /><set value='21114454' /><set value='2171565' /><set value='23144454' /> </dataset>	<styles><definition><style name='XScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_xScale' /><style name='YScaleAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='XAnim' type='ANIMATION' duration='0.5' start='0' param='_yscale' /><style name='AlphaAnim' type='ANIMATION' duration='0.5' start='0' param='_alpha' /></definition>	<application><apply toObject='CANVAS' styles='XScaleAnim, YScaleAnim,AlphaAnim' /><apply toObject='DIVLINES' styles='XScaleAnim,AlphaAnim' /><apply toObject='VDIVLINES' styles='YScaleAnim,AlphaAnim' /> <apply toObject='HGRID' styles='YScaleAnim,AlphaAnim' /></application></styles></chart>";


%>

<div id="expand" >
<div class="column" >
	<div class="portlet" >
            <div class="portlet-header" style="width:450px;">&nbsp;&nbsp;Stock Purchase Details</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/MSLine.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=purchase %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="450" />
                                <jsp:param name="chartHeight" value="250" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>
    <div class="portlet" >
            <div class="portlet-header" style="width:450px;">&nbsp;&nbsp;Current Stock Worth Details</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/MSLine.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=worth %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="450" />
                                <jsp:param name="chartHeight" value="250" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>

</div>


<div class="column" >
	<div class="portlet" >
            <div class="portlet-header" style="width:450px;">&nbsp;&nbsp;Stock Issue Details</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/MSLine.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=issues %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="450" />
                                <jsp:param name="chartHeight" value="250" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>
    <div class="portlet" >
                <div class="portlet-header" style="width:450px;">&nbsp;&nbsp;Stock Transfer Details</div>
                        <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/MSLine.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=transfer %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="450" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                        </div>
	</div>

</div>

</div><!-- End demo -->
</body>
</html>
