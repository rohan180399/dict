<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
    <body>
        <form name="customerWise" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br><br>
            Vehicle Driver Advance Details
            <br>
            <br>


               <table width="100%" align="center" border="0" id="table" class="sortable">
               <thead>                   
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>Route</h3></th>
                        <th><h3>VehicleNo</h3></th>
                        <th><h3>VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>Driver</h3></th>
                        <th><h3>Already PaidAmount</h3></th>
                        <th><h3>ToBePaid Today</h3></th>
                        <th><h3>Paid</h3></th>
                        <th><h3>action</h3></th>
                    </tr>
                    </thead>
                    <tbody>

                            <tr height="30">
                                <td class="text2" align="left"> 1 </td>
                                <td class="text2"    >Vehicle Driver</td>
                                  <c:if test="${viewVehicleDriverAdvanceList != null}" >
                       <c:forEach items="${viewVehicleDriverAdvanceList}" var="trip">
                                <td class="text2">
                                    <c:out value="${trip.customerName}"/>
                                </td>
                                <td class="text2" align="left"><c:out value="${trip.cNotes}" /></td>
                                <td class="text2" ><c:out value="${trip.tripCode}"/></td>
                                <td class="text2" align="left"> <c:out value="${trip.routeName}"/></td>
                       </c:forEach></c:if>
                                  <c:if test="${viewVehicleDriverAdvanceListSize == '0'}" >
                                <td class="text2" >&nbsp;</td>
                                <td class="text2">
                                   &nbsp;
                                </td>
                                <td class="text2" align="left"> &nbsp;</td>
                                <td class="text2" align="left">&nbsp;</td>
                     </c:if>
                                  <c:if test="${vehicleDriverAdvanceList != null}" >
                                      <c:forEach items="${vehicleDriverAdvanceList}" var="advance">
                                          
                                <td class="text2"  align="left"> <c:out value="${advance.regNo}"/> </td>
                                <td class="text2" align="left"> <c:out value="${advance.vehicleTypeName}" /></td>
                                <td class="text2"  align="left"> <c:out value="${advance.primaryDriverName}"/> </td>
                                <td class="text2"  align="left"> <c:out value="${advance.paidAdvance}"/> </td>
                                <td class="text2" align="left"> <c:out value="${advance.requestedAdvance}" /></td>



                                 <c:if test="${advance.approvalStatus==1}">
                                       <td class="text2" align="left">
                                           Waiting For Approval
                                       </td>
                                 </c:if>
                                 <c:if test="${advance.approvalStatus==3}">
                                        <td class="text2" align="left">
                                   Rejected
                                      </td>
                                 </c:if>
                                 <c:if test="${(advance.paidStatus!='Y')&& (advance.approvalStatus==2)}">
                                     <td class="text2" >-</td>
                                         
                                 <td class="text2" align="left"><a href="/throttle/vehicleDriverAdvancePay.do?tripAdvanceId=<c:out value="${advance.vehicleDriverAdvanceId}"/>&vehicleId=<c:out value="${advance.vehicleId}"/>
                                                                   &approvalStatus=<c:out value="${advance.approvalStatus}"/>&paidStatus=<c:out value="${advance.paidStatus}"/>&regNo=<c:out value="${advance.regNo}"/>
                                                                   &vehicleTypeName=<c:out value="${advance.vehicleTypeName}"/>&primaryDriver=<c:out value="${advance.primaryDriverName}"/>
                                                                   &paidAdvance=<c:out value="${advance.paidAdvance}"/>&requestedAdvance=<c:out value="${advance.requestedAdvance}"/>">pay</a></td>
                                 </c:if>


                              <c:if test="${advance.approvalStatus==3}">
                                       <td class="text2" >
                                      Not Paid
                                      </td>
                              </c:if>

                                <c:if test="${advance.paidStatus=='Y'}">
                                <td class="text2" align="left"><font color="green"><c:out value="${advance.paidAdvance}"/></font></td>
                                <td class="text2" align="left"><font color="green">Paid</font></td>
                                </c:if>

                                <c:if test="${advance.paidStatus !='Y' && advance.approvalStatus==1}">
                                    <td class="text2" align="center"><font color="red">Not Paid</font></td>
                                </c:if>

                                

                              


                            </tr>
                                            </c:forEach></c:if>

            </tbody>
            </table>

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
