package ets.domain.security.business;

import java.util.Date;

/**
 *
 * @author vidya
 */
public class SecurityTO {

    public SecurityTO() {
    }
    String currentDate = "";
    String woDate = "";
    String woId = "";
    String vno = "";
    String vehicleOperationPoint = "";
    String vehicleOperationPointId = "";
    String vehicleTypeName = "";
    String vehicleTypeId = "";
    String modelId = "";
    String vehicleNumber = "";
    String remarks = "";
    String vehicleId = "";
    String purpose = "";
    String intime = "";
    String vehicleKm = "";
    String vehicleDate = "";
    int purposeId = 0;
    String purposeName = "";
    String requestId = "";
    String servicePointId = "";
    String requiredDate = "";
    String servicePointName = "";
    String status = "";
    String userId = "";
    String outtime = "";
    String vehicleFromDate = "";
    String vehicleToDate = "";
    String vehicleInPurpose = "";
    String vehicleStatus = "";
    
    int hourMeter = 0;
    int workKm= 0;
    int workHm= 0;
    int vioId = 0;
    String compId = "";

    public int getWorkHm() {
        return workHm;
    }

    public void setWorkHm(int workHm) {
        this.workHm = workHm;
    }

    public int getWorkKm() {
        return workKm;
    }

    public void setWorkKm(int workKm) {
        this.workKm = workKm;
    }
    

   
    

    public String getVehicleDate() {
        return vehicleDate;
    }

    public void setVehicleDate(String vehicleDate) {
        this.vehicleDate = vehicleDate;
    }

    public String getVehicleKm() {
        return vehicleKm;
    }

    public void setVehicleKm(String vehicleKm) {
        this.vehicleKm = vehicleKm;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }
//vijay
    String km = "";

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String getServicePointName() {
        return servicePointName;
    }

    public void setServicePointName(String servicePointName) {
        this.servicePointName = servicePointName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleOperationPoint() {
        return vehicleOperationPoint;
    }

    public void setVehicleOperationPoint(String vehicleOperationPoint) {
        this.vehicleOperationPoint = vehicleOperationPoint;
    }

    public String getVehicleOperationPointId() {
        return vehicleOperationPointId;
    }

    public void setVehicleOperationPointId(String vehicleOperationPointId) {
        this.vehicleOperationPointId = vehicleOperationPointId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getWoDate() {
        return woDate;
    }

    public void setWoDate(String woDate) {
        this.woDate = woDate;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public int getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(int purposeId) {
        this.purposeId = purposeId;
    }

    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(String purposeName) {
        this.purposeName = purposeName;
    }

    public String getVehicleFromDate() {
        return vehicleFromDate;
    }

    public void setVehicleFromDate(String vehicleFromDate) {
        this.vehicleFromDate = vehicleFromDate;
    }

    public String getVehicleInPurpose() {
        return vehicleInPurpose;
    }

    public void setVehicleInPurpose(String vehicleInPurpose) {
        this.vehicleInPurpose = vehicleInPurpose;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getVehicleToDate() {
        return vehicleToDate;
    }

    public void setVehicleToDate(String vehicleToDate) {
        this.vehicleToDate = vehicleToDate;
    }

    public int getHourMeter() {
        return hourMeter;
    }

    public void setHourMeter(int hourMeter) {
        this.hourMeter = hourMeter;
    }

    public int getVioId() {
        return vioId;
    }

    public void setVioId(int vioId) {
        this.vioId = vioId;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getVno() {
        return vno;
    }

    public void setVno(String vno) {
        this.vno = vno;
    }
    
}
