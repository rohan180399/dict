<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script>
    function saveCategory() {
//        alert("test");
        var errStr = "";
        if (document.getElementById("categoryName").value == "") {
            errStr = "Please select valid Category Name.\n";
            alert(errStr);
            document.getElementById("categoryName").focus();
        }
        else if (document.getElementById("categoryCode").value == "") {
            errStr = "Please select valid Category Code.\n";
            alert(errStr);
            document.getElementById("categoryCode").focus();
        }
        else if (document.getElementById("description").value == "") {
            errStr = "Please select valid Description.\n";
            alert(errStr);
            document.getElementById("description").focus();
        }
        else if (document.getElementById("gstCategoryName").value == "") {
            errStr = "Please select valid GST Category Name.\n";
            alert(errStr);
            document.getElementById("gstCategoryName").focus();
        }
        if (errStr == "") {
            document.gstMaster.action = "/throttle/saveCategoryMaster.do";
            document.gstMaster.method = "post";
            document.gstMaster.submit();
        }
    }
</script>
<script>
    function setValues(sno, categoryId, categoryName, categoryCode, gstCategoryId, description,activeInd) {
        
        

        var count = parseInt(document.getElementById("count").value);
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("categoryId").value = categoryId;
        document.getElementById("categoryName").value = categoryName;
        document.getElementById("categoryCode").value = categoryCode;
        document.getElementById("gstCategoryName").value = gstCategoryId;
        document.getElementById("description").value = description;
        document.getElementById("activeInd").value = activeInd;
        
        $("#categoryName").attr("readonly", "readonly");
        $("#categoryCode").attr("readonly", "readonly");
        
        //        $("#cityName").val().readOnly;
    }
//    function hideFields(){
//        $("#categoryName").attr("readonly", "readonly");
//        $("#categoryCode").attr("readonly", "readonly");
//    }
</script>>


<title> Category Master</title>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active"> Category Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onLoad="document.cityMaster.zoneName.focus();">
                <!--<body onload="initialize(28.3213, 77.5435, 'New Delhi')">-->
                <form name="gstMaster"  method="POST">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                        <input type="hidden" name="categoryId" id="categoryId" value=""  />
                        <tr>
                        <table class="table table-info mb30 table-hover" style="width:100%">
                            <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"> Category Master</td></tr>
                            <tr>
                                <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                            </tr>
                            <tr>
                                <td >&nbsp;&nbsp;<font color="red">*</font>Category Name</td>
                                <td ><input type="text" name="categoryName" id="categoryName" class="form-control" style="width:180px;height:40px;" maxlength="50"></td>
                                <td >&nbsp;&nbsp;<font color="red">*</font>Category Code</td>
                                <td  >
                                    <input type="text" name="categoryCode" id="categoryCode" class="form-control" style="width:180px;height:40px;"    maxlength="50"  style="width:500px;height: 30px;" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>GST Category Name</td>
                                <td><select class="form-control" name="gstCategoryName" id="gstCategoryName" required style="width:180px;height:40px">
                                    <c:if test="${getCategoryName != null}">
                                        <option value="0" >--select--</option>
                                        <c:forEach items="${getCategoryName}" var="catName">
                                            <option value="<c:out value="${catName.gstCategoryId}"/>"><c:out value="${catName.categoryName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                <!--<input type="text" value="<c:out value="${catName.gstCategoryId}"/>">-->
                                </select></td>
                                <td >&nbsp;&nbsp;<font color="red">*</font>Status</td>
                                <td ><select  align="center" class="form-control" name="activeInd" id="activeInd" class="form-control" style="width:180px;height:40px;">
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" style="display: none">In-Active</option>
                            </select></td>
                            </tr>
                            <tr>
                                <td >&nbsp;&nbsp;<font color="red">*</font>Description</td>
                                <td >
                                    <textarea name="description" id="description" class="form-control"   style="width:180px;height:50px;"></textarea>
                                </td>
                            <td><input type="button"  class="btn btn-info" value="Save"  onClick="saveCategory()"></td>
                            </tr>
                        </table>
                    <h2 align="center"> Category Master List</h2>


                    <table  id="table" class="table table-info mb30" style="width:100%" >
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th>S.No</th>
                        <th>Category Name</th>
                        <th>Category Code </th>
                        <th>GST Category Name</th>
                        <th>Descriptions </th>
                        <th>Status</th>
                        <th>Select</th>
                        </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <c:if test = "${CategoryMasterList != null}">
                                <c:forEach items="${CategoryMasterList}" var="catm">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td   align="left"> <%= sno%> </td>
                                        <td   align="left"><c:out value="${catm.categoryName}"/></td>
                                        <td   align="left"> <c:out value="${catm.categoryCode}" /></td>
                                        <td   align="left"><c:out value="${catm.gstCategoryName}"/></td>
                                        <td   align="left"><c:out value="${catm.description}"/></td>
                                        <td   align="left"><c:out value="${catm.activeInd}"/></td>
                                        <td align="center"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${catm.categoryId}" />', '<c:out value="${catm.categoryName}" />', '<c:out value="${catm.categoryCode}" />', '<c:out value="${catm.gstCategoryId}" />', '<c:out value="${catm.description}" />', '<c:out value="${catm.activeInd}" />');" /></td>
                                    </tr>
                                        <!--<td align="center"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${catm.categoryId}" />', '<c:out value="${catm.categoryName}" />', '<c:out value="${catm.categoryCode}" />', '<c:out value="${catm.gstCategoryName}" />', '<c:out value="${catm.description}" />', '<c:out value="${catm.activeInd}" />');hideFields()" /></td>-->
                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                    </table>
                            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>