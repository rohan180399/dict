<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script language="javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
//                     alert("HI"+hi); 
                document.financeDetails.action = '/throttle/handlefinanceAdviceExcel.do?param=ExportExcel';
                document.financeDetails.submit();
            }
            else {
//                     alert("HI"+Search); 
                document.financeDetails.action = '/throttle/handlefinanceAdviceExcel.do?param=Search';
                document.financeDetails.submit();
            }
        }

    }
</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">???????</a>
              </span>-->

<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.ViewFinanceAdvice" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.ViewFinanceAdvice" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="financeDetails" method="post" >
                    <table class="table table-bordered" id="report" >
                        <!--                        <tr height="30" id="index" >
                                                    <td colspan="4"  style="background-color:#5BC0DE;">
                                                            <b><spring:message code="operations.reports.label.ViewFinanceAdvice" text="default text"/></b>
                                                    </td>
                                                </tr>-->

                        <%!
                        public String NullCheck(String inputString)
                             {
                                     try
                                     {
                                             if ((inputString == null) || (inputString.trim().equals("")))
                                                             inputString = "";
                                     }
                                     catch(Exception e)
                                     {
                                                             inputString = "";
                                     }
                                     return inputString.trim();
                             }
                        %>

                        <%

                         String today="";
                         String fromday="";
                           
                         fromday = NullCheck((String) request.getAttribute("fromdate"));
                         today = NullCheck((String) request.getAttribute("todate"));

                         if(today.equals("") && fromday.equals("")){
                         Date dNow = new Date();
                         Calendar cal = Calendar.getInstance();
                         cal.setTime(dNow);
                         cal.add(Calendar.DATE, 0);
                         dNow = cal.getTime();

                         Date dNow1 = new Date();
                         Calendar cal1 = Calendar.getInstance();
                         cal1.setTime(dNow1);
                         cal1.add(Calendar.DATE, -6);
                         dNow1 = cal1.getTime();

                         SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                         today = ft.format(dNow);
                         fromday = ft.format(dNow1);
                         }

                        %>
                        <div id="first">
                            <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>
                                        <td height="30"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate" style="width:260px;height:40px;" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td height="30"><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" style="width:260px;height:40px;" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value="<%=today%>"></td>
                                    </tr>
                                    <tr >


                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">
                                            &nbsp;&nbsp; <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>"></td>

                                    </tr>
                                </table>
                            </td>
                            </tr>
                    </table>
                    <br>

                    <c:if test = "${financeAdviceSize != '0'}" >
                        <table class="table table-info mb30 table-hover" id="table" class="sortable">
                            <thead>
                                <tr >
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.AdviceDate" text="default text"/></th>                            
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.AdviceType" text="default text"/></th>                            
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.NoofTrips" text="default text"/></th>
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.EstimatedAmt" text="default text"/>.</th>
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.RequestedAmt" text="default text"/>.</th>
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.PaidAmt" text="default text"/>.</th>                            
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.View" text="default text"/></th>
                                    <th style="border-left:1px solid white"><spring:message code="operations.reports.label.FinanceDetailsExcel" text="default text"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0,sno = 1;%>
                                <c:forEach items="${financeAdvice}" var="fd">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" ><c:out value="${fd.advicedate}"/> </td>                                

                                        <td align="left" >
                                            <c:if test="${(fd.batchType=='b') || (fd.batchType=='B')}" >
                                                Batch
                                            </c:if>
                                            <c:if test="${(fd.batchType=='a') || (fd.batchType=='A')}" >
                                                Adhoc
                                            </c:if>
                                            <c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" >
                                                Manual
                                            </c:if>
                                        </td>

                                        <td align="left" ><c:out value="${fd.tripday}"/></td>                                
                                        <td align="left" >
                                            <c:if test="${(fd.estimatedadvance=='')||(fd.estimatedadvance==null)}" >
                                                0
                                            </c:if>
                                            <c:if test="${(fd.estimatedadvance!='')||(fd.estimatedadvance!=null)}" >
                                                <c:out value="${fd.estimatedadvance}"/>
                                            </c:if>
                                        </td>
                                        <td align="left" ><c:out value="${fd.requestedadvance}"/></td>
                                        <td align="left" ><c:out value="${fd.actualadvancepaid}"/></td>
                                        <td align="left" >
                                            <a href="/throttle/viewFinanceReportAdviceDetais.do?dateval=<c:out value="${fd.advicedate}"/>&active=<c:out value="${fd.isactive}"/>&type=<c:out value="${fd.batchType}"/>"><spring:message code="operations.reports.label.ViewDetails" text="default text"/></a>
                                        </td>                                
                                        <td align="left" >
                                            <a href="/throttle/handlefinanceAdviceReportExcel.do?dateval=<c:out value="${fd.advicedate}"/>&active=<c:out value="${fd.isactive}"/>&type=<c:out value="${fd.batchType}"/>"><spring:message code="operations.reports.label.ExcelDownload" text="default text"/></a>
                                        </td>                                

                                    </tr>
                                    <%
                                               index++;
                                               sno++;
                                    %>
                                </c:forEach>

                            </tbody>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
