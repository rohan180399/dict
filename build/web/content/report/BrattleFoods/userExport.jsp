<%-- 
    Document   : manageCustomerExport
    Created on : Jan 12, 2021, 5:37:43 PM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="accountReceivable" method="post">
            <%
                            Date dNow = new Date();
                            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                            //System.out.println("Current Date: " + ft.format(dNow));
                            String curDate = ft.format(dNow);
                            String expFile = "CustomerList" + curDate + ".xls";

                            String fileName = "attachment;filename=" + expFile;
                            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                            response.setHeader("Content-disposition", fileName);
            %>

            <br>

            <c:if test="${UserList !=null}">
                <table style="width:auto" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <td><h3>S. No.</h3></td>
                            <td><h3>User Name</h3></td>
                            <td><h3>Role Name</h3></td>
                            <td><h3>Menu Path</h3></td>
                            <td><h3>Permit</h3></td>
                            <td><h3>Active Status</h3></td>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <%int sno=1;
                        int index =0;
                        String classText="";
                        %>
                        <c:forEach items="${UserList}" var="CustomerList">
                            <%int oddEven = index % 2;
                          if (oddEven > 0) {
                                     classText = "text2";
                                 } else {
                                     classText = "text1";
                                 }
                            %>
                            <tr>
                                <td  height="30"><%=sno++%></td>
                                <td  height="30"><c:out value="${CustomerList.userName}"/></td>
                                <td  height="30" align="left"><c:out value="${CustomerList.rolename}"/></td>
                                <td  height="30" align="left"><c:out value="${CustomerList.menupath}"/></td>
                                <td  height="30"><c:out value="${CustomerList.permit}"/></td>
                                <td  height="30"><c:out value="${CustomerList.active}"/></td>
                             
<%--                                <td  height="30">
                                    <c:if test="${(customerList.custStatus =='n') || (customerList.custStatus =='N')}" >
                                        InActive
                                    </c:if>
                                    <c:if test="${(customerList.custStatus =='y') || (customerList.custStatus =='Y')}" >
                                        Active
                                    </c:if>
                                </td>--%>
                            </tr>
                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>

            </c:if>

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
