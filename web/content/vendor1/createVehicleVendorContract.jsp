<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>




    </head>
    <script language="javascript">
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#custName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerName.do",
                        dataType: "json",
                        data: {
                            custName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#custName').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#custName').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        $(document).ready(function() {
            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerCode.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#customerCode').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#customerCode').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });



        function checkVehicle() {
            document.vehicleVendorContract.action = '/throttle/createVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
        function submitPage() {
//            var value=document.getElementById("contractType").value;
//            if (value == '2') {
//              document.getElementById("fixedAmount").value=document.getElementById("fixedAmountAK").value;
//              document.getElementById("rateExtraKm").value=document.getElementById("rateExtraKmAK").value;
//              document.getElementById("fuleExpenseby").value=document.getElementById("fuleExpensebyAK").value;
//              document.getElementById("fuleExpenseby").value=document.getElementById("fuleExpensebyAK").value;
//            }
            document.vehicleVendorContract.action = '/throttle/saveVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
        function checkContractType(value) {
//            alert(value);
            if (value == '1') {
                $("#fixedkmtable").show();
                $("#actualkmtable").hide();
                $("#fixedKmTr").show();
                $("#fixedKmTr1").show();
                $("#actualKmTr").hide();
            } else {
                $("#actualkmtable").show();
                $("#fixedkmtable").hide();
                $("#fixedKmTr1").hide();
                $("#fixedKmTr").hide();
                $("#actualKmTr").show();
            }
        }
    </script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });
    </script>

    <body onload="checkContractType(1);">
        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>

            <table width="980" align="center" border="0" id="table2" >
                <tr>
                    <td class="contenthead" colspan="4" >Create Vehicle Vendor</td>
                </tr>
                <tr>
                    <td height="30"><font color="red">*</font>Start Date</td>
                    <td><input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker"></td>
                <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}" />" ></td>

                <td height="30"><font color="red">*</font>End Date</td>
                <td><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker"></td>
                </tr>
                <tr>
                    <td height="30"><font color="red">*</font>Vehicle Type</td>
                    <td>
                        <select class="form-control" name="vehicleTypeId" id="vehicleTypeId" onchange="checkVehicle();">
                            <option value="0">---Select---</option>
                            <c:if test = "${TypeList != null}" >
                                <c:forEach items="${TypeList}" var="Type">
                                    <option value='<c:out value="${Type.vehicleTypeId}" />'><c:out value="${Type.vehicleTypeName}" /></option>
                                </c:forEach >
                                <script>
                                    document.getElementById('vehicleTypeId').value = '<c:out value="${vehicleTypeId}"/>'
                                </script>
                            </c:if>
                        </select>
                    </td>
<!--                    <td height="30"><font color="red">*</font>Vehicle No</td>
                    <td>
                        <select name="vehicleId" id="vehicleId" class="form-control">
                            <option value="0">-select-</option>
                            <c:if test = "${vehicleList != null}" >
                                <c:forEach items="${vehicleList}" var="reg">
                                        <option value='<c:out value="${reg.vehicleId}" />'> <c:out value="${reg.vehicleNo}" /></option>
                                </c:forEach>
                            </c:if>
                               <script>
                                    document.getElementById('vehicleId').value = '<c:out value="${vehicleId}"/>'
                                </script>          
                        </select>
                    </td>-->
                </tr>
                <tr>
                    <td height="30"><font color="red">*</font>Contract Type</td>
                    <td>
                        <select name="contractType" id="contractType" class="form-control" onchange="checkContractType(this.value);">
                            <!--<option value="0" selected>--Select--</option>-->
                            <option value="1" >Fixed Km</option>
                            <option value="2" >Actual Km</option>
                        </select>
                    </td>
                    <td height="30"><font color="red">*</font>Operation Type</td>
                    <td>
                        <select name="operationType" id="operationType" class="form-control">
                            <!--<option value="0" selected>--Select--</option>-->
                            <option value="1" >Primary</option>
                            <option value="2" >Secondary</option>
                        </select>
                    </td>

                </tr>
                <tr>
                    <!--                <div id="fixedkmtable">-->
                    <td class="contenthead" id="fixedkmtable" colspan="4" >Terms & Conditions (if fixed km)</td> 
                    <!--</div>-->
                    <!--<div id="actualkmtable">-->
                    <td class="contenthead" id="actualkmtable" colspan="4" >Terms & Conditions (if Actual km)</td> 
                    <!--</div>-->
                </tr>
                <tr id="fixedKmTr">
                    <td height="30"><font color="red">*</font>Fixed km</td>
                    <td><input type="text" name="fixedKm" id="fixedKm" value="0" class="form-control"></td>
                    <td height="30"><font color="red">*</font>Fixed Amount</td>
                    <td><input type="text" name="fixedAmount" id="fixedAmount" value="0" class="form-control"></td>
                </tr> 
                <tr id="fixedKmTr1">
                    <td height="30"><font color="red">*</font>Rate/Extra Km</td>
                    <td><input type="text" name="rateExtraKm" id="rateExtraKm" value="0" class="form-control"></td>
                    <td height="30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr id="actualKmTr">
                    <td height="30"><font color="red">*</font>Rate per Km</td>
                    <td><input type="text" name="ratePerKm" id="ratePerKm" value="0" class="form-control"></td>
                    <td height="30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td height="30"><font color="red">*</font>Driver Reponsibility</td>
                    <td>
                        <select name="driverReponsibility" id="driverReponsibility" class="form-control">
                            <option value="1" >Interem</option>
                            <option value="2" >vendor</option>
                        </select> 
                    </td>
                    <td height="30">Fuel & Expense By</td>
                    <td> 
                        <select name="fuleExpenseby" id="fuleExpenseby" class="form-control">
                            <option value="1" >Interem</option>
                            <option value="2" >vendor</option>
                        </select> 
                    </td>
                </tr>
                <tr>
                    <td height="30"><font color="red">*</font>Payment Type</td>
                    <td>
                        <select name="paymentType" id="paymentType" class="form-control">
                            <option value="1" >Trip Wise</option>
                            <option value="2" >Monthly</option>
                        </select> 
                    </td>
                    <td height="30">Payment Schedule Days</td>
                    <td> 
                        <input type="text" name="paymentScheduleDays" id="paymentScheduleDays" value="" class="form-control">
                    </td>
                </tr>
                <!--                <table id="actualkmtable" width="980" align="center"  cellpadding="0" cellspacing="0" style="visibility:hidden">
                                    <th>Terms & Conditions (Actual km)</th> 
                                    <tr>
                                        <td height="30"><font color="red">*</font>Fixed Amount</td>
                                        <td><input type="text" name="fixedAmountAK" id="fixedAmountAK" value="" class="form-control"></td>
                                        <td height="30"><font color="red">*</font>Rate/Extra Km</td>
                                        <td><input type="text" name="rateExtraKmAK" id="rateExtraKmAK" value="" class="form-control"></td>
                                    </tr> 
                                    <tr>
                                        <td height="30">Fuel & Expense By</td>
                                        <td> 
                                            <select name="fuleExpensebyAK" id="fuleExpensebyAK" class="form-control">
                                                <option value="0" selected>--Select--</option>
                                                <option value="1" >TTK</option>
                                                <option value="2" >vendor</option>
                                            </select> 
                                        </td>
                                        <td height="30">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>Payment Type</td>
                                        <td>
                                            <select name="paymentTypeAK" id="paymentTypeAK" class="form-control">
                                                <option value="0" selected>--Select--</option>
                                                <option value="1" >Trip Wise</option>
                                                <option value="2" >Monthly</option>
                                            </select> 
                                        </td>
                                        <td height="30">Payment Schedule Days</td>
                                        <td> 
                                            <input type="text" name="paymentScheduleDaysAK" id="paymentScheduleDaysAK" value="" class="form-control">
                                        </td>
                                    </tr>
                                </table>  -->
                <tr>
                    <td colspan="4" align="center">
                        <input type="button"   value="SAVE" class="button" name="save" onClick="submitPage();">
                    </td>
                </tr>
            </table>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
