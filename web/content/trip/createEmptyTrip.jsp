<%--
    Document   : createEmptyTrip
    Created on : Jan 20, 2014, 2:59:01 PM
    Author     : Arul
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script language="">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityFromName.do",
                            dataType: "json",
                            data: {
                                cityFrom: request.term,
                                cityToId: document.getElementById('cityToId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if (items == '') {
                                    $('#cityFromId').val('');
                                    $('#cityFrom').val('');
                                    $('#cityToId').val('');
                                    $('#cityTo').val('');
                                } else {
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        $('#cityTo').focus();
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                $('#cityTo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityToName.do",
                            dataType: "json",
                            data: {
                                cityTo: request.term,
                                cityFromId: document.getElementById('cityFromId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if (items == '') {
                                    $('#cityToId').val('');
                                    $('#cityTo').val('');
                                } else {
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityToId').val(tmp[0]);
                        $('#cityTo').val(tmp[1]);
                        $('#emptyTripRemarks').focus();
                        fetchRouteInfo();
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                }

            });


            var httpRequest;
            function checkRouteCode() {
                var cityFromId = document.getElementById('cityFromId').value;
                var cityToId = document.getElementById('cityToId').value;
                if (cityFromId != '' && cityToId != '') {
                    var url = '/throttle/checkRouteDetails.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
                    if (window.ActiveXObject) {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } else if (window.XMLHttpRequest) {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() {
                        processRequest();
                    };
                    httpRequest.send(null);
                }
            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        $("#showHideRouteStatus").show();
                        if (val != "" && val != 'null') {
                            $("#routeStatus").text('Route Exists Code is :' + val);
                        } else {
                            $("#routeStatus").text('Route Does Not Exists');
                            $("#showHideRouteKm").show();
                            $("#showHideRouteExpense").show();
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }



            var httpReq;
            var temp = "";
            function fetchRouteInfo() {
                var originId = document.getElementById('cityFromId').value;
                var destinationId = document.getElementById('cityToId').value;
                var vehicleTypeId = document.getElementById('vehicleTypeId').value;
                if (originId != '') {
                    var url = "/throttle/checkPreStartRoute.do?preStartLocationId=" + originId + "&originId=" + destinationId + "&vehicleTypeId=" + vehicleTypeId;

                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() {
                        processFetchRouteCheck();
                    };
                    httpReq.send(null);
                }

            }

            function processFetchRouteCheck()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        //alert(temp);
                        if (temp != '' && temp != null && temp != 'null') {
                            var tempVal = temp.split('-');
                            $("#totalKm").val(tempVal[0]);
                            $("#totalHours").val(tempVal[1]);
                            $("#totalMinutes").val(tempVal[2]);
                            $("#expense").val(tempVal[3]);
                            document.getElementById("totalKm").readOnly = true;
                            document.getElementById("totalHours").readOnly = true;
                            document.getElementById("totalMinutes").readOnly = true;
                            document.getElementById("expense").readOnly = true;
                            $("#showHideRouteStatus").hide();
                            $("#routeStatus").text('');
                        } else {
                            $("#showHideRouteStatus").show();
                            $("#routeStatus").text('Route Does Not Exists');
                            document.getElementById("totalKm").readOnly = true;
                            document.getElementById("totalHours").readOnly = true;
                            document.getElementById("totalMinutes").readOnly = true;
                            document.getElementById("expense").readOnly = true;
                        }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }
            //end ajax for pre location

            function submitPage() {
                    if ($("#routeStatus").text() != '')  {
                        alert("Please Check " + $("#routeStatus").text());
                        return;
                    }else if ((document.getElementById('driver1Name').value == '') && (document.getElementById('driver1Id').value == '')) {
                        alert("Please configure the vehicle driver mapping for vehicle " + document.getElementById('vehicleNo').value);
                        return;
                    } else if (document.getElementById('startDate').value == '') {
                        alert("Please select trip start date");
                        document.getElementById('startDate').focus();
                        return;
                    }else if (document.getElementById('cityFromId').value == '' || document.getElementById('cityFrom').value == '') {
                        alert("choose the from location");
                        document.getElementById('cityFrom').focus();
                        return;
//                    }else if (document.getElementById('cityFromId').value == '' || document.getElementById('cityFrom').value != '') {
//                        alert("Invalid from location");
//                        document.getElementById('cityFrom').focus();
//                        return;
                    } else if (document.getElementById('cityToId').value == '' || document.getElementById('cityTo').value == '') {
                        alert("choose the to location");
                        document.getElementById('cityTo').focus();
                        return;
//                    } else if (document.getElementById('cityToId').value == '' || document.getElementById('cityTo').value != '') {
//                        alert("Invalid the to location");
//                        document.getElementById('cityTo').focus();
//                        return;
                    } else if (document.getElementById('totalKm').value == '') {
                        alert("Please Enter total km");
                        document.getElementById('totalKm').focus();
                        return;
                    } else if (document.getElementById('totalHours').value == '') {
                        alert("Please Enter total hours");
                        document.getElementById('totalHours').focus();
                        return;
                    } else if (document.getElementById('expense').value == '') {
                        alert("Please Enter trip expense");
                        document.getElementById('expense').focus();
                        return;
                    }else if (document.getElementById('purpose').value == '') {
                        alert("Please select the Purpose");
                        document.getElementById('purpose').focus();
                        return;
                    }else {
                        $("#save").hide();
                        document.trip.action = "/throttle/saveEmptyTripSheet.do";
                        document.trip.submit();
                    }
                }


        </script>





    </head>


    <body>

        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>




            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td class="contenthead" colspan="6" >Trip Details</td>
                </tr>
                <tr>
                    <td class="text2" colspan="6" id="showHideRouteStatus" style="display: none" align="center"><label id="routeStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">Customer</td>
                    <td class="text1"> <select name="customer" id="customer">
                            <c:if test="${customerDetails != null}">
                              <c:forEach items="${customerDetails}" var="customerDetails">
                                <option value='<c:out value="${customerDetails.customerId}"/>'><c:out value="${customerDetails.customerName}"/></option>
                                </c:forEach>
                            </c:if>


                </select></td>
                <td class="text1"> Empty Trip Purpose </td>
                <td class="text1">
                    <select name="purpose" id="purpose">
                        <option value='0'>--Select--</option>
                        <option value='1'>For R&M</option>
                        <option value='2'>Before Load</option>
                        <option value='3'>After Load</option>
                        <option value='4'>Other</option>
                    </select>
                </td>
                </tr>
                <tr>
                    <td class="text2">Trip Start Date</td>
                    <td class="text2"><input type="text" name="startDate" Id="startDate" class="datepicker" value=''></td>
                    <td class="text2">Trip Start Time</td>
                    <td class="text2">HH: <select name="startHour"  id="startHour">
                            <option value='00'>00</option>
                            <option value='01'>01</option>
                            <option value='02'>02</option>
                            <option value='03'>03</option>
                            <option value='04'>04</option>
                            <option value='05'>05</option>
                            <option value='06'>06</option>
                            <option value='07'>07</option>
                            <option value='08'>08</option>
                            <option value='09'>09</option>
                            <option value='10'>10</option>
                            <option value='11'>11</option>
                            <option value='12'>12</option>
                            <option value='13'>13</option>
                            <option value='14'>14</option>
                            <option value='15'>15</option>
                            <option value='16'>16</option>
                            <option value='17'>17</option>
                            <option value='18'>18</option>
                            <option value='19'>19</option>
                            <option value='20'>20</option>
                            <option value='21'>21</option>
                            <option value='22'>22</option>
                            <option value='23'>23</option>


                        </select>
                        MI: <select name="startMinute"  id="startMinute" >
                            <option value='00'>00</option>
                            <option value='01'>01</option>
                            <option value='02'>02</option>
                            <option value='03'>03</option>
                            <option value='04'>04</option>
                            <option value='05'>05</option>
                            <option value='06'>06</option>
                            <option value='07'>07</option>
                            <option value='08'>08</option>
                            <option value='09'>09</option>
                            <option value='10'>10</option>
                            <option value='11'>11</option>
                            <option value='12'>12</option>
                            <option value='13'>13</option>
                            <option value='14'>14</option>
                            <option value='15'>15</option>
                            <option value='16'>16</option>
                            <option value='17'>17</option>
                            <option value='18'>18</option>
                            <option value='19'>19</option>
                            <option value='20'>20</option>
                            <option value='21'>21</option>
                            <option value='22'>22</option>
                            <option value='23'>23</option>
                            <option value='24'>24</option>
                            <option value='25'>25</option>
                            <option value='26'>26</option>
                            <option value='27'>27</option>
                            <option value='28'>28</option>
                            <option value='29'>29</option>
                            <option value='30'>30</option>
                            <option value='31'>31</option>
                            <option value='32'>32</option>
                            <option value='33'>33</option>
                            <option value='34'>34</option>
                            <option value='35'>35</option>
                            <option value='36'>36</option>
                            <option value='37'>37</option>
                            <option value='38'>38</option>
                            <option value='39'>39</option>
                            <option value='40'>40</option>
                            <option value='41'>41</option>
                            <option value='42'>42</option>
                            <option value='43'>43</option>
                            <option value='44'>44</option>
                            <option value='45'>45</option>
                            <option value='46'>46</option>
                            <option value='47'>47</option>
                            <option value='48'>48</option>
                            <option value='49'>49</option>
                            <option value='50'>50</option>
                            <option value='51'>51</option>
                            <option value='52'>52</option>
                            <option value='53'>53</option>
                            <option value='54'>54</option>
                            <option value='55'>55</option>
                            <option value='56'>56</option>
                            <option value='57'>57</option>
                            <option value='58'>58</option>
                            <option value='59'>59</option>

                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="text1">Origin</td>
                    <td class="text1"><input type="hidden" name="origin" Id="cityFromId" class="textbox" value=''>
                        <input type="text" name="cityFrom" Id="cityFrom" class="textbox" value='' onchange="resetCityFromId()">
                    </td>
                    <td class="text1">Destination</td>
                    <td class="text1"><input type="hidden" name="destination" Id="cityToId" class="textbox" value=''>
                        <input type="text" name="cityTo" Id="cityTo" class="textbox" value='' onchange="resetCityToId()"></td>
                </tr>
                <script>
                        function resetCityFromId(){
                            if(document.getElementById("cityFromId").value == ""){
                                $("#cityFrom").val('');
                            }
                        }
                        function resetCityToId(){
                            if(document.getElementById("cityToId").value == ""){
                                $("#cityTo").val('');
                            }
                        }
                </script>
                <tr>
                    <td class="text2">Vehicle No</td>
                    <td class="text2">
                        <input type="hidden" name="vehicleNo" Id="vehicleNo" class="textbox" value="<c:out value="${vehicleNo}"/>">
                        <input type="hidden" name="tripType" Id="tripType" class="textbox" value="<c:out value="${tripType}"/>">
                        <c:out value="${vehicleNo}"/>
                    </td>
                <input type="hidden" name="vehicleId" Id="vehicleId" class="textbox" value="<c:out value="${vehicleId}"/>">
                <input type="hidden" name="vehicleTypeId" Id="vehicleTypeId" class="textbox" value="<c:out value="${vehicleTypeId}"/>">
                <input type="hidden" name="vehicleTypeName" Id="vehicleTypeName" class="textbox" value="<c:out value="${vehicleTypeName}"/>">
                <td class="text2">Vehicle Capacity (MT)</td>
                <td class="text2">
                    <input type="hidden" name="vehicleTonnage" Id="vehicleTonnage" readonly class="textbox" value="<c:out value="${vehicleTonnage}"/>">
                    <input type="hidden" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="textbox" value="0"><c:out value="${vehicleTonnage}"/>
                </td>
                </tr>

                <tr>
                    <td class="text1">Primary Driver </td>
                    <td class="text1">
                        <input type="hidden" name="driver1Name"  readonly  Id="driver1Name" class="textbox" value="<c:out value="${primaryDriverName}"/>"  ><c:out value="${primaryDriverName}"/>
                        <input type="hidden" name="driver1Id" Id="driver1Id" class="textbox" value="<c:out value="${primaryDriverId}"/>"  >
                    </td>
                    <td class="text1">Secondary Driver(s) </td>
                    <td class="text1" colspan="5" >
                        <input type="hidden" name="driver2Name"  readonly Id="driver2Name" class="textbox" value="<c:out value="${secondaryDriver1Name}"/>"  >
                        <input type="hidden" name="driver2Id" Id="driver2Id" class="textbox" value="<c:out value="${secondaryDriver1Id}"/>"  >
                        <input type="hidden" name="driver3Name"  readonly  Id="driver3Name" class="textbox" value="<c:out value="${secondaryDriver2Name}"/>"  >
                        <input type="hidden" name="driver3Id" Id="driver3Id" class="textbox" value="<c:out value="${secondaryDriver2Id}"/>"  ><c:out value="${secondaryDriver1Name}"/>,<c:out value="${secondaryDriver2Name}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2">Travel Km</td>
                    <td class="text2"><input type="text" name="totalKm" id="totalKm" class="textbox" value="" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                    <td class="text2">Travel Time</td>
                    <td class="text2">Hours:<input type="text" name="totalHours" Id="totalHours" class="textbox" value=""  style="width: 40px" onKeyPress="return onKeyPressBlockCharacters(event);">Minutes:<input type="text" name="totalMinutes" Id="totalMinutes" class="textbox" value=""  style="width: 40px" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                </tr>
                <tr>
                    <td class="text1">Expense</td>
                    <td class="text1"><input type="text" name="expense" Id="expense" class="textbox" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                    <td class="text1">Remarks</td>
                    <td><textarea name="emptyTripRemarks" id="emptyTripRemarks"></textarea></td>
                </tr>
                <tr><td colspan="6" align="center"><input type="button" class="button" id="save" name="Save" value="Create Empty Trip" style="width: 150px" onclick="submitPage()"/></td></tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>