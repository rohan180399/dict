<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
         <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
  
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addDesigna.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterDesig.do';
            }
            document.desigDetail.submit();
        }
    </script>
    <script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Designation" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>



        </ol>
      </div>
      </div>
             <div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
      <body onload="setImages(1,1,0,0,0,0);document.desigDetail.desigName.focus();">        
        <form method="post" name="desigDetail">
            
  <%@ include file="/content/common/message.jsp" %>

             
 <%
            int index = 0;
            ArrayList DesignaList = (ArrayList) session.getAttribute("DesignaList");
%>    
            <c:if test = "${DesignaList != null}" >
                 <table class="table table-info mb30 table-hover"  id="table">
                <thead>
              
                    <tr align="center" height="43">
                        <th align="center" height="25" ><div ><center><spring:message code="hrms.label.SNo" text="default text"/></center></div></th>
                        <th align="center" height="25" ><div ><center><spring:message code="hrms.label.DesignationName" text="default text"/></center></div></th>
                        <th align="center" height="25" ><div ><center><spring:message code="hrms.label.Description" text="default text"/></center></div></th>
                        <th align="center" height="25" ><div ><center><spring:message code="hrms.label.Status" text="default text"/></center></div></th>
                        <th align="center" height="25" ><div ><center>&nbsp;<spring:message code="hrms.label.Grade" text="default text"/></center></div></th>
                    </tr>
                </thead>
                    <%

                    %>
                    <tbody>
                    <c:forEach items="${DesignaList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="38" align="center"> 
                            <td  align="left" height="30"><%=index + 1%></td>
                            <td  align="left"  height="30"><input type="hidden" name="designationId" value='<c:out value="${list.desigId}"/>'> <c:out value="${list.desigName}"/></td>
                            <td  align="left" height="30"><c:out value="${list.description}"/></td>
                            <td  align="left" height="30">                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                            <td  height="30"><a href="/throttle/viewGrade.do?designationId=<c:out value="${list.desigId}"/>&desigName=<c:out value="${list.desigName}"/>" class="href"><spring:message code="hrms.label.Grade" text="default text"/></a></td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    </tbody>
                </table>
            </c:if> 
            
            <center>
                <br>
                <table class="table table-info mb30 table-hover" >
                <tr>
                    <td align="center">
                        <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.add" text="default text"/>" name="add" onClick="submitPage(this.name)"/>
<!--                    </td>
                    <td align="center">-->&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.modify" text="default text"/>" name="modify" onClick="submitPage(this.name)"/>
                    </td>
                </tr>
 </table>
                <input type="hidden" name="reqfor" value="">
            </center>
             <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <br><br>
                 <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/><span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
          
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>