/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.web;

/**
 *
 * @author HP
 */
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class GenerateQrCode {

    /**
     * @param args the command line arguments
     */
    //static function that creates QR Code  
    public static int generateQRcode(String data, String path, String charset, Map map, int h, int w) throws WriterException, IOException {
//the BitMatrix class represents the 2D matrix of bits  
//MultiFormatWriter is a factory class that finds the appropriate Writer subclass for the BarcodeFormat requested and encodes the barcode with the supplied contents.  
        int status = 0;
        try {
            BitMatrix matrix = new MultiFormatWriter().encode(new String(data.getBytes(charset), charset), BarcodeFormat.QR_CODE, w, h);
            MatrixToImageWriter.writeToFile(matrix, path.substring(path.lastIndexOf('.') + 1), new File(path));

        } catch (Exception ex) {
            System.out.println("ex-generateQRcode--" + ex);
        }
        return status;
    }
//main() method  

    public static int setPathQRCode(String invoicecode,String qrcode) throws WriterException, IOException, NotFoundException {
//data that we want to store in the QR code  
        int qrStatus = 0;
        String str = "";
         
        try {
//path where we want to get QR Code  
            System.out.println("hiii=================XXXXXXXXXXXXXXXXXXXX");
            str = qrcode;
            System.out.println("qrcode---"+str);
            String path = "/var/lib/tomcat7/webapps/Qrimages/QR"+invoicecode+".png";
//            String path = "D:/projects/DICT/dict/Qrimages/QR"+invoicecode+".png";
//            String path = "C:/GIT-Projects/dict/Qrimages/QR"+invoicecode+".png";
//Encoding charset to be used  
            String charset = "UTF-8";
            Map<EncodeHintType, ErrorCorrectionLevel> hashMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
//generates QR code with Low level(L) error correction capability  
            hashMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
//invoking the user-defined method that creates the QR code  
            generateQRcode(str, path, charset, hashMap, 400, 400);//increase or decrease height and width accodingly   
//prints if the QR code is generated   
            System.out.println("QR Code created successfully.");
        } catch (Exception ex) {
            System.out.println("ex---" + ex);
        }
        return qrStatus;
    }

}
