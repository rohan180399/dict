<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<title>JobCard</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.operation.business.OperationTO" %> 
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <%@ page import="java.util.*" %> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
</head>


        <script>
            
            
            
            function print(ind)
            {       
                var DocumentContainer = document.getElementById('print'+ind);
                var WindowObject = window.open('', "TrackHistoryData", 
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }            
            
            
        </script>
<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  

	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">Arabic</a>
	  </span>


<body>
<form name="mpr"  method="post" >                        
            <br>
            
            
            <%
            int index = 0;
            int sno = 0;

            int service = 0;
            ArrayList cardDetail = new ArrayList();
            cardDetail = (ArrayList) request.getAttribute("DetailsList");
            int jobcardId = (Integer) request.getAttribute("jobcardId");
            String[] problemNames,symptoms;
            String techTemp[] = null;
            String techNames = "";
            OperationTO operationTO = new OperationTO();

            int jlistSize = 36;
            int secNameLimit = 30;
            int probLimit = 30;

            System.out.println("jobcard in jsp=" + cardDetail.size());

            String secName = "";
            String probName = "";




            int cardDet = cardDetail.size();
            if (cardDet != 0) {
                for (int i = 0; i < cardDet; i = i + jlistSize) {
            %>
<div id="print<%= i %>">

<style type="text/css">
table {
font:normal 12px arial;
}

h1 {
font-size:18px;
font-weight:normal;
padding:0px;
margin:0px;
}

.dashed {
border:1px dashed #000;
border-bottom-style:dashed;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}
.line {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}

.rgline {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:solid;
border-left-style:none;
}
</style>
<script type="text/javascript" src="/throttle/js/code39.js"></script>
<style type="text/css">
    #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
</style>



<table width="600" height="785" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
<td>
    <table width="100%" >
        <tr>
            <td>
                <img src="images/tahoos_logo.png" alt="tahoos" style="height:100px;width:190px;margin-left: 0px;"/>
            </td>
            <td align="right" >
                <div id="externalbox" style="width:3in">
                    <div id="inputdata" ></div>
                </div>
            </td>
        </tr>
    </table>
</td>

</tr>

		<%
		
		ArrayList details = new ArrayList();
		details = (ArrayList) request.getAttribute("details");
		OperationTO opTO = new OperationTO();
		OperationTO headContent = new OperationTO();
                if(details.size() != 0){
		headContent = (OperationTO) details.get(0);
                }
		%>

<tr>
<td valign="top" height="30">
					<!-- job card &amp; reg no -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="224" height="30" align="left"><h1><spring:message code="service.label.BayNo"  text="default text"/>:
                                        <%=headContent.getBayNo()%></h1></td>
					<td width="300" height="30" align="center"><h1>
                                                <% if("0".equals(headContent.getTrailerId())){ %>
                                                Truck &nbsp;
                                                <% }else{ %>
                                                Trailer &nbsp;
                                                <% } %>

                                                <spring:message code="service.label.JobCard"  text="default text"/>:<%=request.getAttribute("jcMYFormatNo")%>
                                            </h1></td>
					<td width="176" align="right"><h1> <%= headContent.getRegno() %></h1></td>
					</tr>
                                        <tr>

                                        <td width="224" height="30" align="left"><h1><%=headContent.getPriority1()%></h1></td>
					<td width="300" height="30" align="center"><h1><%=headContent.getCompName()%></h1></td>
					<td width="176" align="right"><h1> <%= headContent.getServicetypeName() %></h1></td>

                                        </tr>
					</table>


</td>
</tr>

<tr>
<td valign="top">
					<!-- Customer Details -->
                                        &nbsp;
                                        <div style="display:none;">
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="120" height="20" valign="bottom"><b><spring:message code="service.label.CustomerName"  text="default text"/> :</b></td>
					<td width="187" height="20" class="dashed"><%= headContent.getCustName() %></td>
					<td width="100" height="20"><b><spring:message code="service.label.Date"  text="default text"/>:</b></td>
					<td width="147" height="20" class="dashed"> <%=headContent.getCreatedDate()%></td>
					</tr>
					<tr>
					<td width="120" height="20" valign="bottom"><b><spring:message code="service.label.CustomerAddress"  text="default text"/> :</b>:</td>
					<td width="187" class="dashed">
                                            <%= headContent.getCustAddress() %>
                                            <%= headContent.getCustCity() %>
                                            <%= headContent.getCustState() %>
                                            Phone:<%= headContent.getCustPhone() %>
                                            Mobile:<%= headContent.getCustMobile() %>
                                            Email<%= headContent.getCustEmail() %>
                                        </td>
					<td width="100" height="20" valign="bottom"><b><spring:message code="service.label.InTime"  text="default text"/> :</b></td>
					<td width="147" class="dashed" valign="bottom"><%= headContent.getIntime() %></td>
					</tr>
					<tr>
					<td width="120" height="30" valign="bottom"><b><spring:message code="service.label.OperationArea"  text="default text"/> :</b></td>
					<td width="187"  valign="bottom"><%= headContent.getUsageName() %> </td>
					<td width="100" height="30" valign="bottom">&nbsp;</td>
                                        <td width="147" >&nbsp;</td>
					</tr>
					</table>
                                        </div>
</td>
</tr>
<tr>
<td valign="top" >
					<!-- vehicle details -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="2" class="rgline" align="left" height="35" style="padding-left:5px; " ><h1>
                                                <% if("0".equals(headContent.getTrailerId())){ %>
                                                Truck Details;
                                                <% }else{ %>
                                                Trailer Details;
                                                <% } %>
                                            </h1></td>
					<td colspan="2" class="line"  align="left" style="padding-left:5px; "><h1><spring:message code="service.label.JobCardDetails"  text="default text"/>:</h1></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " >Make / Model:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getMfrName() %>-<%= headContent.getModelName() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.CurrentKM"  text="default text"/> :</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= request.getAttribute("km") %> KM</b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " ><spring:message code="service.label.RegistrationNo"  text="default text"/>:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getRegno() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.TotalKM"  text="default text"/>:</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getTotalKm() %> KM</b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " ><spring:message code="service.label.RegistrationDate"  text="default text"/>:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getSaleDate() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.serviceType"  text="default text"/>:</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getServicetypeName() %></b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " ><spring:message code="service.label.ChassisNo"  text="default text"/>:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getChassNo() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.Driver"  text="default text"/>:</td>
					<td width="175" height="20" class="line" style="padding-left:5px; "><b>Mr.<%= headContent.getDriverName() %></b></td>
					</tr>
					<tr >
					<td width="110" height="20" class="line" style="padding-left:5px; " ><spring:message code="service.label.EngineNo"  text="default text"/>:</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; "><b><%= headContent.getEngineNo() %></b></td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.ESTDelivery"  text="default text"/>:</td>
                                        <td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getScheduledDate() %></b>&nbsp;</td>
					</tr>
					<tr>
                                        <td width="110" height="20" class="line" style="padding-left:5px; " >&nbsp;</td>
					<td width="165" height="20" class="rgline" style="padding-left:5px; ">&nbsp;</td>
					<td width="100" height="20" class="line" style="padding-left:5px; "><spring:message code="service.label.WorkArea"  text="default text"/> :</td>
                                        <td width="175" height="20" class="line" style="padding-left:5px; "><b><%= headContent.getServiceLocation() %></b>&nbsp;</td>
					</tr>
					<tr>
					<td colspan="4" height="30" valign="top" style="padding:5px; ">
					<b><spring:message code="service.label.JobCardCreator"  text="default text"/>:</b> Mr.<%=request.getAttribute("jobCardUser")%>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td valign="top">
					<!-- complaints -->
					<table width="630"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="4" class="line" align="left" height="35" style="padding-left:5px; " ><h1><spring:message code="service.label.Complaints"  text="default text"/></h1></td>
					</tr>
					<tr >
					<td width="40" height="20" class="rgline" style="padding-left:5px; " ><b><spring:message code="service.label.S.No"  text="default text"/></b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b><spring:message code="service.label.Complaints"  text="default text"/>:</b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b><spring:message code="service.label.Descriptions"  text="default text"/>:</b></td>
					<td width="205" height="20" class="rgline" align="center" style="padding-left:5px; "><b><spring:message code="service.label.Technician"  text="default text"/>:</b></td>
					</tr>
					 <%
                                         String identifiedBy = "";
                                    for (int j = i; (j < cardDetail.size() ) && (j < jlistSize + i) ; j++) {
                                        operationTO = new OperationTO();
                                        operationTO = (OperationTO) cardDetail.get(j);
                                        secName = operationTO.getSecName();
                                        identifiedBy = operationTO.getIdentifiedby();
                                        problemNames = operationTO.getProblemNameSplit();
                                        symptoms = operationTO.getSymptomsSplit();
                                        techNames = operationTO.getTechnicianName();
                                        System.out.println("techNames:"+techNames);
                                        if(!"".equals(techNames) && techNames != null) {
                                            techTemp = techNames.split("~");
                                            techNames = techTemp[1];
                                        }else {
                                            techNames = "Not Assigned";
                                        }
                                        if (operationTO.getSecName().length() > secNameLimit) {
                                            secName = secName.substring(0, secNameLimit - 1);
                                        }

                                        if( operationTO.getProbName().equals("WORKORDERCOMPLAINT") ){ %>
                                <tr>
                                    <Td valign="top" colspan="4" width="750" align="left" HEIGHT="20" class="line" style="border:1px; padding-left :6px;"   > <STRONG><spring:message code="service.label.IdentifiedComplaints"  text="default text"/> </STRONG> </td>
                                </tr>                                                                                                                                  
                                        
                                        <% }else if( operationTO.getProbName().equals("JOBCARDCOMPLAINT") ){ %>
                                <tr>
<!--                                    <Td  colspan="4" align="left"  width="750"  HEIGHT="20"  class="line" style="border:1px;  padding-left :6px;"  > -->
                                        <td colspan="4" class="rgline" align="left" style="padding-left:5px; ">
                                        <STRONG><spring:message code="service.label.CustomerComplaints"  text="COMPLAINTS"/>  </STRONG> </td>
                                </tr>                                                                                                                                                                  
                                        <% }else if( operationTO.getProbName().equals("PERIODICSERVICE") ){
                                            sno--;
                                        %>
                                <tr>
<!--                                    <Td valign="top" colspan="4" align="left"  width="750"  HEIGHT="20" class="line"  style="border:1px;  padding-left :6px;"  ><STRONG><spring:message code="service.label.PERIODICSERVICES    "  text="default text"/> </STRONG> </td>-->
                                    <td colspan="4" class="rgline" align="left" style="padding-left:5px; "><spring:message code="service.label.PERIODICSERVICES"  text="default text"/> </STRONG></td>
                                </tr>    
                                <% }else if( operationTO.getProbName() != "" ){ %>
					
					
					<tr >
					<td width="40" height="20" class="rgline" style="padding-left:5px; " ><%= sno %></td>
					<td width="205" height="20" class="rgline" align="left" style="padding-left:5px; ">
					<% for(int k=0; k<problemNames.length; k++ ) {%>
                                      <%= problemNames[k] %>  <br>
                    
					
					                <%}%>
                                                        <%if("Technician".equals(identifiedBy)){%>
                                                        <b>[Technician Identified]</b>
                                                        <%}%>

					</td>



					<td width="205" height="20" class="rgline" align="left" style="padding-left:5px; ">
					<% for(int m=0; m<symptoms.length; m++ ){%>
					  <%= symptoms[m] %>  <br>
					<%}%>
					
					</td>                                       
                                    <%--
                                    <%

                                       ArrayList technician = new ArrayList();
                                       int cntr = 0;
                                       technician = (ArrayList) request.getAttribute("technicians");
                                       MrsTO mrstechTO = new MrsTO();
                                       if (operationTO.getEmpId() != 1 && operationTO.getEmpId() != 0) {

                                           for (int k = 0; k < technician.size(); k++) {
                                               mrstechTO = (MrsTO) technician.get(k);
                                               if (mrstechTO.getEmpId() == operationTO.getEmpId() && cntr ==0 ) {
                                    %>              
                                    <Td valign="top" HEIGHT="20" class="line"    border-left-style:solid; padding-left :6px;  " align="left"><%=techNames%>&nbsp;</td>
                                            <%  cntr++;  %>
                                    <%  }
                                        }
                                           cntr=0;
                                    } else if (operationTO.getEmpId() == 1) {
                                    %>     
                                    <Td valign="top" HEIGHT="20"  class="line"    border-left-style:solid; padding-left :6px; " align="left">External Technician</td>
                                    
                                    <%} else if (operationTO.getEmpId() == 0) {
                                    %>      
                                    <Td valign="top" HEIGHT="20"  class="line"   border-left-style:solid; padding-left :6px; " align="left">Not Assigned</td>
                                    
                                    <% }else {
                                    %>
                                    <Td valign="top" HEIGHT="20" class="line"    style="border-left-style:solid; padding-left :6px;" align="left"><%=techNames%>&nbsp;</td>

                                    <%
                                     }
                                     %>--%>

                                        <td width="205" height="20" class="rgline" align="left" style="padding-left:5px; ">
                                        <%=techNames%>&nbsp;</td>
                                </tr>


<%  }
                            index++;
                            sno++;
                                    }   %>



                  

					<tr>
					<td colspan="3" height="80" valign="top" style="padding:5px; ">
					<b><spring:message code="service.label.Remarks"  text="default text"/>:</b>
                                        <%= headContent.getRemarks() %>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>

</table>
</div>
<table align="center">
<tr>
<td><center>   
                <input type="button" class="button" name="Print" value="<spring:message code="service.label.Print"  text="default text"/>" onClick="print(<%= i %>)" > &nbsp;        
            </center>
            <br>    
            <br>    
            
              
            <%   }}  %> </td>
</tr>
<tr>
<td ><!-- empty space height --></td>
</tr>
</table>

</body>

<script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                get_object("inputdata").innerHTML = DrawCode39Barcode('<%=jobcardId%>', 0);
                /* ]]> */
            </script>


</html>
