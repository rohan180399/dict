<%-- 
Document   : alterrole
Created on : Jul 21, 2008, 7:19:37 PM
Author     : vidya
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
function submitpage(value)
{
var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
document.alterrole.action = '/throttle/modifyroles.do';
document.alterrole.submit();
}
}


function selectedItemValidation(){
var index = document.getElementsByName("selectedUsers");
var desc = document.getElementsByName("desc");

var chec=0;
var mess = "SubmitForm";
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(isEmpty(desc[i].value)){
alert ("Enter The Descritpion");
desc[i].focus();
mess = "";
break;
}
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
userNames[0].focus();
//break;
}
return mess+"-"+chec;
}


function setSelectbox(i)
{
var selected=document.getElementsByName("selectedUsers") ;
selected[i].checked = 1;
}

</script>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.AddRole" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageRole" text="default text"/> </li>
          <li class=""><spring:message code="settings.label.AddRole" text="default text"/> </li>

                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">

<body background="">
<form name="alterrole" method="post">

<!-- copy there from end -->
<!--<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
 pointer table 
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td>
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
 pointer table 

 title table 
</div>
</div>-->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>
<!-- message table -->
<!-- copy there  end -->
<table class="table table-info mb30 table-hover" >
    <thead>
<tr>
<th  height="30"><div ><spring:message code="settings.label.RoleId" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Rolename" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Description" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Status" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Select" text="default text"/></div></th>
</tr>
</thead>
<% 

int index = 0;
ArrayList usernames = (ArrayList) request.getAttribute("roleRecord");
%>
<c:if test = "${roleRecord != null}">
<c:forEach items="${roleRecord}" var="user"> 
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>


<td height="30" ><input type="hidden" name="roleId" value='<c:out value="${user.roleId}"/>'> <div align="center"><c:out value="${user.roleId}"/></div> </td>
<td height="30" ><input type="textbox" readonly onChange="setSelectbox(<%= index %>)" name="roleName" style="width:250px;height:40px;"  class="form-control" value='<c:out value="${user.roleName}"/>'></td>
<td height="30" ><textarea style="width:250px;height:40px;"  class="form-control" onchange="setSelectbox(<%= index %>)" cols="17" rows="1"  class="text1" name="desc"><c:out value="${user.description}"/> </textarea> </td>
<td height="30" > <div align="center"><select name="activeInds" style="width:250px;height:40px;"  class="form-control" onChange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${user.status == 'Y'}">
<option value="Y" selected><spring:message code="settings.label.Active" text="default text"/></option>
<option value="N"><spring:message code="settings.label.InActive" text="default text"/></option>
</c:when>
<c:otherwise>
<option value="Y"><spring:message code="settings.label.Active" text="default text"/></option>
<option value="N" selected><spring:message code="settings.label.InActive" text="default text"/></option>
</c:otherwise>
</c:choose>
</select></div> </td>
<td  height="30"> <input type="checkbox" name="selectedUsers" value='<%= index %>' > </td>
</tr>


<%
index++;
%>
</c:forEach>
</c:if> 
</table>
<center>
<input type="button" class="btn btn-success" name="save" value="<spring:message code="settings.label.SAVE" text="default text"/>" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="modifyroles" />
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
