<html >
<head>
  <%@page language="java" contentType="text/html; charset=UTF-8"%>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <title>Throttle - Leading Transport Management System</title>

  <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
  <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
  <%
    String selectedLanguage = (String)session.getAttribute("paramName");
    System.out.println("selectedLanguage:"+selectedLanguage);
    if("ar".equals(selectedLanguage)){
    %>
    <link href="content/NewDesign/css/style.default-rtl.css" rel="stylesheet">
  <%
  }
  %>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->


<script>
          $(document).ready(function () {
           jQuery('body').addClass('stickyheader');
        });

  </script>



</head>

<body class="horizontal-menu-sidebar stickyheader">
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="leftpanel sticky-leftpanel" style="background: #418BCA;">
    
    <div class="logopanel">
<!--        <h1><span>[</span> bracket <span>]</span></h1>-->
        <img src="/throttle/images/Throttle_Logo.png" alt="throttle Logo" width="210" height="30" />
    </div><!-- logopanel -->
        
    <div class="leftpanelinner">    
        
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
<!--                <img alt="" src="content/NewDesign/images/photos/loggeduser.png" class="media-object">-->
                <div class="media-body">
                    <h4><%= session.getAttribute("userName") %></h4>
                    
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
<!--              <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>-->
              <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
              <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
              <li><a href="/throttle/logout.do"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>