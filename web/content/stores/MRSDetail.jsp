<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 

    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script>
        
        
    function submitPage(value)
    {
        document.spareIssue.mrsApprovalStatus.value = value;
        selectedItemValidation(value);   
    }
    function newWO(val){           
        window.open('/throttle/OtherServiceStockAvailability.do?itemId='+val, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function selectedItemValidation(value){
        var index = document.getElementsByName("selectedIndex");
        var issueQty = document.getElementsByName("issueQtys");
        var newItem = document.getElementsByName("newItems");
        var rcItem = document.getElementsByName("rcItems");
        var chec=0;
        
        if(value != 'Rejected'){
        for(var i=0;(i<index.length && index.length!=0);i++){
            chec++;
            if(floatValidation(issueQty[i],'Issue Quantity')){       
                return;
            }   
        }
        }
        
        if(value == 'Rejected'){
            for(var i=0;(i<index.length && index.length!=0);i++){
                issueQty[i].value='0';
            }
        }
        
        if(textValidation(document.spareIssue.mrsApprovalRemarks,'Remarks')){   
            return;
        }
        document.spareIssue.action='/throttle/handleAddMrsApprovedQuantity.do';           
        document.spareIssue.submit();
    }
    function setFocus(){
        
        document.spareIssue.issueQtys0.focus();
    }
    </script>
    
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRS"  text="MRS"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="service.label.MRS"  text="MRS"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    
    <body onload="setFocus();">
        <form name="spareIssue"  method="post" >
            
            <c:if test = "${vehicleDetails != null}" >
                <table class="table table-info mb30 table-hover" >
                    <thead>
                    <tr>
                        <th colspan="4" align="center" height="30">Vehicle Details</th>
                    </tr>
                    </thead>
                    <% int index = 0;%>
                    <c:forEach items="${vehicleDetails}" var="vehicle"> 
                        <tr>
                            <td>Vehicle Number</td>
                            <td><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                            <td>KM</td>
                            <td><c:out value="${vehicle.mrsVehicleKm}"/></td>
                        </tr>
                        
                        <tr>
                            <td>Vehicle Type</td>
                            <td><c:out value="${vehicle.mrsVehicleType}"/></td>
                            <td>MFR</td>
                            <td><c:out value="${vehicle.mrsVehicleMfr}"/></td>
                        </tr>
                        
                        <tr>
                            <td>Use Type</td>
                            <td><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
                            <td>Model</td>
                            <td><c:out value="${vehicle.mrsVehicleModel}"/></td>
                        </tr>
                        
                        <tr>
                            <td>Engine No</td>
                            <td><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                            <td>Chasis No</td>
                            <td><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
                        </tr>
                        
                        <tr>
                            <td>Technician</td>
                            <td><c:out value="${vehicle.mrsTechnician}"/></td>
                            <td>Job Card No.</td>
                            <td><c:out value="${vehicle.mrsJobCardNumber}"/></td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </table>
                <% index = 0;%>
                <c:if test = "${mrsDetails != null}" >
                    <c:forEach items="${mrsDetails}" var="mrs"> 
                    <%if (index == 0) {%>
                    
                    <input type="hidden" name="mrsNumber" value="<c:out value="${mrs.mrsNumber}"/>">   
                           
                           <% index++;
            }%>
                           </c:forEach>
                </c:if>
                <br>
                <table class="table table-info mb30 table-hover">
                   <thead>
                    <tr>
                        <% index = 0;%>
                        <c:if test = "${mrsDetails != null}" >
                            <c:forEach items="${mrsDetails}" var="mrs"> 
                                <%if (index == 0) {%>
                                <th colspan="12" align="center"><strong>MRS Number :<c:out value="${mrs.mrsNumber}"/></strong></th>
                                <% index++;
            }%>
                            </c:forEach>
                        </c:if>
                    </tr>
                    <c:if test = "${mrsDetails != null}" >
                        <tr>
                            <th class="contentsub" align="left"  height="30">MFR<br> Code</th>
                            <th >PAPL <br>Code</th>
                            <th class="contentsub" align="left"  height="30">Item<br> Name</th>
                            <th >Requested<br> Qty</th>
                            <th >New Item <br>Stock</th>
                            <th class="contentsub" align="left"  height="30">RC Item <br>Stock</th>
                            <th >OtherService<br>Point Stk</th>
                            <th >PO Raised <br>Qty</th>
                            <th >Local <br>PurchQty</th>
                            <th >Other MRS<br> Req</th>
                            <th >Issue <br>Qty</th>
                        </tr>
                    </thead>
                        <% index = 0;%>
                        
                        <c:forEach items="${mrsDetails}" var="mrs"> 
                            <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                            %>
                            <tr>
                                <td ><c:out value="${mrs.mrsItemMfrCode}"/></td>
                                <td ><c:out value="${mrs.mrsPaplCode}"/></td>
                                <td ><input type="hidden" name="itemIds" value="<c:out value="${mrs.mrsItemId}"/>"><c:out value="${mrs.mrsItemName}"/></td>
                                <td ><input type="hidden" name="requestedQtys" value="<c:out value="${mrs.mrsRequestedItemNumber}"/>"><c:out value="${mrs.mrsRequestedItemNumber}"/></td>
                                <td ><input type="hidden" name="newItems" value="<c:out value="${mrs.mrsLocalServiceItemSum}" />"><c:out value="${mrs.mrsLocalServiceItemSum}" /></td>
                                <td ><input type="hidden" name="rcItems" value="<c:out value="${mrs.rcQty}" />"><c:out value="${mrs.rcQty}" /></td>
                                <td ><a href="" onClick="newWO(<c:out value="${mrs.mrsItemId}"/>);" ><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></td>
                                <td ><c:out value="${mrs.vendorPoQty}" /></td>
                                <td ><c:out value="${mrs.localPoQty}" /></td>
                                <td ><c:out value="${mrs.count}"/></td>
                                <td ><input type="text" name="issueQtys" value='<c:out value="${mrs.mrsRequestedItemNumber}"/>' id="issueQtys<%= index %>" class="form-control" size="5" ></td>
                                
                                <input type="hidden" name="positionIds" value="<c:out value="${mrs.positionId}"/>"  >
                                       <input type="hidden" checked name="selectedIndex" value="<%= index %>"  >
                            </tr>
                            <% index++; %>
                        </c:forEach>
                    </c:if>
                </table>
                <br>
                <div align="center" class="text2"> Remarks &nbsp;
                    <textarea class="form-control" cols="20" rows="2" name="mrsApprovalRemarks">Approve Mrs</textarea>
                </div>
                <br>
                <center>
                    <input type="button" value="Approve" name="Approved" class="button" onclick="submitPage(this.name);" > &nbsp;
                    <input type="button" value="Reject" class="button" name="Rejected" onclick="submitPage(this.name);">
                    <input type="hidden" value="" name="mrsApprovalStatus">                    
                </center>
                <br>
                <br>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


  </div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>


