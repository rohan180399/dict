<%--
    Document   : driverEmbarkment
    Created on : Sep 4, 2012, 1:36:35 PM
    Author     : Ashok
--%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>
    <script type="text/javascript" language="javascript">
        function submitPage(value)
        {
            var driName =document.driverEmbarkment.driName.value;
            // var driName = driNameT.split("-", 0);
            // alert("===="+driName);
            document.driverEmbarkment.settlementId.value=value;

            if(!isEmpty(driName)){
                document.driverEmbarkment.action="/throttle/driverDetails.do";
                document.driverEmbarkment.submit();                
            }else {
                alert("Please Enter Driver Name");
                document.driverEmbarkment.driName.focus();
            }
        <%
                    String settleId = (String) request.getAttribute("settleId");
                    System.out.println("settleId = " + settleId);
                    pageContext.setAttribute("settleId", settleId);
        %>
            }
            function getaddbutt(){
                document.getElementById('addtable').style.display='block';
            }
            function savePage(){
                var driName = document.driverEmbarkment.driName.value;
                var regno = document.driverEmbarkment.regno.value;
                var embarkmentDate =document.driverEmbarkment.embarkmentDate.value;
                var remark =document.driverEmbarkment.remark.value;
                
                if((!(isEmpty(driName) && isEmpty(regno)) || !isEmpty(embarkmentDate) || !isEmpty(remark) ))  {
                    document.driverEmbarkment.action="/throttle/saveDriverV.do";
                    document.driverEmbarkment.submit();
                
                }
            }

            function getDriverName(){
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));
    
            }
            window.onload = getVehicleNos;
            function getVehicleNos(){
                var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/driverVehicleNo.do?"));
            }

            function setValues(){
                var driName = '<%=request.getAttribute("driName")%>';
                if(driName != "null"){
                    document.driverEmbarkment.driName.value=driName;
                }else{                    
                    document.driverEmbarkment.driName.value="";
                }
            }
            function displayCollapse(){               
                if(document.getElementById("exp_table").style.display=="block"){
                    document.getElementById("exp_table").style.display="none";
                    document.getElementById("openClose").innerHTML="Open";                }
                else{
                    document.getElementById("exp_table").style.display="block";
                    document.getElementById("openClose").innerHTML="Close";
                }
            }
    </script>
    <body onload="setValues();">
        <form name="driverEmbarkment" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;"><!--<img src="/throttle/images/close.jpg" width="25" height="25" />-->Close</span></td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:700;">
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="form-control" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                        </td>                                        
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" class="border">
                <%int flag = 2;
                            int index = 0;%>
                <c:if test="${driverEmbDetail != null}">
                    <tr>
                        <td class="contenthead">S.No</td>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Vehicle Number</td>
                        <td class="contenthead">Embarkment Date</td>
                        <td class="contenthead">Alighting Date</td>
                        <td class="contenthead">Alighting Status</td>
                        <td class="contenthead">Settlement Status</td>
                        <td class="contenthead">Remarks</td>
                    </tr>
                    <c:forEach items="${driverEmbDetail}" var="dri">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <tr>
                            <td class="<%=classText%>" height="30"><%=index + 1%></td>
                            <td class="<%=classText%>"><c:out value="${dri.empName}"/></td>
                            <td class="<%=classText%>"><c:out value="${dri.vehicleNo}"/></td>
                            <td class="<%=classText%>"><c:out value="${dri.embarkDate}"/></td>
                            <c:choose>
                                <c:when test="${dri.alightDate == '1899-11-30'}">
                                    <td class="<%=classText%>">&nbsp;</td>
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>"><c:out value="${dri.alightDate}"/></td>
                                </c:otherwise>
                            </c:choose>
                                    <td align="center" class="<%=classText%>"><c:out value="${dri.alghtingStatus}"/></td>
                            <c:if test="${dri.actInd == 'Y'}">
                                <td class="<%=classText%>"><c:out value="Completed"/></td>
                            </c:if>
                            <c:if test="${dri.actInd == 'N'}">
                                <td class="<%=classText%>"><c:out value="In Progress"/></td>
                                <%flag = 1;%>
                            </c:if>
                            <td class="<%=classText%>"><c:out value="${dri.remarks}"/></td>
                            <%index++;%>
                        </c:forEach>
                    </tr>
                </table></c:if>                
                <br>
                <c:if test="${addFlag == 2}">
                    <%if(flag==2){%>
                    <table width="870" cellpadding="0" cellspacing="2" border="0" align="center" class="table3">
                    <tr>
                        <td align="center"><input type="button" name="addbutt"  id="addbutt" class="button" value=" ADD " onclick="getaddbutt()"> </td>
                    </tr>
                </table>
                    <%}%>
                </c:if>                
                <center><font color="blue">&nbsp;</font></center>
                <table width="870" id="addtable" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" style="display: none;">
                    <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td>Vehicle No</td>
                        <td>
                            <input name="regno" id="regno" type="text" class="form-control" size="20" value="" onKeyPress="getVehicleNos();" autocomplete="off">
                        </td>
                        <td>Embarkment Date</td>
                        <td height="30"><input name="embarkmentDate" id="embarkmentDate" type="text" class="datepicker" autocomplete="off"></td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td>Remarks</td>
                        <td colspan="3"><textarea cols="50%" name="remark" id="remark"></textarea></td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td colspan="4" align="center">&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td colspan="4" align="center"><input type="button" class="button"  onclick="savePage(0);" value="Save"></td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                </table>            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
