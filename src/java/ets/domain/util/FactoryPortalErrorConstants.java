/*-----------------------------------------------------------------------------
  * FactoryPortalErrorConstants.java
  * Mar 25, 2008
  *
  * Copyright (c) ES Systems.
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * ES Systems ("Confidential Information"). You shall
  * not disclose such Confidential Information and shall use it only in
  * accordance with the terms of the license agreement you entered into
  * with ES Systems.
  ---------------------------------------------------------------------------*/

 package ets.domain.util;
 /****************************************************************************
  *
  * Modification Log:
  * ---------------------------------------------------------------------------
  * Ver      Date                          Author                       Change
  * --------------------------------------------------------------------------
  * 1.0     Mar 25, 2008                   ES			            Created
  *
  ****************************************************************************/
  /**
   * <br>FactoryPortalErrorConstants object -This class have all the Event Protal
   *  Error constants.
   * @author Satheeshkumar P
   * @version 1.0 25 Mar 2008
   * @since   Event Portal Iteration 0
  */
public class FactoryPortalErrorConstants {
/**
 * MESSAGE_KEY-message
 */
	public static final String MESSAGE_KEY = "message";
	/**
	 * ERROR_KEY-error
	 */
	public static final String ERROR_KEY = "error";
	/**
	 * META_DATA_NOT_FOUND -Error message -When Metadata not able to found
	 */
	public static final String META_DATA_NOT_FOUND
									="Metadata xml not found";
	/**
	 * META_DATA_INVALID-Error message when a Event Inbox
	 * request cannot be created.
	 */
	public static final String META_DATA_INVALID
				="Invalid XML. Event Inbox request cannot be created.";

/**
 * EM_MJ_16 -ERROR_JOB_STATUS_IN_PROCESSING
 */
	public static final String EM_MJ_16 = "ERROR_JOB_STATUS_IN_PROCESSING";
	/**
	 * EM_MJ_16_MESSAGE -Errror message ,Job
	 * cannot be created.Request No is already in Processing
	 */
	public static final String EM_MJ_16_MESSAGE =
		"New Job cannot be created at this point of time. Jobs " +
		"for this Request Number already exists and is in processing.";
	/**
	 * EM_MJ_TO_BE_PROCESSED - ERROR_JOB_STATUS_TO_BE_PROCESSED
	 */
	public static final String EM_MJ_TO_BE_PROCESSED =
							"ERROR_JOB_STATUS_TO_BE_PROCESSED";
	/**
	 * EM_MJ_TO_BE_PROCESSED_MESSAGE -When a Job is already exist
	 *  for the request Number
	 */
	public static final String EM_MJ_TO_BE_PROCESSED_MESSAGE =
		"New Job cannot be created at this point of time. Jobs for " +
		"this Request Number already exists and yet to start processing.";
	/**
	 * EM_MJ_17 -ALL_JOB_STATUS_PROCESSING_COMPLETE
	 */
	public static final String EM_MJ_17 =
					"ALL_JOB_STATUS_PROCESSING_COMPLETE";
	/**
	 * EM_MJ_17_MESSAGE -Error message when a job is
	 * already created for the request Number
	 */
	public static final String EM_MJ_17_MESSAGE =
		"Jobs for this Request Number already exists " +
		"and has successfully completed the processing.";
	/**
	 * FAILED_TO_CREATE_JOB -Error Message when the system failed
	 * to create a Job
	 */
	public static final String FAILED_TO_CREATE_JOB = "Failed to create " +
	"the Job. Please contact the system administrator if problem persists.";
	/**
	 * INVALID_REQ_ENTRY -Error message when a invalid request No entered
	 */
	public static final String INVALID_REQ_ENTRY = "The Request" +
			" number entered does not exists.";
	/**
	 * EM_MJ_18 - LBU_DOC_TYPE
	 */
	public static final String EM_MJ_18 = "LBU_DOC_TYPE";
	/**
	 * EM_MJ_18_MESSAGE - Error message when a Job is created already
	 */
	public static final String EM_MJ_18_MESSAGE = "Job already created " +
			"with LBU document type for this request.";
/**
 * NO_JOB_SEARCH_RESULTS -The no of search Results of Job
 */
	public static final String NO_JOB_SEARCH_RESULTS = "There are no job(s) " +
			"matching the specified search criteria. Please re-define your" +
			" criteria and try again.";
}
