package ets.cewolf.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.DefaultHighLowDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;


public class OHLCDBean implements DatasetProducer, Serializable {


	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(OHLCDBean.class);

		/**
		 *  Produces some random data.
		 */
	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	 String ds1 = "January 1, 2000";
	    	 String ds2 = "February 1, 2000";
	    	 String ds3 = "March 1, 2000";
	    	 String ds4 = "April 1, 2000";
	    	 String ds5 = "May 1, 2000";
	    	 String ds6 = "June 1, 2000";
	    	 String ds7 = "July 1, 2000";
	    	 String ds9 = "September 1, 2000";
	    	 String ds10 = "October 1, 2000";
	    	 String ds11 = "November 1, 2000";

	    	 Date d1= new Date();
	    	 Date d2= new Date();
	    	 Date d3= new Date();
	    	 Date d4= new Date();
	    	 Date d5= new Date();
	    	 Date d6= new Date();
	    	 Date d7= new Date();
	    	 Date d9= new Date();
	    	 Date d10= new Date();
	    	 Date d11= new Date();

	    	 DateFormat df = DateFormat.getDateInstance();
	         try {
	        	d1 = df.parse(ds1);
				d2 = df.parse(ds2);
				d3 = df.parse(ds3);
				d4 = df.parse(ds4);
				d5 = df.parse(ds5);
				d6 = df.parse(ds6);
				d7 = df.parse(ds7);
				d9 = df.parse(ds9);
				d10 = df.parse(ds10);
				d11 = df.parse(ds11);
	         } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



	    	Date[] date = {d1,d2,d3,d4,d5,d6,d7,d9,d10,d11};
	    	double[] high = {5,10,4,8,6,4,5,8,10,12};
	    	double[] low = {1.23,4.2,1.87,3.231,2.567,3,1,2,5,6};
	    	double[] open = {4,10,2,4,3,2,1,2,1,4};
	    	double[] close = {3,7,4,6,5,3,4,7,9,11};
	    	double[] volume={5,20,8,15,10,10,12,11,15,20};
	    	/*public DefaultHighLowDataset(Comparable seriesKey, Date[] date,
	    			double[] high, double[] low, double[] open, double[] close, double[] volume);*/
	    	DefaultHighLowDataset datasetOHLCD = new DefaultHighLowDataset("Series",date,high,low,open,close,volume){

				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				protected void finalize() throws Throwable {
					super.finalize();
					log.debug(this +" finalized.");
				}


	        };
			 return datasetOHLCD;




	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}


}





