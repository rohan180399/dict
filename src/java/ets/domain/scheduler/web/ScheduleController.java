/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.billing.business.BillingBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.business.DistancePojo;
import ets.domain.scheduler.business.Distance;
import ets.domain.scheduler.business.Duration;
import ets.domain.scheduler.business.SchedulerBP;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.finance.business.FinanceBP;
import ets.domain.finance.business.FinanceTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.thread.web.SendEmailDetails;
import ets.domain.thread.web.SendEmailReport;      
import ets.domain.thread.web.SendEmailReportForSupp;
import ets.domain.thread.web.SendSMS;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
//import static ets.domain.util.ThrottleConstants.smsSchedulerStatus;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import com.google.gson.*;
import ets.domain.billing.business.BillingTO;
import ets.domain.scheduler.business.Elements;
import ets.domain.scheduler.business.Rows;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
//import throttlegps.gpsActivity;
//import throttlegps.main.GpsLocationMain;

//excel
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

//excel
/**
 *
 * @author Nivan
 */
public class ScheduleController extends BaseController {

    TripBP tripBP;
    LoginBP loginBP;
    SchedulerBP schedulerBP;
    ReportBP reportBP;
    BillingBP billingBP;
    FinanceBP financeBP;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SchedulerBP getSchedulerBP() {
        return schedulerBP;
    }

    public void setSchedulerBP(SchedulerBP schedulerBP) {
        this.schedulerBP = schedulerBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    public BillingBP getBillingBP() {
        return billingBP;
    }

    public void setBillingBP(BillingBP billingBP) {
        this.billingBP = billingBP;
    }

    public FinanceBP getFinanceBP() {
        return financeBP;
    }

    public void setFinanceBP(FinanceBP financeBP) {
        this.financeBP = financeBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        ////System.out.println("request.getRequestURI() = " + request.getRequestURI());

    }
    int runMinute = 0;
    int runHour = 0;
    
    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    public void executeScheduler() throws FPRuntimeException, FPBusinessException, Exception {
//         printing current system time
        try {
         //   System.out.println("Current Time ----: " + Calendar.getInstance().getTime());
         //   System.out.println("executeScheduler CALLED....");
            try {
                TripTO tripTO = new TripTO();
                ArrayList emailList = new ArrayList();
                emailList = tripBP.getEmailList();

           //     System.out.println("EMAIL EXCUTER->" + emailList.size());
                Iterator itr = emailList.iterator();
//                while (itr.hasNext()) {
//                    tripTO = (TripTO) itr.next();
//                    String mailSendingId = tripTO.getMailSendingId();
//                    System.out.println("mailSendingId---" + mailSendingId);
////////                     new SendEmailDetails(tripTO).start();
//                    URL url = new URL("http://bvm.throttletms.com:8899/DICTServiceAPI/api/v1/sendHtmlEmail?id=" + mailSendingId);
//                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
//                    con.setRequestMethod("POST");
//                    int status = con.getResponseCode();
//                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                    String inputLine;
//                    StringBuffer content = new StringBuffer();
//                    while ((inputLine = in.readLine()) != null) {
//                        content.append(inputLine);
//                    }
//                    in.close();
//                    con.disconnect();
//                    System.out.println("Response status: " + status);
//                    System.out.println(content.toString());
//                }
            } catch (Exception exp) {
                exp.printStackTrace();
            } 

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar c = Calendar.getInstance();
            Date date = new Date();
            c.setTime(date);
            String systemTime = sdf.format(c.getTime()).toString();
            System.out.println("systemTime = " + systemTime);
            String[] dateTemp = systemTime.split(" ");
            String[] timeTemps = dateTemp[1].split(":");

            String[] timeTemp = dateTemp[1].split(":");
            Date today = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            int eInvoiceApi = 0;
            int statusId = 0;
            runMinute = Integer.parseInt(timeTemp[1]);
            runHour = Integer.parseInt(timeTemp[0]);
          //  System.out.println("API call time--" + Integer.parseInt(timeTemp[1]));
            if (runMinute % 5 == 0) {
                  //  System.out.println("API call every minute--" + Integer.parseInt(timeTemp[1]));
                    statusId = 0;
//                    System.out.println("inside API");
//                    eInvoiceApi = reportBP.callEInvoice(statusId);
//                    System.out.println("callEInvoice---" + eInvoiceApi);
//                    eInvoiceApi = reportBP.callESuppInvoice(statusId);
//                    System.out.println("callESuppInvoice---" + eInvoiceApi);
//
//                    eInvoiceApi = reportBP.callECreditInvoice(statusId);
//                    System.out.println("callECreditInvoice---" + eInvoiceApi);
//                    eInvoiceApi = reportBP.callECreditSuppInvoice(statusId);
//                    System.out.println("callECreditSuppInvoice---" + eInvoiceApi);
                
                 

            }
  
//            //    schedulerBP.getInvoicePDFEmail("24204");
//            //     schedulerBP.getInvoicePDFEmail();

            if (runMinute % 2 == 0) {
//                int getGpsDetails = reportBP.getGpsDetails();
            }
            
            runMinute = Integer.parseInt(timeTemp[1]);
            runHour = Integer.parseInt(timeTemp[0]);

            //    schedulerBP.getInvoicePDFEmail("24204");
            //     schedulerBP.getInvoicePDFEmail();
           // boolean executionStatus = false;
            ThrottleConstants.PROPS_VALUES = loadProps();
//          int smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);
            int smsSchedulerStatus = 0;
            //int gpsSchedulerStatus = 0;
            System.out.println("smsSchedulerStatus" + smsSchedulerStatus);
           // String[] tempLatLong = ThrottleConstants.sourceLatLong.split(",");
            //System.out.println("tempLatLong" + tempLatLong);
//            String sourceLat = tempLatLong[0];
//            String sourceLong = tempLatLong[1];
          //  ArrayList contractRateApproveListForMail = new ArrayList();
            //  demo.copy();
            // Gps Main Method start
//            gpsActivity gps =new gpsActivity();
//            gps.gpsData();
            // Gps End  
            //schedulerBP.getDailyVehicleUtilised();
            // schedulerBP.getContractDiffAlert();
            // satrt ---

//            schedulerBP.test();
            //end ----
            ReportTO reportTO1 = new ReportTO();

            if (runMinute % 2 == 0) {
                //  ArrayList getInvoiceList = reportBP.getInvoiceList(reportTO1, 1);
                //System.out.println("getInvoiceList---"+getInvoiceList);
            }
            System.out.println("throttle constant dvmTime...." + ThrottleConstants.PROPS_VALUES.get("dvmTime"));
            System.out.println("throttle constant key dvmTime...." + ThrottleConstants.PROPS_VALUES.containsKey("dvmTime"));

            if (smsSchedulerStatus == 16546555) {

//   einvoice creation
                if (Integer.parseInt(timeTemp[0]) == 8 && Integer.parseInt(timeTemp[1]) == 30) {

                    //einvoice 
                    int userId = 1403;
                    System.out.println("Invoice Mail-------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    BillingTO reportTONew = new BillingTO();
                    ArrayList invoiceForEinv = new ArrayList();
                    invoiceForEinv = reportBP.invoiceForEinv();
                    System.out.println("invoiceForEinv..####..--------------" + invoiceForEinv.size());
                    Iterator itrNew;
                    itrNew = invoiceForEinv.iterator();
                    BillingTO repNew = null;
                    while (itrNew.hasNext()) {
                        repNew = new BillingTO();
                        repNew = (BillingTO) itrNew.next();
                        String invoiceid = repNew.getInvoiceId();
                        reportTONew.setInvoiceId(invoiceid);
                        System.out.println("invoiceid----------------" + invoiceid);
                        int status = billingBP.eInvoiceGenerateAuto(reportTONew, userId);
                        System.out.println("status..####..EinvoiceMoved--------------" + status);
                    }
                    //esupp invoice

                    BillingTO bTo1 = new BillingTO();
                    ArrayList invoiceForESuppinv = new ArrayList();
                    invoiceForESuppinv = reportBP.invoiceForESuppinv();
                    System.out.println("invoiceForESuppinv..####..--------------" + invoiceForESuppinv.size());
                    Iterator itr22;
                    itr22 = invoiceForESuppinv.iterator();
                    BillingTO bTo2 = null;
                    while (itr22.hasNext()) {
                        bTo2 = new BillingTO();
                        bTo2 = (BillingTO) itr22.next();
                        String invoiceid = bTo2.getInvoiceId();
                        bTo1.setInvoiceId(invoiceid);
                        System.out.println("invoiceid----------------" + invoiceid);
//                     status = billingBP.eSuppInvoiceGenerateAuto(bTo1, userId);
                        int status = billingBP.eSuppInvoiceGenerateAuto(bTo1, userId);
                        System.out.println("status..####..EinvoiceMoved--------------" + status);
                    }
                    //ecredit inv
                    BillingTO bTo3 = new BillingTO();
                    ArrayList invoiceForECreditinv = new ArrayList();
                    invoiceForECreditinv = reportBP.invoiceForECreditinv();
                    System.out.println("invoiceForECreditinv..####..--------------" + invoiceForECreditinv.size());
                    Iterator itr23;
                    itr23 = invoiceForECreditinv.iterator();
                    BillingTO bTo4 = null;
                    while (itr23.hasNext()) {
                        bTo4 = new BillingTO();
                        bTo4 = (BillingTO) itr23.next();
                        String invoiceid = bTo4.getInvoiceId();
                        bTo3.setInvoiceId(invoiceid);
                        System.out.println("invoiceid----------------" + invoiceid);
                        int status = billingBP.eCreditInvoiceGenerateAuto(bTo3, userId);
                        System.out.println("status..####..EinvoiceMoved--------------" + status);
                    }

                    //supp credit einv
                    BillingTO bTo5 = new BillingTO();
                    ArrayList invoiceForESuppCreditinv = new ArrayList();
                    invoiceForESuppCreditinv = reportBP.invoiceForESuppCreditinv();
                    System.out.println("invoiceForESuppCreditinv..####..--------------" + invoiceForESuppCreditinv.size());
                    Iterator itr24;
                    itr24 = invoiceForESuppCreditinv.iterator();
                    BillingTO bTo6 = null;
                    while (itr24.hasNext()) {
                        bTo6 = new BillingTO();
                        bTo6 = (BillingTO) itr24.next();
                        String invoiceid = bTo6.getInvoiceId();
                        bTo5.setInvoiceId(invoiceid);
                        System.out.println("invoiceid----------------" + invoiceid);

                        int status = billingBP.eCreditSuppInvoiceGenerateAuto(bTo5, userId);
                        System.out.println("status..####..EinvoiceMoved--------------" + status);
                    }
//
                }

//                }
//                if (runMinute % 2 == 0) {
//                    System.out.println("inside-----------1111111111111111111111111111111111111---------------------------------------------------------------------------------------------------------------------------------");
//                    String downloadCsvFile = reportBP.downloadInvoiceCsvFile();
//                    String downloadSuppInvoiceCsvFile = reportBP.downloadSuppInvoiceCsvFile();
//                    String downloadCreditInvoiceCsvFile = reportBP.downloadCreditInvoiceCsvFile();
//                    String downloadSuppCreditInvoiceCsvFile = reportBP.downloadSuppCreditInvoiceCsvFile();
//                }

//                xml read file
                if (runMinute % 11 == 0) {
                    System.out.println("inside--------22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222211111111------------------------------------------------------------------------------------------------------------------------------------");
//                    String readCsvFile = reportBP.readCsvFile();
                }

//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Calendar c = Calendar.getInstance();
//                Date date = new Date();
//                c.setTime(date);
//                String systemTime = sdf.format(c.getTime()).toString();
//                System.out.println("systemTime = " + systemTime);
//                String[] dateTemp = systemTime.split(" ");
//                String[] timeTemps = dateTemp[1].split(":");
//
//                String[] timeTemp = dateTemp[1].split(":");
//                Date today = new Date();
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(today);
//
//                // Einvoice Api call
//                int eInvoiceApi = 0;
//                int statusId = 0;
                runMinute = Integer.parseInt(timeTemp[1]);
                runHour = Integer.parseInt(timeTemp[0]);
                System.out.println("API call time--" + Integer.parseInt(timeTemp[1]));
            

//                contract approval mail by rohan
//            OperationTO mailTO=new OperationTO();
//            
//          
//            contractRateApproveListForMail = financeBP.getContractRateApproveListForMail();
//                System.out.println("contractRateApproveList---------------"+contractRateApproveListForMail.size());
//
//            String to = "", cc = "";
//            String smtp = "";
//            int emailPort = 0;
//            String frommailid = "";
//            String password = "";
//            
//              Iterator itr11;
//                        itr11 = contractRateApproveList.iterator();
//                        OperationTO rep11 = null;
//
//                        while (itr11.hasNext()) {
//                            rep11 = new OperationTO();
//                            rep11 = (OperationTO) itr11.next();
//                            
//            smtp = ThrottleConstants.smtpServer;
//            emailPort = Integer.parseInt(ThrottleConstants.smtpPort);
//            frommailid = ThrottleConstants.fromMailId;
//            password = ThrottleConstants.fromMailPassword;
//            to = ThrottleConstants.contractApprovingMailId;
////            cc = ThrottleConstants.contractApprovingCCMailId;
//            cc = "rohanb@entitlesolutions.com,rohanb@entitlesolutions.com";
//
//            System.out.println("smtp=" + smtp);
//            System.out.println("emailPort=" + emailPort);
//            System.out.println("frommailid=" + frommailid);
//            System.out.println("password=" + password);
//            System.out.println("to=" + password);
//            // String oldFuelPriceId = "", effectiveDate = "", fuelPrice = "", fuelType = "",fuelTypes = "",fuelUnite = "",cityId="",status="",bunkId="",bunkName="";
//
//            String vechTypId = "", containerTypId = "", lodTypId = "", firstPickPointId = "", finalPointId = "", interim1 = "", interim2 = "", interim3 = "", interim4 = "", contractRateId = "", rateWithReefer = "", rateWithoutReefer = "", orgRateWithReefer = "", orgRateWithoutReefer = "";
//            vechTypId = "1".equals(rep11.getVehicleTypeId()) ? "20 Feet" : "40 Feet";
//            containerTypId = "1".equals(rep11.getContainerTypeId()) ? "20 Feet" : "40 Feet";
//            System.out.println("     rep11.getLoadTypeId()mailTO.getLoadTypeId()" + rep11.getLoadTypeId());
//            lodTypId = "1".equals(rep11.getLoadTypeId()) ? "Empty Trip" : "Load Trip";
//            System.out.println(" rep11.getLoadTypeId()lodTypId" + lodTypId);
//            firstPickPointId = rep11.getFirstPickupId();
//            interim1 = rep11.getInterimPickup1();
//            finalPointId = rep11.getFinalPointId();
//            rateWithReefer = rep11.getRateWithReefer();
//            rateWithoutReefer = rep11.getRateWithoutReefer();
//            contractRateId = rep11.getContractRateId();
//            orgRateWithReefer = rep11.getOrgWithReeferRate();
//            orgRateWithoutReefer = rep11.getOrgWithoutReeferRate();
//            String customerName = rep11.getCustomerName();
//            String createdBy = rep11.getUserName();
//            int userId = rep11.getUserId();
//            System.out.println("mailTO.getUserName():" + rep11.getUserName());
//            System.out.println("mailTO.getOrgWithReeferRate()=" + rep11.getOrgWithReeferRate() + "mailTO.getOrgWithoutReeferRate():" + mailTO.getOrgWithoutReeferRate());
//            String emailFormat = "<html>"
//                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                    + "<tr>"
//                    + "<th colspan='2'>Contract Rate Approval for Customer &nbsp;&nbsp:&nbsp;&nbsp " + customerName + " </th>"
//                    + "</tr>"
//                    + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vechTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Container Type</td><td>&nbsp;&nbsp;" + containerTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Load Type</td><td>&nbsp;&nbsp;" + lodTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Pickup point</td><td>&nbsp;&nbsp;" + firstPickPointId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point1</td><td>&nbsp;&nbsp;" + interim1 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point2</td><td>&nbsp;&nbsp;" + interim2 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point3</td><td>&nbsp;&nbsp;" + interim3 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point4</td><td>&nbsp;&nbsp;" + interim4 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Drop Point</td><td>&nbsp;&nbsp;" + finalPointId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Contract Rate With Reefer</td><td>&nbsp;&nbsp;" + rateWithReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Contract Rate Without Reefer</td><td>&nbsp;&nbsp;" + rateWithoutReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Tariff with reefer</td><td>&nbsp;&nbsp;" + orgRateWithReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Tariff without reefer</td><td>&nbsp;&nbsp;" + orgRateWithoutReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Transport Cost</td><td>&nbsp;&nbsp;" + Math.round(Double.parseDouble(orgRateWithoutReefer) * 0.869) + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Created By</td><td>&nbsp;&nbsp;" + createdBy + "</td></tr>"
//                    + "<tr height='25'><td></td><td></td></tr>"
//                    + "<tr><td colspan='2' align='center'>"
//                    + "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApproval.do?userId=1481&contractRateId=" + contractRateId + "&status=1'>Approve</a>&nbsp;|&nbsp;"
//                    + "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApproval.do?userId=1481&contractRateId=" + contractRateId + "&status=3'>Reject</a>"
//                    + "</td></tr>"
//                    + "</table><br/><br/><br/><br/><br/></body>"
//                    + "<script></script></html>";
//
//            String subject = "Contract Rate Approval Request";
//            String content = emailFormat;
//            if ("suraj bhandari".equalsIgnoreCase(rep11.getUserName())) {
//                to = ThrottleConstants.approvalMailId;
////                to = "mohits@ict.in";
//            } else if ("rashul jain".equalsIgnoreCase(rep11.getUserName())) {
//                to = ThrottleConstants.approvalMailId;
////                to = "sanjayv@ict.in";
//            } else {
//                to = ThrottleConstants.approvalMailId;
////                to = "mohits@ict.in,sanjayv@ict.in";
//            }
//
//            TripTO tripTO = new TripTO();
//            tripTO.setMailTypeId("2");
//            tripTO.setMailSubjectTo(subject);
//            tripTO.setMailSubjectCc(subject);
//            tripTO.setMailSubjectBcc("");
//            tripTO.setMailContentTo(content);
//            tripTO.setMailContentCc(content);
//            tripTO.setMailContentBcc("");
//            tripTO.setMailIdTo(to);
//            tripTO.setMailIdCc(cc);
//            tripTO.setMailIdBcc(cc);
//            tripTO.setContractRateId(contractRateId);
//            tripTO.setUserId(userId);
//            String status="3";
//            tripTO.setStatus(status);
//            
//
//          int  mailSendingId = tripBP.insertMailDetails(tripTO, userId);
//            System.out.println("mailSendingId" + mailSendingId);
//
//            //Aknowledgement email after contract approval or reject.
//            String emailFormat1 = "<html>"
//                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
//                    + "<tr>"
//                    + "<th colspan='2'>Contract Rate Approval for Customer &nbsp;&nbsp:&nbsp;&nbsp " + customerName + " </th>"
//                    + "</tr>"
//                    + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vechTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Container Type</td><td>&nbsp;&nbsp;" + containerTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Load Type</td><td>&nbsp;&nbsp;" + lodTypId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Pickup point</td><td>&nbsp;&nbsp;" + firstPickPointId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point1</td><td>&nbsp;&nbsp;" + interim1 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point2</td><td>&nbsp;&nbsp;" + interim2 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point3</td><td>&nbsp;&nbsp;" + interim3 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Interim Point4</td><td>&nbsp;&nbsp;" + interim4 + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Drop Point</td><td>&nbsp;&nbsp;" + finalPointId + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Contract Rate With Reefer</td><td>&nbsp;&nbsp;" + rateWithReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Contract Rate Without Reefer</td><td>&nbsp;&nbsp;" + rateWithoutReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Tariff with reefer</td><td>&nbsp;&nbsp;" + orgRateWithReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Tariff without reefer</td><td>&nbsp;&nbsp;" + orgRateWithoutReefer + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Transport Cost</td><td>&nbsp;&nbsp;" + Math.round(Double.parseDouble(orgRateWithoutReefer) * 0.869) + "</td></tr>"
//                    + "<tr><td>&nbsp;&nbsp;Created By</td><td>&nbsp;&nbsp;" + createdBy + "</td></tr>"
//                    + "<tr height='25'><td></td><td></td></tr>"
//                    + "<tr><td colspan='2' align='center'>"
//                    //+ "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApproval.do?userId=1480&contractRateId=" + contractRateId + "&status=1'>Approve</a>&nbsp;|&nbsp;"
//                    //+ "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApproval.do?userId=1480&contractRateId=" + contractRateId + "&status=3'>Reject</a>"
//                    + "</td></tr>"
//                    + "</table><br/><br/><br/><br/><br/></body>"
//                    + "<script></script></html>";
//
//            String subject1 = "Contract Rate Approval Request";
//            String content1 = emailFormat;
//            TripTO tripTO1 = new TripTO();
//            tripTO1.setMailTypeId("2");
//            tripTO1.setMailSubjectTo(subject1);
//            tripTO1.setMailSubjectCc(subject1);
//            tripTO1.setMailSubjectBcc("");
//            tripTO1.setMailContentTo(content1);
//            tripTO1.setMailContentCc(content1);
//            tripTO1.setMailContentBcc("");
//            tripTO1.setMailIdTo(to);
//            tripTO1.setMailIdCc(cc);
//            tripTO1.setMailIdBcc("");
//            tripTO1.setContractRateId(contractRateId);
//            tripTO1.setUserId(userId);
//             status="3";
//            tripTO1.setStatus(status);
//
//            mailSendingId = tripBP.insertMailDetails(tripTO1, userId);
//            System.out.println("mailSendingId" + mailSendingId);
//
//                            }
//                
                if (runMinute % 10 == 0) {
//                    int shippingBillNoUpdate=reportBP.shippingBillNoUpdate();
                }
                if (runMinute % 58 == 0) {
                    System.out.println("API call after error --" + Integer.parseInt(timeTemp[1]));
                    System.out.println("inside API");
                    statusId = 2;
                    eInvoiceApi = reportBP.callEInvoice(statusId);
                    System.out.println("inside error callEInvoice API--" + eInvoiceApi);
                    eInvoiceApi = reportBP.callESuppInvoice(statusId);
                    System.out.println("inside error callESuppInvoice API--" + eInvoiceApi);

                    eInvoiceApi = reportBP.callECreditInvoice(statusId);
                    System.out.println("inside error callECreditInvoice API--" + eInvoiceApi);
                    eInvoiceApi = reportBP.callECreditSuppInvoice(statusId);
                    System.out.println("inside error callECreditSuppInvoice API--" + eInvoiceApi);
                }
                // Einvoice Api call

                // Customer contract expiry alert
                String custContractExpiryTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("customerContractExpiryTime") != null) {
                    custContractExpiryTime = ThrottleConstants.PROPS_VALUES.get("customerContractExpiryTime");
                } else {
                    custContractExpiryTime = ThrottleConstants.customerContractExpiryTime;
                }

                System.out.println("custContractExpiryTime:" + custContractExpiryTime);
                String[] custProp = custContractExpiryTime.split(":");
                System.out.println("hp------------- " + timeTemp[0]);
                System.out.println("hp------------- " + timeTemp[1]);
                System.out.println("hp---custProp[0]---------- " + custProp[0]);
                System.out.println("hp-----custProp[1]-------- " + custProp[1]);
                SchedulerTO schedulerTO = new SchedulerTO();
                ArrayList getCustomerContractMailList = new ArrayList();
                ArrayList customerContractExpiryAlert = new ArrayList();
                if (timeTemp[0].equals(custProp[0]) && timeTemp[1].equals(custProp[1])) {
                    getCustomerContractMailList = reportBP.getCustomerContractMailList();
                    System.out.println("getCustomerContractMailList.size" + getCustomerContractMailList.size());
                    int status = 0;
                    Iterator itr = null;
                    itr = getCustomerContractMailList.iterator();
                    while (itr.hasNext()) {
                        schedulerTO = (SchedulerTO) itr.next();
                        System.out.println("MailId-------" + schedulerTO.getEmailId());
                        System.out.println("createdId-------" + schedulerTO.getCreatedBy());
                        customerContractExpiryAlert = reportBP.getCustomerContractExpiryList(schedulerTO);
                        System.out.println("customerContractExpiryAlert" + customerContractExpiryAlert.size());
                    }
                }

                ////////////////////// arun started on 23/09/2020 //////////////////
                int invHour = Integer.parseInt(timeTemps[0]);
                int invTime = Integer.parseInt(timeTemps[1]);

//                if (invTime == 45) {
//
//                    try {
//
//                        BillingTO billingTO = null;
//                        ArrayList closedTrips = new ArrayList();
//                         closedTrips = billingBP.getOrdersForAutoBilling();
//
//                        Iterator itr12;
//                        itr12 = closedTrips.iterator();
//                        while (itr12.hasNext()) {
//                            billingTO = new BillingTO();
//                            billingTO = (BillingTO) itr12.next();
//                            String finYear = "2023";
//                            String tripSheetId = billingTO.getTripId();
//                            String noOfTrips = billingTO.getNoOfTrips();
//                            String noOfOrder = billingTO.getOrderCount();
//
//                            String[] tripId = tripSheetId.split(",");
//                            String[] tripIdIGSTAmount = new String[tripId.length];
//                            String[] tripIdCGSTAmount = new String[tripId.length];
//                            String[] tripIdSGSTAmount = new String[tripId.length];
//
//                            for (int i = 0; i < tripId.length; i++) {
//                                System.out.println("value:" + tripId[i]);
//                                tripIdIGSTAmount[0] = "0";
//                                tripIdCGSTAmount[0] = "0";
//                                tripIdSGSTAmount[0] = "0";
//                            }
//
//                            billingTO.setTripSheetId(tripSheetId);
//                            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeBillingDetails(billingTO);
//
//                            if (ordersToBeBillingDetails.size() > 0) {
//
//                                BillingTO billingTO1 = (BillingTO) ordersToBeBillingDetails.get(0);
//                                String billingParty = billingTO1.getCustomerName();
//                                String customerAddress = billingTO1.getCustomerAddress();
//                                String billingPartyId = billingTO1.getCustomerId();
//                                String customerId = billingTO1.getCustomerId();
//                                String movementType = billingTO1.getMovementType();
//                                String billOfEntryNo = billingTO1.getBillOfEntryNo();
//                                String shipingBillNo = billingTO1.getShipingBillNo();
//                                String organizationId = billingTO1.getOrganizationId();
//                                String billingState = billingTO1.getBillingState();
//                                String GSTNo = billingTO1.getGstNo();
//                                String grDate = billingTO1.getGrDate();
//                                String PANNo = billingTO1.getPanNo();
//                                String companyType = billingTO1.getCompanyType();
//                                String totalRevenue = billingTO1.getEstimatedRevenue();
//
//                                String totalExpToBeBilled = billingTO1.getExpenseToBeBilledToCustomer();
//                                String remarks = "Auto Invoice";
//                                String grandTotal = totalRevenue;
//                                String totalTax = "0";
//                                String cgstAmount = "0";
//                                String sgstAmount = "0";
//                                String igstAmount = "0";
//
//                                String igstPercentage = "0";
//                                String cgstPercentage = "0";
//                                String sgstPercentage = "0";
//                                String otherExpense = totalExpToBeBilled;
//
//                                System.out.println("companyType" + billingTO1.getCompanyType());
//                                System.out.println("grDate:" + billingTO1.getGrDate());
//                                System.out.println("movementType:" + billingTO1.getMovementType());
//                                System.out.println("shipingBillNo:" + billingTO1.getShipingBillNo());
//                                System.out.println("billOfEntryNo:" + billingTO1.getBillOfEntryNo());
//                                System.out.println("billingState:" + billingTO1.getBillingState());
//                                System.out.println("organizationId:" + billingTO1.getOrganizationId());
//                                System.out.println("GSTNo:" + billingTO1.getGstNo());
//
//                                billingTO.setTripSheetId(tripSheetId);
//                                billingTO.setIgstPercentage(igstPercentage);
//                                billingTO.setCgstPercentage(cgstPercentage);
//                                billingTO.setSgstPercentage(sgstPercentage);
//                                billingTO.setIgstAmount(igstAmount);
//                                billingTO.setCgstAmount(cgstAmount);
//                                billingTO.setSgstAmount(sgstAmount);
//                                billingTO.setTotalTax(totalTax);
//                                billingTO.setOrganizationId(organizationId);
//                                billingTO.setBillingState(billingState);
//                                billingTO.setGstNo(GSTNo);
//                                billingTO.setPanNo(PANNo);
//                                billingTO.setOrderCount(noOfOrder);
//                                billingTO.setNoOfTrips(noOfTrips);
//                                billingTO.setUserId("1403");
//                                billingTO.setCustomerId(customerId);
//                                billingTO.setTotalRevenue(totalRevenue);
//                                billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
//                                billingTO.setCustomerAddress(customerAddress);
//                                billingTO.setBillingPartyId(billingPartyId);
//                                billingTO.setBillingParty(billingParty);
//                                billingTO.setRemarks(remarks);
//                                billingTO.setGrandTotal(grandTotal);
//                                billingTO.setFinYear(finYear);
//                                billingTO.setOtherExpense(otherExpense);
//                            }
//
//                            int existStatus = 0;
//                         existStatus = tripBP.getTripExistInBilling(tripId);
//                            System.out.println("existStatus----" + existStatus);
//                            int insertStatus = 0;
//                            if (existStatus == 0) {
//                                insertStatus = billingBP.saveOrderBill(billingTO, 1403, finYear);
//                                System.out.println("insertStatus :" + insertStatus);
//                            int tripGst = billingBP.updateTripGst(String.valueOf(1403), tripId,
//                                    tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
//                            }
//
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                ////////////////////// arun started on 15/09/2020 ////////////////
                //here start
                try {
                    System.out.println("Invoice Mail");
                    ReportTO reportTO = new ReportTO();
                    ArrayList invoiceListEmail = new ArrayList();
                    invoiceListEmail = reportBP.getInvoiceListEmail();
                    System.out.println("getInvoiceListEmail..####..--------------" + invoiceListEmail.size());
                    Iterator itr;
                    itr = invoiceListEmail.iterator();
                    ReportTO rep = null;
                    while (itr.hasNext()) {
                        rep = new ReportTO();
                        rep = (ReportTO) itr.next();
                        String invoiceid = rep.getInvoiceId();
                        String invoiceCode = rep.getInvoiceCode();
                        reportTO.setInvoiceId(invoiceid);
                        reportTO.setCustId(rep.getCustomerId());

                        String content = reportBP.createInvoiceFile(invoiceid, invoiceCode);
                        System.out.println("content = " + content);

                        int insertStatus = 0;
                        if (!"".equals(content)) {
                            reportTO.setFileName(content);
                            insertStatus = reportBP.insertInvoiceFileDetails(reportTO, null);
                            System.out.println("insertStatus :::" + insertStatus);
                        }
                        if (insertStatus > 0) {
                            int invUpdate = reportBP.updateInvoiceCustomerMail(reportTO);
                            System.out.println("invUpdate : " + invUpdate);
                        }
                    }

                    ////////////////////////////// Send Mail Part ////////////////////////
                    System.out.println("send Mail");

                    System.out.println("timeTemps[0] :" + timeTemps[0]);
                    System.out.println("timeTemps[1] :" + timeTemps[1]);
                    int mailtime = Integer.parseInt(timeTemps[0]);
//                    if (mailtime == 1) {

                    String custInvMailTime = "";
                    if (ThrottleConstants.PROPS_VALUES.get("custInvMailTime") != null) {
                        custInvMailTime = ThrottleConstants.PROPS_VALUES.get("custInvMailTime");
                    } else {
                        custInvMailTime = ThrottleConstants.custInvMailTime;
                    }

                    System.out.println("custInvMailTime:" + custInvMailTime);
                    String[] custProps = custInvMailTime.split(":");
                    System.out.println("hp---custInvMailTime---------- " + timeTemp[0]);
                    System.out.println("hp---custInvMailTime---------- " + timeTemp[1]);
                    System.out.println("hp---custProp[0]---------- " + custProps[0]);
                    System.out.println("hp-----custProp[1]-------- " + custProps[1]);
                    System.out.println("runMinute-------- xxxxxxxxxxxxx-----------------------" + runMinute);
//                if(runMinute % 2 == 0){

                    int hrs = Integer.parseInt(timeTemp[0]);
                    int mns = Integer.parseInt(timeTemp[1]);
//                    if (hrs > 18 && mns %30 == 0 && hrs<24) {
                    if (hrs > 18 && mns % 30 == 0 && hrs < 24) {

                        System.out.println("send mail starter :" + timeTemps[0]);
                        System.out.println("send mail starter :" + timeTemps[1]);

                        String mailTemplate = "";
                        mailTemplate = reportBP.getMailTemplate("1", null);
                        mailTemplate = mailTemplate.replaceAll("DTIME", systemTime + " " + 0);

                        System.out.println("send mail mailTemplate :" + mailTemplate);

                        ArrayList uniqueCustomerList = new ArrayList();
                        uniqueCustomerList = reportBP.getUniqueCustomerList();
                        System.out.println("uniqueCustomerList-controller---" + uniqueCustomerList.size());
                        Iterator itr10;
                        itr10 = uniqueCustomerList.iterator();
                        ReportTO rep10 = null;

                        while (itr10.hasNext()) {
                            rep10 = new ReportTO();
                            rep10 = (ReportTO) itr10.next();

                            String customerId = rep10.getCustomerId();
                            System.out.println("customerId---xxxxxxx----------" + customerId);

                            ArrayList customerList = new ArrayList();
                            customerList = reportBP.getCustomerListForEmail(customerId);
                            System.out.println("customerList-controller---" + customerList.size());
                            Iterator itr0;
                            itr0 = customerList.iterator();
                            ReportTO rep0 = null;

                            while (itr0.hasNext()) {
                                rep0 = new ReportTO();
                                rep0 = (ReportTO) itr0.next();
                                reportTO.setInvIds(rep0.getInvIds());
                                String invd = reportTO.getInvIds();
                                String custId = rep0.getCustomerId();
                                String custMailId = rep0.getEmailId();
                                String pdfname = rep0.getPdfName();
                                String customerName = rep0.getCustomerName();
                                String subject = "TMS Invoice - " + customerName;
                                System.out.println("subject---" + subject);
                                String recTo1 = custMailId;
                                String recCc1 = ThrottleConstants.custInvMailIds;
                                String recBcc1 = "arun@ampsolutions.co.in";
                                System.out.println("recTo1--" + recTo1);
                                System.out.println("recCc1--" + recCc1);
                                System.out.println("recBcc1--" + recBcc1);
                                System.out.println("pdfname--" + pdfname);
//                            String recCc1 = "natarajans@entitlesolutions.com,arun@entitlesolutions.com";

                                SendEmail em = new SendEmail();
//                            
                                int check = 0;
//                            int getStatus=financeBP.getStatus();
//                            if (getStatus == 1) {
//                            check = em.sendMultipleAttachPDFMail(recTo1, recCc1, recBcc1, subject , mailTemplate, pdfname);
                                new SendEmailReport(recTo1, recCc1, recBcc1, subject, mailTemplate, pdfname, invd).start();
//
//                            if (check > 0) {
//                                int invUpdate = reportBP.updateInvoiceCustomerSendMail(reportTO);
////                                System.out.println("invUpdate : " + invUpdate);
//                            }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("invoice mail error");
                }

                //here end
                // supplementary invoice mail
                try {

                    System.out.println("Invoice Mail");
                    ReportTO reportTO = new ReportTO();
                    if (runMinute % 5 == 0) {
                        ArrayList invoiceList = new ArrayList();
                        invoiceList = reportBP.getSuppInvoiceListEmail();
                        System.out.println("invoiceList..####.." + invoiceList.size());
                        Iterator itr;
                        itr = invoiceList.iterator();
                        ReportTO rep = null;
                        while (itr.hasNext()) {
                            rep = new ReportTO();
                            rep = (ReportTO) itr.next();
                            String invoiceid = rep.getInvoiceId();
                            String invoiceCode = rep.getInvoiceCode();
                            reportTO.setInvoiceId(invoiceid);
                            reportTO.setCustId(rep.getCustomerId());
                            reportTO.setQrcode(rep.getQrcode());
                            reportTO.setIrn(rep.getIrn());

                            String content = reportBP.createSuppInvoiceFile(invoiceid, invoiceCode);
                            System.out.println("supplement content = " + content);

                            int insertStatus = 0;
                            if (!"".equals(content)) {
                                reportTO.setFileName(content);
                                insertStatus = reportBP.insertSuppInvoiceFileDetails(reportTO, null);
                                System.out.println("insertStatus :::" + insertStatus);
                            }
                            if (insertStatus > 0) {
                                int invUpdate = reportBP.updateSuppInvoiceCustomerMail(reportTO);
                                System.out.println("invUpdate : " + invUpdate);
                            }
                        }
                    }

                    ////////////////////////////// Send Mail Part ////////////////////////
                    System.out.println("send Mail");

                    System.out.println("timeTemps[0] :" + timeTemps[0]);
                    System.out.println("timeTemps[1] :" + timeTemps[1]);
                    int mailtime = Integer.parseInt(timeTemps[0]);
//                    if (mailtime == 1) {

                    String custInvMailTime = "";
                    if (ThrottleConstants.PROPS_VALUES.get("custInvMailTime") != null) {
                        custInvMailTime = ThrottleConstants.PROPS_VALUES.get("custInvMailTime");
                    } else {
                        custInvMailTime = ThrottleConstants.custInvMailTime;
                    }

                    System.out.println("custInvMailTime:" + custInvMailTime);
                    String[] custProps = custInvMailTime.split(":");
                    System.out.println("hp---custInvMailTime---------- " + timeTemp[0]);
                    System.out.println("hp---custInvMailTime---------- " + timeTemp[1]);
                    System.out.println("hp---custProp[0]---------- " + custProps[0]);
                    System.out.println("hp-----custProp[1]-------- " + custProps[1]);
                    int hrs = Integer.parseInt(timeTemp[0]);
                    int mns = Integer.parseInt(timeTemp[1]);
                    if (hrs > 18 && mns % 30 == 0 && hrs < 24) {
//                if(runMinute % 5 == 0){  
                        System.out.println("send mail starter :" + timeTemps[0]);
                        System.out.println("send mail starter :" + timeTemps[1]);

                        String mailTemplate = "";
                        mailTemplate = reportBP.getMailTemplate("1", null);
                        mailTemplate = mailTemplate.replaceAll("DTIME", systemTime + " " + 0);

                        ArrayList uniqueSuppCustomerList = new ArrayList();
                        uniqueSuppCustomerList = reportBP.uniqueSuppCustomerList();
                        System.out.println("uniqueSuppCustomerList-controller---" + uniqueSuppCustomerList.size());
                        Iterator itr11;
                        itr11 = uniqueSuppCustomerList.iterator();
                        ReportTO rep11 = null;

                        while (itr11.hasNext()) {
                            rep11 = new ReportTO();
                            rep11 = (ReportTO) itr11.next();

                            String customerId = rep11.getCustomerId();
                            System.out.println("customerId---xxxxxxx----------" + customerId);
                            ArrayList customerList = new ArrayList();
//                        customerList = reportBP.getCustomerListForEmail();
                            customerList = reportBP.getSuppCustomerListForEmail(customerId);
                            System.out.println("customerList-controller---" + customerList.size());
                            Iterator itr0;
                            itr0 = customerList.iterator();
                            ReportTO rep0 = null;

                            while (itr0.hasNext()) {
                                rep0 = new ReportTO();
                                rep0 = (ReportTO) itr0.next();
                                reportTO.setInvIds(rep0.getInvIds());
                                String custId = rep0.getCustomerId();
                                String custMailId = rep0.getEmailId();
                                String pdfname = rep0.getPdfName();
                                String invd = reportTO.getInvIds();
                                String customerName = rep0.getCustomerName();
                                String subject = "TMS Supplementary Invoice - " + customerName;
                                System.out.println("subject---" + subject);
                                String recTo1 = custMailId;
                                String recCc1 = ThrottleConstants.custInvMailIds;
                                String recBcc1 = "priyeshr@ict.in";
                                System.out.println("recTo1--" + recTo1);
                                System.out.println("recCc1--" + recCc1);
                                System.out.println("recBcc1--" + recBcc1);
                                System.out.println("pdfname--" + pdfname);
//                            String recCc1 = "natarajans@entitlesolutions.com,arun@entitlesolutions.com";

                                SendEmail em = new SendEmail();
                                System.out.println("haiiiiiiiiiiiiiiiii :AA " + pdfname);
                                System.out.println("");
                                int check = 0;

//                            String getStatus=financeBP.getStatus();
//                            if (getStatus == 1) {
//                            check = em.sendMultipleAttachPDFMail(recTo1, recCc1, recBcc1, subject , mailTemplate, pdfname);
                                new SendEmailReportForSupp(recTo1, recCc1, recBcc1, subject, mailTemplate, pdfname, invd).start();
//                            System.out.println("check===" + check);
//                            }
//                            if (check > 0) {
//                                int invUpdate = reportBP.updateSuppInvoiceCustomerSendMail(reportTO);
//                                System.out.println("invUpdate : " + invUpdate);
//                            }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("invoice mail error");
                }

                //invoice resend code start
//                try {
//                    if (runMinute % 30 == 0) {
//                        ReportTO reportTO = new ReportTO();
//                        ArrayList resendInvoiceList = new ArrayList();
//                        resendInvoiceList = reportBP.resendInvoiceList();
//                        System.out.println("resendInvoiceList...." + resendInvoiceList.size());
//                        Iterator itr;
//                        itr = resendInvoiceList.iterator();
//                        ReportTO rep = null;
//                        while (itr.hasNext()) {
//                            rep = new ReportTO();
//                            rep = (ReportTO) itr.next();
//                            String id = rep.getId();
//                            String fromDate = rep.getFromDate();
//                            String toDate = rep.getToDate();
//                            String custId = rep.getCustId();
//
//                            /////////////////////////////////////////////
//                            ArrayList getResendInvoiceListEmail = new ArrayList();
//                            getResendInvoiceListEmail = reportBP.getResendInvoiceListEmail(fromDate, toDate, custId);
//                            System.out.println("getResendInvoiceListEmail..####.." + getResendInvoiceListEmail.size());
//                            Iterator itr2;
//                            itr2 = getResendInvoiceListEmail.iterator();
//
//                            while (itr2.hasNext()) {
//                                rep = new ReportTO();
//                                rep = (ReportTO) itr2.next();
//                                String invoiceid = rep.getInvoiceId();
//                                String invoiceCode = rep.getInvoiceCode();
//                                reportTO.setInvoiceId(invoiceid);
//                                reportTO.setCustId(rep.getCustomerId());
//
//                                System.out.println("invoiceCode----------" + invoiceCode);
//
//                                String content = reportBP.createInvoiceFile(invoiceid, invoiceCode);
//                                System.out.println("content = " + content);
//
//                                int insertStatus = 0;
//                                if (!"".equals(content)) {
//                                    reportTO.setFileName(content);
//                                    insertStatus = reportBP.insertInvoiceFileDetails(reportTO, null);
//                                    System.out.println("insertStatus :::" + insertStatus);
//                                }
//                                if (insertStatus > 0) {
//                                    int invUpdate = reportBP.updateResendCustomerMail(id);
//                                    System.out.println("invUpdate : " + invUpdate);
//                                }
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("Resend Invoice mail error");
//                }
                //invoice resend code start
                // supplementary invoice mail
                ////////////////// Credit Limit //////////////////////////////////
                timeTemp = dateTemp[1].split(":");
                today = new Date();
                calendar = Calendar.getInstance();
                calendar.setTime(today);

                String tempCreditAmountRevert = ThrottleConstants.contractdiffTime;
                timeTemp = dateTemp[1].split(":");
                System.out.println("tempCreditAmountRevert:" + tempCreditAmountRevert);
                System.out.println("timeTemp[0]:" + timeTemp[0]);
                String[] tempCreditAmountRevertProp = tempCreditAmountRevert.split(":");
                System.out.println("tempCreditAmountRevertProp[0]:" + tempCreditAmountRevertProp[0]);
                System.out.println("tempCreditAmountRevertProp[1]:" + tempCreditAmountRevertProp[1]);

                if (timeTemp[0].equals(tempCreditAmountRevertProp[0]) && ((Integer.parseInt(timeTemp[1]) <= Integer.parseInt(tempCreditAmountRevertProp[1])))) {
                    System.out.println("updateTempCreditAmount triggered--");
                    //    schedulerBP.updateTempCreditAmount();
                }

                //  Customer contract expiry alert
                //check eligible trip to send start Email
                ArrayList eligibleTrips = new ArrayList();
                String vehicleNo = "";
                String startLandMark = "[USER LANDMARK] CHOWKI DHANI PARKING";
                String endLandMark = "[USER LANDMARK]";
                try {
                    System.out.println("getEligibleTripForStartEmail= ++++++++______+++++++++++");
                    // arun closed  eligibleTrips = schedulerBP.getEligibleTripForStartEmail();
                    SchedulerTO schTO1 = new SchedulerTO();
                    Iterator itr1 = eligibleTrips.iterator();
                    while (itr1.hasNext()) {
                        schTO1 = (SchedulerTO) itr1.next();
                        vehicleNo = schTO1.getVehicleNo();
                        ArrayList latlong = new ArrayList();
                        latlong = schedulerBP.getLatLong(schTO1.getVehicleNo());
                        String lat = "", lon = "", vehicleLocation = "";
                        String[] fullLoction = null;
                        Iterator itr2 = latlong.iterator();
                        SchedulerTO schTO2 = new SchedulerTO();
                        while (itr2.hasNext()) {
                            schTO2 = (SchedulerTO) itr2.next();
                            lat = schTO2.getLatitude();
                            lon = schTO2.getLongitude();
                            if (schTO2.getLocation() != null && !"".equals(schTO2.getLocation()) && schTO2.getLocation().contains(startLandMark)) {
                                fullLoction = schTO2.getLocation().split("K");
                                vehicleLocation = fullLoction[0];
                            }
                            // calculate distance using lat and long
                            String line, outputString = "";
                            //                try{
                            //                 URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+sourceLat+","+sourceLong+"&destinations="+lat+","+lon+"&key=AIzaSyDoYZk9NSBDKBQW5AYu0Vuq88IC0ue3mZI");
                            //              //   System.out.println("url::::"+url.toString());
                            //                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            //                conn.setRequestMethod("GET");
                            //                
                            //                BufferedReader reader = new BufferedReader(
                            //                new InputStreamReader(conn.getInputStream()));
                            //                while ((line = reader.readLine()) != null) {
                            //                    outputString += line;
                            //                }
                            //         //    System.out.println(outputString);
                            //                }catch(Exception ex){
                            //                continue;
                            //                }
                            //             DistancePojo distancePojo = new Gson().fromJson(outputString, DistancePojo.class);
                            //             System.out.println(distancePojo);
                            //             Rows[] rows= distancePojo.getRows();
                            //             Elements[] element= rows[0].getElements();
                            //             Distance distance=element[0].getDistance();
                            //             Duration duration=element[0].getDuration();
                            //             String durSection=duration.getValue();
                            //             String dis=  distance.getText().replaceAll("km","");
                            //              dis=dis.replaceAll("m","");
                            //             dis=dis.trim();
                            //          System.out.println("distance between::::"+dis);
                            //             if(Double.parseDouble(dis) >= 5.00){
                            //                 String flag="start";
                            //                schedulerBP.sendStartEmail(schTO1.getTripId(),durSection,flag);
                            //                  }
                            System.out.println("vehicleLocation km for start:" + vehicleLocation);
                            if (Double.parseDouble(vehicleLocation) >= 0.1) {
                                String flag = "start";
                                schedulerBP.sendStartEmail(schTO1.getTripId(), "", flag);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("getEligibleTripForStartEmail");
                }

                // end trip email start
                ArrayList endTripList = new ArrayList();
                String endVehicle = "";
                String endTripId = "", operationPoint = "", cityLat = "", cityLong = "";
                try {
                    // arun closed      endTripList = schedulerBP.getEligibleTripForEndEmail();
                    SchedulerTO schTO2 = new SchedulerTO();
                    Iterator itr2 = endTripList.iterator();
                    while (itr2.hasNext()) {
                        schTO2 = (SchedulerTO) itr2.next();
                        endVehicle = schTO2.getVehicleNo();
                        endTripId = schTO2.getTripId();
                        operationPoint = schTO2.getOperationPoint();

                        //vehicle lat long
                        ArrayList vehiclelatlong = new ArrayList();
                        vehiclelatlong = schedulerBP.getLatLong(schTO2.getVehicleNo());
                        String vehlat = "", vehlon = "";
                        Iterator itr4 = vehiclelatlong.iterator();
                        SchedulerTO schTO4 = new SchedulerTO();
                        while (itr4.hasNext()) {
                            schTO4 = (SchedulerTO) itr4.next();
                            vehlat = schTO4.getLatitude();
                            vehlon = schTO4.getLongitude();
                            // }
                            ArrayList latlong = new ArrayList();
                            System.out.println("schTO2.getOperationPoint():" + schTO2.getOperationPoint());
                            //   latlong=schedulerBP.getCityLatLong(schTO2.getOperationPoint());
                            String lat = "", lon = "", vehicleLocation = "";
                            String[] fullLoction = null;
                            // Iterator itr3 = latlong.iterator();
                            // SchedulerTO schTO3 = new SchedulerTO();
                            //                 while (itr3.hasNext()) {
                            //                 schTO3 = (SchedulerTO) itr3.next();
                            //                 lat=schTO3.getLatitude();
                            //                 lon=schTO3.getLongitude();
                            // calculate distance using lat and long
                            String line, outputString = "";
                            if (schTO4.getLocation() != null && !"".equals(schTO4.getLocation()) && !schTO4.getLocation().contains(startLandMark) && schTO4.getLocation().contains(endLandMark)) {
                                fullLoction = schTO4.getLocation().split("K");
                                vehicleLocation = fullLoction[0];
                            }
                            //                try{
                            //                 URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+vehlat+","+vehlon+"&destinations="+lat+","+lon+"&key=AIzaSyDoYZk9NSBDKBQW5AYu0Vuq88IC0ue3mZI");
                            //                 System.out.println("url::::"+url.toString());
                            //                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            //                conn.setRequestMethod("GET");
                            //                
                            //                BufferedReader reader = new BufferedReader(
                            //                new InputStreamReader(conn.getInputStream()));
                            //                while ((line = reader.readLine()) != null) {
                            //                    outputString += line;
                            //                }
                            //             System.out.println(outputString);
                            //                }catch(Exception ex){
                            //                continue;
                            //                }
                            //                
                            //             DistancePojo distancePojo = new Gson().fromJson(outputString, DistancePojo.class);
                            //             System.out.println(distancePojo);
                            //             Rows[] rows= distancePojo.getRows();
                            //             Elements[] element= rows[0].getElements();
                            //             Distance distance=element[0].getDistance();
                            //             Duration duration=element[0].getDuration();
                            //             String durSection="0";
                            //             if(duration !=null){
                            //              durSection=duration.getValue();
                            //             }
                            //             if(distance !=null){
                            //             String dis=  distance.getText().replaceAll("km","");
                            //             dis=dis.replaceAll("m","");
                            //             dis=dis.trim();
                            //             System.out.println("distance between::::"+dis);
                            //            
                            //             if(Double.parseDouble(dis) == 0.00 || Double.parseDouble(dis) <= 1.00){
                            //                 String flag="end";
                            //                schedulerBP.sendStartEmail(schTO2.getTripId(),durSection,flag);
                            //                  }
                            //                 }

                            System.out.println("vehicleLocation km for End:" + vehicleLocation);
                            if (Double.parseDouble(vehicleLocation) >= 0.1) {
                                String flag = "end";
                                schedulerBP.sendStartEmail(schTO2.getTripId(), "", flag);
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println("exception in getEligibleTripForEndEmail");
                    e.printStackTrace();
                }
                ///End trip Email Ends Here  

                String DailyUserActivityTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyUserActivityTime") != null) {
                    DailyUserActivityTime = ThrottleConstants.PROPS_VALUES.get("dailyUserActivityTime");

                } else {
                    DailyUserActivityTime = ThrottleConstants.dailyUserActivityTime;
                }

                timeTemp = dateTemp[1].split(":");
                String[] propertyTemp = DailyUserActivityTime.split(":");
                System.out.println("DailyUserActivityTime:" + DailyUserActivityTime);
                //                System.out.println("propertyTemp:"+propertyTemp[0]+":"+propertyTemp[1]);
                //            Daily User Activity Report

                if (timeTemp[0].equals(propertyTemp[0]) && (timeTemp[1].equals(propertyTemp[1]) || timeTemp[2].equals("00"))) {
                    System.out.println("Raj " + timeTemp[0]);
                    ReportTO reportTO = new ReportTO();
                    TripTO tripTO = new TripTO();
                    String emailFormat = "";
//                    emailFormat = schedulerBP.getUserActivitySummary();
                    System.out.println("content" + emailFormat);
                    tripTO.setMailTypeId("2");
                    tripTO.setMailSubjectTo("Daily User Activity Report");
                    tripTO.setMailSubjectCc("Daily User Activity Report");
                    tripTO.setMailSubjectBcc("Daily User Activity Report");
                    tripTO.setMailContentTo(emailFormat);
                    tripTO.setMailContentCc(emailFormat);
                    tripTO.setMailContentBcc(emailFormat);

                    tripTO.setMailIdTo(ThrottleConstants.userActivityToMailId);
                    tripTO.setMailIdCc(ThrottleConstants.userActivityCcMailId);
                    tripTO.setMailIdBcc(ThrottleConstants.userActivityBccMailId);
                    Integer mails = tripBP.insertMailDetails(tripTO, 1);
                    System.out.println("insertMailDetails@@@" + mails);

                }
                //            Daily User Activity Report

                //daily Gr summary Report
                String dailyGRSummaryTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyGRSummaryTime") != null) {
                    dailyGRSummaryTime = ThrottleConstants.PROPS_VALUES.get("dailyGRSummaryTime");
                } else {
                    dailyGRSummaryTime = ThrottleConstants.dailyGRSummaryTime;
                }
                System.out.println("dailyGRSummaryTime:" + dailyGRSummaryTime);
                String[] GRProp = dailyGRSummaryTime.split(":");
                if (timeTemp[0].equals(GRProp[0]) && (timeTemp[1].equals(GRProp[1]) || timeTemp[1].equals("00"))) {
                    System.out.println("Raj " + timeTemp[0]);
                    //                ReportTO reportTO = new ReportTO();
                    TripTO tripTO = new TripTO();
                    String emailFormat = "";
//                    emailFormat = schedulerBP.getGRCreatedSummary();
                    System.out.println("content" + emailFormat);
                    tripTO.setMailTypeId("2");
                    tripTO.setMailSubjectTo("Daily GR Created Report");
                    tripTO.setMailSubjectCc("Daily GR Created Report");
                    tripTO.setMailSubjectBcc("Daily GR Created Report");
                    tripTO.setMailContentTo(emailFormat);
                    tripTO.setMailContentCc(emailFormat);
                    tripTO.setMailContentBcc(emailFormat);

                    tripTO.setMailIdTo(ThrottleConstants.grSummaryToMailId);
                    tripTO.setMailIdCc(ThrottleConstants.grSummaryCcMailId);
                    tripTO.setMailIdBcc(ThrottleConstants.grSummaryBccMailId);
                    Integer mails = tripBP.insertMailDetails(tripTO, 1);
                    System.out.println("insertMailDetails@@@" + mails);

                }
                // daily Gr summary Report  

                //DVM SCHEDULER
                String dvmTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dvmTime") != null) {
                    dvmTime = ThrottleConstants.PROPS_VALUES.get("dvmTime");
                } else {
                    dvmTime = ThrottleConstants.dvmTime;
                }
                System.out.println("dvmTime:" + dvmTime);
                String[] dvmProp = dvmTime.split(":");
                int dvmstartRange = Integer.parseInt(dvmProp[1]) - 2;
                int dvmendRange = Integer.parseInt(dvmProp[1]) + 2;
                if (timeTemp[0].equals(dvmProp[0]) && ((dvmstartRange <= Integer.parseInt(timeTemp[1])) && (Integer.parseInt(timeTemp[1]) <= dvmendRange))) {
                    SchedulerTO schTO = new SchedulerTO();
                    String emailFormat = "";
                    ArrayList dvmDetailsDays = new ArrayList();
//                    dvmDetailsDays = schedulerBP.getDailyVehicleMovement();
                    System.out.println("dvmDetailsDays = " + dvmDetailsDays);
                }
                //DVM SCHEDULER

                //MonthlyOrderStatus SCHEDULER
                String monthlyOrderStatus = "";
                if (ThrottleConstants.PROPS_VALUES.get("monthlyOrderSummaryTime") != null) {
                    monthlyOrderStatus = ThrottleConstants.PROPS_VALUES.get("monthlyOrderSummaryTime");
                } else {
                    monthlyOrderStatus = ThrottleConstants.monthlyOrderSummaryTime;
                }
                System.out.println("monthlyOrderStatus:" + monthlyOrderStatus);
                String[] monthlyProp = monthlyOrderStatus.split(":");
                if (timeTemp[0].equals(monthlyProp[0]) && (timeTemp[1].equals(monthlyProp[1]) || timeTemp[1].equals("00"))) {
                    ArrayList getMonthlyOrderStatus = new ArrayList();
//                    getMonthlyOrderStatus = schedulerBP.getMonthlyOrderStatus();
                    System.out.println("getMonthlyOrderStatus() = " + getMonthlyOrderStatus);
                    System.out.println("getMonthlyOrderStatus() = " + getMonthlyOrderStatus.size());
                }

                //MonthlyOrderStatus SCHEDULER
                //daily order Generated SCHEDULER
                String dailyOrderStatus = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyOrderSummaryTime") != null) {
                    dailyOrderStatus = ThrottleConstants.PROPS_VALUES.get("dailyOrderSummaryTime");
                } else {
                    dailyOrderStatus = ThrottleConstants.dailyOrderSummaryTime;
                }
                System.out.println("dailyOrderStatus:" + dailyOrderStatus);
                String[] dailyProp = dailyOrderStatus.split(":");
                if (timeTemp[0].equals(dailyProp[0]) && (timeTemp[1].equals(dailyProp[1]) || timeTemp[1].equals("00"))) {
                    ArrayList DailyOrderGenerated = new ArrayList();
//                    DailyOrderGenerated = schedulerBP.getDailyOrderGenerated();
                    System.out.println("getDailyOrderGenerated() = " + DailyOrderGenerated);
                    System.out.println("getDailyOrderGenerated() = " + DailyOrderGenerated.size());
                }
                //daily order Generated SCHEDULER

                //daily vehicle trip  SCHEDULER
                String dailyVehicleUpdateTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyVehicleUpdateTime") != null) {
                    dailyVehicleUpdateTime = ThrottleConstants.PROPS_VALUES.get("dailyVehicleUpdateTime");
                } else {
                    dailyVehicleUpdateTime = ThrottleConstants.dailyVehicleUpdateTime;
                }
                System.out.println("dailyVehicleUpdateTime:" + dailyVehicleUpdateTime);
                String[] dailyVehicleUpdateProp = dailyVehicleUpdateTime.split(":");
                if (timeTemp[0].equals(dailyVehicleUpdateProp[0]) && (timeTemp[1].equals(dailyVehicleUpdateProp[1]) || timeTemp[1].equals("10"))) {
                    //                    ArrayList DailyVehicleTripAlerts = new ArrayList();
                    Integer DailyVehicleTripAlerts = 0;
//                    DailyVehicleTripAlerts = schedulerBP.getDailyVehicleTripAlerts();
                    System.out.println("getDailyVehicleTripAlerts() = " + DailyVehicleTripAlerts);
                    //                    System.out.println("getDailyVehicleTripAlerts() = " + DailyVehicleTripAlerts.size());
                }
                //daily order Generated SCHEDULER

                //DailyProcessMain  SCHEDULER  
                String dailyProcessTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyProcessTime") != null) {
                    dailyProcessTime = ThrottleConstants.PROPS_VALUES.get("dailyProcessTime");
                } else {
                    dailyProcessTime = ThrottleConstants.dailyProcessTime;
                }
                System.out.println("dailyProcessTime:" + dailyProcessTime);
                String[] dailyProcessTimeProp = dailyProcessTime.split(":");
                int startRange = Integer.parseInt(dailyProcessTimeProp[1]) - 2;
                int endRange = Integer.parseInt(dailyProcessTimeProp[1]) + 2;
                System.out.println("startRange:" + startRange + "endRange:" + endRange);
                if (timeTemp[0].equals(dailyProcessTimeProp[0]) && ((startRange <= Integer.parseInt(timeTemp[1])) && (Integer.parseInt(timeTemp[1]) <= endRange))) {
                    //if (timeTemp[0].equals(dailyProcessTimeProp[0]) && (timeTemp[1].equals(dailyProcessTimeProp[1]) || timeTemp[2].equals("00"))) {
                    ArrayList getDailyProcessMain = new ArrayList();
//                    getDailyProcessMain = schedulerBP.getDailyProcessMain();
                    System.out.println("getDailyProcessMain() = " + getDailyProcessMain);
                    System.out.println("getDailyProcessMain() = " + getDailyProcessMain.size());
                }
                //DailyProcessMain SCHEDULER

                //ProcessMain  SCHEDULER  
                String vehicleComplianceAlertTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("vehicleComplianceAlertTime") != null) {
                    vehicleComplianceAlertTime = ThrottleConstants.PROPS_VALUES.get("vehicleComplianceAlertTime");
                } else {
                    vehicleComplianceAlertTime = ThrottleConstants.vehicleComplianceAlertTime;
                }
                System.out.println("vehicleComplianceAlertTime:" + vehicleComplianceAlertTime);
                String[] vehicleComplianceProp = vehicleComplianceAlertTime.split(":");
                if (timeTemp[0].equals(vehicleComplianceProp[0]) && (timeTemp[1].equals(vehicleComplianceProp[1]) || timeTemp[2].equals("00"))) {
                    ArrayList getProcessMain = new ArrayList();
//                    getProcessMain = schedulerBP.getProcessMain();
                    System.out.println("getProcessMain() = " + getProcessMain);
                    System.out.println("getProcessMain() = " + getProcessMain.size());
                }
                //ProcessMain SCHEDULER
                // daily Trip Status start
                String dailyTripAlertTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("dailyTripStatusTime") != null) {
                    dailyTripAlertTime = ThrottleConstants.PROPS_VALUES.get("dailyTripStatusTime");
                } else {
                    dailyTripAlertTime = ThrottleConstants.dailyTripStatusTime;
                }
                System.out.println("dailyTripAlertTime:" + dailyTripAlertTime);
                String[] dailyTripAlertTimeProp = dailyTripAlertTime.split(":");
                if (timeTemp[0].equals(dailyTripAlertTimeProp[0]) && ((Integer.parseInt(timeTemp[1]) <= Integer.parseInt(dailyTripAlertTimeProp[1]) + 3) && (Integer.parseInt(timeTemp[1]) >= Integer.parseInt(dailyTripAlertTimeProp[1]) - 2))) {
                    ArrayList ownLeasedTripCount = new ArrayList();
//                    ownLeasedTripCount = schedulerBP.getOwnLeasedTripCountEmail();
                    System.out.println("ownLeasedTripCount() = " + ownLeasedTripCount);
                    System.out.println("ownLeasedTripCount() = " + ownLeasedTripCount.size());
                }
                // // daily Trip Status End                
                //Monthly billing   SCHEDULER

                calendar.setTime(today);

                calendar.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.add(Calendar.DATE, 1);

                Date firstDayOfMonth = calendar.getTime();

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                System.out.println("Today            : " + df.format(today));
                System.out.println("Last Day of Month: " + df.format(firstDayOfMonth));

                if (today.equals(firstDayOfMonth)) {
                    // if(timeTemp[0].equals("12") && (timeTemp[1].equals("35") || timeTemp[1].equals("00"))){
                    System.out.println("i m calling ");
                    ArrayList getMonthlyBillingDetails = new ArrayList();
//                    getMonthlyBillingDetails = schedulerBP.getMonthlyBillingDetails();
                    // System.out.println("getMonthlyBillingDetails() = " + getMonthlyBillingDetails);
                    System.out.println("getMonthlyBillingDetails() = " + getMonthlyBillingDetails.size());
                }
                String vehicleutilisedTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("vehicleutilisedTime") != null) {
                    vehicleutilisedTime = ThrottleConstants.PROPS_VALUES.get("vehicleutilisedTime");
                } else {
                    vehicleutilisedTime = ThrottleConstants.vehicleutilisedTime;
                }
                System.out.println("vehicleutilisedTime:" + dailyTripAlertTime);
                String[] vehicleutilisedTimeProp = vehicleutilisedTime.split(":");
                if (timeTemp[0].equals(vehicleutilisedTimeProp[0]) && ((Integer.parseInt(timeTemp[1]) <= Integer.parseInt(vehicleutilisedTimeProp[1]) + 3) && (Integer.parseInt(timeTemp[1]) >= Integer.parseInt(vehicleutilisedTimeProp[1]) - 2))) {
                    System.out.println("DailyVehicleUtilised triggered--");
//                    schedulerBP.getDailyVehicleUtilised();
                }
                String contractdiffTime = "";
                if (ThrottleConstants.PROPS_VALUES.get("contractdiffTime") != null) {
                    contractdiffTime = ThrottleConstants.PROPS_VALUES.get("contractdiffTime");
                } else {
                    contractdiffTime = ThrottleConstants.contractdiffTime;
                }
                System.out.println("vehicleutilisedTime:" + dailyTripAlertTime);
                String[] contractdiffTimeProp = contractdiffTime.split(":");
                if (timeTemp[0].equals(contractdiffTimeProp[0]) && ((Integer.parseInt(timeTemp[1]) <= Integer.parseInt(contractdiffTimeProp[1]) + 3) && (Integer.parseInt(timeTemp[1]) >= Integer.parseInt(contractdiffTimeProp[1]) - 2))) {
                    System.out.println("ContractDiffAlert triggered--");
//                    schedulerBP.getContractDiffAlert();
                }
//              invoice xml  part start
//                System.out.println("custInvMailTime:" + custInvMailTime);
//                    String[] custProps = custInvMailTime.split(":");
                System.out.println("hp---xml create---------- " + timeTemp[0]);
                System.out.println("hp---xml create---------- " + timeTemp[1]);

                int hrss = Integer.parseInt(timeTemp[0]);
                int mnss = Integer.parseInt(timeTemp[1]);
//                    if (hrs > 18 && mns %30 == 0 && hrs<24) {
                if (hrss % 18 == 0 && mnss % 32 == 0) {

                    System.out.println("helllo--------------------------------------------------------------------------------------------------------------");
                    ArrayList invoiceListForXml = reportBP.invoiceListForXml();

                    Iterator itr;
                    itr = invoiceListForXml.iterator();
                    ReportTO reportO = null;
                    int i = 1;
                    while (itr.hasNext()) {
                        reportO = new ReportTO();
                        reportO = (ReportTO) itr.next();
                        String id = reportO.getInvoiceId();
                        String fromDate = reportO.getFromDate();
                        String toDate = reportO.getToDate();
                        String createInvoiceXmlFile = reportBP.getInvoiceXMLData(fromDate, toDate, id, i);
                        i++;

                    }

                    ArrayList suppInvoiceListForXml = reportBP.suppInvoiceListForXml();

                    Iterator itr2;
                    itr2 = suppInvoiceListForXml.iterator();
                    ReportTO reportO2 = null;
                    int iiii = 1;
                    while (itr2.hasNext()) {
                        reportO2 = new ReportTO();
                        reportO2 = (ReportTO) itr2.next();
                        String id = "";
                        String fromDate = reportO2.getFromDate();
                        String toDate = reportO2.getToDate();
                        String createInvoiceXmlFile = reportBP.getSuppInvoiceXMLData(fromDate, toDate, id, iiii);
                        iiii++;

                    }
                    ArrayList creaditInvoiceListForXml = reportBP.creaditInvoiceListForXml();

                    Iterator itr3;
                    itr3 = creaditInvoiceListForXml.iterator();
                    ReportTO reportO3 = null;
                    int iii = 1;
                    while (itr3.hasNext()) {
                        reportO3 = new ReportTO();
                        reportO3 = (ReportTO) itr3.next();
                        String id = "";
                        String fromDate = reportO3.getFromDate();
                        String toDate = reportO3.getToDate();
                        String createInvoiceXmlFile = reportBP.getCreditInvoiceXMLData(fromDate, toDate, id, iii);
                        iii++;

                    }
                    ArrayList suppCreaditInvoiceListForXml = reportBP.suppCreaditInvoiceListForXml();

                    Iterator itr4;
                    itr4 = suppCreaditInvoiceListForXml.iterator();
                    ReportTO reportO4 = null;
                    int ii = 1;
                    while (itr4.hasNext()) {
                        reportO4 = new ReportTO();
                        reportO4 = (ReportTO) itr4.next();
                        String id = "";
                        String fromDate = reportO4.getFromDate();
                        String toDate = reportO4.getToDate();
                        String createInvoiceXmlFile = reportBP.getSuppCreditInvoiceXMLData(fromDate, toDate, id, ii);
                        ii++;

                    }
//
                }
//                xml move part
                if (runMinute % 6 == 0) {

                    System.out.println("helllo---------------------------------------------------------------");
                    ArrayList invoiceFileForMove = reportBP.getInvoiceFileForMove();

                    Iterator itr5;
                    itr5 = invoiceFileForMove.iterator();
                    ReportTO reportO5 = null;
                    while (itr5.hasNext()) {
                        reportO5 = new ReportTO();
                        reportO5 = (ReportTO) itr5.next();
                        String id = reportO5.getId();
                        String invoiceId = reportO5.getInvoiceId();
                        String fileName = reportO5.getFileName();
                        String uploadInvToFtp = reportBP.uploadInvToFtp(id, fileName);
                    }

                    ArrayList suppInvoiceFileForMove = reportBP.suppInvoiceFileForMove();

                    Iterator itr6;
                    itr6 = suppInvoiceFileForMove.iterator();
                    ReportTO reportO6 = null;
                    while (itr6.hasNext()) {
                        reportO6 = new ReportTO();
                        reportO6 = (ReportTO) itr6.next();
                        String id = reportO6.getId();
                        String invoiceId = reportO6.getInvoiceId();
                        String fileName = reportO6.getFileName();
                        String uploadSuppInvToFtp = reportBP.uploadSuppInvToFtp(id, fileName);
                    }
//
                    ArrayList creditInvoiceFileForMove = reportBP.creditInvoiceFileForMove();

                    Iterator itr7;
                    itr7 = creditInvoiceFileForMove.iterator();
                    ReportTO reportO7 = null;
                    while (itr7.hasNext()) {
                        reportO7 = new ReportTO();
                        reportO7 = (ReportTO) itr7.next();
                        String id = reportO7.getId();
                        String invoiceId = reportO7.getInvoiceId();
                        String fileName = reportO7.getFileName();
                        String uploadCreditInvToFtp = reportBP.uploadCreditInvToFtp(id, fileName);
                    }
                    ArrayList creditSuppInvoiceFileForMove = reportBP.creditSuppInvoiceFileForMove();

                    Iterator itr8;
                    itr8 = creditSuppInvoiceFileForMove.iterator();
                    ReportTO reportO8 = null;
                    while (itr8.hasNext()) {
                        reportO8 = new ReportTO();
                        reportO8 = (ReportTO) itr8.next();
                        String id = reportO8.getId();
                        String invoiceId = reportO8.getInvoiceId();
                        String fileName = reportO8.getFileName();
                        String uploadSuppCreditInvToFtp = reportBP.uploadSuppCreditInvToFtp(id, fileName);
                    }
//
                }

//                xml download csv file
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        ///e-invoice alert by rohan
        try {

            SendEmail em = new SendEmail();
            String recTo1 = "rohan@ampsolutions.co.in,arun@ampsolutions.co.in";
            String recCc1 = "rohan@ampsolutions.co.in";
            String recBcc1 = "arun@ampsolutions.co.in";
            String subject = "";

            int getEinvoiceList = 0;
            int getESuppinvoiceList = 0;
            int getECreditinvoiceList = 0;
            int getECreditSuppinvoiceList = 0;
            int getAutoEmailList = 0;
            int getAutoEmailSuppList = 0;
            int check = 0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar c = Calendar.getInstance();
            Date date = new Date();
            c.setTime(date);
            String systemTime = sdf.format(c.getTime()).toString();
            System.out.println("systemTime = " + systemTime);
            String[] dateTemp = systemTime.split(" ");
            String[] timeTemps = dateTemp[1].split(":");

            String[] timeTemp = dateTemp[1].split(":");
            int hrs = Integer.parseInt(timeTemp[0]);
            int mns = Integer.parseInt(timeTemp[1]);
            if (hrs == 1000000000) {
                if (mns == 100000000) {

                    getEinvoiceList = reportBP.getEinvoiceList();
                    getESuppinvoiceList = reportBP.getESuppinvoiceList();
                    getECreditinvoiceList = reportBP.getECreditinvoiceList();
                    getECreditSuppinvoiceList = reportBP.getECreditSuppinvoiceList();
                    //here
                    getAutoEmailList = reportBP.getAutoEmailList();
                    getAutoEmailSuppList = reportBP.getAutoEmailSuppList();

                    System.out.println("getEinvoiceList--------------..####.." + getEinvoiceList);
                    System.out.println("getESuppinvoiceList----------..####.." + getESuppinvoiceList);
                    System.out.println("getECreditinvoiceList--------..####.." + getECreditinvoiceList);
                    System.out.println("getECreditSuppinvoiceList----..####.." + getECreditSuppinvoiceList);
                    System.out.println("getAutoEmailList----..####.." + getAutoEmailList);
                    System.out.println("getAutoEmailSuppList----..####.." + getAutoEmailSuppList);

                    System.out.println("runMinute-------- xxxxxxxxxxxxx-----------------------" + runMinute);
                    System.out.println("runHour-------- xxxxxxxxxxxxx-----------------------" + runHour);

                    if (getEinvoiceList >= 25) {
                        subject = getEinvoiceList + ":-E-Invoices were Stopped. Please check E-invoice table";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);
                        //                ReportTO reportTO = new ReportTO();
                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getEinvoiceList + "</b> E-Invoices were Stopped and not sent to the mail. <br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getEinvoiceList + " E-Invoices were Stopped");
                        schedulerTO.setMailSubjectCc("" + getEinvoiceList + " E-Invoices were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getEinvoiceList + " E-Invoices were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        //              
                    }
                    if (getESuppinvoiceList >= 25) {
                        subject = getESuppinvoiceList + ":-Supplementary E-Invoices were Stopped. Please check Supplement E-invoice table";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);

                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getESuppinvoiceList + "</b> Supplementary E-Invoices were Stopped and not sent to the mail. <br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getESuppinvoiceList + "Supplementary E-Invoices were Stopped");
                        schedulerTO.setMailSubjectCc("" + getESuppinvoiceList + "Supplementary E-Invoices were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getESuppinvoiceList + "Supplementary E-Invoices were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                    }
                    if (getECreditinvoiceList >= 25) {
                        subject = getECreditinvoiceList + ":-E-Invoices were Stopped. Please check Credit-note E-invoice table";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);
                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getECreditinvoiceList + "</b> Credit E-Invoices were Stopped and not sent to the mail.<br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getECreditinvoiceList + "Credit E-Invoices were Stopped");
                        schedulerTO.setMailSubjectCc("" + getECreditinvoiceList + "Credit E-Invoices were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getECreditinvoiceList + "Credit E-Invoices were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                    }
                    if (getECreditSuppinvoiceList >= 25) {
                        subject = getECreditSuppinvoiceList + ":-E-Invoices were Stopped. Please check Supp-Credit-note E-invoice table";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);
                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getECreditSuppinvoiceList + "</b> Credit E-Invoices were Stopped and not sent to the mail.<br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getECreditSuppinvoiceList + "Credit Supplementary E-Invoices were Stopped");
                        schedulerTO.setMailSubjectCc("" + getECreditSuppinvoiceList + "Credit Supplementary E-Invoices were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getECreditSuppinvoiceList + "Credit Supplementary E-Invoices were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                    }
                    if (getAutoEmailList >= 25) {
                        subject = getAutoEmailList + ":-Auto-Invoices were Stopped. Please check AutoInvoice";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);
                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getAutoEmailList + "</b> Auto Mail were Stopped and not sent to the mail.<br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getAutoEmailList + "Auto Mails were Stopped");
                        schedulerTO.setMailSubjectCc("" + getAutoEmailList + "Auto Mails were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getAutoEmailList + "Auto Mails were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                    }
                    if (getAutoEmailSuppList >= 25) {
                        subject = getAutoEmailSuppList + ":-E-Invoices were Stopped. Please check E-invoice table";
                        check = em.sendEinvoiceAlertMail(recTo1, recCc1, recBcc1, subject);
                        //                ReportTO reportTO = new ReportTO();
                        SchedulerTO schedulerTO = new SchedulerTO();
                        String emailFormat = "<!DOCTYPE html PUBLIC \\\"-//W3C//DTD XHTML 1.0 Transitional//EN\\\" \\\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\\\">\n"
                                + "<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\" xmlns:v=\\\"urn:schemas-microsoft-com:vml\\\" xmlns:o=\\\"urn:schemas-microsoft-com:office:office\\\">\n"
                                + "    <head>\n"
                                + "    <meta charset=\\\"utf-8\\\"><meta name=\\\"viewport\\\" content=\\\"height=device-height, initial-scale=0.5\\\">\n"
                                + " <title>DICT Alerts</title>\n"
                                + " <style >.wrapper {\n"
                                + "    width: 100%; }\n"
                                + " #outlook a {\n"
                                + "    padding: 0; }\n"
                                + " body\n"
                                + " {\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                                + "    margin:0 auto !important;\n"
                                + "    padding:5px;\n"
                                + "    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                                + "    font-size: 14px;\n"
                                + "    font-style: normal;\n"
                                + "    font-variant: normal;\n"
                                + "    font-weight: 400;\n"
                                + "    line-height: 20px\n"
                                + " }\n"
                                + " img {\n"
                                + "    outline: none;\n"
                                + "    text-decoration: none;\n"
                                + "    -ms-interpolation-mode: bicubic;\n"
                                + "    width: auto;\n"
                                + "    max-width: 100%;\n"
                                + "    clear: both;\n"
                                + "    display: block; }\n"
                                + " table.main {\n"
                                + "    border:0px solid #053587;\n"
                                + "    padding:0px 10px;\n"
                                + "    width: 700px !important;\n"
                                + "    max-width:700px;\n"
                                + " }\n"
                                + " td.fontsize12\n"
                                + " {\n"
                                + "    font-size:12px;\n"
                                + " }\n"
                                + " td a.trackbutton\n"
                                + " {\n"
                                + "    text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                                + "    line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                                + " }\n"
                                + " td.tollfree\n"
                                + " {font-size:14px;color:#053587; padding:5px 2px;}\n"
                                + " table.tableinline\n"
                                + " {\n"
                                + "    font-size:16px;\n"
                                + "    font-weight:bold;\n"
                                + "    color:#FFFFFF;\n"
                                + "    height:30px\n"
                                + " }\n"
                                + " table.text\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;height:30px\n"
                                + " }\n"
                                + " table.tablesub\n"
                                + " {\n"
                                + "    font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                                + " }\n"
                                + " td.tabletdheader\n"
                                + " {\n"
                                + "    padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                                + " }\n"
                                + " td.listhead table td ul.coming_soon\n"
                                + " {\n"
                                + "    line-height:30px;color: #FFFFFF; font-size:13px;\n"
                                + " }\n"
                                + " td.listhead table td ul\n"
                                + " {\n"
                                + "    line-height:25px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " table.fontsize10\n"
                                + " {\n"
                                + "    font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                                + " }\n"
                                + " table.amount_table\n"
                                + " {\n"
                                + "    background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                                + " }\n"
                                + " table.tablebottom\n"
                                + " {\n"
                                + "    width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                                + " }\n"
                                + " div.tablebottom_head\n"
                                + " {\n"
                                + "    font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                                + " }\n"
                                + " div.tablebottom_address\n"
                                + " {\n"
                                + "    font-size:12px;line-height:18px\n"
                                + " }\n"
                                + " td.listhead\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                                + " }\n"
                                + " td.service_head\n"
                                + " {\n"
                                + "    border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                                + " }\n"
                                + " .service_head_upper\n"
                                + " {\n"
                                + "    font-size:14px; text-align: center; margin: 0px;\n"
                                + " }\n"
                                + " td.service_head ul\n"
                                + " {\n"
                                + "    line-height:30px;color: #053587; font-size:13px;\n"
                                + " }\n"
                                + " h3\n"
                                + " {\n"
                                + "    font-size: 15px;\n"
                                + "    color: 053587;\n"
                                + "    line-height: 20px;      margin: 0px;\n"
                                + " }\n"
                                + " p\n"
                                + " {\n"
                                + "    font-size: 13px;\n"
                                + "    color: #000000; padding-left: 45px;\n"
                                + "    margin: 0px;\n"
                                + " }\n"
                                + " span\n"
                                + " {\n"
                                + "    font-size: 14px;padding-left: 0px;\n"
                                + "    color: #053587;\n"
                                + "\n"
                                + "       }\n"
                                + " .left li\n"
                                + "       {\n"
                                + "     float: left;\n"
                                + "     display: block;\n"
                                + "     padding: 5px;margin:0px 0px 0px 5px\n"
                                + " }\n"
                                + "\n"
                                + "    </style>\n"
                                + "    </head>\n"
                                + "    <body>\n"
                                + "<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"  width=\\\"90%\\\">\n"
                                + "<tr>\n"
                                + "<td align=\\\"right\\\" style=\\\"font-size:14px;color:#053587; padding:5px 2px\\\" colspan=\\\"2\\\">\n"
                                + "</td>\n"
                                + "</tr>\n"
                                + "</table>\n"
                                + " \n"
                                + "<br>\n"
                                + "\n"
                                + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                                + "<br>\n"
                                + "<span>\n"
                                + "\n"
                                + "There are Around <b>" + getAutoEmailSuppList + "</b> Supp-AutoMail were Stopped and not sent to the mail. <br> \n"
                                + "</span>\n"
                                + "<br><br>\n"
                                + "<span>Thanking You,</span>\n"
                                + "<br>\n"
                                + "\n"
                                + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                                + "                                    <tr>\n"
                                + "                                        <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                                + "                                            <div class=\"tablebottom_head\">Best Regards,<br>DICT</div>\n"
                                + "                                          \n"
                                + "\n"
                                + "                                            </div>\n"
                                + "                                        </td>\n"
                                + "                                        <td valign=\"bottom\" align=\"right\">\n"
                                + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                                + "                                                <img src=\"http://dict.throttletms.com/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                                + "                                            </a>\n"
                                + "                                        </td>\n"
                                + "                                    </tr>\n"
                                + "\n"
                                + "\n"
                                + "                                    </table> \n"
                                + "</table>\n"
                                + "</body>\n"
                                + "</html>";

                        System.out.println("content" + emailFormat);
                        schedulerTO.setMailTypeId("6");
                        schedulerTO.setMailSubjectTo("" + getEinvoiceList + " Supp-AutoMail were Stopped");
                        schedulerTO.setMailSubjectCc("" + getEinvoiceList + " Supp-AutoMail were Stopped");
                        schedulerTO.setMailSubjectBcc("" + getEinvoiceList + " Supp-AutoMail were Stopped");
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setEmailFormat(emailFormat);
                        schedulerTO.setMailStatus("0");
                        schedulerTO.setMailContentBcc(emailFormat);

                        schedulerTO.setMailIdTo("sachink@jmbaxi.com,rohan@ampsolutions.co.in,arun@ampsolutions.co.in");
                        schedulerTO.setMailIdCc("arun@ampsolutions.co.in");
                        schedulerTO.setMailIdBcc("rohan@ampsolutions.co.in");
                        Integer mails = schedulerBP.insertMailDetails(schedulerTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                    }
                    //              

//                            
//                int check = 0;
//                            int getStatus=financeBP.getStatus();
//                            if (getStatus == 1) {
//                      new SendEmailReport(recTo1, recCc1, recBcc1, subject , mailTemplate, pdfname,invd).start();
//
//                            if (check > 0) {
//                                int invUpdate = reportBP.updateInvoiceCustomerSendMail(reportTO);
////                                System.out.println("invUpdate : " + invUpdate);
//                            }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("invoice mail error");
        }
//
    }

    public HashMap<String, String> loadProps() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        HashMap<String, String> maps = new HashMap<String, String>();
        try {
            ArrayList list = new ArrayList();
            list = loginBP.LoadProps();
            // System.out.println("IM HERE-------");
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
//                    System.out.println("object.getKeyName() = " + object.getKeyName());
//                    System.out.println("object.getValue() = " + object.getValue());
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                }
            }

        } catch (Exception e) {
//            String error = e.getMessage();
        }
        map.clear();
        System.out.println("method..Ends");
        return ThrottleConstants.PROPS_VALUES;
    }

}
