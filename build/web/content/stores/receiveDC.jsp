
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Parveen Auto Care</title>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.purchase.business.PurchaseTO" %>
        <script type="text/javascript" language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions1.js"></script>

        
    </head>
    
    <script>
        function newWindow(){
            window.open('/throttle/content/stores/PriceHistory.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }
    
        
        function tyreItemsValidate(){
            var tyreNos = document.getElementsByName("tyreNo");
            var tyreItemIds = document.getElementsByName("tyreItemId");
            var posIds = document.getElementsByName("posIds");
            for(var i=0;i<tyreItemIds.length; i++){
                if(tyreItemIds[i].value != ''){
                if(tyreNos[i].value==''){
                    alert("Please Enter Tyre Number");
                    tyreNos[i].focus();
                    return 'fail';
                }
                if(document.receiveInvoice.inventoryStatus.value=='N' ){
                    if(posIds[i].value=='0'){
                        alert("Please Select Tyre Position");
                        posIds[i].focus();
                        return 'fail';
                    }
                }
                }                
            }
            return 'pass';
        }    
        
       
        
        function submitItems(){
            var index = document.getElementsByName("selectedIndex");
            var dat = new Date();
            var month = parseInt( dat.getMonth() ) + 1;
            var today = dat.getDate() + '-' + month + '-' + dat.getFullYear();
            
            if(document.receiveInvoice.inVoiceNumber.value=="" && document.receiveInvoice.dcNumber.value=="" ){
                alert("Please Enter Invoice or Dc Number");
                document.receiveInvoice.inVoiceNumber.focus();
                return;
            }
            if(document.receiveInvoice.inVoiceDate.value=="" ){
                alert("Please Enter Invoice Date");
                return;
            }            
              
            if(document.receiveInvoice.transactionType.value=="0" ){
                alert("Please Select Transaction Type");
                document.receiveInvoice.transactionType.focus();
                return;
            }
            if(document.receiveInvoice.transactionType.value == 'DC' && document.receiveInvoice.dcNumber.value == '' ){
                document.receiveInvoice.inVoiceNumber.value = '';
                alert("Please enter DC Number");
                document.receiveInvoice.dcNumber.select();
                document.receiveInvoice.dcNumber.focus();
                return;
            }    
            if(document.receiveInvoice.transactionType.value == 'DC'){
                document.receiveInvoice.inVoiceNumber.value = '';
            }    
            for(var i =0; i<index.length; i++ ){
                if( calculateMRP(i)== 'fail' ){
                    return;
                }    
                if( calculateAmount(i)=='fail' ){
                    return;
                }    
            }  
            if(tyreItemsValidate()=='fail'){
                return;
            }
             if(textValidation(document.receiveInvoice.remarks,'Remarks')){
               return;
               }               
            if(confirm("Please check whether invoice amount is tallied")){
                document.getElementById("buttonStyle").style.visibility="hidden" ;
                document.receiveInvoice.action='/throttle/handleInsertItems.do';
                document.receiveInvoice.submit();
            }
        }
        
        
        function totalBalance(val)
        {
            var index = document.getElementsByName("selectedIndex");
            var itemAmounts = document.getElementsByName("itemAmounts");            
            var tot = 0;            
            var roundOff = 0;
            if(calculateMRP(val)=='fail' ){
                return 'fail';
            }    
            if(calculateAmount(val)=='fail' ){
                return 'fail'; 
            }    

            for(var i =0; i<index.length; i++ ){
                if(itemAmounts[i].value != ''){
                tot = parseFloat( tot ) + parseFloat( itemAmounts[i].value);
                }
            }

            if(isFloat(document.receiveInvoice.freight.value) ) {
                alert("Please Enter Valid Freight Charges");
                document.receiveInvoice.freight.select();
                document.receiveInvoice.freight.focus();
                return 'fail';
            }    
            
            tot = parseFloat(tot) + parseFloat(document.receiveInvoice.freight.value);
            roundOff = parseFloat(tot).toFixed(0) - parseFloat(tot).toFixed(2);
            roundOff = parseFloat(roundOff).toFixed(2);

            if( parseFloat(roundOff) < 0 ){
                roundOff = -(roundOff);
            }    
            tot = parseFloat(tot).toFixed(0);
            document.getElementById("total").innerHTML= '<font>'+tot+'</font>'; 
            document.getElementById("roundOff").innerHTML= '<font>'+roundOff+'</font>'; 
            document.receiveInvoice.inVoiceAmount.value=tot ;                                          
        }    
        
        
        
        
        function calculateAmount(val){               
            var acceptedQty=document.getElementsByName("acceptedQtys");  
            var receivedQty=document.getElementsByName("receivedQtys");  
            var orderedQty=document.getElementsByName("orderedQty");   
            var mrp=document.getElementsByName("mrps");                                       
            var index = document.getElementsByName("selectedIndex");            
            var itemAmounts = document.getElementsByName("itemAmounts");
            
            if( calculateMRP(val) == 'fail' ){
                return 'fail';
            }

            if( isFloat(mrp[val].value) ){
                alert("Please Enter Valid MRP");
                mrp[val].focus();
                mrp[val].select();
                return 'fail';
            }

            if( isFloat(receivedQty[val].value) ){
                alert("Please Enter Valid Received Quantity");
                receivedQty[val].focus();
                receivedQty[val].select();
                return 'fail';
            }

            if( isFloat(acceptedQty[val].value) ){
                alert("Please Enter Valid Accepted Quantity");
                acceptedQty[val].focus();
                acceptedQty[val].select();
                return 'fail';
            }
            /*
            if( parseFloat( orderedQty[val].value ) <  parseFloat(acceptedQty[val].value ) ){            
                alert("Accepted Quantity Should Not Exceed Ordered Quantity");
                acceptedQty[val].focus();
                acceptedQty[val].select();
                return 'fail';
            }
            */
            itemAmounts[val].value = parseFloat( acceptedQty[val].value ) * parseFloat( mrp[val].value );
            itemAmounts[val].value = parseFloat( itemAmounts[val].value ).toFixed(2);            
            return 'pass';
    }
    
    function calculateMRP(val)
    {            
            var mrp=document.getElementsByName("mrps");                
            var unitPrice = document.getElementsByName("unitPrice");
            var tax = document.getElementsByName("tax");
            var discount = document.getElementsByName("discount");
            var priceAfterDiscount = 0;
            
            if( isFloat(discount[val].value) ){
                alert("Please Enter Valid Discount");
                discount[val].focus();
                discount[val].select();
                return 'fail';
            }    
            if( isFloat(tax[val].value) ){
                alert("Please Enter Valid Tax");
                tax[val].focus();
                tax[val].select();
                return 'fail';
            }    
            if( isFloat(unitPrice[val].value) ){
                alert("Please Enter Valid Unit Price");
                unitPrice[val].focus();
                unitPrice[val].select();
                return 'fail';
            }
             
             priceAfterDiscount = parseFloat( unitPrice[val].value ) - ( parseFloat( unitPrice[val].value ) * parseFloat( discount[val].value )/100 );
             mrp[val].value = parseFloat( priceAfterDiscount ) + ( parseFloat( priceAfterDiscount ) * parseFloat( tax[val].value )/100 );
             mrp[val].value = parseFloat( mrp[val].value ).toFixed(2);
             return 'pass';
    }    

function setValues(){
     document.receiveInvoice.inVoiceNumber.focus();
    if('<%=request.getAttribute("inVoiceNumber")%>' != 'null'){
        document.receiveInvoice.inVoiceNumber.value = '<%=request.getAttribute("inVoiceNumber")%>';
    }if('<%=request.getAttribute("inVoiceDate")%>' != 'null'){
        document.receiveInvoice.inVoiceDate.value = '<%=request.getAttribute("inVoiceDate")%>';
    }if('<%=request.getAttribute("inVoiceAmount")%>' != 'null'){
        document.receiveInvoice.inVoiceAmount.value ='<%=  request.getAttribute("inVoiceAmount")%>';
    }if('<%=request.getAttribute("poIds")%>' != 'null'){
        document.receiveInvoice.poIds.value ='<%= request.getAttribute("poIds")%>';
    }if('<%=request.getAttribute("dcNumber")%>' != 'null'){
        document.receiveInvoice.dcNumber.value ='<%=  request.getAttribute("dcNumber")%>';
    }if('<%=request.getAttribute("vendorId")%>' != 'null'){
        document.receiveInvoice.vendorId.value = '<%= request.getAttribute("vendorId")%>';
    }
    
    ajaxPoList(document.receiveInvoice.vendorId.value);         
}

function submitPage(){
if(document.receiveInvoice.poIds.value!='0'){
    //document.receiveInvoice.inVoiceDate.value = dateFormat(document.receiveInvoice.inVoiceDate)   
    document.receiveInvoice.action='/throttle/purchaseItems.do';
    document.receiveInvoice.submit();            
}    
}


function ajaxPoList(value){        
    var vendorId=value;
    if(vendorId!=0){

/*
    if(textValidation(document.receiveInvoice.inVoiceAmount,'Invoice Amount')){  
            document.receiveInvoice.vendorId.value = '0';
            return;
        }
*/        

/*
        if(document.receiveInvoice.inventoryStatus.value==0){
            alert("Inventory Status Should not be Empty");
            document.receiveInvoice.vendorId.value = '0';
            return;
        }  
*/        
        var url='/throttle/purchaseOrderList.do?vendorId='+vendorId;
        if (window.ActiveXObject){
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest){
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() { processAjax();};
        httpReq.send(null);
    }
}    

function processAjax()
{
    if (httpReq.readyState == 4)
        {
            if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();                   
                    var splt = temp.split('-');       
                    setOptions1(temp,document.receiveInvoice.poIds,'0','0');  
                    document.receiveInvoice.poIds.value ='<%= request.getAttribute("getPO")%>';
                }
                else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
            }
            
            function setSelectbox(i)
            {   
                var selected=document.getElementsByName("selectedIndex") ;
                selected[i];
            }  
    </script>  
    
<script>

        var httpRequest1;     
        function getOthers(rowIndex)
        {            
            var temp=0;  
            var index = rowIndex;
            var mfrCode=document.getElementsByName("mfrCode");                        
            var itemCode=document.getElementsByName("itemCode"); 
            var itemName=document.getElementsByName("itemName");   
            
            
            /*            if(mfrCode[index]!=""){
            temp=eval("document.mrs.mfrCode"+sno);
            }else if(itemCode[index]!=""){
            temp=eval("document.mrs.itemCode"+sno);
            }else if(itemName[index]!=""){
            temp=eval("document.mrs.itemName"+sno);
            } */       
            var url='/throttle/getQuandity.do?mfrCode='+mfrCode[index].value+'&itemCode='+itemCode[index].value+'&itemName='+itemName[index].value;                      
            if (window.ActiveXObject)
                {
                    httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                    {
                        httpRequest1 = new XMLHttpRequest();
                    }
                    httpRequest1.open("POST", url, true);
                    httpRequest1.onreadystatechange = function() {
                        go(index,temp);                            
                    } ;
                    httpRequest1.send(null);
        }
                
                
                
                function go(index,temp) {                         
                    if (httpRequest1.readyState == 4) {
                        if (httpRequest1.status == 200) {
                            var cntr = 0;
                            var response = httpRequest1.responseText;  
                            var details=response.split('~'); 
                            var uom=document.getElementsByName("uom"); 
                            
                            var itemIds=document.getElementsByName("itemIds");   
                            
                            var itemId=document.getElementsByName("tyreItemId"); 
                            var mfrCode=document.getElementsByName("mfrCode"); 
                            var itemCode=document.getElementsByName("itemCode");
                            var itemName=document.getElementsByName("itemName");
                            var tyreNo=document.getElementsByName("tyreNo");
                            
                            for(var i=0;i<itemIds.length;i++){                                
                                if( details[0] == itemIds[i].value ){
                                    cntr++;
                                }                                    
                            }
                            if(parseInt(cntr) ==0 )
                                {
                                    alert('Please Enter Valid Item Code');
                                    return;
                                }
                            
                            if(response!=""){
                                var details=response.split('~');                                                           
                                itemName[index].value=details[5];
                                itemCode[index].value=details[4];
                                mfrCode[index].value=details[3];
                                uom[index].value=details[2];
                                
                                itemId[index].value=details[0];   
                                tyreNo[index].focus();
                            }else
                            {
                                alert("Invalid value");
                                //mfrCode.value="";
                                mfrCode[index].focus();
                                mfrCode[index].select();
                            }
                        }
                    }
                }
                
                
                var rowCount=1;
                var sno=0;
                var httpRequest;
                var httpReq;
                var rowIndex = 0;
                function addRow()
                {
                    sno++;                        
                    //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
                    var tab = document.getElementById("items");
                    var newrow = tab.insertRow(rowCount);
                    
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='tyreItemId' value=''  > "+sno+"</td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell0;
                    
                    var cell = newrow.insertCell(1);
                    var cell0 = "<td class='text1' height='25' ><input name='mfrCode' class='form-control'   onchange='getOthers("+rowIndex+")'  type='text'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell0;
                    
                    cell = newrow.insertCell(2);
                    var cell1 = "<td class='text1' height='25'><input name='itemCode' class='form-control'  onchange='getOthers("+rowIndex+")'  type='text'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell1;
                    
                    cell = newrow.insertCell(3);
                    var cell2 = "<td class='text1' height='25'><input name='itemName' id='itemNames"+rowIndex+"' class='form-control'  onFocusout='getOthers("+rowIndex+")'  type='text'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell2;
                    
                    cell = newrow.insertCell(4);
                    var cell3 = " <td class='text1' height='25'><input name='uom' size='5' readonly class='form-control'  type='text'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell3;
                    
                    cell = newrow.insertCell(5);
                    var cell4 = " <td class='text1' height='25'><input name='tyreNo'  class='form-control'  type='text'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell4;
                    
                    cell = newrow.insertCell(6);
                    if(document.receiveInvoice.inventoryStatus.value=='N'){
                    var cell4 = "<td class='text1' height='30'> <select class='form-control' name='posIds'  style='width:150px;'><option  selected value='0'>---Select---</option>"+
                    "<c:if test = "${positionList != null}" ><c:forEach items="${positionList}" var="sec"><option  value='<c:out value="${sec.posId}" />'><c:out value="${sec.posName}" /></c:forEach ></c:if>"+ 
                    "</select></td>";
                    }else{
                    var cell4 = " <td class='text1' height='25'><input name='posIds' value='0'  class='form-control'  type='hidden'></td>";
                    }
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell4;
                    
                    cell = newrow.insertCell(7);
                    var cell4 = " <td class='text1' height='25'><input name='checkBoxIndex' onClick='clearFieldValues("+rowIndex+")' type='checkbox'></td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell4;
                    
                    getItemNames(rowIndex);    
                    rowIndex++;
                    rowCount++;
                    
                }
    
    
function getItemNames(val){
    var itemTextBox = 'itemNames'+ val;    
    var oTextbox = new AutoSuggestControl(document.getElementById(itemTextBox),new ListSuggestions(itemTextBox,"/throttle/handleItemSuggestions.do?"));
    //getVehicleDetails(document.getElementById("regno"));
}     



function clearFieldValues(val)
{
var tyreItemId = document.getElementsByName("tyreItemId");
var checkBoxIndex = document.getElementsByName("checkBoxIndex");
var mfrCode = document.getElementsByName("mfrCode");
var itemCode = document.getElementsByName("itemCode");
var itemName = document.getElementsByName("itemName");
var uom = document.getElementsByName("uom");
var tyreNo= document.getElementsByName("tyreNo"); 
var posIds = document.getElementsByName("posIds"); 

if(checkBoxIndex[val].checked==true){
    getItemNames(val);
    tyreItemId[val].value='';
    mfrCode[val].value='';
    itemCode[val].value='';
    itemName[val].value='';
    uom[val].value='';
    tyreNo[val].value='';
    posIds[val].value='0';
}    

}    

function calculateAcceptedQty(val){    
    if(calculateAmount(val)=='fail' ){
        return 'fail';
    }
    if(totalBalance(val)=='fail' ){
        return 'fail';
    }
}




    
</script>  

    
    <% int indx = 0;%>  
    
    <form name="receiveInvoice" method="post" >
        <body onload="setValues();" >
            <%@ include file="/content/common/path.jsp" %>
          
            <%@ include file="/content/common/message.jsp" %>
            
         <table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
            <table align="center" width="557" border="0" cellspacing="0" cellpadding="0" class="border">
                
                <tr>
                    <td colspan="4" class="contenthead" height="30"><div class="contenthead">Receive Stock</div></td>
                </tr>
                
                <tr>
                    <td class="text2" height="30">&nbsp;&nbsp;Invoice Number :</td>
                    <td class="text2" height="30"><input name="inVoiceNumber" maxlength='30'  class="form-control"  type="text" value="" size="20"></td>
                    <td class="text2" height="30"><font color="red">*</font>Invoice date :</td>
                    <td class="text2" height="30"><input name="inVoiceDate" readonly  class="form-control"  type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receiveInvoice.inVoiceDate,'dd-mm-yyyy',this)"/></td>
                </tr>


                <tr>
                    <td width="120"  height="30" class="text1"><font color="red">*</font>Vendor</td>
                    
                    <td width="149" height="30" class="text1">
                        <select class="text1" name="vendorId" onchange="ajaxPoList(this.value);"   style="width:125px;" >            
                            <option value="0">NA</option>
                            <c:if test = "${vendorLists != null}" >
                                <c:forEach items="${vendorLists}" var="vendor"> 
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>                                
                                </c:forEach>
                            </c:if>
                    </select> </td>
                    <td width="99" height="30" class="text1"><font color="red">*</font>PO / LPO No :</td>
                    
                    <td width="189" height="30" class="text1"><select class="text1" name="poIds"  style="width:125px;"  onChange="submitPage();">
                            
                            <option value="0">-Select-</option>
                            <c:if test = "${getPo != null}" >
                                <c:forEach items="${getPo}" var="po"> 
                                    <option value="<c:out value="${po.poId}"/>"><c:out value="${po.poId}"/></option>
                                    
                                </c:forEach>
                            </c:if>
                    </select> </td>
                    
                </tr>

                <tr>
                    <td width="119" height="30" class="text2"><font color="red">*</font>Inventory Status:</td>
                    <td class="text2" height="30"><select class="form-control" name="inventoryStatus" style="width:125px;" >
                            <c:if test = "${ItemList != null}" >         
                            <% int ind=0; %>                            
                            <c:forEach items="${ItemList}" var="item">         
                            <% if(ind==0){ %>
                            <c:if test = "${item.inventoryStatus == 'Y'}" >         
                            <option value="Y">Normal Purchase</option>
                            </c:if>
                            <c:if test = "${item.inventoryStatus == 'N'}" >         
                            <option value="N">Direct Purchase</option>
                            </c:if>
                            <% ind++; } %>
                            </c:forEach> 
                            </c:if>
                    </select></td>                                        
                    <input name="inVoiceAmount" readonly  maxlength='15' class="form-control" type="hidden" value="" size="20">
                    <td class="text2" height="30">&nbsp;&nbsp;DC Number :</td>
                    <td width="189" height="30" class="text2"> <input name="dcNumber"  maxlength='30'  class="form-control"  type="text" value="" size="20"> </td>
                </tr> 
                
                <tr>
                    <td width="119" height="30" class="text2"><font color="red">*</font>Transaction Type:</td>
                    <td class="text2" height="30">
                    <select class="form-control" name="transactionType" style="width:125px;" >
                        <option value="0" > --Select--</option>
                        <option value="INVOICE" > INVOICE </option>
                        <option value="DC" > DELIVERY CHALLAN </option>
                    </select>
                    </td>                                                            
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>

                </tr>                

            </table>
            
            </td>    
	<td>

		<table width="200" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
		<tr> 
		<td colspan="2" height="30" class="blue" align="center" style="border:1px;border-color:#FFFFFF;border-bottom-style:dashed;">Total</td>
		</tr>

		<tr> 
		<td width="121" height="30"  class="bluetext">SAR.</td>
                 <td width="86" height="30" class="bluetext"><div id="total" ></div>  </td>
                </tr>
		<tr> 
		<td width="121" height="30"  class="bluetext">Round Off</td>
                 <td width="86" height="30" class="bluetext"><div id="roundOff" ></div>  </td>
                </tr>
                
                </table> 
	</td>
 </tr>
  
</table>
            
            
            
            
            
            <br>
            <br>            
                <c:if test = "${ItemList != null}" >         
            <table width="1000" border="0" align="center" class="border" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td width="90" height="30" class="contenthead">S No</td>
                        <td width="100" height="30" class="contenthead">MFR Code</td>
                        <td width="100" height="30" class="contenthead">PAPL Code</td>
                        <td width="300" height="30" class="contenthead">Item Name</td>
                        <td width="100" height="30" class="contenthead">Uom</td>
                        <td width="80" height="30" class="contenthead">Discount(%)</td>
                        <td width="80" height="30" class="contenthead">Tax(%)</td>
                        <td width="80" height="30" class="contenthead">Unit Price</td>
                        <td width="80" height="30" class="contenthead">MRP</td>
                        <td width="150" height="30" class="contenthead">Previous Price(SAR) </td>
                        <td width="80" height="30" class="contenthead">Ordered Qty</td>
                        <td width="80" height="30" class="contenthead">Previously Accepted</td>
                        <td width="80" height="30" class="contenthead">Received Qty</td>
                        <td width="80" height="30" class="contenthead">Accepted Qty</td>
                        <td width="100" height="30" class="contenthead"><div class="contenthead">Amount (SAR)</div> </td>
                        
                        
                    </tr>
                    <% int index = 0;%>
        <c:forEach items="${ItemList}" var="item">               
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
                   
                        <tr>
                            <td class="<%=classText %>" align="left" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.paplCode}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="itemIds" value="<c:out value="${item.itemId}"/>" ><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.uomName}"/></td>                            
                            <td class="<%=classText %>" align="left" height="30"><input type="text" name="discount" value="" onchange="setSelectbox('<%= index %>');" size="7" class="form-control"></td>
                            <td class="<%=classText %>" align="left" height="30">
                            <select name="tax" class="form-control" onchange="setSelectbox('<%= index %>');" >
                            <c:if test="${vatList!=null}" >
                                <c:forEach items="${vatList}" var="vat" >
                                    <option value="<c:out value="${vat.vat}"/>" > <c:out value="${vat.vat}"/> </option>
                                </c:forEach>
                            </c:if>
                            </select>                            
                            </td>
                            <td class="<%=classText %>" align="left" height="30"><input type="text" name="unitPrice" value="" onchange="setSelectbox('<%= index %>');" onFocusOut="calculateMRP('<%= index %>');" size="7" class="form-control"></td>
                            <td class="<%=classText %>" align="left" height="30"><input type="text" readonly name="mrps" value="" onchange="setSelectbox('<%= index %>');" size="7" class="form-control"></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.prePrice}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.approvedQty}"/><input type="hidden" name="orderedQty" value=<c:out value="${item.approvedQty}"/> > </td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${item.acceptedQty}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><input type="text" name="receivedQtys" value="" onchange="setSelectbox('<%= index %>');" size="7" class="form-control"></td>
                            <td class="<%=classText %>" align="left" height="30"><input type="text" name="acceptedQtys" value="" onchange="setSelectbox('<%= index %>');" onFocusOut="calculateAcceptedQty(<%= index %>);" size="7" class="form-control"></td>
                            <td class="<%=classText %>" align="left" height="30"><input type="text"  name="itemAmounts" size="7" class="form-control" readonly value=""></td>
                            
                            <input type="hidden" name="selectedIndex" value='<%= index %>'>
                            <input type="hidden" name="validate" value='N' >
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </table>
                <br>
                 <center><div class="text2" align="center" > Remarks &nbsp;&nbsp;:&nbsp;&nbsp; <textarea class="form-control" name="remarks" rows="2" cols="15" ></textarea> </center>
                <br>
                <br>
                <br>
                <table width="600" border="0" align="center" class="border" cellpadding="0" cellspacing="0" id="items">                            
                    <td width="62" height="30" class="contentsub"><div class="contentsub">Sno</div></td>
                    <td width="62" height="30" class="contentsub"><div class="contentsub">MFRItemCode</div></td>
                    <td width="62" height="30" class="contentsub"><div class="contentsub">PAPLItemCode</div></td>
                    <td width="90" height="30" class="contentsub"><div class="contentsub">Item Name</div></td>                                        
                    <td width="90" height="30" class="contentsub"><div class="contentsub">Uom</div></td>                                        
                    <td width="90" height="30" class="contentsub"><div class="contentsub">Tyre No</div></td>                                        
                    <td width="90" height="30" class="contentsub"><div class="contentsub">Position</div></td>                                        
                    <td width="90" height="30" class="contentsub"><div class="contentsub">Clear</div></td>                                        
                </table>                    
                <br>
                <center>
                    Freight Charges &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" class="form-control" size="5" name="freight" onFocusOut="totalBalance('0');" value="0" >
                </center>
                <br>

                <div id="buttonStyle" style="visibility:visible;" align="center" >
                    <input class="button" type="button" value="Save" onclick="submitItems();">&nbsp;
                    <input value="Add Row" class="button" type="button" onClick="addRow();">                    
                </div>
            </c:if>                                                                 
        </body>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    
</html>
