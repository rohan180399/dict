package ets.domain.customer.web;

import ets.arch.web.BaseController;
import ets.domain.customer.business.CustomerBP;
import ets.domain.customer.business.CustomerTO;
import ets.domain.vendor.business.VendorBP;
import ets.domain.vendor.web.VendorCommand;
import ets.domain.users.business.LoginBP;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.util.Date;

import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import java.io.*;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author vidya
 */
public class CustomerController extends BaseController {

    CustomerCommand customerCommand;
 
    VendorCommand vendorCommand;
    CustomerBP customerBP;
    VendorBP vendorBP;
    LoginBP loginBP;

    
    public VendorCommand getVendorCommand() {
        return vendorCommand;
    }

    public void setVendorCommand(VendorCommand vendorCommand) {
        this.vendorCommand = vendorCommand;
    }

    public VendorBP getVendorBP() {
        return vendorBP;
    }

    public void setVendorBP(VendorBP vendorBP) {
        this.vendorBP = vendorBP;
    }
    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public CustomerCommand getCustomerCommand() {
        return customerCommand;
    }

    public void setCustomerCommand(CustomerCommand customerCommand) {
        this.customerCommand = customerCommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        System.out.println("userId" + userId);
//        System.out.println("loginRecordId" + loginRecordId);
        String getUserFunctionsActive = "";
        String uri = request.getRequestURI();
//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
            String temp[] = getUserFunctionsActive.split("~");
            System.out.println("function Id" + temp[0]);
            System.out.println("function Name  " + temp[0]);
            int status1 = 0;
            int functionId = Integer.parseInt(temp[0]);
            String functionName = temp[1];
//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
        }
        binder.closeNoCatch();
          initialize(request);

    }

public ModelAndView handleViewAddScreen(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> Add Customer ";
        String pageTitle = "Add Customer";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        session.setAttribute("uniquePageId", String.valueOf(java.lang.System.currentTimeMillis()));
        String sessionId=(String)session.getAttribute("uniquePageId");
        System.out.println("sessionId:"+sessionId);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            int loginRecordId = (Integer) session.getAttribute("loginRecordId");
                ArrayList billingTypeList = new ArrayList();
                billingTypeList = customerBP.getBillingTypeList();
                request.setAttribute("billingTypeList", billingTypeList);

                ArrayList accountManagerList = new ArrayList();
                accountManagerList = customerBP.getAccountManagerList();
                request.setAttribute("accountManagerList", accountManagerList);

                ArrayList customerGroup = new ArrayList();
                customerGroup = customerBP.getCustomerGroupList();
                request.setAttribute("customerGroup", customerGroup);
                
                ArrayList stateList = new ArrayList();
                stateList=customerBP.getStateList();
                request.setAttribute("stateList",stateList);
                
                ArrayList organizationList = new ArrayList();
                organizationList=customerBP.getOrganizationList();
                request.setAttribute("organizationList",organizationList);
                
                path = "content/Customer/addCustomer.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleAddCustomer(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        int insertContractRouteMater = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer)session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageUniqueId=request.getParameter("pageId");
        System.out.println("pageId:"+pageUniqueId);
        String sessionId=(String) session.getAttribute("uniquePageId");
       System.out.println("sessionId:"+sessionId);

        try {
            if (customerCommand.getSecondaryBillingTypeId() != null && customerCommand.getSecondaryBillingTypeId() != "") {
                customerTO.setSecondaryBillingTypeId(customerCommand.getSecondaryBillingTypeId());
            }
            if (customerCommand.getCustName() != null && customerCommand.getCustName() != "") {
                customerTO.setCustName(customerCommand.getCustName());
            }
            if (customerCommand.getBillingTypeId() != null && customerCommand.getBillingTypeId() != "") {
                customerTO.setBillingTypeId(customerCommand.getBillingTypeId());
            }
            if (customerCommand.getPaymentType() != null && customerCommand.getPaymentType() != "") {
                customerTO.setPaymentType(customerCommand.getPaymentType());
            }
            if (customerCommand.getCustContactPerson() != null && customerCommand.getCustContactPerson() != "") {
                customerTO.setCustContactPerson(customerCommand.getCustContactPerson());
            }
            if (customerCommand.getCustomerTypeId() != null && customerCommand.getCustomerTypeId() != "") {
                customerTO.setCustomerTypeId(customerCommand.getCustomerTypeId());
            }
            if (customerCommand.getCustAddress() != null && customerCommand.getCustAddress() != "") {
                customerTO.setCustAddress(customerCommand.getCustAddress());
                System.out.println("address==================>>>"+customerCommand.getCustAddress());
            }
            if (customerCommand.getCustAddresstwo() != null && customerCommand.getCustAddresstwo() != "") {
                customerTO.setCustAddresstwo(customerCommand.getCustAddresstwo());
            }
            if (customerCommand.getCustAddressthree() != null && customerCommand.getCustAddressthree() != "") {
                customerTO.setCustAddressthree(customerCommand.getCustAddressthree());
            }
            if (customerCommand.getCustCity() != null && customerCommand.getCustCity() != "") {
                customerTO.setCustCity(customerCommand.getCustCity());
            }
            if (customerCommand.getCustState() != null && customerCommand.getCustState() != "") {
                customerTO.setCustState(customerCommand.getCustState());
            }
            if (customerCommand.getCustPhone() != null && customerCommand.getCustPhone() != "") {
                customerTO.setCustPhone(customerCommand.getCustPhone());
            }
            if (customerCommand.getCustMobile() != null && customerCommand.getCustMobile() != "") {
                customerTO.setCustMobile(customerCommand.getCustMobile());
            }
            if (customerCommand.getCustEmail() != null && customerCommand.getCustEmail() != "") {
                customerTO.setCustEmail(customerCommand.getCustEmail());
            }
            if (customerCommand.getAccountManagerId() != null && customerCommand.getAccountManagerId() != "") {
                customerTO.setAccountManagerId(customerCommand.getAccountManagerId());
            }
            if (customerCommand.getCustomerGroupId() != null && customerCommand.getCustomerGroupId() != "") {
                customerTO.setCustomerGroupId(customerCommand.getCustomerGroupId());
            }
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                customerTO.setCustomerId(customerCommand.getCustomerId());
            }
            if (customerCommand.getCustomerName() != null && customerCommand.getCustomerName() != "") {
                customerTO.setCustomerName(customerCommand.getCustomerName());
            }
            if (customerCommand.getCustStatus() != null && customerCommand.getCustStatus() != "") {
                customerTO.setCustStatus(customerCommand.getCustStatus());
            }
            if (customerCommand.getErpId() != null && customerCommand.getErpId() != "") {
                customerTO.setErpId(customerCommand.getErpId());
            }
            if (request.getParameter("panNo") != null && request.getParameter("panNo") != "") {
                customerTO.setPanNo(request.getParameter("panNo"));
            }
            if (request.getParameter("companyType") != null && request.getParameter("companyType") != "") {
                customerTO.setCompanyType(request.getParameter("companyType"));
            }
            if (customerCommand.getStateId() != null && customerCommand.getStateId() != "") {
                customerTO.setStateId(customerCommand.getStateId());
                System.out.println("getStateId"+customerCommand.getStateId());
            }
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
                System.out.println("getGstNo"+customerCommand.getGstNo());
            }
            if (customerCommand.getOrganizationId() != null && customerCommand.getOrganizationId() != "") {
                customerTO.setOrganizationId(customerCommand.getOrganizationId());
                System.out.println("getOrganizationId"+customerCommand.getOrganizationId());
            }
            System.out.println("pinCode"+request.getParameter("pinCode"));
            customerTO.setPinCode(request.getParameter("pinCode"));
            

            String custTypeId = request.getParameter("custTypeId");
            String customerGroup = request.getParameter("customerGroup");
            String creditLimit = request.getParameter("creditLimit");
            String creditDays = request.getParameter("creditDays");
            String advanceAmount = request.getParameter("advanceAmount");
            String billingNameAddress = request.getParameter("billingNameAddress");
            String fixedCreditLimit = request.getParameter("fixedCreditLimit");
            String usedLimit = request.getParameter("usedLimit");
            String gtaType = request.getParameter("gtaType");
            customerTO.setCreditLimit(creditLimit);
            customerTO.setCreditDays(creditDays);
            customerTO.setGroupId(customerGroup);
            customerTO.setBillingNameAddress(billingNameAddress);
            customerTO.setCustTypeId(custTypeId);
            customerTO.setAdvanceAmount(advanceAmount);
            customerTO.setFixedCreditLimit(fixedCreditLimit);
            customerTO.setUsedLimit(usedLimit);
            customerTO.setGtaType(gtaType);
            String custCode = "";
            int checkCustomerName = 0;
            customerTO.setCustomerName(customerCommand.getCustName());
            checkCustomerName = customerBP.checkCustomerName(customerTO);

               custCode = customerBP.getCustomerCode(customerTO, insertContractRouteMater);
                request.setAttribute("custCode", custCode);
                path = "content/Customer/manageCustomer.jsp";
                if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                        update = customerBP.editUpdateCustomer(customerTO, userId);
                        if (update > 0) {
                            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Update Successfully");
                        }else{
                             request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Customer Failed to Update");
                        }
                } else {
                    if(pageUniqueId.equalsIgnoreCase(sessionId)){
                    if(checkCustomerName == 0) {
                        status = customerBP.processInsertCustomer(customerTO, userId);
                        if (status > 0) {
                            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Customer Added Successfully");
                        }else{
                             request.setAttribute(ParveenErrorConstants.ERROR_KEY, "New Customer Failed to Add");
                        }
                    }else{
                     request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Customer Name Already Exists");
                    }
                    }else{
                     request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Session has expired");
                    }
                }
                CustomerList = customerBP.processCustomerLists(customerTO);
                request.setAttribute("CustomerLists", CustomerList);
                session.setAttribute("uniquePageId", String.valueOf(java.lang.System.currentTimeMillis()));

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }   
    public ModelAndView editSaveCustomerOutstandingCredit(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        int insertContractRouteMater = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer)session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageUniqueId=request.getParameter("pageId");
        System.out.println("pageId:"+pageUniqueId);
        String sessionId=(String) session.getAttribute("uniquePageId");
       System.out.println("sessionId:"+sessionId);
        int roleId = (Integer) session.getAttribute("RoleId");

        try {

            String customerId = request.getParameter("customerId");
            String customerType = request.getParameter("creditType");
            System.out.println("customerType----"+customerType);
            String fixedCreditLimit = request.getParameter("fixedCreditLimit");
            String creditLimit = request.getParameter("creditLimit");
            String creditDays = request.getParameter("creditDays");
            String usedCreditLimit = request.getParameter("usedCreditLimit");
            
            String fixedPDALimit = request.getParameter("fixedPDALimit");
            String advanceAmount = request.getParameter("advanceAmount");
            
            customerTO.setCustomerType(customerType);
            customerTO.setCustomerId(customerId);
            System.out.println("customerType-----"+customerType);
            if("1".equals(customerType)){
            customerTO.setFixedCreditLimit(fixedCreditLimit);
            }else if("2".equals(customerType)){
            customerTO.setFixedCreditLimit(fixedPDALimit);
            }
            System.out.println("fixedCreditLimit-----"+fixedCreditLimit);
            System.out.println("fixedPDALimit-----"+fixedPDALimit);
            
            customerTO.setCreditLimit(creditLimit);
            customerTO.setUsedLimit(usedCreditLimit);
            customerTO.setAdvanceAmount(advanceAmount);
            customerTO.setCreditDays(creditDays);
            
                if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                        update = customerBP.editUpdateCustomerCredit(customerTO, userId);
                        if (update > 0) {
                            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Update Successfully");
                        }else{
                             request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Customer Failed to Update");
                        }
                }
                
                CustomerList = customerBP.processCustomerLists(customerTO);
                request.setAttribute("CustomerLists", CustomerList);
                request.setAttribute("roleId", roleId);
                System.out.println("CustomerList-qq--"+CustomerList.size());
                path = "content/Customer/manageCustomers.jsp";
                session.setAttribute("uniquePageId", String.valueOf(java.lang.System.currentTimeMillis()));

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


     


public ModelAndView editCustomerOutstandingCredit(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("handleViewCustomer ..");
        String path = "";
        ArrayList customerList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Customer";
        menuPath = "Customer  >>Customer Details ";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/Customer/editCustomerCredit.jsp";
                System.out.println("customerId" + customerCommand.getCustomerId());
                if (customerCommand.getCustomerName() != null && customerCommand.getCustomerName() != "") {
                    request.setAttribute("customerName", customerCommand.getCustomerName());
                }
                String customerId = "";
                customerId = request.getParameter("customerId");
                customerTO.setCustomerId(customerId);
               
                ArrayList customerCreditLimitDetails = new ArrayList();
                customerCreditLimitDetails = customerBP.getCustomerCreditLimitDetails(customerTO);
                request.setAttribute("customerCreditLimitDetails", customerCreditLimitDetails);
                
                ArrayList customerCreditLimit = new ArrayList();
                customerCreditLimit = customerBP.getCustomerDetails(customerTO);
                request.setAttribute("customerCreditLimit", customerCreditLimit);
                
                customerList = customerBP.getTripCustomerList();
                request.setAttribute("customerList", customerList);
                
                request.setAttribute("roleId",roleId);
                request.setAttribute("userId",userId);
                
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
//public ModelAndView handleViewCustomer(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        System.out.println("handleViewCustomer ..");
//        String path = "";
//        ArrayList customerList = new ArrayList();
//        HttpSession session = request.getSession();
//        customerCommand = command;
//        String menuPath = "";
//        String pageTitle = "View Customer";
//        menuPath = "Customer  >>Customer Details ";
//        request.setAttribute("pageTitle", pageTitle);
//        CustomerTO customerTO = new CustomerTO();
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//        int userId = (Integer) session.getAttribute("userId");
////        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
//        try {
//                String customerId = "";
//                String customerCode = "";
//                String custName = "";
//                String contractType = "";
//                String value="";
//                String customerType = "";
//
//                customerId = request.getParameter("customerId");
//                contractType = request.getParameter("contractType");
//                customerCode = request.getParameter("customerCode");
//                custName = request.getParameter("custName");
//                customerType = request.getParameter("customerType");
//                
//
//                customerTO.setCustomerId(customerId);
//                customerTO.setContractType(contractType);
//                customerTO.setCustomerType(customerType);
//                System.out.println("customerId" + customerId);
//                System.out.println("ContractType" + contractType);
//
//                String roleId = "" + (Integer) session.getAttribute("RoleId");
//                customerTO.setRoleId(roleId);
//                customerTO.setUserId(userId + "");
//
////                path = "content/Customer/manageCustomer.jsp";
////                setLocale(request, response);
//                request.setAttribute("customerId", customerId);
//                request.setAttribute("custName", custName);
//                request.setAttribute("customerCode", customerCode);
//                request.setAttribute("ContractType", contractType);
//                customerList = customerBP.processCustomerLists(customerTO);
//                request.setAttribute("CustomerLists", customerList);
//                
//           value=request.getParameter("param");
//            System.out.println("value----"+value);
//            if (!"ExportExcel".equals(value)) {
//                System.out.println("iam here");
//                   path = "content/Customer/manageCustomer.jsp";
//            } else {
//                System.out.println("iam here at ");
//                  path = "content/Customer/manageCustomerExport.jsp";
//            }
//        } catch (FPRuntimeException exception) {
//            exception.printStackTrace();
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }


public ModelAndView handleViewCustomers(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("handleViewCustomer ..");
        String path = "";
        ArrayList customerList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Customer";
        menuPath = "Customer  >>Customer Details ";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
                String customerId = "";
                String customerCode = "";
                String custName = "";
                String contractType = "";
                String customerType = "";

                customerId = request.getParameter("customerId");
                contractType = request.getParameter("contractType");
                customerCode = request.getParameter("customerCode");
                custName = request.getParameter("custName");
                customerType = request.getParameter("customerType");

                customerTO.setCustomerId(customerId);
                customerTO.setContractType(contractType);
                customerTO.setCustomerType(customerType);
                System.out.println("customerId" + customerId);
                System.out.println("ContractType" + contractType);

                String roleId = "" + (Integer) session.getAttribute("RoleId");
                customerTO.setRoleId(roleId);
                customerTO.setUserId(userId + "");

                path = "content/Customer/manageCustomers.jsp";
//                setLocale(request, response);
                request.setAttribute("customerId", customerId);
                request.setAttribute("custName", custName);
                request.setAttribute("customerCode", customerCode);
                request.setAttribute("ContractType", contractType);
                customerList = customerBP.processCustomerLists(customerTO);
                request.setAttribute("CustomerLists", customerList);
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleViewAlterScreen(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            int loginRecordId = (Integer) session.getAttribute("loginRecordId");
                String pageTitle = "Alter Customer";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList customerList = new ArrayList();
                path = "content/Customer/alterCustomer.jsp";
                customerList = customerBP.processCustomerList();
                System.err.append("customerlist size in alter path" + customerList.size());
                request.setAttribute("CustomerList", customerList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view customerList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateCustomer(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        try {
            String menuPath = "Customer  >>  Customer Details";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int update = 0;
            String[] idList = customerCommand.getCustIdList();
            String[] nameList = customerCommand.getCustNameList();
            String[] typeList = customerCommand.getCustTypeList();
            String[] contactPersonList = customerCommand.getCustContactPersonList();
            String[] addressList = customerCommand.getCustAddressList();
            String[] cityList = customerCommand.getCustCityList();
            String[] stateList = customerCommand.getCustStateList();
            String[] phoneList = customerCommand.getCustPhoneList();
            String[] mobileList = customerCommand.getCustMobileList();
            String[] emailList = customerCommand.getCustEmailList();
            String[] statusList = customerCommand.getCustStatusList();
            String[] selectedIndex = customerCommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");
            ArrayList List = new ArrayList();
            CustomerTO customerTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                customerTO = new CustomerTO();
                index = Integer.parseInt(selectedIndex[i]);
                customerTO.setCustId(Integer.parseInt(idList[index]));
                customerTO.setCustName(nameList[index]);
                customerTO.setCustomerType(typeList[index]);
                System.out.println("typeList[index]: " + typeList[index]);
                customerTO.setCustContactPerson(contactPersonList[index]);
                customerTO.setCustCity(cityList[index]);
                customerTO.setCustAddress(addressList[index]);
                customerTO.setCustState(stateList[index]);
                customerTO.setCustPhone(phoneList[index]);
                customerTO.setCustMobile(mobileList[index]);
                customerTO.setCustEmail(emailList[index]);
                customerTO.setCustStatus(statusList[index]);
                List.add(customerTO);
            }
            path = "content/Customer/manageCustomer.jsp";
            update = customerBP.processUpdateCustomer(List, userId);
            String pageTitle = "Manage Customer";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerList();
            request.setAttribute("CustomerLists", customerList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //CustomerController

    public ModelAndView handleViewContract(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Contract";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList contractList = new ArrayList();
        menuPath = "Contract  >> Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/contract/manageContract.jsp";
            //customerList = customerBP.processContractList();
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {

         FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         }*/ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewContract --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddContract(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Contract  >> Manage Contract ";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (customerCommand.getContManufacturerName() != null && customerCommand.getContManufacturerName() != "") {
                customerTO.setContManufacturerName(customerCommand.getContManufacturerName());
            }
            if (customerCommand.getContModelName() != null && customerCommand.getContModelName() != "") {
                customerTO.setContModelName(customerCommand.getContModelName());
            }
            if (customerCommand.getContPercentAgeUpSpares() != null && customerCommand.getContPercentAgeUpSpares() != "") {
                customerTO.setContPercentAgeUpSpares(customerCommand.getContPercentAgeUpSpares());
            }
            if (customerCommand.getContPercentAgeUpLabour() != null && customerCommand.getContPercentAgeUpLabour() != "") {
                customerTO.setContPercentAgeUpLabour(customerCommand.getContPercentAgeUpLabour());
            }
            if (customerCommand.getContStatus() != null && customerCommand.getContStatus() != "") {
                customerTO.setContStatus(customerCommand.getContStatus());
            }
            path = "content/contract/addContract.jsp";
            // status = customerBP.processInsertContract(customerTO, userId);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {

         FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customers  >> Bunk Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/manageBunkDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewBunkDetailsV(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        System.out.println("Block2......");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkName = request.getParameter("bunkName");
        System.out.println("bunkName.." + bunkName);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            /*if (!loginBP.checkAuthorisation(userFunctions, "Bunk-View")) {
             path = "content/common/NotAuthorized.jsp";
             } else {*/
            path = "content/Customer/manageBunkDetails.jsp";
            bunkList = customerBP.processBunkList(bunkName);
            request.setAttribute("bunkList", bunkList);
            System.out.println("bunkList.." + bunkList);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCreateBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/addBunkDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
         /*
         * run time exception has occurred. Directed to error page.
         */ /*FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand customerCommand) throws IOException {
        System.out.println("Block4...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bunkList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkName = request.getParameter("bunkName");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBunkName() != null && customerCommand.getBunkName() != "") {
                customerTO.setBunkName(customerCommand.getBunkName());
            }
            if (customerCommand.getRemarks() != null && customerCommand.getRemarks() != "") {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getLocation() != null && customerCommand.getLocation() != "") {
                customerTO.setCurrlocation(customerCommand.getLocation());
            }
            if (customerCommand.getCurrRate() != null && customerCommand.getCurrRate() != "") {
                customerTO.setCurrRate(customerCommand.getCurrRate());
            }
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setBunkState(customerCommand.getState());
            }
            if (customerCommand.getActiveStatus() != null && customerCommand.getActiveStatus() != "") {
                customerTO.setBunkStatus(customerCommand.getActiveStatus());
            }
            if (customerCommand.getFuelType() != null && customerCommand.getFuelType() != "") {
                customerTO.setFuelType(customerCommand.getFuelType());
            }
            path = "content/Customer/manageBunkDetails.jsp";
            System.out.println("1");
            status = customerBP.processInsertBunk(customerTO, userId);
            System.out.println("2");

            bunkList = customerBP.processBunkList(bunkName);

            request.setAttribute("bunkList", bunkList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterbunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkalterList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkId = request.getParameter("bunkId");
        System.out.println("bunkId.." + bunkId);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/addBunkDetails.jsp";

            bunkalterList = customerBP.processBunkalterList(bunkId);
            request.setAttribute("bunkalterList", bunkalterList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView UpdateBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        System.out.println("Block4...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bunkList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkId = request.getParameter("bunkId");
        String bunkName = request.getParameter("bunkName");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBunkName() != null && customerCommand.getBunkName() != "") {
                customerTO.setBunkName(customerCommand.getBunkName());
            }
            if (customerCommand.getRemarks() != null && customerCommand.getRemarks() != "") {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getLocation() != null && customerCommand.getLocation() != "") {
                customerTO.setCurrlocation(customerCommand.getLocation());
            }
            if (customerCommand.getCurrRate() != null && customerCommand.getCurrRate() != "") {
                customerTO.setCurrRate(customerCommand.getCurrRate());
            }
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setBunkState(customerCommand.getState());
            }
            if (customerCommand.getActiveStatus() != null && customerCommand.getActiveStatus() != "") {
                customerTO.setBunkStatus(customerCommand.getActiveStatus());
            }
            if (customerCommand.getFuelType() != null && customerCommand.getFuelType() != "") {
                customerTO.setFuelType(customerCommand.getFuelType());
            }
            path = "content/trip/manageBunkDetails.jsp";
            status = customerBP.processBunkUpdateList(customerTO, userId, bunkId);
            bunkList = customerBP.processBunkList(bunkName);
            request.setAttribute("bunkList", bunkList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewEditCustomer(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> View / Edit Customer ";
        String pageTitle = "View / Edit Customer";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int roleId = (Integer) session.getAttribute("RoleId");
          System.out.println("roleId=====" + roleId);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/Customer/editCustomer.jsp";
//                System.out.println("customerId" + customerCommand.getCustomerId());
//                if (customerCommand.getCustomerName() != null && customerCommand.getCustomerName() != "") {
//                    request.setAttribute("customerName", customerCommand.getCustomerName());
//                }
                String customerId = "";
                customerId = request.getParameter("customerId");
                customerTO.setCustomerId(customerId);
                ArrayList billingTypeList = new ArrayList();
                billingTypeList = customerBP.getBillingTypeList();
                request.setAttribute("billingTypeList", billingTypeList);
                ArrayList accountManagerList = new ArrayList();
                accountManagerList = customerBP.getAccountManagerList();
                request.setAttribute("accountManagerList", accountManagerList);
                ArrayList customerGroup = new ArrayList();
                customerGroup = customerBP.getCustomerGroupList();
                request.setAttribute("customerGroup", customerGroup);
                ArrayList stateList = new ArrayList();
                stateList=customerBP.getStateList();
                request.setAttribute("stateList",stateList);
                ArrayList getCustomerDetails = new ArrayList();
                getCustomerDetails = customerBP.getCustomerDetails(customerTO);
                request.setAttribute("getCustomerDetails", getCustomerDetails);
                ArrayList organizationList = new ArrayList();
                organizationList=customerBP.getOrganizationList();
                request.setAttribute("organizationList",organizationList);
                request.setAttribute("roleId",roleId);
                request.setAttribute("userId",userId);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getCustomerName(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String custName = "";
            response.setContentType("text/html");
            custName = request.getParameter("custName");
            customerTO.setCustName(custName);
            customerList = customerBP.getCustomerName(customerTO);
            System.out.println("customerList.size() = " + customerList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = customerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                customerTO = (CustomerTO) itr.next();
                jsonObject.put("Name", customerTO.getCustomerId() + "-" + customerTO.getCustName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }
    public void getCustomerCode(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
//                JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String customerCode = "";
            response.setContentType("text/html");
            customerCode = request.getParameter("customerCode");
            customerTO.setCustomerCode(customerCode);
            customerList = customerBP.getCustomerCodes(customerTO);
            System.out.println("customerList.size() = " + customerList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = customerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                customerTO = (CustomerTO) itr.next();
                jsonObject.put("Name", customerTO.getCustomerId() + "~" + customerTO.getCustomerCode());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }
 public void checkCustomerNameExists(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
//                JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            int checkCustomerName = 0;
            response.setContentType("text/html");
            customerTO.setCustomerName(request.getParameter("custName"));
            checkCustomerName = customerBP.checkCustomerName(customerTO);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CustomerCount", checkCustomerName);
            pw.print(jsonObject);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

 public ModelAndView configModelFuel(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>config Model Fuel";
        String pageTitle = "configModelFuel";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

                String contractRateId = request.getParameter("contractRateId");
                String status  = request.getParameter("status");
                request.setAttribute("contractRateId", contractRateId);
                request.setAttribute("status", status);
                String vehicleTypeId  = request.getParameter("typeId");
                customerTO.setVehicleTypeId(vehicleTypeId);
                System.out.println("vehicleTypeId@@"+vehicleTypeId);
                System.out.println("contractRateId@@"+contractRateId);
                //
                ArrayList getModelFuelList = new ArrayList();
                getModelFuelList = customerBP.getModelFuelList(vehicleTypeId,contractRateId);
                request.setAttribute("getModelFuelList", getModelFuelList);
                System.out.println("getModelFuellist.size(@@)"+getModelFuelList.size());
                path = "content/Customer/configModelFuel.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
   public void saveModelFuelDetails(HttpServletRequest request, HttpServletResponse response,  CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        int updateStatus = 0;
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "customer  >> saveCustomerSlabRouteDetails ";
        String pageTitle = "Save CustomerSlabRouteDetails ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        PrintWriter pw = response.getWriter();
        try {
            String[] fuelTypeId= request.getParameterValues("fuelTypeId[]");
            String[] fuelModelId= request.getParameterValues("fuelModelId[]");
            String contractRateId= request.getParameter("contractRateId");
            String[] modelId= request.getParameterValues("modelId[]");
            String[] fuelVehicle= request.getParameterValues("fuelVehicle[]");
            String[] fuelDG= request.getParameterValues("fuelDG[]");
            String[] totalFuel= request.getParameterValues("totalFuel[]");
            int inserStatus =0;
            inserStatus = customerBP.saveModelFuelDetails(fuelTypeId,fuelModelId,contractRateId,modelId,fuelVehicle,fuelDG,totalFuel,userId);
            System.out.println("inserStatus"+inserStatus);
           response.setContentType("text/css");

           pw.print(inserStatus);
           } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

   public void checkGstNoExists(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
       
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            int gstEixsts = 0;
            response.setContentType("text/html");
            String gstNo=request.getParameter("gstNo");
            gstEixsts = customerBP.checkGstNo(gstNo);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("gstEixsts", gstEixsts);
            pw.print(jsonObject);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    } 
//  Customer master changes
   
   public ModelAndView handleViewCustomer(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("handleViewCustomer ..");
        String path = "";
        ArrayList customerList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Customer";
        menuPath = "Customer  >>Customer Details ";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
                String customerId = "";
                String customerCode = "";
                String custName = "";
                String contractType = "";
                String value="";
                String customerType = "";

                customerId = request.getParameter("customerId");
                contractType = request.getParameter("contractType");
                customerCode = request.getParameter("customerCode");
                custName = request.getParameter("custName");
                customerType = request.getParameter("customerType");
                

                customerTO.setCustomerId(customerId);
                customerTO.setContractType(contractType);
                customerTO.setCustomerType(customerType);
                customerTO.setCustomerCode(customerCode);
                System.out.println("customerId" + customerId);
                System.out.println("ContractType" + contractType);

                String roleId = "" + (Integer) session.getAttribute("RoleId");
                customerTO.setRoleId(roleId);
                customerTO.setUserId(userId + "");

//                path = "content/Customer/manageCustomer.jsp";
//                setLocale(request, response);
                request.setAttribute("customerId", customerId);
                request.setAttribute("custName", custName);
                request.setAttribute("customerCode", customerCode);
                request.setAttribute("ContractType", contractType);
                customerList = customerBP.processCustomerLists(customerTO);
                request.setAttribute("CustomerLists", customerList);
                
           value=request.getParameter("param");
            System.out.println("value----"+value);
            if (!"ExportExcel".equals(value)) {
                System.out.println("iam here");
                   path = "content/Customer/manageCustomer.jsp";
            } else {
                System.out.println("iam here at ");
                  path = "content/Customer/manageCustomerExport.jsp";
            }
        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
 public ModelAndView handleCustomerDocuments(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        HttpSession session = request.getSession();
//        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        int configDetailsMaster = 0;
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Operation  >> configDetailsMaster ";
        String pageTitle = "configDetailsMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String custId = "";
        String custName = "";
        custId = request.getParameter("customerId");
        custName = request.getParameter("custName");
        System.out.println("custId" + custId);

        try {
            String customerId = request.getParameter("customerId");
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }

//            ArrayList consignorList = new ArrayList();
//            consignorList = customerBP.processContractConsignorList(customerTO, custId);
//            request.setAttribute("ConsignorList", consignorList);

//            ArrayList cityList = new ArrayList();
//            cityList = customerBP.processCityList();
//            request.setAttribute("CityList", cityList);
            request.setAttribute("custName", custName);
            request.setAttribute("customerId", custId);
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            ArrayList getTripAttachments = customerBP.getCustomerAttachments(customerTO);
                System.out.println("getTripAttachments"+getTripAttachments.size());        
            if (getTripAttachments.size() > 0) {
                request.setAttribute("viewPODDetails", getTripAttachments);
            }

            path = "content/Customer/customerFileUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }
    

  public ModelAndView saveCustomerAttachments(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            // return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        customerCommand = command;
        String menuPath = "Operation  >>  Edit Start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String tripSheetId1 = "";
        String customerId = "";
        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        ArrayList getTripAttachments = new ArrayList();
        String tripId = "";
        tripId = request.getParameter("tripId");
        System.out.println("tripId==" + tripId);

        customerCommand = command;

        int status = 0;

        String tripSheetId = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            CustomerTO customerTO = new CustomerTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/content/upload");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            // for ubuntu
//                            String[] splitFileName = uploadedFileName[j].split(".");
                            // for windows
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "/" + tempFilePath;
//                          for windows machines
//                          tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
//                          actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("customerId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            customerId = paramPart.getStringValue();
                            System.out.println("customerId in else==" + customerId);
                        }

                        if (partObj.getName().equals("podRemarks")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            podRemarks1[n] = paramPart.getStringValue();
                            n++;
                        }

                    }
                }
            }

            String file = "";
            String remarks = "";
            String saveFile = "";

            customerTO.setCustomerId(customerId);
            for (int x = 0; x < j; x++) {
                file = tempFilePath[x];
                remarks = podRemarks1[x];
                saveFile = fileSaved[x];
                status = customerBP.saveTripAttachments(file, customerId, remarks, saveFile, customerTO, userId);
                System.out.println("saveTripPodDetails1==" + status);
            }
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Customer Attachment Updated Successfully....");
            }

            getTripAttachments = customerBP.getCustomerAttachments(customerTO);
            if (getTripAttachments.size() > 0) {
                request.setAttribute("viewPODDetails", getTripAttachments);
            }

            String customerId1 = request.getParameter("customerId");
            request.setAttribute("customerId", customerId1);
            request.setAttribute("customerId", customerId);
            System.out.println("customerId=" + customerId);

            String type = request.getParameter("type");
            System.out.println("type===" + type);
            
            ArrayList customerList = customerBP.processCustomerLists(customerTO);
            request.setAttribute("CustomerLists", customerList);
             path ="content/Customer/customerFileUpload.jsp";


            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

//        return new ModelAndView(path);
        return new ModelAndView(path);
    }
   

}
