
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */

            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                //  alert(value);
                if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handlePayablePartReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    }
//                if (document.getElementById('fromDate').value == '') {
//                    alert("Please select from Date");
//                    document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
//                } else {
//                     else {
//                        document.accountReceivable.action = '/throttle/handleTripEndedNotClosedBeyond24.do?param=Search';
//                        document.accountReceivable.submit();
//                    }
//                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }
//
//            function viewCustomerProfitDetails(tripIds) {
////            alert(tripIds);
//                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
//            }
        </script>
     <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Payable Part report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                            <table style="width:100%" align="center" border="0" class="tabouterborder" >
                                    <tr height="30"   ><td  style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Payable part report</td></tr>
                                      <tr height="40">
                                       
                                    
                                          <td class="tabtext"  align="right"><center><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></center>&nbsp;&nbsp;</td>
                                        
                                    </tr>
                                </table> 
           
<!--                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>-->
                <br>
                  <table align="center" border="1" id="table" class="table table-info mb30 table-hover" style="width:100%;" >
                    <thead height="30">
                       
                        <tr >
                            <th >Particulars</th>
                            <th >Status</th>
                            <th >Gr status</th>
                            <th >Opening GR of the Day</th>
                            <th >GR issued during the day</th>
                            <th >GR closed</th>
                            <th >Net GR Still Pending </th>
                            <th >Expense booked during the day</th>
                            <th >pending Expense to be booked</th>
                            <th >trip pending from <7 days</th>
                            <th >Amount pending from <7 Days</th>
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                   
                        <c:set var="totOpenGR" value="${0}"/>    
                        <c:set var="totGRissued" value="${0}"/>    
                        <c:set var="totGRClosed" value="${0}"/>    
                        <c:set var="totNetGRPending" value="${0}"/>    
                        <c:set var="totExpDuringDay" value="${0}"/>    
                        <c:set var="totPendingExp" value="${0}"/>    
                        <c:set var="totpendingGRless7" value="${0}"/>    
                        <c:set var="totAmtPendingless7" value="${0}"/>    

                            <tr  height="30">
                            <td align="center" class="text1" rowspan="15">Trip End not closed</td>
                            <td align="center" class="text1" rowspan="4">own</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grClosed']}"/></td>
                             <c:set var="netGrPending1" value="${tripMap['own-load-openGR'] + tripMap['own-load-issueGR'] - tripMap['own-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending1}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Loaded DSO</td>
                           <td align="center" class="text1"><c:out value="${tripMap['own-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grClosed']}"/></td>
                             <c:set var="netGrPending2" value="${tripMap['own-dso-openGR'] + tripMap['own-dso-issueGR'] - tripMap['own-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending2}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grClosed']}"/></td>
                             <c:set var="netGrPending3" value="${tripMap['own-empty-openGR'] + tripMap['own-empty-issueGR'] - tripMap['own-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending3}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grClosed']}"/></td>
                            <c:set var="netGrPending4" value="${tripMap['own-empty-openGR'] + tripMap['own-empty-issueGR'] - tripMap['own-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending4}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                        <c:set var="totOpenGR" value="${tripMap['own-empty-openGR'] + tripMap['own-dso-openGR'] + tripMap['own-load-openGR'] + tripMap['own-btt-openGR']}"/>   
                        <c:set var="totGRissued" value="${tripMap['own-empty-issueGR'] + tripMap['own-dso-issueGR'] + tripMap['own-load-issueGR'] + tripMap['own-btt-issueGR']}"/>   
                        <c:set var="totGRClosed" value="${tripMap['own-empty-grClosed'] + tripMap['own-dso-grClosed'] + tripMap['own-load-grClosed'] + tripMap['own-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending" value="${netGrPending1 + netGrPending2 + netGrPending3+ netGrPending4}"/>       
                        <c:set var="totExpDuringDay" value="${tripMap['own-empty-expenseDuringDay'] + tripMap['own-dso-expenseDuringDay'] + tripMap['own-load-expenseDuringDay'] + tripMap['own-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp" value="${tripMap['own-empty-pendingExpense'] + tripMap['own-dso-pendingExpense'] + tripMap['own-load-pendingExpense'] + tripMap['own-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless7" value="${tripMap['own-empty-pendingSevenDayExpense'] + tripMap['own-dso-pendingSevenDayExpense'] + tripMap['own-load-pendingSevenDayExpense'] + tripMap['own-btt-pendingSevenDayExpense']}"/>      
                                 
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless7}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Leased</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-grClosed']}"/></td>
                             <c:set var="netGrPending5" value="${tripMap['leased-load-openGR'] + tripMap['leased-load-issueGR'] - tripMap['leased-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending5}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                           <td align="center" class="text1"><c:out value="${tripMap['leased-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-grClosed']}"/></td>
                            <c:set var="netGrPending6" value="${tripMap['leased-dso-openGR'] + tripMap['leased-dso-issueGR'] - tripMap['leased-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending6}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-grClosed']}"/></td>
                            <c:set var="netGrPending7" value="${tripMap['leased-empty-openGR'] + tripMap['leased-empty-issueGR'] - tripMap['leased-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-grClosed']}"/></td>
                             <c:set var="netGrPending8" value="${tripMap['leased-btt-openGR'] + tripMap['leased-btt-issueGR'] - tripMap['leased-btt-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending8}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                             <c:set var="totOpenGR2" value="${tripMap['leased-empty-openGR'] + tripMap['leased-dso-openGR'] + tripMap['leased-load-openGR'] + tripMap['leased-btt-openGR']}"/>   
                        <c:set var="totGRissued2" value="${tripMap['leased-empty-issueGR'] + tripMap['leased-dso-issueGR'] + tripMap['leased-load-issueGR'] + tripMap['leased-btt-issueGR']}"/>   
                        <c:set var="totGRClosed2" value="${tripMap['leased-empty-grClosed'] + tripMap['leased-dso-grClosed'] + tripMap['leased-load-grClosed'] + tripMap['leased-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending2" value="${netGrPending5 + netGrPending6 + netGrPending7+ netGrPending8}"/>       
                        <c:set var="totExpDuringDay2" value="${tripMap['leased-empty-expenseDuringDay'] + tripMap['leased-dso-expenseDuringDay'] + tripMap['leased-load-expenseDuringDay'] + tripMap['leased-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp2" value="${tripMap['leased-empty-pendingExpense'] + tripMap['leased-dso-pendingExpense'] + tripMap['leased-load-pendingExpense'] + tripMap['leased-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless72" value="${tripMap['leased-empty-grlessthn7'] + tripMap['leased-dso-grlessthn7'] + tripMap['leased-load-grlessthn7'] + tripMap['leased-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless72" value="${tripMap['leased-empty-pendingSevenDayExpense'] + tripMap['leased-dso-pendingSevenDayExpense'] + tripMap['leased-load-pendingSevenDayExpense'] + tripMap['leased-btt-pendingSevenDayExpense']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed2}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending2}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay2}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp2}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless72}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless72}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Hire</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-grClosed']}"/></td>
                            <c:set var="netGrPending9" value="${tripMap['hire-load-openGR'] + tripMap['hire-load-issueGR'] - tripMap['hire-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending9}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-grClosed']}"/></td>
                             <c:set var="netGrPending10" value="${tripMap['hire-dso-openGR'] + tripMap['hire-dso-issueGR'] - tripMap['hire-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending10}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                             <td align="center" class="text1"><c:out value="${tripMap['hire-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-grClosed']}"/></td>
                            <c:set var="netGrPending11" value="${tripMap['hire-empty-openGR'] + tripMap['hire-empty-issueGR'] - tripMap['hire-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending11}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-grClosed']}"/></td>
                            <c:set var="netGrPending12" value="${tripMap['hire-btt-openGR'] + tripMap['hire-btt-issueGR'] - tripMap['hire-btt-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending12}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                        <c:set var="totOpenGR3" value="${tripMap['hire-empty-openGR'] + tripMap['hire-dso-openGR'] + tripMap['hire-load-openGR'] + tripMap['hire-btt-openGR']}"/>   
                        <c:set var="totGRissued3" value="${tripMap['hire-empty-issueGR'] + tripMap['hire-dso-issueGR'] + tripMap['hire-load-issueGR'] + tripMap['hire-btt-issueGR']}"/>   
                        <c:set var="totGRClosed3" value="${tripMap['hire-empty-grClosed'] + tripMap['hire-dso-grClosed'] + tripMap['hire-load-grClosed'] + tripMap['hire-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending3" value="${netGrPending9 + netGrPending10 + netGrPending11+ netGrPending12}"/>       
                        <c:set var="totExpDuringDay3" value="${tripMap['hire-empty-expenseDuringDay'] + tripMap['hire-dso-expenseDuringDay'] + tripMap['hire-load-expenseDuringDay'] + tripMap['hire-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp3" value="${tripMap['hire-empty-pendingExpense'] + tripMap['hire-dso-pendingExpense'] + tripMap['hire-load-pendingExpense'] + tripMap['hire-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless73" value="${tripMap['hire-empty-grlessthn7'] + tripMap['hire-dso-grlessthn7'] + tripMap['hire-load-grlessthn7'] + tripMap['hire-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless73" value="${tripMap['hire-empty-pendingSevenDayExpense'] + tripMap['hire-dso-pendingSevenDayExpense'] + tripMap['hire-load-pendingSevenDayExpense'] + tripMap['hire-btt-pendingSevenDayExpense']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR3}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued3}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed3}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending3}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay3}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp3}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless73}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless73}"/></td>
                            </tr>
                            <c:set var="grandTotOpenGr" value="${totOpenGR + totOpenGR1 + totOpenGR2+ totOpenGR3}"/>       
                            <c:set var="grandTotGRissued" value="${totGRissued + totGRissued1 + totGRissued2+ totGRissued3}"/>       
                            <c:set var="grandTotGRClosed" value="${totGRClosed + totGRClosed1 + totGRClosed2+ totGRClosed3}"/>       
                            <c:set var="grandtotNetGRPending" value="${totNetGRPending + totNetGRPending1 + totNetGRPending2+ totNetGRPending3}"/>       
                            <c:set var="grandtotExpDuringDay" value="${totExpDuringDay + totExpDuringDay1 + totExpDuringDay2+ totExpDuringDay3}"/>       
                            <c:set var="grandtotPendingExp" value="${totPendingExp + totPendingExp1 + totPendingExp2+ totPendingExp3}"/>       
                            <c:set var="grandtottotpendingGRless7" value="${totpendingGRless7 + totpendingGRless71 + totpendingGRless72+ totpendingGRless73}"/>       
                            <c:set var="grandtottotAmtPendingless7" value="${totAmtPendingless7 + totAmtPendingless71 + totAmtPendingless72+ totAmtPendingless73}"/>       
                             <tr  height="30">
                            <td align="center" class="text1" >Grand Total</td>
                            <td align="center" class="text1" ></td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${grandTotOpenGr}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRClosed}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotNetGRPending}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotExpDuringDay}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotPendingExp}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotpendingGRless7}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotAmtPendingless7}"/></td>
                            </tr>
                       
                        <%
                            index++;
                            sno++;
                        %>
                   
                </table>
           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
