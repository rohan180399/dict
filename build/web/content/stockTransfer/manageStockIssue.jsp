<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    
                    document.vendorDetail.action ='/throttle/addVendorPage.do';
                }else if(value == 'modify'){
                
                document.vendorDetail.action ='/throttle/';
            }
            document.vendorDetail.submit();
        }
    </script>
    
    <body>
        
        <form method="post" name="vendorDetail">
           
<%@ include file="/content/common/path.jsp" %>
                    

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0;  %>    
            <br>
            <c:if test = "${ApprovedStockList != null}" >
                <table width="550" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
            
                    <tr align="center">
                        <td width="55" align="left" height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td width="95" align="left" height="30" class="contentsub"><div class="contentsub">Request No</div></td>
                        <td width="105" align="left" height="30" class="contentsub"><div class="contentsub">Service Point</div></td>
                        <td width="98" align="left" height="30" class="contentsub"><div class="contentsub">Required date</div></td>
                        <td width="59" align="left" height="30" class="contentsub"><div class="contentsub">Status</div></td>
                        <td width="50" align="left" height="30" class="contentsub"><div class="contentsub">Issue</div></td>
                         
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${ApprovedStockList}" var="list"> 
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>         
                        <tr  width="208" height="40" > 
                            <td class="<%=classText %>" align="left" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${list.requestId}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${list.servicePointName}"/></td>
                             <td class="<%=classText %>" align="left" height="30"><c:out value="${list.requiredDate}"/></td>
                              <td class="<%=classText %>" align="left" height="30"><c:out value="${list.status}"/></td>
                              <c:if test = "${list.reqType == 'NEW'}" >
                             <td class="<%=classText %>" align="left"><a href="/throttle/issueItemPage.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                              <c:if test = "${list.reqType == 'New'}" >
                             <td class="<%=classText %>" align="left"><a href="/throttle/issueItemPage.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                             <c:if test = "${list.reqType == 'RC'}">
                             <td class="<%=classText %>" align="left"><a href="/throttle/rcIssue.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

