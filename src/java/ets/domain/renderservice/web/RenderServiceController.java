/*---------------------------------------------------------------------------
 * RenderServiceController.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.renderservice.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.renderservice.business.RenderServiceBP;
import ets.domain.operation.business.OperationBP;
import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.renderservice.business.JobCardItemTO;
import ets.domain.renderservice.business.RenderServiceTO;

import ets.arch.business.PaginationHelper;         
import ets.arch.util.SendMail;


import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import ets.domain.company.business.CompanyBP;
import ets.domain.company.business.CompanyTO;
import ets.domain.mrs.business.MrsBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.service.business.ServiceBP;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 15, 2009              Srinivasan.R			       Created
 *
 ******************************************************************************/
public class RenderServiceController extends BaseController {

    RenderServiceCommand renderServiceCommand;
    RenderServiceBP renderServiceBP;
    OperationBP operationBP;
    LoginBP loginBP;
    CompanyBP companyBP;
    TripBP tripBP;
    ServiceBP serviceBP;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public RenderServiceCommand getRenderServiceCommand() {
        return renderServiceCommand;
    }

    public RenderServiceBP getRenderServiceBP() {
        return renderServiceBP;
    }

    public void setRenderServiceBP(RenderServiceBP renderServiceBP) {
        this.renderServiceBP = renderServiceBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }
    MrsBP mrsBP;

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public ServiceBP getServiceBP() {
        return serviceBP;
    }

    public void setServiceBP(ServiceBP serviceBP) {
        this.serviceBP = serviceBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

    /**
     * This method caters to get free service
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleCloseJobCardView(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");




        menuPath = "Render Service >>Close Job Card";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        RenderServiceTO closeJcListTO = new RenderServiceTO();
        ArrayList servicePoints = new ArrayList();
        try {
           // setLocale(request, reponse);
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-CloseJobCard")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/RenderService/CloseJobCardView.jsp";
                ArrayList jcList = new ArrayList();
                String regNo = request.getParameter("regNo");
                String jcId = request.getParameter("jobcardId");
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                PaginationHelper pagenation = new PaginationHelper();
                int startIndex = 0;
                int endIndex = 0;
                if (regNo != null) {
                    closeJcListTO.setRegNo(regNo);
                }
                if (jcId != null) {
                    closeJcListTO.setJcId(jcId);
                }

                closeJcListTO.setCompId(request.getParameter("serviceLocation"));
                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);

                jcList = renderServiceBP.getCloseJobCardViewList(closeJcListTO);
                totalRecords = jcList.size();
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                }
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                //////System.out.println("pageNo" + pageNo);
                request.setAttribute("totalPages", totalPages);
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();


                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);

                jcList = renderServiceBP.getCloseJobCardViewList(closeJcListTO);
                servicePoints = operationBP.getServicePoints();
                request.setAttribute("servicePoints", servicePoints);

                if (jcList.size() > 0) {
                    request.setAttribute("jcList", jcList);
                }
                request.setAttribute("regNo", regNo);
                request.setAttribute("jcId", jcId);
                request.setAttribute("compId", request.getParameter("serviceLocation"));
//            }
              
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    public ModelAndView handleCloseJobCardDetailsNew(HttpServletRequest request, HttpServletResponse response, RenderServiceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";

        String pageTitle = "Job Card Status";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >> Schedule Job card";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/CloseJobCard.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
//            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Schedule")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
                int workOrderId = Integer.parseInt(request.getParameter("workOrderId"));
                int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
                String jcMYFormatNo = request.getParameter("jcMYFormatNo");
                int km = Integer.parseInt(request.getParameter("km"));
                int hm = Integer.parseInt(request.getParameter("hm"));
                int compId = Integer.parseInt((String) session.getAttribute("companyId"));

                ArrayList vehicleDetails = new ArrayList();
                ArrayList workOrderDetails = new ArrayList();
                ArrayList problemDetails = new ArrayList();
                ArrayList jobcardList = new ArrayList();
                ArrayList problemList = new ArrayList();
                ArrayList routineServiceList = new ArrayList();
                ArrayList nonGracePeriodList = new ArrayList();
                ArrayList nonGraceSchedList = new ArrayList();

                ArrayList jobCardProblemDetails = new ArrayList();
                ArrayList bodyWorksList = new ArrayList();
                bodyWorksList = operationBP.getBodyWorksList(jobcardId);
                vehicleDetails = operationBP.getVehicleDetails(vehicleId, jobcardId);
                ArrayList scheduledServices = new ArrayList();
                scheduledServices = operationBP.getScheduledServices(vehicleId, jobcardId);
                String compDate = operationBP.getcomDate(jobcardId);
                String[] jdata = compDate.split("~");
                compDate = jdata[0];
                String jRemarks = jdata[1];

                problemDetails = operationBP.getProblemDetails(workOrderId, jobcardId);

                jobCardProblemDetails = operationBP.jobCardProblemDetails(jobcardId);
                OperationTO operatonTO = null;
//                String customer = "";
//                if (jobCardProblemDetails.size() != 0) {
//                    Iterator itr = jobCardProblemDetails.iterator();
//                    while (itr.hasNext()) {
//                        operatonTO = new OperationTO();
//                        operatonTO = (OperationTO) itr.next();
//                        //////System.out.println("in loop");
//                        customer = operatonTO.getIdentifiedby();
//                        if (customer.equalsIgnoreCase("Customer")) {
//                            //////System.out.println("in loop");
//                            problemDetails.add(operatonTO);
//
//                        }
//                    }
//                }

                workOrderDetails = operationBP.getWorkOrderDetails(workOrderId);
                jobcardList = operationBP.getJobcardList(vehicleId, jobcardId);
                problemList = operationBP.getProblemList(vehicleId, workOrderId);
                routineServiceList = operationBP.getServiceList(vehicleId, km, jobcardId);
//                nonGraceSchedList = operationBP.getNonGraceSchedServices(routineServiceList, vehicleId, jobcardId, km, hm);
//                routineServiceList.addAll(nonGraceSchedList);
                nonGracePeriodList = operationBP.handleNonGracePeriodServices(vehicleId,km, routineServiceList);

                ArrayList technicians = new ArrayList();
                technicians = mrsBP.techniciansList(compId);
                request.setAttribute("vehicleDetails", vehicleDetails);
                request.setAttribute("technicians", technicians);
                request.setAttribute("bodyWorksList", bodyWorksList);
                request.setAttribute("vehicleId", vehicleId);
                request.setAttribute("jobCardProblemDetails", jobCardProblemDetails);
                ArrayList sections = new ArrayList();
                sections = serviceBP.getSections();
                request.setAttribute("sections", sections);
                request.setAttribute("vehicleDetails", vehicleDetails);
                request.setAttribute("scheduledServices", scheduledServices);
                //////System.out.println("problemDetails.size() = " + jobCardProblemDetails.size());
//                request.setAttribute("workOrderProblemDetails", problemDetails);
                request.setAttribute("workOrderProblemDetails", jobCardProblemDetails);
                request.setAttribute("workOrderDetails", workOrderDetails);
                request.setAttribute("jobCardList", jobcardList);
                request.setAttribute("problemList", problemList);
                request.setAttribute("routineServiceList", routineServiceList);
                request.setAttribute("nonGracePeriodList", nonGracePeriodList);
                request.setAttribute("workOrderId", workOrderId);
                request.setAttribute("jobcardId", jobcardId);
                request.setAttribute("jcMYFormatNo", "JC-"+jobcardId);

                request.setAttribute("km", request.getParameter("km"));
                request.setAttribute("hm", request.getParameter("hm"));
                request.setAttribute("reqDate", request.getParameter("reqDate"));
                request.setAttribute("compDate", compDate);
                request.setAttribute("jRemarks", jRemarks);

//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  search JobCard Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    public ModelAndView handleCloseJobCardDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >>Close Job Card";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-CloseJobCard")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
              //  setLocale(request, reponse);
                path = "content/RenderService/CloseJobCardDetails.jsp";
                ArrayList jcList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getCloseJobCardDetails(Integer.parseInt(command.getJobCardId()));
                request.setAttribute("jcList", jcList);

                ArrayList secList = new ArrayList();
                secList = renderServiceBP.getSections();
                //////System.out.println("secList" + secList.size());
                request.setAttribute("sections", secList);
                String laborExpense = renderServiceBP.getActualLaborExpenseForClosing(command.getJobCardId());
                String[] temp = null;
                String totalTechnicians = "0";
                String totalHrs = "0";
                String totalAmt = "0";
                //////System.out.println("Hi... con laborExpense: " + laborExpense);
                if (laborExpense != null) {
                    //////System.out.println("con if 1");
                    temp = laborExpense.split("~");
                    //////System.out.println("con if 2");
                    totalTechnicians = temp[0];
                    totalHrs = temp[1];
                    totalAmt = temp[2];
                }

                request.setAttribute("totalTechnicians", totalTechnicians);
                request.setAttribute("totalHrs", totalHrs);
                request.setAttribute("totalAmt", totalAmt);

            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleExtCloseJobCardDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >>Close Job Card";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-CloseJobCard")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/RenderService/CloseExtJobCardDetails.jsp";
                ArrayList jcList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getCloseJobCardDetails(Integer.parseInt(command.getJobCardId()));
                request.setAttribute("jcList", jcList);

                ArrayList secList = new ArrayList();
                secList = renderServiceBP.getSections();
                //////System.out.println("secList" + secList.size());
                request.setAttribute("sections", secList);
                String laborExpense = renderServiceBP.getActualLaborExpenseForClosing(command.getJobCardId());
                String[] temp = null;
                String totalTechnicians = "0";
                String totalHrs = "0";
                String totalAmt = "0";
                if (laborExpense != null) {
                    temp = laborExpense.split("~");
                    totalTechnicians = temp[0];
                    totalHrs = temp[1];
                    totalAmt = temp[2];
                }

                request.setAttribute("totalTechnicians", totalTechnicians);
                request.setAttribute("totalHrs", totalHrs);
                request.setAttribute("totalAmt", totalAmt);

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleJobCardClosure(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ModelAndView mv;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >>Close Job Card";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        RenderServiceTO renTO = new RenderServiceTO();
        mv = new ModelAndView();
        try {
            int userId = (Integer) session.getAttribute("userId");
            String[] selectedIndex = request.getParameterValues("selectedindex");
            //String[] problemId=request.getParameterValues("qty");
            //String[] activityId=request.getParameterValues("option");
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-CloseJobCard")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
//                String[] selectedIndex = command.getSelectedIndex();
//                String[] activityId = null;
//                String[] qty = null;
//                String[] amt = null;
//                String approve = "";
                //String[] problemId = command.getProblemId();
                
                String[] activityId = new String[selectedIndex.length];
                String[] qty = new String[selectedIndex.length];
                String[] amt = new String[selectedIndex.length];
                for (int i = 0; i < selectedIndex.length; i++) {
                    activityId[i] = request.getParameter("activityId" + selectedIndex[i]);
                    qty[i] = request.getParameter("qty" + selectedIndex[i]);
                    amt[i] = request.getParameter("amt" + selectedIndex[i]);
                    //////System.out.println("i/p:" + activityId[i] + ":" + qty[i] + ":" + amt[i] + ":");
                }
                String[] causeId = command.getCauseId();
                String approve = command.getApprove();
                 
                String remarks = command.getRemarks();
                int jobCardId = Integer.parseInt(command.getJobCardId());

                String labourCount = request.getParameter("labourCount");
                String labourHours = request.getParameter("labourHours");
                String labourExpenseAmount = request.getParameter("labourExpenseAmount");
                String consumbalesAmount = request.getParameter("consumbalesAmount");

                //////System.out.println("handleJobCardClosure...con");
                renderServiceBP.doJobCardClosure(userId, jobCardId, activityId, qty, amt, approve, remarks,
                            labourCount, labourHours, labourExpenseAmount, consumbalesAmount);
                //////System.out.println("srini here 111111112222222222222");
                path = "content/RenderService/CloseJobCardView.jsp";
                request.setAttribute("successMessage", "Job Card Closed Succesfully");

                /*

                String to = "";
                String cc = "";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";
                String activitycode = "EMTRP1";

                ArrayList emaildetails = new ArrayList();
                emaildetails = operationBP.getEmailDetails(activitycode);
                Iterator itr1 = emaildetails.iterator();
                OperationTO opTO = new OperationTO();
                if (itr1.hasNext()) {
                    opTO = new OperationTO();
                    opTO = (OperationTO) itr1.next();
                    smtp = opTO.getSmtp();
                    emailPort = Integer.parseInt(opTO.getPort());
                    frommailid = opTO.getEmailId();
                    password = opTO.getPassword();
                }



                ArrayList jobCardList = new ArrayList();
                jobCardList = operationBP.getClosedJobCardDetails(jobCardId);
                Iterator itr2 = jobCardList.iterator();
                String jobcardIssueDate = "";
                String serviceTypeName = "";
                String priorty = "";
                String mappedDriverName = "";
                String vehicleRequiredDate = "";
                int travelledKm = 0;
                String location = "";
                String vehicleNo = "";
                String empName = "";
                String vehicleId = "";
                String jobcardDays = "";
                String jobcardHours = "";
                String plannedCompleteDays = "";
                String plannedCompleteHours = "";
                String actualCompletionDate = "";
                String closedBy = "";
                OperationTO opTO2 = new OperationTO();
                while (itr2.hasNext()) {
                    opTO2 = new OperationTO();
                    opTO2 = (OperationTO) itr2.next();
                    jobcardIssueDate = opTO2.getDate();
                    serviceTypeName = opTO2.getServicetypeName();
                    priorty = opTO2.getPriority1();
                    mappedDriverName = opTO2.getDriverName();
                    vehicleRequiredDate = opTO2.getVehicleRequiredDate();
                    actualCompletionDate = opTO2.getActualCompletionDate();
                    travelledKm = opTO2.getKmReading();
                    location = opTO2.getServiceLocation();
                    empName = opTO2.getEmpName();
                    vehicleNo = opTO2.getRegNo();
                    vehicleId = opTO2.getVehicleid();
                    jobcardDays = opTO2.getJobcardDays();
                    jobcardHours = opTO2.getJobcardHours();
                    plannedCompleteDays = opTO2.getPlannedCompleteDays();
                    plannedCompleteHours = opTO2.getPlannedCompleteHours();
                    closedBy = opTO2.getClosedBy();
                }
                to = tripBP.getFCLeadMailId(vehicleId);
                 String email = operationBP.getJobcardEmail();
                String temp1[] = null;
                temp1 = email.split("~");
                String to1 = temp1[0];
                cc = temp1[1];

                String emailFormat = "";
                emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'>Team, Confirmation mail for jobcard closed for the vehicle : " + vehicleNo + "</th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Issue Date: </td><td>&nbsp;&nbsp;" + jobcardIssueDate + "</td></tr>"
                        + "<tr><td>&nbsp;Service Type Name:</td><td>&nbsp;&nbsp;" + serviceTypeName + "</td></tr>"
                        + "<tr><td>&nbsp;Service Location:</td><td>&nbsp;&nbsp;" + location + "</td></tr>"
                        + "<tr><td>&nbsp;Priority:</td><td>&nbsp;&nbsp;" + priorty + "</td></tr>"
                        + "<tr><td>&nbsp;Driver Name:</td><td>&nbsp;&nbsp;" + mappedDriverName + "</td></tr>"
                        + "<tr><td>&nbsp;Km Reading:</td><td>&nbsp;&nbsp;" + travelledKm + "</td></tr>"
                        + "<tr><td>&nbsp;Planned Completion Date:</td><td>&nbsp;&nbsp;" + vehicleRequiredDate + "</td></tr>"
                        + "<tr><td>&nbsp;Actual Completion Date:</td><td>&nbsp;&nbsp;" + actualCompletionDate + "</td></tr>"
                        + "<tr><td>&nbsp;Created By:</td><td>&nbsp;&nbsp;" + empName + "</td></tr>"
                        + "<tr><td>&nbsp;Closed By:</td><td>&nbsp;&nbsp;" + closedBy + "</td></tr>"
                        + "<tr><td>&nbsp;Remarks:</td><td>&nbsp;&nbsp;" + remarks + "</td></tr>"
                        + "<tr><td colspan='2' align='center'>"
                        + "</td></tr>"
                        + "</table></body></html>";
                String subject = "Jobcrad Closed For Vehicle No: " + vehicleNo;
                String content = emailFormat;
//                String email = operationBP.getJobcardEmail();
//                String temp1[] = null;
//                temp1 = email.split("~");
//                String to1 = temp1[0];
//                cc = temp1[1];

                int mailSendingId = 0;
                TripTO tripTO = new TripTO();
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(to);
                tripTO.setMailIdCc(cc);
                tripTO.setMailIdBcc("");
//                mailSendingId = tripBP.insertMailDetails(tripTO, userId);
                //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, to + "," + to1, cc, userId).start();




                */


                //mv = handleCloseJobCardView(request, reponse, command);
                //////System.out.println("srini here,,,,,,1");
                mv = viewJobCardsForBilling(request, reponse, command);
                //////System.out.println("srini here,,,,,,2");
//            }

        } catch (FPBusinessException exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleJobCardBillStore(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        ModelAndView mv = null;
        menuPath = "Render Service >>Generate Bill";
        String pageTitle = "Generate Bill";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //String[] selectedIndex=request.getParameterValues("selectedindex");
            //String[] problemId=request.getParameterValues("qty");
            //String[] activityId=request.getParameterValues("option");

//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                int userId = (Integer) session.getAttribute("userId");
                String spares = command.getSpares();
                String labour = command.getLabour();
                String total = command.getTotal();
                String discount = command.getDiscount();
                String nett = command.getNett();
                String sparesPercent = command.getSparesPercent();
                String labourPercent = command.getLabourPercent();
                String bodyRepairTotal = command.getBodyRepairTotal();
                String invoiceType = command.getInVoiceType();
                String date = command.getDate();
                String consumablesTotal = request.getParameter("consumablesTotal");
//                bala
                String labourHike = command.getLaborhike();
//                bala ends

                String matlMargin = request.getParameter("matlMargin");
                String matlMarginPercent = request.getParameter("matlMarginPercent");
                String laborMargin = request.getParameter("laborMargin");
                String laborMarginPercent = request.getParameter("laborMarginPercent");
                String nettMargin = request.getParameter("nettMargin");
                String nettMarginPercent = request.getParameter("nettMarginPercent");
                String purchaseSpares = request.getParameter("purchaseSpares");
                String labourExpense = request.getParameter("labourExpense");
                String jobCost = request.getParameter("jobCost");

                //////System.out.println("before vals:" + matlMargin + ":" + matlMarginPercent + ":" + laborMargin + ":" + laborMarginPercent
//                        + ":" + "nettMargin" + nettMargin + "nettMarginPercent" + nettMarginPercent);
                //////System.out.println("before vals:" + purchaseSpares + ":" + labourExpense + ":" + jobCost + ":" + jobCost
//                        + ":" + "nettMargin" + nettMargin + "nettMarginPercent" + nettMarginPercent);

                //////System.out.println("before vals:" + spares + ":" + labour + ":" + discount + ":" + bodyRepairTotal + ":" + "invoiceType" + invoiceType + "Date in Render" + date);
                if ("".equals(spares)) {
                    spares = "0.00";
                }
                if ("".equals(labour)) {
                    labour = "0.00";
                }
                if ("".equals(discount)) {
                    discount = "0.00";
                }
                if ("".equals(bodyRepairTotal)) {
                    bodyRepairTotal = "0.00";
                }
                //////System.out.println("after vals:" + spares + ":" + labour + ":" + discount + ":" + bodyRepairTotal + ":" + "invoiceType" + invoiceType);
                String[] activityId = command.getActivityId();
                String[] activityAmount = command.getActivityAmount();
                String[] billNo = command.getBillNo();
                String[] tax = command.getTax();

                String[] itemId = command.getItemId();
                String[] quantity = command.getQuantity();
                String[] qty = command.getQty();
                String[] price = command.getPrice();
                String[] lineItemAmount = command.getLineItemAmount();


                int jobCardId = Integer.parseInt(command.getJobCardId());
                int billNumber = 0;

                billNumber = renderServiceBP.doJobCardBilling(userId, jobCardId, itemId, quantity, tax, price, lineItemAmount,
                        activityId, qty, activityAmount, billNo, spares, labour, total, discount, nett,
                        sparesPercent, labourPercent, bodyRepairTotal, invoiceType, labourHike, date,
                        matlMargin, matlMarginPercent, laborMargin, laborMarginPercent, nettMargin, nettMarginPercent,
                        purchaseSpares, labourExpense, jobCost, consumablesTotal);
                System.out.println("billNumber:"+billNumber);
                command.setBillNumber(String.valueOf(billNumber));
                
                //mv = viewBillDetails(request, reponse, command);
                mv = viewJobCardsBillDetails(request, reponse, command);


//            }

        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return mv;
    }

    public ModelAndView handleExtJobCardBillStore(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        ModelAndView mv = null;
        menuPath = "Render Service >>Generate Bill";
        String pageTitle = "Generate Bill";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {


//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                int userId = (Integer) session.getAttribute("userId");

                String jobCardId = request.getParameter("jobCardId");
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceRemarks = request.getParameter("invoiceRemarks");
                String invoiceDate = request.getParameter("invoiceDate");
                String sparesAmt = request.getParameter("sparesAmt");
                String sparesRemarks = request.getParameter("sparesRemarks");
                String consumableAmt = request.getParameter("consumableAmt");
                String consumableRemarks = request.getParameter("consumableRemarks");
                String laborAmt = request.getParameter("laborAmt");
                String laborRemarks = request.getParameter("laborRemarks");
                String othersAmt = request.getParameter("othersAmt");
                String othersRemarks = request.getParameter("othersRemarks");
                String totalAmt = request.getParameter("totalAmt");
                String vatPercent = request.getParameter("vatPercent");
                String vatAmt = request.getParameter("vatAmt");
                String serviceTaxPercent = request.getParameter("serviceTaxPercent");
                String serviceTaxAmt = request.getParameter("serviceTaxAmt");

                int billNo = renderServiceBP.saveExtJobCardBill(userId, jobCardId, invoiceNo, invoiceRemarks, invoiceDate, sparesAmt, sparesRemarks,
                        consumableAmt, consumableRemarks, laborAmt, laborRemarks, othersAmt, othersRemarks, totalAmt, vatPercent,
                        vatAmt, serviceTaxPercent, serviceTaxAmt);
                //////System.out.println("billNO:" + billNo);
                command.setBillNumber(String.valueOf(billNo));
                mv = viewExtBillDetails(request, reponse, command);

//            }

        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return mv;
    }

    public ModelAndView handleExtJobCardBillStoreNew(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        ModelAndView mv = null;
        menuPath = "Render Service >>Generate Bill";
        String pageTitle = "Generate Bill";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {


//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                int userId = (Integer) session.getAttribute("userId");

                String jobCardId = request.getParameter("jobCardId");
                String invoiceNo = request.getParameter("invoiceNo");
                String invoiceRemarks = request.getParameter("invoiceRemarks");
                String invoiceDate = request.getParameter("invoiceDate");
//                String sparesAmt = request.getParameter("sparesAmt");
//                String sparesRemarks = request.getParameter("sparesRemarks");
//                String consumableAmt = request.getParameter("consumableAmt");
//                String consumableRemarks = request.getParameter("consumableRemarks");
//                String laborAmt = request.getParameter("laborAmt");
//                String laborRemarks = request.getParameter("laborRemarks");
//                String othersAmt = request.getParameter("othersAmt");
//                String othersRemarks = request.getParameter("othersRemarks");
//                String totalAmt = request.getParameter("totalAmt");
//                String vatPercent = request.getParameter("vatPercent");
//                String vatAmt = request.getParameter("vatAmt");
//                String serviceTaxPercent = request.getParameter("serviceTaxPercent");
//                String serviceTaxAmt = request.getParameter("serviceTaxAmt");

                int billNo = renderServiceBP.saveExtJobCardBillNew(userId, jobCardId, invoiceNo, invoiceRemarks, invoiceDate);
                //////System.out.println("billNO:" + billNo);
                command.setBillNumber(String.valueOf(billNo));
                mv = viewExtBillDetails(request, reponse, command);

//            }

        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return mv;
    }

    public ModelAndView viewJobCardsForBilling(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath =
                "Render Service >> Job Cards for Billing";
        String pageTitle = "Generate Bill";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        RenderServiceTO closeJcListTO = new RenderServiceTO();
        ArrayList servicePoints = new ArrayList();
        try {

//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/commoclosedJobCardBillDetailsn/NotAuthorized.jsp";
//            } else {
               // setLocale(request, reponse);
                path = "content/RenderService/ViewJobCardForBilling.jsp";
                ArrayList jcList = new ArrayList();
                String regNo = request.getParameter("regNo");
                String jcId = request.getParameter("jobcardId");
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                PaginationHelper pagenation = new PaginationHelper();
                int startIndex = 0;
                int endIndex = 0;
                if (regNo != null) {
                    closeJcListTO.setRegNo(regNo);
                }
                if (jcId != null) {
                    closeJcListTO.setJcId(jcId);
                }
                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);
                closeJcListTO.setCompId(request.getParameter("serviceLocation"));

                jcList = renderServiceBP.getClosedJobCardForBill(closeJcListTO);
                totalRecords = jcList.size();
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                }
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                //////System.out.println("pageNo" + pageNo);
                request.setAttribute("totalPages", totalPages);
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();


                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);
                jcList = renderServiceBP.getClosedJobCardForBill(closeJcListTO);
                servicePoints = operationBP.getServicePoints();
                request.setAttribute("servicePoints", servicePoints);
                if (jcList.size() > 0) {
                    request.setAttribute("jcList", jcList);
                }
                request.setAttribute("regNo", regNo);
                request.setAttribute("jcId", jcId);
                request.setAttribute("compId", request.getParameter("serviceLocation"));

//            }
            //////System.out.println("Last Execution");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }



    public ModelAndView viewExtJobCardsBillDetails(
            HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath =
                "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                 // Path Has been changed for new functionality
//                path = "content/RenderService/ClosedExtJobCardBillDetails.jsp";
                path = "content/RenderService/billedForClosedJobcardDetails.jsp";
                ArrayList groupList = new ArrayList();
                ArrayList serviceList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                groupList = renderServiceBP.getGroupList();
                request.setAttribute("groupList", groupList);
                serviceList = renderServiceBP.getServiceList();
                request.setAttribute("serviceList", serviceList);

                ArrayList jcList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getJobCardsBillDetails(Integer.parseInt(command.getJobCardId()));
                request.setAttribute("jcList", jcList);
                request.setAttribute("vehicleId", request.getParameter("vehicleId"));
                String laborExpense = renderServiceBP.getActualLaborExpense(command.getJobCardId());
                request.setAttribute("laborExpense", laborExpense);

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewBillDetails(
            HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList billHeaderInfo = new ArrayList();
        ArrayList billItems = new ArrayList();
        ArrayList billTaxDetails = new ArrayList();
        ArrayList billActivities = new ArrayList();
        ArrayList billRecord = new ArrayList();
        ArrayList companyList = new ArrayList();
        String path = "";
        String vendorNames = "";
        String menuPath = "";
        JobCardItemTO itemTO = new JobCardItemTO();
        menuPath =
                "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/report/jcBill.jsp";
              System.out.println("For Hari" + command.getBillNumber());
                billHeaderInfo = renderServiceBP.getBillHeaderInfo(Integer.parseInt(command.getBillNumber()));
               System.out.println("1");
                billItems = renderServiceBP.getBillItemList(Integer.parseInt(command.getBillNumber()));
              System.out.println("2");
                billActivities = renderServiceBP.getBillLaborCharge(Integer.parseInt(command.getBillNumber()));
               System.out.println("3");
                billTaxDetails = renderServiceBP.getBillTax(Integer.parseInt(command.getBillNumber()));
               System.out.println("4");
                String billServiceTaxDetails = renderServiceBP.getBillServiceTax(Integer.parseInt(command.getBillNumber()));
                String billServiceTaxPercent = renderServiceBP.getBillServiceTaxPercent(Integer.parseInt(command.getBillNumber()));
                System.out.println("5");
                vendorNames = renderServiceBP.getContractVendors(Integer.parseInt(command.getBillNumber()));
                System.out.println("6");
                billRecord.addAll(billItems);
                billRecord.addAll(billTaxDetails);
                billRecord.addAll(billActivities);


                if (!vendorNames.equals("")) {
                    request.setAttribute("vendorNames", vendorNames);
                }
                request.setAttribute("billHeaderInfo", billHeaderInfo);
                request.setAttribute("billItems", billItems);
                request.setAttribute("billActivities", billActivities);
                request.setAttribute("billTaxDetails", billTaxDetails);
                request.setAttribute("billRecord", billRecord);
                request.setAttribute("itemSize", billItems.size());
                request.setAttribute("taxSize", billTaxDetails.size());
                request.setAttribute("laborSize", billActivities.size());
                request.setAttribute("billServiceTaxDetails", billServiceTaxDetails);
                request.setAttribute("billServiceTaxPercent", billServiceTaxPercent);
                //////System.out.println("7:" + billServiceTaxDetails);

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView viewExtBillDetails(
            HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Billing";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/RenderService/viewExtJobCardBillDetails.jsp";
                //////System.out.println("For Hari:" + command.getBillNumber());
                String billNo = command.getBillNumber();
                ArrayList extJobCardBillDetails = renderServiceBP.getExtJobCardBillDetails(billNo);
                request.setAttribute("extJobCardBillDetails", extJobCardBillDetails);
//            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewBillDetails1(
            HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        ArrayList billIds = new ArrayList();
        RenderServiceTO renderServiceTO = null;
        String path = "";
        String vendorNames = "";
        String menuPath = "";
        JobCardItemTO itemTO = new JobCardItemTO();
        int sbillId = 8290;
        int ebillId = 8292;
        menuPath =
                "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
                path = "content/report/jcBillNew.jsp";

                ArrayList billHeaderInfo = null;
                ArrayList billItems = null;
                ArrayList billTaxDetails = null;
                ArrayList billActivities = null;
                ArrayList billRecord = null;
                while (sbillId <= ebillId) {
                    billHeaderInfo = new ArrayList();
                    billItems = new ArrayList();
                    billTaxDetails = new ArrayList();
                    billActivities = new ArrayList();
                    billRecord = new ArrayList();
                    renderServiceTO = new RenderServiceTO();
                    //////System.out.println("========================= start====================================");
                    billHeaderInfo = renderServiceBP.getBillHeaderInfo(sbillId);

                    billItems = renderServiceBP.getBillItemList(sbillId);

                    billActivities = renderServiceBP.getBillLaborCharge(sbillId);

                    billTaxDetails = renderServiceBP.getBillTax(sbillId);

                    vendorNames = renderServiceBP.getContractVendors(sbillId);



                    billRecord.addAll(billItems);
                    billRecord.addAll(billTaxDetails);
                    billRecord.addAll(billActivities);
                    //////System.out.println("billRecord size " + billRecord.size());
                    renderServiceTO.setBillRecord(billRecord);
                    renderServiceTO.setBillHeaderInfo(billHeaderInfo);
                    renderServiceTO.setBillItems(billItems);
                    renderServiceTO.setBillActivities(billActivities);
                    renderServiceTO.setBillTaxDetails(billTaxDetails);
                    renderServiceTO.setVendorNames(vendorNames);
                    billIds.add(renderServiceTO);
                    //////System.out.println("============================= End====================================");
                    sbillId++;

                }

                //////System.out.println("billIds size" + billIds.size());
                request.setAttribute("billIds", billIds);



        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public void getActivityDetails(HttpServletRequest request, HttpServletResponse response, RenderServiceCommand command) throws IOException, FPBusinessException {
        //////System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String acode = request.getParameter("acode");
        String modelId = request.getParameter("modelId");
        String mfrId = request.getParameter("mfrId");
        //////System.out.println("acode:" + acode + "model:" + modelId + "mfrId:" + mfrId);
        String suggestions = renderServiceBP.getActivityDetails(acode, modelId, mfrId);
        //////System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }


        /**
     * This method used to View Standard Charges
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewSubGroup(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        //////System.out.println("standardCharge...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        renderServiceCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int standardChargesMaster = 0;
        OperationTO operationTO = new OperationTO();
        String menuPath = "";
        menuPath = "Operation  >> View Bill SubGroup";
        String pageTitle = "viewSubGroup ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int billGroupId = 0;
        try {
            ArrayList groupList = new ArrayList();
            groupList = renderServiceBP.getGroupList();
            request.setAttribute("groupList", groupList);
            ArrayList subGroupList = new ArrayList();
            subGroupList = renderServiceBP.getSubGroupList(billGroupId);
            request.setAttribute("subGroupList", subGroupList);
            request.setAttribute("subGroupListSize", subGroupList.size());
            path = "content/RenderService/subGroupMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to getSubGroupList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    /**
     * This method used to Save Standard Charges & Update Standard Charges save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveSubGroup(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {

        //////System.out.println("standardCharge...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        renderServiceCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        int updateStatus = 0;
        RenderServiceTO renderServiceTO = new RenderServiceTO();
        String menuPath = "";
        menuPath = "Operation  >> View Bill SubGroup";
        String pageTitle = "Save Bill SubGroup ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList groupList = new ArrayList();
            ArrayList subGroupList = new ArrayList();
            if (renderServiceCommand.getBillSubGroupName() != null && !"".equals(renderServiceCommand.getBillSubGroupName())) {
                renderServiceTO.setBillSubGroupName(renderServiceCommand.getBillSubGroupName());
            }
            if (renderServiceCommand.getBillSubGroupDesc() != null && !"".equals(renderServiceCommand.getBillSubGroupDesc())) {
                renderServiceTO.setBillSubGroupDesc(renderServiceCommand.getBillSubGroupDesc());
            }
            if (renderServiceCommand.getBillGroupId() != null && !"".equals(renderServiceCommand.getBillGroupId())) {
                renderServiceTO.setBillGroupId(renderServiceCommand.getBillGroupId());
            }
            if (renderServiceCommand.getActiveInd() != null && !"".equals(renderServiceCommand.getActiveInd())) {
                renderServiceTO.setActiveInd(renderServiceCommand.getActiveInd());
            }
            //////System.out.println("operationCommand.getStChargeId() = " + renderServiceCommand.getBillSubGroupId());
            if (renderServiceCommand.getBillSubGroupId() != null && !"".equals(renderServiceCommand.getBillSubGroupId())) {
                renderServiceTO.setBillSubGroupId(renderServiceCommand.getBillSubGroupId());
                updateStatus = renderServiceBP.updateSubGroup(renderServiceTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");
            }

            String checkStandardChargeName = "";
            checkStandardChargeName = renderServiceBP.checkSubGroupName(renderServiceTO);
            if (checkStandardChargeName == null) {
                groupList = renderServiceBP.getGroupList();
                request.setAttribute("groupList", groupList);
                insertStatus = renderServiceBP.saveSubGroupName(renderServiceTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted SucessFully");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");
            }
            int billGroupId = 0;
            if (insertStatus != 0 || updateStatus != 0) {
                groupList = renderServiceBP.getGroupList();
                request.setAttribute("groupList", groupList);
                subGroupList = renderServiceBP.getSubGroupList(billGroupId);
                request.setAttribute("subGroupList", subGroupList);

            }
            groupList = renderServiceBP.getGroupList();
            request.setAttribute("groupList", groupList);
            subGroupList = renderServiceBP.getSubGroupList(billGroupId);
            request.setAttribute("subGroupList", subGroupList);
            request.setAttribute("subGroupListSize", subGroupList.size());
            path = "content/RenderService/subGroupMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

      public void checkBillSubGroupName(HttpServletRequest request, HttpServletResponse response, RenderServiceCommand command) throws IOException {
        renderServiceCommand = command;
        String suggestions = "";
        String billSubGroupName = request.getParameter("billSubGroupName");
        RenderServiceTO renderServiceTO = new RenderServiceTO();
        renderServiceTO.setBillSubGroupName(billSubGroupName);
        try {
            suggestions = renderServiceBP.checkSubGroupName(renderServiceTO);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }


      public void getBillSubGroupDetails(HttpServletRequest request, HttpServletResponse response, RenderServiceCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        RenderServiceTO renderServiceTO = null;
        ArrayList probDetails = new ArrayList();
        try {
            int billGroupId = 0;
            billGroupId = Integer.parseInt(request.getParameter("billGroupId"));
            //////System.out.println("billGroupId" + billGroupId);
            String returnValue = "";
            probDetails = renderServiceBP.getSubGroupList(billGroupId);
            Iterator itr = probDetails.iterator();
            while (itr.hasNext()) {
                renderServiceTO = (RenderServiceTO) itr.next();
                returnValue = returnValue + "," + renderServiceTO.getBillSubGroupDetails();
                //////System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to get problems-> " + exception);
            exception.printStackTrace();
        }

    }

      public void getVehicleList(HttpServletRequest request, HttpServletResponse response, RenderServiceCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        RenderServiceTO renderServiceTO = null;
        ArrayList vehicleList = new ArrayList();
        try {
            int vehicleTypeId = 0;
            vehicleTypeId = Integer.parseInt(request.getParameter("vehicleTypeId"));
            //////System.out.println("vehicleTypeId" + vehicleTypeId);
            String returnValue = "";
            vehicleList = renderServiceBP.getVehicleList(vehicleTypeId);
            Iterator itr = vehicleList.iterator();
            while (itr.hasNext()) {
                renderServiceTO = (RenderServiceTO) itr.next();
                returnValue = returnValue + "," + renderServiceTO.getVehicleDetails();
                //////System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to get problems-> " + exception);
            exception.printStackTrace();
        }

    }

       /**
     * This method used to Save Standard Charges & Update Standard Charges save
     * .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveJobCardBilldetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {

        //////System.out.println("standardCharge...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv= null;
        HttpSession session = request.getSession();
        renderServiceCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        int updateStatus = 0;
        RenderServiceTO renderServiceTO = new RenderServiceTO();
        String menuPath = "";
        menuPath = "Operation  >> jobcard Bill Details";
        String pageTitle = "Save Jobcard Bill ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            if (renderServiceCommand.getJobCardId() != null && !"".equals(renderServiceCommand.getJobCardId())) {
                renderServiceTO.setJobCardId(renderServiceCommand.getJobCardId());
            }
            if (renderServiceCommand.getInvoiceNo() != null && !"".equals(renderServiceCommand.getInvoiceNo())) {
                renderServiceTO.setInvoiceNo(renderServiceCommand.getInvoiceNo());
            }
            if (renderServiceCommand.getInvoiceRemarks() != null && !"".equals(renderServiceCommand.getInvoiceRemarks())) {
                renderServiceTO.setInvoiceRemarks(renderServiceCommand.getInvoiceRemarks());
            }
             if (renderServiceCommand.getLabourRemarks() != null && !"".equals(renderServiceCommand.getLabourRemarks())) {
                renderServiceTO.setLabourRemarks(renderServiceCommand.getLabourRemarks());
            }
            if (renderServiceCommand.getInvoiceAmount() != null && !"".equals(renderServiceCommand.getInvoiceAmount())) {
                renderServiceTO.setInvoiceAmount(renderServiceCommand.getInvoiceAmount());
            }
            if (renderServiceCommand.getInvoiceDate() != null && !"".equals(renderServiceCommand.getInvoiceDate())) {
                renderServiceTO.setInvoiceDate(renderServiceCommand.getInvoiceDate());
            }
            if (renderServiceCommand.getTotalCost() != null && !"".equals(renderServiceCommand.getTotalCost())) {
                renderServiceTO.setTotalCost(renderServiceCommand.getTotalCost());
            }
            if (renderServiceCommand.getTotalLabour() != null && !"".equals(renderServiceCommand.getTotalLabour())) {
                renderServiceTO.setTotalLabour(renderServiceCommand.getTotalLabour());
            }
            if (renderServiceCommand.getTotalAmount() != null && !"".equals(renderServiceCommand.getTotalAmount())) {
                renderServiceTO.setTotalAmount(renderServiceCommand.getTotalAmount());
            }
            if (renderServiceCommand.getVatAmount() != null && !"".equals(renderServiceCommand.getVatAmount())) {
                renderServiceTO.setVatAmount(renderServiceCommand.getVatAmount());
            }
            if (renderServiceCommand.getVatRemarks() != null && !"".equals(renderServiceCommand.getVatRemarks())) {
                renderServiceTO.setVatRemarks(renderServiceCommand.getVatRemarks());
            }
            if (renderServiceCommand.getServiceTaxAmount() != null && !"".equals(renderServiceCommand.getServiceTaxAmount())) {
                renderServiceTO.setServiceTaxAmount(renderServiceCommand.getServiceTaxAmount());
            }
            if (renderServiceCommand.getServiceTaxRemarks() != null && !"".equals(renderServiceCommand.getServiceTaxRemarks())) {
                renderServiceTO.setServiceTaxRemarks(renderServiceCommand.getServiceTaxRemarks());
            }
             if (renderServiceCommand.getQuantity() != null && !"".equals(renderServiceCommand.getQuantity())) {
                    renderServiceTO.setQuantity(renderServiceCommand.getQuantity());
            }

           String groupList[] = null;
           String subGroupList[] = null;
           String expenseRemarks[] = null;
           String cost[] = null;
           String labour[] = null;
           String total[] = null;
           String serviceList1[] = null;
           groupList = request.getParameterValues("groupList");
           subGroupList = request.getParameterValues("subGroupList");
           expenseRemarks = request.getParameterValues("expenseRemarks");
           cost = request.getParameterValues("cost");
           labour = request.getParameterValues("labour");
           total = request.getParameterValues("total");
           serviceList1 = request.getParameterValues("serviceList");
           renderServiceTO.setGroupList(groupList);
           renderServiceTO.setSubGroupList(subGroupList);
           renderServiceTO.setExpenseRemarks(expenseRemarks);
           renderServiceTO.setCost(cost);
           renderServiceTO.setLabour(labour);
           renderServiceTO.setTotal(total);
           renderServiceTO.setServiceList1(serviceList1);

           int insertStaus = 0;
           insertStatus = renderServiceBP.insertJobCardBillDetails(renderServiceTO,userId);
           mv = viewJobCardsForBilling(request, reponse, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewJobCardsBilling(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath ="Render Service >> Job Cards for Billing";
        String pageTitle = "Generate Bill";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        RenderServiceTO closeJcListTO = new RenderServiceTO();
        ArrayList servicePoints = new ArrayList();
        try {

               // setLocale(request, reponse);
                path = "content/RenderService/ViewJobCardBilling.jsp";
                ArrayList jcList = new ArrayList();
                String regNo = request.getParameter("regNo");
                String jcId = request.getParameter("jobcardId");
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                PaginationHelper pagenation = new PaginationHelper();
                int startIndex = 0;
                int endIndex = 0;
                if (regNo != null) {
                    closeJcListTO.setRegNo(regNo);
                }
                if (jcId != null) {
                    closeJcListTO.setJcId(jcId);
                }
                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);
                closeJcListTO.setCompId(request.getParameter("serviceLocation"));

                jcList = renderServiceBP.getBilledJobCardForView(closeJcListTO);
                totalRecords = jcList.size();
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                }
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                //////System.out.println("pageNo" + pageNo);
                request.setAttribute("totalPages", totalPages);
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();


                closeJcListTO.setStartIndex(startIndex);
                closeJcListTO.setEndIndex(endIndex);
                jcList = renderServiceBP.getBilledJobCardForView(closeJcListTO);
                servicePoints = operationBP.getServicePoints();
                request.setAttribute("servicePoints", servicePoints);
                String sessionCount = request.getParameter("sessionCount");
                if (jcList.size() > 0) {
                    request.setAttribute("jcList", jcList);
                }else if (jcList.size() == 0 && sessionCount != null){
                    request.setAttribute("sessionCount", sessionCount);
                }
                request.setAttribute("regNo", regNo);
                request.setAttribute("jcId", jcId);
                request.setAttribute("compId", request.getParameter("serviceLocation"));

            //////System.out.println("Last Execution");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception);
            request.setAttribute("errorMessage", exception.getErrorMessage());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView viewJobCardsBillDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath =
                "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
              //  setLocale(request, reponse);
                path = "content/RenderService/ClosedJobCardBillDetailsView.jsp";

                ArrayList jcList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getJobCardsBillDetails(Integer.parseInt(command.getJobCardId()));
                request.setAttribute("jcList", jcList);
                request.setAttribute("vehicleId", request.getParameter("vehicleId"));
                String laborExpense = renderServiceBP.getActualLaborExpense(command.getJobCardId());
                request.setAttribute("laborExpense", laborExpense);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView closedJobCardBillDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath =
                "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
              //  setLocale(request, reponse);
                path = "content/RenderService/ClosedJobCardBillDetails.jsp";

                ArrayList jcList = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getJobCardsBillDetails(Integer.parseInt(command.getJobCardId()));
                request.setAttribute("jcList", jcList);
                request.setAttribute("vehicleId", request.getParameter("vehicleId"));
                String laborExpense = renderServiceBP.getActualLaborExpense(command.getJobCardId());
                request.setAttribute("laborExpense", laborExpense);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    //srini commented on 12 may 15
/*
     public ModelAndView viewJobCardsBillDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >> Job Card Billing";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-Billing")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/RenderService/billedJobcardDetails.jsp";
                ArrayList jcList = new ArrayList();
                ArrayList billDetails = new ArrayList();
                ArrayList billGroupDetails = new ArrayList();
                //////System.out.println("jcid:" + Integer.parseInt(command.getJobCardId()));
                jcList = renderServiceBP.getJobCardsBillDetails(Integer.parseInt(command.getJobCardId()));
                billDetails = renderServiceBP.getBillDetails(Integer.parseInt(command.getJobCardId()));
                billGroupDetails = renderServiceBP.getBillGroupDetails(Integer.parseInt(command.getJobCardId()));
                //////System.out.println("bill Details:"+ billDetails.size());
                request.setAttribute("jcList", jcList);
                request.setAttribute("billDetails", billDetails);
                request.setAttribute("billGroupDetails", billGroupDetails);
                request.setAttribute("vehicleId", request.getParameter("vehicleId"));
                String laborExpense = renderServiceBP.getActualLaborExpense(command.getJobCardId());
                request.setAttribute("laborExpense", laborExpense);
            }

        } catch (FPRuntimeException exception) {
            
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
*///srini commented on 12 may 15
     public ModelAndView viewTyersDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "";
        menuPath = "Render Service >>Tyer Details";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

                path = "content/RenderService/tyersDetails.jsp";
                ArrayList jcList = new ArrayList();
                ArrayList billDetails = new ArrayList();
                ArrayList vehicleNos = new ArrayList();
                vehicleNos = operationBP.getVehicleRegNoForTyer();
            //////System.out.println("vehicleNos.size() = " + vehicleNos.size());
            request.setAttribute("vehicleNos",vehicleNos);



        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
       public ModelAndView handleTyerDetails(HttpServletRequest request, HttpServletResponse reponse, RenderServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
      ModelAndView mv =new ModelAndView();
        String menuPath = "";
        menuPath = "Render Service >> Enter Tyer Details";
        String pageTitle = "Job Card Closure";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        RenderServiceTO renderServiceTO = new RenderServiceTO();
        try {
             int vehicleId=Integer.parseInt(request.getParameter("vehicleId"));

                renderServiceTO.setVehicleId(vehicleId);
                //////System.out.println("vehicleid:"+renderServiceTO.getVehicleId());

                String OldTyerNo=request.getParameter("oldTyerNo");

                renderServiceTO.setOldTyerNo(OldTyerNo);
                //////System.out.println("OldTyerNo:"+renderServiceTO.getOldTyerNo());


                String newTyerNo=request.getParameter("newTyerNo");
                renderServiceTO.setNewTyerNo(newTyerNo);
                String tyerAmount=request.getParameter("tyerAmount");
                renderServiceTO.setTyerAmount(tyerAmount);
                //////System.out.println("tyerAmount:"+renderServiceTO.getTyerAmount());

                String OdometerReading=request.getParameter("odometerReading");
                renderServiceTO.setOdometerReading(OdometerReading);
                //////System.out.println("OdometerReading:"+renderServiceTO.getOdometerReading());


                String changeDate=request.getParameter("changeDate");
                renderServiceTO.setChangeDate(changeDate);
                //////System.out.println("ChangeDate:"+renderServiceTO.getChangeDate());


                String newTyerType=request.getParameter("newTyerType");
                //////System.out.println("NewTyerype1:"+newTyerType);
                renderServiceTO.setNewTyerype(newTyerType);
                //////System.out.println("NewTyerype:"+renderServiceTO.getNewTyerype());
                 String tyerCompanyName=request.getParameter("tyerCompanyName");
                renderServiceTO.setTyerCompanyName(tyerCompanyName);
                 //////System.out.println("TyerCompanyName:"+renderServiceTO.getTyerCompanyName());

                 String remarks=request.getParameter("remarks");
                renderServiceTO.setRemarks(remarks);
                 //////System.out.println("Remarks:"+renderServiceTO.getRemarks());
                int insertStatus = 0;
                insertStatus = renderServiceBP.insertTyerDetails(renderServiceTO,userId);
                path = "content/RenderService/tyersDetails.jsp";
                if(insertStatus!=0)
                {
                     request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tyer Detailed Entered  Successfully....");
                }
                mv=viewTyersDetails(request,reponse,command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view close job card queue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
       // return new ModelAndView(path);
        return mv;
    }
}
