<%-- 
    Document   : alterCountry
    Created on : 7 Nov, 2012, 7:43:12 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> CLPL </title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.alter.countryCode,'countryCode')){
            return;
        }
        if(textValidation(document.alter.countryName,'countryName')){
            return;
        }
        document.alter.action='/throttle/saveAlterCountry.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<c:if test="${countryAlterLists != null}">
    <c:forEach items="${countryAlterLists}" var="CL">
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit Country Master</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font> Name</td>
      <td class="text2"><input name="countryName" type="text" class="form-control" value="<c:out value="${CL.countryName}"/>" maxlength="10" size="20">
          <input type="hidden" name="countryID" value="<c:out value="${CL.countryID}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Code</td>
    <td class="text2"><input name="countryCode" type="text" class="form-control" value="<c:out value="${CL.countryCode}"/>" maxlength="15" size="20"></td>
  </tr>  
</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

