	<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>
  <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AddEmployee" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class="active"><spring:message code="hrms.label.ManageEmployee" text="default text"/></li>
          <li class="active"><spring:message code="hrms.label.AddEmployee" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">

	<script type="text/javascript">
	    $(document).ready(function() {
		$("#datepicker").datepicker({
		    showOn: "button",
		    buttonImage: "calendar.gif",
		    buttonImageOnly: true

		});
	    });

	    $(function() {
		//alert("cv");
		$(".datepicker").datepicker({
		    /*altField: "#alternate",
		     altFormat: "DD, d MM, yy"*/
		    dateFormat: 'dd-mm-yy',
		    yearRange: '1900:' + new Date().getFullYear(),
		    changeMonth: true, changeYear: true
		});
	    });

	</script>
	<script  type="text/javascript" src="js/jq-ac-script.js"></script>

	<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
		$("#tabs").tabs();
	    });
	</script>

	<html>
	    <head>
		<%@page language="java" contentType="text/html; charset=UTF-8"%>
	<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<%@page import="java.util.Locale"%>
		<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
		<title>JSP Page</title>
		<script type="text/javascript">
		    $(document).ready(function() {
			//alert('hai');
			$("#datepicker").datepicker({
			    showOn: "button",
			    buttonImage: "calendar.gif",
			    buttonImageOnly: true

			});



		    });

		    $(function() {
			//	alert("cv");
			$(".datepicker").datepicker({
			    /*altField: "#alternate",
			     altFormat: "DD, d MM, yy"*/
			    dateFormat: 'dd-mm-yy',
			    yearRange: '1900:2030', changeMonth: true, changeYear: true
			});

		    });

		    /*
		     $( ".datepicker" ).datepicker({changeMonth: true,changeYear: true, minDate: 0, maxDate: "-20Y +10D" });

		     } */
		</script>

	<script>
		   function changePageLanguage(langSelection){
		   if(langSelection== 'ar'){
		   document.getElementById("pAlign").style.direction="rtl";
		   }else if(langSelection== 'en'){
		   document.getElementById("pAlign").style.direction="ltr";
		   }
		   }
		 </script>

		  <c:if test="${jcList != null}">
		  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
		  </c:if>

<!--		  <span style="float: right">
			<a href="?paramName=en">English</a>
			|
			<a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
		  </span>-->

		<script language="javascript">
		    function copyAddress(value)
		    {
			if (value == true) {
			    var addAddr;
			    var addCity;
			    var addState;
			    var pinCode;

			    addAddr = document.addLect.addr.value;
			    addCity = document.addLect.city.value;
			    addState = document.addLect.state.value;
			    pinCode = document.addLect.pincode.value;

			    document.addLect.addr1.value = addAddr;
			    document.addLect.city1.value = addCity;
			    document.addLect.state1.value = addState;
			    document.addLect.pincode1.value = pinCode;
			}
			else
			{
			    document.addLect.addr1.value = "";
			    document.addLect.city1.value = "";
			    document.addLect.state1.value = "";


			}
		    }
		    function checkPassword(value) {

			var error = "";
			var illegalChars = /[\W_]/; // allow only letters and numbers
			if ((value.length < 6)) {
			    alert("Enter atleast 6 Characters");
			    document.addLect.password.focus();
			}
			else if (illegalChars.test(value)) {
			    alert("The password contains illegal characters");
			    document.addLect.password.focus();
			}
			return false;
		    }


		    function isEmail(s)
		    {

			if (/[^@]+@[^@]+\.(com)|(co.in)|(in)$/.test(s))
			    return false;
			alert("Email not in valid form!");
			return true;
		    }




		    function isChar(s) {
			if (!(/^-?\d+$/.test(s))) {
			    return false;
			}
			return true;
		    }
		    //Grade Selection




		    function fillSelect(desigId) {
			var url = '/throttle/getGradeForDesig.do?desigId=' + desigId;

			if (window.ActiveXObject)
			{
			    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else if (window.XMLHttpRequest)
			{
			    httpRequest = new XMLHttpRequest();
			}

			httpRequest.open("POST", url, true);
			httpRequest.onreadystatechange = function() {
			    go();
			};
			httpRequest.send(null);
		    }

		    function go() {

			if (httpRequest.readyState == 4)
			{
			    if (httpRequest.status == 200)
			    {
				if (httpRequest.responseText.valueOf() != "") {
				    var options = httpRequest.responseText.valueOf();
				    options = options.split(',');
				    setOptions(options);
				    //document.payVendorCredit.validation.value="submit";
				    //document.getElementById("messag").innerHTML="";

				} else {
				    //document.getElementById("messag").innerHTML="Vendor Data Does not Exists";
				    //document.payVendorCredit.validation.value="dontSubmit";
				    //document.payVendorCredit.vendor_data.select();
				    //document.payVendorCredit.vendor_data.focus();
				}
			    }
			    else
			    {
				alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
			    }
			}
		    }


		    function setOptions(val)
		    {
			var gradeId = document.addLect.gradeId;
			var id = '';
			var name = '';

			gradeId.options.length = 1;
			/*for(var i=0;i<options.length;i++){
			 alert(options[i]);
			 }   */
			for (var i = 0; i < val.length; i++) {
			    if (val[i] != "") {
				id = val[i].split('-');
				name = id[1];
				option1 = new Option(name, id[0]);
				gradeId.options[i] = option1;
			    }
			}
		    }

		    function initCs() {

			var desig = document.getElementById('desigId').value;
			if (desig != "") {
			    var list = document.getElementById("gradeId");
			    while (list.childNodes[0]) {
				list.removeChild(list.childNodes[0])
			    }
			    fillSelect(desig);
			}

			//fillSelect(document.getElementById('desigId').value);
		    }

		    //window.onload=initCs;


		    //Ajax to Check userName
		    var httpRequest;
		    function getProfile(appUserName)
		    {
			var appUserCode = document.addLect.appUserCode.value;
			var userName = appUserCode + appUserName;
			document.addLect.userName.value = userName;
			var url = '/throttle/checkUserName.do?userName=' + userName;
			if (window.ActiveXObject)
			{
			    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else if (window.XMLHttpRequest)
			{
			    httpRequest = new XMLHttpRequest();
			}

			httpRequest.open("GET", url, true);

			httpRequest.onreadystatechange = function() {
			    processRequest();
			};

			httpRequest.send(null);
		    }


		    function processRequest()
		    {
			if (httpRequest.readyState == 4)
			{
			    if (httpRequest.status == 200)
			    {
				if (trim(httpRequest.responseText) != "")
				{
				    document.getElementById("userNameStatus").innerHTML = httpRequest.responseText;
				    document.addLect.password.disabled = true;
				    document.addLect.password.disabled = true;
				    document.addLect.userName.select();
				} else {
				    document.getElementById("userNameStatus").innerHTML = "";
				    document.addLect.password.disabled = false;
				    document.addLect.password.focus();
				}
			    }
			    else
			    {
				alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
			    }
			}
		    }


		    function submitPage(value) {
	//                alert(value);
			var table = document.getElementById("allowanceTBL");
			var rowCount = parseInt(table.rows.length) - parseInt(1);
			document.getElementById("empRelationSize").value = rowCount;
			if (value == 'modify') {
			    document.addLect.action = '/throttle/searchAlterEmp.do';
			    document.addLect.submit();
			    return;
			}
			if (isEmpty(document.addLect.empCode.value)) {
			    alert("Employee Code Field Should Not Be Empty");
			    document.addLect.empCode.focus();
			    return;
			} else if (isEmpty(document.addLect.name.value)) {
			    alert("Employee Name Field Should Not Be Empty");
			    document.addLect.name.focus();
			    return;
			} else if (isChar(document.addLect.name.value)) {
			    alert("Enter Valid Character for Employee Name ");
			    document.addLect.name.focus();
			    return;
			} else if (isEmpty(document.addLect.dateOfBirth.value)) {
			    alert("Date of Birth Field Should Not Be Empty");
			    document.addLect.dateOfBirth.focus();
			    return;
			} else if (isEmpty(document.addLect.dateOfJoining.value)) {
			    alert("Date Of Joining Field Should Not Be Empty");
			    document.addLect.dateOfJoining.focus();
			    return;
	//                } else if (isEmpty(document.addLect.bloodGrp.value)) {
	//                    alert("Blood Group Field Should Not Be Empty");
	//                    document.addLect.bloodGrp.focus();
	//                    return;
			} else if (isEmpty(document.addLect.fatherName.value)) {
			    alert("Father Field Should Not Be Empty");
			    document.addLect.fatherName.focus();
			    return;
			} else if (isEmpty(document.addLect.mobile.value)) {
			    alert("Mobile Should Not Be Empty");
			    document.addLect.mobile.focus();
			    return;
			} else if (isEmpty(document.addLect.qualification.value)) {
			    alert("Qualification Field Should Not Be Empty");
			    document.addLect.qualification.focus();
			    return;
			} else if (isChar(document.addLect.qualification.value)) {
			    alert("Enter Valid Character for Qualification ");
			    document.addLect.qualification.focus();
			    return;
			} else if ((document.addLect.mobile.value != "") && isDigit(document.addLect.mobile.value)) {
			    alert("Mobile Field Should Be Digit");
			    document.addLect.mobile.focus();
			    return;
			} else if (document.addLect.deptid.value == '0') {
			    alert("Please select department");
			    document.addLect.deptid.focus();
			    return;
			} else if (document.addLect.designationId.value == '0') {
			    alert("Please select designation");
			    document.addLect.designationId.focus();
			    return;
			} else if (document.addLect.gradeId.value == '0') {
			    alert("Please select grade");
			    document.addLect.gradeId.focus();
			    return;
			} else if (document.addLect.cmpId.value == '0') {
			    alert("Please select company");
			    document.addLect.cmpId.focus();
			    return;
	//                } else if (isEmpty(document.addLect.phone.value)) {
	//                    alert("Phone No Field Should Be Digit");
	//                    document.addLect.phone.focus();
	//                    return;
	//                } else if ((document.addLect.email.value != "") && isEmail(document.addLect.email.value)) {
	//                    alert("Please Enter Valid email Address");
	//                    document.addLect.email.focus();
	//                    return;
	//                } else if (isEmpty(document.addLect.addr.value)) {
	//                    alert("Present Address Field Should Not Be Empty");
	//                    document.addLect.addr.focus();
	//                    return;
	//                } else if (isEmpty(document.addLect.city.value)) {
	//                    alert("Present City Field Should Not Be Empty");
	//                    document.addLect.city.focus();
	//                    return;
	//                } else if (isEmpty(document.addLect.state.value)) {
	//                    alert("Present State Field Should Not Be Empty");
	//                    document.addLect.state.focus();
	//                    return;
	//                } else if (isEmpty(document.addLect.pincode.value)) {
	//                    alert("Present Pincode Field Should Not Be Empty");
	//                    document.addLect.pincode.focus();
	//                    return;
			}
	//                if (document.getElementById("contractDriver").value == "NO") {
	//                    if (isEmpty(document.addLect.addr1.value)) {
	//                        alert("Permanant Address Field Should Not Be Empty");
	//                        document.addLect.addr1.focus();
	//                        return;
	//                    } else if (isEmpty(document.addLect.city1.value)) {
	//                        alert("Permanant City Field Should Not Be Empty");
	//                        document.addLect.city1.focus();
	//                        return;
	//                    } else if (isEmpty(document.addLect.state1.value)) {
	//                        alert("Permanant State Field Should Not Be Empty");
	//                        document.addLect.state1.focus();
	//                        return;
	//                    } else if (isEmpty(document.addLect.pincode1.value)) {
	//                        alert("Permanant Pincode Field Should Not Be Empty");
	//                        document.addLect.pincode1.focus();
	//                        return;
	//                    } else if (document.addLect.deptid.value == "0" && document.addLect.deptid.value == null) {
	//                        alert("Select any one Department");
	//                        document.addLect.deptid.focus();
	//                        return;
	//                    } else if (document.addLect.designationId.value == "0") {
	//                        alert("Select any one Designation");
	//                        document.addLect.designationId.focus();
	//                        return;
	//                    } else if (document.addLect.gradeId.value == "0") {
	//                        alert("Select any one Grade");
	//                        document.addLect.gradeId.focus();
	//                        return;
	//                    }
	//                }
			if (document.addLect.userStatus.checked == true) {
			    if (isEmpty(document.addLect.userName.value)) {
				alert("User Name Field Should Not Be Empty");
				document.addLect.userName.focus();
				return;
			    }
			    else if (!isName(document.addLect.appUserName.value)) {
				alert("Enter Valid Character for User Name ");
				document.addLect.appUserName.focus();
				return;
			    }
			    else if (isEmpty(document.addLect.password.value)) {
				alert("Please Enter Password");
				document.addLect.password.focus();
				return;
			    } else if (document.addLect.roleId.value == "0") {
				alert("Please Select Employee Role ");
				document.addLect.roleId.focus();
				return;
			    }
	//                    else if (document.addLect.branchId.value == "0") {
	//                        alert("Please Select Branch Name ");
	//                        document.addLect.branchId.focus();
	//                        return;
	//                    }
	//                    else if (document.addLect.companyId.value == "0") {
	//                        alert("Please Select User Company Name ");
	//                        document.addLect.companyId.focus();
	//                        return;
	//                    }
	//                    statusTrue = checkedValue[i].value + "~Y";
	//                    document.getElementById("userStatus1").value = 1;

			}
			if (value == 'alter') {
			    document.addLect.action = '/throttle/alterEmployee.do';
			    document.addLect.submit();
			} else if (value == 'Save') {
			    document.addLect.action = '/throttle/addEmployee.do';
			    document.addLect.submit();
			}


		    }




		    function viewPage()
		    {
			document.addLect.action = '/throttle/EmpViewSearchPage.do';
			document.addLect.submit();
		    }
		</script>
	    </head>
	    <body onLoad="document.addLect.name.focus();
		    setImages(0, 1, 0, 0, 0, 0);">
		<form name="addLect" method="post" enctype="multipart/form-data">
                  
		    <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
			<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
			    <!-- pointer table -->
			    <table class="table table-info mb30 table-hover" >
				<tr>
				    <td >
					<%--<%@ include file="/content/common/path.jsp" %>--%>
                                         
				    </td></tr></table>
			</div>
		    </div>
		    <div id="tabs">
			<ul class="nav nav-tabs">
			    <li class="active"><a href="#PersonalDetails" data-toggle="tab"><span><spring:message code="hrms.label.EnrollmentDetails" text="default text"/></span></a></li>
			    <li><a href="#OfficialDetails" data-toggle="tab"><span><spring:message code="hrms.label.AddressDetails" text="default text"/> </span></a></li>
			    <li><a href="#Experience" data-toggle="tab"><span> <spring:message code="hrms.label.Experience" text="default text"/> </span></a></li>
			    <li><a href="#NomineeAndRelationDetails" data-toggle="tab"><span><spring:message code="hrms.label.NomineeAndRelationDetails" text="default text"/>  </span></a></li>
			    <!--                    <li><a href="#UserPrivilages"><span> User Privileges </span></a></li>-->
			    <li><a href="#Attachments" data-toggle="tab"><span> <spring:message code="hrms.label.Attachments" text="default text"/> </span></a></li>
			    <li><a href="#passport" data-toggle="tab"><span><spring:message code="hrms.label.PassportDetails" text="default text"/> </span></a></li>
			    <li><a href="#Visadetails" data-toggle="tab"><span> <spring:message code="hrms.label.VisaDetails" text="default text"/></span></a></li>
			</ul>
			<div id="PersonalDetails" class="tab-pane active"> 
			    <table class="table table-info mb30 " id="bg" >
				<thead>
                                    <th colspan="4" height="30" ><div class="contenthead"><spring:message code="hrms.label.EmployeeDetails" text="default text"/></div></th>
				</tr>
				<thead>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.Name" text="default text"/></td>
				    <td  height="30"><input type="text" name="name" id="name" size="20"  class="form-control" style="width:220px;height:40px;" value="<c:out value="${name}"/>" maxlength="50" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
				<input type="hidden" name="empRelationSize" id="empRelationSize" size="20"  class="form-control" style="width:220px;height:40px;" value="0" maxlength="50">
				<input type="hidden" name="staffId" id="staffId" size="20"  class="form-control" style="width:220px;height:40px;" value="<c:out value="${staffId}"/>" maxlength="50">
				<input type="hidden" name="staffUserId" id="staffUserId" size="20"  class="form-control" style="width:220px;height:40px;" value="<c:out value="${staffUserId}"/>" maxlength="50">
				<c:if test = "${empCode != null}">
				    <td  height="30"><spring:message code="hrms.label.EmployeeCode" text="default text"/></td>
				    <td  height="30"><a onclick="viewEmployeePhotos('<c:out value="${staffId}"/>')" ><c:out value="${empCode}"/></a></td>
				    </c:if>
				    <c:if test = "${empCode == null}">
				    <td  colspan="2">&nbsp;</td>
				</c:if>
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DateOfBirth" text="default text"/></td>
				    <td  height="30"><input type="text" id="dateOfBirth" name="dateOfBirth" readonly="readOnly" style="width:220px;height:40px;" size="20" class="datepicker"  value="<c:out value="${dateOfBirth}"/>"> </td>
				<script type="text/javascript">
				    $("#dateOfBirth").datepicker({
					yearRange: '1900:1995',
                                         changeMonth: true, changeYear: true, 
                                        format: 'dd-mm-yy'
				    });
				</script> 
				<td  height="30"><font color="red">*</font><spring:message code="hrms.label.DateOfJoining" text="default text"/> </td>
				<td  height="30"><input type="text" id="dateOfJoining" name="dateOfJoining" style="width:220px;height:40px;" size="20" readonly="readOnly" class="datepicker"  value="<c:out value="${dateOfJoining}"/>"></td>
				<script type="text/javascript">
				    $("#dateOfJoining").datepicker({
					yearRange: '1970:+nn', 
                                        changeMonth: true, changeYear: true, 
                                        //dateFormat: 'dd-mm-yy'
                                         format: 'dd-mm-yy'
				    });
				</script> 
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.Gender" text="default text"/></td>
				    <td  height="30">
					<select name="gender" style="width:220px;height:40px;">
					    <c:if test = "${gender != 'Female'}">
						<!--<input type="radio" name="gender" checked value="Male" > Male <input type="radio" name="gender" value="Female" > Female--> 
						<option value="Male" selected><spring:message code="hrms.label.Male" text="default text"/></option>
						<option value="Female"><spring:message code="hrms.label.Female" text="default text"/></option>
					    </c:if>
					    <c:if test = "${gender == 'Female'}">
						<!--<input type="radio" name="gender"  value="Male" > Male <input type="radio" name="gender" checked value="Female" > Female--> 
						<option value="Male"><spring:message code="hrms.label.Male" text="default text"/></option>
						<option value="Female" selected><spring:message code="hrms.label.Female" text="default text"/></option>
					    </c:if>
					</select>
				    </td>
				    <td  height="30"><spring:message code="hrms.label.BloodGroup" text="default text"/></td>
				    <td  height="30"><input type="text" name="bloodGrp" size="20" class="form-control" style="width:220px;height:40px;" value="<c:out value="${bloodGrp}"/>" maxlength="5"> </td>
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.MartialStatus" text="default text"/></td>
				    <td  height="30">
					<select name="martialStatus" style="width:220px;height:40px;">
					    <c:if test = "${martialStatus == null}">
						<option value="Married"><spring:message code="hrms.label.Married" text="default text"/></option>
						<option value="Unmarried" selected><spring:message code="hrms.label.Unmarried" text="default text"/></option>
						<option value="Widow"><spring:message code="hrms.label.Widow" text="default text"/></option>
						<!--                                    <input type="radio" name="martialStatus" value="Married">M
										    <input type="radio" name="martialStatus" value="Unmarried" checked>U
										    <input type="radio" name="martialStatus" value="Widow"> W-->
					    </c:if>
					    <c:if test = "${martialStatus == 'Married'}">
						<option value="Married" selected><spring:message code="hrms.label.Married" text="default text"/></option>
						<option value="Unmarried"><spring:message code="hrms.label.Unmarried" text="default text"/></option>
						<option value="Widow"><spring:message code="hrms.label.Widow" text="default text"/></option>
						<!--                                    <input type="radio" name="martialStatus" value="Married" checked>Married
										    <input type="radio" name="martialStatus" value="Unmarried" >UnMarried 
										    <input type="radio" name="martialStatus" value="Widow">Widow-->
					    </c:if>
					    <c:if test = "${martialStatus == 'Unmarried'}">
						<option value="Married"><spring:message code="hrms.label.Married" text="default text"/></option>
						<option value="Unmarried" selected><spring:message code="hrms.label.Unmarried" text="default text"/></option>
						<option value="Widow"><spring:message code="hrms.label.Widow" text="default text"/></option>
						<!--                                    <input type="radio" name="martialStatus" value="Married">Married
										    <input type="radio" name="martialStatus" value="Unmarried" checked>UnMarried 
										    <input type="radio" name="martialStatus" value="Widow"> Widow-->
					    </c:if>
					    <c:if test = "${martialStatus == 'Widow'}">
						<option value="Married"><spring:message code="hrms.label.Married" text="default text"/></option>
						<option value="Unmarried"><spring:message code="hrms.label.Unmarried" text="default text"/></option>
						<option value="Widow" selected><spring:message code="hrms.label.Widow" text="default text"/></option>
						<!--                                    <input type="radio" name="martialStatus" value="Married">Married
										    <input type="radio" name="martialStatus" value="Unmarried" >UnMarried 
										    <input type="radio" name="martialStatus" value="Widow" checked>Widow-->
					    </c:if>
					</select>
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.FatherName" text="default text"/></td>
				    <td  height="30"><input name="fatherName" type="text" class="form-control" style="width:220px;height:40px;" size="20"  value="<c:out value="${fatherName}"/>" maxlength="50" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.MobileNo" text="default text"/> </td>
				    <td  height="30"><input type="text" name="mobile" size="20" class="form-control" style="width:220px;height:40px;" maxlength="12" value="<c:out value="${mobile}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
				    <td  height="30"><font color="red"></font><spring:message code="hrms.label.HomeNo" text="default text"/> </td>
				    <td  height="30"><input type="text" name="phone" size="20" class="form-control" style="width:220px;height:40px;" maxlength="12" value="<c:out value="${phone}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
				</tr>

				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.Qualification" text="default text"/> </td>
				    <td  height="30"><input type="text" name="qualification" size="20" value="<c:out value="${qualification}"/>" class="form-control" style="width:220px;height:40px;"maxlength="30"></td>
				    <td  height="30">&nbsp;&nbsp;<spring:message code="hrms.label.Email-Id" text="default text"/></td>
				    <td  height="30"><input type="text" name="email" size="20" value="<c:out value="${email}"/>" class="form-control" style="width:220px;height:40px;" maxlength="50"></td>
				</tr>
				<tr>
				    <td colspan="4" height="30" class="contenthead"><B><spring:message code="hrms.label.OfficialDetails" text="default text"/></B></td>
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DepartmentName" text="default text"/></td>
				    <td  height="30">
					<select class="form-control" style="width:220px;height:40px;" name="deptid" id="deptid" style="width:125px;" >
					    <option value="0">---<spring:message code="hrms.label.Select" text="default text"/>---</option>
					    <c:if test = "${DeptList != null}" >
						<c:forEach items="${DeptList}" var="Dept">
						    <option value='<c:out value="${Dept.departmentId}" />'><c:out value="${Dept.name}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
					    document.getElementById("deptid").value = <c:out value="${deptId}"/>;
					    </script>
					</select>
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DesignationName" text="default text"/> </td>
				    <td  height="30"><select class="form-control" style="width:220px;height:40px;" id="designationId" name="designationId" onChange="fillSelect(this.value);
					showIfDriver(this.value)" style="width:125px;">
					    <option value="0">-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <c:if test = "${DesignaList != null}" >
						<c:forEach  items="${DesignaList}" var="Desig">
						    <option value='<c:out value="${Desig.desigId}" />'><c:out value="${Desig.desigName}" /></option>
						</c:forEach >
					    </c:if>
					    <c:if test = "${desigId != null && desigId != ''}" >
						<script>
						document.getElementById("designationId").value = <c:out value="${desigId}"/>;
						$(document).ready(function() {
						    showIfDriver('<c:out value="${desigId}"/>');
						});
						</script>
					    </c:if>        
					</select>
				    </td>

				<script>
				    function showIfDriver(desig) {
	//                                alert("desig " + desig);
					if (desig == "1034") {
					    //alert("desig1 " + desig);
					    $("#drivingLicenceInfo").show();
					    $("#drivingLicenceInfo1").show();
					    $("#drivingLicenceInfo2").show();
					    $("#drivingLicenceInfo3").show();
					} else {
					    $("#drivingLicenceInfo").hide();
					    $("#drivingLicenceInfo1").hide();
					    $("#drivingLicenceInfo2").hide();
					    $("#drivingLicenceInfo3").hide();
					}

				    }
				</script>
				</tr>


				<tr>
                                    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.GradeName" text="default text"/> </td>
				    <td  height="30">
					<select class="form-control" style="width:220px;height:40px;" name="gradeId" id="gradeId" style="width:125px;">
                                            <option value="0" selected> -- <spring:message code="hrms.label.Select" text="default text"/> -- </option>
					    <c:if test = "${GradeList != null}" >
						<c:forEach  items="${GradeList}" var="grade">
						    <option value='<c:out value="${grade.gradeId}" />'><c:out value="${grade.gradeName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
                                                <c:if test="${gradeId != '0'}">
						document.getElementById("gradeId").value = <c:out value="${gradeId}"/>;
                                                if(document.getElementById("gradeId").value == ''){
						document.getElementById("gradeId").value = '0';
                                                }
                                                </c:if>
					    </script>        
					</select>
				    </td>

				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.Company" text="default text"/> </td>
				    <td  height="30"><select class="form-control" style="width:220px;height:40px;" name="cmpId" id="cmpId" style="width:125px;">
					    <option value="0">-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <c:if test = "${companyList!= null}" >
						<c:forEach  items="${companyList}" var="comp">
						    <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("cmpId").value = <c:out value="${cmpId}"/>;
					    </script>       
					</select></td>
				</tr>
				<tr>
				    <td  height="30"><spring:message code="hrms.label.ReferenceName" text="default text"/></td>
				    <td  height="30"><input type="text" name="referenceName" maxlength="35" id="referenceName" class="form-control" style="width:220px;height:40px;" value="<c:out value="${referenceName}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>
				    <td  height="30"><spring:message code="hrms.label.ContractEmployee" text="default text"/></td>
				    <td  height="30">
					<select class="form-control" style="width:220px;height:40px;" name="contractDriver" id="contractDriver" style="width:125px;" onChange="showDriverDetails();">
					    <option value="">-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <option value="YES"><spring:message code="hrms.label.Yes" text="default text"/></option>
					    <option value="NO" selected><spring:message code="hrms.label.No" text="default text"/></option>
					    <script>
						document.getElementById("contractDriver").value = <c:out value="${driverType}"/>;
					    </script>
					</select>
				    </td>
				</tr>
				<script>
				    function showDriverDetails() {
					//alert(document.getElementById("contractDriver").value);
					if (document.getElementById("contractDriver").value == "NO") {
					    document.getElementById("vendorCompanySpan1").style.display = 'none'
					    document.getElementById("vendorCompanySpan2").style.display = "none";
                                             document.getElementById("vendorCompanySpan3").style.display = 'block'
					    document.getElementById("vendorCompanySpan4").style.display = "block";
					} else {
					    document.getElementById("vendorCompanySpan1").style.display = 'block'
					    document.getElementById("vendorCompanySpan2").style.display = "block";
					    document.getElementById("vendorCompanySpan3").style.display = 'none'
					    document.getElementById("vendorCompanySpan4").style.display = "none";
					}
				    }
				</script>
				<tr>
				    <td height="30" ><spring:message code="hrms.label.Status" text="default text"/></td>
				    <td height="30" ><select name="activeInd" class="form-control" style="width:220px;height:40px;" style="width:125px;">
					    <c:choose>
						<c:when test="${status == 'Y' || status == null}">
						    <option value="Y" selected><spring:message code="hrms.label.Active" text="default text"/></option>
						    <option value="N"><spring:message code="hrms.label.InActive" text="default text"/></option>
						</c:when>
						<c:otherwise>
						    <option value="Y"><spring:message code="hrms.label.Active" text="default text"/></option>
						    <option value="N" selected><spring:message code="hrms.label.InActive" text="default text"/></option>
						</c:otherwise>
					    </c:choose>
					</select>
				    </td>
				    <td  id="vendorCompanySpan1" height="30" style="display: none"><spring:message code="hrms.label.VendorName" text="default text"/></td>
				    <td  id="vendorCompanySpan2" style="display: none">
					<select class="form-control" style="width:220px;height:40px;" name="vendorCompany" id="vendorCompany"  style="width:125px;">
					    <option value='0' checked>--<spring:message code="hrms.label.Select" text="default text"/>--</option>
					    <c:if test = "${vendorList != null}" >
						<c:forEach items="${vendorList}" var="vlist">
						    <option value='<c:out value="${vlist.vendorId}" />'><c:out value="${vlist.vendorName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("vendorCompany").value = <c:out value="${vendorCompany}"/>;
					    </script>
					</select>
				    </td>
                                    <td id="vendorCompanySpan3"></td>
                                    <td id="vendorCompanySpan4"></td>
				</tr>
			    </table>
			    <table class="table table-info mb30 table-hover" id="bg" >
				<tr>
				    <td height="30" colspan='4' align='center' > <font color="#FF0033" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "><div align="center" id="userNameStatus" style="height:25px; "></div></font> </td>
				    <td>
				</tr>
				<tr>
				    <td height="30" ><spring:message code="hrms.label.Clickhereifneeduserprivilege" text="default text"/></td>
				    <td height="30" ><input type="hidden" name="userStatus1" id="userStatus1" size="20" class="form-control" style="width:220px;height:40px;" maxlength="6" value="0">
					<input type="checkbox" class="form-control" style="width:100px;height:20px;" name="userStatus" id="userStatus" onClick="showTab();" /></td>
				    <td height="30" >&nbsp;</td>
				    <td height="30" >&nbsp;</td>
				    <td>
				</tr>
				<c:if test = "${appUserName != ''}">
				    <script>

					$(document).ready(function() {
					    document.getElementById("userStatus").checked = true;
					    showTab();
					});
				    </script>
				</c:if>

				<tr id="userStat" style="visibility:hidden;">
				    <td height="30" ><font color="red">*</font><spring:message code="hrms.label.UserName" text="default text"/> :</td>
				    <td height="30" >
					<!--<input type="text" class="form-control" style="width:220px;height:40px;" name="userName"  value="" maxlength="20" onchange="getProfile(this.value)" />-->
					<input type="text"  class="form-control" style="width:220px;height:40px;" name="userName" id="userName" id="userName" value="<c:out value="${userName}" />" maxlength="20"/>
					<input type="hidden" class="form-control" style="width:80;height:40px;" name="appUserCode" id="appUserCode"   value="100@"  readonly style="width: 35px;"/>
					<input type="hidden" class="form-control" style="width:220px;height:40px;" name="appUserName" id="appUserName"  value="<c:out value="${appUserName}" />" maxlength="20" onChange="getProfile(this.value)" />
					<!--<input type="text" class="form-control" style="width:220px;height:40px;" name="userName" id="userName"  value="<c:out value="${userName}" />" maxlength="20" onChange="getProfile(this.value)" />-->
				    </td>
				    <td height="30" ><font color="red">*</font><spring:message code="hrms.label.Password" text="default text"/>  :</td>
				    <td height="30" ><input type="password" class="form-control" style="width:220px;height:40px;"  name="password" id="password" maxlength="10"  value="<c:out value="${password}" />" /></td>
				    <td>
				</tr>
				<tr id="userStat1"  style="visibility:hidden;">
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.EmployeeRole" text="default text"/></td>
				    <td  height="30"><select class="form-control" style="width:220px;height:40px;" name="roleId" id="roleId" style="width:125px;">
					    <option value="0">-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <c:if test = "${roleList != null}" >
                                                <option value="0">-select-</option>
						<c:forEach  items="${roleList}" var="role">
						    <option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("roleId").value = <c:out value="${roleId}" />;
					    </script>
					</select></td>

	<!--                            <td  height="30"><font color="red">*</font>Branch Name</td>
				    <td  height="30"><select class="form-control" style="width:220px;height:40px;" name="branchId" id="branchId" style="width:125px;">
					    <option value="0">-Select-</option>
					    <c:if test = "${branchList != null}" >
						<c:forEach  items="${branchList}" var="role">
						    <option value='<c:out value="${role.branchId}" />'><c:out value="${role.branchName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("branchId").value = <c:out value="${branchId}" />;
					    </script>
					</select></td>-->
				</tr>
				<tr id="userStat2"  style="visibility:hidden;">
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.CompanyList" text="default text"/></td>
				    <td  height="30"><select class="form-control" style="width:220px;height:40px;" name="companyId" id="companyId" style="width:125px;">
					    <c:if test = "${companyLists != null}" >
						<c:forEach  items="${companyLists}" var="company">
						    <option value='<c:out value="${company.companyId}" />'><c:out value="${company.companyName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("companyId").value = '<c:out value="${companyId}" />';
					    </script>
					</select></td>

				    <td colspan="2">&nbsp;</td>
				</tr>
			    </table>

			    <br>        
			    <br>  
			    <center>
				<%--<c:if test = "${staffId == null}" >
				    <input type="button" class="btn btn-success" value="Save" name="Save" onClick="submitPage(this.name)">
				</c:if>
				<c:if test = "${staffId != null}" >
				    <input type="button" class="btn btn-success" value="alter" name="alter" onClick="submitPage(this.name)">
				</c:if>--%>
				<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
			    </center>
			</div>
			<div id="OfficialDetails" class="tab-pane">
			    <table class="table table-info mb30 table-hover" id="bg" >
				<!--                        <script>
							    $(document).ready(function () {



							    });
							</script>-->
                                <thead>
				<tr>
				    <th class="contenthead" height="30" colspan="4"><b><spring:message code="hrms.label.PresentAddress" text="default text"/></b></th>
				</tr>
                                </thead>
				<tr>
				    <td  height="30"><spring:message code="hrms.label.Address" text="default text"/></td>
				    <td  height="30"> <textarea name="addr" size="20" class="form-control" style="width:220px;height:40px;"  maxlength="100" ><c:out value="${addr}"/></textarea></td>
				    <td  height="30"><spring:message code="hrms.label.City" text="default text"/></td>
				    <td  height="30"><input type="text" name="city" size="20" class="form-control" style="width:220px;height:40px;" maxlength="50" value="<c:out value="${city}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
				</tr>
				<tr>
				    <td  height="30"><spring:message code="hrms.label.State" text="default text"/></td>
				    <td  height="30"><input type="text" name="state" size="20" class="form-control" style="width:220px;height:40px;" maxlength="50" value="<c:out value="${state}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
				    <td  height="30"><spring:message code="hrms.label.pincode" text="default text"/></td>
				    <td  height="30"><input type="text" name="pincode" size="20" class="form-control" style="width:220px;height:40px;" maxlength="6" value="<c:out value="${pincode}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
				</tr>
				<tr>
				    <td class="contenthead" height="30" colspan="2"><b><spring:message code="hrms.label.PermanentAddress" text="default text"/></b> </td>
				    <td colspan="2" class="contenthead" height="30"><input type="checkbox" name="chec" id="chec" maxlength="100" onClick="copyAddress(this.checked)" value="Y">
				       <spring:message code="hrms.label.IfTemporaryAddressisSameAsPermanentAddressClickHere" text="default text"/>  </td>
				</tr>
				<c:if test = "${chec != null && chec !=''}">
				    <c:if test = "${chec == 'Y'}">
					<script>
					    $(document).ready(function() {
						document.getElementById("chec").checked = true;
						copyAddress(true);
					    });
					</script>
				    </c:if>
				    <c:if test = "${chec == 'N'}">
					<script>
					    $(document).ready(function() {
						document.getElementById("addr1").value = <c:out value="${addr1}"/>;
						document.getElementById("city1").value = <c:out value="${city1}"/>;
						document.getElementById("state1").value = <c:out value="${state1}"/>;
						document.getElementById("pincode1").value = <c:out value="${pincode1}"/>;
					    });
					</script>
				    </c:if>
				</c:if>
				<tr >
				    <td  height="30"><spring:message code="hrms.label.Address" text="default text"/> </td>
				    <td  height="30"><textarea  name="addr1" id="addr1" size="20" class="form-control" style="width:220px;height:40px;" value="" maxlength="45"></textarea></td>
				    <td  height="30"><spring:message code="hrms.label.City" text="default text"/></td>
				    <td  height="30"><input type="text" name="city1" id="city1" size="20" class="form-control" style="width:220px;height:40px;" maxlength="20" value="" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
				</tr>
				<tr>
				    <td  height="30"><spring:message code="hrms.label.State" text="default text"/></td>
				    <td  height="30"><input type="text" name="state1" id="state1" size="20" maxlength="20" class="form-control" style="width:220px;height:40px;" value="" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
				    <td  height="30"><spring:message code="hrms.label.pincode" text="default text"/></td>
				    <td  height="30"><input type="text" name="pincode1" id="pincode1" size="20" maxlength="6" class="form-control" style="width:220px;height:40px;" value="" ></td>
				</tr>
				<tr>
				    <td id="drivingLicenceInfo2" colspan="4" height="30" class="contenthead"><B><spring:message code="hrms.label.DrivingLicenseDetail" text="default text"/></B></td>
				</tr>
				<tr id="drivingLicenceInfo"><td ><spring:message code="hrms.label.DrivingLicenseNo" text="default text"/></td>
				    <td ><input type="text" name="drivingLicenseNo" class="form-control" style="width:220px;height:40px;" maxlength="30" value="<c:out value="${drivingLicenseNo}"/>" /></td>
				    <td ><spring:message code="hrms.label.ValidUpto" text="default text"/></td>
				    <td ><input type="text" id="licenseExpDate" class="form-control" style="width:220px;height:40px;" name="licenseExpDate" class="datepicker"  value="<c:out value="${licenseDate}"/>"/></td>
				<script type="text/javascript">
				    $("#licenseExpDate").datepicker({
					yearRange: '2014:2030', changeMonth: true, changeYear: true,dateFormat: 'dd-mm-yy'
				    });
				</script> 
				</tr>

				<tr id="drivingLicenceInfo1"><td ><spring:message code="hrms.label.LicenseType" text="default text"/></td>
				    <td ><select class="form-control" style="width:220px;height:40px;" name="licenseType" id="licenseType" style="width:125px;">
					    <option value="" > -- <spring:message code="hrms.label.Select" text="default text"/> -- </option>
					    <option value="LMV" ><spring:message code="hrms.label.LMV" text="default text"/></option>
					    <option value="LMV With Batch" ><spring:message code="hrms.label.LMVWithBatch" text="default text"/></option>
					    <option value="Heavy" ><spring:message code="hrms.label.Heavy" text="default text"/></option>
					</select>
					<script>
					    document.getElementById("licenseType").value = <c:out value="${licenseType}"/>;
					</script>
				    </td>
				    <td ><spring:message code="hrms.label.State" text="default text"/></td>
				    <td ><input type="text" name="licenseState" value="<c:out value="${licenseState}"/>" class="form-control" style="width:220px;height:40px;" maxlength="20" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>
				</tr>



				<!--                <tr id="drivingLicenceInfo2"><td >Driver Type</td>
						    <td ><select class="form-control" style="width:220px;height:40px;" name="driverType" style="width:125px;">
							    <option value="" > -- Select -- </option>
							    <option value="PRR" >CLPL</option>
							    <option value="ATT" >ATT</option>
							</select>
						    </td>
						    <td >Vendor Company/Introducer</td>
						    <td ><input type="text" name="vendorCompany" id="vendorCompany" value="" class="form-control" style="width:220px;height:40px;"/></td>
						</tr>-->
				<input type="hidden" name="driverType" id="driverType" value="" class="form-control" style="width:220px;height:40px;"/>
				<tr>
				    <td colspan="4" height="30" class="contenthead"><B><spring:message code="hrms.label.SalaryDetails" text="default text"/></B></td>
				</tr>
				<tr><td ><spring:message code="hrms.label.SalaryType" text="default text"/></td>
				    <td >
					<select class="form-control" style="width:220px;height:40px;" name="salaryType" id="salaryType" style="width:125px;">
					    <option value="" > --<spring:message code="hrms.label.Select" text="default text"/>  -- </option>
					    <option value="Daily" ><spring:message code="hrms.label.Daily" text="default text"/></option>
					    <option value="Monthly" ><spring:message code="hrms.label.Monthly" text="default text"/></option>
					</select>
					<script>
					    document.getElementById("salaryType").value = '<c:out value="${salaryType}"/>';
					</script>
				    </td>
				    <td ><spring:message code="hrms.label.ConsolidatedSalary" text="default text"/></td>
				    <td ><input type="text" name="basicSalary" id="basicSalary" class="form-control" style="width:220px;height:40px;" value="0" maxlength="10" /></td>
				<script>
				    document.getElementById("basicSalary").value = <c:out value="${basicSalary}"/>;
				</script>
				</tr>
				<tr><td ><spring:message code="hrms.label.IsEligibleForESI" text="default text"/></td>
				    <td >
					<c:if test = "${esiEligible != 'N'}" >
					    <input type="radio" name="esiEligible" checked value="Y" > <spring:message code="hrms.label.Yes" text="default text"/> <input type="radio" name="esiEligible" value="N" ><spring:message code="hrms.label.No" text="default text"/> 
					</c:if>
					<c:if test = "${DesignaList == 'N'}" >
					    <input type="radio" name="esiEligible"  value="Y" ><spring:message code="hrms.label.Yes" text="default text"/>  <input type="radio" name="esiEligible" checked value="N" ><spring:message code="hrms.label.No" text="default text"/> 
					</c:if>
				    </td>
				    <td ><spring:message code="hrms.label.IsEligibleForPF" text="default text"/></td>
				    <td >
					<c:if test = "${esiEligible != 'N'}" >
					    <input type="radio" name="pfEligible"  value="Y" checked><spring:message code="hrms.label.Yes" text="default text"/>  <input type="radio" name="pfEligible" value="N"  ><spring:message code="hrms.label.No" text="default text"/> 
					</c:if>
					<c:if test = "${esiEligible == 'N'}" >
					    <input type="radio" name="pfEligible"  value="Y" ><spring:message code="hrms.label.Yes" text="default text"/>  <input type="radio" name="pfEligible" value="N" checked ><spring:message code="hrms.label.No" text="default text"/> 
					</c:if>
				    </td>
				</tr>
				<tr>
				    <td ><spring:message code="hrms.label.BankAccountNo" text="default text"/></td>
				    <td ><input type="text" name="bankAccountNo" id="bankAccountNo" maxlength="25" class="form-control" style="width:220px;height:40px;" value="<c:out value="${bankAccountNo}"/>"  /></td>
				    <td ><spring:message code="hrms.label.BankName" text="default text"/></td>
				    <td ><input type="text" name="bankName" id="bankName" maxlength="25" class="form-control" style="width:220px;height:40px;" value="<c:out value="${bankName}"/>"  /></td>
				</tr>
				<tr>
				    <td ><spring:message code="hrms.label.BankBranch" text="default text"/></td>
				    <td ><input type="text" name="bankBranchName" id="bankBranchName" maxlength="25" class="form-control" style="width:220px;height:40px;" value="<c:out value="${bankBranchName}"/>"  /></td>
				    <td ><spring:message code="hrms.label.A/CHolderName" text="default text"/></td>
				    <td ><input type="text" name="accountHolderName" id="accountHolderName" maxlength="25" class="form-control" style="width:220px;height:40px;" value="<c:out value="${accountHolderName}"/>"  /></td>
				</tr>
			    </table>
			    <br>        
			    <br>        
			    <center>
				<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
			    </center>
			</div>
			<div id="Experience" class="tab-pane">
			    <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
				<tr>
				    <th colspan="4" height="30" class="contenthead"><B><spring:message code="hrms.label.PreviousExperienceandEmployerDetails" text="default text"/></B></th>
				</tr>
                                </thead>
				<tr><td ><spring:message code="hrms.label.YearsOfExperience" text="default text"/> </td>
				    <td ><input type="text" name="yearOfExperience" id="yearOfExperience" class="form-control" style="width:220px;height:40px;" maxlength="2" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/>
				    </td>
				<script>
				    document.getElementById("yearOfExperience").value = <c:out value="${yearOfExperience}"/>;
				</script>
				<td ><spring:message code="hrms.label.Designation" text="default text"/></td>
				<td ><input type="text" name="preEmpDesignation" id="preEmpDesignation" class="form-control" style="width:220px;height:40px;" value="<c:out value="${prevEmpDesignation}"/>" maxlength="25"/></td>
				</tr>
				<tr><td ><spring:message code="hrms.label.Company" text="default text"/></td>
				    <td >
					<input type="text" name="prevCompanyName" id="prevCompanyName" class="form-control" style="width:220px;height:40px;"  value="<c:out value="${prevEmpDesignation}"/>" maxlength="30"/>
				    </td>
				<script>
				</script>
				<td ><spring:message code="hrms.label.Address" text="default text"/></td>
				<td ><input type="text" name="prevCompanyAddress" class="form-control" style="width:220px;height:40px;"  maxlength="35" value="<c:out value="${prevCompanyAddress}"/>"/></td>
				</tr>
				<tr><td ><spring:message code="hrms.label.City" text="default text"/></td>
				    <td ><input type="text" name="prevCompanyCity" class="form-control" style="width:220px;height:40px;" maxlength="20" value="<c:out value="${prevCompanyCity}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"/>
				    </td>
				    <td ><spring:message code="hrms.label.State" text="default text"/></td>
				    <td ><input type="text" name="preEmpState" class="form-control" style="width:220px;height:40px;" maxlength="20" value="<c:out value="${prevEmpState}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>
				</tr>
				<tr>
				    <td ><spring:message code="hrms.label.pincode" text="default text"/></td>
				    <td ><input type="text" name="preEmpPincode" class="form-control" style="width:220px;height:40px;" maxlength="6" value="<c:out value="${prevEmployeePincode}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
				    <td ><spring:message code="hrms.label.ContactNo" text="default text"/></td>
				    <td ><input type="text" name="prevCompanyContact" class="form-control" style="width:220px;height:40px;" maxlength="12" value="<c:out value="${prevCompanyContact}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/>
				    </td>

				</tr>
			    </table>
			    <br>        
			    <br>        
			    <center>
				<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
			    </center>
			</div>            
			<div id="NomineeAndRelationDetails" class="tab-pane">  
			    <table class="table table-info mb30 table-hover" id="bg" >
				<tr>
                                <thead>
				    <th colspan="4" height="30" class="contenthead"><B><spring:message code="hrms.label.NomineeDetails" text="default text"/></B></th>
                            </thead>
                            </tr>
				<tr><td ><spring:message code="hrms.label.Name" text="default text"/></td>
				    <td ><input type="text" name="nomineeName" class="form-control" style="width:220px;height:40px;" maxlength="30" value="<c:out value="${nomineeName}"/>"/>
				    </td>
				    <td ><spring:message code="hrms.label.Relation" text="default text"/></td>
				    <td ><select class="form-control" style="width:220px;height:40px;" name="nomineeRelation" id="nomineeRelation" style="width:125px;">
					    <option value="0">-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <c:if test = "${relationList!= null}" >
						<c:forEach  items="${relationList}" var="rel">
						    <option value='<c:out value="${rel.relationId}" />'><c:out value="${rel.relationName}" /></option>
						</c:forEach >
					    </c:if>
					    <script>
						document.getElementById("nomineeRelation").value = '<c:out value="${nomineeRelation}"/>';
					    </script>
					</select></td>
				</tr>
				<tr> <td ><spring:message code="hrms.label.DateOfBirth" text="default text"/></td>
				    <td ><input type="text" class="form-control" style="width:220px;height:40px;" id="nomineeDob" name="nomineeDob" id="nomineeDob" value="<c:out value="${nomineeDob}"/>"  readonly="readOnly" size="20" class="datepicker" onChange="getNomineeAge(this.value)"></td>
				<script type="text/javascript">
				    $("#nomineeDob").datepicker({
					yearRange: '1900:+nn', changeMonth: true, changeYear: true
				    });
				</script> 
				<td ><spring:message code="hrms.label.Age" text="default text"/></td>
				<td ><input type="text" name="nomineeAge" id="nomineeAge" class="form-control" style="width:220px;height:40px;"  value="0" /></td>
				<script>
				    document.getElementById("nomineeAge").value = <c:out value="${nomineeAge}"/>;
				</script>
				</tr>
			    </table>

			    <table id="allowanceTBL" class="table table-info mb30 table-hover"  ><center><spring:message code="hrms.label.EmployeeFamilyDetail" text="default text"/></center>
				    <%
				int sno = 0;
				    %>   
				   
                                    <thead>
                                    <tr>
					<th class="contenthead" height="30"><spring:message code="hrms.label.SNo" text="default text"/></th>
					<th class="contenthead" height="30"><spring:message code="hrms.label.Name" text="default text"/></th>
					<th class="contenthead" height="30"><spring:message code="hrms.label.Relation" text="default text"/></th>
					<th class="contenthead" height="30"><spring:message code="hrms.label.DateOfBirth" text="default text"/></th>
					<th class="contenthead" height="30"><spring:message code="hrms.label.Age" text="default text"/></td>
				    </tr>
                                    </thead>
                                     <c:if test="${familyList != null}">
				    <c:forEach items="${familyList}" var="family">
					<tr>
					    <td  height="40"><%= sno+1%></td>
					<input type="hidden" name="empRelationIds" id="empRelationIds<%=sno%>" value="<c:out value="${family.empRelationId}"/>"  class="form-control" style="width:220px;height:40px;" />
					<td  height="40"><input type="text" name="relationName" id="relationName<%=sno%>" value="<c:out value="${family.relationNames}"/>"  class="form-control" style="width:220px;height:40px;" /></td>
					<td  height="40">
					   dfgd <select class="form-control" style="width:220px;height:40px;" name="relation" id="relation<%=sno%>" >
						<option value='0'>-<spring:message code="hrms.label.Select" text="default text"/>-</option>
						<c:if test = "${relationList!= null}" >
						    <c:forEach  items="${relationList}" var="rel">
							<option value='<c:out value="${rel.relationId}" />'><c:out value="${rel.relationName}" /></option>
						    </c:forEach >
						</c:if>
					    </select>
					</td>
					<script>
					    document.getElementById("relation" + <%=sno%>).value = <c:out value="${family.relationId}"/>;
					</script>
					<td  height="40"><input type="text" name="relationDOB" id="relationDOB<%=sno%>" class="datepicker" onchange="showAge('<%=sno%>');" value="<c:out value="${family.relationDobs}"/>"  style="width:220px;height:40px;"/>
					<script type="text/javascript">
				    $("#relationDOB<%=sno%>").datepicker({
					 changeMonth: true, changeYear: true
				    });
				</script>
                                </td>
                                        <td  height="40"><input type="text" name="relationAge" id="relationAge<%=sno%>" value="<c:out value="${family.relationAges}"/>"  class="form-control" style="width:220px;height:40px;" /></td>
					</tr>
					<%sno++;%>
				    </c:forEach>
				</c:if>
				<br>
			    </table>
			    <%--<c:if test="${familyList == null}">--%>
			    <center><c:out value="${family.relationType}"/>
				<INPUT type="button" class="btn btn-success" value="<spring:message code="hrms.label.ADDROW" text="default text"/>" onClick="addRow()" />
				<INPUT type="button" class="btn btn-success" value="<spring:message code="hrms.label.DELETEROW" text="default text"/>" onClick="deleteRow()" />
			    </center>
			    <%--</c:if>--%>
			    <br>        
			    <br>        
			    <center>
				<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
			    </center>
			</div>            

			<div id="passport" class="tab-pane">
			    <table class="table table-info mb30 table-hover" id="bg">
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.PassportNo" text="default text"/>:</td>
				    <td  height="30">
					<input type="text" name="passportNo" id="passportNo" size="20" class="form-control" style="width:220px;height:40px;" maxlength="20" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${passportNo}"/>">
					<!--<input type="file" id="employeePhoto" name="employeePhoto" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0">-->
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.PassportName" text="default text"/>:</td>
				    <td  height="30">
					<input type="text" name="passportName" id="passportName" size="20" class="form-control" style="width:220px;height:40px;" maxlength="30" onKeyPress="return onKeyPressBlockNumbers(event);" value="<c:out value="${passportName}"/>">
				    </td>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.PassportType" text="default text"/>:</td>
				    <td  height="30">
					<select name="passportType" id="passportType" class="form-control" style="width:220px;height:40px;">

					    <option value="1"><spring:message code="hrms.label.RegularPassport" text="default text"/></option>
					    <option value="2" selected><spring:message code="hrms.label.DiplomaticPassport" text="default text"/></option>
					    <option value="3"><spring:message code="hrms.label.OfficialPassport" text="default text"/> </option>

					</select>
					<script>
					   document.getElementById("passportType").value = <c:out value="${passportType}"/>; 
					</script>
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.Nationality" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="nationality" id="nationality" size="20" class="form-control" style="width:220px;height:40px;" maxlength="15" onKeyPress="return onKeyPressBlockNumbers(event);" value="<c:out value="${nationality}"/>">
				    </td>

				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.PlaceOfIssue" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="palceOfIssue" id="palceOfIssue" size="20" class="form-control" style="width:220px;height:40px;" maxlength="15" onKeyPress="return onKeyPressBlockNumbers (event);" value="<c:out value="${palceOfIssue}"/>">
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.PlaceOfBirth" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="palceOfBirth" id="palceOfBirth" size="20" class="form-control" style="width:220px;height:40px;" maxlength="20" onKeyPress="return onKeyPressBlockNumbers (event);" value="<c:out value="${palceOfBirth}"/>">
				    </td>

				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DateOfIssue" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="dateOfIssue" style="width:220px;height:40px;" id="dateOfIssue" size="20" class="datepicker"  value="<c:out value="${dateOfIssue}"/>" >
					<!--<input type="file" id="employeePhoto" name="employeePhoto" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0">-->
				    </td>
                                    <script type="text/javascript">
				    $("#dateOfIssue").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DateOfexpiry" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="dateOfExpiry" id="dateOfExpiry" size="20" style="width:220px;height:40px;" class="datepicker" value="<c:out value="${dateOfExpiry}"/>"  >
					<!--<input type="file" id="employeePhoto" name="employeePhoto" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0">-->
				    </td>
<script type="text/javascript">
				    $("#dateOfExpiry").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				</tr>

				</table>
				<br>
				<br>
			    <table class="table table-info mb30 table-hover" border="1">
				<tr>
				<td><spring:message code="hrms.label.SNo" text="default text"/></td>
				<c:if test="${possportDeatils != null}">
				<td><spring:message code="hrms.label.FileName" text="default text"/></td>
				</c:if>
				<td><spring:message code="hrms.label.File" text="default text"/></td>
				<td colspan="3"><spring:message code="hrms.label.Remarks" text="default text"/></td>

			    </tr>
				<tr>
				<td>&nbsp;1:</td>
				 <c:if test="${possportDeatils != null}">
				     <td> <img src="/throttle/displayLogoBlobData.do?uploadId=<c:out value ="${uploadId}"/>"  style="width:200px;height:40px;" data-toggle="modal" data-target="#myModal" title="<c:out value ="${remarks}"/>"/></td>
				 <input type="hidden" id="uploadId" name="uploadId" value="<c:out value="${uploadId}"/>" >
				 </c:if>
				<td><input type="file" name="passportPhotoFile" id="passportPhotoFile" size="45"  multiple style="width:500px;" /> 
				<!--<td><input type="file" name="file" size="45"  multiple style="width:500px;" />--> 
				<!--<td ><spring:message code="hrms.label.Remarks" text="default text"/></td>-->
				<td  colspan="3"><textarea cols="120" rows="2" class="form-control" style="width:200px;height:40px;" name="uploadRemarks" id="uploadRemarks" value="<c:out value="${uploadRemarks}"/>" ></textarea> </td>
				</tr>
				<tr>
			    </table>
			    <center>
			    <font color="green" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
				<ul id="messages" ></ul>&nbsp;&nbsp;
			    </font>
			  </center>
			    <br>
			    <tr>
				<center>
				<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
				<a><input type="button" class="btn btn-success btnPrevious"  value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
			    </center>
				<center>

				    <!--<input id="uploadBtn" type="button" class="btn btn-success" value="Upload Files" onclick="upload()" />-->

				</center>
			    </tr>
			    <br>        
			    <br>  
			    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
					<div class="modal-content">
					    <div class="modal-body">
						<img src="/throttle/displayLogoBlobData.do?uploadId=<c:out value ="${uploadId}"/>" class="img-responsive" title="<c:out value ="${remarks}"/>">
					    </div>
					</div>
				    </div>
				</div>


			    <script type="text/javascript">

					// $(document).ready(function ()
					// {
					//    $("#uploadBtn").click(function ()
					function upload()
					{
	//                                var contractId = $("#contractId").val();
					var uploadId = $("#uploadId").val();
					var uploadRemarks = $("#uploadRemarks").val();
					var passportNo = $("#passportNo").val();
					var passportName = $("#passportName").val();
					var nationality = $("#nationality").val();
					var palceOfIssue = $("#palceOfIssue").val();
					var palceOfBirth = $("#palceOfBirth").val();
					var dateOfExpiry = $("#dateOfExpiry").val();
					var dateOfIssue = $("#dateOfIssue").val();
					var passportType = $("#passportType").val();

					$('input[name="file"]').each(function (index, value)
					{
					var file = value.files[0];
					if (file)
					{
					var formData = new FormData();
					formData.append('file', file);
					$.ajax({
					url: './uploadEmpPassportDetails.do?uploadId=' + uploadId + '&uploadRemarks=' + uploadRemarks+ '&passportNo='+passportNo+'&passportName='+passportName+'&nationality='+nationality+'&palceOfIssue='+palceOfIssue+'&palceOfBirth='+palceOfBirth+'&dateOfExpiry='+dateOfExpiry+'&dateOfIssue='+dateOfIssue+'&passportType='+passportType,
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					success: function (data, textStatus, jqXHR) {
					var message = jqXHR.responseText;
					//                                               alert(message);
					//                                               var temp = message.split('/');
					//                                               var p = temp[11];
					//                                               $("#messages").append("<li>" + p + "</li>");
					if (message == 0) {
					$("#messages").text("Uploads added failed ");
					} else {
					//                                                insertStatus = message;
					$("#messages").text("Uploads Successfully ");
					}

					},
					error: function (jqXHR, textStatus, errorThrown) {
					$("#messages").append("<li style='color: red;'>" + textStatus + "</li>");
					}
					});
					}
					count++;
					});
	//                                document.customerContract.action = '/throttle/manageCustomerContract.do';
	//                                document.customerContract.submit();
					}
			</script>
			</div>
			<div id="Visadetails" class="tab-pane">
			    <table class="table table-info mb30 table-hover" id="bg">
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.VisaProvider" text="default text"/>:</td>
				    <td  height="30">
					<input type="text" name="visaProvider" id="visaProvider" size="20" class="form-control" style="width:220px;height:40px;" maxlength="35" onKeyPress="return onKeyPressBlockNumbers (event);" value="<c:out value="${visaProvider}"/>">
					<!--<input type="file" id="employeePhoto" name="employeePhoto" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0">-->
				    </td>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.VisaType" text="default text"/>:</td>
				    <td  height="30">
					<select name="visaType" id="visaType" class="form-control" style="width:220px;height:40px;">

					    <option value="0" selected>-<spring:message code="hrms.label.Select" text="default text"/>-</option>
					    <option value="1"><spring:message code="hrms.label.BusinessVisa" text="default text"/></option>
					    <option value="2" ><spring:message code="hrms.label.EmploymentVisa" text="default text"/></option>
					    <option value="3"><spring:message code="hrms.label.TransitVisa" text="default text"/> </option>

					</select>
					<script>
					  document.getElementById("visaType").value = <c:out value="${visaType}"/>;   
					</script>
				    </td>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DateOfIssue" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="visaDateOfIssue" class="form-control" style="width:220px;height:40px;" id="visaDateOfIssue" size="20" class="datepicker" value="<c:out value="${visaDateOfIssue}"/>"  >
				    </td>
                                     <script type="text/javascript">
				    $("#visaDateOfIssue").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.DaysofValidity" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="visaDateOfValidity" id="visaDateOfValidity" size="20" maxlength="3" class="datepicker" style="width:220px;height:40px;" onKeyPress="return onKeyPressBlockCharacters (event);" value="<c:out value="${visaDateOfValidity}"/>" >
				    </td>
                                           <script type="text/javascript">
				    $("#visaDateOfValidity").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				</tr>
				<tr>
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.CountryEnteringDate" text="default text"/></td>
				    <td  height="30">
					<input type="text" name="countryEnteringDate" class="form-control" style="width:220px;height:40px;" id="countryEnteringDate" size="20" class="datepicker"  value="<c:out value="${countryEnteringDate}"/>" >
				    </td>
                                    <script type="text/javascript">
				    $("#countryEnteringDate").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				    <td  height="30"><font color="red">*</font><spring:message code="hrms.label.VisaExpireDate" text="default text"/></td>
				    <td  height="30">
					<input type="text" class="form-control" style="width:220px;height:40px;" name="visaValidity" id="visaValidity" size="20" class="datepicker" value="<c:out value="${visaValidity}"/>"  >
				    </td>
                                    <script type="text/javascript">
				    $("#visaValidity").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
				</script> 
				</tr>

			    </table>
			    <br>
			    <br>
			    <table class="table table-info mb30 table-hover" id="bg" border="1">
				<tr>
				<td><spring:message code="hrms.label.SNo" text="default text"/></td>
				<c:if test="${visaDetails != null}">
				<td><spring:message code="hrms.label.FileName" text="default text"/></td>
				</c:if>
				<td><spring:message code="hrms.label.File" text="default text"/></td>
				<td colspan="3"><spring:message code="hrms.label.Remarks" text="default text"/></td>

			    </tr>
				<tr>
				<td>&nbsp;1:</td>
				 <c:if test="${visaDetails != null}">
				     <td> <img src="/throttle/displayLogoBlobDataVisa.do?visaId=<c:out value ="${visaId}"/>"  style="width: 200px;height: 30px" data-toggle="modal" data-target="#myModalVisa" title=""/></td>
				 <input type="hidden" id="visaId" name="visaId" value="<c:out value="${visaId}"/>" >
				 </c:if>
				<td><input type="file" name="visaPhoto" id="visaPhoto" size="45"  multiple style="width:500px;" /> 
				<!--<td><input type="file" name="file" size="45"  multiple style="width:500px;" />--> 
				<!--<td ><spring:message code="hrms.label.Remarks" text="default text"/></td>-->
				<td  colspan="3"><textarea cols="120" rows="2" class="form-control" style="width:200px;height:40px;" name="visaRemarks" id="visaRemarks" value="<c:out value="${visaRemarks}"/>" ></textarea> </td>
				</tr>
				<tr>
			    </table>

			    <div id="myModalVisa" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%;height: 100%">
				    <div class="modal-dialog">
					<div class="modal-content">
					    <div class="modal-body">
						<img src="/throttle/displayLogoBlobDataVisa.do?visaId=<c:out value ="${visaId}"/>" class="img-responsive" title="">
					    </div>
					</div>
				    </div>
				</div>
			    <center>
				<font color="green" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
				<ul id="messages" ></ul>&nbsp;&nbsp;
				</font>
			    </center>
			    <br>        
			    <br>        
			    <center>
				
				<!--<a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>-->
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
			    </center>  
                             <br> 
                             <br> 
			    <center>
				<c:if test = "${staffId == null}" >
				    <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.SAVE" text="default text"/>" name="Save" onClick="submitPage(this.name)">
				</c:if>
				<c:if test = "${staffId != null}" >
				    <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.Alter" text="default text"/>" name="alter" onClick="submitPage(this.name)">
				</c:if>
			    </center>  

			</div>
			<div id="Attachments" class="tab-pane">
			     <table class="table table-info mb30 table-hover" id="bg">
				<tr>
				    <td><font color="red">*</font><spring:message code="hrms.label.EmployeePhoto" text="default text"/></td>
				    <c:if test="${StaffList != null}">
				   <td> <img src="/throttle/displayLogoBlobDataEmp.do?staffId=<c:out value ="${staffId}"/>"  style="width: 30px;height: 30px" data-toggle="modal" data-target="#myModalEmp" title=""/></td>
				    </c:if>
				    <td>
					<input type="hidden" name="empCode" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" readonly value="0">
					<input type="file" id="employeePhoto" name="employeePhoto" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0" style="width:500px;">
				    </td>
				    <!--<td><p><font size="2" color="blue">File Size !</font></p></td>-->

				</tr>
				<tr id="drivingLicenceInfo3" style="display: none">
				    <td><font color="red">*</font><spring:message code="hrms.label.LicensePhoto" text="default text"/></td>
				     <c:if test="${StaffList != null}">
				     <td> <img src="/throttle/displayLogoBlobDataLicense.do?staffId=<c:out value ="${staffId}"/>"  style="width: 30px;height: 30px" data-toggle="modal" data-target="#myModalLicense" title=""/></td>
				     </c:if>
				    <td>
					<input type="file" id="employeeLicense" name="employeeLicense" size="20" class="form-control" style="width:220px;height:40px;" maxlength="10" value="0" style="width:500px;">
				    </td>

				</tr>
			    </table>
			    <div id="myModalEmp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
					<div class="modal-content">
					    <div class="modal-body">
						<img src="/throttle/displayLogoBlobDataEmp.do?staffId=<c:out value ="${staffId}"/>" class="img-responsive" title="<c:out value ="${remarks}"/>">
					    </div>
					</div>
				    </div>
			    </div>
			    <div id="myModalLicense" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
					<div class="modal-content">
					    <div class="modal-body">
						<img src="/throttle/displayLogoBlobDataLicense.do?staffId=<c:out value ="${staffId}"/>" class="img-responsive" title="<c:out value ="${remarks}"/>">
					    </div>
					</div>
				    </div>
			    </div>                
			    <br>        
			    <br> 
			    <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;"/></a>
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>

			    </center>

			</div>
		    </div>
		    <br>
		    <SCRIPT language="javascript">
			var poItems = 1;
			var rowCount = 2;
			var sno = 1;
			var snumber = 1;

			function addRow()
			{
			    //   alert(rowCount);
			    if (sno < 20) {
				sno++;
				var tab = document.getElementById("allowanceTBL");
				rowCount = tab.rows.length;

				snumber = (rowCount) - 1;
				if (snumber == 1) {
				    snumber = parseInt(rowCount);
				} else {
				    snumber++;
				}
				//alert( (rowCount)-1) ;
				//alert(rowCount);
				var newrow = tab.insertRow((rowCount));
				newrow.height = "30px";
				// var temp = sno1-1;
				var cell = newrow.insertCell(0);
				var cell0 = "<td class='text1'> " + snumber + "</td>";
				//cell.setAttribute(cssAttributeName,"text1");
				cell.innerHTML = cell0;

				cell = newrow.insertCell(1);
				cell0 = "<td class='text1'><input type='hidden' name='empRelationIds' id='empRelationIds" + snumber + "' value=''  class='textbox' /><input type='text' name='relationName' id='relationName" + snumber + "' class='textbox' style='width:220px;height:40px;'/></td>";
				//cell.setAttribute(cssAttributeName,"text1");
				cell.innerHTML = cell0;

				cell = newrow.insertCell(2);
				cell0 = "<td class='text1'><select class='textbox' name='relation' id='relation" + snumber + "' style='width:220px;height:40px;'><option value='0'>-Select-</option><c:if test = "${relationList!= null}" ><c:forEach  items="${relationList}" var="rel"><option value='<c:out value="${rel.relationId}" />'><c:out value="${rel.relationName}" /></option></c:forEach ></c:if></select></td>";
				//cell.setAttribute(cssAttributeName,"text1");
				cell.innerHTML = cell0;

				cell = newrow.insertCell(3);
				cell0 = "<td class='text1'><input type='text' class='datepicker' name='relationDOB' id='relationDOB" + snumber + "'  onchange='showAge(" + snumber + ")' style='width:220px;height:40px;'/></td>";
				//cell.setAttribute(cssAttributeName,"text1");
				cell.innerHTML = cell0;
                                   // alert('hai' + snumber);
			         callDatePick(snumber);
//				$('#relationDOB1').datepicker({
//				    yearRange: '1900:+nn', changeMonth: true, changeYear: true
//				});

				cell = newrow.insertCell(4);
				cell0 = "<td class='text1'><input type='text' name='relationAge' id='relationAge" + snumber + "'  class='textbox' style='width:220px;height:40px;'/></td>";
				//cell.setAttribute(cssAttributeName,"text1");
				cell.innerHTML = cell0;
				if (rowCount == 1) {
				    cell = newrow.insertCell(5);
				    cell0 = "<td class='text1'</td>";
				    //cell.setAttribute(cssAttributeName,"text1");
				    cell.innerHTML = cell0;
				} else {
				    cell = newrow.insertCell(5);
				    cell0 = "<td class='text1'><input type='checkbox' name='delete' id='delete" + snumber + "'  /></td>";
				    //cell.setAttribute(cssAttributeName,"text1");
				    cell.innerHTML = cell0;
				}
				rowCount++;
                               

			    }

//			    $(document).ready(function() {
//				//alert('hai');
//				$("#datepicker").datepicker({
//				    showOn: "button",
//				    buttonImage: "calendar.gif",
//				    buttonImageOnly: true
//
//				});
//
//
//
//			    });
//
//			    $(function() {
//				//	alert("cv");
//				$(".datepicker").datepicker({
//				    /*altField: "#alternate",
//				     altFormat: "DD, d MM, yy"*/
//				    changeMonth: true, changeYear: true
//				});
//
//			    });
                            
			}
         function callDatePick(sno){
         alert("relationDOB"+sno)
         $("#relationDOB1").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
				    });
         
         }
			function deleteRow() {
			    try {
				var table = document.getElementById("allowanceTBL");
				rowCount = table.rows.length;
				for (var i = 0; i < rowCount; i++) {
				    var row = table.rows[i];
				    var chkbox = row.cells[5].childNodes[0];
				    if (null != chkbox && true == chkbox.checked) {
					if (rowCount <= 1) {
					    alert("Cannot delete all the rows.");
					    break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
					snumber = i;
				    }
	//                        alert(snumber);
				}
			    } catch (e) {
				alert(e);
			    }
			}
			function showAge(value) {
	//                    alert("value"+value);
			    var dob = $('#relationDOB' + value).val();
			    if (dob != '') {
				var today = new Date();
				today = today.getFullYear();
	//                        alert(today);
	//                        alert(dob);
				var dobYear = dob.split("-");
	//                        alert(dobYear[2]);

				var age = Math.floor((today - dobYear[2]));

	//                        alert(age);
				$('#relationAge' + value).val(age);

			    }


			}

			function getNomineeAge(value) {
			    if (value != '') {
				var today = new Date();
				today = today.getFullYear();
	//                        alert(today);
				var dobYear = value.split("-");
	//                        alert(dobYear[2]);

				var age = Math.floor((today - dobYear[2]));

	//                        alert(age);
				$('#nomineeAge').val(age);

			    }
			}
			function showTab() {
	//                    alert("Hi I am here ");
			    if (document.addLect.userStatus.checked == true) {
				document.getElementById("userStat").style.visibility = "visible";
				document.getElementById("userStat1").style.visibility = "visible";
				document.getElementById("userStat2").style.visibility = "visible";
				document.getElementById("userStatus1").value = 1;
			    } else {
				document.getElementById("userStat").style.visibility = "hidden";
				document.getElementById("userStat1").style.visibility = "hidden";
				document.getElementById("userStat2").style.visibility = "hidden";
				document.getElementById("userStatus1").value = "";
			    }
			}
			function viewEmployeePhotos(staffId) {
			    window.open('/throttle/content/employee/displayBlobData.jsp?staffId=' + staffId + '&empPhoto=1', 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
			}
			function viewEmployeeLicense(staffId) {
			    window.open('/throttle/content/employee/displayBlobData.jsp?staffId=' + staffId + '&empPhoto=2', 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
			}
		    </SCRIPT>
		    <script>
                        $('.btnNext').click(function(){
                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });
                            $('.btnPrevious').click(function(){
                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
//			$(".nexttab").click(function() {
//			    var selected = $("#tabs").tabs("option", "selected");
//			    $("#tabs").tabs("option", "selected", selected + 1);
//			});
//			$(".prevtab").click(function() {
//			    var selected = $("#tabs").tabs("option", "selected");
//			    $("#tabs").tabs("option", "selected", selected - 1);
//			});
		    </script>
		    <script>
			$(document).ready(function() {
			    if (document.getElementById("staffId").value == "" || document.getElementById("staffId").value == null)
			    {
				//alert(document.getElementById("appUserName").value);
				document.getElementById("userName").value = "";
	//                        document.getElementById("appUserName").value = "";
				document.getElementById("password").value = "";
				document.getElementById("roleId").value = "0";
	//                        document.getElementById("branchId").value = "0";
	//                        document.getElementById("companyId").value = "0";
			    }
			});
		    </script>
		<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
	    </body>
	</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>