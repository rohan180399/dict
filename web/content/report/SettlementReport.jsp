<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" charset="utf-8">
            $(function () {
                var tabContainers = $('div.tabs > div');
                tabContainers.hide().filter(':first').show();

                $('div.tabs ul.tabNavigation a').click(function () {
                    tabContainers.hide();
                    tabContainers.filter(this.hash).show();
                    $('div.tabs ul.tabNavigation a').removeClass('selected');
                    $(this).addClass('selected');
                    return false;
                }).filter(':first').click();
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <style type="text/css" media="screen">

            UL.tabNavigation {
                list-style: none;
                margin: 0;
                padding: 0;
            }

            UL.tabNavigation LI {
                display: inline;
            }

            UL.tabNavigation LI A {
                padding: 3px 5px;
                background-color: #60a3e8;
                color: #000;
                text-decoration: none;
            }

            UL.tabNavigation LI A.selected,
            UL.tabNavigation LI A:hover {
                background-color: #76b3f1;
                color: #fff;
                padding-top: 7px;
            }

            UL.tabNavigation LI A:focus {
                outline: 0;
            }

            div.tabs > div {
                padding: 5px;
                margin-top: 3px;
            }

            div.tabs > div h2 {
                margin-top: 0;
                width:98%;
            }

            #first {
                background-color: #76b3f1;
            }

            #second {
                background-color: #76b3f1;
            }

            #third {
                background-color: #76b3f1;
            }

            #exp {
                height:55px;
                background:#e7f3ff;
            }

            .txt1 {
                color:#a0a9b8;
            }

            .txt1 a {
                color:#a0a9b8;
                text-decoration:none;
            }
            .txt1 a:hover {
                color:#000;
                text-decoration:underline;
            }


        </style>
    </head>
    <script type="text/javascript">
        function submitPage(value)
        {
            var fromDate=document.driverSettlementRpt.fromDate.value;
            var toDate=document.driverSettlementRpt.toDate.value;
            var regno=document.driverSettlementRpt.regno.value;
            var driName=document.driverSettlementRpt.driName.value;
            document.driverSettlementRpt.settlementId.value=value;

            if((!(isEmpty(fromDate) && isEmpty(toDate)) || !isEmpty(regno) || !isEmpty(driName) ))  {
                document.driverSettlementRpt.action="/throttle/driverSettlementReport.do";
                document.getElementById('exp_table').style.display='none';
                document.driverSettlementRpt.submit();
            }else {
                alert("Please enter any one value ");
            }
        }
        function setFocus()
        {
            var sdate='<%=request.getAttribute("fromDate")%>';
            var edate='<%=request.getAttribute("toDate")%>';
            var regno='<%=request.getAttribute("regNo")%>';
            var driName='<%=request.getAttribute("driName")%>';
            if(sdate!='null' && edate!='null'){
                document.driverSettlementRpt.fromDate.value=sdate;
                document.driverSettlementRpt.toDate.value=edate;
            } if(regno!='null'){
                document.driverSettlementRpt.regno.value=regno;
            }
            if(driName!='null'){
                document.driverSettlementRpt.driName.value=driName;
            }
        }

        window.onload = getDriverName;
        function getDriverName(){
        var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));
        }
        window.onload = getVehicleNos;
        function getVehicleNos(){
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        }



        function displayCollapse(){
            if(document.getElementById("exp_table").style.display=="block"){
                document.getElementById("exp_table").style.display="none";
                document.getElementById("openClose").innerHTML="Open";
            } else{
                document.getElementById("exp_table").style.display="block";
                document.getElementById("openClose").innerHTML="Close";
            }
        }
    </script>
    <body onload="setFocus();getVehicleNos();">
        <form name="driverSettlementRpt" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="950" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="form-control" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                        </td>
                                        <td>Vehicle No</td>
                                        <td><input name="regno" id="regno" type="text" class="form-control" size="20" value="" onKeyPress="getVehicleNos();" autocomplete="off"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.driverSettlementRpt.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.driverSettlementRpt.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <c:if test = "${settlementReport != null}" >
                <%
                    int index = 0;
                    ArrayList settlementReport = (ArrayList) request.getAttribute("settlementReport");
                    if (settlementReport.size() != 0) {
                %>
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="2" id="bg">
                    <tr>
                        <td class="contentsub" height="30">Driver Name</td>
                        <td class="contentsub" height="30">From Date</td>
                        <td class="contentsub" height="30">To Date</td>
                        <td class="contentsub" height="30">Vehicle No</td>
                        <td class="contentsub" height="30">Total Days</td>
                        <td class="contentsub" height="30">Running KM</td>
                        <td class="contentsub" height="30">No of Trips</td>
                        <td class="contentsub" height="30">Total Ton Amount</td>
                        <td class="contentsub" height="30">Total Diesel</td>
                        <td class="contentsub" height="30">Diesel Amount</td>
                        <!--<td class="contentsub" height="30">Income</td>-->
                        <td class="contentsub" height="30">Total Expenses</td>
                        <td class="contentsub" height="30">Driver Advance</td>
                        <!--<td class="contentsub" height="30">Driver Cleaner Expenses</td>
                        <td class="contentsub" height="30">driver Bata Salary</td>-->
                        <td class="contentsub" height="30">Total Salary</td>
                        <td class="contentsub" height="30">Due From Driver</td>
                        <td class="contentsub" height="30">Total Amount</td>
                    </tr>
                    <c:forEach items="${settlementReport}" var="sett">
                        <c:set var="total" value="${total+1}"></c:set>
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.driverName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.fromDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.toDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.vehicleNo}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.totalDays}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.runningKm}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.noOfTrips}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.totalTonnageAmount}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.totalDiesel}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.dieselAmount}"/></td>
                            <!--<td class="<%=classText%>"  height="30"><c:out value="${sett.income}"/></td>-->
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.totalExpenses}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.driverAdvance}"/></td>
                            <!--<td class="<%=classText%>"  height="30"><c:out value="${sett.driverCleanerExpenses}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${sett.driverBataSalary}"/></td>-->
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.totalSalary}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.dDueFromDriver}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${sett.totalAmount}"/></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <%
                            }
                %>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>