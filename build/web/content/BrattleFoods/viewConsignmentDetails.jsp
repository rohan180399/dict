<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript" language="javascript">
    function submitPage() {
        if (document.getElementById('consignmentStatusId').value == '0') {
            alert("please select status");
            document.getElementById('consignmentStatusId').focus();
        } else if (document.getElementById('cancelRemarks').value == '') {
            alert("please enter the remarks");
            document.getElementById('cancelRemarks').focus();
        } else {
            document.cNote.action = '/throttle/cancelConsignment.do';
            document.cNote.submit();
        }
    }

</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> CNote</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">View Consignment Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">



            <body>
                <form name="cNote" method="post">
                    <c:set var="orderStatus" value="0" />
                    <c:if test="${consignmentList != null}">
                        <c:forEach items="${consignmentList}" var="cnote">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Consignment Note <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${cnote.consignmentOrderId}"/>"/></td></tr>
                                <tr>
                                    <td  >Entry Option</td>
                                    <td  >
                                        <c:if test="${cnote.entryType == 1}">
                                            <label>Manual</label>
                                        </c:if>
                                        <c:if test="${cnote.entryType == 2}">
                                            <label>Excel Upload</label>
                                        </c:if>
                                    </td>
                                    <td   colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td  >Consignment Note </td>
                                    <td  ><label><c:out value="${cnote.consignmentNoteNo}"/></label></td>
                                    <td  >Consignment Date</td>
                                    <td  ><label><c:out value="${cnote.consignmentOrderDate}"/></label></td>
                                            <c:set var="orderStatus" value="${cnote.consigmentOrderStatus}" />
                                <tr>
                                <tr>
                                    <td  >Customer Reference No</td>
                                    <td  ><label><c:out value="${cnote.consignmentRefNo}"/></label></td>
                                    <td  >Customer Reference  Remarks</td>
                                    <td  ><label><c:out value="${cnote.consignmentOrderRefRemarks}"/></label></td>
                                <tr>
                                    <td  >Customer Type</td>
                                    <td  >
                                        <c:if test="${customerTypeList != null}">
                                            <c:forEach items="${customerTypeList}" var="cusType">
                                                <c:if test="${cnote.customerTypeId == cusType.customerTypeId}">
                                                    <label><c:out value="${cusType.customerTypeName}"/></label>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </td>
                                    <td  >Product Category</td>
                                    <td  >
                                        <c:if test="${productCategoryList != null}">
                                            <c:forEach items="${productCategoryList}" var="proList">
                                                <c:if test="${cnote.productCategoryId == proList.productCategoryId}">
                                                    <label><c:out value="${proList.customerTypeName}"/></label>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </td>
                            </table>
                            <br>
                            <br>

                            <div id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active" data-toggle="tab"><a href="#customerDetail"><span>Basic Details</span></a></li>
                                    <li data-toggle="tab"><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>
                                    <li data-toggle="tab"><a href="#paymentDetails"><span>Invoice Info</span></a></li>
                                        <c:if test="${orderStatus == 5 || orderStatus == 3 || orderStatus == 6}">
                                        <li><a href="#action"><span>Action</span></a></li>
                                        </c:if>

                                </ul>

                                <div id="customerDetail">
                                    <table class="table table-info mb30 table-hover" style="width:100%">
                                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" >Contract Customer Details</td></tr>
                                        <tr>
                                            <td  >Name</td>
                                            <td  ><label><c:out value="${cnote.customerName}"/></label></td>
                                            <td  >Code</td>
                                            <td  ><label><c:out value="${cnote.customerAddress}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td  >Address</td>
                                            <td  ><label><c:out value="${cnote.customerAddress}"/></label></td>
                                            <td  >Pincode</td>
                                            <td  ><label><c:out value="${cnote.customerPincode}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td  >Mobile No</td>
                                            <td  ><label><c:out value="${cnote.customerMobile}"/></label></td>
                                            <td  >E-Mail ID</td>
                                            <td  ><label><c:out value="${cnote.customerEmail}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td  >Phone No</td>
                                            <td  ><label><c:out value="${cnote.customerPhone}"/></label></td>

                                            <td  >Billing Type</td>
                                            <td  >
                                                <c:if test="${billingTypeList != null}">
                                                    <c:forEach items="${billingTypeList}" var="billType">
                                                        <c:if test="${cnote.customerBillingType == billType.billingTypeId}">
                                                            <label><c:out value="${billType.billingTypeName}"/></label>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </td>

                                        </tr>
                                    </table>
                                    <c:if test="${cnote.customerTypeId == '1'}" >      
                                        <div id="contractDetails" style="display: none">
                                            <table>
                                                <tr>
                                                    <td  >Contract No :</td>
                                                    <td  ><label><c:out value="${cnote.customerContractId}"/></label></td>
                                                    <td  >Contract Expiry Date :</td>
                                                    <td  ><label><c:out value="${cnote.contractFrom}"/></label></td>
                                                    <td   >&nbsp;&nbsp;&nbsp;</td>
                                                    <td  ><input type="button" class="btn btn-info" value="View Contract" onclick="viewCustomerContract()"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <script>
                                            function viewCustomerContract() {
                                                window.open('/throttle/viewCustomerContract.do?custId=' +<c:out value="${cnote.customerContractId}"/>, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                            }
                                        </script>
                                    </c:if>            
                                    <br>
                                    <br>

                                    <table class="table table-info mb30 table-hover" style="width:100%">
                                        <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"  colspan="6" >Consignment Details</td></tr>
                                        <tr>
                                            <c:if test="${cnote.customerBillingType eq 3 && cnote.customerTypeId eq 1}">
                                                <td  >Origin</td>
                                                <td  ><label><c:out value="${cnote.origin}"/></label></td>
                                                <td  >Destination</td>
                                                <td  ><label><c:out value="${cnote.destination}"/></label></td>
                                                    </c:if>
                                                    <c:if test="${cnote.customerTypeId eq 2}">
                                                <td  >Origin</td>
                                                <td  ><label><c:out value="${cnote.origin}"/></label></td>
                                                <td  >Destination</td>
                                                <td  ><label><c:out value="${cnote.destination}"/></label></td>
                                                    </c:if>
                                                    <c:if test="${cnote.customerBillingType ne 3 && cnote.customerTypeId eq 1}">
                                                <td  >Contract Route </td>
                                                <td  ><label><c:out value="${cnote.routeContractCode}"/></label></td>
                                                <td   colspan="2">&nbsp;</td>
                                            </c:if>
                                            <td  >Business Type</td>
                                            <td  >
                                                <label>
                                                    <c:if test="${cnote.budinessType == 1}">
                                                        Primary
                                                    </c:if>
                                                    <c:if test="${cnote.budinessType == 2}">
                                                        Secondary
                                                    </c:if>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >Multi Pickup</td>
                                            <td  ><label>
                                                    <c:if test="${cnote.multiPickup == 'Y'}">
                                                        Yes
                                                    </c:if>
                                                    <c:if test="${cnote.multiPickup == 'N'}">
                                                        No
                                                    </c:if>
                                                </label></td>
                                            <td  >Multi Delivery</td>
                                            <td  ><label>
                                                    <c:if test="${cnote.multiDelivery == 'Y'}">
                                                        Yes
                                                    </c:if>
                                                    <c:if test="${cnote.multiDelivery == 'N'}">
                                                        No
                                                    </c:if>
                                                </label></td>
                                            <td  >Special Instruction</td>
                                            <td  ><label><c:out value="${cnote.consigmentInstruction}"/></label></td>
                                        </tr>

                                        <tr>
                                            <td colspan="4" >

                                                <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                                    <tr id="tableDesingTH" height="30">
                                                    <td width="20" align="center" height="30" >Sno</td>
                                                        <td height="30" >Product/Article Code</td>
                                                        <td height="30" >Product/Article Name </td>
                                                        <td height="30" >Batch Code</td>
                                                        <td height="30" >No of Packages</td>
                                                        <td height="30" >UOM</td>
                                                        <td height="30" >Total Weight (in Kg)</td>
                                                    </tr>
                                                    <% int i = 1;%>
                                                    <c:if test="${consignmentArticles != null}">
                                                        <tbody>
                                                            <c:forEach items="${consignmentArticles}" var="article">
                                                                <tr>
                                                                    <td   height="30" ><label><%=i++%></label></td>
                                                                    <td   height="30" ><label><c:out value="${article.articleCode}"/></label></td>
                                                                    <td   height="30" ><label><c:out value="${article.articleName}"/></label></td>
                                                                    <td   height="30" ><label><c:out value="${article.batchName}"/></label></td>
                                                                    <td   height="30" ><label><c:out value="${article.packageNos}"/></label></td>
                                                                    <td   height="30" >
                                                                        <c:if test="${article.unitOfMeasurement == 1}">
                                                                            Box
                                                                        </c:if>
                                                                        <c:if test="${article.unitOfMeasurement == 2}">
                                                                            Bag
                                                                        </c:if>
                                                                        <c:if test="${article.unitOfMeasurement == 3}">
                                                                            Pallet
                                                                        </c:if>
                                                                        <c:if test="${article.unitOfMeasurement == 4}">
                                                                            Each
                                                                        </c:if>
                                                                    </td>
                                                                    <td   height="30" ><label><c:out value="${article.packageWeight}"/></label></td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </c:if>
                                                </table>
                                                <br>
                                                <br>
                                            </td>
                                            <td colspan="2">
                                                <table  class="table table-info mb30 table-hover" style="width:100%">
                                                    <tr>
                                                        <td colspan="2" height="100">

                                                        </td>
                                                    </tr>
                                                    <tr id="tableDesingTH" height="30">
                                                        <td>
                                                            <label >Total No Packages</label>
                                                            <label><c:out value="${cnote.totalPackages}"/></label>
                                                        </td>
                                                        <td>
                                                            <label >Total Weight (Kg)</label>
                                                            <label><c:out value="${cnote.totalWeight}"/></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <table class="table table-info mb30 table-hover" style="width:100%" id="bg">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th colspan="6" >Vehicle (Required) Details</th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td >Service Type</td>
                                            <td><label>
                                                    <c:if test="${cnote.serviceType == 1}">
                                                        FTL
                                                    </c:if>
                                                    <c:if test="${cnote.serviceType == 2}">
                                                        LTL
                                                    </c:if>
                                                </label>
                                            </td>
                                            <td >Vehicle Type</td>
                                            <td > <c:if test="${vehicleTypeList != null}">
                                                    <c:forEach items="${vehicleTypeList}" var="vehType">
                                                        <c:if test="${cnote.vehicleTypeId == vehType.vehicleTypeId}">
                                                            <label><c:out value="${vehType.vehicleTypeName}"/></label>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if></label></td>
                                            <td >Reefer Required</td>
                                            <td ><label><label>
                                                        <c:if test="${cnote.reeferRequired == 'Yes'}">
                                                            Yes
                                                        </c:if>
                                                        <c:if test="${cnote.reeferRequired == 'No'}">
                                                            No
                                                        </c:if>
                                                    </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >Vehicle Required Date</td>
                                            <td ><label><c:out value="${cnote.vehicleRequiredDate}"/></label></td>
                                            <td >Vehicle Required Time</td>
                                            <td ><label><c:out value="${cnote.vehicleRequiredTime}"/></label></td>
                                            <td >Special Instruction</td>
                                            <td ><label><c:out value="${cnote.vehicleInstruction}"/></label></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <table   class="table table-info mb30 table-hover" style="width:100%" id="bg">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th  height="30" colspan="6">Consignor Details</th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td  >Consignor Name</td>
                                            <td  ><label><c:out value="${cnote.consignorName}"/></label></td>
                                            <td  >Mobile No</td>
                                            <td  ><label><c:out value="${cnote.consignorMobile}"/></label></td>
                                            <td  >Address</td>
                                            <td  ><label><c:out value="${cnote.consignorAddress}"/></label></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <table   class="table table-info mb30 table-hover" style="width:100%" id="bg">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th height="30" colspan="6">Consignee Details</th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td  >Consignee Name</td>
                                            <td  ><label><c:out value="${cnote.consigneeName}"/></label></td>
                                            <td  >Mobile No</td>
                                            <td  ><label><c:out value="${cnote.consigneeMobile}"/></label></td>
                                            <td  >Address</td>
                                            <td  ><label><c:out value="${cnote.consigneeAddress}"/></label></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                </div>



                                <div id="routeDetail">
                                    <table  class="table table-info mb30 table-hover" style="width:100%">
                                       <thead>
                                        <tr id="tableDesingTH" height="30">
                                                <th height="30" >Point Name</th>
                                                <th height="30" >Type</th>
                                                <th height="30" >Route Order</th>
                                                <th height="30" >Address</th>
                                                <th height="30" >Planned Date</th>
                                                <th height="30" >Planned Time</th>
                                            </tr>
                                        </thead>
                                        <c:if test="${consignmentPoint != null}">
                                            <tbody>
                                                <c:forEach items="${consignmentPoint}" var="point">
                                                    <tr>
                                                        <td   height="30" ><label><c:out value="${point.cityName}"/></label></td>
                                                        <td   height="30" ><label><c:out value="${point.consignmentPointType}"/></label></td>
                                                        <td   height="30" ><label><c:out value="${point.pointSequence}"/></label></td>
                                                        <td   height="30" ><label><c:out value="${point.pointAddress}"/></label></td>
                                                        <td   height="30" ><label><c:out value="${point.pointDate}"/></label></td>
                                                        <td   height="30" ><label><c:out value="${point.pointPlanTime}"/></label></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </c:if>
                                    </table>
                                    <br>
                                    <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                    <br>
                                    <br>
                                </div>




                                <div id="paymentDetails">
                                    <table   class="table table-info mb30 table-hover" style="width:100%" id="contractFreightTable">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th height="30" colspan="2" >Contract Freight Details</th>
                                        </tr>
                                        </thead>
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th  >Freight Charges</th>
                                            <th >INR. <label><c:out value="${cnote.freightCharges}"/></label></th>
                                        </tr>
                                         </thead>
                                        <tr>
                                            <td  >Sub Total</td>
                                            <td  >INR. <label><c:out value="${cnote.totalStatndardCharges}"/></label></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <table   class="table table-info mb30 table-hover" style="width:100%" id="bg">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                            <th  height="30" colspan="6" align="right">Total Charges</th>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <th class="contentsub"  height="30" align="right">INR.<label><c:out value="${cnote.totalCharges}"/></label></th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <!--<a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>-->
                                </center>
                                </div>
                                <script>
//                                    $(".nexttab").click(function() {
//                                        var selected = $("#tabs").tabs("option", "selected");
//                                        $("#tabs").tabs("option", "selected", selected + 1);
//                                    });
                                    $('.btnNext').click(function() {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                $('.btnPrevious').click(function() {
                                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                });
                                </script>

                                <c:if test="${orderStatus == 5 || orderStatus == 3 || orderStatus == 6}">
                                    <div id="action">
                                        <table  class="table table-info mb30 table-hover" style="width:100%" id="contractFreightTable">
                                        <thead>
                                        <tr id="tableDesingTH" height="30">
                                                <th  height="30" colspan="4" >Consignment Action</th>
                                            </tr>
                                        </thead>
                                            <tr>
                                                <td  >Consignment Status</td>
                                                <td  ><select name="consignmentStatusId" id="consignmentStatusId" class="form-control" style="width:180px;height:40px;" onchange="updateConsignment(this.value)">
                                                        <option value="0">--Select--</option>
                                                       
                                                        <c:if test="${orderStatus == 5}">
                                                            <option value="4">Cancel Consignment</option>
                                                            <option value="3">Hold Consignment</option>
                                                        </c:if>
                                                        <c:if test="${orderStatus == 3}">
                                                            <option value="4">Cancel Consignment</option>
                                                            <option value="<c:out value="${cnote.nextStatusId}"/>">UnHold Consignment</option>
                                                        </c:if>
                                                    </select></td>
                                                <td  >Remarks</td>
                                                <td  ><textarea name="cancelRemarks" id="cancelRemarks" cols="100" rows="6" class="form-control" style="width:180px;height:80px;"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                            <center>
                                                <input type="button" class="btn btn-info" value="Save" name="Save" onclick="submitPage()" style="display:none;" id="saveConsignment">
                                            </center>
                                            </td>
                                            </tr>
                                        </table>
                                        <script>
                                            function updateConsignment(val) {
                                                if (val == '0') {
                                                    $("#saveConsignment").hide();
                                                    $("#cancelRemarks").readOnly = true;
                                                } else {
                                                    $("#saveConsignment").show();
                                                    $("#cancelRemarks").readOnly = false;
                                                }
                                            }
                                        </script>

                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>
                        <c:if test="${cancelConsignment != null}">
                            <script>
                                window.close();
                            </script>
                        </c:if>        
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>