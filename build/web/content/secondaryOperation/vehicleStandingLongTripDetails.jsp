<%--
    Document   : vehicleStandingLong
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <!--<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">-->
    <!--<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

</head>
<body>
    <form>
        <br>
        <br>
        <% int count = 0;%>
        <table width="700" align="center" border="0" id='tableProgress' class="sortable" style="width: 600px;">
            <thead>
                <tr height="70">
                    <th style="width: 90px;text-align: center;">S.No</th>
                    <th style="width: 90px;text-align: center;">Freeze In Date</th>
                    <th style="width: 90px;text-align: center;">Freeze In Time</th>
                    <th style="width: 90px;text-align: center;">Freeze Out Date</th>
                    <th style="width: 90px;text-align: center;">Freeze Out Time</th>
                    <th style="width: 90px;text-align: center;">Current Location</th>
                    <th style="width: 90px;text-align: center;">Remarks</th>
                </tr>
            </thead>
            <% int index = 0;
                int sno = 0;
            %>
            <c:if test="${vehicleTransitPeriodList != null}">
                <tbody>
                    <c:forEach items="${vehicleTransitPeriodList}" var="vsl">
                        <%
                            sno++;
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr height="30">
                            <td align="left" class="<%=classText%>"><%=sno%></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.freezeInDate}" /></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.freezeInTime}" /></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.freezeOutDate}" /></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.freezeOutTime}" /></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.currentLocation}" /></td>
                            <td align="left" class="<%=classText%>"><c:out value="${vsl.remarks}" /></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </tbody>
                <input type="hidden" name="count" id="count" value="<%=sno%>" />
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("tableProgress");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="200">200</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("tableProgress", 1);
            </script>
        </c:if>
        <br>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
