<%-- 
    Document   : CommodityInvoiceReportExcel
    Created on : 22 Jan, 2021, 10:44:01 AM
    Author     : thilak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
     

        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    
    <body>
        <form name="suppInvoiceReport" method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "CommodityInvoiceReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
           %>
           
            <br>
            <c:if test="${CommodityInvoiceReport != null}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>GR No</h3></th>
                            <th align="center"><h3>GR Date</h3></th>
                            <th align="center"><h3>Container No</h3></th>
                            <th align="center"><h3>Container Size</h3></th>
                            <th align="center"><h3>Movement</h3></th>
                            <th align="center"><h3>Commodity</h3></th>
                            <th align="center"><h3>Transporter</h3></th>
                            <th align="center"><h3>Route Info</h3></th>
                            <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>TT Type</h3></th>
                            <th align="center"><h3>Billing Party</h3></th>
                            <th align="center"><h3>Invoice Code</h3></th>
                            <th align="center"><h3>Invoice Date</h3></th>
                            <th align="center"><h3>Freight</h3></th>
                            <th align="center"><h3>Other Expense</h3></th>
                            <th align="center"><h3>Shipping Bill No</h3></th>
                            <th align="center"><h3>Bill Of Entry No</h3></th>
                            
                         </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${CommodityInvoiceReport}" var="csList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="40" >
                                <td class="<%=classText%>"><%=index%></td>
                                <td  class="<%=classText%>"><c:out value="${csList.grNo}"/></td>
                                <td   class="<%=classText%>"><c:out value="${csList.grDate}"/></td>
                                <td   class="<%=classText%>"><c:out value="${csList.containerNo}"/></td>
                                <td   class="<%=classText%>"><c:out value="${csList.containerSize}"/></td>
                              <td  class="<%=classText%>"><c:out value="${csList.movementType}"/>
                                <td class="<%=classText%>"><c:out value="${csList.commodity}"/></td>
                                <td   class="<%=classText%>" ><c:out value="${csList.transporter}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${csList.routeInfo}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${csList.vehicleNo}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${csList.ttType}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${csList.billingParty}"/>
                                  </td>
                                <td class="<%=classText%>"  ><c:out value="${csList.invoiceCode}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${csList.invoiceDate}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${csList.freight}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${csList.otherExpense}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${csList.shipingBillNo}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${csList.billOfEntryNo}"/></td>
                                
                                </tr>
                                <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>

               
            </c:if>
            <br>
            <br>
</form>
    </body>
</html>

