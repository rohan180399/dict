
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.fuelManagement.business.FuelManagementTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">
        var httpRequest;
        var temp = "";
       function reload(){
           window.location.reload();
          
           }
        function setVisibility(){
            if(document.fuelFilling.outFill.checked==true){
                if(textValidation(document.fuelFilling.regNo,'Vehicle No')){
                    document.fuelFilling.regNo.focus();
                    return 'false';
                    }
                window.open('/throttle/outFillFuel.do?regNo='+document.fuelFilling.regNo.value, 'PopupPage', 'height=450,width=550,scrollbars=yes,resizable=yes');
             
    }      
        
    }
    
    function calculate(){
        var price=document.fuelFilling.fuelPrice.value;
        document.fuelFilling.amount.value=(parseFloat(price)*parseFloat(document.fuelFilling.fuelFilled.value)).toFixed(2);        
        var total=parseInt(document.fuelFilling.present.value) - parseInt(document.fuelFilling.previous.value)
        
        var avg=total/(parseFloat(document.fuelFilling.lastFilled.value)+parseFloat(document.fuelFilling.fuelFilled.value));
       
       document.fuelFilling.totalKm.value=total;
        if(document.fuelFilling.previous.value=="0"){
        document.fuelFilling.avgKm.value="0";    
        document.fuelFilling.totalKm.value="0";
            }else{
        document.fuelFilling.avgKm.value=parseFloat(avg).toFixed(2);
        }if(!textValidation(document.fuelFilling.fuelFilled,'Fuel Filled')){
        if((parseInt(document.fuelFilling.fuelFilled.value) < parseInt(document.fuelFilling.tank.value))){
        document.fuelFilling.tankReading.value=parseFloat(document.fuelFilling.tank.value)-parseFloat(document.fuelFilling.fuelFilled.value);
        document.fuelFilling.runReading.value=parseFloat(document.fuelFilling.run.value)+parseFloat(document.fuelFilling.fuelFilled.value);
   
}else{
    alert("Fuel Filled Greater Than Tank reading");
    document.fuelFilling.fuelFilled.focus();
    }
    }
    }
    function submitPage(){
        var chek=validation();
        if(chek=='true'){
        document.fuelFilling.action='/throttle/addFuelFilling.do';
        document.fuelFilling.submit();
          
       
        }
        
    }
    function validation(){
        
        if(textValidation(document.fuelFilling.couponNo,'Coupon No')){
            return 'false';
            }
        if(textValidation(document.fuelFilling.regNo,'Vehicle No')){
            return 'false';
            }
        if(textValidation(document.fuelFilling.present,'Vehicle Current Km')){
            return 'false';
            }
        if(textValidation(document.fuelFilling.fuelFilled,'Fuel Filled')){
            return 'false';
            }
        if(textValidation(document.fuelFilling.date,'Date')){
            return 'false';
            }
            /*
        if(document.fuelFilling.outFill.checked==true){
            if(textValidation(document.fuelFilling.place,'Place')){
                return 'false';
                }

            if(textValidation(document.fuelFilling.fuel,'Fuel Filled')){
                return 'false';
                }

            if(textValidation(document.fuelFilling.outFillKm,'Meter Reading')){
                return 'false';
                }
            
            }
            */
            return 'true';
        
        }

       
    function reset(){
        document.fuelFilling.typeId.value='';
        document.fuelFilling.companyId.value='';
        document.fuelFilling.servicePtId.value='';
        document.fuelFilling.previous.value='';
        document.fuelFilling.present.value='';
        document.fuelFilling.regNo.value='';
        document.fuelFilling.amount.value='';
        document.fuelFilling.fuelFilled.value='';
        document.fuelFilling.fuel.value='';
        document.fuelFilling.outFill.checked=false;
        setVisibility();
        document.fuelFilling.outFillKm.value='';
        document.fuelFilling.totalKm.value='';
        document.fuelFilling.avgKm.value='';
        document.fuelFilling.date.value='';
        document.fuelFilling.couponNo.value='';
        
        
    }
    function getVehicleNos(){
        
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
    }    
    
    
    function ajax(regNo) {
        
        if(regNo!=''){
        var url='/throttle/getVehDetails.do?regNo='+regNo;
      
        if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
                {
                    httpRequest = new XMLHttpRequest();
                }
                
                httpRequest.open("POST", url, true);
                httpRequest.onreadystatechange = function() {processAjax();};
                httpRequest.send(null);
            }
            }
            
            function processAjax()
            {
                if (httpRequest.readyState == 4)
                    {
                        if(httpRequest.status == 200)
                            {
                                temp = httpRequest.responseText.valueOf();
                                
                                if(temp !=""){
                                var result=temp.split('-');                                
                                document.fuelFilling.typeId.value=result[1];
                                document.fuelFilling.servicePtId.value=result[2];
                                document.fuelFilling.companyId.value=result[3];
                                document.fuelFilling.previous.value=result[4];
                                document.fuelFilling.lastFilled.value=result[5];
                                
                                }else{
                                
                                alert("Please Config Vehicle Company");
                                }
                            }
                            else
                                {
                                    alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                }
                            }
                        }
        function setValues(){
           if('<%=request.getAttribute("regNo")%>'!='null'){
            document.fuelFilling.regNo.value='<%=request.getAttribute("regNo")%>';
            ajax('<%=request.getAttribute("regNo")%>');
                }
            }
                        
    </script>
    <body onload="getVehicleNos();setValues();">
        <form  name="fuelFilling" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" width="650"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
            <table align="center" width="600" class="border" border="0" cellspacing="0" cellpadding="0">
                <tr >
                    <Td colspan="4" class="contenthead">Fuel Filling</Td>
                </tr>
                
                <tr >
                    <td class="text2"><font color="red">*</font>Date</td>
                    <td class="text2"><input name="date" class="form-control" type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuelFilling.date,'dd-mm-yyyy',this)"/></td>
                    <td class="text2"><font color="red">*</font>Coupon Number</td>
                    <td class="text2"><input name="couponNo" class="form-control" type="text" value="" size="20"></td>
               <input type="hidden" name="lastFilled" >
               
               </tr>
                
                <tr>
                    <td class="text1"><font color="red">*</font>Vehicle Number</td>
                    <td class="text1"><input name="regNo" id="regno" class="form-control" onfocusout="ajax(this.value);" type="text"  size="20"></td>
                    <td class="text1"><font color="red">*</font>Vehicle Type</td> 
                    <td class="text1"><input name="typeId" readonly class="form-control" type="text"  size="20"></td> 
                </tr>
                
                <tr>
                    <td class="text2"><font color="red">*</font>Owning Company</td>
                    <td class="text2"><input name="companyId" readonly class="form-control" type="text" value="" size="20"></td>
                    <td class="text2"><font color="red">*</font>Using Company</td>
                    <td class="text2"><input name="servicePtId" readonly class="form-control" type="text" value="" size="20"></td>
                </tr>
                <tr>
                    
                    <td class="text1"><font color="red">*</font>Previous Reading(KM)</td>
                    <td class="text1"><input name="previous" class="form-control" type="text" value="" size="20"></td>
                    <td class="text1"><font color="red">*</font>Present Reading(KM)</td>
                    <td class="text1"><input name="present" class="form-control" type="text"  value="" size="20"></td>
                </tr>
                <tr>
                    <td class="text2"><font color="red">*</font>Fuel Filled</td>
                    <td class="text2"><input name="fuelFilled" class="form-control" type="text" onfocusout="calculate();" value="" size="20"></td>
                    <td class="text2"><font color="red">*</font>Amount</td>
                    <input type="hidden" name="fuelPrice" value='<c:out value="${fuelPrice}"/>'>
                    <td class="text2"><input name="amount" readonly class="form-control" type="text"  size="20"></td>
                </tr>
                  <tr>
                    <td class="text1">&nbsp;&nbsp;OutFill</td>
                    <td class="text1"><input name="outFill" class="form-control" type="checkbox" onclick="setVisibility();" value="" size="20"></td>     
                    <td class="text1">&nbsp;</td>
                    <td class="text1">&nbsp;</td>
                </tr>
                <tr>
                    
                    <td class="text2"><font color="red">*</font>Total Km</td>
                    <td class="text2"><input name="totalKm" class="form-control" type="text"  size="20"></td>
                    <td class="text2"><font color="red">*</font>Average Km</td>
                    <td class="text2"><input name="avgKm" class="form-control" type="text" size="20"></td>
                </tr>
                
                <tr>    
                    <td class="text1"><font color="red">*</font>Tank Reading</td>                    
                  <td class="text1"><input name="runReading" class="form-control" readonly type="text" value='<c:out value="${runReading}"/>' size="20"></td>                  
                <input type="hidden" name="run" value='<c:out value="${runReading}"/>'>
                <td class="text1">&nbsp;</td>
                    <td class="text1">&nbsp;</td>
                    <c:forEach items="${TankReading}" var="tak">
                    <td class="text1"><input name="tankReading" class="form-control" readonly type="hidden" value='<c:out value="${tak.present}"/>' size="20"></td>                  
               <input type="hidden" name="tank" value='<c:out value="${tak.present}"/>'>
               </c:forEach>
                </tr>
                             
            </table>
            
            <br><br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" />
                <input type="button" class="button" value="Reset" onclick="reset();" />    
                
            </center>
        </td>
        <td>
            <table  align="Right" width="250" class="border" border="0" cellspacing="0" cellpadding="0">
                <br>

<div class="widget-content">
    <%
ArrayList comparisionList = (ArrayList) request.getAttribute("TankReading");

int size = comparisionList.size(); 
String[][] arrData = new String[size][3];

int i = 0;   
Iterator itr = comparisionList.iterator();
FuelManagementTO fuelTO = new FuelManagementTO();
while(itr.hasNext()){
fuelTO = (FuelManagementTO) itr.next();
arrData[i][0] = fuelTO.getCompanyName();
arrData[i][1] = fuelTO.getPresent();
arrData[i][2] = fuelTO.getCapacity();

i++;
}

String  strXML = "<graph caption='Tank Reading'  YAxisName='Total Capacity' showNames='1' rotateNames='0' XAxisName='Service Point' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";

//Initialize <categories> element - necessary to generate a stacked chart
String strCategories = "<categories>";

//Initiate <dataset> elements
String strDataProdA = "<dataset seriesName='Current Fuel Reading' color='BF9900'>";
String strDataProdB = "<dataset seriesName='Remaining Capacity' color='F0EE99'>";

//Iterate through the data	
for(int j=0;j<arrData.length;j++){
//Append <category name='...' /> to strCategories
strCategories += "<category name='" + arrData[j][0] + "' />";
//Add <set value='...' /> to both the datasets
strDataProdA += "<set value='" + arrData[j][1] + "' />";
strDataProdB += "<set value='" + arrData[j][2] + "' />";
}

//Close <categories> element
strCategories += "</categories>";

//Close <dataset> elements
strDataProdA += "</dataset>";
strDataProdB +="</dataset>";

//Assemble the entire XML now
strXML += strCategories + strDataProdA + strDataProdB + "</graph>";

%>


<p>
        <table>
            <tr>
                <td>
                      <jsp:include page="FusionChartsRenderer.jsp" flush="true"> 
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_StackedColumn2D.swf" /> 
                <jsp:param name="strURL" value="" /> 
                <jsp:param name="strXML" value="<%=strXML %>" /> 
                <jsp:param name="chartId" value="productSales" /> 
                <jsp:param name="chartWidth" value="250" /> 
                <jsp:param name="chartHeight" value="330" />
                <jsp:param name="debugMode" value="false" /> 	
                <jsp:param name="registerWithJS" value="false" /> 
        </jsp:include>
                </td>
            </tr>
            
        </table>
          
            
    </p>

</div>

</table></td>
            </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
