
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.http.*" %>
        <title>Vehicle Accident Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        
            function saveVehicleId(value){
              if (document.viewVehicleDetails.vehicleId.value == '') {
                    alert('Please Select vehicle no');
                    document.viewVehicleDetails.vehicleId.focus();
                  return;
              }if(value == "Save") {

                    document.viewVehicleDetails.action = '/throttle/addAccidentVehicle.do';
                    document.viewVehicleDetails.submit();
                }
            }
        </script>
<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

    </head>
<!--    setImages(1,0,0,0,0,0);-->
    <body onLoad="setImages(1,0,0,0,0,0);">
        <form name="viewVehicleDetails"  method="post" >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>
<!-- <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">-->
               <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="accidents.label.VehicleAccidentDetails" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Accidents"  text="Accidents"/></a></li>
          <li class="active"><spring:message code="accidents.label.VehicleAccidentDetails" text="default text"/></li>
        </ol>
      </div>
      </div>
           
            <%
                        int index = 1;

            %>
                  <div class="contentpanel">
                     <div class="panel panel-default">
                         <div class="panel-body">

            <c:if test="${VehicleAccident == null }" >
                <center><font color="red" size="2"> <spring:message code="accidents.label.NoRecordsFound" text="default text"/></font></center>
            </c:if>
            <c:if test="${VehicleAccident != null }" >
                             <table  class="table table-info mb30 table-hover">
                <td  colspan="6"  width="100%"  style="border-color:#5BC0DE;padding:16px;">
                    <thead>
                        <tr height="50">
                            <th><spring:message code="accidents.label.SNo" text="default text"/></th>
                            <th><spring:message code="accidents.label.VehicleNumber" text="default text"/></th>
                            <th><spring:message code="accidents.label.InsurerName" text="default text"/></th>
                            <th><spring:message code="accidents.label.PolicyNo" text="default text"/></th>
                            
<!--                            <th>Licence No</th>-->
                            <th><spring:message code="accidents.label.Select" text="default text"/></th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                                String style = "text1";%>
                    <c:forEach items="${VehicleAccident}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.acc_VehicleNo}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.ins_Name}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.ins_PolicyNo}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; ">
                                <c:if test="${veh.accInsertStat=='0'}" >
                                        <a href="/throttle/viewVehicleAccidentPage.do?accident_Id=<c:out value="${veh.accident_Id}"/>"><spring:message code="accidents.label.ADD" text="default text"/></a>
                                    </c:if>
                                <c:if test="${veh.accInsertStat=='1'}" >
                                    <!--<a href="/throttle/viewVehicleAccidentPage.do?accident_Id=<c:out value="${veh.accident_Id}"/>">edit</a>-->
                                <a href="/throttle/editAccidentVehicle.do?accident_Id=<c:out value="${veh.accident_Id}"/>"><spring:message code="accidents.label.EDIT" text="default text"/> </a>
                                 </c:if>
                               
                            </td>
                              
                        </tr>
                    </c:forEach>
                        </tbody>
                             </td>
                </table>
                </td>

                <div class="container">
                    
                    <!-- Trigger the modal with a button -->
                    <button type="button"  id="modalPopup" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal" style="display: none;"><spring:message code="accidents.label.OpenModel" text="default text"/></button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title center"><spring:message code="accidents.label.AccidentVehicleDetails" text="default text"/></h4>
                                </div>
                                <div class="modal-body">
                                    <label><spring:message code="accidents.label.VehicleNo" text="default text"/></label>
                                     <select name="vehicleId" id="vehicleId" style="width:260px;height:40px;"  class="form-control" style="height:20px; width:122px;"  class="form-control" >
                                            <c:if test="${vehicleList != null}">
                                                <option value="" selected>--<spring:message code="accidents.label.Select" text="default text"/>--</option>
                                                <c:forEach items="${vehicleList}" var="vehicleList">
                                                    <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    
                                </div>
                                <div class="modal-footer">
                                    <input type="button" class="btn btn-success"  name="Save" value="<spring:message code="accidents.label.SAVE" text="default text"/>" onclick="saveVehicleId(this.value);" />
                                    <button type="button" class="btn btn-success"  data-dismiss="modal"><spring:message code="accidents.label.CLOSE" text="default text"/></button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </c:if>
                
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="accidents.label.EntriesPerPage" text="default text"/></span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="accidents.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="accidents.label.Of" text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value){
            if(value == 'add'){
               // alert(add);
                document.getElementById("modalPopup").click();
              
                        ////                document.viewVehicleDetails.action = '/throttle/viewVehicleAccidentPage.do';
//                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>
</div>
                
    </div>
        </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

