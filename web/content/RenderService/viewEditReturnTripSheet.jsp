<%--
    Document   : viewTripSheetNew
    Created on : Feb 11, 2013, 7:24:40 PM
    Author     : srini
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.net.*,java.io.*,java.util.*,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Return Trip Details</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function bataCalculation(sno){
                var inKm = 0;
                var outKm = 0;
                var totKm = 0;
                if(document.getElementById("stageIdExp"+sno).value == "Bata") {
                    if(document.getElementById("kmsIn").value != ""){
                        inKm = parseInt(document.getElementById("kmsIn").value);
                    }
                    if(document.getElementById("kmsOut").value != ""){
                        outKm = parseInt(document.getElementById("kmsOut").value);
                    }
                }

                var expHead = document.getElementById("stageIdExp"+sno).value;
                var temp = expHead.split("~");
                //alert(expHead);
                //alert(temp[0]);
                if(temp[0] == "1002") { //diesel expenses
                    document.getElementById("tripFuelLitre"+sno).value=0;
                    document.getElementById("tripFuelLitre"+sno).readOnly=false;
                }else{
                    document.getElementById("tripFuelLitre"+sno).value=0;
                    document.getElementById("tripFuelLitre"+sno).readOnly=true;
                }

                totKm = (inKm - outKm);
                document.getElementById("tripExpensesAmount"+sno).value = (totKm * 0.75).toFixed(0);
                calculateAmount();

            }
            function calculateAmount(){
                
                var totalAllowance = document.getElementById('existAllowance').value;
                var totalExpenses = document.getElementById('existExpenses').value;
                var allowanceRowSize = document.getElementsByName("tripAllowanceAmount");
                var expenseRowSize = document.getElementsByName("tripExpensesAmount");
                //alert("totalAllowance:"+totalAllowance);
                //alert("allowanceRowSize.length:"+allowanceRowSize.length);
                //alert("totalExpenses"+totalExpenses);
                for(var i=0; i<allowanceRowSize.length;i++) {
                        totalAllowance=parseFloat(totalAllowance)+parseFloat(allowanceRowSize[i].value);
                    
                }
                for(var i=0; i<expenseRowSize.length;i++) {
                    totalExpenses=parseFloat(totalExpenses)+parseFloat(expenseRowSize[i].value);
                }
                if(document.getElementById("expExpAmt")){
                    //alert("Element exists");
                    var expExpAmtSize = document.getElementsByName("expExpAmt");
                    var expExistExpAmtSize = document.getElementsByName("expExpAmtExist");
                    for(var i=0; i<expExpAmtSize.length;i++) {
                        totalExpenses=parseFloat(totalExpenses)-parseFloat(expExistExpAmtSize[i].value)+parseFloat(expExpAmtSize[i].value);;
                    }

                }




                if(document.getElementById("totalFuelAmount").value != ""){
                    totalExpenses = parseFloat(totalExpenses) + parseFloat(document.getElementById("totalFuelAmount").value);
                }
                
               
                document.getElementById("totalExpenses").value = parseFloat(totalExpenses).toFixed(2);
                document.getElementById("totalAllowance").value = parseFloat(totalAllowance).toFixed(2);
                document.getElementById("nettAllowance").value = parseFloat(totalAllowance).toFixed(2);
                
            }
            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';
            function addAllowanceRow() {
                var currentDate = new Date();
                var day = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var myDate= day + "-" + month + "-" + year;

                if(sno < 9){
                    sno++;
                    var tab = document.getElementById("allowanceTBL");
                    var rowCount = tab.rows.length;
                    snumber = parseInt(rowCount)-1;
                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                    newrow.height="30px";

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><select class='form-control' name='stageId' style='width:130px'  id='stageId"+sno+"'><option selected value=0>---Select---</option><c:if test = "${opLocation != null}" ><c:forEach items="${opLocation}" var="opl"><option  value='<c:out value="${opl.locationId}" />'><c:out value="${opl.locationName}" /> </c:forEach ></c:if> </select></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text2'><input name='tripAllowanceDate' id='tripAllowanceDate"+snumber+"' type='text' value='"+myDate+"' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='tripAllowanceAmount' id='tripAllowanceAmount"+snumber+"' type='text' class='form-control' onBlur='calculateAmount();' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    //cell0 = "<td class='text1'><select class='form-control' name='tripAllowancePaidBy' style='width:125px'  id='tripAllowancePaidBy"+sno+"'><option selected value=0>---Select---</option><c:if test = "${paidBy != null}" ><c:forEach items="${paidBy}" var="paid"><option  value='<c:out value="${paid.issuerId}" />'><c:out value="${paid.issuerName}" /> </c:forEach ></c:if> </select></td>";
                    cell0 = "<td class='text1'><input id='tripAllowancePaidBy' name='tripAllowancePaidBy"+snumber+"' type='hidden' value='<%= session.getAttribute("userId")%>' /><input id='AllowancepaidName' name='AllowancepaidName' type='text' class='form-control' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(5);
                    cell0 = "<td class='text1'><input name='tripAllowanceRemarks' type='text' class='form-control' id='tripAllowanceRemarks' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(6);
                    var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                    cell.innerHTML = cell1;

                    $( ".datepicker" ).datepicker({
                        /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });
                }
            }
            function delAllowanceRow() {
                try {
                    var table = document.getElementById("allowanceTBL");
                    rowCount = table.rows.length-1;
                    for(var i=2; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }sumAllowanceAmt();
                }catch(e) {
                    alert(e);
                }
            }
            function validateSubmit() {
                if(isEmpty(document.getElementById("routeId").value)){
                    alert("Select Route");
                    document.getElementById("routeId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("vehicleId").value)){
                    alert("Select Vehicle");
                    document.getElementById("vehicleId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("tripCode").value)){
                    alert("Trip Code is not filled");
                    document.getElementById("tripCode").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsOut").value)){
                    alert("Kms Out is not filled");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isDigit(document.getElementById("kmsOut").value)){
                    alert("Kms Out accepts only numeric values");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsIn").value)){
                    document.getElementById("kmsIn").focus();
                    alert("Kms Out is not filled");
                    return false;
                }else if(isDigit(document.getElementById("kmsIn").value)){
                    alert("Kms In accepts only numeric values");
                    document.getElementById("kmsIn").focus();
                    return false;
                }/*else if(parseFloat(document.getElementById("kmsIn").value) == 0){
                    alert("Kms In Should not be 0");
                    document.getElementById("kmsIn").focus();
                    return false;
                }*/else if(isEmpty(document.getElementById("arrivalDate").value)){
                    alert("Arrival Date is not filled");
                    document.getElementById("arrivalDate").focus();
                    return false;
                }/*else if(document.getElementById('totalKms').value="null"){
                    alert("totalKms is not null");
                    document.getElementById("kmsIn").focus();
                    return false;
                }*/
            }


            

        </script>
    </head>
    <body>
        <form name="tripSheet" action="updateReturnTripSheet.do" method="post" onsubmit="return validateSubmit();" >
            <%@ include file="/content/common/message.jsp" %>

            <center><h2> Edit Return Trip / Movement </h2>
                <input type="hidden" name="tripSheetIdParam" value="<%=request.getParameter("tripSheetIdParam")%>" />
                <input type="hidden" name="returnTripId" value="<%=request.getParameter("returnTripId")%>" />
                <input type="hidden" name="driverNameId" value="<%=request.getParameter("driverNameId")%>" />
                <input type="hidden" name="driverType" value="<%=request.getParameter("driverType")%>" />
                <input type="hidden" name="driverVendorId" value="<%=request.getParameter("driverVendorId")%>" />
                <input type="hidden" name="vehicleVendorId" value="<%=request.getParameter("vehicleVendorId")%>" />
                <input type="hidden" name="vehicleType" value="<%=request.getParameter("vehicleType")%>" />
            </center>
            <div style="padding-left: 60px;">

                <table cellpadding="0" cellspacing="4" border="0" width="90%">
                    <tr>
                        <td class="contenthead">Return Trip Movement Details </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                                <c:if test = "${returnTripList != null}" >


                                    <c:forEach items="${returnTripList}" var="returnTrip">

                                        <tr>
                                            <td class="texttitle1">
                                                From: <c:out value="${returnTrip.returnFromLocation}" />
                                            </td>
                                            <td class="texttitle1">
                                                To: <c:out value="${returnTrip.returnToLocation}" />
                                            </td>
                                            <td class="texttitle1">
                                                Trip Type: <c:out value="${returnTrip.returnTripType}" />
                                            </td>
                                            <td class="texttitle1">
                                                Product: <c:out value="${returnTrip.returnProductName}" />
                                            </td>
                                            <td class="texttitle1">
                                                Open Date: <c:out value="${returnTrip.openDateTime}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="texttitle2">
                                                Close Date: <c:out value="${returnTrip.closedDate}" />
                                            </td>

                                            <td class="texttitle2">
                                                Out / In  Km: <c:out value="${returnTrip.outKM}" /> / <c:out value="${returnTrip.inKM}" />
                                            </td>
                                            <td class="texttitle2">
                                                Distance: <c:out value="${returnTrip.returnKM}" />
                                            </td>
                                            <td class="texttitle2">
                                                Tonnage: <c:out value="${returnTrip.returnTon}" />
                                            </td>
                                            <td class="texttitle2">
                                                Revenue: <c:out value="${returnTrip.returnAmount}" />
                                            </td>

                                        </c:forEach >
                                    </c:if>




                                <tr style="height: 10px;">
                                    <td align="right" class="TableColumn" colspan="5">
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left"  colspan="5" rowspan="1">
                                        <h2>Payment Details</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="allowanceTBL" name="allowanceTBL">
                                                <tr style="width:50px;">
                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                    <th class="contenthead">Location</th>
                                                    <th class="contenthead">Date</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Paid Person</th>
                                                    <th class="contenthead">Remarks</th>
                                                    <th class="contenthead">Delete</th>
                                                </tr>
                                                <%
                                                            int snoAllo = 0;
                                                            float totalAllowanceGiven = 0.00F;

                                                            TripAllowanceTO allowanceTo = null;
                                                            ArrayList allowanceAL = (ArrayList) request.getAttribute("tripAllowanceDetails");
                                                            if (allowanceAL != null) {
                                                                Iterator it = allowanceAL.iterator();
                                                                while (it.hasNext()) {
                                                                    allowanceTo = new TripAllowanceTO();
                                                                    allowanceTo = (TripAllowanceTO) it.next();
                                                                    snoAllo++;
                                                                    totalAllowanceGiven = totalAllowanceGiven + Float.parseFloat(allowanceTo.getTripAllowanceAmount());
                                                %>
                                                <tr>
                                                    <td><%=snoAllo%></td>
                                                    <td><%=allowanceTo.getLocationName()%>
                                                    </td>
                                                    <td>
                                                        <%=allowanceTo.getTripAllowanceDate()%>
                                                    </td>
                                                    <td>
                                                        <%=allowanceTo.getTripAllowanceAmount()%>

                                                    </td>
                                                    <td>
                                                        
                                                        <%=allowanceTo.getTripAllowancePaidBy()%>
                                                    </td>
                                                    <td>
                                                        <%=allowanceTo.getTripAllowanceRemarks()%>
                                                        <input type="hidden" name="tripAllowanceId" id="tripAllowanceId" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <%
                                                                }

                                                            }
                                                %>
                                                <script type="text/javascript">

                                                            function sumAllowanceAmt(){
                                                                var sumAmt=0;
                                                                var totAmt=0;
                                                                sumAmt= document.getElementsByName('tripAllowanceAmount');
                                                                for(i=0;i<sumAmt.length;i++){
                                                                    totAmt=parseInt(totAmt)+parseInt(sumAmt[i].value);
                                                                    document.getElementById('totalAllowance').value=parseInt(totAmt);
                                                                }
                                                            }
                                                            function sumExpenses(){
                                                                var sumAmt=0;
                                                                sumAmt= parseInt(document.getElementById('totalAllowance').value)+parseInt(document.getElementById('totalFuelAmount').value);
                                                                document.getElementById('totalExpenses').value=parseInt(sumAmt);
                                                            }
                                                            function setBalance(){
                                                                var sumAmt=0;
                                                                sumAmt= parseInt(document.getElementById('totalAllowance').value)-parseInt(document.getElementById('totalFuelAmount').value);
                                                                document.getElementById('balanceAmount').value=parseInt(sumAmt);
                                                            }
                                                        </script>
                                                <tr>
                                                    <td colspan="6" align="center">
                                                        <input type="button" name="add" value="Add"  onclick="addAllowanceRow()" id="add" class="button" />
                                                        &nbsp;&nbsp;&nbsp;

                                                        <input type="button" name="delete" value="Delete" onclick="delAllowanceRow()" id="delete" class="button" />

                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr align="center">
                                  

                                    <td align="left" colspan="3" class="TableColumn">
                                        Total
                                    </td>
                                    <td align="right" class="TableColumn">
                                        <input name="totalAllowance" type="text" class="form-control" value="<%=totalAllowanceGiven%>" id="totalAllowance" readonly  />
                                        <input name="existAllowance" id="existAllowance"  type="hidden" class="form-control" value="<%=totalAllowanceGiven%>"  readonly  />
                                    </td>
                                    <td class="TableRowNew">
                                    </td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_Tr1" align="left">
                                    <td align="left" class="TableColumn" colspan="5" rowspan="1">
                                        <h2>Fuel Details</h2>
                                    </td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_Tr2">
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" class="border" id="fuelTBL" name="fuelTBL" style="width: 100%; left: 30px;" class="border">

                                                <tr style="width:40px;">
                                                    <th class="contenthead">S No</th>
                                                    <th class="contenthead">Bunk Name</th>
                                                    <th class="contenthead">Place</th>
                                                    <th class="contenthead">Date</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Ltrs</th>
                                                    <!--<th class="contenthead">Person</th>-->
                                                    <th class="contenthead">Remarks</th>
                                                    <th class="contenthead">Delete</th>
                                                </tr>
                                                <%
                                                            int snoFuel = 0;
                                                            float totalLitres = 0.00F;
                                                            float totalFuelAmount = 0.00F;

                                                            TripFuelDetailsTO fuelTo = new TripFuelDetailsTO();
                                                            ArrayList fuelAL = (ArrayList) request.getAttribute("fuelDetails");
                                                            if (fuelAL != null) {
                                                                Iterator itFuel = fuelAL.iterator();
                                                                while (itFuel.hasNext()) {
                                                                    fuelTo = new TripFuelDetailsTO();
                                                                    fuelTo = (TripFuelDetailsTO) itFuel.next();
                                                                    snoFuel++;
                                                                    totalLitres = totalLitres + Float.parseFloat(fuelTo.getTripFuelLtrs());
                                                                    totalFuelAmount = totalFuelAmount + Float.parseFloat(fuelTo.getTripFuelAmounts());
                                                %>
                                                <tr>

                                                    <td> <%=snoFuel%> </td>

                                                    <td>
                                                        <%=fuelTo.getTripBunkName()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripBunkPlace()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelDate()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelAmounts()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelLtrs()%>
                                                    </td>
                                                    <td>
                                                        <%=fuelTo.getTripFuelRemarks()%>
                                                        <input type="hidden" name="HfId" id="HfId" />

                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <%
                                                                }

                                                            }
                                                %>
                                                <tr>
                                                    <td colspan="8" align="center">
                                                        <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />
                                                        &nbsp;&nbsp;&nbsp;
                                                        <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="button" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>

                                            </table>
                                        </div>
                                        <script type="text/javascript">
                                            var poItems = 0;
                                            var rowCount='';
                                            var sno='';
                                            var snumber = '';

                                            function addFuelRow()
                                            {
                                                var currentDate = new Date();
                                                var day = currentDate.getDate();
                                                var month = currentDate.getMonth() + 1;
                                                var year = currentDate.getFullYear();
                                                var myDate= day + "-" + month + "-" + year;
                                                if(sno < 9){
                                                    sno++;
                                                    var tab = document.getElementById("fuelTBL");
                                                    var rowCount = tab.rows.length;

                                                    snumber = parseInt(rowCount)-1;
                                                    //                    if(snumber == 1) {
                                                    //                        snumber = parseInt(rowCount);
                                                    //                    }else {
                                                    //                        snumber++;
                                                    //                    }

                                                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                    newrow.height="30px";
                                                    // var temp = sno1-1;
                                                    var cell = newrow.insertCell(0);
                                                    var cell0 = "<td><input type='hidden'  name='fuelItemId' /> "+snumber+"</td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(1);
                                                    cell0 = "<td class='text2'><select class='form-control' name='bunkName' style='width:170px'  id='bunkName"+sno+"'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(2);
                                                    cell0 = "<td class='text2'><input name='bunkPlace' type='text' class='form-control' id='bunkPlace' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(3);
                                                    cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate"+snumber+"' type='text' class='datepicker' value='"+myDate+"' style='width:80px;' id='fuelDate' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(4);
                                                    cell0 = "<td class='text1'><input name='fuelAmount' onBlur='sumFuel();' type='text' class='form-control' value='0' style='width:80px;' id='fuelAmount' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(5);
                                                    cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' type='text' class='form-control' id='fuelLtrs' value='0' style='width:80px;' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    /*cell = newrow.insertCell(6);
                       cell0 = "<td class='text1'><input name='fuelFilledBy' type='text' class='form-control' id='fuelFilledBy' class='Textbox' /></td>";
                       //cell.setAttribute(cssAttributeName,"text1");
                       cell.innerHTML = cell0;*/

                                                    cell = newrow.insertCell(6);
                                                    cell0 = "<td class='text1'><input name='fuelRemarks' type='text' class='form-control' id='fuelRemarks' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(7);
                                                    var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell1;
                                                    // rowCount++;


                                                    $( ".datepicker" ).datepicker({
                                                        /*altField: "#alternate",
                           altFormat: "DD, d MM, yy"*/
                                                        changeMonth: true,changeYear: true
                                                    });
                                                }
                                            }


                                            function delFuelRow() {
                                                try {
                                                    var table = document.getElementById("fuelTBL");
                                                    rowCount = table.rows.length-1;
                                                    for(var i=2; i<rowCount; i++) {
                                                        var row = table.rows[i];
                                                        var checkbox = row.cells[7].childNodes[0];
                                                        if(null != checkbox && true == checkbox.checked) {
                                                            if(rowCount <= 1) {
                                                                alert("Cannot delete all the rows");
                                                                break;
                                                            }
                                                            table.deleteRow(i);
                                                            rowCount--;
                                                            i--;
                                                            sno--;
                                                            // snumber--;
                                                        }
                                                    }sumFuel();
                                                }catch(e) {
                                                    alert(e);
                                                }
                                            }


                                           
                                            function sumFuel(){
                                                var totFuel=0;
                                                totFuel= document.getElementsByName('fuelLtrs');
                                                var totltr = document.getElementById('existFuelLtrs').value;
                                                var totAmt = document.getElementById('existFuelAmount').value;
                                                
                                                totAmount= document.getElementsByName('fuelAmount');
                                                for(i=0;i<totFuel.length;i++){
                                                    totltr=parseInt(totltr)+parseInt(totFuel[i].value);
                                                    document.getElementById('totalFuelLtrs').value=parseInt(totltr);
                                                }
                                                for(i=0;i<totAmount.length;i++){
                                                    totAmt=parseInt(totAmt)+parseInt(totAmount[i].value);
                                                    document.getElementById('totalFuelAmount').value=parseInt(totAmt);
                                                }
                                                calculateAmount();
                                            }
                                            function NumericOnly(){

                                            }

                                            function totalKmsFunc(){
                                                var kmDiff=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                                if(kmDiff<0){kmDiff=0;}
                                                document.getElementById('totalKms').value=kmDiff;
                                            }

                                        </script>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right" colspan="3"  class="TableColumn">
                                        Total Liters
                                    </td>
                                    <td align="left" class="TableColumn">
                                        <input name="totalFuelLtrs" type="text" class="form-control"  value="<%=totalLitres%>" id="totalFuelLtrs" readonly  />
                                        <input name="existFuelLtrs" id="existFuelLtrs" type="hidden" class="form-control"  value="<%=totalLitres%>" readonly  />
                                    </td>

                                    <td class="TableRowNew">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right" colspan="3" class="TableColumn">
                                        Total
                                    </td>
                                    <td align="left" class="TableColumn">
                                        <input name="totalFuelAmount" type="text" class="form-control" value="<%=totalFuelAmount%>" id="totalFuelAmount"  readonly  />
                                        <input name="existFuelAmount" id="existFuelAmount" type="hidden" class="form-control" value="<%=totalFuelAmount%>"  readonly  />
                                    </td>
                                    <td class="TableRowNew">
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                                <tr align="left">
                                                    <td align="left"  colspan="6" rowspan="1">
                                                        <h2>Trip Expenses </h2>
                                                    </td>
                                                </tr>



                                                <script type="text/javascript">
                                                    var poItems = 0;
                                                    var rowCount='0';
                                                    var sno='0';
                                                    var snumber = '0';

                                                    function addExpensesRow()
                                                    {
                                                        var currentDate = new Date();
                                                        var day = currentDate.getDate();
                                                        var month = currentDate.getMonth() + 1;
                                                        var year = currentDate.getFullYear();
                                                        var myDate= day + "-" + month + "-" + year;

                                                        if(sno < 9){
                                                            sno++;
                                                            var tab = document.getElementById("expensesTBL");
                                                            var rowCount = tab.rows.length;

                                                            snumber = parseInt(rowCount)-1;
                                                            //                    if(snumber == 1) {
                                                            //                        snumber = parseInt(rowCount);
                                                            //                    }else {
                                                            //                        snumber++;
                                                            //                    }

                                                            var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                            newrow.height="30px";
                                                            // var temp = sno1-1;
                                                            var cell = newrow.insertCell(0);
                                                            var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(1);
                                                            //cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='form-control' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-Select-</option><option value='Bata'>Bata</option><option value='Toll'>Toll</option><option value='Coolie'>Coolie</option><option value='Mamool'>Mamool Loading Charges</option><option value='Parking Charges'>Parking Charges</option><option value='Loading and Unloading'>Loading and Unloading</option><option value='Others'>Others</option></select></td>";
                                                            cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='form-control' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-select-</option><c:if test = "${expensesList != null}" ><c:forEach items="${expensesList}" var="elist"><option  value='<c:out value="${elist.expenseId}" />~<c:out value="${elist.expenseName}" />'><c:out value="${elist.expenseName}" /> </c:forEach ></c:if> </select></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(2);
                                                            cell0 = "<td class='text2'><input name='tripExpensesDate' id='tripExpensesDate"+snumber+"' type='text' class='datepicker' value='"+myDate+"' class='Textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(3);
                                                            cell0 = "<td class='text1'><input name='tripFuelLitre' readonly id='tripFuelLitre"+snumber+"' type='text' class='form-control' value='0' OnKeyPress='NumericOnly();'   class='Textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(4);
                                                            cell0 = "<td class='text1'><input name='tripExpensesAmount' id='tripExpensesAmount"+snumber+"' type='text' class='form-control'  onkeyup='calculateAmount();' class='Textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(5);
                                                            //cell0 = "<td class='text1'><input name='tripExpensesPaidBy' id='tripExpensesPaidBy"+snumber+"' type='text' class='form-control'  /></td>";
                                                            cell0 = "<td class='text1'><input name='tripExpensesPaidBy' id='tripExpensesPaidBy"+snumber+"' type='hidden' value='<%= session.getAttribute("userName")%>' /><input id='tripExpensesPaidName' name='tripExpensesPaidName' type='text' class='form-control' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(6);
                                                            cell0 = "<td class='text1'><input name='tripExpensesRemarks'id='tripExpensesRemarks"+snumber+"' type='text' class='form-control'  /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(7);
                                                            var cell1 = "<td><input type='checkbox' name='deleteItemExp' value='"+snumber+"'   /> </td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell1;
                                                            // rowCount++;



                                                            $( ".datepicker" ).datepicker({
                                                                /*altField: "#alternate",
                                                                        altFormat: "DD, d MM, yy"*/
                                                                changeMonth: true,changeYear: true
                                                            });

                                                        }
                                                    }


                                                    function delExpensesRow() {
                                                        try {
                                                            var table = document.getElementById("expensesTBL");
                                                            rowCount = table.rows.length-1;
                                                            for(var i=2; i<rowCount; i++) {
                                                                var row = table.rows[i];
                                                                var checkbox = row.cells[6].childNodes[0];
                                                                if(null != checkbox && true == checkbox.checked) {
                                                                    if(rowCount <= 1) {
                                                                        alert("Cannot delete all the rows");
                                                                        break;
                                                                    }
                                                                    table.deleteRow(i);
                                                                    rowCount--;
                                                                    i--;
                                                                    sno--;
                                                                    // snumber--;
                                                                }
                                                            }sumAllowanceAmt();
                                                        }catch(e) {
                                                            alert(e);
                                                        }
                                                    }
                                                    function returnTripDetail(){
                                                        if(document.getElementById('returnTrip').checked == true){
                                                            document.getElementById("ReturnDetail").style.display="block";
                                                        }else{
                                                            document.getElementById("ReturnDetail").style.display="none";
                                                        }
                                                    }

                                                </script>
                                                <tr>
                                                    <td align="center" class="TableColumn" colspan="6" rowspan="1" style="height: 0%">
                                                        <div>
                                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="expensesTBL" name="expensesTBL">
                                                                <tr>
                                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                                    <th class="contenthead">Expenses</th>
                                                                    <th class="contenthead">Date</th>
                                                                    <th class="contenthead">Fuel Litre</th>
                                                                    <th class="contenthead">Amount</th>
                                                                    <th class="contenthead">Paid Person</th>
                                                                    <th class="contenthead">Remarks</th>
                                                                    <th class="contenthead">&nbsp;</th>
                                                                </tr>
                                                                <%
                                                                            double expTotal = 0;
                                                                            int expSno = 0;
                                                                            TripFuelDetailsTO expTo = new TripFuelDetailsTO();
                                                                            ArrayList expAL = (ArrayList) request.getAttribute("etmExpDetails");
                                                                            if (expAL != null) {
                                                                                Iterator itExp = expAL.iterator();
                                                                                while (itExp.hasNext()) {
                                                                                    expSno++;
                                                                                    expTo = new TripFuelDetailsTO();
                                                                                    expTo = (TripFuelDetailsTO) itExp.next();
                                                                                    expTotal += Double.parseDouble(expTo.getAmount());
                                                                                %>
                                                                                <tr style="width:50px;">
                                                                                    <td width="50"><%=expSno%></td>
                                                                                    <td><%=expTo.getCategory()%></td>
                                                                                    <td><%=expTo.getTdate()%></td>
                                                                                    <td><%=expTo.getTripFuelLtrs()%></td>
                                                                                    <td>

                                                                                        <%if("1000".equals(expTo.getExpenseId()) || "Fixed".equalsIgnoreCase(expTo.getMode())){ //driver bata that is fixed earlier%>
                                                                                        <input type="hidden" name="expExpAmtExist" id="expExpAmtExist" value="<%=expTo.getAmount()%>" />
                                                                                        <input type="text" name="expExpAmt" id="expExpAmt" onBlur="calculateAmount();" value="<%=expTo.getAmount()%>" />
                                                                                        <input type="hidden" readonly name="expExpId" id="expExpId" value="<%=expTo.getTripExpenseId()%>" />                                                                                        
                                                                                        <%}else{%>
                                                                                        <%=expTo.getAmount()%>
                                                                                        <%}%>
                                                                                    </td>
                                                                                    <td><%=expTo.getMode()%></td>
                                                                                    <td><%=expTo.getRemark()%></td>
                                                                                </tr>
                                                                                <%


                                                                                }
                                                                            }

                                                                %>

                                                                <tr>
                                                                    <td colspan="6" align="center">
                                                                        <input type="button" name="add" value="Add" onclick="addExpensesRow()" id="add" class="button" />
                                                                        &nbsp;&nbsp;&nbsp;

                                                                        <input type="button" name="delete" value="Delete" onclick="delExpensesRow()" id="delete" class="button" />

                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr align="center">
                                                    <td align="right" class="TableColumn">
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                    </td>

                                                    <td class="TableRowNew">
                                                    </td>

                                                    <td align="right" class="TableColumn">
                                                        Total Advances
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="nettAllowance" type="text" class="form-control" value="<%=totalAllowanceGiven%>" id="nettAllowance" readonly  />
                                                    </td>
                                                    <td class="TableRowNew">
                                                    </td>
                                                </tr>
                                                <tr id="TrTotalDetails" align="center">

                                                    <td align="right" class="TableColumn">

                                                        &nbsp;

                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                    </td>

                                                    <td class="TableRowNew">
                                                    </td>

                                                    <td align="right" class="TableColumn">
                                                        Total Expenses
                                                    </td>

                                                    <td align="left" class="TableColumn">

                                                        <input name="totalExpenses" type="text" class="form-control" value="<%=expTotal + totalFuelAmount%>" id="totalExpenses" readonly  />
                                                        <input name="existExpenses"  id="existExpenses" type="hidden" class="form-control" value="<%=expTotal%>" readonly  />

                                                    </td>

                                                    <td class="TableRowNew">
                                                        <input type="hidden" name="hfETMTotal" id="hfETMTotal" value="0" />
                                                    </td>

                                                </tr>


                                                
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                               
                            </table>
                        </td>
                    </tr>

                    <tr>

                        <td>
                            <center>
                                <input type="submit" name="save" value="Save" onclick="return Mandatory();" id="save" class="button" style="width:120px;" />
                            </center>
                        </td>
                    </tr>
                </table>
            </div>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
