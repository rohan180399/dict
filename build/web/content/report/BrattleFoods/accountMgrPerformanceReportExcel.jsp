<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "AccountMgrPerformanceSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test="${AccountMgrPerformanceReportDetails != null}">
                  <table align="center" border="1" id="table" class="sortable" style="width:1000px;" >

                    <thead>
                    <tr>   
                    <th><h3> S.No</h3></th>
                    <th width="120"><h3>Account Manager Name</h3></th>
                    <th><h3>Total trips</h3></th>
                    <th><h3>Total Sales</h3></th>
                    <th><h3>TotalKms</h3></th>
                    <th><h3>Revenue/km</h3></th>
                    <th><h3>cost/km</h3></th>
                    <th><h3>Margin/km</h3></th>
                    <th><h3>WFU Days</h3></th>
                    <th><h3>Total Cost</h3></th>
                    <th><h3>TotalReferHrs</h3></th>
                    
                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${AccountMgrPerformanceReportDetails}" var="account">
                            
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                 <td class="text1"><%=index++%></td>
                                <td  width="30" class="text1"><c:out value="${account.empName}"/></td>
                                <td  width="30" class="text1"><c:out value="${account.tripNos}"/></td>
                                <td  width="30" class="text1"><c:out value="${account.revenue}"/></td>
                                <td  width="30" class="text1"><c:out value="${account.totalkms}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${account.revenue/account.totalkms}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${account.totalCost/account.totalkms}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${(account.revenue/account.totalkms)-(account.totalCost/account.totalkms)}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${(account.wfuHours/24)}"/></td>
                                
                                <td  width="30" class="text1"><c:out value="${account.totalCost}"/></td>
                                
                                <td  width="30" class="text1"><c:out value="${account.totalHms}"/></td>
                               

                            </tr>
                        </c:forEach>
                            
                    </tbody>
                </table>

                
            </c:if>
                     
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
