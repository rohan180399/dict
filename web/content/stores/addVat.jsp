
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="/throttle/js/validate.js"></script>   
 <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
</head>
<script>
    function submitpage()
    {
       if(floatValidation(document.dept.vat,"VAT")){
           return;
       }
       if(textValidation(document.dept.effectiveDate,"effectiveDate")){
           return;
       }
       if(textValidation(document.dept.desc,"Description")){
           return;
       }
       document.dept.action= "/throttle/handleAddVat.do";
       document.dept.submit();
    }
</script>


<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

        <c:if test="${jcList != null}">
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        
        
             <span style="float: right">
		                                <a href="?paramName=en">English</a>
		                                |
		                                <a href="?paramName=ar">Arabic</a>
                            </span>
        


<body>
<form name="dept"  method="post">
    <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
<td colspan="2" class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.ADDVAT(%)"  text="default text"/>
</div></td>
</tr>
<tr>
<td class="text1" height="30"><font color=red>*</font><spring:message code="stores.label.VATRate(%)"  text="default text"/></td>
<td class="text1" height="30"><input name="vat" maxlength="10" type="text" class="form-control" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font><spring:message code="stores.label.EffectiveDate"  text="default text"/></td>
<td class="text2" height="30">
    <input name="effectiveDate" type="text" class="datepicker"  readonly="readonly" id="effectiveDate" value=""/>
</td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font><spring:message code="stores.label.Description"  text="default text"/></td>
<td class="text2" height="30"><textarea class="form-control" name="desc"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.ADD"  text="default text"/>" class="button" onclick="submitpage();">

</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
