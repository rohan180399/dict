
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    </head>
    <script language="javascript">

    function show_src() {
        document.getElementById('exp_table').style.display='none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display='block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display='none';
    }

    function submitPage(){
        var chek=validation();
        if(chek=='true'){

            document.fcReport.action = '/throttle/vehiclePermitReport.do';
            document.fcReport.submit();
        }
    }
    function validation(){
        if(document.fcReport.companyId.value==0){
            alert("Please Select Data for Company Name");
            document.fcReport.companyId.focus();
            return 'false';
        }/*
        else  if(textValidation(document.fcReport.dueIn,'Due Days')){
            return 'false';
        }
        else  if(textValidation(document.fcReport.toDate,'TO Date')){
            return'false';
        }*/
        return 'true';
    }
    function setValues(){
        if('<%=request.getAttribute("companyId")%>'!='null'){
            document.fcReport.companyId.value='<%=request.getAttribute("companyId")%>';
            document.fcReport.dueIn.value='<%=request.getAttribute("dueIn")%>';
            //document.fcReport.toDate.value='<%=request.getAttribute("toDate")%>';
        }
        /*
        if('<%=request.getAttribute("vendorId")%>'!='null'){
            document.fcReport.vendorId.value='<%=request.getAttribute("vendorId")%>';
        }
        */
        if('<%=request.getAttribute("regNo")%>'!='null'){
            document.fcReport.regNo.value='<%=request.getAttribute("regNo")%>';
        }


    }
    function getVehicleNos(){

        var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
    }
</script>
<body onload="getVehicleNos();setValues();">
    <form name="fcReport">
         <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>

<table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:800;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Permit Due Report</li>
</ul>
<div id="first">
<table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
        <td align="left" height="30"><font color="red">*</font>Operation Point</td>
        <td><select class="form-control" name="companyId" style="width:125px">
                        <option value="0">-select-</option>
                        <c:if test = "${LocationLists != null}" >
                            <c:forEach items="${LocationLists}" var="company">
                                <c:choose>
                                    <c:when test="${company.companyTypeId==1011}" >
                                        <option value="<c:out value='${company.cmpId}'/>"><c:out value="${company.name}"/></option>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                        </c:if>

                </select> </td>
        <td align="left" height="30">Vehicle No</td>
        <td height="30"><input name="regNo" id="regno" type="text" class="form-control"  value=""> </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Due in </td>
            <td><input name="dueIn" type="text" class="form-control" value="30" size="20" /> &nbsp; days</td>
            <input name="fromDate" type="hidden" class="form-control" value="" size="15" />
            <td>&nbsp;</td>
            <td>&nbsp;</td>

            <input name="toDate" type="hidden" class="form-control" value="" size="13">
            <td><input type="button" value="Search" class="button" name="search" onClick="submitPage()">
            <input type="hidden" value="" name="reqfor"></td>
        </tr>
</table>
</div></div>
</td>
</tr>
</table>
        <br>
        <br>
        <% int index = 0;%>
        <%int oddEven = 0;%>
        <%  String classText = "";%>
        <c:if test = "${DueList != null}" >
            <table width="815" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

                <tr>
                    <td width="35" class="contentsub">Sno</td>
                    <td width="132" height="30" class="contentsub">Operation Point </td>
                    <td width="127" height="30" class="contentsub">Vehicle No </td>
                    <td width="121" class="contentsub">Expiry Date </td>
                    <td width="121" class="contentsub">Permit Type </td>
                    <td width="174" height="30" class="contentsub">Due In Days </td>
                </tr>
                <c:forEach items="${DueList}" var="fc">
                    <%
        oddEven = index % 2;
        if (oddEven > 0) {
            classText = "text2";
        } else {
            classText = "text1";
        }
                    %>

                    <tr>
                        <td class="<%=classText%>"><%=index + 1%></td>
                        <td class="<%=classText%>" height="30"><c:out value="${fc.companyName}" /></td>
                        <td class="<%=classText%>" height="30"><c:out value="${fc.regNo}" /> </td>
                        <td class="<%=classText%>" height="30"><c:out value="${fc.nextFc}" /> </td>
                        <td class="<%=classText%>" height="30"><c:out value="${fc.permitType}" /> </td>
                        <td class="<%=classText%>" height="30"><c:out value="${fc.due}" /></td>
                    </tr>
                    <% index++;%>
                </c:forEach>
            </table>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
    
</html>
