<%-- 
    Document   : newCountry
    Created on : 7 Nov, 2012, 7:02:56 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Account Type</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {
        if(textValidation(document.add.countryName,'countryName')){
            return;
        }
        if(textValidation(document.add.countryCode,'countryCode')){
            return;
        }        
        document.add.action='/throttle/saveNewCountry.do';
        document.add.submit();
}
function setFocus(){
    document.add.groupName.focus();
    }

</script>
<body onload="setFocus();">
<form name="add" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

<table align="center" width="300" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Add Country</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font> Name</td>
    <td class="text2"><input name="countryName" type="text" class="form-control" value="" maxlength="10" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Code</td>
    <td class="text2"><input name="countryCode" type="text" class="form-control" value="" maxlength="15" size="20"></td>
  </tr>
</table>
<br>
<br>
<center>
    <input type="button" class="button" value="Save" onclick="submitPage();" />
    &emsp;<input type="reset" class="button" value="Clear">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

