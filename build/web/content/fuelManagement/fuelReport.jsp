
<html>
    <head>
     <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %> 
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


        function submitPage(val){
            var chek=validation();
            if(chek=='true'){
                 if(val=='GoTo'){
            var temp=document.report.GoTo.value;       
            if(temp!='null'){
            document.report.pageNo.value=temp;
            }
        }
        
            document.report.button.value=val;
            document.report.action='/throttle/fuelReport.do';
            document.report.submit();
        }
        }
        function submitExcel(){
            document.report.action='/throttle/fuelReportExcel.do';
            document.report.submit();
            }
        function validation(){
            if(textValidation(document.report.fromDate,'From Date')){
                return 'false';                
                }
            if(textValidation(document.report.toDate,'TO Date')){
                return 'false';
                
                }
                if(document.report.companyId.value=='0'){
                    alert("Please select Filling Station");
                    document.report.companyId.focus();
                    return 'false';
                    }
            return 'true';
            }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }       
        function setValues(){
            if('<%=request.getAttribute("companyId")%>' !='null'){
            document.report.fromDate.value='<%=request.getAttribute("fromDate")%>';
            document.report.toDate.value='<%=request.getAttribute("toDate")%>';
            document.report.companyId.value='<%=request.getAttribute("companyId")%>';
            }if('<%=request.getAttribute("regNo")%>' !='null'){
            document.report.regNo.value='<%=request.getAttribute("regNo")%>';
            }if('<%=request.getAttribute("vehTypeId")%>' !='null'){
            document.report.vehTypeId.value='<%=request.getAttribute("vehTypeId")%>';
            }if('<%=request.getAttribute("couponNo")%>' !='null'){
            document.report.couponNo.value='<%=request.getAttribute("couponNo")%>';
            }if('<%=request.getAttribute("fromKm")%>' !='null'){
            document.report.fromKm.value='<%=request.getAttribute("fromKm")%>';
            }if('<%=request.getAttribute("toKm")%>' !='null'){
            document.report.toKM.value='<%=request.getAttribute("toKm")%>';
            }if('<%=request.getAttribute("outFill")%>' !='null'){
            document.report.outFill.value='<%=request.getAttribute("outFill")%>';            
            }if('<%=request.getAttribute("usingCompId")%>' !='null'){
            document.report.usingCompId.value='<%=request.getAttribute("usingCompId")%>';
            }
            if('<%=request.getAttribute("owningCompId")%>' !='null'){
            document.report.owningCompId.value='<%=request.getAttribute("owningCompId")%>';
            }
            
            }
    
    </script>
    <body onload="getVehicleNos();setValues();">
        <form name="report" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Fuel Report Search</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr height="30">
    <td>Coupon Number</td>
    <td><input name="couponNo" class="form-control" type="text" value="" size="20"></td>
    <td>VehicleNo</td>
    <td><input name="regNo" id="regno" class="form-control" type="text" value="" size="20"></td>
    <td>Vehicle Type</td>
    <td><select class="form-control" name="vehTypeId" style="width:125px">
        <option value="1">---Select---</option>
        <c:if test = "${TypeList != null}" >
            <c:forEach items="${TypeList}" var="veh">
                <option value='<c:out value="${veh.typeId}" />'><c:out value="${veh.typeName}" /></option>
            </c:forEach>
        </c:if>
    </select></td>
    </tr>
    <tr height="30">
        <td>AvgKM From</td>
        <td><input name="fromKm" class="form-control" type="text" value="" size="20"></td>
        <td>AvgKM TO</td>
        <td><input name="toKM" class="form-control" type="text" value="" size="20"></td>
          <td>Filled Status</td>
        <td><select class="form-control" name="outFill" style="width:125px">
             <option value="0">--Select--</option>
                <option value="N">InFill</option>
                 <option value="Y">OutFill</option>
        </select></td>

    </tr>

    <tr height="30">
        <td><font color="red">*</font>From Date</td>
        <td><input name="fromDate" class="form-control" type="text"  size="20">
             <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.fromDate,'dd-mm-yyyy',this)"/></td>
        <td ><font color="red">*</font>TO Date</td>
        <td ><input name="toDate" class="form-control" type="text"  size="20">
             <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.toDate,'dd-mm-yyyy',this)"/></td>
        <td><font color="red">*</font>Filling Station</td>
        <td><select class="form-control" name="companyId" style="width:125px">
            <option value="0">---Select---</option>
            <c:if test = "${CompanyList != null}" >
                <c:forEach items="${CompanyList}" var="comp">
                <c:choose>
                <c:when test="${comp.companyTypeId==1012}" >
                    <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
                </c:when>
                </c:choose>
               </c:forEach>
            </c:if>
    </select></td>
    </tr>
    <tr>
        <td><font color="red">*</font>Owning Company</td>
        <td><select class="form-control" name="owningCompId" style="width:125px">
            <option value="0">---Select---</option>
            <c:if test = "${OwningCompanyList != null}" >
                <c:forEach items="${OwningCompanyList}" var="comp2">
                    <option value='<c:out value="${comp2.custId}" />'><c:out value="${comp2.custName}" /></option>
               </c:forEach>
            </c:if>
    </select></td>
        <td><font color="red">*</font>Using Company</td>
        <td><select class="form-control" name="usingCompId" style="width:125px">
            <option value="0">---Select---</option>
            <c:if test = "${UsingCompanyList != null}" >
                 <c:forEach items="${UsingCompanyList}" var="comp1">
                <c:choose>
                <c:when test="${comp1.activeInd=='Y'}" >
                   <option value='<c:out value="${comp1.usingCompId}" />'><c:out value="${comp1.companyName}" /></option>
                 </c:when>
                </c:choose>
               </c:forEach>
            </c:if>
    </select></td>
    <td colspan="2"><input type="button" class="button" name="Search" value="Fetch Data" onclick="submitPage(this.value);"></td>
    </tr>
</table>
</div></div>
</td>
</tr>
</table>
    <br>
            <c:if test = "${FuelFillingList!= null}" >
         <%
    
    
    ArrayList FuelFillingList = (ArrayList) request.getAttribute("FuelFillingList"); 
     if(FuelFillingList.size() != 0){
    %>         
                <table align="center" width="1300" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td colspan="4" class="contenthead">Fuel Report</td>
                    </tr>
                    
                    <tr height="30">
                        <td class="contentsub">Date</td>                           
                        <td class="contentsub">Filling Place</td>
                         <td class="contentsub">Coupon No</td>
                        <td class="contentsub">Filled Status</td>
                        <td class="contentsub">Vehicle No</td>                                                                  
                        <td class="contentsub">Mfr</td>                                                                  
                        <td class="contentsub">Model</td>                                                                  
                        <td class="contentsub">Usage Type</td>                                                                  
                        <td class="contentsub">Owning Company</td>                                                                  
                        <td class="contentsub">Using Company</td>                                                                  
                        <td class="contentsub">Litres</td>
                        <td class="contentsub">Amount</td>
                        <td class="contentsub">Present Km</td>                                                                
                        <td class="contentsub">Pre Km</td>                                                                
                        <td class="contentsub">Total Km</td>
                        <td class="contentsub">Avg Km</td>
                        <td class="contentsub">Reading</td>
                        <td class="contentsub">Capacity</td>
                      
                        
                        
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${FuelFillingList}" var="fuel"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">
                            
                            <td class="<%=classText %>"><input type="hidden" name="dates" value='<c:out value="${fuel.date}"/>'><c:out value="${fuel.date}"/></td>                             
                            <td class="<%=classText %>"><input type="hidden" name="companyNames" value='<c:out value="${fuel.companyName}"/>'><c:out value="${fuel.companyName}"/></td> 
                             <td class="<%=classText %>"><input type="hidden" name="couponNos" value='<c:out value="${fuel.couponNo}"/>'><c:out value="${fuel.couponNo}"/></td>
                            <c:if test="${fuel.outFill=='N'}">
                            <td class="<%=classText %>"><input type="hidden" name="outFills" value='InFill'>In Fill</td>
                        </c:if>
                         <c:if test="${fuel.outFill=='Y'}">
                            <td class="<%=classText %>"><input type="hidden" name="outFills" value='OutFill'>Out Fill</td>
                        </c:if>
                           
                            <td class="<%=classText %>"><input type="hidden" name="regNos" value='<c:out value="${fuel.regNo}"/>'><c:out value="${fuel.regNo}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="mfrs" value='<c:out value="${fuel.mfr}"/>'><c:out value="${fuel.mfr}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="models" value='<c:out value="${fuel.model}"/>'><c:out value="${fuel.model}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="usageTypes" value='<c:out value="${fuel.usageType}"/>'><c:out value="${fuel.usageType}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="owningComps" value='<c:out value="${fuel.owningComp}"/>'><c:out value="${fuel.owningComp}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="usingComps" value='<c:out value="${fuel.servicePtName}"/>'><c:out value="${fuel.servicePtName}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="fuels" value='<c:out value="${fuel.fuelFilled}"/>'><c:out value="${fuel.fuelFilled}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="amounts" value='<c:out value="${fuel.rate}"/>'><c:out value="${fuel.rate}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="presents" value='<c:out value="${fuel.present}"/>'><c:out value="${fuel.present}"/></td>                             
                            <td class="<%=classText %>"><input type="hidden" name="previous" value='<c:out value="${fuel.previous}"/>'><c:out value="${fuel.previous}"/></td>                             
                            <td class="<%=classText %>"><input type="hidden" name="totalKms" value='<c:out value="${fuel.totalKm}"/>'><c:out value="${fuel.totalKm}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="avgKms" value='<c:out value="${fuel.avgKm}"/>'><c:out value="${fuel.avgKm}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="runReadings" value='<c:out value="${fuel.runReading}"/>'><c:out value="${fuel.runReading}"/></td> 
                            <td class="<%=classText %>"><input type="hidden" name="tankReadings" value='<c:out value="${fuel.tankReading}"/>'><c:out value="${fuel.tankReading}"/></td> 
                                                        
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <center><input type="button" class="button" name="Excel" value="To Excel" onclick="submitExcel();"></center>
             <%
                 }
                 %>
           </c:if>
          <br>
          <%@ include file="/content/common/pagination.jsp"%>   
 <br><br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
