<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.designation.business.DesignationTO" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

    function submitpage(value)
    {


        var checValidate = selectedItemValidation();
        var splt = checValidate.split("-");
        if (splt[0] == 'SubmitForm' && splt[1] != 0) {


            document.modify.action = '/throttle/modifyDesig.do';
            document.modify.submit();
        }
    }

    function setSelectbox(i)
    {
        var selected = document.getElementsByName("selectedIndex");
        selected[i].checked = 1;
    }

    function selectedItemValidation() {
        var index = document.getElementsByName("selectedIndex");
        var desigName = document.getElementsByName("desigNames");
        var chec = 0;
        var mess = "SubmitForm";
        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (index[i].checked) {
                chec++;
                if (isEmpty(desigName[i].value)) {
                    alert("Enter The Designation Name");
                    desigName[i].focus();
                    mess = "";
                    break;
                } else if (isChar(desigName[i].value)) {
                    alert(" Designation Name must be in Character");
                    desigName[i].focus();
                    mess = "";
                    break;
                }
            }
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            desigName[0].focus();
//break;
        }
        return mess + "-" + chec;
    }

    function isChar(s) {
        if (!(/^-?\d+$/.test(s))) {
            return false;
        }
        return true;
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>


    <!--[if lte IE 7]>
    <style type="text/css">
    
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/ 
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AlterDesignation" text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>
                <li class=""><spring:message code="hrms.label.AlterDesignation" text="default text"/></li>



            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="document.modify.desigNames[0].focus();">
                    <form method="post" name="modify"> 
                        <%
                           int index = 0;
                       ArrayList DesignaList = (ArrayList) request.getAttribute("DesignaList");
                       System.out.println("size in jsp="+DesignaList.size() );
                        %>        

                        <table class="table table-info mb30 table-hover" >
                            <c:if test = "${DesignaList != null}" >

                                <thead>

                                    <tr>
                                        <th  height="30"><spring:message code="hrms.label.DesignationId" text="default text"/></th>
                                        <th  height="30"><spring:message code="hrms.label.DesignationName" text="default text"/> </th>
                                        <th  height="30"><spring:message code="hrms.label.Descriptions" text="default text"/></th>
                                        <th  height="30"><spring:message code="hrms.label.Status" text="default text"/></th>
                                        <th  height="30"><spring:message code="hrms.label.CheckBox" text="default text"/></th>
                                    </tr>
                                </thead>
                            </c:if>

                            <c:if test = "${DesignaList != null}" >
                                <c:forEach items="${DesignaList}" var="Desig"> 		
                                    <%

                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                    classText = "text2";
                                    } else {
                                    classText = "text1";
                                    }
                                    %>
                                    <tr>
                                        <td class="<%=classText %>" height="30"><input type="hidden" name="desigIds" value='<c:out value="${Desig.desigId}"/>'> <div align="center"><c:out value="${Desig.desigId}"/></div> </td>                                           
                                        <td class="<%=classText %>" height="30"><input type="text" class="form-control" style="width:220px;height:40px;" name="desigNames" value="<c:out value="${Desig.desigName}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                        <td class="<%=classText %>" height="30"><input type="text" class="form-control" style="width:220px;height:40px;" name="descriptions" value="<c:out value="${Desig.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                        <td height="30" class="<%=classText %>"> <div align="center"><select name="activeInds" class="form-control" style="width:220px;height:40px;" onchange="setSelectbox(<%= index %>)" style="width:125px">
                                                    <c:choose>
                                                        <c:when test="${Desig.activeInd == 'Y'}">
                                                            <option value="Y" selected><spring:message code="hrms.label.Active" text="default text"/></option>
                                                            <option value="N"><spring:message code="hrms.label.InActive" text="default text"/></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="Y"><spring:message code="hrms.label.Active" text="default text"/></option>
                                                            <option value="N" selected><spring:message code="hrms.label.InActive" text="default text"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </select>
                                            </div> 
                                        </td>
                                        <td width="77" height="30" class="<%=classText %>"><input type="checkbox" class="form-control" name="selectedIndex" value='<%= index %>'></td>
                                    </tr>
                                    <%
                                    index++;
                                    %>
                                </c:forEach >
                            </c:if> 
                        </table>
                        <center>
                            <br>
                            <input type="button" name="save" value="<spring:message code="hrms.label.SAVE" text="default text"/>" onClick="submitpage(this.name)" class="btn btn-success" />
                            <input type="hidden" name="reqfor" value="designa" />
                        </center>
                        <br>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
