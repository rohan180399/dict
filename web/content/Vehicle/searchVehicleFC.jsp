<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>

<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">

        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>
<script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        
                              <style>
                        #index td {
   color:white;
}
</style>
    <!--setImages(1,0,0,0,0,0);-->
     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehicleInspection"  text="Vehicle Inspection"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="stores.label.VehicleInspection"  text="Vehicle Inspection"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
        <body onLoad="getVehicleNos();
            setImages(0, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >

            <%@ include file="/content/common/message.jsp" %>
<table class="table table-bordered" >
              <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b> <spring:message code="trucks.label.VehicleInspection"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
            <tr>
                <td align="center" style="border-color:#5BC0DE;padding:16px;">
                         <table>
                            <tr>
                                <td><spring:message code="trucks.label.VehicleNumber"  text="default text"/></td>
                                <td>
                                   <input type="textbox" id="regno" name="regno" value="<c:out value="${regNo}" />" class="form-control" style="width:260px;height: 38px;">
                                </td>
<!--                                <td></td>
                                <td>-->
<!--                                    <select class="form-control" name="vendorId" id="vendorId" type="textbox" style="width:260px;height: 38px;">
                                              <option value="">---Select---</option>
                                                <c:if test = "${vendorListCompliance != null}" >
                                                    <c:forEach items="${vendorListCompliance}" var="list">
                                                        <option value='<c:out value="${list.vendorId}" />'><c:out value="${list.vendorName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>
                                             <script>
                                                 document.getElementById("vendorId").value ='<c:out value="${vendorId}"/>'
                                             </script>-->
                                <!--</td>-->  
<!--                            </tr>
                            <tr>-->
                                <td><font color="red">*</font><spring:message code="trucks.label.FromDate"  text="default text"/></td>
                                <td height="30"><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
                                <td><font color="red">*</font><spring:message code="trucks.label.ToDate"  text="default text"/></td>
                                <td height="30"><input name="toDate" id="toDate" value="<c:out value="${toDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
<!--                            </tr>
                            <tr>-->
                                <td></td> 
                                <td></td> 
                            </tr>
                            <tr colspane="4" >
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td><input type="button" class="btn btn-success" name="Search" value="<spring:message code="trucks.label.Search"  text="default text"/>" onclick="submitPage(this.name);">
                                </td>  
                            </tr>
                        </table>
                </td>
            </tr>
        </table>

            
            <br>

            <%
                        int index = 1;

            %>


            <c:if test="${vehiclesFCList == null }" >
                <br>
                <center><font color="red" size="2"> <spring:message code="trucks.label.NoRecordsFound"  text="default text"/> </font></center>
            </c:if>
            <c:if test="${vehiclesFCList != null }" >
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                             <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                            <th><spring:message code="trucks.label.VehicleNumber"  text="default text"/></th>
                            <!--<th>Company Name</th>-->
                            <th><spring:message code="trucks.label.FCDate"  text="default text"/></th>
                            <th><spring:message code="trucks.label.FCRenewableDate"  text="default text"/></th>
                            <th><spring:message code="trucks.label.RTODetails"  text="default text"/></th>
                            <th><spring:message code="trucks.label.FCAmount"  text="default text"/></th>
                            <th><spring:message code="trucks.label.ElapsedDays"  text="default text"/></th>
                            <c:if test="${listId == '1'}">
                            <th><spring:message code="trucks.label.InsuranceAction"  text="default text"/></th>
                            </c:if>
                            <c:if test="${listId == '2'}">
                            <th><spring:message code="trucks.label.RoadTaxAction"  text="default text"/></th>
                            </c:if>
                            <c:if test="${listId == '3'}">
                            <th><spring:message code="trucks.label.FCAction"  text="default text"/></th>
                            </c:if>
                            <c:if test="${listId == '4'}">
                            <th><spring:message code="trucks.label.PermitAction"  text="default text"/></th>
                            </c:if>
                            <c:if test="${listId == '5'}">
                            <th><spring:message code="trucks.label.AMCAction"  text="default text"/></th>
                            </c:if>
                            <c:if test="${fleetTypeId == '1'}">
                            <th><spring:message code="trucks.label.Action"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Rotation"  text="default text"/></th>
                           </c:if>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <c:forEach items="${vehiclesFCList}" var="veh" >
                     
                        <tr>
                            <td  height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /> </td>
                            <%--<td  height="30" style="padding-left:30px; "> <c:out value="${veh.companyId}" /> </td>--%>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.fcDate}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.fcExpiryDate}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.fcAmount}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.rtoDetail}" /></td>
                            <c:if test="${veh.elapsedDays < '0' }">
                            <td   height="30" style="padding-left:30px;"><font color="red" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <c:if test="${veh.elapsedDays >= '0' }">
                            <td   height="30" style="padding-left:30px;"><font color="green" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <c:if test="${fleetTypeId == '1'}">
                            <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=1&fleetTypeId=1'  ><spring:message code="trucks.label.edit"  text="default text"/> </a>
                                &nbsp;
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  ><spring:message code="trucks.label.view"  text="default text"/> </a>
                                &nbsp;
                                &nbsp;
                                <c:if test="${veh.activeInd != '0'}" >
                                    <spring:message code="trucks.label.closeJobCard"  text="default text"/>
                                </c:if>
                               <%-- <c:if test="${veh.activeInd == '0'}" >
                                    <a href='/throttle/createJobcard.do?vehicleId=<c:out value="${veh.vehicleId}" />'  >makeInActive </a>
                                </c:if> --%>
                            </td>
                            <td  align="left"><a href="/throttle/tyresRotation.do?vehicleId=<c:out value='${veh.vehicleId}' />&axleTypeId=<c:out value='${veh.axleId}' />"><spring:message code="trucks.label.Rotation"  text="default text"/></a></td>
                            </c:if>
                            <c:if test="${listId == '1'}">
                            <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'> <img src="/throttle/images/addlogos.png" height="20px;"/></a>
                                &nbsp;
                                &nbsp;
<!--                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  >view </a>
                                &nbsp;
                                &nbsp;-->
                            </td>
                            </c:if>
                            <c:if test="${listId == '2'}">
                            <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'><spring:message code="trucks.label.add"  text="default text"/> </a>
                                &nbsp;
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  ><spring:message code="trucks.label.view"  text="default text"/> </a>
                                &nbsp;
                                &nbsp;
                            </td>
                            </c:if>
                            <c:if test="${listId == '3'}">
                            <td  height="30" style="padding-left:50px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'><img src="/throttle/images/addlogos.png" height="20px;"/> </a>
<!--                                &nbsp;
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  >view </a>
                                &nbsp;
                                &nbsp;-->
                            </td>
                            </c:if>
                            <c:if test="${listId == '4'}">
                            <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'> <spring:message code="trucks.label.add"  text="default text"/></a>
                                &nbsp;
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  ><spring:message code="trucks.label.view"  text="default text"/> </a>
                                &nbsp;
                                &nbsp;
                            </td>
                            </c:if>
                            <c:if test="${listId == '5'}">
                            <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'><spring:message code="trucks.label.add"  text="default text"/> </a>
                                
<!--                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=1'  >view </a>
                                &nbsp;
                                &nbsp;-->
                            </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                        </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="trucks.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 7);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=3';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }

       function getVehicleNos() {
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        }

    </script>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


