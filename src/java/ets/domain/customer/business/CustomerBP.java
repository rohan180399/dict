package ets.domain.customer.business;

/**
 *
 * @author vidya
 *
 */
import ets.domain.customer.data.CustomerDAO;
import ets.domain.customer.business.CustomerTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;
import java.util.Iterator;

public class CustomerBP {

    private CustomerDAO customerDAO;

    public CustomerDAO getCustomerDAO() {
        return customerDAO;
    }

    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    public int processInsertCustomer(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.doInsertCustomer(customerTO, userId);

        return status;
    }

    public ArrayList processCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getCustomerList();

        return customerList;
    }

    public ArrayList lpsCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = customerDAO.getLPSCustomerList();

        return customerList;
    }

    public ArrayList processActiveCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        ArrayList activeCustomerList = new ArrayList();
        customerList = customerDAO.getCustomerList();
        CustomerTO customerTO = null;
        Iterator itr = customerList.iterator();
//        if (customerList.size() == 0) {
//            throw new FPBusinessException("EM-CUST-01");
//        }
        while (itr.hasNext()) {
            customerTO = new CustomerTO();
            customerTO = (CustomerTO) itr.next();
            if (customerTO.getCustStatus().equalsIgnoreCase("y")) {
                activeCustomerList.add(customerTO);
            }
        }
        return activeCustomerList;
    }

    public int processUpdateCustomer(ArrayList List, int userId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.doUpdateCustomer(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-03");
        }
        return status;
    }

    public int processInsertContract(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.doInsertContract(customerTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CONT-02");
        }
        return status;
    }

    public ArrayList processBunkList(String bunkName) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = customerDAO.getBunkList(bunkName);
        if (bunkList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bunkList;
    }

    public int processInsertBunk(CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        System.out.println("BP 1");
        status = customerDAO.doInsertBunk(customerTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-02");
        }
        return status;
    }

    public ArrayList processBunkalterList(String bunkId) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = customerDAO.getBunkalterList(bunkId);
        if (bunkList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bunkList;
    }

    public int processBunkUpdateList(CustomerTO customerTO, int userId, String bunkId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = customerDAO.doUpdateBunk(customerTO, userId, bunkId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return status;
    }

    public ArrayList processPackingList() throws FPBusinessException, FPRuntimeException {
        ArrayList packList = new ArrayList();
        packList = customerDAO.getPackingList();
        if (packList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return packList;
    }

    public String getToDestination(String destination) {
        //String destination = "";
        destination = destination + "%";
        destination = customerDAO.getToDestination(destination);
        return destination;
    }
public ArrayList getBillingTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList billingTypeList = new ArrayList();
        billingTypeList = customerDAO.getBillingTypeList();
        return billingTypeList;
    }
public ArrayList getAccountManagerList() throws FPBusinessException, FPRuntimeException {
        ArrayList accountManagerList = new ArrayList();
        accountManagerList = customerDAO.getAccountManagerList();
        return accountManagerList;
    }
public ArrayList getCustomerGroupList() throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = customerDAO.getCustomerGroupList();
        return result;
    }

   public ArrayList getCustomerName(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
             ArrayList getCustomerName = new ArrayList();
             getCustomerName = customerDAO.getCustomerName(customerTO);
             return getCustomerName;
         }
   public ArrayList getCustomerCodes(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
             ArrayList getCustomerCodes = new ArrayList();
             getCustomerCodes = customerDAO.getCustomerCodes(customerTO);
             return getCustomerCodes;
         }

           public String getCustomerCode(CustomerTO customerTO ,int insertContractRouteMater) {
	            String custCode = "";
	            custCode = customerDAO.getCustomerCode(customerTO,insertContractRouteMater);
	            return custCode;
       }

    public int editUpdateCustomer(CustomerTO customerTO ,int userId) {
	             int status = 0;
	            status = customerDAO.updateCustomer(customerTO,userId);
	            return status;
    }
    public int editUpdateCustomerCredit(CustomerTO customerTO ,int userId) {
	             int status = 0;
	            status = customerDAO.editUpdateCustomerCredit(customerTO,userId);
	            return status;
    }


  public ArrayList processCustomerLists(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
          ArrayList customerList = new ArrayList();
          customerList = customerDAO.getCustomerLists(customerTO);

          return customerList;
    }
  public ArrayList getCustomerDetails(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
          ArrayList customerList = new ArrayList();
          customerList = customerDAO.getCustomerDetails(customerTO);

          return customerList;
    }
  public ArrayList getCustomerCreditLimitDetails(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException {
          ArrayList customerList = new ArrayList();
          customerList = customerDAO.getCustomerCreditLimitDetails(customerTO);

          return customerList;
    }
  public int checkCustomerName(CustomerTO customerTO) throws FPBusinessException, FPRuntimeException{
        int customerName = 0;
        customerName = customerDAO.checkCustomerName(customerTO);
        return customerName;
    }
  public ArrayList getModelFuelList(String vehicleTypeId,String contractRateId) throws FPBusinessException, FPRuntimeException {
        ArrayList getModelFuelList = new ArrayList();
        getModelFuelList = customerDAO.getModelFuelList(vehicleTypeId,contractRateId);
        return getModelFuelList;
    }
  public int saveModelFuelDetails(String[] fuelTypeId,String[] fuelModelId,String contractRateId,String[] modelId,String[] fuelVehicle,String[] fuelDG,String[] totalFuel,Integer userId) throws FPRuntimeException, FPBusinessException {
        int insertstatus = 0;
        insertstatus = customerDAO.saveModelFuelDetails(fuelTypeId,fuelModelId,contractRateId,modelId,fuelVehicle,fuelDG,totalFuel,userId);
        return insertstatus;
    }

public ArrayList getStateList() throws FPBusinessException, FPRuntimeException {
        ArrayList getStateList = new ArrayList();
        getStateList = customerDAO.getStateList();
        return getStateList;
    }

public ArrayList getOrganizationList() throws FPBusinessException, FPRuntimeException {
        ArrayList getOrganizationList = new ArrayList();
        getOrganizationList = customerDAO.getOrganizationList();
        return getOrganizationList;
    }

public int checkGstNo(String gstNo) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = customerDAO.checkGstNo(gstNo);
       
        return status;
    }

  public ArrayList getTripCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        customerList = customerDAO.getTripCustomerList();
        return customerList;
    }
  public ArrayList getCustomerAttachments(CustomerTO customerTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = customerDAO.getCustomerAttachments(customerTO);
        return podDetails;
    }


    public int saveTripAttachments(String actualFilePath, String customerId, String podRemarks1, String fileSaved, CustomerTO customerTO, int userId) throws FPRuntimeException, FPBusinessException {
            int status = 0;
            status = customerDAO.saveTripAttachments(actualFilePath, customerId, podRemarks1, fileSaved, customerTO, userId);
            return status;
        }

}

