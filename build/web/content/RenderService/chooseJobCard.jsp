<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  

<script>

    function generateMRS(name)
    {
        if (document.generateMrs.mrsJobCardNumber.value == '') {
            alert("Please Select Job Card Number");
            document.generateMrs.mrsJobCardNumber.focus();
        }
        else if (name == 'generate') {
            var jcno = document.generateMrs.mrsJobCardNumber.value;
            if (checkISValid(jcno)) {
                document.generateMrs.action = '/throttle/generateMrsPage.do';
                document.generateMrs.submit();
            }
        } else if (name == 'search') {
            if (isSelect(document.generateMrs.mrsJobCardNumber, 'Job Card No')) {
            } else {
                document.generateMrs.action = '/throttle/searchMrs.do';
                document.generateMrs.submit();
            }
        }

    }
    function setFocus()
    {
        var jobcardId = '<%=request.getAttribute("jobcardId")%>';
        if (jobcardId != 'null') {
            document.generateMrs.mrsJobCardNumber.value = jobcardId;
        }

    }
    function checkISValid(jcno) {

        //alert(document.getElementById("mrsJobCardNumber1"));
        //alert(document.getElementByName("mrsJobCardNumber1").length);
        var c = 0;
        var JcNos = document.getElementsByName("mrsJobCardNumber1");

        for (var i = 0; i < JcNos.length; i++) {
            if (parseInt(JcNos[i].value) == parseInt(jcno))
                c++;
        }
        if (c == 1) {
            return true
        } else {
            alert("Enter Valid JobcardNo");
            document.generateMrs.mrsJobCardNumber.value = '';
            document.generateMrs.search.focus();
            document.generateMrs.mrsJobCardNumber.focus();
            return false;
        }

    }


</script>
<script>
    function changePageLanguage(langSelection) {

        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.GenerateMRS"  text="GenerateMRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="general.label.service"  text="Service"/></a></li>
            <li class="active"><spring:message code="service.label.GenerateMRS"  text="GenerateMRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onload="setFocus()">


                <form name="generateMrs"  method="post" >


                    <!-- message table -->

                    <%@ include file="/content/common/message.jsp"%>


                    <table  class="table table-info mb30 table-hover">
                        <thead>
                        <th colspan="4"><spring:message code="service.label.GenerateMRS" text="default text"/> </th>
                        </thead>

                        <tr>
                            <td><spring:message code="service.label.JobCardNo"  text="default text"/>
                            </td>
                            <td><input style="width:260px;height:40px;"  class="form-control" autocomplete="off" id="mrsJobCardNumber" name="mrsJobCardNumber" value="">
                            </td>
                            <td>
                                <input type="button" class="btn btn-success" value="<spring:message code="service.label.Go"  text="default text"/>" name="search" onClick="generateMRS(this.name);">&emsp;&emsp;
                                <input type="button" class="btn btn-success" value="<spring:message code="service.label.GenerateMaterialSlip"  text="default text"/>" name="generate" onClick="generateMRS(this.name);">
                            </td>



                            <c:if test = "${jobCardlist != null}" >
                                <c:forEach items="${jobCardlist}" var="mfr">
                                <input  name="mrsJobCardNumber1" id="mrsJobCardNumber1" value='<c:out value="${mfr.jobCardId}" />' type="hidden">
                            </c:forEach >
                        </c:if> 
                        <td></td>
                        </tr>
                    </table>



                    <c:if test = "${mrsList != null}" >
                        <% 
                int index=0; 
                 ArrayList mrsList = (ArrayList) request.getAttribute("mrsList"); 
                     if(mrsList.size() != 0){
                        %>
                        <div  id="tabVisible" >



                            <table align="center"  width=600" border="0" cellspacing="0" cellpadding="0" class="border">

                                <tr>
                                    <td><div class="contentsub"><spring:message code="service.label.SNo"  text="default text"/>
                                        </div></td>
                                    <td><div class="contentsub"><spring:message code="service.label.MaterialSlipNo"  text="default text"/></div></td>                    
                                    <td><div class="contentsub"><spring:message code="service.label.Technician"  text="default text"/></div></td>
                                    <td><div class="contentsub"><spring:message code="service.label.Date"  text="default text"/></div></td>
                                    <td><div class="contentsub"><spring:message code="service.label.ApprovalStatus"  text="default text"/></div></td>
                                </tr>
                                <c:forEach items="${mrsList}" var="service"> 
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                        classText = "text2";
                                        } else {
                                        classText = "text1";
                                        }
                                    %>
                                    <tr>
                                        <td><%=index+1%></td>
                                        <td><c:out value="${service.mrsId1}"/></td>
                                        <td><c:out value="${service.empName}"/> </td>
                                        <td><c:out value="${service.mrsCreatedDate}"/> </td>
                                        <td align="center"><a href="/throttle/mrsStatus.do?status=<c:out value="${service.status}"/>&mrsId=<c:out value="${service.mrsId1}"/>&mrsJobCardNumber=<c:out value="${service.mrsJobCardNumber}"/>"><c:out value="${service.status}"/></a></td>
                                            <%index++;%>
                                        </c:forEach>
                                <tr>                                              
                            </table>



                        </div>
                        <%
                        }
                        %>

                    </c:if>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>


    </div>
</div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>
