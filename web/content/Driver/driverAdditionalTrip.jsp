<%--
    Document   : AdditionalTrip
    Created on : Sep 4, 2012, 1:36:35 PM
    Author     : Ashok
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
    </head>

    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script type="text/javascript">
        function submitPage(value)
        {
            var driName =document.AdditionalTrip.driName.value;
            // var driName = driNameT.split("-", 0);
            // alert("===="+driName);
            document.AdditionalTrip.settlementId.value=value;

            if(!isEmpty(driName)){
                document.AdditionalTrip.action="/throttle/driverAdditionalTrip.do";
                document.AdditionalTrip.submit();
                
            }else {
                alert("Please Enter Driver Name");
                document.AdditionalTrip.driName.focus();
            }
        <%
                    String settleId = (String) request.getAttribute("settleId");
                    System.out.println("settleId = " + settleId);
                    pageContext.setAttribute("settleId", settleId);
        %>
            }
            function getaddbutt(){
                document.getElementById('addtable').style.display='block';
            }
            function savePage(){
                var embId =document.AdditionalTrip.embId.value;
                var driName =document.AdditionalTrip.driName.value;
                var regno =document.AdditionalTrip.regno.value;
                var embarkmentDate =document.AdditionalTrip.embarkmentDate.value;
                var remark =document.AdditionalTrip.remark.value;
                var routeName =document.AdditionalTrip.routeid.value;
                var fromDate =document.AdditionalTrip.fromDate.value;
                var toDate =document.AdditionalTrip.toDate.value;
               
                if((!(isEmpty(driName) && isEmpty(regno)) || !isEmpty(embarkmentDate) || !isEmpty(routeName) || !isEmpty(remark) || !isEmpty(fromDate) || !isEmpty(toDate) ))  {
                    document.AdditionalTrip.action="/throttle/saveAdditionalTripV.do";
                    document.AdditionalTrip.submit();
                    
                }
            }

            function getDriverName(){
                
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));
    
            }
            function getVehicleNos(){
                var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
            }
            function getRouteName(){
                
                var oTextbox = new AutoSuggestControl(document.getElementById("routeName"),new ListSuggestions("routeName","/throttle/handleRouteName.do?"));

            }
            window.onload = getVehicleNos;
         

            function setValues(){
                var driName = '<%=request.getAttribute("driName")%>';
                if(driName != "null"){
                    document.AdditionalTrip.driName.value=driName;
                }else{
                    document.AdditionalTrip.driName.value="";
                }
            }
            function displayCollapse(){
                if(document.getElementById("exp_table").style.display=="block"){
                    document.getElementById("exp_table").style.display="none";
                    document.getElementById("openClose").innerHTML="Open";                }
                else{
                    document.getElementById("exp_table").style.display="block";
                    document.getElementById("openClose").innerHTML="Close";
                }
            }
    </script>
    <body onload="setValues();">
        <form name="AdditionalTrip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;"><!--<img src="/throttle/images/close.jpg" width="25" height="25" />-->Close</span></td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:700;">
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="form-control" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                        </td>
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" class="border">
                <br>
                <%int flag = 0;%>
                <c:if test="${driverAdditional != null}">
                    <% int index = 0; %>
                    <tr>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Vehicle Number</td>
                        <td class="contenthead">Embarkment Date</td>
                        <td class="contenthead">Alighting Date</td>
                        <td class="contenthead">Alighting Status</td>
                        <td class="contenthead">Settlement Status</td>
                        <td class="contenthead">Remarks</td>
                    </tr>
                    <c:forEach items="${driverAdditional}" var="dri">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <tr>
                            <td class="<%=classText%>"><c:out value="${dri.empName}"/></td>
                            <td class="<%=classText%>"><c:out value="${dri.vehicleNo}"/></td>
                            <td class="<%=classText%>"><c:out value="${dri.embarkDate}"/></td>
                            <c:choose>
                                <c:when test="${dri.alightDate == '1899-11-30'}">
                                    <td class="<%=classText%>">&nbsp;</td>
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>"><c:out value="${dri.alightDate}"/></td>
                                </c:otherwise>
                            </c:choose>
                            <td class="<%=classText%>"><c:out value="${dri.alghtingStatus}"/></td>
                            <c:if test="${dri.actInd == 'Y'}">
                                <td class="<%=classText%>"><c:out value="Completed"/></td>
                                <%flag = 2;%>
                            </c:if>
                            <c:if test="${dri.actInd == 'N'}">
                                <td class="<%=classText%>"><c:out value="In Progress"/></td>
                                <%flag = 1;%>
                            </c:if>
                            <td class="<%=classText%>"><c:out value="${dri.remarks}"/></td>
                        </tr>
                        <c:if test="${dri.actInd == 'N'}">
                            <c:if test="${addSaveDriverStatus != null}">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table3" >
                                    <tr>
                                        <td>
                                            <center><font color="blue"><b>Driver Additional Trip successfully  </b></font></center>
                                        </td>
                                    </tr>
                                </table>
                            </c:if>
                            <c:if test="${addSaveDriverStatus == null}">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table3">
                                    <tr>
                                        <td align="center"><input type="button" name="addbutt"  id="addbutt" class="button" value=" ADD " onclick="getaddbutt()"> </td>
                                    </tr>
                                </table>
                            </c:if>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td></tr>

                            <tr>
                            <center><font color="blue">&nbsp;</font></center>
                            <table width="830" id="addtable"  cellpadding="0" cellspacing="2" border="0" align="center" class="TableMain" style="display: none" >
                                <tr>
                                <input name="embId" id="embId" type="hidden" value="<c:out value="${dri.embarkId}"/>">
                                <td class="texttitle2">Vehicle No</td>
                                <td class="texttitle2"><input name="regno" id="regno" type="text" class="form-control" size="20" value="<c:out value="${dri.vehicleNo}"/>" autocomplete="off" readonly></td>
                                <td class="texttitle2">Embarkment Date</td>
                                <td class="texttitle2"><input name="embarkmentDate" id="embarkmentDate" type="text" class="form-control" size="20" value="<c:out value="${dri.embarkDate}"/>" autocomplete="off" readonly></td>
                                <!--                                <td height="30"><input name="embarkmentDate" id="embarkmentDate" type="text" class="datepicker" autocomplete="off"></td>-->
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                        Route
                                    </td>
                                    <td class="texttitle1">
                                        <select name="routeid"  id="routeid" class="form-control" style="width:120px;">
                                            <option value=""> -Select- </option>
                                            <c:if test = "${routeList != null}" >
                                                <c:forEach items="${routeList}" var="rl">
                                                    <option value='<c:out value="${rl.routeId}" />'><c:out value="${rl.routeName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <input type="hidden" name="routeDetails" id="routeDetails" value='<c:out value="${routeData}" />' />
                                    </td>


                                    <td class="texttitle1">Additional Trip</td>
                                    <td class="texttitle1"><input name="addTrip" id="addTrip" type="text" class="form-control" size="20" value="" autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">Form Date</td>
                                    <td class="texttitle2" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" autocomplete="off"></td>
                                    <td class="texttitle2">To Date</td>
                                    <td class="texttitle2" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">Remarks</td>
                                    <td class="texttitle1"><textarea cols="30%" name="remark" id="remark"></textarea></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="center">
                                        <input type="button" class="button"  onclick="savePage(0);" value="Save">
                                        &emsp;<input type="reset" class="button" value="Clear">
                                    </td>
                                </tr>
                            </table>
                        </c:if>
                            <%index++;%>
                    </c:forEach>

                    </tr>
                </table>
            </c:if>

            <% if (flag == 2) {%>
            <br>
            <br>
            <br>
            <br>
            <center><font color="red">&nbsp; Driver Not Yet Assigned Any Route </font></center>

            <%}%>
            <c:if test="${driverEmbDetailSize == 0}">
                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table3" >
                    <tr>
                        <td>
                            <center><font color="red"><b>This Driver Not Yet Embarked </b></font></center>
                        </td>
                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
