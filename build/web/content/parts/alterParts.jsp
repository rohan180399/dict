<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script>
    function computeMrp() {
        var spriceVal = document.update.sprice.value;
        var w = document.update.vatId.selectedIndex;
        var taxPercentage = document.update.vatId.options[w].text;
        document.update.mrp.value = parseFloat(spriceVal) + (parseFloat(spriceVal) * parseFloat(taxPercentage) / 100);
    }
    function submitPage(value)

    {

        if (value == "alter") {


            if (isSelect(document.update.categoryId, "categoryName")) {
                return;
            } else
            if (isSelect(document.update.modelId, "modelName")) {
                return;
            }

            document.update.action = '/throttle/alterPartsdid.do';
            document.update.submit();
        }

    }

    var httpReq;
    var temp = "";
    function ajaxData()
    {
        var url = "/throttle/getModels1.do?mfrId=" + document.update.mfrId.value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processAjax();
        };
        httpReq.send(null);
    }


    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();

                setOptions(temp, document.update.modelId);
                document.update.modelId.value = document.update.model.value;
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }


    function ajaxRackData()
    {
        var url = "/throttle/getSubRack.do?rackId=" + document.update.rackId.value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processRackAjax();
        };
        httpReq.send(null);
    }


    function processRackAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();

                setOptions(temp, document.update.subRackId);
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
</script>
</head>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PARTS"  text="Spare Parts"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.PARTS"  text="Spare Parts"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form  name="update" method="post">
                    <c:if test="${GetPartsDetail!=null}">
                        <c:forEach items="${GetPartsDetail}" var="comp" >

                            <table class="table table-info mb30 table-hover">
                                <thead><tr><th colspan="4"><spring:message code="stores.label.PARTS"  text="Spare Parts"/></th></tr></thead>

                                <input type="hidden" class="form-control" style="width:260px;height:40px;" name="sectionId" value="0" >
                                <input type="hidden" class="form-control" style="width:260px;height:40px;" name="model" value='<c:out value="${comp.modelId}" />'  >
                                <tr>
                                    <td  ><spring:message code="stores.label.Category"  text="default text"/></td>
                                    <td   ><select class="form-control" style="width:260px;height:40px;"  name="categoryId" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${CategoryList != null}" >
                                                <c:forEach items="${CategoryList}" var="Cat"> 
                                                    <c:choose>
                                                        <c:when test="${comp.categoryId == Cat.categoryId}" >
                                                            <option selected value='<c:out value="${Cat.categoryId}" />'><c:out value="${Cat.categoryName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value='<c:out value="${Cat.categoryId}" />'><c:out value="${Cat.categoryName}" /></option>
                                                        </c:otherwise>
                                                    </c:choose> 

                                                </c:forEach >
                                            </c:if>   	
                                        </select></td>

                                    <td ><spring:message code="stores.label.Manufacturer"  text="default text"/></td>
                                    <td   ><select class="form-control" style="width:260px;height:40px;" name="mfrId"    onchange="ajaxData();" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${MfrList != null}" >
                                                <c:forEach items="${MfrList}" var="Mfr"> 
                                                    <c:choose>
                                                        <c:when test="${comp.mfrId == Mfr.mfrId}" >                                                    
                                                            <option selected value='<c:out value="${Mfr.mfrId}" />'><c:out value="${Mfr.mfrName}" /></option>                                                    
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value='<c:out value="${Mfr.mfrId}" />'><c:out value="${Mfr.mfrName}" /></option>                                                    
                                                        </c:otherwise>
                                                    </c:choose> 
                                                </c:forEach >
                                            </c:if>  	
                                        </select>
                                    </td>
                                </tr>
                                <tr >
                                    <td><spring:message code="stores.label.Model"  text="default text"/></td>
                                    <td  ><select class="form-control" style="width:260px;height:40px;"  name="modelId" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${ModelList != null}" >
                                                <c:forEach items="${ModelList}" var="Md">
                                                    <c:choose>
                                                        <c:when test="${comp.modelId == Md.modelId}" >
                                                            <option selected value='<c:out value="${Md.modelId}" />'><c:out value="${Md.modelName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option selected   value='<c:out value="${Md.modelId}" />'><c:out value="${Md.modelName}" /></option>
                                                        </c:otherwise>
                                                    </c:choose> 
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>  

                                    <td ><spring:message code="stores.label.MfrCode"  text="default text"/></td>
                                    <td ><input name="mfrCode" type="text"  class="form-control" style="width:260px;height:40px;" value='<c:out value="${comp.mfrCode}" />' ></td>
                                </tr>

                                <tr >
                                    <td ><spring:message code="stores.label.CompanyCode"  text="default text"/></td>
                                    <td ><input name="paplCode"  readonly type="text" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.paplCode}" />></td>

                                    <td  ><spring:message code="stores.label.ItemName"  text="default text"/><input type="hidden" name="itemId" value='<c:out value="${comp.itemId}" />' ></td>
                                    <td  ><input name="itemName" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${comp.itemName}" />' ></td>
                                </tr>
                                <tr>
                                    <td ><spring:message code="stores.label.Uom"  text="default text"/> </td>
                                    <td  ><select class="form-control" style="width:260px;height:40px;"  readonly="yes" name="uomId" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${UomList != null}" >
                                                <c:forEach items="${UomList}" var="UM"> 
                                                    <c:choose>
                                                        <c:when test="${comp.uomId == UM.uomId}" >                                                                                                
                                                            <option selected value='<c:out value="${UM.uomId}" />'><c:out value="${UM.uomName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>                                                                                        
                                                            <option value='<c:out value="${UM.uomId}" />'><c:out value="${UM.uomName}" /></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>   
                                        <%--
                                            <td ><spring:message code="stores.label.GroupName"  text="default text"/></td>
                                            <td  ><select class="form-control" style="width:260px;height:40px;"  readonly="yes" name="groupId" >
                                                    <option value="0">---Select---</option>
                                                    <c:forEach items="${groupList}" var="group">
                                                        <c:choose>
                                                        <c:when test="${group.groupId==comp.groupId}">
                                                            <option value='<c:out value="${group.groupId}"/>' selected><c:out value="${group.groupName}"/></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value='<c:out value="${group.groupId}"/>' ><c:out value="${group.groupName}"/></option>
                                                        </c:otherwise>
                                                            </c:choose>
                                                    </c:forEach>

                                </select></td>
                        </tr>
                        <tr>
                            <td  ><spring:message code="stores.label.TaxPercentage"  text="default text"/></td>
                            <td   ><select class="form-control" style="width:260px;height:40px;"  readonly="yes" name="vatId" >
                                    <option value="0">---Select---</option>
                                    <c:forEach items="${vatList}" var="vat">
                                        <c:choose>
                                        <c:when test="${vat.vatId==comp.vatId}">
                                            <option value='<c:out value="${vat.vatId}"/>' selected><c:out value="${vat.vat}"/></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='<c:out value="${vat.vatId}"/>' ><c:out value="${vat.vat}"/></option>
                                        </c:otherwise>
                                            </c:choose>
                                    </c:forEach>

                                </select></td>
                                        --%>
                                <input name="vatId"  type="hidden" value=<c:out value="${comp.vatId}" /> />
                                <input name="groupId"  type="hidden" value=<c:out value="${comp.groupId}" /> />
                                <td  ><spring:message code="stores.label.ItemSpecification"  text="default text"/></td>
                                <td  ><textarea class="form-control" style="width:260px;height:40px;"  name="specification"><c:out value="${comp.specification}" /></textarea></td>

                                </tr>
                                <tr>
                                    <td ><spring:message code="stores.label.IsReconditionable"  text="default text"/></td>
                                    <td >
                                        <select name="reConditionable"  class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.reConditionable}" /> >                                
                                            <c:if test="${comp.reConditionable=='Y' || comp.reConditionable=='y' }" >
                                                <option  value='Y' selected><spring:message code="stores.label.Yes"  text="default text"/> </option>
                                                <option  value='N' ><spring:message code="stores.label.No"  text="default text"/> </option>
                                            </c:if>
                                            <c:if test="${comp.reConditionable=='N' || comp.reConditionable=='n' }" >
                                                <option value='Y' ><spring:message code="stores.label.Yes"  text="default text"/> </option>
                                                <option value='N' selected ><spring:message code="stores.label.No"  text="default text"/> </option>
                                            </c:if>
                                        </select>
                                    </td>

                                    <td ><spring:message code="stores.label.ScrapUom"  text="default text"/> </td>
                                    <td  ><select class="form-control" style="width:260px;height:40px;"  name="scrapUomId" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${UomList != null}" >
                                                <c:forEach items="${UomList}" var="SOUM"> 
                                                    <c:choose>
                                                        <c:when test="${comp.scrapUomId == SOUM.uomId}" >                                                                                                
                                                            <option selected value='<c:out value="${SOUM.uomId}" />'><c:out value="${SOUM.uomName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>                                                                                        
                                                            <option value='<c:out value="${SOUM.uomId}" />'><c:out value="${SOUM.uomName}" /></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>   
                                </tr>
                                <tr>
                                    <td  ><spring:message code="stores.label.MaximumQty"  text="default text"/> </td>
                                    <td  ><input name="maxQuandity"  type="text" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.maxQuandity}" />></td>

                                    <td  ><spring:message code="stores.label.RoLevel"  text="default text"/></td>
                                    <td  ><input name="roLevel"  type="text" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.roLevel}" />></td>
                                </tr>
                                <tr >
                                    <td><spring:message code="stores.label.Rack"  text="default text"/></td>
                                    <td  ><select class="form-control" style="width:260px;height:40px;"  name="rackId" onChange="ajaxRackData();" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${getRackList != null}" >
                                                <c:forEach items="${getRackList}" var="RK"> 
                                                    <c:choose>
                                                        <c:when test="${comp.rackId == RK.rackId}" >                                                    
                                                            <option selected value='<c:out value="${RK.rackId}" />'><c:out value="${RK.rackName}" /></option>                                                    
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value='<c:out value="${RK.rackId}" />'><c:out value="${RK.rackName}" /></option>                                                    
                                                        </c:otherwise>
                                                    </c:choose> 
                                                </c:forEach >
                                            </c:if>  	
                                        </select>
                                    </td>

                                    <td ><spring:message code="stores.label.SubRack"  text="default text"/></td>
                                    <td  ><select class="form-control" style="width:260px;height:40px;"  name="subRackId" >
                                            <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                            <c:if test = "${SubRackList != null}" >
                                                <c:forEach items="${SubRackList}" var="SRK"> 
                                                    <c:choose>
                                                        <c:when test="${comp.subRackId == SRK.subRackId}" >                                                    
                                                            <option selected value='<c:out value="${SRK.subRackId}" />'><c:out value="${SRK.subRackName}" /></option>                                                    
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value='<c:out value="${SRK.subRackId}" />'><c:out value="${SRK.subRackName}" /></option>                                                    
                                                        </c:otherwise>
                                                    </c:choose> 
                                                </c:forEach >
                                            </c:if>  	
                                        </select></td>   
                                </tr>
                                <!--                        <tr>
                                                        <td  ><font color="red">*</font><spring:message code="stores.label.SellingPrice"  text="default text"/></td>
                                                        <td  ><input name="sprice"  type="text" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.sellingPrice}" /> onchange="computeMrp();"></td>
                                
                                                        <td  ><font color="red">*</font><spring:message code="stores.label.MRP(withtax)"  text="default text"/></td>
                                                        <td  ><input name="mrp"  readonly type="text" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.mrp}" /> </td>
                                                        </tr>-->
                            </table>
                            <input name="mrp"  readonly type="hidden" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.mrp}" /> />
                            <input name="sprice"  type="hidden" class="form-control" style="width:260px;height:40px;" value=<c:out value="${comp.sellingPrice}" /> onchange="computeMrp();" />

                        </c:forEach>    
                    </c:if>    

                    <br><br>
                    <center><input type="button" class="btn btn-success" value="<spring:message code="stores.label.ALTER"  text="default text"/>" name="alter" onClick="submitPage(this.name);" /></center>
                    <input type="hidden" value="" name="temp" >
                    <input type="hidden" name=" " value="itemId">

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
