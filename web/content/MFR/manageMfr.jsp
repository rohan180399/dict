
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addMfrPage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterMfr.do';
            }
            document.desigDetail.submit();
        }
    </script>
    
    <body>
        
        <form method="post" name="desigDetail">
           <%@ include file="/content/common/path.jsp" %>
     
<%@ include file="/content/common/message.jsp" %>

 <% int index = 0;  %>    
            <br>
            <c:if test = "${MfrList != null}" >
                <table width="550" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                     
                    <tr align="center">
                        <td height="30" class="contenthead"><div class="contenthead">S.No</div></td>
                        <td height="30" class="contenthead"><div class="contenthead">Manufacture Name</div></td>
                        <td height="30" class="contenthead"><div class="contenthead">Description</div></td>
                        <td height="30" class="contenthead"><div class="contenthead">Activity Group</div></td>
                        <td height="30" class="contenthead"><div class="contenthead">Body Works Group</div></td>
                        <td height="30" class="contenthead"><div class="contenthead">Status</div></td>
                        
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${MfrList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="20" > 
                            <td class="<%=classText %>" height="30" style="padding-left:30px; "><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="manufacturerId" value='<c:out value="${list.mfrId}"/>'> <c:out value="${list.mfrName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.description}"/></td>

                            <td class="<%=classText %>" height="30">                                
                                <c:if test = "${list.activityGroupId == 1}" >
                                    Volvo Group
                                </c:if>
                                <c:if test = "${list.activityGroupId == 2}" >
                                    Non Volvo Group
                                </c:if>
                            </td>
                            <td class="<%=classText %>" height="30">
                                <c:if test = "${list.bodyWorkGroupId == 3}" >
                                    Cargo
                                </c:if>
                                <c:if test = "${list.bodyWorkGroupId == 4}" >
                                    Hi End (Luxury)
                                </c:if>
                                <c:if test = "${list.bodyWorkGroupId == 5}" >
                                    Ordinary
                                </c:if>
                            </td>
                            <td class="<%=classText %>" height="30">
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <br>
                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">
                <c:if test = "${MfrList != null}" >
                    <input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="button">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
