<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleUtilizationReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test="${vehicleUtilizationList == null}">
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                </center>
            </c:if>
            <c:if test="${vehicleUtilizationList != null}">
                  <table align="center" border="1" id="table" class="sortable" style="width:1000px;" >
                    <thead>
                        <tr>
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>Vehicle Type</h3></th>
                           <th align="center"><h3>Fleet Center</h3></th>
                            <th align="center"><h3>Total Utilised Days</h3></th>
                            <th align="center"><h3>Total KM Run</h3></th>
                             <th align="center"><h3>Billed Km</h3></th>
                            <th align="center"><h3>Reefer Hrs Run</h3></th>
                            <th align="center"><h3>Vehicle Utilization(%)</h3></th>
                        </tr>
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${vehicleUtilizationList}" var="utilList">
                                  <c:set var="days" value="${totalDays}"/>
                                  <c:set var="percentage" value="${(utilList.utilisedDays*100) /days }"/>
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td class="<%=classText%>"  ><%=index++%></td>
                                            <td class="<%=classText%>"  >
                                               <a href="" onclick="viewVehicleDetails('<c:out value="${utilList.vehicleId}"/>')"><c:out value="${utilList.regNo}"/></a></td>
                                            <td class="<%=classText%>"   ><c:out value="${utilList.modelName}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${utilList.companyName}"/></td>
                                            <td class="<%=classText%>" align="right"    ><c:out value="${utilList.utilisedDays}"/></td>
                                            <td class="<%=classText%>" align="right"    ><c:out value="${utilList.runKm}"/></td>
                                              <td class="<%=classText%>" align="right"    ><c:out value="${utilList.billableKM}"/></td>
                                            <td class="<%=classText%>" align="right"    ><c:out value="${utilList.runHm}"/></td>
                                            <td class="<%=classText%>" align="right"  ><fmt:formatNumber pattern="##0.00" value="${percentage}"/>
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
