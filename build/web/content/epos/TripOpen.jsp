<%
    Date today = new Date();
    SimpleDateFormat sdftSql = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    String date = "";
    String time = "";
    String NewTripId = "";
    String userId = "101";
    Connection conn = null;

    try{
        //IdentityNo,DeviceId,RouteCode,OutKM,DriverName,CleanerStatus,DateTime
        String identityNo = request.getParameter("IdentityNo");
        String deviceId = request.getParameter("DeviceId");
        String routeId = request.getParameter("RouteCode");
        String outKm = request.getParameter("OutKM");
        String driverId = request.getParameter("DriverId");
        String cleanerStatus = request.getParameter("CleanerStatus");
        String locationId = request.getParameter("LocationCode");
        String customerId = request.getParameter("CustomerId");
        String tripType = request.getParameter("TripType");
        String openDateTime = request.getParameter("Datetime");
        
        FPLogUtils.fpErrorLog((new StringBuilder()).append("identityNo: "+identityNo));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("deviceId: "+deviceId));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("routeId: "+routeId));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("outKm: "+outKm));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("driverId: "+driverId));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("cleanerStatus: "+cleanerStatus));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("locationId: "+locationId));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("customerId: "+customerId));
        FPLogUtils.fpErrorLog((new StringBuilder()).append("openDateTime: "+openDateTime));

        if(openDateTime != null && openDateTime.contains("$")) {
            date = openDateTime.split("\\$")[0];
            date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
            time = openDateTime.split("\\$")[1];
            openDateTime = date+" "+time;
        } else if(openDateTime != null && openDateTime.contains(" ")) {
            date = openDateTime.split(" ")[0];
            date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
            time = openDateTime.split(" ")[1];
            openDateTime = date+" "+time;
        } else {
            openDateTime = sdftSql.format(today);
        }

        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);

        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        conn = DriverManager.getConnection(jdbcUrl, userName, password);
        Statement stmt = conn.createStatement();
        ResultSet SAR = stmt.executeQuery("SELECT Eligible_Flag FROM ra_route_master where route_id='"+routeId+"'");
        String eFlag="";
        while(SAR.next()){            
            eFlag = SAR.getString("Eligible_Flag");
        }
        //System.out.println("routeId: "+routeId);

        int eCount=0, count=1;
        if (eFlag.equals("Y")) {
            ResultSet res = stmt.executeQuery("SELECT Eligible_Count FROM ra_trip_eligible_status_epos where Route_Id=" + routeId);
            while (res.next()) {
                eCount = Integer.parseInt(res.getString("Eligible_Count"));
                System.out.println("eCount : "+eCount);
            }
            if (eCount==0) {
                PreparedStatement pstmt1 = conn.prepareStatement("INSERT INTO ra_trip_eligible_status_epos (Route_Id, Eligible_Count, Act_Ind, "
                        + "CreatedBy, CreatedDate) VALUES (?,?,?,?,now())");
                pstmt1.setString(1, routeId);
                pstmt1.setInt(2, count);
                pstmt1.setString(3, "Y");
                pstmt1.setString(4, userId);
                pstmt1.executeUpdate();
            }else{
                eCount = eCount + 1;
                PreparedStatement pstmt1 = conn.prepareStatement("UPDATE ra_trip_eligible_status_epos SET Eligible_Count = ?, "
                        + "ModifiedBy = ?, ModifiedDate = now() WHERE Route_Id = ?");
                pstmt1.setInt(1, eCount);
                pstmt1.setString(2, userId);
                pstmt1.setString(3, routeId);
                pstmt1.executeUpdate();
            }
        } 

        String regNo="";
        String vehicleId="";
        Statement stmt2 = conn.createStatement();
	 ResultSet res3 = stmt2.executeQuery("SELECT vr.reg_no,vdm.VehicleId from papl_vehicle_reg_no vr, ra_vehicle_driver_map_epos vdm where "
                + "vr.Vehicle_Id=vdm.VehicleId and vr.Active_Ind='Y' "
		+" and vdm.IdentityNo='"+identityNo+"'");
	while(res3.next()) {
		regNo = res3.getString("reg_no");
		vehicleId = res3.getString("VehicleId");
	}

        PreparedStatement pstmt = conn.prepareStatement("INSERT INTO ra_trip_open_close_epos (IdentityNo, DeviceId, RouteId, "
                + "OutKM, OpenLocationId, DriverId, CleanerStatus, CustomerId, TripType, OpenDateTime, vehicleid, Settlement_Flag, Status, Act_Ind, "
                + "CreatedBy, CreatedDate) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,now())");
        pstmt.setString(1, identityNo);
        pstmt.setString(2, deviceId);
        pstmt.setString(3, routeId);
        pstmt.setString(4, outKm);
        pstmt.setString(5, locationId);
        pstmt.setString(6, driverId);
        pstmt.setString(7, cleanerStatus);
        pstmt.setString(8, customerId);
        pstmt.setString(9, tripType);
        pstmt.setString(10, openDateTime);
        pstmt.setString(11, vehicleId);
        pstmt.setString(12, "N");
        pstmt.setString(13, "Open");
        pstmt.setString(14, "Y");
        pstmt.setString(15, userId);
        pstmt.executeUpdate();
               
        ResultSet res = stmt.executeQuery("SELECT last_insert_id() as tripId ");
        while(res.next()) {
            NewTripId = res.getString("tripId");            
        }
        
	Statement stmt3 = conn.createStatement();
        /*String routeName="";         
	 ResultSet res4 = stmt3.executeQuery("SELECT concat(from_location, ' - ', to_location) as RouteName FROM ra_route_master where route_id="+routeId);
	while(res4.next()) {
		routeName = res4.getString("RouteName");
	}*/
         String driverName="";
         Statement stmt4 = conn.createStatement();
	 ResultSet res5 = stmt3.executeQuery("SELECT Emp_Name FROM papl_emp_master where Emp_Id="+driverId);
	while(res5.next()) {
		driverName = res5.getString("Emp_Name");
	}
        //tripsheetid, tripdate, routeid, vehicleid, depdate, driverid, kmout;
         System.out.println("openDateTime "+openDateTime);
         String[] temp = null, temp1 = null;
                temp = openDateTime.split(" ");
                String sDate = temp[0];                
        PreparedStatement pstmt1 = conn.prepareStatement("INSERT INTO tripsheet (epostripid, tripdate, routeid, vehicleid, depdate, driverid, kmout, status) "
                + "VALUES (?,?,?,?,?, ?,?, ?)");
        pstmt1.setString(1, NewTripId);
        pstmt1.setString(2, sDate);
        pstmt1.setString(3, routeId);
        pstmt1.setString(4, vehicleId);
        pstmt1.setString(5, sDate);
        pstmt1.setString(6, driverName);
        pstmt1.setString(7, outKm);
        pstmt1.setString(8, "Y");
        pstmt1.executeUpdate();        
	out.print((NewTripId+"$"+regNo+"$").trim());        
       if(conn != null) {
            conn.close();
        }

    }catch(Exception e) {
        out.print("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Trip Open --> ").append(e.getMessage()).toString());
    }
    finally{
        if(conn != null) {
            conn.close();

        }
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.util.Date,java.text.SimpleDateFormat,ets.domain.util.FPLogUtils" %>