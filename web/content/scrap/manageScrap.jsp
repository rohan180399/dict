<%-- 
    Document   : manageParts
    Created on : Mar 2, 2009, 2:49:16 PM
    Author     : karudaiyar Subramaniam
--%>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.scrap.business.ScrapTO" %>
<%@ page import="ets.domain.section.business.SectionTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>PAPL</title>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

<script language="javascript">    
 

function submitPage(value){ 
if(value == "search"){
    
 
document.scrap.action = '/throttle/searchScrapPage.do';
 
}
else if(value == "save"){
        aletr("save");
        var index = document.getElementsByName("selectedIndex");
        var chec=0;
        for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
         alert("chk"+chec);
        }
       }
    document.scrap.action = '/throttle/saveScrapPage.do';

    
}
document.scrap.submit();
}
 
    function setFocus(){
        
        var sectionId='<%=request.getAttribute("sectionId")%>';        
        var uomId='<%=request.getAttribute("uomId")%>';        
       if(sectionId!='null'){           
         document.scrap.sectionId.value=sectionId;
         document.scrap.uomId.value=uomId;          
        }
    }
    
function setSelectbox(i)
{
 // alert(i);
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
 
}

</script>
       
       <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
       
       
</head>
<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<body onLoad="setFocus();">
<form name="scrap"  method="post" >


            
<%@ include file="/content/common/path.jsp" %>
        

<%@ include file="/content/common/message.jsp" %>


<table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg" >
 <tr >
<td colspan="5" class="contenthead"><div class="contenthead"> <spring:message code="stores.label.SEARCH"  text="default text"/>
</div></td>
</tr>
 
<tr >
<td class="text1" height="30"><spring:message code="stores.label.Section"  text="default text"/></td>
<td class="text1"  ><select class="form-control" name="sectionId" >
<option value="">---Select---</option>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="Dept"> 
<option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
</c:forEach >
</c:if>  	
</select></td>

<td class="text1" height="30"><spring:message code="stores.label.Uom"  text="default text"/></td>
<td class="text1"  ><select class="form-control" name="uomId" >
<option value="">---Select---</option>
<c:if test = "${UomList != null}" >
<c:forEach items="${UomList}" var="Dept"> 
<option value='<c:out value="${Dept.uomId}" />'><c:out value="${Dept.uomName}" /></option>
</c:forEach >
</c:if>  	
</select></td>   
<td class="text1"><input type="button" class="button" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" name="search" onClick="submitPage(this.name)"></td>
</tr>
</table>
 <% int index = 0;  %>    
            <br>
            <c:if test = "${SectionWiseList != null}" >
                <table width="500" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
 	 	 	 	 	 	 	 	 	 	 	 	                    
                    <tr align="center">
                        <td height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.S.No"  text="default text"/></div></td>
                        <td height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.CompanyCode"  text="default text"/></div></td>
                        <td height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.ItemName"  text="default text"/></div></td>
                        <td height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.Uom"  text="default text"/></div></td>                        
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${SectionWiseList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %> 
                        <tr>                             
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.paplCode}"/></td>
                            <input type="hidden" name="paplCodes" value="<c:out value="${list.paplCode}"/>">
                            <td class="<%=classText %>" height="30"><c:out value="${list.itemName}"/></td>
                            <input type="hidden" name="itemIds" value="<c:out value="${list.itemId}"/>">
                            <input type="hidden" name="itemNames" value="<c:out value="${list.itemName}"/>">
                            <td class="<%=classText %>" height="30"><c:out value="${list.uomName}"/></td>
                            <input type="hidden" name="uomId" value="<c:out value="${list.uomId}"/>">
                            <input type="hidden" name="uomName" value="<c:out value="${list.uomName}"/>">                                                            
                      </tr>
                        <%
            index++;
             
                        %>
                    </c:forEach >                    
                </table>
            </c:if>
<center>

 
<input type="hidden" value="" name="temp" >
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
 
</html>



