
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script>

    function setLevelValues(levelValue)
    {
        var temp = levelValue.split("~");
        //alert(temp[0]+":::"+temp[1]+":::"+temp[2]);
        document.add.primaryID.value = temp[0];
        document.add.levelgroupId.value = temp[1];
        document.add.fullName.value = temp[2];

    }
    function submitPage()
    {
        if (textValidation(document.add.ledgerName, 'ledgerName')) {
            return;
        } else if (document.add.primaryLevelList.value == '0') {
            alert("Please Select Group");
            return 'false';
        } else if (isEmpty(document.add.description.value)) {
            alert("Please Enter Description");
            document.add.description.focus();
            return;

        }
        document.add.action = '/throttle/saveNewLedger.do';
        document.add.submit();
    }

    function setValues(sno, ledgerID, ledgerName, ledgerCode, levelID, groupname, description, amountType, openingBalance, groupCode) {
        var count = parseInt(document.getElementById("count").value);
//                document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("ledgerID").value = ledgerID;
        document.getElementById("ledgerName").value = ledgerName;
        document.getElementById("ledgerCode").value = ledgerCode;
        document.getElementById("primaryLevelList").value = groupCode + '~' + levelID + '~' + groupname;
//                document.getElementById("primaryLevelList").value = groupname;
        document.getElementById("description").value = description;
        //   document.getElementById("acctType").value = amountType;
        document.getElementById("acctType").value = amountType;
        document.getElementById("acctType").disabled = "true";
        document.getElementById("openingBalance").value = openingBalance;
        document.getElementById("openingBalance").readOnly = "true";
        document.add.primaryID.value = groupCode;
        document.add.levelgroupId.value = levelID;
        document.add.fullName.value = groupname;


    }

</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageLedgerMaster" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance" text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageLedgerMaster" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="add" method="post" class="form-horizontal form-bordered">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr height="30" id="index">
                                <th colspan="4"  ><b><spring:message code="finance.label.AddLedger" text="default text"/></b></th>&nbsp;&nbsp;&nbsp;                    
                            </tr>
                        </thead>
                        <input type="hidden" name="primaryID" id="primaryID" />
                        <input type="hidden" name="levelgroupId" id="LevelGroupID" />
                        <input type="hidden" name="fullName" id="fullName"  />
                        <input type="hidden" name="ledgerCode" id="ledgerCode"  />
                        <input type="hidden" name="ledgerID" id="ledgerID"  />
                        <tr height="30">
                            <td ><font color="red">*</font><spring:message code="finance.label.LedgerName" text="default text"/></td>
                            <td >
                                <input name="ledgerName" type="text" class="form-control" id="ledgerName" value="" style="width:260px;height:40px;">
                            </td>
                            <!--                </tr>
                                            <tr height="30">-->
                            <td  height="30"><font color="red">*</font><spring:message code="finance.label.Primary/Levelmaster" text="default text"/></td>
                            <td  height="30">
                                <select class="form-control" name="primaryLevelList" id="primaryLevelList" style="width:260px;height:40px;" onchange="setLevelValues(this.value)">
                                    <option value="0">---<spring:message code="finance.label.Select" text="default text"/>---</option>
                                    <c:if test = "${ledgerlevelList != null}" >
                                        <c:forEach items="${ledgerlevelList}" var="levelM">
                                            <option value='<c:out value="${levelM.groupID}" />~<c:out value="${levelM.levelgroupId}" />~<c:out value="${levelM.levelgroupName}" />'><c:out value="${levelM.levelgroupName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font><spring:message code="finance.label.OpeningBalance" text="default text"/></td>
                            <td  height="30">
                                <input type="Hidden" name="openingBalance" id="openingBalance" style="width:260px;height:40px;" class="form-control">
                                <select class="form-control" name="acctType" id="acctType" class="amountType" style="width:260px;height:40px;">
                                    <option value="CREDIT"><spring:message code="finance.label.CREDIT" text="default text"/></option>
                                    <option value="DEBIT"><spring:message code="finance.label.DEBIT" text="default text"/></option>
                                </select>
                            </td>

                            <!--</tr>-->
                            <!--                <tr>
                                                <td  height="30"><font color="red">*</font>Opening Balance Date</td>
                                                <td  height="30">
                                                     <input name="openingBalanceDate" type="text" class="datepicker1"  id="openingBalanceDate" value=""/>
                                                </td>
                                            </tr>-->
                            <!--<tr>-->
                            <td  height="30"><font color="red">*</font><spring:message code="finance.label.Description" text="default text"/></td>
                            <td  height="30">
                                <textarea name="description" id="description" class="form-control" style="width:260px;height:40px;"></textarea>
                            </td>
                        </tr>

                    </table>
                    <center>
                        <input type="button" class="btn btn-success" value="<spring:message code="finance.label.Save" text="default text"/>" onclick="submitPage();" style="width:100px;height:35px;"/>
                        &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="finance.label.Clear" text="default text"/>" style="width:100px;height:35px;">
                    </center>
                    <br>
                    <!--<table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">-->
                    <c:if test = "${ledgerLists != null}" >
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th><spring:message code="finance.label.Sno" text="default text"/></th>
                            <th><spring:message code="finance.label.LedgerCode" text="default text"/></th>
                            <th><spring:message code="finance.label.LedgerName" text="default text"/></th>
                            <th><spring:message code="finance.label.GroupName" text="default text"/></th>
                            <th><spring:message code="finance.label.Description" text="default text"/></th>
                            <th><spring:message code="finance.label.ActiveStatus" text="default text"/></th>
                            <th><spring:message code="finance.label.Edit" text="default text"/></th>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <c:forEach items="${ledgerLists}" var="LL">
                                    <%
                                             sno++;
                                    %>
                                    <tr height="30">
                                        <td   align="left"> <%= sno%> </td>
                                        <td  align="left"> <c:out value="${LL.ledgerCode}" /></td>
                                        <td   align="left"> <c:out value="${LL.ledgerName}"/> </td>
                                        <td   align="left"> <c:out value="${LL.groupname}"/> </td>
                                        <td   align="left"> <c:out value="${LL.description}"/> </td>

                                        <td   align="left"> <c:out value="${LL.active_ind}"/> </td>
                                        <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${LL.ledgerID}" />', '<c:out value="${LL.ledgerName}" />', '<c:out value="${LL.ledgerCode}" />', '<c:out value="${LL.levelID}" />', '<c:out value="${LL.groupname}" />', '<c:out value="${LL.description}" />', '<c:out value="${LL.amountType}" />', '<c:out value="${LL.openingBalance}" />', '<c:out value="${LL.groupCode}" />');" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                    </table>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="finance.label.EntriesPerPage" text="default text"/></span>
                        </div>
                        <div id="navigation" >
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="finance.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of" text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>



