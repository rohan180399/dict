<%-- 
    Document   : containerMovementReport
    Created on : Apr 21, 2016, 12:40:49 PM
    Author     : Gulshan kumar
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("please select From Date");
                    document.getElementById('fromDate').focus();
                }
                if (document.getElementById('toDate').value == '') {
                    alert("please select To Date");
                    document.getElementById('toDate').focus();
                }
                else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleContainerMovement.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleContainerMovement.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }

            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }


            function viewCustomerProfitDetails(tripIds) {
                //alert(tripIds);
                window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>

        <style type="text/css">





            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }



        </style>
    </head>
    <body onload="setValue();
            sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">containerMovementReport</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr>
                                        <!--                                        <td>Month</td>
                                                                                <td>
                                                                                    <select class="" id="monthId" name="monthId">
                                                                                        <option value="0" selected> -Select- </option>
                                                                                        <option value="1">JAN</option>
                                                                                        <option value="2">FEB</option>
                                                                                        <option value="3">MAR</option>
                                                                                        <option value="4">APR</option>
                                                                                        <option value="5">MAY</option>
                                                                                        <option value="6">JUN</option>
                                                                                        <option value="7">JUL</option>
                                                                                        <option value="8">AUG</option>
                                                                                        <option value="9">SEP</option>
                                                                                        <option value="10">OCT</option>
                                                                                        <option value="11">NOV</option>
                                                                                        <option value="12">DEC</option>
                                                                                    </select>
                                        
                                                                                    <script>
                                                                                        document.getElementById("monthId").value = "<c:out value="${monthId}"/>";
                                                                                    </script>
                                                                                </td>-->
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>


                                        <td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                        <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>

            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >

                        <th><h3 align="center">Empty</h3></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Loni</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Dadri & TKD</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>DICT</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Total in TEU's</h3></center></th>
                        <th><h3 align="center">Total in TEU's</h3></th>
                    </tr>
                    <tr>
                        <th style="border-left:1px solid white"><h3>Transporter</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3></h3></th>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:forEach items="${containerEmptyMovementList}" var="emptyList">
                        <c:set var="emptydictTotaltwentyftTEUs" value="${emptyList.ownloniemptytwentyft + emptyList.owndadriTKDemptytwentyft + emptyList.owndictemptytwentyft}"></c:set>
                        <c:set var="emptydictTotalFourtyftTEUs" value="${emptyList.ownloniemptyfourtyft + emptyList.owndadriTKDemptyfourtyft + emptyList.owndictemptyfourtyft}"></c:set>
                        <c:set var="emptydictTotalTEUs" value="${emptydictTotaltwentyftTEUs + emptydictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.ownloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.ownloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="emptyleaseTotaltwentyftTEUs" value="${emptyList.leasedloniemptytwentyft + emptyList.leaseddadriTKDemptytwentyft + emptyList.leaseddictemptytwentyft}"></c:set>
                        <c:set var="emptyleaseTotalFourtyftTEUs" value="${emptyList.leasedloniemptyfourtyft + emptyList.leaseddadriTKDemptyfourtyft + emptyList.leaseddictemptyfourtyft}"></c:set>
                        <c:set var="emptyleaseTotalTEUs" value="${emptyleaseTotaltwentyftTEUs + emptyleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.leasedloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leasedloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="emptyOtherTotaltwentyftTEUs" value="${emptyList.otherloniemptytwentyft + emptyList.otherdadriTKDemptytwentyft + emptyList.otherdictemptytwentyft}"></c:set>
                        <c:set var="emptyOtherTotalFourtyftTEUs" value="${emptyList.otherloniemptyfourtyft + emptyList.otherdadriTKDemptyfourtyft + emptyList.otherdictemptyfourtyft}"></c:set>
                        <c:set var="emptyOtherTotalTEUs" value="${emptyOtherTotaltwentyftTEUs + emptyOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="loniTotalemptytwentyft" value="${emptyList.ownloniemptytwentyft + emptyList.leasedloniemptytwentyft + emptyList.otherloniemptytwentyft}"></c:set>
                        <c:set var="loniTotalemptyfourtyft" value="${emptyList.ownloniemptyfourtyft + emptyList.leasedloniemptyfourtyft + emptyList.otherloniemptyfourtyft}"></c:set>
                        <c:set var="dadriTKDemptytwentyft" value="${emptyList.owndadriTKDemptytwentyft + emptyList.leaseddadriTKDemptytwentyft + emptyList.otherdadriTKDemptytwentyft}"></c:set>
                        <c:set var="dadriTKDemptyfourtyft" value="${emptyList.owndadriTKDemptyfourtyft + emptyList.leaseddadriTKDemptyfourtyft + emptyList.otherdadriTKDemptyfourtyft}"></c:set>
                        <c:set var="dictTotalemptytwentyft" value="${emptyList.owndictemptytwentyft + emptyList.leaseddictemptytwentyft + emptyList.otherdictemptytwentyft}"></c:set>    
                        <c:set var="dictTotalemptyfourtyft" value="${emptyList.owndictemptyfourtyft + emptyList.leaseddictemptyfourtyft + emptyList.otherdictemptyfourtyft}"></c:set>    
                        <c:set var="TotalemptytwentyftTEU" value="${emptydictTotaltwentyftTEUs + emptyleaseTotaltwentyftTEUs + emptyOtherTotaltwentyftTEUs}"></c:set>    
                        <c:set var="TotalemptyfourtyftTEU" value="${emptydictTotalFourtyftTEUs + emptyleaseTotalFourtyftTEUs + emptyOtherTotalFourtyftTEUs}"></c:set>    
                        <c:set var="TotalemptyTEU" value="${ TotalemptytwentyftTEU + TotalemptyfourtyftTEU }"></c:set>    
                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${loniTotalemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${loniTotalemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptytwentyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptyfourtyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptyTEU}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br>
            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >

                        <th><h3 align="center">Export</h3></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Loni</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Dadri & TKD</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>DICT</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Total in TEU's</h3></center></th>
                        <th><h3 align="center">Total in TEU's</h3></th>
                    </tr>
                    <tr>
                        <th style="border-left:1px solid white"><h3>Transporter</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3></h3></th>
                    </tr>   
                </thead>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:forEach items="${containerExportMovementList}" var="exportList">
                        <c:set var="exportdictTotaltwentyftTEUs" value="${exportList.ownloniexporttwentyft + exportList.owndadriTKDexporttwentyft + exportList.owndictexporttwentyft}"></c:set>
                        <c:set var="exportdictTotalFourtyftTEUs" value="${exportList.ownloniexportfourtyft + exportList.owndadriTKDexportfourtyft + exportList.owndictexportfourtyft}"></c:set>
                        <c:set var="exportdictTotalTEUs" value="${exportdictTotaltwentyftTEUs + exportdictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.ownloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.ownloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="exportleaseTotaltwentyftTEUs" value="${exportList.leasedloniexporttwentyft + exportList.leaseddadriTKDexporttwentyft + exportList.leaseddictexporttwentyft}"></c:set>
                        <c:set var="exportleaseTotalFourtyftTEUs" value="${exportList.leasedloniexportfourtyft + exportList.leaseddadriTKDexportfourtyft + exportList.leaseddictexportfourtyft}"></c:set>
                        <c:set var="exportleaseTotalTEUs" value="${exportleaseTotaltwentyftTEUs + exportleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.leasedloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leasedloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="exportOtherTotaltwentyftTEUs" value="${exportList.otherloniexporttwentyft + exportList.otherdadriTKDexporttwentyft + exportList.otherdictexporttwentyft}"></c:set>
                        <c:set var="exportOtherTotalFourtyftTEUs" value="${exportList.otherloniexportfourtyft + exportList.otherdadriTKDexportfourtyft + exportList.otherdictexportfourtyft}"></c:set>
                        <c:set var="exportOtherTotalTEUs" value="${exportOtherTotaltwentyftTEUs + exportOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.otherloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="loniTotalexporttwentyft" value="${exportList.ownloniexporttwentyft + exportList.leasedloniexporttwentyft + exportList.otherloniexporttwentyft}"></c:set>
                        <c:set var="loniTotalexportfourtyft" value="${exportList.ownloniexportfourtyft + exportList.leasedloniexportfourtyft + exportList.otherloniexportfourtyft}"></c:set>
                        <c:set var="dadriTKDexporttwentyft" value="${exportList.owndadriTKDexporttwentyft + exportList.leaseddadriTKDexporttwentyft + exportList.otherdadriTKDexporttwentyft}"></c:set>
                        <c:set var="dadriTKDexportfourtyft" value="${exportList.owndadriTKDexportfourtyft + exportList.leaseddadriTKDexportfourtyft + exportList.otherdadriTKDexportfourtyft}"></c:set>
                        <c:set var="dictTotalexporttwentyft" value="${exportList.owndictexporttwentyft + exportList.leaseddictexporttwentyft + exportList.otherdictexporttwentyft}"></c:set>    
                        <c:set var="dictTotalexportfourtyft" value="${exportList.owndictexportfourtyft + exportList.leaseddictexportfourtyft + exportList.otherdictexportfourtyft}"></c:set>    
                        <c:set var="TotalexporttwentyftTEU" value="${exportdictTotaltwentyftTEUs + exportleaseTotaltwentyftTEUs + exportOtherTotaltwentyftTEUs}"></c:set>    
                        <c:set var="TotalexportfourtyftTEU" value="${exportdictTotalFourtyftTEUs + exportleaseTotalFourtyftTEUs + exportOtherTotalFourtyftTEUs}"></c:set>    
                        <c:set var="TotalexportTEU" value="${ TotalexporttwentyftTEU + TotalexportfourtyftTEU }"></c:set>  
                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${loniTotalexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${loniTotalexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexporttwentyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexportfourtyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexportTEU}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead> 
               <tr height="25" align="right">
                   <th align="center"><h3>Import</h3></th>
                    <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Loni</h3></center></th>
                    <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Dadri & TKD</h3></center></th>
                    <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>DICT</h3></center></th>
                    <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Total in TEU's</h3></center></th>
                    <th><h3 align="center">Total in TEU's</h3></th>
                </tr>

                <tr>
                        <th style="border-left:1px solid white"><h3>Transporter</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3></h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    
                        
                   <c:forEach items="${containerImportMovementList}" var="importList">
                        <c:set var="importdictTotaltwentyftTEUs" value="${importList.ownloniImporttwentyft + importList.owndadriTKDImporttwentyft + importList.owndictImporttwentyft}"></c:set>
                        <c:set var="importdictTotalFourtyftTEUs" value="${importList.ownloniImportfourtyft + importList.owndadriTKDImportfourtyft + importList.owndictImportfourtyft}"></c:set>
                        <c:set var="importdictTotalTEUs" value="${exportdictTotaltwentyftTEUs + exportdictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.ownloniImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.ownloniImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.owndadriTKDImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.owndadriTKDImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.owndictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.owndictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importdictTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importdictTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importdictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="importleaseTotaltwentyftTEUs" value="${importList.leasedloniImporttwentyft + importList.leaseddadriTKDImporttwentyft + importList.leaseddictImporttwentyft}"></c:set>
                        <c:set var="importleaseTotalFourtyftTEUs" value="${importList.leasedloniImportfourtyft + importList.leaseddadriTKDImportfourtyft + importList.leaseddictImportfourtyft}"></c:set>
                        <c:set var="importleaseTotalTEUs" value="${importleaseTotaltwentyftTEUs + importleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.leasedloniImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leasedloniImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddadriTKDImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddadriTKDImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importleaseTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importleaseTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="importOtherTotaltwentyftTEUs" value="${importList.ownloniImporttwentyft + importList.otherdadriTKDImporttwentyft + importList.otherdictImporttwentyft}"></c:set>
                        <c:set var="importOtherTotalFourtyftTEUs" value="${importList.ownloniImportfourtyft + importList.otherdadriTKDImportfourtyft + importList.otherdictImportfourtyft}"></c:set>
                        <c:set var="importOtherTotalTEUs" value="${importOtherTotaltwentyftTEUs + importOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.otherloniImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherloniImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherdadriTKDImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherdadriTKDImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherdictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherdictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importOtherTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importOtherTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="loniTotalImporttwentyft" value="${importList.ownloniImporttwentyft + importList.leasedloniImporttwentyft + importList.otherloniImporttwentyft}"></c:set>
                        <c:set var="loniTotalImportfourtyft" value="${importList.ownloniImportfourtyft + importList.leasedloniImportfourtyft + importList.otherloniImportfourtyft}"></c:set>
                        <c:set var="dadriTKDImporttwentyft" value="${importList.owndadriTKDImporttwentyft + importList.leaseddadriTKDImporttwentyft + importList.otherdadriTKDImporttwentyft}"></c:set>
                        <c:set var="dadriTKDImportfourtyft" value="${importList.owndadriTKDImportfourtyft + importList.leaseddadriTKDImportfourtyft + importList.otherdadriTKDImportfourtyft}"></c:set>
                        <c:set var="dictTotalImporttwentyft" value="${importList.owndictImporttwentyft + importList.leaseddictImporttwentyft + importList.otherdictImporttwentyft}"></c:set>    
                        <c:set var="dictTotalImportfourtyft" value="${importList.owndictImportfourtyft + importList.leaseddictImportfourtyft + importList.otherdictImportfourtyft}"></c:set>    
                        <c:set var="TotalImporttwentyftTEU" value="${importdictTotaltwentyftTEUs + importleaseTotaltwentyftTEUs + importOtherTotaltwentyftTEUs}"></c:set>    
                        <c:set var="TotalImportfourtyftTEU" value="${importdictTotalFourtyftTEUs + importleaseTotalFourtyftTEUs + importOtherTotalFourtyftTEUs}"></c:set>    
                        <c:set var="TotalImportTEU" value="${ TotalImporttwentyftTEU + TotalImportfourtyftTEU }"></c:set>  
                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${loniTotalImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${loniTotalImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalImporttwentyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalImportfourtyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalImportTEU}"/></td>
                        </tr>
                    </c:forEach>     
                </tbody>

            </table>
            <br>
            <br>
             <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead> 
                <tr height="25" >
                    <th align="center"><h3>Summary</h3></th>
                    <th align="center"><h3>20'</h3></th>
                    <th align="center"><h3>40'</h3></th>
                    <th align="center"><h3>Total In TEU's</h3></th>
                   
                </tr>
                </thead>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:set var="TotalEmptyTEU" value="${TotalemptytwentyftTEU + TotalemptyfourtyftTEU}"></c:set>
                        <tr height="30">
                            <td align="left">Total Empty</td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptytwentyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalemptyfourtyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalEmptyTEU}"/></td>
                    </tr>

                    <c:set var="TotalExportTEU" value="${TotalexporttwentyftTEU + TotalexportfourtyftTEU}"></c:set>
                        <tr height="30">
                            <td align="left">Total Export</td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexporttwentyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalexportfourtyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalExportTEU}"/></td>
                    </tr>

                    <c:set var="TotalImportTEU" value="${dictTotalimporttwentyft + dictTotalimportfourtyft}"></c:set>
                        <tr height="30">
                            <td align="left">Total Import/DSO</td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalimporttwentyft}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${dictTotalimportfourtyft}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalImportTEU}"/></td>
                    </tr>

                    <c:set var="TotaltwentyTEU" value="${TotalemptytwentyftTEU + TotalexporttwentyftTEU + dictTotalimporttwentyft}"></c:set>
                    <c:set var="TotalfourtyTEU" value="${TotalemptyfourtyftTEU + TotalexportfourtyftTEU + dictTotalimportfourtyft}"></c:set>
                    <c:set var="TotalTEUs" value="${TotaltwentyTEU + TotalfourtyTEU}"></c:set>
                        <tr height="30">
                            <td align="left"><b>Total</b></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotaltwentyTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalfourtyTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalTEUs}"/></td>
                    </tr>
                </tbody>

            </table>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>