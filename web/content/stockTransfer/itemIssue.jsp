
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>

        <title>MRSList</title>
    </head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript">

        function submitPage(value)
        {
            var checValidate = selectedItemValidation();
        }

        function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex") ;
            selected[i].checked=1;
        }
        function submitRcIssue(val){
            var itemId = document.getElementsByName("itemIds");
            var approvedQtys = document.getElementsByName("approvedQtys");
            var app = parseFloat(approvedQtys[val].value);
            
            var url = '/throttle/issueRc.do?requestId='+document.stockTransfer.requestId.value+'&itemId='+itemId[val].value+'&approvedQtys='+app;
            url = url + '&rowIndex='+val;
            
            window.open(url , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
            // document.stockTransfer.action='/throttle/issueRc.do';
            // document.stockTransfer.submit();

        }



        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var itemId = document.getElementsByName("itemIds");
            var issuedQty = document.getElementsByName("issuedQtys");
            var approvedQty = document.getElementsByName("approvedQtys");
            var stockQty = document.getElementsByName("stockQtys");
            var rcQty = document.getElementsByName("rcQtys");
            var remark = document.getElementsByName("remarks");
            var tyreId = document.getElementsByName("tyreId");
            var cntr = 0;
            var tot=0;
            var itemValue=0;
            var itemIndex=0;
            alert(index.length);
            for(var i=0;(i<index.length && index.length!=0);i++){
                if(index[i].checked){
                    cntr++;
                    if(parseInt(itemValue)!=parseInt(itemId[i].value)){
                        tot=parseFloat(issuedQty[i].value)+parseFloat(rcQty[i].value);
                        itemValue=itemId[i].value;
                        itemIndex=i;
                    }else{
                        tot=parseFloat(tot)+parseFloat(issuedQty[i].value)+parseFloat(rcQty[i].value);
                    }


                    if((parseFloat(issuedQty[i].value)) > parseFloat(stockQty[i].value) ){
                        alert("Issued Quantity Should not be Greater Than Stock  Quantity");
                        issuedQty[i].focus();
                        return;
                    }
                    if(parseFloat(tot)>parseFloat(approvedQty[itemIndex].value))
                    {

                        alert("Issued Quantity Should not be Greater Than Approved Quantity");
                        issuedQty[itemIndex].focus();
                        return;
                    }
                    if(floatValidation(issuedQty[i],'approvedQty')){
                        return;
                    }
                }
                <%--if((issuedQty[i].value)  > stockQty[i].value){
                alert("Issued Quantity Shouls not be Greater Than Stock Quantity");
                return;
                }--%>
            }
            if(cntr==0){
                alert("No Items to Receive");
                return;
            }
            if(textValidation(document.stockTransfer.remarks,'Remarks')){
                return;
            }
            document.stockTransfer.action='/throttle/issuedItemToTransfer.do';
            document.stockTransfer.submit();

        }
        function setFocus(){
           

            var appr = '<% request.getAttribute("approvedQtys");%>'
            if(appr!=null)
                {
           <%--         alert("Session Quantity-->"+appr);--%>
                }
        }

        function test()
        {
            var test = document.stockTransfer.rcItemIds.value;
        }

        function fixTyrePrice(ind)
        {
            var price = document.getElementsByName("priceIds");
            var issue = document.getElementsByName("issuedQtys");
            var tyres = document.getElementsByName("tyres");
            var tyre = document.getElementsByName("tyreId");
            var tyreType = document.getElementsByName("tyreType");
            var splt = tyres[ind].value.split("-");
            tyre[ind].value = splt[0];
            price[ind].value = splt[1];
            tyreType[ind].value = splt[2];
        }


    </script>

    <form method="post"  name="stockTransfer">
        <body onload="setFocus();">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <% int index = 0;%>
            <c:if test = "${ApprovedStockListAll != null}" >
                <c:if test = "${PriceList != null}" >
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" id="bg" class="border">
                        <tr>
                            <td colspan="9" height="80" align="center" class="contenthead" ><div class="contenthead">Issue Item</div></td>

                        </tr>

                        <c:forEach items="${ApprovedStockListAll}" var="mpr">                          
                            <% if (index == 0) {%>
                            <input type="hidden" name="requestId" value=<c:out value="${mpr.requestId}"/> >
                            <tr>
                                <td class="text1" height="30">REQUEST ID</td>
                                <td class="text1" height="30"> <c:out value="${mpr.requestId}"/> </td>
                            </tr>
                            <tr>
                                <td class="text2" height="30">SERVICE POINT</td>
                                <td class="text2" height="30"> <c:out value="${mpr.servicePointName}"/> </td>
                            </tr>
                            <input type="hidden" name="approvedId" value="<c:out value="${mpr.approvedId}"/>">
                            <% index++;
                                        }%>
                        </c:forEach>

                    </table>
                    <br>
                    <br>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" id="bg" class="border">
                        <%index = 0;
                                    String classText = "";
                                    int oddEven = 0;
                        %>

                        <c:if test = "${ApprovedStockListAll != null}" >                          
                            <tr>
                                <td colspan="11" align="center" class="text2" height="30"><strong>Approved Items</strong></td>
                            </tr>
                            <tr>
                                <td class="contenthead" height="30"><div class="contenthead">Mfr Code</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">Papl Code</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">Name</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">Uom</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">Apprd Qty</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">New Qty</div></td>
<!--                                <td class="contenthead" height="30"><div class="contenthead">RcItemQty</div></td>-->
                                <td class="contenthead" height="30"><div class="contenthead"> Vat(%)</div></td>
                                <td class="contenthead" height="30"><div class="contenthead">Price</div></td>
<!--                                <td class="contenthead" height="30"><div class="contenthead">Price Type</div></td>-->
                                <td class="contenthead" height="30"><div class="contenthead">StockQty</div></td>
<!--                                <td class="contenthead" height="30"><div class="contenthead">RcItem</div></td>-->
                                <td class="contenthead" height="30"><div class="contenthead">Select</div></td>
                            </tr>
                            <c:set var="isfirst" value="true"/>
                            <c:set var="itemId" value=''/>
                            <c:set var="index1" value='0'/>
                            

                                    <c:forEach items="${newArray}" var="new">
                                        <c:if test="${new.categoryId != '1011'}" >                                                                   
                                                <c:if test="${itemId == '' || itemId != new.itemId}">                                                    
                                                    <c:set var="isfirst" value="true"/>
                                                </c:if>
                                            <c:if test="${new.stockQty== '0.00' && isfirst }">                                                
                                                <c:set var="isfirst" value="false"/>                                        
                                            <%
                                                        oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                                            %>
                                        
                                            
                                            <tr>

                                                <td class="<%=classText%>" height="30">  <c:out value="${new.mfrCode}"/></td>
                                                <td class="<%=classText%>" height="30"><c:out value="${new.paplCode}"/></td>
                                                <td class="<%=classText%>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${new.itemId}"/>"><c:out value="${new.itemName}"/></td>
                                                <td class="<%=classText%>" height="30"> <c:out value="${new.uomName}"/> </td>
                                                <td class="<%=classText%>" height="30"><input type="hidden" id="approvedQtys<%=index%>" name="approvedQtys" value="<c:out value="${new.approvedQuandity}"/>"><c:out value="${new.approvedQuandity}"/></td>
                                                <td class="<%=classText%>" height="30"><input type="text" id="issuedQtys<%=index%>"  name="issuedQtys" size="5" class="form-control" value="0" onchange="setSelectbox('<%= index%>');" > </td>
<!--                                                <td class="<%=classText%>" height="30"><input type="text" size="5" name="rcQtys" readonly class="form-control" value="0"></td>-->
                                                <input type="hidden" size="5" name="rcQtys" value="0">
                                                <td class="<%=classText%>" ><input type="text" readonly name="taxs" size="5" value="<c:out value="${new.tax}"/>"> </td>
                                                <td class="<%=classText%>" ><input type="text" readonly name="priceIds" size="5" value="<c:out value="${new.price}"/>"> </td>
<!--                                                <td class="<%=classText%>" ><input type="hidden" readonly name="priceTypes" size="5" value="<c:out value="${new.pricetype}"/>"> </td>-->
                                                <td class="<%=classText%>" height="30"><input type="text" name="stockQtys" size="5" readonly value="<c:out value="${new.stockQty}"/>"></td>
<!--                                                <td class="<%=classText%>" align="left"> <a  href="" onclick="submitRcIssue(<%=index%>);" >RcItem</a> </td>-->
                                            <input type="hidden" name="categoryId" value="<c:out value="${new.categoryId}"/>" >
                                            <input type="hidden" name="tyreId" value="" >
                                            <input type="hidden" name="tyres" value="" >
                                            <input type="hidden" name="tyreType" value="" >

                                            <td width="77" height="30" class="<%=classText%>"><input type="checkbox" name="selectedIndex" value='<%= index%>'></td>

                                            <%
                                                        index++;
                                            %>

                                            
                                            </tr>
                                            
                                            <c:set var="itemId" value="${new.itemId}"/>
                                          
                                            
                                    </c:if>
                                    <c:if test="${new.stockQty!= '0.00'}">   
                                               
                                                <%
                                                        oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                                            %>


                                           <tr>

                                                <td class="<%=classText%>" height="30">  <c:out value="${new.mfrCode}"/></td>
                                                <td class="<%=classText%>" height="30"><c:out value="${new.paplCode}"/></td>
                                                <td class="<%=classText%>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${new.itemId}"/>"><c:out value="${new.itemName}"/></td>
                                                <td class="<%=classText%>" height="30"> <c:out value="${new.uomName}"/> </td>
                                                <td class="<%=classText%>" height="30"><input type="hidden" id="approvedQtys<%=index%>" name="approvedQtys" value="<c:out value="${new.approvedQuandity}"/>"><c:out value="${new.approvedQuandity}"/></td>
                                                <td class="<%=classText%>" height="30"><input type="text" id="issuedQtys<%=index%>"  name="issuedQtys" size="5" class="form-control" value="0" onchange="setSelectbox('<%= index%>');" > </td>
<!--                                                <td class="<%=classText%>" height="30"><input type="text" size="5" name="rcQtys" readonly class="form-control" value="0"></td>-->
                                                <input type="hidden" size="5" name="rcQtys" readonly class="form-control" value="0">
                                                <td class="<%=classText%>" ><input type="text" readonly name="taxs" size="5" value="<c:out value="${new.tax}"/>"> </td>
                                                <td class="<%=classText%>" ><input type="text" readonly name="priceIds" size="5" value="<c:out value="${new.price}"/>"> </td>
<!--                                                <td class="<%=classText%>" ><input type="hidden" readonly name="priceTypes" size="5" value="<c:out value="${new.pricetype}"/>"> </td>-->
                                                <input type="hidden" readonly name="priceTypes" size="5" value="<c:out value="${new.pricetype}"/>">
                                                <td class="<%=classText%>" height="30"><input type="text" name="stockQtys" size="5" readonly value="<c:out value="${new.stockQty}"/>"></td>
<!--                                                <td class="<%=classText%>" align="left"> <a  href="" onclick="submitRcIssue(<%=index%>);" >RcItem</a> </td>-->
                                            <input type="hidden" name="categoryId" value="<c:out value="${new.categoryId}"/>" >
                                            <input type="hidden" name="tyreId" value="" >
                                            <input type="hidden" name="tyres" value="" >
                                            <input type="hidden" name="tyreType" value="" >

                                            <td width="77" height="30" class="<%=classText%>"><input type="checkbox" name="selectedIndex" value='<%= index%>'></td>

                                            <%
                                                        index++;
                                            %>
                                            <c:set var="itemId" value="${new.itemId}"/>
                                                
                                            </tr>
                                            
                                            
                    </c:if>
                                    <%--</c:forEach>--%>
                    </c:if>
                    </c:forEach>
                                <%--</c:if>--%>

<c:forEach items="${ApprovedStockListAll}" var="Asl">
                                <c:if test="${Asl.categoryId == '1011'}" >
                                    <%

                                                oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr>
                                        
                                        <td class="<%=classText%>" height="30"><c:out value="${Asl.itemId}"/>  <c:out value="${Asl.mfrCode}"/></td>
                                        <td class="<%=classText%>" height="30"><c:out value="${Asl.paplCode}"/></td>
                                        <td class="<%=classText%>" height="30"><input type="hidden" name=itemIds value="<c:out value="${Asl.itemId}"/>"><c:out value="${Asl.itemName}"/></td>
                                        
                                        <td class="<%=classText%>" height="30"><c:out value="${Asl.uomName}"/></td>
                                        <td class="<%=classText%>" height="30"><input type="hidden" id="approvedQtys<%=index%>" name="approvedQtys" value="<c:out value="${Asl.approvedQuandity}"/>"><c:out value="${Asl.approvedQuandity}"/></td>
                                        <td class="<%=classText%>" height="30"><select name="tyres" class="form-control" onChange="fixTyrePrice('<%= index%>');">
                                                <option value="0" >--select-- </option>
                                                 
                                                <c:if test="${tyreList != null}" >
                                                    <c:forEach items="${tyreList}" var="tyre">
                                                        <c:if test="${Asl.itemId == tyre.itemId}" >
                                                            <option value='<c:out value="${tyre.rcItemId }"/>-<c:out value="${tyre.price }"/>-<c:out value="${tyre.tyreType }"/>' > <c:out value="${tyre.tyreType }"/>-<c:out value="${tyre.tyreNo }"/>-<c:out value="${tyre.price }"/>  </option>
                                                        </c:if>
                                                    </c:forEach >
                                                </c:if>

                                            </select>
                                        </td>
<!--                                        <td class="<%=classText%>" height="30"><input type="text" size="5" name="rcQtys" class="form-control" value=""></td>-->
                                        <input type="hidden" size="5" name="rcQtys" class="form-control" value="">
                                        <td class="<%=classText%>" ><input type="text" readonly name="taxs" size="5" value="14.50"> </td>
                                        <td class="<%=classText%>" >
                                            <input type="text" readonly name="priceIds" size="5" value="">
                                        </td>

                                        <%--<td class="<%=classText%>" height="30"><input type="text" name="priceTypes" size="5" readonly value='<c:out value="${tyre.tyreType }"/>'></td>--%>
                                        <td class="<%=classText%>" height="30"><input type="text" name="stockQtys" size="5" readonly value=""></td>                                        
                                        <!--<td class="<%=classText%>" align="left"> <a  href="" onclick="submitRcIssue(<%=index%>);" >RcItem</a> </td>-->
                                    <input type="hidden" name="categoryId" value="<c:out value="${Asl.categoryId}"/>" >
                                    <input type="hidden" name="tyreId" value="" >
                                    <input type="hidden" name="issuedQtys" value="1" >
                                    <input type="hidden" name="tyreType" value="" >


                                    <td width="77" height="30" class="<%=classText%>"><input type="checkbox" name="selectedIndex" value='<%= index%>'></td>

                                    </tr>
                                    <%
                                                index++;
                                    %>
                                </c:if>



                            </c:forEach >
                        </c:if>
                    </table>
                </c:if>

                <br>
                <div  align="center" > Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" name="remarks" ></textarea> </div>
                <br>
                <center>
                    <input type="button" class="button" name="Approve" value="Transfer" onClick="submitPage(this.name);" >

                </center>

            </c:if>
        </body>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>

