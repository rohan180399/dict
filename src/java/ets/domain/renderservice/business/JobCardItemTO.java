/*---------------------------------------------------------------------------
 * ProblemActivityTO.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.renderservice.business;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 15, 2009              Srinivasan.R			       Created
 *
 ******************************************************************************/
public class JobCardItemTO {

    private int itemId = 0;
    private String quantity = "";
    private String price = "";
    private String purchasePrice = "";
    private String lineItemAmount = "";
    private String purchaseAmount = "";
    private String itemName = "";
    private String itemCode = "";
    private String paplCode = "";
    private String tax = "";
    private String GrnNo = "";
    private String uom = "";
    private String amount = "";
    private String sparesAmount = "";
    private String recordType = "item";

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
        //this.lineItemAmount = Float.parseFloat(price) * Float.parseFloat(this.quantity);        
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
        //this.lineItemAmount = Float.parseFloat(quantity) * Float.parseFloat(this.price);
    }

    public String getLineItemAmount() {
        return lineItemAmount;
    }

    public void setLineItemAmount(String lineItemAmount) {
        this.lineItemAmount = lineItemAmount;
    }



    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getGrnNo() {
        return GrnNo;
    }
    public void setGrnNo(String GrnNo) {
        this.GrnNo = GrnNo;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSparesAmount() {
        return sparesAmount;
    }

    public void setSparesAmount(String sparesAmount) {
        this.sparesAmount = sparesAmount;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(String purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }
    

    
}
