<%--
    Document   : paymentVoucherPrintPage
    Created on : Mar 26, 2013, 11:12:59 PM
    Author     : Entitle
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">


            <div id="printDiv">

                <table width="760" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                    <c:if test="${paymentVoucherDetails != null}">
                        <c:forEach items="${paymentVoucherDetails}" var="pd">
                            <c:set var="voucherNo" value="${pd.voucherNo}"/>
                            <c:set var="vDate" value="${pd.vDate}"/>
                            <c:set var="branchLedgerName" value="${pd.branchLedgerName}"/>
                            <c:set var="driverLedgerName" value="${pd.driverLedgerName}"/>
                            <c:set var="accountDetail" value="${pd.accountDetail}"/>
                            <c:set var="accountsAmount" value="${pd.accountsAmount}"/>
                            <c:set var="narration" value="${pd.narration}"/>
                        </c:forEach>
                    </c:if>
                    <tr>
                        <td height="30" style="text-align:left; text-transform:uppercase; "colspan="2">Chettinad Logistics Private Limited, Karikkali</td>
                        <td height="30" style="text-align:right; ">CashPayment Voucher</td>
                    </tr>
                    <tr>
                        <td height="30" style="text-align:left; ">No :</td>
                        <td height="30" style="text-align:left; "><c:out value="${voucherNo}"/></td>
                        <td height="30" style="text-align:right; ">Date : <c:out value="${vDate}"/></td>
                    </tr>
                    <tr>
                        <td height="30" style="text-align:left; ">Credit A/c :</td>
                        <td height="30" style="text-align:left; " colspan="2"><c:out value="${branchLedgerName}"/></td>
                    </tr>
                    <tr>
                        <td height="30" style="text-align:left; ">Remarks :</td>
                        <td height="30" style="text-align:left; " colspan="2"><c:out value="${narration}"/></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr>
                                    <td width="40%" height="30" style="text-align:left; border-bottom:1px dashed;">Debit A/c</td>
                                    <td width="30%" height="30" style="text-align:left; border-bottom:1px dashed;">On Account of</td>
                                    <td width="30%" height="30" style="text-align:right; border-bottom:1px dashed;">Amount (SAR.)</td>
                                </tr>
                                <tr>
                                    <td height="30" style="text-align:left; text-transform:uppercase; "><c:out value="${driverLedgerName}"/></td>
                                    <td height="30"><c:out value="${accountDetail}"/></td>
                                    <td height="30" style="text-align:right;"><c:out value="${accountsAmount}"/></td>
                                </tr>
                                <tr>
                                    <td height="30">&nbsp;</td>
                                    <td height="30">Voucher Total :</td>
                                    <td height="30" style="text-align:right;"><span style="border-top:1px dashed ; padding-top:5px; width:150px; display:inline-block; "><c:out value="${accountsAmount}"/></span></td>
                                </tr>
                                <tr>
                                    <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                        <%
                                        if(pageContext.getAttribute("accountsAmount") != null && !"".equals(pageContext.getAttribute("accountsAmount"))){
                                             String test = (String) pageContext.getAttribute("accountsAmount");
                                            double temp = Double.parseDouble(test);
                                            //double temp = Double.parseDouble(pageContext.getAttribute("accountsAmount"));
                                            numberWords1.setRoundedValue(String.valueOf(java.lang.Math.ceil(temp)));
                                            numberWords1.setNumberInWords(numberWords1.getRoundedValue());
                                        }

                                       %>    

                                    <td height="30" colspan="3" style="text-align:left; border-top:1px dashed;">Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</td>
                                </jsp:useBean>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:100px; ">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr>
                                    <td width="20%" style="text-align:center;">Prepared by</td>
                                    <td width="20%" style="text-align:center;">Checked by</td>
                                    <td width="20%" style="text-align:center;">Passed by</td>
                                    <td width="20%" style="text-align:center;">Receiver</td>
                                    <td width="20%" style="text-align:center;">Cashier</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                <tr>
                <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <br>
                    <br>
                    <br>
                    <br>
                    <tr align="center">
                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                            <input align="center" type="button" onclick="print();" value = " Print "   />
                        </td>
                    </tr>
                </table>
                </tr>
                </table>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
