<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<%@ page import="java.util.*" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
        });
    });

</script>







  <!-- Content Wrapper. Contains page content -->
<!--  <div class="content-wrapper">-->
    <!-- Content Header (Page header) -->
    

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="service.label.CreateJobCard"  text="Create JobCard"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="service.label.Service" text="default text"/></a></li>
          <li class="active"><spring:message code="service.label.CreateJobCard" text="default text"/></li>
        </ol>
      </div>
      </div>
    




    <SCRIPT>






        var httpRequest1;
        function getProblems(secId, sno) {
            if (secId != 'null') {
                var list1 = eval("document.workOrder.probId" + sno);
                while (list1.childNodes[0]) {
                    list1.removeChild(list1.childNodes[0])
                }

                var url = '/throttle/getProblems.do?secId=' + secId;

                if (window.ActiveXObject)
                {
                    httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest1 = new XMLHttpRequest();
                }
                httpRequest1.open("POST", url, true);
                httpRequest1.onreadystatechange = function() {
                    go(sno);
                };
                httpRequest1.send(null);
            }
        }

        function go(sno) {
            if (httpRequest1.readyState == 4) {
                if (httpRequest1.status == 200) {
                    var response = httpRequest1.responseText;
                    var list = eval("document.workOrder.probId" + sno);
                    var details = response.split(',');
                    var probId = 0;
                    var probName = '--<spring:message code="service.label.Select"  text="default text"/>--';
                    var x = document.createElement('option');
                    var name = document.createTextNode(probName);
                    x.appendChild(name);
                    x.setAttribute('value', probId)
                    list.appendChild(x);
                    for (i = 1; i < details.length; i++) {
                        temp = details[i].split('-');
                        probId = temp[0];
                        probName = temp[1];
                        x = document.createElement('option');
                        name = document.createTextNode(probName);
                        x.appendChild(name);
                        x.setAttribute('value', probId)
                        list.appendChild(x);
                    }
                }
            }


        }


        var rowCount = 2;
        var sno = 0;
        var style = "text1";
        function addRow()
        {

            if (rowCount % 2 == 0) {
                style = "text2";
            } else {
                style = "text1";
            }
            sno++;
            var tab = document.getElementById("addRows");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var temp = sno - 1;
            var cell0 = "<td class='text1' height='25' ><input type='hidden' name='selectedindex' value='" + temp + "'>" + sno + " </td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell1 = "<td class='text1' height='25'><input name='hiddenElemenet1' value='secId" + sno + "' type='hidden'><select style='width:260px;height:40px;'  class='form-control' id='secId'  name='secId" + sno + "' onchange='setSelectbox(sno-1);getProblems(this.value," + sno + ")'><option  selectd value=0>---<spring:message code="service.label.Select"  text="default text"/>---</option><c:if test = "${sections != null}" ><c:forEach items="${sections}" var="sec"><option  value='<c:out value="${sec.secId}" />'><c:out value="${sec.secName}" /></c:forEach ></c:if> </select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(2);
            var cell2 = "<td class='text1' height='25'><input name='hiddenElemenet2' value='probId" + sno + "' type='hidden'><select style='width:260px;height:40px;'  class='form-control' id='probId'onchange='setSelectbox(sno-1)'; style='width:250px;' name='probId" + sno + "'><option  selectd value=0>---<spring:message code="service.label.Select"  text="default text"/>---</option></select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(3);
            var cell3 = " <td class='text1' height='25'><input name='hiddenElemenet3' value='symptoms" + sno + "' type='hidden'><textarea  name='symptoms" + sno + "' onchange='setSelectbox(sno-1)'; onkeyup='maxlength(this.form.symptoms" + sno + ",75)'   style='width:260px;height:40px;'  class='form-control'></textarea></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(4);
            var cell4 = " <td class='text1' height='25'><input name='hiddenElemenet4' value='severity" + sno + "' type='hidden'><select name='severity" + sno + "'onchange='setSelectbox(sno-1)';  style='width:260px;height:40px;'  class='form-control'><option value='1'><spring:message code="service.label.Low"  text="default text"/></option><option value='2' ><spring:message code="service.label.Medium"  text="default text"/></option><option selected value='3'><spring:message code="service.label.High"  text="default text"/></option></select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell4;


            var elementName = "secId" + sno;
            document.getElementById(elementName).focus();
            rowCount++;
        }

        function generateWO(){
            $("#generate").hide();
            var btn = document.getElementById("generate");

            if(jobCardFor == 1){
                if(document.getElementById("vehicleId") == ''){
                    alert("please select the truck");
                    $("#generate").show();
                }
            }
            if(jobCardFor == 2){
                if(document.getElementById("trailerId") == ''){
                    alert("please select the trailer");
                    $("#generate").show();
                }
            }
            if (isSelect(document.workOrder.dateOfIssue, 'Date Of Issue '));
            else if (textValidation(document.workOrder.reqDate, 'Required date'));
            else if (numberValidation(document.workOrder.kmReading, 'kmReading'));
            else if (numberValidation(document.workOrder.hourMeter, 'hourMeter'));
            else if (isSelect(document.workOrder.serviceType, 'serviceType'));
            else if (isSelect(document.workOrder.serviceLocation, 'serviceLocation'));
            else if (textValidation(document.workOrder.driverName, 'driverName'));
            else if (textValidation(document.workOrder.remark, 'Remark'));
            else {
                if (parseInt(document.workOrder.kmReading.value) < parseInt(document.workOrder.actualKm.value)) {
                    alert("Entered Kilometer is Less Than Last Known KM");
                    document.workOrder.kmReading.focus();
                    document.workOrder.kmReading.select();
                    $("#generate").show();
                    return 'notSubmit';
                }

                var checValidate = selectedItemValidation();
                if (checValidate == 'SubmitForm') {
                    //alert("sucess");
                    //document.workOrder.action = '/throttle/createJobcardWO.do';
                    document.workOrder.action = '/throttle/conCreateJobcardWO.do';
                    btn.disabled = 'true';
                    document.workOrder.submit();
                }
            }
            $("#generate").show();
        }

        function setSelectbox(i)
        {

            var selected = document.getElementsByName("selectedindex");
            selected[i].checked = 1;
        }

        function selectedItemValidation() {

            var index = document.getElementsByName("selectedindex");

            var chec = 0;
            var mess = "SubmitForm";
            var j = 1;
            if (index.length == 0) {
                alert("Please Add Any Problem By Clicking Add Row");
            }
            for (var i = 0; (i < index.length && index.length != 0); i++) {
                var secId = eval("document.workOrder.secId" + j);

                var probId = eval("document.workOrder.probId" + j);
                var symptoms = eval("document.workOrder.symptoms" + j);
                var severity = eval("document.workOrder.severity" + j);
                if (index[i]) {
                    chec++;

                    if (isSelect(secId, 'section')) {
                        return 'notSubmit';
                    } else if (isSelect(probId, "Fault")) {
                        return 'notSubmit';
                    } else if (textValidation(symptoms, "symptoms")) {
                        return 'notSubmit';
                    } else if (isSelect(severity, "severity")) {
                        return 'notSubmit';
                    }
                }
                j++;
            }
            if (chec == 0) {
                //alert("Please Select Any One And Then Proceed");
                //km[0].focus();
                return 'notSubmit';
            }
            return 'SubmitForm';
        }

//        function getVehicleNos() {
//
//            var oform-control = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
//            //getVehicleDetails(document.getElementById("regno"));
//        }


        var httpReq;
        function callAjax() {
            var jobCardFor = document.getElementById("jobCardFor").value;
            if(jobCardFor == 1){
                if (trim(document.workOrder.regno.value) == '') {
                    alert("Please Enter Vehicle Registration Number");
                    document.workOrder.regno.value = '';
                    document.workOrder.regno.select();
                    document.workOrder.regno.focus();
                    document.workOrder.kmReading.value = '';
                    return;
                }
            }
            //alert("am here...")
            var vehicleNo = document.workOrder.regno.value;
            var trailerNo = document.workOrder.trailerNo.value;
            var url = '/throttle/checkActualKm.do?vehicleNo=' + vehicleNo +'&trailerNo='+trailerNo;
            if (vehicleNo != '' || trailerNo != '' ) {
                if (window.ActiveXObject) {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest) {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function() {
                    processAjax();
                };
                httpReq.send(null);
            }

        }
        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200){
                    temp = httpReq.responseText.valueOf();
                    var jobCardFor = document.getElementById("jobCardFor").value;
                    var kminfo = temp.split('~');
                    document.workOrder.actualKm.value = kminfo[0];
                    if(jobCardFor == 2){
                        document.workOrder.kmReading.value = kminfo[0];
                        document.workOrder.totalKm.value = kminfo[0];
                    }
                    if (parseInt(document.workOrder.actualKm.value) > parseInt(document.workOrder.kmReading.value)) {
                        alert("Please Enter Valid Km");
                        document.workOrder.kmReading.focus();
                    }else {

                        if(jobCardFor == 1){
                            //document.workOrder.totalKm.value = parseFloat(document.workOrder.kmReading.value)-parseFloat(document.workOrder.actualKm.value);
                            document.workOrder.totalKm.value = parseFloat(document.workOrder.kmReading.value);
                        }
                    }
                }else{
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }


        var httpRequest;
        function checkDriverMapped(vehicleId) {
            if (vehicleId != '') {
                var url = '/throttle/checkVehicleDriverMapping.do?vehicleId=' + vehicleId;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);
            }
        }
        function processRequest() {
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var val = httpRequest.responseText.valueOf();
                    //alert(val.trim());
                    if (val.trim() !== "") {
                        var temp = val.split("~");
                        //document.getElementById("mappingId").value = temp[0];
                        document.getElementById("driverId").value = temp[1];
                        document.getElementById("driverName").value = temp[2];
//                        document.getElementById("secondaryDriverIdOne").value = temp[3];
//                        document.getElementById("secondaryDriverOne").value = temp[4];
//                        document.getElementById("secondaryDriverIdTwo").value = temp[5];
//                        document.getElementById("secondaryDriverTwo").value = temp[6];
                    } else {
//                        document.getElementById("mappingId").value = 0;
                        document.getElementById("driverId").value = '';
                        document.getElementById("driverName").value = '';
//                        document.getElementById("secondaryDriverIdOne").value = '';
//                        document.getElementById("secondaryDriverOne").value = '';
//                        document.getElementById("secondaryDriverIdTwo").value = '';
//                        document.getElementById("secondaryDriverTwo").value = '';
                    }
                } else {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }

        //alert("am here....1");
                $(document).ready(function() {
                     //alert("am here...");

            // Use the .autocomplete() method to compile the list based on input from user
            $('#driverName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getTechinicanDriverName.do",
                        dataType: "json",
                        data: {
                            driverName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            var primaryDriver = $('#driverName').val();
                            if (items == '' && primaryDriver != '') {
                                alert("Driver Name Not Valid");
                                $('#driverName').val('');
                                $('#driverId').val('');
                                $('#driverName').focus();
                            } else {
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    $('#driverName').val(value);
                    $('#driverId').val(id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


        });

        
        //alert("am here 2");

        function setValues(val) {
            var temp = val.split("~");
            document.getElementById("vehicleId").value = temp[0];
            document.getElementById("regno").value = temp[1];
            document.getElementById("kmReading").value = temp[2];
            document.getElementById("driverName").value = temp[3];
            document.getElementById("driverId").value = temp[4];
            callAjax();
        }
        function setTrailerValues(val) {
            var temp = val.split("~");
            document.getElementById("trailerId").value = temp[0];
            document.getElementById("trailerNo").value = temp[1];
            document.getElementById("kmReading").value = temp[2];
            //document.getElementById("driverName").value = temp[3];
            //document.getElementById("driverId").value = temp[4];
            callAjax();
        }

        function validateJobCardFor(){
            //alert("am here...");
            var jobCardFor = document.getElementById("jobCardFor").value;
            if(jobCardFor == 1){
                document.getElementById("vehicleBased").style.display = 'block';
                document.getElementById("vehicleBased1").style.display = 'block';
                document.getElementById("trailerBased").style.display = 'none';
                document.getElementById("trailerBased1").style.display = 'none';
            }else if(jobCardFor == 2){
                document.getElementById("vehicleBased").style.display = 'none';
                document.getElementById("vehicleBased1").style.display = 'none';
                document.getElementById("trailerBased").style.display = 'block';
                document.getElementById("trailerBased1").style.display = 'block';
            }else {
                document.getElementById("vehicleBased").style.display = 'none';
                document.getElementById("vehicleBased1").style.display = 'none';
                document.getElementById("trailerBased").style.display = 'none';
                document.getElementById("trailerBased1").style.display = 'none';
            }
        }
        function deleteRow(sno) {
//                    alert("sno"+sno);
                    if (sno != 1) {
//                        sno=sno-1;
                        document.getElementById("addRows").deleteRow(sno);
//                        sno1--;
                    } else {
                        alert("Can't Delete The Row");
                    }

                }
    </SCRIPT>



<div class="contentpanel">
<div class="panel panel-default">
  <div class="panel-body">

    <body onload="addRow();
        document.workOrder.regno.focus();" >



        

        <form name="workOrder" method="post" class="form-horizontal form-bordered">

                  <table  class="table table-info mb30 table-hover">
                      <thead><tr><th colspan="4"><spring:message code="service.label.CreateJobCard" text="default text"/></th></tr></thead>
               <td  colspan="4"  style="border-color:#5BC0DE;padding:16px;">

                <tr>
                    <td ><spring:message code="service.label.JobCardFor"  text="default text"/></td>
                    <td ><select name="jobCardFor" id="jobCardFor" style="width:260px;height:40px;"  class="form-control" onchange="validateJobCardFor();" >
                            <option value="0" selected >-<spring:message code="service.label.Select"  text="default text"/>-</option>
                            <option value="1"><spring:message code="service.label.Truck"  text="default text"/></option>
                            <option value="2"><spring:message code="service.label.Trailer"  text="default text"/></option>
                        </select>
                    </td>
                    <td  >                        
                        <font color="red">*</font><spring:message code="service.label.ServiceVendor"  text="default text"/>
                    </td>
                    <td  ><select style="width:260px;height:40px;"  class="form-control"  name="serviceVendor">
                            <option selected   value=0><spring:message code="service.label.NA"  text="default text"/></option>
                            <c:if test = "${serviceVendors != null}" >
                                <c:forEach items="${serviceVendors}" var="sv">
                                    <option  value='<c:out value="${sv.vendorId}"/>'>
                                        <c:out value="${sv.vendorName}" />
                                    </c:forEach >
                                </c:if>
                        </select> 
                    </td>
                    
                </tr>

                    <tr>
                    <td  >
                        <div id="vehicleBased" style="display:none;">
                        <font color="red">*</font><spring:message code="service.label.VehicleNo"  text="default text"/>
                        </div>
                        <div id="trailerBased" style="display:none;">
                            <font color="red">*</font><spring:message code="operations.label.TrailerNo"  text="TrailerNo"/>
                        </div>
                    </td>
                    
                        <%
                        String regNo = "";
                        if(request.getAttribute("regNo") != null){
                            regNo = (String)request.getAttribute("regNo");
                        }

                        %>
                    <td >

                        <div id="vehicleBased1" style="display:none;">
                            <input name="regno" id="regno"  maxlength="11" type="hidden"  class="form-control" size="20" value='<%=regNo%>' >
                            <c:if test="${vehicleRegNoForJobCard != null}">
                                <select name="vehicleDetails" id="vehicleDetails" style="width:260px;height:40px;"  class="form-control" onchange='setValues(this.value)'>
                                    <option value='0~~0~~~'>--<spring:message code="service.label.Select"  text="default text"/>--</option>
                                    <c:forEach items="${vehicleRegNoForJobCard}" var="vehicle">
                                        <option value="<c:out value="${vehicle.vehicleId}"/>~<c:out value="${vehicle.vehicleNo}"/>~<c:out value="${vehicle.tripendkm}"/>~<c:out value="${vehicle.empName}"/>~<c:out value="${vehicle.empId}"/>"><c:out value="${vehicle.vehicleNo}"/></option>
                                    </c:forEach>
                                </select>
                            </c:if>
                        </div>
                        <div id="trailerBased1" style="display:none;">
                            <input name="trailerNo" id="trailerNo"  maxlength="11" type="hidden"  class="form-control" size="20" value='' >
                            <c:if test="${trailerNos != null}">
                                <select name="trailerDetails" id="trailerDetails" style="width:260px;height:40px;"  class="form-control" onchange='setTrailerValues(this.value)'>
                                    <option value='0~~0~~~' selected >--<spring:message code="service.label.Select"  text="default text"/>--</option>
                                    <c:forEach items="${trailerNos}" var="trailer">
                                        <option value="<c:out value="${trailer.trailerId}"/>~<c:out value="${trailer.trailerNo}"/>~<c:out value="${trailer.tripendkm}"/>~0~0"><c:out value="${trailer.trailerNo}"/></option>
                                    </c:forEach>
                                </select>
                            </c:if>
                        </div>
                    </td>
                    
                <input name="trailerId" id="trailerId"  type="hidden"  class="form-control" size="20" value="0">
                <input name="vehicleId" id="vehicleId"  type="hidden"  class="form-control" size="20" value="0">
                <input name="mfrName"  type="hidden"  class="form-control" size="20" value="">
                <input name="eCost"  type="hidden"  class="form-control" size="20" value="0">
                <input name="modName" type="hidden"  class="form-control" size="20" value="">
                <input name="useType"  type="hidden"  class="form-control" size="20" value="">
                <input name="jobCardType"  type="hidden"  class="form-control" size="20" value="1">
                <input name="workOrderId"  type="hidden"  class="form-control" size="20" value="0">
                <td ><font color="red">*</font> <spring:message code="service.label.DateOfIssue"  text="default text"/> </td>
                <td  ><input id="dateOfIssue" name="dateOfIssue" type="text" style="width:260px;height:40px;"    class="datepicker form-control"  value="<%=session.getAttribute("currentDate")%>">

                </td>
                <script type="text/javascript">
                  $("#dateOfIssue").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
                });
                </script>
                </tr>
                <tr>
                    <td  ><font color="red">*</font><spring:message code="service.label.PlaceofIssue"  text="default text"/></td>
                <input name="compId" type="hidden"  value='<%=session.getAttribute("companyId")%>'>
               <!--<input name="compId" type="hidden"  value='1022'>-->
                <input name="serviceLocation" type="hidden"  value='<%=session.getAttribute("companyId")%>'>
                <!--<input name="serviceLocation" type="hidden"  value='1022'>-->
                <td  ><%=session.getAttribute("companyName")%></td>

                <td >&nbsp;&nbsp;<spring:message code="service.label.Priority"  text="default text"/></td>
                <td  ><select style="width:260px;height:40px;"  class="form-control"  name="priority">
                        <option selected value="1"> <spring:message code="service.label.Normal"  text="default text"/></option>
                        <option value="2"> <spring:message code="service.label.Urgent"  text="default text"/></option>
                        <option  value="3"><spring:message code="service.label.Critical"  text="default text"/> </option>
                    </select></td>
                </tr>
                <tr>
                    <td ><font color="red">*</font><spring:message code="service.label.RequiredDateOfDelivery"  text="default text"/></td>
                    
                    <td  ><input name="reqDate" id="reqDate" type="text" style="width:260px;height:40px;"  class="datepicker form-control"  value=""></td>
                     <script type="text/javascript">
                  $("#reqDate").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
                });
                </script>
                    <td  ><font color="red">*</font><spring:message code="service.label.RequiredTimeOfDelivery"  text="default text"/></td>
                    <td  >
                        <select style="width:70px;height:25px;"   name="hour">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        &nbsp;
                        <select style="width:70px;height:25px;" name="minute">
                            <option value="00">00</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                        </select>
                        &nbsp;
                        <select style="width:70px;height:25px;"  name="sec">
                            <option value="0"><spring:message code="service.label.AM"  text="default text"/></option>
                            <option value="1"><spring:message code="service.label.PM"  text="default text"/></option></select>
                    </td>
                </tr>

                <tr>
                    <td  ><font color="red">*</font><spring:message code="service.label.KmReading(WhenWOIsCreated)"  text="default text"/> </td>
                    <td  ><input name="kmReading" id='kmReading'  maxlength="20" type="text" onchange='callAjax();'  style="width:260px;height:40px;"  class="form-control" value=""></td>
                    <td ><spring:message code="service.label.TotalKmRunSoFar"  text="default text"/> </td>
                    <td  ><input name="totalKm"  maxlength="20" type="text" readonly style="width:260px;height:40px;"  class="form-control" value=""></td>
                </tr>
                <tr>
                    <td  ><spring:message code="service.label.KmReading(LastKnown)"  text="default text"/>  </td>
                    <td  ><input name="actualKm"  maxlength="20" type="text" readonly style="width:260px;height:40px;"  class="form-control" value=""></td>

                <input name="hourMeter"  type="hidden"   maxlength="20" class="form-control" value="0"></td>

                <td  ><font color="red">*</font> <spring:message code="service.label.ServiceType"  text="default text"/></td>
                <td  >
                    <select style="width:260px;height:40px;"  class="form-control" name="serviceType">
                        <option selected   value=0>---<spring:message code="service.label.Select"  text="default text"/>---</option>
                        <c:if test = "${serviceTypes != null}" >
                            <c:forEach items="${serviceTypes}" var="mfr">
                                <option  value='<c:out value="${mfr.servicetypeId}" />'>
                                    <c:out value="${mfr.servicetypeName}" />
                                </c:forEach >
                            </c:if>        
                    </select>  </td>
                </tr>
                <tr>
                    <td  ><font color="red">*</font><spring:message code="service.label.ServiceLocation"  text="default text"/></td>
                    <td  >
                        <textarea name="serviceLocn" style="width:260px;height:40px;"  class="form-control"   ></textarea>

                    </td>
                    <td  ><font color="red">*</font><spring:message code="service.label.Driver"  text="default text"/> </td>
                    <td  > <input  name="driverName" id="driverName" type="text"   maxlength="100" style="width:260px;height:40px;"  class="form-control" value="">
                    </td>
                <input name="driverId" id="driverId" type="hidden" value="" >
                </tr>
                <tr >
                <input name="cust" type="hidden" value="Existing Customer" >

                <td  >&nbsp;&nbsp<spring:message code="service.label.Remarks"  text="default text"/> </td>
                <td  >
                    <textarea name="remark"  style="width:260px;height:40px;"  class="form-control"     onkeyup="maxlength(this.form.remark, 300)" ></textarea>
                </td>
               
                <%--
                <td  ><font color="red">*</font><spring:message code="service.label.JobCardType"  text="default text"/></td>
                <td  ><select class="form-control" id="jobCardTypeNew" name="jobCardTypeNew">
                        <option value="0" selected>--<spring:message code="service.label.Select"  text="default text"/>---</option>
                        <option value="repair"><spring:message code="service.label.Repair"  text="default text"/></option>
                        <option value="pm"><spring:message code="service.label.PM"  text="default text"/></option>
                    </select>
                </td>
                --%>
                <td colspan="2">&nbsp;</td>
                <input type="hidden" name="jobCardTypeNew" value="repair">
                <input type="hidden" name="bayNo" value="1">
                <!--<td  >Bay</td>-->
                <!--<td  >
                   <select class="form-control" style="width:125px;" name="bayNo">
                <option selected   value=0>---Select---</option>
                <option    value=1>1</option>
                <option    value=2>2</option>
                <option    value=3>3</option>
                <option    value=4>4</option>
                <option    value=5>5</option>
                <option    value=6>6</option>
                <option    value=7>7</option>
                <option    value=8>8</option>
                <option    value=9>9</option>
                <option    value=10>10</option>
                <option    value=11>11</option>
                <option    value=12>12</option>
                <option    value=13>13</option>
                <option    value=14>14</option>
                <option    value=15>15</option>
                <option    value=16>16</option>
                <option    value=17>17</option>
                <option    value=18>18</option>
                <option    value=19>19</option>
                <option    value=20>20</option>
                <option    value=21>21</option>
                <option    value=22>22</option>
                <option    value=23>23</option>
                <option    value=24>24</option>
                <option    value=25>25</option>
                <option    value=26>26</option>
                <option    value=27>27</option>
                <option    value=28>28</option>
                      
                </select>
                </td>-->
                </tr>
                      
                  </table>

                 
                    
                     <table class="table table-info mb30 table-hover" id="addRows" >
                <thead>
                <tr>
                    <th><spring:message code="service.label.S.No"  text="default text"/></th>
                    <th><spring:message code="service.label.Section"  text="default text"/></th>
                    <th><spring:message code="service.label.Fault"  text="default text"/></th>
                    <th><spring:message code="service.label.Symptoms"  text="default text"/></th>
                    <th><spring:message code="service.label.Severity"  text="default text"/></th>

                </tr>
                </thead>
                <tr>
                    <td colspan="6" align="center" ><h4><spring:message code="service.label.Complaints"  text="default text"/></h4></td>
                </tr>
            </table>
                  </td>
            <center>
                <input type="button" value="<spring:message code="service.label.AddRow"  text="default text"/>" class="btn btn-success" onclick="addRow();">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" value="<spring:message code="service.label.Generate"  text="default text"/>" name="generate" id="generate" class="btn btn-success" onclick="generateWO();">
                <input type="hidden" value="viewWorkOrder" name="reqfor">
            </center>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </body>
        </div>
        
        
        </div>
        </div>
        
  

<%@ include file="/content/common/NewDesign/settings.jsp" %>
