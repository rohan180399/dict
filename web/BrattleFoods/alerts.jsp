<%-- 
    Document   : alerts
    Created on : Oct 18, 2013, 11:59:52 AM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });

            });
        </script>
        <script type="text/javascript">
            function showDays(val) {
                var element = document.getElementById('km');
                if (val == 'km') {
                    element.style.display = 'block';

                } else {
                    element.style.display = 'none';
                }

                var element = document.getElementById('day');
                if (val == 'day') {
                    element.style.display = 'block';
                }
                else {
                    element.style.display = 'none';
                }
                var element = document.getElementById('km1');
                if (val == 'km') {
                    element.style.display = 'block';

                } else {
                    element.style.display = 'none';
                }

                var element = document.getElementById('day1');
                if (val == 'day') {
                    element.style.display = 'block';
                }
                else {
                    element.style.display = 'none';
                }
            }
        </script>
    </head>
    <body onload="showDays('km')">
        <form name="alerts" method="post"  >
            <%
            request.setAttribute("menuPath","Operations >> Alerts Configuration");
            %>
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>

            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr height="10">
                        <td colspan="4" class="contenthead">Alert Details</td>
                    </tr>
                    <tr>
                        <td class="text2" height="30">Alert Name</td>
                        <td class="text2" height="30"><input name="alertName" id="alertName" class='form-control' type="text"  value='' ></td>
                        <td class="text2" height="30">Alert Description</td>
                        <td class="text2" height="30"><textarea name="AlertDes" AlertDes class='form-control' type="text"  value='' ></textarea></td>
                    </tr>
                    <tr>
                        <td class="text1" height="30">Alert Based On</td>
                        <td class="text1"  >
                            <select name="AlertBased" id="contactinfo" class="form-control" onchange="showDays(this.value);">
                                <option value="km">Km's</option>
                                <option value="day">Day's</option>
                            </select>
                        </td>

                        <td class="text1" >
                            <div id="km" >Alerts to be Raised before Km's: <input type="text" class="form-control" name="km"  /><br /></div>
                            <div id="day" style="display: none;">Alerts to be Raised before day's : <input type="text" class="form-control" name="day" /><br /></div>
                        </td>

                    </tr>




                    <tr>
                        <td class="text1" height="30">To email id(,Separated)</td>
                        <td class="text1" height="30"><textarea name="toEmail" maxlength='14' class='form-control' type="text"  value='' ></textarea></td>
                        <td class="text1" height="30">Cc email id(,Separated)</td>
                        <td class="text1" height="30"><textarea name="ccEmail" maxlength='14' class='form-control' type="text"  value='' ></textarea></td>
                    </tr>
                    <tr>
                        <td class="text2" >Email Subject</td>
                        <td class="text2" height="30"><input name="emailSub" maxlength='14' class='form-control' type="text"  value='' ></td>
                        <td class="text2" height="30">Alert Frequency</td>
                        <td class="text2"  >
                            <select class="form-control" name="frequently" id="frequently"  style="width:125px;">
                                <option value="0">---Select---</option>
                                <option value='1' >Once A Day</option>
                                <option value='2' >Once A Week</option>
                                <option value='3' >Once A month</option>
                                <option value='4' selected>Fort Nightly</option>
                            </select>
                        </td>
                    </tr>


                </table>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td colspan="4" class="contenthead">Escalation Rule</td>
                    </tr>
                    <tr>
                        <td class="text2" >
                            <div id="km1" style="display: none;">No Of Km's: <input type="text" class="form-control" name="km1" /><br /></div>
                            <div id="day1" style="display: none;">No Of day's: <input type="text" class="form-control" name="day1" /><br /></div>
                        </td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">To email id(,Separated)</td>
                        <td class="text2" height="30"><textarea name="toEmail1" maxlength='14' class='form-control' type="text"  value='' ></textarea></td>
                    </tr>

                    <tr>
                        <td class="text1" height="30">Cc email id(,Separated)</td>
                        <td class="text1" height="30"><textarea name="ccEmail1" maxlength='14' class='form-control' type="text"  value='' ></textarea></td>
                        <td class="text1" height="30">Email Subject</td>
                        <td class="text1" height="30"><input name="emailSub1" maxlength='14' class='form-control' type="text"  value='' ></td>
                    </tr>

                    <tr>
                        <td class="text2" height="30">Repeat Frequency</td>
                        <td class="text2"  >
                            <select class="form-control" name="repeat" id="repeat"  style="width:125px;">
                                <option value="0">---Select---</option>
                                <option value='1' >Once A Day</option>
                                <option value='2' >Once A Week</option>
                                <option value='3' >Once A month</option>
                                <option value='4' selected>Fort Nightly</option>
                            </select>
                        </td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30"> &nbsp;</td>
                    </tr>

                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="submit" class="button" value="Save" name="Submit" onClick="submitPage1()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <h2 align="center">Alerts List</h2>
            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="80">
                        <th><h3>S.No</h3></th>
                        <th><h3>Name </h3></th>
                        <th><h3>Description</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Raise Alert before Day's/Km's</h3></th>
                        <th><h3>To Email-Id</h3></th>
                        <th><h3>To CC-Id</h3></th>
                        <th><h3>Email Subject</h3></th>
                        <th><h3>Alert Frequency</h3></th>
                        <th><h3>Escalate Day's/Km's</h3></th>
                        <th><h3>Escalate To Email-Id</h3></th>
                        <th><h3>Escalate To CC-Id</h3></th>
                        <th><h3>Escalate Email Subject</h3></th>
                        <th><h3>Escalate Frequently</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">Road Tax Due</td>
                        <td align="left" class="text2">Road Tax Due</td>
                        <td align="left" class="text2">Days</td>
                        <td align="left" class="text2">60 Days</td>
                        <td align="left" class="text2">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text2">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text2">Road Tax Due Alert</td>
                        <td align="left" class="text2">Once a Week</td>
                        <td align="left" class="text2">60 Days</td>
                        <td align="left" class="text2">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text2">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text2">Road Tax Due Escalate</td>
                        <td align="left" class="text2">Once a Day</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text1">2</td>
                        <td align="left" class="text1">Insurance Due</td>
                        <td align="left" class="text1">Insurance Due</td>
                        <td align="left" class="text1">Days</td>
                        <td align="left" class="text1">60 Days</td>
                        <td align="left" class="text1">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text1">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text1">Insurance Due Alert</td>
                        <td align="left" class="text1">Once a Week</td>
                        <td align="left" class="text1">60 Days</td>
                        <td align="left" class="text1">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text1">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text1">Insurance Due Escalate</td>
                        <td align="left" class="text1">Once a Day</td>
                        <td align="left" class="text1"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">3</td>
                        <td align="left" class="text2">Service Due</td>
                        <td align="left" class="text2">Service Due</td>
                        <td align="left" class="text2">Km</td>
                        <td align="left" class="text2">Once a Day</td>
                        <td align="left" class="text2">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text2">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text2">Service Due Alert</td>
                        <td align="left" class="text2">Once a 20000Km</td>
                        <td align="left" class="text2">Once a Day</td>
                        <td align="left" class="text2">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text2">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text2">Service Due Escalate</td>
                        <td align="left" class="text2">Once a 1000km</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text1">4</td>
                        <td align="left" class="text1">Permit Due</td>
                        <td align="left" class="text1">Permit Due</td>
                        <td align="left" class="text1">Days</td>
                        <td align="left" class="text1">60 Days</td>
                        <td align="left" class="text1">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text1">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text1">Permit Due Alert</td>
                        <td align="left" class="text1">Once a Week</td>
                        <td align="left" class="text1">60 Days</td>
                        <td align="left" class="text1">nipun.kohli@brattlefoods.com,srini@entitlesolutions.com</td>
                        <td align="left" class="text1">mathan@entitlesolutions.com,arul@entitlesolutions.com</td>
                        <td align="left" class="text1">Permit Due Escalate</td>
                        <td align="left" class="text1">Once a Day</td>
                        <td align="left" class="text1"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
