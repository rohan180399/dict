

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
<!--        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

<!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">



            function computeExpense(){

                var distance = document.trip.preStartLocationDistance.value;
                var vehicleMileage = document.trip.vehicleMileage.value;
                var tollRate = document.trip.tollRate.value;
                var fuelPrice = document.trip.fuelPrice.value;

                if(distance.trim() != '' && distance.trim() != '0'){
                    var fuelLtrs = parseFloat(distance) / parseFloat(vehicleMileage);
                    var fuelCost = (parseFloat(fuelPrice) * fuelLtrs);
                    var tollCost = parseFloat(distance) * parseFloat(tollRate);
                    var totalCost = (fuelCost + tollCost).toFixed(2);
                    document.trip.preStartRouteExpense.value = totalCost;
                    document.trip.preStartLocationDurationHrs.value = 0;
                    document.trip.preStartLocationDurationMins.value = 0;
                }

             }
            var httpReq;
                var temp = "";




            function fetchRouteInfo() {
                var preStartLocationId = document.trip.preStartLocationId.value;
                var originId = document.trip.originId.value;
                var vehicleTypeId = document.trip.vehicleTypeId.value;
                if(preStartLocationId != ''){
                     var url = "/throttle/checkPreStartRoute.do?preStartLocationId="+preStartLocationId+"&originId="+originId+"&vehicleTypeId="+vehicleTypeId;
                     alert(url);
                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() {
                        processFetchRouteCheck();
                    };
                    httpReq.send(null);
                }

            }

            function processFetchRouteCheck()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        //alert(temp);
                        if(temp != '' && temp != null  && temp != 'null'){
                            var tempVal = temp.split('-');
                            document.trip.preStartLocationDistance.value = tempVal[0];
                            document.trip.preStartLocationDurationHrs.value = tempVal[1];
                            document.trip.preStartLocationDurationMins.value = tempVal[2];
                            document.trip.preStartRouteExpense.value = tempVal[3];
                            document.getElementById("preStartLocationDistance").readOnly = true;
                            document.getElementById("preStartLocationDurationHrs").readOnly = true;
                            document.getElementById("preStartLocationDurationMins").readOnly = true;
                            document.getElementById("preStartRouteExpense").readOnly = true;
                        }else{
                            alert('valid route does not exists between pre start location and trip start location');
                            document.getElementById("preStartLocationDistance").readOnly = false;
                            document.getElementById("preStartLocationDurationHrs").readOnly = false;
                            document.getElementById("preStartLocationDurationMins").readOnly = false;
                            document.getElementById("preStartRouteExpense").readOnly = false;
                        }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }
            
            

            
            //end ajax for pre location
            function saveAction() {
                var statusCheck = false;
                var temp = document.trip.vehicleId.value;
                var date = document.trip.vehicleChangeDate.value;
                //alert(temp);
                if(temp != ''){
                    statusCheck = true;
                }else{
                    alert('please allot vehicle');
                    $("#vehicleNo").focus();
                }
               
                

                if(statusCheck){
                    if($("#vehicleChangeDate").val() == ''){
                        alert("please select vehicle change date");
                        $("#vehicleChangeDate").focus();
                    }else if($("#lastVehicleEndKm").val() == ''){
                        alert("please enter last vehicle end km");
                        $("#lastVehicleEndKm").focus();
                    }else if($("#vehicleStartKm").val() == ''){
                        alert("please enter change vehicle start km");
                        $("#vehicleStartKm").focus();
                    }else if($("#cityId").val() == ''){
                        alert("please select vehicle change city");
                        $("#cityId").focus();
                    }else if($("#vehicleCapUtil").val() == ''){
                        alert("please check vehicle capacity");
                        $("#vehicleCapUtil").focus();
                    }else if($("#driver1Name").val() == '' && $("#driver1Name").val() == 'Driver Not Mapped'){
                        alert("please check driver name is not mapped with vehicle");
                        $("#driver1Name").focus();
                    }else{
                    $("#save").hide();
                    document.trip.action = "/throttle/saveTripSheetForVehicleChange.do";
                    document.trip.submit();
                        
                    }
                }
            }
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



    </script>
          <script type="text/javascript">
        function calculateDate(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        if(prevTime.getTime() < thisTime.getTime()){
            if(difference > 0){
            alert(" Selected Date is greater than scheduled date ");
            document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
        }
        }
        }
         function calculateTime(){
      var endDates = document.getElementById("tripScheduleDate").value;
        var endTimeIds = document.getElementById("tripScheduleTime").value;
        var tempDate1 = endDates.split("-");
        var tempTime3 = endTimeIds.split(" ");
        var tempTime1 = tempTime3[0].split(":");
        if(tempTime3[1] =="PM"){
          tempTime1[0] = 12 + parseInt(tempTime1[0]);
        }
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
        var minute = document.getElementById("preStartLocationPlanTimeMins").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],parseInt(tempTime1[0]),tempTime1[1]);              // now
       var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
        if(prevTime.getTime() < thisTime.getTime()){
         if(hoursDifference > 0){
            alert("Selected Time is greater than scheduled Time ");
            document.getElementById("preStartLocationPlanTimeHrs").focus();
                 }
     }
     }
        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>


        <script type="text/javascript" language="javascript">




            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }



        </script>

<script type="text/javascript">

            function computeVehicleCapUtil(){
                setVehicleValues();
                var orderWeight = document.trip.totalWeight.value;
                var vehicleCapacity = document.trip.vehicleTonnage.value;
                if(vehicleCapacity > 0) {
                    var utilPercent = (orderWeight/vehicleCapacity) * 100;
                    //document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


                }else{
                   //document.trip.vehicleCapUtil.value = 0;
                }
            }
            
            function setVehicleValues() {
                var value = document.trip.vehicleNo.value;

                if(value != 0){
                    var tmp = value.split('-');
                    $('#vehicleId').val(tmp[0]);
                    //alert("1");
                    $('#vehicleNoEmail').val(tmp[1]);
                    //alert("2");
                    $('#vehicleTonnage').val(tmp[2]);
                    //alert("3");
                    $('#driver1Id').val(tmp[3]);
                    //alert("4");
                    $('#driver1Name').val(tmp[4]);
                    //alert("5");
                    $('#driver2Id').val(tmp[5]);
                    //alert("6");
                    $('#driver2Name').val(tmp[6]);
                    //alert("7");
                    $('#driver3Id').val(tmp[7]);
                    //alert("8");
                    $('#driver3Name').val(tmp[8]);
                    $('#actionName').empty();
                     var actionOpt = document.trip.actionName;
                     var optionVar = new Option("-select-", '0');
                     actionOpt.options[0] = optionVar;
                     optionVar = new Option("Freeze", '1');
                     actionOpt.options[1] = optionVar;
                     $("#actionDiv").hide();
                }else{
                    $('#vehicleId').val('');
                    $('#vehicleNoEmail').val('');
                    $('#vehicleTonnage').val('');
                    $('#driver1Id').val('');
                    $('#driver1Name').val('');
                    $('#driver2Id').val('');
                    $('#driver2Name').val('');
                    $('#driver3Id').val('');
                    $('#driver3Name').val('');
                    $('#actionName').empty();
                    var actionOpt = document.trip.actionName;
                     var optionVar = new Option("-select-", '0');
                     actionOpt.options[0] = optionVar;
                     optionVar = new Option("Cancel Order", 'Cancel');
                     actionOpt.options[1] = optionVar;
                     optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                     actionOpt.options[2] = optionVar;
                     optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                     actionOpt.options[3] = optionVar;
                     $("#actionDiv").show();
                }
            }
            //start ajax for vehicle Nos
                $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getVehicleRegNos.do",
                            dataType: "json",
                            data: {
                                vehicleNo: request.term,
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        //alert(value);
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        $('#vehicleTonnage').val(tmp[2]);
                        $('#driver1Id').val(tmp[3]);
                        $('#driver1Name').val(tmp[4]);
                        $('#driver2Id').val(tmp[5]);
                        $('#driver2Name').val(tmp[6]);
                        $('#driver3Id').val(tmp[7]);
                        $('#driver3Name').val(tmp[8]);
                        //$itemrow.find('#vehicleId').val(tmp[0]);
                        //$itemrow.find('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });
            //end ajax for vehicle Nos

        </script>



    </head>


    <body >



        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>

            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            %>
            <%
            String vehicleMileageAndTollRate = "";
            String vehicleMileage = "0";
            String tollRate = "0";
            String fuelPrice = "0";
            String[] temp = null;
            if(request.getAttribute("vehicleMileageAndTollRate") != null){
                vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                temp = vehicleMileageAndTollRate.split("-");
                vehicleMileage = temp[0];
                tollRate = temp[1];
                fuelPrice = temp[2];
            }
            %>
            <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
            <input type="hidden" name="tollRate" value="<%=tollRate%>" />
            <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
            <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>" />
            <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>" />




<table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${trip.orderRevenue}" /></td>

                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>

                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                     <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />

                                     <input type="hidden" name="orderExpense" Id="orderExpense" class="textbox" value='<c:out value="${trip.orderExpense}" />' >
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>


<br>
<br>
<br>
<br>
            <div id="tabs" >
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
<!--                    <li><a href="#action"><span>Action</span></a></li>-->
<!--                    <li><a href="#driverDetail"><span>Driver</span></a></li>
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
<!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
<!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>
                         <input type ="hidden" id="accVehicleId" name="accVehicleId" value="<c:out value="${vehicleId}"/>" >
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                        <tr>
<!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td class="text1">CNote No(s)</td>
                            <td class="text1">
                                <c:out value="${trip.cNotes}" />
                            </td>
                            <td class="text1">Billing Type</td>
                            <td class="text1">
                                <c:out value="${trip.billingType}" />
                            </td>
                        </tr>
                        <tr>
<!--                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001</td>-->
                            <td class="text2">Customer Name</td>
                            <td class="text2">
                                <c:out value="${trip.customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${trip.customerName}" />'>
                                <input type="hidden" name="tripId" Id="tripId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                <input type="hidden" name="originId" Id="originId" class="textbox" value='<c:out value="${trip.originId}" />'>
                                <input type="hidden" name="vehicleTypeId" Id="vehicleTypeId" class="textbox" value='<c:out value="${trip.vehicleTypeId}" />'>
                                <input type="hidden" name="vehicleNo1" Id="vehicleNo1" class="textbox" value='<c:out value="${trip.vehicleNo}" />'>
                            </td>
                            <td class="text2">Customer Type</td>
                            <td class="text2" colspan="3" >
                                <c:out value="${trip.customerType}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${trip.routeInfo}" />
                            </td>
<!--                            <td class="text1">Route Code</td>
                            <td class="text1" >DL001</td>-->
                            <td class="text1">Reefer Required</td>
                            <td class="text1" >
                                <c:out value="${trip.reeferRequired}" />
                            </td>
                            <td class="text1">Order Est Weight (MT)</td>
                            <td class="text1" >
                                <c:out value="${trip.totalWeight}" />
                                <input type="hidden" name="totalWeight" Id="totalWeight" class="textbox" value='<c:out value="${trip.totalWeight}" />' readonly >
                            </td>
                        </tr>




                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTypeName}" />
                            </td>
                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                            <td class="text2">
                                <select name="vehicleNo" id="vehicleNo" onchange="computeVehicleCapUtil();" class="textbox" >
                                        <c:if test="${vehicleNos != null}">
                                            <option value="0" selected>--select--</option>
                                            <c:forEach items="${vehicleNos}" var="vehNo">
                                                <option value='<c:out value="${vehNo.vehicleId}"/>-<c:out value="${vehNo.vehicleNo}"/>-<c:out value="${vehNo.vehicleTonnage}"/>-<c:out value="${vehNo.primaryDriverId}"/>-<c:out value="${vehNo.primaryDriverName}"/>-<c:out value="${vehNo.secondaryDriver1Id}"/>-<c:out value="${vehNo.secondaryDriver1Name}"/>-<c:out value="${vehNo.secondaryDriver2Id}"/>-<c:out value="${vehNo.secondaryDriver2Name}"/>'><c:out value="${vehNo.vehicleNo}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                            </td>
                            <input type="hidden" name="vehicleId" Id="vehicleId" class="textbox" value="">
                            <td class="text2">Vehicle Capacity (MT)</td>
                            <td class="text2"><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly class="textbox" value=""></td>
                        </tr>
                        <tr >
                            <td class="text1"><font color="red">*</font>Vehicle Change Date</td>
                            <td class="text1"><input type="text" name="vehicleChangeDate" id="vehicleChangeDate"  class="datepicker" value="<%=startDate%>" >

                            <td class="text1"><font color="red">*</font>Vehicle Change Time</td>
                            <td class="text1" height="25" >
                                HH:<select name="vehicleChangeHour" id="vehicleChangeHour" class="textbox" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleChangeMinute" id="vehicleChangeMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                        </tr>
                        <tr>
                            <td class="text1">Last Vehicle Start Odometer Reading</td>
                            <td class="text1">
                                <input type="hidden" name="lastVehicleStartKm" Id="lastVehicleStartKm" class="textbox" value='<c:out value="${trip.startKm}"/>'  style="width: 80px" >
                                <c:out value="${trip.startKm}"/>
                            </td>
                            <td class="text1">Last Vehicle End Odometer Reading</td>
                            <td class="text1">
                                <input type="text" name="lastVehicleEndKm" Id="lastVehicleEndKm" class="textbox" value='0'  style="width: 80px" onKeyPress='return onKeyPressBlockCharacters(event);' onkeyup="calculateKm()">
                            </td>
                            <td class="text1">Last Vehicle Total Odometer Reading</td>
                            <td class="text1">
                                <label id="totalKm">0</label>
                            </td>
                            <script>
                                function calculateKm(){
                                  var lastVehicleStartKm = $("#lastVehicleStartKm").val();
                                  var lastVehicleEndKm = $("#lastVehicleEndKm").val();
                                  var res = parseFloat(lastVehicleEndKm)-parseFloat(lastVehicleStartKm);
                                  $("#totalKm").text(res);
                                }
                            </script>
                        </tr>
                        <tr>
                            <td class="text1">Last Vehicle Start Reefer HM</td>
                            <td class="text1">
                                <input type="hidden" name="lastVehicleStartOdometer" Id="lastVehicleStartOdometer" class="textbox" value='<c:out value="${trip.startHm}"/>' style="width: 80px" >
                                <c:out value="${trip.startHm}"/>
                            </td>
                            <td class="text1">Last Vehicle End Reefer HM</td>
                            <td class="text1">
                                <input type="text" name="lastVehicleEndOdometer" Id="lastVehicleEndOdometer" class="textbox" value='0' style="width: 80px" onKeyPress='return onKeyPressBlockCharacters(event);' onkeyup="calculateHm()">
                            </td>
                            <script>
                                function calculateHm(){
                                  var lastVehicleStartOdometer = $("#lastVehicleStartOdometer").val();
                                  var lastVehicleEndOdometer = $("#lastVehicleEndOdometer").val();
                                  var res = parseFloat(lastVehicleEndOdometer)-parseFloat(lastVehicleStartOdometer);
                                  $("#totalHm").text(res);
                                }
                            </script>
                            <td class="text1">Last Vehicle Total Reefer HM</td>
                            <td class="text1">
                                <label id="totalHm">0</label>
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="text1">New Vehicle Start Odometer Reading</td>
                            <td class="text1" >
                                <input type="text" name="vehicleStartKm" Id="vehicleStartKm" class="textbox" value='0' style="width: 80px" >
                            </td>
                            <td class="text1">New Vehicle Start Reefer Hm</td>
                            <td class="text1" >
                                <input type="text" name="vehicleStartOdometer" Id="vehicleStartOdometer" class="textbox" value='0' style="width: 80px" >
                            </td>
                            <td class="text1">Vehicle Change City</td>
                             <td  width="80"> <select name="cityId" id="cityId" class="textbox" style="height:20px; width:122px;"" >
                               <c:if test="${cityList != null}">
                                <option value="" selected>--Select--</option>
                                <c:forEach items="${cityList}" var="cityList">
                                    <option value='<c:out value="${cityList.cityId}"/>'><c:out value="${cityList.cityName}"/></option>
                                </c:forEach>
                               </c:if>
                               </select>
                           </td>
                        </tr>

                        <tr>
                            <td class="text1">Veh. Cap [Util%]</td>
                            
                                <td class="text1"><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="textbox" value="0"></td>
                            
                            <td class="text1">Special Instruction</td>
                            <td class="text1">-</td>
                            <td class="text1">Trip Schedule</td>
                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                             <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                            <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                        </tr>


                        <tr>
                            <td class="text2"><font color="red">*</font>Primary Driver </td>
                            <td class="text2" >
                                <input type="text" name="driver1Name"  readonly  Id="driver1Name" class="textbox" value=""  >
                                <input type="hidden" name="driver1Id" Id="driver1Id" class="textbox" value=""  >
                            </td>
                            <td class="text2"><font color="red">*</font>Secondary Driver(s) </td>
                            <td class="text2" colspan="5" >
                                <input type="text" name="driver2Name"  readonly Id="driver2Name" class="textbox" value=""  >
                                <input type="hidden" name="driver2Id" Id="driver2Id" class="textbox" value=""  >
                                <input type="text" name="driver3Name"  readonly  Id="driver3Name" class="textbox" value=""  >
                                <input type="hidden" name="driver3Id" Id="driver3Id" class="textbox" value=""  >
                            </td>
                        </tr>
                       <tr>
                            <td class="text1" > Reason for Change </td>
                            <td class ="text1">
                                <select id="reasonForChange" name="reasonForChange" class="textbox">
                                    <<option value ="0">--Select--</option>
                                    <option value ="1">Repair</option>
                                    <option value ="2"> Accident</option>
                                </select>
                            </td>
                             <td class="text1" >Remarks</td>
                             <td class="text1" >
                                <textarea rows="3" cols="30" class="textbox" name="vehicleRemarks" id="vehicleRemarks"   style="width:142px"></textarea>
                             </td>
                        </tr>
                            </c:forEach>
                       </c:if>
                    </table>
                    <br/>
                    <br/>

                    

                    <br/>
                    <center>
                        <input type="button" onclick="saveAction();" class="button" value="save" name="Next" id="save"/>
                    </center>
                </div>
                <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                    <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr >
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                      </tr>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">

                        <tr >
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td class="text1" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                      </tr>




                        </c:forEach >
                    </table>
                    </c:if>

<!--
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>


                    <br>
                    <br>
                </div>

                <div id="action">-->
                    <br>
                    <br>
                    
                    <br>
                    
                </div>




                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>