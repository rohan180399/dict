<%-- 
    Document   : ledgerReport.jsp
    Created on : Dec 11, 2012, 5:39:26 PM
    Author     : entitle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">

        function submitPage(val)
        {
            if(val == 'fetchData'){
                var ledgerId = document.ledgerReport.ledgerId.value;
                if(ledgerId != '0')
                {
                    document.ledgerReport.action = '/throttle/ledgerReport.do';
                    document.ledgerReport.submit();
                }else{
                    alert('please select ledger');
                    document.lederReport.ledgerReport.focus();
                }
            }else if(val == 'export'){
                var ledgerId = document.ledgerReport.ledgerId.value;
                if(ledgerId != '0')
                {
                    document.ledgerReport.action = '/throttle/exportLedgerReport.do';
                    document.ledgerReport.submit();
                }else{
                    alert('please select ledger');
                    document.lederReport.ledgerReport.focus();
                }
            }
            
        }
        function setValues(){
            var ledgerId = <%=request.getAttribute("ledgerId")%>;
            var fromDate = '<%=request.getAttribute("fromDate")%>';
            var toDate = '<%=request.getAttribute("toDate")%>';
            if(toDate != 'null'){
                document.ledgerReport.toDate.value = toDate;
            }
            if(fromDate != 'null'){
                document.ledgerReport.fromDate.value = fromDate;
            }
            if(ledgerId != null){

                document.ledgerReport.ledgerId.value = ledgerId;
            }
        }


    </script>
    <body onload="setValues();">
        <%//System.out.println(request.getAttribute("fromDate"));
        %>
        <form name="ledgerReport" method="post" >
<!--            <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
             pointer table 
             message table 
            <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>-->

            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
                <tr>
                    <td class="contenthead" height="30"><div class="contenthead" align="center">Voucher</div></td>
                    <td class="contenthead" height="30">
                        <div class="contenthead" align="center">
                            <select class="textbox" name="ledgerId">
                                <option selected   value='0'>-select-</option>
                                <c:if test = "${journalLedgerList != null}" >
                                    <c:forEach items="${journalLedgerList}" var="jlist">
                                        <option  value='<c:out value="${jlist.ledgerID}"/>'><c:out value="${jlist.ledgerName}" /></option>
                                    </c:forEach >
                                </c:if>
                            </select>

                        </div>
                    </td>
                    <td class="contenthead" height="30">From Date </td>
                    <td class="contenthead" height="30">
                        <input type="textbox" id="fromDate" name="fromDate" value=""  size="20" class="datepicker">
                    </td>
                    <td class="contenthead" height="30">To Date </td>
                    <td class="contenthead" height="30">
                        <input type="textbox" id="toDate" name="toDate" value=""  size="20" class="datepicker">
                    </td>
                    </tr>
                <tr >
                    <td colspan="7" align="center" height="30">
                        <input type="button" class="button" onclick="submitPage(this.name);" name="fetchData" value="FetchData">
                        <input type="button" class="button" onclick="submitPage(this.name);" name="export" value="export">
                    </td>
                </tr>

            </table>
            <p>&nbsp;</p>
            <div align="center">

                <c:if test = "${ledgerTransactionList != null}" >
                    <table width="700" class="border" border="1">
                        <tr class="contentsub">
                            <td class="contentsub" width="10%" ><div class="contentsub">Sno</div></td>
                            <td class="contentsub" width="10%" ><div class="contentsub">Date</div></td>
                            <td class="contentsub" width="70%"><div class="contentsub">Particulars</div></td>                            
                            <td class="contentsub" width="10%"><div class="contentsub">Debit</div></td>
                            <td class="contentsub" width="10%"><div class="contentsub">Credit</div></td>
                        </tr>
                        <c:set var="creditTotal" value="${0}" />
                        <c:set var="debitTotal" value="${0}" />
                        <%
                        String openingBalance = (String)request.getAttribute("openingBalance");
                        String amountType = (String)request.getAttribute("amountType");
                        String debitOpen = "";
                        String creditOpen = "";
                        if("DEBIT".equals(amountType)){
                            debitOpen = openingBalance;
                            %>
                            <c:set var="debitTotal" value="${openingBalance}" />
                            <tr>
                            <td colspan="3" align="right" class="text2"  height="30"><b>OpeningBalance</b></td>
                            <td  class="text2" align="right"  height="30"><b><c:out value="${openingBalance}" /></b></td>
                            <td  class="text2" align="right"  height="30">&nbsp;</td>
                            </tr>
                            <%
                        }else{
                            creditOpen = openingBalance;
                            %>
                            <c:set var="creditTotal" value="${openingBalance}" />
                            <tr>
                            <td colspan="3" align="right" class="text2"  height="30"><b>OpeningBalance</b></td>
                            <td  class="text2" align="right"  height="30">&nbsp;</td>
                            <td  class="text2" align="right"  height="30"><b><c:out value="${openingBalance}" /></b></td>
                            </tr>
                            <%
                        }
                        int cntr = 0;
                        %>

                        <c:forEach items="${ledgerTransactionList}" var="txnlist">
                            <tr>
                                <% cntr++; %>
                                <td class="text1" ><%=cntr%></td>
                                <td class="text1" ><c:out value="${txnlist.accountEntryDate}" /></td>
                                <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                    <c:set var="creditTotal" value="${creditTotal + txnlist.accountAmount}"/>
                                    <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Cr. <br>
                                        &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                    <td class="text1" align ="right">&nbsp;</td>
                                    <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>                                    
                                </c:if>
                                <c:if test = "${txnlist.accountType == 'CREDIT'}" >
                                    <c:set var="debitTotal" value="${debitTotal + txnlist.accountAmount}"/>
                                    <td class="text1" >&nbsp;&nbsp;&nbsp;To &nbsp;<c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr. <br>
                                        &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>                                    
                                    <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                    <td class="text1" align ="right">&nbsp;</td>
                                </c:if>
                            </tr>
                        </c:forEach>
                            <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Total</b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${debitTotal}" /></b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${creditTotal}" /></b></td>
                              </tr>
                            <c:if test = "${creditTotal > debitTotal}" >
                                <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Closing Balance</b></td>
                                    <td  class="text2" align="right"  height="30">&nbsp;</td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${creditTotal-debitTotal}" /> &nbsp;Cr.</b></td>
                                </tr>
                            </c:if>
                            <c:if test = "${creditTotal < debitTotal}" >
                                <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Closing Balance</b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                                    <td  class="text2" align="right"  height="30">&nbsp;</td>
                                </tr>
                            </c:if>
                            <c:if test = "${creditTotal < debitTotal}" >
                                <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Closing Balance</b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                                    <td  class="text2" align="right"  height="30">&nbsp;</td>
                                </tr>
                            </c:if>
                            <c:if test = "${creditTotal == debitTotal}" >
                                <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Closing Balance</b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                                    <td  class="text2" align="right"  height="30">&nbsp;</td>
                                </tr>
                            </c:if>


                        <!--modified on 20th April 2013
                        <c:forEach items="${ledgerTransactionList}" var="txnlist">
                            <tr>
                                <td class="text1" ><c:out value="${txnlist.accountEntryDate}" /></td>
                            <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr.</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                <td class="text1" align ="right">&nbsp;</td>
                            </c:if>
                            <c:if test = "${txnlist.accountType == 'CREDIT'}" >
                                <td class="text1" >&nbsp;&nbsp;&nbsp;To &nbsp;<c:out value="${txnlist.ledgerName}" />&nbsp; A/C <br>
                                                    &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                <td class="text1" align ="right">&nbsp;</td>
                                <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                            </c:if>
                        </tr>
                        </c:forEach>
                        -->
                    </table>
                </c:if>
            </div>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>