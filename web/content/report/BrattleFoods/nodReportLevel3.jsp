<%-- 
    Document   : branchCommisionReport
    Created on : Sep 3, 2015, 4:15:18 PM
    Author     : SriniEntitle
--%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--<html>-->
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<head>
    <!--<title>NOD REPORT</title>-->
    <link rel="stylesheet" href="/throttle/css/displaytag.css" type="text/css" media="screen">
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script src="/throttle/js/popup.js" type="text/javascript"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/general.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">

    <!--        <script type="text/javascript">
            function openLevel3Report(branchId, branchName) {
                var statusName = document.getElementById("statusName").value;
                var statusId = document.getElementById("statusId").value;
                var day = document.getElementById("day").value;
                window.open('/throttle/nodReportLevel3.do?branchName='+branchName+'&branchId='+branchId+'&statusName='+statusName+'&statusId='+statusId+'&day=' + day, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
            </script>-->

</head>
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.NOD Report" text="NOD Report"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Report"/></a></li>
                <li class=""><spring:message code="hrms.label.NOD Report" text="NOD Report"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <%--<%@ include file="/content/common/path.jsp" %>--%>
                <center>
                    <%@include file="/content/common/message.jsp"%>
                </center>
                <form name="nodReport"  method="post">

                    <c:if test="${nodLevel3List != null}">
                        <table class="table table-info mb30 table-hover" id="bg"   >
                            <thead>
                                <tr>
                                    <th colspan="4" height="30" >NOD Report Details</th>
                                </tr>
                            </thead>
                        </table>                
                        <table class="table table-info mb30 table-hover" id="table"   >
                            <thead>

                                <tr  >
                                    <th>Sno </th>
                                    <th>Trip Code</th>
                                    <th>C Note</th>
                                    <th>Customer Name</th>
                                    <th>Route</th>
                                    <th>Vehicle Type</th>
                                    <th>Created On</th>
                                    <th>Trip Start Date</th>
                                    <th>Trip End Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>

                                <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                className = "text1";
                                } else {
                                className = "text2";
                                }
                                int checkBillType=0;
                                %>
                                <c:forEach items="${nodLevel3List}" var="list">
                                    <tr>                            
                                        <td ><%=sno%></td>
                                        <td ><c:out value="${list.tripCode}"/></td>
                                        <td ><c:out value="${list.cNotes}"/></td>
                                        <td ><c:out value="${list.customerName}"/></td>
                                        <td ><c:out value="${list.routeInfo}"/></td>
                                        <td ><c:out value="${list.vehicleType}"/></td>
                                        <td ><c:out value="${list.createdOn}"/></td>
                                        <td ><c:out value="${list.tripStartDate}"/></td>
                                        <td ><c:out value="${list.tripEndDate}"/></td>

                                    </tr>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <c:if test="${nodLevel3List == null}">
                        <center>
                            <br>
                            <br>
                            <br>
                            <b style="color: red"> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; No Records Found</b>
                        </center>
                    </c:if>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>  
    <!--</html>-->