

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.operation.business.OperationTO" %> 
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <%@ page import="java.util.*" %> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <title>JobCard</title>
        
    </head>
    <body>
        
        
        <script>
            
            
            
            function print(ind)
            {       
                var DocumentContainer = document.getElementById('print'+ind);
                var WindowObject = window.open('', "TrackHistoryData", 
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }            
            
            
        </script>
        
        <form name="mpr"  method="post" >                        
            <br>
            
            
            <%
            int index = 0;
            int sno = 0;

            int service = 0;
            ArrayList cardDetail = new ArrayList();
            cardDetail = (ArrayList) request.getAttribute("DetailsList");
            String[] problemNames,symptoms;            
            OperationTO operationTO = new OperationTO();

            int jlistSize = 36;
            int secNameLimit = 30;
            int probLimit = 30;

            System.out.println("jobcard in jsp=" + cardDetail.size());

            String secName = "";
            String probName = "";




            int cardDet = cardDetail.size();
            if (cardDet != 0) {
                for (int i = 0; i < cardDet; i = i + jlistSize) {
            %>
            
            
            
            <div id="print<%= i %>" >
                <style type="text/css">
                    .header {font-family:Arial;
                        font-size:15px;
                        color:#000000;
                        text-align:center;
                        padding-top:10px;
                        font-weight:bold;
                    }
                    .border {border:1px;
                        border-color:#CCCCCC;
                        border-style:solid;
                    }
                    .text1 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:12px;
                        color:#000000;
                    }
                    .text2 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:16px;
                        color:#000000;
                    }
                    .text3 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:17px;
                        color:#000000;
                    }
                    
                </style>    
                
                <table width="750" align="center" style="border:1px; border-color:#E8E8E8; border-style:solid;" border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td  colspan="2" height="30" align="center" class="text3"><b>Your Company Name.</b></td>
                    </tr>
                    <tr>
                        <td height="130" valign="top">
                            <table width="375" height="130" border="0" class="border" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <Td height="40" COLSPAN="2" width="400" class="text3" align="center">
                                        <b>JOB CARD </b>
                                    </Td>
                                </tr>
                                <%

                  ArrayList details = new ArrayList();
                  details = (ArrayList) request.getAttribute("details");
                  OperationTO opTO = new OperationTO();
                  OperationTO headContent = new OperationTO();
                  headContent = (OperationTO) details.get(0);
                                %>
                                <tr>
                                    <Td align="left" width="200" height="22" class="text2" style="padding-left:10px; "><span class="text1"> Date</span></Td>
                                    <Td align="left" width="200" height="22" class="text2" style="padding-left:10px; ">: &nbsp;  <b><span class="text1"> <%=headContent.getCreatedDate()%></span> </b></Td>                                    
                                </tr>
                                <tr>
                                    <Td align="left" height="22"  class="text2" style="padding-left:10px; "><span class="text1">Job Card No</span></Td>
                                    <Td align="left" height="22"  class="text2" style="padding-left:10px; ">: &nbsp; <b> <span class="text1"><%=request.getAttribute("jcMYFormatNo")%> </span>  </b> </Td>
                                </tr>
                                <tr>
                                    <Td align="left" height="22"  class="text2" style="padding-left:10px; "><span class="text1">Workshop</span></Td>
                                    <Td align="left" height="22"  class="text2" style="padding-left:10px; ">: &nbsp;  <b><span class="text1"> <%=request.getAttribute("compName")%> </span>  </b></Td>
                                </tr>
                                <tr>
                                    <Td height="22"  style="padding-left:10px;" class="text2"><span class="text1"> Vehicle No</span> </Td>
                                    <Td height="22"  style="padding-left:10px;" class="text2"> : &nbsp; <b> <span class="text1"> <%= headContent.getRegno() %> </span> </b> </Td>
                                </tr>                                
                            </table>
                        </td>
                        <Td height="125" valign="top" >
                            <table width="375" height="130" align="left" border="0" cellpadding="0" cellspacing="0" class="border">
                                <tr>
                                    <Td style="padding-left:10px;" WIDTH="150" class="text1"> Driver Name </Td>
                                    <Td style="padding-left:10px;" width="225" class="text1">: &nbsp; <b><%= headContent.getDriverName() %> </b></Td>
                                </tr>
                                <tr>
                                    <Td style="padding-left:10px;" class="text1">Operation area </Td>
                                    <Td style="padding-left:10px;" class="text1">: &nbsp; <b> <%= headContent.getUsageName() %> </b></Td>
                                </tr>
                                <tr>
                                    <Td style="padding-left:10px;" class="text1"> Kilometer</Td>
                                    <Td style="padding-left:10px;" class="text1">: &nbsp; <b><%= request.getAttribute("km") %> </b> </Td>
                                </tr>
                                <tr>
                                    <Td style="padding-left:10px;" class="text1"> Vehicle Make: </Td>
                                    <Td style="padding-left:10px;" class="text1">: &nbsp; <b><%= headContent.getMfrName() %> </b> </Td>
                                </tr>
                                <tr>
                                    <Td style="padding-left:10px;" class="text1"> Time IN  </Td>
                                    <Td style="padding-left:10px;" class="text1">: &nbsp; &nbsp  </Td>
                                </tr>
                                <tr>
                                    <Td style="padding-left:10px;" class="text1"> Time OUT&nbsp&nbsp;:  </Td>
                                    <Td style="padding-left:10px;" class="text1">: &nbsp; &nbsp <span class="text3" ></span> </Td>
                                </tr>
                            </table>
                            
                        </Td>
                    </tr>
                    
                    <tr>
                        <Td valign="top" colspan="2">
                            
                            <table width="750" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">
                                
                                
                                <tr>
                                    <Td class="text2" width="10" height="30" valign="top" style="font-size:12px; padding-left:5px; border:1px; border-color:#CCCCCC; border-style:solid;"> <strong>S.NO</strong></Td>
                                    <Td class="text2" width="75" valign="top" style="font-size:12px; padding-left:10px; border:1px; border-color:#CCCCCC; border-style:solid;" align="center"><strong>SECTION</strong></Td>
                                    <Td class="text2" width="250"  valign="top" style="font-size:12px; padding-left:10px; border:1px; border-color:#CCCCCC; border-style:solid;" align="center"><strong>COMPLAINTS</strong></Td>
                                    <Td  class="text2" width="250" valign="top" style="font-size:12px; padding-left:10px; border:1px; border-color:#CCCCCC; border-style:solid;" align="center"><strong>SYMPTOMS</strong></Td>                                          
                                    <Td class="text2" width="120" valign="top" style="font-size:12px; padding-left:10px; border:1px; border-color:#CCCCCC; border-style:solid;" align="center"><strong>TECHNICIAN</strong></Td>                                    
                                </tr> 
                                
                                <%
                                    for (int j = i; (j < cardDetail.size() ) && (j < jlistSize + i) ; j++) {
                                        operationTO = new OperationTO();
                                        operationTO = (OperationTO) cardDetail.get(j);
                                        secName = operationTO.getSecName();
                                        problemNames = operationTO.getProblemNameSplit();
                                        symptoms = operationTO.getSymptomsSplit();
                                        if (operationTO.getSecName().length() > secNameLimit) {
                                            secName = secName.substring(0, secNameLimit - 1);
                                        }

                                        
                                        if( operationTO.getProbName().equals("WORKORDERCOMPLAINT") ){ %>
                                <tr>
                                    <Td valign="top" colspan="5" width="750" align="center" HEIGHT="20" class="text1" style="border:1px; padding-left :6px;"   > <STRONG>IDENTIFIED COMPLAINTS </STRONG> </td>                                     
                                </tr>                                                                                                                                  
                                        
                                        <% }else if( operationTO.getProbName().equals("JOBCARDCOMPLAINT") ){ %>
                                <tr>
                                    <Td  colspan="5" align="center"  width="750"  HEIGHT="20"  class="text1" style="border:1px;  padding-left :6px;"  > <STRONG> CUSTOMER COMPLAINTS </STRONG> </td>                                     
                                </tr>                                                                                                                                                                  
                                        <% }else if( operationTO.getProbName().equals("PERIODICSERVICE") ){ %>
                                <tr>
                                    <Td valign="top" colspan="5" align="center"  width="750"  HEIGHT="20" class="text1"  style="border:1px;  padding-left :6px;"  ><STRONG>PERIODIC SERVICES </STRONG> </td>                                     
                                </tr>    
                                <% }else if( operationTO.getProbName() != "" ){ %>
                                
                                
                                <tr>
                                    <Td valign="top" HEIGHT="20" class="text1"  style="border:1px;  border-right-style:solid; padding-left :6px;" align="left"   > <%= sno %> </td>                                     
                                    <Td valign="top" HEIGHT="20" class="text1"  style="border:1px;  border-right-style:solid;  padding-left :6px;" align="LEFT"> <%= secName %> &nbsp; </td>                                     
                                    <Td valign="top" HEIGHT="20" class="text1" style="border:1px; border-right-style:solid;  padding-left :6px;" align="left"> 
                                    <% for(int k=0; k<problemNames.length; k++ ) {%>
                                      <%= problemNames[k] %>  <br>
                                    <%}%>  
                                    </td>
                                    <Td valign="top" HEIGHT="20" class="text1" style="border:1px;  border-right-style:solid;  padding-left :6px;" align="LEFT"> 
                                    
                                    <% for(int m=0; m<symptoms.length; m++ ){%>
                                      <%= symptoms[m] %>  <br>
                                    <%}%>
                                    
                                    </td>                                       
                                                                       
                                    <%

                                       ArrayList technician = new ArrayList();
                                       int cntr = 0;
                                       technician = (ArrayList) request.getAttribute("technicians");
                                       MrsTO mrstechTO = new MrsTO();
                                       if (operationTO.getEmpId() != 1 && operationTO.getEmpId() != 0) {

                                           for (int k = 0; k < technician.size(); k++) {
                                               mrstechTO = (MrsTO) technician.get(k);
                                               if (mrstechTO.getEmpId() == operationTO.getEmpId() && cntr ==0 ) {
                                    %>              
                                    <Td valign="top" HEIGHT="20" class="text1"  style="border:1px;  border-left-style:solid; padding-left :6px;  " align="left"><%=mrstechTO.getEmpName()%></td>
                                            <%  cntr++;  %>
                                    <%  }
                                        }
                                           cntr=0;
                                    } else if (operationTO.getEmpId() == 1) {
                                    %>     
                                    <Td valign="top" HEIGHT="20"  class="text1" style="border:1px;   border-left-style:solid; padding-left :6px; " align="left">External Technician</td>                           
                                    <%} else if (operationTO.getEmpId() == 0) {
                                    %>      
                                    <Td valign="top" HEIGHT="20"  class="text1"  style="border:1px;  border-left-style:solid; padding-left :6px; " align="left">Not Assigned</td>                           
                                    <% } %>
                                    
                                </tr>
                                

                       <%  
                        } 
                            index++;  
                            sno++;
                        }
                  
                  while( (index % jlistSize) != 0 ) { %>
                                <tr>
                                    <Td valign="top" HEIGHT="20" class="text1"  style="border:1px;  border-right-style:solid; padding-left :6px;" align="right"   > &nbsp;  </td>                                     
                                    <Td valign="top" HEIGHT="20" class="text1"  style="border:1px;  border-right-style:solid;  padding-left :6px;" align="LEFT"> &nbsp; </td>                                     
                                    <Td valign="top" HEIGHT="20"  class="text1" style="border:1px; border-right-style:solid;  padding-left :6px;" align="left"> &nbsp;  </td>
                                    <Td valign="top" HEIGHT="20" class="text1" style="border:1px;  border-right-style:solid;  padding-left :6px;" align="LEFT"> &nbsp; </td>                                       
                                    <Td valign="top" HEIGHT="20" class="text1" style="border:1px;   padding-left :6px;" align="LEFT"> &nbsp; </td>                                       
                                </tr>                                        
                                
                                <% index++; }  %>                                
                                
                            </table>
                        </td>
                    </tr>
                    <Tr>
                        <Td valign="top" colspan="6">
                            <table width="750" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">
                                
                                <Tr>
                                    <Td width="110" class="text1" height="30" > &nbsp; </Td>
                                    <Td width="110" class="text1" height="30" >&nbsp;</Td>
                                    <Td width="110" class="text1" height="30" >&nbsp;</Td>
                                    <Td width="110" class="text1" height="30" >&nbsp;</Td>
                                    <Td width="110" class="text1" height="30" >&nbsp;</Td>
                                    <Td width="110" class="text1" height="30" >&nbsp;</Td>
                                </Tr>
                                
                                <Tr>
                                    <Td  class="text1"><strong>Mechanic Signature
                                    </strong></Td>
                                    <Td  class="text1"><strong>Electrician Signature	
                                    </strong></Td>
                                    <Td  class="text1"><strong>BB Signature	
                                    </strong></Td>
                                    <Td  class="text1"><strong>Supervisor Signature	
                                    </strong></Td>
                                    <Td  class="text1"><strong>Driver Signature	
                                    </strong></Td>
                                    <Td  class="text1"><strong>WM Signature</strong></Td>
                                </Tr>
                            </table>
                        </Td>
                    </Tr>
                </table>
            </div>
            <center>   
                <input type="button" class="button" name="Print" value="Print" onClick="print(<%= i %>)" > &nbsp;        
            </center>
            <br>    
            <br>    
            
            <%  }   %>  
            <%  }   %>  
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
