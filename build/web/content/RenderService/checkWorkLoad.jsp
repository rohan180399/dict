<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="ets.domain.operation.business.OperationTO" %>    
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
</head>
<script>

function getWorkOrders(date)
{
    
    document.mrs.action ='/throttle/checkWorkOrder.do?sDate'+date;
    document.mrs.submit();    
}
function setFocus(){
    var temp='<%=request.getAttribute("date")%>';
    
    if(temp!=null){
        document.mrs.scheduleDate.value=temp;
        }
}
//window.onload=getWorkOrders();
</script>

<body onload="setFocus()">
<form name="mrs" method="post">
    
    
    
<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>

<!--<table width="200" border="0" align="center" cellpadding="0" cellspacing="0" class="border" >
    
     
 <tr height="30">
    <td colspan="5" class="contenthead"><div class="contenthead">Scheduled Work Order</div> </td>
    </tr>

<tr>
    <td class="text2" height="30" colspan="3">Date</td><td></td>
 <td class="text2" height="20" width="150" > <input name="scheduleDate" readonly  type="text" class="form-control" value=''>
<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mrs.scheduleDate,'dd-mm-yyyy',this)"/></td>       
</tr>
</table>
<br>
<center>
    <input type="button" class="button"  onclick="getWorkOrders(scheduleDate.value);" value="Search">    
</center>    -->
<br>
<br>
    <%
      ArrayList scheduledWODetails = (ArrayList) request.getAttribute("scheduledWODetails"); 
     if(scheduledWODetails.size() != 0){
    %>
<c:if test = "${scheduledWODetails != null}" >  
  
<table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">

<% 
int index=0; 
%>

<tr>
<td class="contentsub"  height="30"><div class="contentsub">S.No</div></td>    
<td class="contentsub"  height="30"><div class="contentsub">Work Order No</div></td>
<td class="contentsub" height="30"><div class="contentsub">Vehicle No</div></td>
<td class="contentsub" height="30"><div class="contentsub">Operation Point</div></td>
<td class="contentsub" height="30"><div class="contentsub">ServiceType</div></td>
<td class="contentsub" height="30"><div class="contentsub">Delivery Date</div></td>
<td class="contentsub" height="30"><div class="contentsub">Create Jobcard</div></td>
</tr>

      <c:forEach items="${scheduledWODetails}" var="service"> 
      <%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    
    }
    %>

<tr>
<td class="<%=classText%>"  height="30"><%=index+1%></td>
<td class="<%=classText%>"  height="30"><c:out value="${service.workOrderId}"/></td>
<td class="<%=classText%>"  height="30"><c:out value="${service.regno}"/></td>
<td class="<%=classText%>"  height="30"><c:out value="${service.compName}"/></td>
<td class="<%=classText%>"  height="30"><c:out value="${service.servicetypeName}"/></td>
<td class="<%=classText%>"  height="30"><c:out value="${service.scheduledDeleivery}"/></td>
<td class="<%=classText%>"  height="30"><a href="/throttle/createJobcardPageForWorkOrder.do?workOrderId=<c:out value="${service.workOrderId}"/>">Create Jobcard</a></td>
</tr>
<%index++;%>
</c:forEach>
</table>
</c:if>
<%
}
%>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </body>
</html>
