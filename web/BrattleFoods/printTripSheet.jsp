<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

</head>
     <body>
        <form name="settle" method="post">
           
            <br>
            <br>
                    <div id='print'>
                    <table  border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Sheet</td>
                        </tr>

                        <tr>
                            <td class="text1">Trip Id :</td>
                            <td class="text1">1000</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1">&nbsp;</td>

                        </tr>
                        <tr>
                            
                            <td class="text1">Trip Sheet Date</td>
                            <td class="text1">11-10-2013</td>
                            <td class="text1">CNote No</td>
                            <td class="text1">101,102</td>
                            <td class="text1">Customer Freight Rate/Km (SAR)</td>
                            <td class="text1">12</td>
                        </tr>
                        <tr>
                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001></td>
                            <td class="text2">Customer Name</td>
                            <td class="text2">Parveen Travells</td>
                            <td class="text2">Customer ContactNo</td>
                            <td class="text2">993456789</td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">Chennai-Delhi</td>
                            <td class="text1">Route Code</td>
                            <td class="text1">DL001</td>
                            <td class="text1">Vehicle No</td>
                            <td class="text1">DL 01 G 2025</td>
                        </tr>
                        <tr>
                            <td class="text2">Actual Pickup Date</td>
                            <td class="text2">10-10-2013</td>
                            <td class="text2">Estimated Delivery Date</td>
                            <td class="text2">14-10-2013</td>
                            <td class="text2">Carrying Weight</td>
                            <td class="text2">200TON</td>
                        </tr>
                        <tr>
                            <td class="text1">Trip Start Date</td>
                            <td class="text1">11-10-2013</td>
                            <td class="text1">Trip Start Time</td>
                            <td class="text1">10:00AM</td>
                                 
                            <td class="text1">Km Out Start Point</td>
                            <td class="text1">100KM</td>
                        </tr>
                        <tr>
                            <td class="text2">Reefer Start Date</td>
                            <td class="text2">11-10-2013</td>
                            <td class="text2">Reefer Start Time</td>
                            <td class="text2">01:30PM</td>
                            <td class="text2">Reefer Hm Out Start Point</td>
                            <td class="text2">250KM</td>
                        </tr>
                        <tr>
                            <td class="text1">Trip Status</td>
                            <td class="text1">Open</td>
                            <td class="text1">Vehicle Ownership</td>
                            <td class="text1">own</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1">&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Check List</td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle RC Copy</td>
                            <td class="text1">&nbsp;</td>
                            <td class="text1" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle FC Copy</td>
                            <td class="text2">R Srinivasan</td>
                            <td class="text2">Vehicle FC Valid Date</td>
                            <td class="text2">25-12-2013</td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Copy</td>
                            <td class="text1">R Srinivasan</td>
                            <td class="text1">Vehicle Insurance Valid Date</td>
                            <td class="text1">12-12-2013</td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit</td>
                            <td class="text2">BF00001</td>
                            <td class="text2">Vehicle Permit Valid Date</td>
                            <td class="text2">30-12-2013</td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax</td>
                            <td class="text2">BF001</td>
                            <td class="text2">Road Tax Valid Date</td>
                            <td class="text2">30-12-2013</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                   
               
                    <table border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg" >
                        <tr>
                            <td>CNote : 101</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td>
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                   
                   
                    
                   
                    <br>
                    <br>
                    <table border="1" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="CNoteAdd">
                        <tr>
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" >Starting Point</td>
                            <td class="contenthead" height="30" >Type</td>
                            <td class="contenthead" height="30" >St Address</td>
                            <td class="contenthead" height="30" >Est Date&amp;Time</td>
                            <td class="contenthead" height="30" >Ending Point</td>
                            <td class="contenthead" height="30" >Type</td>
                            <td class="contenthead" height="30" >End Address</td>
                            <td class="contenthead" height="30" >Est Date&amp;Time</td>
                            <td class="contenthead" height="30" >Total Distance</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Chennai</td>
                            <td>PickUp & DROP</td>
                            <td>anna nager</td>
                            <td>10-10-2013/09:00AM</td>
                            <td>Anna Road</td>
                            <td>DROP</td>
                            <td>Delhi</td>
                            <td>16-10-2013/01:00PM</td>
                            <td>1800km</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Chennai</td>
                            <td>PickUp</td>
                            <td>gundy</td>
                            <td>01-11-2013/10:30AM</td>
                            <td>Court Road</td>
                            <td>DROP</td>
                            <td>Delhi</td>
                            <td>05-11-2013/11:00AM</td>
                            <td>1800km</td>
                        </tr>
                        
                    </table>
                   
                    <br>
                    <br>
              
                    <center><h3> Driver Details</h3></center>
                    <table border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg" >
                        <tr style="width:50px;">
                            <td width="50" class="contenthead">S No&nbsp;</td>
                            <td class="contenthead">Driver Name</td>
                            <td class="contenthead">Log Start Km</td>
                            <td class="contenthead">Log End Km</td>
                            <td class="contenthead">Km Operate</td>
                        </tr>
                        <tr>
                            <td class="text1">1.</td>
                            <td class="text1">Babu</td>
                            <td class="text1">1100 </td>
                            <td class="text1">1300</td>
                            <td class="text1">200</td>
                        </tr>
                        <tr>
                            <td class="text2">2.</td>
                            <td class="text2">Arun</td>
                            <td class="text2">1300</td>
                            <td class="text2">1570</td>
                            <td class="text2">270</td>
                        </tr>
                        <tr>
                            <td class="text1">3.</td>
                            <td class="text1">Madhan</td>
                            <td class="text1">1570</td>
                            <td class="text1">1810</td>
                            <td class="text1">240</td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    
                   
                    <center><h3>Cleaner Details</h3></center>
                    <table border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg" >
                        <tr style="width:50px;">
                            <td width="50" class="contenthead">S No&nbsp;</td>
                            <td class="contenthead">Cleaner Name</td>
                        </tr>
                        <tr>
                            <td class="text1">1.</td>
                            <td class="text1">Surya</td>
                        </tr>
                        <tr>
                            <td class="text2">2.</td>
                            <td class="text2">Sangaya</td>
                        </tr>
                        <tr>
                            <td class="text1">3.</td>
                            <td class="text1">Raja</td>
                        </tr>

                    </table>
                    <br>
                    <br>
                   
                   
                    
                        
                        <table  border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                            <center><h3>Trip Advance Details</h3></center>
                            </tr>
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Driver Name</td>
                                <td class="contenthead">Account No</td>
                                <td class="contenthead">Transaction Date and Time</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">Babu</td>
                                <td class="text1">IB11236576A001</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">2000</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">Arun</td>
                                <td class="text2">IB11236576A002</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">2000</td>
                            </tr>
                            <tr>
                                <td class="text1">3</td>
                                <td class="text1">Madhan</td>
                                <td class="text1">IB11236576A003</td>
                                <td class="text1">22-10-2013</td>
                                <td class="text1">2100</td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        
                        

<!--                <div id="expDetail">
                    <div style="border: #ffffff solid">
                        <h4>Fuel Expense Details</h4>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Bunk Name</td>
                                <td class="contenthead">Address</td>
                                <td class="contenthead">Date and Time</td>
                                <td class="contenthead">Liter</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">Madhava Agencies</td>
                                <td class="text1">Madhavaram Chennai</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">30</td>
                                <td class="text1">1500</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">Guntur Enterprises</td>
                                <td class="text2">Gunter AP</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">30</td>
                                <td class="text2">1500</td>
                            </tr>
                        </table>
                        <br>
                        <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" id="FuelExpenseTBL" >
                            <tr style="width:50px;">
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th Bunk Name</th>
                                <th class="contenthead">Address</th>
                                <th Expense Date</th>
                                <th Liter</th>
                                <th Amount</th>
                            </tr>
                        </table>
                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Add Row" name="save" onClick="addFuelExpenseRow()">
                        </center>
                    </div>
                    <br>
                    <div style="border: #ffffff solid">
                        <h4>Supplementary Expense Details</h4>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Date</td>
                                <td class="contenthead">Description</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">Food</td>
                                <td class="text1">200</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">Food</td>
                                <td class="text2">200</td>
                            </tr>
                            <tr>
                                <td class="text2">3</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">Air Check</td>
                                <td class="text2">50</td>
                            </tr>
                        </table>
                        <br>
                        <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" id="suppExpenseTBL" >
                            <tr style="width:50px;">
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th Expense Date</th>
                                <th class="contenthead">Expenses Details</th>
                                <th Amount</th>
                            </tr>
                        </table>
                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Add Row" name="save" onClick="addSuppExpenseRow()">
                            <a  class="nexttab" href=""><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                        </center>
                        <br/>
                        <br/>
                    </div>
                </div>-->

              
                
                        <table border="0" class="border" align="center" width="800" cellpadding="0" cellspacing="0" id="bg">
                            <tr style="width:50px;">
                                <th colspan="8" class="contenthead">Remarks</th>
                            </tr>
                            <tr>
                                <td class="text2">Remarks</td>
                                <td class="text2"><p>hi the given order</p> <p>will want to delever</p></td>
                            </tr>

                        </table>
                        <br/>

                    </div>
            <center>
                <input type="button" value="Print" onClick="print('print');" >
            </center>
            <script>
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>