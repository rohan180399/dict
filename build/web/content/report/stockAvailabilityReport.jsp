
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Stock Available</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
</head>


<script type="text/javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


  function submitPage(val)
    {
        
       
        if(val=='GoTo'){
            var temp=document.searchPart.GoTo.value;    
            
            document.searchPart.pageNo.value=temp;
            
        }
        document.searchPart.button.value=val;
                
      
 document.searchPart.action="/throttle/stkAvbReport.do";
      
       document.searchPart.submit();        
    }
    
     
var httpReq;
var temp = "";
function ajaxData()
{
 
var url = "/throttle/getModels1.do?mfrId="+document.searchPart.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}

function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();                 
        setOptions(temp,document.searchPart.modelId);        
        if('<%= request.getAttribute("modelId")%>' != 'null' ){
            document.searchPart.modelId.value = <%= request.getAttribute("modelId")%>;
        }
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}

    function setFocus(){
         
        var itemCode='<%=request.getAttribute("itemCode")%>';
        
        var paplCode='<%=request.getAttribute("paplCode")%>';
         var itemName='<%=request.getAttribute("itemName")%>';
          var mfrId='<%=request.getAttribute("mfrId")%>';
           var modelId='<%=request.getAttribute("modelId")%>';
           var reConditionable='<%=request.getAttribute("reConditionable")%>';
          var categoryId='<%=request.getAttribute("categoryId")%>';
    
    if(itemCode!='null' ){
        document.searchPart.itemCode.value=itemCode;
    }
    if(paplCode!='null' ){
        document.searchPart.paplCode.value=paplCode;
    }
    if(itemName!='null' ){
        document.searchPart.itemName.value=itemName;
    }
    if(mfrId!='null' ){
        document.searchPart.mfrId.value=mfrId;
        ajaxData();        
    }
    if(reConditionable!='null' ){
        document.searchPart.reConditionable.value=reConditionable;
    }
    if(categoryId!='null' ){
        document.searchPart.categoryId.value=categoryId;
    }

}     
    

        function getItemNames(){
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
        //getVehicleDetails(document.getElementById("regno"));
        } 

    
    
    
    
</script>


<body onload="setFocus();getItemNames();">
<form name="searchPart"  method="post"  >
<%@ include file="/content/common/path.jsp" %> 
<%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Search Parts</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">    
    <tr>
        <td height="30">Item Code</td>
        <td height="30"><input name="itemCode" type="text" class="form-control" value="" ></td>
        <td height="30">PAPL Code</td>
        <td height="30"><input name="paplCode" type="text" class="form-control" value=""></td>
        <td height="30">RC Status</td>
        <td height="30"><select name="reConditionable" class="form-control"  style="width:125px;" >
                        <option value="">--Select--</option>
                        <option value='Y'>Yes </option>
                        <option value='N'>No</option>
                        </select>
        </td>
        <td height="30" rowspan="3" valign="middle"><input type="button" value="search" class="button" name="search" onClick="submitPage(this.name)" >    </td>
    </tr>
    <tr>
        <td height="30">Category</td>
        <td height="30"><select class="form-control" name="categoryId"  style="width:125px;" >
                        <option value="0">---Select---</option>
                        <c:if test = "${CategoryList != null}" >
                        <c:forEach items="${CategoryList}" var="Dept">
                        <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                        </c:forEach >
                        </c:if>
                        </select>
        </td>
        <td height="30">MFR</td>
        <td height="30"><select class="form-control" name="mfrId" onchange="ajaxData();"  style="width:125px;" >
                        <option value="">---Select---</option>
                        <c:if test = "${MfrList != null}" >
                        <c:forEach items="${MfrList}" var="Dept">
                        <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                        </c:forEach >
                        </c:if>
                        </select>
        </td>
        <td height="30">Model</td>
        <td height="30">
            <select class="form-control" name="modelId"  style="width:125px;" >
            <option value="0">---Select---</option>
            <c:if test = "${ModelList != null}" >
            <c:forEach items="${ModelList}" var="test">
            <option value='<c:out value="${test.modelId}" />'><c:out value="${test.modelName}" /></option>
            </c:forEach >
            </c:if>
            </select>
        </td>
    </tr>
    <tr>
        <td height="30">Item Name</td>
        <td height="30"><input id="itemName" size="20" maxlength="30" name="itemName" type="text" class="form-control" value=""></td>
        
        <td height="30">&nbsp;</td>
        <td height="30">&nbsp;</td>
        <td height="30">&nbsp;</td>
        <td height="30">&nbsp;</td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

 <% int index = 0;  
    int pageIndex = (Integer)request.getAttribute("pageNo");
 		%>    
            <br>
            <c:if test = "${IndexedPartsDetail != null}" >
                <table width="895" cellpadding="0" align="center" cellspacing="0" id="bg" class="border">
 	 	 	 	 	 	 	 	 	 	 	 	                    
                    <tr align="center">
                        <td width="34" height="30" class="contenthead">S.No</td>
                        <td width="84" height="30" class="contenthead">Mfr</td>
                        <td width="96" height="30" class="contenthead">Model</td>
                        <td width="68" height="30" class="contenthead">Mfr Code</td>
                        <td width="79" height="30" class="contenthead">PAPL Code</td>
                        <td width="195" height="30" class="contenthead">Item Name</td>                        
                        <td width="40" height="30" class="contenthead">Stock Level</td>
                        <td width="28" height="30" class="contenthead">RC state</td>
                        <td width="40" height="30" class="contenthead">Scrap out</td>
                        <td width="32" height="30" class="contenthead">Max Qty</td>
                        <td width="33" height="30" class="contenthead">Ro Level</td>
                        <td width="56" height="30" class="contenthead">Rack</td>
                        <td width="63" height="30" class="contenthead">Sub-Rack</td>
                       
                    </tr>
                    <%
					index = ((pageIndex-1)*10)+1 ;
                    %>
                    
                    <c:forEach items="${IndexedPartsDetail}" var="list"> 	
                        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr >                             
                            <td class="<%=classText %>" width="34" height="30"><%=index %></td>
                            <td class="<%=classText %>" width="84"  height="30"><c:out value="${list.mfrName}"/></td>
                            <td class="<%=classText %>"  width="96" height="30"><c:out value="${list.modelName}"/></td>
                            <td class="<%=classText %>" width="68"  height="30"><c:out value="${list.mfrCode}"/></td>
                            <td class="<%=classText %>" width="79"  height="30"><c:out value="${list.paplCode}"/></td>
                            <td class="<%=classText %>" width="195"  height="30"><c:out value="${list.itemName}"/></td>                            
                            <td class="<%=classText %>" width="40"  height="30"><c:out value="${list.stockLevel}"/></td>
                            <td class="<%=classText %>" width="28"  height="30"><c:out value="${list.reConditionable}"/></td>
                            <td class="<%=classText %>" width="40"  height="30"><c:out value="${list.uomName}"/></td>
                               <td class="<%=classText %>" width="32"  height="30"><c:out value="${list.maxQuandity}"/></td>
                               <td class="<%=classText %>" width="33"  height="30"><c:out value="${list.roLevel}"/></td>
                               <td class="<%=classText %>" width="56"  height="30"><c:out value="${list.rackName}"/></td>
                               <td class="<%=classText %>"  width="63"  height="30"><c:out value="${list.subRackName}"/></td>
                                 
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                </table>                         
            </c:if>
  
      


    
<table width="500px" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px; border-color:#FFFFFF; ">
		<tr>
		<td>
                      <%@ include file="/content/common/pagination.jsp"%>      
                </td>        
		</tr>
		</table>
                 <br>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

    