<%-- 
    Document   : alterAccountType
    Created on : 1 Nov, 2012, 10:08:30 AM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> CLPL </title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.alter.accountEntryTypeName,'accountTypeName')){
            return;
        }
        if(textValidation(document.alter.accountEntryTypeCode,'voucherTypeCode')){
            return;
        }
        if(textValidation(document.alter.description,'description')){
            return;
        }

        document.alter.action='/throttle/saveAlterAccountType.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<c:if test="${accountTypealterLists != null}">
    <c:forEach items="${accountTypealterLists}" var="ATL">
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit Account Type</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Account Type Name</td>
      <td class="text2"><input name="accountEntryTypeName" type="text" class="form-control" value="<c:out value="${ATL.accountEntryTypeName}"/>" size="20">
          <input type="hidden" name="accountEntryTypeID" value="<c:out value="${ATL.accountEntryTypeID}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Account Type Code</td>
    <td class="text2"><input name="accountEntryTypeCode" type="text" class="form-control" value="<c:out value="${ATL.accountEntryTypeCode}"/>" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Description</td>
    <td class="text2"><input name="description" type="text" class="form-control" value="<c:out value="${ATL.description}"/>" size="20"></td>
  </tr>
</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

