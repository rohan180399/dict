<%--
    Document   : Model Fuel List 
   Created on : Apr 18, 2012, 4:38:42 PM
    Author     : Arul
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<html>




    <body>
        <!--<body>-->
        <form name="dhcCharge">
            <%--<%@ include file="/content/common/message.jsp" %>--%>
            <!--            <table width="650px" align="center" ><tr>
                                <td bgcolor=#5e90af><center><font color="white" size="4">Slab Rate List</font></center></td>
                            </tr>
                        </table>-->

            <center><h3><span id="modelFuelStatus" style="color: green;"></span></h3></center>
            <center><h3><span id="modelFuelStatusError" style="color: red;"></span></h3></center>
            <c:if test="${status == 1}">
                <div id="modelfuel">
                <%--<c:if test = "${getModelFuelList == null}">--%>
                <table align="center" class="table table-info mb30 table-hover" id="head" style="width:auto">
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">

                            <!--<th>SNo.</th>-->
                            <!--<th>Model</th>-->
                            <th>Fuel</th>
                            <th>Fuel DG</th>
                            <th>Total Fuel</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <input type="text" name="fVehicle" id="fVehicle" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);"  onkeyup="sumFuels(this.value);" value="" style="width:120px;"/></td>
                        <td align="left"  ><input type="text" name="fuelDG" id="fuelDG" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="sumFuels(this.value);" value="" style="width:120px;"/></td>
                        <td align="left"  > <input type="text" name="totFuel" id="totFuel" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;" readonly/></td>
                        </td>
                    </tr>
                </table>
                <center>
                    <table>
                        <tr>
                            <td class="text1" ><input type="button" class="btn btn-info"  id="copy" name="copy"  value="COPY ALL" onclick="setFuels();"/></td>
                        </tr>
                    </table>
                    <br>
                <%--</c:if>--%>
                    <table align="center" class="table table-info mb30 table-hover" id="table" style="width:auto">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">

                                <th>SNo.</th>
                                <th>Fuel Type</th>
                                <th>Model</th>
                                <th>Fuel  Ltr / Kg</th>
                                <th>Fuel DG</th>
                                <th>Total Fuel</th>
                            </tr>
                        </thead>
                        <tbody>

                            <% int sno = 0;%>
                            <c:if test = "${getModelFuelList != null}">
                            <input type="hidden" name="contractRateIds" id="contractRateIds"  value="<c:out value="${contractRateId}"/>"/>
                            <c:forEach items="${getModelFuelList}" var="model">
                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>

                                <tr>
                                    <td class="<%=className%>"  align="center"> <%= sno%>  </td>
                                    <td align="left"><input type="hidden" id="fuelTypeId" name="fuelTypeId1" value='<c:out value="${model.fuelTypeId}"/>'/><c:out value="${model.fuelType}"/></td>
                                    <td align="left"><c:out value="${model.modelName}"/></td>
                                    <td align="left">

                                        <input type="hidden" name="fuelModelId" id="fuelModelId1<%=sno%>" value="<c:out value="${model.fuelModelId}"/>" style="width:120px;"/>
                                        <input type="hidden" name="modelId" id="modelId1<%=sno%>" value="<c:out value="${model.modelId}"/>" style="width:120px;"/>
                                        <input type="text" name="fuelVehicles" id="fuelVehicles1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);"  onkeyup="sumFuel(<%=sno%>);" value="<c:out value="${model.fuelVehicle}"/>" style="width:120px;"/></td>
                                    <td align="left"  ><input type="text" name="fuelDGs" id="fuelDGs1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="sumFuel(<%=sno%>);" value="<c:out value="${model.fuelDG}"/>" style="width:120px;"/></td>
                                    <td align="left"  > <input type="text" name="totalFuels" id="totalFuels1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${model.totalFuel}"/>" style="width:120px;" readonly/></td>
                                </tr>
                            </c:forEach>

                            </tbody>
                        </table>
                    </c:if>
                    <center>
                        <table>
                            <tr class="text2">
                                <!--<td> <input type="hidden"  value="" id="finds"></td><input type='hidden' class='text1' name="slabIds" id="slabIds" value='<c:out value="${slabId}"/>'>-->
                                <!--<td class="text1"><input type="button" class="button" value="Add Row" id="subs" onClick="addRowSlab('0,0,0,0,0,0,0'), setValue();"></td>-->
                                <td class="text1"><input type="button" class="btn btn-info"  id="saveModel" name="saveModel"  value="save" onclick="submitModelFuel();"/></td>
                            </tr>

                        </table>
                    </center> 
            </div>
            </c:if>
            <c:if test="${status == 2}">
                <div id="modelfuel">
                <%--<c:if test = "${getModelFuelList == null}">--%>
                <table align="center" class="table table-info mb30 table-hover" id="head" style="width:auto;display: none">
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">

                            <!--<th>SNo.</th>-->
                            <!--<th>Model</th>-->
                            <th>Fuel</th>
                            <th>Fuel DG</th>
                            <th>Total Fuel</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <input type="text" name="fVehicle" id="fVehicle" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);"  onkeyup="sumFuels(this.value);" value="" style="width:120px;"/></td>
                        <td align="left"  ><input type="text" name="fuelDG" id="fuelDG" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="sumFuels(this.value);" value="" style="width:120px;"/></td>
                        <td align="left"  > <input type="text" name="totFuel" id="totFuel" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;" readonly/></td>
                        </td>
                    </tr>
                </table>
                <center>
                    <table style="display:none"> 
                        <tr>
                            <td class="text1" ><input type="button" class="btn btn-info"  id="copy" name="copy"  value="COPY ALL" onclick="setFuels();"/></td>
                        </tr>
                    </table>
                    <br>
                <%--</c:if>--%>
                    <table align="center" class="table table-info mb30 table-hover" id="table" style="width:auto">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">

                                <th>SNo.</th>
                                <th>Model</th>
                                <th>Fuel</th>
                                <th>Fuel DG</th>
                                <th>Total Fuel</th>
                            </tr>
                        </thead>
                        <tbody>

                            <% int sno = 0;%>
                            <c:if test = "${getModelFuelList != null}">
                            <input type="hidden" name="contractRateIds" id="contractRateIds"  value="<c:out value="${contractRateId}"/>"/>
                            <c:forEach items="${getModelFuelList}" var="model">
                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>

                                <tr>
                                    <td class="<%=className%>"  align="center"> <%= sno%>  </td>
                                    <td align="left"><c:out value="${model.modelName}"/></td>
                                    <td align="left">

                                        <input type="hidden" name="fuelModelId" id="fuelModelId1<%=sno%>" value="<c:out value="${model.fuelModelId}"/>" style="width:120px;"/>
                                        <input type="hidden" name="modelId" id="modelId1<%=sno%>" value="<c:out value="${model.modelId}"/>" style="width:120px;"/>
                                        <input type="hidden" name="fuelVehicles" id="fuelVehicles1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);"  onkeyup="sumFuel(<%=sno%>);" value="<c:out value="${model.fuelVehicle}"/>" style="width:120px;"/><c:out value="${model.fuelVehicle}"/></td>
                                    <td align="left"  ><input type="hidden" name="fuelDGs" id="fuelDGs1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="sumFuel(<%=sno%>);" value="<c:out value="${model.fuelDG}"/>" style="width:120px;"/><c:out value="${model.fuelDG}"/></td>
                                    <td align="left"  > <input type="hidden" name="totalFuels" id="totalFuels1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${model.totalFuel}"/>" style="width:120px;" readonly/><c:out value="${model.totalFuel}"/></td>
                                </tr>
                            </c:forEach>

                            </tbody>
                        </table>
                    </c:if>
                    <center>
                        <table style="display:none">
                            <tr class="text2">
                                <!--<td> <input type="hidden"  value="" id="finds"></td><input type='hidden' class='text1' name="slabIds" id="slabIds" value='<c:out value="${slabId}"/>'>-->
                                <!--<td class="text1"><input type="button" class="button" value="Add Row" id="subs" onClick="addRowSlab('0,0,0,0,0,0,0'), setValue();"></td>-->
                                <td class="text1"><input type="button" class="btn btn-info"  id="saveModel" name="saveModel"  value="save" onclick="submitModelFuel();"/></td>
                            </tr>

                        </table>
                    </center> 
            </div>
            </c:if>
            

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>