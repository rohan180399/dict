<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
        <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitSearch()
        {
            if(isEmpty(document.mileageData.toDate.value))
            {
                alert("Select End Date");
                document.mileageData.toDate.focus();
                return false;
            }
            else
            {
                document.mileageData.action='/throttle/mileageReport.do';
                document.mileageData.submit();
            }
        }
        function setValues(){
            
            if('<%= request.getAttribute("toDate")%>' != '    null'){
                document.mileageData.toDate.value='<%= request.getAttribute("toDate")%>';
            }
            if('<%= request.getAttribute("mfrId")%>' != '    null'){
                document.mileageData.mfrId.value='<%= request.getAttribute("mfrId")%>';
            }
            if('<%= request.getAttribute("usageType")%>' != '    null'){
                document.mileageData.usageType.value='<%= request.getAttribute("usageType")%>';
            }
            if('<%= request.getAttribute("vehicleType")%>' != '    null'){
                document.mileageData.vehicleType.value='<%= request.getAttribute("vehicleType")%>';
            }
            if('<%= request.getAttribute("year")%>' != '    null'){
                document.mileageData.year.value='<%= request.getAttribute("year")%>';
            }

        }




    </script>
    <body onload="setValues()">



            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
           
        <form name="mileageData">
            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Mileage Report</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
        <td height="20"><font color="red">*</font>End Date</td>
        <td height="20"><input type="text" name="toDate"  readonly class="form-control">
            <img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(document.mileageData.toDate,'dd-mm-yyyy',this)"/></td>

        <td height="20">MFR</td>
        <td height="20">
            <select style="width:150px" name="mfrId"><option value="0">--Select--</option>
                <c:forEach items="${mfrList}" var="mfr">
                    <option value='<c:out value="${mfr.mfrId}"/>'><c:out value="${mfr.mfrName}"/>  </option>
                </c:forEach>
            </select>
        </td>

        <td height="20">Vehicle Age(in years)</td>
        <td height="20"><select  name="year" style="width:150px;" >
                <option value="0">-Select--</option>
                <option value="1"><1</option>
                <option value="2"><2</option>
                <option value="3"><3</option>
                <option value="5">>5</option>
            </select>
        </td>
    </tr>
    <tr>

        <td height="20">Usage Type</td>
        <td height="20">
            <select style="width:150px;" name="usageType">
                <option  value="0">--Select--</option>
                <c:forEach items="${vehicleUsage}" var="usage">
                    <option value='<c:out value="${usage.usageId}"/>'><c:out value="${usage.usageName}"/></option>
                </c:forEach>
            </select>

        </td>

        <td height="20">Vehicle Type</td>
        <td height="20">
            <select style="width:150px" name="vehicleType">
                <option  value="0"> --Select--</option>
                <c:forEach items="${vehicleTypes}" var="type">
                    <option value='<c:out value="${type.typeId}"/>'><c:out value="${type.typeName}"/></option>
                </c:forEach>

            </select>
        </td>
        <td height="20"><input type="button" class="button" name="search" value="search" onclick="submitSearch()">
        </td>

        <td height="20">&nbsp;
        </td>
    </tr>
</table>
</div></div>
</td>
</tr>
</table>

            <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" id="bg">

                



            </table>
            <br/>
            <center>
                
            </center>
            <br/>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            <br/><br/>
        <%
if(request.getAttribute("mileageData") != null){

ArrayList mileageData = (ArrayList) request.getAttribute("mileageData");
ArrayList colourList = (ArrayList) request.getAttribute("colourList");
System.out.println("mileageData"+mileageData.size());
System.out.println("colourList"+colourList.size());


String  strXML = "<graph caption='Mileage Report' xAxisName='MFR' yAxisName='Km/L' decimalPrecision='0' formatNumberScale='0'>";

//Initiate <dataset> elements
String strDataProdA = "";

//Iterate through the data

Iterator itr = mileageData.iterator();
Iterator itrC = colourList.iterator();
ReportTO reportTO = new ReportTO();
ReportTO reportT = new ReportTO();

while(itr.hasNext() && itrC.hasNext()){
reportTO = (ReportTO) itr.next();
reportT = (ReportTO) itrC.next();
strDataProdA += "<set name='" + reportTO.getMfrName() + "'  value='" + reportTO.getKm() + "' color='"+reportT.getColour()+"' />";
}


//Assemble the entire XML now
strXML +=  strDataProdA  + "</graph>";
System.out.println("strXML-->"+strXML);

%>
            <p>
            <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_Column3D.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML %>" />
                <jsp:param name="chartId" value="productSales" />
                <jsp:param name="chartWidth" value="850" />
                <jsp:param name="chartHeight" value="450" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                </td>
            </tr>

        </table>


    </p>
    <%
}
%>

    </body>
</html>
