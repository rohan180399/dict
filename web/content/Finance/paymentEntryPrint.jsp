<%-- 
    Document   : paymentEntryPrint
    Created on : Oct 22, 2013, 10:13:10 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.util.ThrottleConstants" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>\
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



//        function print()
//        {
//            var DocumentContainer = document.getElementById("printDiv");
//            var WindowObject = window.open('', "TrackHistoryData",
//                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
//            WindowObject.document.writeln(DocumentContainer.innerHTML);
//            WindowObject.document.close();
//            WindowObject.focus();
//            WindowObject.print();
//            WindowObject.close();
//        }
//
//        function back()
//        {
//            window.history.back()
//        }

        function printCashPaymentEntry(val, voucherNo) {
            document.suggestedPS.action = '/throttle/printCashPaymentEntry.do?voucherCode=' + val + '&voucherNo=' + voucherNo;
            document.suggestedPS.submit();
        }
        
        
  function findPrinter() {
         var applet = document.jzebra;
         if (applet != null) {
            // Searches for locally installed printer with "zebra" in the name
            applet.findPrinter("Zebra");
         }
         
         // *Note:  monitorFinding() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDoneFinding()" and handle your next steps there.
         monitorFinding();
      }

       function print() {
	        var head ="Chettinad Logistics Private Limited, Karikkali";
		var title = "CASH PAYMENT VOUCHER";
		var voucherNo = document.getElementById("voucherNo").value;
		var date = document.getElementById("tripDate").value;
//                var credit = document.getElementById("branchLedgerName").value;
                
		var spaceLine = "----------------------------------------------------------------------------";
                var driverLedgerName = document.getElementsByName("driverLedgerName");
		var remarks = document.getElementsByName("remarks");
		var accountAmount = document.getElementsByName("accountAmount");
		var totalAmount = document.getElementById("totalAmount").value;
		var rupeesWord = document.getElementById("rupeesWord").value;
	
//		var pre = "Prepared by";
//		var che = "Checked by";
//		var pas = "Passed by";
//		var reci = "Receiver";
//		var cash = "Cashier";
                var applet = document.jzebra;
		
         if (applet != null) {
			applet.findPrinter();	
//                        applet.append("\n");
                        applet.append("                      "+head+"                  \n");
                        applet.append("                                  "+title+"                 \n");
                        applet.append("\n");
                        applet.append("   NO        : "+voucherNo+"                                        "+date+"\n");
			//applet.append("\n");
			applet.append("   CreditA/C : KARIKALI CASH\n\n");
			//applet.append("\n");
			applet.append("   Debit A/c                       Narration                        Amount(Rs.)\n");
			//applet.append("\n"); 
			applet.append("   "+spaceLine+"\n");
                        for(var i=0; i<driverLedgerName.length; i++){
                         applet.append("    " + driverLedgerName[i].value.wordWrap(25, "\n"+"    " , true)  +"                                                "+"\n");
                         applet.append("                                                                  "+ accountAmount[i].value +"\n" );
                         applet.append("                              " + remarks[i].value.wordWrap(25, "\n"+"                                  ", true) +"\n");
			//applet.append("    "+driverLedgerName[i].value+"                <br>"+remarks[i].value+"    "+accountAmount[i].value+"\n");
//			applet.append("    "+driverLedgerName+"                         <br>"+remarks+"            "+accountAmount+"\n");
                        }
			applet.append("                                                                 --------------\n");
			//applet.append("\n");
			applet.append("                                     Voucher Total :                "+totalAmount+"\n");
			applet.append("   "+spaceLine+"\n");
			applet.append("   Rupees "+rupeesWord+" Only\n");
			applet.append("\n");
			applet.append("\n");
			applet.append("\n");
			applet.append("   Prepared by      Checked by      Passed by      Cashier      Receiver\n");
                        applet.append("\n");
			applet.append("\n");
			//applet.append("\n");
			//applet.append("\n");
            //applet.append("A310,116,0,3,1,1,N,\"FROM SAMPLE.HTML\"\n");
            //applet.append("A310,146,0,3,1,1,N,\"EDIT EPL_SAMPLE.TXT\"\n");
            //applet.appendImage(getPath() + "img/image_sample_bw.png", "EPL", 150, 300);
            //while (!applet.isDoneAppending()) {} //wait for image to download to java
            applet.print(); // send commands to printer
	 }
	 
	 
	
	 
         // *Note:  monitorPrinting() still works but is too complicated and
         // outdated.  Instead create a JavaScript  function called 
         // "jzebraDonePrinting()" and handle your next steps there.
	 monitorPrinting();
         
         /**
           *  PHP PRINTING:
           *  // Uses the php `"echo"` function in conjunction with jZebra `"append"` function
           *  // This assumes you have already assigned a value to `"$commands"` with php
           *  document.jZebra.append(<?php echo $commands; ?>);
           */
           
         /**
           *  SPECIAL ASCII ENCODING
           *  //applet.setEncoding("UTF-8");
           *  applet.setEncoding("Cp1252"); 
           *  applet.append("\xDA");
           *  applet.append(String.fromCharCode(218));
           *  applet.append(chr(218));
           */
         
      }
      String.prototype.wordWrap = function(m, b, c){
      	var i, j, l, s, r;
      	if(m < 1)
      	return this;
      	for(i = -1, l = (r = this.split("\n")).length; ++i < l; r[i] += s)
      	for(s = r[i], r[i] = ""; s.length > m; r[i] += s.slice(0, j) + ((s = s.slice(j)).length ? b : ""))
      	j = c == 2 || (j = s.slice(0, m + 1).match(/\S*(\s)?$/))[1] ? m : j.input.length - j[0].length
      	|| c == 1 && m || j.input.length + (j = s.slice(m).match(/^\S*/)).input.length;
      	return r.join("\n");
};
      function monitorPrinting() {
	var applet = document.jzebra;
	if (applet != null) {
	   if (!applet.isDonePrinting()) {
	      window.setTimeout('monitorPrinting()', 100);
	   } else {
	      var e = applet.getException();
	      alert(e == null ? "Printed Successfully" : "Exception occured: " + e.getLocalizedMessage());
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
      function monitorFinding() {
	var applet = document.jzebra;
	if (applet != null) {
	   if (!applet.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = applet.getPrinter();
              alert(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            alert("Applet not loaded!");
        }
      }
    </script>
    <%--

    --%>

    <body>
        <form name="suggestedPS" method="post">
            <c:if test="${cashPaymentEntry != null}">
                <c:forEach items="${cashPaymentEntry}" var="CPE">
                    <c:set var="accountEntryDate" value="${CPE.accountEntryDate}"/>
                </c:forEach>
            </c:if>

            <div id="printDiv" style="border: 2px solid">

                <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                    <tr>
                        <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4"><%=ThrottleConstants.companyName%><br>
                                <center><u> Cash Payment Voucher<u></center>
                                            </td>

                                            </tr>
                                            <c:if test="${cashPaymentEntry != null}">

                                                <tr>
                                                    <td height="30" colspan="2" height="30" style="text-align:left; ">Voucher No:&nbsp;<c:out value="${voucherNo}"/></td>
                                                    <td height="30" colspan="2" height="30" style="text-align:center; ">Date:<c:out value="${accountEntryDate}"/> </td>
                                                <input type="hidden" id="voucherNo" name="voucherNo" value="<c:out value="${voucherNo}"/>">
                                                <input type="hidden" id="tripDate" name="tripDate" value="<c:out value="${accountEntryDate}"/>">
                                                </tr>
                                                <tr>
                                                    <td height="30" colspan="2" height="30" style="text-align:left; ">Credit A/C : KARIKALI CASH</td>

                                                </tr>
                                                <tr>
                                                    <td height="30" style="text-align:left; border-bottom:1px dashed;">Debit A/C</td>
                                                    <td height="30" style="text-align:left; border-bottom:1px dashed;">Narration</td>
                                                    <td height="30" style="text-align:right; border-bottom:1px dashed;">Amount(Rs.)</td>
                                                </tr>
                                                <c:set var="debitTotal" value="${0.00}"/>
                                                <c:forEach items="${cashPaymentEntry}" var="CPE">
                                                    <c:set var="debitAmmount" value="${CPE.debitAmmount}"/>
                                                    <c:set var="accountEntryDate" value="${CPE.accountEntryDate}"/>
                                                    <tr>
                                                        <c:if test="${CPE.accountsType == 'DEBIT'}">
                                                            <c:set var="debitTotal" value="${CPE.debitAmmount + debitTotal}"/>
                                                       <input type="hidden" id="totalAmount" name="totalAmount" value="<c:out value="${debitTotal}"/>">
                                                        </c:if>
                                                        <input type="hidden" id="driverLedgerName" name="driverLedgerName" value="<c:out value="${CPE.ledgerName}"/>">
                                                        <input type="hidden" id="remarks" name="remarks" value="<c:out value="${CPE.remarks}"/>"> 
                                                        <input type="hidden" id="accountAmount" name="accountAmount" value="<c:out value="${CPE.debitAmmount}"/>">
                                                        <td height="50" style="text-align:left; "><c:out value="${CPE.ledgerName}"/></td>
                                                        <td height="50" style="text-align:left; "><c:out value="${CPE.remarks}"/></td>
                                                        <td height="50" style="text-align:right; "><c:out value="${CPE.debitAmmount}"/></td>
                                                    </tr>
                                                </c:forEach>
                                            </c:if>
                                            <tr>
                                                <td height="30" colspan="2"></td>
                                                <td height="30" colspan="2" style="text-align:left; border-bottom:1px dashed;"></td>
                                            </tr>
                                            <tr>
                                                <td height="30" colspan="2" style="text-align:right;">Voucher Total :</td>
                                                <td height="30" colspan="2" style="text-align:right; "> <c:out value="${debitTotal}"/></td>
                                            </tr>
                                            <tr>
                                                <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                    <%
                                                    if(pageContext.getAttribute("debitTotal") != null && !"".equals(pageContext.getAttribute("debitTotal"))){
//                                                         String test = (String) pageContext.getAttribute("debitTotal");
                                                       // double temp = Double.parseDouble(debitTotal);
                                                    double temp =(Double) pageContext.getAttribute("debitTotal");
                                                        //double temp = Double.parseDouble(pageContext.getAttribute("debitTotal"));
                                                        numberWords1.setRoundedValue(String.valueOf(java.lang.Math.ceil(temp)));
                                                        numberWords1.setNumberInWords(numberWords1.getRoundedValue());
                                                    }

                                                    %>    
                                        <input type="hidden" id="rupeesWord" name="rupeesWord" value="<jsp:getProperty name="numberWords1" property="numberInWords" />">
                                                    <td height="30" colspan="3" style="text-align:left; border-top:1px dashed;">Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</td>
                                                </jsp:useBean>
                                            </tr>
                                            <tr>
                                            <br>
                                            <br>
                                            <br>

                                            <td colspan="5">
                                                <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                                                    <tr>
                                                        <td height="150" width="20" style="text-align:left;">Prepared by</td>
                                                        <td height="150" width="20" style="text-align:left;">Checked by</td>
                                                        <td height="150" width="20" style="text-align:center;">Passed by</td>
                                                        <td height="150" width="20" style="text-align:right;">Receiver</td>
                                                        <td height="150" width="20" style="text-align:right;">Cashier</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            </tr>
                                            <applet name="jzebra" code="jzebra.PrintApplet.class" archive="/throttle/content/Finance/jzebra.jar" width="100px" height="100px">
                                           <param name="printer" value="zebra">
                                          </applet><br/>
                                            <tr align="center">
                                                <td colspan="4">
                                                    <input type=button onClick="print()" value="Print" />
                                                </td>
                                            </tr>
                                            </table>

                                            </div>
                                            <br>
                                            <c:if test = "${cashPaymentList != null}" >

                                                <br>
                                                <table align="center" width="100%" border="0" id="table" class="sortable">
                                                    <thead>
                                                        <tr>
                                                            <th><h3>S.No</h3></th>
                                                            <th ><h3>Entry Date</h3></th>
                                                            <th><h3>Voucher Code</h3></th>
                                                            <th><h3>Credit Ledger</h3></th>
                                                            <th><h3>Credit Amount</h3></th>
                                                            <th><h3>Debit Ledger</h3></th>
                                                            <th><h3>Debit Amount</h3></th>
                                                            <th><h3>Print</h3></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <% int index = 0;%>
                                                        <c:forEach items="${cashPaymentList}" var="PL">
                                                            <%
                                                                        String classText = "";
                                                                        int oddEven = index % 2;
                                                                        if (oddEven > 0) {
                                                                            classText = "text1";
                                                                        } else {
                                                                            classText = "text2";
                                                                        }
                                                            %>
                                                            <tr>
                                                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.accountEntryDate}"/> </td>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${PL.voucherNo}" /></td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.creditLedgerName}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.creditAmount}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.debitLedgerName}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.debitAmount}"/> </td>
                                                                <td class="<%=classText%>"  align="left"><a href="" onclick="printCashPaymentEntry('<c:out value="${PL.voucherCode}" />', '<c:out value="${PL.voucherNo}"/>')">Print</a></td>

                                                            </tr>

                                                            <% index++;%>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                            <br>
                                            <br>
                                            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                            <div id="controls">
                                                <div id="perpage">
                                                    <select onchange="sorter.size(this.value)">
                                                        <option value="5" selected="selected">5</option>
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                    <span>Entries Per Page</span>
                                                </div>
                                                <div id="navigation">
                                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                                </div>
                                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                            </div>
                                            <script type="text/javascript">
                                                var sorter = new TINY.table.sorter("sorter");
                                                sorter.head = "head";
                                                sorter.asc = "asc";
                                                sorter.desc = "desc";
                                                sorter.even = "evenrow";
                                                sorter.odd = "oddrow";
                                                sorter.evensel = "evenselected";
                                                sorter.oddsel = "oddselected";
                                                sorter.paginate = true;
                                                sorter.currentid = "currentpage";
                                                sorter.limitid = "pagelimit";
                                                sorter.init("table", 1);
                                            </script>
                                            </body>
                                            </html>