

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
   
    </script>
    <body >
        <form name="bodyPartsReport" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            <br>
      <c:set var="Amount" value="0" />
             <% int index = 0;%> 
              <%int oddEven=0;%>
              <%  String classText = "";%>
            <c:if test = "${BodyBillDetails != null}" >
                <table width="646" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                   <tr>
                    <td  width="51" height="30" class="contenthead">Details</td>       
                   </tr>
                   <tr class="contenthead">
                        <td  width="51" height="30" class="contentsub">Date</td>                            
                        <td  width="32" height="30" class="contentsub">JC No</td>                                                    
                        <td  width="78" height="30" class="contentsub">Problem</td>
                        <td  width="96" height="30" class="contentsub">Activity</td>
                        <td  width="65" height="30" class="contentsub">Amount</td>
                    </tr>
                   
                    <c:forEach items="${BodyBillDetails}" var="billDetail"> 
                        <%
            classText = "";
             oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                        <tr>
                            <td class="<%=classText%>" width="51" height="30"><c:out value="${billDetail.billDate}" /></td>
                            <td class="<%=classText%>" width="61" height="30"><c:out value="${billDetail.jobCardId}" /></td>                           
                            <td class="<%=classText%>" width="101" height="30"><c:out value="${billDetail.problem}" /></td>
                            <td class="<%=classText%>" width="120" height="30"><c:out value="${billDetail.activity}" /></td>
                            <td class="<%=classText%>" width="90" height="30"><c:out value="${billDetail.totalAmount}" /></td>  
                           <c:set var="Amount" value="${Amount + billDetail.totalAmount}" />  
                        </tr> 
                         
                        <% index++;%>
                    </c:forEach>
                    
                    <tr >
                        <td   height="30" class="text2">&nbsp;</td>                                                                                                   
                        <td   height="30" class="text2">&nbsp;</td>
                        <td   height="30" class="text2"><b>TotalAmount</b></td>
                        <td class="text2" height="30"> 
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${Amount}" pattern="##.00"/></b>  
                    </td>
                    <td   height="30" class="text2">&nbsp;</td>     
 </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
</html>
