<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
        <title>MRSList</title>
    </head>
    
    
    
    <script>

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

function searchSubmit(){
/*if(document.bill.toDate.value==''){
    alert("Please Enter Till Date");
    return;
}
*/
document.bill.action = '/throttle/periodicServiceList.do'
document.bill.submit();    

}    

function setDate(serviceType){
    
    if(serviceType != 'null'){
        document.bill.regno.value=serviceType;
    }    
}


function getVehicleNos(){
    
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}



    </script>
    
    <body onLoad="getVehicleNos();setDate('<%= request.getAttribute("regno") %>' )">
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>                       
            <%@ include file="/content/common/message.jsp" %>
<table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:800;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Service Due  Report</li>
    </ul>
    <div id="first">
    <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
            <td align="left" height="30">Operation Point</td>
            <td><select class="form-control" name="opId" style="width:125px">                            
                            <c:if test = "${LocationLists != null}" >
                                <c:forEach items="${LocationLists}" var="company">
                                    <c:if test = "${company.companyTypeId == 1011}" >
                                    <option value='<c:out value="${company.cmpId}"/>'><c:out value="${company.name}"/></option>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                    </select> </td>
            <td align="left" height="30"><font color="red">*</font>Vehicle No</td>
            <td height="30"><input name="regno" id="regno" type="text" class="form-control" maxlength="11" value="" /></td>
<!--            <td align="left" height="30"><font color="red">*</font>Till Date</td>-->
<!--            <td height="30"><input name="toDate" type="text" class="form-control" value="" size="13" /> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/></td>-->
        <td><input type="button"  value="Search" class="button" name="search" onClick="searchSubmit()">   </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
        
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            int cntr=1;
            %>            
            <br>
            <br>
        <c:if test = "${serviceList != null}" >
        <table width="817" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
            
            <tr>
                <td width="35" class="contentsub">Sno</td>
                <td width="132" height="30" class="contentsub"> Operation Point </td>
                <td width="127" height="30" class="contentsub">Vehicle No </td>
                <td width="138" height="30" class="contentsub">Last Reported KM &amp; Date </td>
                <td width="121" class="contentsub">Projected KM as on Date </td>
                <td width="264" height="30" class="contentsub">Due For </td>
            </tr>
            <c:set var="vehNo" value="" />            
            
            <c:forEach items="${serviceList}" var="service">
                <% if(index==0 ){ %>
                    <c:set var="vehNo" value="${service.regno}" />      
                <% } %>
                
                
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                            
                      
               <% if(index == 0){ %>
                <tr>                    
                <td width="35" class="<%=classText %>"><%=cntr %></td>
                <td width="132" height="30" class="<%=classText %>"> 
                <c:if test = "${LocationLists != null}" >
                    <c:forEach items="${LocationLists}" var="company"> 
                        <c:if test = "${company.cmpId ==  compId}" >
                        <c:out value="${company.name}"/>
                        </c:if>                                   
                    </c:forEach>
                </c:if>                
                </td>
                <td width="127" height="30" class="<%=classText %>"><c:out value="${service.regno}" /> </td>
                <td width="138" height="30" class="<%=classText %>"> <c:out value="${service.lastServicedKm}" />&nbsp;Km ,&nbsp;<c:out value="${service.servicedDate}" /> </td>
                <td width="121" class="<%=classText %>"><c:out value="${service.currentKM}" /></td>
                <td width="264" height="30" class="<%=classText %>">
                    
                    <c:if test = "${service.periodicServiceList != null}" >
                    <c:forEach items="${service.periodicServiceList}" var="serviceList">
                        
                        <c:out value="${serviceList.serviceName}"/> (<c:out value="${serviceList.kmReading}" />)

                    </c:forEach>
                </c:if>
                </td>
                </tr>      
                <% cntr++; %>
               <% } %>



                <% if(index != 0){ %>
                <c:if test="${service.regno!=vehNo }" >                 
                <tr>                    
                <td width="35" class="<%=classText %>"><%=cntr %></td>
                <td width="132" height="30" class="<%=classText %>"> 
                <c:if test = "${LocationLists != null}" >
                    <c:forEach items="${LocationLists}" var="company"> 
                        <c:if test = "${company.cmpId ==  compId}" >
                        <c:out value="${company.name}"/>
                        </c:if>                                   
                    </c:forEach>
                </c:if>                
                </td>
                <td width="127" height="30" class="<%=classText %>"><c:out value="${service.regno}" /> </td>
                <td width="138" height="30" class="<%=classText %>"> <c:out value="${service.lastServicedKm}" />&nbsp;Km ,&nbsp;<c:out value="${service.servicedDate}"/></td>
                <td width="121" class="<%=classText %>"><c:out value="${service.currentKM}" /></td>
                <td width="264" height="30" class="<%=classText %>"> 
                    <c:if test = "${service.periodicServiceList != null}" >
                    <c:forEach items="${service.periodicServiceList}" var="serviceList">
                        
                        <c:out value="${serviceList.serviceName}"/> (<c:out value="${serviceList.kmReading}" />)<br>

                    </c:forEach>
                    </c:if>

                </td>
                </tr>   
                <% cntr++; %>
                </c:if>                

                <c:if test="${service.regno==vehNo }" >        
                <tr>                    
                <td width="35" class="<%=classText %>">&nbsp;</td>
                <td width="132" height="30" class="<%=classText %>">&nbsp;</td>
                <td width="127" height="30" class="<%=classText %>">&nbsp;</td>
                <td width="138" height="30" class="<%=classText %>">&nbsp; </td>
                <td width="121" class="<%=classText %>"> &nbsp; </td>
                <td width="264" height="30" class="<%=classText %>"> 
                    <c:if test = "${service.periodicServiceList != null}" >
                    <c:forEach items="${service.periodicServiceList}" var="serviceList">
                        
                        <c:out value="${serviceList.serviceName}"/> (<c:out value="${serviceList.kmReading}" />)<br>

                    </c:forEach>
                </c:if>
                </td>
                </tr>
                </c:if>                
                <% } %> 
                 
                 
                 
                <c:set var="vehNo" value="${service.regno}" /> 
                <% index++; %>
            </c:forEach>
            
        </table>
        </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
