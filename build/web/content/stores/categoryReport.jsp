
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
    
    <script language="javascript">
       function submitPage(){
           var chek=validation();
           if(chek=='true'){
               
               if(document.salesBillTrend.itemType.value=='1')
                   {
                       
                document.salesBillTrend.action='/throttle/categoryReport.do';
                document.salesBillTrend.submit();
                }
                else
                    {
                        
                        document.salesBillTrend.action='/throttle/categoryNewReport.do';
                document.salesBillTrend.submit();
                        }
           }
       }
       function validation(){
               if(textValidation(document.salesBillTrend.fromDate,'From Date')){
                   document.salesBillTrend.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.salesBillTrend.toDate,'To Date')){
                   document.salesBillTrend.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           var b='<%=request.getAttribute("itemType")%>';
           if(a!='null'){
               
                document.salesBillTrend.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.salesBillTrend.toDate.value='<%=request.getAttribute("toDate")%>';
           }
           if(b!='null'){
               document.salesBillTrend.itemType.value=b;
               }
       }
    </script>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label.CategorywiseReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label.CategorywiseReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValues();">
        <form  name="salesBillTrend" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>                 
            <%@ include file="/content/common/message.jsp" %>
            <!--<br>-->
           
            <table class="table table-info mb30 table-hover">
                <thead><tr><th colspan="4"><spring:message code="fmsstores.label.CategorywiseReport"  text="default text"/></th></tr></thead>
<!--                <tr >
                    <Td colspan="4" class="contenthead">Categorywise Report</Td>
                </tr>-->
                
                <tr>
                    <td ><font color="red">*</font><spring:message code="fmsstores.label.FromDate"  text="default text"/></td>
                    <td ><input name="fromDate" style="width:260px;height:40px;" class="datepicker form-control"  autocomplete="off" type="text" value="" size="20">
                    <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.fromDate,'dd-mm-yyyy',this)"/></td>-->
                   <td  height="25"><font color="red">*</font><spring:message code="fmsstores.label.ToDate"  text="default text"/></td>
                   <td ><input name="toDate" style="width:260px;height:40px;" class="datepicker form-control"  autocomplete="off" type="text" value="" size="20">
                    <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.toDate,'dd-mm-yyyy',this)"/></td>-->
               </tr>
               <tr>
                    <td ><font color="red">*</font><spring:message code="fmsstores.label.ItemType"  text="default text"/></td>
                    <td >
                        <select name="itemType" class="form-control" style="width:260px;height:40px;">
                            <option value="1"><spring:message code="fmsstores.label.RC"  text="default text"/></option>
                            <option value="2"><spring:message code="fmsstores.label.New"  text="default text"/></option>
                    </select></td>
                    <td></td>
               <td><input type="button" class="btn btn-success" value="<spring:message code="fmsstores.label.Search"  text="default text"/>" onclick="submitPage();" />          
               </td>
               </tr>
            </table>
            
            <!--<br><br>-->
                
            <br>
                <table class="table table-info mb30 table-hover">
            <c:if test = "${categoryRcReport != null}" >
                <thead>
                <tr>
                    <th><spring:message code="fmsstores.label.Sno"  text="default text"/></th>
                    <th ><spring:message code="fmsstores.label.CategoryName"  text="default text"/></th>
                    <th ><spring:message code="fmsstores.label.StockWorth"  text="default text"/></th>
                </tr>
                </thead>
                <%
					int index = 1 ;
                    %>
                <c:forEach items="${categoryRcReport}" var="pd">
                          <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                    <tr>  
                       <td class="<%=classText %>"><%=index%></td>
                    <td class="<%=classText %>"><c:out value="${pd.categoryName}"/></td>
                    <td class="<%=classText %>"><c:out value="${pd.amount}"/></td>
                    </tr>
                       <%
            index++;
                        %>
                </c:forEach> 
            </c:if>
        </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
