
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>



<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDoYZk9NSBDKBQW5AYu0Vuq88IC0ue3mZI"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2YEWWi-juKcJyjyIKB3-8lEw9GZxNiCc&libraries=places"></script>
<script type="text/javascript">
    function getLatLongDistacne() {
//    var latitude1 = 29.156386;
//var longitude1 = 77.0329163;
//var latitude2 = 28.5574544;
//var longitude2 = 77.51775;
        alert("google...");
//    var distance = (google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(latitude1, longitude1), new google.maps.LatLng(latitude2, longitude2))/1000).toFixed(2);
//    alert(distance);

        var source = new google.maps.LatLng(29.156386, 77.0329163);


        var destination = new google.maps.LatLng(28.5574544, 77.51775);
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix({
            origins: [source],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function(response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                var distance = response.rows[0].elements[0].distance.text;
                alert(distance);
                var duration = response.rows[0].elements[0].duration.text;


            } else {
                alert("Unable to find the distance via road.");
            }
        });
    }


    function mailTriggerPage(tripId) {
        window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewCurrentLocation(vehicleRegNo) {
        var url = ' http://115.112.176.88:9090/fleetvigil/jsp/maps/Vehicle_Locator.jsp?cid=ICTRIPL&vehicle=' + vehicleRegNo;
        //alert(url);
        window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewTripDetails(tripId, vehicleId, tripType) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails1(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        document.tripSheet.action = '/throttle/viewVehicle.do?vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
            document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
        }
        if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
            document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("zoneId")%>' != 'null') {
            document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
        }

        if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
            document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
//            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
//                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
//            }
        if ('<%=request.getAttribute("podStatus")%>' != 'null') {
            document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
        }

    }

    function submitPage() {
        document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value + "&param=search";
        document.tripSheet.submit();
    }

    function submitPage1() {
        document.tripSheet.action = '/throttle/viewTripSheets.do?param=exportExcel';
        document.tripSheet.submit();
    }
    function changeVehicleAfterTripStart(tripId, vehicleId, tripType) {
        document.tripSheet.action = '/throttle/changeVehicleAfterTripStart.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }


</script>

<script type="text/javascript">
    var httpRequest;
    function getLocation() {
        var zoneid = document.getElementById("zoneId").value;
        if (zoneid != '') {

            // Use the .autocomplete() method to compile the list based on input from user
            $('#cityFrom').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getLocationName.do",
                        dataType: "json",
                        data: {
                            cityId: request.term,
                            zoneId: document.getElementById('zoneId').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#cityFromId').val(tmp[0]);
                    $('#cityFrom').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        }
    }

</script>



<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.PrimaryOperation22"  text="Trip Sheet"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active"><c:out value="${menuPath}"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body sorter.size(5);
                  setValues();">
                  <form name="tripSheet" method="post">

                    <table class="table table-info mb30 table-hover"  align="left" style="width:70%;">

                        <tr >
                            <td>Billing Type</td>
                            <td>
                                <select name="billingType" id="billingType" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="1" >Primary</option>
                                    <option value="2" >Secondary</option>
                                    <option value="3" >All</option>
                                </select>
                            </td>
                            <td>Vehicle No</td>
                            <td>
                                <select name="vehicleId" id="vehicleId" class="form-control"  >
                                    <c:if test="${vehicleList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleList}" var="vehicleList">
                                            <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Trip Sheet No</td>
                            <td>
                                <input name="tripSheetId" id="tripSheetId" type="text" class="form-control"   value="<c:out value="${tripSheetId}"/>"  >
                            </td>

                            <td>Vehicle Type</td>
                            <td>
                                <select name="vehicleTypeId" id="vehicleTypeId" class="form-control"  >
                                    <c:if test="${vehicleTypeList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleTypeList}" var="vehicleTypeList">
                                            <option value='<c:out value="${vehicleTypeList.typeId}"/>'><c:out value="${vehicleTypeList.typeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>Fleet Center</td>
                            <td>
                                <select name="fleetCenterId" id="fleetCenterId" class="form-control"   >
                                    <c:if test="${companyList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${companyList}" var="companyList">
                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Zone</td>
                            <td>
                                <select name="zoneId" id="zoneId" class="form-control"   onchange="getLocation();" >
                                    <c:if test="${zoneList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${zoneList}" var="zoneList">
                                            <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Location</td>
                            <td>
                                <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityName}"/>" class="form-control" >
                                <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityId}"/>" class="form-control"  >

                            </td>
                            <td>Customer</td>
                            <td>
                                <select name="customerId" id="customerId" class="form-control"  >
                                    <c:if test="${customerList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.customerName}"/>'><c:out value="${customerList.customerName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="<c:out value="${toDate}"/>">
                            </td>
                            <td>Status</td>
                            <td>
                                <select name="tripStatusId" id="tripStatusId" class="form-control"  >
                                    <c:if test="${statusDetails != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>POD Status</td>
                            <td>
                                <select name="podStatus" id="podStatus" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="0" >POD Uploaded</option>
                                    <option value="1" >POD not Uploaded</option>
                                </select>
                            </td>
                        </tr>

                        <tr >
                            <td colspan="8" align="center">
                                <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                                <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                                <input type="button"   value="Search" class="btn btn-success" name="Search" onclick="submitPage();" style="width:100px;height:30px;font-weight: bold;padding:1px;">

                                <c:if test="${statusId == 100}">
                                    <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel" style="width:100px;height:30px;font-weight: bold;padding:1px;">
                                </c:if>
                                <c:if test="${statusId != 100}">
                                    &nbsp;
                                </c:if>
                        </tr>
                    </table>
                    <% int index = 1;%>
                    <c:if test="${tripDetails == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                        <c:if test="${tripDetails != null}">

                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr >
                                    <th>Sno</th>
                                        <c:if test="${tripType == 1}">
                                        <th>POD</th>
                                        </c:if>
                                    <th>Vehicle No </th>
                                    <th>Container No(s) </th>
                                    <th>GR No </th>
                                    <th>Trip Code</th>
                                    <th>Customer Name</th>
                                    <th>Order Type </th>
                                    <th>Transporter</th>
                                    <th>Billing Party Name </th>

                                    <th>Route </th>
                                    <th>Driver Name with Contact </th>
                                    <th>Vehicle Type </th>
                                    <th>Trip Schedule</th>
                                    <th >Expected Arrival Date</th>
                                    <th>Trip Status</th>
                                        <%--  <th>C-Note Edit</th> --%>
                                    <th>LinkToMap</th>

                                    <th>AdminAction</th>


                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${tripDetails}" var="tripDetails">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td class="form-control" >
                                            <%=index%>
                                        </td>

                                        <c:if test="${tripType == 1}">
                                            <td class="form-control" align="center">
                                                <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                    <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                        <c:if test="${tripDetails.podCount == '0'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.podCount != '0'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                        </c:if>
                                                    </c:if>
                                            </td>
                                        </c:if>

                                        <td class="form-control" >
                                            <a href="#" onclick="viewVehicleDetails(<c:out value="${tripDetails.vehicleId}"/>)"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.containerNo}"/></td>
                                        <td class="form-control" ><c:out value="${tripDetails.grNo}"/></td>
                                        <td class="form-control"  >
                                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 0);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                        </td>

                                        <td class="form-control" align="center">
                                            <c:out value="${tripDetails.billingParty}"/>
                                        </td>
                                        <td class="form-control" align="center">

                                            <c:out value="${tripDetails.movementTypeName}"/>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleVendor}"/></td>


                                        <td  align="center" class="form-control" >
                                            <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                <c:out value="${tripDetails.customerName}"/>
                                            </c:if>
                                            <c:if test="${tripDetails.customerName == 'Empty Trip'}">
                                                <img src="images/emptytrip.png" alt="EmptyTrip"   title="Empty Trip"/>
                                            </c:if>
                                        </td>


                                        <td class="form-control" >
                                            <%--   <c:if test="${tripDetails.routeNameStatus > '2'}">
                                                   <img src="images/Black_star1.png" style="height:20%;width:20%;" alt="MultiPoint"  title=" MultiPoint"/><c:out value="${tripDetails.routeInfo}"/>
                                               </c:if>
                                               <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                                   <c:out value="${tripDetails.routeInfo}"/>
                                               </c:if>  --%>
                                            <c:out value="${tripDetails.routeInfo}"/>
                                        </td>

                                        <td class="form-control" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                        <td class="form-control" ><c:out value="${tripDetails.tripScheduleDate}"/> &nbsp; <c:out value="${tripDetails.tripScheduleTime}"/></td>
                                        <td class="form-control" ><c:out value="${tripDetails.expectedArrivalDate}"/> &nbsp; <c:out value="${tripDetails.expectedArrivalTime}"/></td>

                                        <td class="form-control" >
                                            <%--  <c:if test="${tripDetails.extraExpenseStatus == 0}">
                                                  <c:out value="${tripDetails.status}"/>
                                                  <c:if test="${tripDetails.statusId == 23}">
                                                      <c:out value="${tripDetails.deattchLoc}"/>
                                                  </c:if>
                                              </c:if> --%>
                                            <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                <c:if test="${tripDetails.statusId == 14}">
                                                    <c:out value="${tripDetails.status}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.statusId != 14}">
                                                    FC Closure
                                                </c:if>

                                            </c:if>
                                        </td>


                                        <c:if test="${roleId != '1030'}">

                                            <%--     <td class="form-control" >
                                                     <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo != 'Empty Trip' && roleId != '1032' }">
                                                         <a href="getConsignmentDetailsForEditAfterTripStart.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>"><c:out value="${tripDetails.cNotes}"/></a>
                                                     </c:if>
                                                     <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo == 'Empty Trip'}">
                                                         &nbsp;
                                                     </c:if>
                                                 </td> --%>
                                        </c:if>
                                        <td class="<%=className%>" width="450" align="left">
                                            <c:if test="${tripDetails.statusId <= 10}">
                                                <c:if test="${tripDetails.gpsLocation == 'NA'}">
                                                    NA
                                                </c:if>
                                                <c:if test="${tripDetails.gpsLocation != 'NA'}">
                                                    <a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.gpsDeviceId}" />');"><c:out value="${tripDetails.gpsLocation}" /></a>
                                                </c:if>
                                            </c:if>
                                        </td>
                                        <td class="form-control" >
                                            <c:if test="${tripDetails.statusId <= 32}">
                                                <c:if test="${roleId != '1030'}">
                                                    <c:if test="${RoleId == 1023 || RoleId == 1069 || RoleId == 1068 && tripDetails.statusId == 10}">
    <!--                                                    <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);">Change Vehicle</a>-->
                                                        <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);" style="color:#272E39;"   >
                                                            Change Vehicle
                                                        </a>
                                                        &nbsp;
                                                    </c:if>

                                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                                        Empty Trip Waiting For Approval
                                                    </c:if>

                                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                                        <c:if test="${tripDetails.statusId == 6}">
                                                            <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">AllotVehicle</a>


                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 7}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 8 }">
                                                            <c:if test="${RoleId != 1033}">
                                                                <c:if test="${tripDetails.nextTrip != 2}">
    <!--                                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">UnFreeze</a>-->
                                                                    <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                        UnFreeze
                                                                    </a>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                <c:if test="${tripDetails.tripCountStatus == 0}">
    <!--                                                                1&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>-->
                                                                    <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                        Start Trip
                                                                    </a>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.nextTrip != 0 }">
                                                                <c:if test="${tripDetails.tripCountStatus == 0}">
                                                                    2&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId == 0}">
                                                                3&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId != 0}">
                                                                &nbsp;Started
                                                                <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                    &nbsp;<br> <a href="confirmNextTrip.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Next Trip</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.nextTrip == 1 && tripDetails.nextTripCountStatus != 0}">
                                                                    &nbsp;<br>
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->


<!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                   Request Advance
                                                                 </a>-->
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${RoleId == 1023 || RoleId == 1033}">

                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                    </c:if>
                                                                </c:if>


                                                            </c:if>

                                                        </c:if>


                                                        <c:if test="${tripDetails.statusId == 10}">



                                                            <c:if test="${tripDetails.paymentType == 3 }">
                                                                &nbsp;
                                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                            </c:if>

                                                            <c:if test="${tripDetails.paymentType == 4 }">
                                                                &nbsp;
                                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.lastPointId == 617}">
                                                                <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    &nbsp;
    <!--                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />">closure3</a>-->
                                                                    <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />" style="color:#272E39;"   >
                                                                        Trip Closure
                                                                    </a>

                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.lastPointId != 617}">
                                                                <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                                </c:if>
                                                            </c:if>
                                                            &nbsp;

                                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032}">

                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                    </c:if>
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                        <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance4</a>-->
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <!--&nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance2</a>-->
                                                                    </c:if>
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                        <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance3</a>-->
                                                                    </c:if>
                                                                </c:if>

                                                                <c:if test="${tripDetails.tripType != 2}">
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance1</a>-->
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                       Request Advance
                                                                     </a>-->
                                                                </c:if>


                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 18}">

                                                            &nbsp;
                                                            <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>

                                                            &nbsp;
                                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                                <!--<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->
                                                            </c:if>
                                                        </c:if>

                                                        <c:if test="${tripDetails.statusId == 12 ||  tripDetails.statusId == 16  ||  tripDetails.statusId == 32}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />"> Trip Closure1</a>
                                                                &nbsp;
                                                                <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                                    <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                </c:if>

                                                            </c:if>
                                                            <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 0}">
                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">Trip closure2</a>
                                                                &nbsp;
                                                            </c:if>

                                                            <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 1}">
                                                                <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                            </c:if>
                                                            <c:if test="${RoleId != 1023 && RoleId != 1069 && RoleId != 1068 }">
                                                                <a href="#" style="color:#ff6699;"  onclick="printTripGrn('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                                    GR Print
                                                                </a>
                                                            </c:if>
                                                        </c:if>


                                                    </c:if>

                                                </c:if>


                                                <c:if test="${RoleId == '1023' || RoleId == 1069 || RoleId == 1068}">

                                                    <a href="#" style="color:#ff6699;"  onclick="printTripGrn('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                        GR Print
                                                    </a>
                                                    <a href="#" style="color:greenyellow;" onclick="printTripAdva('<c:out value="${tripDetails.tripSheetId}"/>&param=advance')" >
                                                        Advance Print
                                                    </a>
                                                    <a href="#" style="color:#3399ff;" onclick="printTripFul('<c:out value="${tripDetails.tripSheetId}"/>&param=fuel')" >
                                                        Fuel Print
                                                    </a>

                                                </c:if>
                                                <c:if test="${RoleId != '1023' && RoleId != 1069 && RoleId != 1068}">
                                                    <c:if test="${ tripDetails.statusId == 10}">
                                                        <a href="#" style="color:#ff6699;"  onclick="printTripGrn('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                            GR Print
                                                        </a>
                                                        <a href="getExpensePrintDetails.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&param=advance" style="color:greenyellow;"   >
                                                            Advance Print
                                                        </a>
                                                        <a href="getExpensePrintDetails.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&param=fuel" style="color:#3399ff;"   >
                                                            Fuel Print
                                                        </a>
                                                    </c:if>
                                                </c:if>



                                            </c:if>
                                            <c:if test="${RoleId == '1023' || RoleId == 1069 || RoleId == 1068}">
                                                <c:if test="${tripDetails.statusId == 13 }">
                                                    <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>
                                                </c:if>
                                                <c:if test="${tripDetails.statusId >= 8 && tripDetails.statusId <= 14}">
                                                    <c:if test="${RoleId == 1023 || RoleId == 1069 }">
                                                        <a href="handleTripFuel.do?nextTrip=0&tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                            Fuel Update
                                                        </a>
                                                    </c:if>
                                                    <c:if test="${RoleId == 1023 || RoleId == 1069 || RoleId == 1068}">
                                                        <a href="updateGrDetails.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&grNumber=<c:out value="${tripDetails.grNumber}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>" style="color:#272E39;"   >
                                                            GR Update
                                                        </a>
                                                    </c:if>
                                                    <c:if test="${RoleId == '1023' }">
                                                                                                   <!--<a href="handleDeleteTripSheet.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripDetails.tripType}"/>&statusId=<c:out value="${statusId}"/>&tripStatusId=<c:out value="${tripDetails.statusId}"/>" style="color:#272E39;"   >
                                                                                                    Trip Update
                                                                                                   </a>-->
                                                        &nbsp;
                                                    </c:if>
                                                </c:if>
                                            </c:if>
                                            <%--<c:if test="${tripDetails.statusId >= 13}">
                                                                                            &nbsp;
                                                                                             <a href="#" style="color:#ff6699;"  onclick="printTripGrn('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                                                                    GR Print
                                                                                                </a>
                                                                                                <a href="getExpensePrintDetails.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&param=advance" style="color:greenyellow;"   >
                                                                                                    Advance Print
                                                                                                </a>
                                                                                                <a href="getExpensePrintDetails.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&param=fuel" style="color:#3399ff;"   >
                                                                                                    Fuel Print
                                                                                                </a>
                                                                                             </c:if>--%>
                                        </td>



                                    </tr>

                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $(".clsr").click(function() {

                                document.getElementById("spinner").style.display = "block";
                                document.getElementById("loader").style.display = "block";
                                document.getElementById('loader').className += 'ui-loader-background';
                            });
                        });
                        function GenTripGr(tripSheetId) {
                            $.ajax({
                                url: "/throttle/handleTripGRGeneration.do",
                                dataType: "text",
                                data: {
                                    tripId: tripSheetId

                                },
                                success: function(temp) {
                                    // alert(data);
                                    if (temp != '') {
                                        alert("GR Generated Successfully!!");

                                    } else {
                                        alert('Something Went wrong! Please check.');
                                    }
                                }
                            });

                        }
                        function printTripGrn(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId, 'PopupPage', 'height = 500, width = 600, scrollbars = yes, resizable = yes');
                        }
                        function printTripAdva(tripId) {
                            window.open('/throttle/getExpensePrintDetails.do?tripId=' + tripId, 'PopupPage', 'height = 500, width = 600, scrollbars = yes, resizable = yes');
                        }
                        function printTripFul(tripId) {
                            window.open('/throttle/getExpensePrintDetails.do?tripId=' + tripId, 'PopupPage', 'height = 500, width = 600, scrollbars = yes, resizable = yes');
                        }
                        function printTripChallan(tripSheetId) {
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

