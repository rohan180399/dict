
<%--
    Document   : suggestedPinkSlip
    Created on : Dec 6, 2012, 9:36:09 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>
    <script type="text/javascript">


        function fillData(){
            var routeId = document.getElementById("destination").value;
            document.suggestedPS.selectedRouteId.value = routeId;
        }

        function submitPage()
        {
            if(textValidation(document.suggestedPS.selectedRouteId,'RouteName')){
                return;
            }

            var sino = parseInt(document.suggestedPS.sino.value);
            var ids = "";
            var dest = "";
            var destName = "";
            var tonnage = "";
            var ton = "";
            var bags = "";
            var bag = "";
//            var billStatus = 0;
            var val = "";
            var tonRate = "";
            var tonRateMarket = "";
            var billStatus = "";
            var productName = "";
            var packing = "";
            //var statusValue = 0;
            var twoLPS ="";
            var count1;
            var count2;
            var Rid;
            var Rid1="";
            for (var i = 1; i <= sino; i++) {
                if(document.getElementById("selectLPSId"+i).checked){
                    if(ids == ""){
                        ids = document.getElementById("lpsID"+i).value;
                        twoLPS ="N";
                    } else {
                        ids = ids +","+document.getElementById("lpsID"+i).value;
                        twoLPS ="Y";

                    }
                    //Ton start
                    if(ton == ""){
                        ton = document.getElementById("tonnage"+i).value;
                    } else {
                        ton = ton +","+document.getElementById("tonnage"+i).value;
                    }
                    //Bag start
                    if(bag == ""){
                        bag = document.getElementById("bags"+i).value;
                    } else {
                        bag = bag +","+document.getElementById("bags"+i).value;
                    }
                    //tonnageRate start
                    if(tonRate == ""){
                        tonRate = document.getElementById("tonnageRate"+i).value;
                    } else {
                        tonRate = tonRate +","+document.getElementById("tonnageRate"+i).value;
                    }
                    //tonRateMarket start
                    if(tonRateMarket == ""){
                        tonRateMarket = document.getElementById("tonnageRateMarket"+i).value;
                    } else {
                        tonRateMarket = tonRateMarket +","+document.getElementById("tonnageRateMarket"+i).value;
                    }
                    //billStatus start
                    if(billStatus == ""){
                        billStatus = document.getElementById("billStatus"+i).value;
                    } else {
                        billStatus = billStatus +","+document.getElementById("billStatus"+i).value;
                    }

                    //productName start
                    if(productName == ""){
                        productName = document.getElementById("productName"+i).value;
                    } else {
                        productName = productName +","+document.getElementById("productName"+i).value;
                    }

                    //packing start
                    if(packing == ""){
                        packing = document.getElementById("packing"+i).value;
                    } else {
                        packing = packing +","+document.getElementById("packing"+i).value;
                    }

                    //destination start
                    if(dest == ""){
                        Rid1 = document.getElementById("destination"+i).value;
                        Rid = Rid1.split("-");
                        dest = Rid[1];
                        destName = Rid[0];
                    } else {
                        Rid1 = document.getElementById("destination"+i).value;
                        Rid = Rid1.split("-");
                        dest = dest +","+Rid[1];
                        destName = destName +","+Rid[0];
                    }

                    //For tonnage start
                    if(tonnage == ""){
                        tonnage = parseInt(document.getElementById("tonnage"+i).value);
                    }else{
                        tonnage = tonnage + parseInt(document.getElementById("tonnage"+i).value);
                    }
                    //For tonnage end

                    

                    //For Bill Status start
                    if(document.getElementById("billStatus"+i).value =="Paid"){
                        count1 = true;
                        val="Paid";
                    }if(document.getElementById("billStatus"+i).value =="ToPay"){
                        count2 = true;
                        val="ToPay";
                    }
                }
            }
            if(count1 == true && count2 == true)
            {
                val="mix";
                mixAlert()
            }

            if(ids != ""){
                document.getElementById("selectedLpsIds").value  = ids;
                document.getElementById("selecteddestination").value  = dest;
                document.getElementById("selecteddestinationName").value  = destName;
                document.getElementById("selectedTonnage").value  = tonnage;
                document.getElementById("selectedTon").value  = ton;
                document.getElementById("selectedBags").value  = bag;
                document.getElementById("twoLPS").value  = twoLPS;
                document.getElementById("selectedBillStatus").value  = val ;
                document.getElementById("selectedTonRate").value  = tonRate ;
                document.getElementById("selectedTonRateMarket").value  = tonRateMarket ;
                document.getElementById("selectedbillStatus").value  = billStatus ;
                document.getElementById("selectedPacking").value  = packing ;
                document.getElementById("selectedProductName").value  = productName ;
                document.suggestedPS.action="/throttle/savePinkSlip.do";
                document.suggestedPS.submit();
            } else {
                alert("No LPS selected...");
            }
        }
        function mixAlert()
        {
            var strconfirm = confirm("You select both Paid and To Pay.     Are you sure you want to continue");
            if (strconfirm == true){return true;}else{return false}
        }



        function goBack()
        {
            window.history.back()
        }



    </script>

    <%
                String driverId = (String) request.getAttribute("driName");
                String destination = (String) request.getAttribute("destination");
                String routeId = (String) request.getAttribute("routeId");
                String regno = (String) request.getAttribute("regno");
                String ownership = (String) request.getAttribute("ownership");

    %>



    <body >
        <form name="suggestedPS" method="post">
            <table width="870" cellpadding="0" cellspacing="2" align="center" border="0" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr><td><b>Selected Values</b></td></tr>
                <tr>
                    <td style="padding:15px;" align="right">
                        <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                            <tr>
                                <td>Vehicle No</td>
                                <td><input name="regno" id="regno"  value="<%=regno%>" class="textbox" readonly></td>
                                <td>Driver Name</td>
                                <td><input name="driName" id="driName"  value="<%=driverId%>" class="textbox" readonly></td>
                            </tr>
                            <tr>
                                <td><font color="red">*</font>Preferred Route:</td>
                                <td class="text">
                                    <input name="destination" id="destination" type="text" class="textbox" value="<%=destination%>" size="20" onfocus="getDestination();" autocomplete="off" readonly>
                                    <input name="selectedRouteId" id="selectedRouteId" type="hidden" class="textbox" value="" size="20" autocomplete="off">
                                </td>
                                <c:if test="${lorryDetailList != null}">
                                    <c:forEach items="${lorryDetailList}" var="ldl">
                                        <c:set var="ownership" value="${ldl.ownership}" />
                                        <c:set var="vehicleTonnage" value="${ldl.vehicleTonnage}" />
                                        <td>Ownership:</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${ownership == 1}">
                                                    <input type="text" name="ownershipName" id="ownershipName"  value="Own" class="textbox" readonly>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="text" name="ownershipName" id="ownershipName"  value="Attach" class="textbox" readonly>
                                                </c:otherwise>
                                            </c:choose>
                                                <input type="hidden" name="ownership" id="ownership"  value="<c:out value="${ownership}"/>" class="textbox" readonly>
                                            </td>
                                            <td>Total Tonnage:</td>
                                            <td><input name="vehicleTonnage" id="vehicleTonnage"  value="<c:out value="${vehicleTonnage}"/>" class="textbox" readonly></td>
                                        </c:forEach>

                                </c:if>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br/>
            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                <tr>
                    <td valign="top">
                        <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                                <tr><th colspan="6">Order List Based On The LPS</th></tr>
                                <%int index = 0;%>
                                <c:if test="${LPSOrderList != null}">
                                    <tr>
                                        <td class="contenthead">S.No</td>
                                        <td class="contenthead">LPS Number</td>
                                        <td class="contenthead">Tonnage</td>
                                        <td class="contenthead">Bags</td>
                                        <td class="contenthead">Destination</td>
                                        <td class="contenthead">Bill Status</td>
                                        <td class="contenthead">Priority</td>

                                    </tr>

                                    <c:forEach items="${LPSOrderList}" var="LPS">

                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>" height="30"><%=index + 1%></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.lpsNumber}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.quantity}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.bags}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.destination}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.billStatus}"/></td>
                                            <td class="<%=classText%>"><c:out value="${LPS.clplPriority}"/></td>
                                        </tr>
                                        <%index++;%>
                                    </c:forEach>
                                </c:if>
                            </table>
                        </div>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <%
                                    int sino = 0;
                        %>

                        <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                                <tr><th colspan="6">Suggested Orders</th></tr>

                                <%int index1 = 0;%>
                                <%int index2 = 0;%>
                                <%int index3 = 0;%>
                                <tr><th colspan="6">Preferred Route Order List</th></tr>
                                <c:if test="${SuggestedPinkSlip != null}">
                                    <tr>
                                        <!--                                        <td class="contenthead">S.No</td>-->
                                        <td class="contenthead">LPS Number</td>
                                        <td class="contenthead">Tonnage</td>
                                        <td class="contenthead">Bags</td>
                                        <td class="contenthead">Destination</td>
                                        <td class="contenthead">Bill Status</td>
                                        <td class="contenthead">Priority</td>
                                        <td class="contenthead">Select</td>

                                    </tr>

                                    <c:forEach items="${SuggestedPinkSlip}" var="SPS">
                                        <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }

                                                    sino++;
                                        %>

                                        <tr>
<!--                                        <td class="<%=classText1%>" height="30"><%=index1 + 1%></td>-->
                                            <td class="<%=classText1%>"><c:out value="${SPS.lpsNumber}"/></td>
                                            <td class="<%=classText1%>"><c:out value="${SPS.quantity}"/></td>
                                            <td class="<%=classText1%>"><c:out value="${SPS.bags}"/></td>
                                            <td class="<%=classText1%>"><c:out value="${SPS.destination}"/></td>
                                            <td class="<%=classText1%>"><c:out value="${SPS.billStatus}"/></td>
                                            <td class="<%=classText1%>"><c:out value="${SPS.clplPriority}"/></td>
                                            <td class="<%=classText1%>">
                                                <input type="hidden" name="lpsID" id="lpsID<%=sino%>" value="<c:out value="${SPS.lpsID}"/>" />
                                                <input type="hidden" name="destination" id="destination<%=sino%>" value="<c:out value="${SPS.destination}"/>" />
                                                <input type="hidden" name="tonnage" id="tonnage<%=sino%>" value="<c:out value="${SPS.quantity}"/>" />
                                                <input type="hidden" name="bags" id="bags<%=sino%>" value="<c:out value="${SPS.bags}"/>" />
                                                <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${SPS.billStatus}"/>" />
                                                <input type="hidden" name="tonnageRate" id="tonnageRate<%=sino%>" value="<c:out value="${SPS.tonnageRate}"/>" />
                                                <input type="hidden" name="tonnageRateMarket" id="tonnageRateMarket<%=sino%>" value="<c:out value="${SPS.tonnageRateMarket}"/>" />
                                                <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${SPS.billStatus}"/>" />
                                                <input type="hidden" name="productName" id="productName<%=sino%>" value="<c:out value="${SPS.productName}"/>" />
                                                <input type="hidden" name="packing" id="packing<%=sino%>" value="<c:out value="${SPS.packing}"/>" />
                                                <input type="checkbox" name="selectLPSId"  id="selectLPSId<%=sino%>" />
                                            </td>
                                        </tr>
                                        <%index1++;%>
                                    </c:forEach>
                                </c:if>
                                <tr><th colspan="6">High Priority List</th></tr>
                                <c:if test="${SuggestedLPSList != null}">
                                    <tr>
                                        <!--                                        <td class="contenthead">S.No</td>-->
                                        <td class="contenthead">LPS Number</td>
                                        <td class="contenthead">Tonnage</td>
                                        <td class="contenthead">Bags</td>
                                        <td class="contenthead">Destination</td>
                                        <td class="contenthead">Bill Status</td>
                                        <td class="contenthead">Priority</td>
                                        <td class="contenthead">Select</td>

                                    </tr>
                                    <c:forEach items="${SuggestedLPSList}" var="LPS">
                                        <%
                                                    String classText2 = "";
                                                    int oddEven2 = index2 % 2;
                                                    if (oddEven2 > 0) {
                                                        classText2 = "text2";
                                                    } else {
                                                        classText2 = "text1";
                                                    }

                                        %>

                                        <tr>
                                            <c:if test="${LPS.clplPriority == 'High'}">
                                                <%
                                                            sino++;
                                                %>
<!--                                                <td class="<%=classText2%>" height="30"><%=index2 + 1%></td>-->
                                                <td class="<%=classText2%>"><c:out value="${LPS.lpsNumber}"/></td>
                                                <td class="<%=classText2%>"><c:out value="${LPS.quantity}"/></td>
                                                <td class="<%=classText2%>"><c:out value="${LPS.bags}"/></td>
                                                <td class="<%=classText2%>"><c:out value="${LPS.destination}"/></td>
                                                <td class="<%=classText2%>"><c:out value="${LPS.billStatus}"/></td>
                                                <td class="<%=classText2%>"><c:out value="${LPS.clplPriority}"/></td>
                                                <td class="<%=classText2%>">
                                                    <input type="hidden" name="lpsID" id="lpsID<%=sino%>" value="<c:out value="${LPS.lpsID}"/>" />
                                                    <input type="hidden" name="destination" id="destination<%=sino%>" value="<c:out value="${LPS.destination}"/>" />
                                                    <input type="hidden" name="tonnage" id="tonnage<%=sino%>" value="<c:out value="${LPS.quantity}"/>" />
                                                    <input type="hidden" name="bags" id="bags<%=sino%>" value="<c:out value="${LPS.bags}"/>" />
                                                    <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${LPS.billStatus}"/>" />
                                                    <input type="hidden" name="tonnageRate" id="tonnageRate<%=sino%>" value="<c:out value="${LPS.tonnageRate}"/>" />
                                                    <input type="hidden" name="tonnageRateMarket" id="tonnageRateMarket<%=sino%>" value="<c:out value="${LPS.tonnageRateMarket}"/>" />
                                                    <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${LPS.billStatus}"/>" />
                                                    <input type="hidden" name="productName" id="productName<%=sino%>" value="<c:out value="${LPS.productName}"/>" />
                                                    <input type="hidden" name="packing" id="packing<%=sino%>" value="<c:out value="${LPS.packing}"/>" />
                                                    <input type="checkbox" name="selectLPSId"  id="selectLPSId<%=sino%>" />
                                                </td>
                                            </c:if>
                                        </tr>
                                        <%index2++;%>
                                    </c:forEach>
                                </c:if>
                                <tr><th colspan="6">Next LPS inline</th></tr>
                                <c:if test="${SuggestedLPSList != null}">
                                    <tr>
                                        <!--                                        <td class="contenthead">S.No</td>-->
                                        <td class="contenthead">LPS Number</td>
                                        <td class="contenthead">Tonnage</td>
                                        <td class="contenthead">Bags</td>
                                        <td class="contenthead">Destination</td>
                                        <td class="contenthead">Bill Status</td>
                                        <td class="contenthead">Priority</td>
                                        <td class="contenthead">Select</td>

                                    </tr>
                                    <c:forEach items="${SuggestedLPSList}" var="LPS">
                                        <%
                                                    String classText3 = "";
                                                    int oddEven3 = index3 % 2;
                                                    if (oddEven3 > 0) {
                                                        classText3 = "text2";
                                                    } else {
                                                        classText3 = "text1";
                                                    }

                                        %>

                                        <tr>
                                            <c:if test="${LPS.clplPriority != 'High'}">
                                                <%
                                                            sino++;
                                                %>
<!--                                                <td class="<%=classText3%>" height="30"><%=index3 + 1%></td>-->
                                                <td class="<%=classText3%>"><c:out value="${LPS.lpsNumber}"/></td>
                                                <td class="<%=classText3%>"><c:out value="${LPS.quantity}"/></td>
                                                <td class="<%=classText3%>"><c:out value="${LPS.bags}"/></td>
                                                <td class="<%=classText3%>"><c:out value="${LPS.destination}"/></td>
                                                <td class="<%=classText3%>"><c:out value="${LPS.billStatus}"/></td>
                                                <td class="<%=classText3%>"><c:out value="${LPS.clplPriority}"/></td>
                                                <td class="<%=classText3%>">
                                                    <input type="hidden" name="lpsID" id="lpsID<%=sino%>" value="<c:out value="${LPS.lpsID}"/>" />
                                                    <input type="hidden" name="destination" id="destination<%=sino%>" value="<c:out value="${LPS.destination}"/>" />
                                                    <input type="hidden" name="tonnage" id="tonnage<%=sino%>" value="<c:out value="${LPS.quantity}"/>" />
                                                    <input type="hidden" name="bags" id="bags<%=sino%>" value="<c:out value="${LPS.bags}"/>" />
                                                    <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${LPS.billStatus}"/>" />
                                                    <input type="hidden" name="tonnageRate" id="tonnageRate<%=sino%>" value="<c:out value="${LPS.tonnageRate}"/>" />
                                                    <input type="hidden" name="tonnageRateMarket" id="tonnageRateMarket<%=sino%>" value="<c:out value="${LPS.tonnageRateMarket}"/>" />
                                                    <input type="hidden" name="billStatus" id="billStatus<%=sino%>" value="<c:out value="${LPS.billStatus}"/>" />
                                                    <input type="hidden" name="productName" id="productName<%=sino%>" value="<c:out value="${LPS.productName}"/>" />
                                                    <input type="hidden" name="packing" id="packing<%=sino%>" value="<c:out value="${LPS.packing}"/>" />
                                                    <input type="checkbox" name="selectLPSId"  id="selectLPSId<%=sino%>" />
                                                </td>
                                            </c:if>
                                        </tr>
                                        <%index3++;%>
                                    </c:forEach>
                                </c:if>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>

            <input type="hidden" name="sino" id="sino" value="<%=sino%>" />
            <input type="hidden" name="selectedLpsIds" id="selectedLpsIds" value="" />
            <input type="hidden" name="selecteddestination" id="selecteddestination" value="" />
            <input type="hidden" name="selecteddestinationName" id="selecteddestinationName" value="" />
            <input type="hidden" name="selectedTonnage" id="selectedTonnage" value="" />
            <input type="hidden" name="selectedBillStatus" id="selectedBillStatus" value="" />
            <input type="hidden" name="selectedTon" id="selectedTon" value="" />
            <input type="hidden" name="selectedBags" id="selectedBags" value="" />
            <input type="hidden" name="twoLPS" id="twoLPS" value="" />
            <input type="hidden" name="selectedTonRate" id="selectedTonRate" value="" />
            <input type="hidden" name="selectedTonRateMarket" id="selectedTonRateMarket" value="" />
            <input type="hidden" name="selectedbillStatus" id="selectedbillStatus" value="" />
            <input type="hidden" name="selectedPacking" id="selectedPacking" value="" />
            <input type="hidden" name="selectedProductName" id="selectedProductName" value="" />
            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                <tr>
                    <td align="right"><input type="button" class="button"  value="Back" onclick="goBack()"></td>
                    <td>&nbsp;</td>
                    <td align="left"><input type="button" class="button"  onclick="fillData();submitPage();" value="Generate Pink Slip" ></td>
                </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <script type="text/javaScript">
            function getDestination(){
                var oTextbox = new AutoSuggestControl(document.getElementById("destination"),new ListSuggestions("destination","/throttle/handleToDestination.do?"));
            }
        </script>
    </body>
</html>

