<%-- 
    Document   : secondaryTripSchedule
    Created on : Feb 8, 2014, 2:37:50 PM
    Author     : madhan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
  <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });

            });
        </script>   
    </head>
    <script language="javascript">
        function submitPage(value) {
            if (value == 'nextWeek') {
                alert("nextWeek");
                document.viewSecondaryTrip.action = '/throttle/secondaryTripSheduleNext.do';
                document.viewSecondaryTrip.submit();
            } else {
                var checkedValue = document.getElementsByName('checkedValue');
                var secRouteIds = document.getElementsByName('secRouteId');
                var cntr = 0;
                var cntr1 = 0;
                var temp = 0;
                var temp1 = 0;
                var statusTrue = "";
                var statusFalse = "";
                alert(checkedValue.length);
                for (var i = 0; i < checkedValue.length; i++) {
                    if (checkedValue[i].checked == true) {
                        statusTrue = checkedValue[i].value + "~Y";
                        if (cntr == 0) {
                            temp = statusTrue;
                            document.getElementById("scheduleDatesTrue").value = statusTrue;
                        } else {
                            temp = temp + "," + statusTrue;
                            document.getElementById("scheduleDatesTrue").value = temp;


                        }
                        cntr++;
                    } else {
                        statusFalse = checkedValue[i].value + "~N";
                        if (cntr1 == 0) {
                            temp1 = statusFalse;
                            document.getElementById("scheduleDatesFalse").value = statusFalse;
                        } else {
                            temp1 = temp1 + "," + statusFalse;
                            document.getElementById("scheduleDatesFalse").value = temp1;
                        }
                        cntr1++;
                    }
                }
                var passedValue = document.getElementById("scheduleDatesTrue").value + "," + document.getElementById("scheduleDatesFalse").value;
                document.viewSecondaryTrip.action = '/throttle/saveSecondaryTripSchedule.do?passedValue=' + passedValue;
                document.viewSecondaryTrip.submit();
            }
        }

    </script>
    <body>
        <form name="viewSecondaryTrip" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">

                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                    <tr>
                        <td height="30">Customer Code</td>
                        <td><c:out value="${customerCode}"/></td>
                        <td height="30">Customer Name</td>
                        <td><input name="customerId" id="customerId" type="hidden" class="textbox" vallue="<c:out value="${customerId}"/>"><c:out value="${customerName}"/></td>
                    </tr>
                    <tr>
                        <td height="30">Schedule Start Date</td>
                        <td><input name="fromDate" id="fromDate" type="hidden" class="textbox"  vallue="<c:out value="${fromDate}"/>"><c:out value="${fromDate}"/></td>
                        <td height="30">Schedule End Date</td>
                        <td><input name="toDate" id="toDate" type="hidden" class="textbox" vallue="<c:out value="${toDate}"/>"><c:out value="${toDate}"/></td>
                    </tr>
                </table>
                </div>
                </div>
                </td>
                </tr>
            </table>
            <br>
<!--            <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                <tr>
                    <td height="30">Create Trip For Previous Date</td>
                    <td><input type="text" class="datepicker" name="dateWiseTripSchedule" id="dateWiseTripSchedule"/></td>
                    <td><input type="button" class="button" value="Search" name="viewDateWiseTrip" id="viewDateWiseTrip" onclick="submitPage();"/></td>
                </tr>
            </table>-->
            <br>

            <%--<c:if test = "${ds != null}" >--%>
            <table width="100%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                <th><h3>Vehicle No</h3></th>
                <%int a = 0;%>
                <c:if test = "${dateList != null}" >
                    <c:forEach items="${dateList}" var="customer" begin="0" end="6">
                        <%a++;%>
                        <!--<input type="hidden" name="scheduledate<%=a%>" id="scheduledate<%=a%>" value="<c:out value="${customer.dateName}" />"/>-->
                        <input type="hidden" name="scheduledate" id="scheduledate" value="<c:out value="${customer.dateName}" />"/>
                        <th><h3><c:out value="${customer.dateName}"/></h3></th>
                        </c:forEach>
                    </c:if>   
                </tr> 
                </thead>
                <tbody>
                    <% int index = 0,sno = 1;%>
                    <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                    %>
                    <c:if test = "${SecondaryContractRouteList != null}" >
                        <c:forEach items="${SecondaryContractRouteList}" var="scrl">
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><input type="hidden" name="secRouteId" id="secRouteId" value="<c:out value="${scrl.secondaryRouteId}"/>" /><c:out value="${scrl.secondaryRouteName}"/></td>
                                    <c:forEach items="${dateList}" var="customer">
                                        <c:if test="${customer.secondaryRouteId == scrl.secondaryRouteId}">
                                        <td align="left" class="text2">
                                            <c:if test="${customer.scheduleStatus == 'Y'}">
                                                <a href="/throttle/allotSecondaryVehicleType.do?scheduleDate=<c:out value="${customer.date}"/>&customerId=<c:out value="${customerId}"/>&secondaryRouteId=<c:out value="${customer.secondaryRouteId}"/>&customerName=<c:out value="${customerName}"/>" >Create Trip</a>
                                            </c:if>
                                            <c:if test="${customer.scheduleStatus == 'N'}">
                                                -
                                            </c:if>
                                            <c:if test="${customer.scheduleStatus == 'C'}">
                                                Trip Created
                                            </c:if>
                                        </td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <table width="100%" align="center" border="0" id="table">
                <tr>
                    <td colspan="4" align="center">
                        <input type="hidden" name="scheduleDatesTrue" id="scheduleDatesTrue" value=""/>
                        <input type="hidden" name="scheduleDatesFalse" id="scheduleDatesFalse" value=""/>
                    </td>
                </tr>
<!--                <tr>
                    <td>scheduled Date</td>
                    <td><input type="text" class="datepicker" name="scheduleDate" id="scheduleDate"/></td>
                    <td><input type="button" class="button" value="Search" name="viewDateWiseTrip" id="viewDateWiseTrip" onclick="submitPage();"/></td>
                </tr>-->
                <script>
                    function submitPage(){
                        if($("#scheduleDate").val() == ""){
                            alert("please select date");
                            $("#scheduleDate").focus();
                        }else{
                            document.viewSecondaryTrip.action = '/throttle/viewDateWiseSecondaryTripSchedule.do?scheduleDate=' + $("#scheduleDate").val();
                            document.viewSecondaryTrip.submit(); 
                        }
                    }
                </script>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


</html>