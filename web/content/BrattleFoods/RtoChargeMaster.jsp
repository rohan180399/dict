
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript">

        function submitPage(){
            var errStr = "";
            if (document.getElementById("rtoName").value == "0") {
                errStr = "Please enter RTO name.\n";
                alert(errStr);
                document.getElementById("rtoName").focus();
            }
            else if (document.getElementById("vehicleCount").value == "")
            {
                errStr = "Please enter Total vehicle .\n";
                alert(errStr);
                document.getElementById("vehicleCount").focus();
            }
            else if (document.getElementById("amounts").value == "")
            {
                errStr = "Please enter amounts\n";
                alert(errStr);
                document.getElementById("amounts").focus();
            }
            else if (document.getElementById("fromDate").value == "")
            {
                errStr = "Please select from date\n";
                alert(errStr);
                document.getElementById("fromDate").focus();
            } else if (document.getElementById("toDate").value == "")
            {
                errStr = "Please  To date\n";
                alert(errStr);
                document.getElementById("toDate").focus();
            }

            if (errStr == "") {
                document.rtocharge.action = " /throttle/saveRtoCharges.do";
                document.rtocharge.method = "post";
                document.rtocharge.submit();
            }
        }
        
        $(document).ready(function () {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function () {
         $(".datepicker").datepicker({         
                yearRange: '1900:' + new Date().getFullYear(),
                changeMonth: true, changeYear: true
            });
        });

         function getRtoVehicleCount(rtoId){
         $.ajax({
                url: '/throttle/getRtoVehicleCount.do',
                data: {rtoOfficeId:rtoId},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {                       
                       var rtoId=data.vehicleCount;
                       document.getElementById("vehicleCount").value=rtoId;
                    });
                }
            });
             
         }
         function setRtoChargeMaster(rtoVehicleId,rtoChargeId,vehicleCount,amount,status,fromDate,toDate,checkId){
         if(document.getElementById(checkId).checked==true){
         document.getElementById("rtoChargeId").value=rtoChargeId;
         document.getElementById("rtoName").value=rtoVehicleId;
         document.getElementById("vehicleCount").value=vehicleCount;
         document.getElementById("amounts").value=amount;
         document.getElementById("status").value=status;
         document.getElementById("fromDate").value=fromDate;
         document.getElementById("toDate").value=toDate;
         }else{
         document.getElementById("rtoChargeId").value="";
         document.getElementById("rtoName").value="0";
         document.getElementById("vehicleCount").value="0";
         document.getElementById("amounts").value="";
         document.getElementById("status").value="Y";
         document.getElementById("fromDate").value="";
         document.getElementById("toDate").value="";
         }
         }
    </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">RTO Charge Master</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="rtocharge"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">RTO Charge Master</td></tr>
                    <tr>
                        <td >&nbsp;&nbsp;<font color="red">*</font>Rto Name</td>
                        <td >
                            <select id="rtoName" name="rtoName"  class="form-control" style="width:240px;height:40px;"  autocomplete="off" onchange="getRtoVehicleCount(this.value)">
                                <option value="0" selected>---Select---</option>
                                <c:if test = "${rtonamelist != null}">
                                    <c:forEach items="${rtonamelist}" var="cc">
                                        <option value="<c:out value="${cc.id}" />"><c:out value="${cc.rtoName}" /></option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </td>
                        <td >&nbsp;&nbsp;<font color="red">*</font>No of Vehicle :</td>

                        <td ><input type="text" name="vehicleCount" id="vehicleCount" readonly class="form-control" style="width:240px;height:40px;" autocomplete="off"/></td>
                    </tr>

                    <tr>
                        <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>

                        <td >
                            <select  id="desatus" name="destatus" class="form-control" style="width:240px;height:40px;"  autocomplete="off"   >
            <!--<select id="status" name="status"  class="form-control" style="width:240px;height:40px;"  autocomplete="off" onchange="getRtoVehicleCount(this.value)">-->                    
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive"  >In-Active</option>
                            </select>
                        </td>
                        <td>&nbsp;&nbsp;<font color="red">*</font>Amount : &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td><input type="text" id="amounts" name="amounts" class="form-control" style="width:240px;height:40px;"></td>
                    </tr>

                    <tr>
                        <td >&nbsp;&nbsp;<font color="red">*</font>From Date&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td ><input type="text" name="fromDate" id="fromDate" class="datepicker , form-control" style="width:240px;height:40px;"/></td>

                        <td >&nbsp;&nbsp;<font color="red">*</font>To Date &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td ><input type="text" name="toDate" id="toDate" class="datepicker ,form-control" style="width:240px;height:40px;" /></td>
                    </tr>
                </table>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
               
            <h2 align="center">Vehicle charges list </h2>
            <table  id="table" class="table table-info mb30" style="width:1000px;" >
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                        <th>S.No</th>
                        <th width="120">Rto Name</th>
                        <th width="150">Total Vehicles</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th width="150">valid From</th>
                        <th width="150">valid To</th>
                        <th width="150">Edit</th>
                    </tr>
                </thead>
                <tbody>

                    <% int sno = 0;%>
                    <c:if test = "${chargesmaster != null}">
                        <c:forEach items="${chargesmaster}" var="pc">
                            <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="center"> <%= sno%>                                    
                                </td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.rtoName}" /></td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.vehicleNo}" /></td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.amounts}" /></td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.status}" /></td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.fromdate}"/></td>
                                <td class="<%=className%>"  align="center"> <c:out value="${pc.todate}"/></td>
                                <td class="<%=className%>"  align="center"> <input type="checkbox" name="checkRtoCherge" id="checkRtoCherge<%= sno%>" onclick="setRtoChargeMaster('<c:out value="${pc.rtoVehicleId}" />','<c:out value="${pc.rtoChargeId}" />','<c:out value="${pc.vehicleNo}" />','<c:out value="${pc.amounts}" />','<c:out value="${pc.status}" />','<c:out value="${pc.fromdate}" />','<c:out value="${pc.todate}" />',this.id)" </td>
                            </tr>

                        </c:forEach>
                    </tbody>
                </c:if>
            </table>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <input type="hidden" name="rtoChargeId" id="rtoChargeId" value=""/>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form> 
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>