
<%-- 
    Document   : DailyCashReport
    Created on : Apr 28, 2016, 6:39:19 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleViewDailyCashReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleViewDailyCashReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }


        </script>
        <%
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String fDate = "";
            String tDate = "";
            if (request.getAttribute("fromDate") != null) {
                fDate = (String) request.getAttribute("fromDate");
            } else {
                fDate = dateFormat.format(date);
            }
            if (request.getAttribute("toDate") != null) {
                tDate = (String) request.getAttribute("toDate");
            } else {
                tDate = dateFormat.format(date);
            }
        %>
    </head>
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Daily Cash Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<%=fDate%>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<%=tDate%>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <c:if test = "${dailyCashDetails != null }" >
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">S.No</td>
                            <td rowspan="2" class="contenthead">Date</td>
                            <td rowspan="2" class="contenthead">G.R No.</td>
                            <td rowspan="2" class="contenthead">Vehicle No</td>
                            <td rowspan="2" class="contenthead">Transporter</td>
                            <td rowspan="2" class="contenthead">Billing Party</td>
                            <td rowspan="2" class="contenthead">Route</td>
                            <td rowspan="2" class="contenthead">Diesel ltrs</td>
                            <td rowspan="2" class="contenthead">Cash Dr</td>
                            <td rowspan="2" class="contenthead">Cash Received</td>
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${dailyCashDetails}" var="csList">
                        <tr>
                            <td align="center"><%=sno%></td>
                            <td align="center"><c:out value="${csList.paidDate}"/></td>
                            <td align="center"><c:out value="${csList.grNo}"/></td>
                            <td align="center"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center"><c:out value="${csList.transporter}"/></td>
                            <td align="left"><c:out value="${csList.customerName}"/></td>
                            <td align="center"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center"><c:out value="${csList.liters}"/></td>
                            <td align="center"><c:out value="${csList.paidCash}"/></td>
                            <td align="center"><c:out value=""/></td>
                        </tr>
                        <%
                   index++;
                     sno++;
                        %>
                        <c:set var="cashReceived" value="${cashReceived}"></c:set>
                        <c:set var="cashDr" value="${cashDr + csList.paidCash}"></c:set>
                        <c:set var="cashInHand" value="${cashReceived - cashDr }"></c:set>
                    </c:forEach>
                </table>
                <br>
                <br>
                <table align="center" width="500" cellpadding="5" cellspacing="0">
                    <tr>
                        <td  class="contenthead">Cash Received</td>
                        <td class="contenthead">Cash Dr</td>
                        <td class="contenthead">Cash In-Hand</td>
                    </tr>
                    <tr>
                        <td align="center" ><c:out value="${cashReceived}"/></td>
                        <td align="center" ><c:out value="${cashDr}"/></td>
                        <td align="center" ><c:out value="${cashInHand}"/></td>
                    </tr>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>

