/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.tyres.web;

/**
 *
 * @author vinoth
 */
import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.tyres.business.TyresBP;
import ets.domain.tyres.business.TyresTO;

import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.vehicle.business.VehicleTO;
import java.io.*;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TyresController extends BaseController {

    TyresCommand tyrescommand;
    TyresBP tyresBP;
    LoginBP loginBP;
    VehicleBP vehicleBP;

    public TyresBP getTyresBP() {
        return tyresBP;
    }

    public void setTyresBP(TyresBP tyresBP) {
        this.tyresBP = tyresBP;
    }

    public TyresCommand getTyresCommand() {
        return tyrescommand;
    }

    public void setTyresCommand(TyresCommand tyrescommand) {
        this.tyrescommand = tyrescommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

    public ModelAndView handleVehicleList(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList vehiclesList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList vehicleDetails = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        menuPath = "Tyres >> View Vehicle List ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                path = "content/tyres/ViewTyreRotationList.jsp";
                vehiclesList = tyresBP.getVehiclesList();
                mfrList = tyresBP.getMfrList();
                vehicleDetails = tyresBP.getVehicleDetails(tyreTo);
                request.setAttribute("mfrList", mfrList);
                request.setAttribute("vehiclesList", vehiclesList);
                request.setAttribute("vehicleDetails", vehicleDetails);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTyreRotation(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList vehiclesList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList vehicleDetails = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        menuPath = "Tyres >> Tyre Rotation ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String vehicleId = request.getParameter("vehicleId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/tyres/TyresRotation.jsp";
                vehicleDetails = tyresBP.getVehiclesList();
                /*mfrList = tyresBP.getMftList();
                vehicleDetails = tyresBP.getVehicleDetails(tyreTo);
                request.setAttribute("mfrList", mfrList);
                request.setAttribute("vehiclesList", vehiclesList);
                request.setAttribute("vehicleDetails", vehicleDetails);*/
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getTyresReportByTyre(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList vehiclesList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList tyresDetails = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        menuPath = "Tyres >> Tyre Rotation ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String vehicleId = request.getParameter("vehicleId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/tyres/TyresReportByTyreNo.jsp";
                tyreTo.setTyreNumber(tyrescommand.getTyreNumber());
                tyreTo.setFromDate(tyrescommand.getFromDate());
                tyreTo.setToDate(tyrescommand.getToDate());

                tyresDetails = tyresBP.getTyresReportByTyre(tyreTo);
                System.out.println("list size in controller-->" + tyresDetails);

                request.setAttribute("tyresDetails", tyresDetails);
                request.setAttribute("tyreNumber", tyrescommand.getTyreNumber());
                request.setAttribute("fromDate", tyrescommand.getFromDate());
                request.setAttribute("toDate", tyrescommand.getToDate());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getTyreNoSuggestions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String tyreNo = request.getParameter("tyreNumber");
        System.out.println("tyreNo" + tyreNo);

        String suggestions = tyresBP.processTyreNoSuggests(tyreNo);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public ModelAndView getTyresReportByVehicle(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList vehiclesList = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList tyresDetails = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        menuPath = "Tyres >> Tyre Rotation ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String vehicleId = request.getParameter("vehicleId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/tyres/TyresReportByVehicleNo.jsp";
                System.out.println("tyrescommand.getVehicleRegNo()-->" + tyrescommand.getRegno());
                tyreTo.setVehicleRegNo(tyrescommand.getRegno());
                tyreTo.setFromDate(tyrescommand.getFromDate());
                tyreTo.setToDate(tyrescommand.getToDate());
                tyresDetails = tyresBP.getTyresReportByVehicle(tyreTo);
                request.setAttribute("tyresDetails", tyresDetails);
                request.setAttribute("fromDate", tyrescommand.getFromDate());
                request.setAttribute("toDate", tyrescommand.getToDate());
                request.setAttribute("regno", tyrescommand.getRegno());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //--------Tyres Rotations--------------------------
    public ModelAndView handleTyresRotationsList(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList mfrList = new ArrayList();
        ArrayList tyresRotationList = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        menuPath = "Tyres >> Tyre Rotation ";
        System.out.println("tyres rotation called");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/tyres/TyresRotationList.jsp";
                String regNo = request.getParameter("regNo");
                String mfrId = request.getParameter("mfrId");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");

                tyreTo.setVehicleRegNo(regNo);
                tyreTo.setMfrId(mfrId);
                tyreTo.setFromDate(fromDate);
                tyreTo.setToDate(toDate);

                mfrList = tyresBP.getMfrList();
                request.setAttribute("mfrList", mfrList);

                tyresRotationList = tyresBP.getVehicleDetailsList(tyreTo);
                request.setAttribute("tyresRotationList", tyresRotationList);

                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);
                request.setAttribute("regNo", regNo);
                request.setAttribute("mfrId", mfrId);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //--------Tyres Rotations--------------------------
    public ModelAndView handleTyresRotations(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
	        if (request.getSession().isNew()) {
	            return new ModelAndView("content/login.jsp");
	        }
	        String path = "";
	        TyresTO tyreTo = new TyresTO();
	        HttpSession session = request.getSession();
	        ArrayList tyresList = new ArrayList();
	        ArrayList vehicleList = new ArrayList();
	        ArrayList currentTyrePosition = new ArrayList();
	        ArrayList tyresRotationList = new ArrayList();
	        tyrescommand = command;
	        String menuPath = "";
	        menuPath = "Tyres >> Tyre Rotation ";
	        System.out.println("tyres rotation called");
	        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
	        try {
	            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
	            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
	                path = "content/common/NotAuthorized.jsp";
	            } else {
	                String pageTitle = "View Tyre Rotation";
	                request.setAttribute("pageTitle", pageTitle);
	                path = "content/tyres/TyresRotation.jsp";
	                String vehicleId = request.getParameter("vehicleId");
	                String axleTypeId = request.getParameter("axleTypeId");
	                request.setAttribute("axleTypeId", axleTypeId);
	                tyreTo.setVehicleId(vehicleId);
	                Date today = new Date();
	                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	                String startDate = sdf.format(today);
	                request.setAttribute("startDate", startDate);

	                vehicleList = tyresBP.getVehicleDetailsList(tyreTo);
	                request.setAttribute("vehicleList", vehicleList);

	                tyresRotationList = tyresBP.getTyreRotation(tyreTo);
	                request.setAttribute("tyresRotationList", tyresRotationList);

	                tyresList = tyresBP.getTyresList(tyreTo);
	                request.setAttribute("tyresList", tyresList);
	                request.setAttribute("vehicleId", vehicleId);
	                ArrayList tyrePossitionList = new ArrayList();
	                tyrePossitionList = tyresBP.getVehicleTyresNoList(tyreTo);
	                request.setAttribute("tyrePossitionList", tyrePossitionList);
	                int tyreRotationCount = 0;
	                tyreRotationCount = tyresBP.getVehicleTyresRotationCount(tyreTo);
	                request.setAttribute("tyreRotationCount", tyreRotationCount);
	                VehicleTO vehicleTO = new VehicleTO();
	                vehicleTO.setVehicleId(vehicleId);
	                ArrayList oemList = new ArrayList();
	                oemList = vehicleBP.getOemList(vehicleTO);
	                System.out.println("oemListcontrol" + oemList);
	                request.setAttribute("oemListSize", oemList.size());
	                request.setAttribute("oemList", oemList);
	                ArrayList tyreItemList = new ArrayList();
	                tyreItemList = vehicleBP.processTyreItems();
	                request.setAttribute("tyreItemList", tyreItemList);
	                System.out.println("size of tyrelist===" + tyreItemList.size());
	            }
	        } catch (FPRuntimeException exception) {
	            /*
	             * run time exception has occurred. Directed to error page.
	             */
	            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
	            return new ModelAndView("content/common/error.jsp");
	        } catch (Exception exception) {
	            exception.printStackTrace();
	            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
	            return new ModelAndView("content/common/error.jsp");
	        }
	        return new ModelAndView(path);
    }


    public ModelAndView saveTyresRotations(HttpServletRequest request, HttpServletResponse response, TyresCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";

        TyresTO tyreTo = new TyresTO();
        HttpSession session = request.getSession();
        ArrayList tyresList = new ArrayList();
        ArrayList vehicleList = new ArrayList();
        ArrayList currentTyrePosition = new ArrayList();
        ArrayList tyresRotationList = new ArrayList();
        tyrescommand = command;
        String menuPath = "";
        int userId = (Integer) session.getAttribute("userId");
        menuPath = "Tyres >> Tyre Rotation ";
        System.out.println("tyres rotation called");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Tyre Rotation";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/tyres/TyresRotation.jsp";
                String vehicleId = request.getParameter("vehicleId");
                String rotationNo = request.getParameter("rotationNo");
                String rotationDate = request.getParameter("rotationDate");
                String runKm = request.getParameter("runKm");
                String remarks = request.getParameter("remarks");
                tyreTo.setVehicleId(vehicleId);
                tyreTo.setTyreRotationNo(rotationNo);
                tyreTo.setTyreRotationDate(rotationDate);
                tyreTo.setTyreRotationKm(runKm);
                tyreTo.setTyreRotationRemarks(remarks);

                int rotationId = tyresBP.saveTyreRotation(tyreTo);
                int status = 0;
                System.out.println("rotationId  in controller " + rotationId);
                String tyreId[] = request.getParameterValues("tyreId");
                String positionId[] = request.getParameterValues("positionId");
                if (tyreId != null) {
                    for (int i = 0; i < tyreId.length; i++) {
                        status = tyresBP.saveTyreRotationPosition(rotationId, vehicleId, tyreId[i], positionId[i], userId);
                    }
                }

                request.setAttribute("vehicleId", vehicleId);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public void updateTyreRotation(HttpServletRequest request, HttpServletResponse response, TyresCommand command) throws IOException, FPRuntimeException, FPBusinessException {
	        tyrescommand = command;
	        HttpSession session = request.getSession();
	        String path = "";
	        int userId = (Integer) session.getAttribute("userId");
	        int companyType = (Integer) session.getAttribute("companyTypeId");
	        String companyId = (String) session.getAttribute("companyId");
	        System.out.println("in controller");
	        TyresTO tyreTO = new TyresTO();
	        try {
	            String[] positionData = request.getParameterValues("positionData[]");
	            String[] stepneyData = request.getParameterValues("stepneyData[]");
	            String vehicleId = request.getParameter("vehicleId");
	            String rotationNo = request.getParameter("rotationNo");
	            String rotationDate = request.getParameter("rotationDate");
	            String runKm = request.getParameter("runKm");
	            String remarks = request.getParameter("remarks");
	            int insertStatus = 0;
	            tyreTO.setVehicleId(vehicleId);
	            tyreTO.setPositionData(positionData);
	            tyreTO.setTyreRotationNo(rotationNo);
	            tyreTO.setTyreRotationDate(rotationDate);
	            tyreTO.setTyreRotationKm(runKm);
	            tyreTO.setTyreRotationRemarks(remarks);
	            tyreTO.setStepneyData(stepneyData);
	            insertStatus = tyresBP.updateTyreRotation(tyreTO, userId);
	            System.out.println("insertStatus" + insertStatus);
	            response.setContentType("text/css");
	            if (insertStatus > 0) {
	                int tyreRotationCount = 0;
	                tyreRotationCount = tyresBP.getVehicleTyresRotationCount(tyreTO);
	                response.getWriter().println(tyreRotationCount);
	            } else {
	                response.getWriter().println(0);
	            }
	        } catch (UnknownHostException ex) {
	            ex.printStackTrace();
	        }
    }

}
