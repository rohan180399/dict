
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>    
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>      
    </head>
    <script language="javascript">
 function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
        function submitPage(){
           var chek=validation();
           if(chek=='true'){
            document.tyreReport.action='/throttle/tyrePoWoRpt.do';
            document.tyreReport.submit();        
        }
        }
          function validation(){
              
             if(textValidation(document.tyreReport.fromDate,'From Date')){
                return 'false';
            }            
            else  if(textValidation(document.tyreReport.toDate,'TO Date')){
                return 'false';
            }            
            return 'true';
        }
      function setValues(){
        if('<%=request.getAttribute("fromDate")%>'!='null'){                            
                document.tyreReport.fromDate.value='<%=request.getAttribute("fromDate")%>';                
                document.tyreReport.toDate.value='<%=request.getAttribute("toDate")%>';                
            }
            if('<%=request.getAttribute("tyreNo")%>' != 'null'){
             document.tyreReport.tyreNo.value='<%=request.getAttribute("tyreNo")%>';   
             }
            if('<%=request.getAttribute("status")%>' != 'null'){
             document.tyreReport.status.value='<%=request.getAttribute("status")%>';   
             }
            }
    </script>
    <body onload="setValues();">
        <form name="tyreReport">
              <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>


<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Tyres PO-WO Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
        <td height="30">Order Type </td>
        <td height="30"><select name="status" class="form-control" style="width:125px">
                <option value="0">-select-</option>
                <option value="N">PO</option>
                <option value="Y">WO</option>
        </select></td>
        <td  height="30">Tyre No </td>
        <td  height="30"><input name="tyreNo" type="text" class="form-control" value="" size="20">
        <td rowspan="2" valign="middle"><input type="button" name="" value="Fetch Data" class="button" onclick="submitPage();"></td>
    </tr>
    <tr>
        <td  height="30"><font color="red">*</font>From Date </td>
        <td  height="30"><input name="fromDate" type="text" class="form-control" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tyreReport.fromDate,'dd-mm-yyyy',this)"/> </td>
        <td  height="30"><font color="red">*</font>To Date </td>
        <td  height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tyreReport.toDate,'dd-mm-yyyy',this)"/> </td>
        <td  height="30">&nbsp;</td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

<br>  
            <c:if test = "${TyreList != null}" >
            
                <table width="915" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td  class="contentsub">Date</td>
                        <td  height="30" class="contentsub">PO/WO</td>                        
                        <td  height="30" class="contentsub">Invoice No</td>
                        <td  class="contentsub">Vehicle No </td>    
                        <td  height="30" class="contentsub">UOM</td>
                        <td  height="30" class="contentsub">Tyre No </td>
                        <td  height="30" class="contentsub">Pos</td>
                        <td  class="contentsub">Tyre Amount</td>
                        
                    </tr>
                     <c:set var="Amount" value="0" />
                    <% int index = 0;%>
                    <c:forEach items="${TyreList}" var="tyre"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>"><c:out value="${tyre.createdDate}" /></td>
                           <c:choose>
                                <c:when test="${tyre.status=='N'}">
                                    <td class="<%=classText %>" height="30">WO-<c:out value="${tyre.poId}" /></td>
                                    </c:when>
                                   <c:otherwise>
                                        <td class="<%=classText %>" height="30">PO-<c:out value="${tyre.poId}" /></td>
                                    </c:otherwise>                               
                            </c:choose>
                            <td class="<%=classText %>" height="30"><c:out value="${tyre.invoiceId}" /></td>
                            <td class="<%=classText %>"><c:out value="${tyre.regNo}" /></td>
                            <td class="<%=classText %>" height="30"><c:out value="${tyre.uomName}" /></td>   
                            
                            <td class="<%=classText %>" height="30"> <c:out value="${tyre.tyreNo}" /> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${tyre.posName}" /></td>
                            <td class="<%=classText %>"> <c:out value="${tyre.totalAmount}" /> </td>
                          <c:set var="Amount" value="${Amount + tyre.totalAmount}" />   
                        </tr>
                        <%index++;%>
                    </c:forEach>
                     <tr>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>                       
                        <td class="text2" height="30"><b>Total Amount</b></td>                       
                        <td class="text2" align="left"  height="30"> 
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${Amount}" pattern="##.00"/></b>            
                        </td>  
                         <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                         <td class="text2" height="30">&nbsp;</td>
                    </tr>
                </table>
            </c:if>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
