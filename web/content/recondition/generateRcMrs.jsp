<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
</head>
<script>

function generateMRS(name)
{
    if(document.generateMrs.rcWorkId.value=='0'){
    alert("Please Select WO Number");
    document.generateMrs.rcWorkId.focus();
    }     
   else if(name=='generate'){
    document.generateMrs.action="/throttle/generateRcMrsPage.do";    
    document.generateMrs.submit();       
    }else if(name=='search'){
    document.generateMrs.action='/throttle/searchRcMrs.do';
    document.generateMrs.submit();
    }
}
function setFocus()
{
    var rcWorkId='<%=request.getAttribute("rcWorkId")%>';
    if(rcWorkId!='null'){
     document.generateMrs.rcWorkId.value=rcWorkId;
    }

}
function checkISValid(jcno){

    //alert(document.getElementById("rcWork"));
    //alert(document.getElementByName("rcWork").length);
    var c=0;
    var JcNos=document.getElementsByName("rcWork");

  for(var i=0;i<JcNos.length;i++){
      if(parseInt(JcNos[i].value)==parseInt(jcno))
          c++;
  }
    if(c==1){
//
    }else{
        alert("Enter Valid JobcardNo");
        document.generateMrs.rcWorkId.value='';
        document.generateMrs.search.focus();
        document.generateMrs.rcWorkId.focus();
    }

}

</script>



  <body onload="setFocus()">
  <form name="generateMrs"  method="post" >


<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp"%>

<c:if test = "${rcWorkOrder != null}" >
<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="400" class="border">
<tbody><tr>
<td colspan="4" class="contenthead" height="30"><div class="contenthead">Generate MRS</div></td>
</tr>
<tr>
<td class="text2" height="30">Work Order No</td>
<td class="text2" height="30">
    <select name="rcWorkId"     class="form-control">
        <option value="0">--Select--</option>

<c:forEach items="${rcWorkOrder}" var="rc">
        <option value='<c:out value="${rc.rcWorkId}" />'><c:out value="${rc.rcWorkId}" /></option>
</c:forEach >
</select>
    </td>







</tr>



</tbody>
</table>

<br>
<center>
<input type="button" class="button" value="Go" name="search" onClick="generateMRS(this.name);">
<input type="button" class="button" value="Generate MRS" name="generate" onClick="generateMRS(this.name);">
</center>
<br>
</c:if>
<c:if test = "${rcMrsList != null}" >
        <%
int index=0;
 ArrayList rcMrsList = (ArrayList) request.getAttribute("rcMrsList");
     if(rcMrsList.size() != 0){
%>
<div  id="tabVisible" >



<table align="center"  width=600" border="0" cellspacing="0" cellpadding="0" class="border">

                        <tr>
                    <td class="contentsub" height="30"><div class="contentsub">S No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">MRS No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Technician</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Approval Status</div></td>
                </tr>
      <c:forEach items="${rcMrsList}" var="rc">
<%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
    <tr>
<td  height="30" class="<%=classText%>"><%=index+1%></td>
<td  height="30" class="<%=classText%>"><c:out value="${rc.mrsId}"/></td>
<td  height="30" class="<%=classText%>"><c:out value="${rc.empName}"/> </td>
<td  height="30" class="<%=classText%>"><c:out value="${rc.createdDate}"/> </td>
<td align="center"  height="30" class="<%=classText%>"><a href="/throttle/rcMrsStatus.do?status=<c:out value="${rc.status}"/>&mrsId=<c:out value="${rc.mrsId}"/>&rcWorkId=<c:out value="${rc.rcWorkId}"/>"><c:out value="${rc.status}"/></a></td>
<%index++;%>
</c:forEach>
<tr>
    </table>
<br>



</div>
<%
}
%>

</c:if>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </body>
</html>
