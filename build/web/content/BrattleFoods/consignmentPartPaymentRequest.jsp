<%-- 
    Document   : paymentType
    Created on : Jan 28, 2014, 12:22:43 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <%@page contentType="text/html" import="java.util.*" %>


    <%= new java.util.Date() %>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });


        function onKeyPressBlockNumbers(e)
        {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /\d/;
            return !reg.test(keychar);
        }


        function extractNumber(obj, decimalPlaces, allowNegative)
        {
            var temp = obj.value;

            // avoid changing things if already formatted correctly
            var reg0Str = '[0-9]*';
            if (decimalPlaces > 0) {
                reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
            } else if (decimalPlaces < 0) {
                reg0Str += '\\.?[0-9]*';
            }
            reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
            reg0Str = reg0Str + '$';
            var reg0 = new RegExp(reg0Str);
            if (reg0.test(temp))
                return true;

            // first replace all non numbers
            var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
            var reg1 = new RegExp(reg1Str, 'g');
            temp = temp.replace(reg1, '');

            if (allowNegative) {
                // replace extra negative
                var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
                var reg2 = /-/g;
                temp = temp.replace(reg2, '');
                if (hasNegative)
                    temp = '-' + temp;
            }

            if (decimalPlaces != 0) {
                var reg3 = /\./g;
                var reg3Array = reg3.exec(temp);
                if (reg3Array != null) {
                    // keep only first occurrence of .
                    //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                    var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                    reg3Right = reg3Right.replace(reg3, '');
                    reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                    temp = temp.substring(0, reg3Array.index) + '.' + reg3Right;
                }
            }

            obj.value = temp;
        }
        function blockNonNumbers(obj, e, allowDecimal, allowNegative)
        {
            var key;
            var isCtrl = false;
            var keychar;
            var reg;

            if (window.event) {
                key = e.keyCode;
                isCtrl = window.event.ctrlKey
            }
            else if (e.which) {
                key = e.which;
                isCtrl = e.ctrlKey;
            }

            if (isNaN(key))
                return true;

            keychar = String.fromCharCode(key);

            // check for backspace or delete, or if Ctrl was pressed
            if (key == 8 || isCtrl)
            {
                return true;
            }

            reg = /\d/;
            var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
            var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

            return isFirstN || isFirstD || reg.test(keychar);
        }
    </script>
    <script type="text/javascript">

        function submitPage() {
            if (document.getElementById('requestOnPay').value == '') {
                alert("Please select payment request date");
                document.getElementById('requestOnPay').focus();
            } else if (document.getElementById('paymentRequestRemarks').value == '') {
                alert("Please Enter payment request remarks");
                document.getElementById('paymentRequestRemarks').focus();
            } else {
                document.paymentType.action = '/throttle/savePartPaymentApprovalRequest.do';
                document.paymentType.submit();
            }
        }
    </script>


</head>
<body onload="//calcAmountToBePay();">
    <form name="paymentType" method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@include file="/content/common/message.jsp" %>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border">
            <tr align="center">
                <td colspan="2" align="center" class="contenthead" height="30">
                    <div class="contenthead">Consignment Part Payment Approval Request</div></td>
            </tr>
            <tr>
                <td class="text1" height="30">Cnote No</td>
                <td class="text1" height="30">
                    <input name="consignmentOrderId" id="consignmentOrderId" type="hidden" class="textbox" value="<c:out value="${consignmentOrderId}"/>">
                    <input name="cNoteNo" id="cNoteNo" type="hidden" class="textbox" value="<c:out value="${cNoteNo}"/>">
                    <input name="freightAmount" id="freightAmount" type="hidden" class="textbox" value="<c:out value="${freightAmount}"/>">
                    <input name="customerId" id="customerId" type="hidden" class="textbox" value="<c:out value="${customerId}"/>">
                    <c:out value="${cNoteNo}"/>
                    <input name="paymentTypeId" id="paymentTypeId" type="hidden" class="textbox" value="<c:out value="${paymentTypeId}"/>"> </td>
            <input name="nextPage" id="nextPage" type="hidden" class="textbox" value="<c:out value="${nextPage}"/>"> </td>
            </tr>
            <tr>
                <td class="text2" height="30">Payment Type</td>
                <td class="text2" height="30">
                    <c:if test="${paymentTypeId=='1'}" >
                        Credit
                    </c:if>
                    <c:if test="${paymentTypeId=='2'}" >
                        Advance
                    </c:if>
                    <c:if test="${paymentTypeId=='3'}" >
                        To Pay and Advance
                    </c:if>
                    <c:if test="${paymentTypeId=='4'}" >
                        To Pay
                    </c:if>
                </td>
            </tr>
            <c:if test="${paymentTypeId == 3}">
                <c:set var="paidNowPercent" value="${freightAmount/100}"/>
                <c:set var="paidNow1" value="${paidNowPercent*firstPart}"/>
                <c:set var="paidNow2" value="${paidNowPercent*secondPart}"/>

            </c:if>
            <tr>
                <td class="text1" height="30" >First Part Amount (INR.)</td>
                <td class="text1" height="30" >
                    <input type="hidden" name="firstPartAmount" id="firstPartAmount" value="<c:out value="${paidNow1}"/>"/>
                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paidNow1}" /></td>
            </tr>
            <tr>
                <td class="text2" height="30" >Second Part Amount (INR.)</td>
                    <input type="hidden" name="secondPartAmount" id="secondPartAmount" value="<c:out value="${paidNow2}"/>"/>
                    <td class="text2" height="30" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paidNow2}"  /></td>
            </tr>
            <tr>
                <td class="text1" height="30"><font color="red">*</font>Requested Pay Date</td>
                <td class="text1" height="30">
                    <input name="requestOnPay" id="requestOnPay" type="text" class="datepicker" value="">
                </td>
            </tr>
            <tr>
                <td class="text2" height="30">Payment Remarks</td>
                <td class="text2" height="30"><textarea id="paymentRequestRemarks" name="paymentRequestRemarks"></textarea></td>
            </tr>

        </table>
        <br>
        <center>
            <input type="button" value="save" class="button" onClick="submitPage();">
        </center>
        <br>
        <br>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
