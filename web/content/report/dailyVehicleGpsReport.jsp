<%-- 
    Document   : DailyVehicleGpsReport
    Created on : aug 16, 2016, 09:37:16 AM
    Author     : raj
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">



            function viewLogDetails(tripId) {
                window.open('/throttle/viewTripLog.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }


            //auto com

//            $(document).ready(function () {
//                // Use the .autocomplete() method to compile the list based on input from user
//                $('#vehicleNo').autocomplete({
//                    source: function (request, response) {
//                        $.ajax({
//                            url: "/throttle/getRegistrationNo.do",
//                            dataType: "json",
//                            data: {
//                                regno: request.term
//                            },
//                            success: function (data, textStatus, jqXHR) {
//                                var items = data;
//                                response(items);
//                            },
//                            error: function (data, type) {
//                                console.log(type);
//                            }
//                        });
//                    },
//                    minLength: 1,
//                    select: function (event, ui) {
//                        var value = ui.item.Name;
//                        var tmp = value.split('-');
//                        $('#vehicleId').val(tmp[0]);
//                        $('#vehicleNo').val(tmp[1]);
//                        return false;
//                    }
//                }).data("autocomplete")._renderItem = function (ul, item) {
//                    var itemVal = item.Name;
//                    var temp = itemVal.split('-');
//                    itemVal = '<font color="green">' + temp[1] + '</font>';
//                    return $("<li></li>")
//                            .data("item.autocomplete", item)
//                            .append("<a>" + itemVal + "</a>")
//                            .appendTo(ul);
//                };
//            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.userAccessReport.action = '/throttle/handleDailyVehicleGpsReport.do?param=ExportExcel';
                        document.userAccessReport.submit();
                    } else {
                        document.userAccessReport.action = '/throttle/handleDailyVehicleGpsReport.do?param=Search';
                        document.userAccessReport.submit();
                    }
                }
            }
//            function setValue(){
//                if('<%=request.getAttribute("page")%>' !='null'){
//                var page = '<%=request.getAttribute("page")%>';
//                    if(page == 1){
//                      submitPage('search');
//                    }
//                }
//            }


          
        </script>
   <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Daily Vehicle GPS Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="userAccessReport"   method="post">
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;"  align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">DAILY VEHICLE GPS REPORT</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr height="30">
                                        <td align="center"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td align="center"><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" ></td>
                                    </tr>
                                    <tr>
                                         <!--<td></td>&emsp;-->
                                         <td></td>
                                         <td colspan="2" align="center"><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);">
                                         <input type="button" class="btn btn-info" name="ExportExcel" onclick="submitPage(this.name);" value="Export"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <br>
            <c:if test="${userLoginList != '0'}">
                <table  id="table" class="table table-info mb30" width="100%" >
                    <thead>
                       <tr id="tableDesingTH" height="30">
                            <th align="center">S.No</th>
                            <th align="center">Gps Mobile No</th>
                            <th align="center">Vehicle No</th>
                            <th align="center">Party Name</th>
                            <th align="center">Route</th>
                            <th align="center">Location</th>
                            <th align="center">Idle Time</th>
                            <th align="center">Remarks</th>
                            <th align="center">Category</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${gpsVehiclelist}" var="gpsVehiclelist">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr>
                                <td class="<%=classText%>"><%=index++%></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.gpsSimNo}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.vehicleNo}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.billingParty}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.routeName}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.gpsLocation}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.idleTime}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.remarks}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.vendorName}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" >5</option>
                            <option value="10"selected="selected">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
        </form>
     <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
   </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>