<%@taglib uri='cewolf.tld' prefix='cewolf' %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.*"%>
<%@page import="de.laures.cewolf.*"%>
<%@page import="de.laures.cewolf.tooltips.*"%>
<%@page import="de.laures.cewolf.links.*"%>
<%@page import="org.jfree.data.*"%>
<%@page import="org.jfree.data.time.*"%>
<%@page import="org.jfree.data.gantt.*"%>
<%@page import="org.jfree.chart.*"%>
<%@page import="org.jfree.chart.plot.*"%>
<%@page import="org.jfree.data.category.*"%>
<%@page import="org.jfree.data.general.*"%>
<%@page import="org.jfree.data.xy.*"%>
<%@page import="java.awt.*" %>
<%@page import="org.jfree.chart.axis.*" %>
<%@page import="org.jfree.chart.labels.*" %>
<%@page import="org.jfree.chart.axis.NumberAxis" %>
<%@page import="org.jfree.chart.plot.*" %>
<%@page import="org.jfree.chart.renderer.*" %>
<%@page import="org.jfree.chart.renderer.category.*" %>
<%@page import="org.jfree.data.*" %>
<%@page import="org.jfree.chart.axis.CategoryAxis"%>
<%@page import="org.jfree.util.Rotation"%>
<%@page import="org.jfree.chart.axis.CategoryLabelPositions"%>
<%@page import="javax.swing.Timer"%>
<%@page import="java.awt.event.ActionListener"%>
<%@page import="java.awt.event.ActionEvent"%>
<%@page import=" org.jfree.chart.labels.StandardPieSectionLabelGenerator"  %>
<%@page import="java.text.DecimalFormat"     %>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       


<%--This is the chart processor code which is used to customise the chart genearted by ceowlf tags below --%>
<%   
ChartPostProcessor stackedBar3DChartPostProcessor = new ChartPostProcessor() {

public void processChart(Object chart, Map params) 
{
JFreeChart objJFreeChart = (JFreeChart)chart;
//objJFreeChart.setBackgroundPaint(new Color(255, 255, 255));
CategoryPlot plot = objJFreeChart.getCategoryPlot();
plot.setRangeGridlinesVisible(false);
plot.setNoDataMessage("No data available");
plot.setNoDataMessageFont(new Font("Verdana", Font.BOLD, 11));
plot.setNoDataMessagePaint(new Color(214, 80,6));
// customise the range axis...
NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
rangeAxis.setLabelFont(new Font("Verdana",Font.BOLD,12));
CategoryAxis domainAxis = plot.getDomainAxis();
domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
domainAxis.setLabelFont(new Font("Verdana", Font.BOLD, 12));
domainAxis.setMaximumCategoryLabelWidthRatio(0.75f);
// customise the renderer...
BarRenderer3D renderer = (BarRenderer3D) plot.getRenderer();
renderer.setMaximumBarWidth(0.05);
renderer.setItemMargin(0.0);



GradientPaint gp0 = new GradientPaint(
0.0f, 0.0f, new Color(84, 252, 14),     0.0f, 500.0f, new Color(9, 118,6)
);
GradientPaint gp1 = new GradientPaint(
0.0f, 0.0f, new Color(210, 119, 8),     0.0f, 500.0f, new Color(214, 80,6)
);
GradientPaint gp2 = new GradientPaint(
0.0f, 0.0f, new Color(85, 170, 218),     0.0f, 500.0f, new Color(29,94, 223)
);
renderer.setSeriesPaint(0, gp0);
renderer.setSeriesPaint(1, gp1);
renderer.setSeriesPaint(2, gp2);
renderer.setItemLabelsVisible(true);
objJFreeChart.getCategoryPlot().setRenderer(renderer);

}	
};

pageContext.setAttribute("stackedBar3DPostProcessor",stackedBar3DChartPostProcessor);
String summ = (String)request.getAttribute("summ");
ArrayList serviceSummary = (ArrayList) request.getAttribute("serviceSummary");
System.out.println("serviceSummary in jsp "+serviceSummary.size());

%>
 

<%--From here its Displaying Fluidity Reports-- --%>
<body onLoad="setValues()" >
<form name="chart" method="post" >
    

<table width='500' align="center" bgcolor='white' border='0' cellpadding='0' cellspacing='0' class='repcontain'>
<tr>
    <td class="text1" > From Date : </td>
    <td class="text1" >
       <input type="text" class="form-control" readonly name="fromDate" value="" >
       <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.fromDate,'dd-mm-yyyy',this)"/>            
    </td>
</tr>
<tr>
    <td class="text2" > To Date : </td>
    <td class="text2" >  
        <input type="text" class="form-control" readonly name="toDate" value="" >
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.toDate,'dd-mm-yyyy',this)"/>    
    </td>
</tr>
<tr> <td colspan="2" align="center">&nbsp;</td> </tr>
<tr>    
    <td colspan="2" align="center">
        <input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" > </td>
</tr>    

</table>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


<table width='100%' bgcolor='white' border='0' cellpadding='0' cellspacing='0' class='repcontain'>
<tr style="text-align: center;font-size: 12;">
<th bgcolor="white">Management Reports</th>
</tr>

</table>
<br>
<br>

<table width='100%' bgcolor='white' border='1' cellpadding='5' cellspacing='0' class='repcontain'>
<tr>
<td bgcolor="Service Summary">
<jsp:useBean id="serviceData" class="ets.cewolf.beans.StackedBean"/>
<%	serviceData.setServiceSummary(serviceSummary); %>
<cewolf:chart id="ExampleChart" type="stackedverticalbar3d"  xaxislabel="Service Points" yaxislabel="No. of Vehicles"
showlegend="true"
title="<%= summ %>" legendanchor="south">
<cewolf:data>
<cewolf:producer id="serviceData"/>    
</cewolf:data>
<cewolf:chartpostprocessor id="stackedBar3DPostProcessor">
</cewolf:chartpostprocessor>
</cewolf:chart>

<cewolf:img chartid="ExampleChart" renderer="/cewolf" width="800" height="400" align="center">
<cewolf:map  tooltipgeneratorid="serviceData" linkgeneratorid="serviceData"></cewolf:map>
</cewolf:img>
</td>
</tr>


</table>
</body>

<script>
    function searchSubmit()
    {        
        document.chart.action="/throttle/handleServiceGraphData.do"
        document.chart.submit();
    }
    
function setValues(){    
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.chart.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }        
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.chart.toDate.value='<%= request.getAttribute("toDate") %>';
    }    
}    
    
</script>   



