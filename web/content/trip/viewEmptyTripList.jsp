<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <script language="javascript">
        function submitPage() {

                document.customer.action = '/throttle/handleTripClosureRequest.do';
                document.customer.submit();
        }
    </script>
    <body>
        <form name="customer" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Empty Trip  Request</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                           <%!
                           public String NullCheck(String inputString)
                                {
                                        try
                                        {
                                                if ((inputString == null) || (inputString.trim().equals("")))
                                                                inputString = "";
                                        }
                                        catch(Exception e)
                                        {
                                                                inputString = "";
                                        }
                                        return inputString.trim();
                                }
                           %>

                           <%

                            String today="";
                            String fromday="";

                            fromday = NullCheck((String) request.getAttribute("fromdate"));
                            today = NullCheck((String) request.getAttribute("todate"));

                            if(today.equals("") && fromday.equals("")){
                            Date dNow = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dNow);
                            cal.add(Calendar.DATE, 0);
                            dNow = cal.getTime();

                            Date dNow1 = new Date();
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(dNow1);
                            cal1.add(Calendar.DATE, -6);
                            dNow1 = cal1.getTime();

                            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                            today = ft.format(dNow);
                            fromday = ft.format(dNow1);
                            }

            %>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value="<%=today%>"></td>
                                        <td>&nbsp;</td>
                                        <td><input type="button" class="button" name="search" onClick="submitPage();" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <c:if test = "${emptyTripDetails != null}" >
                <table width="80%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>                            
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Consignment Note</h3></th>
                            <th><h3>Trip Code</h3></th>
                            <th><h3>Customer Name</h3></th>
                            <th><h3>Request Type</h3></th>
                            <th><h3>View</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                           <c:forEach items="${emptyTripDetails}" var="emptyTrip">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${emptyTrip.vehicleNo}"/> </td>
                                <td align="left" class="text2"><c:out value="${emptyTrip.consignmentNote}"/> </td>
                                <td align="left" class="text2"><c:out value="${emptyTrip.tripCode}"/></td>
                                <td align="left" class="text2"><c:out value="${emptyTrip.customerName}"/></td>
                                <td align="left" class="text2">Empty Trip Approval</td>
                                <td align="left" class="text2">
                                <c:if test = "${emptyTrip.emptyTripApprovalStatus == 1}" >
                                    <a href="/throttle/viewEmptyTripApproval.do?tripId=<c:out value="${emptyTrip.tripId}"/>">view details</a>
                                </c:if>
                                     <c:if test = "${emptyTrip.emptyTripApprovalStatus != 1}" >
                                    APPROVED
                                     </c:if>
                                </td>

                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


</html>
