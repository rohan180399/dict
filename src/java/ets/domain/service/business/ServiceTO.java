/*---------------------------------------------------------------------------
 * ServiceTO.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.service.business;

import java.io.Serializable;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ServiceTO implements Serializable {

    private String checkListId = "";
    private String checkListPoint = "";
    private String checked = "";
    private String serviced = "";
    private String remarks = "";
    private int km = 0;
    private int hm = 0;
    private int month = 0;
    private int mfrId = 0;
    private int groupId = 0;
    private String mfrName = "";
    private int modId = 0;
    private int fserviceId = 0;
    private String desc = "";
    private String status = "";
    private int serviceId = 0;
    private int userId = 0;
    private String serviceName = "";
    private String serviceCode = "";
    private String activityCode = "";
    private String vtypeName = "";
    private int vtypeId = 0;
    private String hourMeter = "";
    private float labour = 0;
    private String secName = "";
    private int secId = 0;
    private int activityId = 0;
    private String activityName = "";
    private String probDetails = "";
    private int probId = 0;
    private int modelId = 0;

    private String modName = "";
    private String percentage = "0";
    private String ratePerHour = "0";
    private String time = "0";
    private String serviceTypeId = "";
    private String serviceTypeName = "";
  private String activeInd = "";

   

    public String getModName() {
        return modName;
    }

    public void setModName(String modName) {
        this.modName = modName;
    }

    public int getProbId() {
        return probId;
    }

    public void setProbId(int probId) {
        this.probId = probId;
    }

    public String getProbDetails() {
        return probDetails;
    }

    public void setProbDetails(String probDetails) {
        this.probDetails = probDetails;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public String getHourMeter() {
        return hourMeter;
    }

    public void setHourMeter(String hourMeter) {
        this.hourMeter = hourMeter;
    }

    public float getLabour() {
        return labour;
    }

    public void setLabour(float labour) {
        this.labour = labour;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public int getVtypeId() {
        return vtypeId;
    }

    public void setVtypeId(int vtypeId) {
        this.vtypeId = vtypeId;
    }

    public String getVtypeName() {
        return vtypeName;
    }

    public void setVtypeName(String vtypeName) {
        this.vtypeName = vtypeName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getFserviceId() {
        return fserviceId;
    }

    public void setFserviceId(int fserviceId) {
        this.fserviceId = fserviceId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getMfrId() {
        return mfrId;
    }

    public void setMfrId(int mfrId) {
        this.mfrId = mfrId;
    }

    public int getModId() {
        return modId;
    }

    public void setModId(int modId) {
        this.modId = modId;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getHm() {
        return hm;
    }

    public void setHm(int hm) {
        this.hm = hm;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getRatePerHour() {
        return ratePerHour;
    }

    public void setRatePerHour(String ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public String getCheckListPoint() {
        return checkListPoint;
    }

    public void setCheckListPoint(String checkListPoint) {
        this.checkListPoint = checkListPoint;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getServiced() {
        return serviced;
    }

    public void setServiced(String serviced) {
        this.serviced = serviced;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }
    
    
    
    
}
