<%--
Document   : managerole
Created on : Jul 19, 2008, 4:23:46 PM
Author     : vidya
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript">
    function submitPage(value) {
        if (value == 'add') {
            document.managerole.action = '/throttle/saveRoleMenu.do';
            document.managerole.submit();
        } else if (value == 'moddel') {
            document.managerole.action = '/throttle/alterrole.do';
            document.managerole.reqfor.value = 'alterrole';
            document.managerole.submit();
        }
    }

    function setRoleBasedFunction(){
        document.managerole.action = '/throttle/roleMenu.do';
        document.managerole.submit();
    }

    function setFunctionStatus(sno){
        var checkedStatus = document.getElementById("selectedStatus"+sno).checked;
        if(checkedStatus == true){
            document.getElementById("functionStatus"+sno).value = 1;
        }else{
            document.getElementById("functionStatus"+sno).value = 0;
        }
    }
</script>



<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="settings.label.ManageRole Menu" text="Manage Role Menu"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
            <li class=""><spring:message code="settings.label.ManageRole" text="default text"/> </li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >

                <form method="post" name="managerole">

                    <!-- copy there from end -->
                    <!--<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                    <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                     pointer table
                    <table class="table table-info mb30 table-hover" >
                    <tr>
                    <td >

                    </td></tr></table>
                     pointer table

                    </div>
                    </div>-->

                    <!-- message table -->

                    <!-- message table -->
                    <!-- copy there  end -->

                    <!--<table class="table table-bordered">-->

                    <table  class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th>Role</th>
                                <th>
                                    <c:if test="${roleRecord != null}">
                                        <select name="roleId" id="roleId" onchange="setRoleBasedFunction()" class="form-control">
                                        <option value="0">--Select-</option>
                                        <c:forEach items="${roleRecord}" var="role" >
                                            <option value="<c:out value="${role.roleId}"/>"><c:out value="${role.roleName}"/></option>
                                        </c:forEach>
                                        </select>
                                    </c:if>
                                    <script>
                                        document.getElementById("roleId").value='<c:out value="${roleId}"/>';
                                    </script>
                                </th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Menu</td>
                            <td>
                                <c:if test="${menuList != null}">
                                    <select name="menuId" id="menuId" onchange="setRoleBasedFunction()" class="form-control">
                                    <option value="0">--Select-</option>
                                    <c:forEach items="${menuList}" var="menu">
                                        <option value="<c:out value="${menu.menuId}"/>"><c:out value="${menu.menuName}"/></option>
                                    </c:forEach>
                                    </select>
                                </c:if>
                                <script>
                                    document.getElementById("menuId").value='<c:out value="${menuId}"/>';
                                </script>
                            </td>
                            <td>SubMenu</td>
                            <td>
                            <c:if test="${submenuList != null}">
                                <select name="subMenuId" id="subMenuId" onchange="setRoleBasedFunction()" class="form-control">
                                <option value="0">--Select-</option>
                                <c:forEach items="${submenuList}" var="subMenu">
                                    <option value="<c:out value="${subMenu.subMenuId}"/>"><c:out value="${subMenu.subMenuName}"/></option>
                                </c:forEach>
                                </select>
                            </c:if>
                            <script>
                                document.getElementById("subMenuId").value='<c:out value="${subMenuId}"/>';
                            </script>
                            </td>
                        </tr>
                    </table>
                    <table  class="table table-info mb30 table-hover" id="table">

                        <thead>
                            <tr>
                                <th height="30" ><div ><spring:message code="settings.label.Sno" text="Sno"/></div></th>
                                <th height="30" ><div ><spring:message code="settings.label.MenuName" text="Menu"/></div></th>
                                <th height="30" ><div ><spring:message code="settings.label.SubMenuName" text="SubMenu"/></div></th>
                                <th height="30" ><div ><spring:message code="settings.label.FunctionName" text="Function"/></div></th>
                                <th height="30" ><div ><spring:message code="settings.label.Select" text="Select All"/>&nbsp;<input type="checkbox" id="selectAll" onclick="selectAllMenus()" /></div></th>
                            </tr>
                        </thead>
                        <script>
                            function selectAllMenus(){
                              var selectAll = document.getElementById("selectAll").checked;
                              var functionStatus = document.getElementsByName("functionStatus");
                              var selectedStatus = document.getElementsByName("selectedStatus");
                              if(selectAll == true){
                                  for(var i=0; i<functionStatus.length; i++){
                                     document.getElementById("selectedStatus"+i).checked=true;
                                     document.getElementById("functionStatus"+i).value=1;
                                  }
                              }else{
                                  for(var i=0; i<functionStatus.length; i++){
                                     document.getElementById("selectedStatus"+i).checked=false;
                                     document.getElementById("functionStatus"+i).value=0;
                                  }
                              }
                            }
                        </script>

                        <%
                        int index = 0;
                        ArrayList roletypes = (ArrayList) request.getAttribute("roleRecord");
                        if(roletypes.size()!=0) {


                        %>
                        <c:if test = "${roleMenuList != null}">
                            <c:forEach items="${roleMenuList}" var="role">
                                <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                classText = "text2";
                                } else {
                                classText = "text1";
                                }
                                %>

                                <tr>

                                    <td>
                                        <input type="hidden" name="menuIds" value='<c:out value="${role.menuId}" />' />
                                        <input type="hidden" name="subMenuIds" value='<c:out value="${role.subMenuId}" />' />
                                        <input type="hidden" name="functionIds" value='<c:out value="${role.functionId}" />' />
                                        <input type="hidden" name="insertStatus" value='<c:out value="${role.insertStatus}" />' />
                                        <div align="center"><%=index+1%></div>
                                    </td>
                                    <td>
                                        <c:out value="${role.menuName}" />
                                    </td>
                                    <td>
                                        <c:out value="${role.subMenuName}" />
                                    </td>
                                    <td>
                                        <c:out value="${role.functionName}"/>
                                    </td>
                                    <td>
                                        <c:if test="${role.functionSts == 0}">
                                        <input type="checkbox" name="selectedStatus" id="selectedStatus<%=index%>" onclick="setFunctionStatus('<%=index%>')" >
                                        <input type="hidden" name="functionStatus" id="functionStatus<%=index%>" value="0" >
                                        </c:if>
                                        <c:if test="${role.functionSts == 1}">
                                            <input type="checkbox" name="selectedStatus" id="selectedStatus<%=index%>" onclick="setFunctionStatus('<%=index%>')" checked>
                                        <input type="hidden" name="functionStatus" id="functionStatus<%=index%>" value="1" >
                                        </c:if>
                                    </td>
                                </tr>
                                <%
                                index++;
                                %>
                            </c:forEach >
                        </c:if>

                        <%
                        }
                        %>
                        </td>
                    </table>
                    <c:if test="${roleId != 0}">
                    <center>
                        <input type="button" class="btn btn-info" name="add" value="<spring:message code="settings.label.Save" text="Save"/>" onClick="submitPage(this.name)" class="button">
                    </center>
                    </c:if>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls" class="col-sm-3">
                        <div id="perpage" class="panel-body">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation" class="panel-body">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text" class="panel-body">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
