<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
    <script>                         
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

function submitPage(val){
       
        if(val=='GoTo'){
            var temp=document.mpr.GoTo.value;                 
            if(temp!='null'){
            document.mpr.pageNo.value=temp;
            }
        }        
        document.mpr.button.value=val;        
        document.mpr.action = '/throttle/reqItemsReport.do'
        document.mpr.submit();        
}

function setValues()
{
    if('<%= request.getAttribute("mfrCode") %>' !='null'){
        document.mpr.mfrCode.value = '<%= request.getAttribute("mfrCode") %>';
    }
    if('<%= request.getAttribute("paplCode") %>' !='null'){
        document.mpr.paplCode.value = '<%= request.getAttribute("paplCode") %>';
    }
    if('<%= request.getAttribute("categoryId") %>' !='null'){
        document.mpr.categoryId.value = '<%= request.getAttribute("categoryId") %>';
    }
    if('<%= request.getAttribute("searchAll") %>' !='null'){
        document.mpr.searchAll.value = '<%= request.getAttribute("searchAll") %>';
    }
}    





        </script>
        
        <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label.RequiredItems" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label.RequiredItems" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        
        
    <body onLoad="setValues();" >        
        <form name="mpr" method="post">                    
            <%--<%@ include file="/content/common/path.jsp" %>--%>            
            <!-- pointer table -->
             <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    

            <%
                    int pageIndex = (Integer)request.getAttribute("pageNo");
                    int index1 = ((pageIndex-1)*10)+1 ;
            %>
<table class="table table-info mb30 table-hover">
        <thead><tr><th colspan="4"><spring:message code="fmsstores.label.RequiredItems"  text="default text"/></th></tr></thead>
    <tr>
        <td height="30"><spring:message code="fmsstores.label.ItemCode"  text="default text"/></td>
        <td height="30"><input name="mfrCode" type="text" class="form-control" style="width:260px;height:40px;" value="" ></td>
        <td height="30"><spring:message code="fmsstores.label.PAPLCode"  text="default text"/></td>
        <td height="30"><input name="paplCode" type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
    </tr>
    <tr>
        <td height="30"><spring:message code="fmsstores.label.Category"  text="default text"/></td>
        <td height="30"><select class="form-control" style="width:260px;height:40px;" name="categoryId"  >
                <option value="0">---<spring:message code="fmsstores.label.Select"  text="default text"/>---</option>
                <c:if test = "${CategoryList != null}" >
                <c:forEach items="${CategoryList}" var="Dept">
                <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                </c:forEach >
                </c:if>
                </select>
        </td>
        <td height="30"><spring:message code="fmsstores.label.SearchType"  text="default text"/></td>
        <td height="30"><select class="form-control" style="width:260px;height:40px;" name="searchAll"  >
                <option value="">---<spring:message code="fmsstores.label.Select"  text="default text"/>---</option>
                <option value="Y"><spring:message code="fmsstores.label.All"  text="default text"/></option>
                <option value="N"><spring:message code="fmsstores.label.RequiredItems"  text="default text"/></option>
                </select></td>
    </tr>
    <tr>
    <td align="center" colspan="4"><input type="button" class="btn btn-success" name="Search" value="<spring:message code="fmsstores.label.Search"  text="default text"/>" onClick="submitPage(this.value)" > </td>
    </tr>

    </table>
    
                <c:if test = "${requiredItemsList != null}" >   
                <%
            String classText = "";
            int oddEven = 0;
            int index = 0;
                %>
            <table class="table table-info mb30 table-hover" id="bg" >              

                <thead>
                        <tr>
                            <th width="25" height="30" ><b><spring:message code="fmsstores.label.Sno"  text="default text"/></b></th>						
                            <th width="69" height="30" ><b><spring:message code="fmsstores.label.MfrCode"  text="default text"/></b></th>
                            <th width="71" height="30" ><b><spring:message code="fmsstores.label.PAPLCode"  text="default text"/></b></th>                       
                            <th width="144" height="30"><b><spring:message code="fmsstores.label.ItemName"  text="default text"/></b></th>
                            <th width="31" height="30" ><b><spring:message code="fmsstores.label.Uom"  text="default text"/></b></td>
                            <th width="60" height="30" ><b><spring:message code="fmsstores.label.ReorderLevel"  text="default text"/></b> </th>                                                                                                                                               
                            <th width="58" height="30" ><b><spring:message code="fmsstores.label.StockBalance"  text="default text"/></b> </th>                                                                                                                                                                                                                                                                                                                
                            <th width="69" height="30" ><b><spring:message code="fmsstores.label.PoRaisedQty"  text="default text"/></b> </th>                                                                                                                                                                          
                        </tr>  
                </thead>
                    <c:forEach items="${requiredItemsList}" var="item"> 
                        <%

            oddEven = index1 % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td  height="30"><%= index1 %></td>						
                            <td  height="30"><c:out value="${item.mfrCode}"/></td>
                            <td  height="30"><c:out value="${item.paplCode}"/></td>                       
                            <td  height="30"><c:out value="${item.itemName}"/></td>
                            <td  height="30"><c:out value="${item.uomName}"/></td>
                            <td  height="30"><c:out value="${item.roLevel}"/></td>
                            <td  height="30"><c:out value="${item.stockBalance}"/></td>                            
                            <td  height="30"><c:out value="${item.poRaisedQty}"/> </td>
                           
                        </tr>
                        <%
            index++;
            index1++;
                        %>
                    </c:forEach>
                  
            </table>

            <br>           
                <input type="hidden" name="purchaseType" value="" >          
            <br>           
</c:if>     
     
<br>
 <%@ include file="/content/common/pagination.jsp"%>      
<br>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
