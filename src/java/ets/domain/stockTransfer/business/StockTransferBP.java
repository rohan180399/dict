/*---------------------------------------------------------------------------
 * ServiceCommand.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.stockTransfer.business;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.stockTransfer.data.StockTransferDAO;
import ets.domain.mrs.data.MrsDAO;
import ets.domain.mrs.business.MrsTO;
import ets.domain.stockTransfer.business.StockTransferTO;
import java.util.ArrayList;
import java.util.Iterator;

public class StockTransferBP {

    private StockTransferDAO stocktransferDAO;
    private MrsDAO mrsDAO;
    private StockTransferTO stocktransferTO;

    public StockTransferDAO getStocktransferDAO() {
        return stocktransferDAO;
    }

    public void setStocktransferDAO(StockTransferDAO stocktransferDAO) {
        this.stocktransferDAO = stocktransferDAO;
    }

    public MrsDAO getMrsDAO() {
        return mrsDAO;
    }

    public void setMrsDAO(MrsDAO mrsDAO) {
        this.mrsDAO = mrsDAO;
    }

    public StockTransferTO getStocktransferTO() {
        return stocktransferTO;
    }

    public void setStocktransferTO(StockTransferTO stocktransferTO) {
        this.stocktransferTO = stocktransferTO;
    }

    public ArrayList processServicePtList(int companyType, int companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList servicePtList = new ArrayList();
        servicePtList = stocktransferDAO.getServicePtList(companyType, companyId);
        if (servicePtList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");

        }
        return servicePtList;
    }

    public ArrayList processServicePtList(int companyType) throws FPRuntimeException, FPBusinessException {
        ArrayList servicePtList = new ArrayList();
        servicePtList = stocktransferDAO.getServicePtList(companyType);
        if (servicePtList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");

        }
        return servicePtList;
    }

    public ArrayList processRequesttList(int companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList requestList = new ArrayList();
        requestList = stocktransferDAO.getRequestList(companyId);
        if (requestList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return requestList;
    }

    public ArrayList processItemtList(int companyId, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {
        ArrayList requestList = new ArrayList();
        ArrayList filteredList = new ArrayList();
        int requestPtId=Integer.parseInt(stocktransferTO.getRequestPtId());
        requestList = stocktransferDAO.getItemList(requestPtId, stocktransferTO.getRequestId());
        String requestId = stocktransferTO.getRequestId();       
        if (requestList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        MrsTO mrsTO = new MrsTO();
        Iterator itr = requestList.iterator();
        StockTransferTO filterTO = null;
        while (itr.hasNext()) {
            stocktransferTO = (StockTransferTO) itr.next();
            filterTO = new StockTransferTO();
            int item_Id = Integer.parseInt(stocktransferTO.getItemId());                  
            String requestPtNewItems = mrsDAO.getLocalServicePointQuantity(item_Id, requestPtId);
            String approvePtNewItems = mrsDAO.getLocalServicePointQuantity(item_Id, companyId);
            String approvePtRCItems = mrsDAO.getMrsRcItems(item_Id,companyId);
            String requestPtRCItems = mrsDAO.getMrsRcItems(item_Id,requestPtId);
            String otherStockReq = stocktransferDAO.getOtherStockReqList(requestId, item_Id);
            
            filterTO.setCompanyName(stocktransferTO.getCompanyName());
            filterTO.setItemName(stocktransferTO.getItemName());
            filterTO.setRemarks(stocktransferTO.getRemarks());
            filterTO.setRequestPtNewItem(requestPtNewItems);
            filterTO.setRequestPtRcItem(String.valueOf(requestPtRCItems));
            filterTO.setApprovePtNewItem(approvePtNewItems);
            filterTO.setApprovePtRcItem(approvePtRCItems);
            filterTO.setItemId(stocktransferTO.getItemId());
            filterTO.setMfrCode(stocktransferTO.getMfrCode());
            filterTO.setPaplCode(stocktransferTO.getPaplCode());
            filterTO.setRequestedQty(stocktransferTO.getRequestedQty());
            filterTO.setOtherStockReq(String.valueOf(otherStockReq));
            filteredList.add(filterTO);
        }
        return filteredList;
    }

    public int processApprovedQty(int userId, ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = stocktransferDAO.insertApprovedQty(userId, List, stocktransferTO);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");

        }
        return status;
    }

    public ArrayList processGetapprovedStockList(int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList vendorList = new ArrayList();
        vendorList = stocktransferDAO.getapprovedStockList(companyId);
        if (vendorList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return vendorList;
    }

    // processGetapprovedStockListAll
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetapprovedStockListAll(int requestId) throws FPRuntimeException, FPBusinessException {

        ArrayList approvedStockListAll = new ArrayList();
        ArrayList newApprovedStockListAll = new ArrayList();
        Iterator itr ;     
        StockTransferTO stk;
        int quantity =0;
        approvedStockListAll = stocktransferDAO.getapprovedStockListAll(requestId);
        itr = approvedStockListAll.iterator();
        float temp=0.0f;
        while(itr.hasNext()){
            stk = new StockTransferTO();
            stk = (StockTransferTO)itr.next();
            if(stk.getCategoryId().equals("1011")){
                temp=Float.parseFloat(stk.getApprovedQuandity());
                quantity =(int)temp; 
                while(quantity > 0){
                    System.out.println("quantity="+quantity);
                    quantity--;
                    stk.setApprovedQuandity("1");
                    newApprovedStockListAll.add(stk);
                }                
            }else{
                newApprovedStockListAll.add(stk);    
            }
        }
        System.out.println("newApprovedStockListAll="+newApprovedStockListAll.size());
        
        if (approvedStockListAll.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return newApprovedStockListAll;
    }
    public ArrayList processGetRCApprovedStockListAll(int requestId) throws FPRuntimeException, FPBusinessException {

        ArrayList approvedStockListAll = new ArrayList();
        ArrayList newApprovedStockListAll = new ArrayList();
        Iterator itr ;
        StockTransferTO stk;
        int quantity =0;
        approvedStockListAll = stocktransferDAO.getRCApprovedStockListAll(requestId);

        if (approvedStockListAll.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return approvedStockListAll;
    }

    //processGetPriceList
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetPriceList(int requestId,int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList priceList = new ArrayList();
        priceList = stocktransferDAO.getPriceList(requestId,companyId);

        
        if(priceList.size()==0){
           //throw new FPBusinessException("EM-GEN-01"); 
        }
        return priceList;
    }
     public ArrayList processRcItemList(int requestId ,int itemId,int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList rcItemList = new ArrayList();
        rcItemList = stocktransferDAO.getRcItemList(requestId,itemId,companyId); 
         if (rcItemList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcItemList;
    }

    public int processTransferItems(ArrayList List,ArrayList tyreList, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        status = stocktransferDAO.insertTransferItems(List,tyreList, stocktransferTO);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }
     public int processTransferRcItems(ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        status = stocktransferDAO.doInsertRcItems(List, stocktransferTO);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }
     public int processTransferRcItemsNew(ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        status = stocktransferDAO.doInsertRcItemsNew(List, stocktransferTO);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }

    public ArrayList processReceivedStockListAll(StockTransferTO stocktransferTO,int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedStockListAll = new ArrayList();
        receivedStockListAll = stocktransferDAO.getReceivedStockListAll(stocktransferTO,companyId);
        if (receivedStockListAll.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedStockListAll;
    }

    public ArrayList processTransferedItemList(String gdId,StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        ArrayList transferedItems = new ArrayList();
        transferedItems = stocktransferDAO.getTransferedItems(gdId,stocktransferTO);
        if (transferedItems.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return transferedItems;
    }
    public ArrayList processTransferedRcItem(String gdId,StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        ArrayList transferedRcItems = new ArrayList();
        transferedRcItems = stocktransferDAO.getTransferedRcItems(gdId,stocktransferTO); 
         if (transferedRcItems.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return transferedRcItems;
    }

    public int processReceivedNewItems(ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;      
        status = stocktransferDAO.doReceivedNewItems(List, stocktransferTO);
        System.out.println("In BP grId"+status);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }
     public int processUpdateRcItems(ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;      
        System.out.println("Status in BP B4"+status);
        status = stocktransferDAO.updateRcItems(List, stocktransferTO); 
        System.out.println("Status in BP"+status);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }
     public int processUpdateRcItemsNew(ArrayList List, StockTransferTO stocktransferTO) throws FPRuntimeException, FPBusinessException {

        int status = 0;
        System.out.println("Status in BP B4"+status);
        status = stocktransferDAO.updateRcItemsNew(List, stocktransferTO);
        System.out.println("Status in BP"+status);
        if (status == 0) {
            throw new FPBusinessException("EM-ST-02");
        }
        return status;
    }

// Raja Integration
    
    
    
    public void processGenerateStRequest(StockTransferTO purch) throws FPBusinessException, FPRuntimeException {                
        int status =0;
        status = stocktransferDAO.doGenerateStRequest(purch);
        if(status==0){
            throw new FPBusinessException("EM-PO-01");
        }
    }
    
    
    public ArrayList processStRequestList(StockTransferTO purch) throws FPBusinessException, FPRuntimeException {                
        ArrayList reqList = new ArrayList();
        reqList = stocktransferDAO.getStRequestList(purch);
        if(reqList.size()==0){
            throw new FPBusinessException("EM-PO-01");
        }
        return reqList;
    }    
    
    public ArrayList processTyresList(int companyId) throws FPBusinessException, FPRuntimeException {                
        ArrayList reqList = new ArrayList();
        reqList = stocktransferDAO.getTyrePriceList(companyId);
        if(reqList.size()==0){
//            throw new FPBusinessException("EM-PO-01");
        }
        return reqList;
    }    
    
//Hari

public ArrayList processReqIdDetail(int requestId) throws FPBusinessException, FPRuntimeException {
        ArrayList reqIdDetail = new ArrayList();
        reqIdDetail = stocktransferDAO.getReqIdDetail(requestId);
        if(reqIdDetail.size()==0){
            throw new FPBusinessException("EM-GEN-01");
        }
        return reqIdDetail;
    }
    }
