<%-- 
    Document   : alterVoucher
    Created on : 31 Oct, 2012, 3:34:18 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> CLPL </title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.alter.voucherTypeName,'voucherTypeName')){
            return;
        }
        if(textValidation(document.alter.voucherTypeCode,'voucherTypeCode')){
            return;
        }
        if(textValidation(document.alter.description,'description')){
            return;
        }

        document.alter.action='/throttle/saveAlterVoucher.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<c:if test="${voucheralterLists != null}">
    <c:forEach items="${voucheralterLists}" var="VL">
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit Group</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Voucher Name</td>
      <td class="text2"><input name="voucherTypeName" type="text" class="form-control" value="<c:out value="${VL.voucherTypeName}"/>" size="20">
          <input type="hidden" name="voucherTypeID" value="<c:out value="${VL.voucherTypeID}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Voucher Code</td>
    <td class="text2"><input name="voucherTypeCode" type="text" class="form-control" value="<c:out value="${VL.voucherTypeCode}"/>" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Description</td>
    <td class="text2"><input name="description" type="text" class="form-control" value="<c:out value="${VL.description}"/>" size="20"></td>
  </tr>
</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

