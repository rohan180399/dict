

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


        var httpReq;
        var temp = "";
        function ajaxData()
        {
 
            var url = "/throttle/getModels1.do?mfrId="+document.purchaseReport.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() { processAjax(); } ;
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp,document.purchaseReport.modelId);
                    if('<%= request.getAttribute("modelId")%>' != 'null' ){
                        document.purchaseReport.modelId.value = <%= request.getAttribute("modelId")%>;
                    }
                }
                else
                {
                    alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                }
            }
        }
        
        
        
        function submitPage(){
            var chek=validation();
            if(chek=='true'){                              
                document.purchaseReport.action = '/throttle/purchaseReport.do';
                document.purchaseReport.submit();
            }
        }
        function validation(){
            if(document.purchaseReport.companyId.value==0){
                alert("Please Select Data for Company Name");
                document.purchaseReport.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.purchaseReport.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.purchaseReport.toDate,'TO Date')){
                return'false';
            }
            return 'true';
        }
        
        function setValues(){
            var poId='<%=request.getAttribute("poId")%>';
            if('<%=request.getAttribute("companyId")%>'!='null'){
                document.purchaseReport.companyId.value='<%=request.getAttribute("companyId")%>';
                document.purchaseReport.vendorId.value='<%=request.getAttribute("vendorId")%>';
            }
            if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.purchaseReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
            }
            if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.purchaseReport.toDate.value='<%=request.getAttribute("toDate")%>';
            }
            if('<%=request.getAttribute("categoryId")%>'!='null'){
                document.purchaseReport.categoryId.value='<%=request.getAttribute("categoryId")%>';
            }
            if('<%=request.getAttribute("mfrId")%>'!='null'){
                document.purchaseReport.mfrId.value='<%=request.getAttribute("mfrId")%>';
            }
            if('<%=request.getAttribute("modelId")%>'!='null'){
                ajaxData();
                document.purchaseReport.modelId.value='<%=request.getAttribute("modelId")%>';
            }
            if('<%=request.getAttribute("mfrCode")%>'!='null'){
                document.purchaseReport.mfrCode.value='<%=request.getAttribute("mfrCode")%>';
            } 
            if('<%=request.getAttribute("paplCode")%>'!='null'){
                document.purchaseReport.paplCode.value='<%=request.getAttribute("paplCode")%>';
            }
            if('<%=request.getAttribute("itemName")%>'!='null'){
                document.purchaseReport.itemName.value='<%=request.getAttribute("itemName")%>';
            }
            if('<%=request.getAttribute("purType")%>'!='null'){
                document.purchaseReport.purType.value='<%=request.getAttribute("purType")%>';
            }
            if(poId!='null'){                       
                document.purchaseReport.poId.value='<%=request.getAttribute("poId")%>';
            }
           
        }
        function getItemNames(){
            var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
            
        } 
    </script>
    <body onload="setValues();getItemNames();">
        <form name="purchaseReport" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Material Purchase Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
            <td width="114" height="30">&nbsp;&nbsp;Category</td>
            <td width="172" height="30"><select name="categoryId" class="form-control" style="width:125px">
                    <option value="0">-select-</option>
                    <c:if test = "${categoryList != null}" >
                        <c:forEach items="${categoryList}" var="category">
                            <option value="<c:out value="${category.categoryId}"/>"><c:out value="${category.categoryName}"/></option>
                        </c:forEach>
                    </c:if>

                </select></td>
            <td height="30">&nbsp;&nbsp;MFR</td>
            <td height="30"><select name="mfrId" onchange="ajaxData();" class="form-control">
                    <option value="0">-select-</option>
                    <c:if test = "${MfrLists != null}" >
                        <c:forEach items="${MfrLists}" var="mfr">
                            <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                        </c:forEach>
                    </c:if>
                </select></td>
            <td height="30">&nbsp;&nbsp;Model</td>
            <td height="30"><select name="modelId" class="form-control" style="width:125px">
                    <option value="0">-select-</option>
                    <c:if test = "${ModelLists != null}" >
                        <c:forEach items="${ModelLists}" var="model">
                            <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                        </c:forEach>
                    </c:if>
                </select></td>
                
        </tr>
        <tr>
            <td height="30"><font color="red">*</font>Location</td>
            <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                    <c:set var="companyId" value="${companyId}"/>
                    <c:out value="${companyId}"/>
                    <c:if test = "${LocationLists != null}" >
                        <c:forEach items="${LocationLists}" var="company">
                            <c:if test="${company.companyTypeId==1012 && company.cmpId==companyId}" >
                                <option selected value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                            </c:if>
                        </c:forEach>
                    </c:if>

                </select></td>
            <td width="148">&nbsp;&nbsp;Vendor</td>
            <td width="148"><select name="vendorId" class="form-control" style="width:125px">
                    <option value="0">-select-</option>
                    <c:if test = "${VendorLists != null}" >
                        <c:forEach items="${VendorLists}" var="vendor">
                            <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                        </c:forEach>
                    </c:if>
                </select></td>
            <td width="162" height="30">&nbsp;&nbsp;PO/ LPO No</td>
            <td width="151" height="30" class="text1"> <input name="poId" type="text" class="form-control"  size="20"></td>
        </tr>
        <tr>
            <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
            <td width="172" height="30"><input name="itemName" id="itemName" type="text"  class="form-control" value=""></td>
            <td width="111" height="30">&nbsp;&nbsp;Mfr Item Code</td>
            <td width="151" height="30"><input name="mfrCode" type="text" class="form-control" value="" size="20"></td>
            <td>&nbsp;&nbsp;PAPL Code</td>
            <td height="30"><input name="paplCode" type="text" class="form-control" value="" size="20"></td>
        </tr>
        <tr>
            <td height="30">Purchase Type</td>
            <td height="30"><select name="purType" class="form-control" style="width:130px">
                    <option value="0">-select-</option>
                    <option value="N">Direct</option>
                    <option value="Y">Vendor</option>
                </select></td>
            <td width="80" height="30"><font color="red">*</font>From Date</td>
            <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.purchaseReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
            <td width="148"><font color="red">*</font>To Date</td>
            <td width="172" height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
                <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.purchaseReport.toDate,'dd-mm-yyyy',this)"/></span></td>
        </tr>
        <tr><td colspan="6" align="center"> <input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
                <input type="hidden" value="" name="reqfor">
                </td></tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
            <br>
            <c:if test = "${purchaseReportList != null}" >
                <table width="1266" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td width="71" class="contentsub">Date</td>    
                        <td width="102" height="30" class="contentsub">Manufacturer</td>
                        <td width="82" class="contentsub">Model</td>
                        <td width="104" height="30" class="contentsub">Mfr Item Code </td>
                        <td width="82" class="contentsub">PAPL Code </td>
                        <td width="142" height="30" class="contentsub">Item Name</td>
                        <td width="78" height="30" class="contentsub">Category</td>
                        <td width="71" height="30" class="contentsub">Vendor</td>
                        <td width="71" height="30" class="contentsub">Purchase Type</td>                        
                        <td width="76" height="30" class="contentsub">PO / LPO No </td>
                    <blockquote>&nbsp;</blockquote>
                    <td width="80" height="30" class="contentsub">Accepted Qty</td>
                    <td width="80" height="30" class="contentsub">Unused Qty</td>
                    <td width="80" height="30" class="contentsub">Unit Price</td>
                    <td width="99" height="30" class="contentsub">Purchase Value (SAR) </td>
                    </tr>
                    <c:set var="directPurAmount" value="0" />
                    <c:set var="vendorPurAmount" value="0" />
                    <c:set var="unusedAmount" value="0" />
                    <% int index = 0;%> 
                    <c:forEach items="${purchaseReportList}" var="purchase"> 
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>	

                        <tr>
                            <td class="<%=classText%>"><c:out value="${purchase.purDate}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.mfrName}" /></td>
                            <td class="<%=classText%>"><c:out value="${purchase.modelName}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.mfrCode}" /></td>
                            <td class="<%=classText%>"><c:out value="${purchase.paplCode}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.itemName}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.categoryName}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.vendorName}" /> </td>

                            <c:choose>
                                <c:when test="${purchase.purType=='n' || purchase.purType=='N'}" >
                                    <td class="<%=classText%>" height="30">Direct Purchase</td>
                                    <c:set var="directPurAmount" value="${directPurAmount + purchase.purchaseAmt}" />
                                </c:when>
                                <c:otherwise>
                                    <td class="<%=classText%>" height="30">Vendor Purchase</td>
                                    <c:set var="vendorPurAmount" value="${vendorPurAmount + purchase.purchaseAmt}" />
                                </c:otherwise>
                            </c:choose>


                            <td class="<%=classText%>" height="30"><c:out value="${purchase.poId}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.aqty}" /></td>    
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.unusedQty}" /></td>    
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.unitPrice}" /></td>    
                            <td class="<%=classText%>" height="30"><c:out value="${purchase.purchaseAmt}" /></td>
                            <c:set var="unusedAmount" value="${unusedAmount + (purchase.unusedQty * purchase.unitPrice) }" />
                        </tr>  
                        <% index++;%>
                    </c:forEach>
                    <tr >
                        <td width="71" class="text2">&nbsp;</td>    
                        <td width="102" height="30" class="text2">&nbsp;</td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="104" height="30" class="text2">&nbsp; </td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="142" height="30" class="text2">&nbsp;</td>
                        <td width="100" height="30" class="text2"><b>Vendor Purchase Amount</b></td>
                        <td width="99" height="30" class="text2">
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${vendorPurAmount}" pattern="##.00"/></b>  
                        </td>
                        <td width="78" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>                                               
                        <td width="71" height="30" class="text2">&nbsp;</td>                                                                   
                        <td width="71" height="30" class="text2">&nbsp;</td>    
                        <td width="71" height="30" class="text2">&nbsp;</td>    

                    </tr>
                    <tr >
                        <td width="71" class="text2">&nbsp;</td>    
                        <td width="102" height="30" class="text2">&nbsp;</td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="104" height="30" class="text2">&nbsp; </td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="142" height="30" class="text2">&nbsp;</td>
                        <td width="100" height="30" class="text2"><b>Direct Purchase Amount</b></td>
                        <td width="80" height="30" class="text2">
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber  value="${directPurAmount}" pattern="##.00"/> </b> 
                        </td>  
                        <td width="78" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>                        
                        <td width="71" height="30" class="text2">&nbsp;</td>                                                                      
                        <td width="71" height="30" class="text2">&nbsp;</td> 
                        <td width="80" height="30" class="text2">&nbsp;</td> 

                    </tr>
                    <tr >
                        <td width="71" class="text2">&nbsp;</td>    
                        <td width="102" height="30" class="text2">&nbsp;</td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="104" height="30" class="text2">&nbsp; </td>
                        <td width="82" class="text2">&nbsp;</td>
                        <td width="142" height="30" class="text2">&nbsp;</td>
                        <td width="100" height="30" class="text2"><b>Unused Stock Value</b></td>
                        <td width="80" height="30" class="text2">
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber  value="${unusedAmount}" pattern="##.00"/></b>  
                        </td>  
                        <td width="78" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>
                        <td width="71" height="30" class="text2">&nbsp;</td>                        
                        <td width="71" height="30" class="text2">&nbsp;</td>                                                                      
                        <td width="71" height="30" class="text2">&nbsp;</td> 
                        <td width="80" height="30" class="text2">&nbsp;</td> 

                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>

