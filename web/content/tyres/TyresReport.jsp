<%-- 
    Document   : TyresReport
    Created on : Mar 19, 2012, 3:31:22 PM
    Author     : Senthil
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Tyres Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        function validate() {          
            if(vehicleRegNo.getElementById("vehicleRegNo").value == ""){
                alert("Vehicle No is Not Filled");
                return false;
            } else {
                return true;
            }

        }



        </script>




    </head>
<body>
<form name="tyreReportVehicle" action="#" method="post" onsubmit="return validate();" >

<%

String vehicleRegNo = (String) request.getAttribute("vehicleRegNo");
if(vehicleRegNo == null) {
    vehicleRegNo = "";
}

String fromDate = (String) request.getAttribute("fromDate");
if(fromDate == null){
    fromDate = "";
}

String toDate = (String) request.getAttribute("toDate");
if(toDate == null) {
    toDate = "";
}

%>

    <table align="center">
        <tr>
            <td>
 <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Vehicle wise Tyres Report</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Vehicle No</td><td>
                <input type="text" name="vehicleRegNo" id="vehicleRegNo" class="textbox" value="<%=vehicleRegNo%>" />
            </td>
            
            <td><font color="red">*</font>From Date</td><td><input name="fromDate" id="fromDate" value="<%=fromDate%>"  readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tyreReportVehicle.fromDate,'dd-mm-yyyy',this)"/></td>
            <td><font color="red">*</font>To Date</td><td><input name="toDate" id="toDate" value="<%=toDate%>" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tyreReportVehicle.toDate,'dd-mm-yyyy',this)"/></td>
            <td colspan="2"><input type="submit" name="search" value="View List" onClick="submitpage()" class="button" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
    <br>

    <%
int sino = 0;
String tdClassName = "text1";
%>

    <table width="900" align="center" cellspacing="0" cellpadding="0" class="table2">
    <tr>
        <td  class="contenthead">S.No</td>
        <td  class="contenthead">Tyre No</td>
        <td  class="contenthead">Make</td>
        <td  class="contenthead">In Date</td>
        <td  class="contenthead">Out Date</td>
        <td  class="contenthead">Type</td>
        <td  class="contenthead">Total KM</td>
    </tr>

    <c:if test="${tyresDetails != null}">
    <c:forEach items="${tyresDetails}" var="tyres">
<%
sino++;
if((sino%2) == 0){
tdClassName = "text2";
} else {
tdClassName = "text1";
}

%>

    <tr>
        <td  class="<%=tdClassName%>"><%=sino%></td>
        <td  class="<%=tdClassName%>"><c:out value="${tyres.tyreNumber}"/></td>
        <td  class="<%=tdClassName%>">MRF</td>
        <td  class="<%=tdClassName%>"><c:out value="${tyres.fromDate}"/></td>
        <td  class="<%=tdClassName%>">
            <c:if test="${tyres.toDate == '00-00-0000'}" >
             Till Date
            </c:if>
            <c:if test="${tyres.toDate ne '00-00-0000'}" >
            <c:out value="${tyres.toDate}"/>
            </c:if>
        </td>
        <td  class="<%=tdClassName%>">New</td>
        <td  class="<%=tdClassName%>">10,000</td>
    </tr>
</c:forEach>

            </c:if>

  


    </table>

</td>
        </tr>
        <tr>
            <td>
<h2>Summary</h2>
</td> </tr>
        <tr><td>

 <table align="left" width="250" cellspacing="0" cellpadding="2">
    <tr>
        <td class="texttitle1">1.</td>
        <td class="texttitle1">Avg. KM Run</td>
        <td class="texttitle1">10,000</td>
    </tr>
    <tr>
        <td class="texttitle1">2.</td>
        <td class="texttitle1">Apollo 9X20 </td>
        <td class="texttitle1">10,000</td>
    </tr>
     <tr>
        <td class="texttitle1">3.</td>
        <td class="texttitle1">MRF 9X20 </td>
        <td class="texttitle1">10,000</td>
    </tr>
 </table>


    </td>
    </tr>
</table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
