




<%--This is the schedule processor code which is used to customise the schedule genearted by ceowlf tags below --%>


<%--From here its Displaying Fluidity Reports-- --%>

     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
<script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
</script>

<body onLoad="setValues()" >

<form name="schedule" method="post" >


            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">RC Expenses</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
    <td height="20" > From Date : </td>
    <td height="20">
       <input type="text" class="form-control" readonly name="fromDate"   >
       <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.schedule.fromDate,'dd-mm-yyyy',this)"/>
    </td>

    <td height="20"> To Date : </td>
    <td height="20">
        <input type="text" class="form-control" readonly name="toDate"  onchange="StartDateCheck(this);" >
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.schedule.toDate,'dd-mm-yyyy',this)"/>
    </td>
    <td><input type="button" class="button" name="search" value="search" onclick="searchSubmit()"></td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

<br/>

<%
if(request.getAttribute("serviceSummary") != null){

ArrayList serviceSummary = (ArrayList) request.getAttribute("serviceSummary");
System.out.println("serviceSummary in Scedule Deviation jsp"+serviceSummary.size());

int siz = serviceSummary.size();
String[][] arrData = new String[siz][3];

int i1 = 0;

Iterator itr = serviceSummary.iterator();
ReportTO reportTO = new ReportTO();
while(itr.hasNext()){
reportTO = (ReportTO) itr.next();
int complete = Integer.parseInt(reportTO.getCompleted());
int planned = Integer.parseInt(reportTO.getPlanned());
arrData[i1][0] = reportTO.getNotPlanned();
arrData[i1][1] = (complete+planned)+"";
arrData[i1][2] = reportTO.getBillingCompleted();
System.out.println("arrData-----"+arrData[i1][0]+"-"+arrData[i1][1]+"After Add-"+arrData[i1][2]+"-"+complete+"-"+planned);
i1++;
}



//String  strXML = "<graph caption='Management Report' XAxisName='CompanyName' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";
String  []strXML = new String[siz];
       //




String []strDataProdA =new String[siz];
String []strDataProdB =new String[siz];
String []strDataProdC =new String[siz];




for(int i=0;i<arrData.length;i++){
strXML[i] += "<chart isSliced='1' slicingDistance='3' decimalPrecision='0'>";
strDataProdA[i] += "<set name ='Created' color='99CC00' alpha='85' value='" + arrData[i][0] + "' />";
strDataProdB[i] += "<set name ='Planned' color='FFCCAA'  alpha='85' value='" + arrData[i][1] + "' />";
strDataProdC[i] += "<set name ='Billing Completed' color='444444' alpha='85' value= '"+ arrData[i][2]+"' />";
strXML[i] +=  strDataProdA[i] + strDataProdB[i] + strDataProdC[i]  + "</chart>";
System.out.println("strXML["+i+"]"+strXML[i]);
}


//Assemble the entire XML now


System.out.println("strXML-->"+strXML);
%>


<table width='70%' align="center" bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>
<c:if test = "${serviceSummary != null}" >
        <tr>
            <td class="contentsub">S.No</td>
            <td class="contentsub">Company Name</td>





            <td class="contentsub">Created</td>
            <td class="contentsub">Planned</td>
            <td class="contentsub">Completed</td>
            <td class="contentsub">Billing Completed</td>
        </tr>
        <%
					int index = 1 ;
                    %>
    <c:forEach items="${serviceSummary}" var="serviceSummary">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>

        <td class="<%=classText %>"><c:out value="${serviceSummary.companyName}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.notPlanned}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.planned}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.completed}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.billingCompleted}"/></td>






</tr>
<%
            index++;
                        %>

   </c:forEach>
</c:if>



</table>
<table align="center" style="margin-left:10px;" border="0">
    <tr>
       

<%
int i=0;
%>

<c:forEach items="${serviceSummary}" var="serviceSummary">
 <td>
             <table align="center" style="margin-left:10px;" border="0">
                 
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_Funnel.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML[i] %>" />
                <jsp:param name="chartId" value="productSales" />
                <jsp:param name="chartWidth" value="150" />
                <jsp:param name="chartHeight" value="250" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                    
                </td>
            </tr>
           
            <tr>
                <td>
                    <c:out value="${serviceSummary.companyName}"/>
                   
                </td>
            </tr>
            <% i++; %>
        </table>
      </td>
      
    </c:forEach>
       
    </tr>
</table>

<%
}
%>

</body>

<script>
    function searchSubmit()
    {
        if(isEmpty(document.schedule.fromDate.value))
            {
                alert("Select From Date");
                document.schedule.fromDate.focus();
                return false;
            }
            else if(isEmpty(document.schedule.toDate.value))
            {
                alert("Select End Date");
                document.schedule.toDate.focus();
                return false;
            }
        else
            {
                document.schedule.action="/throttle/scheduleDeviationGraphData.do";
                document.schedule.submit();
            }
    }

function setValues(){
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.schedule.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.schedule.toDate.value='<%= request.getAttribute("toDate") %>';
    }
}
Date.fromDDMMYYYY = function (s) {return (/^(\d\d?)\D(\d\d?)\D(\d{4})$/).test(s) ? new Date(RegExp.$3, RegExp.$2 - 1, RegExp.$1) : new Date (s)}

            function StartDateCheck(field){
                var val = field.value;                
                var startDate = Date.fromDDMMYYYY (val);
                var currentDate = new Date();
                currentDate.setHours(0);
                currentDate.setMinutes(0);
                currentDate.setSeconds(0);
                currentDate.setMilliseconds(0);

                if (startDate < currentDate){
                    alert("Start date should current date or feature date!!");
                    field.value="";
                    field.focus();
                }
            }
</script>




