
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */

            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                //  alert(value);
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleShippingBillReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleShippingBillReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }
//
//            function viewCustomerProfitDetails(tripIds) {
////            alert(tripIds);
//                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
//            }
        </script>
     <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Shipping Billz Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                                <table style="width:100%" align="center" border="0" class="tabouterborder" >
                                    <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Trip end but not closed</td></tr>
                                      <tr height="40">
                                        <td class="tabtext"><font color="red">*</font>From Date</td>
                                        <td class="tabtext" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" style="width:180px;height:25px;color:black" ></td>
                                        <td class="tabtext"><font color="red">*</font>To Date</td>
                                        <td class="tabtext" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" style="width:180px;height:25px;color:black"></td>
<!--                                    </tr>
                                    <tr>-->
                                        <td class="tabtext"  align="right"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;">&nbsp;&nbsp;</td>
                                        <td class="tabtext" ><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></td>
                                    </tr>
                                </table>
            <c:if test = "${tripDetails != null}" >
<!--                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>-->
                <br>
                 <table class="table table-info mb30" >
                    <thead height="30">
                       
                        <tr >
                            <th >S no</th>
                            <th >Container no</th>
                            <th >container Size</th>
                            <th >container Type </th>
                            <th >shippingBillNo</th>
                            <th >shippingBillDate</th>
                            <th >gateInDate </th>
                            <th >requestNo</th>
                            <th >VehicleNo</th>
                            <th >containerQty</th>
                            <th >G.R.no</th>
                            <th >G.R Date</th>
                            <th >updateStatus</th>
                            <th >tripCount</th>
                           
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                    <c:forEach items="${tripDetails}" var="csList">
                       

                            <tr  height="30">
                                <td align="center" class="text1"><%=sno%></td>
                            
                            <td align="center" class="text1"><c:out value="${csList.containerNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.containerSize}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.containerType}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.shippingBillNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.shippingBillDate}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.gateInDate}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.requestNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.containerQty}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.grNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.grDate}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.updateStatus}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.tripCount}"/></td>
                           

                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
