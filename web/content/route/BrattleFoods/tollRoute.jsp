<%-- 
    Document   : tollRoute
    Created on : Jan 7, 2016, 4:17:27 PM
    Author     : SriniEntitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Toll Route</title>
    </head>
    <body>
        <%

            if (request.getAttribute("availTollList") != null) {

        %> 

        <table align="center" border="1" cellpadding="0" cellspacing="0" width="500" id="bg"> 
            <tr>
                <td class="contenthead" valign="center" height="35" >Available Tolls </td>
                <td class="contenthead" height="35"></td>
                <td class="contenthead" valign="center" height="35">Assigned Tolls </td>
            </tr>
            <tr>
                <td valign="top">
                    <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                        <tr>  
                            <td width="150"><select style="width:200px;"  id="availableFunc" multiple size="10" name="availableFunc">
                                    <c:if test = "${availTollList != null}" >
                                        <c:forEach items="${availTollList}" var="toll"> 
                                            <option value='<c:out value="${toll.tollId}" />'><c:out value="${toll.tollName}" /></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                        </tr>
                    </table>                       

                </td>   

                <td height="100" valign="center">
                    <table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
                        <tr>
                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="form-control" value=">" onClick="copyAddress()"></td></tr>
                        <tr >
                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="form-control" value="<" onClick="copyAddress1()"></td></tr>


                    </table></td>
                <td valign="top">

                    <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                        <tr>
                            <td width="150">
                                <select style="width:200px;" id="assignedfunc" multiple size="10" name="assignedfunc" >
                                    <c:if test = "${assignedTollList != null}" >
                                        <c:forEach items="${assignedTollList}" var="assigned"> 
                                            <option value='<c:out value="${assigned.tollId}" />'><c:out value="${assigned.tollName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                        </tr>
                    </table></td>
            </tr> 
        </table>
        <br>
    <center>  
        <c:if test="${routeId != null && routeId!='0'}">
            <input type="button" name="updatetoll" value="Update Toll"  class="button" onclick="updateToll();">
            <input type="hidden" name="routeIds" id="routeIds" value='<c:out value="${routeId}" />' >

        </c:if>
    </center>
    <% }%>
   

</body>
</html>
