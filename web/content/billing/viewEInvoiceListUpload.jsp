<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        
        if (value == 'Proceed') {
            
            document.upload.action = "/throttle/saveEInvoiceUpload.do?param=" + value;
            document.upload.submit();
        } else {
//            alert("hai");
        var invoiceId = document.upload.invoiceId.value;

//alert("invoiceId===="+invoiceId);
            var file = document.upload.importBpclTransaction.value;
            if (file == '') {
                alert("Please select any Excel");
                return false;
            } else {
                document.upload.action = "/throttle/eInvoiceUpload.do?param=" + value +"&invoiceId="+invoiceId;
                document.upload.submit();
            }
        }
    }
</script>

<html>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <c:if test="${orderUploadListSize <= 0}">
                        <form name="upload" method="post" enctype="multipart/form-data">
                        </c:if>
                        <c:if test="${orderUploadListSize > 0}">
                            <form name="upload" method="post"> 
                            </c:if>
                            <%@ include file="/content/common/message.jsp" %>
                            <br>
                            <br>

                            <%
                            if(request.getAttribute("errorMessage")!=null){
                            String errorMessage=(String)request.getAttribute("errorMessage");                
                            %>
                            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                                    <%}%>
                                    <c:if test="${orderUploadListSize <= 0}">
                                <table class="table table-info mb30 table-hover" style="width:50%">
                                    <tr>
                                        <td style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:20px;" colspan="2">E-Invoice Upload</td>
                                    </tr>
                                    <tr>
                                        <td class="text2"><font size="4">Select E-Invoice Excel</font></td>
                                        <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransactions">
                                            <input type="hidden" name="invoiceId" id="invoiceId" value="<%= request.getParameter("invoiceId")%>">
                                            <br>
                                            <a href="Dict_Einvoice_Sample.xls"><font color="red" size="4">Sample XLS</font></a></td>                             
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2" class="text1" align="center" ><input type="button" class="btn btn-info" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                                    </tr>
                                </table>
                            </c:if>
                            <br>
                            <br>
                            <br>
                            <% int i = 1 ;%>
                            <% int index = 0;%> 
                            <%int oddEven = 0;%>
                            <%  String classText = "";%>    
                            <c:if test="${orderUploadListSize > 0}">
                                <table class="table table-info mb30 table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="20">E-Invoice Upload Details&nbsp;</th>
                                        </tr>                                        
                                        <tr>
                                            <th >S No</th>
                                            <th >Invoice Id</th>
                                            <th >Document Date</th>
                                            <th >Document Number</th>
                                            <th >Document Type</th>
                                            <th >Seller GSTIN</th>
                                            <th >Buyer GSTIN</th>
                                            <th >Status</th>
                                            <th >Acknowledgement Number</th>
                                            <th >Acknowledgement Date</th>
                                            <th >IRN</th>
                                            <th >Signed QR Code</th>
                                            <th >EWB Number</th>
                                            <th >EWB Date</th>
                                            <th >EWB Valid Till</th>
                                            <th >Info</th>
                                            <th >Error</th>
                                            
                                        </tr>
                                    </thead>
                                    <c:forEach items="${orderUploadList}" var="bpcl">
                                        <%
                                      oddEven = index % 2;
                                      if (oddEven > 0) {
                                          classText = "text2";
                                      } else {
                                          classText = "text1";
                                        }
                                        %>
                                        <tr>
                                            <input type="hidden" name="invoiceCode" id="invoiceCode" value="<c:out value="${bpcl.invoiceCode}"/>">
                                            <td class="<%=classText%>" ><%=i++%></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="invoiceCode" id="invoiceCode" value="<c:out value="${bpcl.invoiceCode}"/>" /><c:out value="${bpcl.invoiceCode}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="documentDate" id="documentDate" value="<c:out value="${bpcl.documentDate}"/>" /><c:out value="${bpcl.documentDate}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="documentNumber" id="documentNumber" value="<c:out value="${bpcl.documentNumber}"/>" /><c:out value="${bpcl.documentNumber}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="documentType" id="documentType" value="<c:out value="${bpcl.documentType}"/>" /><c:out value="${bpcl.documentType}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="sellerGst" id="sellerGst" value="<c:out value="${bpcl.sellerGst}"/>" /><c:out value="${bpcl.sellerGst}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="buyerGst" id="buyerGst" value="<c:out value="${bpcl.buyerGst}"/>" /><c:out value="${bpcl.buyerGst}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="status" id="status" value="<c:out value="${bpcl.status}"/>" /><c:out value="${bpcl.status}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="accNum" id="accNum" value="<c:out value="${bpcl.accNum}"/>" /><c:out value="${bpcl.accNum}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="accDate" id="accDate" value="<c:out value="${bpcl.accDate}"/>" /><c:out value="${bpcl.accDate}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="irn" id="irn" value="<c:out value="${bpcl.irn}"/>" /><c:out value="${bpcl.irn}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="signedQr" id="signedQr" value="<c:out value="${bpcl.signedQr}"/>" /><c:out value="${bpcl.signedQr}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="ewbNum" id="ewbNum" value="<c:out value="${bpcl.ewbNum}"/>" /><c:out value="${bpcl.ewbNum}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="ewbDate" id="ewbDate" value="<c:out value="${bpcl.ewbDate}"/>" /><c:out value="${bpcl.ewbDate}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="ewbValid" id="ewbValid" value="<c:out value="${bpcl.ewbValid}"/>" /><c:out value="${bpcl.ewbValid}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="info" id="infoe" value="<c:out value="${bpcl.info}"/>" /><c:out value="${bpcl.info}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="error" id="error" value="<c:out value="${bpcl.error}"/>" /><c:out value="${bpcl.error}"/></font></td>
                                            
                                        </tr>
                                        <%index++;%>
                                    </c:forEach>
                                </table>
                                <br>
                                <br>
                                <br>
                                <center>
                                    <input type="button"  class="btn btn-info" value="Proceed" onclick="submitPage(this.value)"/>
                                </center>

                            </c:if>
                            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>