<%-- 
    Document   : reqIdDetail
    Created on : May 6, 2010, 4:45:33 PM
    Author     : root
--%>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>
        <%@ page import="java.util.*" %>
        <title>Goods Delivery Details</title>




    </head>
    <body>


        <script>
            function submitPage(val){
                if(val=='approve'){
                    document.mpr.status.value="APPROVED"
                    document.mpr.action="/throttle/approveMpr.do"
                    document.mpr.submit();
                }else if(val=='reject'){
                document.mpr.status.value="REJECTED"
                document.mpr.action="/throttle/approveMpr.do"
                document.mpr.submit();
            }
        }


        function print(val)
        {
            var DocumentContainer = document.getElementById('print'+val);
            var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            //WindowObject.close();
        }


        </script>

        <form name="mpr"  method="post" >
            <br>


            <%
            int index = 0;
            ArrayList woDetail = new ArrayList();
            woDetail = (ArrayList) request.getAttribute("reqIdDetail");
            StockTransferTO purch = new StockTransferTO();
            StockTransferTO headContent = new StockTransferTO();

            headContent = (StockTransferTO) woDetail.get(0);
            int itemNameLimit = 25;
            int mfrCodeLimit = 10;
            String itemName = "";
            String mfrCode = "";

            int listSize = 10;

            for (int i = 0; i < woDetail.size(); i = i + listSize) {
            %>
            <div id="print<%=i%>" >

                <style type="text/css">
                    .header {font-family:Arial;
                        font-size:15px;
                        color:#000000;
                        text-align:center;
                        padding-top:10px;
                        font-weight:bold;
                    }
                    .border {border:1px;
                        border-color:#000000;
                        border-style:solid;
                    }
                    .text1 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:14px;
                        color:#000000;
                    }
                    .text2 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:16px;
                        color:#000000;
                    }
                    .text3 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:18px;
                        color:#000000;
                    }

                </style>

                <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td valign="top" colspan="2" height="30" align="center" class="border"><strong>STOCK REQUEST NOTE</strong></td>
                    </tr>

                    <tr>

                        <td height="60" valign="top">
                            <table height="70" width="700" border="0" cellpadding="0" cellspacing="0" class="border">
                                <tr>
                                    <Td colspan="2" class="text3"  height="30" align="center"  width="600">
                                        <strong> Your Company Name.</strong>
                                    </Td>
                                </tr>
                                <tr>
                                    <Td colspan="2"  align="center" class="text1" width="600">address 1,
                                      address 2.
                                      PHONE-xxx xxx.
                                    </Td>
                                </tr>
                                <tr>
                                    <Td  width="300" align="left" class="text1" style="padding-left:10px; ">Request No.&nbsp;<strong> <%= headContent.getRequestId()%></strong>	</Td>
                                    <Td width="300" align="right" class="text1" style="padding-right:10px; ">Date .&nbsp<strong><%= headContent.getCreatedDate() %> </strong></Td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <c:if test = "${reqIdDetail != null}" >

                        <tr>
                            <td height="45"  valign="top" >
                                <%  index = 0;%>
                                <table width="700" height="30" border="0" cellpadding="0" cellspacing="0" class="border">

                                    <c:forEach items="${reqIdDetail}" var="stDetail">
                                        <% if (index == 0) {%>

                                        <tr>
                                            <Td class="text1"  height="30" style="padding-left:20px;" align="left">
                                                From.&nbsp;
                                                <c:if test = "${operationPointList != null}" >
                                                    <c:forEach items="${operationPointList}" var="Dept">
                                                        <c:if test = "${Dept.spId==stDetail.fromSpId }" >
                                                            <strong><c:out value="${Dept.spName}"/></strong>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </Td>

                                            <Td class="text1" align="right" style="padding-right:20px;" >
                                                To.&nbsp

                                                <c:if test = "${operationPointList != null}" >
                                                    <c:forEach items="${operationPointList}" var="Dept">
                                                        <c:if test = "${Dept.spId==stDetail.toSpId }" >
                                                            <strong> <c:out value="${Dept.spName}"/></strong>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </Td>
                                        </tr>


                                        <% index++;
                }%>
                                    </c:forEach>

                                </table>
                            </Td>
                        </tr>
                    </c:if>



                    <tr>
                    <Td valign="top" colspan="2">

                       <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">



                            <tr>
        <Td class="text2" width="5" height="28" align="center" valign="top" class="border" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;"> <strong>NO.</strong></Td>
        <Td class="text2" valign="top" width="50" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>FOLIO NO</strong></Td>
        <Td class="text2"  valign="top" width="220" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>NOMENCLATURE</strong></Td>
        <Td  class="text2" valign="top" width="20" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></Td>
        <Td  class="text2" valign="top" width="40" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>Req.Qty</strong></Td>
        </tr>

                            <%	index = 0;
                for (int j = i; (j < (listSize + i)) && (j < woDetail.size()); j++) {
                    purch = new StockTransferTO();
                    purch = (StockTransferTO) woDetail.get(j);
                         mfrCode = purch.getPaplCode();
                     itemName  = purch.getItemName();

                if(purch.getPaplCode().length() > mfrCodeLimit ){
                    mfrCode = mfrCode.substring(0,mfrCodeLimit-1);
                }if(purch.getItemName().length() > itemNameLimit ){
                    itemName = itemName.substring(0,itemNameLimit-1);
                }

                            %>



                            <tr>
                                <Td valign="top" HEIGHT="20" class="text1" width="25"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right" ><%= j + 1 %></td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="15"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= mfrCode %> &nbsp; </td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="220"  style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %></</td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="20"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= purch.getUomName() %> </td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="20"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= purch.getIssuedQty() %></td>
                               
                            </tr>
                            <%
                    index++;
                }
                            %>

                            <% while (index <= 10) {%>
                            <tr>
                                <Td valign="top" HEIGHT="20"  class="text1" width="20"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="80"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left">   &nbsp;</td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="200"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                                <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                                
                            </tr>
                            <% index++;
                } %>


                            


                        </table>
                    </td>


                    <Tr>
                        <Td width="110" class="text1" >&nbsp;</Td>
                        <Td width="336">&nbsp;</Td>
                        <Td width="154" class="text1" align="right" >&nbsp;</Td>
                    </Tr>


                    <Tr>
                        <Td valign="top" colspan="3">
                            <table width="700" height="50" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

                                <Tr>
                                    <Td width="200" height="30" class="text1" >&nbsp;</Td>
                                    <Td width="200" height="30" class="text1" >&nbsp;</Td>
                                    <Td width="200" height="30" class="text1" > &nbsp; </Td>
                                </Tr>


                                <Tr>
                                    <Td width="200" class="text1" style="padding-left:10px;" align="left" ><strong>Issued  by</strong></Td>
                                    <Td width="200" class="text1" align="center" ><strong>Receiver's Signature</strong></Td>
                                    <Td width="200" class="text1"  style="padding-right:10px;" align="right"  align="center" ><strong>Authorised Signatory</strong></Td>
                                </Tr>

                            </table>
                        </Td>
                    </Tr>
                </table>
            </div>
            <center>
                <input type="button" class="button" name="Print" value="Print" onClick="print(<%=i%>);" > &nbsp;
            </center>
            <% } %>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

