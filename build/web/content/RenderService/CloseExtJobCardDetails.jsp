
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
<%@ page import="ets.domain.renderservice.business.ProblemActivityTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript" src="/throttle/js/validate.js"></script>    
    <script>
        function deleteRow() {
            try {
            var table = document.getElementById("addRows");
            //rowCount = table.rows.length;
 	    //alert(rowCount);

            for(var i=1; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[7].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {

                    if(rowCount <= 2) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    
                    var sno = i-1;
                    alert(sno);
                    var elementName = "amt"+sno;

                    var amountVal = document.getElementById(elementName).value;
                    alert(amountVal);
                    document.getElementById("totalValue").innerHTML = parseFloat(document.getElementById("totalValue").innerHTML) - parseFloat(amountVal);
                    document.getElementById("totalValue").innerHTML = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);

                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }

            }

                if(rowCount > 2){
                        document.jobCardClose.deleterowbutton.disabled = false;
                }else {
                        document.jobCardClose.deleterowbutton.disabled = true;
                }

            }catch(e) {
                alert(e);
            }
        }
        var rowCount=2;
          var sno=0;
          var style="text1";
          function addRow()
          {

               if(rowCount%2==0){
                   style="text2";
               }else{
                    style="text1";
               }
               sno++;
               var tab = document.getElementById("addRows");
               var newrow = tab.insertRow(rowCount);

               var cell = newrow.insertCell(0);
               var temp=sno-1;
               var cell0 = "<td class='text1' height='25' ><input type='hidden' name='selectedindex' value='"+sno+"'>"+sno+" </td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell0;

               
               cell = newrow.insertCell(1);
               var cell1 = "<td class='text1' height='25'><input name='acode' value='acode"+sno+"' type='hidden'><input name='acode"+sno+"' id='acode"+sno+"' size='10'  value='' type='text' onChange='callAjax(this.value,"+sno+");'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell1;

               cell = newrow.insertCell(2);
               cell1 = "<td class='text1' height='25'><input id='section"+sno+"' size='10' name='section"+sno+"' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell1;

               cell = newrow.insertCell(3);
               var cell2 = "<td class='text1' height='25'><input  id='activityName"+sno+"' name='activityName"+sno+"' value='' readonly type='text'><input  id='activityId"+sno+"' name='activityId"+sno+"' value='' type='hidden'></td>";
               cell.setAttribute("className",style);
               //alert(cell2);
               cell.innerHTML = cell2;

               cell = newrow.insertCell(4);
               var cell3 = "<td class='text1' height='25'><input  id='qty"+sno+"'  size='10' name='qty"+sno+"' onFocusOut='computeLabor(this.value,"+sno+");' value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell3;

               cell = newrow.insertCell(5);
               var cell4 = "<td class='text1' height='25'><input  id='rate"+sno+"'  size='10' name='rate"+sno+"' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell4;

               cell = newrow.insertCell(6);
               var cell5 = "<td class='text1' height='25'><input  id='amt"+sno+"'  size='10' name='amt"+sno+"' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell5;

               cell = newrow.insertCell(7);
               var cell6 = "<td class='text1' height='25' ><input type='checkbox' name='deleteInd' value='"+sno+"'   > </td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell6;

                var elementName = "acode"+sno;
                document.getElementById(elementName).focus();
                //document.getElementById(elementName).select();

               rowCount++;
               document.jobCardClose.deleterowbutton.disabled = false;
          }

        function submitPage()
        {
            document.getElementById("savePage").style.visibility = "hidden";
            var selectedIndex=document.getElementsByName("selectedIndex");
            var status=document.getElementsByName("approve");           
            var cntr = 0;
            var temp;
            var c=0; 
            var s=0;

            for(var i=0;i<status.length;i++) {                                
                if(status[i].checked==true){
                    s++;                   
                }
            }                           
            
            if(s==0){
                alert("Please select Quality Approval");
            }else if(textValidation(document.jobCardClose.remarks,'remarks')){
                 }
                 else{                           
                 document.jobCardClose.action="jobCardClosure.do";
                 document.jobCardClose.submit();
            }
        }
        
    function addActivity(probId,probName){        
        window.open('/throttle/content/Maintenance/addActivity.jsp?prId='+probId+'&propName='+probName , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }        
        
    function addCause(probId,probName){        
        window.open('/throttle/content/Maintenance/addCause.jsp?prId='+probId+'&propName='+probName , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }        
    
    function refreshPage(){
                 document.jobCardClose.action="/throttle/closeJobCardDetails.do";
                 document.jobCardClose.submit();        
    }    


function computeLabor(qty, sno){
          var elementName = "qty"+sno;
          if( qty == '' || qty == 0){
            alert("Please Enter Valid Data");
            document.getElementById(elementName).select();
            document.getElementById(elementName).focus();
            return;
          }
          var aqty = document.getElementsByName("qty");
          var rate = document.getElementsByName("rate");
            
            //alert(elementName);
            elementName = "rate"+sno;
            var rate = document.getElementById(elementName).value;
            var amountVal = parseFloat(rate)*parseFloat(qty);

            //alert(amountVal);
            elementName = "amt"+sno;
            document.getElementById(elementName).value=amountVal;
            
            var tamt = 0;
            elementName = "rate"+sno;
            var elementName1 = "rate"+sno;
            //alert(sno);
            for(var k = 1; k <= sno; k++){
                elementName = "qty"+k;
                elementName1 = "rate"+k;
                tamt = tamt + parseFloat(document.getElementById(elementName).value)*parseFloat(document.getElementById(elementName1).value);
            }
            document.getElementById("totalValue").innerHTML = tamt;
            //document.getElementById("totalValue").innerHTML = parseFloat(document.getElementById("totalValue").innerHTML) + parseFloat(amountVal);
            document.getElementById("totalValue").innerHTML = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
}
var httpReq;
function callAjax(acode, tempsno){

          var stat = false;
          if( acode == '' ){
            alert("Please Enter ActivityCode");
            var elementName = acode+sno;
            document.getElementById(elementName).focus();
            return;
          }else {
            var acodes = document.getElementsByName("acode");
            var elemntName="";
            var j = acodes.length -1;
            for(var i=0; i<j ; i++){
                elemntName = acodes[i].value;                
                //alert(document.getElementById(elemntName).value +":"+ acode);
                if(document.getElementById(elemntName).value == acode){
                    alert("This Activity Code Already Entered.");
                    elemntName = acodes[j].value;
                    document.getElementById(elemntName).value = "";
                    elemntName = acodes[i].value;
                    document.getElementById(elemntName).select();
                    stat = true;
                }
            }
          }

       if(!stat){
       var sno = parseInt(tempsno);
       var modelId = document.jobCardClose.vehicleModelId.value;
       var mfrId = document.jobCardClose.vehicleMfrId.value;

        var url='/throttle/getActivityDetails.do?acode='+acode+'&modelId='+modelId+'&mfrId='+mfrId;
        //alert(url);
        
            if (window.ActiveXObject){
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest){
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() { processAjax(sno);};
            httpReq.send(null);
    }

}
function processAjax(sno)
{
    if (httpReq.readyState == 4)
        {
            if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    if(temp != "" && temp != null){
                    var activityInfo=temp.split('~');
                    var elementName = "activityName"+sno;
                    //alert(elementName);
                    document.getElementById(elementName).value=activityInfo[4];
                    elementName = "section"+sno;
                    
                    var tvar = eval("document.jobCardClose.section"+sno);
                    tvar.value = activityInfo[1];
//                    document.getElementById(elementName).value=activityInfo[1];
                    
                    elementName = "rate"+sno;
                    
                    document.getElementById(elementName).value=activityInfo[7];
                    elementName = "amt"+sno;                    
                    document.getElementById(elementName).value=activityInfo[7];

                    elementName = "activityId"+sno;
                    document.getElementById(elementName).value=activityInfo[2];

                    elementName = "qty"+sno;
                    
                    document.getElementById(elementName).value=1;
                    var elementName = "qty"+sno;
                    document.getElementById(elementName).select();
                }else {
                    alert("Activity Code is either invalid or it is yet to be configured");
                    elementName = "acode"+sno;
                    document.getElementById(elementName).select();
                }
                }
                else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
}
function setValues(){
    document.jobCardClose.deleterowbutton.disabled = true;
}
    
    </script>
    
    <body onload="setValues();addRow();">
      <% int ifCloseReqd = 0; %>  
        <form name="jobCardClose" method="post">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                
                </div>
            </div>
           

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
   
            
            <c:if test = "${jcList != null}" >
            
            <c:forEach items="${jcList}" var="list"> 	

		<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border" >
		<tbody>
				<tr>
		    <td colspan="6" class="contentsub" height="30"><div class="contentsub">General details</div></td>
		</tr>
		<tr>
		<td class="text1" height="30"><b>Work Order Number</b></td>
		<td class="text1" height="30"><b>Job Card Number</b></td>
		<td class="text1" height="30"><b>Vehicle Type</b></td>
                <td class="text1" height="30"><b>MFG</b></td>
		<td class="text1" height="30"><b>Class, Usage</b></td>
		<td class="text1" height="30"><b>Model</b></td>
		</tr>
		<tr>
                    <input type="hidden" name="vehicleModelId" value='<c:out value="${list.modelId}"/>' />
                    <input type="hidden" name="vehicleMfrId" value='<c:out value="${list.mfrId}"/>' />
		<td class="text2" height="30"><c:out value="${list.vehicleNo}"/></td>
		<td class="text2" height="30"><c:out value="${list.jobCardId}"/></td>
		<td class="text2" height="30"><c:out value="${list.vehicleTypeName}"/></td>
		
		<input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>' />
		<input type="hidden" name="jcId" value='<c:out value="${list.jobCardId}"/>' />
		
		<td class="text2" height="30"><c:out value="${list.mfrName}"/></td>
		<td class="text2" height="30"><c:out value="${list.className}"/> - <c:out value="${list.usageTypeName}"/></td>
		<td class="text2" height="30"><c:out value="${list.modelName}"/></td>
		</tr>
		 </tbody></table>
		<br>
    
        <table width="90%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
            <tr>
            <td class="bottom" width="8%" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>

            <td  width="12%" align="right"><h2>Total SAR.</h2></td>
            <td  width="10%" align="left"><h2><div id="totalValue">0.00</div></h2></td>
            
            </tr>

        </table>
        <br>


<div style="height:200px;overflow:auto;">
<table align="center" id="addRows" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
<tr>
<td colspan="8" align="center" class="text2" height="30"><strong>Service Particulars</strong></td>
</tr>

<tr>
<td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
<td height="30" class="contentsub"><div class="contentsub">Activity Code</div></td>
<td height="30" class="contentsub"><div class="contentsub">Section</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Activity Name</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Quantity</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Rate</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Amount</div></td>
<td  height="30" class="contentsub"><div class="contentsub">select</div></td>

</tr>

</table>

<center>
<input type="button" value="AddRow" name="addrow" class="button" onclick="addRow();">
<input type="button" class="button" name="deleterowbutton" value="Delete"  onClick="deleteRow();" >
</center>


</div>
                        
                    </c:forEach >
                    
                
            </c:if> 
            <center>
                <br>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
		<tbody><tr>
		<td class="text2" align="center" colspan="8" height="30"><strong>Vendor Bill Particulars</strong></td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Invoice No</b></td>
                    <td class="text1" align="left"><input type="text" name="invoiceNo" value="" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Invoice Remarks</b></td>
                    <td class="text1" align="left"><textarea name="invoiceRemarks" rowspan="3" colspan="6" class="form-control"> </textarea></td>
                    <td class="text1" align="left"><b>Invoice Date</b></td>
                    <td class="text1" align="left"><input type="text" name="invoiceDate" value="" class="form-control" /> </td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Spares Value</b></td>
                    <td class="text2" align="left"><input type="text" name="sparesAmt" value="" class="form-control" /> </td>
                    <td class="text2" align="left"><b>Spares Remarks / Details</b></td>
                    <td class="text2" align="left"><textarea name="sparesRemarks" rowspan="3" colspan="6" class="form-control" > </textarea></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Consumables Value</b></td>
                    <td class="text1" align="left"><input type="text" name="consumableAmt" value="" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Consumables Remarks / Details</b></td>
                    <td class="text1" align="left"><textarea name="consumableRemarks" rowspan="3" colspan="6" class="form-control" > </textarea></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Labour Value</b></td>
                    <td class="text2" align="left"><input type="text" name="laborAmt" value="" class="form-control" /> </td>
                    <td class="text2" align="left"><b>Labour Remarks / Details</b></td>
                    <td class="text2" align="left"><textarea name="laborRemarks" rowspan="3" colspan="6" class="form-control" > </textarea></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Others Value</b></td>
                    <td class="text1" align="left"><input type="text" name="othersAmt" value="" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Others Remarks / Details</b></td>
                    <td class="text1" align="left"><textarea name="othersRemarks" rowspan="3" colspan="6" class="form-control" > </textarea></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>

                </table>







                
 <br>
     <br>
   <table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="500" class="border">
<tbody><tr>
<td colspan="4" class="text2" align="center" height="30"><strong>Quality Approval</strong></td>
</tr>

<tr>
<td width="117" height="30" class="contentsub">Authorised Person</td>
<td width="169" height="30" class="contentsub">Approve</td>
<td width="52" height="30" class="contentsub">Hold</td>

</tr>
<tr>
<td class="text2" height="30">Works Manager</td>
<td class="text2" height="30"><div align="center"><input type="radio" name="approve" checked value="Y"></div></td>
<td class="text2" height="30"><input type="radio" name="approve" value="N"></td>
</tr>
<tr>
<td class="text1" height="30">Remarks</td>
<td class="text1" height="30"><textarea name="remarks"></textarea></td>
<Td class="text1"></Td>
</tr>
 </tbody></table>
 
  <br>
 <div id="savePage" style="visibility:visible;" align="center" >
 <input class="button" type="button" value="Close Job Card" onclick="submitPage()">
 </div>
 <input class="button" type="button" value="Refresh" onclick="refreshPage()">
 


                
            </center>

            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
