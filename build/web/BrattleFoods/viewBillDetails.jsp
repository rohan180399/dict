<%--
    Document   : BillDetails
    Created on : Nov 4, 2013, 8:15:50 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
         <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        <script>
            function submitPage()
            {
                        alert('hi');
                        document.billDetails.action="/throttle/content/report/viewbillgeneration.jsp";
                        document.billDetails.submit();
            }
        </script>
    </head>
    <%
        String menuPath = "Finance >> View Bills";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="billDetails" method="post">
             <%@ include file="/content/common/path.jsp" %>



            <%@ include file="/content/common/message.jsp"%>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
               <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1"> View Bill Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                  <tr>
                                      <td><font color="red">*</font>Customer Name</td>
                                      <td height="30">
                                          <input name="fromDat1e" id="fromDate1" type="text" class="form-control"  onclick="ressetDate(this);">
                                      </td>
                                        
                                        
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <td><input type="button" class="button"   value="FETCH DATA" onclick="submitPage();"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table width="950" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
               <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="150" class="contentsub">Bill.No</th>
                        <th width="100" class="contentsub">Date</th>
                        <th width="130" class="contentsub">Customer Name</th>
                        <th width="130" class="contentsub">Bill Type</th>
                        <th width="130" class="contentsub">Bill Amount</th>
                        <th width="130" class="contentsub">Status</th>
                        <th width="130" class="contentsub">Details</th>
                    </tr>
               </thead>
               <tbody>
                   <tr>
                       <td class="text1">1</td>
                       <td class="text1">INV/13-14/100548</td>
                       <td class="text1">03-11-2013</td>
                       <td class="text1">M/s Doshi Foods</td>
                       <td class="text1">Actual KMs</td>
                       <td class="text1">14,900.00</td>
                       <td class="text1">Not Paid</td>
                       <td class="text1"><a href="printbillgeneration.jsp">details</a></td>
                   </tr>
                   <tr>
                       <td class="text2">2</td>
                       <td class="text2">INV/13-14/100569</td>
                       <td class="text2">04-11-2013</td>
                       <td class="text2">M/s Bakers Circle</td>
                       <td class="text2">Point to Point</td>
                       <td class="text2">1,25,000.00</td>
                       <td class="text2">Not Paid</td>
                       <td class="text2"><a href="printbillgeneration.jsp">details</a></td>
                   </tr>
                   <tr>
                       <td class="text1">3</td>
                       <td class="text1">INV/13-14/100564</td>
                       <td class="text1">04-11-2013</td>
                       <td class="text1">M/s Balaji & Co</td>
                       <td class="text1">Actual KMs</td>
                       <td class="text1">84,570.00</td>
                       <td class="text1">Paid</td>
                       <td class="text1"><a href="printbillgeneration.jsp">details</a></td>
                   </tr>
                   <tr>
                       <td class="text2">4</td>
                       <td class="text2">INV/13-14/100587</td>
                       <td class="text2">04-11-2013</td>
                       <td class="text2">M/s Rajan & Co</td>
                       <td class="text2">Point to Point Weight</td>
                       <td class="text2">1,09,000.00</td>
                       <td class="text2">Not Paid</td>
                       <td class="text2"><a href="printbillgeneration.jsp">details</a></td>
                   </tr>
                   
               </tbody>
           </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
