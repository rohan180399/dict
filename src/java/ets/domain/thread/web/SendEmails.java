/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.thread.web;

/**
 *
 * @author vinoth
 */
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.util.ThrottleConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;           
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.mail.EmailException;

public class SendEmails implements Runnable {

    public SendEmails() {
    }
    TripBP tripBP;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    String SMTP = "";
    int PORT = 0;
    String Frommailid = "";
    String Password = "";
    String EmailSubject = "";
    String BodyMessage = "";
    String Toemailid = "";
    String CCemailid = "";
    int Status = 0;
    String MailSendingId = "";
    TripTO tripTO = new TripTO();
    public SendEmails(TripBP tripBP,TripTO tripTO) {
        this.tripBP = tripBP;
        this.tripTO = tripTO;
        SMTP = ThrottleConstants.smtpServer;
        PORT = Integer.parseInt(ThrottleConstants.smtpPort);
        Frommailid = ThrottleConstants.fromMailId;
        Password = ThrottleConstants.fromMailPassword;
        EmailSubject = tripTO.getMailSubjectTo();
        BodyMessage = tripTO.getMailContentTo();
        Toemailid = tripTO.getMailIdTo();
        CCemailid = tripTO.getMailIdCc();
        MailSendingId = tripTO.getMailSendingId();
//        Toemailid = "arul@entitlesolutions.com";
//        CCemailid = "nithya@entitlesolutions.com";

    }

    public void run() {

        try {

            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");

//            if (Toemailid.indexOf("alok.shrivastava@brattlefoods.com") >= 0
//                    || Toemailid.indexOf("dipak.sutar@brattlefoods.com") >= 0) {
//                if (!CCemailid.equals("") && !CCemailid.equals("-")) {
//                    CCemailid = CCemailid + "," + "subhrakanta.mishra@brattlefoods.com";
//                } else {
//                    CCemailid = "subhrakanta.mishra@brattlefoods.com";
//                }
//            }
            System.out.println("Toemailid.." + Toemailid);
            String[] to = Toemailid.split(","); // added this line

            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Frommailid));

            InternetAddress[] toAddress = new InternetAddress[to.length];

            System.out.println("Toemailid:" + Toemailid);
            System.out.println("CCemailid:" + CCemailid);

            if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                for (int i = 0; i < to.length; i++) { // changed from a while loop
                    toAddress[i] = new InternetAddress(to[i]);
                }

                for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                    message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                }
            }

            ////////////// cc part////////////////////
            if (CCemailid.equals("-") || CCemailid.equals("")) {
                CCemailid = "lawrence.pereira@ttgroupglobal.com";
            } else {
                CCemailid = CCemailid;
            }
            if (!CCemailid.equals("-")) {
                String[] cc = CCemailid.split(","); // added this line
                int ccLen = cc.length;
                InternetAddress[] ccAddress = new InternetAddress[ccLen];
                int j = 0;
                for (j = 0; (j < cc.length && !CCemailid.equals("")); j++) { // changed from a while loop
                    ccAddress[j] = new InternetAddress(cc[j]);
                }
                for (j = 0; j < ccAddress.length; j++) { // changed from a while loop
                    message.addRecipient(Message.RecipientType.CC, ccAddress[j]);
                }
            }
            System.out.println("Toemailid1:" + Toemailid);
            System.out.println("CCemailid1:" + CCemailid);
            message.setSubject(EmailSubject);
            message.setContent(BodyMessage, "text/html");
            Transport transport = session.getTransport("smtp");
            transport.connect(SMTP, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            System.out.println("Email sent Successfully....");
            tripTO.setMailSendingId(MailSendingId);
            tripTO.setMailDeliveredStatusId("1");
            tripTO.setMailDeliveredResponse("Delivered Sucessfully");
            tripBP.updateMailStatus(tripTO);
        } catch (Exception e) {
            e.printStackTrace();
            String exp = e.toString();
            System.out.println("Unable to send Email....");
            tripTO.setMailSendingId(MailSendingId);
            tripTO.setMailDeliveredStatusId("0");
            tripTO.setMailDeliveredResponse(exp);
            try {
                tripBP.updateMailStatus(tripTO);
            } catch (FPRuntimeException ex) {
                Logger.getLogger(SendEmails.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FPBusinessException ex) {
                Logger.getLogger(SendEmails.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

    }
    
}
