<%--
    Document   : alterState
    Created on : 8 Nov, 2012, 11:08:28 AM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ledger Alter</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>
        function submitPage()
        {
            alert("1")
            if(textValidation(document.alter.districtName,'districtName')){
                return;
            }
                 alert("2")
            if(textValidation(document.alter.districtCode,'districtCode')){
                return;
            }
                 alert("3")
            if(textValidation(document.alter.districtID,'districtID')){
                return;
            }
//                 alert("4")
//             if(document.alter.countryID.value=='0'){
//                alert("Please Select Country");
//                return 'false';
//            }
//                 alert("5")
//            if(document.alter.stateID.value=='0'){
//                alert("Please Select State");
//                return 'false';
//            }
                 alert("4")
            if(textValidation(document.alter.description,'description')){
                return;
            }
                 alert("5")
            document.alter.action='/throttle/updateDistrict.do';
            document.alter.submit();
        }


    </script>
    <body>
        <form name="alter" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${districtalterList != null}">
                <c:forEach items="${districtalterList}" var="DL">
                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                        <tr height="30">
                            <Td colspan="2" class="contenthead">Edit District</Td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Name</td>
                            <td class="text2"><input name="districtName" type="text" class="textbox" value="<c:out value="${DL.districtName}"/>" maxlength="1" size="20">
                                <input type="hidden" name="districtID" value="<c:out value="${DL.districtID}"/>"> </td>
                        </tr>
                        <tr height="30">
                            <td class="text1"><font color="red">*</font>Code</td>
                           
                            <td class="text1"> <input name="districtCode" type="text" class="textbox" value="<c:out value="${DL.districtCode}"/>" maxlength="15" size="20">
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Country</td>
                            <td class="text2">
                                <select name="countryID" class="textbox" style="width:125px" >
                                    <c:if test="${CountryLists!=null}">
                                        <c:forEach items="${CountryLists}" var="CL" >
                                            <c:choose>
                                                <c:when test="${CL.countryID==DL.countryID}" >
                                                    <option value="<c:out value="${CL.countryID}"/>" selected > <c:out value="${CL.countryName}"/> </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:out value="${CL.countryID}"/>" > <c:out value="${CL.countryName}"/> </option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                         <tr height="30">
                    <td class="text1"><font color="red">*</font> State Name</td>
                    <td class="text1">
                        <select class="textbox" name="stateID"  style="width:125px">
                            <option value="0">---Select---</option>
                            <c:if test = "${StateLists != null}" >
                                <c:forEach items="${StateLists}" var="SL">
                                    <option value='<c:out value="${SL.stateID}" />'><c:out value="${SL.stateName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Description</td>
                            <td class="text2"><input name="description" type="text" class="textbox" value="<c:out value="${DL.description}"/>" maxlength="15" size="20"></td>
                        </tr>
                    </table>
                </c:forEach>
            </c:if>

            <br>
            <br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

