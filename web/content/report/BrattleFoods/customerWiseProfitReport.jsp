
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */

            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                //  alert(value);
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }  else if (document.getElementById('customerId').value == '') {
                    alert("Please select customer");
                    document.getElementById('customerId').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleCustomerWiseProfitabilityReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleCustomerWiseProfitabilityReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }
//
//            function viewCustomerProfitDetails(tripIds) {
////            alert(tripIds);
//                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
//            }
        </script>
     <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Customer wise profit Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                                <table style="width:100%" align="center" border="0" class="tabouterborder" >
                                    <tr height="30"   ><th colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Customer wise profit</th></tr>
                                      <tr height="40">
                                        <th class="tabtext"><font color="red">*</font>From Date</th>
                                        <th class="tabtext" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" style="width:180px;height:25px;color:black" ></th>
                                        <th class="tabtext"><font color="red">*</font>To Date</th>
                                        <th class="tabtext" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" style="width:180px;height:25px;color:black"></th>
                                          <th  class="tabtext"><font color="red">*</font>Customer</th>
                            <th class="tabtext">
                                <select name="customerId" id="customerId" style="height:22px;width:150px;">
                                    <option value="">--Select---</option>
                                    <c:forEach items="${customerList}" var="custList">
                                        <option value="<c:out value="${custList.custId}"/>"><c:out value="${custList.custName}"/></option>
                                    </c:forEach>
                                      <script>
                                    document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                </script>
                                </select> </th>
<!--                                    </tr>
                                    <tr>-->
                                        <th class="tabtext"  align="right"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;">&nbsp;&nbsp;</th>
                                        <th class="tabtext" ><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></th>
                                    </tr>
                                </table>
            <c:if test = "${tripDetails != null}" >
<!--                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>-->
                <br>
                  <table class="table table-info mb30 table-hover" id="table" class="sortable">
                    <thead height="30">
                      <tr >
                           <th >S no</th>
                            <th >Customer Name</th>
                            <th >Gr No</th>
                            <th >movement Type</th>
                            <th >routeInfo</th>
                            <th >containerNo</th>
                            <th >Container Type</th>
                            <th >Revenue</th>
                            <th >Expense</th>
                            <th >profit</th>
                            <th >profit(%)</th>
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                    <c:forEach items="${tripDetails}" var="csList">
                       

                            <tr  height="45">
                                <th align="center" class="text1"><%=sno%></th>
                             <th align="center" class="text1"><c:out value="${csList.customerName}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.grNo}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.movementType}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.routeInfo}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.containerNo}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.typeName}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.estimatedRevenue}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.estimatedExpenses}"/></th>
                            <th align="center" class="text1"><c:out value="${csList.estimatedRevenue - csList.estimatedExpenses}"/></th>
                            
                           <th align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${((csList.estimatedRevenue -csList.estimatedExpenses)/csList.estimatedRevenue)*100}" /></th>
                           

                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
                     <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
