<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  

<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions1.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>         
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<link href="css/box-style.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script src="/throttle/js/jquery.ui.core.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

</head>

<script type="text/javascript">

function generateMRS(val)
{
    checkValidation();
}

function checkValidation() {

    var index = document.getElementsByName("selectedIndex");
    var mfritemCode = document.getElementsByName("mfrCode");
    var itemCode = document.getElementsByName("itemCode");
    var itemIds = document.getElementsByName("itemIds");
    var buyPrices = document.getElementsByName("unitPrice");
    var discounts = document.getElementsByName("discount");
    var cntr = 0;

    if (document.stkRequest.vendorId.value == '0') {
        alert("Please select vendor");
        document.stkRequest.vendorId.focus();
        return;
    }



    for (var i = 0; (i < itemIds.length && itemIds.length != 0); i++) {

        if (itemIds[i].value != '') {
            cntr++;
            if (textValidation(itemCode[i], 'ItemCode')) {
                return;
            }
        }
    }
    if (cntr == 0) {
        alert("please enter any item");
        itemIds[i].focus();
        return;
    }
    for (var i = 0; (i < itemIds.length && itemIds.length != 0); i++) {

        if (itemIds[i].value != '') {
            if (buyPrices[i].value == '' || buyPrices[i].value == 0) {
                alert("please enter valid unit price");
                buyPrices[i].focus();
                return;

            }
        }
        /*
         if(discounts[i].value == '' || discounts[i].value == 0){
         alert("please enter valid discount");
         discounts[i].focus();
         return;
         
         }
         */
        if (parseFloat(discounts[i].value) > 100) {
            alert("please Enter valid discount");
            discounts[i].focus();
            return;

        }
    }
    document.stkRequest.action = "/throttle/saveVendorItems.do";
    if (confirm('Sure to Proceed...')) {
        document.getElementById("buttonStyle").style.visibility = "hidden";
        document.stkRequest.submit();
    }
}

</script>




<script type="text/javascript">

    function checkExistence(list, length, message) {
        var value = list[length].value
        for (var i = 0; i < list.length - 1; i++) {
            //alert(list[i].value +"=="+ value)
            if (list[i].value == value.toUpperCase()) {
                alert(message + " already exists..");

                var mfrCode = document.getElementsByName("mfrCode");
                var itemCode = document.getElementsByName("itemCode");
                var itemName = document.getElementsByName("itemName");
                var buyPrices = document.getElementsByName("unitPrice");
                //alert(buyPrices.length+":"+i+":"+length);
                mfrCode[length].value = '';
                itemCode[length].value = '';
                itemName[length].value = '';
                //alert("am here..."+buyPrices[i].value);
                buyPrices[i].select();
                return false;


            }

        }
        return true;
    }

    var httpRequest1;
    function getOthers(rowIndex)
    {
        var temp = 0;
        var index = rowIndex - 1;
        //alert("rowIndex - get others:"+rowIndex);
        var mfrCode = document.getElementsByName("mfrCode");
        var itemCode = document.getElementsByName("itemCode");
        var itemName = document.getElementsByName("itemName");
        var status = false;
        if (mfrCode[index].value != '') {
            status = checkExistence(mfrCode, index, "mfrCode");

        } else if (itemCode[index].value != '') {
            status = checkExistence(itemCode, index, "itemCode");

        } else if (itemName[index].value != '') {
            status = checkExistence(itemName, index, "itemName");

        }
        //alert(status);
        if (status) {
            var url = '/throttle/getQuandity.do?mfrCode=' + mfrCode[index].value + '&itemCode=' + itemCode[index].value + '&itemName=' + itemName[index].value;
            if (window.ActiveXObject)
            {
                httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpRequest1 = new XMLHttpRequest();
            }
            httpRequest1.open("POST", url, true);
            httpRequest1.onreadystatechange = function() {
                go(index, temp);
            };
            httpRequest1.send(null);
        }
    }



    function go(index, temp) {
        //alert("in go.....");
        if (httpRequest1.readyState == 4) {
            if (httpRequest1.status == 200) {
                var response = httpRequest1.responseText.valueOf();

                var itemId = document.getElementsByName("itemIds");
                var mfrCode = document.getElementsByName("mfrCode");
                var itemCode = document.getElementsByName("itemCode");
                var itemName = document.getElementsByName("itemName");
                var buyPrices = document.getElementsByName("buyPrice");
                var unitPrices = document.getElementsByName("unitPrice");
                var discounts = document.getElementsByName("discount");
                if (response != "") {

                    var details = response.split('~');
                    itemName[index].value = details[5];
                    itemCode[index].value = details[4];
                    mfrCode[index].value = details[3];
                    itemId[index].value = details[0];
                    buyPrices[index].value = 0.00;
                    unitPrices[index].value = 0.00;
                    discounts[index].value = 0.00;
                    unitPrices[index].select();
                    //buyPrices[index].focus();
                } else
                {
                    alert("Invalid value");
                    temp.value = "";
                }
            }
        }
    }


    function computeBuyPrice(index) {
        index--;
        var buyPrices = document.getElementsByName("buyPrice");
        var unitPrices = document.getElementsByName("unitPrice");
        var discounts = document.getElementsByName("discount");
//                    alert(unitPrices[index].value);
//                    alert(discounts[index].value);
//                    alert(buyPrices[index].value);
        buyPrices[index].value = unitPrices[index].value * (1 - (discounts[index].value / 100));
        buyPrices[index].value = parseFloat(buyPrices[index].value).toFixed(2);
        unitPrices[index].value = parseFloat(unitPrices[index].value).toFixed(2);
        discounts[index].value = parseFloat(discounts[index].value).toFixed(2);
    }

    var rowCount = 1;
    var sno = 0;
    var httpRequest;
    var httpReq;
    var rowIndex = 0;
//                alert(document.stkRequest.rIndex);
//
//                if(document.stkRequest.rIndex != null) {
//                    rowCount = document.stkRequest.rIndex.value;
//                }
//
    var styl = '';
    function addRow()
    {

        if (parseInt(rowCount) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }




        sno++;
        document.stkRequest.selectedIndex.value = sno;

        //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
        var tab = document.getElementById("items");
        var iRowCount = tab.getElementsByTagName('tr').length;
        //alert("len:"+iRowCount);
        rowCount = iRowCount;
        var newrow = tab.insertRow(rowCount);
        sno = rowCount;
        rowIndex = rowCount;
        //alert("rowIndex:"+rowIndex);
        var cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='itemIds' value='' > " + sno + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='25' ><input name='mfrCode' class='form-control'   onchange='getOthers(" + rowIndex + ")'  type='text'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell1 = "<td class='text1' height='25'><input name='itemCode' class='form-control'  onchange='getOthers(" + rowIndex + ")'  type='text'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell1;

        cell = newrow.insertCell(3);
        var cell2 = "<td class='text1' height='25'><input name='itemName' id='itemNames" + rowIndex + "' class='form-control'  onchange='getOthers(" + rowIndex + ")'  type='text'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;

//                    cell = newrow.insertCell(4);
//                    cell2 = "<td class='text1' height='25'><select class='form-control' name='vatId' id='vatId"+rowIndex+"'><c:forEach items='${vatList}' var='vatData'><option value='<c:out value='${vatData.vatId}'/>'><c:out value='${vatData.vat}'/></option></c:forEach></select></td>";
//                    cell.setAttribute("className",styl);
//                    cell.innerHTML = cell2;

        cell = newrow.insertCell(4);
        cell2 = "<td class='text1'  height='25'><input type='hidden' value='0' name='vatId' id='vatId" + rowIndex + "' /> <input name='unitPrice' size='10'  id='unitPrice" + rowIndex + "' onchange='computeBuyPrice(" + rowIndex + ");' class='form-control'   type='text'></td>";
        //alert(cell2);
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;

        cell = newrow.insertCell(5);
        cell2 = "<td class='text1'  height='25'><input name='discount' size='10'  id='discount" + rowIndex + "' onchange='computeBuyPrice(" + rowIndex + ");'  class='form-control'   type='text'></td>";

        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;

        cell = newrow.insertCell(6);
        cell2 = "<td class='text1' height='25'><input name='buyPrice' size='10'  readonly id='buyPrice" + rowIndex + "' class='form-control'   type='text'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell2;

        getItemNames(rowIndex);
        var itemCode = document.getElementsByName("itemCode");
        itemCode[rowCount - 1].focus();

        rowIndex++;
        rowCount++;

    }




    function getItemNames(val) {
        var itemTextBox = 'itemNames' + val;
        var oTextbox = new AutoSuggestControl(document.getElementById(itemTextBox), new ListSuggestions(itemTextBox, "/throttle/handleItemSuggestions.do?"));
        //getVehicleDetails(document.getElementById("regno"));
    }

    function clearFieldValues(val)
    {
        getItemNames(val);
        var itemId = document.getElementsByName('itemIds');
        var mfrCode = document.getElementsByName('mfrCode');
        var itemCode = document.getElementsByName('itemCode');
        var itemName = document.getElementsByName('itemName');
        var selectedInd = document.getElementsByName('selectedInd');
        if (selectedInd[val].checked == true) {
            itemName[val].value = "";
            itemCode[val].value = "";
            mfrCode[val].value = "";
            itemId[val].value = "";
        }
    }





    function searchVendorItems() {
        //alert("submitting.....");
        document.stkRequest.action = "/throttle/manageVendorItemConfigPage.do";
        document.stkRequest.submit();
    }

    function setFocus() {
        document.stkRequest.vendorId.focus();
    }
    function setValues() {
        //alert('<%= request.getAttribute("vendorId")%>');
        if ('<%= request.getAttribute("vendorId")%>' != 'null') {
            document.stkRequest.vendorId.value = <%= request.getAttribute("vendorId")%>;
        }
    }




</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="service.label.MRS"  text="MRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onLoad="setFocus();
                    setValues();">
                <form name="stkRequest" method="post" >


                    <%@ include file="/content/common/message.jsp"%>
                    <td colspan="4"  style="background-color:#5BC0DE;">
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="7"  height="30" align="center">
                            <div  align="center"><spring:message code="vendors.label.ConfigureVendor"  text="default text"/></div></th>
                            </tr>
                            </thead>
                            <c:if test = "${vendorList != null}" >

                                <!--<table  class="table table-info mb30 table-hover">-->
                                <tr>
                                    <td  height="30"><font color="red">*</font><spring:message code="vendors.label.VendorName"  text="default text"/></td>
                                    <td >
                                        <select class="form-control" style="width:360px;height:40px;" name="vendorId" onchange="searchVendorItems();" >
                                            <option value='0'>-<spring:message code="vendors.label.Select"  text="default text"/>-</option>

                                            <c:forEach items="${vendorList}" var="vendor"> 
                                                <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>     
                                            </c:forEach>  
                                        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                            </table>
                            <!--</table>-->
                            <br>    
                            <table class="table table-info mb30 table-hover" id="items" >
                                <thead>
                                    <tr>
                                        <th height="30"><spring:message code="vendors.label.SNo"  text="default text"/></th>
                                        <th height="30"><spring:message code="vendors.label.MFRItemcode"  text="default text"/></th>
                                        <th height="30"><spring:message code="vendors.label.ItemCode"  text="default text"/></th>
                                        <th height="30"><spring:message code="vendors.label.ItemName"  text="default text"/></th>
                                        <!--                        <th height="30">VAT %age</th>-->
                                        <th height="30"><spring:message code="vendors.label.UnitPrice"  text="default text"/></th>
                                        <th height="30"><spring:message code="vendors.label.OfferedDiscount(%age)"  text="default text"/></th>
                                        <th height="30"><spring:message code="vendors.label.BuyingPrice"  text="default text"/></th>
                                        <!--                        <th colspan="2" height="30">Clear</th>-->
                                <input type="hidden" name="selectedIndex" value="">
                                </tr>
                                </thead>
                                <%int rIndex = 0;
                                int index = 1;
                                %>
                                <c:forEach items="${vendorItemList}" var="list">
                                    <%
            

                        String classText = "";

                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr >
                                        <td class="<%=classText %>"  height="30"><%=index %></td>
                                        <td class="<%=classText %>"  height="30"><c:out value="${list.mfrCode}"/>
                                            <input type='hidden'  name='itemIds' value='<c:out value="${list.itemId}"/>' >
                                            <input type='hidden'  name='mfrCode' value='<c:out value="${list.mfrCode}"/>' >
                                        </td>
                                        <td class="<%=classText %>"  height="30"><c:out value="${list.paplCode}"/>
                                            <input type='hidden'  name='itemCode' value='<c:out value="${list.paplCode}"/>' >
                                        </td>
                                        <td class="<%=classText %>"  height="30"><c:out value="${list.itemName}"/>
                                            <input type='hidden'  name='itemName' value='<c:out value="${list.itemName}"/>' >
                                        </td>
                                        <%--
                                        <td class="<%=classText %>"  height="30">
                                             <select class="form-control" name="vatId" >
                                                    <c:forEach items="${vatList}" var="vatData">
                                                        <c:if test = "${vatData.vatId == list.vatId}" >
                                                            <option selected value="<c:out value="${vatData.vatId}"/>"><c:out value="${vatData.vat}"/></option>
                                                        </c:if>
                                                        <c:if test = "${vatData.vatId != list.vatId}" >
                                                            <option value="<c:out value="${vatData.vatId}"/>"><c:out value="${vatData.vat}"/></option>
                                                        </c:if>
                                                    </c:forEach>
                                            </select>

                            </td> --%>
                                    <input type="hidden" name="vatId" value="0" />
                                    <td class="<%=classText %>"  height="30">
                                        <input type='text' size="10" name='unitPrice' class='form-control' onchange="computeBuyPrice(<%=index%>);" value='<c:out value="${list.unitPrice}"/>' >
                                    </td>
                                    <td class="<%=classText %>"  height="30">
                                        <input type='text' size="10"  name='discount' class='form-control'  onchange="computeBuyPrice(<%=index%>);" value='<c:out value="${list.discount}"/>' >
                                    </td>
                                    <td class="<%=classText %>"  height="30">
                                        <input type='text' size="10"  name='buyPrice' readonly class='form-control' value='<c:out value="${list.buyPrice}"/>' >
                                    </td>
        <!--                            <td class="<%=classText %>"  height="30">&nbsp;</td>-->
                                    </tr>
                                    <%
                        index++;
                        rIndex++;
                                    %>
                                </c:forEach >
                                <%
                                System.out.println("rIndex:"+rIndex);
                                rIndex--;
                                %>
                                <input type="hidden" name="rIndex" value=<%=rIndex%> />
                            </table>
                            <br>
                            <div id="buttonStyle" style="visibility:visible;" align="center" >
                                <input value="<spring:message code="vendors.label.AddRow"  text="default text"/>" class="btn btn-info" type="button" onClick="addRow();" >
                                <input type="button" class="btn btn-info" value="<spring:message code="vendors.label.SAVE"  text="default text"/>" name="Save"   onClick="generateMRS(this.name)"/>
                            </div>
                            <br>
                        </c:if>     

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
