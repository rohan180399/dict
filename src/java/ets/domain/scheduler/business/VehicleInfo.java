/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.business;

/**
 *
 * @author Nivan
 */
public class VehicleInfo {

    public VehicleInfo() {

    }

    private String vehicleId = "";
    private String latitude = "";
    private String longitude = "";
    private String currentLocation = "";
    private String vehicleRtoNo = "";
    private String vehicleAlias = "";
    private String gpsDateTime = "";
    private String vehicleSpeed = "";
    private String vehicleDayKm = "";
    private String vehicleOdometerReading = "";
    private String geoFeneceLoc = "";
    private String geoFeneceInd = "";
    private String geoFeneceDateTime = "";
    private String vendor = "";

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getVehicleRtoNo() {
        return vehicleRtoNo;
    }

    public void setVehicleRtoNo(String vehicleRtoNo) {
        this.vehicleRtoNo = vehicleRtoNo;
    }

    public String getVehicleAlias() {
        return vehicleAlias;
    }

    public void setVehicleAlias(String vehicleAlias) {
        this.vehicleAlias = vehicleAlias;
    }

    public String getGpsDateTime() {
        return gpsDateTime;
    }

    public void setGpsDateTime(String gpsDateTime) {
        this.gpsDateTime = gpsDateTime;
    }

    public String getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(String vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    public String getVehicleDayKm() {
        return vehicleDayKm;
    }

    public void setVehicleDayKm(String vehicleDayKm) {
        this.vehicleDayKm = vehicleDayKm;
    }

    public String getVehicleOdometerReading() {
        return vehicleOdometerReading;
    }

    public void setVehicleOdometerReading(String vehicleOdometerReading) {
        this.vehicleOdometerReading = vehicleOdometerReading;
    }

    public String getGeoFeneceLoc() {
        return geoFeneceLoc;
    }

    public void setGeoFeneceLoc(String geoFeneceLoc) {
        this.geoFeneceLoc = geoFeneceLoc;
    }

    public String getGeoFeneceInd() {
        return geoFeneceInd;
    }

    public void setGeoFeneceInd(String geoFeneceInd) {
        this.geoFeneceInd = geoFeneceInd;
    }

    public String getGeoFeneceDateTime() {
        return geoFeneceDateTime;
    }

    public void setGeoFeneceDateTime(String geoFeneceDateTime) {
        this.geoFeneceDateTime = geoFeneceDateTime;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    
    
}
