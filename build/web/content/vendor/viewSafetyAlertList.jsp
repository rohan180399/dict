<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.customer.business.CustomerTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<!--        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>-->

<!-- Our jQuery Script to make everything work -->

<!--        <script  type="text/javascript" src="js/jq-ac-script.js"></script>-->

<script language="javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerName.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#custName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    $(document).ready(function() {
        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerCode.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#customerCode').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#customerCode').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    //    function checkValue(value,id){
    //                if(value == '' && id=='custName'){
    //                   $('#customerCode').attr('readonly', true);
    //                   document.getElementById('customerId').value = '';
    //                }
    //                if(value == '' && id=='customerCode'){
    //                   $('#custName').attr('readonly', true);
    //                   document.getElementById('customerCode').value = '';
    //                }
    //            }
    //

    function submitPage(value) {
        var param="trans";
        if (value == "add") {
                document.manufacturer.action = '/throttle/handleViewAddCustomer.do?param='+param;
            document.manufacturer.submit();
        } else if (value == 'search') {
            document.manufacturer.action = '/throttle/handleViewCustomer.do';
            document.manufacturer.submit();
        }
    }

</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>
<!--	  <span style="float: right">
                <a href="?paramName=en">English</a>
                |
                <a href="?paramName=ar">العربية</a>
          </span>-->
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="safety.label.AlertView"  text="default text"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="safety.label.Safety"  text="default text"/></a></li>
            <li class="active"><spring:message code="safety.label.AlertList"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="manufacturer" method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" id="table">
                        <!--                <table width="100%" align="center" border="0" id="table" class="sortable">-->
                        <thead>
                            <tr height="40">
                                <th><spring:message code="safety.label.SNo"  text="default text"/></th>
                                <th><spring:message code="safety.label.AlertFor"  text="default text"/></th>
                                <th><spring:message code="safety.label.VehicleNo"  text="default text"/></th>
                                <th><spring:message code="safety.label.TrailerNo"  text="default text"/></th>
                                <th><spring:message code="safety.label.EmployeeName"  text="default text"/> </th>
                                <th><spring:message code="safety.label.Drivername"  text="default text"/></th>
                                <th><spring:message code="safety.label.CreatedDate"  text="default text"/></th>
                                <th><spring:message code="safety.label.Description"  text="default text"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% int index = 0, sno = 1;%>

                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <c:if test="${alertDetails!=null}">
                                <c:forEach items="${alertDetails}" var="list">
                                    <tr height="30">
                                        <td  ><%=sno%></td>
                                        <td  ><c:out value="${list.alertFor}"/> </td>
                                        <td  ><c:out value="${list.vehicleNo}"/> </td>
                                        <td  ><c:out value="${list.trailerNo}"/> </td>
                                        <td  ><c:out value="${list.empName}"/> </td>
                                        <td  ><c:out value="${list.driverName}"/> </td>
                                        <td  ><c:out value="${list.createdDate}"/> </td>
                                        <td  ><c:out value="${list.description}"/> </td>

                                    </tr>
                                    <%
                                                index++;
                                                sno++;
                                    %>

                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="safety.label.EntriesPerPage"  text="default text"/>
                            </span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="safety.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="safety.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>