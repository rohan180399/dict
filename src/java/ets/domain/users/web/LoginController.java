/*----------------------------------------------------------------------------
 * LoginController.java
 * Mar 31, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/


package ets.domain.users.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import ets.arch.business.PaginationHelper;
import ets.domain.users.web.CryptoLibrary;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.crypto.Cipher.*;
import javax.crypto.SecretKey.*;
import javax.crypto.KeyGenerator.*;
import javax.crypto.spec.SecretKeySpec.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;

import ets.domain.vehicle.business.VehicleBP;

import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;

import ets.arch.web.BaseController;

import ets.arch.exception.FPRuntimeException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Date;
import javax.servlet.http.HttpServlet;

/**
 * *****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 Mar 31, 2008 ES Created
 *
 *****************************************************************************
 */
/**
 * <br>LoginController object - Spring framework mandated controller class for
 * user login Functionality.
 *
 * @author Srinivasan.R
 * @version 1.0 31 Mar 2008
 * @since event Portal Iteration 0
 */
public class LoginController extends BaseController {

    /**
     * loginCommand -loginCommand's Instansce
     */
    LoginCommand loginCommand;
    /**
     * loginBP -LoginBP's Instansce
     */
    LoginBP loginBP;
    VehicleBP vehicleBP;

    /**
     * @return LoginBP - returns the loginBP.
     */
    public LoginBP getLoginBP() {
        return this.loginBP;
    }

    /**
     * @param loginBP The loginBP to set.
     */
    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public VehicleBP getVehicleBP() {
        return this.vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);
//        System.out.println("***********************************" );
//        System.out.println("getRequestURL="+request.getRequestURL()  );
//        System.out.println("ContextPath="+request.getContextPath()  );
//        System.out.println("Method="+request.getMethod()  );
//        System.out.println("AuthType="+request.getAuthType()  );
//        System.out.println("RemoteUser="+request.getRemoteUser()  );
//        System.out.println("RequestURI="+request.getRequestURI());
//        System.out.println("SessionId="+request.getRequestedSessionId());
//        System.out.println("RemoteAddr="+request.getRemoteAddr() );
//        System.out.println("***********************************" );

    }

    /**
     * This method used to handle Login .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleLoginValidate(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws IOException {

//        System.out.println("k i am in handleloginvalidate ");
        HttpSession session = request.getSession();
        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        String userName = loginCommand.getUserName();
        CryptoLibrary cl = new CryptoLibrary();
        PrintWriter pw = response.getWriter();
        try {

            if (loginCommand.getUserName() != null && !"".equals(loginCommand.getUserName())) {
                loginTO.setUserName(loginCommand.getUserName());
            }

            int loginStatus = 0;
            String password = "";
            password = loginBP.checkUserName(loginTO);
//            System.out.println("password-->" + password);
            if (!"".equals(password)) {
                password = cl.decrypt(password);
                session.setAttribute("password", password);
//                System.out.println("pass " + password);
            }
            pw.print(password);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    /**
     * This method used to handle Login Request.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView loginRequest(HttpServletRequest request, HttpServletResponse response, LoginCommand command) {

        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        String userName = loginCommand.getUserName();
        CryptoLibrary cl = new CryptoLibrary();
        String menuPath = "Login   >>   Home Page ";
        String pageTitle = "Home Page";
        request.setAttribute("pageTitle", pageTitle);
        try {

            System.out.println("loginCommand.getUserName():" + loginCommand.getUserName());
            if (loginCommand.getUserName() != null && loginCommand.getUserName() != "") {
                loginTO.setUserName(loginCommand.getUserName());
//                session.setAttribute("userName", loginCommand.getUserName());
            }

            if (loginCommand.getPassword() != null && loginCommand.getPassword() != "") {
                String password = cl.encrypt(loginCommand.getPassword());
                loginTO.setPassword(password);
            }

            int loginStatus = 0;
            loginStatus = loginBP.checkExistUser(loginTO);
            System.out.println("userId " + loginStatus);

            //System.out.println("login" + loginStatus);
            String password = "";
//            password = loginBP.checkUserName(loginTO);
            System.out.println("password-->" + password);
//            if (!"".equals(password)) {
//                password = cl.decrypt(password);
////                System.out.println("pass " + password);
//            }
            if (loginStatus != 0) {
                HttpSession session = request.getSession(true);

                String sessionPageParam = session.getId() + "" + System.currentTimeMillis();
                System.out.println("sessionPageParam2:" + sessionPageParam);
                session.setAttribute("sessionPageParam", sessionPageParam);

                session.setAttribute("userId", loginStatus);
                userDetails = loginBP.getUserDetails(loginStatus);
                System.out.println(" size in userDetlails :  " + userDetails.size());
                session.setAttribute("UserDetails", userDetails);
                Date date = new Date();

                SimpleDateFormat sdf = null;
                PaginationHelper pagenation = new PaginationHelper();
                String buttonClicked = request.getParameter("button");
                session.setAttribute("totalRecords", pagenation.getTotalRecords());
                //System.out.println("date1"+date);
//                   DateFormat df = null;
//                   df=DateFormat.getDateInstance();
//                   System.out.println("date2"+df.format(date));
                sdf = new SimpleDateFormat("dd-MM-yyyy");
                session.setAttribute("currentDate", sdf.format(date));

//                System.out.println("pagenation.getTotalRecords()" + pagenation.getTotalRecords());
                Iterator itr = userDetails.iterator();
                LoginTO logTO = new LoginTO();
                int count = 0;
                if (itr.hasNext()) {
                    loginTO = (LoginTO) itr.next();
                    if (count == 0) {
                        session.setAttribute("hubId", loginTO.getHubId());
                        System.out.println("UserName : in Login  " + loginTO.getUserName());
                        session.setAttribute("UserName", loginTO.getUserName());
//                        System.out.println("loginTO.getDesigId()  " + loginTO.getDesigId());
                        session.setAttribute("DesigId", loginTO.getDesigId());
                        session.setAttribute("DesigName", loginTO.getDesigName());
//                        System.out.println("loginTO.getCompanyId()  " + loginTO.getCompanyId());
                        session.setAttribute("DeptId", loginTO.getDeptId());
                        session.setAttribute("companyId", loginTO.getCompanyId());
                        session.setAttribute("companyName", loginTO.getCompanyName());
                        session.setAttribute("companyTypeId", loginTO.getCompanyTypeId());
//                        System.out.println("loginTO.getCompanyTypeId()"+loginTO.getCompanyTypeId());
                        session.setAttribute("companyTypeName", loginTO.getCompanyTypeName());
                        session.setAttribute("emailId", loginTO.getEmailId());
                        session.setAttribute("password", cl.decrypt(loginTO.getPassword()));
                    }
                    System.out.println("loginTO.getRoleId()  " + loginTO.getRoleId());
                    session.setAttribute("RoleId", loginTO.getRoleId());
                    logTO.setRoleId(loginTO.getRoleId());
                    count++;
                }
                ArrayList unavailableFunction = new ArrayList();
                ArrayList getFunction = new ArrayList();
                getFunction = loginBP.getUserFunctions(loginStatus);
                session.setAttribute("loginRecordId", "0");
//                System.out.println("userFunction size is : " + getFunction.size());
                session.setAttribute("userFunction", getFunction);
                unavailableFunction = loginBP.getUserUnavailableFunctions(loginStatus);
//                System.out.println("unavailableFunction Size is in login Controller " + unavailableFunction.size());
                session.setAttribute("unavailableFunctions", unavailableFunction);
                ArrayList getUserRoles = new ArrayList();
                getUserRoles = loginBP.getUserAvailableRoles(loginStatus);
                ArrayList getUserRole = new ArrayList();
                getUserRole = loginBP.getUserRoles(userName);
//                System.out.println("userRoles size " + getUserRoles.size());
//                session.setAttribute("userRole actual ", getUserRole.size());
                session.setAttribute("UserRoles", getUserRoles);

                //menu list by vijay start
                ArrayList userMenus = new ArrayList();
                userMenus = loginBP.getUserMenus(loginStatus);
                session.setAttribute("userMenus", userMenus);
//                System.out.println("userMenus vijay"+userMenus.size());
                //menu list by vijay end
                session.setAttribute("userName", userName);
//                session.setAttribute("password", loginCommand.getPassword());
//                System.out.println("UserName is " + userName);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                //   loginBP.updatePops();
                session.setAttribute("paramName", "en");
                setLocale(request, response);

                ArrayList menuList = new ArrayList();
                ArrayList userMenuList = new ArrayList();
                ArrayList userSubMenuList = new ArrayList();
                userMenuList = loginBP.getUserMenuList(logTO);
                Iterator it = userMenuList.iterator();
                LoginTO lTO = new LoginTO();
                while (it.hasNext()) {
                    lTO = (LoginTO) it.next();
                    lTO.setRoleId(logTO.getRoleId());
                    userSubMenuList = loginBP.getUserSubMenuList(lTO);
                    lTO.setUserSubMenuList(userSubMenuList);
                    menuList.add(lTO);
                }
                System.out.println("menuList.size() Arularasan Here = " + menuList.size());
                session.setAttribute("menuList", menuList);
                session.setAttribute("userMenus", userMenuList);

                path = "content/report/dashboardOperation.jsp";
                /*
                 if(loginStatus == 1100)
                 {
                 path = "content/common/layout1.jsp";
                 }

                 else{
                 path = "content/common/layout.jsp";
                 }
                 */
            } else {
//                System.out.println("Login Failed");
                path = "content/common/login.jsp";
            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView languageChangeLoginRequest(HttpServletRequest request, HttpServletResponse response, LoginCommand command) {

        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        ArrayList userDetails = new ArrayList();
        HttpSession session = request.getSession();
        String path = "";
        String userName = loginCommand.getUserName();
        CryptoLibrary cl = new CryptoLibrary();
        String menuPath = "Login   >>   Home Page ";
        String pageTitle = "Home Page";
        request.setAttribute("pageTitle", pageTitle);
        try {

            path = "content/report/dashboardOperation.jsp";
            /*
                 if(loginStatus == 1100)
                 {
                 path = "content/common/layout1.jsp";
                 }

                 else{
                 path = "content/common/layout.jsp";
                 }
             */

            String param = request.getParameter("paramName");
            session.setAttribute("paramName", param);
            request.setAttribute("paramName", param);
            System.out.println("paramName....:" + param);
            setLocale(request, response);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView mloginRequest(HttpServletRequest request, HttpServletResponse response, LoginCommand command) {

        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        String userName = loginCommand.getUserName();
        CryptoLibrary cl = new CryptoLibrary();
        String menuPath = "Login   >>   Home Page ";
        String pageTitle = "Home Page";
        request.setAttribute("pageTitle", pageTitle);
        try {

            path = "content/common/loginResponsive.jsp";

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    public ModelAndView loginRespRequest(HttpServletRequest request, HttpServletResponse response, LoginCommand command) {

        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        String userName = loginCommand.getUserName();
        CryptoLibrary cl = new CryptoLibrary();
        String menuPath = "Login   >>   Home Page ";
        String pageTitle = "Home Page";
        request.setAttribute("pageTitle", pageTitle);
        try {

            if (loginCommand.getUserName() != null && loginCommand.getUserName() != "") {
                loginTO.setUserName(loginCommand.getUserName());
//                session.setAttribute("userName", loginCommand.getUserName());
            }

            if (loginCommand.getPassword() != null && loginCommand.getPassword() != "") {
                String password = cl.encrypt(loginCommand.getPassword());
                loginTO.setPassword(password);
            }

            int loginStatus = 0;
            loginStatus = loginBP.checkExistUser(loginTO);
//            System.out.println("userId " + loginStatus);

            //System.out.println("login" + loginStatus);
//            String password = "";
//            password = loginBP.checkUserName(loginTO);
////            System.out.println("password-->" + password);
//            if (!"".equals(password)) {
//                password = cl.decrypt(password);
////                System.out.println("pass " + password);
//            }
            if (loginStatus != 0) {
                HttpSession session = request.getSession(true);

                session.setAttribute("userId", loginStatus);
                userDetails = loginBP.getUserDetails(loginStatus);
//                System.out.println(" size in userDetlails :  " + userDetails.size());
                session.setAttribute("UserDetails", userDetails);
                Date date = new Date();

                SimpleDateFormat sdf = null;
                PaginationHelper pagenation = new PaginationHelper();
                String buttonClicked = request.getParameter("button");
                session.setAttribute("totalRecords", pagenation.getTotalRecords());
                //System.out.println("date1"+date);
//                   DateFormat df = null;
//                   df=DateFormat.getDateInstance();
//                   System.out.println("date2"+df.format(date));
                sdf = new SimpleDateFormat("dd-MM-yyyy");
                session.setAttribute("currentDate", sdf.format(date));

//                System.out.println("pagenation.getTotalRecords()" + pagenation.getTotalRecords());
                Iterator itr = userDetails.iterator();
                int count = 0;
                if (itr.hasNext()) {
                    loginTO = (LoginTO) itr.next();
                    if (count == 0) {
//                        System.out.println("UserName : in Login  " + loginTO.getUserName());
                        session.setAttribute("UserName", loginTO.getUserName());
//                        System.out.println("loginTO.getDesigId()  " + loginTO.getDesigId());
                        session.setAttribute("DesigId", loginTO.getDesigId());
                        session.setAttribute("DesigName", loginTO.getDesigName());
//                        System.out.println("loginTO.getCompanyId()  " + loginTO.getCompanyId());
                        session.setAttribute("DeptId", loginTO.getDeptId());
                        session.setAttribute("companyId", loginTO.getCompanyId());
                        session.setAttribute("companyName", loginTO.getCompanyName());
                        session.setAttribute("companyTypeId", loginTO.getCompanyTypeId());
//                        System.out.println("loginTO.getCompanyTypeId()"+loginTO.getCompanyTypeId());
                        session.setAttribute("companyTypeName", loginTO.getCompanyTypeName());
                        session.setAttribute("password", cl.decrypt(loginTO.getPassword()));
                    }
//                    System.out.println("loginTO.getRoleId()  " + loginTO.getRoleId());
                    session.setAttribute("RoleId", loginTO.getRoleId());
                    count++;
                }
                ArrayList unavailableFunction = new ArrayList();
                ArrayList getFunction = new ArrayList();
                getFunction = loginBP.getUserFunctions(loginStatus);
//                System.out.println("userFunction size is : " + getFunction.size());
                session.setAttribute("userFunction", getFunction);
                unavailableFunction = loginBP.getUserUnavailableFunctions(loginStatus);
//                System.out.println("unavailableFunction Size is in login Controller " + unavailableFunction.size());
                session.setAttribute("unavailableFunctions", unavailableFunction);
                ArrayList getUserRoles = new ArrayList();
                getUserRoles = loginBP.getUserAvailableRoles(loginStatus);
                ArrayList getUserRole = new ArrayList();
                getUserRole = loginBP.getUserRoles(userName);
//                System.out.println("userRoles size " + getUserRoles.size());
//                session.setAttribute("userRole actual ", getUserRole.size());
                session.setAttribute("UserRoles", getUserRoles);

                //menu list by vijay start
                ArrayList userMenus = new ArrayList();
                userMenus = loginBP.getUserMenus(loginStatus);
                session.setAttribute("userMenus", userMenus);
//                System.out.println("userMenus vijay"+userMenus.size());
                //menu list by vijay end
                session.setAttribute("userName", userName);
//                session.setAttribute("password", loginCommand.getPassword());
//                System.out.println("UserName is " + userName);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                path = "content/common/menuResponsive.jsp";
                /*
                 if(loginStatus == 1100)
                 {
                 path = "content/common/layout1.jsp";
                 }

                 else{
                 path = "content/common/layout.jsp";
                 }
                 */
            } else {
//                System.out.println("Login Failed");
                path = "content/common/login.jsp";
            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    /**
     * This method used to User Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView getuser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "";
//        System.out.println("search user");
        LoginTO loginTO = new LoginTO();
        String menuPath = " User   >> Manage User ";
        String pageTitle = "Manage User";

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        if (session.getAttribute("message") != null && session.getAttribute("message") != "") {
            session.removeAttribute("message");

        }
        try {

            String user = loginCommand.getLetters();
            if (user != null && user != "") {
                loginTO.setLetters(user);
            }
            request.setAttribute("Letter", user);
            ArrayList userlist = new ArrayList();
            userlist = loginBP.getUserList(loginTO);
            if (userlist.size() != 0) {
                request.setAttribute("userList", userlist);
//                System.out.println("userList  " + userlist.size());
                path = "content/user/manageuser.jsp";
                request.setAttribute("pageTitle", pageTitle);
            } else {
                request.setAttribute("errorMessage", "User does not exists");
                path = "content/user/manageuser.jsp";
                request.setAttribute("pageTitle", pageTitle);
            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to retrieve user details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View the User Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewuser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = " User   >> Manage User ";
        String pageTitle = "Manage User";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "User-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/user/manageuser.jsp";
//            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to retrieve user details --> " + exception);
            exception.printStackTrace();
        }

        session.removeAttribute("userList");
        return new ModelAndView(path);

    }

    /**
     * This method used to Add the User Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView adduser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "";
        String menuPath = " User   >> Manage User  >> Add User ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Add User";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "User-Add")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/user/adduser.jsp";
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to retrieve user details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Insert the User Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView insertuser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "";
        String pageTitle = "Add User";
        request.setAttribute("pageTitle", pageTitle);
        String menuPath = " User   >> Manage User  >>  Add User";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int UserId = (Integer) session.getAttribute("userId");
        LoginTO loginTO = new LoginTO();

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "User-Add")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (!"".equals(loginCommand.getUserName()) && loginCommand.getUserName() != null) {
                loginTO.setUserName(loginCommand.getUserName());
                loginTO.setUserId(UserId);
            }
            if (loginCommand.getPassword() != "" && loginCommand.getPassword() != null) {
                String password = loginCommand.getPassword().trim();
                CryptoLibrary cLib = new CryptoLibrary();
                password = cLib.encrypt(password);
                loginTO.setPassword(password);
            }

            int status = 0;
            status = loginBP.insertUserDetails(loginTO);
            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "User Details Added Successfully");
                path = "content/user/adduser.jsp";
            } else {

                path = "content/user/adduser.jsp";
            }
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to insert user data --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    /**
     * This method used to view Modify User Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alteruser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = " User   >> Manage User  >>  Alter User ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Alter User";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "User-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String user = loginCommand.getLetters();
            LoginTO loginTO = new LoginTO();
            if (user != null && user != "") {
                loginTO.setLetters(user);
            }

            ArrayList userlist = new ArrayList();
            userlist = loginBP.getUserList(loginTO);
            request.setAttribute("userList", userlist);
            path = "content/user/alteruser.jsp";
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            path = "content/common/error.jsp";
            FPLogUtils.fpErrorLog("Failed to view alter user --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Modify User Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView modifyuser(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "";
        String menuPath = " User   >>  Manage Password ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Manage User";
        request.setAttribute("pageTitle", pageTitle);
        int UserId1 = (Integer) session.getAttribute("userId");
        System.out.println("UserId1" + UserId1);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "User-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String[] userId = loginCommand.getUserIds();
            String[] userName = loginCommand.getUserNames();
            String[] activeStatus = loginCommand.getActiveInds();
            String[] selectedIndex = loginCommand.getSelectedUsers();
            int index = 0;
            int modify = 0;
            ArrayList userlist = new ArrayList();
            LoginTO loginTO = new LoginTO();
            for (int i = 0; i < selectedIndex.length; i++) {
                loginTO = new LoginTO();
                index = Integer.parseInt(selectedIndex[i]);
                loginTO.setUserId(Integer.parseInt(userId[index]));
                loginTO.setUserName(userName[index]);
                loginTO.setStatus(activeStatus[index]);
                userlist.add(loginTO);
            }

            modify = loginBP.modifyUserDetails(userlist, UserId1);
            if (modify != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "User Details Modified SuccessFully");
            }
            String str = (String) session.getAttribute("Letter");
            loginTO.setLetters(str);
            ArrayList modifyusers = new ArrayList();
            modifyusers = loginBP.getUserList(loginTO);
            session.removeAttribute("userList");
            request.setAttribute("userList", modifyusers);
            path = "content/user/manageuser.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to update user details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to View All Roles .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView allroles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        String menuPath = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Role-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Manage Role";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList rolelist = new ArrayList();
            rolelist = loginBP.getRoles();

            request.setAttribute("roleRecord", rolelist);
            path = "content/user/managerole.jsp";
            menuPath = "User   >>   Manage Role";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    /**
     * This method used to Add Page Of Role .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView addrole(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Role-Add")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Add Role";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/user/addrole.jsp";
            menuPath = "User  >>  Manage Role   >>   Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Modify Page Role
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterrole(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Role-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Alter Role";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList rolelist = new ArrayList();
            rolelist = loginBP.getRoles();
            request.setAttribute("roleRecord", rolelist);
            menuPath = "User  >>  Manage Role   >>   Alter";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/user/alterrole.jsp";
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view alter role data --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Add Role Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView insertrole(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
//        System.out.println("In insertrole");
        HttpSession session = request.getSession();
        loginCommand = command;
        int UserId = (Integer) session.getAttribute("userId");
        LoginTO loginTO = new LoginTO();
        String path = "";
        String menuPath = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Role-Add")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Manage Role";
            request.setAttribute("pageTitle", pageTitle);
            if (loginCommand.getRoletype() != "" && loginCommand.getRoletype() != null) {
                loginTO.setRoleName(loginCommand.getRoletype());
                loginTO.setUserId(UserId);
            }

            if (loginCommand.getRoledesc() != "" && loginCommand.getRoledesc() != null) {
                loginTO.setDescription(loginCommand.getRoledesc());
            }

            int status = 0;
            status = loginBP.insertRoleDetails(loginTO);
            if (status != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Roles Added Successfully");
            }
            session.removeAttribute("roleRecord");
            ArrayList rolelist = new ArrayList();
            rolelist = loginBP.getRoles();

            request.setAttribute("roleRecord", rolelist);
            path = "content/user/managerole.jsp";
            menuPath = "User  >>  Manage Role";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to insert role data --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Modify Role Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView modifyroles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "";
        String menuPath = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Role-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String[] roleId = loginCommand.getRoleId();
            String[] roletype = loginCommand.getRoleName();
            String[] description = loginCommand.getDesc();
            String[] activeStatus = loginCommand.getActiveInds();
            String[] selectedIndex = loginCommand.getSelectedUsers();
            int index = 0;
            int modify = 0;
            int UserId = (Integer) session.getAttribute("userId");
            ArrayList rolelist = new ArrayList();
            LoginTO loginTO = new LoginTO();
            for (int i = 0; i < selectedIndex.length; i++) {
                loginTO = new LoginTO();
                index = Integer.parseInt(selectedIndex[i]);
                loginTO.setRoleId(Integer.parseInt(roleId[index]));
                loginTO.setRoleName(roletype[index]);
                loginTO.setDescription(description[index]);
                loginTO.setStatus(activeStatus[index]);
                rolelist.add(loginTO);
            }

            modify = loginBP.modifyRoleDetails(rolelist, UserId);
//            System.out.println("modify"+modify);
            if (modify == 0) {
                request.setAttribute("errorMessage", "Updation failed");
            } else {
                request.setAttribute("successMessage", "Updated successfully");
                String pageTitle = "Manage Role";
                request.setAttribute("pageTitle", pageTitle);
                session.removeAttribute("roleRecord");
                ArrayList modifyroles = new ArrayList();
                modifyroles = loginBP.getRoles();
                request.setAttribute("roleRecord", modifyroles);
                path = "content/user/managerole.jsp";
                menuPath = "User  >>  Manage Role";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            }
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to update role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Get user Role Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView userroles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        String menuPath = "";
        HttpSession session = request.getSession();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "UserRole-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "User Role";
            request.setAttribute("pageTitle", pageTitle);
            menuPath = "User  >>  User Roles ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("userName", "");
            path = "content/user/userroles.jsp";
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to update role details --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView(path);

    }

    /**
     * This method used to View Page of Modify User Password.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterPassword(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Password-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Alter Password";
            request.setAttribute("pageTitle", pageTitle);
            menuPath = "User  >>  Modify Password ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/user/modifyUser.jsp";
//            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to update role details --> " + exception);
            exception.printStackTrace();
        }

        session.removeAttribute("userPassword");
        return new ModelAndView(path);
    }

    public ModelAndView handleRecoverPasswordPage(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        String path = "";
        String menuPath = "";
        menuPath = "User  >>  Modify Password ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Recover Password";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Password-Recover")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/user/recoverPassword.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView(".bt.error");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to retrieve user password  --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Recover User Password .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView recoverPass(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        String path = "";
        String menuPath = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Password-Recover")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (loginCommand.getUserName() != null && !"".equals(loginCommand.getUserName())) {
                loginTO.setUserName(loginCommand.getUserName());
            }
            menuPath = "User  >>  Recover Password ";
            String pageTitle = "Recover Password";
            request.setAttribute("pageTitle", pageTitle);
            String password = "";
            path = "content/user/recoverPassword.jsp";
            password = loginBP.getUserPassword(loginTO);
            if (password != null & password != "") {
                session.removeAttribute("userPassword");
                CryptoLibrary cl = new CryptoLibrary();
                password = cl.decrypt(password);
                request.setAttribute("userPassword", password);
//                System.out.println("password is  " + password);
                request.setAttribute("userName", loginCommand.getUserName());
            }
            session.setAttribute("password", password);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView(".bt.error");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to retrieve user password  --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Modify User Password.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView changePass(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        String menuPath = "";
        menuPath = "User  >>  Modify Password ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String pageTitle = "Alter Password";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Password-Change")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (loginCommand.getUserName() != null && !"".equals(loginCommand.getUserName())) {
                loginTO.setUserName(loginCommand.getUserName());
            }

            if (loginCommand.getUserId() != null && !"".equals(loginCommand.getUserId())) {
                loginTO.setUserId(Integer.parseInt(loginCommand.getUserId()));
            }

            if (loginCommand.getNewPassword() != null && !"".equals(loginCommand.getNewPassword())) {
                CryptoLibrary cl = new CryptoLibrary();

                String password = cl.encrypt(loginCommand.getNewPassword());
//                System.out.println("password " + password);
                loginTO.setPassword(password);
            }

            int updateStatus = 0;
            updateStatus = loginBP.changePassword(loginTO, userId);
            if (updateStatus != 0) {
                session.setAttribute("password", loginCommand.getNewPassword());
                request.setAttribute("successMessage", "User Password Modified Successfully");
            } else {
                request.setAttribute("errorMessage", "User Password Modification Failed");
            }
            path = "content/user/modifyUser.jsp";
//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView(".bt.error");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to change user password --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Get User Role Details .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView searchUserRoles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        LoginTO loginTO = new LoginTO();
        //int userId = (Integer) session.getAttribute("userId");
        String user = loginCommand.getUserName();
//        System.out.println(" userName " + user);
        String path = "";
        String menuPath = "";
        menuPath = "User  >>  User Roles ";
        String pageTitle = "User Role";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "UserRole-Search")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/user/userroles.jsp";
            if (loginCommand.getUserName() != null && loginCommand.getUserName() != "") {
                loginTO.setUserName(loginCommand.getUserName());
                request.setAttribute("userName", loginCommand.getUserName());
            }

            ArrayList userrole = new ArrayList();
            ArrayList rolelist = new ArrayList();
            userrole = loginBP.getUserRoles(user);
            rolelist = loginBP.getMinRoles(user);
//            System.out.println("rolelist.size() " + rolelist.size() + " userrole.size() " + userrole.size());

            request.setAttribute("userName", user);
            request.setAttribute("Rolelist", rolelist);
            request.setAttribute("userRoles", userrole);
            path = "content/user/userroles.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView(".bt.error");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search user roles--> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Save User Role Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveUserRoles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String pageTitle = "User Role";
        request.setAttribute("pageTitle", pageTitle);
        String user = loginCommand.getUserName();
        request.setAttribute("userName", loginCommand.getUserName());
//        System.out.println("userName " + user);
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute("userName", user);
        String path = "";
        String menuPath = "";
        menuPath = "User  >>  User Roles ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "UserRole-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String[] roles = loginCommand.getAssignedRole();

//            System.out.println("length is  " + roles.length);
            int status = 0;
            ArrayList userrole = new ArrayList();
            ArrayList rolelist = new ArrayList();
            if (roles == null) {
                status = loginBP.deleteroles(roles, user, userId);
                if (status != 0) {

                    userrole = loginBP.getUserRoles(user);
                    rolelist = loginBP.getMinRoles(user);

                    request.setAttribute("Rolelist", rolelist);
                    request.setAttribute("userRoles", userrole);
                    //request.setAttribute("userName", user);
                    path = "content/user/userroles.jsp";
                }

            } else {

                for (int i = 0; i < roles.length; i++) {
//                    System.out.println("RoleId is :" + roles[i]);
                }

                if (userrole.size() != 0 || userrole.size() == 0) {
                    session.removeAttribute("Rolelist");
                    session.removeAttribute("userRoles");
                    status = loginBP.saveuserroles(roles, user, userId);
                    if (status != 0) {
                        userrole = loginBP.getUserRoles(user);
                        rolelist = loginBP.getMinRoles(user);
                        session.setAttribute("Rolelist", rolelist);
                        session.setAttribute("userRoles", userrole);
                        path = "content/user/userroles.jsp";
                    } else {
//                        System.out.println("Roles not assigned");
                        path = "content/user/userroles.jsp";
                    }

                }
//                }
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "User Roles Assigned Successfully");
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView(".bt.error");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to save user roles--> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to GEt All Active Roles.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView allActiveRoles(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rolelist = new ArrayList();
        String path = "";
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "RoleFunction-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            rolelist = loginBP.getActiveRoles();

            request.setAttribute("roleList", rolelist);
            path = "content/user/rolefunctions.jsp";

//            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);

    }

    /**
     * This method used to Get All Functions.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView getFunctions(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        loginCommand = command;
        // System.out.println("in getFunctions ");
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "RoleFunction-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int roleId = Integer.parseInt(loginCommand.getRolesId());

            ArrayList funcLists = new ArrayList();
            ArrayList funcList = new ArrayList();
            funcList = loginBP.getAvailableFunction(roleId);
            funcLists = loginBP.getAssignedFucntions(roleId);
            // System.out.println("funcList "+funcList.size());
            if (funcList.size() != 0) {
                ArrayList rolenames = new ArrayList();
                rolenames = loginBP.getActiveRoles();
                request.setAttribute("roleList", rolenames);
                // System.out.println("size is " + funcList.size());
                request.setAttribute("rolelist", roleId);
                request.setAttribute("AvailableFunction", funcLists);
                request.setAttribute("AssignedFucntions", funcList);
                request.setAttribute("PathText", "Manage Role-Functions   >   Assign Roles-Functions");
                path = "content/user/rolefunctions.jsp";
            } else {
                ArrayList rolenames = new ArrayList();
                rolenames = loginBP.getActiveRoles();
                request.setAttribute("roleList", rolenames);
                request.setAttribute("rolelist", roleId);
                request.setAttribute("AvailableFunction", funcLists);
                request.setAttribute("AssignedFucntions", funcList);
                request.setAttribute("PathText", "Manage Role-Functions   >   Assign Roles-Functions");
                path = "content/user/rolefunctions.jsp";

//                }
            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Assign Functions.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView assignFunctions(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();

        String path = "";
        loginCommand = command;
        //System.out.println("in getFunctions ");
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "";
        menuPath = "User  >>  Role Function ";
        String pageTitle = "Role Function";
        request.setAttribute("pageTitle", pageTitle);

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "RoleFunction-Modify")) {
                menuPath = "Not Authorised";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                path = "content/common/NotAuthorized.jsp";
            } else {
                int roleId = Integer.parseInt(loginCommand.getRolesId());
                // System.out.println("roleId is  " + roleId);
                String[] assignedFucntion = loginCommand.getAssignedfunc();
                // System.out.println("length is  " + assignedFucntion.length);
                int status = 0;
                path = "content/user/rolefunctions.jsp";
                if (assignedFucntion == null) {
                    status = loginBP.deletefunctions(assignedFucntion, roleId, userId);
                    ArrayList funcLists = new ArrayList();
                    ArrayList funcList = new ArrayList();
                    funcLists = loginBP.getAssignedFucntions(roleId);

                    funcList = loginBP.getAvailableFunction(roleId);
                    request.setAttribute("AvailableFunction", funcLists);
                    request.setAttribute("AssignedFucntions", funcList);

                } else {
                    ArrayList funcLists = new ArrayList();
                    funcLists = loginBP.getAssignedFucntions(roleId);
                    //System.out.println("size of funcLists is " + funcLists.size());
                    ArrayList funcList = new ArrayList();
                    ArrayList rolenames = new ArrayList();
                    rolenames = loginBP.getActiveRoles();
                    request.setAttribute("roleList", rolenames);
                    request.setAttribute("rolelist", roleId);
                    if (funcLists.size() != 0) {
                        //System.out.println("i am here fc");
                        status = loginBP.insertfunctions(assignedFucntion, roleId, userId);
                        if (status != 0) {

                            funcLists = loginBP.getAssignedFucntions(roleId);

                            funcList = loginBP.getAvailableFunction(roleId);
                            request.setAttribute("AvailableFunction", funcLists);
                            request.setAttribute("AssignedFucntions", funcList);
                            request.setAttribute("rolelist", roleId);

                            ArrayList getFunction = new ArrayList();
                            getFunction = loginBP.getUserFunctions(userId);
                            //System.out.println("userFunction size is : " + getFunction.size());
                            session.setAttribute("userFunction", getFunction);

                            path = "content/user/rolefunctions.jsp";

                        } else {
                            funcList = loginBP.getAvailableFunction(roleId);
                            //request.setAttribute("Rolelist", rolenames);
                            request.setAttribute("AssignedFucntions", funcList);
                            ArrayList getFunction = new ArrayList();
                            getFunction = loginBP.getUserFunctions(userId);
                            //System.out.println("userFunction size is : " + getFunction.size());
                            session.setAttribute("userFunction", getFunction);
                            path = "content/user/rolefunctions.jsp";

                        }

                    } else {
                        status = loginBP.insertfunctions(assignedFucntion, roleId, userId);
                        if (status != 0) {

                            funcLists = loginBP.getAssignedFucntions(roleId);

                            funcList = loginBP.getAvailableFunction(roleId);
                            request.setAttribute("AvailableFunction", funcLists);
                            request.setAttribute("AssignedFucntions", funcList);
                            ArrayList getFunction = new ArrayList();
                            getFunction = loginBP.getUserFunctions(userId);
                            //System.out.println("userFunction size is : " + getFunction.size());
                            session.setAttribute("userFunction", getFunction);
                            path = "content/user/rolefunctions.jsp";

                        } else {
                            funcList = loginBP.getAvailableFunction(roleId);

                            request.setAttribute("AssignedFucntions", funcList);
                            ArrayList getFunction = new ArrayList();
                            getFunction = loginBP.getUserFunctions(userId);
                            //System.out.println("userFunction size is : " + getFunction.size());
                            session.setAttribute("userFunction", getFunction);
                            path = "content/user/rolefunctions.jsp";

                        }

                    }
                }
            }
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Role Function Assigned Successfully");
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to view role details --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to handle Logout .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleLogout(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rolelist = new ArrayList();
        String path = "";
        try {
//            loginBP.loginHistory((Integer) session.getAttribute("userId"), request.getRequestedSessionId() , request.getRemoteAddr() ,"","logout");
            session.invalidate();

            String responsiveStatus = request.getParameter("respStatus");
            if (responsiveStatus == null) {
                if (request.getAttribute("respStatus") != null) {
                    responsiveStatus = (String) request.getAttribute("respStatus");
                }
                path = "content/common/login.jsp";
            }
            if (responsiveStatus != null && "Y".equals(responsiveStatus)) {
                request.setAttribute("respStatus", "Y");
                path = "content/common/loginResponsive.jsp";
            } else {
                path = "content/common/login.jsp";
            }
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to handleLogout --> " + exception);
            exception.printStackTrace();
        }

        return new ModelAndView(path);
    }

    /**
     * This method used to Handle Home .
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleHomePage(HttpServletRequest request, HttpServletResponse reponse, LoginCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList rolelist = new ArrayList();
        String path = "";
        String menuPath = "Login   >>   Home Page ";
        String pageTitle = "Home Page";
        request.setAttribute("pageTitle", pageTitle);
        try {
//                   session.invalidate();
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to handleLogout --> " + exception);
            exception.printStackTrace();
        }
        return new ModelAndView("content/common/layout.jsp");

    }

    public void handleUserNameList(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws IOException {

        HttpSession session = request.getSession();
        loginCommand = command;

        // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        //if (!loginBP.checkAuthorisation(userFunctions, "Designation-ViewPage")) {
        //  path = "content/common/NotAuthorized.jsp";
        //} else {
        String userName = loginCommand.getUserName();
//        System.out.println("staffName  " + userName);
        String suggestions = loginBP.getUserNameSuggestions(userName);
        PrintWriter writer = response.getWriter();
//        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public void checkUserName(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws IOException {
        loginCommand = command;
        String userName = loginCommand.getUserName();
        boolean checkStatus = false;

        checkStatus = loginBP.checkUserName(userName);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
//        System.out.println("check Status  " + checkStatus);
        if (checkStatus == true) {
            writer.print("");
        } else {
            writer.print("User Name Already Exist");
        }
        writer.close();
    }

    public ModelAndView driverEmbarkment(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverEmbDetail = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Driver Embarkment";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverEmbarkment.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView driverDetails(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverEmbDetail = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Setting >> Driver Embarkment";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverEmbarkment.jsp";
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                System.out.println("driverId===" + driverId);
                loginTO.setDriName(driverId);
            }
            driverEmbDetail = loginBP.getDriverEmbarkDetail(loginTO);
            if (driverEmbDetail.size() != 0) {
                request.setAttribute("driverEmbDetail", driverEmbDetail);
            }
            request.setAttribute("addFlag", "2");
            request.setAttribute("driName", loginCommand.getDriName());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveDriverV(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int SaveDriverV = 0;
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "";
        String menuPath = "Setting >> Driver Embarkment ";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        ArrayList driverEmbDetail = new ArrayList();
        try {
            path = "content/Driver/driverEmbarkment.jsp";
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);
            }
            if (loginCommand.getRegno() != null && !"".equals(loginCommand.getRegno())) {
                loginTO.setRegno(loginCommand.getRegno());
            }
            if (loginCommand.getEmbarkmentDate() != null && !"".equals(loginCommand.getEmbarkmentDate())) {
                loginTO.setEmbarkmentDate(loginCommand.getEmbarkmentDate());
            }
            if (loginCommand.getRemark() != null && !"".equals(loginCommand.getRemark())) {
                loginTO.setRemark(loginCommand.getRemark());
            }
            SaveDriverV = loginBP.getsaveDriverV(loginTO);
            if (SaveDriverV != 0) {
                driverEmbDetail = loginBP.getDriverEmbarkDetail(loginTO);
                //System.out.println("driverEmbDetail.size() = " + SaveDriverV);
                request.setAttribute("driverEmbDetail", driverEmbDetail);
            }
            //System.out.println("SaveDriverV------------------------"+SaveDriverV);
            request.setAttribute("driverEmbDetailStatus", SaveDriverV);

            if (SaveDriverV == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Driver Embarked Successfully");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Driver Embarked Failed");
            }
            request.setAttribute("driName", loginCommand.getDriName());
            request.setAttribute("regno", loginCommand.getRegno());
            request.setAttribute("embarkmentDate", loginCommand.getEmbarkmentDate());
            request.setAttribute("remark", loginCommand.getRemark());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView driverAlighting(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverEmbDetail = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverAlighting.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alightDriverDetails(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverEmbDetail = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverAlighting.jsp";
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);

            }
            driverEmbDetail = loginBP.getDriverEmbarkDetail(loginTO);
            if (driverEmbDetail.size() != 0) {
                System.out.println("driverEmbDetail.size() = " + driverEmbDetail.size());
                request.setAttribute("driverEmbDetail", driverEmbDetail);
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "This Driver Not Yet Embarked");
            }
            //request.setAttribute("driverEmbDetailSize", driverEmbDetail.size());
            request.setAttribute("driName", loginCommand.getDriName());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alightDateSaveDriverV(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        // ArrayList alightSaveDriverV = new ArrayList();
        int alightSaveDriverV = 0;
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "";
        String menuPath = "";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        ArrayList driverEmbDetail = new ArrayList();
        try {
            path = "content/Driver/driverAlighting.jsp";

            if (loginCommand.getEmpId() != null && !"".equals(loginCommand.getEmpId())) {
                loginTO.setEmpId(loginCommand.getEmpId());
                //  System.out.println("loginCommand.getEmpId()-----1-----" + loginCommand.getEmpId());
            }
            if (loginCommand.getEmbId() != null && !"".equals(loginCommand.getEmbId())) {
                loginTO.setEmbId(loginCommand.getEmbId());
                // System.out.println("loginCommand.getEmbId()------2----" + loginCommand.getEmbId());
            }
            if (loginCommand.getDriverName() != null && !"".equals(loginCommand.getDriverName())) {
                loginTO.setDriverName(loginCommand.getDriverName());
                //  System.out.println("loginCommand.getDriverName()----3------" + loginCommand.getDriverName());
            }
            if (loginCommand.getEmbDate() != null && !"".equals(loginCommand.getEmbDate())) {
                loginTO.setEmbDate(loginCommand.getEmbDate());
                //   System.out.println("loginCommand.getEmbDate()----4------" + loginCommand.getEmbDate());
            }
            if (loginCommand.getAliVehileNo() != null && !"".equals(loginCommand.getAliVehileNo())) {
                loginTO.setAliVehileNo(loginCommand.getAliVehileNo());
                //   System.out.println("loginCommand.getVehiNo()----5------" + loginCommand.getAliVehileNo());
            }
            if (loginCommand.getAlightingDateA() != null && !"".equals(loginCommand.getAlightingDateA())) {
                loginTO.setAlightingDateA(loginCommand.getAlightingDateA());
                //  System.out.println("loginCommand.getAlightingDateA()----6------" + loginCommand.getAlightingDateA());
            }
            if (loginCommand.getAlighingRemark() != null && !"".equals(loginCommand.getAlighingRemark())) {
                loginTO.setAlighingRemark(loginCommand.getAlighingRemark());
                // System.out.println("loginCommand.getAlighingRemark()----7------" + loginCommand.getAlighingRemark());
            }
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);

            }
            alightSaveDriverV = loginBP.getAlightsaveDriverV(loginTO);
            if (alightSaveDriverV != 0) {
                System.out.println("alightSaveDriverV = " + alightSaveDriverV);
                request.setAttribute("alightSaveDriverV", alightSaveDriverV);
                driverEmbDetail = loginBP.getDriverEmbarkDetail(loginTO);
                request.setAttribute("driverEmbDetail", driverEmbDetail);
            }
            request.setAttribute("driverEmbDetailStatus", alightSaveDriverV);
            request.setAttribute("empId", loginCommand.getEmpId());
            request.setAttribute("embId", loginCommand.getEmbId());
            request.setAttribute("driverName", loginCommand.getDriverName());
            request.setAttribute("embDate", loginCommand.getEmbDate());
            request.setAttribute("aliVehileNo", loginCommand.getAliVehileNo());
            request.setAttribute("alightingDateA", loginCommand.getAlightingDateA());
            request.setAttribute("alighingRemark", loginCommand.getAlighingRemark());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView openDriverRemarks(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverRemarks = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverRemarks.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView DriverRemarks(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverRemarks = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverRemarks.jsp";
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);
            }
            if (loginCommand.getRegno() != null && !"".equals(loginCommand.getRegno())) {
                loginTO.setRegno(loginCommand.getRegno());
            }
            driverRemarks = loginBP.getDriverRemarks(loginTO);
            System.out.println("driverRemarks.size() = " + driverRemarks.size());
            if (driverRemarks.size() != 0) {
                request.setAttribute("driverEmbDetail", driverRemarks);
            }
            request.setAttribute("driverEmbDetailSize", driverRemarks.size());
            System.out.println("driverRemarks.size()---" + driverRemarks.size());
            System.out.println("555555555555555555555555");

            request.setAttribute("driName", loginCommand.getDriName());
            request.setAttribute("regno", loginCommand.getRegno());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView additionalTrip(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverRemarks = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverAdditionalTrip.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView remarkSaveDriverV(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        //System.out.println(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        // ArrayList remarkSaveDriverV = new ArrayList();
        int remarkSaveDriverV = 0;
        ArrayList driverRemarks = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "";
        String menuPath = "";
        String[] temp = null;
        String driverId = "";
        String[] temp1 = null;
        String EmpId = "";
        String[] temp2 = null;
        String EmbId = "";
        String[] temp3 = null;
        String DriverName = "";
        String[] temp4 = null;
        String AliVehileNo = "";
        String[] temp5 = null;
        String ActInd = "";
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/Driver/driverRemarks.jsp";

            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                System.out.println("loginCommand.getDriName()-----111-----" + loginCommand.getDriName());
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                System.out.println("driverId----1------" + driverId);
                loginTO.setDriName(driverId);
                loginTO.setEmpId(driverId);
            }
            if (loginCommand.getEmbId() != null && !"".equals(loginCommand.getEmbId())) {
                temp2 = loginCommand.getEmbId().split(",");
                EmbId = temp2[0];
                loginTO.setEmbId(EmbId);
//                System.out.println("EmbId ====2a=== " + EmbId);
            }
            if (loginCommand.getDriverName() != null && !"".equals(loginCommand.getDriverName())) {
                temp3 = loginCommand.getDriverName().split(",");
                DriverName = temp3[0];
                loginTO.setDriverName(DriverName);
//                System.out.println("DriverName ====2a=== " + DriverName);
            }
            if (loginCommand.getAliVehileNo() != null && !"".equals(loginCommand.getAliVehileNo())) {
                temp4 = loginCommand.getAliVehileNo().split(",");
                AliVehileNo = temp4[0];
                loginTO.setAliVehileNo(AliVehileNo);
//                System.out.println("AliVehileNo ====2a=== " + AliVehileNo);
            }
            if (loginCommand.getActInd() != null && !"".equals(loginCommand.getActInd())) {
                temp5 = loginCommand.getActInd().split(",");
                ActInd = temp5[0];
                loginTO.setActInd(ActInd);
//                System.out.println("ActInd ====2a=== " + ActInd);
            }
            if (loginCommand.getAlighingRemark() != null && !"".equals(loginCommand.getAlighingRemark())) {
                loginTO.setAlighingRemark(loginCommand.getAlighingRemark());
                System.out.println("loginCommand.getAlighingRemark()----7------" + loginCommand.getAlighingRemark());
            }
            remarkSaveDriverV = loginBP.getRemarkSaveDriverV(loginTO);
            if (remarkSaveDriverV != 0) {
                System.out.println("remarkSaveDriverV.size() = " + remarkSaveDriverV);
                request.setAttribute("remarkSaveDriverV", remarkSaveDriverV);
                driverRemarks = loginBP.getDriverRemarks(loginTO);
                request.setAttribute("driverEmbDetail", driverRemarks);
            }
            request.setAttribute("driverEmbDetailVal", remarkSaveDriverV);

            request.setAttribute("empId", loginCommand.getEmpId());
            request.setAttribute("embId", loginCommand.getEmbId());
            request.setAttribute("driverName", loginCommand.getDriverName());
            request.setAttribute("embDate", loginCommand.getEmbDate());
            request.setAttribute("aliVehileNo", loginCommand.getAliVehileNo());
            request.setAttribute("alightingDateA", loginCommand.getAlightingDateA());
            request.setAttribute("alighingRemark", loginCommand.getAlighingRemark());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView driverAdditionalTrip(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList driverAdditional = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        try {
            path = "content/Driver/driverAdditionalTrip.jsp";
            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);

            }
            System.out.println("1");
            driverAdditional = loginBP.getDriverAdditionalTrip(loginTO);
            if (driverAdditional.size() != 0) {
                //    System.out.println("driverAdditional.size() = " + driverAdditional.size());
                request.setAttribute("driverAdditional", driverAdditional);
            }
            System.out.println("2");
            request.setAttribute("driverEmbDetailSize", driverAdditional.size());
            System.out.println("driverAdditional.size()===" + driverAdditional.size());
            request.setAttribute("driName", loginCommand.getDriName());
            System.out.println("3");

            ArrayList rList = new ArrayList();
            rList = vehicleBP.getRouteList();
            System.out.println("4");
            request.setAttribute("routeList", rList);
            System.out.println("5");

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAdditionalTripV(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        // System.out.println(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        //ArrayList additionalTripV = new ArrayList();
        int additionalTripV = 0;
        ArrayList driverAdditional = new ArrayList();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "";
        String menuPath = "";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        String driverId = "";
        String[] tempRout = null;
        String routeidval = "";
        try {
            path = "content/Driver/driverAdditionalTrip.jsp";

            if (loginCommand.getDriName() != null && !"".equals(loginCommand.getDriName())) {
                // loginTO.setDriName(loginCommand.getDriName());
                temp = loginCommand.getDriName().split("-");
                driverId = temp[1];
                loginTO.setDriName(driverId);

                // System.out.println("loginCommand.getDriName()-----1-----" + loginCommand.getDriName());
            }

            if (loginCommand.getEmbId() != null && !"".equals(loginCommand.getEmbId())) {
                loginTO.setEmbId(loginCommand.getEmbId());
                //  System.out.println("loginCommand.getEmbId()----2------" + loginCommand.getEmbId());
            }
            if (loginCommand.getRegno() != null && !"".equals(loginCommand.getRegno())) {
                loginTO.setRegno(loginCommand.getRegno());
                // System.out.println("loginCommand.getRegno()----2------" + loginCommand.getRegno());
            }
            if (loginCommand.getEmbarkmentDate() != null && !"".equals(loginCommand.getEmbarkmentDate())) {
                loginTO.setEmbarkmentDate(loginCommand.getEmbarkmentDate());
                //  System.out.println("loginCommand.getEmbarkmentDate()----3------" + loginCommand.getEmbarkmentDate());
            }
            if (loginCommand.getRouteName() != null && !"".equals(loginCommand.getRouteName())) {
                tempRout = loginCommand.getRouteName().split("-");
                routeidval = tempRout[1];
                loginTO.setRouteName(routeidval);
                //  System.out.println("routeidval----4------" + routeidval);
            }

            if (loginCommand.getAddTrip() != null && !"".equals(loginCommand.getAddTrip())) {
                loginTO.setAddTrip(loginCommand.getAddTrip());
                // System.out.println("loginCommand.getAddTrip()----5------" + loginCommand.getAddTrip());
            }
            if (loginCommand.getFromDate() != null && !"".equals(loginCommand.getFromDate())) {
                loginTO.setFromDate(loginCommand.getFromDate());
                // System.out.println("loginCommand.getFromDate()----6----" + loginCommand.getFromDate());
            }
            if (loginCommand.getToDate() != null && !"".equals(loginCommand.getToDate())) {
                loginTO.setToDate(loginCommand.getToDate());
                // System.out.println("loginCommand.getToDate()----7-----" + loginCommand.getToDate());
            }
            if (loginCommand.getRemark() != null && !"".equals(loginCommand.getRemark())) {
                loginTO.setRemark(loginCommand.getRemark());
                // System.out.println("loginCommand.getRemark()----8------" + loginCommand.getRemark());
            }
            additionalTripV = loginBP.getsaveAdditionalTripV(loginTO);
            if (additionalTripV != 0) {
                // System.out.println("remarkSaveDriverV.size() = " + additionalTripV.size());
                request.setAttribute("remarkSaveDriverV", additionalTripV);
                driverAdditional = loginBP.getDriverAdditionalTrip(loginTO);
                System.out.println("driverAdditional=+---+=" + driverAdditional.size());
                request.setAttribute("driverAdditional", driverAdditional);
            }
            request.setAttribute("addSaveDriverStatus", additionalTripV);

            request.setAttribute("embId", loginCommand.getEmbId());
            request.setAttribute("driName", loginCommand.getDriName());
            request.setAttribute("regno", loginCommand.getRegno());
            request.setAttribute("embarkmentDate", loginCommand.getEmbarkmentDate());
            request.setAttribute("remark", loginCommand.getRemark());
            request.setAttribute("addTrip", loginCommand.getAddTrip());

            request.setAttribute("routeName", loginCommand.getRouteName());
            request.setAttribute("fromDate", loginCommand.getFromDate());
            request.setAttribute("toDate", loginCommand.getToDate());

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleRouteName(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // System.out.println("in ajax ");
        HttpSession session = request.getSession();
        String routeName = request.getParameter("routeName");
        // System.out.println("routeName---" + routeName);
        String suggestions = loginBP.handleRouteName(routeName);
        // System.out.println("suggestions----" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public void driverVehicle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // System.out.println("test---------------------------------------------- ");
        HttpSession session = request.getSession();
        String regno = request.getParameter("regno");
        //  System.out.println("regno" + regno);
        String suggestions = loginBP.driverVehicle(regno);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public ModelAndView roleMenu(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        int roleId = (Integer) session.getAttribute("RoleId");
        boolean status = false;
        try {
            String accessRoleId = request.getParameter("roleId");
            String menuId = request.getParameter("menuId");
            String subMenuId = request.getParameter("subMenuId");
            if (accessRoleId != null && !"".equals(accessRoleId)) {
                status = true;
                loginTO.setAccessRoleId(accessRoleId);
                request.setAttribute("roleId", accessRoleId);
            } else {
                request.setAttribute("roleId", 0);
            }
            if (menuId != null && !"".equals(menuId)) {
                status = true;
                loginTO.setMenuId(menuId);
                request.setAttribute("menuId", menuId);
            } else {
                loginTO.setMenuId("");
                request.setAttribute("menuId", 0);
            }
            if (subMenuId != null && !"".equals(subMenuId)) {
                status = true;
                loginTO.setSubMenuId(subMenuId);
                request.setAttribute("subMenuId", subMenuId);
            } else {
                loginTO.setSubMenuId("");
                request.setAttribute("subMenuId", 0);
            }
            ArrayList menuList = new ArrayList();
            ArrayList submenuList = new ArrayList();
            if (status) {
                loginTO.setRoleId(roleId);
                Map<String, String> map = new HashMap<String, String>();
                ArrayList roleMenu = new ArrayList();
                ArrayList roleMenuList = new ArrayList();
                roleMenuList = loginBP.getRoleMenuList(loginTO);
                /*  Iterator itr = roleMenuList.iterator();
                LoginTO lTO = new LoginTO();
                LoginTO logTO = new LoginTO();
                int serialNo = 0;
                while(itr.hasNext()){
                    logTO = new LoginTO();
                    lTO = (LoginTO) itr.next();
                    if(map.containsKey(lTO.getMenuName())){
                        map.put(lTO.getMenuName(), "");
                        logTO.setMenuName("");
                        logTO.setSerialNo("");
                    }else{
                        map.put(lTO.getMenuName(), lTO.getMenuName());
                        logTO.setMenuName(lTO.getMenuName());
                        logTO.setSerialNo(String.valueOf(serialNo++));
                    }
                    if(map.containsKey(lTO.getSubMenuName())){
                        map.put(lTO.getSubMenuName(), "");
                        logTO.setSubMenuName("");
                    }else{
                        map.put(lTO.getSubMenuName(), lTO.getSubMenuName());
                        logTO.setSubMenuName(lTO.getSubMenuName());
                        logTO.setMenuName(lTO.getMenuName());
                        logTO.setSerialNo(String.valueOf(serialNo++));
                    }
                    System.out.println("map = " + map);
                    logTO.setFunctionName(lTO.getFunctionName());
                    logTO.setMenuId(lTO.getMenuId());
                    logTO.setSubMenuId(lTO.getSubMenuId());
                    logTO.setFunctionId(lTO.getFunctionId());
                    logTO.setFunctionSts(lTO.getFunctionSts());
                    roleMenu.add(logTO);
                } */
                request.setAttribute("roleMenuList", roleMenuList);
                menuList = loginBP.getMenuList(loginTO);
                submenuList = loginBP.getSubMenuList(loginTO);
            }
            ArrayList rolelist = new ArrayList();
            rolelist = loginBP.getRoles();
            request.setAttribute("roleRecord", rolelist);
            request.setAttribute("menuList", menuList);
            request.setAttribute("submenuList", submenuList);
            path = "content/user/manageroleMenu.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveRoleMenu(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        LoginTO loginTO = new LoginTO();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        int roleId = (Integer) session.getAttribute("RoleId");
        boolean status = false;
        try {
            String accessRoleId = request.getParameter("roleId");
            String menuId = request.getParameter("menuId");
            String subMenuId = request.getParameter("subMenuId");
            String[] menuIds = request.getParameterValues("menuIds");
            loginTO.setMenuIds(menuIds);
            String[] subMenuIds = request.getParameterValues("subMenuIds");
            loginTO.setSubMenuIds(subMenuIds);
            String[] functionIds = request.getParameterValues("functionIds");
            loginTO.setFunctionIds(functionIds);
            String[] functionStatus = request.getParameterValues("functionStatus");
            loginTO.setFunctionStatus(functionStatus);
            String[] insertStatus = request.getParameterValues("insertStatus");
            loginTO.setInsertQueryStatus(insertStatus);
            if (accessRoleId != null && !"".equals(accessRoleId)) {
                status = true;
                loginTO.setAccessRoleId(accessRoleId);
                request.setAttribute("roleId", accessRoleId);
            } else {
                request.setAttribute("roleId", 0);
            }
            if (menuId != null && !"".equals(menuId)) {
                status = true;
                loginTO.setMenuId(menuId);
                request.setAttribute("menuId", menuId);
            } else {
                loginTO.setMenuId("");
                request.setAttribute("menuId", 0);
            }
            if (subMenuId != null && !"".equals(subMenuId)) {
                status = true;
                loginTO.setSubMenuId(subMenuId);
                request.setAttribute("subMenuId", subMenuId);
            } else {
                loginTO.setSubMenuId("");
                request.setAttribute("subMenuId", 0);
            }
            ArrayList menuList = new ArrayList();
            ArrayList submenuList = new ArrayList();
            if (status) {
                if (!"0".equals(accessRoleId)) {
                    loginBP.saveRoleMenu(loginTO);
                }
                loginTO.setRoleId(roleId);
                Map<String, String> map = new HashMap<String, String>();
                ArrayList roleMenu = new ArrayList();
                ArrayList roleMenuList = new ArrayList();
                roleMenuList = loginBP.getRoleMenuList(loginTO);
                /* Iterator itr = roleMenuList.iterator();
                LoginTO lTO = new LoginTO();
                LoginTO logTO = new LoginTO();
                int serialNo = 0;
                while(itr.hasNext()){
                    logTO = new LoginTO();
                    lTO = (LoginTO) itr.next();
                    if(map.containsKey(lTO.getMenuName())){
                        map.put(lTO.getMenuName(), "");
                        logTO.setMenuName("");
                        logTO.setSerialNo("");
                    }else{
                        map.put(lTO.getMenuName(), lTO.getMenuName());
                        logTO.setMenuName(lTO.getMenuName());
                        logTO.setSerialNo(String.valueOf(serialNo++));
                    }
                    if(map.containsKey(lTO.getSubMenuName())){
                        map.put(lTO.getSubMenuName(), "");
                        logTO.setSubMenuName("");
                    }else{
                        map.put(lTO.getSubMenuName(), lTO.getSubMenuName());
                        logTO.setSubMenuName(lTO.getSubMenuName());
                        logTO.setMenuName(lTO.getMenuName());
                        logTO.setSerialNo(String.valueOf(serialNo++));
                    }
                    System.out.println("map = " + map);
                    logTO.setFunctionName(lTO.getFunctionName());
                    logTO.setMenuId(lTO.getMenuId());
                    System.out.println("lTO.getMenuId() = " + lTO.getMenuId());
                    logTO.setFunctionSts(lTO.getFunctionSts());
                    logTO.setSubMenuId(lTO.getSubMenuId());
                    logTO.setFunctionId(lTO.getFunctionId());
                    roleMenu.add(logTO);
                } */
                request.setAttribute("roleMenuList", roleMenuList);
                menuList = loginBP.getMenuList(loginTO);
                submenuList = loginBP.getSubMenuList(loginTO);
            }
            request.setAttribute("menuList", menuList);
            request.setAttribute("submenuList", submenuList);

            ArrayList rolelist = new ArrayList();
            rolelist = loginBP.getRoles();
            request.setAttribute("roleRecord", rolelist);
            path = "content/user/manageroleMenu.jsp";
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView invalidAccess(HttpServletRequest request, HttpServletResponse response, LoginCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        loginCommand = command;
        String path = "  ";
        String menuPath = "Settint";
        request.setAttribute("menuPath", menuPath);
        String[] temp = null;
        try {
            path = "content/common/NotAuthorized.jsp";

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

}
