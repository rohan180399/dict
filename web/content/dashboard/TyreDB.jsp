<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<html lang="en">
<head>
        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a  {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:140%;
}
	.column { width: 435px; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>





<%






// used dash board codes





String tyreDistribution = "<chart caption='' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+
                          "<set value='20' label='Apollo'  color='3EA99F' alpha='60'/>"+
                          "<set value='30' label='JK Tyres'  color='99CC00' alpha='60'/>"+
                          "<set value='40' label='MRF'  color='357EC7' alpha='60'/>"+
                          "<set value='50' label='BridegeStone'  color='F535AA' alpha='60'/>"+
                          "<set value='60' label='Michelim' color='F87431' alpha='60'/>"+
                          "</chart>";













String apolloTyres = "<chart yAxisName='Values' caption='Apollo Tyre Life' numberPrefix='Km ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New' value='40000' color='3EA99F' />  "+
        "<set label='1st ReTread' value='25000' color='F87431' /> "+
         "<set label='2nd ReTread' value='15000' color='F535AA' /> "+
          "<set label='3rd ReTread' value='10000' color='357EC7' /> "+
        "</chart> ";

String jkTyres = "<chart yAxisName='Values' caption='JK Tyre Life' numberPrefix='Km ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New' value='44000' color='3EA99F' />  "+
        "<set label='1st ReTread' value='21000' color='F87431' /> "+
         "<set label='2nd ReTread' value='18000' color='F535AA' /> "+
          "<set label='3rd ReTread' value='8000' color='357EC7' /> "+
        "</chart> ";
String mrfTyres = "<chart yAxisName='Values' caption='MRF Tyre Life' numberPrefix='Km ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New' value='36000' color='3EA99F' />  "+
        "<set label='1st ReTread' value='22000' color='F87431' /> "+
         "<set label='2nd ReTread' value='14000' color='F535AA' /> "+
          "<set label='3rd ReTread' value='11000' color='357EC7' /> "+
        "</chart> ";
String bsTyres = "<chart yAxisName='Values' caption='BridgeStone Tyre Life' numberPrefix='Km ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New' value='48000' color='3EA99F' />  "+
        "<set label='1st ReTread' value='29000' color='F87431' /> "+
         "<set label='2nd ReTread' value='18000' color='F535AA' /> "+
          "<set label='3rd ReTread' value='14000' color='357EC7' /> "+
        "</chart> ";
String mTyres = "<chart yAxisName='Values' caption='Michelim Tyre Life' numberPrefix='Km ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='New' value='30000' color='3EA99F' />  "+
        "<set label='1st ReTread' value='20000' color='F87431' /> "+
         "<set label='2nd ReTread' value='11000' color='F535AA' /> "+
          "<set label='3rd ReTread' value='6000' color='357EC7' /> "+
        "</chart> ";



%>

<div id="expand">
<div class="column" style="width:430px;">
	
	
        
        <div class="portlet" >
            <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Tyre Distribution</div>
                <div class="portlet-content">
                <table align="center"   cellspacing="0px" >
                <tr>
                    <td>
                        <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                            <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                            <jsp:param name="strURL" value="" />
                            <jsp:param name="strXML" value="<%=tyreDistribution %>" />
                            <jsp:param name="chartId" value="productSales" />
                            <jsp:param name="chartWidth" value="400" />
                            <jsp:param name="chartHeight" value="250" />
                            <jsp:param name="debugMode" value="false" />
                            <jsp:param name="registerWithJS" value="false" />
                        </jsp:include>
                    </td>
                </tr>
                </table>
                </div>
	</div>
    <div class="portlet" >
                <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Apollo Tyres Life </div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=apolloTyres %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="400" />
                                <jsp:param name="chartHeight" value="250" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>

</div>
<div class="column" style="width:430px;">
	
    <div class="portlet" >
                    <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;JK Tyre Life</div>
                        <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=jkTyres %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="400" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                        </div>
	</div>
    <div class="portlet" >
                        <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;BridgeStone Tyre Life</div>
                            <div class="portlet-content">
                            <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                        <jsp:param name="strURL" value="" />
                                        <jsp:param name="strXML" value="<%=bsTyres %>" />
                                        <jsp:param name="chartId" value="productSales" />
                                        <jsp:param name="chartWidth" value="400" />
                                        <jsp:param name="chartHeight" value="250" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                            </table>
                            </div>
	</div>

</div>

    <div class="column" style="width:430px;">
	
            <div class="portlet" >
	                            <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Michelim Tyre Life</div>
	                                <div class="portlet-content">
	                                <table align="center"   cellspacing="0px" >
	                                <tr>
	                                    <td>
	                                        <jsp:include page="FusionChartsRenderer.jsp" flush="true">
	                                            <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
	                                            <jsp:param name="strURL" value="" />
	                                            <jsp:param name="strXML" value="<%=mTyres %>" />
	                                            <jsp:param name="chartId" value="productSales" />
	                                            <jsp:param name="chartWidth" value="400" />
	                                            <jsp:param name="chartHeight" value="250" />
	                                            <jsp:param name="debugMode" value="false" />
	                                            <jsp:param name="registerWithJS" value="false" />
	                                        </jsp:include>
	                                    </td>
	                                </tr>
	                                </table>
	                                </div>
	</div>
        <div class="portlet" >
			    <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;MRF Tyre Life </div>
				<div class="portlet-content">
				<table align="center"   cellspacing="0px" >
				<tr>
				    <td>
					<jsp:include page="FusionChartsRenderer.jsp" flush="true">
					    <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
					    <jsp:param name="strURL" value="" />
					    <jsp:param name="strXML" value="<%=mrfTyres %>" />
					    <jsp:param name="chartId" value="productSales" />
					    <jsp:param name="chartWidth" value="400" />
					    <jsp:param name="chartHeight" value="250" />
					    <jsp:param name="debugMode" value="false" />
					    <jsp:param name="registerWithJS" value="false" />
					</jsp:include>
				    </td>
				</tr>
				</table>
				</div>
	</div>

</div>
    


</div><!-- End demo -->
</body>
</html>
