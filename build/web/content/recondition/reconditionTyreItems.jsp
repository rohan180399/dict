<%-- 
    Document   : addVehicle
    Created on : Feb 27, 2009, 6:46:00 PM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>


        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
        <script language="javascript">    
            
            
            function submitPage(value) { 
                if(validate()=='fail'){                    
                    return;
                }   
                if(value == "save"){                                          
                    document.addVehicle.action = '/throttle/addTyreFaultItems.do';
                    document.addVehicle.submit();
                }
            }
            
            
            var httpRequest;          
            function getVehicleDetails() { 
                if(document.addVehicle.regNo.value != ''){
                var url='/throttle/checkVehicleExists.do?regno='+document.addVehicle.regNo.value;  
            
                if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest = new XMLHttpRequest();
                        }
                        httpRequest.open("POST", url, true);
                        httpRequest.onreadystatechange = function() {go1(); } ;
                        httpRequest.send(null);
                }    
                }
                    

      function go1() {                         
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var response = httpRequest.responseText;                                      
                   var temp=response.split('-');
                    if(response!=""){                        
                           alert('Vehicle Already Exists');
                           document.getElementById("Status").innerHTML=httpRequest.responseText.valueOf()+" Already Exists";                        
                           document.addVehicle.regNo.focus();
                           document.addVehicle.regNo.select();                           
                           document.addVehicle.regNoCheck.value='exists';                           
                    }else
                    {         
                           document.addVehicle.regNoCheck.value='Notexists';                           
                           document.getElementById("Status").innerHTML="";                        
                    }
                }
            }
        }
        
        
        
        
        
        

        </script>
        
        <SCRIPT>
            var rowCount=1;
            var sno=0;
            var row=0;
            var httpRequest;
            var httpReq;
            var styl = "";
<%   ArrayList tyreItems = (ArrayList) request.getAttribute("tyreItemList"); 
     ArrayList positionList= (ArrayList) request.getAttribute("positionList"); 
     VehicleTO veh = new VehicleTO();
     MrsTO mrs = new MrsTO();          
     %>          
            
            function addRow()
            {
                if(parseInt(rowCount) %2==0)
                {
                    styl="text2";
                }else{
                    styl="text1";
                }                 
                sno++;
                var tab = document.getElementById("addTyres");
                var newrow = tab.insertRow(rowCount);
                
                var cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' > "+sno+"</td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;
                
                
                // TyreIds
                var cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='25' >"+
                "<select name='itemIds' class='form-control' style='width:300px;'   > <option value='0'>-Select-</option>"+
                <% ;
                Iterator itr = tyreItems.iterator();
                while(itr.hasNext()){
                    veh = new VehicleTO();
                    veh = (VehicleTO)itr.next();
                %>        
                "<option value=<%= veh.getItemId() %> >  <%= veh.getItemName() %> </option>"+
                <% } %>

                +"</select> </td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;
                
                
                // Positions
                cell = newrow.insertCell(2);
               var cell0 = "<td class='text1' height='25' >"+
                "<select name='positionIds' style='width:125px;'  class='form-control' > <option value='0'>-Select-</option>"+
                <% ;
                itr = positionList.iterator();
                while(itr.hasNext()){
                    mrs = new MrsTO();
                    mrs = (MrsTO)itr.next();
                %>        
                "<option value=<%= mrs.getPosId() %> >  <%= mrs.getPosName() %> </option>"+
                <% } %>

                +"</select> </td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;            
                
                
                
                cell = newrow.insertCell(3);
                var cell1 =  "<td class='text1' height='30' ><input type='text' name='tyreIds' onChange='checkTyreId("+rowCount+")' class='form-control' >";
                
                cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
                cell.setAttribute("className","text1");
                cell.innerHTML = cell1;

                cell = newrow.insertCell(4);
                var cell1 =  "<td class='text1' height='30' ><input type='text'   id='regno"+row+"' name='regNo' onkeypress='getVehicleNos("+row+")'  class='form-control' >";
                
                cell.setAttribute("className","text1");
                
                cell.innerHTML = cell1;
                row++;
                rowCount++;
            }
            
var httpReq;
var temp = "";
function ajaxData()
{
var url = "/throttle/getModels1.do?mfrId="+document.addVehicle.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}


function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();        
        setOptions(temp,document.addVehicle.modelId);
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}





            function validate()
            {
                var tyreIds = document.getElementsByName("tyreIds");
                var positionIds = document.getElementsByName("positionIds");
                var itemIds = document.getElementsByName("itemIds");
                var tyreExists = document.getElementsByName("tyreExists");
                
                var cntr = 0;
                                
                for(var i=0;i<tyreIds.length;i++){                    
                    for(var j=0;j<tyreIds.length;j++){                        
                            if( (tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value==tyreIds[j].value  ) && (i != j) && (tyreIds[i].value != '')   ){
                                cntr++;  
                            }                        
                    }                    
                    if( parseInt(cntr) > 0){
                        alert("Same Tyre Number should not exists twice");                        
                        return "fail";
                        break;
                    }  
                }                
                
             <%--   for(var i=0;i<positionIds.length;i++){
                    for(var j=0;j<positionIds.length;j++){                        
                            if( (positionIds[i].value == positionIds[j].value) && (positionIds[i].value==positionIds[j].value  ) && (i != j) && (positionIds[i].value!='0'  )  ){
                                cntr++;  
                            }                        
                    }                    
                    if( parseInt(cntr) > 0){
                        alert("Tyre Positions should not be repeated");                        
                        return "fail";
                        break;
                    }  
                }--%>
                
                for(var i=0;i<tyreIds.length;i++){ 
                    
                    if(itemIds[i].value != '0'){    
                        if(positionIds[i].value == '0'){    
                            alert('Please Select Position');
                            positionIds[i].focus();
                            return "fail";
                        }else if(tyreIds[i].value == ''){
                            alert('Please Enter Tyre No');
                            tyreIds[i].focus();
                            return "fail";                    
                        }else if(tyreExists[i].value == 'exists'){
                            alert('Tyre number '+tyreIds[i].value+' is already fitted to another vehicle');
                            tyreIds[i].focus();
                            tyreIds[i].select();
                            return "fail";
                        }
                   }     
                }
                
                return "pass";                
            }
            
            


var httpRequest;
function checkTyreId(val)
{
    val = val-1;
    var tyreNo = document.getElementsByName("tyreIds");
    var tyreExists = document.getElementsByName("tyreExists");    
    if(tyreNo[val].value != ''){
        var url = '/throttle/checkVehicleTyreNo.do?tyreNo='+tyreNo[val].value;
        if (window.ActiveXObject)
        {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
        httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() { processRequest(tyreNo[val].value,val); } ;
        httpRequest.send(null);
        }
}


function processRequest(tyreNo,index)
{


if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{        
    var tyreExists = document.getElementsByName("tyreExists");
    if(httpRequest.responseText.valueOf()!=""){
       var temp=httpRequest.responseText.valueOf();
       var reg=temp.split('-');
       var vehicle=document.getElementById("regno"+index);
       vehicle.value=reg[1];
       tyreExists[index].value='exists';
    }else {
        document.getElementById("userNameStatus").innerHTML="";
        tyreExists[index].value='notExists';
    }
}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}

function getVehicleNos(val){
    //onkeypress='getList(sno,this.id)'
    
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"+val),new ListSuggestions("regno"+val,"/throttle/getAllVehicleNos.do?val="));
} 
            
            
        </script>
        
        
        
        
    </head>
    <!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
    <![endif]-->
    <body onLoad="addRow();setImages(0,0,1,0,0,0);">
        <form name="addVehicle" method="post"  >
<%@ include file="/content/common/path.jsp" %>
    

<%@ include file="/content/common/message.jsp" %>
               
<font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
    <div align="center" id="Status">&nbsp;&nbsp;</div>
</font>

   
            <br>
<div align="center" style="font-family:Arial, Helvetica, sans-serif; color:#f5533d; font-size:12px; font-weight:bold;" id="userNameStatus">&nbsp;&nbsp;</div>

<table  align="center" border="0" cellpadding="0" cellspacing="0" width="650" class="border">
<td class="text1" height="30">Vendor&nbsp;
    <select name="vendorId" class="form-control" style="width:255px">
        <option value='0' > --Select-- </option>
        <c:if test = "${vendorList != null}" >
            <c:forEach items="${vendorList}" var="vend">
                
                    <option value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" />  </option>

            </c:forEach>
        </c:if>
    </select>
</td>
</table>


            <p align="center"><span class="contenthead" ><strong>&nbsp;&nbsp;Fault Tyre Items&nbsp;&nbsp;</strong></span></p>
            <table border="0" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="addTyres">
                <tr >
                    <td width="120" class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>

                    <td width="120" class="contenthead" height="30" ><div class="contenthead">Item</div></td>
                    <td width="116" class="contenthead" height="30" ><div class="contenthead">Position</div> </td>
                    <td width="116" class="contenthead" height="30" ><div class="contenthead">Tyre Number</div></td>
                    <td width="120" class="contenthead" height="30" ><div class="contenthead">Vehicle No</div></td>
                </tr>                                                
            </table>
            <br> <br>            
<br>    
            <center>
                <input type="button"  class="button" value="Add" name="save" onClick="submitPage(this.name)">
                <input type="button" class="button" value="Add Row" name="save" onClick="addRow()">    
            </center>
<br>            
<br>            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>


