<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
        <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
        function submitSearch()
        {
            if(isEmpty(document.externalLabour.fromDate.value))
            {
                alert("Select From Date");
                document.externalLabour.fromDate.focus();
                return false;
            }
            else if(isEmpty(document.externalLabour.toDate.value))
            {
                alert("Select End Date");
                document.externalLabour.toDate.focus();
                return false;
            }
            else
            {
                document.externalLabour.action='/throttle/externalLabourBillGraphData.do';
                document.externalLabour.submit();
            }
        }
        function setValues(){
            if('<%= request.getAttribute("fromDate")%>' != '    null'){
                document.externalLabour.fromDate.value='<%= request.getAttribute("fromDate")%>';
            }
            if('<%= request.getAttribute("toDate")%>' != '    null'){
                document.externalLabour.toDate.value='<%= request.getAttribute("toDate")%>';
            }
            if('<%= request.getAttribute("vendorId")%>' != 'null'){
                document.externalLabour.vendorId.value='<%= request.getAttribute("vendorId")%>';
            }
        }

        Date.fromDDMMYYYY = function (s) {return (/^(\d\d?)\D(\d\d?)\D(\d{4})$/).test(s) ? new Date(RegExp.$3, RegExp.$2 - 1, RegExp.$1) : new Date (s)}

            function StartDateCheck(field){
                var val = field.value;                
                var startDate = Date.fromDDMMYYYY (val);
                var currentDate = new Date();
                currentDate.setHours(0);
                currentDate.setMinutes(0);
                currentDate.setSeconds(0);
                currentDate.setMilliseconds(0);

                if (startDate < currentDate){
                    alert("Start date should current date or feature date!!");
                    field.value="";
                    field.focus();
                }
            }

    </script>
    <body onload="setValues()">
         <%@ include file="/content/common/path.jsp" %>
                <%@ include file="/content/common/message.jsp" %>
        
        <form name="externalLabour" method="post">
          
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Sales Bill Trend</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
    <td><font color="red">*</font>From Date</td>
    <td><input size="16" type="text" readonly name="fromDate" class="form-control"/><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.externalLabour.fromDate,'dd-mm-yyyy',this)"/></td>
    <td><font color="red">*</font>To Date</td>
    <td><input class="form-control" size="16" type="text" readonly name="toDate" onchange="StartDateCheck(this);" /><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.externalLabour.toDate,'dd-mm-yyyy',this)"/></td>
</tr>

<tr>
    <td height="25"><font color="red">*</font>External Labour</td>
    <td height="25"><select class="form-control" name="vendorId" style="width:255px">
        <option value="0" >--Select--</option>
        <c:forEach items="${contractVendor}" var="vendor">
            <option value='<c:out value="${vendor.vendorId}"/>'>
                <c:out value="${vendor.vendorName}"/>
            </option>
        </c:forEach>
    </select></td>
    <td height="25"><input type="button" class="button" name="search" value="search" onclick="submitSearch();"/></td>
    <td height="25">&nbsp;</td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>
                               
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <br/>
        <%
                    if (request.getAttribute("externalLabour") != null) {

                        ArrayList externalLabour = (ArrayList) request.getAttribute("externalLabour");
                        ArrayList colourList = (ArrayList) request.getAttribute("colourList");
                        System.out.println("externalLabour" + externalLabour.size());
                        System.out.println("colourList" + colourList.size());
                        String strXML = "<graph caption='External Labour Bill' xAxisName='Month' yAxisName='Bill Amount' decimalPrecision='0' formatNumberScale='0'>";


                        //Initiate <dataset> elements
                        String strDataProdA = "";

                        //Iterate through the data
                        Iterator itr = externalLabour.iterator();
                        Iterator itrC = colourList.iterator();
                        ReportTO reportTO = new ReportTO();
                        ReportTO reportT = new ReportTO();
                        while (itr.hasNext() && itrC.hasNext()) {
                            reportTO = (ReportTO) itr.next();
                            reportT = (ReportTO) itrC.next();

                            strDataProdA += "<set name='" + reportTO.getMonthName() + "' value='" + reportTO.getReportAmount() + "'  color='" + reportT.getColour() + "' />";
                            System.out.println("Month-"+reportTO.getMonthName()+"-Amount-"+reportTO.getReportAmount()+"-Color"+reportT.getColour());

                        }





                        //Add <set value='...' /> to both the datasets


                        //Assemble the entire XML now
                        strXML += strDataProdA + "</graph>";
                        System.out.println("strXML-->" + strXML);

        %>
        <p>
        <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                        <jsp:param name="chartSWF" value="/throttle/swf/FCF_Column3D.swf" />
                        <jsp:param name="strURL" value="" />
                        <jsp:param name="strXML" value="<%=strXML%>" />
                        <jsp:param name="chartId" value="productSales" />
                        <jsp:param name="chartWidth" value="800" />
                        <jsp:param name="chartHeight" value="450" />
                        <jsp:param name="debugMode" value="false" />
                        <jsp:param name="registerWithJS" value="false" />
                    </jsp:include>
                </td>
            </tr>

        </table>


    </p>
    <%
                }
    %>

</body>
</html>