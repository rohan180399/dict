package ets.domain.section.business;

/**
 *
 * @author vidya
 */
public class SectionTO {

    private String serviceTypeId = "";
    private String serviceTypeName = "";
    private String desc = "";
    private String status = "";
    private String sectionId = "";
    private String sellingPrice = "";
    private String mrp = "";
    private String sectionCode = "";
    private String sectionName = "";
    private String description = "";
    private String activeInd = "";
    private String uomId = "";
    private String uomName = "";
    private String itemCode = "";
    private String paplCode = "";
    private String itemName = "";
    private String maxQuandity = "";
    private String roLevel = "";
    private String uom = "";
    private String scrapUomId = "";
    private String specification = "";
    private String reConditionable = "";
    private String mfrId = "";
    private String modelId = "";
    private String modelName = "";
    private String scrapUomName = "";
    private String mfrName = "";
    private String mfrCode = "";
    private String itemId = "";
    private String subRackId = "";
    private String rackId = "";
    private String subRackName = "";
    private String rackName = "";
    private String startIndex = "";
    private String endIndex = "";
    private String categoryId = "";
    private String categoryName = "";
    private String stockLevel = "";
    //Hari
    private String lastIssuedQty= "";
    private String lastTransferedQty= "";
    private String lastPurchasedQty= "";
    private String price="";
    private String vatId=null;
    private String vat=null;
    private String groupId=null;
    private String groupName=null;
    private String vendorId=null;
    private String vendorName=null;
    private String seatCapacity = null;
    
     private String mfr = null;
     private String model = null;
     private String typeId = null;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
    
    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLastPurchasedQty() {
        return lastPurchasedQty;
    }

    public void setLastPurchasedQty(String lastPurchasedQty) {
        this.lastPurchasedQty = lastPurchasedQty;
    }

    public String getLastTransferedQty() {
        return lastTransferedQty;
    }

    public void setLastTransferedQty(String lastTransferedQty) {
        this.lastTransferedQty = lastTransferedQty;
    }
    public String getLastIssuedQty() {
        return lastIssuedQty;
    }

    public void setLastIssuedQty(String lastIssuedQty) {
        this.lastIssuedQty = lastIssuedQty;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(String endIndex) {
        this.endIndex = endIndex;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }
    
    

    public String getRackName() {
        return rackName;
    }

    public void setRackName(String rackName) {
        this.rackName = rackName;
    }

    public String getSubRackName() {
        return subRackName;
    }

    public void setSubRackName(String subRackName) {
        this.subRackName = subRackName;
    }

    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public String getSubRackId() {
        return subRackId;
    }

    public void setSubRackId(String subRackId) {
        this.subRackId = subRackId;
    }

    public String getScrapUomName() {
        return scrapUomName;
    }

    public void setScrapUomName(String scrapUomName) {
        this.scrapUomName = scrapUomName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getScrapUomId() {
        return scrapUomId;
    }

    public void setScrapUomId(String scrapUomId) {
        this.scrapUomId = scrapUomId;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getReConditionable() {
        return reConditionable;
    }

    public void setReConditionable(String reConditionable) {
        this.reConditionable = reConditionable;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMaxQuandity() {
        return maxQuandity;
    }

    public void setMaxQuandity(String maxQuandity) {
        this.maxQuandity = maxQuandity;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRoLevel() {
        return roLevel;
    }

    public void setRoLevel(String roLevel) {
        this.roLevel = roLevel;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(String stockLevel) {
        this.stockLevel = stockLevel;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(String seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

 

    

}
