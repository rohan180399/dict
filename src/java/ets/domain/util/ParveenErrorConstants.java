/*-----------------------------------------------------------------------------
  * BharathlErrorConstants.java
  * Nov 20, 2008
  *
  * Copyright (c) ETS Systems.
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * ETS Systems ("Confidential Information"). You shall
  * not disclose such Confidential Information and shall use it only in
  * accordance with the terms of the license agreement you entered into
  * with ETS Systems.
  ---------------------------------------------------------------------------*/

 package ets.domain.util;
 /****************************************************************************
  *
  * Modification Log:
  * ---------------------------------------------------------------------------
  * Ver      Date                          Author                       Change
  * --------------------------------------------------------------------------
  * 1.0     Nov 20, 2008                   ETS			            Created
  *
  ****************************************************************************/
  /**
   * <br>BharathlErrorConstants object -This class have all the Event Protal
   *  Error constants.
  */
public class ParveenErrorConstants {
	/**
	 * MESSAGE_KEY-message
	 */
	public static final String MESSAGE_KEY = "successMessage";
	/**
	 * ERROR_KEY-error
	 */
	public static final String ERROR_KEY = "errorMessage";

        /**
	 * PATH_KEY-path
	 */
	public static final String PATH_KEY = "menuPath";

}
