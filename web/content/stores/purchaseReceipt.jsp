<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

        
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"> </script> 


        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript">


            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
                });

            });


        </script>
        
        <script>
            function submitPag(){  
        if(document.mpr.toDate.value==''){
            alert("Please Enter From Date");
        }else if(document.mpr.fromDate.value==''){
            alert("Please Enter to Date");
        }                 
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }            
            
            function setDate(fDate,tDate){
                if(fDate != 'null'){
                document.mpr.fromDate.value=fDate;
                }
                if(tDate != 'null'){
                document.mpr.toDate.value=tDate;
                }
                                
            }
            
        </script>


         <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Purchase Receipts </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active">Purchase Receipts</li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>')">        
        <form name="mpr" method="post" >                    
             
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-close">&times;</a>
                        <a href="" class="minimize">&minus;</a>
                    </div><!-- panel-btns -->
                    <h5 class="panel-title">Search Filters</h5>
                </div>
            <div class="panel-body">
                
            <%@ include file="/content/common/message.jsp" %>  
            <table class="table table-bordered">
                
                <tr>
                    <td ><spring:message code="stores.label.FromDate"  text="default text"/>
</td>
                    <td ><input  name="fromDate" class="form-control pull-right datepicker" type="text" value="" size="20">
<!--                    <img src="/throttle/images/cal.gif" name="Calendar"  readonly  onclick="displayCalendar(document.mpr.fromDate,'dd-mm-yyyy',this)"/></td>-->

                    <td ><spring:message code="stores.label.TODate"  text="default text"/></td>
                    <td ><input name="toDate" class="form-control pull-right datepicker"  type="text" value="" size="20">

                    <td  align="center"> <input class="button"  type="button" name="search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPag();"> </td>
                </tr>
            </table> 
	</div>
                </div>
                <c:if test = "${purchaseReceiptList != null}" >
            <table class="table table-info mb30 table-hover" id="table">
            <thead>
                <tr>
                    <th >SNo</th>
                    <th >Invoice No</th>
                    <th >Vendor</th>
                    <th >PurchaseType</th>
                    <th >SupplyDate</th>
                    <th >details</th>
                </tr>
            </thead>
                <% int index = 1;
            String classText = "";
            int oddEven = 0;
                %>

                    <c:forEach items="${purchaseReceiptList}" var="mpr">
                        
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td><%=index%></td>
                            <td><c:out value="${mpr.inVoiceNumber}"/></td>
                            <td><c:out value="${mpr.vendorName}"/></td>
                            <td><c:out value="${mpr.purchaseType}"/></td>
                            <td><c:out value="${mpr.createdDate}"/><c:out value='${mpr.reqQty}'/></td>
                            <td><a href="/throttle/createBarCodeFile.do?supplyId=<c:out value='${mpr.supplyId}'/>&invoiceNo=<c:out value='${mpr.inVoiceNumber}'/>&poId=<c:out value='${mpr.reqQty}'/>"> BarCode File </a> </td>
                        </tr>
                        <%
                        index++;
                        %>
                        
                    </c:forEach>
                </c:if>                  
            </table>                        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


      <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 1);
        </script>


      </div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>



