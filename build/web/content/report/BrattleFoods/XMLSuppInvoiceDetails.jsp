
<%--
    Document   : DailyCashReport
    Created on : Apr 28, 2016, 6:39:19 PM
    Author     : Gulshan kumar
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css">
    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }
</style>

<script>
    $(document).ready(function() {
        var invoiceType = "2";
        $('#invoiceCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getInvoiceList.do",
                    dataType: "json",
                    data: {
                        invoiceCode: request.term, invoiceType: invoiceType
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#invoiceId').val('');
                            $('#invoiceCode').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#invoiceId').val(tmp[0]);
                $('#invoiceCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>

<script>
    $(document).ready(function() {
        $('.ball, .ball1').removeClass('stop');
        $('.trigger').click(function() {
            $('.ball, .ball1').toggleClass('stop');
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();

        } else {
            document.accountReceivable.action = '/throttle/getSuppInvoiceXMLData.do?param=Search';
            document.accountReceivable.submit();
        }

    }
    function submitPageFTP(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();

        } else {
            document.accountReceivable.action = '/throttle/generateSuppInvoiceXMLData.do?param=Search';
            document.accountReceivable.submit();
        }

    }




</script>
<%
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String fDate = "";
    String tDate = "";
    if (request.getAttribute("fromDate") != null) {
        fDate = (String) request.getAttribute("fromDate");
    } else {
        fDate = dateFormat.format(date);
    }
    if (request.getAttribute("toDate") != null) {
        tDate = (String) request.getAttribute("toDate");
    } else {
        tDate = dateFormat.format(date);
    }

%>

<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("type")%>' != 'null') {
            document.getElementById('type').value = '<%=request.getAttribute("type")%>';
        }
    }

    function clearInvoiceId() {
        var invoiceCode = $("#invoiceCode").val();
        if (invoiceCode == "") {
            $("#invoiceId").val("");
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Supplementry Invoice Details (XML)</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValues();">
           <form name="accountReceivable" action=""  method="post">
                    <%@ include file="/content/common/message.jsp" %>
              
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Invoice Details (XML)</td></tr>

                        <tr>
                            <td height="30">&nbsp;&nbsp;Supp.Invoice No</td>
                            <td><input type="hidden" name="invoiceType" id="invoiceType" value="2"  class="form-control" style="width:240px;height:40px">
                                <input type="hidden" name="invoiceId" id="invoiceId" value="<c:out value="${invoiceId}"/>"  class="form-control" style="width:240px;height:40px">
                                <input type="text" name="invoiceCode" id="invoiceCode" value="<c:out value="${invoiceCode}"/>"  class="form-control" style="width:240px;height:40px" onchange="clearInvoiceId()">
                            </td>

                        </tr>

                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:240px;height:40px;" value="<%=fDate%>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:240px;height:40px;" value="<%=tDate%>"></td>

                        </tr>
                        <tr>

                            <td colspan="4" align="center"><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Download" onclick="clearInvoiceId();
                                                submitPage(this.name);">
                                <input type="button" class="btn btn-info" name="FTP"  id="FTP"  value="FTP" onclick="clearInvoiceId();
                                                  submitPageFTP(this.name);"></td>
                        </tr>
                    </table>
        </div></div>
</td>
</tr>
</table>
<br>
<br>


</form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

