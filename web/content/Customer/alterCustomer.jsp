<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.customer.business.CustomerTO" %>   
</head>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
function submitPage(){
    var checValidate = selectedItemValidation();
}
function setSelectbox(i){
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
} 
function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var custName = document.getElementsByName("custNameList");
var custContactPerson = document.getElementsByName("custContactPersonList");
var custAddress = document.getElementsByName("custAddressList");
var custCity = document.getElementsByName("custCityList");
var custState = document.getElementsByName("custStateList");
var custPhone = document.getElementsByName("custPhoneList");
var custMobile = document.getElementsByName("custMobileList");
var custEmail = document.getElementsByName("custEmailList");
var custStatus = document.getElementsByName("custStatusList");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(custName[i],'Customer name')){       
        return;
   }     
 if(textValidation(custContactPerson[i],'Contact Person')){ 
        return;
   }     
 if(textValidation(custAddress[i],'Customer Address')){ 
        return;
   }     
 if(textValidation(custCity[i],'Customer City')){ 
        return;
   }     
 if(textValidation(custState[i],'Customer State')){ 
        return;
   }     
 if(numberValidation(custPhone[i],'Customer Phone')){ 
        return;
   }     
 if(numberValidation(custMobile[i],'Customer Mobile')){ 
        return;
   } 
    if(textValidation(custEmail[i],'Customer Email')){ 
        return;
   } 
    if(textValidation(custStatus[i],'CustomerStatus')){ 
        return;
   } 
document.manufacturer.action = '/throttle/handleUpdateCustomer.do';
document.manufacturer.submit();
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
custName[0].focus();
}
}
</script>
<body>
<form name="manufacturer"  method="post" >
<%@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" %>
<br>
 <c:if test = "${CustomerList != null}" >
<table width="624" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="70" height="30" class="contentsub"><div class="contentsub">Cus Id</div></td>
<td width="133" height="30" class="contentsub"><div class="contentsub">CustomerName</div></td>
<td width="133" height="30" class="contentsub"><div class="contentsub">CustomerType</div></td>
<td width="133" height="30" class="contentsub"><div class="contentsub">Contactperson</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">Address</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">City</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">State</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">Phone No</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">Mobile No</div></td>
<td width="255" height="30" class="contentsub"><div class="contentsub">Email</div></td>
<td width="66" height="30" class="contentsub"><div class="contentsub">Status</div></td>
<td width="66" height="30" class="contentsub"><div class="contentsub">Select</div></td>
</tr>
<% int index=0; %>
 <c:forEach items="${CustomerList}" var="customer"> 
  <%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td class="<%=classText %>" height="30"><input type="hidden"class="textbox"name="custIdList" value="<c:out value='${customer.custId}'/>"style="width:100px;"><c:out value='${customer.custId}'/> </td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custNameList" value="<c:out value='${customer.custName}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"> </td>
<td class="<%=classText %>" height="30">        
    <select class="textbox" name="custTypeList" style="width:125px;">
    <c:if test="${customer.customerType == '1'}">
            <option value="1" selected>CCL Customer</option>
            <option value="2">CLPL Customer</option>
    </c:if>
    <c:if test="${customer.customerType == '2'}">
            <option value="1">CCL Customer</option>
            <option value="2" selected>CLPL Customer</option>
    </c:if>
    </select>
</td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custContactPersonList" value="<c:out value='${customer.custContactPerson}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custAddressList" value="<c:out value='${customer.custAddress}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custCityList" value="<c:out value='${customer.custCity}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custStateList" value="<c:out value='${customer.custState}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custPhoneList" value="<c:out value='${customer.custPhone}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custMobileList" value="<c:out value='${customer.custMobile}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="custEmailList" value="<c:out value='${customer.custEmail}'/>" style="width:100px;" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><select class="textbox" name="custStatusList" style="width:100px;" onchange="setSelectbox('<%= index %>');">    
 <c:if test="${(customer.custStatus=='n') || (customer.custStatus=='N')}" >
 <option value="Y" >Active</option><option value="N" selected>InActive</option>                           
 </c:if>   
<c:if test="${(customer.custStatus=='y') || (customer.custStatus=='Y')}" >
 <option value="Y" selected>Active</option><option value="N">InActive</option>
</c:if>
</select></td>
<td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
 <%
    index++;
  %>
</c:forEach> 
</table>
<br>
<center>
<input type="button" value="Save" class="button" onClick="submitPage();">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </c:if> 
</body>
</html>
