<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    
    <script>
        function submitPage(){
            var chek=Validation();
            if(chek==1){            
            document.vehicleReport.action='/throttle/mechanicPerformanceDetails.do';
            document.vehicleReport.submit();
            }
        }
        function Validation(){              
            if(textValidation(document.vehicleReport.fromDate,'From Date')){
               return 0;              
                }
            if(textValidation(document.vehicleReport.toDate,'To Date')){
               return 0;              
                }
                                
                return 1;
            }

        function setValues(){

            if('<%=request.getAttribute("fromDate")%>'!='null'){
            document.vehicleReport.fromDate.value ='<%=request.getAttribute("fromDate")%>';
            document.vehicleReport.toDate.value ='<%=request.getAttribute("toDate")%>';
           
            }
            if('<%=request.getAttribute("technicianId")%>' != 'null'){
              document.vehicleReport.technicianId.value='<%=request.getAttribute("technicianId")%>';
              }
            if('<%=request.getAttribute("spId")%>' != 'null'){
              document.vehicleReport.spId.value='<%=request.getAttribute("spId")%>';
              }

            }
            
            
    
    function printPage()
    {       
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            

     function newWindow(techId,fromDate,toDate ){
        //alert('/throttle/mechPerformanceData.do?technicianId='+techId+"&fromDate="+fromDate+"&toDate="+toDate);
        window.open('/throttle/mechPerformanceData.do?technicianId='+techId+"&fromDate="+fromDate+"&toDate="+toDate, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }            
            
    </script>

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Mechanic Performance </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Reports</a></li>
          <li class="active">Mechanic Performance</li>
        </ol>
      </div>
      </div>

<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onload="setValues();">
    <form name="vehicleReport">
    
        <%@ include file="/content/common/message.jsp" %>
        
        
           <table class="table table-bordered">
               <thead>
          <tr>
              <th height="30"  colspan="8" >Mechanic Performance </th>
            </tr>
</thead>
            <tr>
                 <td class="text2" align="left" height="30">&nbsp;&nbsp;Technician</td>
                 <td class="text2"  ><select class="textbox" name="technicianId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${TechnicianList != null}" >
                            <c:forEach items="${TechnicianList}" var="tec"> 
                                <option value='<c:out value="${tec.technicianId}" />'><c:out value="${tec.technician}" /></option>
                            </c:forEach >
                        </c:if>  	
                </select>
                </td>

                <td class="text2" align="left" height="30">&nbsp;&nbsp;Location</td>
                 <td class="text2"  ><select class="textbox" name="spId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${servicePointList != null}" >
                            <c:forEach items="${servicePointList}" var="sp">
                                <option value='<c:out value="${sp.spId}" />'><c:out value="${sp.spName}" /></option>
                            </c:forEach >
                        </c:if>
                </select>
                </td>
                
               <td class="text2" align="left" height="30"><font color="red">*</font>From Date</td>               
                <td  class="text2" height="30"><input name="fromDate" type="text" class="textbox"  size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.fromDate,'dd-mm-yyyy',this)"/></td>
                 <td class="text2" align="left" height="30"><font color="red">*</font>To Date </td> 
                <td  class="text2" height="30"><input name="toDate" type="text" class="textbox"  size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.toDate,'dd-mm-yyyy',this)"/></td>
            </tr>
      
        </table>
        <center>   
        <br>
             <input type="button" class="button" value="Search" onclick="submitPage();">   
            </center>
        <br>
        <br>
        <c:if test = "${mechPerformanceList != null}" >
            <center> <input type="button" class="button" name="print" value="print" onClick="printPage();" > </center>
        <div id="printPage" >
    <table class="table table-bordered">
        <thead>
                <tr>
                    
                    <th  height="30">Technician</th>
                    <th  height="30">Location</th>
                    <th  height="30">Estimated Hrs</th>
                    <th  height="30">Actual Hrs</th>
                    <th  height="30">Productive Hours Available</th>
                    <th  height="30">Productivity</th>
                    <th  height="30">&nbsp;</th>
                </tr>
                </thead>
                <% int index = 0;%> 
                <c:forEach items="${mechPerformanceList}" var="mech"> 
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
                    

                        <td class="<%=classText %>" height="30" ><c:out value="${mech.technician}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.companyName}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${mech.estHrs}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.actHrs}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${noOfDays}" /></td>                        
                        <td class="<%=classText %>" height="30">
                            
                            <fmt:formatNumber type="percent" maxFractionDigits="2" value="${mech.actHrs/noOfDays}" />
                        </td>                        
                        <td class="<%=classText %>" height="30">
                            <a href=# onclick=newWindow('<c:out value="${mech.technicianId}" />','<c:out value="${fromDate}" />','<c:out value="${toDate}" />')>details</a>
                        </td>                        

                    </tr>
                    <% index++;%>
                </c:forEach>
                
            </table>
     
            </div>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
 </div>


        </div>
        </div>



<%@ include file="/content/common/NewDesign/settings.jsp" %>

