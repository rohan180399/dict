
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<style type="text/css">
    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }
</style>
<script>
    $(document).ready(function() {
        $('.ball, .ball1').removeClass('stop');
        $('.trigger').click(function() {
            $('.ball, .ball1').toggleClass('stop');
        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    //auto com

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getRegistrationNo.do",
                    dataType: "json",
                    data: {
                        regno: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                $('#vehicleNo').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>



<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
//                } else if (document.getElementById('grNo').value == '') {
//                    alert("Please select GR NO");
//                    document.getElementById('grNo').focus();
//                } else if (document.getElementById('consignorNo').value == '') {
//                    alert("Please select consignor No");
//                    document.getElementById('consignorNo').focus();
//                } else if (document.getElementById('vehicleNo').value == '') {
//                    alert("Please select vehicleNo");
//                    document.getElementById('vehicleNo').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/viewTripVehicleReport.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/viewTripVehicleReport.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Vehicle Trips Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="getVehicleNos();">
                <!--<body>-->
                <form name="accountReceivable" action=""  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table style="width:100%" align="center" border="0" class="tabouterborder" >
<!--                        <tr height="30"   >
                            <td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">GR Summary</td></tr>
                         <tr height="40">
                            <td class="tabtext"><font color="red"></font>GR No</td>
                            <td  class="tabtext" height="30"><input name="grNo" id="grNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${grNo}"/>" ></td>
                            <td class="tabtext"><font color="red"></font>Container No</td>
                            <td  class="tabtext" height="30"><input name="containerNo" id="containerNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${containerNo}"/>"></td>
                        </tr>
                         <tr height="40">
                            <td  class="tabtext"><font color="red"></font>Vehicle No</td>
                            <td  class="tabtext" height="30"><input name="vehicleNo" id="vehicleNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${vehicleNo}"/>" ></td>
                            <td  class="tabtext"><font color="red"></font>Movement Type</td>
                            <td  class="tabtext" height="30">
                                <select name="movementType" id="movementType" type="text" class="textbox" style="width:180px;height:25px;" >
                                    <option value=''>--select--</option>     
                                    <option value='Import'>Import</option>     
                                    <option value='Export'>Export</option>     
                                    <option value='Repo'>Repo</option>     
                                    <option value='Import DSO'>Import DSO</option>     
                                    <option value='Export DSO'>Export DSO</option>     
                                </select>
                                <script>
                                                document.getElementById("movementType").value = '<c:out value="${movementType}"/>'
                                </script>
                                <input name="movementType" id="movementType" type="text" class="text" value="<c:out value="${movementType}"/>"></td>
                        </tr>-->
                         <tr height="40">
                            <td  class="tabtext" ><font color="red">*</font>From Date</td>
                            <td  class="tabtext" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:25px;color:black" value="<c:out value="${fromDate}"/>" ></td>
                            <td class="tabtext"><font color="red">*</font>To Date</td>
                            <td  class="tabtext" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:25px;color:black" value="<c:out value="${toDate}"/>"></td>
<!--                        </tr>
                        <tr height="40">-->
                       

                        <td colspan="2" align="center"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;">&nbsp;&nbsp;
                            <input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></td>
                        </tr>

                    </table>
                        <br>
                        <br>
                    


                        <!--<table border="1" id="table" align="center" width="100%" cellpadding="0" cellspacing="1" >-->
                        <table  id="table" class="table table-info mb30" style="width:100%;" >
                            <thead height="30">
                                <tr id="tableDesingTH" height="30">
                                    <th rowspan="2" >S.No</th>
                                     <th rowspan="2" >Vehicle Reg. No.</th>
                                     <th rowspan="2" >No. Of. Trips</th>
                                     <th rowspan="2" >OwnerShip</th>
                                   
                                    
                                </tr> 

                            </thead>
                            <% int index = 0,sno = 1;%>
                         
                            <c:forEach items="${getVehicleTripDetails}" var="csList">
                                <tr>
                                   <td align="center"><%=sno%></td>
                           
                            <td align="left"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="left"><c:out value="${csList.noOfTrips}"/></td>
                           
                              <c:if test = "${csList.ownType == 1}" >
                                  <td>Own</td>
                              </c:if>
                           
                              <c:if test = "${csList.ownType == 2}" >
                                  <td>Hire</td>
                              </c:if>

                                </tr>
                                <%
                           index++;
                             sno++;
                                %>

                            </c:forEach>

                        </table>
                  

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script>
                        function getVehicleNos() {
                            //onkeypress='getList(sno,this.id)'
                            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                        }
                    </script>          

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
