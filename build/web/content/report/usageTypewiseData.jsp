<%-- 
    Document   : usageTypewiseData
    Created on : Sep 25, 2010, 3:38:32 PM
    Author     : Admin
--%>
<%--This is the chart processor code which is used to customise the chart genearted by ceowlf tags below --%>


<%--From here its Displaying Fluidity Reports-- --%>

     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
          <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
     </head>


     <body onload ="setValues();">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<form name="typewise" method="post" >
<table width='500' align="center" bgcolor='white' border='0' cellpadding='0' cellspacing='0' class='repcontain'>
<tr>
    <td class="text1" > From Date : </td>
    <td class="text1" >
       <input type="text" class="form-control" readonly name="fromDate" value="" >
       <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.typewise.fromDate,'dd-mm-yyyy',this)"/>
    </td>

    <td class="text2" > To Date : </td>
    <td class="text2" >
        <input type="text" class="form-control" readonly name="toDate" value="" >
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.typewise.toDate,'dd-mm-yyyy',this)"/>
    </td>
</tr>
<tr> <td colspan="4" align="center">&nbsp;</td> </tr>
<tr>
    <td colspan="4" align="center">
        <input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" > </td>
</tr>

</table>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            <%
if(request.getAttribute("usageTypewiseSummary") != null){

ArrayList usageSummary = (ArrayList) request.getAttribute("usageTypewiseSummary");
System.out.println("usageSummary"+usageSummary.size());

int siz = usageSummary.size();
String[][] arrData = new String[siz][5];

int i1 = 0;

Iterator itr = usageSummary.iterator();
ReportTO reportTO = new ReportTO();
while(itr.hasNext()){
reportTO = (ReportTO) itr.next();
arrData[i1][0] = reportTO.getCompanyName();
arrData[i1][2] = reportTO.getSpareAmount();
arrData[i1][3] = reportTO.getLabour();
//String nov=new Integer(reportTO.getNoOfVehicles()).toString();
//nov=reportTO.getNoOfVehicles();
arrData[i1][1] = new Integer(reportTO.getNoOfVehicles()).toString();;
arrData[i1][4] = reportTO.getTotalAmount();
System.out.println("arrData----"+arrData[i1][0]+"-"+arrData[i1][1]+"-"+arrData[i1][2]+"-"+arrData[i1][3]+"-"+arrData[i1][4]);
i1++;
}



String  strXML = "<graph caption='Management Report' XAxisName='CompanyName' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";

//Initialize <categories> element - necessary to generate a stacked chart
String strCategories = "<categories>";

//Initiate <dataset> elements
String strDataProdA = "<dataset seriesName='NoOfVehicles' color='AFD8F8'>";
String strDataProdB = "<dataset seriesName='SpareAmount' color='FF0000'>";
String strDataProdC = "<dataset seriesName='LaborAmount' color='F6BD0F'>";
String strDataProdD = "<dataset seriesName='TotalAmount' color='8BBA00'>";

//Iterate through the data
for(int i=0;i<arrData.length;i++){
//Append <category name='...' /> to strCategories
strCategories += "<category name='" + arrData[i][0] + "' />";
//Add <set value='...' /> to both the datasets
strDataProdA += "<set value='" + arrData[i][1] + "' />";
strDataProdB += "<set value='" + arrData[i][2] + "' />";
strDataProdC += "<set value='" + arrData[i][3] + "' />";
strDataProdD += "<set value='" + arrData[i][4] + "' />";
}

//Close <categories> element
strCategories += "</categories>";

//Close <dataset> elements
strDataProdA += "</dataset>";
strDataProdB +="</dataset>";
strDataProdC +="</dataset>";
strDataProdD +="</dataset>";

//Assemble the entire XML now
strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";

%>
<table width='70%' align="center" bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>



    <c:if test = "${usageTypewiseSummary != null}" >
        <tr>
            <td class="contentsub">S.No</td>
            <td class="contentsub">Usage Types</td>
            <td class="contentsub">Spares Amount</td>
            <td class="contentsub">Labour Amount</td>
            <td class="contentsub">No.Of.Vehicles</td>
            <td class="contentsub">No.Of.Cards</td>
            <td class="contentsub">Total Amount</td>
        </tr>
        <%
					int index = 1 ;
                    %>
    <c:forEach items="${usageTypewiseSummary}" var="usageTypewiseSummary">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.companyName}"/></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.spareAmount}"/></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.labour}"/></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.noOfVehicles}"/></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.noOfCards}"/></td>
        <td class="<%=classText %>"><c:out value="${usageTypewiseSummary.totalAmount}"/></td>






</tr>
<%
            index++;
                        %>

   </c:forEach>
</c:if>

</table>

<p>
            <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_StackedColumn2D.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML %>" />
                <jsp:param name="chartId" value="usageTypewiseReport" />
                <jsp:param name="chartWidth" value="800" />
                <jsp:param name="chartHeight" value="450" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                </td>
            </tr>

        </table>


    </p>
<%
}
%>
</body>

<script>
    function searchSubmit()
    {
        document.typewise.action="/throttle/usageTypewisereport.do"
        document.typewise.submit();
    }

function setValues(){
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.typewise.fromDate.value='<%= request.getAttribute("fromDate") %>';
       // alert(document.typewise.fromDate.value);
    }
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.typewise.toDate.value='<%= request.getAttribute("toDate") %>';
       // alert(document.typewise.toDate.value);
    }
}

</script>





