/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.accident.web;

import ets.domain.accident.business.AccidentBP;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.business.PaginationHelper;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.company.business.CompanyTO;
import ets.domain.company.business.CompanyBP;    
import ets.domain.operation.business.OperationBP;
import ets.domain.users.business.LoginBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.mrs.business.MrsBP;    
import ets.domain.operation.business.OperationTO;
import ets.domain.operation.web.OperationCommand;
import ets.domain.accident.web.AccidentCommand;
import ets.domain.accident.business.AccidentBP;
import ets.domain.accident.business.AccidentTO;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.business.TripBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.util.FPLogUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import ets.domain.vendor.business.VendorTO;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

public class AccidentController extends BaseController {

    /**
     * Creates a new instance of __NAME__
     */
    public AccidentController() {
    }
    AccidentCommand accidentCommand;
    AccidentBP accidentBP;
    LoginBP loginBP;
    TripBP tripBP;
    

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public AccidentBP getAccidentBP() {
        return accidentBP;
    }

    public void setAccidentBP(AccidentBP accidentBP) {
        this.accidentBP = accidentBP;
    }

   public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);


    }

    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
   
  
   //jp Accident Details
    public ModelAndView handleVehicleAccidentDetailsPage(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
             if (request.getSession().isNew()) {
                 return new ModelAndView("content/common/login.jsp");
             }
             String path = "";
             ArrayList vehicleAccidentList = new ArrayList();
             ArrayList bankLedgerList = new ArrayList();
             ArrayList bankReceiptList = new ArrayList();
             HttpSession session = request.getSession();
             accidentCommand = command;
             TripTO tripTO = new TripTO();
             AccidentTO accidentTO = null;
             String menuPath = "";
             String pageTitle = "View Accident Details";
             menuPath = "Vehicle  >> Accident Details";
             request.setAttribute("pageTitle", pageTitle);
             request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     
             String buttonClicked = "";
             if (request.getParameter("button") != null && request.getParameter("button") != "") {
                 buttonClicked = request.getParameter("button");
             }
             try {
                 ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                 path = "content/Accident/viewVehicleAccident.jsp";
                 String accidentId = request.getParameter("accident_Id");
                 accidentTO = new AccidentTO();
                 //                if (accident_Id != null && accident_Id != "") {
                 //                    accidentTO.setAccident_Id(accident_Id);
                 //                }
                 ArrayList VehicleAccident = new ArrayList();
                 VehicleAccident = accidentBP.getVehicleAccident();
                 System.out.println("VehicleAccident = " + VehicleAccident.size());
                 request.setAttribute("VehicleAccident", VehicleAccident);
     
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(accidentId);
                 System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
                 ArrayList vehicleList = new ArrayList();
                 vehicleList = tripBP.getVehicleList();
                 request.setAttribute("vehicleList", vehicleList);
//                 
//                 ArrayList vehicleRegNos = new ArrayList();
//            vehicleRegNos = accidentBP.getVehicleRegNos();
//            request.setAttribute("vehicleRegNos", vehicleRegNos);
     
             } catch (FPRuntimeException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             } catch (Exception exception) {
                 exception.printStackTrace();
                 FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             }
             return new ModelAndView(path);
         }
     
         public ModelAndView handleviewVehicleAccidentPage(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
             if (request.getSession().isNew()) {
                 return new ModelAndView("content/common/login.jsp");
             }
             String path = "";
             ArrayList vehicleAccidentList = new ArrayList();
             ArrayList VehicleAccidentDetailUnique = new ArrayList();
             ArrayList bankReceiptList = new ArrayList();
             HttpSession session = request.getSession();
             accidentCommand = command;
             String menuPath = "";
             AccidentTO accidentTO = null;
             String pageTitle = "View Accident Details";
             menuPath = "Vehicle  >> Accident Details";
             request.setAttribute("pageTitle", pageTitle);
             request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     
             String buttonClicked = "";
             if (request.getParameter("button") != null && request.getParameter("button") != "") {
                 buttonClicked = request.getParameter("button");
             }
             String accidentId = request.getParameter("accident_Id");
             System.out.println("accidentId" + accidentId);
             request.setAttribute("accidentId", accidentId);
             accidentTO = new AccidentTO();
             //                if (accident_Id != null && accident_Id != "") {
             //                    accidentTO.setAccident_Id(accident_Id);
             //                }
     
             try {
                 ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                 path = "content/Accident/vehicleAccidentPage.jsp";
                 
                 ArrayList VehicleAccident = new ArrayList();
                 VehicleAccident = accidentBP.getVehicleAccident();
                 System.out.println("VehicleAccident = " + VehicleAccident.size());
                 request.setAttribute("VehicleAccident", VehicleAccident);
//               
                 
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(accidentId);
                 System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
     
             } catch (FPRuntimeException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             } catch (Exception exception) {
                 exception.printStackTrace();
                 FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             }
             return new ModelAndView(path);
         }
     
         public ModelAndView handleinsertAccidentVehicle(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
     
             if (request.getSession().isNew()) {
                 return new ModelAndView("content/common/login.jsp");
             }
             accidentCommand = command;
             HttpSession session = request.getSession();
             String path = "";
     
             int userId = (Integer) session.getAttribute("userId");
             int companyType = (Integer) session.getAttribute("companyTypeId");
             String companyId = (String) session.getAttribute("companyId");
             int madhavaramSp = 1011;
             ModelAndView mv = new ModelAndView();
     
             path = "content/Accident/vehicleAccidentPage.jsp";
             String menuPath = "Vehicle  >>  ADD  ";
             request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
             AccidentTO accidentTO = new AccidentTO();
             CompanyTO companyTO = new CompanyTO();
             //multipart
     
             String newFileName = "", actualFilePath = "";
             String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
             boolean isMultipart = false;
             Part partObj = null;
             FilePart fPart = null;
             int i = 0;
             int j = 0;
             int m = 0;
             int n = 0;
             int p = 0;
             int s = 0;
             int imageId = 0;
             String tripSheetId1 = "";
             String userId1 = "";
             String acc_VehicleNo1 = "";
             String acc_VehicleDetail1 = "";
             String acc_DriverName1 = "";
             // String settlementType = request.getParameter("settlementType");
             String acc_DriverLicNo1 = "";
             String acc_InsurerName1 = "";
             String acc_PolicyNo1 = "";
             String acc_ContactDetail1 = "";
             String acc_ContactPerson1 = "";
             String casualty_Name1 = "";
             String casualty_Address1 = "";
             String casualty_ConDetail1 = "";
             String casualty_Type1 = "";
     
             String casualty_AlterNo1 = "";
             String casualty_ClaimDetail1 = "";
             String acc_ReferenceNo1 = "";
             String acc_SurveyorNo1 = "";
             String acc_FirNo1 = "";
             String witeness_Name1 = "";
             String witeness_Address1 = "";
             String witeness_Contact1 = "";
             String rto_Fine1 = "";
             String rto_StationRelease1 = "";
             String rto_Date1 = "";
             String workshop_Type1 = "";
             String ins_Tie_up1 = "";
             String adhoc_Type1 = "";
     
             //Add vehicle end
             String surv_InspectionDate1 = "";
             String surv_QuotationAmt1 = "";
             String surv_InsurerName1 = "";
             String surv_PolicyNo1 = "";
             String reim_Date1 = "";
             String reim_Amt1 = "";
             String reim_VehicleExcpen1 = "";
             String reim_BillAmt1 = "";
             String lawyerName1 = "";
             String law_ContactNo1 = "";
             String law_court_Address1 = "";
             String law_Fees1 = "";
             String miscels_Fees1 = "";
             String ins_Insurer1 = "";
             String ins_Owner1 = "";
             String adhoc_BillAmt1 = "";
             String adhoc_PaidAmt1 = "";
             String acc_SurveyorName1 = "";
             String ploice_Address1 = "";
             String spotAddress1 = "";
             String accidentDate1 = "";
             String accFirDate1 = "";
             String perOfDisability1 = "";
     //              String altVehicleId= "";
     
     
             //        String[] podRemarks1 = new String[10];
             String[] fileSaved = new String[10];
             //        String[] lrNumber1 = new String[10];
             String[] uploadedFileName = new String[10];
             String[] tempFilePath = new String[10];
             String[] vehicleDetails1 = new String[10];
             String[] uploadedFile = new String[10];
             String[] accidentUploadId = new String[10];
     
             String vehicleId1 = request.getParameter("vehicleId");
             String altVehicleId = "";
     
     
     
     
             try {
                 request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     
     
     
                 isMultipart = org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload.isMultipartContent(request);
                 if (isMultipart) {
                     System.out.println("this is tht");
                     MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                     while ((partObj = parser.readNextPart()) != null) {
                         System.out.println("part Name:" + partObj.getName());
                         if (partObj.isFile()) {
                             actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                             System.out.println("Server Path == " + actualServerFilePath);
                             tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                             System.out.println("Server Path After Replace== " + tempServerFilePath);
                             Date now = new Date();
                             String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                             String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                             fPart = (FilePart) partObj;
                             uploadedFileName[j] = fPart.getFileName();
                             System.out.println("fPart.getFileName() = " + fPart.getFileName());
     
                             if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                                 System.out.println("partObj.getName() = " + partObj.getName());
                                 String[] splitFileName = uploadedFileName[j].split("\\.");
                                 fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                 System.out.println("fileSavedAs = " + fileSavedAs);
                                 fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                                 fileName = fileSavedAs;
                                 tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
                                 actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                                 System.out.println("tempPath..." + tempFilePath);
                                 System.out.println("actPath..." + actualFilePath);
                                 long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                                 System.out.println("fileSize..." + fileSize);
                                 File f1 = new File(actualFilePath);
                                 System.out.println("check " + f1.isFile());
                                 f1.renameTo(new File(tempFilePath[j]));
                                 System.out.println("tempPath = " + tempFilePath);
                                 System.out.println("actPath = " + actualFilePath);
                                 //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                                 //                            String part1 = parts.replace("\\", "");
                             }
     
                             System.out.println("fileName..." + fileName);
                             if (tempFilePath[j] != null && !"".equals(tempFilePath[j])) {
                                 j++;
                             }
                         } else if (partObj.isParam()) {
                             if (partObj.getName().equals("tripSheetId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 tripSheetId1 = paramPart.getStringValue();
                             }
     
     
                             if (partObj.getName().equals("vehicleDetails")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 uploadedFile[s] = paramPart.getStringValue();
                                 System.out.println("uploadedFile[j] = " + uploadedFile[s]);
                                 s++;
                             }
     
                             if (partObj.getName().equals("accidentUploadId")) {
                                 String insuImageIdTemp = "";
                                 ParamPart paramPart = (ParamPart) partObj;
                                 insuImageIdTemp = paramPart.getStringValue();
                                 System.out.println("insuImageIdTemp = " + insuImageIdTemp);
                                 accidentUploadId[imageId] = insuImageIdTemp;
                                 imageId++;
                             }
                             if (partObj.getName().equals("userId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 userId1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_VehicleNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_VehicleNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_VehicleDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_VehicleDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_DriverName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_DriverName1 = paramPart.getStringValue();
                             }
     
                             if (partObj.getName().equals("acc_DriverLicNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_DriverLicNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_InsurerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_InsurerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_PolicyNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_PolicyNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ContactDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ContactDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ContactPerson")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ContactPerson1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Name")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Name1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_ConDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_ConDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Type1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_AlterNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_AlterNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_ClaimDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_ClaimDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ReferenceNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ReferenceNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_SurveyorNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_SurveyorNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_FirNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_FirNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Name")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Name1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Contact")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Contact1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_Fine")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_Fine1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_StationRelease")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_StationRelease1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_Date")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_Date1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("workshop_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 workshop_Type1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ins_Tie_up")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Tie_up1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_Type1 = paramPart.getStringValue();
                             }
                             //add vehicle end
                             if (partObj.getName().equals("surv_InspectionDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_InspectionDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_QuotationAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_QuotationAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_InsurerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_InsurerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_PolicyNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_PolicyNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_Date")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_Date1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_Amt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_Amt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_VehicleExcpen")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_VehicleExcpen1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_BillAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_BillAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("lawyerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 lawyerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_ContactNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_ContactNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_court_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_court_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_Fees")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_Fees1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("miscels_Fees")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 miscels_Fees1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("vehicleId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 vehicleId1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accident_Id")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 altVehicleId = paramPart.getStringValue();
                                 System.out.println("accident_Id = " + altVehicleId);
                             }
                             if (partObj.getName().equals("ins_Insurer")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Insurer1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ins_Owner")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Owner1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_BillAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_BillAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_PaidAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_PaidAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ploice_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ploice_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_SurveyorName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_SurveyorName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("spotAddress")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 spotAddress1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accidentDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 accidentDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accFirDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 accFirDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("perOfDisability")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 perOfDisability1 = paramPart.getStringValue();
                             }
     
     
                         }
                     }
                 }
                 int index = j;
                 String[] file = new String[index];
                 System.out.println("filekoik" + file);
                 String remarks = "";
     
                 String[] saveFile = new String[index];
                 ;
     
                 accidentTO = new AccidentTO();
     
                 int status = 0;
     
                 //file upload
     
     
                 accidentTO.setUsageId(userId1);
                 System.out.println("userId" + accidentTO.getUsageId());
                 accidentTO.setAcc_VehicleNo(acc_VehicleNo1);
                 System.out.println("acc_VehicleNo" + accidentTO.getAcc_VehicleNo());
                 accidentTO.setAcc_VehicleDetail(acc_VehicleDetail1);
                 System.out.println("acc_VehicleDetail" + accidentTO.getAcc_VehicleDetail());
                 accidentTO.setAcc_DriverName(acc_DriverName1);
                 System.out.println("acc_DriverName" + accidentTO.getAcc_DriverName());
                 accidentTO.setAcc_DriverLicNo(acc_DriverLicNo1);
                 accidentTO.setAcc_InsurerName(acc_InsurerName1);
                 accidentTO.setAcc_PolicyNo(acc_PolicyNo1);
                 accidentTO.setAcc_ContactDetail(acc_ContactDetail1);
                 accidentTO.setAcc_ContactPerson(acc_ContactPerson1);
                 accidentTO.setCasualty_Name(casualty_Name1);
                 accidentTO.setCasualty_Address(casualty_Address1);
                 accidentTO.setCasualty_ConDetail(casualty_ConDetail1);
                 accidentTO.setCasualty_Type(casualty_Type1);
                 accidentTO.setCasualty_AlterNo(casualty_AlterNo1);
                 accidentTO.setCasualty_ClaimDetail(casualty_ClaimDetail1);
                 accidentTO.setAcc_ReferenceNo(acc_ReferenceNo1);
                 accidentTO.setAcc_SurveyorNo(acc_SurveyorNo1);
                 accidentTO.setAcc_FirNo(acc_FirNo1);
                 accidentTO.setWiteness_Name(witeness_Name1);
                 accidentTO.setWiteness_Address(witeness_Address1);
                 accidentTO.setWiteness_Contact(witeness_Contact1);
                 accidentTO.setRto_Fine(rto_Fine1);
                 accidentTO.setRto_StationRelease(rto_StationRelease1);
                 accidentTO.setRto_Date(rto_Date1);
                 accidentTO.setWorkshop_Type(workshop_Type1);
                 accidentTO.setIns_Tie_up(ins_Tie_up1);
                 accidentTO.setAdhoc_Type(adhoc_Type1);
                 accidentTO.setSurv_InspectionDate(surv_InspectionDate1);
     
     
                 accidentTO.setSurv_QuotationAmt(surv_QuotationAmt1);
                 accidentTO.setSurv_InsurerName(surv_InsurerName1);
                 accidentTO.setSurv_PolicyNo(surv_PolicyNo1);
                 accidentTO.setReim_Date(reim_Date1);
                 accidentTO.setReim_Amt(reim_Amt1);
                 accidentTO.setReim_VehicleExcpen(reim_VehicleExcpen1);
                 accidentTO.setReim_BillAmt(reim_BillAmt1);
                 accidentTO.setLawyerName(lawyerName1);
                 accidentTO.setLaw_ContactNo(law_ContactNo1);
                 accidentTO.setLaw_court_Address(law_court_Address1);
                 accidentTO.setLaw_Fees(law_Fees1);
                 accidentTO.setMiscels_Fees(miscels_Fees1);
                 accidentTO.setIns_Insurer(ins_Insurer1);
                 accidentTO.setIns_Owner(ins_Owner1);
                 accidentTO.setAdhoc_BillAmt(adhoc_BillAmt1);
                 accidentTO.setAdhoc_PaidAmt(adhoc_PaidAmt1);
                 accidentTO.setAcc_SurveyorName(acc_SurveyorName1);
                 accidentTO.setPloice_Address(ploice_Address1);
                 accidentTO.setSpotAddress(spotAddress1);
                 accidentTO.setAccidentDate(accidentDate1);
                 accidentTO.setAccFirDate(accFirDate1);
                 accidentTO.setPerOfDisability(perOfDisability1);
     
     
                 accidentTO.setVehicleId(vehicleId1);
     
                 accidentTO.setAccident_Id(altVehicleId);
     
     
                 //accidentTO.setVehicleDetails(vehicleDetails1);
     
                 System.out.println("j = " + j);
                 System.out.println("file = " + file.length);
                 System.out.println("tempFilePath[0] = " + tempFilePath[0]);
                 System.out.println("tempFilePath[1] = " + tempFilePath[1]);
     
     
                 for (int x = 0; x < j; x++) {
                     System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                     file[x] = tempFilePath[x];
                     System.out.println("saveFile[x] = " + fileSaved[x]);
                     saveFile[x] = fileSaved[x];
     
     
                 }
                 //end maultipart
     
     
                 // String insId=request.getParameter("insId");
     
                 String pageTitle = "View Vehicles";
                 String accidentId = request.getParameter("accident_Id");
     
     
//                 ArrayList InsComanyNameList = new ArrayList();
//                 InsComanyNameList = accidentBP.processInsCompanyList();
//                 request.setAttribute("InsComanyNameList", InsComanyNameList);
     
                 ArrayList vehicleAccidentList = new ArrayList();
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(accidentId);
                 System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
     
     
     
                 request.setAttribute("pageTitle", pageTitle);
                 int Accident_Id = 0;
     
                 Accident_Id = accidentBP.insertVehicleAccidentDetails(accidentTO, userId, madhavaramSp, file, tripSheetId1, saveFile, uploadedFile, accidentUploadId, altVehicleId);
     
                 mv = handleviewVehicleAccidentPage(request, response, command);
//                 ArrayList vehicleRegNos = new ArrayList();
//                 vehicleRegNos = accidentBP.getVehicleRegNos();
//                 request.setAttribute("vehicleRegNos", vehicleRegNos);
                 path = "content/Accident/viewVehicleAccident.jsp";
                 request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Added Successfully");
     
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(accidentId);
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
     
             } catch (FPRuntimeException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             } catch (FPBusinessException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
                 request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                         exception.getErrorMessage());
             } catch (Exception exception) {
                 exception.printStackTrace();
                 FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             }
             return mv;
         }
     
         public ModelAndView handleEditAccidentVehicle(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
     
             if (request.getSession().isNew()) {
                 return new ModelAndView("content/common/login.jsp");
             }
             String path = "";
             ArrayList vehicleAccidentList = new ArrayList();
             ArrayList bankReceiptList = new ArrayList();
             HttpSession session = request.getSession();
             accidentCommand = command;
             String menuPath = "";
             AccidentTO accidentTO = null;
             String pageTitle = "View Accident Details";
             menuPath = "Vehicle  >> Accident Details";
             request.setAttribute("pageTitle", pageTitle);
             request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
             String altVehicleId = "";
             String buttonClicked = "";
             if (request.getParameter("button") != null && request.getParameter("button") != "") {
                 buttonClicked = request.getParameter("button");
             }
             String accident_Id = request.getParameter("accident_Id");
             System.out.println("accident_Id" + accident_Id);
             
             accidentTO = new AccidentTO();
             if (accident_Id != null && accident_Id != "") {
                 accidentTO.setAccident_Id(accident_Id);
             }
     
             try {
                 ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                 path = "content/Accident/EditVehicleAccident.jsp";
     
                 request.setAttribute("accident_Id", accident_Id);
     
                 ArrayList VehicleAccidentDetailUnique = new ArrayList();
                 VehicleAccidentDetailUnique = accidentBP.processVehicleAccidentDetail(accidentTO);
                 System.out.println("VehicleAccidentDetailUnique=" + VehicleAccidentDetailUnique.size());
                 request.setAttribute("VehicleAccidentDetailUnique", VehicleAccidentDetailUnique);
     
                 ArrayList AccidentUploadDetails = new ArrayList();
                 AccidentUploadDetails = accidentBP.getVehicleAccidentImageDetails(Integer.parseInt(accident_Id));
                 System.out.println("AccidentUploadDetails=" + AccidentUploadDetails.size());
                 request.setAttribute("AccidentUploadDetails", AccidentUploadDetails);
     
     
     
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(accident_Id);
                 System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
                 ArrayList VehicleAccident = new ArrayList();
                 VehicleAccident = accidentBP.getVehicleAccident();
                 System.out.println("VehicleAccident = " + VehicleAccident.size());
                 request.setAttribute("VehicleAccident", VehicleAccident);
                 
     
     
             } catch (FPRuntimeException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             } catch (Exception exception) {
                 exception.printStackTrace();
                 FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             }
             return new ModelAndView(path);
         }
     
         public ModelAndView handleupdateSaveAccidentVehicle(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
     
             if (request.getSession().isNew()) {
                 return new ModelAndView("content/common/login.jsp");
             }
             accidentCommand = command;
             HttpSession session = request.getSession();
             String path = "";
     
             int userId = (Integer) session.getAttribute("userId");
             int companyType = (Integer) session.getAttribute("companyTypeId");
             String companyId = (String) session.getAttribute("companyId");
             int madhavaramSp = 1011;
             ModelAndView mv = new ModelAndView();
     
             path = "content/Accident/vehicleAccidentPage.jsp";
             String menuPath = "Vehicle  >>  ADD  ";
             request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
             AccidentTO accidentTO = new AccidentTO();
             CompanyTO companyTO = new CompanyTO();
             //multipart
     
             String newFileName = "", actualFilePath = "";
             String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
             boolean isMultipart = false;
             Part partObj = null;
             FilePart fPart = null;
             int i = 0;
             int j = 0;
             int m = 0;
             int n = 0;
             int p = 0;
             int s = 0;
             int editStatusId = 0;
             int editStatusId1 = 0;
             int editStatusId2 = 0;
     
             int imageId = 0;
             String tripSheetId1 = "";
             String userId1 = "";
             String acc_VehicleNo1 = "";
             String acc_VehicleDetail1 = "";
             String acc_DriverName1 = "";
             // String settlementType = request.getParameter("settlementType");
             String acc_DriverLicNo1 = "";
             String acc_InsurerName1 = "";
             String acc_PolicyNo1 = "";
             String acc_ContactDetail1 = "";
             String acc_ContactPerson1 = "";
             String casualty_Name1 = "";
             String casualty_Address1 = "";
             String casualty_ConDetail1 = "";
             String casualty_Type1 = "";
     
             String casualty_AlterNo1 = "";
             String casualty_ClaimDetail1 = "";
             String acc_ReferenceNo1 = "";
             String acc_SurveyorNo1 = "";
             String acc_FirNo1 = "";
             String witeness_Name1 = "";
             String witeness_Address1 = "";
             String witeness_Contact1 = "";
             String rto_Fine1 = "";
             String rto_StationRelease1 = "";
             String rto_Date1 = "";
             String workshop_Type1 = "";
             String ins_Tie_up1 = "";
             String adhoc_Type1 = "";
     
             //Add vehicle end
             String surv_InspectionDate1 = "";
             String surv_QuotationAmt1 = "";
             String surv_InsurerName1 = "";
             String surv_PolicyNo1 = "";
             String reim_Date1 = "";
             String reim_Amt1 = "";
             String reim_VehicleExcpen1 = "";
             String reim_BillAmt1 = "";
             String lawyerName1 = "";
             String law_ContactNo1 = "";
             String law_court_Address1 = "";
             String law_Fees1 = "";
             String miscels_Fees1 = "";
             String ins_Insurer1 = "";
             String ins_Owner1 = "";
             String adhoc_BillAmt1 = "";
             String adhoc_PaidAmt1 = "";
             String acc_SurveyorName1 = "";
             String ploice_Address1 = "";
             String altVehicleId = "";
     
             String spotAddress1 = "";
             String accidentDate1 = "";
             String accFirDate1 = "";
             String perOfDisability1 = "";
     
     
             //        String[] podRemarks1 = new String[10];
             String[] fileSaved = new String[10];
             //        String[] lrNumber1 = new String[10];
             String[] uploadedFileName = new String[10];
             String[] tempFilePath = new String[10];
             String[] vehicleDetails1 = new String[10];
             String[] uploadedFile = new String[10];
             String[] accidentUploadId = new String[10];
             String[] insuImageId = new String[10];
             String[] editStatus = new String[10];
             String[] editStatus1 = new String[10];
             String[] editStatus2 = new String[10];
     
             String vehicleId1 = request.getParameter("vehicleId");
             String accident_Id1 = request.getParameter("accident_Id");
     
     
     
             try {
                 request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
     
     
     
                 isMultipart = org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload.isMultipartContent(request);
                 if (isMultipart) {
                     System.out.println("this is tht");
                     MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                     while ((partObj = parser.readNextPart()) != null) {
                         System.out.println("part Name:" + partObj.getName());
                         if (partObj.isFile()) {
                             actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                             System.out.println("Server Path == " + actualServerFilePath);
                             tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                             System.out.println("Server Path After Replace== " + tempServerFilePath);
                             Date now = new Date();
                             String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                             String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                             fPart = (FilePart) partObj;
                             uploadedFileName[j] = fPart.getFileName();
                             System.out.println("fPart.getFileName() = " + fPart.getFileName());
     
                             if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                                 System.out.println("partObj.getName() = " + partObj.getName());
                                 String[] splitFileName = uploadedFileName[j].split("\\.");
                                 fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                 System.out.println("fileSavedAs = " + fileSavedAs);
                                 fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                                 fileName = fileSavedAs;
                                 tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
                                 actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                                 System.out.println("tempPath..." + tempFilePath);
                                 System.out.println("actPath..." + actualFilePath);
                                 long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                                 System.out.println("fileSize..." + fileSize);
                                 File f1 = new File(actualFilePath);
                                 System.out.println("check " + f1.isFile());
                                 f1.renameTo(new File(tempFilePath[j]));
                                 System.out.println("tempPath = " + tempFilePath);
                                 System.out.println("actPath = " + actualFilePath);
                                 //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                                 //                            String part1 = parts.replace("\\", "");
                             }
     
                             System.out.println("fileName..." + fileName);
                             if (tempFilePath[j] != null && !"".equals(tempFilePath[j])) {
                                 j++;
                             }
                         } else if (partObj.isParam()) {
                             if (partObj.getName().equals("tripSheetId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 tripSheetId1 = paramPart.getStringValue();
                             }
     
     
                             if (partObj.getName().equals("vehicleDetails")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 uploadedFile[s] = paramPart.getStringValue();
                                 System.out.println("uploadedFile[j] = " + uploadedFile[s]);
                                 s++;
                             }
     
                             if (partObj.getName().equals("accidentUploadId")) {
                                 String insuImageIdTemp = "";
                                 ParamPart paramPart = (ParamPart) partObj;
                                 insuImageIdTemp = paramPart.getStringValue();
                                 System.out.println("insuImageIdTemp = " + insuImageIdTemp);
                                 accidentUploadId[imageId] = insuImageIdTemp;
                                 imageId++;
                             }
                             if (partObj.getName().equals("userId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 userId1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_VehicleNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_VehicleNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_VehicleDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_VehicleDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_DriverName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_DriverName1 = paramPart.getStringValue();
                             }
     
                             if (partObj.getName().equals("acc_DriverLicNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_DriverLicNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_InsurerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_InsurerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_PolicyNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_PolicyNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ContactDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ContactDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ContactPerson")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ContactPerson1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Name")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Name1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_ConDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_ConDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_Type1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_AlterNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_AlterNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("casualty_ClaimDetail")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 casualty_ClaimDetail1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_ReferenceNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_ReferenceNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_SurveyorNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_SurveyorNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_FirNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_FirNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Name")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Name1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("witeness_Contact")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 witeness_Contact1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_Fine")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_Fine1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_StationRelease")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_StationRelease1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("rto_Date")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 rto_Date1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("workshop_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 workshop_Type1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ins_Tie_up")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Tie_up1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_Type")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_Type1 = paramPart.getStringValue();
                             }
                             //add vehicle end
                             if (partObj.getName().equals("surv_InspectionDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_InspectionDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_QuotationAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_QuotationAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_InsurerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_InsurerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("surv_PolicyNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 surv_PolicyNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_Date")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_Date1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_Amt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_Amt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_VehicleExcpen")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_VehicleExcpen1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("reim_BillAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 reim_BillAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("lawyerName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 lawyerName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_ContactNo")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_ContactNo1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_court_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_court_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("law_Fees")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 law_Fees1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("miscels_Fees")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 miscels_Fees1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("vehicleId")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 vehicleId1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accident_Id")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 altVehicleId = paramPart.getStringValue();
                                 //                             System.out.println("accident_Id = " + accident_Id);
                             }
                             //                           if (partObj.getName().equals("accident_Id")) {
                             //                               ParamPart paramPart = (ParamPart) partObj;
                             //                               accident_Id1 = paramPart.getStringValue();
                             //                           }
                             if (partObj.getName().equals("ins_Insurer")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Insurer1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ins_Owner")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ins_Owner1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_BillAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_BillAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("adhoc_PaidAmt")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 adhoc_PaidAmt1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("ploice_Address")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 ploice_Address1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("acc_SurveyorName")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 acc_SurveyorName1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("spotAddress")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 spotAddress1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accidentDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 accidentDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("accFirDate")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 accFirDate1 = paramPart.getStringValue();
                             }
                             if (partObj.getName().equals("perOfDisability")) {
                                 ParamPart paramPart = (ParamPart) partObj;
                                 perOfDisability1 = paramPart.getStringValue();
                             }
     
                             if (partObj.getName().equals("editStatus")) {
                                 String editStatusTemp = "";
                                 ParamPart paramPart = (ParamPart) partObj;
                                 editStatusTemp = paramPart.getStringValue();
                                 System.out.println("editStatusTemp = " + editStatusTemp);
                                 editStatus[editStatusId] = editStatusTemp;
                                 editStatusId++;
                             }
                             if (partObj.getName().equals("editStatus1")) {
                                 String editStatusTemp1 = "";
                                 ParamPart paramPart = (ParamPart) partObj;
                                 editStatusTemp1 = paramPart.getStringValue();
                                 System.out.println("editStatusTemp1 = " + editStatusTemp1);
                                 editStatus1[editStatusId1] = editStatusTemp1;
                                 editStatusId1++;
                             }
                             if (partObj.getName().equals("editStatus2")) {
                                 String editStatusTemp2 = "";
                                 ParamPart paramPart = (ParamPart) partObj;
                                 editStatusTemp2 = paramPart.getStringValue();
                                 System.out.println("editStatusTemp2 = " + editStatusTemp2);
                                 editStatus2[editStatusId2] = editStatusTemp2;
                                 editStatusId2++;
                             }
     
     
                         }
                     }
                 }
                 int index = j;
                 String[] file = new String[index];
                 String[] uploadedFileTemp = new String[s];
                 String[] insuImageIdTemp = new String[imageId];
                 String[] editStatusTemp = new String[editStatusId];
                 String[] editStatusTemp1 = new String[editStatusId1];
                 String[] editStatusTemp2 = new String[editStatusId2];
     
                 for (int uploadFile = 0; uploadFile < s; uploadFile++) {
                     uploadedFileTemp[uploadFile] = uploadedFile[uploadFile];
                     System.out.println("uploadFile = " + uploadedFileTemp[uploadFile]);
     
                 }
                 for (int inImageId = 0; inImageId < imageId; inImageId++) {
                     insuImageIdTemp[inImageId] = insuImageId[inImageId];
                     System.out.println(" insuImageIdTemp[inImageId] = " + insuImageIdTemp[inImageId]);
                 }
                 for (int editStat = 0; editStat < editStatusId; editStat++) {
                     editStatusTemp[editStat] = editStatus[editStat];
                     System.out.println(" editStatusTemp[editStatusId] = " + editStatusTemp[editStat]);
                 }
                 for (int editStat1 = 0; editStat1 < editStatusId1; editStat1++) {
                     editStatusTemp1[editStat1] = editStatus1[editStat1];
                     System.out.println(" editStatusTemp[editStatusId1] = " + editStatusTemp1[editStat1]);
                 }
                 for (int editStat2 = 0; editStat2 < editStatusId2; editStat2++) {
                     editStatusTemp2[editStat2] = editStatus2[editStat2];
                     System.out.println(" editStatusTemp[editStatusId2] = " + editStatusTemp2[editStat2]);
                 }
     
                 System.out.println("uploadedFileTemp.length = " + uploadedFileTemp.length);
                 System.out.println("insuImageIdTemp = " + insuImageIdTemp.length);
                 System.out.println("editStatusTemp = " + editStatusTemp.length);
     
     
                 System.out.println("filekoik" + file);
                 String remarks = "";
     
                 String[] saveFile = new String[index];
                 ;
     
                 accidentTO = new AccidentTO();
     
                 int status = 0;
     
                 //file upload
     
     
                 accidentTO.setUsageId(userId1);
                 System.out.println("userId" + accidentTO.getUsageId());
                 accidentTO.setAcc_VehicleNo(acc_VehicleNo1);
                 System.out.println("acc_VehicleNo" + accidentTO.getAcc_VehicleNo());
                 accidentTO.setAcc_VehicleDetail(acc_VehicleDetail1);
                 System.out.println("acc_VehicleDetail" + accidentTO.getAcc_VehicleDetail());
                 accidentTO.setAcc_DriverName(acc_DriverName1);
                 System.out.println("acc_DriverName" + accidentTO.getAcc_DriverName());
                 accidentTO.setAcc_DriverLicNo(acc_DriverLicNo1);
                 accidentTO.setAcc_InsurerName(acc_InsurerName1);
                 accidentTO.setAcc_PolicyNo(acc_PolicyNo1);
                 accidentTO.setAcc_ContactDetail(acc_ContactDetail1);
                 accidentTO.setAcc_ContactPerson(acc_ContactPerson1);
                 accidentTO.setCasualty_Name(casualty_Name1);
                 accidentTO.setCasualty_Address(casualty_Address1);
                 accidentTO.setCasualty_ConDetail(casualty_ConDetail1);
                 accidentTO.setCasualty_Type(casualty_Type1);
                 accidentTO.setCasualty_AlterNo(casualty_AlterNo1);
                 accidentTO.setCasualty_ClaimDetail(casualty_ClaimDetail1);
                 accidentTO.setAcc_ReferenceNo(acc_ReferenceNo1);
                 accidentTO.setAcc_SurveyorNo(acc_SurveyorNo1);
                 accidentTO.setAcc_FirNo(acc_FirNo1);
                 accidentTO.setWiteness_Name(witeness_Name1);
                 accidentTO.setWiteness_Address(witeness_Address1);
                 accidentTO.setWiteness_Contact(witeness_Contact1);
                 accidentTO.setRto_Fine(rto_Fine1);
                 accidentTO.setRto_StationRelease(rto_StationRelease1);
                 accidentTO.setRto_Date(rto_Date1);
                 accidentTO.setWorkshop_Type(workshop_Type1);
                 accidentTO.setIns_Tie_up(ins_Tie_up1);
                 accidentTO.setAdhoc_Type(adhoc_Type1);
                 accidentTO.setSurv_InspectionDate(surv_InspectionDate1);
     
     
                 accidentTO.setSurv_QuotationAmt(surv_QuotationAmt1);
                 accidentTO.setSurv_InsurerName(surv_InsurerName1);
                 accidentTO.setSurv_PolicyNo(surv_PolicyNo1);
                 accidentTO.setReim_Date(reim_Date1);
                 accidentTO.setReim_Amt(reim_Amt1);
                 accidentTO.setReim_VehicleExcpen(reim_VehicleExcpen1);
                 accidentTO.setReim_BillAmt(reim_BillAmt1);
                 accidentTO.setLawyerName(lawyerName1);
                 accidentTO.setLaw_ContactNo(law_ContactNo1);
                 accidentTO.setLaw_court_Address(law_court_Address1);
                 accidentTO.setLaw_Fees(law_Fees1);
                 accidentTO.setMiscels_Fees(miscels_Fees1);
                 accidentTO.setIns_Insurer(ins_Insurer1);
                 accidentTO.setIns_Owner(ins_Owner1);
                 accidentTO.setAdhoc_BillAmt(adhoc_BillAmt1);
                 accidentTO.setAdhoc_PaidAmt(adhoc_PaidAmt1);
                 accidentTO.setAcc_SurveyorName(acc_SurveyorName1);
                 accidentTO.setPloice_Address(ploice_Address1);
                 accidentTO.setSpotAddress(spotAddress1);
                 accidentTO.setAccidentDate(accidentDate1);
                 accidentTO.setAccFirDate(accFirDate1);
     
     
                 accidentTO.setVehicleId(vehicleId1);
     
                 accidentTO.setAccident_Id(altVehicleId);
     
     
                 //accidentTO.setVehicleDetails(vehicleDetails1);
     
                 System.out.println("j = " + j);
                 System.out.println("file = " + file.length);
                 System.out.println("tempFilePath[0] = " + tempFilePath[0]);
                 System.out.println("tempFilePath[1] = " + tempFilePath[1]);
     
     
                 for (int x = 0; x < j; x++) {
                     System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                     file[x] = tempFilePath[x];
                     System.out.println("saveFile[x] = " + fileSaved[x]);
                     saveFile[x] = fileSaved[x];
     
     
                 }
                 //end maultipart
     
     
                 // String insId=request.getParameter("insId");
     
                 String pageTitle = "View Vehicles";
     
     
     
                 ArrayList AccidentUploadDetails = new ArrayList();
                 AccidentUploadDetails = accidentBP.getVehicleAccidentImageDetails(Integer.parseInt(altVehicleId));
                 request.setAttribute("AccidentUploadDetails", AccidentUploadDetails);
     
     
//                 ArrayList InsComanyNameList = new ArrayList();
//                 InsComanyNameList = accidentBP.processInsCompanyList();
//                 request.setAttribute("InsComanyNameList", InsComanyNameList);
     
                 ArrayList vehicleAccidentList = new ArrayList();
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(altVehicleId);
                 System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
     
     
                 request.setAttribute("pageTitle", pageTitle);
                 int Accident_Id = 0;
     
                 Accident_Id = accidentBP.updateSaveAccidentVehicle(accidentTO, userId, madhavaramSp, file, tripSheetId1, saveFile, uploadedFileTemp, insuImageIdTemp, editStatusTemp, editStatusTemp1, editStatusTemp2);
                 //(accidentTO, userId, madhavaramSp,file, tripSheetId1, saveFile,uploadedFileTemp,insuImageIdTemp,editStatusTemp,editStatusTemp1,editStatusTemp2);
                 mv = handleviewVehicleAccidentPage(request, response, command);
                 
//                 ArrayList vehicleRegNos = new ArrayList();
//                 vehicleRegNos = accidentBP.getVehicleRegNos();
//                 request.setAttribute("vehicleRegNos", vehicleRegNos);
                
                 path = "content/Accident/viewVehicleAccident.jsp";
                 request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Updated Successfully");
     
                 vehicleAccidentList = accidentBP.getVehicleAccidentList(altVehicleId);
                 request.setAttribute("vehicleAccidentList", vehicleAccidentList);
     
     
             } catch (FPRuntimeException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             } catch (FPBusinessException exception) {
                 /*
                  * run time exception has occurred. Directed to error page.
                  */
                 FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
                 request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                         exception.getErrorMessage());
             } catch (Exception exception) {
                 exception.printStackTrace();
                 FPLogUtils.fpErrorLog("Failed to retrieve Add vehicle page data --> " + exception);
                 return new ModelAndView("content/common/error.jsp");
             }
             return mv;
    }
         
         public ModelAndView handleVehicleAccidentHistory(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList vehicleAccidentList = new ArrayList();
        ArrayList vehicleAccidentHistory = new ArrayList();
        ArrayList viewvehicleAccidentHistorylist = new ArrayList();
        ArrayList vehicleAccidentHistorylist = new ArrayList();
        ArrayList AccidentUploadDetails = new ArrayList();
        HttpSession session = request.getSession();
        accidentCommand = command;
        String menuPath = "";
        AccidentTO accidentTO = null;
        String pageTitle = "View Accident Details";
        menuPath = "Vehicle  >> Accident Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String altVehicleId = "";
        String buttonClicked = "";
        if (request.getParameter("button") != null && request.getParameter("button") != "") {
            buttonClicked = request.getParameter("button");
        }
        String accidentId = request.getParameter("accidentId");
        System.out.println("accidentId" + accidentId);

        String insId = request.getParameter("insId");
        System.out.println("insId" + insId);

        accidentTO = new AccidentTO();




        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //   path = "content/Accident/vehicleAccidentHistory.jsp";

            String vehicleId = request.getParameter("vehicleId");
            System.out.println("vehicleId" + vehicleId);
            if (accidentId != null && !"".equals("accidentId") && insId != null && !"".equals("insId")) {
                System.out.println("Vehicle3");

                accidentTO.setInsId(insId);
                accidentTO.setAccident_Id(accidentId);

                viewvehicleAccidentHistorylist = accidentBP.viewvehicleAccidentHistorylist(accidentTO);
                System.out.println("viewvehicleAccidentHistorylist=" + viewvehicleAccidentHistorylist.size());
                request.setAttribute("viewvehicleAccidentHistorylist", viewvehicleAccidentHistorylist);



                AccidentUploadDetails = accidentBP.getVehicleAccidentImageDetails(Integer.parseInt(accidentId));
                System.out.println("AccidentUploadDetails=" + AccidentUploadDetails.size());
                request.setAttribute("AccidentUploadDetails", AccidentUploadDetails);

                 ArrayList VehicleAccidentDetailUnique = new ArrayList();
                 VehicleAccidentDetailUnique = accidentBP.processVehicleAccidentDetail(accidentTO);
                 System.out.println("VehicleAccidentDetailUnique=" + VehicleAccidentDetailUnique.size());
                 request.setAttribute("VehicleAccidentDetailUnique", VehicleAccidentDetailUnique);


                vehicleAccidentList = accidentBP.getVehicleAccidentList(accidentId);
                System.out.println("vehicleAccidentList = " + vehicleAccidentList.size());
                request.setAttribute("vehicleAccidentList", vehicleAccidentList);

//                ArrayList InsComanyNameList = new ArrayList();
//                InsComanyNameList = accidentBP.processInsCompanyList();
//                request.setAttribute("InsComanyNameList", InsComanyNameList);


                path = "content/Accident/viewVehicleAccidentHistoryList.jsp";
                
                request.setAttribute("accident_Id", accidentId);


            } else if (vehicleId != null && !"".equals("vehicleId") && accidentId == null || "".equals("accidentId") && insId == null || "".equals("insId")) {
                System.out.println("Vehicle2");
                path = "content/Accident/vehicleAccidentHistoryList.jsp";

                vehicleAccidentHistorylist = accidentBP.getVehicleAccidentHistorylist(accidentTO, vehicleId);
                System.out.println("vehicleAccidentHistorylist=" + vehicleAccidentHistorylist.size());
                request.setAttribute("vehicleAccidentHistorylist", vehicleAccidentHistorylist);
            } else {
                System.out.println("Vehicle1");
                path = "content/Accident/vehicleAccidentHistory.jsp";
                vehicleAccidentHistory = accidentBP.processVehicleAccidentHistory(accidentTO);
                System.out.println("vehicleAccidentHistory=" + vehicleAccidentHistory.size());
                request.setAttribute("vehicleAccidentHistory", vehicleAccidentHistory);
            }

            

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
         
         
         public ModelAndView addAccidentVehicle(HttpServletRequest request, HttpServletResponse response, AccidentCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        accidentCommand = command;
        String menuPath = "";
        AccidentTO accidentTO = null;
        String pageTitle = "Add Accident Details";
        menuPath = "Vehicle  >> Accident Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        accidentTO = new AccidentTO();
        int addVehicleStatus =0;
         System.out.println("addAccidentVehicle = ");
          int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (accidentCommand.getVehicleId() != null && accidentCommand.getVehicleId() != "") {
                accidentTO.setVehicleId(accidentCommand.getVehicleId());
//                System.out.println("accidentCommand.getVehicleId() = " + accidentCommand.getVehicleId());
            }
            
            addVehicleStatus = accidentBP.addAccidentVehicle(accidentTO,userId);
            if (addVehicleStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Accident Vehicle Added Successfully");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Accident Vehicle  Not Added Successfully");
            }
            path = "content/Accident/viewVehicleAccident.jsp";
                
            ArrayList VehicleAccident = new ArrayList();
            VehicleAccident = accidentBP.getVehicleAccident();
            System.out.println("VehicleAccident = " + VehicleAccident.size());
            request.setAttribute("VehicleAccident", VehicleAccident);
            
            ArrayList vehicleList = new ArrayList();
            vehicleList = accidentBP.getVehicleList();
            request.setAttribute("vehicleList", vehicleList);
            
//            ArrayList vehicleRegNos = new ArrayList();
//            vehicleRegNos = accidentBP.getVehicleRegNos();
//            request.setAttribute("vehicleRegNos", vehicleRegNos);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

}
