/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.company.data;

/**
 *
 * @author vinoth
 */
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.company.business.CompanyTO;
import ets.domain.users.web.CryptoLibrary;

public class CompanyDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";

    public CompanyDAO() {
    }

public int doInsertNewCompany(CompanyTO companyTO, int userId,SqlMapSession session) {
        Map map = new HashMap();
        int status = 0;       
        map.put("Name", companyTO.getName());
        map.put("companyType", companyTO.getCompanyTypeId());
        map.put("Address", companyTO.getAddress());
        map.put("City", companyTO.getCity());
        map.put("district", companyTO.getDistrict());
        map.put("state", companyTO.getState());
        map.put("pinCode", companyTO.getPinCode());
        map.put("phone1", companyTO.getPhone1());
        map.put("phone2", companyTO.getPhone2());
        map.put("fax", companyTO.getFax());
        map.put("email", companyTO.getEmail());
        map.put("gstNo", companyTO.getGstNo());
        map.put("stateId", companyTO.getStateId());
        map.put("organizationId", companyTO.getOrganizationId());
        map.put("web", companyTO.getWeb());
        map.put("userId", userId);
        try {
            status = (Integer) session.update("company.insertcompany", map);           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public String getCompanyName(int companyId) {
        Map map = new HashMap();
        map.put("companyId", companyId);       
        String companyName = null;
        try {
            companyName = (String) getSqlMapClientTemplate().queryForObject("company.checkcompanyName", map);           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "desigName", sqlException);
        }
        return companyName;
    }

    public ArrayList getCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("company.getCompanyList", map);           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return companyList;
    }

    //Get Make Data DAO Start
    public ArrayList getMakeList() {
        Map map = new HashMap();
        ArrayList makeList = new ArrayList();
        try {
            makeList = (ArrayList) getSqlMapClientTemplate().queryForList("company.getMakeList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return makeList;
    }
    public ArrayList GetTyreLifeCycleList() {
        Map map = new HashMap();
        ArrayList TyreLifeList = new ArrayList();
        try {
            TyreLifeList = (ArrayList) getSqlMapClientTemplate().queryForList("company.getCompanyList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return TyreLifeList;
    }
    //Get Make Data DAO End

    public ArrayList getCompanyDetail(int companyId) {
        Map map = new HashMap();
        map.put("companyId", companyId);      
        ArrayList companyDetail = null;
        try {
            companyDetail = (ArrayList) getSqlMapClientTemplate().queryForList("company.getCompanyDetail", map);           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "desigName", sqlException);
        }
        return companyDetail;
    }

    public ArrayList getCompanyTypes() {
        Map map = new HashMap();
        ArrayList companyTypes = null;
        try {
            companyTypes = (ArrayList) getSqlMapClientTemplate().queryForList("company.getCompanyTypes", map);            
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "desigName", sqlException);
        }
        return companyTypes;
    }
public int doUpdateCompany(CompanyTO companyTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        CryptoLibrary cl = new CryptoLibrary();       
        map.put("userId", userId);
        map.put("companyId", companyTO.getCmpId());
        map.put("name", companyTO.getName());
        map.put("companyType", companyTO.getCompanyTypeId());
        map.put("address", companyTO.getAddress());
        map.put("city", companyTO.getCity());
        map.put("district", companyTO.getDistrict());
        map.put("state", companyTO.getState());
        map.put("pinCode", companyTO.getPinCode());
        map.put("phone1", companyTO.getPhone1());
        map.put("phone2", companyTO.getPhone2());
        map.put("fax", companyTO.getFax());
        map.put("email", companyTO.getEmail());
        map.put("web", companyTO.getWeb());
        map.put("status", companyTO.getStatus());
        map.put("stateId", companyTO.getStateId());
        map.put("organizationId", companyTO.getOrganizationId());
        map.put("gstNo", companyTO.getGstNo());
        try {
            status = (Integer) getSqlMapClientTemplate().update("company.updatecompany", map);           
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "doUpdateCompany", sqlException);
        }
        return status;
    }
}
