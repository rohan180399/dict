/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.scheduler.business.LoadCSVTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.scheduler.business.VehicleInfo;
import java.util.Iterator;

/**
 *
 * @author sabreesh
 */
public class SchedulerDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "SchedulerDAO";
    private SchedulerTO schTO;

    public ArrayList processWayBillList(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList getWayBillList = new ArrayList();
        try {
            getWayBillList = (ArrayList) session.queryForList("scheduler.getWayBillList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWayBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWayBillList", sqlException);
        }
        return getWayBillList;
    }
    
    public ArrayList processWayBillToCSV(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList wayBillsListForCSV = new ArrayList();
        map.put("wayBillId",schedulerTO.getWayBillId());
        try {
            wayBillsListForCSV = (ArrayList) session.queryForList("scheduler.getWayBillListForCSV", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gpsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gpsDetails", sqlException);
        }
        return wayBillsListForCSV;
    }
    
    public int updateWayBillCSVStatus(SchedulerTO schedulerTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("wayBillId", schedulerTO.getWayBillId());
        map.put("status", 1);
        try {
            updateStatus = (Integer) session.update("scheduler.updateCSVStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gpsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gpsDetails", sqlException);
        }
        return updateStatus;
    }
    
    public int processGPSData(SchedulerTO schedulerTO, String fromDate, String toDate) {
        Map map = new HashMap();
        int insertStatus = 0;
        ArrayList gpsList = new ArrayList();
        try {
            gpsList = (ArrayList) schedulerTO.getVehicleInfo();
            Iterator itr = gpsList.iterator();
            while (itr.hasNext()) {
                VehicleInfo veh = (VehicleInfo) itr.next();
                map.put("vehicleId", veh.getVehicleId());
                map.put("vehicleAlias", veh.getVehicleAlias());
                map.put("vehicleRtoNo", veh.getVehicleRtoNo());
                map.put("currentLocation", veh.getCurrentLocation());
                map.put("gpsDateTime", veh.getGpsDateTime());
                map.put("latitiude", veh.getLatitude());
                map.put("longitutde", veh.getLongitude());
                map.put("vehicleSpeed", veh.getVehicleSpeed());
                map.put("vehicleDaykm", veh.getVehicleDayKm());
                map.put("odometerReading", veh.getVehicleOdometerReading());
                map.put("geofeneceLoc", veh.getGeoFeneceLoc());
                map.put("geofenceInd", veh.getGeoFeneceInd());
                map.put("geofenceDateTime", veh.getGeoFeneceDateTime());
                map.put("vendor", veh.getVendor());
                // insert gps log
                insertStatus = (Integer) getSqlMapClientTemplate().update("scheduler.insertVehicleLocationLog", map);
                String checkVehicleStatus = (String) getSqlMapClientTemplate().queryForObject("scheduler.checkVehicleStatus", map);
                if(checkVehicleStatus == null){
                // insert gps details
                insertStatus = (Integer) getSqlMapClientTemplate().update("scheduler.insertVehicleLocation", map);
                }else {
                // update gps details
                insertStatus = (Integer) getSqlMapClientTemplate().update("scheduler.updateVehicleLocation", map);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gpsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gpsDetails", sqlException);
        }
        return insertStatus;
    }

    
    public ArrayList processOgplList(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList getWayBillList = new ArrayList();
        try {
            getWayBillList = (ArrayList) session.queryForList("scheduler.getOgplList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWayBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWayBillList", sqlException);
        }
        return getWayBillList;
    }
    
    public ArrayList processOGPLToCSV(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList wayBillsListForCSV = new ArrayList();
        map.put("ogplId",schedulerTO.getOgplId());
        try {
            wayBillsListForCSV = (ArrayList) session.queryForList("scheduler.getOGPLListForCSV", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gpsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gpsDetails", sqlException);
        }
        return wayBillsListForCSV;
    }
    
    public int updateOgplCSVStatus(SchedulerTO schedulerTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("ogplId", schedulerTO.getOgplId());
        map.put("status", 1);
        try {
            updateStatus = (Integer) session.update("scheduler.updateOgplCSVStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOgplCSVStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOgplCSVStatus", sqlException);
        }
        return updateStatus;
    }
    
    public int insertOgplBarCodeDetails(SchedulerTO schedulerTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        ArrayList barCodeList  = (ArrayList) schedulerTO.getBarCodeList();
        try {
            if(barCodeList.size() > 0)
            updateStatus = (Integer) session.update("scheduler.insertOgplBarCodeDetails", schedulerTO);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOgplCSVStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOgplCSVStatus", sqlException);
        }
        return updateStatus;
    }
    
    
    public ArrayList processOgplLoadList(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList ogplLoadList = new ArrayList();
        try {
            ogplLoadList = (ArrayList) session.queryForList("scheduler.getOgplLoadList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOgplLoadList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOgplLoadList", sqlException);
        }
        return ogplLoadList;
    }
    
    public ArrayList processOgplUnLoadList(SchedulerTO schedulerTO, String fromDate, String toDate, SqlMapClient session) {
        Map map = new HashMap();
        ArrayList ogplLoadList = new ArrayList();
        try {
            ogplLoadList = (ArrayList) session.queryForList("scheduler.processOgplUnLoadList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processOgplUnLoadList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "processOgplUnLoadList", sqlException);
        }
        return ogplLoadList;
    }
    
    public int updateBarCodeLoad(SchedulerTO schedulerTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        ArrayList barCodeList  = (ArrayList) schedulerTO.getBarCodeList();
        SchedulerTO schTO = new SchedulerTO();
        try {
            System.out.println("barCodeList.size() = " + barCodeList.size());
            if(barCodeList.size() > 0){
                Iterator itr = barCodeList.iterator();
                while(itr.hasNext()){
                    schTO = (SchedulerTO) itr.next();
                    updateStatus = (Integer) session.update("scheduler.updateBarCodeLoad", schTO);
                }
            }
            map.put("status", 1);
            map.put("ogplId", schedulerTO.getOgplId());
            updateStatus = (Integer) session.update("scheduler.updateOgplLoadedStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOgplCSVStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOgplCSVStatus", sqlException);
        }
        return updateStatus;
    } 
    
    public int updateBarCodeUnLoad(SchedulerTO schedulerTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        ArrayList barCodeList  = (ArrayList) schedulerTO.getBarCodeList();
        SchedulerTO schTO = new SchedulerTO();
        try {
            if(barCodeList.size() > 0){
                Iterator itr = barCodeList.iterator();
                while(itr.hasNext()){
                    schTO = (SchedulerTO) itr.next();
                    updateStatus = (Integer) session.update("scheduler.updateBarCodeUnLoad", schTO);
                    
                }
            }
            map.put("status", 1);
            map.put("consignmentStatus", 12);
            map.put("ogplId", schedulerTO.getOgplId());
            map.put("tripCode", schedulerTO.getTripCode());
            updateStatus = (Integer) session.update("scheduler.updateOgplUnLoadedStatus", map);
            session.update("scheduler.updateTripConsignmentUnLoadedStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOgplCSVStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOgplCSVStatus", sqlException);
        }
        return updateStatus;
    } 
    
    public int insertCSVData(LoadCSVTO csvTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        try {
        updateStatus = (Integer) session.update("scheduler.insertCSVDATA", csvTO);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOgplCSVStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOgplCSVStatus", sqlException);
        }
        return updateStatus;
    } 
    
    // scheduler starts here
    
    public ArrayList getDailyVehicleMovement() {
            Map map = new HashMap();
            ArrayList dvmDetailsDays = new ArrayList();
            try {
                    dvmDetailsDays = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDailyVehicleMovement", map);
                    System.out.println("getDvmDetailsDays"+dvmDetailsDays.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getDvmDetailsDays Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getDvmDetailsDays", sqlException);
            }
           
            return dvmDetailsDays;
    }   
    public ArrayList getTotalOrderList() {
            Map map = new HashMap();
            ArrayList getTotalOrderList = new ArrayList();
            try {
                    getTotalOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTotalOrderList", map);
                    System.out.println("getTotalOrderList"+getTotalOrderList.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTotalOrderList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getTotalOrderList", sqlException);
            }
           
            return getTotalOrderList;
    }   
    public ArrayList getCreatedOrderList(String consignmentOrderId,String status) {
            Map map = new HashMap();
            map.put("consignmentOrderId",consignmentOrderId);
            System.out.println("map@@@"+map);
            ArrayList getCreatedOrderList = new ArrayList();
            if(status == "created"){
              map.put("consignmentOrderStatus",5); 
              
            }else if(status == "freezed"){
              map.put("consignmentOrderStatus",8);
              
            }else if(status == "inProgress"){
              map.put("consignmentOrderStatus",10);
            
            }else if(status == "endOrder"){
              map.put("consignmentOrderStatus",12);   
            
            }else if(status == "closedOrder"){
              map.put("consignmentOrderStatus",13);   
            }
            try {
                    getCreatedOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getCreatedOrderList", map);
                    System.out.println("getCreatedOrderList"+getCreatedOrderList.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCreatedOrderList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getCreatedOrderList", sqlException);
            }
           
            return getCreatedOrderList;
    }   
    public ArrayList getUserActivitySummary() {
            Map map = new HashMap();
            ArrayList userActivitySummary = new ArrayList();
            try {
                    userActivitySummary = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getUserActivitySummary", map);
                    System.out.println("userActivitySummary"+userActivitySummary.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getUserActivitySummary Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getUserActivitySummary", sqlException);
            }
           
            return userActivitySummary;
    } 
    
    public ArrayList getGRCreatedSummary9AM() {
            Map map = new HashMap();
            ArrayList getGRCreatedSummary9AM = new ArrayList();
            try {
                    getGRCreatedSummary9AM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getGRCreatedSummary9AM", map);
                    System.out.println("getGRCreatedSummary9AM"+getGRCreatedSummary9AM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getGRCreatedSummary9AM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getGRCreatedSummary9AM", sqlException);
            }
           
            return getGRCreatedSummary9AM;
    }   
    public ArrayList getOrderSummary9AM() {
            Map map = new HashMap();
            ArrayList getOrderSummary9AM = new ArrayList();
            try {
                    getOrderSummary9AM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getOrderSummary9AM", map);
                    System.out.println("getOrderSummary9AM"+getOrderSummary9AM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getOrderSummary9AM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getOrderSummary9AM", sqlException);
            }
           
            return getOrderSummary9AM;
    }   
    public ArrayList getTripCLosedSummary9AM() {
            Map map = new HashMap();
            ArrayList getTripCLosedSummary9AM = new ArrayList();
            try {
                    getTripCLosedSummary9AM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTripCLosedSummary9AM", map);
                    System.out.println("getTripCLosedSummary9AM"+getTripCLosedSummary9AM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTripCLosedSummary9AM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getTripCLosedSummary9AM", sqlException);
            }
           
            return getTripCLosedSummary9AM;
    } 
    
    public ArrayList getGRCreatedSummary9PM() {
            Map map = new HashMap();
            ArrayList getGRCreatedSummary9PM = new ArrayList();
            try {
                    getGRCreatedSummary9PM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getGRCreatedSummary9PM", map);
                    System.out.println("getGRCreatedSummary9AM"+getGRCreatedSummary9PM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getGRCreatedSummary9AM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getGRCreatedSummary9AM", sqlException);
            }
           
            return getGRCreatedSummary9PM;
    }   
    public ArrayList getOrderSummary9PM() {
            Map map = new HashMap();
            ArrayList getOrderSummary9PM = new ArrayList();
            try {
                    getOrderSummary9PM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getOrderSummary9PM", map);
                    System.out.println("getOrderSummary9PM"+getOrderSummary9PM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getOrderSummary9PM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getOrderSummary9PM", sqlException);
            }
           
            return getOrderSummary9PM;
    }   
    public ArrayList getTripCLosedSummary9PM() {
            Map map = new HashMap();
            ArrayList getTripCLosedSummary9PM = new ArrayList();
            try {
                    getTripCLosedSummary9PM = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTripCLosedSummary9PM", map);
                    System.out.println("getTripCLosedSummary9PM"+getTripCLosedSummary9PM.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTripCLosedSummary9PM Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getTripCLosedSummary9AM", sqlException);
            }
           
            return getTripCLosedSummary9PM;
    }   
    
    
    //daily process main
    public ArrayList getHourlyTotalOrderList() {
            Map map = new HashMap();
            ArrayList getHourlyTotalOrderList = new ArrayList();
            try {
                    getHourlyTotalOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getHourlyTotalOrderList", map);
                    System.out.println("getHourlyTotalOrderList"+getHourlyTotalOrderList.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getHourlyTotalOrderList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getHourlyTotalOrderList", sqlException);
            }
           
            return getHourlyTotalOrderList;
    }   
    public ArrayList tripEndedNotClosedBeyond24() {
            Map map = new HashMap();
            ArrayList tripEndedNotClosedBeyond24 = new ArrayList();
            try {
                    tripEndedNotClosedBeyond24 = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.tripEndedNotClosedBeyond24", map);
                    System.out.println("tripEndedNotClosedBeyond24"+tripEndedNotClosedBeyond24.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("tripEndedNotClosedBeyond24 Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "tripEndedNotClosedBeyond24", sqlException);
            }
           
            return tripEndedNotClosedBeyond24;
    }   
    public ArrayList getTripFreezingEveryTwoHourAlert(int hour) {
            Map map = new HashMap();
            ArrayList getTripFreezingEveryTwoHourAlert = new ArrayList();
            try {
                    getTripFreezingEveryTwoHourAlert = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTripFreezingEveryTwoHourAlert", map);
                    System.out.println("getTripFreezingEveryTwoHourAlert"+getTripFreezingEveryTwoHourAlert);
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTripFreezingEveryTwoHourAlert Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getTripFreezingEveryTwoHourAlert", sqlException);
            }
           
            return getTripFreezingEveryTwoHourAlert;
    }   
     //daily process main end
    
    
    // Daily Vehicle Trip Details scheduler
    
    public Integer getDailyVehicleTripDetails() {
        Map map = new HashMap();
        String suggestions = "";
        SchedulerTO schTO =new SchedulerTO();    
        Integer status=0;
        try {
            ArrayList alertsDetails = new ArrayList();
            ArrayList alertsDetailsTest = new ArrayList();
//            ArrayList getVehicleTripIds = new ArrayList();
            alertsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTotalVehicle", map);
            Iterator itr = alertsDetails.iterator();
            while (itr.hasNext()) {
                schTO = new SchedulerTO();
                schTO = (SchedulerTO) itr.next();
                System.out.println("vehicleId:"+ schTO.getVehicleId());
                map.put("vehicleId",schTO.getVehicleId());
                String tripIds = (String) getSqlMapClientTemplate().queryForObject("scheduler.getVehicleTripId", map);
//                getVehicleTripIds = (String) getSqlMapClientTemplate().queryForList("scheduler.getVehicleTripId", map);
                System.out.println("TripId:"+tripIds);  
                
                map.put("vehicleId",schTO.getVehicleId());
                map.put("tripId",tripIds);
                System.out.println("map@@@@"+map);
                
                status = (Integer) getSqlMapClientTemplate().update("scheduler.insertVehicleTripDetails", map);
                System.out.println("insertVehicleTripDetails###=="+status);
                
                alertsDetailsTest.add(schTO);
            System.out.println("alertsDetails.size="+alertsDetailsTest.add(schTO));
            }
            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return status;
    }
    
 
     
  // process main
     
     public ArrayList getPermitDueFullDetails() {
            Map map = new HashMap();
            ArrayList getPermitDueFullDetails = new ArrayList();
            try {
                    getPermitDueFullDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getPermitDueFullDetails", map);
                    System.out.println("getPermitDueFullDetails"+getPermitDueFullDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getPermitDueFullDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getPermitDueFullDetails", sqlException);
            }
           
            return getPermitDueFullDetails;
    }   
     public ArrayList getInsuranceDueFullDetails() {
            Map map = new HashMap();
            ArrayList getInsuranceDueFullDetails = new ArrayList();
            try {
                    getInsuranceDueFullDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getInsuranceDueFullDetails", map);
                    System.out.println("getInsuranceDueFullDetails"+getInsuranceDueFullDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getInsuranceDueFullDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getInsuranceDueFullDetails", sqlException);
            }
           
            return getInsuranceDueFullDetails;
    }   
     public ArrayList getFcDueFullDetails() {
            Map map = new HashMap();
            ArrayList getFcDueFullDetails = new ArrayList();
            try {
                    getFcDueFullDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getFcDueFullDetails", map);
                    System.out.println("getFcDueFullDetails"+getFcDueFullDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getFcDueFullDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getFcDueFullDetails", sqlException);
            }
           
            return getFcDueFullDetails;
    }   
     public ArrayList getRoadTaxDueFullDetails() {
            Map map = new HashMap();
            ArrayList getRoadTaxDueFullDetails = new ArrayList();
            try {
                    getRoadTaxDueFullDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getRoadTaxDueFullDetails", map);
                    System.out.println("getRoadTaxDueFullDetails"+getRoadTaxDueFullDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getRoadTaxDueFullDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getRoadTaxDueFullDetails", sqlException);
            }
           
            return getRoadTaxDueFullDetails;
    }   
     public ArrayList getServiceDueFullDetails() {
            Map map = new HashMap();
            ArrayList getServiceDueFullDetails = new ArrayList();
            try {
                    getServiceDueFullDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getServiceDueFullDetails", map);
                    System.out.println("getServiceDueFullDetails"+getServiceDueFullDetails.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getServiceDueFullDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getServiceDueFullDetails", sqlException);
            }
           
            return getServiceDueFullDetails;
    }   
     
     
//     --------------Monthly Billing List------------
     
     public ArrayList getTotalBillingList() {
            Map map = new HashMap();
            ArrayList getTotalBillingList = new ArrayList();
            try {
                    getTotalBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTotalOrderList", map);
                    System.out.println("getTotalBillingList"+getTotalBillingList.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getTotalBillingList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getTotalBillingList", sqlException);
            }
           
            return getTotalBillingList;
    }
     public ArrayList getVehicleNos(String ownerShip) {
            Map map = new HashMap();
            map.put("ownerShip",ownerShip);
            ArrayList getVehicleNos = new ArrayList();
            try {
                    getVehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getVehicleNos", map);
                    System.out.println("getVehicleNos"+getVehicleNos.size());
                    
                    if("3".equals(ownerShip)){
                     getVehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getMarketVehicleNos", map);
                    System.out.println("getVehicleNos"+getVehicleNos.size());
                    }
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getVehicleNos Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getVehicleNos", sqlException);
            }
           
            return getVehicleNos;
    }
     public ArrayList getBillingList(String ownerShip) {
            Map map = new HashMap();
            map.put("ownerShip",ownerShip);
            ArrayList getBillingList = new ArrayList();
            try {
                    getBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getBillingList", map);
                    System.out.println("getBillingList"+getBillingList.size());
                    
                    if("3".equals(ownerShip)){
                     getBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getMarketBillingList", map);
                    System.out.println("getBillingList"+getBillingList.size());
                    }
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getBillingList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getBillingList", sqlException);
            }
           
            return getBillingList;
    }
     public ArrayList getVehicleTripOwnCount() {
            Map map = new HashMap();
//            map.put("ownerShip",ownerShip);
            ArrayList getVehicleTripOwnCount = new ArrayList();
            try {
                    getVehicleTripOwnCount = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getVehicleTripOwnCount", map);
                    System.out.println("getVehicleTripOwnCount"+getVehicleTripOwnCount);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getVehicleTripOwnCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "getVehicleTripOwnCount", sqlException);
            }
           
            return getVehicleTripOwnCount;
    }
     public ArrayList getVehicleTripLeasedCount() {
            Map map = new HashMap();
//            map.put("ownerShip",ownerShip);
            ArrayList vehicleTripLeasedCount = new ArrayList();
            try {
                    vehicleTripLeasedCount = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.vehicleTripLeasedCount", map);
                    System.out.println("vehicleTripLeasedCount"+vehicleTripLeasedCount);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return vehicleTripLeasedCount;
    }
     public ArrayList getEligibleTripForStartEmail() {
            Map map = new HashMap();
//            map.put("ownerShip",ownerShip);
            ArrayList eligibleTripForStartEmail = new ArrayList();
            try {
                    eligibleTripForStartEmail = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getEligibleTripForStartEmail", map);
                    System.out.println("getEligibleTripForStartEmail-->"+eligibleTripForStartEmail.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return eligibleTripForStartEmail;
    }
     public ArrayList getEligibleTripForEndEmail() {
            Map map = new HashMap();
//            map.put("ownerShip",ownerShip);
            ArrayList eligibleTripForEndEmail = new ArrayList();
            try {
                    eligibleTripForEndEmail = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getEligibleTripForEndEmail", map);
                    System.out.println("getEligibleTripForEndEmail"+eligibleTripForEndEmail.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return eligibleTripForEndEmail;
    }
     public ArrayList getLatLong(String vehicleNo) {
            Map map = new HashMap();
           map.put("vehicleNo",vehicleNo);
            ArrayList latlong = new ArrayList();
            try {
                    latlong = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getLatLong", map);
                    System.out.println("getLatLong"+latlong.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return latlong;
    }
     public ArrayList getCityLatLong(String pointId) {
            Map map = new HashMap();
           map.put("pointId",pointId);
            ArrayList latlong = new ArrayList();
            System.out.println("getCityLatLong map"+map);
            try {
                    latlong = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getCityLatLong", map);
                    System.out.println("getCityLatLong"+latlong.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return latlong;
    }
     public ArrayList getStartEmailDetails(String tripId) {
            Map map = new HashMap();
           map.put("tripId",tripId);
            ArrayList tripDetails = new ArrayList();
            try {
                    tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getStartEmailDetails", map);
                    System.out.println("tripDetails"+tripDetails.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return tripDetails;
    }
     public int updateStartEmailNotification(String tripId) {
            Map map = new HashMap();
           map.put("tripId",tripId);
           int status=0;
           
            try {
                    status = (Integer) getSqlMapClientTemplate().update("scheduler.updateStartEmailNotification", map);
                    System.out.println("status....."+status);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return status;
    }
     public int updateEndEmailNotification(String tripId) {
            Map map = new HashMap();
           map.put("tripId",tripId);
           int status=0;
           System.out.println("map for update end notification:"+map);
            try {
                    status = (Integer) getSqlMapClientTemplate().update("scheduler.updateEndEmailNotification", map);
                    System.out.println("status....."+status);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return status;
    }
     public int updateInvoiceEmailNotification(String invoiceId,String status) {
            Map map = new HashMap();
           map.put("invoiceId",invoiceId);
           map.put("activeInd",status);
           int status1=0;
           System.out.println("map for update end notification:"+map);
            try {
                    status1 = (Integer) getSqlMapClientTemplate().update("scheduler.updateInvoiceEmailNotification", map);
                    System.out.println("status....."+status);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return status1;
    }
     public String getCustomerEmailId(String tripId) {
            Map map = new HashMap();
           map.put("tripId",tripId);
           String email="";
           
            try {
                    email = (String) getSqlMapClientTemplate().queryForObject("scheduler.getCustomerEmailId", map);
                    System.out.println("email....."+email);
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return email;
    }
     
     public ArrayList getOwnLeasedTripCount() {
            Map map = new HashMap();
          
            ArrayList ownLeasedTripCount = new ArrayList();
            try {
                    ownLeasedTripCount = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getOwnLeasedTripCount", map);
                    System.out.println("ownLeasedTripCount"+ownLeasedTripCount.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("vehicleTripLeasedCount Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return ownLeasedTripCount;
    }
 public ArrayList getInvoiceCustomerList(String fromDate,String toDate) {
            Map map = new HashMap();
             map.put ("fromDate",fromDate);
             map.put ("toDate",toDate);
            ArrayList customerList = new ArrayList();
            System.out.println("map...."+map);
            try {
                    customerList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getInvoiceCustomerList", map);
                    System.out.println("customerList"+customerList.size());
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getInvoiceCustomerList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return customerList;
    }
     public ArrayList getInvoiceIdList(String customerId,String fromDate,String toDate) {
            Map map = new HashMap();
          
            ArrayList invoiceIdList = new ArrayList();
            try {
                map.put("customerId",customerId);
                map.put("fromDate",fromDate);
                map.put("toDate",toDate);
                System.out.println("map ....."+map);
                if(fromDate != null && !"".equalsIgnoreCase(fromDate) && toDate != null && !"".equalsIgnoreCase(toDate)){
                    invoiceIdList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getInvoiceIdList", map);
                    System.out.println("invoiceIdList"+invoiceIdList.size());
                }
                    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getInvoiceCustomerList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-OPR-01", CLASS, "vehicleTripLeasedCount", sqlException);
            }
           
            return invoiceIdList;
    }
          
      public String getCustomerMailId( String customerId) {
        Map map = new HashMap();
        String mailId = "";
        try {
                map.put("customerId", customerId);
                // insert gps log
                 System.out.println("map ....."+map);
                 mailId = (String) getSqlMapClientTemplate().queryForObject("scheduler.getCustomerMailId", map);
 System.out.println("mailId ....."+mailId);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gpsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gpsDetails", sqlException);
        }
        return mailId;
    }
       public ArrayList vehicleUtilReport() {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
       
        System.out.println("map" + map);
        try {
            System.out.println(" vehicleUtilReport = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.vehicleUtilReport", map);
            System.out.println(" vehicleUtilReport===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "rateDiffReport List", sqlException);
        }

        return tripList;
    }
       public ArrayList rateDiffReport() {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
       
        System.out.println("map" + map);
        try {
            System.out.println(" rateDiffReport = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.rateDiffReport", map);
            System.out.println(" rateDiffReport===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "rateDiffReport List", sqlException);
        }

        return tripList;
    }

        public int updateTempCreditAmount() {

        int updateTempStatus = 0;
        Map map = new HashMap();
        ArrayList tempDetails = new ArrayList();

        tempDetails = (ArrayList) getSqlMapClientTemplate().queryForList("scheduler.getTempCreditAmount", map);
        Iterator itr = tempDetails.iterator();

        while (itr.hasNext()) {
            schTO = new SchedulerTO();
            schTO = (SchedulerTO) itr.next();

            String custId = schTO.getCustId();
            String tempId = schTO.getTempId();
            String availAmt = schTO.getAvailAmt();
            String receiptCode = schTO.getReceiptCode();
            

            System.out.println("custId   :" + custId);
            System.out.println("tempId   :" + tempId);
            System.out.println("availAmt :" + availAmt);
            System.out.println("receiptCode :" + receiptCode);

            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;

            map.put("customerId", custId);
            map.put("tempId", tempId);
            map.put("tempCrediId", tempId);
            map.put("transactionName", "TempCredit.Expired");
            map.put("transactionType", "Debit");
            map.put("creditAmount", "0");
            map.put("outStandingAmount", "0");
            map.put("totalFreightAmount", "0");
            map.put("consignmentOrderId", "0");
            map.put("custType", "1");
            map.put("userId", "1011");

            availBlockedAmount = (String) getSqlMapClientTemplate().queryForObject("operation.getAvailandBlockedAmount", map);
            System.out.println("availBlockedAmount------" + availBlockedAmount);

            availBlockedAmountTemp = availBlockedAmount.split("~");
            map.put("fixedLimit", availBlockedAmountTemp[0]);
            //map.put("availAmount", availBlockedAmountTemp[1]);
            map.put("blockedAmount", availBlockedAmountTemp[2]);
            map.put("usedLimit", availBlockedAmountTemp[3]);

            map.put("debitAmount", availAmt);
            map.put("tempAmount", availAmt);

            double avail = Double.parseDouble(availBlockedAmountTemp[1]) - Double.parseDouble(availAmt);

            int updateCreditLimit = (Integer) getSqlMapClientTemplate().update("scheduler.updateCreditLimit", map);
            System.out.println("updateCreditLimit in customer master : " + updateCreditLimit);

            map.put("availAmount", avail);

            int updateOutstandingLog = (Integer) getSqlMapClientTemplate().update("operation.updateOutstandingLog", map);
            System.out.println("updateOutstandingLog in customer master : " + updateOutstandingLog);

            updateTempStatus = (Integer) getSqlMapClientTemplate().update("scheduler.updateTempStatus", map);
            System.out.println("updateTempStatus  : " + updateTempStatus);

        }

        return updateTempStatus;

    }
        

      public int insertMailDetails(SchedulerTO schedulerTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailStatus", schedulerTO.getMailStatus());
        map.put("mailTypeId", schedulerTO.getMailTypeId());
        map.put("mailSubjectTo", schedulerTO.getMailSubjectTo());
        map.put("mailSubjectCc", schedulerTO.getMailSubjectCc());
        map.put("mailSubjectBcc", schedulerTO.getMailSubjectBcc());
        map.put("mailContentTo", schedulerTO.getEmailFormat());
        map.put("mailContentCc", schedulerTO.getEmailFormat());
        map.put("mailContentBcc", "");
        map.put("mailExcelFilePath", schedulerTO.getFilenameContent());
        map.put("mailTo", schedulerTO.getMailIdTo());
        map.put("mailCc", schedulerTO.getMailIdCc());
        map.put("mailBcc", schedulerTO.getMailIdBcc());
        map.put("userId", userId);
        System.out.println("map ###= " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("report.insertMailDetails", map);
            System.out.println("insertMailDetails###=="+insertMailDetails);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        return insertMailDetails;

    }
 

}
