// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   MrsCommand.java
package ets.domain.mrs.web;

public class MrsCommand {

    public MrsCommand() {
        mrsNumber = "";
        mrsJobCardNumber = "";
        mrsVehicleNumber = "";
        mrsCreatedDate = "";
        mrsStatus = "";
        mrsVehicleKm = "";
        mrsVehicleType = "";
        mrsVehicleUsageType = "";
        mrsVehicleMfr = "";
        mrsVehicleModel = "";
        mrsVehicleEngineNumber = "";
        mrsVehicleChassisNumber = "";
        mrsItemMfrCode = "";
        mrsPaplCode = "";
        mrsItemName = "";
        mrsItemMax = "";
        mrsItemQuantity = "";
        mrsRequestedItemNumber = "";
        mrsIssueQuantity = "";
        mrsItemId = "";
        mrsApprovalRemarks = "";
        mrsTechnician = "";
        mrsApprovalStatus = "";
        mrsOtherServiceItemSum = "";
        mrsLocalServiceItemSum = "";
        qty = "";
        mrsCompanyName = "";
        mrsItemSum = "";
        count = "";
        newStock = "";
        rcStock = "";
        poQuantity = "";
        approvedQty = "";
        uomName = "";
        totalIssued = "";
        priceId = "";
        price = "";
        rcItemId = "";
        companyId = 0;
        priceIds = "";
        rcItemIds = "";
        faultStatus = "";
        actionType = "";
        quantityRI = 0;
        itemType = "";
        mrsIds = null;
        mfrCode = "";
        itemCode = "";
        itemName = "";
        itemIds = null;
        requestedQtys = null;
        issueQtys = null;
        selectedIndex = null;
        positionIds = null;
        //Hari
        reqFor = null;
        customerName = null;
        address = null;
        mobileNo = null;
        date = null;
        remarks = null;
        mrsId = null;
        spareAmount = null;
        hike= null;
        spareRetAmount= null;
        discount= null;
        nettAmount= null;
        tax=null;
        spareTaxAmount=null;
    }



    public String getSpareTaxAmount() {
        return spareTaxAmount;
    }

    public void setSpareTaxAmount(String spareTaxAmount) {
        this.spareTaxAmount = spareTaxAmount;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getHike() {
        return hike;
    }

    public void setHike(String hike) {
        this.hike = hike;
    }

    public String getMrsId() {
        return mrsId;
    }

    public void setMrsId(String mrsId) {
        this.mrsId = mrsId;
    }

    public String getNettAmount() {
        return nettAmount;
    }

    public void setNettAmount(String nettAmount) {
        this.nettAmount = nettAmount;
    }

    public String getSpareAmount() {
        return spareAmount;
    }

    public void setSpareAmount(String spareAmount) {
        this.spareAmount = spareAmount;
    }

    public String getSpareRetAmount() {
        return spareRetAmount;
    }

    public void setSpareRetAmount(String spareRetAmount) {
        this.spareRetAmount = spareRetAmount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getReqFor() {
        return reqFor;
    }

    public void setReqFor(String reqFor) {
        this.reqFor = reqFor;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMrsCreatedDate() {
        return mrsCreatedDate;
    }

    public void setMrsCreatedDate(String mrsCreatedDate) {
        this.mrsCreatedDate = mrsCreatedDate;
    }

    public String getMrsJobCardNumber() {
        return mrsJobCardNumber;
    }

    public void setMrsJobCardNumber(String mrsJobCardNumber) {
        this.mrsJobCardNumber = mrsJobCardNumber;
    }

    public String getMrsNumber() {
        return mrsNumber;
    }

    public void setMrsNumber(String mrsNumber) {
        this.mrsNumber = mrsNumber;
    }

    public String getMrsStatus() {
        return mrsStatus;
    }

    public void setMrsStatus(String mrsStatus) {
        this.mrsStatus = mrsStatus;
    }

    public String getMrsVehicleNumber() {
        return mrsVehicleNumber;
    }

    public void setMrsVehicleNumber(String mrsVehicleNumber) {
        this.mrsVehicleNumber = mrsVehicleNumber;
    }

    public String getMrsVehicleChassisNumber() {
        return mrsVehicleChassisNumber;
    }

    public void setMrsVehicleChassisNumber(String mrsVehicleChassisNumber) {
        this.mrsVehicleChassisNumber = mrsVehicleChassisNumber;
    }

    public String getMrsVehicleEngineNumber() {
        return mrsVehicleEngineNumber;
    }

    public void setMrsVehicleEngineNumber(String mrsVehicleEngineNumber) {
        this.mrsVehicleEngineNumber = mrsVehicleEngineNumber;
    }

    public String getMrsVehicleKm() {
        return mrsVehicleKm;
    }

    public void setMrsVehicleKm(String mrsVehicleKm) {
        this.mrsVehicleKm = mrsVehicleKm;
    }

    public String getMrsVehicleMfr() {
        return mrsVehicleMfr;
    }

    public void setMrsVehicleMfr(String mrsVehicleMfr) {
        this.mrsVehicleMfr = mrsVehicleMfr;
    }

    public String getMrsVehicleModel() {
        return mrsVehicleModel;
    }

    public void setMrsVehicleModel(String mrsVehicleModel) {
        this.mrsVehicleModel = mrsVehicleModel;
    }

    public String getMrsVehicleType() {
        return mrsVehicleType;
    }

    public void setMrsVehicleType(String mrsVehicleType) {
        this.mrsVehicleType = mrsVehicleType;
    }

    public String getMrsVehicleUsageType() {
        return mrsVehicleUsageType;
    }

    public void setMrsVehicleUsageType(String mrsVehicleUsageType) {
        this.mrsVehicleUsageType = mrsVehicleUsageType;
    }

    public String getMrsItemMfrCode() {
        return mrsItemMfrCode;
    }

    public void setMrsItemMfrCode(String mrsItemMfrCode) {
        this.mrsItemMfrCode = mrsItemMfrCode;
    }

    public String getMrsItemName() {
        return mrsItemName;
    }

    public void setMrsItemName(String mrsItemName) {
        this.mrsItemName = mrsItemName;
    }

    public String getMrsPaplCode() {
        return mrsPaplCode;
    }

    public void setMrsPaplCode(String mrsPaplCode) {
        this.mrsPaplCode = mrsPaplCode;
    }

    public String getMrsItemMax() {
        return mrsItemMax;
    }

    public void setMrsItemMax(String mrsItemMax) {
        this.mrsItemMax = mrsItemMax;
    }

    public String getMrsItemQuantity() {
        return mrsItemQuantity;
    }

    public void setMrsItemQuantity(String mrsItemQuantity) {
        this.mrsItemQuantity = mrsItemQuantity;
    }

    public String getMrsRequestedItemNumber() {
        return mrsRequestedItemNumber;
    }

    public void setMrsRequestedItemNumber(String mrsRequestedItemNumber) {
        this.mrsRequestedItemNumber = mrsRequestedItemNumber;
    }

    public String getMrsIssueQuantity() {
        return mrsIssueQuantity;
    }

    public void setMrsIssueQuantity(String mrsIssueQuantity) {
        this.mrsIssueQuantity = mrsIssueQuantity;
    }

    public String getMrsItemId() {
        return mrsItemId;
    }

    public void setMrsItemId(String mrsItemId) {
        this.mrsItemId = mrsItemId;
    }

    public String getMrsApprovalRemarks() {
        return mrsApprovalRemarks;
    }

    public void setMrsApprovalRemarks(String mrsApprovalRemarks) {
        this.mrsApprovalRemarks = mrsApprovalRemarks;
    }

    public String getMrsApprovalStatus() {
        return mrsApprovalStatus;
    }

    public void setMrsApprovalStatus(String mrsApprovalStatus) {
        this.mrsApprovalStatus = mrsApprovalStatus;
    }

    public String getMrsCompanyName() {
        return mrsCompanyName;
    }

    public void setMrsCompanyName(String mrsCompanyName) {
        this.mrsCompanyName = mrsCompanyName;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String Count) {
        count = Count;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getMrsOtherServiceItemSum() {
        return mrsOtherServiceItemSum;
    }

    public void setMrsOtherServiceItemSum(String mrsOtherServiceItemSum) {
        this.mrsOtherServiceItemSum = mrsOtherServiceItemSum;
    }

    public String getMrsItemSum() {
        return mrsItemSum;
    }

    public void setMrsItemSum(String mrsItemSum) {
        this.mrsItemSum = mrsItemSum;
    }

    public String getMrsLocalServiceItemSum() {
        return mrsLocalServiceItemSum;
    }

    public void setMrsLocalServiceItemSum(String mrsLocalServiceItemSum) {
        this.mrsLocalServiceItemSum = mrsLocalServiceItemSum;
    }

    public String getNewStock() {
        return newStock;
    }

    public void setNewStock(String newStock) {
        this.newStock = newStock;
    }

    public String getRcStock() {
        return rcStock;
    }

    public void setRcStock(String rcStock) {
        this.rcStock = rcStock;
    }

    public String getPoQuantity() {
        return poQuantity;
    }

    public void setPoQuantity(String poQuantity) {
        this.poQuantity = poQuantity;
    }

    public String getApprovedQty() {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty) {
        this.approvedQty = approvedQty;
    }

    public String getTotalIssued() {
        return totalIssued;
    }

    public void setTotalIssued(String totalIssued) {
        this.totalIssued = totalIssued;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getRcItemId() {
        return rcItemId;
    }

    public void setRcItemId(String rcItemId) {
        this.rcItemId = rcItemId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getFaultStatus() {
        return faultStatus;
    }

    public void setFaultStatus(String faultStatus) {
        this.faultStatus = faultStatus;
    }

    public String getPriceIds() {
        return priceIds;
    }

    public void setPriceIds(String priceIds) {
        this.priceIds = priceIds;
    }

    public int getQuantityRI() {
        return quantityRI;
    }

    public void setQuantityRI(int quantityRI) {
        this.quantityRI = quantityRI;
    }

    public String getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String[] getMrsIds() {
        return mrsIds;
    }

    public void setMrsIds(String mrsIds[]) {
        this.mrsIds = mrsIds;
    }

    public String[] getIssueQtys() {
        return issueQtys;
    }

    public void setIssueQtys(String issueQtys[]) {
        this.issueQtys = issueQtys;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String itemIds[]) {
        this.itemIds = itemIds;
    }

    public String[] getRequestedQtys() {
        return requestedQtys;
    }

    public void setRequestedQtys(String requestedQtys[]) {
        this.requestedQtys = requestedQtys;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String selectedIndex[]) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(String positionIds[]) {
        this.positionIds = positionIds;
    }

    public String getMrsTechnician() {
        return mrsTechnician;
    }

    public void setMrsTechnician(String mrsTechnician) {
        this.mrsTechnician = mrsTechnician;
    }
    private String mrsNumber;
    private String mrsJobCardNumber;
    private String mrsVehicleNumber;
    private String mrsCreatedDate;
    private String mrsStatus;
    private String mrsVehicleKm;
    private String mrsVehicleType;
    private String mrsVehicleUsageType;
    private String mrsVehicleMfr;
    private String mrsVehicleModel;
    private String mrsVehicleEngineNumber;
    private String mrsVehicleChassisNumber;
    private String mrsItemMfrCode;
    private String mrsPaplCode;
    private String mrsItemName;
    private String mrsItemMax;
    private String mrsItemQuantity;
    private String mrsRequestedItemNumber;
    private String mrsIssueQuantity;
    private String mrsItemId;
    private String mrsApprovalRemarks;
    private String mrsTechnician;
    private String mrsApprovalStatus;
    private String mrsOtherServiceItemSum;
    private String mrsLocalServiceItemSum;
    private String qty;
    private String mrsCompanyName;
    private String mrsItemSum;
    private String count;
    private String newStock;
    private String rcStock;
    private String poQuantity;
    private String approvedQty;
    private String uomName;
    private String totalIssued;
    private String priceId;
    private String price;
    private String rcItemId;
    private int companyId;
    private String priceIds;
    private String rcItemIds;
    private String faultStatus;
    private String actionType;
    private int quantityRI;
    private String itemType;
    private String mrsIds[];
    private String mfrCode;
    private String itemCode;
    private String itemName;
    private String itemIds[];
    private String requestedQtys[];
    private String issueQtys[];
    private String selectedIndex[];
    private String positionIds[];
    //Hari Start
    private String reqFor;
    private String customerName;
    private String address;
    private String mobileNo;
    private String date;
    private String remarks;
    private String mrsId;
    private String spareAmount;
    private String hike;
    private String spareRetAmount;
    private String discount;
    private String nettAmount;
    private String tax;
    private String spareTaxAmount;
}
