
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Billing</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Billing</a></li>
            <li class="active">Receipt Print</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="enter" method="post">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String startDate = sdf.format(today);
                    %>
                    <div id="printContent" >
                        <table align="center"  style="-webkit-print-color-adjust: exact;border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                               background:url(images/dict-logoWatermark.png);
                               /*background-repeat:no-repeat;*/
                               background-position:center;
                               border:1px solid;
                               /*background-color: white;*/
                               /*border-color:#CD853F;width:800px;*/
                               border:1px solid;
                               /*opacity:0.8;*/
                               font-weight:bold;">

                            <tr>
                                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                                    <table align="left" >
                                        <tr>
                                            <td colspan="2">
                                                <font size="2">PAN NO-AACCB8054G </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">GST No : 06AACCB8054G1ZT</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">CIN NO-U63040MH2006PTC159885</font><br>
                                            </td>

                                        </tr>
                                        <tr>
                                            <!--<td align="left"><img src="images/GatiLogo.png" width="100" height="50"/></td>-->
                                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                            <td style="padding-left: 60px; padding-right:50px;">


                                        <font size="3"><center><b><u>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</u></b></center></font><br>
                                        <center> <font size="2" > <u>Receipt</u> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</font></center>

                                </td>
                                <td></td>
                            </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; " align='center'>
                              
                                <table align='center' border="1" width="100%">
                                    <tr align='center'>
                                        <td align='center'>Date</td>
                                        <td align='center'><c:out value="${paymentDate}"/></td>
                                        
                                    </tr>
                                  
                                    <tr>
                                        <td align='center'>Customer</td>
                                        <td align='center'><c:out value="${customerName}"/></td>
                                        
                                    </tr>
                                    <tr>
                                        <td align='center'>Receipt Code</td>
                                        <td align='center'><c:out value="${receiptCode}"/></td>
                                        
                                    </tr>
                                    <tr>
                                        <td align='center'>Payment Mode</td>
                                        <td align='center'><c:out value="${paymentMode}"/></td>
                                        
                                    </tr>
                                    <tr>
                                        <td align='center'>RTGS/Cheque/DD no</td>
                                        <td align='center'><c:out value="${voucherNo}"/></td>
                                        
                                    </tr>
                                    <tr>
                                        <td align='center'>Bank</td>
                                        <td align='center'><c:out value="${bankName}"/></td>
                                        
                                    </tr>

                                    <tr>
                                        <td align='center'>Amount</td>
                                        <td align='center'><c:out value="${amount}"/> </td>
                                       
                                    </tr>

                                </table>
                            </td></tr>
                                <br>
                                </table>
                    </div>
                    <br>
                    <br>
                    <center><input type="button" class="btn btn-info"  value="Print" onClick="print('printContent');" style="width:90px;height:30px;"></center>
                    <script type="text/javascript">
                        function print(val)
                        {
                            var DocumentContainer = document.getElementById(val);
                            var WindowObject = window.open('', "TrackHistoryData",
                                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            WindowObject.close();
                        }
                    </script>
                 <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
