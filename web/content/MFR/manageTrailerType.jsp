
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/handleViewTrailerTypeAddPage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/handleViewTrailerTypeAlterPage.do';
            }
            document.desigDetail.submit();
        }
    </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="operations.label.Trailer"  text="Trailer"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.Trailer"  text="Trailer"/></a></li>
          <li class="active"><spring:message code="operations.label.Trailer"  text="Trailer"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        
        <form method="post" name="desigDetail">


           
 <% int index = 0;  %>    
            
            <c:if test = "${TypeList != null}" >
                <%@ include file="/content/common/message.jsp" %>
                <table class="table table-info mb30 table-hover">
                    <thead>
                   
                    <tr >
                        <th><spring:message code="trailers.label.SNo" text="default text"/></th>
                        <th><spring:message code="trailers.label.TrailerType" text="default text"/></th>
                        <th><spring:message code="trailers.label.TrailerTonnage" text="default text"/></th>
                        <th><spring:message code="trailers.label.TrailerCapacity(CBM)" text="default text"/></th>
                        <th><spring:message code="trailers.label.Description" text="default text"/></th>
                        <th><spring:message code="trailers.label.Status" text="default text"/></th>
                       
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${TypeList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="420" height="20" >
                            <td><%=index + 1%></td>
                            <td><input type="hidden" name="typeId" value='<c:out value="${list.typeId}"/>'> <c:out value="${list.typeName}"/></td>
                            <td>&nbsp;&nbsp;<c:out value="${list.tonnage}"/></td>
                            <td><c:out value="${list.capacity}"/></td>
                            <td><c:out value="${list.description}"/></td>
                            <td>                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                   <spring:message code="trailers.label.Active" text="default text"/> 
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    <spring:message code="trailers.label.InActive" text="default text"/>
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    </td>
</table>
                    
            </c:if> 
            <center>
                <input type="button" name="add" class="btn btn-success" value="<spring:message code="trailers.label.ADD" text="default text"/>" onClick="submitPage(this.name)" class="button">
                <c:if test = "${TypeList != null}" >
                    <input type="button" class="btn btn-success" value="<spring:message code="trailers.label.ALTER" text="default text"/>" name="modify" onClick="submitPage(this.name)" class="button">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>