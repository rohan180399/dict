<%-- 
    Document   : tripPlanningEdit
    Created on : Nov 4, 2013, 11:33:44 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    </head>
    <script>
        function submitWindow(){
            document.tripPlanning.action = '/throttle/BrattleFoods/viewTripPlanning.jsp';
            document.tripPlanning.submit();
        }
    </script>
    <body>
        <% String menuPath = "Operations >>  Edit Trip Planning";
                    request.setAttribute("menuPath", menuPath);
        %>
        <form name="tripPlanning"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trip Planning</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>


                                        <td width="80" height="30"><font color="red">*</font>From Date</td>
                                        <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                                            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuleprice.fromDate,'dd-mm-yyyy',this)"/></span></td>
                                        <td width="80" height="30"><font color="red">*</font>To Date</td>
                                        <td width="182"><input name="toDate" type="text" class="form-control" value="" size="20">
                                            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuleprice.toDate,'dd-mm-yyyy',this)"/></span></td>

                                        <td><input type="button"   value="Search" class="button" name="search" onClick="submitWindow()"></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
        <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
            <tr>
                <td class="contenthead" colspan="4" >Customer Details</td>
            </tr>
            <tr>
                <td class="text1">Name</td>
                <td class="text1"><input name="customerName" type="text" class="form-control"  id="customerName" value="Freight Systems" readonly="" /></td>
                <td class="text1">Code</td>
                <td class="text1"><input name="customerCode" type="text" class="form-control"  id="customerCode" value="CD001" readonly="" /></td>
            </tr>
            <tr>
                <td class="text2">Address</td>
                <td class="text2"><textarea rows="1" cols="16" readonly>Delhi</textarea></td>
                <td class="text2">Pincode</td>
                <td class="text2"><input name="pincode" type="text"  class="form-control"  id="pincode" readonly="" value="110001"/></td>
            </tr>
            <tr>
                <td class="text1">Mobile No</td>
                <td class="text1"><input name="mobileNo" type="text"  class="form-control"  id="mobileNo" value="9994564561" readonly="" /></td>
                <td class="text1">E-Mail ID</td>
                <td class="text1"><input name="mailId" type="text" class="form-control"  id="mailId" value="laxman@brattlefoods.com" readonly=""/></td>
            </tr>
            <tr>
                <td class="text2">Phone No</td>
                <td class="text2"><input name="phoneNo" type="text"  class="form-control" maxlength="10"   id="phoneNo" value="9994564561" readonly=""  /></td>
                <td class="text2" colspan="2">&nbsp;</td>
            </tr>

        </table>
        <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">

            <tr>
                <td class="contenthead" colspan="6" >Consignment Details</td>
            </tr>
            <tr>
                <td class="text1">Origin</td>
                <td class="text1"><select id="origin" name="origin" onchange='originDetais()'> <option value="0">-Select-</option>
                        <option value='Balwal' selected=""> Balwal</option>
                        <option value='Chandigarh' >Chandigarh</option>
                        <option value='Chennai' >Chennai</option>
                        <option value='Delhi' >Delhi</option>
                    </select></td>
                <td class="text1">Destination</td>
                <td class="text1"><select class="form-control" id="destination" name="destination" onchange='destinationDetais()'> <option value="0">-Select-</option>
                        <option value='Balwal' > Balwal</option>
                        <option value='Chandigarh' >Chandigarh</option>
                        <option value='Chennai' >Chennai</option>
                        <option value='Delhi' selected="">Delhi</option>
                    </select></td>
                <td class="text1">Business Type</td>
                <td class="text1">
                    <select name="businessType" class="form-control"  id="businessType" style="width:120px;">
                        <option value="0"> --Select-- </option>
                        <option value="1" selected=""> Primary  </option>
                        <option value="2"> Secondary  </option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="text2">Multi Pickup</td>
                <td class="text2"><input type="checkbox" class="form-control" name="multiPickup" id="multiPickup" onclick="multipickupShow()" checked=""></td>
                <td class="text2">Multi Delivery</td>
                <td class="text2"><input type="checkbox" class="form-control" name="multiDelivery" id="multiDelivery" checked=""></td>
                <td class="text2">Special Instruction</td>
                <td class="text2"><textarea rows="1" cols="16">Food Items</textarea></td>
            </tr>
        </table>
        <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">

            <tr>
                <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
            </tr>

            <tr>
                <td class="text2">Vehicle No</td>
                <td class="text2"><input type="textbox" class="datepicker" name="vehicleNo" value="DL 18 J 2345"/></td>
                <td class="text2">CNote No</td>
                <td class="text2"><input type="textbox" class="datepicker" name="vehicleNo" value="10,11"/></td>
                <td class="text2" colspan="2">&nbsp;</td>
               
            </tr>
            <tr>
                <td class="text1">Service Type</td>
                <td class="text1">
                    <select name="serviceType" class="form-control"  id="paymentType" style="width:120px;">
                        <option value="0"> -Select- </option>
                        <option value="1" selected=""> FTL </option>
                        <option value="2"> LTL </option>
                    </select>
                </td>
                <td class="text1">Vehicle Type</td>
                <td class="text1"> <select name="paymentType" class="form-control"  id="paymentType" style="width:120px;">
                        <option value="0"> -Select- </option>
                        <option value="1" selected=""> Ashok Leyland/2516 </option>
                        <option value="2"> Tata/3118 </option>
                    </select></td>
                <td class="text1">Reefer Required</td>
                <td class="text1">
                    <select name="paymentType" class="form-control"  id="paymentType" style="width:120px;">
                        <option value="0" selected=""> Yes </option>
                        <option value="1"> No </option>
                    </select>
                </td>
            </tr>
            <tr>

                <td class="text2">Vehicle Required Date</td>
                <td class="text2"><input type="textbox" class="datepicker" name="vehcleRequiredDate" id="vehcleRequiredDate" onchange='selectVehicleStDate(this.value)' value="06-11-2013"></td>
            <td class="text2">Vehicle Required Time</td>
            <td class="text2"><select id='vehicleRequiredTime' onchange='selectVehicleStTime(this.value)'>
                    <option value="06:00">6:00 AM</option>
                    <option value="06:30">6:30 AM</option>
                    <option value="07:00">7:00 AM</option>
                    <option value="07:30">7:30 AM</option>
                    <option value="08:00">8:00 AM</option>
                    <option value="08:30">8:30 AM</option>
                    <option value="09:00"  selected>9:00 AM</option>
                    <option value="09:30">9:30 AM</option>
                    <option value="10:00">10:00 AM</option>
                    <option value="10:30">10:30 AM</option>
                    <option value="11:00">11:00 AM</option>
                    <option value="11:30">11:30 AM</option>
                    <option value="12:00">12:00 PM</option>
                    <option value="12:30">12:30 PM</option>
                    <option value="13:00">1:00 PM</option>
                    <option value="13:30">1:30 PM</option>
                    <option value="14:00">2:00 PM</option>
                    <option value="14:30">2:30 PM</option>
                    <option value="15:00">3:00 PM</option>
                    <option value="15:30">3:30 PM</option>
                    <option value="16:00">4:00 PM</option>
                    <option value="16:30">4:30 PM</option>
                    <option value="17:00">5:00 PM</option>
                    <option value="17:30">5:30 PM</option>
                    <option value="18:00">6:00 PM</option>
                    <option value="18:30">6:30 PM</option>
                    <option value="19:00">7:00 PM</option>
                    <option value="19:30">7:30 PM</option>
                    <option value="20:00">8:00 PM</option>
                    <option value="20:30">8:30 PM</option>
                    <option value="21:00">9:00 PM</option>
                    <option value="21:30">9:30 PM</option>
                    <option value="22:00">10:00 PM</option>
                </select></td>
            <td class="text2">Special Instruction</td>
            <td class="text2"><textarea rows="1" cols="16">Food Items</textarea></td>
        </tr>
    </table>
    <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
        <tr>
            <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
        </tr>
        <tr>
            <td class="text2">Consignor Name</td>
            <td class="text2"><input type="text" class="form-control" id="consignorName" name="consignorName" value="Baker Ex "></td>
            <td class="text2">Mobile No</td>
            <td class="text2"><input type="text" class="form-control" id="phoneNo" name="phoneNo" value="9993452546" maxlength="10"></td>
            <td class="text2">Address</td>
            <td class="text2"><textarea rows="1" cols="16"></textarea>Kashipur </td>
        </tr>
    </table>
    <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
        <tr>
            <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
        </tr>
        <tr>
            <td class="text2">Consignee Name</td>
            <td class="text2"><input type="text" class="form-control" id="consignorName" name="consignorName" value="Baker Ex "></td>
            <td class="text2">Mobile No</td>
            <td class="text2"><input type="text" class="form-control" id="phoneNo" name="phoneNo" value="9993452546" maxlength="10"></td>
            <td class="text2">Address</td>
            <td class="text2"><textarea rows="1" cols="16"></textarea>Delhi </td>
        </tr>
    </table>
    <br/>
    <br/>
<center>
    <input type="button" class="button" value="Update" name="Update" onclick="submitWindow()">
</center>
</body>
</html>
