<%-- 
    Document   : containerMovementReportExcel
    Created on : May 11, 2016, 1:07:16 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >

                        <th><h3 align="center">Empty</h3></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Loni</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Dadri & TKD</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>DICT</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Total in TEU's</h3></center></th>
                        <th><h3 align="center">Total in TEU's</h3></th>
                    </tr>
                    <tr>
                        <th style="border-left:1px solid white"><h3>Transporter</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3></h3></th>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:forEach items="${containerEmptyMovementList}" var="emptyList">
                        <c:set var="emptydictTotaltwentyftTEUs" value="${emptyList.ownloniemptytwentyft + emptyList.owndadriTKDemptytwentyft + emptyList.owndictemptytwentyft}"></c:set>
                        <c:set var="emptydictTotalFourtyftTEUs" value="${emptyList.ownloniemptyfourtyft + emptyList.owndadriTKDemptyfourtyft + emptyList.owndictemptyfourtyft}"></c:set>
                        <c:set var="emptydictTotalTEUs" value="${emptydictTotaltwentyftTEUs + emptydictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.ownloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.ownloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.owndictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptydictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="emptyleaseTotaltwentyftTEUs" value="${emptyList.leasedloniemptytwentyft + emptyList.leaseddadriTKDemptytwentyft + emptyList.leaseddictemptytwentyft}"></c:set>
                        <c:set var="emptyleaseTotalFourtyftTEUs" value="${emptyList.leasedloniemptyfourtyft + emptyList.leaseddadriTKDemptyfourtyft + emptyList.leaseddictemptyfourtyft}"></c:set>
                        <c:set var="emptyleaseTotalTEUs" value="${emptyleaseTotaltwentyftTEUs + emptyleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.leasedloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leasedloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.leaseddictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="emptyOtherTotaltwentyftTEUs" value="${emptyList.otherloniemptytwentyft + emptyList.otherdadriTKDemptytwentyft + emptyList.otherdictemptytwentyft}"></c:set>
                        <c:set var="emptyOtherTotalFourtyftTEUs" value="${emptyList.otherloniemptyfourtyft + emptyList.otherdadriTKDemptyfourtyft + emptyList.otherdictemptyfourtyft}"></c:set>
                        <c:set var="emptyOtherTotalTEUs" value="${emptyOtherTotaltwentyftTEUs + emptyOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherloniemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherloniemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdictemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyList.otherdictemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${emptyOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="loniTotalemptytwentyft" value="${emptyList.ownloniemptytwentyft + emptyList.leasedloniemptytwentyft + emptyList.otherloniemptytwentyft}"></c:set>
                        <c:set var="loniTotalemptyfourtyft" value="${emptyList.ownloniemptyfourtyft + emptyList.leasedloniemptyfourtyft + emptyList.otherloniemptyfourtyft}"></c:set>
                        <c:set var="dadriTKDemptytwentyft" value="${emptyList.owndadriTKDemptytwentyft + emptyList.leaseddadriTKDemptytwentyft + emptyList.otherdadriTKDemptytwentyft}"></c:set>
                        <c:set var="dadriTKDemptyfourtyft" value="${emptyList.owndadriTKDemptyfourtyft + emptyList.leaseddadriTKDemptyfourtyft + emptyList.otherdadriTKDemptyfourtyft}"></c:set>
                        <c:set var="dictTotalemptytwentyft" value="${emptyList.owndictemptytwentyft + emptyList.leaseddictemptytwentyft + emptyList.otherdictemptytwentyft}"></c:set>    
                        <c:set var="dictTotalemptyfourtyft" value="${emptyList.owndictemptyfourtyft + emptyList.leaseddictemptyfourtyft + emptyList.otherdictemptyfourtyft}"></c:set>    
                        <c:set var="TotalemptytwentyftTEU" value="${emptydictTotaltwentyftTEUs + emptyleaseTotaltwentyftTEUs + emptyOtherTotaltwentyftTEUs}"></c:set>    
                        <c:set var="TotalemptyfourtyftTEU" value="${emptydictTotalFourtyftTEUs + emptyleaseTotalFourtyftTEUs + emptyOtherTotalFourtyftTEUs}"></c:set>    
                        <c:set var="TotalemptyTEU" value="${ TotalemptytwentyftTEU + TotalemptyfourtyftTEU }"></c:set>    
                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${loniTotalemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${loniTotalemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalemptytwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalemptyfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptytwentyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptyfourtyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptyTEU}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br>
            <br>
            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >
                        <th><h3 align="center">Export</h3></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Loni</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Dadri & TKD</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>DICT</h3></center></th>
                        <th colspan="2" align="center" style="border-left:1px solid white"><center><h3>Total in TEU's</h3></center></th>
                        <th><h3 align="center">Total in TEU's</h3></th>
                    </tr>
                    <tr>
                        <th style="border-left:1px solid white"><h3>Transporter</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>

                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>20'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3>40'</h3></th>
                        <th style="border-left:1px solid white;border-top:1px solid white"><h3></h3></th>
                </thead>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:forEach items="${containerExportMovementList}" var="exportList">
                        <c:set var="exportdictTotaltwentyftTEUs" value="${exportList.ownloniexporttwentyft + exportList.owndadriTKDexporttwentyft + exportList.owndictexporttwentyft}"></c:set>
                        <c:set var="exportdictTotalFourtyftTEUs" value="${exportList.ownloniexportfourtyft + exportList.owndadriTKDexportfourtyft + exportList.owndictexportfourtyft}"></c:set>
                        <c:set var="exportdictTotalTEUs" value="${exportdictTotaltwentyftTEUs + exportdictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.ownloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.ownloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.owndictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportdictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="exportleaseTotaltwentyftTEUs" value="${exportList.leasedloniexporttwentyft + exportList.leaseddadriTKDexporttwentyft + exportList.leaseddictexporttwentyft}"></c:set>
                        <c:set var="exportleaseTotalFourtyftTEUs" value="${exportList.leasedloniexportfourtyft + exportList.leaseddadriTKDexportfourtyft + exportList.leaseddictexportfourtyft}"></c:set>
                        <c:set var="exportleaseTotalTEUs" value="${exportleaseTotaltwentyftTEUs + exportleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.leasedloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leasedloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.leaseddictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="exportOtherTotaltwentyftTEUs" value="${exportList.otherloniexporttwentyft + exportList.otherdadriTKDexporttwentyft + exportList.otherdictexporttwentyft}"></c:set>
                        <c:set var="exportOtherTotalFourtyftTEUs" value="${exportList.otherloniexportfourtyft + exportList.otherdadriTKDexportfourtyft + exportList.otherdictexportfourtyft}"></c:set>
                        <c:set var="exportOtherTotalTEUs" value="${exportOtherTotaltwentyftTEUs + exportOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${exportList.otherloniexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherloniexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdictexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportList.otherdictexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotaltwentyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotalFourtyftTEUs}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${exportOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="loniTotalexporttwentyft" value="${exportList.ownloniexporttwentyft + exportList.leasedloniexporttwentyft + exportList.otherloniexporttwentyft}"></c:set>
                        <c:set var="loniTotalexportfourtyft" value="${exportList.ownloniexportfourtyft + exportList.leasedloniexportfourtyft + exportList.otherloniexportfourtyft}"></c:set>
                        <c:set var="dadriTKDexporttwentyft" value="${exportList.owndadriTKDexporttwentyft + exportList.leaseddadriTKDexporttwentyft + exportList.otherdadriTKDexporttwentyft}"></c:set>
                        <c:set var="dadriTKDexportfourtyft" value="${exportList.owndadriTKDexportfourtyft + exportList.leaseddadriTKDexportfourtyft + exportList.otherdadriTKDexportfourtyft}"></c:set>
                        <c:set var="dictTotalexporttwentyft" value="${exportList.owndictexporttwentyft + exportList.leaseddictexporttwentyft + exportList.otherdictexporttwentyft}"></c:set>    
                        <c:set var="dictTotalexportfourtyft" value="${exportList.owndictexportfourtyft + exportList.leaseddictexportfourtyft + exportList.otherdictexportfourtyft}"></c:set>    
                        <c:set var="TotalexporttwentyftTEU" value="${exportdictTotaltwentyftTEUs + exportleaseTotaltwentyftTEUs + exportOtherTotaltwentyftTEUs}"></c:set>    
                        <c:set var="TotalexportfourtyftTEU" value="${exportdictTotalFourtyftTEUs + exportleaseTotalFourtyftTEUs + exportOtherTotalFourtyftTEUs}"></c:set>    
                        <c:set var="TotalexportTEU" value="${ TotalexporttwentyftTEU + TotalexportfourtyftTEU }"></c:set>  
                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${loniTotalexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${loniTotalexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dadriTKDexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalexporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalexportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexporttwentyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexportfourtyftTEU}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexportTEU}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
<br>
<br>
            <table border="" style="border:none #666666;width:500px;padding-left: 75px;"  align="left"  cellpadding="0" cellspacing="1" >
                <tr height="25" align="right">

                    <td align="center" style="background-color: #6374AB; color: #ffffff">Import </td>
                    <th colspan="2" align="center" style="background-color: #6374AB; color: #ffffff"><center>DICT</center></th>
                    <td align="center" style="background-color: #6374AB; color: #ffffff">Total in TEU's</td>
                </tr>

                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Transporter</td>
                    <th style="background-color: #6374AB; color: #ffffff">20'</th>
                    <th style="background-color: #6374AB; color: #ffffff">40'</th>
                    <td></td>
                </tr>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:forEach items="${containerImportMovementList}" var="importList">
                        <c:set var="importdictTotaltwentyftTEUs" value="${importList.owndictImporttwentyft}"></c:set>
                        <c:set var="importdictTotalFourtyftTEUs" value="${importList.owndictImportfourtyft}"></c:set>
                        <c:set var="importdictTotalTEUs" value="${importdictTotaltwentyftTEUs + importdictTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">DICT</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.owndictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.owndictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importdictTotalTEUs}"/></td>
                        </tr>

                        <c:set var="importleaseTotaltwentyftTEUs" value="${importList.leaseddictImporttwentyft}"></c:set>
                        <c:set var="importleaseTotalFourtyftTEUs" value="${importList.leaseddictImportfourtyft}"></c:set>
                        <c:set var="importleaseTotalTEUs" value="${importleaseTotaltwentyftTEUs + importleaseTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Leased</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.leaseddictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importleaseTotalTEUs}"/></td>
                        </tr>
                        <c:set var="importOtherTotaltwentyftTEUs" value="${importList.otherdictImporttwentyft}"></c:set>
                        <c:set var="importOtherTotalFourtyftTEUs" value="${importList.otherdictImportfourtyft}"></c:set>
                        <c:set var="importOtherTotalTEUs" value="${importOtherTotaltwentyftTEUs + importOtherTotalFourtyftTEUs }"></c:set>
                            <tr height="30">
                                <td align="left">Others</td>
                                <td align="center" class="<%=classText%>"><c:out value="${importList.otherdictImporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importList.otherdictImportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${importOtherTotalTEUs}"/></td>
                        </tr>
                        <c:set var="dictTotalimporttwentyft" value="${importList.owndictImporttwentyft + importList.leaseddictImporttwentyft + importList.otherdictImporttwentyft}"></c:set>
                        <c:set var="dictTotalimportfourtyft" value="${importList.owndictImportfourtyft + importList.leaseddictImportfourtyft + importList.otherdictImportfourtyft}"></c:set>
                        <c:set var="TotalimportTEU" value="${dictTotalimporttwentyft + dictTotalimportfourtyft}"></c:set>

                            <tr height="30">
                                <td align="left"><b>Total</b></td>
                                <td align="center" class="<%=classText%>"><c:out value="${dictTotalimporttwentyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalimportfourtyft}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalimportTEU}"/></td>
                        </tr>
                    </c:forEach>
                </tbody>

            </table>
            <br>
            <br>
            <table border="" style="border:none #666666;width:500px;clear:left;padding-top:20px "  align="center"  cellpadding="0" cellspacing="1" >

                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Summary</td>
                    <th style="background-color: #6374AB; color: #ffffff">20'</th>
                    <th style="background-color: #6374AB; color: #ffffff">40'</th>
                    <td style="background-color: #6374AB; color: #ffffff">Total In TEU's</td>
                </tr>
                <tbody>
                    <% // int index = 1;%>
                    <%
//                        String classText = "";
//                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:set var="TotalEmptyTEU" value="${TotalemptytwentyftTEU + TotalemptyfourtyftTEU}"></c:set>
                        <tr height="30">
                            <td align="left">Total Empty</td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalemptytwentyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalemptyfourtyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalEmptyTEU}"/></td>
                    </tr>

                    <c:set var="TotalExportTEU" value="${TotalexporttwentyftTEU + TotalexportfourtyftTEU}"></c:set>
                        <tr height="30">
                            <td align="left">Total Export</td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotalexporttwentyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalexportfourtyftTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalExportTEU}"/></td>
                    </tr>

                    <c:set var="TotalImportTEU" value="${dictTotalimporttwentyft + dictTotalimportfourtyft}"></c:set>
                        <tr height="30">
                            <td align="left">Total Import/DSO</td>
                            <td align="center" class="<%=classText%>"><c:out value="${dictTotalimporttwentyft}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${dictTotalimportfourtyft}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalImportTEU}"/></td>
                    </tr>

                    <c:set var="TotaltwentyTEU" value="${TotalemptytwentyftTEU + TotalexporttwentyftTEU + dictTotalimporttwentyft}"></c:set>
                    <c:set var="TotalfourtyTEU" value="${TotalemptyfourtyftTEU + TotalexportfourtyftTEU + dictTotalimportfourtyft}"></c:set>
                    <c:set var="TotalTEUs" value="${TotaltwentyTEU + TotalfourtyTEU}"></c:set>
                        <tr height="30">
                            <td align="left"><b>Total</b></td>
                            <td align="center" class="<%=classText%>"><c:out value="${TotaltwentyTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalfourtyTEU}"/></td>
                        <td align="center" class="<%=classText%>"><c:out value="${TotalTEUs}"/></td>
                    </tr>
                </tbody>

            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
