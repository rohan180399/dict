
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
>
 <%@ page import="java.text.DecimalFormat" %>
             <%@ page import="java.text.NumberFormat" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
          
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function () {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function savePage(value) {
                document.trialBalance.action = '/throttle/dayBookSummary.do?param=' + value;
                document.trialBalance.submit();
            }


        </script>
        
        <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageFinancialYear"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageFinancialYear"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        
        
    <body>
        <form name="trialBalance" method="post" action="" >
         
            <%                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String fDate = "";
                String tDate = "";
                if (request.getAttribute("fromDate") != null) {
                    fDate = (String) request.getAttribute("fromDate");
                } else {
                    fDate = "01-04-2016";
                }
                if (request.getAttribute("toDate") != null) {
                    tDate = (String) request.getAttribute("toDate");
                } else {
                    tDate = dateFormat.format(date);
                }

            %>
            <center>
                <h2><%=ThrottleConstants.companyName%></h2>
                 <table class="table table-info mb30 table-hover">
                     <thead>
                         <tr height="30" id="index">
                             <th colspan="4"  ><b><spring:message code="finance.label.DayBookSummary"  text="default text"/></b></th>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                     </thead>
                <br>
                <!--<table>-->
                    <tr>
                        <td><spring:message code="finance.label.FromDate"  text="default text"/>:</td>
                        <td><input type="text" class='datepicker' value="<%=fDate%>" name="fromDate" style="height:40px;width: 260px;"/></td>
                        <td><spring:message code="finance.label.ToDate"  text="default text"/>:</td>
                        <td><input type="text" class='datepicker' value="<%=tDate%>" name="toDate" style="height:40px;width: 260px;"/></td>
                    </tr>
                </table>
                <br>
                <input type="button" id="saveButton" class="btn btn-success" value="<spring:message code="finance.label.FetchData"  text="default text"/>" name="search" onclick="savePage(this.name);" style="width:100px;height:35px;"/>
                <input type="button" class="btn btn-success" onclick="savePage(this.name);" name="export" value="<spring:message code="finance.label.Export"  text="default text"/>" style="width:100px;height:35px;">
            </center>
            <br>
            <table class="table table-info mb30 table-hover"   >
                <thead>
                <tr  id="index" height="30">
                    <th  align="left" width="60%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.ACCOUNTHEAD"  text="default text"/></b></th>
                    <th  align="right" width="20%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.DEBITS"  text="default text"/></b></th>
                    <th  align="right" width="20%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.CREDITS"  text="default text"/></b></th>
                </tr>
                </thead>
                <% int index = 0;%>
                <% int sno = 0;%>
                <c:if test = "${voucherMasterList != null}" >
                    <c:forEach items="${voucherMasterList}" var="VL">
                        <c:if test = "${ledgerTransactionList != null}" >
                            <c:set var="formId" value=""/>
                            <c:set var="formDebitAmmount" value="${0.00}"/>
                            <c:set var="formCreditAmmount" value="${0.00}"/>
                            <c:set var="formName" value=""/>
                            <c:forEach items="${ledgerTransactionList}" var="LL">
                                <c:if test = "${VL.formId == LL.formId}" >
                                     <c:set var="formDebitAmmount" value="${formDebitAmmount + LL.debitAmmount}"/> 
                                     <c:set var="formCreditAmmount" value="${formCreditAmmount + LL.creditAmmount}"/> 
                                      <c:if test="${formId != LL.formId}">
                                    <tr>
                                        <td  width="60%" style="color: #0464BB; text-align: left; font-weight: bold; font-size: small;">
                                          &emsp;&emsp;
                                                <c:if test="${LL.formId == '3'}"> <c:set var="formName" value="CASH RECEIPT TOTAL"/>
                                                    <spring:message code="finance.label.CASHRECEIPT"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '1'}"> <c:set var="formName" value="CASH PAYMENT TOTAL"/>
                                                    <spring:message code="finance.label.CASHPAYMENT"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '2'}"> <c:set var="formName" value="BANK PAYMENT TOTAL"/>
                                                    <spring:message code="finance.label.BANKPAYMENT"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '4'}"> <c:set var="formName" value="BANK RECEIPT TOTAL"/>
                                                    <spring:message code="finance.label.BANKRECEIPT"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '5'}"> <c:set var="formName" value="CONTRA TOTAL"/>
                                                    <spring:message code="finance.label.CONTRA"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '6'}"> <c:set var="formName" value="JOURNAL TOTAL"/>
                                                    <spring:message code="finance.label.JOURNAL"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '7'}"> <c:set var="formName" value="PRIVATE LORRY HIRE CHARGES PAID TOTAL"/>
                                                     <spring:message code="finance.label.PRIVATELORRYHIRECHARGESPAID"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '8'}"> <c:set var="formName" value="PLHP TOTAL"/>
                                                    <spring:message code="finance.label.PRIVATELORRYHIREPAYABLE"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '9'}"> <c:set var="formName" value="FRV TOTAL"/>
                                                    <spring:message code="finance.label.FRV"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '10'}"> <c:set var="formName" value="MLHR TOTAL"/>
                                                    <spring:message code="finance.label.MLHR"  text="default text"/>
                                                </c:if>
                                                <c:if test="${LL.formId == '11'}"> <c:set var="formName" value="TSV TOTAL"/>
                                                    <spring:message code="finance.label.TSV"  text="default text"/>
                                                </c:if>
                                                <% sno++;%>
                                        </td>
                                        <td  align ="right" width="20%">&nbsp;&nbsp;&nbsp; </td>
                                        <td  align ="right" width="20%">&nbsp;&nbsp;&nbsp; </td>

                                    </tr>
                                     </c:if>
                                    <tr>
                                        <td  width="60%">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <c:out value="${LL.ledgerName}" />&nbsp; A/C </td>
                                        <td  align ="right" width="20%">&nbsp;&nbsp;&nbsp; <c:out value="${LL.debitAmmount}" /></td>
                                        <td  align ="right" width="20%">&nbsp;&nbsp;&nbsp; <c:out value="${LL.creditAmmount}" /></td>

                                    </tr>
                                    <c:set var="formId" value="${LL.formId}"/>
                                    <% index++;%>
                                </c:if>
                            </c:forEach>
                                     <c:if test = "${formName != ''}" >
                                    <tr>
                                        <td  width="60%" align ="right" style="color: #0464BB; text-align: right; font-weight: bold; font-size: small;">
                                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${formName}" />
                                     </td>
                                        <td  width="20%" align ="right" style="color: #0464BB; text-align: right; font-weight: bold; font-size: small;">
                                            &nbsp;&nbsp;&nbsp;<fmt:formatNumber type="number" pattern="##0.00" groupingUsed="false" value="${formDebitAmmount}" />
                                        </td>
                                        <td  width="20%" align ="right" style="color: #0464BB; text-align: right; font-weight: bold; font-size: small;">
                                             &nbsp;&nbsp;&nbsp;<fmt:formatNumber type="number" pattern="##0.00" groupingUsed="false" value="${formCreditAmmount}" />
                                        </td>

                                    </tr>
                                    </c:if>
                        </c:if>
                    </c:forEach>
                </c:if>

            </table>

            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>