

<html>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    
    <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

    <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %> 
    <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %> 
    <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %> 
       

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>    
    <%@ page import="java.util.*" %>

    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <title>Job Card Bill</title>

</head>
    
    



    
    
<script>



    function print(val)
    {       
        var DocumentContainer = document.getElementById(val);        
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();   
    }            


</script>


<body>    

<% 

int itemNameLength = 35,uomLength=10;
JobCardItemTO itemTO=null;
ProblemActivitiesTO activityTO = null;
int pageSize = 24;
String[] activity = null;
float discountVal = 0.00f;
ArrayList billRecord = (ArrayList) request.getAttribute("billRecord"); 
ArrayList billHeaderInfo = (ArrayList) request.getAttribute("billHeaderInfo");
    Iterator itr = billHeaderInfo.iterator();
    RenderServiceTO jcto =null;
    while(itr.hasNext()) {
        jcto = (RenderServiceTO)itr.next();
        discountVal = Float.parseFloat(jcto.getDiscount());
    }
int totalRecords = billRecord.size();
int itemSize = (Integer)request.getAttribute("itemSize");
int laborSize = (Integer) request.getAttribute("laborSize") ;
int taxSize = (Integer) request.getAttribute("taxSize") ;
float laborTotal = 0; 
float spareTotal = 0; 
float taxTotal = 0; 
double total = 0; 
int index = 0;
int loopCntr = 0;

System.out.println("am here...0:"+billRecord.size());

for(int i=0;i< billRecord.size(); i=i+pageSize){
loopCntr = i;
%>

<div id="print<%= i %>" >

<style>


.text1 {
font-family:Tahoma;
font-size:14px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
line-height:15px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
line-height:15px;
}


</style>
<% int cntr1 = 1; %>

    <c:if test= "${billHeaderInfo != null}"  >
        <c:forEach items="${billHeaderInfo}" var="bill" >
            <% if (cntr1 ==1) {

%>
    <table width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:##E8E8E8; border-style:solid;" >
        <tr>
            <td height="27" colspan="4" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
               <b> <c:out value="${bill.invoiceType}" /> INVOICE </b>
            </td>
        </tr>
        <c:set var="jobcardNo" value="${bill.jcMYFormatNo}" />
        <c:set var="createdDate" value="${bill.date}" />
        <c:set var="billDiscount" value="${bill.discount}" />
        <c:set var="jcManualCreatedDate" value="${bill.createdDate}" />
        <c:set var="manualJcNo" value="${bill.remarks}" />
        <c:set var="servicePtName" value="${bill.servicePtName}" />
        <tr>
            <td class="text2" colspan="2" width="427"  align="left" rowspan="2" scope="col" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                Your Company Logo <br>
                <p> <b> Company Name </b><br>
                   address 1,<br>
                    address 2,<br>
                    Pin Code xxx xxx.<br>
            E-mail : abc@xyx.com</p></td>
            <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <p>Invoice No<br>
                    <c:if test= "${bill.invoiceNo != ''}"  >
                <b> <c:out value="${bill.invoiceNo}" /> </b> 
                </c:if>
                <c:if test= "${bill.invoiceNo == ''}"  >
                <b> <c:out value="${bill.billNo}" /> </b>
                </c:if>
                </p>
            </td>
            <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                <p>Dated<br>
                <b> <c:out value="${bill.billDate}" /> </b> </p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Vehicle No.<br>
                <b> <c:out value="${bill.vehicleNo}" /> </b> </p>
            </td>
            <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                <p>Other Reference<br>
                <!--<b><c:out value="${bill.companyName}" /></b>-->
                    
                    <!--<b>-&nbsp;<c:out value="${bill.usageTypeName}" /></b>-->
                    
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2" rowspan="4" align="left" valign="top" class="text2" scope="col"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Customer<br>
                <b>M/S <c:out value="${bill.customerName}" /></b> <br>
                 <c:out value="${bill.address}" /> <br>                
                    <c:out value="${bill.city}" /> <br>
                    <c:out value="${bill.state}" /> <br>
                    <c:out value="${bill.phone}" /> <br>
                    <c:out value="${bill.email}" /> <p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Customer's Order No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatch Document  No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatched through <br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Destination<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td  class="text2"  colspan="2" align="left"  style="border:1px;">
                <p>Terms of Delivery <br>
                <p> &nbsp;   
            </td>
        </tr>
    </table>
            <% }
    cntr1++;
%>
    </c:forEach>
</c:if>    
    
    
    

    
    
   

    <table width="725" <%--height="99"--%>  border="0" cellpadding="0"  style="border:1px; border-left-color:##E8E8E8; border-left-style:solid; border-right-color:##E8E8E8; border-right-style:solid;"  cellspacing="0">
        <tr>
	    <th width="30" class="text2"  height="30" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; " scope="col" >Sno</th>
            <th class="text2" width="407"  height="30"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col" >Description of Parts </th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Price</th>
            <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">VAT %</th>
            <th width="50"  class="text2" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Qty.</th>
            <th width="50"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">UOM</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">Amount</th>
        </tr>

<% if(loopCntr <= itemSize ){ %>         
<% 

        for(int j=loopCntr; ( j < (itemSize) ) && (j < i + pageSize  ) ; j++){	

       ++loopCntr;
       itemTO = new JobCardItemTO();
       itemTO = (JobCardItemTO) billRecord.get(j);
String itemName = "";
String uom = "";
       itemName = itemTO.getItemName();
       uom = itemTO.getUom();
       if(itemName.length() > itemNameLength){
           itemName = itemName.substring(0,itemNameLength-1);
       }
       if(uom.length() > uomLength){
           uom = uom.substring(0,uomLength-1);
       }
   
%>
        


        <tr>        
            <td  class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <% index++; %> <%= index %>
            </td>            
            <td  class="text2"   align="left"   style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                <%= itemName %> 
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                <%= itemTO.getPrice() %>
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"   align="center" >
                <%= itemTO.getTax() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= itemTO.getQuantity() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= uom %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; "  align="right" >
                <%= itemTO.getAmount() %>
            </td>
            <% spareTotal =(float) spareTotal + Float.parseFloat(itemTO.getAmount()); %>
        </tr>
    <% } %>
    
    <% if( (loopCntr < i+pageSize ) && (loopCntr == itemSize) ) { %>

        <tr>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>          
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <b>Spare Net Amount</b>
            </td>
            <td  class="text2" align="right"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >            
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; " align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "   align="right" >
                    <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% spareTotalRound.setRoundedValue(String.valueOf(spareTotal)); %> 			 			                    
                        <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b>
                    </jsp:useBean>     
                
            </td>
        </tr>
        
        <% } %>
        
        
<% } %>        
    





<% 
System.out.println("am here...1");
    for(int k=loopCntr; ( k < ( itemSize + taxSize) ) && ( k < i+ pageSize ) && (k<totalRecords ) ; k++){	

       itemTO = new JobCardItemTO();
       itemTO = (JobCardItemTO) billRecord.get(k);
       ++loopCntr;
%>
   
        <tr>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>          
            <td  class="text2"  align="right" style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               	 OUTPUT VAT @ <%= itemTO.getTax() %>
            </td>
            <td  class="text2" align="right"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >            
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  align="right" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "   align="right" >
                <%= itemTO.getAmount() %> <c:set var="taxTotal" value="0" />
            </td>
        </tr>            
        <% taxTotal = (float) taxTotal + Float.parseFloat(itemTO.getAmount()); %>
        <% } %>
    

<% 
System.out.println("am here...2");
if(loopCntr > (itemSize+taxSize-1) && loopCntr < i+pageSize ) { %>

        <tr>
            <th width="30" class="text2"  height="30" style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid;  border-top-color:#CCCCCC; border-top-style:solid;  border-left-color:#CCCCCC; border-left-style:solid;" scope="col" >&nbsp;</th>
            <th colspan="5" width="625" class="text2" style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid;  border-top-color:#CCCCCC; border-top-style:solid;" scope="col" >   Labour Charges</th>
            <th  width="65"  class="text2"  style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-top-color:#CCCCCC; border-top-style:solid;"  scope="col">&nbsp;</th>
        </tr>

<% 
    for(int l=loopCntr; ( l < ( billRecord.size() ) && ( loopCntr < i+pageSize )  ) ; l++ ) {
       activityTO = new ProblemActivitiesTO();
       activityTO = (ProblemActivitiesTO) billRecord.get(l);
       activity = activityTO.getActivitySplt();
       ++loopCntr;
%>




        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
		<% index++; %> 
                 <%= index %>	
            </td>            
            <td class="text2" colspan="3"  align="left"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <% for(int k=0;k<activity.length ; k++){ %>
                      <%= activity[k] %>   <br>
                    <% } %> 
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <!--<%= activityTO.getQty() %>-->
                    -
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                -
            </td>
            <td class="text2"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%= activityTO.getAmount() %>
                <% laborTotal = (float) laborTotal + Float.parseFloat(activityTO.getAmount()); %>
            </td>
            
        </tr>
<% } %>

<% if( (loopCntr <= i + pageSize) && (loopCntr == (itemSize+taxSize+laborSize) ) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>            
            <td class="text2" colspan="5" align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <b> Labour Total </b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                    <jsp:useBean id="laborTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% laborTotalRound.setRoundedValue(String.valueOf(laborTotal)); %> 			 			                    
                         <b> <jsp:getProperty name="laborTotalRound" property="roundedValue" />  </b>
                    </jsp:useBean>                  
            </td>            
        </tr>
        <tr><td><br></td></tr>
        
<% } %>        
        
        
<% } %>        

<% 
System.out.println("am here...3");
total = (double) (spareTotal + laborTotal + taxTotal ); %>      

            <% for(int m=loopCntr; ( (m%pageSize) != 0); m++){ %> 

       <%-- <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  scope="row" >
                &nbsp;Hi
            </td>            
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                
                &nbsp;
            </td>            
        </tr>  --%>
            <% } %>
            
<% if(loopCntr != (totalRecords) ) { %>            

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>            
            <td class="text2" colspan="5"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                
                &nbsp;
            </td>            
        </tr> 

                <tr>
	            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	                &nbsp;
	            </td>            
	            <td class="text2" colspan="5"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	               &nbsp;
	            </td>
	            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                
	                &nbsp;
	            </td>            
        </tr> 
        
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>            
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                
                &nbsp;
            </td>            
        </tr> 
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>            
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid; "  align="right" >                
                &nbsp;
            </td>            
        </tr>   
        
        
<% } if(loopCntr == (totalRecords) ) {
System.out.println("am here...4");
    %>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>            
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Spare Net Amount
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <jsp:getProperty name="spareTotalRound" property="roundedValue" /> 
            </td>            
        </tr>
        
        
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>            
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Total Vat
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                 <!--   <jsp:useBean id="totalVatt"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% totalVatt.setRoundedValue(String.valueOf(taxTotal)); %> 			 			                    
                        <jsp:getProperty name="totalVatt" property="roundedValue" /> 
                    </jsp:useBean> -->
                <%= itemTO.getAmount() %>
            </td>            
        </tr>        
        
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>            
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Labour Total
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                 		
                        <jsp:getProperty name="laborTotalRound" property="roundedValue" />                                  
            </td>            
        </tr>
<%

    
    double serviceTaxFraction = 0;
    String billServiceTaxPercent = (String)request.getAttribute("billServiceTaxPercent");
    String serviceTax = (String)request.getAttribute("billServiceTaxDetails"); 
try{
    
    
    if(Float.parseFloat(serviceTax) > 0) {
%>
    <jsp:useBean id="serviceTaxRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
    <% serviceTaxRound.setRoundedValue(String.valueOf(serviceTax)); %>
    </jsp:useBean>    
    <%
if(!billServiceTaxPercent.equals("12.36"))  {
%>
    <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Service Tax (@<%=billServiceTaxPercent%>%)
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <jsp:getProperty name="serviceTaxRound" property="roundedValue" />
            </td>
        </tr>

<%
} else {

double serviceTaxNew = 12;
double edu_cess = 2;
double hr_edu_cess = 1;
if(laborTotal > 0){
    serviceTaxNew = (laborTotal * serviceTaxNew)/100;
}
edu_cess = ((serviceTaxNew * edu_cess ) / 100);
hr_edu_cess = ((serviceTaxNew * hr_edu_cess ) / 100);
%>
<tr>
        <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
            &nbsp;
        </td>
        <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
            Service Tax (@12%)
        </td>
        <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
            <%=new DecimalFormat("#0.00").format(serviceTaxNew)%>
        </td>
    </tr>
<tr>
    <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
            &nbsp;
    </td>
    <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
        Edu cess (@2%)
    </td>
    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
        <%=new DecimalFormat("#0.00").format(edu_cess)%>
    </td>
</tr>
<tr>
    <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
         &nbsp;
    </td>
    <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
        Hr. Edu.cess (@1%)
    </td>
    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
        <%=new DecimalFormat("#0.00").format(hr_edu_cess)%>
    </td>
</tr>
<%

}
}
}catch(ArithmeticException ae)     {
    System.out.println("Arithmetic Exception in SErvice Tax"+ae.getMessage());
}
%>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <b>Total</b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
 			<%
                        NumberFormat formatter2 = new DecimalFormat("#0.00");
                        total = total + Float.parseFloat(serviceTax);
                      //  out.println("<h1>"+total+"</h1>");
                        %>
                        <%--</tr> <jsp:getProperty name="numberWords2" property="roundedValue" />--%>
                        <b><%=formatter2.format(java.lang.Math.ceil(total)) %> </b>

            </td>
        </tr>
        <%if(discountVal > 0){%>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Discount(<%=discountVal%>%)
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
 			<% 
                        NumberFormat formatter1 = new DecimalFormat("#0.00");
                        %>
                        <%--</tr> <jsp:getProperty name="numberWords2" property="roundedValue" />--%>
                        <b><%=formatter1.format(java.lang.Math.ceil(total*discountVal/100)) %> </b>

            </td>
        </tr>
        <%}%>
        
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC; " scope="row" >
				&nbsp;
            </td>            
            <td class="text2" colspan="5"  align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               <b> Grand Total <span class="WebRupee">SAR.</span></b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >                
                    <jsp:useBean id="numberWords2"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% numberWords2.setRoundedValue(String.valueOf(total));
                        NumberFormat formatter = new DecimalFormat("#0.00");
                        %>
                        <%--</tr> <jsp:getProperty name="numberWords2" property="roundedValue" />--%>
                        <b><%=formatter.format(java.lang.Math.ceil(total - (total*discountVal/100))) %> </b>
                    </jsp:useBean>                 
            </td>            
        


 
<% } %>        
        
<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;  border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                &nbsp;
            </td>            
            <td class="text2" colspan="5"  align="center" style="border:1px; border-color:#FFFFFF;    border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                <b>Contnd..</b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF;  border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;"  align="right" >                
               &nbsp;
            </td>            
        </tr>   






<% } %>
        
    </table>    




<table width="725" height="79" border="0" style="border:1px; border-color:##E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
<tr>
<td width="720">
<div class="text2" >    
 <% if(loopCntr == (totalRecords) ) { %>
 Amount Chargeable (in words)
  <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% numberWords1.setRoundedValue(String.valueOf(java.lang.Math.ceil(total-(total*discountVal/100)))); %>
 			
 			<% numberWords1.setNumberInWords(numberWords1.getRoundedValue()); %>
                    
                    <b>  Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</b>
                    </jsp:useBean> 
<% } %>    
 <br>
<%--<br>--%>

Remarks:
<% if(loopCntr == (totalRecords) ) { %>
<% if(request.getAttribute("vendorNames") != null ){ %>
<!--<b> <%= request.getAttribute("vendorNames") %> </b>-->
<% } %>
<% } %>
<br>
<table height="10" align="left" cellpadding="0" cellspacing="0" border="0">
    
    <tr>
        <td width="200" class="text2">
            <b>  Jobcard Created Date</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${jcManualCreatedDate}" /></b>
        </td>
    </tr>
    <%--<tr>
        <td width="200" class="text2">
            <b>AS PER <c:out value="${servicePtName}" /> &nbsp;Manual JC NO</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${manualJcNo}" /></b>
        </td>
    </tr>--%>
    <tr>
        <td width="200" class="text2">
            <b> AS PER <c:out value="${servicePtName}" />&nbsp;JC.No</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${jobcardNo}" /></b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
        <b> Company's VAT TIN</b>
        </td>
        <td class="text2">
            :&nbsp;<b>xxxxxx</b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
        <b> Company's CST No</b>
        </td>
        <td class="text2">
            :&nbsp;<b>xxx</b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
        <b> Service Tax Regn No</b>
        </td>
        <td class="text2">
            :&nbsp;<b>xxxx</b>
        </td>
    </tr>
    <tr>
        <td width="200" class="text2">
            <b>Customer VAT/TIN/SalesTax Ref</b>
        </td>
        <%--<td class="text2">
            :&nbsp;
        </td>--%>
    </tr>
<%--</table>
</div>
        
<table width="720" border="0" cellpadding="0" cellspacing="0">
--%>    <tr>
    <td class="text2" width="380" align="left"><b>Declaration</b> </td>
    <td class="text2" width="315" align="right"><b> for your company name. </b> </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">We declare that this invoice shows the actual price of the</td>
    <td class="text2" width="325" align="right"> &nbsp;</td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">goods described and that all particulars are true and correct</td>
    <td class="text2" width="325" align="right">&nbsp; </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">&nbsp; </td>
    <td class="text2" width="315" align="right"><b> Authorized Signatory </b></td>
    </tr>
    <tr>
    <table border="0" align="center">
     <tr>
        <td class="text2"  align="center">
            <p><b>SUBJECT TO CHENNAI JURISDICTION</b><br>
               This is a Computer Generated Invoice</p></td>
     </tr>
    </table>
    </tr>
     
</table>  

</div>

</td>
</tr>
</table>

    

<%--<br>
<div ALIGN="CENTER" class="text2">
SUBJECT TO CHENNAI JURISDICTION &nbsp;This is a Computer Generated Invoice
</div>--%>
</div>
<br>
    <center>   
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;        
    </center>
<br>    
<br>    
<% } %>


</body>
</html>

