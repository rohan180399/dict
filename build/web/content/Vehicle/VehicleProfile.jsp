<%-- 
    Document   : VehicleProfile
    Created on : 13 Mar, 2012, 1:41:50 PM
    Author     : kannan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" src="/throttle/FusionCharts.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/prettify/prettify.js"></script>
        <script type="text/javascript" src="/throttle/ui/js/json2.js"></script>
        <script type="text/javascript">
            function viewProfile(vehicleId) {        
                if(vehicleId != "") {
                    document.vehicleProfile.action = "/throttle/viewVehicleProfile.do";
                    document.vehicleProfile.method = "post";
                    document.vehicleProfile.submit();
                }
            }
        </script>
        <style type="text/css">
            .link {
                font: normal 12px Arial;
                text-transform:uppercase;
                padding-left:10px;
                font-weight:bold;
            }
            .link a  {
                color:#7f8ba5;
                text-decoration:none;
            }
            .link a:hover {
                color:#7f8ba5;
                text-decoration:underline;
            }
        </style>
        <style type="text/css">
            #expand { width:100%; }
            .column { width: 250px; float: left; }
            .portlet {width: 410px; margin: 0 1em 1em 0; border:1px solid #CCCCCC;}
            .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move;font-weight: bold;color:#FFFFFF; background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;height:25px;font-size: 14px; vertical-align: middle; }
            .portlet-header .ui-icon { float: right; }
            .portlet-content { padding: 0.4em; }
            .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
            .ui-sortable-placeholder * { visibility: hidden; }
        </style>
    </head>
    <!--setImages(1,0,0,0,0,0);-->
    <body>
        <form name="vehicleProfile">
            <center>
                <h2>Vehicle Profile</h2>
            </center>
            <table width="90%" class="table2" cellspacing="0" align="center">
                <tr>
                    <td class="texttitle1">Vehicle Number</td><td class="text1">
                        <select name="vehicleId" onchange="viewProfile(this.value);">
                            <option value="">-select-</option>
                            <c:if test = "${vehicleRegNos != null}" >
                                <c:forEach items="${vehicleRegNos}" var="reg">
                                    <option value='<c:out value="${reg.vehicleId}" />'> <c:out value="${reg.regNo}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>

                </tr>
            </table>
            <script type="text/javascript">

                if("<%=request.getAttribute("vehicleId")%>" != "null" && "<%=request.getAttribute("vehicleId")%>" != null) {
                    document.vehicleProfile.vehicleId.value = "<%=request.getAttribute("vehicleId")%>";
                }

            </script>
            <table width="90%" class="table2" cellspacing="0" align="center">
                <tr><td colspan="4" class="contenthead">Vehicle Details</td></tr>
                <c:if test = "${vehicleProfileList != null}" >
                    <c:forEach items="${vehicleProfileList}" var="vp">
                        <tr>
                            <td class="texttitle1">Vehicle Number</td><td class="text1"><c:out value="${vp.regNo}" /></td>
                            <td class="texttitle1">Registration Date</td><td class="text1"><c:out value="${vp.registrationDate}" /></td>
                        </tr>
                        <!--<tr>
                            <td class="texttitle2">Usage</td><td class="text2">Cargo</td>
                            <td class="texttitle2">Warranty Period</td><td class="text2"><c:out value="${vp.war_period}" /></td>
                        </tr>-->
                        <tr>
                            <td class="texttitle2">MFRs</td><td class="text2"><c:out value="${vp.mfrName}" /></td>
                            <td class="texttitle2">Model</td><td class="text2"><c:out value="${vp.modelName}" /></td>                            
                        </tr>                        
                        <tr>
                            <td class="texttitle1">Engine No</td><td class="text1"><c:out value="${vp.engineNo}" /></td>
                            <td class="texttitle1">Chassis No</td><td class="text1"><c:out value="${vp.chassisNo}" /></td>
                        </tr>                        
                        <tr>
                            <td class="texttitle2">KM Reading</td><td class="text2"><c:out value="${vp.kmReading}" /></td>
                            <td class="texttitle2">HM Reading</td><td class="text2"><c:out value="${vp.hmReading}" /></td>
                        </tr>                        
                        <tr>
                            <td class="texttitle1">Daily Running KM</td><td class="text1"><c:out value="${vp.dailyKm}" /></td>
                            <td class="texttitle1">Daily Running HM</td><td class="text1"><c:out value="${vp.dailyHm}" /></td>                            
                        </tr>
                        <tr>
                            <td class="texttitle2">Customer</td><td class="text2"><c:out value="${vp.custName}" /></td>
                            <td class="texttitle2">GPS Tracking System</td><td class="text2"><c:out value="${vp.gpsSystem}" /></td>
                        </tr>
                        <tr><td colspan="4" class="contenthead">Insurance Details</td></tr>
                        <tr>
                            <td class="texttitle1">Insurance Company</td><td class="text1"><c:out value="${vp.companyName}" /></td>
                            <td class="texttitle1">Premium No</td><td class="text1"><c:out value="${vp.premiumNo}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Insurance Expiry Date</td><td class="text2"><c:out value="${vp.insExpiryDate}" /></td>
                            <td class="texttitle2">Insurance Premium Amount</td><td class="text2"><c:out value="${vp.insPremiumAmount}" /></td>
                        </tr>
                        <tr><td colspan="4" class="contenthead">Permit Details</td></tr>
                        <tr>
                            <td class="texttitle1">Permit Type</td><td class="text1"><c:out value="${vp.permitType}" /></td>
                            <td class="texttitle1">Permit No</td><td class="text1"><c:out value="${vp.permitNo}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Permit Expiry Date</td><td class="text2"><c:out value="${vp.permitExpiryDate}" /></td>
                            <td class="texttitle2">Permit Amount</td><td class="text2"><c:out value="${vp.permitAmount}" /></td>
                        </tr>
                        <tr><td colspan="4" class="contenthead">AMC Details</td></tr>
                        <tr>
                            <td class="texttitle1">AMC Company Name</td><td class="text1"><c:out value="${vp.amcCompanyName}" /></td>
                            <td class="texttitle1">AMC Amount</td><td class="text1"><c:out value="${vp.amcAmount}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">AMC From Date</td><td class="text2"><c:out value="${vp.amcFromDate}" /></td>
                            <td class="texttitle2">AMC To Date</td><td class="text2"><c:out value="${vp.amcToDate}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">AMC Duration</td><td class="text1"><c:out value="${vp.amcDuration}" /></td>
                            <td class="texttitle1">Vehicle Group</td><td class="text1"><c:out value="${vp.groupName}" /></td>
                        </tr>
                        <tr><td colspan="4" class="contenthead">FC Details</td></tr>
                        <tr>
                            <td class="texttitle1">Next FC Date</td><td class="text1"><c:out value="${vp.nextFCDate}" /></td>
                            <td class="texttitle1">RTO Detail</td><td class="text1"><c:out value="${vp.rtoDetail}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Receipt No</td><td class="text2"><c:out value="${vp.receiptNo}" /></td>
                            <td class="texttitle2">AMC Amount</td><td class="text2"><c:out value="${vp.fcAmount}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Remark</td><td class="text1"><c:out value="${vp.remarks}" /></td>
                            <td class="texttitle1">&nbsp;</td><td class="text1">&nbsp;</td>
                        </tr>
                        <tr><td colspan="4" class="contenthead">Road Tax Details</td></tr>
                        <tr>
                            <td class="texttitle1">Road Tax From Date</td><td class="text1"><c:out value="${vp.roadTaxFromDate}" /></td>
                            <td class="texttitle1">Road Tax To Date</td><td class="text1"><c:out value="${vp.roadTaxToDate}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Road Tax Period</td><td class="text2"><c:out value="${vp.roadTaxPeriod}" /></td>
                            <td class="texttitle2">Road Tax Amount</td><td class="text2"><c:out value="${vp.roadTaxAmount}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Road Tax Paid Location</td><td class="text1"><c:out value="${vp.roadTaxPaidLocation}" /></td>
                            <td class="texttitle1">Road Tax Receipt No</td><td class="text1"><c:out value="${vp.roadTaxReceiptNo}" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Road Tax Receipt Date</td><td class="text2"><c:out value="${vp.roadTaxReceiptDate}" /></td>
                            <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
                        </tr>
                    </table>
            <br>
            <table width="90%" class="table2" cellspacing="0" align="center">
                <tr><td colspan="4" class="contenthead" align="center">OEM Details</td></tr>
                <tr>
                    <td class="contenthead">S.No</td><td class="contenthead">Item</td>
                    <td class="contenthead">Position</td><td class="contenthead">OEM Number</td>
                </tr>
                <%
                            int sno = 0;
                %>
                <c:if test = "${tyreList != null}" >
                    <c:forEach items="${tyreList}" var="tyre">
                        <%
                                    sno++;
                        %>
                        <tr>
                            <td class="text1"><%=sno%></td><td class="text1"><c:out value="${tyre.itemName}" /> </td>
                            <td class="text1"><c:out value="${tyre.description}" /></td>
                            <td class="text1"><c:out value="${tyre.tyreNo}" /></td>
                        </tr>
                    </c:forEach></c:if>

                </table>
            <br>
            <table width="90%" class="table2" cellspacing="0" align="center">
                <tr><td colspan="4" class="contenthead">Cost Details</td></tr>
                <tr>
                    <td class="texttitle1">Buying Price</td><td class="text1"><c:out value="${vp.vehicleCost}" /></td>
                <td class="texttitle1">Depreciation</td><td class="text1"><c:out value="${(vp.vehicleCost * vp.vehicleDepreciation)/100}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Current Price</td><td class="text2"><c:out value="${vp.vehicleCost- ((vp.vehicleCost * vp.vehicleDepreciation)/100)}" /></td>
                <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
            </tr>

            <tr><td colspan="4" class="contenthead">Government Compliance</td></tr>
            <tr>
                <td class="texttitle1">IC</td><td class="text1"><c:out value="${vp.premiumPaidAmount}" /></td>
                <td class="texttitle1">FC Cost</td><td class="text1"><c:out value="${vp.fcAmount}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Road Tax</td><td class="text2"><c:out value="${vp.roadTaxPaidAmount}" /></td>
                <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
            </tr>

            <tr><td colspan="4" class="contenthead">Fuel Cost</td></tr>
            <tr>
                <td class="texttitle1">KM Run</td><td class="text1"><c:out value="${vp.tripRunKm}" /></td>
                <td class="texttitle1">Fuel Consumed</td><td class="text1"><c:out value="${vp.fuelLiters}" /></td>
            </tr>
            <tr>
                <td class="texttitle2">Total Fuel Cost</td><td class="text2"><c:out value="${vp.fuelAmount}" /></td>
                <td class="texttitle2">&nbsp;</td><td class="text2">&nbsp;</td>
            </tr>


            <tr><td colspan="4" class="contenthead">Driver Bata</td></tr>
            <tr>
                <td class="texttitle1">KM Run</td><td class="text1">&nbsp;</td>
                <td class="texttitle1">Cost</td><td class="text1">&nbsp;</td>
            </tr>
            <tr><td colspan="4" class="contenthead">Service Cost</td></tr>
            <tr>
                <td class="texttitle1" >Total</td><td class="text1"><c:out value="${vp.serviceCost}" /></td>
                <td class="texttitle1">Cost/month</td><td class="text1"><c:out value="${vp.serviceCost}" /></td>
            </tr>

        </table>
            <!--  <table width="90%" height="50" class="table2" cellspacing="0" align="center">
                  <tr>
                      <td>
                          <div style="overflow:auto;width:100%;height:150px; margin: 0px;">
                  <table width="100%" align="center"cellspacing="0" cellpadding="0" class="table2">
              <tr>
                  <td  class="contenthead">S.No</td>
                  <td  class="contenthead">Tyre No</td>
                  <td  class="contenthead">Make</td>
                  <td  class="contenthead">Position</td>
                  <td  class="contenthead">Description</td>
                  <td  class="contenthead">From Date</td>
                  <td  class="contenthead">To Date</td>
                  <td  class="contenthead">Type</td>
                  <td  class="contenthead">Km</td>
                  <td  class="contenthead">Cost</td>
              </tr>


          <tr>
              <td  class="text1">1</td>
              <td  class="text1">T100014</td>
              <td  class="text1">BRIDGESTONE</td>
              <td  class="text1">RRO</td>
              <td  class="text1">RRO</td>
              <td  class="text1">26-08-2009</td>
              <td  class="text1">26-02-2011</td>
              <td  class="text1">New</td>
              <td  class="text1">40,000</td>
              <td  class="text1">25,000</td>

          </tr>


          <tr>
              <td  class="text2">2</td>
              <td  class="text2">T100011</td>
              <td  class="text2">BRIDGESTONE</td>
              <td  class="text2">RRI</td>
              <td  class="text2">Rear Right Inner</td>
              <td  class="text2">26-08-2009</td>
              <td  class="text2">26-02-2011</td>
              <td  class="text2">New</td>
              <td  class="text2">40,000</td>
              <td  class="text2">25,000</td>

          </tr>


          <tr>
              <td  class="text1">3</td>
              <td  class="text1">T100012</td>
              <td  class="text1">BRIDGESTONE</td>
              <td  class="text1">RLO</td>
              <td  class="text1">Right Outer</td>
              <td  class="text1">26-08-2009</td>
              <td  class="text1">07-06-2011</td>
              <td  class="text1">New</td>
              <td  class="text1">40,000</td>
              <td  class="text1">25,000</td>
          </tr>


          <tr>
              <td  class="text2">4</td>
              <td  class="text2">T100013</td>
              <td  class="text2">BRIDGESTONE</td>
              <td  class="text2">RLI</td>
              <td  class="text2">RLI</td>
              <td  class="text2">26-08-2009</td>
              <td  class="text2">07-06-2011</td>
              <td  class="text2">New</td>
              <td  class="text2">40,000</td>
              <td  class="text2">25,000</td>
          </tr>


          <tr>
              <td  class="text1">5</td>
              <td  class="text1">MOJ352851</td>
              <td  class="text1">BRIDGESTONE</td>
              <td  class="text1">FR</td>
              <td  class="text1">Front Right</td>
              <td  class="text1">29-06-2011</td>
              <td  class="text1">Till Date</td>
              <td  class="text1">RC</td>
              <td  class="text1">20,000</td>
              <td  class="text1">10,000</td>
          </tr>


          <tr>
              <td  class="text2">6</td>
              <td  class="text2">MOJ336551</td>
              <td  class="text2">BRIDGESTONE</td>
              <td  class="text2">FL</td>
              <td  class="text2">Front Left</td>
              <td  class="text2">29-06-2011</td>
              <td  class="text2">Till Date</td>
              <td  class="text2">RC</td>
              <td  class="text2">20,000</td>
              <td  class="text2">10,000</td>
          </tr>

          </table>
          </td>
              </tr>

      </table>-->
    <br>
    <%

                String serviceCost = "<chart caption='Service Cost' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"
                        + "<set value='30000' label='Engine'  color='3EA99F' alpha='60'/>"
                        + "<set value='30000' label='Body Works'  color='99CC00' alpha='60'/>"
                        + "<set value='10000' label='Transmission'  color='357EC7' alpha='60'/>"
                        + "<set value='60000' label='Tyres'  color='F535AA' alpha='60'/>"
                        + "<set value='5000' label='Others'  color='FFEA00' alpha='60'/>"
                        + "</chart>";


    %>
    <%--<div id="expand">
        <center>
            <div class="portlet" >
                <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Service Cost</div>
                <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=serviceCost%>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="400" />
                                    <jsp:param name="chartHeight" value="250" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </center>
    </div>--%>
</c:forEach></c:if>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>


