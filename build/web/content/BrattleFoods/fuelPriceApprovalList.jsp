<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#cityName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#cityId').val(Id);
                $itemrow.find('#cityName').val(Name);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });


    function resetCityId() {
        if (document.getElementById('cityName').value == '') {
            document.getElementById('cityId').value = '';
        }
    }
    </script>
    <script type=text/javascript>
    function searchPage() {
         document.fuleprice.action = "/throttle/fuelPriceApprovalList.do?";
    document.fuleprice.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Fuel</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Fuel</a></li>
            <li class="active"> Manage Fuel Price</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="fuleprice"  method="post" >
                    <br>
                    <table class="table table-info mb30 table-hover" style="width:60%">
                        <tr   ><td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Fuel Revisal Approval</td></tr>


                        <td><font color="red">*</font>From Date</td>
                        <td><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:40px;"  onclick="ressetDate(this);" value=""></td>
                        <td><font color="red">*</font>To Date</td>
                        <td><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:40px;" onclick="ressetDate(this);" value=""></td>

                        <td  align="left">
                            <input type="button" class="btn btn-info"   value="Search" onclick="searchPage()">
                        </td>
                        </tr>

                    </table>

                    <br>
                    <h2 align="center"> View Fuel Approval List</h2>
                    <br>
                    <table class="table table-info mb30 table-hover" style="width:100%" id="table">
                        <thead height="40">
                            <tr height="40" id="tableDesingTH">
                                <th> S.No </th>
                                <th> Bunk Name </th>
                                <th> Effective Date From </th>
                                <th> Fuel Price (INR) </th>
                                <th> Last Price (INR) </th>
                                <th> Change in Price (INR) </th>
                                <th> Action </th>

                            </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>

                            <c:if test = "${viewfuelPriceMaster != null}" >
                                <%
                                                    sno++;
                                                   String className = "text1";
                                                   if ((sno % 1) == 0) {
                                                       className = "text1";
                                                   } else {
                                                       className = "text2";
                                                   }
                                %>
                                <c:forEach items="${viewfuelPriceMaster}" var="fuelPriceMaster">
                                    <c:set var="approvedStatus" value="${fuelPriceMaster.approveStatus}"/>
                                    <tr height="30">
                                        <td    height="30"> <%= sno++%></td>
                                        <td   align="left" ><c:out value="${fuelPriceMaster.bunkName}"/></td>
                                        <td   align="left"> <input type="hidden" id="oldFuelPriceId" name="oldFuelPriceId" value="<c:out value="${fuelPriceMaster.fuelPriceId}"/>"/>
                                            <c:out value="${fuelPriceMaster.effectiveDate}"/></td>
                                        <td   align="left" ><c:out value="${fuelPriceMaster.fuelPrice}"/></td>
                                        <td   align="left" ><c:out value="${fuelPriceMaster.lastPrice}"/></td>
                                        <td   align="left" ><c:out value="${fuelPriceMaster.priceDiff}"/></td>
                                        <c:if test="${approvedStatus == null || approvedStatus == ''}"> 
                                            <td   align="left" >  <a style='text-decoration: none' href="/throttle/updateFuelPriceMasters.do?userId=1477&effectiveDate=<c:out value="${fuelPriceMaster.effectiveDate}"/>&fuelPrice=<c:out value="${fuelPriceMaster.fuelPrice}"/>&oldFuelPriceId=<c:out value="${fuelPriceMaster.lastPrice}"/>&bunkId=<c:out value="${fuelPriceMaster.bunkId}"/>&bunkName=<c:out value="${fuelPriceMaster.bunkName}"/>&status=1">Approve</a>&nbsp;|&nbsp;
                                                <a style='text-decoration: none' href="/throttle/updateFuelPriceMasters.do?userId=1477&effectiveDate=<c:out value="${fuelPriceMaster.effectiveDate}"/>&fuelPrice=<c:out value="${fuelPriceMaster.fuelPrice}"/>&oldFuelPriceId=<c:out value="${fuelPriceMaster.lastPrice}"/>&bunkId=<c:out value="${fuelPriceMaster.bunkId}"/>&bunkName=<c:out value="${fuelPriceMaster.bunkName}"/>&status=0">Reject</a>
                                            </td>
                                        </c:if>

                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                    <script type="text/javascript">
                        function submitPage() {
                            if (document.getElementById('effectiveDate').value == '') {
                                alert("Please select date");
                                document.getElementById('effectiveDate').focus();
                            } else if (document.getElementById('fuelPrice').value == '') {
                                alert("Please enter fuel price");
                                document.getElementById('fuelPrice').focus();
                            } else if (document.getElementById('fuelType').value == '0') {
                                alert("Please select fuelType");
                                document.getElementById('fuelType').focus();
                            } else if (document.getElementById('bunkIds').value == '0') {
                                alert("Please select BunkName");
                                document.getElementById('bunkIds').focus();
                            } else {
                                document.fuleprice.action = '/throttle/approveFuelPrice.do';
                                //                      document.fuleprice.action = '/throttle/saveFuelPriceMaster.do';
                                document.fuleprice.submit();
                            }
                        }
                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

