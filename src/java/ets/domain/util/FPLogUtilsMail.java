/*-----------------------------------------------------------------------------
  * FPLogUtils.java
  * Mar 25, 2008
  *
  * Copyright (c) ES Systems.
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * ES Systems ("Confidential Information"). You shall
  * not disclose such Confidential Information and shall use it only in
  * accordance with the terms of the license agreement you entered into
  * with ES Systems.
  ---------------------------------------------------------------------------*/

 package ets.domain.util;

import ets.arch.util.SendBrattleLogMail;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/****************************************************************************
 *
 * Modification Log:
 * -----------------------------------------------------------------------------
 * Ver       Date                              Author                 Change
 * -----------------------------------------------------------------------------
 * 1.0       Mar 25, 2008                      ES			      Created
 *
 *
 *****************************************************************************/
/**
 * <br>FPLogUtils object - Event Protal Log Util class.
 * @author Satheeshkumar P
 * @version 1.0 26 Mar 2008
 * @since   Event Portal Iteration 0
*/
public class FPLogUtilsMail
 {
		private static final Log LOG = LogFactory.getLog( FPLogUtils.class );
		/**
		 * This method would log the statement if the log4jproperties
		 * is set to Debug mode.
		 * @param loggableStatement - Object to be logged in
		 */

		public static void fpDebugLog ( Object loggableStatement )
		{
			if ( LOG.isDebugEnabled() )
			{
				LOG.debug( loggableStatement );
			}
		}

		/**
		 * This method would log the statement if the log4jproperties
		 * is set to Error mode.
		 * @param loggableStatement - Object to be logged in
		 */
		public static void fpErrorLog ( Object loggableStatement ){
			if ( LOG.isErrorEnabled() )
			{
				LOG.error( loggableStatement );
			}
//                        String canonicalHostName = "";
//                        String hostName = "";
//                        String hostAddress = "";
//                        try{
//                        InetAddress inetAddr = InetAddress.getLocalHost();
//                        System.out.println("Canonical host name: " + inetAddr.getCanonicalHostName());
//                        canonicalHostName = inetAddr.getCanonicalHostName();
//                        System.out.println("Host Name: " + inetAddr.getHostName());
//                        hostName = inetAddr.getHostName();
//                        System.out.println("Host Address: " + inetAddr.getHostAddress());
//                        hostAddress = inetAddr.getHostAddress();
//                        }catch (UnknownHostException e) {
//                            e.printStackTrace();
//                        }
//                          String smtp = "smtp.gmail.com";
////                        String smtp = "sg2nlvphout-v01.shr.prod.sin2.secureserver.net";
//                        int emailPort = 465;
//                        String frommailid = "madhand@entitlesolutions.com";
//                        String password = "entitle2nilesh";
//                        String subject = "Brattle Error Log For Host Name :"+hostName+" and Host Add:"+hostAddress;
//                        String content = loggableStatement.toString();
//                        String to = "arul@entitlesolutions.com";
//                        new SendBrattleLogMail(smtp, emailPort, frommailid, password, subject, content, to, "").start();
//                        return;
		}

		/**
		 * This method would log the statement if the log4jproperties
		 * is set to Error mode.
		 * @param loggableStatement - Object to be logged in
		 * @param throwable Exception to be logged in
		 */
		public static void fpErrorLog ( Object loggableStatement, Throwable throwable )
		{
			if ( LOG.isErrorEnabled() )
			{
				LOG.error( loggableStatement , throwable );
			}
		}

		/**
		 * This method would log the statement if the log4jproperties
		 * is set to Fatal mode.
		 * @param loggableStatement - Object to be logged in
		 */
		public static void fpFatalLog ( Object loggableStatement )
		{
			if ( LOG.isFatalEnabled() )
			{
				LOG.fatal( loggableStatement );
			}
		}

		/**
		 * This method would log the statement if the log4jproperties
		 * is set to FATAL mode.
		 * @param loggableStatement - Object to be logged in
		 * @param throwable Exception to be logged in
		 */
		public static void fpFatalLog ( Object loggableStatement, Throwable throwable )
		{
			if ( LOG.isFatalEnabled() )
			{
				LOG.fatal( loggableStatement , throwable );
			}
		}
		/**
		 * This method would log the statement if the log4jproperties
		 * is set to INFO mode.
		 * @param loggableStatement - Object to be logged in
		 */
		public static void fpInfoLog ( Object loggableStatement )
		{
			if ( LOG.isInfoEnabled() )
			{
				LOG.info( loggableStatement );
			}
		}

		/**
		 * This method would log the statement if the log4jproperties
		 * is set to TRACE mode.
		 * @param loggableStatement - Object to be logged in
		 */
		public static void fpTraceLog ( Object loggableStatement )
		{
			if ( LOG.isTraceEnabled() )
			{
				LOG.trace( loggableStatement );
			}
		}
}
