<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
<%@ page import="ets.domain.racks.business.RackTO" %>

<script language="javascript" src="/throttle/js/validate.js"></script> 
<script>
    function submitPage()
    {
        if(textValidation(document.addSubRack.rackId,' Rack Name')){   
            return;
         }  
         if(textValidation(document.addSubRack.subRackName,'Sub Rack Name')){   
            return;
        }  
        if(textValidation(document.addSubRack.subRackDescription,'Sub Rack Description')){   
            return;
        } 
         
        document.addSubRack.action='/throttle/handleAddSubRack.do';
        document.addSubRack.submit();
    }
    function setFocus(){
        document.addSubRack.rackId.focus();
        }
</script>


<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

 

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.SubRack"  text="SubRacks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.SubRack"  text="SubRacks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">


<body onload="setFocus();">
<form name="addSubRack"  method="post" > 

<table class="table table-bordered table-hover">

  
<tr>
<td ><font color="red">*</font><spring:message code="stores.label.RackName"  text="default text"/></td>
<td ><select class="form-control" name="rackId" >
<c:if test = "${rackLists != null}" >   
<c:forEach items="${rackLists}" var="rack">   
<option value="<c:out value="${rack.rackId}"/>"><c:out value="${rack.rackName}"/></option> 
</c:forEach>
</c:if>  
</select>
</td>
</tr>
 
<tr>
<td class="text1" height="30"><font color="red">*</font><spring:message code="stores.label.SubRackName"  text="default text"/></td>
<td class="text1" height="30"><input type="text" name="subRackName" class="form-control"></td>
</tr>
<tr>
<td ><font color="red">*</font><spring:message code="stores.label.RackDescription"  text="default text"/></td>
<td ><textarea class="form-control" name="subRackDescription"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.Save"  text="default text"/>" class="button" onclick="submitPage();">
&emsp;<input type="reset" class="button" value="<spring:message code="stores.label.Clear"  text="default text"/>">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
