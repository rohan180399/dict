<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleBPCLTransactionDetailsExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{
                        document.BPCLTransaction.action = '/throttle/handleBPCLTransactionDetailsExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <body>
        <form name="BPCLTransaction" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>GPS Vehicle Tracking </b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="form-control" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <tr>
                                        <td><font color="red">*</font>Vehicle No</td>
                                        <td height="30"><input name="vehicleNo" id="vehicleNo" type="text"  value="" ></td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="button" class="button" name="Track" onclick="submitPage(this.name);" value="Search"></td>
                                        <td><input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

          
                <table align="center" border="0" id="table" class="sortable" width="100%" >

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>Timestamp</h3></th>
                            <th align="center"><h3>Latitude</h3></th>
                            <th align="center"><h3>Longitude</h3></th>
                            <th align="center"><h3>Location</h3></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                    
                      
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>">1</td>
                                <td  width="30" class="<%=classText%>">TN 09 4568</td>
                                <td width="30" class="<%=classText%>"  >2015-08-20 13:00:00</td>
                                <td  width="30" class="<%=classText%>">13.08</td>
                                <td width="30" class="<%=classText%>">80.27</td>
                                <td width="30" class="<%=classText%>" >Toll Palza,chennai</td>
                               
                            </tr>
                            <tr>
                                <td class="<%=classText%>">1</td>
                                <td  width="30" class="<%=classText%>">TN 09 4568</td>
                                <td width="30" class="<%=classText%>"  >2015-08-20 13:30:00</td>
                                <td  width="30" class="<%=classText%>">13.20</td>
                                <td width="30" class="<%=classText%>">79.11</td>
                                <td width="30" class="<%=classText%>" >Chittoor,AP</td>

                            </tr>
                            <tr>
                                <td class="<%=classText%>">1</td>
                                <td  width="30" class="<%=classText%>">TN 09 4568</td>
                                <td width="30" class="<%=classText%>"  >2015-08-20 14:00:00</td>
                                <td  width="30" class="<%=classText%>">12.92</td>
                                <td width="30" class="<%=classText%>">79.133</td>
                                <td width="30" class="<%=classText%>" >vellore,TN</td>

                            </tr>
                            <tr>
                                <td class="<%=classText%>">1</td>
                                <td  width="30" class="<%=classText%>">TN 09 4568</td>
                                <td width="30" class="<%=classText%>"  >2015-08-20 14:30:00</td>
                                <td  width="30" class="<%=classText%>">12.92</td>
                                <td width="30" class="<%=classText%>">79.133</td>
                                <td width="30" class="<%=classText%>" >vellore,TN</td>

                            </tr>
                            <tr>
                                <td class="<%=classText%>">1</td>
                                <td  width="30" class="<%=classText%>">TN 09 4568</td>
                                <td width="30" class="<%=classText%>"  >2015-08-20 15:00:00</td>
                                <td  width="30" class="<%=classText%>">12.92</td>
                                <td width="30" class="<%=classText%>">79.133</td>
                                <td width="30" class="<%=classText%>" >vellore,TN</td>

                            </tr>

                      
                    </tbody>
                </table>
d 
           

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

