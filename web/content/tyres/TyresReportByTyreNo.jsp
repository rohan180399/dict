<%--
    Document   : TyresReport
    Created on : Mar 19, 2012, 3:31:22 PM
    Author     : Senthil
--%>

<%@page contentType="text/html" import="java.sql.*,java.util.Iterator,java.util.ArrayList,java.text.DecimalFormat,ets.domain.tyres.business.TyresTO" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Tyres Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        function viewReport() {
            var err = "";
            if(document.getElementById("tyreNumber").value == ""){
                err = err+"Tyre No is Not Filled \n";
                
            }
            if(err == ""){
                document.tyreReportVehicle.action = "/throttle/tyresReportByTyreNo.do";
                document.tyreReportVehicle.method="post";
                document.tyreReportVehicle.submit();
            }

        }

        function getTyreNo(){
        var oTextbox = new AutoSuggestControl(document.getElementById("tyreNumber"),new ListSuggestions("tyreNumber","/throttle/handleTyreNoSuggestions.do?"));
        //getVehicleDetails(document.getElementById("regno"));
        }

        </script>




    </head>
<body onload="getTyreNo();">
<form name="tyreReportVehicle" >

<%

String tyreNumber = (String) request.getAttribute("tyreNumber");
if(tyreNumber == null) {
    tyreNumber = "";
}

String fromDate = (String) request.getAttribute("fromDate");
if(fromDate == null){
    fromDate = "";
}

String toDate = (String) request.getAttribute("toDate");
if(toDate == null) {
    toDate = "";
}

%>

    <table align="center">
        <tr>
            <td>
 <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Tyre wise Tyres Report</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Tyre No</td><td>
                <input type="text" name="tyreNumber" id="tyreNumber" class="textbox" value="<%=tyreNumber%>" />
            </td>

            
            <td colspan="2"><input type="button" name="search" value="View List"  class="button" onclick="viewReport()" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
    <br>

    <%
int sino = 0;
int overAllKm = 0;
String toDateDisp = "";
String tdClassName = "text1";
String mfrName = "";


TyresTO tyresTO = new TyresTO();
ArrayList tyresDetails = (ArrayList) request.getAttribute("tyresDetails");
if(tyresDetails != null && tyresDetails.size() >0) {
%>

    <table width="900" align="center" cellspacing="0" cellpadding="0" class="table2">
    <tr>
        <td  class="contenthead">S.No</td>
        <td  class="contenthead">Vehicle No</td>
        <td  class="contenthead">Position</td>
        <td  class="contenthead">Make</td>
        <td  class="contenthead">In Date</td>
        <td  class="contenthead">Out Date</td>
        <td  class="contenthead">Type</td>
        <td  class="contenthead">Active Status</td>
        <td  class="contenthead">In KM</td>
        <td  class="contenthead">Out KM</td>
        <td  class="contenthead">Total KM</td>
    </tr>

    
    <%
    Iterator it = tyresDetails.iterator();
    String type = "";
    String fromKm = "";
    String toKm = "";
    String totalKm = "";
    while(it.hasNext()){
        tyresTO = (TyresTO) it.next();
        mfrName = tyresTO.getItemName();
        type = tyresTO.getRcType();
        fromKm = tyresTO.getFromKm();
        toKm = tyresTO.getToKm();
        if("NA".equals(toKm)){
           totalKm = "NA";
        }else{
            totalKm = "" + (Integer.parseInt(toKm) - Integer.parseInt(fromKm));
        }
        if("N".equals(type)){
            type = "New";
        }else {
            type = "RC";
        }
        /*
        if(mfrName != null ) {
            mfrName = mfrName.substring(0,mfrName.indexOf(" "));
        }
        */
        sino++;
        if((sino%2) == 0){
            tdClassName = "text2";
        } else {
            tdClassName = "text1";
        }
        toDateDisp = tyresTO.getToDate();
        if(toDateDisp != null && toDateDisp.equals("00-00-0000")){
            toDateDisp = "Till Date";
        }
        if(totalKm != null && !totalKm.equals("NA")){
            overAllKm += Integer.parseInt(totalKm);
        }
    %>

    <tr>
        <td  class="<%=tdClassName%>"><%=sino%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getVehicleRegNo()%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getTyrePosition()%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getItemName()%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getFromDate()%></td>
        <td  class="<%=tdClassName%>"><%=toDateDisp%></td>
        <td  class="<%=tdClassName%>"><%=type%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getActive()%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getFromKm()%></td>
        <td  class="<%=tdClassName%>"><%=tyresTO.getToKm()%></td>
        <td  class="<%=tdClassName%>"><%=totalKm%></td>
    </tr>

<%
}
%>
    </table>
</td>
        </tr>

        <tr><td><h2>Summary</h2></td> </tr>
        <tr><td>
             <table align="left" width="250" cellspacing="0" cellpadding="2">
                <tr>
                    <td>Overall KM Run</td>
                    <td><%=overAllKm%></td>
                </tr>
             </table>
    </td>
    </tr>
</table>


<%
}else{
%>
<br>
<br>
<font color="red" size="3"> Tyre is not used yet (or) invalid Tyre no </font>
<%
}
%>        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
