<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveOrder.do?param=' + value;
            document.upload.submit();
        } else {
            var file = document.upload.importBpclTransaction.value;
            if (file == '') {
                alert("Please select any Excel");
                return false;
            } else {
                document.upload.action = '/throttle/orderUpload.do?param=' + value;
                document.upload.submit();
            }
        }
    }
</script>

<html>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <c:if test="${orderUploadListSize <= 0}">
                        <form name="upload" method="post" enctype="multipart/form-data">
                        </c:if>
                        <c:if test="${orderUploadListSize > 0}">
                            <form name="upload" method="post"> 
                            </c:if>
                            <%@ include file="/content/common/message.jsp" %>
                            <br>
                            <br>

                            <%
                            if(request.getAttribute("errorMessage")!=null){
                            String errorMessage=(String)request.getAttribute("errorMessage");                
                            %>
                            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                                    <%}%>
                                    <c:if test="${orderUploadListSize <= 0}">
                                <table class="table table-info mb30 table-hover" style="width:50%">
                                    <tr>
                                        <td style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:20px;" colspan="2">Order Upload</td>
                                    </tr>
                                    <tr>
                                        <td class="text2"><font size="4">Select Order Excel</font></td>
                                        <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction">
                                            <br>
                                            <a href="Dict_Sample_Order.xls"><font color="red" size="4">Sample XLS</font></a></td>                             
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text1" align="center" ><input type="button" class="btn btn-info" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                                    </tr>
                                </table>
                            </c:if>
                            <br>
                            <br>
                            <br>
                            <% int i = 1 ;%>
                            <% int index = 0;%> 
                            <%int oddEven = 0;%>
                            <%  String classText = "";%>    
                            <c:if test="${orderUploadListSize > 0}">
                                <table class="table table-info mb30 table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="20">Order Details&nbsp;</th>
                                        </tr>                                        
                                        <tr>
                                            <th >S No</th>
                                            <th >Customer Name</th>
                                            <th >Billing Party</th>
                                            <th >Consignee</th>
                                            <th >Movement Type</th>
                                            <th >Shipping Line</th>
                                            <th >Vehicle Type</th>
                                            <th >ISO/DSO</th>
                                            <th >FS/MT REPO</th>
                                            <th >Handling</th>
                                            <th >Cargo</th>
                                            <th >Pickup Location</th>
                                            <th >Point1</th>
                                            <th >Point2</th>
                                            <th >Point3</th>
                                            <th >Point4</th>
                                            <th >To Location</th>
                                            <th >Todays Booking 20'</th>
                                            <th >Todays booking 40'</th>
                                            <th >Result</th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${orderUploadList}" var="bpcl">
                                        <%
                                      oddEven = index % 2;
                                      if (oddEven > 0) {
                                          classText = "text2";
                                      } else {
                                          classText = "text1";
                                        }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>" ><%=i++%></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="customerName" id="customerName" value="<c:out value="${bpcl.customerName}"/>" /><c:out value="${bpcl.customerName}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="billingParty" id="billingParty" value="<c:out value="${bpcl.billingParty}"/>" /><c:out value="${bpcl.billingParty}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="consigneeName" id="consigneeName" value="<c:out value="${bpcl.consigneeName}"/>" /><c:out value="${bpcl.consigneeName}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="movementType" id="movementType" value="<c:out value="${bpcl.movementType}"/>" /><c:out value="${bpcl.movementType}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="shipingLineNo" id="shipingLineNo" value="<c:out value="${bpcl.shipingLineNo}"/>" /><c:out value="${bpcl.shipingLineNo}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${bpcl.vehicleTypeName}"/>" /><c:out value="${bpcl.vehicleTypeName}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="iso" id="iso" value="<c:out value="${bpcl.iso}"/>" /><c:out value="${bpcl.iso}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="isRepo" id="isRepo" value="<c:out value="${bpcl.isRepo}"/>" /><c:out value="${bpcl.isRepo}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="handling" id="handling" value="<c:out value="${bpcl.handling}"/>" /><c:out value="${bpcl.handling}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="cargo" id="cargo" value="<c:out value="${bpcl.cargo}"/>" /><c:out value="${bpcl.cargo}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="pickupLocation" id="pickupLocation" value="<c:out value="${bpcl.pickupLocation}"/>" /><c:out value="${bpcl.pickupLocation}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="point1Name" id="point1Name" value="<c:out value="${bpcl.point1Name}"/>" /><c:out value="${bpcl.point1Name}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="point2Name" id="point2Name" value="<c:out value="${bpcl.point2Name}"/>" /><c:out value="${bpcl.point2Name}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="point3Name" id="point3Name" value="<c:out value="${bpcl.point3Name}"/>" /><c:out value="${bpcl.point3Name}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="point4Name" id="point4Name" value="<c:out value="${bpcl.point4Name}"/>" /><c:out value="${bpcl.point4Name}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="toCityName" id="toCityName" value="<c:out value="${bpcl.toCityName}"/>" /><c:out value="${bpcl.toCityName}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="todayBooking20" id="todayBooking20" value="<c:out value="${bpcl.todayBooking20}"/>" /><c:out value="${bpcl.todayBooking20}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="todayBooking40" id="todayBooking40" value="<c:out value="${bpcl.todayBooking40}"/>" /><c:out value="${bpcl.todayBooking40}"/></font></td>
                                            <td class="<%=classText%>" ><input type="hidden" name="flag" id="flag" value="<c:out value="${bpcl.flag}"/>" /></font>                                            
                                                <c:if test="${bpcl.flag == 0}">
                                                    <font size="3" color="green"><c:out value="Proceed"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 1}">
                                                    <font size="2" color="red"><c:out value="CustomerName is Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 2}">
                                                    <font size="2" color="red"><c:out value="BillingParty Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 3}">
                                                    <font size="2" color="red"><c:out value="ConsigneeName Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 4}">
                                                    <font size="2" color="red"><c:out value="PickupLocation Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 5}">
                                                    <font size="2" color="red"><c:out value="DropLocation Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 6}">
                                                    <font size="2" color="red"><c:out value="20FT Contract Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 7}">
                                                    <font size="2" color="red"><c:out value="VehicleType Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 8}">
                                                    <font size="2" color="red"><c:out value="MovementType InValid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 9}">
                                                    <font size="2" color="red"><c:out value="Shipper Invalid"/></font>
                                                </c:if>
                                                <c:if test="${bpcl.flag == 10}">
                                                    <font size="2" color="red"><c:out value="40FT Contract Invalid"/></font>
                                                </c:if>
                                                <br>
                                                <input type="hidden" name="pickupId" id="pickupId" value="<c:out value="${bpcl.fromCityName}"/>" />
                                                <input type="hidden" name="cust_id" id="cust_id" value="<c:out value="${bpcl.customerId}"/>" />
                                                <input type="hidden" name="cityTo" id="cityTo" value="<c:out value="${bpcl.cityTo}"/>" />
                                                <input type="hidden" name="contractTypeId" id="contractTypeId" value="<c:out value="${bpcl.contractTypeId}"/>" />
                                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${bpcl.vehicleId}"/>" />
                                            </td>
                                        </tr>
                                        <%index++;%>
                                    </c:forEach>
                                </table>
                                <br>
                                <br>
                                <br>
                                <center>
                                    <input type="button"  class="btn btn-info" value="Proceed" onclick="submitPage(this.value)"/>
                                </center>

                            </c:if>
                            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>