

<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.contract.business.ContractTO" %>  
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<%@ page import="ets.domain.service.business.ServiceTO" %>  
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.PeriodicService" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class="active"><spring:message code="serviceplan.label.PeriodicService" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body>
<form name="manageMaintenance" method="post">
        


<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>
<%
ArrayList maintenanceDetails = (ArrayList) request.getAttribute("maintenanceDetails"); 
     if(maintenanceDetails.size() != 0){
%>
<c:if test = "${maintenanceDetails != null}" >
    
      <td  colspan="4"  style="border-color:#5BC0DE;padding:16px;">
  <table class="table table-info mb30 table-hover" id="table">
      <thead>
<tr>
<th width="45" height="30" ><div ><spring:message code="serviceplan.label.SNo" text="default text"/></div></th>
<th width="172" height="30" ><div ><spring:message code="serviceplan.label.Maintenance" text="default text"/></div></th>
<th width="158" height="30" ><div ><spring:message code="serviceplan.label.Description" text="default text"/></div></th>
<th align="center" width="76" height="30" ><div ><spring:message code="serviceplan.label.Status" text="default text"/></div></th>
</tr>
      </thead>
<% 
int index=0; 
%>

      <c:forEach items="${maintenanceDetails}" var="service"> 
<%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td  height="30" ><%=index+1%></td>

<td  height="30" ><c:out value="${service.serviceName}"/></td>
<td  height="30" ><c:out value="${service.desc}"/> </td>
<td align="center"  height="30" ><c:out value="${service.status}"/>
<%index++;%>
</c:forEach>

</table>
</c:if>
<center>
<input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.ADD" text="default text"/>" name="add" onClick="submitPage(this.name)">
<input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.ALTER" text="default text"/>" name="alter" onClick="submitPage(this.name)">
<input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.CONFIG" text="default text"/>" name="config" onClick="submitPage(this.name)">
</center>
<%
}else{
%>
<center>
<input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.ADD" text="default text"/>" name="add" onClick="submitPage(this.name)">
</center>
<%
}
%>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
<script language="javascript">
function submitPage(value){
if(value == "add"){
document.manageMaintenance.action = '/throttle/addMaintenance.do';
document.manageMaintenance.submit();
}else if(value == 'alter'){
document.manageMaintenance.action = '/throttle/alterMaintenance.do';
document.manageMaintenance.submit();
}else if(value == 'config'){
document.manageMaintenance.action = '/throttle/configMaintenance.do';
document.manageMaintenance.submit();
}
}
</script>

</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>