/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.billing.web;

/**
 *
 * @author vinoth
 */
public class BillingCommand {

    
    private String customerId = "";
    private String fromDate = "";
    private String toDate = "";

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
    
    
    
}
