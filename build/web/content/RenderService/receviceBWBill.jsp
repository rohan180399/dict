<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>  
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %>  
        <%@ page import="ets.domain.operation.business.OperationTO" %>  
    </head>
    <script>

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function computeTotal(){
            document.generateMrs.totalAmount.value =
                    parseFloat(document.generateMrs.sparesAmount.value) +
                    parseFloat(document.generateMrs.vatAmount.value) +
                    parseFloat(document.generateMrs.serviceAmount.value) +
                    parseFloat(document.generateMrs.serviceTaxAmount.value);
            document.generateMrs.totalAmount.value = parseFloat(document.generateMrs.totalAmount.value).toFixed(2);
        }
        function generateMRS(name)
        {
            if(name=='generate'){
                if(textValidation(document.generateMrs.invoiceNo,'Invoice No'));        
                else if(floatValidation(document.generateMrs.totalAmount,'Total Amount'));        
                else{
                    document.generateMrs.action='/throttle/generateBWBill.do';
                    document.generateMrs.submit();
                }
            }else if(name=='search'){
            if(isSelect(document.generateMrs.mrsJobCardNumber,'Work Order No '))
                {}else{
                    document.generateMrs.action='/throttle/searchBWBill.do';
                    document.generateMrs.submit();                
                }
            }
            
        }
        function setFocus()
        {
            var jobcardId='<%=request.getAttribute("jcId")%>';
            var woNo ='<%=request.getAttribute("woNo")%>';                      
            if(jobcardId!='null'){
                document.generateMrs.jcId.value=jobcardId;               
            }
            getWoList();
            
        }
        
        function setWoId()
        {
            var woNo ='<%=request.getAttribute("woNo")%>';             
            if(woNo!='null'){                               
                document.generateMrs.mrsJobCardNumber.value = woNo;
            }            
        }    
        
        
        var httpRequest;          
        function getWoList() { 
            if(document.generateMrs.jcId.value != '0'){
                var url = '/throttle/getContractWoList.do?jcNo='+document.generateMrs.jcId.value;
                
                if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest = new XMLHttpRequest();
                        }
                        httpRequest.open("POST", url, true);
                        httpRequest.onreadystatechange = function() {go1(); } ;
                        httpRequest.send(null);
                    }    
                }
                
                
                function go1() {                         
                    if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                            var response = httpRequest.responseText;                               
                            if(response!=""){                        
                                setOptions( response ,document.generateMrs.mrsJobCardNumber );                                
                            }
                        }
                    }
                    setWoId();
                }
                
                
    </script>
    
    
    
    <body onload="setFocus();">
        <form name="generateMrs" method="post" >
            
            
            <%@ include file="/content/common/path.jsp" %>
            
            <!-- pointer table -->
            
            <!-- message table -->

<%@ include file="/content/common/message.jsp"%>


            <c:if test = "${jobCardlist != null}" >



    <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:800;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Generate Body Works Bill</li>
    </ul>
    <div id="first">
    <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
            <td align="left" height="30">Job Card No</td>
            <td><select class='form-control' id='jcId'  name='jcId' onChange="getWoList();" >
                <option selected   value='0' >---Select---</option>
                <c:forEach items="${jobCardlist}" var="mfr">
                    <option  value='<c:out value="${mfr.jobCardId}" />'>
                    <c:out value="${mfr.jobCardId}" />
                </c:forEach >
            </select>  </td>
            <td align="left" height="30"><font color="red">*</font>Work Order No</td>
            <td height="30"><select class='form-control' id='mrsJobCardNumber'  name='mrsJobCardNumber' >
                <option value='0' >---Select---</option>
            </select></td>
        <td><input type="button" class="button" value="Go" name="search" onClick="generateMRS(this.name);"  />   </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

            </c:if>

            <br>
            <c:if test = "${vendorList != null}" >
                <div  id="tabVisible" >
                    <%
            int index = 0;
                    %>
                    
                          
                    <table align="center"  width=50%" border="0" cellspacing="0" cellpadding="0" class="border">
                        
                        <tr>
                            <td colspan="2" class="contenthead" height="30"><div class="contenthead">Body Work Bill Details</div></td>
                        </tr>
                        
                        <c:forEach items="${vendorList}" var="service"> 
                            <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                            %>
                            <tr>
                            <tr>
                                <td class="text1" height="30">Job Card No</td>                    
                                <td  height="30" class="text1"><c:out value="${service.jobCardId}"/></td>
                            </tr>
                            
                            <tr>
                                <td class="text2" height="30">Vendor Name</td>
                                <td  height="30" class="text2"><c:out value="${service.vendorName}"/> </td>
                            </tr>
                            <tr>
                                <td class="text1" height="30">Invoice No</td>
                                <td class="text1" height="30"> <input name="invoiceNo" type="text" class="form-control" value=""></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30">Spares Amount</td>
                                <td class="text2" height="30"> <input name="sparesAmount" type="text"  onChange="computeTotal();" class="form-control" value="0"></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30">VAT Amount</td>
                                <td class="text1" height="30"> <input name="vatAmount" type="text" onChange="computeTotal();" class="form-control" value="0"></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30">Service Amount</td>
                                <td class="text2" height="30"> <input name="serviceAmount" type="text"  onChange="computeTotal();" class="form-control" value="0"></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30">Service Tax Amount</td>
                                <td class="text1" height="30"> <input name="serviceTaxAmount" type="text"  onChange="computeTotal();" class="form-control" value="0"></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30">Total Amount</td>
                                <td class="text2" height="30"> <input name="totalAmount" type="text" readonly class="form-control" value="0"></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30">Bill Remarks</td>
                                <td class="text1" height="30"> <textarea name="remarks" rows="5" cols="30"><c:out value="${service.remarks}"/> </textarea></td>
                            </tr>
                            
                            <%index++;%>
                        </c:forEach>
                        <tr>                                              
                    </table>
                    <br>
                </div>

                <center>
                    <input type="button" class="button" value="Generate Bill" name="generate" onClick="generateMRS(this.name);">
                </center>
            </c:if>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
