<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleVehicleOdometerReadingExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{   
                        document.BPCLTransaction.action = '/throttle/handleVehicleOdometerReadingExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>

 <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">العربية</a>
	  </span>-->


<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.VehicleLastknownReadingReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="header.label.Reports" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.VehicleLastknownReadingReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="BPCLTransaction" method="post">
           
            <table class="table table-bordered" id="report" >
                      
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate" style="width:250px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" style="width:250px;height:40px;" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                      <td><font color="red">*</font><spring:message code="operations.reports.label.VehicleType" text="default text"/></td>
                                      <td height="30"><select class="form-control" name="vehicletype" style="width:250px;height:40px;" id="vehicletype" type="text" >
                                              <option value="2"><spring:message code="operations.reports.label.Primary" text="default text"/> </option>
                                              <option value="1"><spring:message code="operations.reports.label.Secondary" text="default text"/></option>
                                          </select></td>

                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">&nbsp;&nbsp;
                                        <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>"></td>
                                    </tr>

                                </table>
                    </td>
            </table>

            <c:if test="${vehicleReadingDetails!= null}">
                <table class="table table-info mb30 table-hover" id="table" class="sortable" width="100%" >

                    <thead>
                        <tr height="50">
                            <th align="center"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.Vehicle" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.TripCode" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.KMReading" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.KMReadingDate" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.HmReading" text="default text"/></th>
                            <th align="center"><spring:message code="operations.reports.label.HmReadingDate" text="default text"/></th>
                                                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${vehicleReadingDetails}" var="BPCLTD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.vehicleNo}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.tripCode}"/></td>
                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.kmReading}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.kmReadingDate}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.hmReading}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.hmReadingDate}"/></td>
                                                               
                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

               
                                    <script language="javascript" type="text/javascript">
               function viewTripDetails(tripIds) {
            //alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitMergingDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
            </script>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

