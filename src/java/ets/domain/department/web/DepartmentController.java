package ets.domain.department.web;

/**
 *
 * @author vinoth
 */
import ets.arch.web.BaseController;
import ets.domain.department.business.DepartmentBP;
import ets.domain.department.business.DepartmentTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import java.io.*;

public class DepartmentController extends BaseController {

    DepartmentCommand departmentcommand;
    DepartmentBP departmentBP;
    LoginBP loginBP;

    public DepartmentBP getDepartmentBP() {
        return departmentBP;
    }

    public void setDepartmentBP(DepartmentBP departmentBP) {
        this.departmentBP = departmentBP;
    }

    public DepartmentCommand getDepartmentCommand() {
        return departmentcommand;
    }

    public void setDepartmentcommand(DepartmentCommand departmentcommand) {
        this.departmentcommand = departmentcommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

    public ModelAndView handleAddDepartmentPage(HttpServletRequest request, HttpServletResponse reponse, DepartmentCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "HRMS >>Manage Department  >> Add Department ";
        String pageTitle = "Add Department";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Department-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/Department/addDepartment.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Add  Department Page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handleAddDepartment(HttpServletRequest request, HttpServletResponse response, DepartmentCommand command) throws IOException {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        int status = 0;
//        String path = "";
//        departmentcommand = command;
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList departmentList = new ArrayList();
//        DepartmentTO departmentTO = new DepartmentTO();
//        String menuPath = "";
//        menuPath = "HRMS >>Manage Department";
//        String pageTitle = "Manage Department";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//
//            if (departmentcommand.getName() != null && departmentcommand.getName() != "") {
//                departmentTO.setName(departmentcommand.getName());
//            }
//            if (departmentcommand.getDescription() != null && departmentcommand.getDescription() != "") {
//                departmentTO.setDescription(departmentcommand.getDescription());
//            }
//            path = "content/Department/manageDepartment.jsp";
//            status = departmentBP.processInsertDepartment(departmentTO, userId);
//            departmentList = departmentBP.getDepartmentList();
//            request.setAttribute("DepartmentLists", departmentList);
//            if (status > 0) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Department Added Successfully");
//            }
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Add Department " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public ModelAndView handleViewDepartment(HttpServletRequest request, HttpServletResponse response, DepartmentCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String rollNumber = "";
        String path = "";
        HttpSession session = request.getSession();
        departmentcommand = command;
        String menuPath = "";
        menuPath = "HRMS >>Manage Department";
        String pageTitle = "View Department";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Department-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute("pageTitle", pageTitle);
                ArrayList departmentList = new ArrayList();
                path = "content/Department/manageDepartment.jsp";
                System.out.println("hiiiiiiiiiiiiiiiiiii");
                departmentList = departmentBP.getDepartmentList();
                request.setAttribute("DepartmentLists", departmentList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Department --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewAlterScreen(HttpServletRequest request, HttpServletResponse response, DepartmentCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        departmentcommand = command;
        String menuPath = "";
        menuPath = "HRMS >>Manage Department >>Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Department-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Alter Department";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList departmentList = new ArrayList();
                path = "content/Department/alterDepartment.jsp";
                departmentList = departmentBP.getDepartmentList();
                request.setAttribute("DepartmentLists", departmentList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Department --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handleUpdateDepartment(HttpServletRequest request, HttpServletResponse reponse, DepartmentCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        departmentcommand = command;
//        String path = "";
//        try {
//            String menuPath = "HRMS  >>  Manage Department  >>  View";
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            int index = 0;
//            int update = 0;
//            String[] ids = departmentcommand.getDepartmentIds();
//            String[] names = departmentcommand.getNames();
//            String[] descs = departmentcommand.getDescriptions();
//            String[] activeStatus = departmentcommand.getStats();
//            String[] selectedIndex = departmentcommand.getSelectedIndex();
//            int userId = (Integer) session.getAttribute("userId");
//
//            ArrayList List = new ArrayList();
//            DepartmentTO departmentTO = null;
//            for (int i = 0; i < selectedIndex.length; i++) {
//                departmentTO = new DepartmentTO();
//                index = Integer.parseInt(selectedIndex[i]);
//                departmentTO.setDepartmentId((ids[index]));
//                departmentTO.setName(names[index]);
//                departmentTO.setDescription(descs[index]);
//                departmentTO.setStatus(activeStatus[index]);
//                List.add(departmentTO);
//            }
//            path = "content/Department/manageDepartment.jsp";
//            update = departmentBP.processUpdatetDepartment(List, userId);
//            String pageTitle = "Manage Department";
//            request.setAttribute("pageTitle", pageTitle);
//            ArrayList departmentList = new ArrayList();
//            departmentList = departmentBP.getDepartmentList();
//            request.setAttribute("DepartmentLists", departmentList);
//
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Department Details Modified Successfully");
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Modify Department Details  --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//    
    
public ModelAndView handleAddDepartment(HttpServletRequest request, HttpServletResponse response, DepartmentCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        departmentcommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList departmentList = new ArrayList();
        DepartmentTO departmentTO = new DepartmentTO();
        String menuPath = "";
        menuPath = "HRMS >>Manage Department";
        String pageTitle = "Manage Department";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (departmentcommand.getName() != null && departmentcommand.getName() != "") {
                departmentTO.setName(departmentcommand.getName());
            }
            if (departmentcommand.getDescription() != null && departmentcommand.getDescription() != "") {
                departmentTO.setDescription(departmentcommand.getDescription());
            }
            if (departmentcommand.getToMail() != null && departmentcommand.getToMail() != "") {
                departmentTO.setToMail(departmentcommand.getToMail());
            }
            if (departmentcommand.getCcMail() != null && departmentcommand.getCcMail() != "") {
                departmentTO.setCcMail(departmentcommand.getCcMail());
            }
            path = "content/Department/manageDepartment.jsp";
            status = departmentBP.processInsertDepartment(departmentTO, userId);
            departmentList = departmentBP.getDepartmentList();
            request.setAttribute("DepartmentLists", departmentList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Department Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add Department " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }










    public ModelAndView handleUpdateDepartment(HttpServletRequest request, HttpServletResponse reponse, DepartmentCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        departmentcommand = command;
        String path = "";
        try {
            String menuPath = "HRMS  >>  Manage Department  >>  View";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int index = 0;
            int update = 0;
            String[] ids = departmentcommand.getDepartmentIds();
            String[] names = departmentcommand.getNames();
            String[] descs = departmentcommand.getDescriptions();
            String[] toMailss = departmentcommand.getToMails();
            String[] ccMailss = departmentcommand.getCcMails();
            String[] activeStatus = departmentcommand.getStats();
            String[] selectedIndex = departmentcommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            DepartmentTO departmentTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                departmentTO = new DepartmentTO();
                index = Integer.parseInt(selectedIndex[i]);
                departmentTO.setDepartmentId((ids[index]));
                departmentTO.setName(names[index]);
                departmentTO.setDescription(descs[index]);
                departmentTO.setToMail(toMailss[index]);
                departmentTO.setCcMail(ccMailss[index]);
                departmentTO.setStatus(activeStatus[index]);
                List.add(departmentTO);
            }
            path = "content/Department/manageDepartment.jsp";
            update = departmentBP.processUpdatetDepartment(List, userId);
            String pageTitle = "Manage Department";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList departmentList = new ArrayList();
            departmentList = departmentBP.getDepartmentList();
            request.setAttribute("DepartmentLists", departmentList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Department Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Modify Department Details  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


}
