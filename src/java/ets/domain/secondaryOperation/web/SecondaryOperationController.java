/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.web;

import ets.domain.customer.business.CustomerBP;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.SendMail;
import ets.arch.web.BaseController;
import ets.domain.customer.business.CustomerTO;
import ets.domain.customer.web.CustomerCommand;
import ets.domain.operation.business.OperationBP;
import ets.domain.mis.business.MisBP;
import ets.domain.mis.business.MisTO;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.vehicle.business.VehicleTO;      
import ets.domain.operation.business.OperationTO;
import ets.domain.operation.web.OperationCommand;
import ets.domain.secondaryOperation.business.SecondaryOperationBP;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.users.business.LoginBP;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Arul
 */
public class SecondaryOperationController extends BaseController {

    SecondaryOperationBP secondaryOperationBP;
    OperationBP operationBP;
    TripBP tripBP;
    SecondaryOperationCommand secondaryOperationCommand;
    CustomerBP customerBP;
    VehicleBP vehicleBP;
    MisBP misBP;
    LoginBP loginBP;

    public MisBP getMisBP() {
        return misBP;
    }

    public void setMisBP(MisBP misBP) {
        this.misBP = misBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public SecondaryOperationBP getSecondaryOperationBP() {
        return secondaryOperationBP;
    }

    public void setSecondaryOperationBP(SecondaryOperationBP secondaryOperationBP) {
        this.secondaryOperationBP = secondaryOperationBP;
    }

    public SecondaryOperationCommand getSecondaryOperationCommand() {
        return secondaryOperationCommand;
    }

    public void setSecondaryOperationCommand(SecondaryOperationCommand secondaryOperationCommand) {
        this.secondaryOperationCommand = secondaryOperationCommand;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        //////System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();
        initialize(request);

    }

    public ModelAndView createSecondaryCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            //path = "content/secondaryOperation/secondaryContractRouteMaster.jsp";
            path = "content/secondaryOperation/secondaryContract.jsp";
            String customerId = request.getParameter("customerId");
            String customerName = request.getParameter("customerName");
            String billType = request.getParameter("billType");
            request.setAttribute("billType", billType);
            request.setAttribute("customerId", customerId);
            request.setAttribute("customerName", customerName);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = operationBP.getMileageConfigList(customerId);
            request.setAttribute("mileageConfigList", mileageConfigList);
            String currentDieselFuelPrice = "";
            currentDieselFuelPrice = operationBP.getCurrentFuelPrice();
            request.setAttribute("currentDieselFuelPrice", currentDieselFuelPrice);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }
            ArrayList humanResourceList = new ArrayList();
            humanResourceList = secondaryOperationBP.getHumanResourceList();
            request.setAttribute("humanResourceList", humanResourceList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createSecondaryCustomerContractRoute(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            path = "content/secondaryOperation/secondaryContractRouteMaster.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());

            String customerId = request.getParameter("customerId");
            operationTO.setCustomerId(customerId);
            ArrayList customerDetails = new ArrayList();
            customerDetails = secondaryOperationBP.getViewSecondaryCustomerdetails(operationTO);
            request.setAttribute("customerDetails", customerDetails);
            request.setAttribute("customerId", customerId);
            String currentDieselPrice = "";
            currentDieselPrice = operationBP.getCurrentFuelPrice();
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            Iterator itr = customerDetails.iterator();
            SecondaryOperationTO opTO2 = new SecondaryOperationTO();
            while (itr.hasNext()) {
                opTO2 = new SecondaryOperationTO();
                opTO2 = (SecondaryOperationTO) itr.next();
                currentFuelPrice = opTO2.getContractCngCost();
                currentDieselPrice = opTO2.getContractDieselCost();
            }
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            request.setAttribute("currentDieselPrice", currentDieselPrice);

            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = operationBP.getMileageConfigList(customerId);
            request.setAttribute("mileageConfigList", mileageConfigList);

            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getCity(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            response.setContentType("text/html");
            cityName = request.getParameter("cityName");
            operationTO.setCityName(cityName);
            userDetails = secondaryOperationBP.getCity(operationTO);
            //////System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getCityName());
                jsonObject.put("Id", operationTO.getCityId());
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getCustomerPoints(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            response.setContentType("text/html");
            cityName = request.getParameter("cityName");
            operationTO.setCityName(cityName);
            String customerId = request.getParameter("customerId");
            operationTO.setCustomerId(customerId);

            userDetails = secondaryOperationBP.getCustomerPoints(operationTO);
            //////System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getCityName());
                jsonObject.put("Id", operationTO.getCityId());
                jsonObject.put("pointType", operationTO.getPointType());
                jsonObject.put("pointAddress", operationTO.getPointAddresss());
                jsonObject.put("latitude", operationTO.getLatitude());
                jsonObject.put("longitude", operationTO.getLongitude());
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getSecondaryCustomerDetails(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String customerName = "";
            response.setContentType("text/html");
            customerName = request.getParameter("customerName");
            operationTO.setCustomerName(customerName);
            userDetails = secondaryOperationBP.getSecondaryCustomerDetails(operationTO);
            //////System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getCustomerName());
                jsonObject.put("Id", operationTO.getCustomerId());
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView saveSecondaryRoute(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
            if (secondaryOperationCommand.getRouteName() != null && !"".equals(secondaryOperationCommand.getRouteName())) {
                operationTO.setRouteName(secondaryOperationCommand.getRouteName());
            }
            if (secondaryOperationCommand.getFuelCost() != null && !"".equals(secondaryOperationCommand.getFuelCost())) {
                operationTO.setFuelCost(secondaryOperationCommand.getFuelCost());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getFixedKmPerMonth() != null && !"".equals(secondaryOperationCommand.getFixedKmPerMonth())) {
                operationTO.setFixedKmPerMonth(secondaryOperationCommand.getFixedKmPerMonth());
            }
            if (secondaryOperationCommand.getFixedReeferHours() != null && !"".equals(secondaryOperationCommand.getFixedReeferHours())) {
                operationTO.setFixedReeferHours(secondaryOperationCommand.getFixedReeferHours());
            }
            if (secondaryOperationCommand.getFixedReeferMinutes() != null && !"".equals(secondaryOperationCommand.getFixedReeferMinutes())) {
                operationTO.setFixedReeferMinutes(secondaryOperationCommand.getFixedReeferMinutes());
            }
            if (secondaryOperationCommand.getRouteValidFrom() != null && !"".equals(secondaryOperationCommand.getRouteValidFrom())) {
                operationTO.setRouteValidFrom(secondaryOperationCommand.getRouteValidFrom());
            }
            if (secondaryOperationCommand.getRouteValidTo() != null && !"".equals(secondaryOperationCommand.getRouteValidTo())) {
                operationTO.setRouteValidTo(secondaryOperationCommand.getRouteValidTo());
            }
            if (secondaryOperationCommand.getDistance() != null && !"".equals(secondaryOperationCommand.getDistance())) {
                operationTO.setDistance(secondaryOperationCommand.getDistance());
            }
            if (secondaryOperationCommand.getTotalHours() != null && !"".equals(secondaryOperationCommand.getTotalHours())) {
                operationTO.setTotalHours(secondaryOperationCommand.getTotalHours());
            }
            if (secondaryOperationCommand.getTotalMinutes() != null && !"".equals(secondaryOperationCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(secondaryOperationCommand.getTotalMinutes());
            }
            if (secondaryOperationCommand.getTotalReeferHours() != null && !"".equals(secondaryOperationCommand.getTotalReeferHours())) {
                operationTO.setTotalReeferHours(secondaryOperationCommand.getTotalReeferHours());
            }
            if (secondaryOperationCommand.getTotalReeferMinutes() != null && !"".equals(secondaryOperationCommand.getTotalReeferMinutes())) {
                operationTO.setTotalReeferMinutes(secondaryOperationCommand.getTotalReeferMinutes());
            }
            if (secondaryOperationCommand.getTotalWaitMinutes() != null && !"".equals(secondaryOperationCommand.getTotalWaitMinutes())) {
                operationTO.setTotalWaitMinutes(secondaryOperationCommand.getTotalWaitMinutes());
            }
            if (secondaryOperationCommand.getAverageKM() != null && !"".equals(secondaryOperationCommand.getAverageKM())) {
                operationTO.setAverageKM(secondaryOperationCommand.getAverageKM());
            }
            if (userId != 0) {
                operationTO.setUserId(userId);
            }
            String[] pointType = request.getParameterValues("pointType");
            String[] cityId = request.getParameterValues("cityId");
            String[] cityName = request.getParameterValues("cityName");
            String[] pointAddresss = request.getParameterValues("pointAddresss");
            String[] pointSequence = request.getParameterValues("pointSequence");
            String[] latitude = request.getParameterValues("latitude");
            String[] longitude = request.getParameterValues("longitude");
            String[] travelKm = request.getParameterValues("travelKm");
            String[] travelHour = request.getParameterValues("travelHour");
            String[] travelMinute = request.getParameterValues("travelMinute");
            String[] reeferHour = request.getParameterValues("travelHour");
            String[] reeferMinute = request.getParameterValues("travelMinute");
            String[] waitMinute = request.getParameterValues("waitMinute");
            String[] parkingCost = request.getParameterValues("parkingCost");
            String[] vehTypeId = request.getParameterValues("vehTypeId");
            String[] fuelCostPerKms = request.getParameterValues("fuelCostPerKms");
            String[] tollAmounts = request.getParameterValues("tollAmounts");
            String[] addlTollAmounts = request.getParameterValues("addlTollAmounts");
            String[] miscCostKm = request.getParameterValues("miscCost");
            String[] totExpense = request.getParameterValues("totExpense");
            String referMinute = request.getParameter("totalRefer");
            operationTO.setTotalReferMin(referMinute);
            String[] fuelCostPerMin = request.getParameterValues("fuelCostPerMin");

            int insertContractRouteMater = 0;
            int insertContractRouteDetails = 0;
            int insertContractRouteCostDetails = 0;
            insertContractRouteMater = secondaryOperationBP.insertContractRouteMater(operationTO);
            if (insertContractRouteMater > 0) {
                for (int i = 0; i < pointType.length; i++) {
                    operationTO.setPointType(pointType[i]);
                    operationTO.setCityId(cityId[i]);
                    operationTO.setCityName(cityName[i]);
                    operationTO.setPointAddresss(pointAddresss[i]);
                    operationTO.setPointSequence(pointSequence[i]);
                    operationTO.setLatitude(latitude[i]);
                    operationTO.setLongitude(longitude[i]);
                    operationTO.setTravelKm(travelKm[i]);
                    operationTO.setTravelHour(travelHour[i]);
                    operationTO.setTravelMinute(travelMinute[i]);
                    operationTO.setReeferHour(reeferHour[i]);
                    operationTO.setReeferMinute(reeferMinute[i]);
                    operationTO.setWaitMinute(waitMinute[i]);
                    insertContractRouteDetails = secondaryOperationBP.insertContractRouteDetails(operationTO, insertContractRouteMater);
                }
            }
            if (insertContractRouteMater > 0) {
                for (int i = 0; i < vehTypeId.length; i++) {
                    operationTO.setVehTypeId(vehTypeId[i]);
                    operationTO.setFuelCostPerKms(fuelCostPerKms[i]);
                    operationTO.setTollAmounts(tollAmounts[i]);
                    operationTO.setAddlTollAmounts(addlTollAmounts[i]);
                    operationTO.setParkingCost(parkingCost[i]);
                    operationTO.setMiscCostKm(miscCostKm[i]);
                    operationTO.setTotExpense(totExpense[i]);
                    operationTO.setFuelCostPerMin(fuelCostPerMin[i]);
                    insertContractRouteCostDetails = secondaryOperationBP.insertContractRouteCostDetails(operationTO, insertContractRouteMater);
                }
            }
            if (insertContractRouteMater > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Route Added Successfully....");
            }
            String secondaryRouteCode = "";
            secondaryRouteCode = secondaryOperationBP.getSecondaryRouteCode(insertContractRouteMater);
            request.setAttribute("secondaryRouteCode", secondaryRouteCode);
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = secondaryOperationBP.getMileageConfigList(secondaryOperationCommand.getCustomerId());
            request.setAttribute("mileageConfigList", mileageConfigList);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }

            TripTO tripTO = new TripTO();
            String[] values1 = request.getParameterValues("cityName");
            int len = values1.length;
            for (int i = 0; i < values1.length; i++) {
                //////System.out.println("cityName:" + values1[i]);
            }
            String pickup = values1[0];
            String finalPoint = values1[len - 2];
            //////System.out.println("pickup cityName:" + pickup);
            //////System.out.println("Drop cityName:" + finalPoint);

            String customerId = request.getParameter("customerId");
            //////System.out.println("CustomerId for To EMail:" + customerId);
            String emailInfoTo = operationBP.getSecondaryRouteApprovalPerson(customerId);
            String emailCc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            //   operationTO = new OperationTO();
            //smtp = operationTO.getSmtp();
            // emailPort = Integer.parseInt(operationTO.getPort());
            // frommailid = operationTO.getEmailId();
            // password = operationTO.getPassword();

            String activitycode = "SEC-ROUTE";
            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();

            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                String toMailId = tripTO.getTomailId();
                String emailTo = "";
            }
            emailCc = "madhand@entitlesolutions.com";

            String city = request.getParameter("cityName");
            //////System.out.println("CityNames:" + city);
            String routeName = request.getParameter("routeName");
            String customerName = request.getParameter("customerNameEmail");
            String totalKm = request.getParameter("distance");
            String totalHr = request.getParameter("totalHours");
            String totalMin = request.getParameter("totalMinutes");
            String totalWaitMin = request.getParameter("totalWaitMinutes");
            String emailFormat = "<html>"
                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th colspan='2'>Secondary Route Details</th>"
                    + "</tr>"
                    + "<tr><td>&nbsp;Route Nmae</td><td>&nbsp;&nbsp;" + routeName + "</td></tr>"
                    + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                    + "<tr><td>&nbsp;Total Km</td><td>&nbsp;&nbsp;" + totalKm + "</td></tr>"
                    + "<tr><td>&nbsp;Total Travel Hours</td><td>&nbsp;&nbsp;" + totalHr + "</td></tr>"
                    + "<tr><td>&nbsp;Total Travel Minute</td><td>&nbsp;&nbsp;" + totalMin + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Total Wait Minute</td><td>&nbsp;&nbsp;" + totalWaitMin + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Pick up</td><td>&nbsp;&nbsp;" + pickup + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Final Drop</td><td>&nbsp;&nbsp;" + finalPoint + "</td></tr>"
                    + "<tr><td colspan='2' align='center'>"
                    + "<a style='text-decoration: none' href='http://203.124.105.244:8089/throttle/saveSecondaryRouteApproval.do?mailId=" + emailInfoTo + "&userId=1081" + "&routeName=" + routeName + "&customerName=" + customerName + "&totalKm=" + totalKm + "&totalHr=" + totalHr + "&totalMin=" + totalMin + "&totalWaitMin=" + totalWaitMin + "&pickup=" + pickup + "&finalPoint=" + finalPoint + "&approvestatus=1'>Approve</a>&nbsp;|&nbsp;"
                    + "<a style='text-decoration: none' href='http://203.124.105.244:8089/throttle/saveSecondaryRouteApproval.do?mailId=" + emailInfoTo + "&userId=1081" + "&routeName=" + routeName + "&customerName=" + customerName + "&totalKm=" + totalKm + "&totalHr=" + totalHr + "&totalMin=" + totalMin + "&totalWaitMin=" + totalWaitMin + "&pickup=" + pickup + "&finalPoint=" + finalPoint + "&approvestatus=2'>Reject</a>"
                    + "</td></tr>"
                    + "</table></body></html>";

            String subject = "Secondary Route Creation Approval For Customer-" + customerName + " RouteName " + routeName;
            String content = emailFormat;
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(emailInfoTo);
            tripTO.setMailIdCc(emailCc);
            tripTO.setMailIdBcc("");
//            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, emailInfoTo, emailCc, userId).start();

            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerList();
            request.setAttribute("CustomerLists", customerList);
            path = "content/Customer/manageCustomer.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveSecondaryRouteApproval(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {

        String path = "";
        HttpSession session = request.getSession();

        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        String menuPath = "";
        int update = 0;
        String pageTitle = "Advance Approve";
        menuPath = "Finance  >> Advance Approve ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String approvestatus = request.getParameter("approvestatus");
            String routeName = request.getParameter("routeName");
            String mailId = request.getParameter("mailId");
            operationTO.setRouteName(routeName);
            operationTO.setApprovestatus(approvestatus);
            String approvalStatusText = "";
            if (userId != 0) {
                operationTO.setUserId(userId);
            }
            String getApprovedBy = secondaryOperationBP.getSecRouteApprovalBy(routeName);
            //////System.out.println("GetApprovedBy:" + getApprovedBy);
            if (getApprovedBy == null) {
                if (approvestatus.equals("1")) {
                    approvalStatusText = "Approved";
                    ;
                    update = secondaryOperationBP.UpdateSecondaryRouteApproval(routeName, mailId);

                } else {
                    approvalStatusText = "Rejected";
                    update = secondaryOperationBP.UpdateSecondaryRouteRejection(routeName, mailId);

                }
                //////System.out.println("update Route:" + update);

                String fcHeadEmailId = "";
                String customerName = request.getParameter("customerName");
                String totalKm = request.getParameter("totalKm");
                String totalHr = request.getParameter("totalHr");
                String totalMin = request.getParameter("totalMin");
                String totalWaitMin = request.getParameter("totalWaitMin");
                String pickup = request.getParameter("pickup");
                String finalPoint = request.getParameter("finalPoint");
                TripTO tripTO = new TripTO();
                ArrayList FcHeademaildetails = new ArrayList();
                FcHeademaildetails = tripBP.getSecFCmailcontactDetails(customerName);
                Iterator itr2 = FcHeademaildetails.iterator();
                if (itr2.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr2.next();
                    fcHeadEmailId = tripTO.getFcHeadEmail();
                    //////System.out.println("Secondary FCHead:" + fcHeadEmailId);
                }

                String to = tripTO.getMailIdTo();
                String cc = "naved.ahmad@brattlefoods.com";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";
                String activitycode = "SEC-ROUTE-ACK";
                ArrayList emaildetails = new ArrayList();
                emaildetails = tripBP.getEmailDetails(activitycode);
                Iterator itr1 = emaildetails.iterator();
                if (itr1.hasNext()) {
                    tripTO = new TripTO();
                    tripTO = (TripTO) itr1.next();
                    smtp = tripTO.getSmtp();
                    emailPort = Integer.parseInt(tripTO.getPort());
                    frommailid = tripTO.getEmailId();
                    password = tripTO.getPassword();

                }
                String emailFormat = "<html>"
                        + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                        + "<tr>"
                        + "<th colspan='2'><p>Hi, <b>Response For Secondary Route Approval</b></p></th>"
                        + "</tr>"
                        + "<tr><td>&nbsp;Route Nmae</td><td>&nbsp;&nbsp;" + routeName + "</td></tr>"
                        + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                        + "<tr><td>&nbsp;Total Km</td><td>&nbsp;&nbsp;" + totalKm + "</td></tr>"
                        + "<tr><td>&nbsp;Total Travel Hours</td><td>&nbsp;&nbsp;" + totalHr + "</td></tr>"
                        + "<tr><td>&nbsp;Total Travel Minute</td><td>&nbsp;&nbsp;" + totalMin + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Total Wait Minute</td><td>&nbsp;&nbsp;" + totalWaitMin + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Pick up</td><td>&nbsp;&nbsp;" + pickup + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Final Drop</td><td>&nbsp;&nbsp;" + finalPoint + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;" + approvalStatusText + "&nbsp;By</td><td>&nbsp;&nbsp;" + mailId + "</td></tr>"
                        + "<tr><td>&nbsp;&nbsp;Status</td><td>&nbsp;&nbsp;" + approvalStatusText + "</td></tr></table></body></html>";

                String subject = approvalStatusText + ": Your Secondary Route Approval Request Status For Route - " + routeName;
                String content = emailFormat;

                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(fcHeadEmailId);
                tripTO.setMailIdCc(cc);
                tripTO.setMailIdBcc("");
                tripTO.setMailId(mailId);
                new SendMail(smtp, emailPort, frommailid, password, subject, content, fcHeadEmailId, cc, userId).start();
                request.setAttribute("successMessage", "Processed Successfully");
                path = "BrattleFoods/response.html";

            } else {
                request.setAttribute("successMessage", "Request Already processe Successfully");
                path = "BrattleFoods/response2.html";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView saveSecondaryContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());

            if (secondaryOperationCommand.getFuelCost() != null && !"".equals(secondaryOperationCommand.getFuelCost())) {
                operationTO.setFuelCost(secondaryOperationCommand.getFuelCost());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }

            if (secondaryOperationCommand.getRouteValidFrom() != null && !"".equals(secondaryOperationCommand.getRouteValidFrom())) {
                operationTO.setRouteValidFrom(secondaryOperationCommand.getRouteValidFrom());
            }
            if (secondaryOperationCommand.getRouteValidTo() != null && !"".equals(secondaryOperationCommand.getRouteValidTo())) {
                operationTO.setRouteValidTo(secondaryOperationCommand.getRouteValidTo());
            }
            if (secondaryOperationCommand.getExtraKmCalculation() != null && !"".equals(secondaryOperationCommand.getExtraKmCalculation())) {
                operationTO.setExtraKmCalculation(secondaryOperationCommand.getExtraKmCalculation());
            }
            if (secondaryOperationCommand.getContractCngCost() != null && !"".equals(secondaryOperationCommand.getContractCngCost())) {
                operationTO.setContractCngCost(secondaryOperationCommand.getContractCngCost());
            }
            if (secondaryOperationCommand.getContractDieselCost() != null && !"".equals(secondaryOperationCommand.getContractDieselCost())) {
                operationTO.setContractDieselCost(secondaryOperationCommand.getContractDieselCost());
            }
            if (secondaryOperationCommand.getRateChangeOfCng() != null && !"".equals(secondaryOperationCommand.getRateChangeOfCng())) {
                operationTO.setRateChangeOfCng(secondaryOperationCommand.getRateChangeOfCng());
            }
            if (secondaryOperationCommand.getRateChangeOfDiesel() != null && !"".equals(secondaryOperationCommand.getRateChangeOfDiesel())) {
                operationTO.setRateChangeOfDiesel(secondaryOperationCommand.getRateChangeOfDiesel());
            }

            if (userId != 0) {
                operationTO.setUserId(userId);
            }

            String[] vehTypeId = request.getParameterValues("vehTypeId");
            String[] noOfVehicle = request.getParameterValues("noOfVehicle");
            String[] extraKmCharge = request.getParameterValues("extraKmCharge");
            String[] fixedKm = request.getParameterValues("fixedKm");
            String[] fixedKmCharge = request.getParameterValues("fixedKmCharge");

            int insertContractMater = 0;
            int insertContractDetails = 0;
            insertContractMater = secondaryOperationBP.insertContractMaster(operationTO);

            if (insertContractMater > 0) {
                for (int i = 0; i < vehTypeId.length; i++) {
                    operationTO.setVehTypeId(vehTypeId[i]);
                    operationTO.setNoOfVehicle(noOfVehicle[i]);
                    operationTO.setFixedKm(fixedKm[i]);
                    operationTO.setFixedKmCharge(fixedKmCharge[i]);
                    operationTO.setExtraKmCharge(extraKmCharge[i]);
                    insertContractDetails = secondaryOperationBP.insertContractDetails(operationTO, insertContractMater);
                }
            }

            String[] hrId = request.getParameterValues("hrId");
            String[] noOfPersons = request.getParameterValues("noOfPersons");
            String[] fixedAmount = request.getParameterValues("fixedAmount");

            int insertHumanResource = 0;

            if (insertContractMater > 0) {
                for (int i = 0; i < hrId.length; i++) {
                    operationTO.setHrId(hrId[i]);
                    operationTO.setNoOfPersons(noOfPersons[i]);
                    operationTO.setFixedAmount(fixedAmount[i]);
                    if (!noOfPersons[i].equals("") && !fixedAmount[i].equals("") && !hrId[i].equals("0")) {
                        insertHumanResource = secondaryOperationBP.insertHumanResourceDetails(operationTO, insertContractMater);
                    }
                }
            }

            if (insertContractMater > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Added Successfully....");
            }
            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerList();
            request.setAttribute("CustomerLists", customerList);
            path = "content/Customer/manageCustomer.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView secondaryTripShedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//        String path = "";
//        HttpSession session = request.getSession();
//        String menuPath = "";
//        String pageTitle = "Work Order Approval";
//        request.setAttribute("pageTitle", pageTitle);
//        menuPath = "Secondary Operation >> Secondary Trip Schedule";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        secondaryOperationCommand = command;
//
//        try {
//            path = "content/secondaryOperation/SecondaryTripSchedule.jsp";
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            //////System.out.println("size is  " + userFunctions.size());
//            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
//                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
//                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
//            }
//            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
//                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
//            }
//            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
//                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
//            }
//            if (secondaryOperationCommand.getNextWeek() != null && !"".equals(secondaryOperationCommand.getNextWeek())) {
//                operationTO.setNextWeek(secondaryOperationCommand.getNextWeek());
//            }
//            String nextWeek = secondaryOperationCommand.getNextWeek();
//            //////System.out.println("nextweekday = " + secondaryOperationCommand.getNextWeek());
//
//            Date today = new Date();
//            //////System.out.println("today is " + toddMMyy(today));
//
//            //
//            Calendar now = Calendar.getInstance();
//            //////System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
////                    + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
//
//            String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
//            //////System.out.println("Current day is : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
//            String dayName = strDays[now.get(Calendar.DAY_OF_WEEK) - 1];
//            int day = 0;
//            int dayStartStatus = 0;
//            int dayEndStatus = 0;
//            if ("Sunday".equals(dayName)) {
//                day = 1;
//                dayStartStatus = 0;
//                dayEndStatus = 6;
//            } else if ("Monday".equals(dayName)) {
//                day = 2;
//                dayStartStatus = 1;
//                dayEndStatus = 5;
//            } else if ("Tuesday".equals(dayName)) {
//                day = 3;
//                dayStartStatus = 2;
//                dayEndStatus = 4;
//            } else if ("Wednesday".equals(dayName)) {
//                day = 4;
//                dayStartStatus = 3;
//                dayEndStatus = 3;
//            } else if ("Thusday".equals(dayName)) {
//                day = 5;
//                dayStartStatus = 4;
//                dayEndStatus = 2;
//            } else if ("Friday".equals(dayName)) {
//                day = 6;
//                dayStartStatus = 5;
//                dayEndStatus = 1;
//            } else if ("Saturday".equals(dayName)) {
//                day = 7;
//                dayStartStatus = 6;
//                dayEndStatus = 0;
//            }
//
//            Date fromDate = DateUtils.addDays(today, -dayStartStatus);
//            //////System.out.println("FromDate =" + toddMMyy(fromDate));
//            request.setAttribute("fromDate", toddMMyy(fromDate));
//            Date toDate = DateUtils.addDays(today, dayEndStatus);
//            //////System.out.println("To Date = " + toddMMyy(toDate));
//            request.setAttribute("toDate", toddMMyy(toDate));
//
//            List<Date> dates = new ArrayList<Date>();
//            DateFormat formatter;
//            String dayOfWeek = "";
//            formatter = new SimpleDateFormat("dd-MM-yyyy");
//            Date startDate = fromDate;
//            Date endDate = toDate;
//            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
//            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
//            long curTime = startDate.getTime();
//            while (curTime <= endTime) {
//                dates.add(new Date(curTime));
//                curTime += interval;
//            }
//
//            ArrayList dateList = new ArrayList();
//            for (int i = 0; i < dates.size(); i++) {
//                operationTO = new SecondaryOperationTO();
//                Date lDate = (Date) dates.get(i);
//                String ds = formatter.format(lDate);
//                String ds1 = formatter.format(lDate);
//                ds = ds + " / " + strDays[i];
//                operationTO.setDate(ds1);
//                operationTO.setDateName(ds);
//                dateList.add(operationTO);
//                //////System.out.println(" Date is ..." + ds);
//            }
//            request.setAttribute("dateList", dateList);
//            //////System.out.println("dateList.size() = " + dateList.size());
//            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
//                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
//            }
//            ArrayList SecondaryContractRouteList = new ArrayList();
//            SecondaryContractRouteList = secondaryOperationBP.getSecondaryContractRouteMasterlist(operationTO);
//            request.setAttribute("SecondaryContractRouteList", SecondaryContractRouteList);
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public static String toddMMyy(Date day) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
        String date = formatter.format(day);
        return date;
    }

    public ModelAndView secondaryTripSheduleNext(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Secondary Operation >> Secondary Trip Schedule";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        SecondaryOperationCommand SecondaryOperationCommand = new SecondaryOperationCommand();

        try {

            path = "content/secondaryOperation/SecondaryTripSchedule.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());

            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
            }

            Date toDate = new Date();
            //////System.out.println("today is " + toddMMyy(toDate));
//            Date nextweekday = DateUtils.addDays(toDate, 7);
            //////System.out.println("nextweekday = " + toddMMyy(nextweekday));
Date nextweekday = new Date();
            //                Date nextweekday = new Date();
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
            //System.out.println(simpleDateformat.format(nextweekday));

            simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
            //System.out.println(simpleDateformat.format(nextweekday));

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(nextweekday);
            //////System.out.println("Current date : " + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format

            Calendar now = Calendar.getInstance();
            //////System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
//                    + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));

            String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
            //////System.out.println("Current day is : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
            String dayName = strDays[now.get(Calendar.DAY_OF_WEEK) - 1];

            String customerId = request.getParameter("customerId");
            int day = 0;
            int dayStartStatus = 0;
            int dayEndStatus = 0;
            if ("Sunday".equals(dayName)) {
                day = 1;
                dayStartStatus = 0;
                dayEndStatus = 6;
            } else if ("Monday".equals(dayName)) {
                day = 2;
                dayStartStatus = 1;
                dayEndStatus = 5;
            } else if ("Tuesday".equals(dayName)) {
                day = 3;
                dayStartStatus = 2;
                dayEndStatus = 4;
            } else if ("Wednesday".equals(dayName)) {
                day = 4;
                dayStartStatus = 3;
                dayEndStatus = 3;
            } else if ("Thusday".equals(dayName)) {
                day = 5;
                dayStartStatus = 4;
                dayEndStatus = 2;
            } else if ("Friday".equals(dayName)) {
                day = 6;
                dayStartStatus = 5;
                dayEndStatus = 1;
            } else if ("Saturday".equals(dayName)) {
                day = 7;
                dayStartStatus = 6;
                dayEndStatus = 0;
            }

//            Date fromDate = DateUtils.addDays(nextweekday, -dayStartStatus);
            //////System.out.println("FromDate =" + toddMMyy(fromDate));

     //       Date toDate1 = DateUtils.addDays(nextweekday, dayEndStatus);
            //////System.out.println("To Date = " + toddMMyy(toDate1));
Date toDate1 =new Date();
 Date fromDate = new Date();
            request.setAttribute("fromDate", toddMMyy(fromDate));
            request.setAttribute("toDate", toddMMyy(toDate1));

            List<Date> dates = new ArrayList<Date>();
            DateFormat formatter;
            String dayOfWeek = "";
            formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date startDate = fromDate;
            Date endDate = toDate1;
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }

            ArrayList dateList = new ArrayList();
            for (int i = 0; i < dates.size(); i++) {
                operationTO = new SecondaryOperationTO();
                Date lDate = (Date) dates.get(i);
                String ds1 = formatter.format(lDate);
                ds1 = ds1 + " / " + strDays[i];
                operationTO.setDateName(ds1);
                dateList.add(operationTO);
                //////System.out.println(" Date is ..." + ds1);
            }
            request.setAttribute("dateList", dateList);
            //////System.out.println("dateList.size() = " + dateList.size());
            operationTO.setCustomerId(customerId);
            ArrayList SecondaryContractRouteList = new ArrayList();
            SecondaryContractRouteList = secondaryOperationBP.getSecondaryContractRouteMasterlist(operationTO);
            request.setAttribute("SecondaryContractRouteList", SecondaryContractRouteList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method is used to save secondary TripShedule.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView saveSecondaryTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        String menuPath = "";
        menuPath = "Secondary Operation >> Trip Schedule";
        String pageTitle = "";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //////System.out.println("hr" + ":" + "mm" + ":00");
        ModelAndView mv = null;
        try {
            //////System.out.println("secondaryOperationCommand.getCustomerName() = " + secondaryOperationCommand.getCustomerName());
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            String passedValue = request.getParameter("passedValue");

            int insertStatus = 0;
            insertStatus = secondaryOperationBP.saveSecondaryTripSchedule(operationTO, userId, passedValue);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Route has been sucessfully scheduled");
                path = "content/secondaryOperation/viewSecondaryTripSchedule.jsp";
               // mv = handleViewSecondaryTripSchedule(request, response, command);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

//    public ModelAndView handleViewSecondaryTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
//        HttpSession session = request.getSession();
//        int status = 0;
//        String path = "";
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList routeList = new ArrayList();
//        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
//        String menuPath = "";
//        menuPath = "Secondary Operation >> View Trip Schedule ";
//        String pageTitle = "";
//        request.setAttribute("pageTitle", pageTitle);
//        String productId = request.getParameter("productId");
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        //////System.out.println("hr" + ":" + "mm" + ":00");
//        secondaryOperationCommand = command;
//        try {
//            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
//                operationTO1.setCustomerId(secondaryOperationCommand.getCustomerId());
//                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
//            }
//            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
//                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
//            }
//            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
//                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
//            }
//
//            Date today = new Date();
//            //////System.out.println("today is " + toddMMyy(today));
//            Date nextweekday = DateUtils.addDays(today, 7);
//            //System.out.println("nextweekday = " + toddMMyy(nextweekday));
//
//            //                Date nextweekday = new Date();
//            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(nextweekday);
//            //System.out.println("Current date : " + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
//
//            Calendar now = Calendar.getInstance();
//            //System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
//                    + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
//
//            String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
//            //System.out.println("Current day is : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
//            String dayName = strDays[now.get(Calendar.DAY_OF_WEEK) - 1];
//            int day = 0;
//            int dayStartStatus = 0;
//            int dayEndStatus = 0;
//            if ("Sunday".equals(dayName)) {
//                day = 1;
//                dayStartStatus = 0;
//                dayEndStatus = 6;
//            } else if ("Monday".equals(dayName)) {
//                day = 2;
//                dayStartStatus = 1;
//                dayEndStatus = 5;
//            } else if ("Tuesday".equals(dayName)) {
//                day = 3;
//                dayStartStatus = 2;
//                dayEndStatus = 4;
//            } else if ("Wednesday".equals(dayName)) {
//                day = 4;
//                dayStartStatus = 3;
//                dayEndStatus = 3;
//            } else if ("Thusday".equals(dayName)) {
//                day = 5;
//                dayStartStatus = 4;
//                dayEndStatus = 2;
//            } else if ("Friday".equals(dayName)) {
//                day = 6;
//                dayStartStatus = 5;
//                dayEndStatus = 1;
//            } else if ("Saturday".equals(dayName)) {
//                day = 7;
//                dayStartStatus = 6;
//                dayEndStatus = 0;
//            }
//
//            Date fromDate = DateUtils.addDays(today, -dayStartStatus);
//            //System.out.println("FromDate =" + toddMMyy(fromDate));
//            request.setAttribute("fromDate", toddMMyy(fromDate));
//            Date toDate = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate));
//            request.setAttribute("toDate", toddMMyy(toDate));
//
//            Date toDate1 = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate1));
//            List<Date> dates = new ArrayList<Date>();
//            DateFormat formatter;
//            String dayOfWeek = "";
//            formatter = new SimpleDateFormat("dd-MM-yyyy");
//            Date startDate = fromDate;
//            Date endDate = toDate1;
//            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
//            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
//            long curTime = startDate.getTime();
//            while (curTime <= endTime) {
//                dates.add(new Date(curTime));
//                curTime += interval;
//            }
//            ArrayList SecondaryContractRouteList = new ArrayList();
//            SecondaryContractRouteList = secondaryOperationBP.getSecondaryContractRouteMasterlist(operationTO1);
//            request.setAttribute("SecondaryContractRouteList", SecondaryContractRouteList);
//            ArrayList tripScheduleList = new ArrayList();
//            String tripSchedulStatus = "";
//            String secondaryRouteStatus = "";
//            String temp[] = null;
//            ArrayList dateList = new ArrayList();
//            Iterator itr = SecondaryContractRouteList.iterator();
//            SecondaryOperationTO operationTO2 = new SecondaryOperationTO();
//            while (itr.hasNext()) {
//                tripScheduleList = new ArrayList();
//                operationTO2 = new SecondaryOperationTO();
//                operationTO2 = (SecondaryOperationTO) itr.next();
//                for (int i = 0; i < dates.size(); i++) {
//                    operationTO = new SecondaryOperationTO();
//                    Date lDate = (Date) dates.get(i);
//                    String ds = formatter.format(lDate);
//                    String ds1 = formatter.format(lDate);
//                    ds1 = ds1 + " / " + strDays[i];
//                    operationTO.setDateName(ds1);
//                    operationTO.setDate(ds);
//                    operationTO.setCustomerId(operationTO1.getCustomerId());
//                    operationTO.setSecondaryRouteId(operationTO2.getSecondaryRouteId());
//                    tripSchedulStatus = secondaryOperationBP.getTripScheduleStatus(operationTO);
//                    if (tripSchedulStatus != null) {
//                        operationTO.setScheduleStatus(tripSchedulStatus);
//                    } else {
//                        operationTO.setScheduleStatus("N");
//                    }
//                    tripScheduleList.add(operationTO);
//                }
//                dateList.addAll(tripScheduleList);
//
//            }
//            request.setAttribute("dateList", dateList);
//            path = "content/secondaryOperation/viewSecondaryTripSchedule.jsp";
//            //System.out.println("FromDate =" + toddMMyy(fromDate));
//            //System.out.println("To Date = " + toddMMyy(toDate));
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//
//            return new ModelAndView("content/common/error.jsp");
//
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public ModelAndView secondaryTripSheduleView(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Secondary Operation >> View Trip Schedule";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList secondaryTripCustomerList = new ArrayList();
            secondaryTripCustomerList = secondaryOperationBP.getSecondaryTripCustomer(operationTO);
            request.setAttribute("secondaryTripCustomerList", secondaryTripCustomerList);
            path = "content/secondaryOperation/manageSecondaryCustomer.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSecondaryCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            path = "content/secondaryOperation/viewSecondaryContractRouteMaster.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String customerId = request.getParameter("customerId");
            operationTO.setCustomerId(customerId);
            //System.out.println("size is  " + userFunctions.size());
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = secondaryOperationBP.getMileageConfigList(customerId);
            request.setAttribute("mileageConfigList", mileageConfigList);

            String routeId = request.getParameter("secondaryRouteId");
            operationTO.setSecondaryRouteId(routeId);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = secondaryOperationBP.getContractRouteDetails(operationTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);

            ArrayList rateCostDetails = new ArrayList();
            rateCostDetails = secondaryOperationBP.getContractRateCostDetails(operationTO);
            request.setAttribute("rateCostDetails", rateCostDetails);

            ArrayList routeContractDetails = new ArrayList();
            routeContractDetails = secondaryOperationBP.getRouteContractDetails(operationTO);
            request.setAttribute("routeContractDetails", routeContractDetails);

            String currentDieselPrice = "";
            currentDieselPrice = operationBP.getCurrentFuelPrice();
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }
            ArrayList SecondaryCustomerList = new ArrayList();
            SecondaryCustomerList = secondaryOperationBP.getViewSecondaryCustomerdetails(operationTO);
            request.setAttribute("SecondaryCustomerList", SecondaryCustomerList);
            SecondaryOperationTO opTO = new SecondaryOperationTO();
            Iterator itr = SecondaryCustomerList.iterator();
            while (itr.hasNext()) {
                opTO = new SecondaryOperationTO();
                opTO = (SecondaryOperationTO) itr.next();
                currentFuelPrice = opTO.getContractCngCost();
                currentDieselPrice = opTO.getContractDieselCost();
            }
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            request.setAttribute("currentDieselPrice", currentDieselPrice);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleEditViewCustomer(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract Route >> edit / view";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            String customerId = request.getParameter("customerId");
            operationTO.setCustomerId(customerId);
            path = "content/secondaryOperation/viewSecondaryCustomerRoute.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = secondaryOperationBP.getMileageConfigList(customerId);
            request.setAttribute("mileageConfigList", mileageConfigList);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = secondaryOperationBP.getContractRouteDetails(operationTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);

            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView editSecondaryTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
//        HttpSession session = request.getSession();
//        int status = 0;
//        String path = "";
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList routeList = new ArrayList();
//        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
//        String menuPath = "";
//        menuPath = "Secondary Operation >> Trip Schedule Edit";
//        String pageTitle = "";
//        request.setAttribute("pageTitle", pageTitle);
//        String productId = request.getParameter("productId");
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        //System.out.println("hr" + ":" + "mm" + ":00");
//        secondaryOperationCommand = command;
//        try {
//            ArrayList secondaryTripCustomerList = new ArrayList();
//            secondaryTripCustomerList = secondaryOperationBP.getSecondaryTripCustomer(operationTO);
//            request.setAttribute("secondaryTripCustomerList", secondaryTripCustomerList);
//            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
//                operationTO1.setCustomerId(secondaryOperationCommand.getCustomerId());
//                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
//            }
//            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
//                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
//            }
//            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
//                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
//            }
//            //System.out.println("customerCode is " + secondaryOperationCommand.getCustomerCode());
//            //System.out.println("customerName is " + secondaryOperationCommand.getCustomerName());
//            //System.out.println("customerId is " + secondaryOperationCommand.getCustomerId());
//            Date today = new Date();
//            //System.out.println("today is " + toddMMyy(today));
//            Date nextweekday = DateUtils.addDays(today, 7);
//            //System.out.println("nextweekday = " + toddMMyy(nextweekday));
//
//            //                Date nextweekday = new Date();
//            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(nextweekday);
//            //System.out.println("Current date : " + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
//
//            Calendar now = Calendar.getInstance();
//            //System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
//                    + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
//
//            String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
//            //System.out.println("Current day is : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
//            String dayName = strDays[now.get(Calendar.DAY_OF_WEEK) - 1];
//            int day = 0;
//            int dayStartStatus = 0;
//            int dayEndStatus = 0;
//            if ("Sunday".equals(dayName)) {
//                day = 1;
//                dayStartStatus = 0;
//                dayEndStatus = 6;
//            } else if ("Monday".equals(dayName)) {
//                day = 2;
//                dayStartStatus = 1;
//                dayEndStatus = 5;
//            } else if ("Tuesday".equals(dayName)) {
//                day = 3;
//                dayStartStatus = 2;
//                dayEndStatus = 4;
//            } else if ("Wednesday".equals(dayName)) {
//                day = 4;
//                dayStartStatus = 3;
//                dayEndStatus = 3;
//            } else if ("Thusday".equals(dayName)) {
//                day = 5;
//                dayStartStatus = 4;
//                dayEndStatus = 2;
//            } else if ("Friday".equals(dayName)) {
//                day = 6;
//                dayStartStatus = 5;
//                dayEndStatus = 1;
//            } else if ("Saturday".equals(dayName)) {
//                day = 7;
//                dayStartStatus = 6;
//                dayEndStatus = 0;
//            }
//
//            Date fromDate = DateUtils.addDays(today, -dayStartStatus);
//            //System.out.println("FromDate =" + toddMMyy(fromDate));
//            request.setAttribute("fromDate", toddMMyy(fromDate));
//            Date toDate = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate));
//            request.setAttribute("toDate", toddMMyy(toDate));
//
//            Date toDate1 = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate1));
//            List<Date> dates = new ArrayList<Date>();
//            DateFormat formatter;
//            String dayOfWeek = "";
//            formatter = new SimpleDateFormat("dd-MM-yyyy");
//            Date startDate = fromDate;
//            Date endDate = toDate1;
//            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
//            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
//            long curTime = startDate.getTime();
//            while (curTime <= endTime) {
//                dates.add(new Date(curTime));
//                curTime += interval;
//            }
//            ArrayList SecondaryContractRouteList = new ArrayList();
//            SecondaryContractRouteList = secondaryOperationBP.getSecondaryContractRouteMasterlist(operationTO1);
//            request.setAttribute("SecondaryContractRouteList", SecondaryContractRouteList);
//            ArrayList tripScheduleList = new ArrayList();
//            String tripSchedulStatus = "";
//            String secondaryRouteStatus = "";
//            String temp[] = null;
//            ArrayList dateList = new ArrayList();
//            Iterator itr = SecondaryContractRouteList.iterator();
//            SecondaryOperationTO operationTO2 = new SecondaryOperationTO();
//            while (itr.hasNext()) {
//                tripScheduleList = new ArrayList();
//                operationTO2 = new SecondaryOperationTO();
//                operationTO2 = (SecondaryOperationTO) itr.next();
//                for (int i = 0; i < dates.size(); i++) {
//                    operationTO = new SecondaryOperationTO();
//                    Date lDate = (Date) dates.get(i);
//                    String ds = formatter.format(lDate);
//                    String ds1 = formatter.format(lDate);
//                    ds1 = ds1 + " / " + strDays[i];
//                    operationTO.setDateName(ds1);
//                    operationTO.setDate(ds);
//                    operationTO.setCustomerId(operationTO1.getCustomerId());
//                    operationTO.setSecondaryRouteId(operationTO2.getSecondaryRouteId());
//                    tripSchedulStatus = secondaryOperationBP.getTripScheduleStatus(operationTO);
//                    if (tripSchedulStatus != null) {
//                        operationTO.setScheduleStatus(tripSchedulStatus);
//                    } else {
//                        operationTO.setScheduleStatus("N");
//                    }
//                    tripScheduleList.add(operationTO);
//                }
//                dateList.addAll(tripScheduleList);
//
//            }
//            request.setAttribute("dateList", dateList);
//            operationTO1.setFromDate(toddMMyy(fromDate));
//            operationTO1.setToDate(toddMMyy(toDate1));
//            request.setAttribute("fromDate", toddMMyy(fromDate));
//            request.setAttribute("toDate", toddMMyy(toDate1));
//            path = "content/secondaryOperation/editSecondaryTripSchedule.jsp";
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//
//            return new ModelAndView("content/common/error.jsp");
//
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public ModelAndView updateSecondaryTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        String menuPath = "";
        menuPath = "Secondary Operation >> Trip Schedule  ";
        String pageTitle = "";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //System.out.println("hr" + ":" + "mm" + ":00");
        ModelAndView mv = null;
        secondaryOperationCommand = command;
        try {
            //System.out.println("secondaryOperationCommand.getCustomerName() = " + secondaryOperationCommand.getCustomerName());
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            String passedValue = request.getParameter("passedValue");

            int insertStatus = 0;
            insertStatus = secondaryOperationBP.updateSecondaryTripSchedule(operationTO, userId, passedValue);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Route has been sucessfully scheduled update");
                path = "content/secondaryOperation/manageSecondaryCustomer.jsp";
               // mv = handleViewSecondaryTripSchedule(request, response, command);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView allotSecondaryVehicleType(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
        SecondaryOperationTO operationTO2 = new SecondaryOperationTO();
        TripTO tripTO = new TripTO();
        String menuPath = "";
        menuPath = "Secondary Operation >> Create Trip Sheet";
        String pageTitle = "Manage Product Master";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //System.out.println("hr" + ":" + "mm" + ":00");
        ModelAndView mv = null;
        secondaryOperationCommand = command;
        try {
            //System.out.println("secondaryOperationCommand.getCustomerName() = " + secondaryOperationCommand.getCustomerName());
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getScheduleDate() != null && secondaryOperationCommand.getScheduleDate() != "") {
                operationTO.setScheduleDate(secondaryOperationCommand.getScheduleDate());
                request.setAttribute("scheduleDate", secondaryOperationCommand.getScheduleDate());
            }
            if (secondaryOperationCommand.getSecondaryRouteId() != null && secondaryOperationCommand.getSecondaryRouteId() != "") {
                operationTO.setSecondaryRouteId(secondaryOperationCommand.getSecondaryRouteId());
                request.setAttribute("secondaryRouteId", operationTO.getSecondaryRouteId());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
                request.setAttribute("customerName", operationTO.getCustomerName());
            }

            ArrayList vehicleRegNoList = new ArrayList();
            vehicleRegNoList = secondaryOperationBP.getSecondaryVehicleList(operationTO);
            request.setAttribute("vehicleRegNoList", vehicleRegNoList);

            ArrayList trailerNos = tripBP.getTrailerNos(tripTO);
            request.setAttribute("trailerNos", trailerNos);

            ArrayList secondarCustomerList = new ArrayList();
            secondarCustomerList = secondaryOperationBP.getSecondaryTripCustomer(operationTO);
            Iterator itr1 = secondarCustomerList.iterator();
            while (itr1.hasNext()) {
                operationTO1 = (SecondaryOperationTO) itr1.next();
                request.setAttribute("customerId", operationTO1.getCustomerId());
                request.setAttribute("billingTypeId", operationTO1.getBillingTypeId());
                request.setAttribute("customerName", operationTO1.getCustName());
                request.setAttribute("customerCode", operationTO1.getCustomerCode());
                request.setAttribute("customerAddress", operationTO1.getCustAddress());
                request.setAttribute("pincode", operationTO1.getPincode());
                request.setAttribute("email", operationTO1.getEmail());
                request.setAttribute("mobileNo", operationTO1.getMobileNo());
                request.setAttribute("phone", operationTO1.getPhone());
                request.setAttribute("billingTypeName", operationTO1.getBillingTypeName());
                request.setAttribute("customerType", "contract");
            }

            ArrayList orderPointDetails = new ArrayList();
            orderPointDetails = secondaryOperationBP.getRoutePointDetails(operationTO);
            request.setAttribute("orderPointDetails", orderPointDetails);
            request.setAttribute("totalPoints", orderPointDetails.size());
            Iterator itr2 = orderPointDetails.iterator();
            operationTO1 = new SecondaryOperationTO();
            String originPointName = "";
            String destinationPointName = "";
            int a = 1;
            while (itr2.hasNext()) {
                operationTO1 = (SecondaryOperationTO) itr2.next();
                if (a == 1) {
                    request.setAttribute("originPointId", operationTO1.getPointId());
                    request.setAttribute("originPointName", operationTO1.getPointName());
                    originPointName = operationTO1.getPointName();
                } else if (a == orderPointDetails.size()) {
                    request.setAttribute("destinationPointId", operationTO1.getPointId());
                    request.setAttribute("destinationPointName", operationTO1.getPointName());
                    destinationPointName = operationTO1.getPointName();
                }
                a++;
            }
            ArrayList routeMaster = new ArrayList();
            routeMaster = secondaryOperationBP.getContractRouteDetails(operationTO);
            Iterator itr3 = routeMaster.iterator();
            while (itr3.hasNext()) {
                operationTO2 = (SecondaryOperationTO) itr3.next();
                request.setAttribute("secondaryRouteCode", operationTO2.getSecondaryRouteCode());
                request.setAttribute("secondaryRouteName", operationTO2.getSecondaryRouteName());
                request.setAttribute("totalTravelKm", operationTO2.getTotalTravelKm());
                request.setAttribute("totalTravelHour", operationTO2.getTotalTravelHour());
                request.setAttribute("totalTravelMinute", operationTO2.getTotalTravelMinute());
            }
            request.setAttribute("routeMaster", routeMaster);
            //ArrayList vehicleTypeList = new ArrayList();
            //vehicleTypeList = operationBP.getVehicleTypeList();
            //request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);

            path = "content/secondaryOperation/allotVehicleType.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createSecondaryTripSheet(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        //System.out.println("userId = " + 0);
        int userId = (Integer) session.getAttribute("userId");
        //System.out.println("userId = " + userId);
        OperationCommand operationCommand = null;
        operationCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        SecondaryOperationTO operationTO4 = new SecondaryOperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Secondary Operation >>  Create Trip Sheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        RedirectView redirectView = null;
        try {
            int insertStatus = 0;
            operationTO.setEntryType("1");
            operationTO.setConsignmentDate(request.getParameter("consignmentDate"));
            operationTO.setOrderReferenceNo("Secondary Trip");
            operationTO.setOrderReferenceRemarks("Secondary Trip");
            operationTO.setProductCategoryId("0");
            //generate tripcode
            String cNoteCode = "CO/13-14/";
            String cNoteCodeSequence = operationBP.getCnoteCodeSequence();
            cNoteCode = cNoteCode + cNoteCodeSequence;
            operationTO.setConsignmentNoteNo(cNoteCode);
            String customerId = request.getParameter("customerId");

            operationTO.setCustomerId(customerId);
            operationTO4.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getCustomer(operationTO4);
            String tempn[] = null;
            tempn = customerDetails.split("~");
            operationTO.setCustomerCode(tempn[1]);
            operationTO.setCustomerName(tempn[2]);
            operationTO.setCustomerTypeId("1");
            operationTO.setCustomerAddress(tempn[13]);
            operationTO.setPincode(tempn[14]);
            operationTO.setCustomerPhoneNo(tempn[6]);
            operationTO.setCustomerMobileNo(tempn[7]);
            operationTO.setMailId(tempn[8]);
            operationTO.setBillingTypeId(request.getParameter("billingTypeId"));
            operationTO.setContractId(50);
            if (operationCommand.getDestination() != null && !"".equals(operationCommand.getDestination())) {
                operationTO.setDestination(operationCommand.getDestination());
            }
            if (operationCommand.getOrigin() != null && !"".equals(operationCommand.getOrigin())) {
                operationTO.setOrigin(operationCommand.getOrigin());
            }
            operationTO.setBusinessType("2");
            operationTO.setMultiPickup("N");
            operationTO.setMultiDelivery("N");
            operationTO.setConsignmentOrderInstruction("Secondary Trip");
            operationTO.setTotalPackage("0");
            operationTO.setTotalWeightage("0");
            operationTO.setServiceType("1");
            operationTO.setVehicleId(Integer.parseInt(request.getParameter("vehicleId")));
            operationTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            operationTO.setVehicleTypeName(request.getParameter("productCategoryName"));
            operationTO.setRouteContractId(Integer.parseInt(request.getParameter("routeContractId")));
            operationTO.setReeferRequired(request.getParameter("reeferRequired"));
            operationTO4.setSecondaryRouteId(request.getParameter("routeContractId"));

            String routeId = "0";

            ArrayList orderPointDetails = new ArrayList();
            orderPointDetails = secondaryOperationBP.getRoutePointDetails(operationTO4);

            //System.out.println("routeId = " + routeId);
            if (routeId != null) {
                operationTO.setRouteId(routeId);
            } else {
                operationTO.setRouteId("0");
            }
            operationTO.setContractRateId("0");

            if (operationCommand.getTotalKm() != null && !"".equals(operationCommand.getTotalKm())) {
                operationTO.setTotalKm(operationCommand.getTotalKm());
            }
            if (operationCommand.getTotalHours() != null && !"".equals(operationCommand.getTotalHours())) {
                operationTO.setTotalHours(operationCommand.getTotalHours());
            }
            if (operationCommand.getTotalMinutes() != null && !"".equals(operationCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(operationCommand.getTotalMinutes());
            }
            operationTO.setRateWithReefer("0");
            operationTO.setRateWithoutReefer("0");
            operationTO.setVehicleRequiredDate(request.getParameter("consignmentDate"));
            operationTO.setVehicleRequiredHour("00");
            operationTO.setVehicleRequiredMinute("00");
            operationTO.setVehicleInstruction("Secondary Trip");
            operationTO.setConsignorName(tempn[2]);
            operationTO.setConsignorPhoneNo(tempn[6]);
            operationTO.setConsignorAddress(tempn[13]);
            operationTO.setConsigneeName(tempn[2]);
            operationTO.setConsigneePhoneNo(tempn[6]);
            operationTO.setConsigneeAddress(tempn[13]);
            operationTO.setTotFreightAmount("0");
            operationTO.setDocCharges("0");
            operationTO.setOdaCharges("0");
            operationTO.setOdaCharges("0");
            operationTO.setMultiPickupCharge("0");
            operationTO.setMultiDeliveryCharge("0");
            operationTO.setHandleCharges("0");
            operationTO.setOtherCharges("0");
            operationTO.setUnloadingCharges("0");
            operationTO.setLoadingCharges("0");
            operationTO.setSubTotal("0");
            operationTO.setTotalCharges("0");
            insertStatus = operationBP.insertConsignmentNoteForEmptyTrip(operationTO, userId);
            //System.out.println("insert Status..." + insertStatus);
            if (insertStatus > 0) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                //System.out.println(dateFormat.format(date));
                request.setAttribute("curDate", dateFormat.format(date));
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/BrattleFoods/consignmentNote.jsp";

                //send email
                /////////////////Email Part//////////////////////////////////////////////
                String to = "";
                String activitycode = "EMREQ1";
                //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";

                ArrayList emaildetails = new ArrayList();
                emaildetails = operationBP.getEmailDetails(activitycode);
                Iterator itr = emaildetails.iterator();
                OperationTO operationTO1 = null;
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    smtp = operationTO1.getSmtp();
                    emailPort = Integer.parseInt(operationTO1.getPort());
                    frommailid = operationTO1.getEmailId();
                    password = operationTO1.getPassword();
                    to = operationTO1.getTomailId();
                }

                String toEmailId = operationBP.getEmailToList(operationTO.getCustomerId());
                ;

                String[] temp = toEmailId.split(",");
                to = "";
                int cntr = 0;
                for (int i = 0; i < temp.length; i++) {
                    if (!"-".equals(temp[i]) && !"".equals(temp[i])) {
                        if (cntr == 0) {
                            to = temp[i];
                        } else {
                            to = to + "," + temp[i];

                        }
                        cntr++;
                    }
                }

                ArrayList emailConsignmentDetails = operationBP.getConsignmentDetails(operationTO.getConsignmentOrderId());

                itr = emailConsignmentDetails.iterator();
                operationTO1 = null;

                String vehicleType = "";
                String totalHours = "";
                String totalDistance = "";
                String totalPackage = "";
                String totalWeightage = "";
                String reeferMaximumTemperature = "";
                String reeferMinimumTemperature = "";
                String productCategoryName = "";
                String route = "";
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    vehicleType = operationTO1.getVehicleTypeName();
                    totalHours = operationTO1.getTotalHours();
                    totalDistance = operationTO1.getTotalDistance();
                    totalPackage = operationTO1.getTotalPackage();
                    totalWeightage = operationTO1.getTotalWeightage();
                    route = operationTO1.getConsigmentOrigin() + "-" + operationTO1.getConsigmentDestination();
                    productCategoryName = operationTO1.getProductCategoryName() + "(Temp " + operationTO1.getReeferMinimumTemperature() + " - " + operationTO1.getReeferMaximumTemperature() + ")";
                }

                String emailFormat = "Team, <br><br> New Order Details is given below <br> Consignment Note Created for Customer Order Ref No: " + operationTO.getConsignmentNoteNo();
                emailFormat = emailFormat + "<br> Customer: " + operationTO.getCustomerName();
                emailFormat = emailFormat + "<br> Vehicle Type: " + vehicleType;
                emailFormat = emailFormat + "<br> Vehicle Required Date: " + operationTO.getVehicleRequiredDate() + " Hour:" + operationTO.getVehicleRequiredHour();
                emailFormat = emailFormat + "<br> Route: " + route;
                emailFormat = emailFormat + "<br> Product Category: " + productCategoryName;
                emailFormat = emailFormat + "<br> Reefer: " + operationTO.getReeferRequired();
                emailFormat = emailFormat + "<br> Total Packages (Nos): " + operationTO.getTotalPackage();
                emailFormat = emailFormat + "<br> Total Weight (Kg): " + operationTO.getTotalWeightage();
                emailFormat = emailFormat + "<br> Total Distance (Km): " + totalDistance;
                emailFormat = emailFormat + "<br> Transit Hours (Hr): " + totalHours;
                emailFormat = emailFormat + "<br><br>  Thanks,<br> Team BrattleFoods. ";

                String subject = "Consignment Note Created For : " + operationTO.getCustomerName() + ". Route " + route + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                ;
                String content = emailFormat;

                //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
                //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "").start();
                //end of email part
                /*
                ArrayList contractRouteList = new ArrayList();
                contractRouteList = operationBP.getContractRouteList(operationTO);
                request.setAttribute("contractRouteList", contractRouteList);

                ArrayList vehicleTypeList = new ArrayList();
                vehicleTypeList = operationBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                ArrayList billingTypeList = new ArrayList();
                billingTypeList = operationBP.getBillingTypeList();
                request.setAttribute("billingTypeList", billingTypeList);
                ArrayList productCategoryList = new ArrayList();
                productCategoryList = operationBP.getProductCategoryList();
                request.setAttribute("productCategoryList", productCategoryList);
                 */
            }

            if (insertStatus > 0) {
                TripTO tripTO = new TripTO();
                String consignment = String.valueOf(insertStatus);
                tripTO.setConsignmentId(consignment);
                String[] consignmentOrderId = {consignment};
                //String tripDate = request.getParameter("tripDate");
                String vehicleId = request.getParameter("vehicleId");
                String vehicleNoEmail = request.getParameter("vehicleNo");
                String driver1Id = request.getParameter("driver1Id");
                String driver2Id = request.getParameter("driver2Id");
                String driver3Id = request.getParameter("driver3Id");
                String orderExpense = "0";
                String orderRevenue = "0";
                String profitMargin = "0";
                String tripScheduleDate = request.getParameter("startDate");
                String tripScheduleTime = request.getParameter("startHour") + ":" + request.getParameter("startMinute");
                String tripScheduleTimeHrs = request.getParameter("startHour");
                String tripScheduleTimeMins = request.getParameter("startMinute");
                String totalHours = request.getParameter("totalHours");

                String cNotes = cNoteCode;
                String billingType = tempn[4];
                String customerName = tempn[2];
                String customerType = "Contract";
                String routeInfo = request.getParameter("routeInfo");
                tripTO.setOrigin(request.getParameter("cityFrom"));
                tripTO.setDestination(request.getParameter("cityTo"));
                String productInfo = request.getParameter("productCategoryName");
                String reeferRequired = request.getParameter("reeferRequired");
                String totalWeight = "0";

                String vehicleType = request.getParameter("vehicleTypeName");
                String vehicleNo = request.getParameter("vehicleNo");
                String vehicleTonnage = request.getParameter("vehicleTonnage");
                String vehicleCapUtil = request.getParameter("vehicleCapUtil");
                String tripRemarks = "Empty Trip";

                String actionName = "1";
                String actionRemarks = "Empty Trip";
                String preStartLocationId = request.getParameter("preStartLocationId");
                String preStartLocationPlanDate = request.getParameter("preStartLocationPlanDate");
                String preStartLocationPlanTimeHrs = request.getParameter("preStartLocationPlanTimeHrs");
                String preStartLocationPlanTimeMins = request.getParameter("preStartLocationPlanTimeMins");
                String preStartLocationDistance = request.getParameter("preStartLocationDistance");
                String preStartLocationDurationHrs = request.getParameter("preStartLocationDurationHrs");
                String preStartLocationDurationMins = request.getParameter("preStartLocationDurationMins");
                String preStartLocationVehicleMileage = request.getParameter("vehicleMileage");
                String preStartLocationTollRate = request.getParameter("tollRate");
                String preStartLocationRouteExpense = request.getParameter("preStartRouteExpense");
                String preStartLocationStatus = "1";
                String originId = request.getParameter("origin");
                String destinationId = request.getParameter("destination");

                if (preStartLocationStatus == null) {
                    preStartLocationStatus = "0";
                }
                String roleId = "" + (Integer) session.getAttribute("RoleId");
                String companyId = (String) session.getAttribute("companyId");
                tripTO.setRoleId(roleId);
                tripTO.setCompanyId(companyId);
                tripTO.setProductInfo(productInfo);
                tripTO.setCustomerId(customerId);
                tripTO.setOriginId(originId);
                tripTO.setDestinationId(destinationId);
                tripTO.setPreStartLocationStatus(preStartLocationStatus);
                tripTO.setActionName(actionName);
                tripTO.setActionRemarks(actionRemarks);
                tripTO.setPreStartLocationId(preStartLocationId);
                if ("1".equals(tripTO.getPreStartLocationStatus())) {
                    tripTO.setPreStartLocationPlanDate(tripScheduleDate);
                    tripTO.setPreStartLocationPlanTimeHrs(tripScheduleTimeHrs);
                    tripTO.setPreStartLocationPlanTimeMins(tripScheduleTimeMins);
                } else {
                    if (preStartLocationRouteExpense == null || "".equals(preStartLocationRouteExpense)) {
                        preStartLocationRouteExpense = "0";
                    }
                    //System.out.println("orderExpense:" + orderExpense + " preStartLocationRouteExpense:" + preStartLocationRouteExpense);
                    orderExpense = "" + (Float.parseFloat(orderExpense) + Float.parseFloat(preStartLocationRouteExpense));
                    tripTO.setPreStartLocationPlanDate(preStartLocationPlanDate);
                    tripTO.setPreStartLocationPlanTimeHrs(preStartLocationPlanTimeHrs);
                    tripTO.setPreStartLocationPlanTimeMins(preStartLocationPlanTimeMins);
                }

                tripTO.setPreStartLocationDistance(preStartLocationDistance);
                tripTO.setPreStartLocationDurationHrs(preStartLocationDurationHrs);
                tripTO.setPreStartLocationDurationMins(preStartLocationDurationMins);
                tripTO.setPreStartLocationVehicleMileage(preStartLocationVehicleMileage);
                tripTO.setPreStartLocationTollRate(preStartLocationTollRate);
                tripTO.setPreStartLocationRouteExpense(preStartLocationRouteExpense);

                //points related
                tripTO.setUserId(userId);
                tripTO.setVehicleNo(vehicleNoEmail);
                tripTO.setVehicleTypeName(vehicleType);
                tripTO.setVehicleCapUtil(vehicleCapUtil);
                tripTO.setTripRemarks(tripRemarks);
                tripTO.setcNotes(cNotes);
                tripTO.setBillingType(billingType);
                tripTO.setCustomerName(customerName);
                tripTO.setCustomerType(customerType);
                tripTO.setRouteInfo(routeInfo);
                tripTO.setReeferRequired(reeferRequired);
                tripTO.setTotalWeight(totalWeight);

                tripTO.setConsignmentOrderId(consignmentOrderId);
                //tripTO.setTripDate(tripDate);
                tripTO.setVehicleId(vehicleId);
                tripTO.setTotalHrs(totalHours);
                tripTO.setTripTransitHours(totalHours);

                operationTO.setVehicleId(Integer.parseInt(vehicleId));

                String driver1Name = "";
                String driver2Name = "";
                String driver3Name = "";
                String driverName = "";
                //System.out.println("srini vehicleid is:" + operationTO.getVehicleId());
                ArrayList vehicleDriverInfo = operationBP.getVehicleDriverMappingForVehicleId(operationTO);
                Iterator itr1 = vehicleDriverInfo.iterator();
                OperationTO operationTO3 = new OperationTO();
                if (itr1.hasNext()) {
                    //System.out.println("12");
                    operationTO3 = new OperationTO();
                    operationTO3 = (OperationTO) itr1.next();
                    tripTO.setPrimaryDriverId(operationTO3.getPrimaryDriverId());
                    //System.out.println("tripTO.setPrimaryDriverId():" + tripTO.getPrimaryDriverId());
                    //System.out.println("operationTO3.setPrimaryDriverId():" + operationTO3.getPrimaryDriverId());
                    tripTO.setSecondaryDriver1Id(operationTO3.getSecondaryDriverIdOne());
                    tripTO.setSecondaryDriver2Id(operationTO3.getSecondaryDriverIdTwo());
                    driver1Name = operationTO3.getPrimaryDriverName();
                    driver2Name = operationTO3.getSecondaryDriverNameOne();
                    driver3Name = operationTO3.getSecondaryDriverNameTwo();
                }

                //System.out.println("insertStatus:" + insertStatus);
//                orderRevenue = tripBP.getConsignmentOrderRevenue(insertStatus + "");
//                orderExpense = tripBP.getConsignmentOrderExpense(insertStatus + "");
                //System.out.println("orderRevenue:" + orderRevenue);
                //System.out.println("orderExpense:" + orderExpense);
                tripTO.setOrderExpense(orderExpense);
                tripTO.setOrderRevenue(orderRevenue);
                tripTO.setProfitMargin(profitMargin);
                tripTO.setTripScheduleDate(tripScheduleDate);
                tripTO.setTripScheduleTime("00:00:00");
                //tripTO.setTripScheduleTimeHrs(tripScheduleTime);

                if (!"".equals(driver1Name)) {
                    driverName = driver1Name;
                } else if (!"".equals(driver2Name)) {
                    driverName = driverName + "," + driver2Name;
                } else if (!"".equals(driver3Name)) {
                    driverName = driverName + "," + driver3Name;
                }
                tripTO.setRouteContractId(operationTO.getRouteContractId());

                Iterator itr10 = orderPointDetails.iterator();
                SecondaryOperationTO sTO = null;
                while (itr10.hasNext()) {
                    sTO = new SecondaryOperationTO();
                    sTO = (SecondaryOperationTO) itr10.next();
                    //System.out.println("pointId :" + sTO.getPointId());
                    //System.out.println("getPointType :" + sTO.getPointType());
                    //System.out.println("getPointSequence :" + sTO.getPointSequence());
                    //System.out.println("getPointAddresss :" + sTO.getPointAddresss());
                }
                int status = tripBP.saveSecondaryTripSheet(tripTO, orderPointDetails);
                //System.out.println("tripId = " + status);
                int tripid = status;
                int updateTripScheduleStatus = 0;
                String secondaryRouteId = operationTO4.getSecondaryRouteId();
                updateTripScheduleStatus = secondaryOperationBP.updateTripScheduleStatus(tripid, customerId, secondaryRouteId, tripScheduleDate, userId);
                String tripCode = tripBP.getTripCode(tripid);
                if (status > 0) {

                    //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                    String to = "";
                    String cc = "";
                    String smtp = "";
                    int emailPort = 0;
                    String frommailid = "";
                    String password = "";
                    String activitycode = "EMTRP0";

                    ArrayList emaildetails = new ArrayList();
                    emaildetails = tripBP.getEmailDetails(activitycode);
                    itr1 = emaildetails.iterator();

                    if (itr1.hasNext()) {
                        tripTO = new TripTO();
                        tripTO = (TripTO) itr1.next();
                        smtp = tripTO.getSmtp();
                        emailPort = Integer.parseInt(tripTO.getPort());
                        frommailid = tripTO.getEmailId();
                        password = tripTO.getPassword();
                        to = tripTO.getTomailId();
                        cc = tripTO.getTomailId();
                    }

                    String emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNoEmail + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routeInfo + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "<a style='text-decoration: none' href='http://192.168.2.30:8084/throttle/saveEmptyTripApproval.do?userId=1161&tripid=" + tripid + "&routename=" + routeInfo + "&vehicleno=" + vehicleNoEmail + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                            + "<a style='text-decoration: none' href='http://192.168.2.30:8084/throttle/saveEmptyTripApproval.do?userId=1161&tripid=" + tripid + "&routename=" + routeInfo + "&vehicleno=" + vehicleNoEmail + "&approvestatus=3'>Reject</a>"
                            + "</td></tr>"
                            + "</table></body></html>";

                    //String subject = "Trip Closure Approval for Consignment Note - " + cnotename;
                    String subject = "Empty Trip Approval Request for Trip Start" + customerName + " Vehicle " + vehicleNoEmail + " Route " + routeInfo;
                    String content = emailFormat;

                    //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
                    //fetch mail content
                    /*
                    tripTO.setUserId(userId);
                    request.setAttribute("tripSheetId", status);

                    String tripId = String.valueOf(status);
                    tripTO.setTripId(tripId);
                    tripTO.setTripSheetId(tripId);
                    ArrayList expiryDateDetails = new ArrayList();
                    expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
                    request.setAttribute("expiryDateDetails", expiryDateDetails);

                    ArrayList tripDetails = new ArrayList();
                    ArrayList tripPointDetails = new ArrayList();
                    tripDetails = tripBP.getTripDetails(tripTO);
                    tripPointDetails = tripBP.getTripPointDetails(tripTO);

                    request.setAttribute("tripDetails", tripDetails);
                    request.setAttribute("tripPointDetails", tripPointDetails);
                    ArrayList statusDetails = new ArrayList();
                    statusDetails = tripBP.getTripStausDetails(tripTO);
                    if (statusDetails.size() > 0) {
                    request.setAttribute("statusDetails", statusDetails);
                    }
                    String tripSheetId = String.valueOf(status);
                    ArrayList tripAdvanceDetails = new ArrayList();
                    tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);
                    request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

                    ArrayList tripPreStartDetails = new ArrayList();
                    tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
                    request.setAttribute("tripPreStartDetails", tripPreStartDetails);
                     */
                    path = "BrattleFoods/tripSheetRedirectPage.jsp";

                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView createSecondaryTripSheet(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        String path = "";
//
//        String menuPath = "Operation  >>  Trip Planning ";
//        int userId = (Integer) session.getAttribute("userId");
//        secondaryOperationCommand = command;
//        OperationTO operationTO = new OperationTO();
//        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
////            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
////                path = "content/common/NotAuthorized.jsp";
////            } else {
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            String pageTitle = "Create Secondary Trip Sheet";
//            request.setAttribute("pageTitle", pageTitle);
//
//            /*
//             Consignment Creatio For SEcondary Start
//             */
//            int insertStatus = 0;
//            if (secondaryOperationCommand.getCustomerTypeId() != null && !"".equals(secondaryOperationCommand.getCustomerTypeId())) {
//                operationTO.setCustomerTypeId(secondaryOperationCommand.getCustomerTypeId());
//            }
//
//            if (secondaryOperationCommand.getEntryType() != null && !"".equals(secondaryOperationCommand.getEntryType())) {
//                operationTO.setEntryType(secondaryOperationCommand.getEntryType());
//            }
////            if (secondaryOperationCommand.getConsignmentNoteNo() != null && !"".equals(secondaryOperationCommand.getConsignmentNoteNo())) {
////                operationTO.setConsignmentNoteNo(secondaryOperationCommand.getConsignmentNoteNo());
////            }
//            if (secondaryOperationCommand.getConsignmentDate() != null && !"".equals(secondaryOperationCommand.getConsignmentDate())) {
//                operationTO.setConsignmentDate(secondaryOperationCommand.getConsignmentDate());
//            }
//            if (secondaryOperationCommand.getOrderReferenceNo() != null && !"".equals(secondaryOperationCommand.getOrderReferenceNo())) {
//                operationTO.setOrderReferenceNo(secondaryOperationCommand.getOrderReferenceNo());
//            }
//            if (secondaryOperationCommand.getOrderReferenceRemarks() != null && !"".equals(secondaryOperationCommand.getOrderReferenceRemarks())) {
//                operationTO.setOrderReferenceRemarks(secondaryOperationCommand.getOrderReferenceRemarks());
//            }
//            if (secondaryOperationCommand.getProductCategoryId() != null && !"".equals(secondaryOperationCommand.getProductCategoryId())) {
//                operationTO.setProductCategoryId(secondaryOperationCommand.getProductCategoryId());
//            }
//
//            String cNoteCode = "CO/13-14/";
//            String cNoteCodeSequence = operationBP.getCnoteCodeSequence();
//            cNoteCode = cNoteCode + cNoteCodeSequence;
//            operationTO.setConsignmentNoteNo(cNoteCode);
//            if (operationTO.getCustomerTypeId().equals("1")) {
//                if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
//                    operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
//                }
//                if (secondaryOperationCommand.getCustomerName() != null && !"".equals(secondaryOperationCommand.getCustomerName())) {
//                    operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
//                }
//                if (secondaryOperationCommand.getCustomerCode() != null && !"".equals(secondaryOperationCommand.getCustomerCode())) {
//                    operationTO.setCustomerCode(secondaryOperationCommand.getCustomerCode());
//                }
//                if (secondaryOperationCommand.getCustomerAddress() != null && !"".equals(secondaryOperationCommand.getCustomerAddress())) {
//                    operationTO.setCustomerAddress(secondaryOperationCommand.getCustomerAddress());
//                }
//                if (secondaryOperationCommand.getPincode() != null && !"".equals(secondaryOperationCommand.getPincode())) {
//                    operationTO.setPincode(secondaryOperationCommand.getPincode());
//                }
//                if (secondaryOperationCommand.getCustomerMobileNo() != null && !"".equals(secondaryOperationCommand.getCustomerMobileNo())) {
//                    operationTO.setCustomerMobileNo(secondaryOperationCommand.getCustomerMobileNo());
//                }
//                if (secondaryOperationCommand.getMailId() != null && !"".equals(secondaryOperationCommand.getMailId())) {
//                    operationTO.setMailId(secondaryOperationCommand.getMailId());
//                }
//                if (secondaryOperationCommand.getCustomerPhoneNo() != null && !"".equals(secondaryOperationCommand.getCustomerPhoneNo())) {
//                    operationTO.setCustomerPhoneNo(secondaryOperationCommand.getCustomerPhoneNo());
//                }
//                if (secondaryOperationCommand.getBillingTypeId() != null && !"".equals(secondaryOperationCommand.getBillingTypeId())) {
//                    operationTO.setBillingTypeId(secondaryOperationCommand.getBillingTypeId());
//                }
//                if (secondaryOperationCommand.getSecondaryRouteId() != null && secondaryOperationCommand.getSecondaryRouteId() != "") {
//                    //operationTO.setSecondaryRouteId(secondaryOperationCommand.getSecondaryRouteId());
//                }
//                if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
//                    operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
//                }
//                if (secondaryOperationCommand.getScheduleDate() != null && secondaryOperationCommand.getScheduleDate() != "") {
//                    //operationTO.setScheduleDate(secondaryOperationCommand.getScheduleDate());
//                    request.setAttribute("scheduleDate", secondaryOperationCommand.getScheduleDate());
//                }
//                if (secondaryOperationCommand.getDestination() != null && !"".equals(secondaryOperationCommand.getDestination())) {
//                    operationTO.setDestination(secondaryOperationCommand.getDestination());
//                }
//                if (secondaryOperationCommand.getOrigin() != null && !"".equals(secondaryOperationCommand.getOrigin())) {
//                    operationTO.setOrigin(secondaryOperationCommand.getOrigin());
//                }
//                if (secondaryOperationCommand.getBusinessType() != null && !"".equals(secondaryOperationCommand.getBusinessType())) {
//                    operationTO.setBusinessType(secondaryOperationCommand.getBusinessType());
//                }
//                if (secondaryOperationCommand.getMultiPickup() != null && !"".equals(secondaryOperationCommand.getMultiPickup())) {
//                    operationTO.setMultiPickup(secondaryOperationCommand.getMultiPickup());
//                }
//                if (secondaryOperationCommand.getMultiDelivery() != null && !"".equals(secondaryOperationCommand.getMultiDelivery())) {
//                    operationTO.setMultiDelivery(secondaryOperationCommand.getMultiDelivery());
//                }
//                if (secondaryOperationCommand.getConsignmentOrderInstruction() != null && !"".equals(secondaryOperationCommand.getConsignmentOrderInstruction())) {
//                    operationTO.setConsignmentOrderInstruction(secondaryOperationCommand.getConsignmentOrderInstruction());
//                }
//
//                if (secondaryOperationCommand.getServiceType() != null && !"".equals(secondaryOperationCommand.getServiceType())) {
//                    operationTO.setServiceType(secondaryOperationCommand.getServiceType());
//                }
//
//                if (secondaryOperationCommand.getReeferRequired() != null && !"".equals(secondaryOperationCommand.getReeferRequired())) {
//                    operationTO.setReeferRequired(secondaryOperationCommand.getReeferRequired());
//                }
//                if (secondaryOperationCommand.getRouteContractId() != null && !"".equals(secondaryOperationCommand.getRouteContractId())) {
//                    operationTO.setRouteContractId(Integer.parseInt(secondaryOperationCommand.getRouteContractId()));
//                }
//                if (secondaryOperationCommand.getRouteId() != null && !"".equals(secondaryOperationCommand.getRouteId())) {
//                    operationTO.setRouteId(secondaryOperationCommand.getRouteId());
//                }
//                if (secondaryOperationCommand.getContractRateId() != null && !"".equals(secondaryOperationCommand.getContractRateId())) {
//                    operationTO.setContractRateId(secondaryOperationCommand.getContractRateId());
//                }
//                if (secondaryOperationCommand.getTotalKm() != null && !"".equals(secondaryOperationCommand.getTotalKm())) {
//                    operationTO.setTotalKm(secondaryOperationCommand.getTotalKm());
//                }
//                if (secondaryOperationCommand.getTotalHours() != null && !"".equals(secondaryOperationCommand.getTotalHours())) {
//                    operationTO.setTotalHours(secondaryOperationCommand.getTotalHours());
//                }
//                if (secondaryOperationCommand.getTotalMinutes() != null && !"".equals(secondaryOperationCommand.getTotalMinutes())) {
//                    operationTO.setTotalMinutes(secondaryOperationCommand.getTotalMinutes());
//                }
//                if (secondaryOperationCommand.getRateWithReefer() != null && !"".equals(secondaryOperationCommand.getRateWithReefer())) {
//                    operationTO.setRateWithReefer(secondaryOperationCommand.getRateWithReefer());
//                }
//                if (secondaryOperationCommand.getRateWithoutReefer() != null && !"".equals(secondaryOperationCommand.getRateWithoutReefer())) {
//                    operationTO.setRateWithoutReefer(secondaryOperationCommand.getRateWithoutReefer());
//                }
//                if (secondaryOperationCommand.getVehicleRequiredDate() != null && !"".equals(secondaryOperationCommand.getVehicleRequiredDate())) {
//                    operationTO.setVehicleRequiredDate(secondaryOperationCommand.getVehicleRequiredDate());
//                }
//                if (secondaryOperationCommand.getVehicleRequiredHour() != null && !"".equals(secondaryOperationCommand.getVehicleRequiredHour())) {
//                    operationTO.setVehicleRequiredHour(secondaryOperationCommand.getVehicleRequiredHour());
//                }
//                if (secondaryOperationCommand.getVehicleRequiredMinute() != null && !"".equals(secondaryOperationCommand.getVehicleRequiredMinute())) {
//                    operationTO.setVehicleRequiredMinute(secondaryOperationCommand.getVehicleRequiredMinute());
//                }
//                if (secondaryOperationCommand.getVehicleInstruction() != null && !"".equals(secondaryOperationCommand.getVehicleInstruction())) {
//                    operationTO.setVehicleInstruction(secondaryOperationCommand.getVehicleInstruction());
//                }
//                if (secondaryOperationCommand.getConsignorName() != null && !"".equals(secondaryOperationCommand.getConsignorName())) {
//                    operationTO.setConsignorName(secondaryOperationCommand.getConsignorName());
//                }
//                if (secondaryOperationCommand.getConsignorPhoneNo() != null && !"".equals(secondaryOperationCommand.getConsignorPhoneNo())) {
//                    operationTO.setConsignorPhoneNo(secondaryOperationCommand.getConsignorPhoneNo());
//                }
//                if (secondaryOperationCommand.getConsignorAddress() != null && !"".equals(secondaryOperationCommand.getConsignorAddress())) {
//                    operationTO.setConsignorAddress(secondaryOperationCommand.getConsignorAddress());
//                }
//                if (secondaryOperationCommand.getConsigneeName() != null && !"".equals(secondaryOperationCommand.getConsigneeName())) {
//                    operationTO.setConsigneeName(secondaryOperationCommand.getConsigneeName());
//                }
//                if (secondaryOperationCommand.getConsigneePhoneNo() != null && !"".equals(secondaryOperationCommand.getConsigneePhoneNo())) {
//                    operationTO.setConsigneePhoneNo(secondaryOperationCommand.getConsigneePhoneNo());
//                }
//                if (secondaryOperationCommand.getConsigneeAddress() != null && !"".equals(secondaryOperationCommand.getConsigneeAddress())) {
//                    operationTO.setConsigneeAddress(secondaryOperationCommand.getConsigneeAddress());
//                }
//                if (secondaryOperationCommand.getTotFreightAmount() != null && !"".equals(secondaryOperationCommand.getTotFreightAmount())) {
//                    operationTO.setTotFreightAmount(secondaryOperationCommand.getTotFreightAmount());
//                }
//            }
//            String vehicleId = "";
//            String vehicleTypeId = "";
//            if (request.getParameter("vehicleId") != null && !"".equals(request.getParameter("vehicleId"))) {
//                vehicleId = request.getParameter("vehicleId");
//            }
//            vehicleTypeId = secondaryOperationBP.getVehicleTypeId(vehicleId);
//            if (request.getParameter("vehicleTypeId") != null && !"".equals(request.getParameter("vehicleTypeId"))) {
//                operationTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
//            }
////            insertStatus = operationBP.insertConsignmentNote(operationTO, userId);
//            /*
//             Consignment Creatio For SEcondary End
//             */
//            TripTO tripTO = new TripTO();
//
//            if (secondaryOperationCommand.getSecondaryRouteCode() != null && secondaryOperationCommand.getSecondaryRouteCode() != "") {
//                operationTO1.setSecondaryRouteCode(secondaryOperationCommand.getSecondaryRouteCode());
//                request.setAttribute("secondaryRouteCode", secondaryOperationCommand.getSecondaryRouteCode());
//
//            }
//
//            ArrayList consignmentList = new ArrayList();
//            ArrayList consignmentList1 = new ArrayList();
//            ArrayList consignmentList2 = new ArrayList();
//            consignmentList = tripBP.getConsignmentList(tripTO);
//
//            //calculate revenue if possible
//            //check if vehicle type id is given in consignment order for revenue calculation
//            Iterator itr = consignmentList.iterator();
//            TripTO tripTONew = null;
//            TripTO trpTO = null;
//            float totalRevenue = 0.00F;
//            String revenue = "0";
//            float totalExpense = 0.00F;
//            String expense = "0";
//
//            String vehicleNo = request.getParameter("vehicleNo");
//            if (vehicleId == null) {
//                vehicleId = "";
//            }
//            String roleId = "" + (Integer) session.getAttribute("RoleId");
//            String companyId = (String) session.getAttribute("companyId");
//            tripTO.setRoleId(roleId);
//            tripTO.setCompanyId(companyId);
//
//            String vehicleMileageAndTollRate = tripBP.getVehicleMileageAndTollRate(vehicleTypeId);
//            ArrayList orderPointDetails = new ArrayList();
//            orderPointDetails = secondaryOperationBP.getRoutePointDetails(operationTO);
//            request.setAttribute("orderPointDetails", orderPointDetails);
//            request.setAttribute("totalPoints", orderPointDetails.size());
//            Iterator itr2 = orderPointDetails.iterator();
//            operationTO1 = new SecondaryOperationTO();
//            String originPointName = "";
//            String destinationPointName = "";
//            int a = 0;
//            while (itr2.hasNext()) {
//                operationTO1 = (SecondaryOperationTO) itr2.next();
//                if (a == 0) {
//                    request.setAttribute("originPointId", operationTO1.getPointId());
//                    request.setAttribute("originPointName", operationTO1.getPointName());
//                    originPointName = operationTO1.getPointName();
//                } else if (a == orderPointDetails.size()) {
//                    request.setAttribute("destinationPointId", operationTO1.getPointId());
//                }
//                request.setAttribute("destinationPointName", operationTO1.getPointName());
//                destinationPointName = operationTO1.getPointName();
//                a++;
//            }
//
//            request.setAttribute("vehicleMileageAndTollRate", vehicleMileageAndTollRate);
//            request.setAttribute("orderPointDetails", orderPointDetails);
//            request.setAttribute("consignmentList", consignmentList);
//            request.setAttribute("routeInfo", originPointName + "-" + destinationPointName);
//            request.setAttribute("reeferRequired", "N");
//            request.setAttribute("totalWeight", "0");
//            request.setAttribute("vehicleType", "0");
//
//            //getVehicleRegNos
//            roleId = "" + (Integer) session.getAttribute("RoleId");
//            companyId = (String) session.getAttribute("companyId");
//            tripTO.setRoleId(roleId);
//            tripTO.setCompanyId(companyId);
//            tripTO.setVehicleNo("");
//            tripTO.setVehicleId("");
//            ArrayList vehicleNos = tripBP.getVehicleRegNos(tripTO);
//            //System.out.println("vehicleNos.size() = " + vehicleNos.size());
//            request.setAttribute("vehicleNos", vehicleNos);
//
//            path = "content/secondaryOperation/createSecondaryTripSheet.jsp";
//            //}
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView editSecondaryCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            path = "content/secondaryOperation/editSecondaryContractRouteMaster.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String customerId = request.getParameter("customerId");
            operationTO.setCustomerId(customerId);
            //System.out.println("size is  " + userFunctions.size());
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = secondaryOperationBP.getMileageConfigList(customerId);
            request.setAttribute("mileageConfigList", mileageConfigList);

            String routeId = request.getParameter("secondaryRouteId");
            operationTO.setSecondaryRouteId(routeId);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = secondaryOperationBP.getContractRouteDetails(operationTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);
            request.setAttribute("contractRouteDetailsSize", contractRouteDetails.size());

            ArrayList rateCostDetails = new ArrayList();
            rateCostDetails = secondaryOperationBP.getContractRateCostDetailsEdit(operationTO);
            request.setAttribute("rateCostDetails", rateCostDetails);

            ArrayList routeContractDetails = new ArrayList();
            routeContractDetails = secondaryOperationBP.getRouteContractDetails(operationTO);
            request.setAttribute("routeContractDetails", routeContractDetails);
            request.setAttribute("routeContractDetailsSize", routeContractDetails.size());

            String currentDieselPrice = "";
            currentDieselPrice = operationBP.getCurrentFuelPrice();
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }
            ArrayList SecondaryCustomerList = new ArrayList();
            SecondaryCustomerList = secondaryOperationBP.getViewSecondaryCustomerdetails(operationTO);
            request.setAttribute("SecondaryCustomerList", SecondaryCustomerList);
            SecondaryOperationTO opTO = new SecondaryOperationTO();
            Iterator itr = SecondaryCustomerList.iterator();
            while (itr.hasNext()) {
                opTO = new SecondaryOperationTO();
                opTO = (SecondaryOperationTO) itr.next();
                currentFuelPrice = opTO.getContractCngCost();
                currentDieselPrice = opTO.getContractDieselCost();
            }
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            request.setAttribute("currentDieselPrice", currentDieselPrice);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEditSecondaryCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        menuPath = "Seconadry Customer Contract >> Create";
        secondaryOperationCommand = command;
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();

        int update = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is453456  " + userFunctions.size());
            //System.out.println("size is secondary route name is " + secondaryOperationCommand.getSecondaryRouteId());
            String referMinute = request.getParameter("totalRefer");

            operationTO.setTotalReferMin(referMinute);
            if (secondaryOperationCommand.getRouteName() != null && !"".equals(secondaryOperationCommand.getRouteName())) {
                operationTO.setRouteName(secondaryOperationCommand.getRouteName());
            }
            if (secondaryOperationCommand.getFuelCost() != null && !"".equals(secondaryOperationCommand.getFuelCost())) {
                operationTO.setFuelCost(secondaryOperationCommand.getFuelCost());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getFixedKmPerMonth() != null && !"".equals(secondaryOperationCommand.getFixedKmPerMonth())) {
                operationTO.setFixedKmPerMonth(secondaryOperationCommand.getFixedKmPerMonth());
            }
            if (secondaryOperationCommand.getFixedReeferHours() != null && !"".equals(secondaryOperationCommand.getFixedReeferHours())) {
                operationTO.setFixedReeferHours(secondaryOperationCommand.getFixedReeferHours());
            }
            if (secondaryOperationCommand.getFixedReeferMinutes() != null && !"".equals(secondaryOperationCommand.getFixedReeferMinutes())) {
                operationTO.setFixedReeferMinutes(secondaryOperationCommand.getFixedReeferMinutes());
            }
            if (secondaryOperationCommand.getRouteValidFrom() != null && !"".equals(secondaryOperationCommand.getRouteValidFrom())) {
                operationTO.setRouteValidFrom(secondaryOperationCommand.getRouteValidFrom());
            }
            if (secondaryOperationCommand.getRouteValidTo() != null && !"".equals(secondaryOperationCommand.getRouteValidTo())) {
                operationTO.setRouteValidTo(secondaryOperationCommand.getRouteValidTo());
            }
            if (secondaryOperationCommand.getDistance() != null && !"".equals(secondaryOperationCommand.getDistance())) {
                operationTO.setDistance(secondaryOperationCommand.getDistance());
            }
            if (secondaryOperationCommand.getTotalHours() != null && !"".equals(secondaryOperationCommand.getTotalHours())) {
                operationTO.setTotalHours(secondaryOperationCommand.getTotalHours());
            }
            if (secondaryOperationCommand.getTotalMinutes() != null && !"".equals(secondaryOperationCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(secondaryOperationCommand.getTotalMinutes());
            }
            if (secondaryOperationCommand.getTotalReeferHours() != null && !"".equals(secondaryOperationCommand.getTotalReeferHours())) {
                operationTO.setTotalReeferHours(secondaryOperationCommand.getTotalReeferHours());
            }
            if (secondaryOperationCommand.getTotalReeferMinutes() != null && !"".equals(secondaryOperationCommand.getTotalReeferMinutes())) {
                operationTO.setTotalReeferMinutes(secondaryOperationCommand.getTotalReeferMinutes());
            }
            if (secondaryOperationCommand.getTotalWaitMinutes() != null && !"".equals(secondaryOperationCommand.getTotalWaitMinutes())) {
                operationTO.setTotalWaitMinutes(secondaryOperationCommand.getTotalWaitMinutes());
            }
            if (secondaryOperationCommand.getSecondaryRouteId() != null && !"".equals(secondaryOperationCommand.getSecondaryRouteId())) {
                operationTO.setSecondaryRouteId(secondaryOperationCommand.getSecondaryRouteId());

            }
            if (secondaryOperationCommand.getAverageKM() != null && !"".equals(secondaryOperationCommand.getAverageKM())) {
                operationTO.setAverageKM(secondaryOperationCommand.getAverageKM());
            }

            operationTO.setFixedReeferHours("0");
            operationTO.setFixedReeferMinutes("0");

            if (userId != 0) {
                operationTO.setUserId(userId);
            }
            String[] secondaryRouteDetailId = request.getParameterValues("secondaryRouteDetailId");
            String[] updateValue = request.getParameterValues("updateValue");
            String[] pointType = request.getParameterValues("pointType");
            String[] cityId = request.getParameterValues("cityId");
            String[] cityName = request.getParameterValues("cityName");
            String[] pointAddresss = request.getParameterValues("pointAddresss");
            String[] pointSequence = request.getParameterValues("pointSequence");
            String[] parkingCost = request.getParameterValues("parkingCost");
            String[] latitude = request.getParameterValues("latitude");
            String[] longitude = request.getParameterValues("longitude");
            String[] travelKm = request.getParameterValues("travelKm");
            String[] travelHour = request.getParameterValues("travelHour");
            String[] travelMinute = request.getParameterValues("travelMinute");
            String[] reeferHour = request.getParameterValues("travelHour");
            String[] reeferMinute = request.getParameterValues("travelMinute");
            String[] waitMinute = request.getParameterValues("waitMinute");
            String[] activeStatus = request.getParameterValues("activeStatus");

            String[] secondaryRouteCostId = request.getParameterValues("secondaryRouteCostId");
            String[] vehTypeId = request.getParameterValues("vehTypeId");
            String[] fuelCostPerKms = request.getParameterValues("fuelCostPerKms");
            String[] tollAmounts = request.getParameterValues("tollAmounts");
            String[] addlTollAmounts = request.getParameterValues("addlTollAmounts");
            String[] miscCostKm = request.getParameterValues("miscCost");
            String[] totExpense = request.getParameterValues("totExpense");
            String[] fuelCostPerMin = request.getParameterValues("fuelCostPerMin");
            //System.out.println("size is secondary route name is " + vehTypeId.length);
            int insertContractRouteMater = 0;
            int insertContractRouteDetails = 0;
            int insertContractRouteCostDetails = 0;

            update = secondaryOperationBP.updateContractRouteMater(operationTO);
            if (update > 0) {
                for (int i = 0; i < pointType.length; i++) {
                    operationTO.setPointType(pointType[i]);
                    operationTO.setCityId(cityId[i]);
                    operationTO.setCityName(cityName[i]);
                    operationTO.setPointAddresss(pointAddresss[i]);
                    operationTO.setPointSequence(pointSequence[i]);
                    operationTO.setLatitude(latitude[i]);
                    operationTO.setLongitude(longitude[i]);
                    operationTO.setTravelKm(travelKm[i]);
                    operationTO.setTravelHour(travelHour[i]);
                    operationTO.setTravelMinute(travelMinute[i]);
                    operationTO.setReeferHour(reeferHour[i]);
                    operationTO.setReeferMinute(reeferMinute[i]);
                    operationTO.setWaitMinute(waitMinute[i]);
                    operationTO.setActiveStatus(activeStatus[i]);
                    //System.out.println("updateValue[i] = " + updateValue[i]);
                    if ("0".equals(updateValue[i])) {
                        operationTO.setSecondaryRouteDetailId(secondaryRouteDetailId[i]);
                        update = secondaryOperationBP.updateContractRouteDetails(operationTO);
                        //System.out.println("This is nithi pappu updateValue[i] = " + updateValue[i]);
                    } else {
                        insertContractRouteMater = Integer.parseInt(operationTO.getSecondaryRouteId());
                        insertContractRouteDetails = secondaryOperationBP.insertContractRouteDetails(operationTO, insertContractRouteMater);
                    }
                }
            }
            if (update > 0) {
                int insert = 0;
                for (int i = 0; i < vehTypeId.length; i++) {
                    operationTO.setSecondaryRouteCostId(secondaryRouteCostId[i]);
                    operationTO.setVehTypeId(vehTypeId[i]);
                    operationTO.setFuelCostPerKms(fuelCostPerKms[i]);
                    operationTO.setTollAmounts(tollAmounts[i]);
                    operationTO.setParkingCost(parkingCost[i]);
                    operationTO.setAddlTollAmounts(addlTollAmounts[i]);
                    operationTO.setMiscCostKm(miscCostKm[i]);
                    operationTO.setTotExpense(totExpense[i]);
                    operationTO.setFuelCostPerMin(fuelCostPerMin[i]);
                    if (secondaryRouteCostId[i].equals("0")) {
                        insert = secondaryOperationBP.insertContractRouteCostDetails(operationTO, Integer.parseInt(secondaryOperationCommand.getSecondaryRouteId()));
                    } else {
                        update = secondaryOperationBP.updateContractRouteCostDetails(operationTO);
                    }
                }
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Route Updated Successfully....");
            }
            path = "content/secondaryOperation/viewSecondaryCustomerRoute.jsp";
            ArrayList mileageConfigList = new ArrayList();
            mileageConfigList = secondaryOperationBP.getMileageConfigList(secondaryOperationCommand.getCustomerId());
            request.setAttribute("mileageConfigList", mileageConfigList);

            ArrayList contractRouteDetails = new ArrayList();
            contractRouteDetails = secondaryOperationBP.getContractRouteDetails(operationTO);
            request.setAttribute("contractRouteDetails", contractRouteDetails);

            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            String configDetails = "";
            configDetails = secondaryOperationBP.getConfigDetails(operationTO);
            String[] conTemp = null;
            if (configDetails != null) {
                conTemp = configDetails.split(",");
                request.setAttribute("avgTollAmount", conTemp[0]);
                request.setAttribute("avgMisCost", conTemp[1]);
                request.setAttribute("avgDriverIncentive", conTemp[2]);
                request.setAttribute("avgFactor", conTemp[3]);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView secondaryCustomerPoint(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String customerId = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {

            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
                request.setAttribute("customerName", operationTO.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", operationTO.getCustomerId());

            }
            customerId = request.getParameter("customerId");
            //System.out.println("customerId  " + customerId);

            ArrayList SecondaryCustomerPointsList = new ArrayList();
            SecondaryCustomerPointsList = secondaryOperationBP.getSecondaryCustomerPoints(operationTO, customerId);
            request.setAttribute("SecondaryCustomerPointsList", SecondaryCustomerPointsList);
            //System.out.println("SecondaryCustomerPointsList  " + SecondaryCustomerPointsList);
            request.setAttribute("customerName", operationTO.getCustomerName());
            request.setAttribute("customerId", operationTO.getCustomerId());
            path = "content/BrattleFoods/secondaryCustomerPointMaster.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveSecondaryCustomerPointMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {

        //System.out.println("Secondary Point Master.........");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String customerId = "";
        int userId = (Integer) session.getAttribute("userId");
        //                (Integer) session.getAttribute("userId");
        int saveCityMaster = 0;
        int updateStatus = 0;
        //        int cityMasterList=0;
        String menuPath = "";
        menuPath = "Operation  >> Secondary Customer Point Master ";
        String pageTitle = "Save Secondary Customer Point Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList SecondaryCustomerPointsList = new ArrayList();

        try {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());

            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
            }
            if (secondaryOperationCommand.getPointName() != null && !"".equals(secondaryOperationCommand.getPointName())) {
                operationTO.setPointName(secondaryOperationCommand.getPointName());
            }
            if (secondaryOperationCommand.getPointType() != null && !"".equals(secondaryOperationCommand.getPointType())) {
                operationTO.setPointType(secondaryOperationCommand.getPointType());
            }
            if (secondaryOperationCommand.getPointAddress() != null && !"".equals(secondaryOperationCommand.getPointAddress())) {
                operationTO.setPointAddress(secondaryOperationCommand.getPointAddress());
            }
            if (secondaryOperationCommand.getCityId() != null && !"".equals(secondaryOperationCommand.getCityId())) {
                operationTO.setCityId(secondaryOperationCommand.getCityId());
            }
            if (secondaryOperationCommand.getStatus() != null && !"".equals(secondaryOperationCommand.getStatus())) {
                operationTO.setStatus(secondaryOperationCommand.getStatus());
            }
            if (secondaryOperationCommand.getLatitudePosition() != null && !"".equals(secondaryOperationCommand.getLatitudePosition())) {
                operationTO.setLatitudePosition(secondaryOperationCommand.getLatitudePosition());
            }
            if (secondaryOperationCommand.getLongitudePosition() != null && !"".equals(secondaryOperationCommand.getLongitudePosition())) {
                operationTO.setLongitudePosition(secondaryOperationCommand.getLongitudePosition());
            }
            //System.out.println("secondaryOperationCommand.getPointId() = " + secondaryOperationCommand.getPointId());
            customerId = request.getParameter("customerId");
            //System.out.println("customerId  " + customerId);
            if (secondaryOperationCommand.getPointId() != null && !"".equals(secondaryOperationCommand.getPointId())) {
                operationTO.setPointId(secondaryOperationCommand.getPointId());
                updateStatus = secondaryOperationBP.updateSecondaryRoute(operationTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");
                SecondaryCustomerPointsList = secondaryOperationBP.getSecondaryCustomerPoints(operationTO, customerId);
                request.setAttribute("SecondaryCustomerPointsList", SecondaryCustomerPointsList);
                request.setAttribute("customerName", operationTO.getCustomerName());
                request.setAttribute("customerId", operationTO.getCustomerId());
                path = "content/BrattleFoods/secondaryCustomerPointMaster.jsp";
            } else {
                saveCityMaster = secondaryOperationBP.saveSecondaryRoutes(operationTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted SucessFully");
                SecondaryCustomerPointsList = secondaryOperationBP.getSecondaryCustomerPoints(operationTO, customerId);
                request.setAttribute("SecondaryCustomerPointsList", SecondaryCustomerPointsList);
                request.setAttribute("customerName", operationTO.getCustomerName());
                request.setAttribute("customerId", operationTO.getCustomerId());
                path = "content/BrattleFoods/secondaryCustomerPointMaster.jsp";
            }

            if (saveCityMaster != 0 || updateStatus != 0) {

                SecondaryCustomerPointsList = secondaryOperationBP.getSecondaryCustomerPoints(operationTO, customerId);
                request.setAttribute("SecondaryCustomerPointsList", SecondaryCustomerPointsList);
                request.setAttribute("customerName", operationTO.getCustomerName());
                request.setAttribute("customerId", operationTO.getCustomerId());
                path = "content/BrattleFoods/secondaryCustomerPointMaster.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View city master
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewCityMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("CityMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        String menuPath = "";
        String customerId = "";
        menuPath = "Operation  >> CityMaster ";
        String pageTitle = "CityMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            customerId = request.getParameter("customerId");
            //System.out.println("customerId  " + customerId);
            ArrayList SecondaryCustomerPointsList = new ArrayList();
            SecondaryCustomerPointsList = secondaryOperationBP.getSecondaryCustomerPoints(operationTO, customerId);
            request.setAttribute("SecondaryCustomerPointsList", SecondaryCustomerPointsList);
            path = "content/BrattleFoods/secondaryCustomerPointMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView secondaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Secondary Driver Settlement ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry >> Driver Settlement ";
        String fromDate = "", toDate = "";
        TripTO tripTO = new TripTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/secondaryDriverSettlement.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());

            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
                fromDate = secondaryOperationCommand.getFromDate();
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
                toDate = secondaryOperationCommand.getToDate();
            }
            if (secondaryOperationCommand.getPrimaryDriverId() != null && !"".equals(secondaryOperationCommand.getPrimaryDriverId())) {
                operationTO.setPrimaryDriverId(secondaryOperationCommand.getPrimaryDriverId());
                request.setAttribute("primaryDriverId", secondaryOperationCommand.getPrimaryDriverId());
                String primaryDriver = request.getParameter("primaryDriver");
                request.setAttribute("primaryDriver", primaryDriver);
            }
            if (secondaryOperationCommand.getPrimaryDriver() != null && !"".equals(secondaryOperationCommand.getPrimaryDriver())) {
                operationTO.setPrimaryDriverId(secondaryOperationCommand.getPrimaryDriver());
                request.setAttribute("primaryDriverId", secondaryOperationCommand.getPrimaryDriver());
            }
            ArrayList getSecondaryFleet = new ArrayList();
            getSecondaryFleet = secondaryOperationBP.getSecondaryFleet(operationTO);
            request.setAttribute("getSecondaryFleet", getSecondaryFleet);
            //System.out.println("secondary fleet:" + getSecondaryFleet.size());

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            //System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                operationTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                operationTO.setToDate(toDate);
            }

            String startingBalance = secondaryOperationBP.getStartingBalance(secondaryOperationCommand.getPrimaryDriverId());
            //System.out.println("startingBalance = " + startingBalance);
            if (startingBalance != null) {
                request.setAttribute("startingBalance", startingBalance);
            } else {
                request.setAttribute("startingBalance", "0");
            }
            ArrayList tripClosureDetails = new ArrayList();
            tripClosureDetails = secondaryOperationBP.getTripClosureDetails(operationTO);
            request.setAttribute("tripClosureDetails", tripClosureDetails);
            request.setAttribute("tripClosureDetailsSize", tripClosureDetails.size());

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveSecondaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        ModelAndView mv = null;
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        String fromDate = "", toDate = "";
        TripTO tripTO = new TripTO();
        int insertDriverSettlement = 0;
        int insertDriverSettlementDetails = 0;
        int insertStatus = 0;
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/secondaryDriverSettlement.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());

            String primaryDriverId = request.getParameter("driverId");
            String totalRcmAllocation = request.getParameter("totalRcmAllocation");
            String totalBpclAllocation = request.getParameter("totalAdvanceAmount");
            String totalExtraExpense = request.getParameter("totalExtraExpense");
            String totalMiscellaneousExpense = request.getParameter("totalMiscExpense");
            String totalDriverBhatta = request.getParameter("totalDriverBatta");
            String totalExpenses = request.getParameter("totalExpenses");
            String balanceAmount = request.getParameter("balanceAmount");
            String startingBalance = request.getParameter("startingBalance");
            String endingBalance = request.getParameter("endingBalance");
            String paymentMode = request.getParameter("paymentMode");
            String settlementRemarks = request.getParameter("settlementRemarks");
            String payAmount = request.getParameter("payAmount");
            String cfAmount = request.getParameter("cfAmount");

            operationTO.setPayAmount(payAmount);
            operationTO.setCfAmount(cfAmount);
            operationTO.setPrimaryDriverId(primaryDriverId);
            operationTO.setPrimaryDriverId(primaryDriverId);
            operationTO.setTotalBpclAllocation(totalBpclAllocation);
            operationTO.setTotalRcmAllocation(totalRcmAllocation);
            operationTO.setTotalExtraExpense(totalExtraExpense);
            operationTO.setTotalMiscellaneousExpense(totalMiscellaneousExpense);
            operationTO.setTotalDriverBhatta(totalDriverBhatta);
            operationTO.setTotalExpenses(totalExpenses);
            operationTO.setTotalExtraExpense(primaryDriverId);
            operationTO.setStartingBalance(startingBalance);
            operationTO.setEndingBalance(endingBalance);
            operationTO.setBalanceAmount(balanceAmount);
            operationTO.setSettlementRemarks(settlementRemarks);
            operationTO.setPaymentMode(paymentMode);
            operationTO.setUserId(userId);

            insertDriverSettlement = secondaryOperationBP.insertSecondaryDriverSettlement(operationTO, userId);

            String[] tripId = request.getParameterValues("tripId");
            String[] fuelPrice = request.getParameterValues("fuelPrice");
            String[] runKMs = request.getParameterValues("runKMs");
            String[] runHours = request.getParameterValues("runHours");
            String[] fuelConsumption = request.getParameterValues("fuelConsumption");
            String[] rcmAllocation = request.getParameterValues("rcmAllocation");
            String[] bpclAllocation = request.getParameterValues("bpclAllocation");
            String[] extraExpense = request.getParameterValues("extraExpense");
            String[] totalMiscellaneous = request.getParameterValues("totalMiscellaneous");
            String[] driverBhatta = request.getParameterValues("driverBhatta");
            String[] totalExpense = request.getParameterValues("totalExpense");
            String[] tripBalance = request.getParameterValues("tripBalance");

            tripTO.setStatusId("14");
            if (insertDriverSettlement > 0) {
                for (int i = 0; i < tripId.length; i++) {
                    operationTO.setTripId(tripId[i]);
                    tripTO.setTripSheetId(tripId[i]);
                    operationTO.setFuelCost(fuelPrice[i]);
                    operationTO.setTotalKm(runKMs[i]);
                    operationTO.setTotalHours(runHours[i]);
                    operationTO.setFuelConsumption(fuelConsumption[i]);
                    operationTO.setRcmExpense(rcmAllocation[i]);
                    operationTO.setAdvanceAmount(bpclAllocation[i]);
                    operationTO.setExtraExpense(extraExpense[i]);
                    operationTO.setMiscExpense(totalMiscellaneous[i]);
                    operationTO.setDriverBatta(driverBhatta[i]);
                    operationTO.setTotalExpense(totalExpense[i]);
                    operationTO.setTripBalance(tripBalance[i]);
                    insertDriverSettlementDetails = secondaryOperationBP.insertSecondaryDriverSettlementDetails(operationTO, insertDriverSettlement);
                    insertStatus = tripBP.updateStatus(tripTO, userId);
                    //System.out.println("the update status of driver settlement is" + insertStatus);
                }
            }

            // trip WFC
            ArrayList tripClosureDetails = new ArrayList();
            tripClosureDetails = secondaryOperationBP.getTripClosureDetails(operationTO);
            request.setAttribute("tripClosureDetails", tripClosureDetails);
            request.setAttribute("tripClosureDetailsSize", tripClosureDetails.size());
            mv = viewSecondaryDriverSettlement(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView viewSecondaryDriverSettlement(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "View Secondary Driver Settlement";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry >> Driver Settlement View";
        String fromDate = "", toDate = "";
        TripTO tripTO = new TripTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/viewSecondaryDriverSettlement.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());

            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
                fromDate = secondaryOperationCommand.getFromDate();
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
                toDate = secondaryOperationCommand.getToDate();
            }
            if (secondaryOperationCommand.getPrimaryDriverId() != null && !"".equals(secondaryOperationCommand.getPrimaryDriverId())) {
                operationTO.setPrimaryDriverId(secondaryOperationCommand.getPrimaryDriverId());
                request.setAttribute("primaryDriverId", secondaryOperationCommand.getPrimaryDriverId());
            }
            String primaryDriver = request.getParameter("primaryDriver");
            request.setAttribute("primaryDriver", primaryDriver);
            if (secondaryOperationCommand.getPrimaryDriver() != null && !"".equals(secondaryOperationCommand.getPrimaryDriver())) {
                operationTO.setPrimaryDriverId(secondaryOperationCommand.getPrimaryDriver());
                request.setAttribute("primaryDriverId", secondaryOperationCommand.getPrimaryDriver());
            }
            String secondaryFleet = request.getParameter("secondaryFleet");
            request.setAttribute("secondaryFleet", secondaryFleet);

            ArrayList getSecondaryFleet = new ArrayList();
            getSecondaryFleet = secondaryOperationBP.getSecondaryFleet(operationTO);
            request.setAttribute("getSecondaryFleet", getSecondaryFleet);
            //System.out.println("secondary fleet:" + getSecondaryFleet.size());

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            //System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                operationTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                operationTO.setToDate(toDate);
            }

            ArrayList driverSettlementDetails = new ArrayList();
            driverSettlementDetails = secondaryOperationBP.getSecondaryDriverSettlementDetails(operationTO);
            request.setAttribute("driverSettlementDetails", driverSettlementDetails);
            request.setAttribute("driverSettlementDetailsSize", driverSettlementDetails.size());

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void selectSecFleetDriver(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();

        TripTO tripTO = new TripTO();

        String path = "";
        PrintWriter pw = response.getWriter();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            String compName = "";
            response.setContentType("text/html");
            compName = request.getParameter("secondaryFleet");
            //System.out.println(" Trip Secondary Fleet:" + compName);
            operationTO.setCompanyName(compName);
            //System.out.println(" Trip Secondary Fleet in :" + operationTO.getCompanyName());
            String roleId = "" + (Integer) session.getAttribute("RoleId");

            tripTO.setRoleId(roleId);

            ArrayList getSecondaryDriver = new ArrayList();
            getSecondaryDriver = secondaryOperationBP.getSecondaryDriverNameForSettelment(operationTO);

            //System.out.println("SecondryDriver.size() = " + getSecondaryDriver.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getSecondaryDriver.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getEmpName());
                jsonObject.put("Id", operationTO.getEmpId());
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewSecondaryTripDriverSettlement(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Secondary Driver Settlement";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry >> Driver Settlement";
        String fromDate = "", toDate = "";
        TripTO tripTO = new TripTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/viewSecondaryTripDriverSettlement.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());

            String settlementId = request.getParameter("settlementId");
            operationTO.setTripSettlementId(settlementId);

            String driverName = request.getParameter("driverName");
            request.setAttribute("driverName", driverName);

            fromDate = request.getParameter("fromDate");
            request.setAttribute("fromDate", fromDate);

            toDate = request.getParameter("toDate");
            request.setAttribute("toDate", toDate);

            String driverId = request.getParameter("driverId");
            request.setAttribute("driverId", driverId);

            operationTO.setFromDate(fromDate);
            operationTO.setToDate(toDate);
            operationTO.setPrimaryDriverId(driverId);

            ArrayList tripSettlementDetails = new ArrayList();
            tripSettlementDetails = secondaryOperationBP.getSecondaryDriverTripSettlementDetails(operationTO);
            request.setAttribute("tripSettlementDetails", tripSettlementDetails);
            request.setAttribute("tripSettlementDetailsSize", tripSettlementDetails.size());

            ArrayList driverSettlementDetails = new ArrayList();
            driverSettlementDetails = secondaryOperationBP.getSecondaryDriverSettlementDetails(operationTO);
            request.setAttribute("driverSettlementDetails", driverSettlementDetails);
            request.setAttribute("driverSettlementDetailsSize", driverSettlementDetails.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSecondaryCustomerContracts(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("viewSecondaryCustomerContracts...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        String menuPath = "";
        menuPath = "Operation  >> View Secondary Contract ";
        String pageTitle = "CityMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
                request.setAttribute("customerName", operationTO.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", operationTO.getCustomerId());
            }
            String billType = request.getParameter("billType");
            request.setAttribute("billType", billType);
            String currentDieselPrice = "";
            currentDieselPrice = operationBP.getCurrentFuelPrice();
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);

            ArrayList SecondaryCustomerVehicleList = new ArrayList();
            SecondaryCustomerVehicleList = secondaryOperationBP.getViewSecondaryCustomerVehicle(operationTO);
            request.setAttribute("SecondaryCustomerVehicleList", SecondaryCustomerVehicleList);
            ArrayList SecondaryCustomerList = new ArrayList();
            SecondaryCustomerList = secondaryOperationBP.getViewSecondaryCustomerdetails(operationTO);
            request.setAttribute("SecondaryCustomerList", SecondaryCustomerList);
            SecondaryOperationTO opTO = new SecondaryOperationTO();
            Iterator itr = SecondaryCustomerList.iterator();
            while (itr.hasNext()) {
                opTO = new SecondaryOperationTO();
                opTO = (SecondaryOperationTO) itr.next();
                currentFuelPrice = opTO.getContractCngCost();
                currentDieselPrice = opTO.getContractDieselCost();
            }
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            ArrayList humanResourceDetails = new ArrayList();
            humanResourceDetails = secondaryOperationBP.getViewHumanResourceDetails(operationTO);
            request.setAttribute("humanResourceDetails", humanResourceDetails);
            //System.out.println("humanResourceDetailssixebdf" + humanResourceDetails.size());

            path = "content/secondaryOperation/viewSecondaryContract.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView customerBillPayment(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/customerBillPayment.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerPaymentDetails = new ArrayList();
            customerPaymentDetails = secondaryOperationBP.getCustomerPaymentDetails(operationTO);
            request.setAttribute("customerPaymentDetails", customerPaymentDetails);
            request.setAttribute("customerPaymentDetailsSize", customerPaymentDetails.size());

            request.setAttribute("fromDate", secondaryOperationCommand.getFromDate());
            request.setAttribute("toDate", secondaryOperationCommand.getToDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCollectBillAmount(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/customerCollectBillDetails.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustId());
            }
            if (secondaryOperationCommand.getCustId() != null && !"".equals(secondaryOperationCommand.getCustId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustId());
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerPaymentDetails = new ArrayList();
            customerPaymentDetails = secondaryOperationBP.getCustomerCollectBillDetails(operationTO);
            request.setAttribute("customerPaymentDetails", customerPaymentDetails);
            request.setAttribute("customerPaymentDetailsSize", customerPaymentDetails.size());

            request.setAttribute("fromDate", secondaryOperationCommand.getFromDate());
            request.setAttribute("toDate", secondaryOperationCommand.getToDate());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleBillPaymentsDetails(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        int userId = (Integer) session.getAttribute("userId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        try {
            path = "content/secondaryOperation/customerBillPaymentDetails.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerPaymentDetails = new ArrayList();
            customerPaymentDetails = secondaryOperationBP.getCustomerPaymentDetails(operationTO);
            request.setAttribute("customerPaymentDetails", customerPaymentDetails);
            request.setAttribute("customerPaymentDetailsSize", customerPaymentDetails.size());

            request.setAttribute("fromDate", secondaryOperationCommand.getFromDate());
            request.setAttribute("toDate", secondaryOperationCommand.getToDate());
            request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editSecondaryCustomerContracts(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("viewSecondaryCustomerContracts...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        String menuPath = "";
        menuPath = "Operation  >> CityMaster ";
        String pageTitle = "CityMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
                request.setAttribute("customerName", operationTO.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", operationTO.getCustomerId());
            }
            String billType = request.getParameter("billType");
            request.setAttribute("billType", billType);
            String currentDieselPrice = "";
            currentDieselPrice = operationBP.getCurrentFuelPrice();
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            String currentFuelPrice = "";
            currentFuelPrice = secondaryOperationBP.getCngPrice();
            request.setAttribute("currentFuelPrice", currentFuelPrice);

            ArrayList SecondaryCustomerVehicleList = new ArrayList();
            SecondaryCustomerVehicleList = secondaryOperationBP.getViewSecondaryCustomerVehicle(operationTO);
            request.setAttribute("SecondaryCustomerVehicleList", SecondaryCustomerVehicleList);
            ArrayList SecondaryCustomerList = new ArrayList();
            SecondaryCustomerList = secondaryOperationBP.getViewSecondaryCustomerdetails(operationTO);
            request.setAttribute("SecondaryCustomerList", SecondaryCustomerList);
            SecondaryOperationTO opTO = new SecondaryOperationTO();
            Iterator itr = SecondaryCustomerList.iterator();
            while (itr.hasNext()) {
                opTO = new SecondaryOperationTO();
                opTO = (SecondaryOperationTO) itr.next();
                currentFuelPrice = opTO.getContractCngCost();
                currentDieselPrice = opTO.getContractDieselCost();
            }
            request.setAttribute("currentFuelPrice", currentFuelPrice);
            request.setAttribute("currentDieselPrice", currentDieselPrice);
            ArrayList humanResourceDetails = new ArrayList();
            humanResourceDetails = secondaryOperationBP.getViewHumanResourceDetails(operationTO);
            if (humanResourceDetails.size() > 0) {
                request.setAttribute("humanResourceDetails", humanResourceDetails);
            }
            //System.out.println("humanResourceDetailssixebdf" + humanResourceDetails.size());
            request.setAttribute("humanResourceDetailsSize", humanResourceDetails.size());

            ArrayList humanResourceList = new ArrayList();
            humanResourceList = secondaryOperationBP.getHumanResourceList();
            request.setAttribute("humanResourceList", humanResourceList);

            path = "content/secondaryOperation/editSecondaryContract.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveEditSecondaryContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String contractId = "";
        String pageTitle = "Create Secondary Customer Contract";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Seconadry Customer Contract >> Create";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //System.out.println("size is  " + userFunctions.size());

            if (secondaryOperationCommand.getFuelCost() != null && !"".equals(secondaryOperationCommand.getFuelCost())) {
                operationTO.setFuelCost(secondaryOperationCommand.getFuelCost());
            }
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }

            if (secondaryOperationCommand.getRouteValidFrom() != null && !"".equals(secondaryOperationCommand.getRouteValidFrom())) {
                operationTO.setRouteValidFrom(secondaryOperationCommand.getRouteValidFrom());
            }
            if (secondaryOperationCommand.getRouteValidTo() != null && !"".equals(secondaryOperationCommand.getRouteValidTo())) {
                operationTO.setRouteValidTo(secondaryOperationCommand.getRouteValidTo());
            }
            if (secondaryOperationCommand.getPointId() != null && !"".equals(secondaryOperationCommand.getPointId())) {
                operationTO.setPointId(secondaryOperationCommand.getPointId());
            }
            if (secondaryOperationCommand.getId() != null && !"".equals(secondaryOperationCommand.getId())) {
                operationTO.setId(secondaryOperationCommand.getId());
            }
            if (secondaryOperationCommand.getContractId() != null && !"".equals(secondaryOperationCommand.getContractId())) {
                operationTO.setContractId(secondaryOperationCommand.getContractId());
            }
            if (secondaryOperationCommand.getAverageKM() != null && !"".equals(secondaryOperationCommand.getAverageKM())) {
                operationTO.setAverageKM(secondaryOperationCommand.getAverageKM());
            }
            if (secondaryOperationCommand.getExtraKmCalculation() != null && !"".equals(secondaryOperationCommand.getExtraKmCalculation())) {
                operationTO.setExtraKmCalculation(secondaryOperationCommand.getExtraKmCalculation());
            }
            if (secondaryOperationCommand.getContractCngCost() != null && !"".equals(secondaryOperationCommand.getContractCngCost())) {
                operationTO.setContractCngCost(secondaryOperationCommand.getContractCngCost());
            }
            if (secondaryOperationCommand.getContractDieselCost() != null && !"".equals(secondaryOperationCommand.getContractDieselCost())) {
                operationTO.setContractDieselCost(secondaryOperationCommand.getContractDieselCost());
            }
            if (secondaryOperationCommand.getRateChangeOfCng() != null && !"".equals(secondaryOperationCommand.getRateChangeOfCng())) {
                operationTO.setRateChangeOfCng(secondaryOperationCommand.getRateChangeOfCng());
            }
            if (secondaryOperationCommand.getRateChangeOfDiesel() != null && !"".equals(secondaryOperationCommand.getRateChangeOfDiesel())) {
                operationTO.setRateChangeOfDiesel(secondaryOperationCommand.getRateChangeOfDiesel());
            }

            if (userId != 0) {
                operationTO.setUserId(userId);
            }

            String[] vehTypeId = request.getParameterValues("vehTypeId");
            String[] noOfVehicle = request.getParameterValues("noOfVehicle");
            String[] extraKmCharge = request.getParameterValues("extraKmCharge");
            String[] fixedKm = request.getParameterValues("fixedKm");
            String[] fixedKmCharge = request.getParameterValues("fixedKmCharge");
            String[] hrId = request.getParameterValues("hrId");
            String[] noOfPersons = request.getParameterValues("noOfPersons");
            String[] fixedAmount = request.getParameterValues("fixedAmount");
            String[] id = request.getParameterValues("id");
            String[] updateValue = request.getParameterValues("updateValue");
            String[] pointId = request.getParameterValues("pointId");
            int insertContractDetails = 0;
            int insertContractMater = 0;
            int insertHumanResource = 0;
            int update = 0;
            if (operationTO.getContractId() != null && !"".equals(operationTO.getContractId())) {
                insertContractMater = Integer.parseInt(operationTO.getContractId());
            }

            update = secondaryOperationBP.updateContractMaster(operationTO, vehTypeId, noOfVehicle, fixedKm, fixedKmCharge, extraKmCharge, id, hrId, noOfPersons, fixedAmount, updateValue, pointId);
//            if (update > 0) {
//                for (int i = 0; i < vehTypeId.length; i++) {
//                    operationTO.setVehTypeId(vehTypeId[i]);
//                    operationTO.setNoOfVehicle(noOfVehicle[i]);
//                    operationTO.setFixedKm(fixedKm[i]);
//                    operationTO.setFixedKmCharge(fixedKmCharge[i]);
//                    operationTO.setExtraKmCharge(extraKmCharge[i]);
//                    operationTO.setId(id[i]);
//                    if (!"0".equals(operationTO.getId())) {
//                        insertContractDetails = secondaryOperationBP.updateContractDetails(operationTO);
//                    } else if ("0".equals(operationTO.getId())) {
//                        insertContractDetails = secondaryOperationBP.insertContractDetails(operationTO, insertContractMater);
//                    }
//                }
//            }
//            if (update > 0) {
//                for (int i = 0; i < hrId.length; i++) {
//                    operationTO.setHrId(hrId[i]);
//                    operationTO.setNoOfPersons(noOfPersons[i]);
//                    operationTO.setFixedAmount(fixedAmount[i]);
//                    //System.out.println("updateValue[i] = " + updateValue[i]);
//                    if ("0".equals(updateValue[i])) {
//                        operationTO.setPointId(pointId[i]);
//                        insertHumanResource = secondaryOperationBP.updateHumanResourceDetails(operationTO);
//                    } else if (!"0".equals(hrId[i])) {
//                        insertHumanResource = secondaryOperationBP.insertHumanResourceDetails(operationTO, insertContractMater);
//                    }
//                }
//            }

            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Updated Successfully....");
            }
            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerList();
            request.setAttribute("CustomerLists", customerList);
            path = "content/Customer/manageCustomer.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void fetchVehicleDriverDetails(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        OperationTO operationTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList list = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        String Values1 = "";
        String Values2 = "";
        try {
            String vehicleId = "";
            response.setContentType("text/html");
            vehicleId = request.getParameter("vehicleId");
            operationTO.setVehicleId(Integer.parseInt(vehicleId));
            list = operationBP.getVehicleDriverMappingForVehicleId(operationTO);
            Iterator itr1 = list.iterator();
            while (itr1.hasNext()) {
                OperationTO operationTO1 = new OperationTO();
                operationTO1 = (OperationTO) itr1.next();
                Values1 = operationTO1.getPrimaryDriverId() + "-" + operationTO1.getSecondaryDriverIdOne() + "-"
                        + operationTO1.getSecondaryDriverIdTwo();
                Values2 = operationTO1.getPrimaryDriverName() + "-"
                        + operationTO1.getSecondaryDriverNameOne() + "-" + operationTO1.getSecondaryDriverNameTwo();
                //System.out.println("Values1 = " + Values1);
                //System.out.println("Values2 = " + Values2);
            }
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            String temp1[] = Values1.split("-");
            String temp2[] = Values2.split("-");
            for (int i = 0; i < temp2.length; i++) {
                if (temp2[i] != "") {
                    jsonObject = new JSONObject();
                    jsonObject.put("Id", temp1[i]);
                    jsonObject.put("Name", temp2[i]);
                    jsonArray.put(jsonObject);
                } else {
                    jsonArray.put(jsonObject);
                }
            }
            pw.print(jsonArray);

            //System.out.println("jsonArray = " + jsonArray);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void checkPointName(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        boolean checkStatus = false;
        String pointName = secondaryOperationCommand.getPointName();
        String customerId = secondaryOperationCommand.getCustomerId();
        operationTO.setPointName(pointName);
        operationTO.setCustomerId(customerId);
        checkStatus = secondaryOperationBP.checkPointName(operationTO);
        //System.out.println("checkStatus = " + checkStatus);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
//        //System.out.println("check Status  " + checkStatus);
        if (checkStatus == true) {
            writer.print("Please Check point Name :" + pointName + "  is Already Exists");
        } else {
            writer.print("");
        }
        writer.close();
    }

//    public ModelAndView viewDateWiseSecondaryTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
//        HttpSession session = request.getSession();
//        int status = 0;
//        String path = "";
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList routeList = new ArrayList();
//        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
//        String menuPath = "";
//        menuPath = "Secondary Operation >> View Trip Schedule ";
//        String pageTitle = "";
//        request.setAttribute("pageTitle", pageTitle);
//        String productId = request.getParameter("productId");
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        //System.out.println("hr" + ":" + "mm" + ":00");
//        secondaryOperationCommand = command;
//        try {
//            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
//                operationTO1.setCustomerId(secondaryOperationCommand.getCustomerId());
//                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
//            }
//            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
//                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
//            }
//            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
//                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
//            }
//            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
//            String dateWiseTripSchedule = request.getParameter("dateWiseTripSchedule");
//            Calendar calendar1 = Calendar.getInstance();
//            Date today = new Date();
//            today = fmt.parse(dateWiseTripSchedule);
//            //System.out.println("today is " + toddMMyy(today));
//            Date nextweekday = DateUtils.addDays(today, 7);
//            //System.out.println("nextweekday = " + toddMMyy(nextweekday));
//
//            //                Date nextweekday = new Date();
//            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
//            //System.out.println(simpleDateformat.format(nextweekday));
//
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(nextweekday);
//            //System.out.println("Current date : " + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
//
//            Calendar now = Calendar.getInstance();
//            //System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
//                    + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
//
//            String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
//            //System.out.println("Current day is : " + strDays[now.get(Calendar.DAY_OF_WEEK) - 1]);
//            String dayName = strDays[calendar.get(Calendar.DAY_OF_WEEK) - 1];
//            int day = 0;
//            int dayStartStatus = 0;
//            int dayEndStatus = 0;
//            if ("Sunday".equals(dayName)) {
//                day = 1;
//                dayStartStatus = 0;
//                dayEndStatus = 6;
//            } else if ("Monday".equals(dayName)) {
//                day = 2;
//                dayStartStatus = 1;
//                dayEndStatus = 5;
//            } else if ("Tuesday".equals(dayName)) {
//                day = 3;
//                dayStartStatus = 2;
//                dayEndStatus = 4;
//            } else if ("Wednesday".equals(dayName)) {
//                day = 4;
//                dayStartStatus = 3;
//                dayEndStatus = 3;
//            } else if ("Thusday".equals(dayName)) {
//                day = 5;
//                dayStartStatus = 4;
//                dayEndStatus = 2;
//            } else if ("Friday".equals(dayName)) {
//                day = 6;
//                dayStartStatus = 5;
//                dayEndStatus = 1;
//            } else if ("Saturday".equals(dayName)) {
//                day = 7;
//                dayStartStatus = 6;
//                dayEndStatus = 0;
//            }
//
//            Date fromDate = DateUtils.addDays(today, -dayStartStatus);
//            //System.out.println("FromDate =" + toddMMyy(fromDate));
//            request.setAttribute("fromDate", toddMMyy(fromDate));
//            Date toDate = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate));
//            request.setAttribute("toDate", toddMMyy(toDate));
//
//            Date toDate1 = DateUtils.addDays(today, dayEndStatus);
//            //System.out.println("To Date = " + toddMMyy(toDate1));
//            List<Date> dates = new ArrayList<Date>();
//            DateFormat formatter;
//            String dayOfWeek = "";
//            formatter = new SimpleDateFormat("dd-MM-yyyy");
//            Date startDate = fromDate;
//            Date endDate = toDate1;
//            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
//            long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
//            long curTime = startDate.getTime();
//            while (curTime <= endTime) {
//                dates.add(new Date(curTime));
//                curTime += interval;
//            }
//            ArrayList SecondaryContractRouteList = new ArrayList();
//            SecondaryContractRouteList = secondaryOperationBP.getSecondaryContractRouteMasterlist(operationTO1);
//            request.setAttribute("SecondaryContractRouteList", SecondaryContractRouteList);
//            ArrayList tripScheduleList = new ArrayList();
//            String tripSchedulStatus = "";
//            String secondaryRouteStatus = "";
//            String temp[] = null;
//            ArrayList dateList = new ArrayList();
//            Iterator itr = SecondaryContractRouteList.iterator();
//            SecondaryOperationTO operationTO2 = new SecondaryOperationTO();
//            while (itr.hasNext()) {
//                tripScheduleList = new ArrayList();
//                operationTO2 = new SecondaryOperationTO();
//                operationTO2 = (SecondaryOperationTO) itr.next();
//                for (int i = 0; i < dates.size(); i++) {
//                    operationTO = new SecondaryOperationTO();
//                    Date lDate = (Date) dates.get(i);
//                    String ds = formatter.format(lDate);
//                    String ds1 = formatter.format(lDate);
//                    ds1 = ds1 + " / " + strDays[i];
//                    operationTO.setDateName(ds1);
//                    operationTO.setDate(ds);
//                    operationTO.setCustomerId(operationTO1.getCustomerId());
//                    operationTO.setSecondaryRouteId(operationTO2.getSecondaryRouteId());
//                    tripSchedulStatus = secondaryOperationBP.getDateWiseTripScheduleStatus(operationTO);
//                    if (tripSchedulStatus != null) {
//                        operationTO.setScheduleStatus(tripSchedulStatus);
//                    } else {
//                        operationTO.setScheduleStatus("N");
//                    }
//                    tripScheduleList.add(operationTO);
//                }
//                dateList.addAll(tripScheduleList);
//
//            }
//            request.setAttribute("dateList", dateList);
//            request.setAttribute("dateWiseTripSchedule", today);
//            path = "content/secondaryOperation/dateWiseSecondaryTripSchedule.jsp";
//            //System.out.println("FromDate =" + toddMMyy(fromDate));
//            //System.out.println("To Date = " + toddMMyy(toDate));
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//
//            return new ModelAndView("content/common/error.jsp");
//
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Secondary Date Wise Trip Schedule --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

    public ModelAndView viewClosedTripForSecondaryBilling(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("viewSecondaryCustomerContracts...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Finance  >> Secondary Billing ";
        String pageTitle = "Secondary Billing ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            path = "content/secondaryOperation/viewClosedTripForSecondaryBilling.jsp";
            ArrayList secondaryCustomer = new ArrayList();
            secondaryCustomer = secondaryOperationBP.getSecondaryCustomerList();
            request.setAttribute("secondaryCustomer", secondaryCustomer);
            if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getBillMonth() != null && !"".equals(secondaryOperationCommand.getBillMonth())) {
                operationTO.setBilledMonth(secondaryOperationCommand.getBillMonth());
                request.setAttribute("billMonth", secondaryOperationCommand.getBillMonth());
            }
            if (secondaryOperationCommand.getBillYear() != null && !"".equals(secondaryOperationCommand.getBillYear())) {
                operationTO.setBillYear(secondaryOperationCommand.getBillYear());
                request.setAttribute("billYear", secondaryOperationCommand.getBillYear());
            }
            ArrayList tripNotSettled = new ArrayList();
            if (!"".equals(operationTO.getCustomerId())) {
                String checkTripClosureForBilling = "";
                checkTripClosureForBilling = secondaryOperationBP.checkTripClosure(operationTO);
                if (checkTripClosureForBilling != null) {
                    request.setAttribute("tripCode", checkTripClosureForBilling);
                    tripNotSettled = secondaryOperationBP.getSecondaryTripNotSettledList(operationTO);
                    request.setAttribute("tripNotSettled", tripNotSettled);
                } else {
                    ArrayList secondaryBillingList = new ArrayList();
                    secondaryBillingList = secondaryOperationBP.getSecondaryBillingList(operationTO);
                    if (secondaryBillingList.size() > 0) {
                        request.setAttribute("secondaryBillingList", secondaryBillingList);
                    }
                }
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView generateSecondaryBilling(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("viewSecondaryCustomerContracts...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Finance  >> Secondary Billing ";
        String pageTitle = "Secondary Billing ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            operationTO.setVehicleTypeId(vehicleTypeId);
            String customerId = request.getParameter("customerId");
            request.setAttribute("customerId", customerId);
            operationTO.setCustomerId(customerId);
            String billedMonth = request.getParameter("billedMonth");
            String temp[] = null;
            temp = billedMonth.split("-");
            operationTO.setBilledMonth(temp[0]);
            operationTO.setBillYear(temp[1]);
            path = "content/secondaryOperation/viewTripBillDetails.jsp";
            ArrayList secondaryCustomer = new ArrayList();
            secondaryCustomer = secondaryOperationBP.getSecondaryCustomerList();
            request.setAttribute("secondaryCustomer", secondaryCustomer);
            ArrayList secondaryBillingList = new ArrayList();
            secondaryBillingList = secondaryOperationBP.getSecondaryBillingList(operationTO);
            if (secondaryBillingList.size() > 0) {
                request.setAttribute("secondaryBillingList", secondaryBillingList);
            }
            ArrayList secondaryBillingTripList = new ArrayList();
            secondaryBillingTripList = secondaryOperationBP.getSecondaryBillingTripList(operationTO);
            if (secondaryBillingTripList.size() > 0) {
                request.setAttribute("secondaryBillingTripList", secondaryBillingTripList);
            }
            SecondaryOperationTO opTO = null;
            double totalHumanResource = 0.0f;
            ArrayList viewHumanResourceList = new ArrayList();
            viewHumanResourceList = secondaryOperationBP.getViewHumanResourceDetails(operationTO);
            if (viewHumanResourceList.size() > 0) {
                Iterator itr = viewHumanResourceList.iterator();
                while (itr.hasNext()) {
                    opTO = (SecondaryOperationTO) itr.next();
                    totalHumanResource += Double.parseDouble(opTO.getTotalAmount());
                    request.setAttribute("totalHumanResource", totalHumanResource);
                }
            }

            String vehicleTypeFuelDetails = "";
            String temp1[] = null;
            vehicleTypeFuelDetails = secondaryOperationBP.getVehicleTypeFuelDetails(operationTO);
            if (vehicleTypeFuelDetails != null) {
                temp1 = vehicleTypeFuelDetails.split("-");
                request.setAttribute("fuelTypeId", temp1[0]);
                operationTO.setFuelTypeId(temp1[0]);
                request.setAttribute("fuelPrice", temp1[1]);
                operationTO.setFuelPrice(temp1[1]);
            }
            String contractFuel = "";
            double contractFuelAmount = 0.0f;
            contractFuel = secondaryOperationBP.getContractFuelAmount(operationTO);
            String temp2[] = null;
            if (contractFuel != null) {
                temp2 = contractFuel.split("~");
                request.setAttribute("contractFuelCost", temp2[0]);
                request.setAttribute("contractFuelRateChange", temp2[1]);
            }

            double deviateAmount = 0.0f;
            String vehicleTypeCngDeviationCost = "";
            vehicleTypeCngDeviationCost = secondaryOperationBP.getVehicleTypeCngDeviation(operationTO);
            if (vehicleTypeCngDeviationCost != null) {
                deviateAmount = Double.parseDouble(vehicleTypeCngDeviationCost);
                if (deviateAmount >= 0) {
                    request.setAttribute("deviateAmount", deviateAmount);
                    request.setAttribute("type", "add");
                } else {
                    request.setAttribute("deviateAmount", deviateAmount * -1);
                    request.setAttribute("type", "sub");
                    request.setAttribute("type", "sub");
                }
            } else {
                request.setAttribute("deviateAmount", 0);
                request.setAttribute("type", "add");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void checkSecondaryRouteName(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        boolean checkStatus = false;
        String routeName = secondaryOperationCommand.getRouteName();
        String customerId = secondaryOperationCommand.getCustomerId();
        operationTO.setRouteName(routeName);
        operationTO.setCustomerId(customerId);
        checkStatus = secondaryOperationBP.checkRouteName(operationTO);
        //System.out.println("checkStatus = " + checkStatus);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
//        //System.out.println("check Status  " + checkStatus);
        if (checkStatus == true) {
            writer.print("Please Check Route" + routeName + "  is Already Exists");
        } else {
            writer.print("");
        }
        writer.close();
    }

    /**
     * This method is used to Get Driver Name.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void getSecondaryDriverName(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
            driverName = request.getParameter("driverName");
            operationTO.setDriverName(driverName);
            userDetails = secondaryOperationBP.getSecondaryDriverName(operationTO);
            //System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getEmpName());
                jsonObject.put("Id", operationTO.getEmpId());
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getPrimaryDriverName(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
            driverName = request.getParameter("driverName");
            operationTO.setDriverName(driverName);
            userDetails = secondaryOperationBP.getPrimaryDriverName(operationTO);
            //System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getEmpName());
                jsonObject.put("Id", operationTO.getEmpId());
                jsonObject.put("settledDate", operationTO.getSettledDate());
                jsonObject.put("mobileNo", operationTO.getMobileNo());
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView addSlabRate(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("addSlabRate...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Finance  >> addSlabRate ";
        String pageTitle = "addSlabRate ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int insertSlabRate = 0;
        int updateSlabRate = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {

            path = "content/route/addSlabRate.jsp";
            ArrayList slabTypeList = new ArrayList();
            slabTypeList = secondaryOperationBP.getSlabTypeList();
            request.setAttribute("slabTypeList", slabTypeList);
            //System.out.println("slabTypeListSize   secondaryOperation " + slabTypeList.size());

            ArrayList leasingCustList = new ArrayList();
            leasingCustList = vehicleBP.getLeasingCustomerList();
            request.setAttribute("leasingCustList", leasingCustList);
            //System.out.println("leasingCustList    secondaryOperation" + leasingCustList.size());

            ArrayList customerList = new ArrayList();
            customerList = misBP.getcustomerList();
            request.setAttribute("customerList", customerList);

            String vehicleTypeId = "";
            String routeCostId = "";
            String routeId = "";
            String slabId = "";
            String slabRateId = "";
            String customerId = "";
            String slabRate = "";
            String status = "";
            String vehicleTypeName = "";
            vehicleTypeId = request.getParameter("vehicleTypeId");
            vehicleTypeName = request.getParameter("vehicleTypeName");
            routeCostId = request.getParameter("routeCostId");
            routeId = request.getParameter("routeId");
            slabId = request.getParameter("slabId");
            slabRateId = request.getParameter("slabRateId");
            customerId = request.getParameter("customerId");
            slabRate = request.getParameter("slabRate");
            status = request.getParameter("status");
            //System.out.println("slabVehicleId" + vehicleTypeId);
            //System.out.println("vehicleTypeName" + vehicleTypeName);
            request.setAttribute("slabVehicleTypeName", vehicleTypeName);
            request.setAttribute("slabVehicleId", vehicleTypeId);
            request.setAttribute("slabRouteCostId", routeCostId);
            request.setAttribute("slabRouteId", routeId);
            operationTO.setVehicleTypeId(vehicleTypeId);
            operationTO.setRouteCostId(routeCostId);
            operationTO.setRouteId(routeId);
            operationTO.setSlabRateId(slabRateId);
            operationTO.setSlabId(slabId);
            operationTO.setCustomerId(customerId);
            operationTO.setSlabRate(slabRate);
            operationTO.setStatus(status);
            //System.out.println("vehicleTypeId     " + vehicleTypeId);
            //System.out.println("routeCostId       " + routeCostId);
            //System.out.println("routeId           " + routeId);
            //System.out.println("slabId           " + slabId);
            //System.out.println("slabRateId           " + slabRateId);
            if (slabRateId != null && !"".equals(slabRateId)) {
                updateSlabRate = secondaryOperationBP.saveSlapRate(operationTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");
            }
            if (slabId != null && !"".equals(slabId)) {
                insertSlabRate = secondaryOperationBP.saveSlapRate(operationTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted SucessFully");
            }
            ArrayList vehicleSlabrateList = new ArrayList();
            vehicleSlabrateList = secondaryOperationBP.getvehicleSlabrateList(operationTO);
            request.setAttribute("vehicleSlabrateList", vehicleSlabrateList);
            //System.out.println("vehicleSlabrateListSize3 " + vehicleSlabrateList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView vehicleStandingLongTripDetails(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        //System.out.println("addSlabRate...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Trip  >> Vehicle Transit Period ";
        String pageTitle = "addSlabRate ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int insertVehicleTransitPeriod = 0;
        int userId = (Integer) session.getAttribute("userId");

        try {

            path = "content/secondaryOperation/vehicleStandingLongTripDetails.jsp";


            String tripSheetId = "";
            String vehicleId = "";
            String tripType = "";
            String vehicleNo = "";
            String tripCode = "";
            String status = "";
            String freezeInDate = "";
            String freezeInTime = "";
            String freezeOutDate = "";
            String freezeOutTime = "";
            String currentLocation = "";
            String remarks = "";
            String incidentId = "";
            incidentId = request.getParameter("incidentId");
            tripSheetId = request.getParameter("tripSheetId");
            vehicleId = request.getParameter("vehicleId");
            tripType = request.getParameter("tripType");
            vehicleNo = request.getParameter("vehicleNo");
            tripCode = request.getParameter("tripCode");
            status = request.getParameter("status");
            freezeInDate = request.getParameter("freezeInDate");
            freezeInTime = request.getParameter("freezeInTime");
            freezeOutDate = request.getParameter("freezeOutDate");
            freezeOutTime = request.getParameter("freezeOutTime");
            currentLocation = request.getParameter("currentLocation");
            remarks = request.getParameter("remarks");
            //System.out.println("tripSheetId" + tripSheetId);
            //System.out.println("vehicleId" + vehicleId);
            request.setAttribute("vslTripSheetId", tripSheetId);
            request.setAttribute("vslVehicleId", vehicleId);
            request.setAttribute("vslStatus", status);
            request.setAttribute("vslTripType", tripType);
            operationTO.setFreezeOutDate(freezeOutDate);
            operationTO.setFreezeOutTime(freezeOutTime);
            operationTO.setIncidentId(incidentId);
            operationTO.setStatus(status);
            operationTO.setVehicleId(vehicleId);
            operationTO.setTripSheetId(tripSheetId);
            operationTO.setTripType(tripType);
            operationTO.setVehicleNo(vehicleNo);
            operationTO.setTripCode(tripCode);
            operationTO.setFreezeInDate(freezeInDate);
            operationTO.setFreezeInTime(freezeInTime);
            operationTO.setCurrentLocation(currentLocation);
            operationTO.setRemarks(remarks);
            if (freezeInDate != null && !"".equals(freezeInDate)) {
                insertVehicleTransitPeriod = secondaryOperationBP.saveVehicleTransitPeriod(operationTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted SucessFully");
            }
            ArrayList vehicleTransitPeriodList = new ArrayList();
            vehicleTransitPeriodList = secondaryOperationBP.getVehicleTransitPeriodList(operationTO);
            request.setAttribute("vehicleTransitPeriodList", vehicleTransitPeriodList);
            //System.out.println("vehicleTransitPeriodListSize3 " + vehicleTransitPeriodList.size());
            ArrayList getIncident = new ArrayList();
            getIncident = secondaryOperationBP.getIncident(operationTO);
            request.setAttribute("getIncident", getIncident);


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView getTollGateDetail(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();

        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Toll Gate  >> Add Toll Gate ";
        String pageTitle = "addTollGate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {


            ArrayList tollGateMasterList = new ArrayList();
            tollGateMasterList = secondaryOperationBP.tollGateMasterList(operationTO);
            //System.out.println("tollGateMasterList" + tollGateMasterList.size());
            request.setAttribute("tollGateMasterList", tollGateMasterList);
            path = "content/secondaryOperation/viewTollgateMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView addTollgateMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();

        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Toll Gate  >> Add Toll Gate ";
        String pageTitle = "addTollGate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String tollId = request.getParameter("tollId");
            //System.out.println("tollId" + tollId);
            if (tollId == null || "".equals(tollId)) {
                tollId = "0";
            }
            operationTO.setTollId(tollId);

            ArrayList tollGateMasterList = new ArrayList();
            tollGateMasterList = secondaryOperationBP.tollGateMasterList(operationTO);
            //System.out.println("tollGateMasterList" + tollGateMasterList.size());
            request.setAttribute("tollGateMasterList", tollGateMasterList);

            ArrayList tollDetailsList = new ArrayList();
            tollDetailsList = secondaryOperationBP.getTollgatDetailsList(operationTO);
            //System.out.println("tollMasterList" + tollDetailsList.size());
            request.setAttribute("tollMasterList", tollDetailsList);
            request.setAttribute("tollId", tollId);

            path = "content/secondaryOperation/addTollgateMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView editTollgateMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        HttpSession session = request.getSession();

        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        secondaryOperationCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Toll Gate  >> Add Toll Gate ";
        String pageTitle = "addTollGate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String tollId = request.getParameter("tollId");
            operationTO.setTollId(tollId);
            String param = request.getParameter("param");
            request.setAttribute("param", param);
            String paramEdit = request.getParameter("paramEdit");
            request.setAttribute("paramEdit", paramEdit);
            //System.out.println("param" + param);
            //System.out.println("paramEdit" + paramEdit);

            ArrayList tollGateMasterList = new ArrayList();
            tollGateMasterList = secondaryOperationBP.tollGateMasterList(operationTO);
            //System.out.println("tollGateMasterList" + tollGateMasterList.size());
            request.setAttribute("tollGateMasterList", tollGateMasterList);
            String tollName = "";
            String googleCityName = "";
            String latitude = "";
            String longitude = "";
            String cityId = "";
            String cityName = "";
            String zoneId = "";
            String zoneName = "";
            String countryId = "";
            String countryName = "";
            String currencyCode = "";
            String fromDate = "";
            String toDate = "";
            Iterator itr = tollGateMasterList.iterator();
            while (itr.hasNext()) {
                operationTO = new SecondaryOperationTO();
                operationTO = (SecondaryOperationTO) itr.next();
                tollName = operationTO.getTollName();
                googleCityName = operationTO.getGoogleCityName();
                latitude = operationTO.getLatitude();
                longitude = operationTO.getLongitude();
                cityId = operationTO.getCityId();
                cityName = operationTO.getCityName();
                zoneId = operationTO.getZoneId();
                zoneName = operationTO.getZoneName();
                countryId = operationTO.getCountryId();
                countryName = operationTO.getCountryName();
                currencyCode = operationTO.getCurrencyCode();
                fromDate = operationTO.getFromDate();
                toDate = operationTO.getToDate();
            }





            ArrayList tollDetailList = new ArrayList();
            tollDetailList = secondaryOperationBP.getTollgatDetailsList(operationTO);
            //System.out.println("tollMasterList" + tollDetailList.size());
            request.setAttribute("tollMasterList", tollDetailList);

            request.setAttribute("tollId", operationTO.getTollId());
            request.setAttribute("tollName", tollName);
            //System.out.println(" operationTO.getTollName()" + operationTO.getTollName());
            request.setAttribute("googleCityName", googleCityName);
            request.setAttribute("latitude", latitude);
            request.setAttribute("longitude", longitude);
            request.setAttribute("cityId", cityId);
            request.setAttribute("cityName", cityName);
            request.setAttribute("zoneId", zoneId);
            request.setAttribute("zoneName", zoneName);
            request.setAttribute("countryId", countryId);
            request.setAttribute("countryName", countryName);
            request.setAttribute("currencyCode", currencyCode);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

//            

            path = "content/secondaryOperation/addTollgateMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public void getCityTollList(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList zoneList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityName = "";
            response.setContentType("text/html");
            cityName = request.getParameter("cityName");
            operationTO.setCityName(cityName);
            zoneList = secondaryOperationBP.getCityTollList(operationTO);
            ////////System.out.println("zoneList.size() = " + zoneList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = zoneList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getCityId() + "-" + operationTO.getCityName());
                ////////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            ////////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void cityNameList(HttpServletRequest request, HttpServletResponse response) throws IOException, FPBusinessException {
        //System.out.println("..........ajax in controller.....................");
        HttpSession session = request.getSession();
        String cityName = request.getParameter("cityName");
        //System.out.println("regNo--->" + cityName);
        String CityName = "";
        CityName = secondaryOperationBP.cityNameList(cityName);
        //System.out.println("CityName" + CityName);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(CityName);
        writer.close();
    }

    public void insertTollgetDetails(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        secondaryOperationCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        //System.out.println("in controller");

        int madhavaramSp = 1011;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        try {
            if (secondaryOperationCommand.getTollName() != null && !"".equals(secondaryOperationCommand.getTollName())) {
                operationTO.setTollName(secondaryOperationCommand.getTollName());
            }
            if (secondaryOperationCommand.getCountryId() != null && !"".equals(secondaryOperationCommand.getCountryId())) {
                operationTO.setCountryId(secondaryOperationCommand.getCountryId());
            }
            if (secondaryOperationCommand.getCurrencyCode() != null && !"".equals(secondaryOperationCommand.getCurrencyCode())) {
                operationTO.setCurrencyCode(secondaryOperationCommand.getCurrencyCode());
            }
            if (secondaryOperationCommand.getLatitude() != null && !"".equals(secondaryOperationCommand.getLatitude())) {
                operationTO.setLatitude(secondaryOperationCommand.getLatitude());
            }
            if (secondaryOperationCommand.getLongitude() != null && !"".equals(secondaryOperationCommand.getLongitude())) {
                operationTO.setLongitude(secondaryOperationCommand.getLongitude());
            }

            if (secondaryOperationCommand.getCityName() != null && !"".equals(secondaryOperationCommand.getCityName())) {
                operationTO.setCityName(secondaryOperationCommand.getCityName());
            }
            if (secondaryOperationCommand.getZoneId() != null && !"".equals(secondaryOperationCommand.getZoneId())) {
                operationTO.setZoneId(secondaryOperationCommand.getZoneId());
            }
            if (secondaryOperationCommand.getStatus() != null && !"".equals(secondaryOperationCommand.getStatus())) {
                operationTO.setStatus(secondaryOperationCommand.getStatus());
            }

            if (secondaryOperationCommand.getGoogleCityName() != null && !"".equals(secondaryOperationCommand.getGoogleCityName())) {
                operationTO.setGoogleCityName(secondaryOperationCommand.getGoogleCityName());
            }
            if (secondaryOperationCommand.getCityId() != null && !"".equals(secondaryOperationCommand.getCityId())) {
                operationTO.setCityId(secondaryOperationCommand.getCityId());
            }
            if (secondaryOperationCommand.getFromDate() != null && !"".equals(secondaryOperationCommand.getFromDate())) {
                operationTO.setFromDate(secondaryOperationCommand.getFromDate());
            }
            if (secondaryOperationCommand.getToDate() != null && !"".equals(secondaryOperationCommand.getToDate())) {
                operationTO.setToDate(secondaryOperationCommand.getToDate());
            }
//            if (secondaryOperationCommand.getTollId()!= null && !"".equals(secondaryOperationCommand.getTollId())) {
//                operationTO.setTollId(secondaryOperationCommand.getTollId());
//                
//            }

            String[] vehTypeId = request.getParameterValues("vehTypeIdVal[]");
            String[] axleName = request.getParameterValues("axleNameVal[]");
            String[] monthlyPass = request.getParameterValues("monthlyPassVal[]");
            String[] singleJourney = request.getParameterValues("singleJourneyVal[]");
            String[] returnJourney = request.getParameterValues("returnJourneyVal[]");
            String[] localVehicleCost = request.getParameterValues("localVehicleCostVal[]");
            String[] tollRateId = request.getParameterValues("tollRateIdVal[]");
            //System.out.println("tollRateId" + tollRateId);
            String tollId = request.getParameter("tollId");
            //System.out.println("tollId" + tollId);
            operationTO.setTollId(tollId);
            int insertStatus = 0;
            //System.out.println("else...................");
            insertStatus = secondaryOperationBP.insertTollgetDetails(operationTO, userId, madhavaramSp);
            //System.out.println("insertStatus" + insertStatus);

            if (insertStatus > 0) {
                for (int i = 0; i < vehTypeId.length; i++) {
                    //System.out.println("tollRateId" + tollRateId[i]);
                    //System.out.println("vehTypeId" + vehTypeId[i]);
                    //System.out.println("axleName" + axleName[i]);
                    //System.out.println("monthlyPass" + monthlyPass[i]);
                    //System.out.println("singleJourney" + singleJourney[i]);
                    //System.out.println("returnJourney" + returnJourney[i]);
                    //System.out.println("vehTypeId" + localVehicleCost[i]);

                    int Status = secondaryOperationBP.insertTollgetRateDetails(operationTO, userId, axleName[i], vehTypeId[i], monthlyPass[i], singleJourney[i], returnJourney[i], localVehicleCost[i], tollRateId[i], insertStatus);
                }
            }

            response.setContentType("text/css");
            if (insertStatus > 0) {
                response.getWriter().println(insertStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    public ModelAndView transportCustomerContract(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/createTransportCustomerContract.jsp";
            setLocale(request, response);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            String customerId = "";
            customerId = request.getParameter("custId");
            //System.out.println("customerId:" + customerId);
            operationTO.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getTransportCustomer(operationTO);
            String[] temp = null;
            if (customerDetails != null) {
                temp = customerDetails.split("~");
                request.setAttribute("customerId", customerId);
                request.setAttribute("custId", temp[0]);
                request.setAttribute("customerCode", temp[1]);
                request.setAttribute("customerName", temp[2]);
                request.setAttribute("custPhone", temp[3]);
                request.setAttribute("custMobile", temp[4]);
                request.setAttribute("custEmail", temp[5]);
                request.setAttribute("custAddress", temp[6]);
                request.setAttribute("pincode", temp[7]);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTransportCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/Customer/manageTransportCustomer.jsp";
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getStartDate() != null && secondaryOperationCommand.getStartDate() != "") {
                secTO.setStartDate(secondaryOperationCommand.getStartDate());
            }
            if (secondaryOperationCommand.getEndDate() != null && secondaryOperationCommand.getEndDate() != "") {
                secTO.setEndDate(secondaryOperationCommand.getEndDate());
            }
            if (secondaryOperationCommand.getBillingKmCalculationId() != null && secondaryOperationCommand.getBillingKmCalculationId() != "") {
                secTO.setBillingKmCalculationId(secondaryOperationCommand.getBillingKmCalculationId());
            }
            if (secondaryOperationCommand.getDriverResponsibility() != null && secondaryOperationCommand.getDriverResponsibility() != "") {
                secTO.setDriverResponsibility(secondaryOperationCommand.getDriverResponsibility());
            }
            if (secondaryOperationCommand.getFuelResponsibility() != null && secondaryOperationCommand.getFuelResponsibility() != "") {
                secTO.setFuelResponsibility(secondaryOperationCommand.getFuelResponsibility());
            }
            if (secondaryOperationCommand.getMaxAllowedKm() != null && secondaryOperationCommand.getMaxAllowedKm() != "") {
                secTO.setMaxAllowedKm(secondaryOperationCommand.getMaxAllowedKm());
            }
            if (secondaryOperationCommand.getExtraRunKmCost() != null && secondaryOperationCommand.getExtraRunKmCost() != "") {
                secTO.setExtraRunKmCost(secondaryOperationCommand.getExtraRunKmCost());
            }

            if (secondaryOperationCommand.getBillingTypeId() != null && secondaryOperationCommand.getBillingTypeId() != "") {
                secTO.setBillingTypeId(secondaryOperationCommand.getBillingTypeId());
            }
            String driverResponsibility = request.getParameter("driverResponsibility");
            secTO.setDriverResponsibility(driverResponsibility);
            //System.out.println("i m here/...");

            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            String[] vehicleUnits = request.getParameterValues("vehicleUnits");
            String[] contractType = request.getParameterValues("contractType");
            String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");
            String[] totlaFixedCost = request.getParameterValues("totlaFixedCost");
            String[] extraVehicleRunKm = request.getParameterValues("extraVehicleRunKm");
            String[] iCnt = request.getParameterValues("iCnt");
            String[] iCnt1 = request.getParameterValues("iCnt1");
            String[] trailerTypeId = request.getParameterValues("trailerTypeId");
            String[] trailerTypeUnits = request.getParameterValues("trailerTypeUnits");
            String[] trailerContractType = request.getParameterValues("trailerContractType");
            String[] fixedCostPerTrailer = request.getParameterValues("fixedCostPerTrailer");
            String[] totalTrailerCost = request.getParameterValues("totalTrailerCost");
            String[] holidayType = request.getParameterValues("holidayType");
            String[] compensation = request.getParameterValues("compensation");
            //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
            int contractId = secondaryOperationBP.processInsertTransportCustomerContract(secTO, userId);
            if (contractId > 0) {

                //insert Vehicle Details
                if (vehicleTypeId.length > 0) {
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String vehicleUnit = vehicleUnits[j];
                        String contractTypes = contractType[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String totlaFixedCosts = totlaFixedCost[j];
                        String extraVehicleRunKms = extraVehicleRunKm[j];
                        //System.out.println("vehicleTypeIds:" + vehicleTypeIds + "vehicleUnit:" + vehicleUnit + "contractTypes");
                        //System.out.println("fixedCostPerVehicles:" + fixedCostPerVehicles + "totlaFixedCosts:" + totlaFixedCosts + "contractTypes" + "extraVehicleRunKms:" + extraVehicleRunKms);

                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerVehicleContract(secTO, userId, contractId, vehicleTypeIds, vehicleUnit, contractTypes, fixedCostPerVehicles, totlaFixedCosts, extraVehicleRunKms);
                        }

                    }
                }
                //Insert Trailer Details
                if (trailerTypeId.length > 0) {
                    for (int j = 0; j < trailerTypeId.length; j++) {

                        String trailerTypeIds = trailerTypeId[j];
                        String trailerTypeUnit = trailerTypeUnits[j];
                        String trailerContractTypes = trailerContractType[j];
                        String fixedCostPerTrailers = fixedCostPerTrailer[j];
                        String totalTrailerCosts = totalTrailerCost[j];

                        if (!"".equals(trailerTypeIds) && !"0".equals(trailerTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerTrailerContract(secTO, userId, contractId, trailerTypeIds, trailerTypeUnit, trailerContractTypes, fixedCostPerTrailers, totalTrailerCosts);
                        }

                    }
                }
                // Insert Driver Compensation
                if (holidayType.length > 0) {
                    for (int j = 0; j < holidayType.length; j++) {
                        String typeId = holidayType[j];
                        String compensations = compensation[j];
                        if (!"".equals(typeId) && !"0".equals(typeId)) {
                            status = secondaryOperationBP.processInsertTransportCustomerDriverCompensation(secTO, userId, contractId, typeId, compensations);
                        }
                    }
                }
            }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Leasing contract saved successfully");

            }
            path = "handleViewTransportCustomer.do?customerType=2";
            // }


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView editTransportCustomerContract(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/editTransportCustomerContract.jsp";

            setLocale(request, response);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            String customerId = "";
            customerId = request.getParameter("custId");
            //System.out.println("customerId:" + customerId);
            operationTO.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getTransportCustomer(operationTO);
            String[] temp = null;
            if (customerDetails != null) {
                temp = customerDetails.split("~");
                request.setAttribute("customerId", customerId);
                request.setAttribute("custId", temp[0]);
                request.setAttribute("customerCode", temp[1]);
                request.setAttribute("customerName", temp[2]);
                request.setAttribute("custPhone", temp[3]);
                request.setAttribute("custMobile", temp[4]);
                request.setAttribute("custEmail", temp[5]);
                request.setAttribute("custAddress", temp[6]);
                request.setAttribute("pincode", temp[7]);

            }
            ArrayList contractDetails = new ArrayList();
            contractDetails = secondaryOperationBP.getTransportCustomerContractDetails(operationTO);
            request.setAttribute("contractDetails", contractDetails);
            if (contractDetails.size() > 0) {
                Iterator itr1 = contractDetails.iterator();
                while (itr1.hasNext()) {
                    SecondaryOperationTO secTo = (SecondaryOperationTO) itr1.next();
                    request.setAttribute("contractFrom", secTo.getFromDate());
                    request.setAttribute("contractTo", secTo.getToDate());
                    request.setAttribute("billingType", secTo.getBillingTypeId());
                    request.setAttribute("billingKmCalculationType", secTo.getBillingKmCalculationId());
                    //System.out.println("billingKmCalculationType:" + secTo.getBillingKmCalculationId());
                    request.setAttribute("maxAllowedKm", secTo.getMaxAllowedKm());
                    request.setAttribute("driverResponsibility", secTo.getDriverResponsibility());
                    request.setAttribute("fuelResponsibility", secTo.getFuelResponsibility());
                    request.setAttribute("extraRunKmCost", secTo.getExtraRunKmCost());
                    request.setAttribute("contractId", secTo.getContractId());
                }

            }
            ArrayList contractVehicleDetails = new ArrayList();
            contractVehicleDetails = secondaryOperationBP.getTransportCustomerVehicleDetails(operationTO);
            request.setAttribute("contractVehicleDetails", contractVehicleDetails);
            ArrayList contractTrailerDetails = new ArrayList();
            contractTrailerDetails = secondaryOperationBP.getTransportCustomerTrailerDetails(operationTO);
            request.setAttribute("contractTrailerDetails", contractTrailerDetails);
            ArrayList driverCompensationDetails = new ArrayList();
            driverCompensationDetails = secondaryOperationBP.getTransportCustomerDriverCompensationDetails(operationTO);
            request.setAttribute("driverCompensationDetails", driverCompensationDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEditTransportCustomerContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        ModelAndView mv = null;
        path = "content/Customer/manageTransportCustomer.jsp";
        setLocale(request, response);
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getStartDate() != null && secondaryOperationCommand.getStartDate() != "") {
                secTO.setStartDate(secondaryOperationCommand.getStartDate());
            }
            if (secondaryOperationCommand.getEndDate() != null && secondaryOperationCommand.getEndDate() != "") {
                secTO.setEndDate(secondaryOperationCommand.getEndDate());
            }
            if (secondaryOperationCommand.getBillingKmCalculationId() != null && secondaryOperationCommand.getBillingKmCalculationId() != "") {
                secTO.setBillingKmCalculationId(secondaryOperationCommand.getBillingKmCalculationId());
            }
            if (secondaryOperationCommand.getDriverResponsibility() != null && secondaryOperationCommand.getDriverResponsibility() != "") {
                secTO.setDriverResponsibility(secondaryOperationCommand.getDriverResponsibility());
            }
            if (secondaryOperationCommand.getFuelResponsibility() != null && secondaryOperationCommand.getFuelResponsibility() != "") {
                secTO.setFuelResponsibility(secondaryOperationCommand.getFuelResponsibility());
            }
            if (secondaryOperationCommand.getMaxAllowedKm() != null && secondaryOperationCommand.getMaxAllowedKm() != "") {
                secTO.setMaxAllowedKm(secondaryOperationCommand.getMaxAllowedKm());
            }
            if (secondaryOperationCommand.getExtraRunKmCost() != null && secondaryOperationCommand.getExtraRunKmCost() != "") {
                secTO.setExtraRunKmCost(secondaryOperationCommand.getExtraRunKmCost());
            }

            if (secondaryOperationCommand.getBillingTypeId() != null && secondaryOperationCommand.getBillingTypeId() != "") {
                secTO.setBillingTypeId(secondaryOperationCommand.getBillingTypeId());
            }

            //System.out.println("secondaryOperationCommand.getDriverResponsibility()" + secondaryOperationCommand.getDriverResponsibility());
            //System.out.println("secTO.getDriverResponsibility:" + secTO.getDriverResponsibility());

            String driverResponsibility = request.getParameter("driverResponsibility");
            secTO.setDriverResponsibility(driverResponsibility);
            //System.out.println("driverResponsibility" + driverResponsibility);
            int contractId = Integer.parseInt(request.getParameter("contractId"));
            secTO.setContractId(String.valueOf(contractId));
            // update Contract Details
            int contractUpdate = secondaryOperationBP.processUpdateTransportCustomerContract(secTO, userId);
            //System.out.println("contractUpdate" + contractUpdate);
// update Exsiting Record

            // vehicle contract Details
            String[] vehicleContractId = request.getParameterValues("vehicleContractId");
            String[] fixedCostPerVehicleE = request.getParameterValues("fixedCostPerVehicleE");
            String[] totlaFixedCostE = request.getParameterValues("totlaFixedCostE");
            //System.out.println("vehicleContractId length::" + vehicleContractId.length);
            if (vehicleContractId != null) {
                status = secondaryOperationBP.updateTransportCustomerVehicleContract(vehicleContractId, fixedCostPerVehicleE, totlaFixedCostE, userId);

            }
            // Trailer Contract Deatils
            String[] trailerContractId = request.getParameterValues("trailerContractId");
            String[] fixedCostPerTrailerE = request.getParameterValues("costPerTrailerPerMonthE");
            String[] totalTrailerFixedCostE = request.getParameterValues("totalTrailerCostE");
            if (trailerContractId != null) {
                status = secondaryOperationBP.updateTransportCustomerTrailerContract(trailerContractId, fixedCostPerTrailerE, totalTrailerFixedCostE, userId);

            }
            // End

            //Insert New Contract
            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            String[] vehicleUnits = request.getParameterValues("vehicleUnits");
            String[] contractType = request.getParameterValues("contractType");
            String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");
            String[] totlaFixedCost = request.getParameterValues("totlaFixedCost");
            String[] extraVehicleRunKm = request.getParameterValues("extraVehicleRunKm");
            String[] iCnt = request.getParameterValues("iCnt");
            String[] iCnt1 = request.getParameterValues("iCnt1");
            String[] trailerTypeId = request.getParameterValues("trailerTypeId");
            String[] trailerTypeUnits = request.getParameterValues("trailerTypeUnits");
            String[] trailerContractType = request.getParameterValues("trailerContractType");
            String[] fixedCostPerTrailer = request.getParameterValues("fixedCostPerTrailer");
            String[] totalTrailerCost = request.getParameterValues("totalTrailerCost");
            String[] holidayType = request.getParameterValues("holidayType");
            String[] compensation = request.getParameterValues("compensation");

            // int contractId = secondaryOperationBP.processInsertTransportCustomerContract(secTO, userId);
            if (contractId > 0) {

                //insert Vehicle Details

                if (vehicleTypeId != null && vehicleTypeId.length > 0) {
                    //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String vehicleUnit = vehicleUnits[j];
                        String contractTypes = contractType[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String totlaFixedCosts = totlaFixedCost[j];
                        String extraVehicleRunKms = extraVehicleRunKm[j];
                        //System.out.println("vehicleTypeIds:" + vehicleTypeIds + "vehicleUnit:" + vehicleUnit + "contractTypes");
                        //System.out.println("fixedCostPerVehicles:" + fixedCostPerVehicles + "totlaFixedCosts:" + totlaFixedCosts + "contractTypes" + "extraVehicleRunKms:" + extraVehicleRunKms);

                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerVehicleContract(secTO, userId, contractId, vehicleTypeIds, vehicleUnit, contractTypes, fixedCostPerVehicles, totlaFixedCosts, extraVehicleRunKms);
                        }

                    }
                }
                //Insert Trailer Details
                if (trailerTypeId != null && trailerTypeId.length > 0) {
                    for (int j = 0; j < trailerTypeId.length; j++) {

                        String trailerTypeIds = trailerTypeId[j];
                        String trailerTypeUnit = trailerTypeUnits[j];
                        String trailerContractTypes = trailerContractType[j];
                        String fixedCostPerTrailers = fixedCostPerTrailer[j];
                        String totalTrailerCosts = totalTrailerCost[j];

                        if (!"".equals(trailerTypeIds) && !"0".equals(trailerTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerTrailerContract(secTO, userId, contractId, trailerTypeIds, trailerTypeUnit, trailerContractTypes, fixedCostPerTrailers, totalTrailerCosts);
                        }

                    }
                }
                // Insert Driver Compensation
                if (holidayType.length > 0) {
                    for (int j = 0; j < holidayType.length; j++) {
                        String typeId = holidayType[j];
                        String compensations = compensation[j];
                        if (!"".equals(typeId) && !"0".equals(typeId)) {
                            status = secondaryOperationBP.processInsertTransportCustomerDriverCompensation(secTO, userId, contractId, typeId, compensations);
                        }
                    }
                }
            }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Leasing contract updated successfully");

            }

            path = "handleViewTransportCustomer.do?customerType=2";
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView transportCustomerVehicleConfigure(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        HttpSession session = request.getSession();
        String path = "";
        String custId = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Customer >>Configure Vehicle ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vehicle";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/transportCustomerVehicleConfigure.jsp";
        setLocale(request, response);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            String vehicleType = request.getParameter("vehicleType");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String vehicleUnits = request.getParameter("vehicleUnits");

            String contractId = request.getParameter("contractId");
            request.setAttribute("vehicleType", vehicleType);
            request.setAttribute("vehicleUnits", vehicleUnits);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            request.setAttribute("contractId", contractId);
            custId = request.getParameter("custId");
            request.setAttribute("custId", custId);
            //System.out.println("custId configureVehicleTrailerPage I am here" + custId);


            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            //    path = "content/vendor/configureVehicleTrailerPage.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView transportCustomerTrailerConfigure(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        HttpSession session = request.getSession();
        String path = "";
        String custId = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Customer >>Configure Vehicle ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vehicle";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/transportCustomerTrailerConfigure.jsp";
        setLocale(request, response);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //  if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
            //     path = "content/common/NotAuthorized.jsp";
            // } else {


            String trailorType = request.getParameter("trailorType");
            String trailorTypeId = request.getParameter("trailorTypeId");
            String trailorUnits = request.getParameter("trailorUnits");
            String contractId = request.getParameter("contractId");
            request.setAttribute("trailerType", trailorType);
            request.setAttribute("trailerTypeId", trailorTypeId);
            request.setAttribute("trailerUnit", trailorUnits);
            request.setAttribute("contractId", contractId);
            custId = request.getParameter("custId");
            request.setAttribute("custId", custId);
            //System.out.println("custId configureVehicleTrailerPage I am here" + custId);

//                ArrayList vehicleList = new ArrayList();
//                vehicleList = vehicleBP.processTrailerList(vehicleTO);
//                request.setAttribute("vehicleList", vehicleList);
//                ArrayList trailerTypeList = new ArrayList();
//                trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
//                request.setAttribute("trailerTypeList", trailerTypeList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            // path = "content/vendor/configureVehicleTrailerPage.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView saveVehicleConf(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        //    int conractId = (Integer) session.getAttribute("contractId");
        String menuPath = "Vendor >> View Vehicle trailer Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);

        int status = 0;
        int status1 = 0;
        String custId = "";
        String contractId = "";
        custId = request.getParameter("custId");

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            secTO.setCustId(custId);
            //System.out.println("custId I am here  " + custId);
            secTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            secTO.setVehicleUnit(request.getParameter("vehicleUnits"));

            //System.out.println("contractId I am here  " + request.getParameter("contractId"));
            secTO.setContractId(request.getParameter("contractId"));




            // vehicles
            if (secTO.getContractId() != null && secTO.getContractId() != "") {
                secTO.setContractId(secTO.getContractId());
            }

            if (secondaryOperationCommand.getVehicleRegNo() != null && secondaryOperationCommand.getVehicleRegNo() != "") {
                secTO.setVehicleRegNo(secondaryOperationCommand.getVehicleRegNo());
            }
            if (secondaryOperationCommand.getAgreedDate() != null && secondaryOperationCommand.getAgreedDate() != "") {
                secTO.setAgreedDate(secondaryOperationCommand.getAgreedDate());
            }
            if (secondaryOperationCommand.getRemarks() != null && secondaryOperationCommand.getRemarks() != "") {
                secTO.setRemarks(secondaryOperationCommand.getRemarks());
            }
            if (secondaryOperationCommand.getMfr() != null && secondaryOperationCommand.getMfr() != "") {
                secTO.setMfr(secondaryOperationCommand.getMfr());
            }
            if (secondaryOperationCommand.getModel() != null && secondaryOperationCommand.getModel() != "") {
                secTO.setModel(secondaryOperationCommand.getModel());
            }
            if (secondaryOperationCommand.getActiveInd() != null && secondaryOperationCommand.getActiveInd() != "") {
                secTO.setActiveInd(secondaryOperationCommand.getActiveInd());
            }
            if (secondaryOperationCommand.getVehicleId() != null && secondaryOperationCommand.getVehicleId() != "") {
                secTO.setVehicleId(secondaryOperationCommand.getVehicleId());
            }
            if (secondaryOperationCommand.getMfrId() != null && secondaryOperationCommand.getMfrId() != "") {
                secTO.setMfrId(secondaryOperationCommand.getMfrId());
            }
            if (secondaryOperationCommand.getModelId() != null && secondaryOperationCommand.getModelId() != "") {
                secTO.setModelId(secondaryOperationCommand.getModelId());
            }
            if (secondaryOperationCommand.getId() != null && secondaryOperationCommand.getId() != "") {
                secTO.setId(secondaryOperationCommand.getId());
            }
            if (secondaryOperationCommand.getStartReading() != null && secondaryOperationCommand.getStartReading() != "") {
                secTO.setStartReading(secondaryOperationCommand.getStartReading());
            }
            String[] checkBox = secondaryOperationCommand.getSelectedIndex();
            String[] vehicleRegNo1 = request.getParameterValues("vehicleRegNo");
            //System.out.println("checkBox" + checkBox);
            //System.out.println("vehicleRegNo1 i am here " + checkBox);

            if (vehicleRegNo1 != null) {
                status1 = secondaryOperationBP.processInsertVehicleConf(secTO, userId);
                //System.out.println("vehicles=-=-" + status1);

            }
//                }

//                ArrayList vehicles = new ArrayList();
//                vehicles = vendorBP.getVehicleConfig(vendorTO);
//                request.setAttribute("vehicles", vehicles);
//
//                ArrayList trailerList = new ArrayList();
//                trailerList = vendorBP.getTrailerList(vendorTO);
//                request.setAttribute("trailerList", trailerList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            if (status1 > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicles  Configured Successfully");
            }

            //   path = "content/vendor/transportCustomerVehicleConfigure.jsp";
            path = "handleViewTransportCustomer.do?customerType=2";
//                    vendorList = vendorBP.processGetVendorLists(vendorTO);
//
//                    request.setAttribute("VendorList", vendorList);

            //   }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView saveTrailerConf(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        //    int conractId = (Integer) session.getAttribute("contractId");
        String menuPath = "Vendor >> View Vehicle trailer Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);

        int status = 0;
        int status1 = 0;
        String custId = "";
        String contractId = "";
        custId = request.getParameter("custId");

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            secTO.setCustId(custId);
            //System.out.println("custId I am here  " + custId);
            secTO.setTrailerTypeId(request.getParameter("trailerTypeId"));
            //System.out.println("Trailer TypeId:" + request.getParameter("trailerTypeId"));
            //  secTO.setVehicleUnit(request.getParameter("trailerUnits"));

            //System.out.println("contractId I am here  " + request.getParameter("contractId"));
            secTO.setContractId(request.getParameter("contractId"));




            // vehicles
            if (secTO.getContractId() != null && secTO.getContractId() != "") {
                secTO.setContractId(secTO.getContractId());
            }

            if (secondaryOperationCommand.getTrailerNo() != null && secondaryOperationCommand.getTrailerNo() != "") {
                secTO.setTrailerNo(secondaryOperationCommand.getTrailerNo());
            }
            if (secondaryOperationCommand.getAgreedDate() != null && secondaryOperationCommand.getAgreedDate() != "") {
                secTO.setAgreedDate(secondaryOperationCommand.getAgreedDate());
            }
            if (secondaryOperationCommand.getTrailerRemarks() != null && secondaryOperationCommand.getTrailerRemarks() != "") {
                secTO.setTrailerRemarks(secondaryOperationCommand.getTrailerRemarks());
            }

            if (secondaryOperationCommand.getActiveInd() != null && secondaryOperationCommand.getActiveInd() != "") {
                secTO.setActiveInd(secondaryOperationCommand.getActiveInd());
            }
            if (secondaryOperationCommand.getTrailerId() != null && secondaryOperationCommand.getTrailerId() != "") {
                secTO.setTrailerId(secondaryOperationCommand.getTrailerId());
            }
            if (secondaryOperationCommand.getId() != null && secondaryOperationCommand.getId() != "") {
                secTO.setId(secondaryOperationCommand.getId());
            }

            String[] checkBox = secondaryOperationCommand.getSelectedIndex();
            String[] trailerNo1 = request.getParameterValues("trailerNo");
            //System.out.println("checkBox" + checkBox);
            //System.out.println("trailerNo1 i am here " + checkBox);

            if (trailerNo1 != null) {
                status1 = secondaryOperationBP.processInsertTrailerConf(secTO, userId);
                //System.out.println("trailerNo=-=-" + status1);
//                    if (trailerNo1.length > 0) {
//                        String[] trailerTypeIdcheck = request.getParameterValues("trailerTypeIdcheck");
//                        //System.out.println("trailerTypeIdcheck" + trailerTypeIdcheck);
//
//                        if (!"".equals(trailerNo1) && trailerNo1 != null) {
//                            //System.out.println("trailerNo1=" + trailerNo1.length);
//                            //System.out.println("trailerNo1check1=" + trailerTypeIdcheck.length);
//
//
//                            if (trailerNo1.length > 0) {
////                                int insertStatus = vendorBP.updatesaveVehicles(vendorTO, userId, vehicleRegNo1, vehicleTypeIdcheck1, mfr1, model1, seatCapacity);
////                                //System.out.println("Status" + insertStatus);
//
//                            }
//                        }
////                            }
////                        }
//                    }
            }
//                }

//                ArrayList vehicles = new ArrayList();
//                vehicles = vendorBP.getVehicleConfig(vendorTO);
//                request.setAttribute("vehicles", vehicles);
//
//                ArrayList trailerList = new ArrayList();
//                trailerList = vendorBP.getTrailerList(vendorTO);
//                request.setAttribute("trailerList", trailerList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            if (status1 > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer  Configured Successfully");
            }

            //   path = "content/vendor/transportCustomerTrailerConfigure.jsp";
            path = "handleViewTransportCustomer.do?customerType=2";
//                    vendorList = vendorBP.processGetVendorLists(vendorTO);
//
//                    request.setAttribute("VendorList", vendorList);

            //   }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public void getVehicleRegNoForTransportCustomer(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String responseValue = "";
            String vehicleTypeId = "";
            response.setContentType("text/html");
            responseValue = request.getParameter("vehicleNo");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            secTO.setVehicleNo(responseValue);
            secTO.setVehicleTypeId(vehicleTypeId);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            //System.out.println("i m in con");
            secTO.setVehicleTypeId(vehicleTypeId);
            vehicleNos = secondaryOperationBP.getVehicleRegNoForTransportCustomer(secTO);
            ////System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext() && cntr < 5) {
                JSONObject jsonObject = new JSONObject();
                secTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", secTO.getVehicleId() + "-" + secTO.getVehicleNo() + "-" + secTO.getModel()
                        + "-" + secTO.getMfr() + "-" + secTO.getMfrId() + "-" + secTO.getModelId());
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            ////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public void getTrailerNoForTransportCustomer(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        ArrayList vehicleNos = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String trailerNo = "";
            String trailerTypeId = "";
            response.setContentType("text/html");
            trailerNo = request.getParameter("trailerNo");
            trailerTypeId = request.getParameter("trailerTypeId");
            secTO.setTrailerNo(trailerNo);
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            String companyId = (String) session.getAttribute("companyId");
            //System.out.println("i m in con");
            secTO.setTrailerTypeId(trailerTypeId);
            vehicleNos = secondaryOperationBP.getTrailerNoForTransportCustomer(secTO);
            ////System.out.println("vehicleNos.size() = " + vehicleNos.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleNos.iterator();
            int cntr = 0;
            while (itr.hasNext() && cntr < 5) {
                JSONObject jsonObject = new JSONObject();
                secTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", secTO.getTrailerId() + "-" + secTO.getTrailerNo());
                ////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            ////System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }

    }

    public ModelAndView editTransportCustomerVehicleConfigure(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        HttpSession session = request.getSession();
        String path = "";
        String custId = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Customer >>Configure Vehicle ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vehicle";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/editTransportCustomerVehicleConfigure.jsp";
        setLocale(request, response);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {


            String vehicleType = request.getParameter("vehicleType");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String vehicleUnits = request.getParameter("vehicleUnits");

            String contractId = request.getParameter("contractId");
            request.setAttribute("vehicleType", vehicleType);
            request.setAttribute("vehicleUnits", vehicleUnits);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            request.setAttribute("contractId", contractId);
            custId = request.getParameter("custId");
            request.setAttribute("custId", custId);
            //System.out.println("custId configureVehicleTrailerPage I am here" + custId);
            secTO.setCustId(custId);
            secTO.setContractId(contractId);
            secTO.setVehicleTypeId(vehicleTypeId);
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            ArrayList getVehicleDetails = new ArrayList();
            getVehicleDetails = secondaryOperationBP.getVehicleDetails(secTO);
            request.setAttribute("vehicles", getVehicleDetails);

            //    path = "content/vendor/configureVehicleTrailerPage.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView editTransportCustomerTrailerConfigure(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();

        HttpSession session = request.getSession();
        String path = "";
        String custId = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Customer >>Configure Trailer ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vehicle";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/editTransportCustomerTrailerConfigure.jsp";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {


            String trailerType = request.getParameter("trailorType");
            String tralierTypeId = request.getParameter("trailorTypeId");
            String trailerUnits = request.getParameter("trailorUnits");

            String contractId = request.getParameter("contractId");
            request.setAttribute("trailerType", trailerType);
            request.setAttribute("trailerUnits", trailerUnits);
            request.setAttribute("trailerTypeId", tralierTypeId);
            request.setAttribute("contractId", contractId);
            custId = request.getParameter("custId");
            request.setAttribute("custId", custId);
            //System.out.println("custId configureVehicleTrailerPage I am here" + custId);
            secTO.setCustId(custId);
            secTO.setContractId(contractId);
            secTO.setTrailerTypeId(tralierTypeId);
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            ArrayList getTrailerDetails = new ArrayList();
            getTrailerDetails = secondaryOperationBP.getTrailerDetails(secTO);
            request.setAttribute("trailers", getTrailerDetails);

            //    path = "content/vendor/configureVehicleTrailerPage.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView secondaryLeasingSheduleView(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Leasing Operation >> View Vehicle Schedule";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList customerList = new ArrayList();
//            customerList = customerBP.processTransportCustomerLists(customerTO);
            request.setAttribute("CustomerLists", customerList);
            path = "content/Customer/manageTransportCustomerSchedule.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewMothWiseVehicleSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
        String menuPath = "";
        menuPath = "Leasing Operation >> View Vehicle Trip Schedule ";
        String pageTitle = "";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //System.out.println("hr" + ":" + "mm" + ":00");
        secondaryOperationCommand = command;
        setLocale(request, response);
        try {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO1.setCustomerId(secondaryOperationCommand.getCustomerId());
                request.setAttribute("customerId", secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                request.setAttribute("customerName", secondaryOperationCommand.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerCode() != null && secondaryOperationCommand.getCustomerCode() != "") {
                request.setAttribute("customerCode", secondaryOperationCommand.getCustomerCode());
            }
            ArrayList contractVehicleList = new ArrayList();
            contractVehicleList = secondaryOperationBP.getCustomerContractVehicleList(operationTO1);
            if (contractVehicleList.size() > 0) {
                request.setAttribute("contractVehicleList", contractVehicleList);
            }
            path = "content/secondaryOperation/monthWiseVehicleSchedule.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Secondary Date Wise Trip Schedule --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveVehicleTripSchedule(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        TripTO tripTO = new TripTO();
        String menuPath = "";
        menuPath = "Secondary Operation >> Trip Schedule";
        String pageTitle = "";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //////System.out.println("hr" + ":" + "mm" + ":00");
        ModelAndView mv = null;
        try {
            //System.out.println("secondaryOperationCommand.getCustomerName() = " + secondaryOperationCommand.getCustomerName());
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                tripTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            String passedValue = request.getParameter("passedValue");
            ArrayList orderPointDetails = new ArrayList();
            int insertStatus = 0;
            insertStatus = secondaryOperationBP.saveVehicleTripSchedule(operationTO, userId, passedValue);
            //  insertStatus = tripBP.saveSecondaryTripSheet(tripTO, orderPointDetails);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Vehicle has been sucessfully scheduled");
                path = "content/secondaryOperation/viewSecondaryTripSchedule.jsp";
                mv = secondaryLeasingSheduleView(request, response, command);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView createLeasingTrip(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList routeList = new ArrayList();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        SecondaryOperationTO operationTO1 = new SecondaryOperationTO();
        SecondaryOperationTO operationTO2 = new SecondaryOperationTO();
        TripTO tripTO = new TripTO();
        String menuPath = "";
        menuPath = "Secondary Operation >> Create Trip Sheet";
        String pageTitle = "Manage Product Master";
        request.setAttribute("pageTitle", pageTitle);
        String productId = request.getParameter("productId");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        //System.out.println("hr" + ":" + "mm" + ":00");
        ModelAndView mv = null;
        secondaryOperationCommand = command;
        setLocale(request, response);
        try {
            //System.out.println("secondaryOperationCommand.getCustomerName() = " + secondaryOperationCommand.getCustomerName());
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                operationTO.setCustomerId(secondaryOperationCommand.getCustomerId());
                tripTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getScheduleDate() != null && secondaryOperationCommand.getScheduleDate() != "") {
                operationTO.setScheduleDate(secondaryOperationCommand.getScheduleDate());
                request.setAttribute("scheduleDate", secondaryOperationCommand.getScheduleDate());
            }
            if (secondaryOperationCommand.getSecondaryRouteId() != null && secondaryOperationCommand.getSecondaryRouteId() != "") {
                operationTO.setSecondaryRouteId(secondaryOperationCommand.getSecondaryRouteId());
                request.setAttribute("secondaryRouteId", operationTO.getSecondaryRouteId());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                operationTO.setCustomerName(secondaryOperationCommand.getCustomerName());
                request.setAttribute("customerName", operationTO.getCustomerName());
            }
            if (secondaryOperationCommand.getVehicleId() != null && secondaryOperationCommand.getVehicleId() != "") {
                operationTO.setVehicleId(secondaryOperationCommand.getVehicleId());
                request.setAttribute("vehicleId", secondaryOperationCommand.getVehicleId());
                //System.out.println(" secondaryOperationCommand.getVehicleId():" + secondaryOperationCommand.getVehicleId());
            }
            request.setAttribute("scheduleMonth", request.getParameter("scheduleMonth"));
            request.setAttribute("vehicleNo", request.getParameter("vehicleNo"));
            request.setAttribute("vehicleTypeName", request.getParameter("vehicleTypeName"));

//            ArrayList vehicleRegNoList = new ArrayList();
//            vehicleRegNoList = secondaryOperationBP.getSecondaryVehicleList(operationTO);
//            request.setAttribute("vehicleRegNoList", vehicleRegNoList);

//            ArrayList trailerNos = tripBP.getLeasedTrailerNos(tripTO);
//            request.setAttribute("trailerNos", trailerNos);

            ArrayList secondarCustomerList = new ArrayList();
            secondarCustomerList = secondaryOperationBP.getLeasingTripCustomer(operationTO);
            Iterator itr1 = secondarCustomerList.iterator();
            while (itr1.hasNext()) {
                operationTO1 = (SecondaryOperationTO) itr1.next();
                request.setAttribute("customerId", operationTO1.getCustomerId());
                request.setAttribute("billingTypeId", operationTO1.getBillingTypeId());
                request.setAttribute("customerName", operationTO1.getCustName());
                request.setAttribute("customerCode", operationTO1.getCustomerCode());
                request.setAttribute("customerAddress", operationTO1.getCustAddress());
                request.setAttribute("pincode", operationTO1.getPincode());
                request.setAttribute("email", operationTO1.getEmail());
                request.setAttribute("mobileNo", operationTO1.getMobileNo());
                request.setAttribute("phone", operationTO1.getPhone());
                request.setAttribute("billingTypeName", operationTO1.getBillingTypeName());
                request.setAttribute("customerType", "contract");
            }

            ArrayList orderPointDetails = new ArrayList();
            orderPointDetails = secondaryOperationBP.getRoutePointDetails(operationTO);
            request.setAttribute("orderPointDetails", orderPointDetails);
            request.setAttribute("totalPoints", orderPointDetails.size());
            Iterator itr2 = orderPointDetails.iterator();
            operationTO1 = new SecondaryOperationTO();
            String originPointName = "";
            String destinationPointName = "";
            int a = 1;
            while (itr2.hasNext()) {
                operationTO1 = (SecondaryOperationTO) itr2.next();
                if (a == 1) {
                    request.setAttribute("originPointId", operationTO1.getPointId());
                    request.setAttribute("originPointName", operationTO1.getPointName());
                    originPointName = operationTO1.getPointName();
                } else if (a == orderPointDetails.size()) {
                    request.setAttribute("destinationPointId", operationTO1.getPointId());
                    request.setAttribute("destinationPointName", operationTO1.getPointName());
                    destinationPointName = operationTO1.getPointName();
                }
                a++;
            }
            ArrayList routeMaster = new ArrayList();
            routeMaster = secondaryOperationBP.getContractRouteDetails(operationTO);
            Iterator itr3 = routeMaster.iterator();
            while (itr3.hasNext()) {
                operationTO2 = (SecondaryOperationTO) itr3.next();
                request.setAttribute("secondaryRouteCode", operationTO2.getSecondaryRouteCode());
                request.setAttribute("secondaryRouteName", operationTO2.getSecondaryRouteName());
                request.setAttribute("totalTravelKm", operationTO2.getTotalTravelKm());
                request.setAttribute("totalTravelHour", operationTO2.getTotalTravelHour());
                request.setAttribute("totalTravelMinute", operationTO2.getTotalTravelMinute());
            }
            request.setAttribute("routeMaster", routeMaster);
            //ArrayList vehicleTypeList = new ArrayList();
            //vehicleTypeList = operationBP.getVehicleTypeList();
            //request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList productCategoryList = new ArrayList();
            productCategoryList = operationBP.getProductCategoryList();
            request.setAttribute("productCategoryList", productCategoryList);

            path = "content/secondaryOperation/createLeasingTrip.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Product Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveLeasingTripSheet(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        //System.out.println("userId = " + 0);
        int userId = (Integer) session.getAttribute("userId");
        //System.out.println("userId = " + userId);
        OperationCommand operationCommand = null;
        operationCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        SecondaryOperationTO operationTO4 = new SecondaryOperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Secondary Operation >>  Create Trip Sheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList routeList = new ArrayList();
        RedirectView redirectView = null;
        try {
            int insertStatus = 0;
            operationTO.setEntryType("1");
            operationTO.setConsignmentDate(request.getParameter("consignmentDate"));
            operationTO.setOrderReferenceNo("Secondary Trip");
            operationTO.setOrderReferenceRemarks("Secondary Trip");
            operationTO.setProductCategoryId("0");
            //generate tripcode
            String cNoteCode = "CO/15-16/";
            String cNoteCodeSequence = operationBP.getCnoteCodeSequence();
            cNoteCode = cNoteCode + cNoteCodeSequence;
            operationTO.setConsignmentNoteNo(cNoteCode);
            String customerId = request.getParameter("customerId");

            operationTO.setCustomerId(customerId);
            operationTO4.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getLeasingCustomer(operationTO4);
            String tempn[] = null;
            tempn = customerDetails.split("~");
            operationTO.setCustomerCode(tempn[1]);
            operationTO.setCustomerName(tempn[2]);
            operationTO.setCustomerTypeId("1");
            operationTO.setCustomerAddress(tempn[13]);
            operationTO.setPincode(tempn[14]);
            operationTO.setCustomerPhoneNo(tempn[6]);
            operationTO.setCustomerMobileNo(tempn[7]);
            operationTO.setMailId(tempn[8]);
            operationTO.setBillingTypeId(request.getParameter("billingTypeId"));
            operationTO.setContractId(50);
            if (operationCommand.getDestination() != null && !"".equals(operationCommand.getDestination())) {
                operationTO.setDestination(operationCommand.getDestination());
            }
            if (operationCommand.getOrigin() != null && !"".equals(operationCommand.getOrigin())) {
                operationTO.setOrigin(operationCommand.getOrigin());
            }
            operationTO.setBusinessType("2");
            operationTO.setMultiPickup("N");
            operationTO.setMultiDelivery("N");
            operationTO.setConsignmentOrderInstruction("Leasing Trip");
            operationTO.setTotalPackage("0");
            operationTO.setTotalWeightage("0");
            operationTO.setServiceType("1");
            operationTO.setVehicleId(Integer.parseInt(request.getParameter("vehicleId")));
            operationTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            operationTO.setVehicleTypeName(request.getParameter("productCategoryName"));
            operationTO.setRouteContractId(Integer.parseInt(request.getParameter("routeContractId")));
            operationTO.setReeferRequired(request.getParameter("reeferRequired"));
            operationTO4.setSecondaryRouteId(request.getParameter("routeContractId"));

            String routeId = "0";

            ArrayList orderPointDetails = new ArrayList();
            orderPointDetails = secondaryOperationBP.getLeasingRoutePointDetails(operationTO4);

            //System.out.println("routeId = " + routeId);
            if (routeId != null) {
                operationTO.setRouteId(routeId);
            } else {
                operationTO.setRouteId("0");
            }
            operationTO.setContractRateId("0");

            if (operationCommand.getTotalKm() != null && !"".equals(operationCommand.getTotalKm())) {
                operationTO.setTotalKm(operationCommand.getTotalKm());
            }
            if (operationCommand.getTotalHours() != null && !"".equals(operationCommand.getTotalHours())) {
                operationTO.setTotalHours(operationCommand.getTotalHours());
            }
            if (operationCommand.getTotalMinutes() != null && !"".equals(operationCommand.getTotalMinutes())) {
                operationTO.setTotalMinutes(operationCommand.getTotalMinutes());
            }
            operationTO.setRateWithReefer("0");
            operationTO.setRateWithoutReefer("0");
            operationTO.setVehicleRequiredDate(request.getParameter("consignmentDate"));
            operationTO.setVehicleRequiredHour("00");
            operationTO.setVehicleRequiredMinute("00");
            operationTO.setVehicleInstruction("Leasing Trip");
            operationTO.setConsignorName(tempn[2]);
            operationTO.setConsignorPhoneNo(tempn[6]);
            operationTO.setConsignorAddress(tempn[13]);
            operationTO.setConsigneeName(tempn[2]);
            operationTO.setConsigneePhoneNo(tempn[6]);
            operationTO.setConsigneeAddress(tempn[13]);
            operationTO.setTotFreightAmount("0");
            operationTO.setDocCharges("0");
            operationTO.setOdaCharges("0");
            operationTO.setOdaCharges("0");
            operationTO.setMultiPickupCharge("0");
            operationTO.setMultiDeliveryCharge("0");
            operationTO.setHandleCharges("0");
            operationTO.setOtherCharges("0");
            operationTO.setUnloadingCharges("0");
            operationTO.setLoadingCharges("0");
            operationTO.setSubTotal("0");
            operationTO.setTotalCharges("0");
            insertStatus = operationBP.insertConsignmentNoteForEmptyTrip(operationTO, userId);
            //System.out.println("insert Status..." + insertStatus);
            if (insertStatus > 0) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                //System.out.println(dateFormat.format(date));
                request.setAttribute("curDate", dateFormat.format(date));
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/BrattleFoods/consignmentNote.jsp";

                //send email
                /////////////////Email Part//////////////////////////////////////////////
                String to = "";
                String activitycode = "EMREQ1";
                //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";

                ArrayList emaildetails = new ArrayList();
                emaildetails = operationBP.getEmailDetails(activitycode);
                Iterator itr = emaildetails.iterator();
                OperationTO operationTO1 = null;
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    smtp = operationTO1.getSmtp();
                    emailPort = Integer.parseInt(operationTO1.getPort());
                    frommailid = operationTO1.getEmailId();
                    password = operationTO1.getPassword();
                    to = operationTO1.getTomailId();
                }

                String toEmailId = operationBP.getEmailToList(operationTO.getCustomerId());
                ;

                String[] temp = toEmailId.split(",");
                to = "";
                int cntr = 0;
                for (int i = 0; i < temp.length; i++) {
                    if (!"-".equals(temp[i]) && !"".equals(temp[i])) {
                        if (cntr == 0) {
                            to = temp[i];
                        } else {
                            to = to + "," + temp[i];

                        }
                        cntr++;
                    }
                }

                ArrayList emailConsignmentDetails = operationBP.getConsignmentDetails(operationTO.getConsignmentOrderId());

                itr = emailConsignmentDetails.iterator();
                operationTO1 = null;

                String vehicleType = "";
                String totalHours = "";
                String totalDistance = "";
                String totalPackage = "";
                String totalWeightage = "";
                String reeferMaximumTemperature = "";
                String reeferMinimumTemperature = "";
                String productCategoryName = "";
                String route = "";
                if (itr.hasNext()) {
                    operationTO1 = new OperationTO();
                    operationTO1 = (OperationTO) itr.next();
                    vehicleType = operationTO1.getVehicleTypeName();
                    totalHours = operationTO1.getTotalHours();
                    totalDistance = operationTO1.getTotalDistance();
                    totalPackage = operationTO1.getTotalPackage();
                    totalWeightage = operationTO1.getTotalWeightage();
                    route = operationTO1.getConsigmentOrigin() + "-" + operationTO1.getConsigmentDestination();
                    productCategoryName = operationTO1.getProductCategoryName() + "(Temp " + operationTO1.getReeferMinimumTemperature() + " - " + operationTO1.getReeferMaximumTemperature() + ")";
                }

                String emailFormat = "Team, <br><br> New Order Details is given below <br> Consignment Note Created for Customer Order Ref No: " + operationTO.getConsignmentNoteNo();
                emailFormat = emailFormat + "<br> Customer: " + operationTO.getCustomerName();
                emailFormat = emailFormat + "<br> Vehicle Type: " + vehicleType;
                emailFormat = emailFormat + "<br> Vehicle Required Date: " + operationTO.getVehicleRequiredDate() + " Hour:" + operationTO.getVehicleRequiredHour();
                emailFormat = emailFormat + "<br> Route: " + route;
                emailFormat = emailFormat + "<br> Product Category: " + productCategoryName;
                emailFormat = emailFormat + "<br> Reefer: " + operationTO.getReeferRequired();
                emailFormat = emailFormat + "<br> Total Packages (Nos): " + operationTO.getTotalPackage();
                emailFormat = emailFormat + "<br> Total Weight (Kg): " + operationTO.getTotalWeightage();
                emailFormat = emailFormat + "<br> Total Distance (Km): " + totalDistance;
                emailFormat = emailFormat + "<br> Transit Hours (Hr): " + totalHours;
                emailFormat = emailFormat + "<br><br>  Thanks,<br> Team BrattleFoods. ";

                String subject = "Consignment Note Created For : " + operationTO.getCustomerName() + ". Route " + route + " Vehicle Required On :" + operationTO.getVehicleRequiredDate() + " :" + operationTO.getVehicleRequiredHour();
                ;
                String content = emailFormat;

                //mail.sendmail(smtp, emailPort, frommailid, password, subject, content, to);
                //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, "").start();
                //end of email part
                /*
                ArrayList contractRouteList = new ArrayList();
                contractRouteList = operationBP.getContractRouteList(operationTO);
                request.setAttribute("contractRouteList", contractRouteList);

                ArrayList vehicleTypeList = new ArrayList();
                vehicleTypeList = operationBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                ArrayList billingTypeList = new ArrayList();
                billingTypeList = operationBP.getBillingTypeList();
                request.setAttribute("billingTypeList", billingTypeList);
                ArrayList productCategoryList = new ArrayList();
                productCategoryList = operationBP.getProductCategoryList();
                request.setAttribute("productCategoryList", productCategoryList);
                 */
            }

            if (insertStatus > 0) {
                String trailerIds = request.getParameter("temp");
                TripTO tripTO = new TripTO();
                tripTO.setTrailerId(trailerIds);
                String consignment = String.valueOf(insertStatus);
                tripTO.setConsignmentId(consignment);
                String[] consignmentOrderId = {consignment};
                //String tripDate = request.getParameter("tripDate");
                String vehicleId = request.getParameter("vehicleId");
                String vehicleNoEmail = request.getParameter("vehicleNo");
                String driver1Id = request.getParameter("primaryDriverId");
                String driver2Id = request.getParameter("driver2Id");
                String driver3Id = request.getParameter("driver3Id");
                String orderExpense = "0";
                String orderRevenue = "0";
                String profitMargin = "0";
                //String tripScheduleDate = request.getParameter("startDate");
                String tripScheduleDate = "00-00-0000";
                String tripScheduleTime = request.getParameter("startHour") + ":" + request.getParameter("startMinute");
                String tripScheduleTimeHrs = request.getParameter("startHour");
                String tripScheduleTimeMins = request.getParameter("startMinute");
                String totalHours = request.getParameter("totalHours");

                String cNotes = cNoteCode;
                String billingType = tempn[4];
                String customerName = tempn[2];
                String customerType = "Contract";
                String routeInfo = request.getParameter("routeInfo");
                tripTO.setOrigin(request.getParameter("cityFrom"));
                tripTO.setDestination(request.getParameter("cityTo"));
                String productInfo = request.getParameter("productCategoryName");
                String reeferRequired = request.getParameter("reeferRequired");
                String totalWeight = "0";

                String vehicleType = request.getParameter("vehicleTypeName");
                String vehicleNo = request.getParameter("vehicleNo");
                String vehicleTonnage = request.getParameter("vehicleTonnage");
                String vehicleCapUtil = request.getParameter("vehicleCapUtil");
                String tripRemarks = "Empty Trip";

                String actionName = "1";
                String actionRemarks = "Empty Trip";
                String preStartLocationId = request.getParameter("preStartLocationId");
                //String preStartLocationPlanDate = request.getParameter("preStartLocationPlanDate");
                String preStartLocationPlanDate = "00-00-0000";
                String preStartLocationPlanTimeHrs = request.getParameter("preStartLocationPlanTimeHrs");
                String preStartLocationPlanTimeMins = request.getParameter("preStartLocationPlanTimeMins");
                String preStartLocationDistance = request.getParameter("preStartLocationDistance");
                String preStartLocationDurationHrs = request.getParameter("preStartLocationDurationHrs");
                String preStartLocationDurationMins = request.getParameter("preStartLocationDurationMins");
                String preStartLocationVehicleMileage = request.getParameter("vehicleMileage");
                String preStartLocationTollRate = request.getParameter("tollRate");
                String preStartLocationRouteExpense = request.getParameter("preStartRouteExpense");
                String preStartLocationStatus = "1";
                String originId = request.getParameter("origin");
                String destinationId = request.getParameter("destination");

                if (preStartLocationStatus == null) {
                    preStartLocationStatus = "0";
                }
                String roleId = "" + (Integer) session.getAttribute("RoleId");
                String companyId = (String) session.getAttribute("companyId");
                tripTO.setRoleId(roleId);
                tripTO.setCompanyId(companyId);
                tripTO.setProductInfo(productInfo);
                tripTO.setCustomerId(customerId);
                tripTO.setOriginId(originId);
                tripTO.setDestinationId(destinationId);
                tripTO.setPreStartLocationStatus(preStartLocationStatus);
                tripTO.setActionName(actionName);
                tripTO.setActionRemarks(actionRemarks);
                tripTO.setPreStartLocationId(preStartLocationId);
                if ("1".equals(tripTO.getPreStartLocationStatus())) {
                    tripTO.setPreStartLocationPlanDate(tripScheduleDate);
                    tripTO.setPreStartLocationPlanTimeHrs(tripScheduleTimeHrs);
                    tripTO.setPreStartLocationPlanTimeMins(tripScheduleTimeMins);
                } else {
                    if (preStartLocationRouteExpense == null || "".equals(preStartLocationRouteExpense)) {
                        preStartLocationRouteExpense = "0";
                    }
                    //System.out.println("orderExpense:" + orderExpense + " preStartLocationRouteExpense:" + preStartLocationRouteExpense);
                    orderExpense = "" + (Float.parseFloat(orderExpense) + Float.parseFloat(preStartLocationRouteExpense));
                    tripTO.setPreStartLocationPlanDate(preStartLocationPlanDate);
                    tripTO.setPreStartLocationPlanTimeHrs(preStartLocationPlanTimeHrs);
                    tripTO.setPreStartLocationPlanTimeMins(preStartLocationPlanTimeMins);
                }

                tripTO.setPreStartLocationDistance(preStartLocationDistance);
                tripTO.setPreStartLocationDurationHrs(preStartLocationDurationHrs);
                tripTO.setPreStartLocationDurationMins(preStartLocationDurationMins);
                tripTO.setPreStartLocationVehicleMileage(preStartLocationVehicleMileage);
                tripTO.setPreStartLocationTollRate(preStartLocationTollRate);
                tripTO.setPreStartLocationRouteExpense(preStartLocationRouteExpense);

                //points related
                tripTO.setUserId(userId);
                tripTO.setVehicleNo(vehicleNoEmail);
                tripTO.setVehicleTypeName(vehicleType);
                tripTO.setVehicleCapUtil(vehicleCapUtil);
                tripTO.setTripRemarks(tripRemarks);
                tripTO.setcNotes(cNotes);
                tripTO.setBillingType(billingType);
                tripTO.setCustomerName(customerName);
                tripTO.setCustomerType(customerType);
                tripTO.setRouteInfo(routeInfo);
                tripTO.setReeferRequired(reeferRequired);
                tripTO.setTotalWeight(totalWeight);

                tripTO.setConsignmentOrderId(consignmentOrderId);
                //tripTO.setTripDate(tripDate);
                tripTO.setVehicleId(vehicleId);
                tripTO.setTotalHrs(totalHours);
                tripTO.setTripTransitHours(totalHours);

                operationTO.setVehicleId(Integer.parseInt(vehicleId));

                String driver1Name = "";
                String driver2Name = "";
                String driver3Name = "";
                String driverName = "";
                //System.out.println("srini vehicleid is:" + operationTO.getVehicleId());
                ArrayList vehicleDriverInfo = operationBP.getVehicleDriverMappingForVehicleId(operationTO);
                Iterator itr1 = vehicleDriverInfo.iterator();
                OperationTO operationTO3 = new OperationTO();
                if (itr1.hasNext()) {
                    //System.out.println("12");
                    operationTO3 = new OperationTO();
                    operationTO3 = (OperationTO) itr1.next();

                    //System.out.println("tripTO.setPrimaryDriverId():" + tripTO.getPrimaryDriverId());
                    //System.out.println("operationTO3.setPrimaryDriverId():" + operationTO3.getPrimaryDriverId());

                    driver1Name = operationTO3.getPrimaryDriverName();
                    driver2Name = operationTO3.getSecondaryDriverNameOne();
                    driver3Name = operationTO3.getSecondaryDriverNameTwo();
                }
                tripTO.setPrimaryDriverId(driver1Id);
                tripTO.setSecondaryDriver1Id("0");
                tripTO.setSecondaryDriver2Id("0");
                //System.out.println("tripTO.getPrimaryDriverId:" + tripTO.getPrimaryDriverId());
                //System.out.println("insertStatus:" + insertStatus);
//                orderRevenue = tripBP.getConsignmentOrderRevenue(insertStatus + "");
//                orderExpense = tripBP.getConsignmentOrderExpense(insertStatus + "");
                //System.out.println("orderRevenue:" + orderRevenue);
                //System.out.println("orderExpense:" + orderExpense);
                tripTO.setOrderExpense(orderExpense);
                tripTO.setOrderRevenue(orderRevenue);
                tripTO.setProfitMargin(profitMargin);
                tripTO.setTripScheduleDate(tripScheduleDate);
                tripTO.setTripScheduleTime("00:00:00");
                //tripTO.setTripScheduleTimeHrs(tripScheduleTime);

                if (!"".equals(driver1Name)) {
                    driverName = driver1Name;
                } else if (!"".equals(driver2Name)) {
                    driverName = driverName + "," + driver2Name;
                } else if (!"".equals(driver3Name)) {
                    driverName = driverName + "," + driver3Name;
                }
                tripTO.setRouteContractId(operationTO.getRouteContractId());

                Iterator itr10 = orderPointDetails.iterator();
                SecondaryOperationTO sTO = null;
                while (itr10.hasNext()) {
                    sTO = new SecondaryOperationTO();
                    sTO = (SecondaryOperationTO) itr10.next();
                    //System.out.println("pointId :" + sTO.getPointId());
                    //System.out.println("getPointType :" + sTO.getPointType());
                    //System.out.println("getPointSequence :" + sTO.getPointSequence());
                    //System.out.println("getPointAddresss :" + sTO.getPointAddresss());
                }
//                tripTO.setTripTypeNew("3");
                int status = tripBP.saveSecondaryTripSheet(tripTO, orderPointDetails);
                //System.out.println("tripId = " + status);
                int tripid = status;
                int updateTripScheduleStatus = 0;
                String secondaryRouteId = operationTO4.getSecondaryRouteId();
                String sechduleMonth = request.getParameter("scheduleMonth");
                //System.out.println("sechduleMonth:" + sechduleMonth);
                updateTripScheduleStatus = secondaryOperationBP.updateLeasingTripScheduleStatus(vehicleId, customerId, sechduleMonth, userId);
                String tripCode = tripBP.getTripCode(tripid);
                if (status > 0) {

                    //String to = "nipun.kohli@brattlefoods.com,srini@entitlesolutions.com";
                    String to = "";
                    String cc = "";
                    String smtp = "";
                    int emailPort = 0;
                    String frommailid = "";
                    String password = "";
                    String activitycode = "EMTRP0";

                    ArrayList emaildetails = new ArrayList();
                    emaildetails = tripBP.getEmailDetails(activitycode);
                    itr1 = emaildetails.iterator();

                    if (itr1.hasNext()) {
                        tripTO = new TripTO();
                        tripTO = (TripTO) itr1.next();
                        smtp = tripTO.getSmtp();
                        emailPort = Integer.parseInt(tripTO.getPort());
                        frommailid = tripTO.getEmailId();
                        password = tripTO.getPassword();
                        to = tripTO.getTomailId();
                        cc = tripTO.getTomailId();
                    }

                    String emailFormat = "<html>"
                            + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                            + "<tr>"
                            + "<th colspan='2'>Empty Trip</th>"
                            + "</tr>"
                            + "<tr><td>&nbsp;Trip Code</td><td>&nbsp;&nbsp;" + tripCode + "</td></tr>"
                            + "<tr><td>&nbsp;RCM Expense</td><td>&nbsp;&nbsp;" + orderExpense + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Date</td><td>&nbsp;&nbsp;" + tripScheduleDate + "</td></tr>"
                            + "<tr><td>&nbsp;Schedule Time</td><td>&nbsp;&nbsp;" + tripScheduleTime + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vehicleType + "</td></tr>"
                            + "<tr><td>&nbsp;Vehicle No</td><td>&nbsp;&nbsp;" + vehicleNoEmail + "</td></tr>"
                            + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driverName + "</td></tr>"
                            + "<tr><td>&nbsp;Route Name</td><td>&nbsp;&nbsp;" + routeInfo + "</td></tr>"
                            + "<tr><td>&nbsp;Customer Name</td><td>&nbsp;&nbsp;" + customerName + "</td></tr>"
                            + "<tr><td>&nbsp;&nbsp;Remarks</td><td>&nbsp;&nbsp;" + "Empty Trip" + "</td></tr>"
                            + "<tr><td colspan='2' align='center'>"
                            + "<a style='text-decoration: none' href='http://192.168.2.30:8084/throttle/saveEmptyTripApproval.do?userId=1161&tripid=" + tripid + "&routename=" + routeInfo + "&vehicleno=" + vehicleNoEmail + "&approvestatus=2'>Approve</a>&nbsp;|&nbsp;"
                            + "<a style='text-decoration: none' href='http://192.168.2.30:8084/throttle/saveEmptyTripApproval.do?userId=1161&tripid=" + tripid + "&routename=" + routeInfo + "&vehicleno=" + vehicleNoEmail + "&approvestatus=3'>Reject</a>"
                            + "</td></tr>"
                            + "</table></body></html>";

                    //String subject = "Trip Closure Approval for Consignment Note - " + cnotename;
                    String subject = "Empty Trip Approval Request for Trip Start" + customerName + " Vehicle " + vehicleNoEmail + " Route " + routeInfo;
                    String content = emailFormat;

                    //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
                    //fetch mail content
                    /*
                    tripTO.setUserId(userId);
                    request.setAttribute("tripSheetId", status);

                    String tripId = String.valueOf(status);
                    tripTO.setTripId(tripId);
                    tripTO.setTripSheetId(tripId);
                    ArrayList expiryDateDetails = new ArrayList();
                    expiryDateDetails = tripBP.getExpiryDateDetails(tripTO);
                    request.setAttribute("expiryDateDetails", expiryDateDetails);

                    ArrayList tripDetails = new ArrayList();
                    ArrayList tripPointDetails = new ArrayList();
                    tripDetails = tripBP.getTripDetails(tripTO);
                    tripPointDetails = tripBP.getTripPointDetails(tripTO);

                    request.setAttribute("tripDetails", tripDetails);
                    request.setAttribute("tripPointDetails", tripPointDetails);
                    ArrayList statusDetails = new ArrayList();
                    statusDetails = tripBP.getTripStausDetails(tripTO);
                    if (statusDetails.size() > 0) {
                    request.setAttribute("statusDetails", statusDetails);
                    }
                    String tripSheetId = String.valueOf(status);
                    ArrayList tripAdvanceDetails = new ArrayList();
                    tripAdvanceDetails = tripBP.getTripAdvanceDetails(tripSheetId);
                    request.setAttribute("tripAdvanceDetails", tripAdvanceDetails);

                    ArrayList tripPreStartDetails = new ArrayList();
                    tripPreStartDetails = tripBP.getPreStartTripDetails(tripTO);
                    request.setAttribute("tripPreStartDetails", tripPreStartDetails);
                     */
                    path = "BrattleFoods/tripSheetRedirectPage.jsp";

                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //pavi expense master
    public ModelAndView handleExpenseMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        ////////System.out.println("==handleSlabMaster===...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        String menuPath = "";
        menuPath = "Operation  >> Slab Master ";
        String pageTitle = "Slab Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);


        try {

            ArrayList expenselist = new ArrayList();
            expenselist = secondaryOperationBP.getExpenselist();
            request.setAttribute("expenselist", expenselist);

            // request.setAttribute("countryList", countryList.size());
            path = "content/BrattleFoods/expenseMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveExpenseMaster(HttpServletRequest request, HttpServletResponse reponse, SecondaryOperationCommand command) throws IOException {
        ////////System.out.println("slab master save");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int insertStatus = 0;
        int updateStatus = 0;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        String menuPath = "";
        menuPath = "Operation  >> saveSlabMaster ";
        String pageTitle = "Save Slab Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            ArrayList configDetailsList = new ArrayList();

            if (secondaryOperationCommand.getExpenseName() != null && !"".equals(secondaryOperationCommand.getExpenseName())) {
                secTO.setExpenseName(secondaryOperationCommand.getExpenseName());
            }
            if (secondaryOperationCommand.getExpenseId() != null && !"".equals(secondaryOperationCommand.getExpenseId())) {
                secTO.setExpenseId(secondaryOperationCommand.getExpenseId());
            }
            if (secondaryOperationCommand.getLedgerId() != null && !"".equals(secondaryOperationCommand.getLedgerId())) {
                secTO.setLedgerId(secondaryOperationCommand.getLedgerId());
            }
            if (secondaryOperationCommand.getLevelID() != null && !"".equals(secondaryOperationCommand.getLevelID())) {
                secTO.setLevelID(secondaryOperationCommand.getLevelID());
            }
            if (secondaryOperationCommand.getStatus() != null && !"".equals(secondaryOperationCommand.getStatus())) {
                secTO.setStatus(secondaryOperationCommand.getStatus());
            }

            if ("".equals(secTO.getExpenseId())) {
                insertStatus = secondaryOperationBP.insertExpenseMaster(secTO, userId);
            } else {
                insertStatus = secondaryOperationBP.updateExpenseMaster(secTO, userId);
            }
            ////////System.out.println("saved" + insertStatus);
            if (insertStatus > 0) {
                request.setAttribute("successMessage", "Expense Saved SuccessFully");
            }

            ArrayList expenselist = new ArrayList();
            expenselist = secondaryOperationBP.getExpenselist();
            request.setAttribute("expenselist", expenselist);

            path = "content/BrattleFoods/expenseMaster.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView transportCustomerDailyContract(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/createTransportCustomerDailyContract.jsp";
            setLocale(request, response);

            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            String customerId = "";
            customerId = request.getParameter("custId");
            //System.out.println("customerId:" + customerId);
            operationTO.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getTransportCustomer(operationTO);
            String[] temp = null;
            if (customerDetails != null) {
                temp = customerDetails.split("~");
                request.setAttribute("customerId", customerId);
                request.setAttribute("custId", temp[0]);
                request.setAttribute("customerCode", temp[1]);
                request.setAttribute("customerName", temp[2]);
                request.setAttribute("custPhone", temp[3]);
                request.setAttribute("custMobile", temp[4]);
                request.setAttribute("custEmail", temp[5]);
                request.setAttribute("custAddress", temp[6]);
                request.setAttribute("pincode", temp[7]);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTransportCustomerDailyContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/Customer/manageTransportCustomer.jsp";
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getStartDate() != null && secondaryOperationCommand.getStartDate() != "") {
                secTO.setStartDate(secondaryOperationCommand.getStartDate());
            }
            if (secondaryOperationCommand.getEndDate() != null && secondaryOperationCommand.getEndDate() != "") {
                secTO.setEndDate(secondaryOperationCommand.getEndDate());
            }


            //System.out.println("i m here/...");

            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            String[] trailerTypeId = request.getParameterValues("trailerTypeId");
            String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");

            String[] iCnt = request.getParameterValues("iCnt");
            String[] iCnt1 = request.getParameterValues("iCnt1");

            //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
            int contractId = secondaryOperationBP.processInsertTransportCustomerDailyContract(secTO, userId);
            if (contractId > 0) {

                //insert Vehicle Details
                if (vehicleTypeId.length > 0) {
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String trailerTypeIds = trailerTypeId[j];
                        //System.out.println("vehicleTypeIds:" + vehicleTypeIds);
                        //System.out.println("fixedCostPerVehicles:" + fixedCostPerVehicle);

                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerDailyVehicleContract(secTO, userId, contractId, vehicleTypeIds, trailerTypeIds, fixedCostPerVehicles);
                        }

                    }
                }



            }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Daily contract saved successfully");

            }
            path = "handleViewTransportCustomer.do?customerType=2";
            // }


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleCreateQuotation(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        OperationTO opTO = new OperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/quotation.jsp";
            setLocale(request, response);

            ArrayList salesPerson = new ArrayList();
            salesPerson = secondaryOperationBP.getAccountManager(operationTO);
            request.setAttribute("salesPerson", salesPerson);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            String customerId = "";
            customerId = request.getParameter("custId");
            //System.out.println("customerId:" + customerId);
            operationTO.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getTransportCustomer(operationTO);
            String[] temp = null;
            if (customerDetails != null) {
                temp = customerDetails.split("~");
                request.setAttribute("customerId", customerId);
                request.setAttribute("custId", temp[0]);
                request.setAttribute("customerCode", temp[1]);
                request.setAttribute("customerName", temp[2]);
                request.setAttribute("custPhone", temp[3]);
                request.setAttribute("custMobile", temp[4]);
                request.setAttribute("custEmail", temp[5]);
                request.setAttribute("custAddress", temp[6]);
                request.setAttribute("pincode", temp[7]);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewQuotation(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/viewQuotationList.jsp";
            setLocale(request, response);

            ArrayList quotationList = new ArrayList();
            quotationList = secondaryOperationBP.getQuotationViewList(operationTO);
            request.setAttribute("quotationList", quotationList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editTransportCustomerDailyContract(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/editTransportCustomerDailyContract.jsp";

            setLocale(request, response);
            ArrayList vehicleTypeList = new ArrayList();
            vehicleTypeList = operationBP.getVehicleTypeList();
            request.setAttribute("vehicleTypeList", vehicleTypeList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            String customerId = "";
            customerId = request.getParameter("custId");
            //System.out.println("customerId:" + customerId);
            operationTO.setCustomerId(customerId);
            String customerDetails = "";
            customerDetails = secondaryOperationBP.getTransportCustomer(operationTO);
            String[] temp = null;
            if (customerDetails != null) {
                temp = customerDetails.split("~");
                request.setAttribute("customerId", customerId);
                request.setAttribute("custId", temp[0]);
                request.setAttribute("customerCode", temp[1]);
                request.setAttribute("customerName", temp[2]);
                request.setAttribute("custPhone", temp[3]);
                request.setAttribute("custMobile", temp[4]);
                request.setAttribute("custEmail", temp[5]);
                request.setAttribute("custAddress", temp[6]);
                request.setAttribute("pincode", temp[7]);

            }
            ArrayList contractDetails = new ArrayList();
            contractDetails = secondaryOperationBP.getTransportCustomerDailyContractDetails(operationTO);
            request.setAttribute("contractDetails", contractDetails);
            if (contractDetails.size() > 0) {
                Iterator itr1 = contractDetails.iterator();
                while (itr1.hasNext()) {
                    SecondaryOperationTO secTo = (SecondaryOperationTO) itr1.next();
                    request.setAttribute("contractFrom", secTo.getFromDate());
                    request.setAttribute("contractTo", secTo.getToDate());
                    request.setAttribute("contractId", secTo.getContractId());
                }

            }
            ArrayList contractFlateRateDetails = new ArrayList();
            contractFlateRateDetails = secondaryOperationBP.getTransportCustomerDailyFlatRate(operationTO);
            request.setAttribute("contractFlateRateDetails", contractFlateRateDetails);
            ArrayList contractTrailerDetails = new ArrayList();
            contractTrailerDetails = secondaryOperationBP.getTransportCustomerTrailerDetails(operationTO);
            request.setAttribute("contractTrailerDetails", contractTrailerDetails);
            ArrayList driverCompensationDetails = new ArrayList();
            driverCompensationDetails = secondaryOperationBP.getTransportCustomerDriverCompensationDetails(operationTO);
            request.setAttribute("driverCompensationDetails", driverCompensationDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEditTransportCustomerDailyContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        ModelAndView mv = null;
        path = "content/Customer/manageTransportCustomer.jsp";
        setLocale(request, response);
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            if (secondaryOperationCommand.getStartDate() != null && secondaryOperationCommand.getStartDate() != "") {
                secTO.setStartDate(secondaryOperationCommand.getStartDate());
            }
            if (secondaryOperationCommand.getEndDate() != null && secondaryOperationCommand.getEndDate() != "") {
                secTO.setEndDate(secondaryOperationCommand.getEndDate());
            }
            int contractId = Integer.parseInt(request.getParameter("contractId"));
            secTO.setContractId(String.valueOf(contractId));
            // update Contract Details
            int contractUpdate = secondaryOperationBP.processUpdateTransportCustomerDailyContract(secTO, userId);
            //System.out.println("contractUpdate" + contractUpdate);
// update Exsiting Record

            // vehicle contract Details
            String[] vehicleContractId = request.getParameterValues("vehicleContractId");
            String[] fixedCostPerVehicleE = request.getParameterValues("fixedCostPerVehicleE");

            //System.out.println("vehicleContractId length::" + vehicleContractId.length);
            if (vehicleContractId != null) {
                status = secondaryOperationBP.updateTransportCustomerDailyVehicleContract(vehicleContractId, fixedCostPerVehicleE, userId);

            }
//        // Trailer Contract Deatils
//                String[] trailerContractId = request.getParameterValues("trailerContractId");
//                String[] fixedCostPerTrailerE = request.getParameterValues("costPerTrailerPerMonthE");
//                String[] totalTrailerFixedCostE = request.getParameterValues("totalTrailerCostE");
//                 if (trailerContractId != null) {
//                   status =secondaryOperationBP.updateTransportCustomerTrailerContract(trailerContractId,fixedCostPerTrailerE,totalTrailerFixedCostE,userId);
//
//                }
            // End

            //Insert New Contract
            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            String[] trailerTypeId = request.getParameterValues("trailerTypeId");
            String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");

            // int contractId = secondaryOperationBP.processInsertTransportCustomerContract(secTO, userId);
            if (contractId > 0) {

                //insert Vehicle Details

                if (vehicleTypeId != null && vehicleTypeId.length > 0) {
                    //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String trailerTypeIds = trailerTypeId[j];
                        //System.out.println("vehicleTypeIds:" + vehicleTypeIds);
                        //System.out.println("fixedCostPerVehicles:" + fixedCostPerVehicle);


                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerDailyVehicleContract(secTO, userId, contractId, vehicleTypeIds, trailerTypeIds, fixedCostPerVehicles);
                        }

                    }
                }
                //Insert Trailer Details

                // Insert Driver Compensation

            }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Daily contract updated successfully");

            }

            path = "handleViewTransportCustomer.do?customerType=2";
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public void getDailyContractRate(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();


        PrintWriter pw = response.getWriter();
        try {
            String vehicleTypeId = "";
            Double vehicleTypeExpense = 0.0d;
            String destinationPointName = "";
            String vehicleType = "";
            String trailerTypeId = "";
            String customerId = "";
            String rate = "";
            response.setContentType("text/html");

            vehicleTypeId = request.getParameter("vehicleTypeId");
            customerId = request.getParameter("customerId");
            trailerTypeId = request.getParameter("trailerTypeId");

            secTO.setVehicleTypeId(vehicleTypeId);
            secTO.setTrailerTypeId(trailerTypeId);
            secTO.setCustomerId(customerId);
            ////System.out.println("travelKm" + travelKm + "vehicleTypeId" + vehicleTypeId + "vendorId" + vendorId + "sourceId" + sourceId + "destinatonId" + destinatonId + "ownership" + ownership);


            rate = secondaryOperationBP.getDailyContractedRate(secTO);
            //System.out.println("rate = " + rate);

            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(rate);
            writer.close();

//            JSONArray jsonArray = new JSONArray();
//            Iterator itr = pointKm.iterator();
//            int cntr = 0;
//            while (itr.hasNext() && cntr < 5) {
//                JSONObject jsonObject = new JSONObject();
//                tripTO = (TripTO) itr.next();
//                jsonObject.put("Name", tripTO.getPointKm() );
//                ////System.out.println("jsonObject = " + jsonObject);
//                jsonArray.put(jsonObject);
//                cntr++;
//            }
//            ////System.out.println("jsonArray = " + jsonArray);
//
//            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

   public ModelAndView saveQuotation(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/quotation.jsp";
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (secondaryOperationCommand.getQuotationType() != null && secondaryOperationCommand.getQuotationType() != "") {
                secTO.setQuotationType(secondaryOperationCommand.getQuotationType());
            }
            if (secondaryOperationCommand.getQuotationDate() != null && secondaryOperationCommand.getQuotationDate() != "") {
                secTO.setQuotationDate(secondaryOperationCommand.getQuotationDate());
            }
            if (secondaryOperationCommand.getQuotationStatus() != null && secondaryOperationCommand.getQuotationStatus() != "") {
                secTO.setQuotationStatus(secondaryOperationCommand.getQuotationStatus());
            }
            if (secondaryOperationCommand.getValidity() != null && secondaryOperationCommand.getValidity() != "") {
                secTO.setValidity(secondaryOperationCommand.getValidity());
            }
            if (secondaryOperationCommand.getSalesManager() != null && secondaryOperationCommand.getSalesManager() != "") {
                secTO.setSalesManager(secondaryOperationCommand.getSalesManager());
            }
            if (secondaryOperationCommand.getEmail() != null && secondaryOperationCommand.getEmail() != "") {
                secTO.setEmail(secondaryOperationCommand.getEmail());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                secTO.setCustomerName(secondaryOperationCommand.getCustomerName());
            }
            if (secondaryOperationCommand.getCustomerAddress() != null && secondaryOperationCommand.getCustomerAddress() != "") {
                secTO.setCustomerAddress(secondaryOperationCommand.getCustomerAddress());
            }
            if (secondaryOperationCommand.getDriverResponsibility() != null && secondaryOperationCommand.getDriverResponsibility() != "") {
               // secTO.setDriverResponsibility(secondaryOperationCommand.getDriverResponsibility()); not working
            }
                secTO.setDriverResponsibility(request.getParameter("driverResponsibility"));
                //System.out.println("getDriverResponsibility = " + request.getParameter("driverResponsibility"));
            if (secondaryOperationCommand.getFuelResponsibility() != null && secondaryOperationCommand.getFuelResponsibility() != "") {
                secTO.setFuelResponsibility(secondaryOperationCommand.getFuelResponsibility());
            }
            if (secondaryOperationCommand.getContactPerson() != null && secondaryOperationCommand.getContactPerson() != "") {
                secTO.setContactPerson(secondaryOperationCommand.getContactPerson());
            }
            if (secondaryOperationCommand.getDept() != null && secondaryOperationCommand.getDept() != "") {
                secTO.setDept(secondaryOperationCommand.getDept());
            }
            if (secondaryOperationCommand.getTariffType() != null && secondaryOperationCommand.getTariffType() != "") {
                secTO.setTariffType(secondaryOperationCommand.getTariffType());
            }
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
                String conditions=request.getParameter("conditions");
               secTO.setConditions(conditions);
            String quotationNo = "QTL/15-16/";
            int quotationSequence = secondaryOperationBP.getQuotationSequenceCode();
            quotationNo = quotationNo + String.valueOf(quotationSequence);
            secTO.setQuotationNo(quotationNo);

            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");
            String[] trailerTypeId = request.getParameterValues("trailerTypeId");
            String[] qty = request.getParameterValues("qty");
            String[] perKmCostValue = request.getParameterValues("perKmCost");
            String[] totalFixedCost = request.getParameterValues("totalFixedCost");
            //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
            int quotationId = secondaryOperationBP.processInsertQuotation(secTO, userId);
            if (quotationId > 0) {

                //insert Vehicle Details
                if (vehicleTypeId.length > 0) {
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String trailerTypeIds = trailerTypeId[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String qtys = qty[j];
                        String perKmCostValues = perKmCostValue[j];
                        String totalFixedCosts = totalFixedCost[j];



                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertQuotationDetails(secTO, userId, quotationId, vehicleTypeIds, fixedCostPerVehicles, trailerTypeIds,qtys,perKmCostValues,totalFixedCosts);
                        }

                    }
                }

            }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Quotation Created Succesfully successfully");

            }

            // }


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }
    public ModelAndView handleViewEditQuotation(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/viewQuotation.jsp";
            setLocale(request, response);
            String quotationId = request.getParameter("quotationId");
            operationTO.setQuotationId(quotationId);
            request.setAttribute("quotationId", quotationId);
            ArrayList salesPerson = new ArrayList();
            salesPerson = secondaryOperationBP.getAccountManager(operationTO);
            request.setAttribute("salesPerson", salesPerson);
            ArrayList quotationDetails = new ArrayList();
            quotationDetails = secondaryOperationBP.getQuotationDetails(operationTO);
            Iterator itr = quotationDetails.iterator();
            SecondaryOperationTO SecondaryOperationTONew = null;
            while (itr.hasNext()) {
                SecondaryOperationTONew = new SecondaryOperationTO();
                SecondaryOperationTONew = (SecondaryOperationTO) itr.next();
                request.setAttribute("customerName", SecondaryOperationTONew.getCustomerName());
                request.setAttribute("customerAddress", SecondaryOperationTONew.getCustomerAddress());
                request.setAttribute("quotationType", SecondaryOperationTONew.getQuotationType());
                request.setAttribute("quotationDate", SecondaryOperationTONew.getQuotationDate());
                request.setAttribute("email", SecondaryOperationTONew.getEmail());
                request.setAttribute("slaesManager", SecondaryOperationTONew.getEmpName());
                request.setAttribute("slaesManagerId", SecondaryOperationTONew.getEmpId());
                request.setAttribute("quotationStatus", SecondaryOperationTONew.getQuotationStatus());
                request.setAttribute("validity", SecondaryOperationTONew.getValidity());
                request.setAttribute("fuelResponsiblity", SecondaryOperationTONew.getFuelResponsibility());
                request.setAttribute("driverResponsiblity", SecondaryOperationTONew.getDriverResponsibility());
                request.setAttribute("contactPerson", SecondaryOperationTONew.getContactPerson());
                request.setAttribute("dept", SecondaryOperationTONew.getDept());
                 request.setAttribute("conditions", SecondaryOperationTONew.getConditions());
                 request.setAttribute("tariffType", SecondaryOperationTONew.getTariffType());
                 request.setAttribute("customerId", SecondaryOperationTONew.getCustomerId());
            }
            request.setAttribute("quotationList", quotationDetails);
            ArrayList quotationVehicleDetails = new ArrayList();
            quotationVehicleDetails = secondaryOperationBP.getQuotationVehicleDetails(operationTO);
            //System.out.println("quotationVehicleDetails size:" + quotationVehicleDetails.size());
            request.setAttribute("quotationVehicleDetails", quotationVehicleDetails);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveEditQuotation(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        CustomerTO customerTO = new CustomerTO();
        CustomerCommand customerCommand = null;
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        ModelAndView mv = null;
        path = "content/vendor/viewQuotationList.jsp";
        setLocale(request, response);
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            if (secondaryOperationCommand.getQuotationType() != null && secondaryOperationCommand.getQuotationType() != "") {
                secTO.setQuotationType(secondaryOperationCommand.getQuotationType());
            }
            if (secondaryOperationCommand.getSalesManager() != null && secondaryOperationCommand.getSalesManager() != "") {
                secTO.setSalesManager(secondaryOperationCommand.getSalesManager());
            }
            if (secondaryOperationCommand.getEmail() != null && secondaryOperationCommand.getEmail() != "") {
                secTO.setEmail(secondaryOperationCommand.getEmail());
            }
            if (secondaryOperationCommand.getCustomerAddress() != null && secondaryOperationCommand.getCustomerAddress() != "") {
                secTO.setCustomerAddress(secondaryOperationCommand.getCustomerAddress());
            }
            if (secondaryOperationCommand.getCustomerName() != null && secondaryOperationCommand.getCustomerName() != "") {
                secTO.setCustomerName(secondaryOperationCommand.getCustomerName());
            }
            if (secondaryOperationCommand.getValidity() != null && secondaryOperationCommand.getValidity() != "") {
                secTO.setValidity(secondaryOperationCommand.getValidity());
            }

            if (secondaryOperationCommand.getQuotationStatus() != null && secondaryOperationCommand.getQuotationStatus() != "") {
                secTO.setQuotationStatus(secondaryOperationCommand.getQuotationStatus());
            }
           if (secondaryOperationCommand.getContactPerson() != null && secondaryOperationCommand.getContactPerson() != "") {
                secTO.setContactPerson(secondaryOperationCommand.getContactPerson());
            }

             String conditions= request.getParameter("conditions");
                 secTO.setConditions(conditions);
            String quotationId = request.getParameter("quotationId");
            secTO.setQuotationId(quotationId);
            //System.out.println("quotationId" + quotationId);

            // update Contract Details
            int update = secondaryOperationBP.processUpdateQuotation(secTO, userId);
            //System.out.println("update" + update);
// update Exsiting Record

            // vehicle contract Details
            String[] quotationDetailsId = request.getParameterValues("quotationDetailsId");
            String[] fixedCostPerVehicleE = request.getParameterValues("fixedCostPerVehicleE");
            String[] totalCostE = request.getParameterValues("totalCostE");
            String[] costPerKmE = request.getParameterValues("costPerKmE");

            if (quotationDetailsId != null) {
                status = secondaryOperationBP.updateQuotationDetails(quotationDetailsId, fixedCostPerVehicleE,totalCostE,costPerKmE, userId);

            }
            // send Email
             if("35".equals(secondaryOperationCommand.getQuotationStatus()) ){
                 //System.out.println("inside send mail");
               ArrayList printDetails = new ArrayList();
            printDetails = secondaryOperationBP.getQuotationPrintDetails(secTO);
            ArrayList printHeader = new ArrayList();
            printHeader = secondaryOperationBP.getQuotationHeadertDetails(secTO);
            String contactPerson="";
            String quotationType="";

               Iterator itr3 = printHeader.iterator();
                   SecondaryOperationTO tpTO1=new SecondaryOperationTO();
                 if (itr3.hasNext()) {
                      tpTO1 = (SecondaryOperationTO) itr3.next();
                   contactPerson=tpTO1.getContactPerson();
                   quotationType=tpTO1.getQuotationType();
                     //System.out.println("contactPerson:"+contactPerson);
                 }
                   //System.out.println("conditions:"+conditions);
                String[] temp=conditions.split(",");

         String tomail=secondaryOperationBP.getQuotationEmailId(secTO);

            //System.out.println("tomail:"+tomail);
             String activitycode = "Quotation";
            int emailPort = 0;
            String frommailid = "";
            String smtp = "";
            String password = "";
            String toMailId = "";
             String emailCc = "";
            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
             TripTO tripTO=new TripTO();
            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
              toMailId = tripTO.getTomailId();
//              emailCc = tripTO.getCcmailId();
            }
       StringBuffer contentSB = new StringBuffer();
        Iterator itr2 = printDetails.iterator();
        SecondaryOperationTO tpTO=new SecondaryOperationTO();
            // String emailFormat =
                       contentSB.append("<html>");
                       contentSB.append("<body>");
                       contentSB.append("Dear Mr."+contactPerson);
                       contentSB.append("<br>");
                       contentSB.append("Greeting for a perfect Day");
                       contentSB.append("<br>");
                       contentSB.append("Below is the best we can offer for the equipment that required for your usage,");
                       contentSB.append("<br>");
                       contentSB.append("<table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>");
                       contentSB.append("<tr>");
                       contentSB.append("<th>Vehicle Type</th>");
                       contentSB.append("<th>Trailer  Type</th>");
                       contentSB.append("<th>Rate</th>");
                       contentSB.append("</tr>");
                       while (itr2.hasNext()) {
                          tpTO = (SecondaryOperationTO) itr2.next();
                           contentSB.append("<tr>");
                           contentSB.append("<td>"+tpTO.getVehicleTypeName()+"</td>");

                           //System.out.println("VehicleTypeName:"+tpTO.getVehicleTypeName());

                           contentSB.append("<td>"+ tpTO.getTrailerTypeName()+"</td>");
                           contentSB.append("<td>"+ tpTO.getTotalAmount()+"</td>");

                          contentSB.append("</tr>");
                       }
                        contentSB.append("<br>");
                        contentSB.append("</table>");
                         contentSB.append("<br>");
                         contentSB.append("<table> <tr><td>");
                           for(int i=0;i<temp.length;i++){
                           contentSB.append(temp[i]);
                               //System.out.println("temp:"+temp[i]);
                            contentSB.append("<br>");
                           }
                          contentSB.append("</td></tr></table> ");
                         contentSB.append("</br>");
                        contentSB.append("Kind Regards");
                        contentSB.append("</body>");
                        contentSB.append("</html>");

       String subject = "Quotation- Rate for Equipments ";
       String content = contentSB.toString();
      new SendMail(smtp, emailPort, frommailid, password, subject, content, tomail, emailCc, userId).start();
             }

           // Email End
         if("33".equals(secondaryOperationCommand.getQuotationStatus()) && "2".equalsIgnoreCase(secondaryOperationCommand.getQuotationType())){
            //Add customer
             //System.out.println("inside customer and contract");
            customerTO.setCustomerType("2");
            customerTO.setCustName(secondaryOperationCommand.getCustomerName());
            customerTO.setCustAddress(secondaryOperationCommand.getCustomerAddress());
            customerTO.setSecondaryBillingTypeId("0");
            customerTO.setBillingTypeId("1");
            customerTO.setPaymentType("1");
            customerTO.setCustContactPerson(secondaryOperationCommand.getContactPerson());
            customerTO.setCustCity("NA");
            customerTO.setCustState("NA");
            customerTO.setCustPhone("00");
            customerTO.setCustMobile("00");
            customerTO.setCustEmail(secondaryOperationCommand.getEmail());
            customerTO.setAccountManagerId(secondaryOperationCommand.getSalesManager());
            customerTO.setCustomerGroupId("0");
            customerTO.setCustStatus("");
            customerTO.setCreditLimit("0");
            customerTO.setCreditDays("0");
            customerTO.setGroupId("0");
            customerTO.setBillingNameAddress(secondaryOperationCommand.getCustomerAddress());
            customerTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            String custCode = "";
            int insertContractRouteMater = 0;
            custCode = customerBP.getCustomerCode(customerTO, insertContractRouteMater);
            request.setAttribute("custCode", custCode);
            int custId=Integer.parseInt(secondaryOperationCommand.getCustomerId());
             if ("0".equals(secondaryOperationCommand.getCustomerId()) ) {
                
                custId = customerBP.processInsertCustomer(customerTO, userId);
            } 
            //End



            // Insert Daily Contract Start
            if (secondaryOperationCommand.getCustomerId() != null && secondaryOperationCommand.getCustomerId() != "") {
                secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
           if (secondaryOperationCommand.getQuotationDate() != null && secondaryOperationCommand.getQuotationDate() != "") {
                secTO.setStartDate(secondaryOperationCommand.getQuotationDate());
            }
           if (secondaryOperationCommand.getValidity() != null && secondaryOperationCommand.getValidity() != "") {
                secTO.setEndDate(secondaryOperationCommand.getValidity());
            }
            if(custId !=0){
            secTO.setCustomerId(String.valueOf(custId));
            }else{
             secTO.setCustomerId(secondaryOperationCommand.getCustomerId());
            }
            // update Quotation CustomerId
            int updatecustId=secondaryOperationBP.processUpdateCustomerId(secTO, userId);
            // end

             String[] vehicleTypeId = request.getParameterValues("vehicleTypeIdE");
             String[] trailerTypeId = request.getParameterValues("trailerTypeIdE");
             String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicleE");
             //System.out.println("vehicleTypeId:" + vehicleTypeId.length);
             int contractId=0;
             String checkContractId="";
             checkContractId=secondaryOperationBP.getDailyContractId(secTO);
             String[] testContractId=checkContractId.split("~");
             if("0".equals(testContractId[1])){
             contractId = secondaryOperationBP.processInsertTransportCustomerDailyContract(secTO, userId);
            }else{
            contractId=Integer.parseInt(testContractId[1]);
            }
            if (contractId > 0) {

                //insert Vehicle Details
                if (vehicleTypeId.length > 0) {
                    for (int j = 0; j < vehicleTypeId.length; j++) {


                        String vehicleTypeIds = vehicleTypeId[j];
                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
                        String trailerTypeIds = trailerTypeId[j];
                        //System.out.println("vehicleTypeIds:" + vehicleTypeIds);
                        //System.out.println("fixedCostPerVehicles:" + fixedCostPerVehicle);

                        if (!"".equals(vehicleTypeIds)) {
                            status = secondaryOperationBP.processInsertTransportCustomerDailyVehicleContract(secTO, userId, contractId, vehicleTypeIds, trailerTypeIds, fixedCostPerVehicles);
                        }

                    }
                }



            }
            }
            // end
             
            //Insert New Contract
//                String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
//                String[] vehicleUnits = request.getParameterValues("vehicleUnits");
//                String[] contractType = request.getParameterValues("contractType");
//                String[] fixedCostPerVehicle = request.getParameterValues("fixedCostPerVehicle");
//                String[] totlaFixedCost = request.getParameterValues("totlaFixedCost");
//                String[] extraVehicleRunKm = request.getParameterValues("extraVehicleRunKm");
//                String[] iCnt = request.getParameterValues("iCnt");
//                String[] iCnt1 = request.getParameterValues("iCnt1");
//                String[] trailerTypeId = request.getParameterValues("trailerTypeId");
//                String[] trailerTypeUnits = request.getParameterValues("trailerTypeUnits");
//                String[] trailerContractType = request.getParameterValues("trailerContractType");
//                String[] fixedCostPerTrailer = request.getParameterValues("fixedCostPerTrailer");
//                String[] totalTrailerCost = request.getParameterValues("totalTrailerCost");
//                String[] holidayType = request.getParameterValues("holidayType");
//                String[] compensation = request.getParameterValues("compensation");

            // int contractId = secondaryOperationBP.processInsertTransportCustomerContract(secTO, userId);
//                if (contractId > 0) {
//
//   //insert Vehicle Details
//
//                            if (vehicleTypeId!= null && vehicleTypeId.length > 0) {
//                                  //System.out.println("vehicleTypeId:"+vehicleTypeId.length);
//                                for (int j = 0; j < vehicleTypeId.length; j++) {
//
//
//                                        String vehicleTypeIds = vehicleTypeId[j];
//                                        String vehicleUnit = vehicleUnits[j];
//                                        String contractTypes = contractType[j];
//                                        String fixedCostPerVehicles = fixedCostPerVehicle[j];
//                                        String totlaFixedCosts = totlaFixedCost[j];
//                                        String extraVehicleRunKms = extraVehicleRunKm[j];
//                                       //System.out.println("vehicleTypeIds:"+vehicleTypeIds+"vehicleUnit:"+vehicleUnit+"contractTypes");
//                                       //System.out.println("fixedCostPerVehicles:"+fixedCostPerVehicles+"totlaFixedCosts:"+totlaFixedCosts+"contractTypes"+"extraVehicleRunKms:"+extraVehicleRunKms);
//
//                                        if (!"".equals(vehicleTypeIds)) {
//                                            status = secondaryOperationBP.processInsertTransportCustomerVehicleContract(secTO, userId, contractId, vehicleTypeIds, vehicleUnit, contractTypes, fixedCostPerVehicles, totlaFixedCosts,extraVehicleRunKms);
//                                        }
//
//                                }
//                            }
//                            //Insert Trailer Details
//                        if (trailerTypeId!=null && trailerTypeId.length > 0) {
//                                for (int j = 0; j < trailerTypeId.length; j++) {
//
//                            String trailerTypeIds = trailerTypeId[j];
//                            String trailerTypeUnit = trailerTypeUnits[j];
//                            String trailerContractTypes = trailerContractType[j];
//                            String fixedCostPerTrailers = fixedCostPerTrailer[j];
//                            String totalTrailerCosts = totalTrailerCost[j];
//
//                    if (!"".equals(trailerTypeIds) && !"0".equals(trailerTypeIds)) {
//                        status = secondaryOperationBP.processInsertTransportCustomerTrailerContract(secTO, userId, contractId, trailerTypeIds, trailerTypeUnit, trailerContractTypes, fixedCostPerTrailers, totalTrailerCosts);
//                    }
//
//                                }
//                            }
//                 // Insert Driver Compensation
//                       if (holidayType.length > 0) {
//                         for (int j = 0; j < holidayType.length; j++) {
//                         String typeId = holidayType[j];
//                            String compensations = compensation[j];
//                             if (!"".equals(typeId) && !"0".equals(typeId)) {
//                            status=secondaryOperationBP.processInsertTransportCustomerDriverCompensation(secTO, userId, contractId, typeId, compensations);
//                         }
//                           }
//                       }
//                }
//
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Quotation Updated  successfully");

            }
            mv = handleViewQuotation(request, response, command);
            // path = "handleViewTransportCustomer.do?customerType=2";
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
//        return new ModelAndView((path));
    }
    
    
    
    public ModelAndView handleCreateSafetyAlert(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/createSafteyAlert.jsp";
            setLocale(request, response);
            ArrayList vehicleRegNoForJobCard = new ArrayList();
            vehicleRegNoForJobCard = operationBP.getVehicleRegNoForJobCard();
            request.setAttribute("vehicleRegNoForJobCard", vehicleRegNoForJobCard);
            ArrayList trailerNos = new ArrayList();
            trailerNos = operationBP.trailerNos();
            request.setAttribute("trailerNos", trailerNos);
            ArrayList salesPerson = new ArrayList();
            salesPerson = secondaryOperationBP.getEmp(operationTO);
            request.setAttribute("empList", salesPerson);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveSafetyAlert(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        TripTO tripTO = null;
        //System.out.println("i m in controller");
        secondaryOperationCommand = command;
        SecondaryOperationTO secTO = new SecondaryOperationTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        ModelAndView mv = null;
        path = "content/vendor/viewQuotationList.jsp";
        setLocale(request, response);
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
 //            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {

            String alertFor = request.getParameter("alertFor");
            String vehicleId = request.getParameter("vehicleDetails");
            String trailerId = request.getParameter("trailerDetails");
            String empId = request.getParameter("empDetails");
            String other = request.getParameter("other");
            String description = request.getParameter("description");
            String driverId = request.getParameter("driverId");
            String vehicleNo = request.getParameter("regno");
            String trailerNo = request.getParameter("trailerNo");
            String empName = request.getParameter("empName");
            String alertForName = request.getParameter("alertForName");
            String driver = request.getParameter("driverName");

            // update Contract Details
            int update = secondaryOperationBP.insertSafetyAlert(alertFor, vehicleId, trailerId, empId, other, description, driverId, userId);
            //System.out.println("update" + update);

            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Safety Alert created successfully");

            }

            String activitycode = "SAFETY-ALERT";
            int emailPort = 0;
            String frommailid = "";
            String smtp = "";
            String password = "";
            String toMailId = "";
            String emailCc = "";
            ArrayList emaildetails = new ArrayList();
            emaildetails = tripBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();

            if (itr1.hasNext()) {
                tripTO = new TripTO();
                tripTO = (TripTO) itr1.next();
                smtp = tripTO.getSmtp();
                emailPort = Integer.parseInt(tripTO.getPort());
                frommailid = tripTO.getEmailId();
                password = tripTO.getPassword();
                toMailId = tripTO.getTomailId();
//                emailCc = tripTO.getCcmailId();
            }

            String emailFormat = "<html>"
                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th colspan='2'>Alert Details</th>"
                    + "</tr>"
                    + "<tr><td>&nbsp;Alert For</td><td>&nbsp;&nbsp;" + alertForName + "</td></tr>"
                    + "<tr><td>&nbsp;Truck No</td><td>&nbsp;&nbsp;" + vehicleNo + "</td></tr>"
                    + "<tr><td>&nbsp;Trailer No</td><td>&nbsp;&nbsp;" + trailerNo + "</td></tr>"
                    + "<tr><td>&nbsp;Driver Name</td><td>&nbsp;&nbsp;" + driver + "</td></tr>"
                    + "<tr><td>&nbsp;Employee Name</td><td>&nbsp;&nbsp;" + empName + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Description</td><td>&nbsp;&nbsp;" + description + "</td></tr>"
                    + "</td></tr>"
                    + "</table></body></html>";

            String subject = "Attention-Safety Alert For" + alertForName;
            String content = emailFormat;
 //            tripTO.setMailTypeId("2");
            //            tripTO.setMailSubjectTo(subject);
            //            tripTO.setMailSubjectCc(subject);
            //            tripTO.setMailSubjectBcc("");
            //            tripTO.setMailContentTo(content);
            //            tripTO.setMailContentCc(content);
            //            tripTO.setMailContentBcc("");
            //            tripTO.setMailIdTo(toMailId);
            //            tripTO.setMailIdCc(emailCc);
            //            tripTO.setMailIdBcc(emailCc);
            //            mailSendingId = tripBP.insertMailDetails(tripTO, userId);
            new SendMail(smtp, emailPort, frommailid, password, subject, content, toMailId, emailCc, userId).start();
            mv = handleCreateSafetyAlert(request, response, command);
             // path = "handleViewTransportCustomer.do?customerType=2";
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
        //        return new ModelAndView((path));
    }

    public ModelAndView handleViewSafetyAlert(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();

        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        VehicleTO vehicleTO = new VehicleTO();
        String pageTitle = "View Transport Customer Details";
        menuPath = "Operation >>  New Contract Creation";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList standardChargeList = new ArrayList();
        //System.out.println("i m in controller..");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/vendor/viewSafetyAlertList.jsp";
            setLocale(request, response);

            ArrayList alertDetails = new ArrayList();
            alertDetails = secondaryOperationBP.getSafetyAlertDetails(operationTO);
            Iterator itr = alertDetails.iterator();
            SecondaryOperationTO SecondaryOperationTONew = null;
            while (itr.hasNext()) {
                SecondaryOperationTONew = new SecondaryOperationTO();
                SecondaryOperationTONew = (SecondaryOperationTO) itr.next();

            }
            request.setAttribute("alertDetails", alertDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handlePrintQuotation(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
            if (request.getSession().isNew()) {
                return new ModelAndView("content/common/login.jsp");
            }
            String path = "";
            HttpSession session = request.getSession();

            String menuPath = "";
            SecondaryOperationTO operationTO = new SecondaryOperationTO();
            VehicleTO vehicleTO = new VehicleTO();
            String pageTitle = "View Transport Customer Details";
            menuPath = "Operation >>  New Contract Creation";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList standardChargeList = new ArrayList();
            //System.out.println("i m in controller..");
            try {
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/trip/printQuotation.jsp";
                setLocale(request, response);

                 operationTO.setQuotationId(request.getParameter("quotationId"));
                ArrayList printDetails = new ArrayList();
                printDetails = secondaryOperationBP.getQuotationPrintDetails(operationTO);

                request.setAttribute("printDetails", printDetails);
                ArrayList printHeader = new ArrayList();
                printHeader = secondaryOperationBP.getQuotationHeadertDetails(operationTO);
                Iterator itr1 = printHeader.iterator();
                SecondaryOperationTO tpTO1=new SecondaryOperationTO();
                String quotationType="";
                String conditions="";
                if (itr1.hasNext()) {
                          tpTO1 = (SecondaryOperationTO) itr1.next();

                       quotationType=tpTO1.getQuotationType();
                       conditions=tpTO1.getConditions();
                         //System.out.println("quotationType:"+quotationType);
                         request.setAttribute("quotationType", quotationType);
                         request.setAttribute("conditions", conditions);
}
                request.setAttribute("printHeader", printHeader);

            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
            return new ModelAndView(path);
    }
 public void getDailyCustomerDetails(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
      // OperationTO operationTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String quotationId = "";
            response.setContentType("text/html");
            quotationId = request.getParameter("quotationId");
            //System.out.println("quotationId:"+quotationId);
            operationTO.setQuotationId(quotationId);
            userDetails = secondaryOperationBP.getDailyCustomerDetails(operationTO);
            //System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getCustomerDetails());
                
                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(operationTO.getCustomerDetails());
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }
 public void getQuotationTrailerType(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
      // OperationTO operationTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String quotationId = "";
            response.setContentType("text/html");
            quotationId = request.getParameter("quotationId");
            //System.out.println("quotationId:"+quotationId);
            operationTO.setQuotationId(quotationId);
            userDetails = secondaryOperationBP.getQuotationTrailerType(operationTO);
            //System.out.println("trailer.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getTrailerTypeName());
                jsonObject.put("Id", operationTO.getTrailerTypeId());

                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

 public void getQuotationVehicleType(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
      // OperationTO operationTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String quotationId = "";
            response.setContentType("text/html");
            quotationId = request.getParameter("quotationId");
            //System.out.println("quotationId:"+quotationId);
            operationTO.setQuotationId(quotationId);
            userDetails = secondaryOperationBP.getQuotationVehicleType(operationTO);
            //System.out.println("vehicle.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = userDetails.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (SecondaryOperationTO) itr.next();
                jsonObject.put("Name", operationTO.getVehicleTypeName());
                jsonObject.put("Id", operationTO.getVehicleTypeId());

                //System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

 public void deleteconsignmentContainer(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        OperationTO operationTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
//        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
//            HttpSession session = request.getSession();
            int userId = (Integer) session.getAttribute("userId");
            String contractId = "";
            String consignmentContainerId = "";
            int consignmentContainer = 0;
            response.setContentType("text/html");
            contractId = request.getParameter("contractId");
            consignmentContainerId = request.getParameter("consignmentContainerId");
            operationTO.setContractId(Integer.valueOf(contractId));
            operationTO.setConsignmentContainerId(consignmentContainerId);
            consignmentContainer = operationBP.deleteConsignmentContainer(operationTO, userId);
            //System.out.println("consignmentContainer= = " + consignmentContainer);
//            JSONArray jsonArray = new JSONArray();
//            Iterator itr = userDetails.iterator();
//            while (itr.hasNext()) {
//                JSONObject jsonObject = new JSONObject();
//                operationTO = (OperationTO) itr.next();
//                jsonObject.put("Name", operationTO.getConsignorName());
//                jsonObject.put("Mobile", operationTO.getConsignorPhoneNo());
//                jsonObject.put("Address", operationTO.getConsignorAddress());
//                jsonObject.put("custId", operationTO.getCustomerId());
//                //System.out.println("jsonObject = " + jsonObject);
//                jsonArray.put(jsonObject);
//            }
//            //System.out.println("jsonArray = " + jsonArray);

            pw.print(consignmentContainer);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }


  public ModelAndView customerDistanceContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
            if (request.getSession().isNew()) {
                return new ModelAndView("content/common/login.jsp");
            }
            String path = "";
            HttpSession session = request.getSession();

            String menuPath = "";
            SecondaryOperationTO operationTO = new SecondaryOperationTO();
            VehicleTO vehicleTO = new VehicleTO();
            String pageTitle = "View Transport Customer Details";
            menuPath = "Operation >>  New Contract Creation";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList standardChargeList = new ArrayList();
            //System.out.println("i m in controller..");
            try {
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/BrattleFoods/customerDistanceContractMaster.jsp";
                setLocale(request, response);
                 String companyId = request.getParameter("companyId");
                String companyName = request.getParameter("companyName");
                  String display = request.getParameter("display");
                 ArrayList vehicleTypeList = new ArrayList();
                vehicleTypeList = operationBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                ArrayList containerTypeList = new ArrayList();
                containerTypeList = operationBP.getContainerTypeList();
                request.setAttribute("containerTypeList", containerTypeList);
                request.setAttribute("companyId", companyId);
                request.setAttribute("companyName", companyName);
                  request.setAttribute("customerId", "0");
                request.setAttribute("billingTypeId", "1");
                request.setAttribute("display", display);
                   ArrayList contractList = new ArrayList();
                   operationTO.setCompanyId(companyId);
                   operationTO.setCustomerId("0");
                   contractList=secondaryOperationBP.getCustomerDistanceContractDetails(operationTO);
                     request.setAttribute("contractList", contractList);
                     request.setAttribute("contractListSize", contractList.size());
            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
            return new ModelAndView(path);
    }
public ModelAndView saveCustomerDistanceContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
            if (request.getSession().isNew()) {
                return new ModelAndView("content/common/login.jsp");
            }
            String path = "";
            ModelAndView mv=null;
            HttpSession session = request.getSession();
             secondaryOperationCommand = command;
            String menuPath = "";
            SecondaryOperationTO operationTO = new SecondaryOperationTO();
            OperationTO operationTO1 = new OperationTO();
            VehicleTO vehicleTO = new VehicleTO();
            String pageTitle = "View Transport Customer Details";
            menuPath = "Operation >>  New Contract Creation";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList standardChargeList = new ArrayList();
            //System.out.println("i m in controller..");
               int userId = (Integer) session.getAttribute("userId");
            try {
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/BrattleFoods/customerDistanceContractMaster.jsp";
                setLocale(request, response);
                if (secondaryOperationCommand.getContractNo() != null && !"".equals(secondaryOperationCommand.getContractNo())) {
                        operationTO1.setContractNo(secondaryOperationCommand.getContractNo());
                    }
                    if (secondaryOperationCommand.getCustomerId() != null && !"".equals(secondaryOperationCommand.getCustomerId())) {
                        operationTO1.setCustomerId(secondaryOperationCommand.getCustomerId());
                    }
                    if (secondaryOperationCommand.getContractFrom() != null && !"".equals(secondaryOperationCommand.getContractFrom())) {
                        operationTO1.setContractFrom(secondaryOperationCommand.getContractFrom());
                    }
                    if (secondaryOperationCommand.getContractTo() != null && !"".equals(secondaryOperationCommand.getContractTo())) {
                        operationTO1.setContractTo(secondaryOperationCommand.getContractTo());
                    }
                    if (secondaryOperationCommand.getBillingTypeId() != null && !"".equals(secondaryOperationCommand.getBillingTypeId())) {
                        operationTO1.setBillingTypeId(secondaryOperationCommand.getBillingTypeId());
                    }
                operationTO1.setCompanyID1(request.getParameter("companyId"));

               
operationTO.setCompanyId( request.getParameter("companyId"));
operationTO.setCustomerId( secondaryOperationCommand.getCustomerId());
                    // for update start
          int insertStatus = 0;
                String[] distanceContractIds=null;
                   distanceContractIds=request.getParameterValues("distanceContractId");
                String[] rateWithReefer1=request.getParameterValues("rateWithReefer1");
                String[] rateWithoutReefer1=request.getParameterValues("rateWithoutReefer1");
                String[] validStatus=request.getParameterValues("validStatus");
                  if(distanceContractIds !=null && distanceContractIds.length > 0){
                      insertStatus=Integer.parseInt(secondaryOperationBP.getContractId(operationTO));
                  int update=secondaryOperationBP.updateCustomerDistanceContract(distanceContractIds,rateWithReefer1,rateWithoutReefer1,validStatus);
                }
               
                if(distanceContractIds ==null && distanceContractIds.length == 0){
                    insertStatus = operationBP.insertCustomerContractDetails(operationTO1, userId);
                    if (insertStatus > 0) {
                        operationTO1.setContractId(insertStatus);
                    }
                }
                String[] refernceName = null;
                    if (secondaryOperationCommand.getReferenceName() != null && !"".equals(secondaryOperationCommand.getReferenceName())) {
                        refernceName = secondaryOperationCommand.getReferenceName();
                    }
                String[] vehicleTypeIds = null;
                    if (secondaryOperationCommand.getDistnaceVehicleTypeId() != null && !"".equals(secondaryOperationCommand.getDistnaceVehicleTypeId())) {
                        vehicleTypeIds = secondaryOperationCommand.getDistnaceVehicleTypeId();
                    }
                   
                    
                    // containerTypeId
                    String[] containerTypeId = null;
                    if (secondaryOperationCommand.getContainerTypeId() != null && !"".equals(secondaryOperationCommand.getContainerTypeId())) {
                        containerTypeId = secondaryOperationCommand.getContainerTypeId();
                    }
                    // containerQty
                    String[] containerQty = null;
                    if (secondaryOperationCommand.getContainerQty() != null && !"".equals(secondaryOperationCommand.getContainerQty())) {
                        containerQty = secondaryOperationCommand.getContainerQty();
                    }
                    // containerQty
                    String[] loadTypeId = null;
                    if (secondaryOperationCommand.getLoadTypeId() != null && !"".equals(secondaryOperationCommand.getLoadTypeId())) {
                        loadTypeId = secondaryOperationCommand.getLoadTypeId();
                    }
                    //fromDistance
                    String[] fromDistance = null;
                    if (secondaryOperationCommand.getFromDistance() != null && !"".equals(secondaryOperationCommand.getFromDistance())) {
                        fromDistance = secondaryOperationCommand.getFromDistance();
                    }
                    //toDistance
                    String[] toDistance = null;
                    if (secondaryOperationCommand.getToDistance() != null && !"".equals(secondaryOperationCommand.getToDistance())) {
                        toDistance = secondaryOperationCommand.getToDistance();
                    }
                    String[] rateWithReeferDistance = null;
                    if (secondaryOperationCommand.getRateWithReeferDistance() != null && !"".equals(secondaryOperationCommand.getRateWithReeferDistance())) {
                        rateWithReeferDistance = secondaryOperationCommand.getRateWithReeferDistance();
                    }
                    String[] rateWithoutReeferDistance = null;
                    if (secondaryOperationCommand.getRateWithoutReeferDistance() != null && !"".equals(secondaryOperationCommand.getRateWithoutReeferDistance())) {
                        rateWithoutReeferDistance = secondaryOperationCommand.getRateWithoutReeferDistance();
                    }
                String companyId = request.getParameter("companyId");
                String companyName = request.getParameter("companyName");
                int status=secondaryOperationBP.insertCustomerDistanceContract(refernceName,vehicleTypeIds,containerTypeId,containerQty,fromDistance,toDistance,rateWithReeferDistance,rateWithoutReeferDistance,loadTypeId,String.valueOf(insertStatus),userId);
                String display = request.getParameter("display");
                ArrayList vehicleTypeList = new ArrayList();
                vehicleTypeList = operationBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                ArrayList containerTypeList = new ArrayList();
                containerTypeList = operationBP.getContainerTypeList();
                request.setAttribute("containerTypeList", containerTypeList);
                request.setAttribute("companyId", companyId);
                request.setAttribute("companyName", companyName);
                request.setAttribute("customerId", "0");
                request.setAttribute("billingTypeId", "1");
                request.setAttribute("display", display);
                mv=viewCustomerDistanceContract(request,response,secondaryOperationCommand);
                path = "viewOrderBillDetailsForSubmit.do";
            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
            return  mv;
    }

 public ModelAndView viewCustomerDistanceContract(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) {
            if (request.getSession().isNew()) {
                return new ModelAndView("content/common/login.jsp");
            }
            String path = "";
            HttpSession session = request.getSession();

            String menuPath = "";
            SecondaryOperationTO operationTO = new SecondaryOperationTO();
            VehicleTO vehicleTO = new VehicleTO();
            String pageTitle = "View Transport Customer Details";
            menuPath = "Operation >>  New Contract Creation";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList standardChargeList = new ArrayList();
            //System.out.println("i m in controller..");
            try {
                ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                path = "content/BrattleFoods/viewCustomerDistanceContractMaster.jsp";
                setLocale(request, response);
                 String companyId = request.getParameter("companyId");
                String companyName = request.getParameter("companyName");
                  String display = request.getParameter("display");
                 ArrayList vehicleTypeList = new ArrayList();
                vehicleTypeList = operationBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                ArrayList containerTypeList = new ArrayList();
                containerTypeList = operationBP.getContainerTypeList();
                request.setAttribute("containerTypeList", containerTypeList);
                request.setAttribute("companyId", companyId);
                request.setAttribute("companyName", companyName);
                  request.setAttribute("customerId", "0");
                request.setAttribute("billingTypeId", "1");
                request.setAttribute("display", display);
                   ArrayList contractList = new ArrayList();
                   operationTO.setCustomerId("0");
                   operationTO.setCompanyId(companyId);
                   contractList=secondaryOperationBP.getCustomerDistanceContractDetails(operationTO);
                     request.setAttribute("contractList", contractList);
                     request.setAttribute("contractListSize", contractList.size());
            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
            return new ModelAndView(path);
    }

  public void validateRoute(HttpServletRequest request, HttpServletResponse response, SecondaryOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        secondaryOperationCommand = command;
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String check = "";
        PrintWriter pw = response.getWriter();
        try {
            String origin="", pointId1="", pointId2="", destination="", customerId="", vehicleTypeId="", containerTypeId="", loadTypeId="";
            response.setContentType("text/html");
            origin = request.getParameter("origin");
            pointId1 = request.getParameter("pointId1");
            pointId2 = request.getParameter("pointId2");
            destination = request.getParameter("destination");
            customerId = request.getParameter("customerId");
            vehicleTypeId = request.getParameter("vehicleTypeId");
            containerTypeId = request.getParameter("containerTypeId");
            loadTypeId = request.getParameter("loadTypeId");

            check = secondaryOperationBP.validateContract(origin, pointId1, pointId2, destination, customerId, vehicleTypeId, containerTypeId, loadTypeId);
            //////System.out.println("userDetails.size() = " + userDetails.size());
            JSONArray jsonArray = new JSONArray();
             JSONObject jsonObject = new JSONObject();
              jsonObject.put("Id", check);
               jsonArray.put(jsonObject);
//            Iterator itr = userDetails.iterator();
//            while (itr.hasNext()) {
//                JSONObject jsonObject = new JSONObject();
//                operationTO = (SecondaryOperationTO) itr.next();
//                jsonObject.put("Name", operationTO.getCityName());
//                jsonObject.put("Id", operationTO.getCityId());
//                jsonArray.put(jsonObject);
//            }
            //System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

}
