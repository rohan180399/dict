package ets.domain.vendor.data;

import ets.domain.util.ThrottleConstants;
import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.racks.business.RackTO;
import ets.domain.vendor.business.VendorTO;
import ets.domain.operation.business.OperationTO;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author vidya
 */
public class VendorDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "VendorDAO";
    public int lim = 0;
    private String agreedFuelPrice;

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        try {

            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.VendorLists", map);
            // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return vendorList;

    }
    //getVendorTypeList

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVendorTypeList() {
        Map map = new HashMap();
        ArrayList vendorTypeList = new ArrayList();
        try {
            System.out.println("map values" + map);
            vendorTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.VendorTypeList", map);
            // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "VendorTypeList", sqlException);
        }
        return vendorTypeList;

    }

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doVendorDetails(VendorTO vendorTO, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved) {

        Map map = new HashMap();
        FileInputStream fis = null;
        int tripId = 0;
        int vendorId = 0;
        /*
                * set the parameters in the map for sending to ORM
         */
        try {

            if (tripSheetId1 != null && tripSheetId1 != "") {
                tripId = Integer.parseInt(tripSheetId1);
                map.put("tripSheetId", tripId);
            }
            System.out.println("actualFilePath[0] = " + actualFilePath[0]);
            System.out.println("actualFilePath.length = " + actualFilePath.length);
            for (int x = 0; x < actualFilePath.length; x++) {
                System.out.println("fileSaved[x] = " + fileSaved[x]);
                if (fileSaved[x] != null) {
                    map.put("fileName" + x, fileSaved[x]);
                    System.out.println("fileName map = " + map);
                    System.out.println("actualFilePath = " + actualFilePath[x]);
                    File file = new File(actualFilePath[x]);
                    System.out.println("file = " + file);
                    fis = new FileInputStream(file);
                    System.out.println("fis = " + fis);
                    byte[] podFile = new byte[(int) file.length()];
                    System.out.println("podFile = " + podFile);
                    fis.read(podFile);
                    fis.close();
                    map.put("podFile" + x, podFile);
                    System.out.println("podfile map = " + map);
                    System.out.println("the saveTripPodDetails123455" + map);
                } else {
                    map.put("fileName" + x, null);
                    map.put("podFile" + x, null);
                }
            }

            System.out.println("Tesiting = ");
            map.put("userId", UserId);
            map.put("vendorName", vendorTO.getVendorName());
            map.put("settlementType", vendorTO.getSettlementType());
            map.put("tinNo", vendorTO.getTinNo());
            map.put("vendorTypeId", vendorTO.getVendorTypeId());
            map.put("vendorAddress", vendorTO.getVendorAddress());
            map.put("vendorPhoneNo", vendorTO.getVendorPhoneNo());
            map.put("vendorMailId", vendorTO.getVendorMailId());
            map.put("priceType", vendorTO.getPriceType());
            map.put("creditDays", vendorTO.getCreditDays());
            //        map.put("manufacturerId", vendorTO.getMfrIds());

            map.put("contactName", vendorTO.getContactName());
            map.put("designation", vendorTO.getDesignation());
            map.put("emailId", vendorTO.getEmailId());
            map.put("teleNo", vendorTO.getTeleNo());
            map.put("mobileNo", vendorTO.getMobileNo());
            map.put("faxNo", vendorTO.getFaxNo());
            map.put("bankName", vendorTO.getBankName());
            map.put("branch", vendorTO.getBranch());
            map.put("branchCode", vendorTO.getBranchCode());
            map.put("ifscCode", vendorTO.getIfscCode());
            map.put("micrNo", vendorTO.getMicrNo());
            map.put("msmeId", vendorTO.getMsmeId());
            map.put("nstcId", vendorTO.getNstcId());
            map.put("gstId", vendorTO.getGstId());
            map.put("dgsdId", vendorTO.getDgsdId());
            map.put("eepcId", vendorTO.getEepcId());
            map.put("serViceTax", vendorTO.getSerViceTax());
            map.put("ecId", vendorTO.getEcId());
            map.put("exciseDuty", vendorTO.getExciseDuty());
            map.put("vatId", vendorTO.getVatId());
            map.put("cstId", vendorTO.getCstId());
            map.put("panNo", vendorTO.getPanNo());
            map.put("accntNo", vendorTO.getAccountNo());
            map.put("rocId", vendorTO.getRocId());
            map.put("stateId", vendorTO.getStateId());
            map.put("gstNo", vendorTO.getGstNo());

            System.out.println("maprr" + map);
            int status = 0;
            int mfrstatus = 0;

            String code = "";
            String[] temp;
            int vehicle_id = 0;
            int VendorLedgerId = 0;

            status = (Integer) getSqlMapClientTemplate().update("vendor.insertVendor", map);
            System.out.println("status" + status);
            vendorId = (Integer) getSqlMapClientTemplate().queryForObject("vendor.lastInsertedVendor", map);
            System.out.println("vendorId" + vendorId);

            //Ledger Code start
            if (vendorId != 0) {
                code = (String) getSqlMapClientTemplate().queryForObject("vendor.getLedgerCode", map);
                temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                map.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("vendor.accYearVal", map);
                System.out.println("accYear:" + accYear);
                map.put("accYear", accYear);
                //current year end

                String vendorName = vendorTO.getVendorName() + "-" + vendorId;
                System.out.println("vendorName =====> " + vendorName);

                VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("vendor.insertVendorLedger", map);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    map.put("ledgerId", VendorLedgerId);
                    map.put("vendorId", vendorId);
                    System.out.println("map update ::::=> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("vendor.updateVendorMaster", map);
                }
            }
            //Ledger Code end

            //            map.put("vendorId",vendorId);
            //             map.put("manufacturerId", vendorTO.getMfrIds());
            //            mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.insertMfr", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                    * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vendorId;

    }

    public int insertVendorMfrDetails(int vendorId, int mfrId, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", UserId);

        int status = 0;
        int mfrstatus = 0;

        try {

            map.put("vendorId", vendorId);
            map.put("manufacturerId", mfrId);
            mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.insertMfr", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }

    /**
     * This method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAlterVendorDetail(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        System.out.println("vendorId in dao" + vendorId);
        ArrayList partsDetail = null;
        ArrayList mfrDetail = null;
        try {
            partsDetail = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.GetVendorDetailUnique", map);
            // mfrDetail = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.GetVendorAllMfrList", map);
            System.out.println("GetVendorDetailUnique in dao " + partsDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetVendorDetailUnique", sqlException);
        }
        return partsDetail;
    }
    //doVendorUpdateDetails

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doVendorUpdateDetails(VendorTO vendorTO, int UserId) {

        Map map = new HashMap();
        /*
             * set the parameters in the map for sending to ORM
         */

        map.put("userId", UserId);
        map.put("vendorName", vendorTO.getVendorName());
        map.put("tinNo", vendorTO.getTinNo());
        map.put("vendorTypeId", vendorTO.getVendorTypeId());
        map.put("vendorAddress", vendorTO.getVendorAddress());
        map.put("vendorPhoneNo", vendorTO.getVendorPhoneNo());
        map.put("vendorMailId", vendorTO.getVendorMailId());
        map.put("activeInd", vendorTO.getActiveInd());

        map.put("manufacturerId", vendorTO.getMfrIds());
        map.put("vendorId", vendorTO.getVendorId());

        map.put("contactName", vendorTO.getContactName());
        map.put("designation", vendorTO.getDesignation());
        map.put("emailId", vendorTO.getEmailId());
        map.put("teleNo", vendorTO.getTeleNo());
        map.put("mobileNo", vendorTO.getMobileNo());
        map.put("faxNo", vendorTO.getFaxNo());
        map.put("bankName", vendorTO.getBankName());
        map.put("branch", vendorTO.getBranch());
        map.put("branchCode", vendorTO.getBranchCode());
        map.put("ifscCode", vendorTO.getIfscCode());
        map.put("micrNo", vendorTO.getMicrNo());
        map.put("msmeId", vendorTO.getMsmeId());
        map.put("nstcId", vendorTO.getNstcId());
        map.put("gstId", vendorTO.getGstId());
        map.put("dgsdId", vendorTO.getDgsdId());
        map.put("eepcId", vendorTO.getEepcId());
        map.put("serViceTax", vendorTO.getSerViceTax());
        map.put("ecId", vendorTO.getEcId());
        map.put("exciseDuty", vendorTO.getExciseDuty());
        map.put("vatId", vendorTO.getVatId());
        map.put("cstId", vendorTO.getCstId());
        map.put("panNo", vendorTO.getPanNo());
        map.put("accntNo", vendorTO.getAccountNo());
        map.put("rocId", vendorTO.getRocId());

        int status = 0;
        int mfrstatus = 0;
        //   int vendorId = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("vendor.updateVendor", map);
            //    vendorId = (Integer) getSqlMapClientTemplate().queryForObject("vendor.lastUpdatedVendor", map);
            //            map.put("vendorId",vendorId);
            //             map.put("manufacturerId", vendorTO.getMfrIds());
            //            mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.insertMfr", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateVendor", sqlException);
        }
        return status;

    }

    public int updateVendorMfrDetails(VendorTO vendorTO, String[] mfrids, String[] index, int UserId) {
        Map map = new HashMap();
        int status = 0;
        int check = 0;
        int mfrId = 0;
        int vendorId = 0;
        int temp = 0;
        int status1 = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", UserId);
        vendorId = Integer.parseInt(vendorTO.getVendorId());
        map.put("vendorId", vendorId);
        // map.put("vendorId", vendorId);

        int mfrstatus = 0;

        try {

            check = (Integer) getSqlMapClientTemplate().update("vendor.makeAllInactive", map);
            if (index != null) {
                for (int i = 0; i < index.length; i++) {
                    temp = Integer.parseInt(index[i]);
                    mfrId = Integer.parseInt(mfrids[temp]);
                    map.put("manufacturerId", mfrId);
                    System.out.println("mfr=" + mfrId + "-vend=" + vendorId);
                    //inner updation used to update mfr_config
                    check = (Integer) getSqlMapClientTemplate().queryForObject("vendor.checkVendorConfigExists", map);
                    if (check == 0) {
                        System.out.println("insert");
                        mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.insertVendorConfig", map);
                    } else {
                        System.out.println("update");
                        mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.updateVendorMfr", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateMfr", sqlException);
        }
        return status;

    }

    /**
     * This ajax method used to Get SubRack List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAjaxVendorList(int vendorTypeId) {
        Map map = new HashMap();
        ArrayList ajaxVendorList = new ArrayList();
        map.put("vendorTypeId", vendorTypeId);
        System.out.println("vendorTypeId in DAO" + vendorTypeId);
        try {

            ajaxVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.AjaxVendorList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxSubRackList", sqlException);
        }
        return ajaxVendorList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getItemList(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        ArrayList itemList = new ArrayList();
        try {

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.ItemList", map);
            System.out.println("i am in dao itemList  :" + itemList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return itemList;

    }

    //getAssignedList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAssignedList(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        ArrayList assignedList = new ArrayList();
        try {

            assignedList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.AssignedList", map);
            System.out.println("i am in dao assignedList  :" + assignedList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return assignedList;

    }
    //doAllNoToActiveInd

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doAllNoToActiveInd(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        int status = 0;
        try {
            System.out.println("786-vendor Id in Dao" + vendorId);
            status = (Integer) getSqlMapClientTemplate().update("vendor.doAllNoToActiveInd", map);
            System.out.println("786-vendor Id in Dao  :" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doAllNoToActiveInd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "doAllNoToActiveInd", sqlException);
        }
        return status;

    }

    //resultAssignedList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int getCount(int itemId, int vendorId, int userId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        map.put("itemId", itemId);
        map.put("userId", userId);
        int count = 0;
        int updat = 0;
        int inst = 0;

        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("vendor.getCount", map);
            System.out.println("Count" + count);
            if (count == 0) {
                System.out.println("insert Record");
                inst = (Integer) getSqlMapClientTemplate().update("vendor.doInsertAssignedList", map);
                System.out.println("inst" + inst);
            } else {
                System.out.println("Update Record");
                updat = (Integer) getSqlMapClientTemplate().update("vendor.doUpdateAssignedList", map);
                System.out.println("updat" + updat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return count;

    }

    public int doVendorCategoryConfig(int categoryId, int vendorId, int userId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        map.put("categoryId", categoryId);
        map.put("userId", userId);
        int count = 0;
        int updat = 0;
        int inst = 0;

        try {
            count = (Integer) getSqlMapClientTemplate().queryForObject("vendor.checkVendorCatConfig", map);
            System.out.println("Count" + count);
            if (count == 0) {
                System.out.println("insert Record");
                inst = (Integer) getSqlMapClientTemplate().update("vendor.addVendorCat", map);
                System.out.println("inst" + inst);
            } else {
                System.out.println("Update Record");
                updat = (Integer) getSqlMapClientTemplate().update("vendor.updateVendorCat", map);
                System.out.println("updat" + updat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doVendorCategoryConfig Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "doVendorCategoryConfig", sqlException);
        }
        return count;

    }

    public int doVendorCatInactive(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        int status = 0;
        try {
            System.out.println("786-vendor Id in Dao" + vendorId);
            status = (Integer) getSqlMapClientTemplate().update("vendor.doAllVendorInactive", map);
            System.out.println("786-vendor Id in Dao  :" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doVendorCatInactive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "doVendorCatInactive", sqlException);
        }
        return status;

    }

    public ArrayList getAssignedCatList(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        ArrayList assignedList = new ArrayList();
        try {

            assignedList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.AssignedCatList", map);
            System.out.println("i am in dao assignedList  :" + assignedList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return assignedList;
    }

    public ArrayList getCategoryAssignedList(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        ArrayList assignedList = new ArrayList();
        try {

            assignedList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.AssignedCatList", map);
            System.out.println("i am in dao assignedList  :" + assignedList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return assignedList;

    }

    public ArrayList getCatNotAssignedList(int vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        ArrayList itemList = new ArrayList();
        try {

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.vendorCatList", map);
            System.out.println("i am in dao itemList  :" + itemList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "itemList", sqlException);
        }
        return itemList;

    }

    public ArrayList getVendorLists(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        try {
            try {
                map.put("startIndex", vendorTO.getStartIndex());
                map.put("endIndex", vendorTO.getEndIndex());
                map.put("vendorTypeId", vendorTO.getVendorTypeId());
                System.out.println("my map value" + map);
                vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.VendorList", map);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return vendorList;

    }

    public ArrayList getVendorContractList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vendorContractList = new ArrayList();
        try {

            vendorContractList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVendorContractList", map);
            System.out.println("vendorContractList.size()-->" + vendorContractList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "vendorContractList", sqlException);
        }
        return vendorContractList;

    }

    public int processInsertVehicleVendor(VendorTO vendorTO, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        int contractId = 0;
        try {
            map.put("userId", UserId);
            map.put("agreedFuelPrice", vendorTO.getAgreedFuelPrice());
            map.put("hikeFuelPrice", vendorTO.getHikeFuelPrice());
            map.put("uom", vendorTO.getUom());
            System.out.println("map ===================== " + map);
            if (!"".equals(vendorTO.getVendorId()) && !"".equals(vendorTO.getStartDate()) && !"".equals(vendorTO.getEndDate()) && !"".equals(vendorTO.getPaymentType()) && !"".equals(vendorTO.getContractTypeId())) {
                map.put("vendorId", vendorTO.getVendorId());
                map.put("startDate", vendorTO.getStartDate());
                map.put("endDate", vendorTO.getEndDate());
                map.put("paymentType", vendorTO.getPaymentType());
                map.put("contractTypeId", vendorTO.getContractTypeId());
                map.put("contractTypeId", vendorTO.getContractTypeId());
                contractId = (Integer) session.insert("vendor.InsertVehicleVendorContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

    public int processInsertVehicleVendorRoute(VendorTO vendorTO, int userId, int contractId, String originIdFullTruck, String originNameFullTruck, String destinationIdFullTruck, String destinationNameFullTruck, String travelKmFullTruck, String travelHourFullTruck,
            String travelMinuteFullTruck, String vehicleTypeId, String vehicleUnits, String spotCost,
            String additionalCost, String loadTypeId, String containerTypeId, String containerQty,
            String pointId1, String pointId2, String pointId3, String pointId4, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
         int insertStatus=0;
        try {
            map.put("userId", userId);
            map.put("contractId", contractId);
            if (originIdFullTruck != null && !"".equals(originIdFullTruck)) {
                if (!"".equals(originIdFullTruck) && !"".equals(originNameFullTruck) && !"".equals(destinationIdFullTruck) && !"".equals(destinationNameFullTruck)) {
                    map.put("originIdFullTruck", originIdFullTruck);
                    map.put("originNameFullTruck", originNameFullTruck);
                    map.put("pointId1", pointId1);
                    map.put("pointId2", pointId2);
                    map.put("pointId3", pointId3);
                    map.put("pointId4", pointId4);
                    map.put("destinationNameFullTruck", destinationNameFullTruck);
                    map.put("destinationIdFullTruck", destinationIdFullTruck);
                    map.put("travelKmFullTruck", travelKmFullTruck);
                    map.put("travelHourFullTruck", travelHourFullTruck);
                    map.put("travelMinuteFullTruck", travelMinuteFullTruck);
                    System.out.println("insertHireList map dfgdfh= " + map);
                    int routeContractId = (Integer) session.insert("vendor.insertHireList", map);
                    System.out.println("routeContractId = " + routeContractId);
                    if (routeContractId > 0) {
                        System.out.println("routeContractId Arul Here step4 " + routeContractId);
                        if (vehicleTypeId != null && !"".equals(vehicleTypeId)) {
                            if (vehicleTypeId.contains("~")) {
                                String[] temp = vehicleTypeId.split("~");
                                map.put("vehicleTypeId", temp[0]);
                            } else {
                                map.put("vehicleTypeId", vehicleTypeId);
                            }
                            map.put("vehicleUnits", vehicleUnits);
                            map.put("trailerType", 0);
                            map.put("trailerTypeUnits", 0);
                            map.put("spotCost", spotCost);
                            map.put("additionalCost", additionalCost);
                            map.put("routeContractId", routeContractId);
                            map.put("loadTypeId", loadTypeId);
                            map.put("containerTypeId", containerTypeId);
                            map.put("containerQty", containerQty);
                            map.put("approvalStatus", vendorTO.getApprovalStatus());
                            map.put("requestBy", vendorTO.getRequestBy());
                            map.put("requestOn", vendorTO.getRequestOn());
                            map.put("requestRemarks", vendorTO.getRequestRemarks());

                            map.put("approvalBy", vendorTO.getApprovalBy());
                            map.put("approvalOn", vendorTO.getApprovalOn());
                            map.put("approvalRemarks", vendorTO.getApprovalRemarks());
                            System.out.println("map dfgdfh= " + map);
                             insertStatus = (Integer) session.insert("vendor.insertHire", map);
                              System.out.println("insertStatus***************************************8= " + insertStatus);
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                      * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return insertStatus;

    }

    public int processInsertVehicleVendorDedicate(VendorTO vendorTO, int userId, int contractId, String[] vehicleTypeIdDedicate, String[] vehicleUnitsDedicate,
            String[] contractCategory, String[] fixedCost, String[] fixedHrs, String[] fixedMin, String[] totalFixedCost,
            String[] rateCost, String[] rateLimit, String[] maxAllowableKM, String[] workingDays, String[] holidays, String[] addCostDedicate, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int updateStatus = 0;
        try {
            map.put("userId", userId);
            map.put("contractId", contractId);
            //  map.put("vendorId", vendorId);
            map.put("agreedFuelPrice", vendorTO.getAgreedFuelPrice());
            map.put("hikeFuelPrice", vendorTO.getHikeFuelPrice());
            map.put("uom", vendorTO.getUom());
            System.out.println("agreedFuelPrice insert Vehicle map" + map);
            if (vendorTO.getAgreedFuelPrice() != "") {
                System.out.println("inside condition");
                updateStatus = (Integer) session.update("vendor.updateFuel", map);
                System.out.println("insertStatus for dedicate111111 = " + updateStatus);
            }

            for (int i = 0; i < vehicleTypeIdDedicate.length; i++) {
                if ("".equals(vehicleUnitsDedicate[i])) {
                    vehicleUnitsDedicate[i] = "0";
                }
                if (!"0".equals(vehicleUnitsDedicate[i]) && !"0".equals(vehicleTypeIdDedicate[i])) {
                    map.put("vehicleTypeIdDedicate", vehicleTypeIdDedicate[i]);
                    map.put("vehicleUnitsDedicate", vehicleUnitsDedicate[i]);
                    //                map.put("trailorTypeDedicate", trailorTypeDedicate[i]);
                    //                map.put("trailorUnitsDedicate", trailorUnitsDedicate[i]);
                    map.put("contractCategory", contractCategory[i]);
                    map.put("fixedCost", fixedCost[i]);
                    map.put("fixedHrs", fixedHrs[i]);
                    map.put("fixedMin", fixedMin[i]);
                    map.put("totalFixedCost", totalFixedCost[i]);
                    map.put("rateCost", rateCost[i]);
                    map.put("rateLimit", rateLimit[i]);
                    map.put("maxAllowableKM", maxAllowableKM[i]);
                    map.put("workingDays", workingDays[i]);
                    map.put("holidays", holidays[i]);
                    map.put("addCostDedicate", addCostDedicate[i]);
                    map.put("activeInd", "Y");
                    status = (Integer) session.update("vendor.insertDedicate", map);
                    System.out.println("the insert status is" + status);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }

    public ArrayList getVehicleList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            System.out.println("getVehicleList.map()-->" + map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVehicleList", map);
            System.out.println("getVehicleList.size()-->" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "vehicleList", sqlException);
        }
        return vehicleList;

    }

    public ArrayList getVehicleLists(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            System.out.println("getVehicleList.map()-->" + map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVehicleLists", map);
            System.out.println("getVehicleList.size()-->" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "vehicleList", sqlException);
        }
        return vehicleList;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTypeList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList TypeList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            TypeList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.TypeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return TypeList;

    }

    public ArrayList getVehicleTypeList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            System.out.println("getVehicleTypeListttttttttttt.map()-->" + map);
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVehicleTypeList", map);
            System.out.println("getVehicleTypeList.size()-->" + vehicleTypeList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;

    }

    public ArrayList vendorVehicleContractList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            System.out.println("getVehicleTypeList1111.map()-->" + map);
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.vendorVehicleContractList", map);
            System.out.println("getVehicleTypeList.size()-->" + vehicleTypeList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;

    }

    public ArrayList viewVendorVehicleContract(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList viewVendorVehicleContract = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            map.put("contractId", vendorTO.getContractId());
            System.out.println("getviewVendorVehicleContract.map()-->" + map);
            viewVendorVehicleContract = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVendorVehicleContract", map);
            System.out.println("getviewVendorVehicleContract.size()-->" + viewVendorVehicleContract.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewVendorVehicleContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "viewVendorVehicleContract", sqlException);
        }
        return viewVendorVehicleContract;

    }

    //Hari Start
    public ArrayList getRcVendorList() {
        Map map = new HashMap();
        ArrayList rcVendorList = new ArrayList();
        try {

            rcVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getRcVendorList", map);
            System.out.println("rcVendorList.size()-->" + rcVendorList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return rcVendorList;

    }

    public ArrayList getUomList() {
        Map map = new HashMap();
        ArrayList uomList = new ArrayList();
        try {

            uomList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.UomList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "UomList", sqlException);
        }
        return uomList;

    }

    public ArrayList getCityToList(OperationTO opTO) {
        Map map = new HashMap();
        ArrayList getCityToList = new ArrayList();
        try {
            map.put("cityName", opTO.getCity() + "%");
            map.put("originCityId", opTO.getOriginCityId());
            System.out.println("map = " + map);
            getCityToList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getCityToList", map);
            if (getCityToList.size() == 0) {
                getCityToList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getCityToListReverse", map);
            }
            System.out.println("getCityToList = " + getCityToList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getCityToList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCityToList", sqlException);
        }
        return getCityToList;
    }

    public int updateVendorContractDetails(VendorTO vendorTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("userId", userId);
        map.put("contractTypeId", vendorTO.getContractTypeId());
        map.put("endDate", vendorTO.getEndDate());
        map.put("vendorId", vendorTO.getVendorId());
        System.out.println("map fro update contract = " + map);
        try {
            updateStatus = (Integer) session.update("vendor.updateVendorContract", map);
            System.out.println("updateStatus = " + updateStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCustomerContract", sqlException);
        }

        return updateStatus;
    }

    public String viewVendorVehicle(VendorTO vendorTO) {
        Map map = new HashMap();
        String vendorContractLists = "";

        try {
            map.put("vendorId", vendorTO.getVendorId());

            System.out.println("viewVendorVehicleContract.map()-->" + map);
            vendorContractLists = (String) getSqlMapClientTemplate().queryForObject("vendor.viewVendorVehicle", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewVendorVehicleContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewVendorVehicleContract", sqlException);
        }

        return vendorContractLists;
    }

    public ArrayList getDedicateList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList dedicateList = new ArrayList();

        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vendorId", vendorTO.getVendorId());
        map.put("contractId", vendorTO.getContractId());

        System.out.println("vendorTO.getVendorId()= " + vendorTO.getVendorId());
        System.out.println("map = " + map);

        try {

            dedicateList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getDedicateList", map);

            System.out.println("dedicateHireList.size()= " + dedicateList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDedicateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDedicateList", sqlException);
        }
        return dedicateList;
    }

    public ArrayList getHireList(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList hireList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vendorId", vendorTO.getVendorId());
        map.put("contractId", vendorTO.getContractId());

        System.out.println("vendorTO.getVendorId()= " + vendorTO.getVendorId());
        System.out.println("map = " + map);

        try {
            hireList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getHireList", map);
            System.out.println("dedicateHireList.size()= " + hireList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDedicateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDedicateList", sqlException);
        }
        return hireList;
    }

    public int updateVendorVehicleDedicate(VendorTO vendorTO, int userId, String[] contractDedicateId, String[] fixedCostE, String[] fixedHrsE, String[] fixedMinE, String[] totalFixedCostE, String[] rateCostE,
            String[] rateLimitE, String[] workingDaysE, String[] holidaysE, String[] addCostDedicateE, String[] activeIndE,
            SqlMapClient session) {
        Map map = new HashMap();
        int insertStatus = 0;
        int checkDedicate = 0;
        /*
             * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        map.put("agreedFuelPrice", vendorTO.getAgreedFuelPrice());
        map.put("hikeFuelPrice", vendorTO.getHikeFuelPrice());
        map.put("uom", vendorTO.getUom());
        System.out.println("agreedFuelPrice update Vehicle map" + map);

        try {
            if (contractDedicateId.length > 0) {
                for (int j = 0; j < contractDedicateId.length; j++) {
                    map.put("contractDedicateId", contractDedicateId[j]);
                    map.put("fixedCostE", fixedCostE[j]);
                    map.put("fixedHrsE", fixedHrsE[j]);
                    map.put("fixedMinE", fixedMinE[j]);
                    map.put("totalFixedCostE", totalFixedCostE[j]);
                    map.put("rateCostE", rateCostE[j]);
                    map.put("rateLimitE", rateLimitE[j]);
                    map.put("workingDaysE", workingDaysE[j]);
                    map.put("holidaysE", holidaysE[j]);
                    map.put("addCostDedicateE", addCostDedicateE[j]);
                    map.put("activeIndE", activeIndE[j]);
                    System.out.println("map gerter= " + map);

                    insertStatus = (Integer) session.update("vendor.updateVendorVehicleDedicate", map);
                    System.out.println("insertStatus for dedicate = " + insertStatus);

                    //                     checkDedicate = (Integer) getSqlMapClientTemplate().queryForObject("vendor.checkDedicate", map);
                    //                        System.out.println("checkTrailerNo" + checkDedicate);
                    insertStatus = (Integer) session.update("vendor.updateFuel", map);
                    System.out.println("insertStatus for dedicate333333 = " + insertStatus);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateVendorVehicleDedicate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateVendorVehicleDedicate", sqlException);
        }
        return insertStatus;
    }

    public int updateVendorVehicleRoute(VendorTO vendorTO, int userId, String[] contractHireId, String[] spotCostE, String[] additionalCostE, String[] activeIndD, String vendorName,SqlMapClient session) {
        Map map = new HashMap();
        int insertStatus = 0;
        /*
             * set the parameters in the map for sending to ORM
         */
        map.put("userId", userId);
        try {
            VendorTO mailTO = new VendorTO();

            String[] pointName1E = vendorTO.getPointName1E();
            String[] pointName2E = vendorTO.getPointName2E();
            String[] pointName3E = vendorTO.getPointName3E();
            String[] pointName4E = vendorTO.getPointName4E();
            String[] originNameFullTruckE = vendorTO.getOriginNameFullTruckE();
            String[] destinationNameFullTruckE = vendorTO.getDestinationNameFullTruckE();
            String[] approvalStatusE = vendorTO.getApprovalStatusE();
            String[] vehicleTypeE = vendorTO.getVehicleTypeE();
            String[] loadTypeE = vendorTO.getVehicleTypeE();
            String[] containerTypeIdE1 = vendorTO.getContainerTypeIdE1();
            String[] containerQtyE1 = vendorTO.getContainerQtyE1();

            System.out.println("contractHireId.length" + contractHireId.length);
            if (contractHireId != null) {
                if (contractHireId.length > 0) {
                    for (int j = 0; j < contractHireId.length; j++) {
                        map.put("contractHireId", contractHireId[j]);
                        map.put("spotCostE", spotCostE[j]);
                        map.put("additionalCostE", additionalCostE[j]);
                        map.put("activeIndD", activeIndD[j]);
                        System.out.println("map = " + map);
                        insertStatus = (Integer) session.update("vendor.updateVendorVehicleRoute", map);

                        if ("2".equals(approvalStatusE[j])) {
                            System.out.println("approvalStatus" + approvalStatusE[j]);
                            mailTO.setContractHireId(contractHireId[j]);
                            mailTO.setOriginNameFullTruck(originNameFullTruckE[j]);
                            mailTO.setDestinationNameFullTruck(destinationNameFullTruckE[j]);
                            mailTO.setPoint1Name(pointName1E[j]);
                            mailTO.setVehicleType(vehicleTypeE[j]);
                            mailTO.setLoadTypeId(loadTypeE[j]);
                            mailTO.setContainerTypeId(containerTypeIdE1[j]);
                            mailTO.setContainerQty(containerQtyE1[j]);
                            mailTO.setSpotCost(spotCostE[j]);
                            mailTO.setAdditionalCost(additionalCostE[j]);
                            mailTO.setVendorName(vendorName);
                            System.out.println("userID@@@" + userId);
                            sendContractApprovalMail(mailTO, userId,session);
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateVendorVehicleRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateVendorVehicleRoute", sqlException);
        }
        return insertStatus;
    }

     public int sendContractApprovalMail(VendorTO mailTO, int userId,SqlMapClient session) {
	        System.out.println("sendContractApprovalMail@@@@");
	        int mailSendingId = 0;
	        try {
	//            HttpSession session = request.getSession();
	//            int userId = (Integer) session.getAttribute("userId");
	            System.out.println("userId::" + userId);
	            String to = "",cc="";
	            String smtp = "";
	            int emailPort = 0;
	            String frommailid = "";
	            String password = "";

	            Map map = new HashMap();

	            smtp = ThrottleConstants.smtpServer;
	            emailPort = Integer.parseInt(ThrottleConstants.smtpPort);
	            frommailid = ThrottleConstants.fromMailId;
	            password = ThrottleConstants.fromMailPassword;
	            to = ThrottleConstants.fuelApprovingMailId;
	            cc = ThrottleConstants.contractApprovingCCMailId;

	            System.out.println("smtp=" + smtp);
	            System.out.println("emailPort=" + emailPort);
	            System.out.println("frommailid=" + frommailid);
	            System.out.println("password=" + password);
	            System.out.println("to=" + to);
	            // String oldFuelPriceId = "", effectiveDate = "", fuelPrice = "", fuelType = "",fuelTypes = "",fuelUnite = "",cityId="",status="",bunkId="",bunkName="";

	            String containerQty = "", vechTypId = "", containerTypId = "", lodTypId = "", firstPickPointId = "", finalPointId = "", interim1 = "", interim2 = "", interim3 = "", interim4 = "", contractHireId = "", rateWithReefer = "", rateWithoutReefer = "";
	            vechTypId = "1".equals(mailTO.getVehicleTypeId()) ? "20 Feet" : "40 Feet";
	            containerTypId = "1".equals(mailTO.getContainerTypeId()) ? "20 Feet" : "40 Feet";
	            lodTypId = "1".equals(mailTO.getLoadTypeId()) ? "Empty Trip" : "Load Trip";
	            containerQty = mailTO.getContainerQty();
	            firstPickPointId = mailTO.getOriginNameFullTruck();
	            interim1 = mailTO.getPoint1Name();
	            finalPointId = mailTO.getDestinationNameFullTruck();
	            rateWithReefer = mailTO.getSpotCost();
	            rateWithoutReefer = mailTO.getAdditionalCost();
	            contractHireId = mailTO.getContractHireId();
	            String vendorName = mailTO.getVendorName();
	            System.out.println("vendorName in dao ++++++"+vendorName);
	            String emailFormat = "<html>"
	                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
	                    + "<tr>"
	                    + "<th colspan='2'>Vendor Contract Rate Approval For :"+vendorName+"</th>"
	                    + "</tr>"
	                    + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vechTypId + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Container Type</td><td>&nbsp;&nbsp;" + containerTypId + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Load Type</td><td>&nbsp;&nbsp;" + lodTypId + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Container QTY</td><td>&nbsp;&nbsp;" + containerQty + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Picup point</td><td>&nbsp;&nbsp;" + firstPickPointId + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Interim Point1</td><td>&nbsp;&nbsp;" + interim1 + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Interim Point2</td><td>&nbsp;&nbsp;" + interim2 + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Interim Point3</td><td>&nbsp;&nbsp;" + interim3 + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Interim Point4</td><td>&nbsp;&nbsp;" + interim4 + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Drop Point</td><td>&nbsp;&nbsp;" + finalPointId + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Rate With Reefer</td><td>&nbsp;&nbsp;" + rateWithReefer + "</td></tr>"
	                    + "<tr><td>&nbsp;&nbsp;Rate Without Reefer</td><td>&nbsp;&nbsp;" + rateWithoutReefer + "</td></tr>"
	                    + "<tr height='25'><td></td><td></td></tr>"
	                    + "<tr><td colspan='2' align='center'>"
	                    + "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApprovalVendor.do?userId=" + userId + "&contractRateId=" + contractHireId + "&status=1'>Approve</a>&nbsp;|&nbsp;"
	                    + "<a style='text-decoration: none' href='http://dict.throttletms.com/throttle/updateContractApprovalVendor.do?userId=" + userId + "&contractRateId=" + contractHireId + "&status=3'>Reject</a>"
	                    + "</td></tr>"
	                    + "</table></body>"
	                    + "<script></script></html>";

	            String subject = "Vendor Contract Rate Approval Request";
	            String content = emailFormat;

	            TripTO tripTO = new TripTO();
	            tripTO.setMailTypeId("2");
	            tripTO.setMailSubjectTo(subject);
	            tripTO.setMailSubjectCc(subject);
	            tripTO.setMailSubjectBcc("");
	            tripTO.setMailContentTo(content);
	            tripTO.setMailContentCc(content);
	            tripTO.setMailContentBcc("");
	            tripTO.setMailIdTo(to);
	            tripTO.setMailIdCc(cc);
	            tripTO.setMailIdBcc("");

	            map.put("mailTypeId", tripTO.getMailTypeId());
	            map.put("mailSubjectTo", tripTO.getMailSubjectTo());
	            map.put("mailSubjectCc", tripTO.getMailSubjectCc());
	            map.put("mailSubjectBcc", tripTO.getMailSubjectBcc());
	            map.put("mailContentTo", tripTO.getMailContentTo());
	            map.put("mailContentCc", tripTO.getMailContentCc());
	            map.put("mailContentBcc", tripTO.getMailContentBcc());
	            map.put("mailTo", tripTO.getMailIdTo());
	            map.put("mailCc", tripTO.getMailIdCc());
	            map.put("mailBcc", tripTO.getMailIdBcc());
	            map.put("userId", userId);

	            System.out.println("userId:^^:" + userId);
	            int insertMailDetails = (Integer) session.insert("trip.insertMailDetails", map);
	            System.out.println("mailSendingId" + insertMailDetails);

	        } catch (FPRuntimeException exception) {
	            /*
	             * run time exception has occurred. Directed to error page.
	             */
	            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
	        } catch (Exception exception) {
	            exception.printStackTrace();
	            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
	        }
	        return mailSendingId;
    }

    public int processInsertVehicleContract(VendorTO vendorTO, int UserId) {

        //   Map map1 = new HashMap();
        Map map = new HashMap();
        Map map2 = new HashMap();
        int status = 0;
        // map.put("userId vehicle", UserId);
        //  map.put("contractId", vendorTO.getContractId());
        map.put("userId", UserId);
        try {
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            map2.put("contractDedicateId", vendorTO.getContractDedicateId());
            map2.put("vehicleRegNo", vendorTO.getVehicleRegNo());
            map2.put("agreedDate", vendorTO.getAgreedDate());
            map2.put("mfr", vendorTO.getMfr());
            map2.put("model", vendorTO.getModel());
            map2.put("Remarks", vendorTO.getRemarks());
            map2.put("activeInd", vendorTO.getActiveInd());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            System.out.println("vehicle map2============== =" + map2);

            String[] vehicleRegNo = vendorTO.getVehicleRegNo().split(",");
            String[] agreedDate = vendorTO.getAgreedDate().split(",");
            String[] mfr = vendorTO.getMfr().split(",");
            String[] model = vendorTO.getModel().split(",");
            String[] remarks = vendorTO.getRemarks().split(",");
            //       String [] activeInd= vendorTO.getActiveInd().split(",");

            String temp[] = vendorTO.getVehicleRegNo().split(",");
            System.out.println("temp.length = " + temp.length);
            //    System.out.println("temp.length = "+ vendorTO.getVehicleRegNo().length());

            if (vendorTO.getVehicleRegNo() != null && !"".equals(vendorTO.getVehicleRegNo())) {
                //      if (vendorTO.getVehicleRegNo().length() > 0) {

                //    if (!"".equals(vendorTO.getVehicleRegNo()) && !"".equals(vendorTO.getAgreedDate()) && !"".equals(vendorTO.getRemarks())) {
                if (temp.length > 0) {
                    for (int i = 0; i < temp.length; i++) {
                        Object put1 = map.put("vehicleRegNo", vehicleRegNo[i]);
                        Object put2 = map.put("agreedDate", agreedDate[i]);
                        Object put3 = map.put("mfr", mfr[i]);
                        Object put4 = map.put("model", model[i]);
                        Object put5 = map.put("remarks", remarks[i]);

                        //        map.put("activeInd", vendorTO.getActiveInd());
                        System.out.println("map1 = " + map);
                        System.out.println("put1 = " + put1);
                        System.out.println("put2 = " + put2);
                        System.out.println("put3 = " + put3);
                        System.out.println("put4 = " + put4);
                        System.out.println("put5 = " + put5);
                        status = (Integer) getSqlMapClientTemplate().update("vendor.InsertVehicleVendor", map);
                        System.out.println("status1 = " + status);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleVendorContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;
    }

    public int processInsertTrailerContract(VendorTO vendorTO, String[] trailerType, String[] trailerNo, String[] trailerRemarks, int UserId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", UserId);
            map.put("vendorId", vendorTO.getVendorId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("trailerTypeAxles", vendorTO.getTrailerTypeAxles());
            String temp[] = trailerNo;
            System.out.println("tempTra.length = " + temp.length);
            if (temp.length > 0) {
                for (int i = 0; i < temp.length; i++) {
                    if (trailerNo[i] != null && !"".equals(trailerNo[i])) {
                        System.out.println("trailerNo[i] = " + trailerNo[i]);
                        System.out.println("trailerType[i] = " + trailerType[i]);
                        System.out.println("trailerRemarks[i] = " + trailerRemarks[i]);
                        map.put("trailerType", trailerType[i]);
                        map.put("trailerNo", trailerNo[i]);
                        map.put("trailerRemarks", trailerRemarks[i]);
                        status = (Integer) session.update("vendor.InsertTrailers", map);
                        status = (Integer) session.update("vendor.InsertTrailersMaster", map);
                        System.out.println("status = " + status);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleVendorContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;
    }

    public int processUpdateTrailerContract(VendorTO vendorTO, String[] trailerNo, String[] activeIndTrailor, String[] trailerRemarks, int userId) {
        Map map = new HashMap();
        //  Map map1 = new HashMap();
        Map map2 = new HashMap();
        int status = 0;
        int status1 = 0;
        int checkTrailerNo = 0;
        try {
            map.put("userId", userId);
            map.put("vendorId", vendorTO.getVendorId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("trailerType", vendorTO.getTrailorTypeId());

            System.out.println("trailerNo.length = " + trailerNo.length);

            if (trailerNo.length > 0) {

                for (int i = 0; i < trailerNo.length; i++) {
                    if (trailerNo[i] != null && !"".equals(trailerNo[i])) {
                        System.out.println("trailerNo[i] = " + trailerNo[i]);
                        System.out.println("trailerRemarks[i] = " + trailerRemarks[i]);
                        System.out.println("activeIndTrailor[i] = " + activeIndTrailor[i]);
                        System.out.println("checkTrailerNo map" + map);
                        map.put("trailerNo", trailerNo[i]);
                        map.put("trailerRemarks", trailerRemarks[i]);
                        map.put("activeInd", activeIndTrailor[i]);
                        checkTrailerNo = (Integer) getSqlMapClientTemplate().queryForObject("vendor.checkTrailerNo", map);
                        System.out.println("checkTrailerNo" + checkTrailerNo);
                        if (checkTrailerNo > 0) {
                            System.out.println("update Trailer map" + map);
                            status = (Integer) getSqlMapClientTemplate().update("vendor.updateTrailers", map);
                            status1 = (Integer) getSqlMapClientTemplate().update("vendor.updateTrailersMaster", map);
                            System.out.println("update Trailer status = " + status);
                        } else {
                            System.out.println("Insert Trailer map" + map);
                            status = (Integer) getSqlMapClientTemplate().update("vendor.InsertTrailers", map);
                            status1 = (Integer) getSqlMapClientTemplate().update("vendor.InsertTrailersMaster", map);
                            System.out.println("Insert Trailer status = " + status);
                        }
                        System.out.println("status = " + status);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleVendorContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleConfig(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vehicles = new ArrayList();
        try {
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            System.out.println("getVehicleConfigure.map()-------->" + map);
            vehicles = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVehicleConfigure", map);
            System.out.println("getVehicleConfigure.size()-->" + vehicles.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeLists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getVehicleTypeListsv", sqlException);
        }
        return vehicles;

    }

    public ArrayList getTrailerConfig(VendorTO vendorTO) {

        Map map = new HashMap();
        ArrayList trailerList = new ArrayList();
        try {
            map.put("vendorId", vendorTO.getVendorId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("trailerTypeId", vendorTO.getTrailorTypeId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            System.out.println("getTrailerConfigure.map()-------->" + map);
            trailerList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getTrailerConfigure", map);
            System.out.println("getTrailerConfigure.size()-->" + trailerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerConfigure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getTrailerConfigure", sqlException);
        }
        return trailerList;

    }

    public int updateTrailer(String[] contractDedicateId, String[] trailerType, String[] trailerNo, String[] trailerRemarks, String[] activeInd) {
        {
            Map map = new HashMap();
            int insertStatus = 0;
            /*
             * set the parameters in the map for sending to ORM
             */

            try {

                if (contractDedicateId.length > 0) {
                    for (int j = 0; j < contractDedicateId.length; j++) {
                        map.put("contractDedicateId", contractDedicateId[j]);
                        Object put = map.put("trailerType", trailerType[j]);
                        System.out.println("put1" + put);
                        map.put("trailerNo", trailerNo[j]);
                        map.put("trailerRemarks", trailerRemarks[j]);
                        map.put("activeInd", activeInd[j]);
                        //  map.put("activeIndRate", activeIndRate[j]);
                        System.out.println("map = " + map);

                        //                        insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.checkVendorContractDedicateId", map);
                        //                        if (insertStatus > 0) {
                        insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.updateVendorTrailerDetails", map);
                        //                        } else {
                        //                            insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.InsertTrailers", map);
                        //                        }
                    }
                }

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS,
                        "insertCustomerContractDetails", sqlException);
            }
            return insertStatus;
        }
    }

    public int updateVehicles(String[] contractId, String[] vehicleRegNo, String[] agreedDate, String[] mfr, String[] model, String[] remarks, String[] activeInd) {

        Map map = new HashMap();
        int insertStatus = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {

            if (contractId.length > 0) {
                for (int j = 0; j < contractId.length; j++) {
                    map.put("contractId", contractId[j]);
                    map.put("vehicleRegNo", vehicleRegNo[j]);
                    map.put("agreedDate", agreedDate[j]);
                    map.put("mfr", mfr[j]);
                    map.put("model", model[j]);
                    map.put("remarks", remarks[j]);
                    map.put("activeInd", activeInd[j]);
                    //  map.put("activeIndRate", activeIndRate[j]);
                    System.out.println("map = " + map);

                    //                        insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.checkVendorContractDedicateId", map);
                    //                        if (insertStatus > 0) {
                    insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.updateVendorVehicleDetails", map);
                    //                        } else {
                    //                            insertStatus = (Integer) getSqlMapClientTemplate().update("vendor.InsertTrailers", map);
                    //                        }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getTrailerDetail(VendorTO vendorTO) {

        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        //    map.put("vehicleId", vehicleTO.getVehicleId());
        try {
            System.out.println("i m here 4");
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.searchTrailer", map);
            System.out.println("trailer sizeefsdf:" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getCityFromList(VendorTO vendorTO) {

        Map map = new HashMap();
        ArrayList cityList = new ArrayList();

        map.put("cityName", vendorTO.getCityId() + "%");
        try {
            System.out.println("i m here 4");
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getCityFromList", map);
            System.out.println("trailer sizeefsdf:" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityFromList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getCityFromList", sqlException);
        }
        return cityList;
    }

    public int updatesaveVehicles(VendorTO vendorTO, int userId, String[] vehicleRegNo1, String[] vehicleTypeIdcheck1, String[] mfr1, String[] model1, String[] agreedDate, String[] remarks, SqlMapClient session) {

        Map map = new HashMap();
        int vehicleId = 0;
        int status = 0;
        int insertStatus1 = 0;
        int insertVehicleUsageStatus = 0;
        int insertVehicleOperationPoint = 0;
        int insertVehiclesFcDetails = 0;
        int insertVehiclesClassDetails = 0;
        int insertVehiclesEngineDetails = 0;
        int insertVehiclesHistroy = 0;
        /*
         * set the parameters in the map for sending to ORM
         */

        try {
            map.put("vendorId", vendorTO.getVendorId());
            map.put("ownership", "2");
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            System.out.println("vehicleRegNo1" + vehicleRegNo1);
            map.put("userId", userId);
            if (vehicleRegNo1.length > 0) {
                for (int j = 0; j < vehicleTypeIdcheck1.length; j++) {
                    map.put("vehicleRegNo", vehicleRegNo1[j]);
                    map.put("vehicleTypeIdcheck1", vehicleTypeIdcheck1[j]);
                    map.put("mfr", mfr1[j]);
                    map.put("model", model1[j]);
                    map.put("agreedDate", agreedDate[j]);
                    System.out.println("map select= " + map);
                    System.out.println("vehicleRegNo=" + vehicleRegNo1.length);
                    System.out.println("vehicleTypeIdcheck=" + vehicleTypeIdcheck1.length);
                    System.out.println("mfr=" + mfr1.length);
                    System.out.println("model=" + model1.length);
                    int vehicleCount = 0;
                    vehicleCount = (Integer) session.queryForObject("vendor.getVehicleRegNoExists", map);
                    if (vehicleCount == 0) {
                        vehicleId = (Integer) session.insert("vendor.updatesaveVehicles", map);
                        map.put("vehicleId", vehicleId);
                        System.out.println("insertStatus" + vehicleId);
                        if (vehicleId > 0) {
                            insertStatus1 = (Integer) session.insert("vendor.updatesaveVehicles1", map);
                            insertVehicleUsageStatus = (Integer) session.insert("vendor.saveVehicleusageStatus", map);
                            insertVehicleOperationPoint = (Integer) session.insert("vendor.saveVehicleOperationPoint", map);

                            insertVehiclesFcDetails = (Integer) session.update("vendor.saveVehiclesFcDetails", map);
                            map.put("insertVehiclesFcDetails", insertVehiclesFcDetails);
                            insertVehiclesClassDetails = (Integer) session.update("vendor.saveVehiclesClassDetails", map);
                            map.put("insertVehiclesClassDetails", insertVehiclesClassDetails);
                            insertVehiclesEngineDetails = (Integer) session.update("vendor.saveVehiclesEngineDetails", map);
                            map.put("insertVehiclesEngineDetails", insertVehiclesEngineDetails);
                            insertVehiclesHistroy = (Integer) session.update("vendor.saveVehiclesHistroy", map);
                            map.put("saveVehiclesHistroy", insertVehiclesHistroy);

                            map.put("vehicleRegNo", vehicleRegNo1[j]);
                            map.put("agreedDate", agreedDate[j]);
                            map.put("remarks", remarks[j]);
                            session.update("vendor.InsertVehicleVendor", map);
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCustomerContractDetails", sqlException);
        }
        return vehicleId;
    }

    public ArrayList getAjaxModelList(int mfr, String vehicleTypeId) {
        Map map = new HashMap();
        ArrayList ajaxModelList = new ArrayList();
        map.put("mfr", mfr);
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            System.out.println(" mapppp is"+map);
            ajaxModelList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.AjaxModelList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return ajaxModelList;
    }

    public String getVehicleContractDedicateId(VendorTO vendorTO, int userId) {
        Map map = new HashMap();
        String contractDedicateId = "";
        map.put("userId", userId);
        map.put("vendorId", vendorTO.getVendorId());
        map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
        System.out.println("vendorTO.getVendorId()" + vendorTO.getVendorId());
        try {

            contractDedicateId = (String) getSqlMapClientTemplate().queryForObject("vendor.getVehicleContractDedicateId", map);
            System.out.println("contractDedicateId" + contractDedicateId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return contractDedicateId;
    }

    public int processUpdateVehicleContract(VendorTO vendorTO, String[] configureId, String[] configureIds, String[] replaceVehicleId, String[] vehicleRegNos,
            String[] agreedDates, String[] mfrs, String[] models, String[] vehicleRemarks,
            String[] activeIndVehicles, String[] existVehicle, String[] inDate, String[] replacementStatus, int userId, SqlMapClient session) {

        Map map = new HashMap();
        Map map2 = new HashMap();
        int status = 0;
        int finalStatus = 0;
        int insertStatus = 0;
        int insertStatus1 = 0;
        int updateStatus = 0;
        int updateStatus1 = 0;
        String checkVehicleStatus = "";
        int insertVehicleUsageStatus = 0;
        int insertVehicleOperationPoint = 0;
        int insertVehiclesFcDetails = 0;
        int insertVehiclesClassDetails = 0;
        int insertVehiclesEngineDetails = 0;
        int insertVehiclesHistroy = 0;
        map.put("userId", userId);
        try {
            map.put("contractId", vendorTO.getContractId());
            map.put("vendorId", vendorTO.getVendorId());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("contractDedicateId", vendorTO.getContractDedicateId());
            System.out.println("Map MAIN" + map);
            if (vehicleRegNos != null) {
                if (vehicleRegNos.length > 0) {
                    for (int i = 0; i < vehicleRegNos.length; i++) {
                        System.out.println("vehicleRegNos.length------------------------->" + vehicleRegNos.length);
                        map.put("configureId", configureId[i]);
                        map.put("vehicleId", replaceVehicleId[i]);
                        map.put("vehicleRegNo", vehicleRegNos[i]);
                        map.put("agreedDate", agreedDates[i]);
                        map.put("model", models[i]);
                        map.put("mfr", mfrs[i]);
                        map.put("remarks", vehicleRemarks[i]);
                        map.put("activeInd", activeIndVehicles[i]);
                        map.put("ownership", "2");
                        map.put("existVehicle", existVehicle[i]);

                        map.put("inDate", inDate[i]);
                        System.out.println("map%%%%" + map);
                        if ((existVehicle[i] == null || "".equals(existVehicle[i])) && (replaceVehicleId[i] == null || "".equals(replaceVehicleId[i]))) {//New Vehicle Add
                            System.out.println("Update**********************0%%%%%%");
                            System.out.println("insert vehicle map ===============" + map);
                            insertStatus = (Integer) session.insert("vendor.updatesaveVehicles", map);
                            System.out.println("insertVehicle" + insertStatus);
                            map.put("vehicleId", insertStatus);
                            status = (Integer) session.update("vendor.InsertVehicleVendor", map);
                            System.out.println("InsertVehicleVendor%" + status);
                            if (insertStatus > 0) {
                                insertStatus1 = (Integer) session.insert("vendor.updatesaveVehicles1", map);
                                System.out.println("insert regno%%%% ===============" + map);
                                insertVehicleUsageStatus = (Integer) session.insert("vendor.saveVehicleusageStatus", map);
                                insertVehicleOperationPoint = (Integer) session.insert("vendor.saveVehicleOperationPoint", map);

                                System.out.println("saveVehiclesFcDetails vehicle map ===============" + map);
                                insertVehiclesFcDetails = (Integer) session.update("vendor.saveVehiclesFcDetails", map);
                                System.out.println("saveVehiclesClassDetails vehicle map ===============" + map);
                                insertVehiclesClassDetails = (Integer) session.update("vendor.saveVehiclesClassDetails", map);
                                System.out.println("saveVehiclesEngineDetails vehicle map ===============" + map);
                                insertVehiclesEngineDetails = (Integer) session.update("vendor.saveVehiclesEngineDetails", map);
                                System.out.println("saveVehiclesHistroy vehicle map ===============" + map);
                                insertVehiclesHistroy = (Integer) session.update("vendor.saveVehiclesHistroy", map);
                            }
                            if (insertVehiclesHistroy > 0) {
                                finalStatus = 1;
                            }
                        } else if ((replaceVehicleId[i] != null || !"".equals(replaceVehicleId[i])) && (existVehicle[i] == null || "".equals(existVehicle[i]))) {//Update
                            System.out.println("Update**********************1");
                            System.out.println("Update Vehicles*************" + map);
                            status = (Integer) session.update("vendor.updateVehicleVendor", map);
                            System.out.println("updateVehicleVendor" + status);
                            updateStatus = (Integer) session.update("vendor.updateVehicleContractMaster", map);
                            System.out.println("updateVehicleMaster" + updateStatus);
                            updateStatus1 = (Integer) session.update("vendor.updateVehicleRegisterNo", map);
                            System.out.println("updateVehicleRegisterNo" + updateStatus1);

                            if (updateStatus1 > 0) {
                                finalStatus = 1;
                            }

                        } else if ((replaceVehicleId[i] == null || "".equals(replaceVehicleId[i])) && (existVehicle[i] != null || !"".equals(existVehicle[i]))) {//Replace Vehicle Insert
                            System.out.println("Update**********************2");
                            map.put("configureId", configureId[i]);
                            updateStatus = (Integer) session.update("vendor.updateOutDateConfigure", map);
                            System.out.println("checkVehicleStatus" + updateStatus);
                            map.put("existVehicleId", existVehicle[i]);
                            updateStatus = (Integer) session.update("vendor.updateOutDateRegNo", map);
                            System.out.println("InsertVehicleVendor" + updateStatus);
                            insertStatus = (Integer) session.insert("vendor.updatesaveVehicles", map);
                            System.out.println("insertStatus%%%" + insertStatus);
                            map.put("vehicleId", insertStatus);
                            System.out.println("map&&&" + map);
                            status = (Integer) session.insert("vendor.InsertVehicleConfigForReplace", map);
                            map.put("configureId", status);
                            System.out.println("replacemap###" + updateStatus);
                            updateStatus1 = (Integer) session.update("vendor.insertRegNoforReplace", map);
                            System.out.println("updateRegNoReplace" + updateStatus1);
                            insertStatus1 = (Integer) session.update("vendor.insertReplacementVehicle1", map);
                            System.out.println("updateReplacement" + insertStatus1);
                            if (insertStatus > 0) {

                                insertVehicleUsageStatus = (Integer) session.insert("vendor.saveVehicleusageStatus", map);
                                insertVehicleOperationPoint = (Integer) session.insert("vendor.saveVehicleOperationPoint", map);

                                System.out.println("saveVehiclesFcDetails vehicle map ===============" + map);
                                insertVehiclesFcDetails = (Integer) session.update("vendor.saveVehiclesFcDetails", map);
                                System.out.println("saveVehiclesClassDetails vehicle map ===============" + map);
                                insertVehiclesClassDetails = (Integer) session.update("vendor.saveVehiclesClassDetails", map);
                                System.out.println("saveVehiclesEngineDetails vehicle map ===============" + map);
                                insertVehiclesEngineDetails = (Integer) session.update("vendor.saveVehiclesEngineDetails", map);
                                System.out.println("saveVehiclesHistroy session map ===============" + map);
                                insertVehiclesHistroy = (Integer) session.update("vendor.saveVehiclesHistroy", map);
                            }
                            if (insertVehiclesHistroy > 0) {
                                finalStatus = 1;
                            }

                        } else if ((replaceVehicleId[i] != null || !"".equals(replaceVehicleId[i])) && (existVehicle[i] != null || !"".equals(existVehicle[i]))) {//Change Vehicles Acitve
                            System.out.println("Update**********************3");
                            map.put("vehicleId", replaceVehicleId[i]);
                            map.put("existVehicle", existVehicle[i]);

                            map.put("configureId", configureId[i]);
                            map.put("configureIds", configureIds[i]);
                            map.put("replacementStatus", replacementStatus[i]);
                            System.out.println("Map**********3=" + map);
                            updateStatus = (Integer) session.update("vendor.updateInDateConfigure", map);
                            System.out.println("checkVehicleStatus" + updateStatus);
                            updateStatus = (Integer) session.update("vendor.updateInDateRegNo", map);
                            System.out.println("InsertVehicleVendor" + updateStatus);

                            updateStatus = (Integer) session.update("vendor.updateVehicleContractMaster", map);
                            System.out.println("updateVehicleMaster" + updateStatus);

                            updateStatus = (Integer) session.update("vendor.updateOutDateConfigure", map);
                            System.out.println("checkVehicleStatus" + updateStatus);
                            updateStatus = (Integer) session.update("vendor.updateOutDateRegNo", map);
                            System.out.println("updateOutDateRegNo" + updateStatus);

                            map.put("inDate", inDate[i]);
                            map.put("replacementStatus", replacementStatus[i]);
                            System.out.println("map@@@4" + map);
                            if ("1".equals(replacementStatus[i])) {
                                System.out.println("if");
                                map.put("configureId", configureId[i]);
                                insertStatus1 = (Integer) session.update("vendor.insertReplacementVehicle1", map);
                                System.out.println("replace" + insertStatus1);
                            } else {
                                System.out.println("else");
                                map.put("configureIds", configureIds[i]);
                                insertStatus1 = (Integer) session.update("vendor.insertReplacementVehicle", map);
                                System.out.println("existReplace" + insertStatus1);
                            }

                            if (insertStatus1 > 0) {
                                finalStatus = 1;
                            }

                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleVendorContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return finalStatus;
    }

    public ArrayList getFuelHike(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList fuelHike = new ArrayList();

        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vendorId", vendorTO.getVendorId());
        map.put("contractId", vendorTO.getContractId());
        map.put("contractDedicateId", vendorTO.getContractDedicateId());

        System.out.println("vendorTO.getVendorId()= " + vendorTO.getVendorId());
        System.out.println("map = " + map);

        try {

            fuelHike = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getFuelHike", map);
            System.out.println("fuelHike.size()= " + fuelHike.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelHike Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFuelHike", sqlException);
        }
        return fuelHike;
    }

    public String checkVehicleRegNo(String vehicleRegNo) {
        Map map = new HashMap();
        String status = "";

        String mess = "";
        try {
            vehicleRegNo = vehicleRegNo.replace(" ", "");
            System.out.println("trimmed regno=" + vehicleRegNo);
            map.put("vehicleRegNo", vehicleRegNo);
            status = (String) getSqlMapClientTemplate().queryForObject("vendor.checkVehicleExists", map);
            System.out.println("22222 = " + status);
            if (status != null) {
                mess = status;
            }

            System.out.println("checkVehicleRegNo" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVehicleRegNo", sqlException);
        }

        return mess;
    }

    public String checkTrailerNo(String trailerNo) {
        Map map = new HashMap();
        String status = "";

        String mess = "";
        try {
            trailerNo = trailerNo.replace(" ", "");
            System.out.println("trimmed trailerNo=" + trailerNo);
            map.put("trailerNo", trailerNo);
            status = (String) getSqlMapClientTemplate().queryForObject("vendor.checkTrailerExists", map);
            System.out.println("22222 = " + status);
            if (status != null) {
                mess = status;
            }

            System.out.println("checkTrailerNo3333333" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTrailerNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkTrailerNo", sqlException);
        }

        return mess;
    }

    public int doVendorUpdateDetails(VendorTO vendorTO, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved) {

        Map map = new HashMap();
        FileInputStream fis = null;
        int tripId = 0;
        int vendorId = 0;
        int status = 0;
        /*
          * set the parameters in the map for sending to ORM
         */
        try {

            if (tripSheetId1 != null && tripSheetId1 != "") {
                tripId = Integer.parseInt(tripSheetId1);
                map.put("tripSheetId", tripId);
            }
            //            System.out.println("actualFilePath[0] = " + actualFilePath[0]);
            System.out.println("actualFilePath.length = " + actualFilePath.length);
            for (int x = 0; x < actualFilePath.length; x++) {

                map.put("fileName" + x, fileSaved[x]);
                System.out.println("fileName map = " + map);
                System.out.println("actualFilePath = " + actualFilePath[x]);
                File file = new File(actualFilePath[x]);
                System.out.println("file = " + file);
                fis = new FileInputStream(file);
                System.out.println("fis = " + fis);
                byte[] podFile = new byte[(int) file.length()];
                System.out.println("podFile = " + podFile);
                fis.read(podFile);
                fis.close();
                map.put("podFile" + x, podFile);
                System.out.println("podfile map = " + map);
                System.out.println("the saveTripPodDetails123455" + map);
            }

            map.put("userId", UserId);
            map.put("vendorName", vendorTO.getVendorName());
            map.put("tinNo", vendorTO.getTinNo());
            map.put("vendorTypeId", vendorTO.getVendorTypeId());
            map.put("vendorAddress", vendorTO.getVendorAddress());
            map.put("vendorPhoneNo", vendorTO.getVendorPhoneNo());
            map.put("vendorMailId", vendorTO.getVendorMailId());
            map.put("activeInd", vendorTO.getActiveInd());

            map.put("manufacturerId", vendorTO.getMfrIds());
            map.put("vendorId", vendorTO.getVendorId());

            map.put("contactName", vendorTO.getContactName());
            map.put("designation", vendorTO.getDesignation());
            map.put("emailId", vendorTO.getEmailId());
            map.put("teleNo", vendorTO.getTeleNo());
            map.put("mobileNo", vendorTO.getMobileNo());
            map.put("faxNo", vendorTO.getFaxNo());
            map.put("bankName", vendorTO.getBankName());
            map.put("branch", vendorTO.getBranch());
            map.put("branchCode", vendorTO.getBranchCode());
            map.put("ifscCode", vendorTO.getIfscCode());
            map.put("micrNo", vendorTO.getMicrNo());
            map.put("msmeId", vendorTO.getMsmeId());
            map.put("nstcId", vendorTO.getNstcId());
            map.put("gstId", vendorTO.getGstId());
            map.put("dgsdId", vendorTO.getDgsdId());
            map.put("eepcId", vendorTO.getEepcId());
            map.put("serViceTax", vendorTO.getSerViceTax());
            map.put("ecId", vendorTO.getEcId());
            map.put("exciseDuty", vendorTO.getExciseDuty());
            map.put("vatId", vendorTO.getVatId());
            map.put("cstId", vendorTO.getCstId());
            map.put("panNo", vendorTO.getPanNo());
            map.put("accntNo", vendorTO.getAccountNo());
            map.put("rocId", vendorTO.getRocId());
            map.put("stateId", vendorTO.getStateId());
            map.put("gstNo", vendorTO.getGstNo());

            int mfrstatus = 0;
            //   int vendorId = 0;
 System.out.println("podfile map = " + map);
            status = (Integer) getSqlMapClientTemplate().update("vendor.updateVendor", map);
            //    vendorId = (Integer) getSqlMapClientTemplate().queryForObject("vendor.lastUpdatedVendor", map);
            //            map.put("vendorId",vendorId);
            //             map.put("manufacturerId", vendorTO.getMfrIds());
            //            mfrstatus = (Integer) getSqlMapClientTemplate().update("vendor.insertMfr", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
              * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateVendor", sqlException);
        }
        return status;

    }

    public ArrayList getVendorNameDetails(VendorTO vendorTO) {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        map.put("vendorName", vendorTO.getVendorName() + "%");
        try {
            System.out.println("i m here 4");
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getVendorNameDetails", map);
            System.out.println("trailer sizeefsdf:" + vendorList.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityFromList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getCityFromList", sqlException);
        }
        return vendorList;
    }

    public int checkVendorNameExists(VendorTO vendorTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("vendorName", vendorTO.getVendorName());
            map.put("vendorTypeId", vendorTO.getVendorTypeId());
            status = (Integer) getSqlMapClientTemplate().queryForObject("vendor.checkVendorNameExists", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVendorNameExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVendorNameExists", sqlException);
        }

        return status;
    }

    public String getMarketHireRate(VendorTO vendorTO) {
        Map map = new HashMap();
        String marketHireRate = "";
        try {
            if (vendorTO.getOriginIdFullTruck().equals("")) {
                vendorTO.setOriginIdFullTruck("0");
            }
            map.put("originId", vendorTO.getOriginIdFullTruck());
            if (vendorTO.getPointId1().equals("")) {
                vendorTO.setPointId1("0");
            }
            map.put("pointId1", vendorTO.getPointId1());
            if (vendorTO.getPointId2().equals("")) {
                vendorTO.setPointId2("0");
            }
            map.put("pointId2", vendorTO.getPointId2());
            if (vendorTO.getPointId3().equals("")) {
                vendorTO.setPointId3("0");
            }
            map.put("pointId3", vendorTO.getPointId3());
            if (vendorTO.getPointId4().equals("")) {
                vendorTO.setPointId4("0");
            }
            map.put("pointId4", vendorTO.getPointId4());
            if (vendorTO.getDestinationIdFullTruck().equals("")) {
                vendorTO.setDestinationIdFullTruck("0");
            }
            map.put("destinationId", vendorTO.getDestinationIdFullTruck());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("loadTypeId", vendorTO.getLoadTypeId());
            map.put("containerTypeId", vendorTO.getContainerTypeId());
            map.put("containerQty", vendorTO.getContainerQty());
            System.out.println("map:" + map);
            marketHireRate = (String) getSqlMapClientTemplate().queryForObject("vendor.getMarketHireRate", map);
            System.out.println("marketHireRate = " + marketHireRate);
            if (marketHireRate == null) {
                marketHireRate = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMarketHireRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMarketHireRate", sqlException);
        }

        return marketHireRate;
    }

    public String getMarketHireRate(VendorTO vendorTO, SqlMapClient session) {
        Map map = new HashMap();
        String marketHireRate = "";
        try {
            if (vendorTO.getOriginIdFullTruck().equals("")) {
                vendorTO.setOriginIdFullTruck("0");
            }
            map.put("originId", vendorTO.getOriginIdFullTruck());
            if (vendorTO.getPointId1().equals("")) {
                vendorTO.setPointId1("0");
            }
            map.put("pointId1", vendorTO.getPointId1());
            if (vendorTO.getPointId2().equals("")) {
                vendorTO.setPointId2("0");
            }
            map.put("pointId2", vendorTO.getPointId2());
            if (vendorTO.getPointId3().equals("")) {
                vendorTO.setPointId3("0");
            }
            map.put("pointId3", vendorTO.getPointId3());
            if (vendorTO.getPointId4().equals("")) {
                vendorTO.setPointId4("0");
            }
            map.put("pointId4", vendorTO.getPointId4());
            if (vendorTO.getDestinationIdFullTruck().equals("")) {
                vendorTO.setDestinationIdFullTruck("0");
            }
            map.put("destinationId", vendorTO.getDestinationIdFullTruck());
            map.put("vehicleTypeId", vendorTO.getVehicleTypeId());
            map.put("containerTypeId", vendorTO.getContainerTypeId());
            map.put("loadTypeId", vendorTO.getLoadTypeId());
            map.put("containerQty", vendorTO.getContainerQty());
            System.out.println("map = " + map);
            marketHireRate = (String) session.queryForObject("vendor.getMarketHireRate", map);
            System.out.println("marketHireRate = " + marketHireRate);
            if (marketHireRate == null) {
                marketHireRate = "";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMarketHireRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMarketHireRate", sqlException);
        }

        return marketHireRate;
    }

    public ArrayList getReplaceVehicle(VendorTO vendorTO) {

        Map map = new HashMap();
        ArrayList getReplaceVehicle = new ArrayList();

        map.put("vehicleId", vendorTO.getVehicleId());
        try {
            System.out.println("i m here 4");
            getReplaceVehicle = (ArrayList) getSqlMapClientTemplate().queryForList("vendor.getReplaceVehicle", map);
            System.out.println("getReplaceVehicle:" + getReplaceVehicle.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReplaceVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "getReplaceVehicle", sqlException);
        }
        return getReplaceVehicle;
    }

    public int updateVendorApprovedStatus(int UserId, String contractHireId, String approvalStatus) {
        Map map = new HashMap();
        int updateStatus = 0;
        int status1 = 0;
        map.put("userId", UserId);
        map.put("contractHireId", contractHireId);
        map.put("approvalStatus", approvalStatus);
        try {
            System.out.println("map@@" + map);
            int status = getSqlMapClientTemplate().update("vendor.updateVendorApprovedStatus", map);
            System.out.println("approveCustomerContractRate" + status);
            if ("1".equals(approvalStatus) && status == 1) {
                status1 = 1;
            } else if ("3".equals(approvalStatus) && status == 1) {
                status1 = 3;
            }
            System.out.println("return values" + status1);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCustomerContract", sqlException);
        }

        return status1;
    }

}
