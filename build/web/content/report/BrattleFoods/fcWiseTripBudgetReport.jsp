<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

 <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">العربية</a>
	  </span>-->
        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.fcWiseTripSummary.action = '/throttle/handleTripBudgetReportExcel.do?param=ExportExcel';
                    document.fcWiseTripSummary.submit();
                } else {
                    document.fcWiseTripSummary.action = '/throttle/handleTripBudgetReportExcel.do?param=Search';
                    document.fcWiseTripSummary.submit();
                }
            }
        </script>
<style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>FC Wise Trip Performance Summary</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Finance</a></li>
                <li class="active">FC Wise Trip Performance Summary</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
    <body>
        <form name="fcWiseTripSummary" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp"%>
 <table class="table table-bordered" id="report" >
                        <tr height="30" id="index" >
                            <td colspan="4"  style="background-color:#5BC0DE;">
                                <b> <spring:message code="operations.reports.label.FCWiseTripPerformanceSummary" text="default text"/></b>
                            </td>
                        </tr>                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/>
</td>
                                        <td height="30"><input style="width:250px;height:40px;"   name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/>
</td>
                                        <td height="30"><input style="width:250px;height:40px;"   name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>
">
                                        <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>
"></td>
                                    </tr>

                                </table>
                    </td>
            </table>
                                     </div></div>

                     <br/>

            <c:if test="${FcWiseTripBudgetDetails != null}">
                  <table class="table table-info mb30 table-hover" id="table" class="sortable"  >

                    <thead>
                    <tr>
                    <th><spring:message code="operations.reports.label.SNo" text="default text"/>
 </th>
                    <th width="120"><spring:message code="operations.reports.label.FleetCenterName" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.Cost" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.TotalTrucks" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.TotalKms" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.TotalReferHrs" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.ReferCost" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.TotalCost" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.WFURefer(Approx)" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.cost/km" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.RCM" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.DeviationperTruck/KM" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.%Empty/Loaded(KM)" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.AvgKm" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.FoodingAdvance" text="default text"/>
</th>
                    <th><spring:message code="operations.reports.label.R&MAdvance" text="default text"/>
</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:set var="costPerKM" value="${0}"/>
                        <c:set var="averageKmEmpty" value="${0}"/>
                        <% int index = 1;%>
                        <c:forEach items="${FcWiseTripBudgetDetails}" var="BPCLTD">
                            <c:set var="referCost" value="${BPCLTD.totalHms * 160}"/>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="costPerKM" value="${BPCLTD.totalCost / BPCLTD.totalkms}"/>
                            </c:if>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="averageKmEmpty" value="${(BPCLTD.emptyTripKM/BPCLTD.totalkms)*100}"/>
                            </c:if>
                            <c:set var="averageKm" value="${BPCLTD.totalkms/BPCLTD.noOfVehicles}"/>
                            <c:set var="totalKmEmpty" value="${TotalKmEmpty+BPCLTD.emptyTripKM}"/>


                            <c:set var="totalReferCost" value="${totalReferCost + referCost }"/>
                            <c:set var="totalNoofVehicles" value="${totalNoofVehicles + BPCLTD.noOfVehicles }"/>
                            <c:set var="totalTotalKMs" value="${totalTotalKMs + BPCLTD.totalkms }"/>
                            <c:set var="totalTotalHMs" value="${totalTotalHMs + BPCLTD.totalHms }"/>
                            <c:set var="totalTotalCost" value="${totalTotalCost + BPCLTD.totalCost }"/>
                            <c:set var="totalWfuAmount" value="${totalWfuAmount + BPCLTD.wfuAmount }"/>
                            <c:set var="totalrcm" value="${totalrcm + BPCLTD.rcm }"/>
                            <c:set var="totalFoodingAdvance" value="${totalFoodingAdvance + BPCLTD.foodingAdvance }"/>
                            <c:set var="totalRnmAdvance" value="${totalRnmAdvance + BPCLTD.repairAdvance }"/>
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="text1"><%=index++%></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.companyName}"/></td>
                                <td  width="30" class="text1">&nbsp;</td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.noOfVehicles}"/></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.totalkms}"/></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.totalHms}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${referCost}"/></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.totalCost}"/></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.wfuAmount}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${costPerKM}"/></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.rcm}"/></td>
                                <td  width="30" class="text1">&nbsp;</td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${averageKmEmpty}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${averageKm}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.foodingAdvance}"/></td>
                                <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.repairAdvance}"/></td>

                            </tr>
                        </c:forEach>
                            <c:set var="totalCostPerKM" value="${0}"/>
                            <c:set var="totalAverageKmEmpty" value="${0}"/>
                            <c:set var="totalAverageKm" value="${totalTotalKMs /totalNoofVehicles}"/>
                            <c:if test="${totalTotalKMs > 0.0}">
                            <c:set var="totalCostPerKM" value="${totalTotalCost /totalTotalKMs}"/>
                            </c:if>
                            <c:if test="${totalTotalKMs > 0.0}">
                            <c:set var="totalAverageKmEmpty" value="${(TotalKmEmpty / totalTotalKMs)*100}"/>
                            </c:if>
                            <tr>
                                 <td  width="30" class="text1" colspan="3" align="center">&nbsp;Total</td>
                                 <td  width="30" class="text1"><c:out value="${totalNoofVehicles}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalTotalKMs}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalTotalHMs}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalReferCost}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalTotalCost}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalWfuAmount}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalCostPerKM}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalrcm}"/></td>
                                 <td  width="30" class="text1">&nbsp;</td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalAverageKmEmpty}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalAverageKm}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalFoodingAdvance}"/></td>
                                 <td  width="30" class="text1"><fmt:formatNumber pattern="##0.00" value="${totalRnmAdvance}"/></td>
                            </tr>
                    </tbody>
                </table>


            </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
