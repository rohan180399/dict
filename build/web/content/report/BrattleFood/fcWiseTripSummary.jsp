<%-- 
    Document   : FCWiseTripSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Arul
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                document.fcWiseTripSummary.action = '/throttle/handleFCWiseTripSummaryReport.do?param=ExportExcel';
                document.fcWiseTripSummary.submit();
                }else{
                document.fcWiseTripSummary.action = '/throttle/handleFCWiseTripSummaryReport.do?param=Search';
                document.fcWiseTripSummary.submit();
                }
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
            
            
        </script>
                    
                    <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">???????</a>
	  </span>-->
<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
    <style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.FCWiseTripSummary" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.FCWiseTripSummary" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValue();">
        <form name="fcWiseTripSummary" action=""  method="post">
<!--            <table class="table table-bordered" id="report" >
                        <tr height="30" id="index" >
                            <td colspan="4"  style="background-color:#5BC0DE;">
                        <b><spring:message code="operations.reports.label.FCWiseTripSummary" text="default text"/></b>
                            </td></tr>
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">-->
                                <table  class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="4"><spring:message code="operations.reports.label.FCWiseTripSummary" text="default text"/></th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.TripStartDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate"  autocomplete="off" style="width:260px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.TripEndDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" autocomplete="off" style="width:260px;height:40px;" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                                       <input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
<!--                            </div></div>
                    </td>
                </tr>
            </table>
                                    -->
            
            <c:if test = "${totalSummaryDetailsNWLoaded == null && totalSummaryDetailsNWEmpty == null}" >
                <center>
                <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>
                <div class="container">
                 <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
                </div>
            </div>
                </center>
            </c:if>
            <c:if test = "${totalSummaryDetailsNWLoaded != null && totalSummaryDetailsNWEmpty != null}" >
                <table class="table table-info mb30 table-hover sortable" id="table" >
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseOperated" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseIn-Progress" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseWFU" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseEnded" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseClosed" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseSettled" text="default text"/></th>
                            <th colspan="3"><c:out value="${fromDate}"/>&nbsp;<spring:message code="operations.reports.label.To" text="default text"/>&nbsp;<c:out value="${toDate}"/>&nbsp;<spring:message code="operations.reports.label.FCwiseExtraExpense" text="default text"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><spring:message code="operations.reports.label.FC" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Empty" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Loaded" text="default text"/></td>
                            <td><spring:message code="operations.reports.label.Total" text="default text"/></td>
                        </tr>
                        <tr>
                            <td>NW</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNWEmptyExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp}"/></td>
                        </tr>
                        <tr>
                            <td>NS</td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Gen</td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Tot</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNSEmpty+totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded+totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>
                            
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNSEmpty+endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded+endSummaryDetailsNSLoaded+endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded+endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded+endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNSEmpty+closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded+closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNSEmpty+settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded+settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>
                            
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNSEmpty+expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp+expSummaryDetailsNSLoaded+expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp+expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded+expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                        
                    </tbody>
                    
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
       
    </body>    
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
