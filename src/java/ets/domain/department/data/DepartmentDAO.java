/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.department.data;

/**
 *
 * @author vinoth
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.department.business.DepartmentTO;
import ets.domain.users.web.CryptoLibrary;

public class DepartmentDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "DepartmentDAO";

    public DepartmentDAO() {
    }

//    public int doInsertDepartment(DepartmentTO departmentTO, int userId) {
//        Map map = new HashMap();
//        int status = 0;
//        map.put("name", departmentTO.getName());
//        map.put("description", departmentTO.getDescription());
//        map.put("userId", userId);
//        //////System.out.println("departmentTO.getName()-->"+departmentTO.getName());
//        //////System.out.println("departmentTO.getDescription()-->"+departmentTO.getDescription());
//        //////System.out.println("departmentTO.userId()-->"+userId);
//        try {
//            //////System.out.println("d1");
//           status=(Integer) getSqlMapClientTemplate().update("department.insertdepartment", map);
//            //////System.out.println("status is" + status);
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("doInsertDepartment Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-GEN-01", CLASS, "doInsertDepartment", sqlException);
//        }
//        return status;
//    }

   public ArrayList getDepartmentList() {
        Map map = new HashMap();
        ArrayList departmentList = new ArrayList();
        try {
           departmentList = (ArrayList) getSqlMapClientTemplate().queryForList("department.getDepartmentList", map);
            System.out.println("department size=" + departmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDepartmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return departmentList;
    }
//     public int doUpdateDepartment(ArrayList List, int userId) {
//        Map map = new HashMap();
//        /*
//         * set the parameters in the map for sending to ORM
//         */
//
//        int status = 0;
//        try {
//            Iterator itr = List.iterator();
//            DepartmentTO departmentTO = null;
//            while (itr.hasNext()) {
//                departmentTO = (DepartmentTO) itr.next();
//                map.put("departmentId", departmentTO.getDepartmentId());
//                //////System.out.println("departmentTO.getDepartmentId()-->"+departmentTO.getDepartmentId());
//                map.put("name", departmentTO.getName());
//                //////System.out.println("departmentTO.getName()-->"+departmentTO.getName());
//                map.put("description", departmentTO.getDescription());
//                //////System.out.println("departmentTO.getDescription()-->"+departmentTO.getDescription());
//                map.put("status", departmentTO.getStatus());
//                System.out.print("status="+departmentTO.getStatus()); 
//                map.put("userId", userId);
//                //////System.out.println("updateDepartment map"+map);
//                status = (Integer) getSqlMapClientTemplate().update("department.updateDepartment", map);
//            }
//
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-SYS-01", CLASS,
//                    "getUserAuthorisedFunctions", sqlException);
//        }
//        return status;
//
//    }
      public int doInsertDepartment(DepartmentTO departmentTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("name", departmentTO.getName());
        map.put("description", departmentTO.getDescription());
        map.put("toMail", departmentTO.getToMail());
        map.put("ccMail", departmentTO.getCcMail());
        map.put("userId", userId);
        //////System.out.println("departmentTO.getName()-->"+departmentTO.getName());
        //////System.out.println("departmentTO.getDescription()-->"+departmentTO.getDescription());
        //////System.out.println("departmentTO.userId()-->"+userId);
        try {
            //////System.out.println("d1");
            status = (Integer) getSqlMapClientTemplate().update("department.insertdepartment", map);
            //////System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertDepartment Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "doInsertDepartment", sqlException);
        }
        return status;
    }




    public int doUpdateDepartment(ArrayList List, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            DepartmentTO departmentTO = null;
            while (itr.hasNext()) {
                departmentTO = (DepartmentTO) itr.next();
                map.put("departmentId", departmentTO.getDepartmentId());
                //////System.out.println("departmentTO.getDepartmentId()-->"+departmentTO.getDepartmentId());
                map.put("name", departmentTO.getName());
                //////System.out.println("departmentTO.getName()-->"+departmentTO.getName());
                map.put("description", departmentTO.getDescription());
                //////System.out.println("departmentTO.getDescription()-->"+departmentTO.getDescription());
                map.put("status", departmentTO.getStatus());
                map.put("toMail", departmentTO.getToMail());
                map.put("ccMail", departmentTO.getCcMail());
                System.out.print("status=" + departmentTO.getStatus());
                map.put("userId", userId);
                //////System.out.println("updateDepartment map"+map);
                status = (Integer) getSqlMapClientTemplate().update("department.updateDepartment", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

   
}