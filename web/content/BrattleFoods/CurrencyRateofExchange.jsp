<%--
    Document   : Currency Rate of Exchange
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan, Gulshan kumar(29/12/15)
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
//    ajax

  


    function submitPage()
    {
        var errStr = "";
//        var nameCheckStatus = $("#exchangeCurrencyRateStatus").text();
        if (document.getElementById("fromcurrency").value == "0") {
            errStr = "Please Select From Currency.\n";
            alert(errStr);
            document.getElementById("fromcurrency").focus();
        }
        else if (document.getElementById("tocurrency").value == "0")
        {
            errStr = "Please select to currncy Name.\n";
            alert(errStr);
            document.getElementById("tocurrency").focus();
        }
        else if (document.getElementById("fromcurrencyvalue").value == "")
        {
            errStr = "Please enter from currency value\n";
            alert(errStr);
            document.getElementById("fromcurrencyvalue").focus();
        } else if (document.getElementById("tocurrencyvalue").value == "")
        {
            errStr = "Please enter To currency value\n";
            alert(errStr);
            document.getElementById("tocurrencyvalue").focus();
        }
        else if (document.getElementById("fromdate").value == "")
        {
            errStr = "Please select from date\n";
            alert(errStr);
            document.getElementById("fromdate").focus();
        } else if (document.getElementById("todate").value == "")
        {
            errStr = "Please  To date\n";
            alert(errStr);
            document.getElementById("todate").focus();
        }

        if (errStr == "") {
            document.currencyExchange.action = "/throttle/saveCurrencyRateExchange.do";
            document.currencyExchange.method = "post";
            document.currencyExchange.submit();
        }
    }

    function setValues(sno,fromcurrencyId, tocurrencyId, fromcurrency, fromcurrencyvalue,tocurrency, tocurrencyvalue, fromdate, todate, status, currencyid) {

        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("fromcurrencyId").value = fromcurrencyId;
        document.getElementById("tocurrencyId").value = tocurrencyId;
        document.getElementById("fromcurrency").value = fromcurrency;
        document.getElementById("fromcurrencyvalue").value = fromcurrencyvalue;
        document.getElementById("tocurrency").value = tocurrency;
        document.getElementById("tocurrencyvalue").value = tocurrencyvalue;
        document.getElementById("fromdate").value = fromdate;
        document.getElementById("todate").value = todate;
        document.getElementById("status").value = status;
        document.getElementById("currencyId").value = currencyid;
    }

    
</script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            yearRange: '1900:' + new Date().getFullYear(),
            changeMonth: true, changeYear: true
        });
    });
    function hideSelect(str) {
        var selectobject = document.getElementById("tocurrency");
        for (var i = 0; i < selectobject.length; i++) {
            var c = selectobject.options[i].value;
            if (selectobject.options[i].value == str) {
                $("select[name='tocurrency'] option[value='" + str + "']").hide();
            }
            else {
                $("select[name='tocurrency'] option[value='" + c + "']").show();
            }
        }
    }
</script>

<script>
   $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#fromcurrency').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getFromCurrencyList.do",
                    dataType: "json",
                    data: {
                        fromcurrency: request.term,
                         tocurrencyId: document.getElementById('tocurrencyId').value
                    },
                    success: function (data, textStatus, jqXHR) {
                       var items = data;
                        if (items == '') {
                            $('#fromcurrencyId').val('');
                            $('#fromcurrency').val('');
                            $('#tocurrencyId').val('');
                            $('#tocurrency').val('');
                       } else {
                            response(items);
                        }
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#fromcurrencyId').val(tmp[0]);
                $('#fromcurrency').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });  
    
//    for to currency

        $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#tocurrency').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getToCurrencyList.do",
                    dataType: "json",
                    data: {
                        tocurrency: request.term,
                        fromcurrencyId: document.getElementById('fromcurrencyId').value
                    },
                    success: function (data, textStatus, jqXHR) {
                       var items = data;
                        if (items == '') {
                            $('#tocurrencyId').val('');
                            $('#tocurrency').val('');
                       } else {
                            response(items);
                        }
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#tocurrencyId').val(tmp[0]);
                $('#tocurrency').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });  



</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>

        <form name="currencyExchange"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="currencyId" id="currencyId" value=""  />
                <input type="hidden" name="fromcurrencyId" id="fromcurrencyId" <c:out value="${fromcurrencyId}"/> class="textbox" />
                <input type="hidden" name="tocurrencyId" id="tocurrencyId" <c:out value="${tocurrencyId}"/> class="textbox" />
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contenthead" colspan="8" >Currency Rate of Exchange</td>
                    </tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="exchangeCurrencyRateStatus" style="color: red"></label></td>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>From Currency</td>
                        <td class="text1"><input type="text" name="fromcurrency" id="fromcurrency" class="textbox" maxlength="50" <c:out value="${fromcurrency}"/> /></td>

                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>From Currency Value</td>
                        <td class="text1"><input type="text" name="fromcurrencyvalue" id="fromcurrencyvalue" class="textbox" autocomplete="off"/></td>
                    </tr>

                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Currency &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="text1"><input type="text" name="tocurrency" id="tocurrency" class="textbox" maxlength="50" <c:out value="${tocurrency}"/> /></td>

                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Currency &nbsp;&nbsp;&nbsp;&nbsp;Value</td>
                        <td class="text2"><input type="text" name="tocurrencyvalue" id="tocurrencyvalue" class="textbox" autocomplete="off" /></td>

                    </tr>
                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>From Date&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="text2"><input type="text" name="fromdate" id="fromdate" class="datepicker" /></td>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Date &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="text2"><input type="text" name="todate" id="todate" class="datepicker" /></td>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td class="text1">
                            <select  align="center" class="textbox" name="status" id="status" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" style="display: none">In-Active</option>
                            </select>
                        </td>
                    </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>


            <h2 align="center">List Of Rate of Currency</h2>


            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th width="120"><h3>From Currency</h3></th>
                        <th width="150"><h3>From Currency Value</h3></th>
                        <th><h3>To Currency </h3></th>
                        <th width="150"><h3>To Currency Value</h3></th>
                        <th><h3>From Date</h3></th>
                        <th><h3>To Date</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${currencyList != null}">
                        <c:forEach items="${currencyList}" var="rate">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.fromcurrency}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.fromcurrencyvalue}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.tocurrency}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.tocurrencyvalue}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.fromdate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.todate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${rate.status}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${rate.fromcurrencyId}" />',  '<c:out value="${rate.tocurrencyId}" />','<c:out value="${rate.fromcurrency}" />', '<c:out value="${rate.fromcurrencyvalue}" />', '<c:out value="${rate.tocurrency}" />', '<c:out value="${rate.tocurrencyvalue}" />', '<c:out value="${rate.fromdate}" />', '<c:out value="${rate.todate}" />', '<c:out value="${rate.status}" />', '<c:out value="${rate.currencyid}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>