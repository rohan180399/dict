

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="java.util.*" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->


        <script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>
        <script type="text/javascript">
            function submitPage() {
                var customerId = document.enter.Customer.value;
                if (customerId != 0) {
                document.enter.action = '/throttle/viewclosedLeasedtrip.do';
                document.enter.submit();
                }else{
                  alert('please choose the customer');
                }
            }
            function generateBill() {
                var temp="";
                var cntr = 0;
                var selectedConsignment = document.getElementsByName('consignmentId');
                  for (var i = 0; i < selectedConsignment.length; i++) {
               if (selectedConsignment[i].checked == true) {
                   if (cntr == 0) {
                       temp = selectedConsignment[i].value;
                   } else {
                       temp = temp + "," + selectedConsignment[i].value;
                   }
                   cntr++;
               }
        }
                alert("temp"+temp);
                document.enter.action = '/throttle/gererateLeasedTripBill.do?tripIds='+temp;
                document.enter.submit();
            }
//            function generateBill() {
//                document.enter.action = '/throttle/gererateTripBill.do';
//                document.enter.submit();
//            }
            function selectCheckCondition() {
                if (document.getElementById("select").checked = true) {
                    document.getElementById("select").value = "selected";
                } else {
                    document.getElementById("select").value = "";
                }



            }

             function setBillingOrder(sno,obj,value) {
                    var cnoteId = "";
                    if(obj.checked == true){

                    document.getElementById("tripIdStatus"+sno).value = value;
                    document.getElementById("tripSheetId").value = value;
                    }else{
                    document.getElementById("tripIdStatus"+sno).value = 0;
                    document.getElementById("tripSheetId").value = 0;
                    }
                }
        </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  <spring:message code="billing.label.BillGeneration"  text="default text"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="billing.label.Billing"  text="default text"/></a></li>
            <li class="active"><spring:message code="billing.label.BillGeneration"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
    <body>
        <form name="enter" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <!--<table class="table table-info mb30 table-hover" id="report" >-->
                <!--<tr id="exp_table" >-->
                            
                          
                                <table class="table table-info mb30 table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="4"><spring:message code="billing.label.BillGeneration"  text="default text"/></th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="billing.label.Customer"  text="default text"/></td>
                                        <td height="30">
                                            <select class="form-control" style="width:260px;height:40px;" name="Customer" id="Customer"  style="width:125px;"><option value="0">-<spring:message code="billing.label.Select"  text="default text"/>-</option>
                                                <c:forEach items="${customerList}" var="customerList">
                                                    <option value='<c:out value="${customerList.custName}"/>'><c:out value="${customerList.custName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                        <td><spring:message code="billing.label.ConsignmentOrderNo"  text="default text"/></td>
                                        <td height="30"><input name="orderNo" id="orderNo" type="text" class="form-control" style="width:260px;height:40px;"  ></td>

                                    </tr>
                                    <tr>
                                        <td><spring:message code="billing.label.CustomerOrderNo"  text="default text"/></td>
                                        <td height="30"><input name="customerOrderNo" id="customerOrderNo" type="text" class="form-control" style="width:260px;height:40px;"  ></td>
                                        <td><spring:message code="billing.label.TripCode"  text="default text"/></td>
                                        <td height="30"><input name="tripNo" id="tripNo" type="text" class="form-control" style="width:260px;height:40px;"  ></td>
                                    <tr>
                                    </tr>
                                    <td><font color="red">*</font><spring:message code="billing.label.FromDate"  text="default text"/></td>
                                    <td height="30"><input name="fromDate" id="fromDate" autocomplete="off" type="text" style="width:260px;height:40px;" class="datepicker form-control"  ></td>
                                    <td><font color="red">*</font><spring:message code="billing.label.ToDate"  text="default text"/></td>
                                    <td height="30"><input name="toDate" id="toDate"autocomplete="off" type="text" style="width:260px;height:40px;" class="datepicker form-control" ></td>
                                    </tr>
                                    <tr align="center">
                                      
                                        <td colspan="4"><center><input type="button" class="btn btn-success"   value="<spring:message code="billing.label.FETCHDATA"  text="default text"/>"  onclick="submitPage();"></center></td>
           
                                    </tr>
                                </table>
                            
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <%
            if((ArrayList)request.getAttribute("closedTrips")!=null){
                String fromDate=(String)request.getAttribute("fromDate");
                String toDate=(String)request.getAttribute("toDate");
            ArrayList list=(ArrayList)request.getAttribute("closedTrips");
            %>
            <%if(list.size()>0){%>
            <table class="table table-info mb30 table-hover sortable" id="table" >
                 <thead>
                    <tr height="80">
                    <th><spring:message code="billing.label.Sno"  text="default text"/></th>
                    <th><spring:message code="billing.label.Customer"  text="default text"/></th>
                    <th><spring:message code="billing.label.C-Notes"  text="default text"/></th>
                    <th><spring:message code="billing.label.TripCode"  text="default text"/></th>
                    <th><spring:message code="billing.label.VehicleNo"  text="default text"/></th>
<!--                    <th>POD Status</th>-->
<!--                    <th>Route Name</th>-->
                    <th><spring:message code="billing.label.VehicleType"  text="default text"/></th>                        
                    <th><spring:message code="billing.label.TripStartDate"  text="default text"/></th>
                    <th><spring:message code="billing.label.TripEndDate"  text="default text"/></th>
                    <th><spring:message code="billing.label.TripStartKm"  text="default text"/></th>
                    <th><spring:message code="billing.label.TripEndKm"  text="default text"/></th>
<!--                    <th>Tonnage</th>-->
                    <th><spring:message code="billing.label.Revenue"  text="default text"/></th>
                    <th><spring:message code="billing.label.Status"  text="default text"/></th>
                    <%--11-12-2013--%>
                    <th><spring:message code="billing.label.Action"  text="default text"/></th>
                    <%--11-12-2013--%>
                    <th><spring:message code="billing.label.Select"  text="default text"/></th>
                 </tr>
            </thead>
            <tbody>
                <%int index=0;
                int sno=1;
                String classText ="";
                %>              
                <%
                     String customerName="";
                     String cnoteNo="";
                     String tripId="";
                     String tripCode="";
                     String regNo="";
                     String vehicleType="";
                     String routeName="";
                     String vehicleTypeName="";
                     String tripStartDate="";
                     String tripEndDate="";
                     String tripstartkm="";
                     String tripendkm="";
                     String totalWeight="";
                     String destinationName="";
                     String freightAmt="";
                     String billingType="";
                     String customerId="";
                     String cnoteId="";
                     String checkTripId="";
                     String totalkmrun="";
                     String podStatus="";
                     String reeferRequired="";
                     String vehicleId="";

                     for(int i=0;i<list.size();i++){
                    
                     int oddEven = index % 2;
                      if (oddEven > 0) {
                                 classText = "text2";
                             } else {
                                 classText = "text1";
                             }
                         OperationTO opto=(OperationTO)list.get(i);
                         customerName=opto.getCustName();
                         cnoteNo=opto.getConsignmentNoteNo();
                         tripId=opto.getTripId();
                         tripCode=opto.getTripCode();
                         checkTripId=opto.getTripId();
                         routeName=opto.getRouteName();
                         regNo=opto.getRegno();
                         vehicleTypeName=opto.getVehicleTypeName();
                         tripStartDate=opto.getTripStartDate();
                         tripEndDate=opto.getEndDate();
                         tripstartkm=opto.getTripstartkm();
                         tripendkm=opto.getTripendkm();
                         totalWeight=opto.getTotalWeightage();
                         destinationName=opto.getConsigmentDestination();
                         freightAmt=opto.getFreightcharges();
                         billingType=opto.getBillingTypeId();
                         customerId=opto.getCustomerId();
                         totalkmrun=opto.getTotalkmrun();                                               
                         podStatus=opto.getStatus();                                               
                         reeferRequired=opto.getTempReeferRequired();                                               
                        
                %>
                
                <tr>
                    <td  height="30"><%=sno%></td>
                    <td  height="30" ><%=customerName%></td>
                    <td  height="30" style="width:120px"><%=cnoteNo%></td>
                    <td  height="30" style="width:120px"><%=tripCode%></td>
                    <td  height="30" style="width:120px"><%=regNo%></td>
            <%--        <td  height="30" >
                       
                        <% if("0".equals(podStatus)){%>
                         <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                        <% } else if("1".equals(podStatus)){%>
                        <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                        <% }%>
                    </td>  
                    <td  height="30" style="width:120px"><%=routeName%></td> --%>
                    <td  height="30" style="width:120px"><%=vehicleTypeName%></td>
                    <td  height="30" style="width:120px"><%=tripStartDate%></td>
                    <td  height="30" style="width:120px"><%=tripEndDate%></td>
                    <td  height="30" style="width:120px;text-align: right"><%=tripstartkm%></td>
                    <td  height="30" style="width:120px;text-align: right"><%=tripendkm%></td>
<!--                    <td  height="30" style="width:120px;text-align: right"><%=totalWeight%></td>-->
                    <td  height="30" style="width:120px;text-align: right"><%=freightAmt%></td>
                    <td  height="30"><img src="/throttle/images/icon_closed.png" alt=""/></td>
                        <%--11-12-2013--%>
                    <td  height="30"><spring:message code="billing.label.Settled"  text="default text"/></td>
                    <%--11-12-2013--%>
                    <td  height="30">
<!--                        <a href="/throttle/gererateTripBill.do?tripId=<%=tripId%>&tripSheetId=<%=tripId%>">generate bill</a>-->
                        <input type="checkbox" name="consignmentId" id="consignmentId<%=sno%>" value="<%=tripId%>" onclick="setBillingOrder('<%=sno%>',this,this.value)"/>
                        <input type="hidden" name="customerName<%=sno%>" value="<%=customerName%>"/>
                        <input type="hidden" name="customerId<%=sno%>" value="<%=customerId%>"/>
                        <input type="hidden" name="cnoteNo<%=sno%>" value="<%=cnoteNo%>"/>
                        <input type="hidden" name="destinationName<%=sno%>" value="<%=destinationName%>"/>
                        <input type="hidden" name="freightAmt<%=sno%>" value="<%=freightAmt%>"/>
                        <input type="hidden" name="billingType<%=sno%>" value="<%=billingType%>"/>
                        <input type="hidden" name="customerId<%=sno%>" value="<%=customerId%>"/>
                        <input type="hidden" name="totalweights<%=sno%>" value="<%=totalWeight%>"/>
                        <input type="hidden" name="tripDate<%=sno%>" value="<%=tripStartDate%>"/>
                        <input type="hidden" name="tripId" value="<%=tripId%>"/>
                        <input type="hidden" name="cnoteId<%=sno%>" value="<%=tripId%>"/>
                        <input type="hidden" name="routeName<%=sno%>" value="<%=routeName%>"/>
                        <input type="hidden" name="rowNo<%=sno%>" value="<%=sno%>"/>
                        <input type="hidden" name="totalkmrun<%=sno%>" value="<%=totalkmrun%>"/>
                         <input type="hidden" name="consignmentOrderIdStatus" id="consignmentOrderIdStatus<%=sno%>" value="0" />
                        <input type="hidden" name="tripIdStatus" id="tripIdStatus<%=sno%>" value="0" />
                    </td>
               
                </tr>
                <%
                 cnoteId="500000";
                sno++;
                index++;
                %>
                <%
   


            }%>                

            </tbody>
            </table>
            
            <input type="hidden" name="invoicefromDate" value="<%=fromDate%>"/>
            <input type="hidden" name="invoicetoDate" value="<%=toDate%>"/>
                      <br>
                    <center>
                        <input type="button" class="button" value="<spring:message code="billing.label.GenerateBill"  text="default text"/>" name="Generate Bill" onClick="generateBill();">
                    </center>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="billing.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="billing.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="billing.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <%}}%>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>  
        </div>
        </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>