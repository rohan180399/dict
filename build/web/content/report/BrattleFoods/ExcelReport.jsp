<%-- 
    Document   : ExcelReport
    Created on : 18 Oct, 2022, 11:07:38 AM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
            
        </style>
    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "CustomerWiseMasterLogReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            <c:if test = "${cutomerMasterLogDetails != null}" >
               <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                           <th><h3>S.No</h3></th>
                            <th ><h3>Customer Name</h3></th>
                            <th><h3>Contact Person</h3></th>
                            <th><h3>Customer Address</h3></th>
                            <th><h3>City</h3></th>
                            <th><h3>State</h3></th>
                            <th><h3>Pincode</h3></th>
                            <th><h3>Mobile</h3></th>
                            <th><h3>Phone</h3></th>
                            <th><h3>Email</h3></th>
                            <th><h3>Ledger Id</h3></th>
                            <th><h3>Ledger Code</h3></th>
                            <th><h3>Active Ind</h3></th>
                            <th><h3>Credit Limit</h3></th>
                            <th><h3>Credit Days</h3></th>
                            <th><h3>Customer Billing Name Address</h3></th>
                            <th><h3>Erp Id</h3></th>
                            <th><h3>Pan No</h3></th>
                            <th><h3>GST No</h3></th>
                            <th><h3>Organization Name</h3></th>
                            <th><h3>Company Type</h3></th>
                            <th><h3>Created By</h3></th>
                            <th><h3>Modified By</h3></th>
                            <th><h3>Created On</h3></th>
                            <th><h3>Modified On</h3></th>
                           
                        </tr>
                    </thead>
                    <tbody>
                       <% int index = 0;%>
                        <c:forEach items="${cutomerMasterLogDetails}" var="PL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr height="30">
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.contactPerson}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.custAddress}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.custCity}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.state}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.pincode}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterMobile}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterPhone}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.email}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.ledgerId}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.ledgerCode}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterActiveInd}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterCreditLimit}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.creditDays}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerBillingNameAddress}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.erpId}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.custPanNo}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerMasterGstNo}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.organizationName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.companyType}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.createdBy}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.modifiedBy}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.createdOn}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.modifiedOn}"/> </td>
                                
                            </tr>
                          <% index++;%>
                        </c:forEach>
                </tbody>
            </table>
                         </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>