<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>

    </head>
    <!--setImages(1,0,0,0,0,0);-->


     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Vehicle"  text="Vehicle"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">

    <body onLoad="getVehicleNos();
            setImages(1, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
           
            <%@ include file="/content/common/message.jsp" %>


           <td colspan="4"  style="background-color:#5BC0DE;">
            <table class="table table-info mb30 table-hover">
                <thead>
                <tr>
                    <th colspan="6"><spring:message code="trucks.label.Vehicle"  text="default text"/></th>
                    </tr>
                </thead>
                                    <tr>
                                        <td><spring:message code="trucks.label.VehicleNumber"  text="default text"/></td><td><input type="text" id="regno" name="regNo" value="" class="form-control" ></td>
                                        <td><spring:message code="trucks.label.MFR"  text="default text"/></td><td><select class="form-control" style="width:260px;height:40px;" name="mfrId" >
                                                <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                                <c:if test = "${MfrList != null}" >
                                                    <c:forEach items="${MfrList}" var="Dept">
                                                        <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                        <td><spring:message code="trucks.label.VehicleType"  text="default text"/></td><td><select class="form-control" style="width:260px;height:40px;" name="typeId" >
                                                <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                                <c:if test = "${TypeList != null}" >
                                                    <c:forEach items="${TypeList}" var="Type">
                                                        <option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                    </tr>
                                    <tr style="display:none;" >
                                        <td><spring:message code="trucks.label.UsageType"  text="default text"/></td><td><select class="form-control" style="width:260px;height:40px;" name="usageId" >
                                                <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                                <c:if test = "${usageList != null}" >
                                                    <c:forEach items="${usageList}" var="usage">
                                                        <option value='<c:out value="${usage.usageId}" />'><c:out value="${usage.usageName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>
                                        </td>



                                        <td><spring:message code="trucks.label.VehicleGroup"  text="default text"/></td><td><select class="form-control" name="groupId"  style="width:125px;">
                                                <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                                <c:if test = "${groupList != null}" >
                                                    <c:forEach items="${groupList}" var="grp">
                                                        <option value='<c:out value="${grp.groupId}" />'><c:out value="${grp.groupName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>

                                        </td>
                                        <td class="text1" height="30"><spring:message code="trucks.label.Ownership"  text="default text"/></td>
                                        <td class="text1">
                                            <select class="form-control" name="ownership" id="ownership"  style="width:125px;">
                                                <option value="" checked>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                <option value="1" checked><spring:message code="trucks.label.Own"  text="default text"/></option>
                                                <option value="2" checked><spring:message code="trucks.label.Leasing"  text="default text"/></option>
                                               <%-- <c:if test = "${leasingCustList != null}" >
                                                    <c:forEach items="${leasingCustList}" var="LCList">
                                                        <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                    </c:forEach >
                                                </c:if> --%>
                                            </select>
                                            <script>
                                                document.getElementById("ownership").value = '<c:out value="${ownership}"/>';
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center">
                                        
                                            <input type="button" class="btn btn-success" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                                        <input type="button" class="btn btn-success" name="Search" value="<spring:message code="trucks.label.Search"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                                        <input type="button" class="btn btn-success" name="ExportExcel"  value="<spring:message code="trucks.label.ExportExcel"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"></td>
                                    </tr>
                                </table>
           </td>
                            

            <%
                        int index = 1;

            %>


            <c:if test="${vehicleList == null }" >
                <br>
                <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
            </c:if>
            <c:if test="${vehicleList != null }" >
<!--                <table align="center" border="0" id="table" class="sortable" style="width:auto">-->
                    <table class="table table-info mb30 table-hover" id="table" >
                    <!--<table class="table table-info mb30 table-hover sortable" id="table" >-->
                    <thead>
                        <tr>
                            <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                            <th><spring:message code="trucks.label.VehicleNumber"  text="default text"/></th>
                            <th><spring:message code="trucks.label.DoorNo"  text="default text"/></th>
                            <th><spring:message code="trucks.label.VehicleType"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Make"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Model"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Ownership"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Action"  text="default text"/></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <%
                                String style = "text1";%>
                    <c:forEach items="${vehicleList}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td > <%= index++%> </td>
                            <td > <c:out value="${veh.regNo}" /> </td>
                            <td > <c:out value="${veh.doorNo}" /> </td>
                            <td ><c:out value="${veh.typeName}" /></td>
                            <td ><c:out value="${veh.vehicleMake}" /></td>
                            <td ><c:out value="${veh.modelName}" /></td>
                            <td ><c:out value="${veh.ownership}" /></td>
                            <td >
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=1&fleetTypeId=1'  ><span class="label label-warning"><spring:message code="trucks.label.Edit"  text="default text"/> </span> </a>
                            <c:if test="${veh.ownership == 'own'}">
                                &nbsp;
                            </c:if>
                                &nbsp;
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&editStatus=0&fleetTypeId=1'  ><span class="label label-info"><spring:message code="trucks.label.View"  text="default text"/> </span> </a>
                                
                            </td>
                            
                        </tr>
                    </c:forEach>
                        </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="trucks.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?fleetTypeId=1';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }


        function getVehicleNos() {
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        }

    </script>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>