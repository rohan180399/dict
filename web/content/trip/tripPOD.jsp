

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>


<script type="text/javascript" language="javascript">
    function submitPage() {
        var podFiles = document.getElementsByName("podFile");
        var statusCheck = true;
        for (var i = 0; i < podFiles.length; i++) {
            if (podFiles[i].value == '' || podFiles[i].value == null) {
                alert('upload the pod attachment..')
                statusCheck = false;
            }
        }

        if (statusCheck) {
            document.tripPod.action = '/throttle/saveTripPodDetails.do';
            document.tripPod.submit();
        }
    }

    function popUp(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        if (http.status != 404) {
            popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
        else {
            var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
    }

</script>
<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }

    #tabHead th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li data-toggle="tab"><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li data-toggle="tab"><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">Edit Start Trip Sheet</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="addRow();">

                <form name="tripPod" method="post" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>


                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td>  <font color="white"><b><c:out value="${trip.orderRevenue}" /></b></font></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td>  <font color="white"><b><c:out value="${trip.orderExpense}" /></b></font></td>

                                                    </tr>
                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td>  <font color="white"><b> <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                        <td>

                                                        <td>
                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>


                    <br>
                    <br>
                    <br>
                    <br>
                    <table width="100%">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if(loopCntr == 0) {%>
                                <tr id="index" height="28">
                                    <td  >Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    <td  >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                                    <td  >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                    <td  >Status: <c:out value="${trip.status}"/>
                                        <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>"/>
                                        <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>"/>
                                    </td>
                                </tr>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <div id="tabs" >
                        <ul class="nav nav-tabs">

                            <li class="active" data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                            <li data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                            <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                            <li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>
                            <!--                    <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                            <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                            <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                            <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                            <!--
                            <li data-toggle="tab"><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                            <!--                    <li data-toggle="tab"><a href="#advDetail"><span>Advance</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#expDetail"><span>Expense Details</span></a></li>-->
                            <!--                    <li data-toggle="tab"><a href="#summary"><span>Remarks</span></a></li>-->
                        </ul>

                        <div id="tripDetail">
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr id="index" height="30px">
                                    <td  colspan="6" >Trip Details</td>
                                </tr>

                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">


                                        <tr height="30">
                                            <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                        <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                            <td class="text1">Consignment No(s)</td>
                                            <td class="text1">
                                                <c:out value="${trip.cNotes}" />
                                            </td>
                                            <td class="text1">Billing Type</td>
                                            <td class="text1">
                                                <c:out value="${trip.billingType}" />
                                            </td>
                                        </tr>
                                        <tr height="30">
                                            <!--                            <td class="text2">Customer Code</td>
                                                                        <td class="text2">BF00001</td>-->
                                            <td class="text2">Customer Name</td>
                                            <td class="text2">
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                            </td>
                                            <td class="text2">Customer Type</td>
                                            <td class="text2" colspan="3" >
                                                <c:out value="${trip.customerType}" />
                                            </td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text1">Route Name</td>
                                            <td class="text1">
                                                <c:out value="${trip.routeInfo}" />
                                            </td>
                                            <!--                            <td class="text1">Route Code</td>
                                                                        <td class="text1" >DL001</td>-->
                                            <td class="text1">Reefer Required</td>
                                            <td class="text1" >
                                                <c:out value="${trip.reeferRequired}" />
                                            </td>
                                            <td class="text1">Order Est Weight (MT)</td>
                                            <td class="text1" >
                                                <c:out value="${trip.totalWeight}" />
                                            </td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text2">Vehicle Type</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTypeName}" />
                                            </td>
                                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleNo}" />

                                            </td>
                                            <td class="text2">Vehicle Capacity (MT)</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTonnage}" />

                                            </td>
                                        </tr>

                                        <tr height="30">
                                            <td class="text1">Veh. Cap [Util%]</td>
                                            <td class="text1">
                                                <c:out value="${trip.vehicleCapUtil}" />
                                            </td>
                                            <td class="text1">Special Instruction</td>
                                            <td class="text1">-</td>
                                            <td class="text1">Trip Schedule</td>
                                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                                        </tr>


                                        <tr height="30">
                                            <td class="text2"><font color="red">*</font>Driver </td>
                                            <td class="text2" colspan="5" >
                                                <c:out value="${trip.driverName}" />
                                            </td>

                                        </tr>
                                        <tr height="30">
                                            <td class="text1">Product Info </td>
                                            <td class="text1" colspan="5" >
                                                <c:out value="${trip.productInfo}" />
                                            </td>

                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>
                            <br/>
                            <br/>

                            <c:if test = "${expiryDateDetails != null}" >
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30px">
                                        <td colspan="4" >Vehicle Compliance Check</td>
                                    </tr>
                                    <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                        <tr  height="30px">
                                            <td class="text2">Vehicle FC Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr  height="30px">
                                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr  height="30px">
                                            <td class="text2">Vehicle Permit Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr  height="30px">
                                            <td class="text2">Road Tax Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                        </tr>
                                    </c:forEach>
                                </table>

                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <div id="podDetail" class="tab-pane active">
                            <c:if test="${viewPODDetails != null}">
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="tabHead" height="30px">
                                        <th width="50">S No&nbsp;</th>
                                        <th>City Name</th>
                                        <th>POD file Name</th>
                                        <th>GR Number</th>
                                        <th>POD Remarks</th>
                                    </tr>
                                    <% int index2 = 1;%>
                                    <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                        <%
                                           String classText3 = "";
                                           int oddEven = index2 % 2;
                                           if (oddEven > 0) {
                                               classText3 = "text1";
                                           } else {
                                               classText3 = "text2";
                                           }
                                        %>
                                        <tr height="30px">
                                            <td class="<%=classText3%>" ><%=index2++%></td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                            <td class="<%=classText3%>" >
                                                <a onclick="viewPODFiles('<c:out value="${viewPODDetails.tripPodId}"/>')" href="#"><c:out value="${viewPODDetails.podFile}"/></a>
                                            </td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                        </tr>
                                    </c:forEach>

                                </table>
                            </c:if>
                            <br/>
                            <script>
                                function viewPODFiles(tripPodId) {
                                    window.open('/throttle/content/trip/displayBlobData.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                                }
                            </script>
                            <c:if test="${podDetails != null}">
                                <table class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30px">
                                        <td colspan="6" >Consignor and  Consignee Name</td>
                                    </tr>

                                    <c:set var="count" value="1" scope="page" />
                                    <c:forEach items="${podDetails}" var="podDetails">
                                        <c:if test="${count == 1}">
                                            <tr height="30">
                                                <td class="text1" >Consignor Name</td>
                                                <td class="text1" ><input type='text' name='consignorName' class="textbox" id="consignorName" style="width:150px;height:22px;" value="<c:out value="${podDetails.consignorName}"/>" /></td>
                                                <td class="text1" >Consignor Mobile No</td>
                                                <td class="text1" ><input type='text' name='consignorMobileNo' class="textbox" id="consignorMobileNo" style="width:150px;height:22px;" maxlength="10" value="<c:out value="${podDetails.consignorMobileNo}"/>" /></td>
                                                <td class="text1" >Consignor Address</td>
                                                <td class="text1" ><input type='text' name='consignorAddress' class="textbox" id="consignorAddress" style="width:150px;height:22px;"  value="<c:out value="${podDetails.consignorAddress}"/>" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td class="text2" >Consignee Name</td>
                                                <td class="text2" ><input type='text' name='consigneeName' class="textbox"  id="consigneeName" style="width:150px;height:22px;" value="<c:out value="${podDetails.consigneeName}"/>" />
                                                <td class="text2" >Consignee Mobile No</td>
                                                <td class="text2" ><input type='text' name='consigneeMobileNo' class="textbox"  id="consigneeMobileNo" style="width:150px;height:22px;"  maxlength="10" value="<c:out value="${podDetails.consigneeMobileNo}"/>" />
                                                <td class="text2" >Consignee Address</td>
                                                <td class="text2" ><input type='text' name='consigneeAddress' class="textbox"  id="consigneeAddress" style="width:150px;height:22px;"  value="<c:out value="${podDetails.consigneeAddress}"/>" />
                                                    <input type="hidden" name="consignmentNote" id="consignmentNote" value="<c:out value="${podDetails.consignmentNote}"/>"/></td>
                                            </tr>
                                            <c:set var="count" value="${count + 1}" scope="page" />
                                        </c:if>
                                    </c:forEach>
                                </table>
                            </c:if>
                            <br>
                            <br>
                            <table class="border" border="1" align="center" width="100%" cellpadding="0" cellspacing="0" id="POD1">
                                <tr id="rowId0" style="background-color:#5BC0DE;width:100%;color:white;font-size: 14px;font-weight: bold;" height="30px;">
                                    <td  align="center" height="30" >Sno</td>
                                    <td  height="30" align="center">Location </td>
                                    <td  height="30" align="center">Attachment</td>
                                    <td  height="30" align="center">GR Number </td>
                                    <td  height="30" align="center">POD Remarks</td>
                                    <td height="30" align="center"></td>
                                </tr>
                                <% int index = 1;%>
                                <!--                        <tr height="30">
                                                            <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                                            <td>      <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                
                                                        </tr>-->
                                <tr height="30">
                                    <td colspan="6" align="center">
                                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                        <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                        <input type="button" class="btn btn-success" name="Save" value="Save" onclick="submitPage();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                        <input type="button" class="btn btn-success" name="add" value="Add" onclick="addRow();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                    </td>
                                </tr>
                                <script>
                                    var podRowCount1 = 1;
                                    function addRow() {
                                        var podSno1 = document.getElementById('selectedRowCount').value;
                                        var tab = document.getElementById("POD1");
                                        var newrow = tab.insertRow(podSno1);

                                        var newrow = tab.insertRow(podSno1);
                                        //                alert(newrow.id);
                                        newrow.id = 'rowId' + podSno1;

                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25'><select class='textbox' id='cityId" + podSno1 + "' style='width:150px;height:22px'  name='cityId'><option selected value=0>---Select---</option> <c:if test="${podDetails != null}" ><c:forEach items="${podDetails}" var="podDetails1"><option  value='<c:out value="${podDetails1.podPointId}" />'><c:out value="${podDetails1.cityName}" /> </c:forEach > </c:if> </select></td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='textbox' value='' onchange='checkName(" + podSno1 + ");' ><br><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(3);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' class='textbox'  style='width:150px;height:22px'  name='lrNumber'  id='lrNumber" + podSno1 + "'   value='<%=request.getParameter("tripSheetId")%>' ></td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(4);
                                        cell0 = "<td class='text1' height='25' ><textarea rows='3' cols='30' class='textbox' name='podRemarks' id='podRemarks" + podSno1 + "' style='width:150px;'></textarea></td>";
                                        cell.innerHTML = cell0;

                                        if (podSno1 == 1) {
                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' ></td>";
                                            cell.innerHTML = cell0;
                                        } else {
                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-success'  style='width:100px;height:30px;font-weight: bold;padding:1px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(" + podSno1 + ");' ></td>";
                                            cell.innerHTML = cell0;
                                        }


                                        podSno1++;
                                        if (podSno1 > 0) {
                                            document.getElementById('selectedRowCount').value = podSno1;
                                        }

                                    }


                                </script>

                                <%index++;%>
                            </table>

                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>


                        <script type="text/javascript">
                            function deleteRow(sno) {
                                var rowId = "rowId" + sno;
                                $("#" + rowId).remove();
                                document.getElementById('selectedRowCount').value--;
                            }

                            var ar_ext = ['pdf', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                            function checkName(sno) {
                                var name = document.getElementById("podFile" + sno).value;
                                var ar_name = name.split('.');

                                var ar_nm = ar_name[0].split('\\');
                                for (var i = 0; i < ar_nm.length; i++)
                                    var nm = ar_nm[i];

                                var re = 0;
                                for (var i = 0; i < ar_ext.length; i++) {
                                    if (ar_ext[i] == ar_name[1]) {
                                        re = 1;
                                        break;
                                    }
                                }

                                if (re == 1) {
                                }
                                else {
                                    alert('".' + ar_name[1] + '" is not an file type allowed for upload');
                                    document.getElementById("podFile" + sno).value = '';
                                }
                            }
                        </script>
                        <div id="routeDetail">

                            <c:if test = "${tripPointDetails != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30px">
                                        <td height="30" >S. No</td>
                                        <td height="30" >Point Name</td>
                                        <td height="30" >Type</td>
                                        <td height="30" >Route Order</td>
                                        <td height="30" >Address</td>
                                        <td height="30" >Planned Date</td>
                                        <td height="30" >Planned Time</td>
                                    </tr>
                                    <% int index3 = 1; %>
                                    <c:forEach items="${tripPointDetails}" var="tripPoint">
                                        <%
                                                       String classText1 = "";
                                                       int oddEven2 = index3 % 2;
                                                       if (oddEven2 > 0) {
                                                           classText1 = "text1";
                                                       } else {
                                                           classText1 = "text2";
                                                       }
                                        %>
                                        <tr height="30px">
                                            <td class="<%=classText1%>" height="30" ><%=index3++%></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>

                                <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>
                                                <td class="text1" width="150"> Estimated KM</td>
                                                <td class="text1" width="120" > <c:out value="${trip.estimatedKM}"  />&nbsp;</td>
                                                <td class="text1" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text2" width="150"> Estimated Reefer Hour</td>
                                                <td class="text2" width="120"> <c:out  value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                <td class="text2" colspan="4">&nbsp;</td>
                                            </tr>

                                        </c:forEach>
                                    </c:if>
                                </table>

                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <!--                     <div id="preStart">
                        
                        <c:if test = "${tripPreStartDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                            <td  colspan="4" >Trip Pre Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                              <td class="text1" height="30" >Trip Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                              <tr>
                                <td class="text2" height="30" >Trip Pre Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                <c:if test = "${tripDetails != null}" >
                                        <td class="text2">Trip Pre Start Location / Distance</td>
                                    <c:forEach items="${tripDetails}" var="trip">
                            <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:forEach>
                                </c:if>
                                </tr>
                                <tr>
                                        <td class="text1" height="30" >Trip Pre Start Remarks</td>
                                        <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                        </c:if>
                        <br>
                        <br>
                    </div>-->
                        <div id="startDetail">
                            <c:if test = "${tripStartDetails != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <c:forEach items="${tripStartDetails}" var="startDetails">
                                        <tr id="index" height="30">
                                            <td  colspan="6" > Trip Start Details</td>
                                        </tr>
                                        <tr  height="30">
                                            <td class="text1" height="30" >Trip Planned Start Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Planned Start Time</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Start Reporting Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                        </tr>
                                        <tr  height="30">
                                            <td class="text2" height="30" >Trip Start Reporting Time</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Loading date</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Loading Time</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                        </tr>
                                        <tr  height="30">
                                            <td class="text1" height="30" >Trip Loading Temperature</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Actual Start Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Actual Start Time</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                        </tr>
                                        <tr  height="30">
                                            <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                            <td class="text2" height="30" colspan="2" ></td>
                                        </tr>
                                    </c:forEach >
                                </table>

                                <c:if test = "${tripUnPackDetails != null}" >
                                    <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                        <tr id="index" height="30">
                                            <td width="20" align="center" height="30" >Sno</td>
                                            <td height="30" >Product/Article Code</td>
                                            <td height="30" >Product/Article Name </td>
                                            <td height="30" >Batch </td>
                                            <td height="30" ><font color='red'>*</font>No of Packages</td>
                                            <td height="30" ><font color='red'>*</font>Uom</td>
                                            <td height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                            <td height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                            <td height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                            <td  height="30" ><font color='red'>*</font>Shortage</td>
                                        </tr>


                                        <%int i1=1;%>
                                        <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr height="30">
                                                <td><%=i1%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:115px;height:20px;"/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:115px;height:20px;"/></td>
                                            </tr>
                                            <%i1++;%>
                                        </c:forEach>

                                        <br/>
                                        <br/>
                                        <br/>
                                    </table>

                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </c:if>
                        </div>

                        <div id="statusDetail">
                            <% int index1 = 1; %>

                            <c:if test = "${statusDetails != null}" >
                                <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30">
                                        <td  height="30" >S No</td>
                                        <td  height="30" >Status Name</td>
                                        <td  height="30" >Remarks</td>
                                        <td  height="30" >Created User Name</td>
                                        <td  height="30" >Created Date</td>
                                    </tr>
                                    <c:forEach items="${statusDetails}" var="statusDetails">
                                        <%
                                               String classText = "";
                                               int oddEven1 = index1 % 2;
                                               if (oddEven1 > 0) {
                                                   classText = "text1";
                                               } else {
                                                   classText = "text2";
                                               }
                                        %>
                                        <tr  height="30">
                                            <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>
                                <br/>
                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <c:if test="${tripAdvanceDetails != null}">
                            <div id="advance">
                                <c:if test="${tripAdvanceDetails != null}">
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr id="index" height="30">
                                            <td  width="30">Sno</td>
                                            <td  width="90">Advance Date</td>
                                            <td  width="90">Trip Day</td>
                                            <td  width="120">Estimated Advance</td>
                                            <td  width="120">Requested Advance</td>
                                            <td  width="90"> Type</td>
                                            <td  width="120">Requested By</td>
                                            <td  width="120">Requested Remarks</td>
                                            <td  width="120">Approved By</td>
                                            <td  width="120">Approved Remarks</td>
                                            <td  width="120">Paid Advance</td>
                                        </tr>
                                        <%int index7=1;%>
                                        <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                            <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                            <%
                                                   String classText7 = "";
                                                   int oddEven7 = index7 % 2;
                                                   if (oddEven7 > 0) {
                                                       classText7 = "text1";
                                                   } else {
                                                       classText7 = "text2";
                                                   }
                                            %>
                                            <tr height="30">
                                                <td class="<%=classText7%>"><%=index7++%></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                                <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                    <td class="<%=classText7%>">Adhoc</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                    <td class="<%=classText7%>">Batch</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                    <td class="<%=classText7%>">Manual</td>
                                                </c:if>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                            </tr>
                                        </c:forEach>

                                        <tr height="30">

                                            <td class="text1" colspan="10" align="right" style="color:black"><b>Total Advance Paid </b>&emsp;</td>
                                            <td class="text1" align="right"  style="color:black"><b><c:out value="${totalAdvancePaid}"/></b></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <c:if test="${tripAdvanceDetailsStatus != null}">
                                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr  height="30">
                                                <td  colspan="13" >
                                            <center style="color:black;font-size: 14px;">
                                                Advance Approval Status Details
                                            </center>
                                            </td>
                                            </tr>
                                            <tr id="index" height="30">
                                                <td  width="30">Sno</td>
                                                <td  width="90">Request Date</td>
                                                <td  width="90">Trip Day</td>
                                                <td  width="120">Estimated Advance</td>
                                                <td  width="120">Requested Advance</td>
                                                <td  width="90"> Type</td>
                                                <td  width="120">Requested By</td>
                                                <td  width="120">Requested Remarks</td>
                                                <td  width="120">Approval Status</td>
                                                <td  width="120">Paid Status</td>
                                            </tr>
                                            <%int index13=1;%>
                                            <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                <%
                                                       String classText13 = "";
                                                       int oddEven11 = index13 % 2;
                                                       if (oddEven11 > 0) {
                                                           classText13 = "text1";
                                                       } else {
                                                           classText13 = "text2";
                                                       }
                                                %>
                                                <tr height="30">

                                                    <td class="<%=classText13%>"><%=index13++%></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                    <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                        <td class="<%=classText13%>">Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                        <td class="<%=classText13%>">Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                        <td class="<%=classText13%>">Manual</td>
                                                    </c:if>

                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                    <td class="<%=classText13%>">
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                            &nbsp
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                            Request Approved
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                            Request Rejected
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                            Approval in  Pending
                                                        </c:if>
                                                        &nbsp;</td>
                                                    <td class="<%=classText13%>">
                                                        <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                            Yet To Pay
                                                        </c:if>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                        <br/>
                                        <br/>

                                        <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr  height="30">
                                                    <td  colspan="13" > 
                                                <center style="color:black;font-size: 14px;">
                                                    Old Vehicle Advance Details
                                                </center></td>
                                                </tr>
                                                <tr id="index" height="30">
                                                    <td  width="30">Sno</td>
                                                    <td  width="90">Advance Date</td>
                                                    <td  width="90">Trip Day</td>
                                                    <td  width="120">Estimated Advance</td>
                                                    <td  width="120">Requested Advance</td>
                                                    <td  width="90"> Type</td>
                                                    <td  width="120">Requested By</td>
                                                    <td  width="120">Requested Remarks</td>
                                                    <td  width="120">Approved By</td>
                                                    <td  width="120">Approved Remarks</td>
                                                    <td  width="120">Paid Advance</td>
                                                </tr>
                                                <%int index17=1;%>
                                                <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                    <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                    <%
                                                           String classText17 = "";
                                                           int oddEven17 = index17 % 2;
                                                           if (oddEven17 > 0) {
                                                               classText17 = "text1";
                                                           } else {
                                                               classText17 = "text2";
                                                           }
                                                    %>
                                                    <tr height="30">
                                                        <td class="<%=classText17%>"><%=index7++%></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                        <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                            <td class="<%=classText17%>">Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                            <td class="<%=classText17%>">Batch</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                            <td class="<%=classText17%>">Manual</td>
                                                        </c:if>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                    </tr>
                                                </c:forEach>


                                                <tr height="30">


                                                    <td class="text1" colspan="10" align="right"><b>Total Advance Paid </b>&emsp;</td>
                                                    <td class="text1" align="right"><b><c:out value="${totalVehicleChangeAdvancePaid}"/></b></td>

                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                        </c:if>
                        <div id="endDetail">
                            <c:if test = "${tripEndDetails != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <c:forEach items="${tripEndDetails}" var="endDetails">
                                        <tr id="index" height="30">
                                            <td  colspan="6" > Trip End Details</td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text1" height="30" >Trip Planned End Date</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                            <td class="text1" height="30" >Trip Planned End Time</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                            <td class="text1" height="30" >Trip Actual Reporting Date</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text2" height="30" >Trip Actual Reporting Time</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                            <td class="text2" height="30" >Trip Unloading Date</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                            <td class="text2" height="30" >Trip Unloading Time</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text1" height="30" >Trip Unloading Temperature</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                            <td class="text1" height="30" >Trip Actual End Date</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endDate}" /></td>
                                            <td class="text1" height="30" >Trip Actual End Time</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endTime}" /></td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text2" height="30" >Trip End Odometer Reading(KM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                            <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                            <td class="text2" height="30" >Total Odometer Reading(KM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                        </tr>
                                        <tr height="30">
                                            <td class="text1" height="30" >Total Reefer Reading(HM)</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                            <td class="text1" height="30" >Total Duration Hours</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                            <td class="text1" height="30" >Total Days</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.totalDays}" />

                                        </tr>

                                    </c:forEach >
                                </table>
                                <c:if test = "${tripUnPackDetails != null}" >
                                    <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                        <tr height="30" >
                                            <td  colspan="10" align="center">  <center style="color:black;font-size: 14px;">Consignment Unloading Details</center></td>
                                        </tr>

                                        <tr id="index" height="30">
                                            <td width="20"  align="center" height="30" >Sno</td>
                                            <td  height="30" >Product/Article Code</td>
                                            <td  height="30" >Product/Article Name </td>
                                            <td  height="30" >Batch </td>
                                            <td  height="30" ><font color='red'>*</font>No of Packages</td>
                                            <td  height="30" ><font color='red'>*</font>Uom</td>
                                            <td  height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                            <td  height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                            <td  height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                            <td  height="30" ><font color='red'>*</font>Shortage</td>
                                        </tr>


                                        <%int i1=1;%>
                                        <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr height="30">
                                                <td><%=i1%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly style="width:115px;height:20px;"/></td>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:115px;height:20px;"/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"   style="width:115px;height:20px;" /></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:115px;height:20px;"/></td>
                                            </tr>
                                            <%i1++;%>
                                        </c:forEach>

                                        <br/>
                                    </table>
                                    <br/>
                                    <br/>
                                </c:if>
                                <br/>

                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>

                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>