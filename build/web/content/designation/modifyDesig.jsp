<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bus</title>
<link href="/bharath/css/bharath.css" rel="stylesheet" type="text/css">
<script language="javascript">

function submitpage(value)
        {      
document.modify.action='/bharath/modifyDesig.do';
document.modify.reqfor.value='designa';
document.modify.submit();
}

</script>

</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body>
<form  method="post" name="modify"> 
<!-- copy there from end -->
<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
<!-- pointer table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->
<!-- title table -->
<table width="875" cellpadding="0" cellspacing="0" align="center" border="0">
<tr>
<td height="20"><%@ include file="/content/common/pageTitle.jsp" %></td>
</tr>
</table>
<!-- title table -->
</div>
</div>
<br>
<br>
<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<!-- message table -->
<!-- copy there  end -->
 <%
    int index = 0;
ArrayList DesignaList = (ArrayList) session.getAttribute("DesignaList");
%>        
<br>
<table width="300" align="center" id="bg" cellpadding="0" cellspacing="0">

<tr>
<td class="contentsub" height="30">Designation Id</td> 
<td class="contentsub" height="30">Designation Name </td> 
<td class="contentsub" height="30">Status</td>
<td class="contentsub" height="30">CheckBox</td>
</tr>
<c:if test = "${DesignaList != null}" >
<c:forEach items="${DesignaList}" var="Desig"> 		
<tr>
<td class="text1" height="30"><input type="hidden" name="desigIds" value='<c:out value="${Desig.deptId}"/>'> <div align="center"><c:out value="${Desig.deptId}"/></div> </td>                                           
<td class="text1" height="30"><input type="text" class="textbox" name="desigNames" value="<c:out value="${Desig.desigName}"/>"></td>
<td height="30" class="text1"> <div align="center"><select name="activeInds" class="textbox">
<c:choose>
<c:when test="${Desig.status == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" class="text1"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="designa" />
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
