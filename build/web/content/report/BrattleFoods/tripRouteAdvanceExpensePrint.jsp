
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">Advance Print</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="enter" method="post">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String startDate = sdf.format(today);
                    %>
                    <div id="printContent" >
                        <table align="center"  style="-webkit-print-color-adjust: exact;border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                               background:url(images/dict-logoWatermark.png);
                               /*background-repeat:no-repeat;*/
                               background-position:center;
                               border:1px solid;
                               /*background-color: white;*/
                               /*border-color:#CD853F;width:800px;*/
                               border:1px solid;
                               /*opacity:0.8;*/
                               font-weight:bold;">

                            <tr>
                                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                                    <table align="left" >
                                        <tr>
                                            <td colspan="2">
                                                <font size="2">PAN NO-AACCB8054G </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">GST No : 06AACCB8054G1ZT</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">CIN NO-U63040MH2006PTC159885</font><br>
                                            </td>

                                        </tr>
                                        <tr>
                                            <!--<td align="left"><img src="images/GatiLogo.png" width="100" height="50"/></td>-->
                                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                            <td style="padding-left: 60px; padding-right:50px;">


                                        <font size="3"><center><b><u>Delhi International Cargo Terminal Pvt.Ltd.</u></b></center></font><br>
                                        <center> <font size="2" > <u>Trip(Advance Expenses)</u> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</font></center>

                                </td>
                                <td></td>
                            </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; " align='center'>
                                <table width="100%" align="center">
                                    <tr>
                                        <td ><font size="2">S.No.<c:out value="${voucherNo}"/></font><td/>
                                    </tr>
                                    <tr>
                                        <td ><font size="2">Lorry.No: <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <td style="float: left;padding-left: 70px;" ><font size="2">Date : <c:out value="${expesneDate}"/></font></td>
                                    </tr>
                                    <tr>
                                        <td ><font size="2">Driver's.Name:&nbsp;<c:out value="${driverName}"/></font></td>
                                        <td style="float: left;padding-left: 70px;" ><font size="2">Transport :&nbsp;<c:out value="${transpoter}"/></font></td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2"><font size="2">Route :&nbsp;<c:out value="${routeInfo}"/></font></td>
                                        <!--                                <td width="40%"><font size="2">From&nbsp;...................</font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <td style="float: left;padding-left: 150px;" width="40%"><font size="2">To&nbsp;...................</font></td>-->
                                    </tr>
                                    <tr>
                                        <td ><font size="2">GR.No:&nbsp;DICT/<c:out value="${grNo}"/></font></td>
                                        <td style="float: left;padding-left: 70px;" ><font size="2"> GR Date.Of.Issue:&nbsp;<c:out value="${grDate}"/></font></td>
                                    </tr>
                                    <tr>
                                        <td  ><font size="2">Party:&nbsp;<c:out value="${billingParty}"/></font></td>
                                         <!--<td style="float: left;padding-left: 70px;" ><font size="2">Diesel(ltr):&nbsp;<c:out value="${dieselUsed}"/> & Amount: <c:out value="${dieselCost}"/></font></td>-->
                                    </tr>
                                    <tr>
                                        <td ><font size="2">Trip Advance:&nbsp;Rs/-<c:out value="${foodCost}"/></font></td>
                                        <!--<td style="float: left;padding-left: 70px;" ><font size="2">OP.M/R:&nbsp;<c:out value="${startKm}"/></font></td>-->
                                    </tr>
                                    <!--                          <tr>
                                                                    <td ><font size="2">Vehicle Moved On:&nbsp;&nbsp;<c:out value="${startDate}"/></font></td>
                                                                    <td style="float: left;padding-left: 70px;" ><font size="2">CL.M/R:&nbsp;<c:out value="${endKm}"/></font></td>
                                                                </tr>
                                                                <tr>
                                                                    <td ><font size="2">Vehicle Returned On:&nbsp;&nbsp;<c:out value="${endDate}"/></font></td>
                                                                    <td style="float: left;padding-left: 70px;" ><font size="2">Total Kms:&nbsp;<c:out value="${tripTotalKms}"/></font></td>
                                                                </tr>-->
                                </table>
                                <br>
                                <c:set var="totExpense" value="${0}"/>
                                <table align='center' border="1" width="100%">
                                    <tr align='center'>
                                        <td style="width:2px">S.No</td>
                                        <td style="width:500px">Particulars</td>
                                        <td style="width:100px">Amount</td>
                                    </tr>
                                    <!--                            <tr>
                                                                    <td align='center'>1</td>
                                                                    <td >Diesel/Fuel Expenses
                                    <c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Fuel Expanses' }">
                                             (   <c:out value="${trip.expenseRemarks}"/> )
    
                                        </c:if>
                                    </c:forEach>
                           </td>
                           <td > <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'Fuel Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                            <c:set var="totExpense" value="${trip.totalExpenses + totExpense}"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>-->
                                    <tr>
                                        <td align='center'>1</td>
                                        <td>Foods and Toll Expense</td>
                                        <td>
                                            <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                                <c:if test="${trip.expenseName == 'For Food & Toll' }">
                                                    <c:out value="${trip.expenseValue}"/>
                                                    <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                                </c:if>
                                            </c:forEach>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align='center'>2</td>
                                        <td>Food Expenses</td>
                                        <td> <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                                <c:if test="${trip.expenseName == 'For Food' }">
                                                    <c:out value="${trip.expenseValue}"/>
                                                    <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                                </c:if>
                                            </c:forEach></td>
                                    </tr>
                                    <tr>
                                        <td align='center'>3</td>
                                        <td>Toll Expense</td>
                                        <td><c:forEach items="${getAdvanceTripExpense}" var="trip">
                                                <c:if test="${trip.expenseName == 'For Toll' }">
                                                    <c:out value="${trip.expenseValue}"/>
                                                    <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                                </c:if>
                                            </c:forEach></td>
                                    </tr>
                                    <tr>
                                        <td align='center'>4</td>
                                        <td>Other Expenses</td>
                                        <td><c:forEach items="${getAdvanceTripExpense}" var="trip">
                                                <c:if test="${trip.expenseName == 'Other' }">
                                                    <c:out value="${trip.expenseValue}"/>
                                                    <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                                </c:if>
                                            </c:forEach></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align='center'>Total Amount </td>
                                        <td>&nbsp;<c:out value="${totExpense}"/></td>
                                    </tr>

                                </table>
                                <br>
                                <table align="center">
                                    <tr>
                                        <td width="100%"><font size="2" colspan="2">Received With Thanks Rs<jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                                <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                                <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                            </jsp:useBean></td>
                                        <!--                                <td></td>
                                                                        <td></td>-->
                                    </tr>
                                    <tr>
                                        <td  width="100%"colspan="2"><font size="2">from DICTPL against the expenses incurred as per above mentioned </td>
                                        <!--                                <td></td>
                                                                        <td></td>-->
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 15px;" >Revenue Stamp</td>

                                        <td  colspan="2" align="right"style="padding-top: 15px;"> For DICTPL <td>

                                    </tr>


                                    <tr>
                                        <td style="padding-top: 30px;">Driver's Sign</td>
                                        <td style="padding-right: 250px;padding-top: 30px;">Cashier</td>
                                        <td style="padding-top: 30px;" align="left" >TT(Head)</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 30px;">Created By:&nbsp&nbsp<c:out value="${createdBy}"/></td>
                                    </tr>
                                </table>
                                <br>
                                </table>
                    </div>
                    <br>
                    <br>
                    <center><input type="button" class="btn btn-info"  value="Print" onClick="print('printContent');" style="width:90px;height:30px;"></center>
                    <script type="text/javascript">
                        function print(val)
                        {
                            var DocumentContainer = document.getElementById(val);
                            var WindowObject = window.open('', "TrackHistoryData",
                                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            WindowObject.close();
                        }
                    </script>
                 <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
