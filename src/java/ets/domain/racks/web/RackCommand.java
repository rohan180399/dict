package ets.domain.racks.web;

/**
 *
 * @author vidya
 */
public class RackCommand {

    private String rackId="";
    private String rackName="";
    private String rackDescription="";
    private String rackStatus="";
    private String[] rackIdList;
    private String[] rackNameList;
    private String[] rackDescriptionList;
    private String[] rackStatusList;
    private String[] selectedIndex;
    private String subRackId="";
    private String subRackName="";
    private String subRackDescription="";
    private String subRackStatus="";
    private String[] subRackIdList;
    private String[] subRackNameList;
    private String[] subRackDescriptionList;
    private String[] subRackStatusList;
    
    
    private String sectionId="";
    private String sectionName="";
    private String description="";
    private String activeInd="";
    
    private String[] sectionIds;
    private String[] sectionNames;
    private String[] descriptions;
    private String[] activeInds;
    
    private String uom="";
    private String itemCode="";
      private String paplCode="";
       private String itemName="";
        private String maxQuandity="";
         private String roLevel="";
         private String specification="";
         private String reConditionable="";
         private String scrapUomId="";
         private String scrapUomName="";
         private String mfrId="";
         private String modelId="";
         private String uomId="";
         private String itemId="";

    public String getScrapUomName() {
        return scrapUomName;
    }

    public void setScrapUomName(String scrapUomName) {
        this.scrapUomName = scrapUomName;
    }
         
 private String mfrCode;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

 
 
    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }
 
    public String getScrapUomId() {
        return scrapUomId;
    }

    public void setScrapUomId(String scrapUomId) {
        this.scrapUomId = scrapUomId;
    }

         
    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }
         
         
         

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
         

    
         

    public String getReConditionable() {
        return reConditionable;
    }

    public void setReConditionable(String reConditionable) {
        this.reConditionable = reConditionable;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
         
         

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMaxQuandity() {
        return maxQuandity;
    }

    public void setMaxQuandity(String maxQuandity) {
        this.maxQuandity = maxQuandity;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRoLevel() {
        return roLevel;
    }

    public void setRoLevel(String roLevel) {
        this.roLevel = roLevel;
    }
         
         

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String[] getSectionIds() {
        return sectionIds;
    }

    public void setSectionIds(String[] sectionIds) {
        this.sectionIds = sectionIds;
    }

    public String[] getSectionNames() {
        return sectionNames;
    }

    public void setSectionNames(String[] sectionNames) {
        this.sectionNames = sectionNames;
    }
    
    

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    
    
    
    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public String getRackDescription() {
        return rackDescription;
    }

    public void setRackDescription(String rackDescription) {
        this.rackDescription = rackDescription;
    }

    public String getRackName() {
        return rackName;
    }

    public void setRackName(String rackName) {
        this.rackName = rackName;
    }

    public String getRackStatus() {
        return rackStatus;
    }

    public void setRackStatus(String rackStatus) {
        this.rackStatus = rackStatus;
    }

    public String[] getRackDescriptionList() {
        return rackDescriptionList;
    }

    public void setRackDescriptionList(String[] rackDescriptionList) {
        this.rackDescriptionList = rackDescriptionList;
    }

    public String[] getRackIdList() {
        return rackIdList;
    }

    public void setRackIdList(String[] rackIdList) {
        this.rackIdList = rackIdList;
    }

    public String[] getRackNameList() {
        return rackNameList;
    }

    public void setRackNameList(String[] rackNameList) {
        this.rackNameList = rackNameList;
    }

    public String[] getRackStatusList() {
        return rackStatusList;
    }

    public void setRackStatusList(String[] rackStatusList) {
        this.rackStatusList = rackStatusList;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getSubRackDescription() {
        return subRackDescription;
    }

    public void setSubRackDescription(String subRackDescription) {
        this.subRackDescription = subRackDescription;
    }

    public String[] getSubRackDescriptionList() {
        return subRackDescriptionList;
    }

    public void setSubRackDescriptionList(String[] subRackDescriptionList) {
        this.subRackDescriptionList = subRackDescriptionList;
    }

    public String getSubRackId() {
        return subRackId;
    }

    public void setSubRackId(String subRackId) {
        this.subRackId = subRackId;
    }

    public String[] getSubRackIdList() {
        return subRackIdList;
    }

    public void setSubRackIdList(String[] subRackIdList) {
        this.subRackIdList = subRackIdList;
    }

    public String getSubRackName() {
        return subRackName;
    }

    public void setSubRackName(String subRackName) {
        this.subRackName = subRackName;
    }

    public String[] getSubRackNameList() {
        return subRackNameList;
    }

    public void setSubRackNameList(String[] subRackNameList) {
        this.subRackNameList = subRackNameList;
    }

    public String getSubRackStatus() {
        return subRackStatus;
    }

    public void setSubRackStatus(String subRackStatus) {
        this.subRackStatus = subRackStatus;
    }

    public String[] getSubRackStatusList() {
        return subRackStatusList;
    }

    public void setSubRackStatusList(String[] subRackStatusList) {
        this.subRackStatusList = subRackStatusList;
    }

   
}
