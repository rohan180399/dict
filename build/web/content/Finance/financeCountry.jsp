<%-- 
    Document   : financeCountry
    Created on : 7 Nov, 2012, 6:47:33 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Tax master</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
    <body>
        <form method="post" action="/throttle/addCountryPage.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <c:if test = "${CountryLists != null}" >
                <table align="center" width="650" border="0" cellspacing="0" cellpadding="0" class="border">

                    <tr height="30">
                        <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Country code</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Country Name</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Edit</b></td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${CountryLists}" var="CL">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                            <td class="<%=classText%>" align="left"> <c:out value="${CL.countryCode}" /></td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${CL.countryName}"/> </td>
                            <td class="<%=classText%>" align="left"> <a href="/throttle/alterCountryDetail.do?countryID=<c:out value='${CL.countryID}' />" > Edit </a> </td>
                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <br>
            <center><input type="submit" class="button" value="Add" /></center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
