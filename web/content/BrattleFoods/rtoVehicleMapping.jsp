
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">RTO Vehicle Mapping</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="enter" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colspan="3" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">RTO Vehicle Mapping</td></tr>

                        <tr>
                            <td height="30" >Select Rto Office :&nbsp;</td>
                            <td> <select class="form-control" style="width:240px;height:40px;" name="rtoOfficeId" id="rtoOfficeId"  >
                                    <option value="">---Select---</option>
                                    <c:forEach items="${troMasterList}" var="troMasterList">
                                        <option value='<c:out value="${troMasterList.rtoId}"/>'><c:out value="${troMasterList.rtoName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td  height="30" ><input type="button" align="center" name="search" value="Go" onClick="submitPage()" class="btn btn-info"></td>
                        </tr>
                    </table>
                    
                    <c:if test="${rtoMappedVehicleList != null}" >
                        <table align="center" border="0" id="table" class="sortable" colspan="3" style="width:700px;" >
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th  valign="center" height="35" >Available Vehicles</th>
                                <th  height="35"></th>
                                <th  valign="center" height="35">Assigned Vehicles</th>
                            </tr>
                        </thead>
                            <tr>
                                <td valign="top">
                                    <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                                        <tr>
                                            <td width="150">
                                                <select style="width:200px;"  id="availableFunc" multiple size="10" name="vehicleId">
                                                    <c:if test = "${vehicleList != null}" >
                                                        <c:forEach items="${vehicleList}" var="vehicleList">
                                                            <option value='<c:out value="${vehicleList.vehicleId}" />'><c:out value="${vehicleList.regNo}" /></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                </td>

                                <td height="100" valign="center">
                                    <table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
                                        <tr>
                                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="btn btn-info" value=">" onClick="copyAddress('')"></td></tr>
                                        <tr >
                                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="btn btn-info" value="<" onClick="copyAddress1('')"></td></tr>


                                    </table></td>
                                <td valign="top">

                                    <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                                        <tr>
                                            <td width="150"><select style="width:200px;" id="assignedfunc" multiple size="10" name="assignedVehicle" >
                                                    <c:if test = "${rtoMappedVehicleList != null}" >
                                                        <c:forEach items="${rtoMappedVehicleList}" var="rtoMappedVehicleList">
                                                            <option value='<c:out value="${rtoMappedVehicleList.vehicleId}" />'><c:out value="${rtoMappedVehicleList.vehicleNo}" /></option>
                                                        </c:forEach >
                                                    </c:if>
                                                </select></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                        <center>
                            <input type="button" align="center" name="search" value="SAVE" onClick="saveRtoVehicleMapping()" class="btn btn-info">
                        </center>
                    </c:if>
                </form>
                <script type="text/javascript">
                    function submitPage() {
                        var rtoOfficeId = document.getElementById("rtoOfficeId").value;
                        if (rtoOfficeId == "") {
                            alert("Select Rto Office");
                            document.getElementById("rtoOfficeId").focus();
                            return;
                        }
                        document.enter.action = '/throttle/rtovehiclemapping.do';
                        document.enter.submit();

                    }
                    function saveRtoVehicleMapping() {
                        var counter = document.enter.assignedVehicle.length;
                        if (parseInt(counter) == parseInt(0)) {
                            alert("Assign Any Vehicle");
                            return;
                        }
                        for (var j = 0; j < counter; j++) {
                            //alert(document.userroles.assignedRole.options[j].value);
                            document.enter.assignedVehicle.options[j].selected = true;
                        }
                        document.enter.action = '/throttle/saveRtoVehiclemapping.do';
                        document.enter.submit();
                    }
                    var rtoOfficeId = '<c:out value="${rtoOfficeId}" />';
                    if (rtoOfficeId != null & rtoOfficeId != "") {
                        document.getElementById("rtoOfficeId").value = rtoOfficeId;
                    }

                    function copyAddress(roleId)
                    {
                        var avilableFunction = document.getElementById("availableFunc");
                        var index = 0;
                        var selectedLength = 0;
                        if (avilableFunction.length != 0) {
                            for (var i = 0; i < avilableFunction.options.length; i++) {
                                if (avilableFunction.options[i].selected == true) {
                                    selectedLength++;
                                }
                            }
                            for (var j = 0; j < selectedLength; j++) {
                                for (var i = 0; i < avilableFunction.options.length; i++) {
                                    if (avilableFunction.options[i].selected == true) {

                                        var optionCounter = document.enter.assignedfunc.length;
                                        document.enter.assignedfunc.length = document.enter.assignedfunc.length + 1;
                                        document.enter.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text;
                                        document.enter.assignedfunc.options[optionCounter].value = avilableFunction.options[i].value;
                                        avilableFunction.options[i] = null;
                                        break;
                                    }
                                }
                            }
                        }
                        else {
                            alert("Please Select any Value");
                        }
                    }



                    function copyAddress1(assRole)
                    {
                        var selectedValue = document.getElementById('assignedfunc');
                        var index = 0;
                        var selectedLength = 0;

                        if (selectedValue.length != 0) {
                            for (var i = 0; i < selectedValue.options.length; i++) {
                                if (selectedValue.options[i].selected == true) {
                                    selectedLength++;
                                }
                            }
                            for (var j = 0; j < selectedLength; j++) {
                                for (var i = 0; i < selectedValue.options.length; i++) {
                                    if (selectedValue.options[i].selected == true) {

                                        var optionCounter = document.enter.availableFunc.length;
                                        document.enter.availableFunc.length = document.enter.availableFunc.length + 1;
                                        document.enter.availableFunc.options[optionCounter].text = selectedValue.options[i].text;
                                        document.enter.availableFunc.options[optionCounter].value = selectedValue.options[i].value;
                                        selectedValue.options[i] = null;
                                        break;
                                    }
                                }
                            }
                        }
                        else {
                            alert("Please Select any Value");
                        }
                    }

                </script>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
