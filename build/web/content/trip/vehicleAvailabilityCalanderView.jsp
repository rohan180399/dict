<%@page import="ets.domain.trip.business.TripTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<link href='/throttle/fullcalendar/fullcalendar.css' rel='stylesheet' />
<script src='/throttle/fullcalendar/lib/moment.min.js'></script>
<script src='/throttle/fullcalendar/lib/jquery.min.js'></script>
<script src='/throttle/fullcalendar/fullcalendar.min.js'></script>

<script>
     var jsonVehicleData = [];
</script>

<script>
    <%
    ArrayList vehicleData = (ArrayList)request.getAttribute("vehicleStatus");
    Iterator itr = vehicleData.iterator();
    TripTO tripTONew = null;
                while (itr.hasNext()) {
                    tripTONew = new TripTO();
                    tripTONew = (TripTO) itr.next();
                    %>
                        jsonVehicleData.push({title: '<%= tripTONew.getRouteInfo()%>', start: '<%= tripTONew.getPlanStartDate()%>',end: '<%= tripTONew.getPlanEndDate()%>',url:'viewVehicleDetails.do?', className:'one'});
                    <%

                }
    %>

</script>

<script>

$(document).ready(function() {

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},


		selectable: true,
		selectHelper: true,
		select: function(start, end) {
			//var title = prompt('Event Title:');
			var title = '';
			var eventData;
			if (title) {
				eventData = {
					title: title,
					start: start,
					end: end
				};
				$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
			}

			$('#calendar').fullCalendar('unselect');
		},

		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: {

		},
		eventRender: function(event, element) {
				var dateString = moment(event.start).format('YYYY-MM-DD');
				if(event.className=="one"){
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#e8e8e8');
				}
				if(event.className=="two"){
						// $('.fc-day[data-date="'+ dateString +'"]').addClass('two');
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#fbcdcf');
				}
				if(event.className=="three"){
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#73ff00');
				}
		},
		events: function(start, end, timezone, callback) {
			$('#spinner').show();
			$("#loader").addClass('ui-loader-background');
                        callback(jsonVehicleData);
		}


	});

});
</script>
<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.td.fc-day.one {
		background: #000;
	}
	.td.fc-day.two {
		background: #F00;
	}
	.td.fc-day.three {
		background: #0F0;
	}

</style>
</head>
<body>

	<div id='calendar'></div>

</body>
</html>
