
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <body>
        <form name="grn" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <% int index = 0;%> 
            <c:if test = "${InVoiceItems != null}" >
                <table align="center" width="300" border="0" cellspacing="0" cellpadding="0" class="border">
                    <c:forEach items="${InVoiceItems}" var="stock"> 
                        <% if (index == 0) {%>   
                        <tr>
                            <td class="contentsub" colspan="2" align="center" height="30"> GRN&nbsp;&nbsp;: &nbsp;&nbsp;<c:out value="${stock.supplyId}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Vendor :</td>
                            <td height="30"  class="text1"><c:out value="${stock.vendorName}"/></td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30"> Invoice No:</td>
                            <td height="30"  class="text2"><c:out value="${stock.invoiceId}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Invoice Date :</td>
                            <td height="30"  class="text1"><c:out value="${stock.billDate}"/></td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30"> Freight Charge :</td>
                            <td height="30"  class="text2"><c:out value="${stock.freight}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Invoice Amount :</td>
                            <td height="30"  class="text1"><c:out value="${stock.totalAmount}"/></td>
                        </tr>
                        <% index++;
            }%>               
                    </c:forEach>
                </table>
                <br>
                
                <br>
                
                <table width="690" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border"> 
                    
                    <tr>
                        <td  height="30" class="contentsub">S No</td>
                        <td  height="30" class="contentsub">Item Code</td>
                        <td  height="30" class="contentsub">Item Name</td>                        
                        <td  height="30" class="contentsub">Qty Ordered</td>
                        <td  height="30" class="contentsub">Qty Received</td>
                        <td  height="30" class="contentsub">Unit Price</td>
                        <td  height="30" class="contentsub">Discount</td>
                         <td  height="30" class="contentsub">Tax(%)</td>
                        <td  height="30" class="contentsub">MRP(SAR)</td>                       
                        <td  height="30" class="contentsub">Amount(SAR)</td>
                    </tr>
                    <%  index = 0;%> 
                    <c:forEach items="${InVoiceItems}" var="stock"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr>
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.itemId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.itemName}"/></td>                          
                            <td class="<%=classText %>" height="30"><c:out value="${stock.rqty}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.aqty}"/></td>
                              <td class="<%=classText %>" height="30"><c:out value="${stock.unitPrice}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.discount}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.tax}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${stock.mrp}"/></td>                            
                            <td  class="<%=classText %>" height="30"><c:out value="${stock.itemAmount}"/></td> 
                        </tr>
                        <% index++;%>
                    </c:forEach>
                    
                </table>
            </c:if>
            <br>
            
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
    
</html>
