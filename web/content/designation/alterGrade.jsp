
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

    function submitpage(value)
    {
        var checValidate = selectedItemValidation();
        var splt = checValidate.split("-");
//alert(splt[0]);
        if (splt[0] == 'SubmitForm' && splt[1] != 0) {
            document.modify.action = '/throttle/modifyGrade.do';
            document.modify.submit();
        }
    }
    function selectedItemValidation() {
        var index = document.getElementsByName("selectedIndex");
        var gradeName = document.getElementsByName("gradeNames");
        var chec = 0;
        var mess = "SubmitForm";
        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (index[i].checked) {
                chec++;
                if (isEmpty(gradeName[i].value)) {
                    alert("Enter The Grade Name");
                    gradeName[i].focus();
                    mess = "";
                    break;
                } else if (isChar(gradeName[i].value)) {
                    alert(" Grade Name must be in Character");
                    gradeName[i].focus();
                    mess = "";
                    break;
                }
            }
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            gradeName[0].focus();
//break;
        }
        return mess + "-" + chec;
    }

    function isChar(s) {
        if (!(/^-?\d+$/.test(s))) {
            return false;
        }
        return true;
    }

    function setSelectbox(i)
    {
        var selected = document.getElementsByName("selectedIndex");
        selected[i].checked = 1;
    }

</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    <!--[if lte IE 7]>
    <style type="text/css">
    
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/ 
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AlterGrade" text="default text"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>
          <li class=""><spring:message code="hrms.label.Grade" text="default text"/></li>
          <li class=""><spring:message code="hrms.label.AlterGrade" text="default text"/></li>
        </ol>
      </div>
      </div>

<!--    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AlterGrade" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AddGrade" text="default text"/></h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>
                    <li class=""><spring:message code="hrms.label.Grade" text="default text"/></li>
                    <li class=""><spring:message code="hrms.label.AlterGrade" text="default text"/></li>
                </ol>
            </div>
        </div>-->
        <div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
        <body>
            <form  method="post" name="modify"> 
                <table class="table table-info mb30 table-hover" >
                    <thead>
                        <tr>
                            <th class="text2" height="30" colspan="2"><strong><spring:message code="hrms.label.DesignationName" text="default text"/></strong></th>
                            <th class="text2" height="30" colspan="3"><strong><%= request.getAttribute("DesigName") %></strong></th>
                    <input type="hidden" name="desigName" value="<%= request.getAttribute("DesigName") %>" >
                    </tr>
                    </thead>
                    <c:if test = "${GradeList != null}" >
                        <thead>
                            <tr>
                                <th  height="30"><div ><spring:message code="hrms.label.GradeId" text="default text"/></div></th>
                                <th  height="30"><div ><spring:message code="hrms.label.GradeName" text="default text"/></div> </th>
                                <th  height="30"><div ><spring:message code="hrms.label.Description" text="default text"/></div></th>
                                <th  height="30"><div ><spring:message code="hrms.label.Status" text="default text"/></div></th>
                                <th  height="30"><div ><spring:message code="hrms.label.CheckBox" text="default text"/></div></th>
                            </tr>
                        </thead>
                    </c:if>

                    <% int index=0; %>
                    <c:if test = "${GradeList != null}" >
                        <c:forEach items="${GradeList}" var="grade"> 		
                            <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                            classText = "text2";
                            } else {
                            classText = "text1";
                            }
                            %>
                            <tr>
                                <td  height="30"><input type="hidden" name="gradeIds" value='<c:out value="${grade.gradeId}"/>'> <div align="center"><c:out value="${grade.gradeId}"/></div> </td>                                           
                                <td  height="30"><input type="text" class="form-control" style="width:220px;height:40px;" name="gradeNames" onchange="setSelectbox(<%= index %>)" value="<c:out value="${grade.gradeName}"/>"></td>
                                <td  height="30"><input type="text" class="form-control" style="width:220px;height:40px;" name="descriptions" onchange="setSelectbox(<%= index %>)" value="<c:out value="${grade.description}"/>"></td>
                                <td height="30" > <div align="center"><select name="activeInds" class="form-control" style="width:220px;height:40px;" onchange="setSelectbox(<%= index %>)" >
                                            <c:choose>
                                                <c:when test="${grade.activeInd == 'Y'}">
                                                    <option value="Y" selected><spring:message code="hrms.label.Active" text="default text"/></option>
                                                    <option value="N"><spring:message code="hrms.label.InActive" text="default text"/></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="Y"><spring:message code="hrms.label.Active" text="default text"/></option>
                                                    <option value="N" selected><spring:message code="hrms.label.InActive" text="default text"/></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                    </div> 
                                </td>
                                <td width="77" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                            </tr>
                            <%
                            index++;
                            %>
                        </c:forEach >
                    </c:if> 
                    <input type="hidden" name="designationId" value='<%= request.getAttribute("DesigId") %>'>
                </table>
                <center>
                    <br>
                    <input type="button" name="save" value="<spring:message code="hrms.label.SAVE" text="default text"/>" onClick="submitpage(this.name)" class="btn btn-success" />
                    <input type="hidden" name="reqfor" value="" />
                </center>
                <br>
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </body>
    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
