<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
    $("#datepicker").datepicker({
    showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

    });
    });
            $(function () {
            $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
                    changeMonth: true, changeYear: true
            });
            });</script>   
<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }



</style>

<script>
//    $(document).ready(function() {
//        $('#tarrifType').autocomplete({
//            source: function(request, response) {
//                $.ajax({
//                    url: "/throttle/getDetentionChargeDetails.do",
//                    dataType: "json",
//                    data: {
//                        tarrifType:$("#tarrifType").val(),
//                        tripSheetId:$("#tripSheetId").val(),
//                        totalDays2:$("#totalDays2").val()
//                    },
//                    success: function(data, textStatus, jqXHR) {
//                        var items = data;
//                        if (items == '') {
////                            $('#tarrifType').val('');
//                          
//                        }
//                        response(items);
//                    },
//                    error: function(data, type) {
//                        console.log(type);
//                    }
//                });
//            },
//            minLength: 1,
//        }).data("ui-autocomplete")._renderItem = function(ul, item) {
//            var itemVal = item.Name;
//              $('#detentionAmount').val(itemVal);
//         
//        };
//    });


            function detentionCalculation(val) {
            $.ajax({
            url: '/throttle/getDetentionChargeDetails.do',
                    data: { tarrifType:$("#tarrifType").val(),
                            tripSheetId:$("#tripSheetId").val(),
                            totalDays2:$("#totalDays2").val()},
                    dataType: 'json',
                    success: function (data) {
                    $.each(data, function (i, data) {
                    $('#detentionAmount').val(data.Name);
                    });
                    }
            });
            }



    function saveTripEndDetails() {

    var startKm = document.getElementById("startKM").value;
            var endKm = document.getElementById("endOdometerReading").value;
            if (document.getElementById("movementType").value != '3') {
    if (isEmpty(document.getElementById("vehicleactreportdate").value)) {
    alert('please enter vehicle actual report date');
            document.getElementById("vehicleactreportdate").focus();
            return;
    }
    if (isEmpty(document.getElementById("vehicleloadreportdate").value)) {
    alert('please enter vehicle load report date');
            document.getElementById("vehicleloadreportdate").focus();
            return;
    }
    }
    if (isEmpty(document.getElementById("endDate").value)) {
    alert('please enter vehicle end date');
            document.getElementById("endDate").focus();
            return;
    }
    if (isEmpty(document.getElementById("tripEndHour").value)) {
    alert('please enter trip End Hour');
            document.getElementById("tripEndHour").focus();
            return;
    }
    if (isEmpty(document.getElementById("tripEndMinute").value)) {
    alert('please enter trip End Minute');
            document.getElementById("tripEndMinute").focus();
            return;
    }
    if (isEmpty(document.getElementById("endOdometerReading").value)) {
    alert('please enter end odometer reading');
            document.getElementById("endOdometerReading").focus();
            return;
    }
    if (isEmpty(document.getElementById("endHM").value)) {
    alert('please enter end HM');
            document.getElementById("endHM").focus();
            return;
    }
    if (isEmpty(document.getElementById("tripTransitHour").value)) {
    alert('please enter Total Duration Hours');
            document.getElementById("tripTransitHour").focus();
            return;
    }
    if (isEmpty(document.getElementById("tripDetainHour").value)) {
    alert('please enter Detain Hours');
            document.getElementById("tripDetainHour").focus();
    }
//            if (document.getElementById("movementType").value == '1' || document.getElementById("movementType").value == '6') {
//
//            if (isEmpty(document.getElementById("shipingLineNo").value)) {
//            alert('please enter Shipping Bill Number');
//                    document.getElementById("slN").focus();
//                    return;
//            }
//            }
//            if (document.getElementById("movementType").value == '2') {
//
//            if (isEmpty(document.getElementById("billOfEntry").value)) {
//            alert('please enter Bill of Entry  Number');
//                    document.getElementById("boE").focus();
//                    return;
//            }
//            }
    if (document.getElementById("commodityName").value == '0' || document.getElementById("commodityName").value == '') {
    alert('please enter commodity category');
            document.getElementById("commodityCategory").focus();
            return;
    }


//         if (document.getElementById("commodityName").value == '') {
//             alert('please enter commodity name');
//             document.getElementById("commodityName").focus();
//                return;
//         }

//        if (endKm < startKm) {
//            alert('End Km cannot be lesser than start Km !please check .');
//            document.getElementById("endOdometerReading").focus();
//            return;
//        } else {
    var confirms = confirm("Do you want to continue this commodity?");
//            alert(confirms);
            if (confirms == true) {
//                alert("You Pressed Yes")
    $("#Save").hide();
            document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background'; commodityCategory
            document.tripExpense.action = '/throttle/updateEndTripSheetDuringClosure.do';
            document.tripExpense.submit();
    } else {
//                alert("You Pressed No")
    $("#Save").show();
    }

//        }
    }
    function saveTripOverRideDetails() {
    if (isEmpty(document.getElementById("setteledKm").value)) {
    alert('please enter override end odometer reading');
            document.getElementById("setteledKm").focus();
    }
    if (isEmpty(document.getElementById("setteledHm").value)) {
    alert('please enter override end reefer reading');
            document.getElementById("setteledHm").focus();
    }
    if (isEmpty(document.getElementById("overrideRemarks").value)) {
    alert('please enter override remarks');
            document.getElementById("overrideRemarks").focus();
    } else {
    document.tripExpense.action = '/throttle/updateOverrideTripSheetDuringClosure.do';
            document.tripExpense.submit();
    }
    }
    function saveTripStartDetails() {
    var tripStartKm = document.getElementById("tripStartKm").value;
            var tripStartHm = document.getElementById("tripStartHm").value;
            if (tripStartKm == "" || tripStartKm == "0.00") {
    alert('please enter tripStartKm');
            document.getElementById("tripStartKm").focus();
            return;
    }
    if (tripStartHm == "" || tripStartHm == "0.00") {
    alert('please enter tripStartHm');
            document.getElementById("tripStartHm").focus();
            return;
    } else {
    document.tripExpense.action = '/throttle/updateStartTripSheetDuringClosure.do';
            document.tripExpense.submit();
    }
    }
    function calTotalKM() {
    var startKM = document.getElementById("startKM").value;
            var endKM = document.getElementById("endOdometerReading").value;
            document.getElementById("totalKM").value = endKM - startKM;
    }
    function calSettelTotalKM() {
    var startKM = document.getElementById("startKM").value;
            var endKM = document.getElementById("setteledKm").value;
            var TotalSettelKM = endKM - startKM;
            document.getElementById("setteltotalKM").value = TotalSettelKM;
    }
    function calTotalHM() {
    var startHM = document.getElementById("startHM").value;
            var endHM = document.getElementById("endHM").value;
            document.getElementById("totalHrs").value = endHM - startHM;
    }
    function calSettelTotalHM() {
    var startHM = document.getElementById("startHM").value;
            var endHM = document.getElementById("setteledHm").value;
            var TotalSettelHm = endHM - startHM;
            document.getElementById("setteltotalHrs").value = TotalSettelHm;
    }
    function saveOdoApprovalRequest() {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var odoUsageRemarks = document.tripExpense.odoUsageRemarks.value;
            var requestType = 1; //odo request
            saveApprovalRequest(odoUsageRemarks, requestType, 1)

    }
    function saveOdoApprovalRequest(val) {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var remarks = document.tripExpense.odoUsageRemarks.value;
            var requestType = 1; //odo request
            var tripId = document.tripExpense.tripSheetId.value;
            remarks = remarks.trim();
            var rcmExp = document.tripExpense.estimatedExpense.value;
            var nettExp = document.tripExpense.nettExpense.value;
            var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp + "&remarks=" + remarks;
            document.tripExpense.action = url;
            document.tripExpense.submit();
    }
    function saveOdoReApprovalRequest() {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var odoUsageRemarks = document.tripExpense.odoUsageReApprovalRemarks.value;
            var requestType = 1; //odo request
            saveApprovalRequest(odoUsageRemarks, requestType, 2)
    }
    function saveExpDeviationApprovalRequest() {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var remarks = document.tripExpense.expDeviationRemarks.value;
            var requestType = 2; //exp request
            saveApprovalRequest(remarks, requestType, 3)
    }
    function saveExpDeviationApprovalRequest(val) {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var remarks = document.tripExpense.expDeviationRemarks.value;
            var requestType = 2; //exp request
            //saveApprovalRequest(remarks, requestType, 3);

            var tripId = document.tripExpense.tripSheetId.value;
            remarks = remarks.trim();
            var rcmExp = document.tripExpense.estimatedExpense.value;
            var nettExp = document.tripExpense.nettExpense.value;
            var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp + "&remarks=" + remarks;
            document.tripExpense.action = url;
            document.tripExpense.submit();
    }
    function saveexpDeviationReApprovalRequest() {
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            var remarks = document.tripExpense.expDeviationReApprovalRemarks.value;
            var requestType = 2; //exp request
            saveApprovalRequest(remarks, requestType, 4)
    }

    var httpReq;
            var temp = "";
            function saveApprovalRequest(remarks, requestType, val) {
            var tripId = document.tripExpense.tripSheetId.value;
                    remarks = remarks.trim();
                    var rcmExp = document.tripExpense.estimatedExpense.value;
                    var nettExp = document.tripExpense.nettExpense.value;
                    if (remarks != '') {
            var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&remarks=" + remarks + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp;
                    //alert(url);
                    if (window.ActiveXObject)
            {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
            httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function () {
                    processOdoApprovalRequest(requestType, val);
                    };
                    httpReq.send(null);
            } else {
            alert('please enter request description');
            }

            }

    function processOdoApprovalRequest(requestType, val) {
    if (httpReq.readyState == 4) {
    if (httpReq.status == 200) {
    temp = httpReq.responseText.valueOf();
            //alert(temp);
            if (temp != '0' && temp != '' && temp != null && temp != 'null') {
    if (val == 1) {
    $('#odoApprovalRequest').hide();
            $('#odoPendingApproval').show();
    }
    if (val == 2) {
    $('#odoReApprovalRequest').hide();
            $('#odoPendingReApproval').show();
    }
    if (val == 3) {
    $('#expDeviationApprovalRequest').hide();
            $('#expDeviationPendingApproval').show();
    }
    if (val == 4) {
    $('#expDeviationReApprovalRequest').hide();
            $('#expDeviationPendingReApproval').show();
    }
    }
    } else {
    //alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
    }
    }
    document.tripExpense.action = '/throttle/viewTripExpense.do';
            document.tripExpense.submit();
    }

    function calculateDays2() {

    var fromDate = document.getElementById("vehicleactreportdate").value;
            var toDate = document.getElementById("vehicleloadreportdate").value;
            var tripSheetId = document.getElementById("tripSheetId").value;
            fromDate = fromDate.split("-");
            toDate = toDate.split("-");
            var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2]);
            var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2]);
            var days1 = (date2 - date1) / 1000 / 60 / 60 / 24;
            days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
            document.getElementById("totalDays2").value = days1;
            daysCheck = days1;
            if (daysCheck >= 1){
    document.getElementById("detentionTable").style.display = 'block';
    } else{
    document.getElementById("detentionTable").style.display = 'none';
    }


    }




    function calculateDays() {

    var fromDate = document.getElementById("startDate").value;
            var toDate = document.getElementById("endDate").value;
            fromDate = fromDate.split("-");
            toDate = toDate.split("-");
            var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2]);
            var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2]);
            var days1 = (date2 - date1) / 1000 / 60 / 60 / 24;
            days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
            document.getElementById("totalDays1").value = days1 + 1;
            var stTimeIds = document.getElementById("startTime").value;
            var hour1 = document.getElementById("tripEndHour").value;
            var minute1 = document.getElementById("tripEndMinute").value;
            var endTimeIds = hour1 + ":" + minute1 + ":" + "00";
            var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2] + ' ' + stTimeIds);
            var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2] + ' ' + endTimeIds);
            var days1 = (date2 - date1) / 1000 / 60 / 60;
            days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
            document.getElementById("tripTransitHour").value = Math.floor(days1);
            var fromDate = document.getElementById("vehicleactreportdate").value;
            var endDate = document.getElementById("endDate").value;
            var factoryInHours = document.getElementById("vehicleactreporthour").value;
            var factoryInMin = document.getElementById("vehicleactreportmin").value;
            var factoryTimeIds = factoryInHours + ":" + factoryInMin + ":" + "00";
            fromDate = fromDate.split("-");
            toDate = endDate.split("-");
            var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2] + ' ' + factoryTimeIds);
            var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2] + ' ' + endTimeIds);
            var days1 = (date2 - date1) / 1000 / 60 / 60;
            days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
            document.getElementById("tripFactoryToIcdHour").value = Math.floor(days1);
    }

    function setEndTime() {
    var endtime = document.getElementById('endTime').value;
            var endtimes = endtime.split(":");
            document.tripExpense.tripEndHour.value = endtimes[0];
            document.tripExpense.tripEndMinute.value = endtimes[1];
    }
    function currentTime() {
//                alert("hellogopi");
    var date, curr_hour, curr_minute;
            date = new Date();
            curr_hour = date.getHours();
            curr_minute = date.getMinutes();
            if (parseInt(curr_hour) < 10) {
    curr_hour = '0' + curr_hour;
    }
    if (parseInt(curr_minute) < 10) {
    curr_minute = '0' + curr_minute;
    }
    document.getElementById("vehicleloadreporthour").value = curr_hour;
            document.getElementById("vehicleloadreportmin").value = curr_minute;
            document.getElementById("vehicleactreporthour").value = curr_hour;
            document.getElementById("vehicleactreportmin").value = curr_minute;
            document.getElementById("tripEndHour").value = curr_hour;
            document.getElementById("tripEndMinute").value = curr_minute;
    }

</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
    Date today = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String todayDate = sdf.format(today);
%>

<script type="text/javascript">
            $(document).ready(function () {
    $("#datepicker").datepicker({
    showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

    });
    });
            $(function () {
//alert("cv");
            $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
            });
            });</script>
<script type="text/javascript" language="javascript">
            function submitPage() {
            var expenseType = document.getElementsByName("expenseType");
                    var expenseName = document.getElementsByName("expenseName");
                    var check = 0;
                    for (var i = 0; i < expenseType.length; i++) {
            if (expenseName[i].value != 0) {

            if (expenseType[i].value == 0) {
            check = 1;
            }
            }
            if (check == 1) {
            alert("please select the expense type in row" + (i + 1));
                    document.getElementById("expenseType" + i).focus();
                    return;
            }
            }

            document.getElementById("spinner").style.display = "block";
                    document.getElementById("loader").style.display = "block";
                    document.getElementById('loader').className += 'ui-loader-background';
                    document.tripExpense.action = '/throttle/saveTripOtherExpense.do';
                    document.tripExpense.submit();
            }

    function submitPageDone() {
    var txt;
            var r = confirm("Press a button!");
            if (r == true) {
    document.tripExpense.action = '/throttle/saveDoneTripOtherExpense.do';
            document.tripExpense.submit();
    } else {
    txt = "You pressed Cancel!";
    }
    }
    function closeTrip() {

    if (document.getElementById("commodityName").value == "" || document.getElementById("commodityName").value == "0"){
    alert('please select Commodity Category');
            document.getElementById("commodityName").focus();
            return;
    } else if (isEmpty(document.getElementById("endDate").value)) {
    alert('Alert!!please go to Edit Trip End Details Tab,fill the proper data and then proceed.');
            document.getElementById("endDate").focus();
            return;
    }
//        var confirms = confirm("Do you want to continue this commodity?");
////            alert(confirms);
//            if (confirms == true) {
////                alert("You Pressed Yes")
//                $("#Save").hide();
//                document.getElementById("spinner").style.display = "block";
//                document.getElementById("loader").style.display = "block";
//                document.getElementById('loader').className += 'ui-loader-background';
//                document.tripExpense.action = '/throttle/updateEndTripSheetDuringClosure.do';
//                document.tripExpense.submit();
//            } else {
////                alert("You Pressed No")
//                $("#Save").show();
//            }
    document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            document.tripExpense.action = '/throttle/closeTrip.do';
            document.tripExpense.submit();
    }


    function setExpenseTypeDetails(value, sno) {
    document.getElementById('marginValue' + sno).readOnly = true;
            if (value == '1') {//bill to customer
    document.getElementById('taxPercentage' + sno).value = '';
            document.getElementById('expenses' + sno).value = '';
            //alert("fgd");
            document.getElementById('billModeTemp' + sno).disabled = false;
            document.getElementById('taxPercentage' + sno).readOnly = false;
    } else { // do not bill to customer
    document.getElementById('billMode' + sno).value = 0;
            document.getElementById('billModeTemp' + sno).value = 0;
            document.getElementById('taxPercentage' + sno).value = 0;
            document.getElementById('taxPercentage' + sno).readOnly = true;
            document.getElementById('billModeTemp' + sno).disabled = true;
            document.getElementById('marginValue' + sno).value = 0;
            document.getElementById('expenses' + sno).value = '0';
            document.getElementById('netExpense' + sno).value = '0';
    }
    }
    function setBillMode(sno) {
    document.getElementById('billMode' + sno).value = document.getElementById('billModeTemp' + sno).value;
            var billModeValue = document.getElementById('billModeTemp' + sno).value;
            document.getElementById('marginValue' + sno).value = 0;
            if (billModeValue == '2') {//Not pass thru
    document.getElementById('marginValue' + sno).readOnly = false;
    } else {
    document.getElementById('marginValue' + sno).readOnly = true;
    }
    }
    function checkAvailableAdvance(sno) {

    var avaialbleAdvance = document.getElementById("availableAdvance").value;
            var expesnes = document.getElementsByName("expenses");
            //    for(var i=0;i<expesnes.length;i++){

            var enteredAmount = document.getElementById("expenses" + sno).value;
            alert("enteredAmount:" + enteredAmount + "avaialbleAdvance:" + avaialbleAdvance);
            if (parseFloat(avaialbleAdvance) >= parseFloat(enteredAmount)) {
    avaialbleAdvance = parseFloat(avaialbleAdvance) - parseFloat(enteredAmount);
            document.getElementById("availableAdvance").value = avaialbleAdvance;
    } else {
    //document.getElementById("expenses"+sno).value="";
    alert("your Amount is exceeding .U want to continue..?");
            avaialbleAdvance = parseFloat(avaialbleAdvance) - parseFloat(enteredAmount);
            document.getElementById("availableAdvance").value = avaialbleAdvance;
            // return;
    }
    calTotalExpenses(sno);
            //  }

    }
    function getAvailableAdvance() {
    var paidAdvance = document.getElementById("paidAdvance").value;
            var saveExpesne =<%=request.getAttribute("totalExpesne")%>;
            if (saveExpesne == 'null' || saveExpesne == null) {
    saveExpesne = 0;
    }
    var expesnes = document.tripExpense.expenses;
            var enteredAmount = "";
            for (var i = 0; i < expesnes.length; i++) {
    enteredAmount = enteredAmount + document.getElementById("expenses" + i).value;
    }
    document.getElementById("availableAdvance").value = paidAdvance - (enteredAmount + saveExpesne);
            document.getElementById("availableAdvanceSpan").innerHTML = "Available:  " + (paidAdvance - (enteredAmount + saveExpesne)) + "Rs/-";
    }
    function calTotalExpenses(sno) {

    var marginValue = document.getElementById('marginValue' + sno).value;
            var tax = document.getElementById('taxPercentage' + sno).value;
            if (tax == '') {
    tax = '0';
    }
    if (marginValue == '') {
    marginValue = '0';
    }

    var expenseAmount = document.getElementById('expenses' + sno).value;
            var totalAmount = parseFloat(expenseAmount) + parseFloat(marginValue);
            var totalAmount = totalAmount + (totalAmount * parseFloat(tax) / 100);
            //                var netAmount =  Math.round(parseFloat(totalAmount).toFixed(0))  + Math.round(parseFloat(expenseAmount).toFixed(0))   ;
            //                document.getElementById('netExpense'+sno).value = netAmount;
            document.getElementById('netExpense' + sno).value = totalAmount.toFixed(2);
    }

    function totalNetAmount() {
    var tax = document.getElementsByNames("").value;
            var expenseAmount = document.getElementById('expenses').value;
            var totalAmount = tax / 100 * expenseAmount;
            var netAmount = Math.round(parseFloat(totalAmount).toFixed(0)) + Math.round(parseFloat(expenseAmount).toFixed(0));
            document.getElementById('netExpense').value = netAmount;
    }

    function openPopup1(tripExpenseId) {
    var url = '/throttle/viewExpensePODDetails.do?tripExpenseId=' + tripExpenseId;
            window.open(url, 'PopupPage', 'height=500,width=700,scrollbars=yes,resizable=yes');
    }

    function popUp(url) {
    var http = new XMLHttpRequest();
            http.open('HEAD', url, false);
            http.send();
            if (http.status != 404) {
    popupWindow = window.open(
            url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
    } else {
    var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
    }
    }

    function hideFactoryDetails() {
    var movementType = document.getElementById("movementType").value;
            if (movementType == '3') {
    $("tr.excludeRepo").hide();
            $("td.slN").hide();
            $("td.boE").hide();
    } else if (movementType == '2') {
    $("tr.excludeRepo").show();
            $("td.slN").hide();
            $("td.boE").show();
//            $("td.boE").addClass("text2");
    } else if (movementType == '1' || movementType == '6') {
    $("tr.excludeRepo").show();
            $("td.slN").show();
            $("td.boE").hide();
//            $("td.slN").addClass("text2");
    } else {
    $("td.slN").hide();
            $("td.boE").hide();
    }
    }

</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
            $(document).ready(function () {
    $("#tabs").tabs();
    });</script>

<script>
            function submitExpense(){
            document.tripExpense.action = '/throttle/updateExpense.do';
                    document.tripExpense.submit();
            }
</script>
<script>
    function

</script>


<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
    #index th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;

    }
    #tabHead th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Primary Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><spring:message code="general.label.home"  text="Home"/></li>
            <li>Primary Operation</li>
            <li class="active">Trip Closure</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <form name="tripExpense" method="post">
                <%
                Date today1 = new Date();
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                String endDate = sdf1.format(today1);
                String revenueStr = "";
                %>
                <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                    <tr id="exp_table" >
                        <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
                            <div class="tabs" align="left" style="width:300;">

                                <div id="first">
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>Expected Revenue: INR </b></font></td>
                                                    <td><font color="white"><b> <c:out value="${trip.orderRevenue}" /></b></font></td>

                                                </tr>
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>Projected Expense: INR </b></font></td>
                                                    <td> <font color="white"><b><c:out value="${trip.orderExpense}" /></b></font></td>

                                                </tr>
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>paid Expense:</b></font></td>
                                                    <td> <font color="white"><b> <c:out value="${trip.paidExpense}" /></b></font>

                                                        <input type="hidden" name="paidExpense" value='<c:out value="${trip.paidExpense}" />'>

                                                    </td>
                                                </tr>
                                                <c:set var="profitMargin" value="" />
                                                <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                <c:set var="paidExpense" value="${trip.paidExpense}" />
                                                <c:set var="profitMargin" value="${trip.orderRevenue - trip.orderExpense}" />
                                                <%
                                                            String profitMarginStr = "" + (Double) pageContext.getAttribute("profitMargin");
                                                            revenueStr = "" + (String) pageContext.getAttribute("orderRevenue");
                                                            float profitPercentage = 0.00F;
                                                            if (!"".equals(revenueStr) && !"".equals(profitMarginStr)) {
                                                                profitPercentage = Float.parseFloat(profitMarginStr) * 100 / Float.parseFloat(revenueStr);
                                                            }


                                                %>
                                                <tr id="exp_table" >
                                                    <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                    <td> <font color="white"><b> <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>
                                                        <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                    </td>

                                                </tr>
                                            </table>
                                        </c:forEach>
                                    </c:if>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <br>


                <br>
                <br>
                <br>
                <br>
                <div id="spinner" class="spinner" style="display:none;" >
                    <img id="img-spinner" src="images/page-loader2.gif" alt="Loading" />
                </div>

                <table width="100%">
                    <% int loopCntr = 0;%>
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">
                            <% if (loopCntr == 0) {%>
                            <tr id="index" height="30">
                                <td >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                <td >Trip Code: <c:out value="${trip.tripCode}"/></td>
                                <td >Customer:&nbsp;<c:out value="${trip.customerName}"/></td>
                                <td >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                <td >Status: <c:out value="${trip.status}"/>
                                    <input type="hidden" name="vendorId" value='1' />
                                    <input type="hidden" name="movementType" id="movementType" value='<c:out value="${trip.movementType}"/>' />
                                    <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                    <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                    <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' /></td>
                            <input type="hidden" name="tripType" id="tripType" value='<c:out value="${tripType}"/>' /></td>
                            <input type="hidden" name="statusId" id="statusId" value='<c:out value="${statusId}"/>' /></td>
                            </tr>
                            <% }%>
                            <% loopCntr++;%>
                        </c:forEach>
                    </c:if>
                </table>
                <div id="tabs" >
                    <ul class="nav nav-tabs">

                        <li class="active" data-toggle="tab"><a href="#systemExpDetail"><span>System Expenses</span></a></li>
                        <li data-toggle="tab"><a href="#otherExpDetail"><span>Other Expenses</span></a></li>
                        <li data-toggle="tab"><a href="#closure"><span>Closure Summary</span></a></li>
                        <li data-toggle="tab"><a href="#endDetail"><span>Edit Trip End Details</span></a></li>
                        <li data-toggle="tab"><a href="#editStartDetail"><span>Edit Trip Start Details</span></a></li>
                        <li data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                        <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s)/Route Course/Trip Plan</span></a></li>
                        <li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>
                        <li data-toggle="tab"><a href="#startDetail"><span >Trip Start Details</span></a></li>
                        <li data-toggle="tab"><a href="#endDetails"><span>Trip End Details</span></a></li>
                            <c:if test="${tripType == 1}">
                            <li data-toggle="tab"><a href="#podDetail"><span>Trip POD Details</span></a></li>
                            </c:if>
                            <c:if test="${fuelTypeId == 1002}">
                            <!--<li data-toggle="tab"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>-->
                            </c:if>
                        <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                    </ul>

                    <input type="hidden" id="ownership" name="ownership" value="<c:out value="${vehicleOwnerShip}"/>">


                    <div id="systemExpDetail"  class="tab-pane active" >
                        <c:if test = "${gpsKm == 0}" >
                            <c:if test = "${odometerApprovalStatus == null}" >
                                <div id="odoApprovalRequest" style="display:block;">

                                    <div class="text2" >Request Description</div>
                                    <br>
                                    <textarea name="odoUsageRemarks" style="width:600px;height:80px;">GPS data is un available. please approve trip closure based on odometer and reefer reading.</textarea>
                                    <br>
                                    <br>
                                    <input type="button" style="width:130px;" class="btn btn-success" onclick="saveOdoApprovalRequest('New');" value="request approval" />
                                </div>
                                <br>
                                <div id="odoPendingApproval" style="display:none;" class="text1" >
                                    <br>
                                    request has been submitted.
                                    <br>
                                    <br>
                                    <b>odometer usage for expense calculation request is pending approval</b>
                                </div>
                            </c:if>
                            <c:if test = "${odometerApprovalStatus == 0}" >
                                <br>
                                <br>
                                <div class="text2" >Request Status: <b>request pending approval</b></div>
                                <br>
                            </c:if>
                            <c:if test = "${odometerApprovalStatus == 2}" >
                                <div id="odoReApprovalRequest" style="display:block;">
                                    <div class="text2" >Request Status : <b>request rejected</b></div>
                                    <br>
                                    <div class="text2" >Request Description</div>
                                    <br>
                                    <textarea name="odoUsageReApprovalRemarks"  style="width:600px;height:80px;"></textarea>
                                    <br>
                                    <br>
                                    <input type="button" style="width:150px;" class="button" onclick="saveOdoReApprovalRequest();" value="request re approval" />
                                </div>
                                <br>
                                <div id="odoPendingReApproval" style="display:none;" class="text1">
                                    <br>
                                    request has been submitted
                                    <br>
                                    <br>
                                    <b>odometer usage for expense calculation re-request is pending approval</b>
                                </div>
                            </c:if>
                        </c:if>

                        <%
                                    Float estimatedExpense = 0.00F;
                                    Float totalExpense = 0.00F;
                                    Float paidAdvance = 0.00F;
                                    Float dieselAmount = 0.00F;
                                    if (request.getAttribute("dieselCost") != null) {
                                        dieselAmount = Float.parseFloat((String) request.getAttribute("dieselCost"));
                                    }
                                    if (request.getAttribute("paidAdvance") != null) {
                                        paidAdvance = Float.parseFloat((String) request.getAttribute("paidAdvance"));
                                    }
                                    if (request.getAttribute("estimatedExpense") != null) {
                                        estimatedExpense = Float.parseFloat((String) request.getAttribute("estimatedExpense"));
                                    }
                                    String tripType = (String) request.getAttribute("tripType");
                                    Float secTollCost = 0.00F;


                                    Float secAddlTollCost = 0.00F;
                                    Float secMiscCost = 0.00F;
                                    Float secParkingAmount = 0.00F;
                                    if (request.getAttribute("secTollAmount") != null && !"".equals(request.getAttribute("secTollAmount"))) {
                                        secTollCost = Float.parseFloat((String) request.getAttribute("secTollAmount"));
                                        secAddlTollCost = Float.parseFloat((String) request.getAttribute("secAddlTollAmount"));
                                        secMiscCost = Float.parseFloat((String) request.getAttribute("secMiscAmount"));
                                        secParkingAmount = Float.parseFloat((String) request.getAttribute("secParkingAmount"));
                                    }

                                    Float tollRate = Float.parseFloat((String) request.getAttribute("tollAmount"));
                                    Float driverBattaPerDay = Float.parseFloat((String) request.getAttribute("driverBatta"));
                                    Float driverIncentivePerKm = Float.parseFloat((String) request.getAttribute("driverIncentive"));
                                    Float fuelPrice = Float.parseFloat((String) request.getAttribute("fuelPrice"));
                                    Float milleage = Float.parseFloat((String) request.getAttribute("milleage"));
                                    Float reeferMileage = Float.parseFloat((String) request.getAttribute("reeferConsumption"));

                                    Float runKm = Float.parseFloat((String) request.getAttribute("runKm"));
                                    Float runHm = Float.parseFloat((String) request.getAttribute("runHm"));

                                   /*
    */
                                    String totalDaysValue = (String) request.getAttribute("totaldays");
                                    int gpsKm = Integer.parseInt((String) request.getAttribute("gpsKm"));
                                    int gpsHm = Integer.parseInt((String) request.getAttribute("gpsHm"));
                                    Float totalDays;
                                    if(totalDaysValue != null){
                                    totalDays = Float.parseFloat((String) request.getAttribute("totaldays"));
                                    }else{
                                    totalDays = Float.parseFloat("0.00");
                                    }
                                    Float bookedExpense = Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                    Float miscRate = Float.parseFloat((String) request.getAttribute("miscValue"));
                                    int driverCount = 0;
                                    if (request.getAttribute("driverCount") != null) {
                                        driverCount = Integer.parseInt((String) request.getAttribute("driverCount"));
                                    }
                                    //out.println("driverCount"+driverCount);
                                    Float extraExpenseValue = 0.00F;
                                    extraExpenseValue = Float.parseFloat((String) request.getAttribute("extraExpenseValue"));
                                    Float dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                                    Float driverBatta = totalDays * driverBattaPerDay * driverCount;
                                    Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                                    Float dieselCost = dieselUsed * fuelPrice;
                                    Float tollCost = runKm * tollRate;
                                    Float driverIncentive = runKm * driverIncentivePerKm;
                                    //out.println("tollRate"+tollRate);

                                    Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                                    String fuelTypeId = (String) request.getAttribute("fuelTypeId");
                                    Float preColingAmount = 1.5f;
                                    Float fuelUsed = 0.00F;
                                    Float fuelCost = 0.00F;
                                    if ("2".equals(tripType)) {//secondary
                                        fuelUsed = (runKm / milleage);
                                        fuelCost = fuelUsed * fuelPrice;
                                        if (fuelTypeId.equals("1003")) {
                                            systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount + (fuelPrice * preColingAmount);
                                            preColingAmount = preColingAmount * fuelPrice;
                                        } else {
                                            systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount;
                                            preColingAmount = 0.0f;
                                        }
                                    }

                                   // Float nettExpense = systemExpense + bookedExpense;
                                    int tripCount = (Integer) request.getAttribute("tripCnt");
                                    Float totalPaidExpense = dieselAmount + paidAdvance;
                                    Float nettExpense = totalPaidExpense + bookedExpense;
                                    Float estActualExpDiff = estimatedExpense - nettExpense;
                        %>

                        <input type="hidden" name="vehicleId" value='<c:out value="${vehicleId}"/>' />
                        <input type="hidden" name="estimatedExpense" value='<%=new DecimalFormat("#0.00").format(estimatedExpense)%>' />
                        <input type="hidden" name="extraExpenseValue" value='<%=new DecimalFormat("#0.00").format(extraExpenseValue)%>' />
                        <input type="hidden" name="tollRate" value='<%=new DecimalFormat("#0.00").format(tollRate)%>' />

                        <input type="hidden" name="secTollCost" value='<%=new DecimalFormat("#0.00").format(secTollCost)%>' />
                        <input type="hidden" name="secAddlTollCost" value='<%=new DecimalFormat("#0.00").format(secAddlTollCost)%>' />
                        <input type="hidden" name="secMiscCost" value='<%=new DecimalFormat("#0.00").format(secMiscCost)%>' />
                        <input type="hidden" name="secParkingAmount" value='<%=new DecimalFormat("#0.00").format(secParkingAmount)%>' />

                        <input type="hidden" name="driverBattaPerDay" value='<%=new DecimalFormat("#0.00").format(driverBattaPerDay)%>' />
                        <input type="hidden" name="driverIncentivePerKm" value='<%=new DecimalFormat("#0.00").format(driverIncentivePerKm)%>' />
                        <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                        <input type="hidden" name="mileage" value='<%=new DecimalFormat("#0.00").format(milleage)%>' />
                        <input type="hidden" name="reeferMileage" value='<%=new DecimalFormat("#0.00").format(reeferMileage)%>' />
                        <input type="hidden" name="runKm" value='<%=new DecimalFormat("#0.00").format(runKm)%>' />
                        <input type="hidden" name="runHm" value='<%=new DecimalFormat("#0.00").format(runHm)%>' />
                        <input type="hidden" name="totalDays" value='<%=new DecimalFormat("#0.00").format(totalDays)%>' />

                        <input type="hidden" name="dieselUsed" value='<%=new DecimalFormat("#0.00").format(dieselUsed)%>' />
                        <input type="hidden" name="dieselCost" value='<%=new DecimalFormat("#0.00").format(dieselCost)%>' />
                        <input type="hidden" name="tollCost" value='<%=new DecimalFormat("#0.00").format(tollCost)%>' />
                        <input type="hidden" name="driverIncentive" value='<%=new DecimalFormat("#0.00").format(driverIncentive)%>' />
                        <input type="hidden" name="driverBatta" value='<%=new DecimalFormat("#0.00").format(driverBatta)%>' />
                        <input type="hidden" name="miscRate" value='<%=new DecimalFormat("#0.00").format(miscRate)%>' />
                        <input type="hidden" name="miscValue" value='<%=new DecimalFormat("#0.00").format(miscValue)%>' />
                        <input type="hidden" name="bookedExpense" value='<%=new DecimalFormat("#0.00").format(bookedExpense)%>' />
                        <input type="hidden" name="preColingAmount" value='<%=new DecimalFormat("#0.00").format(preColingAmount)%>' />
                        <input type="hidden" name="nettExpense" value='<%=new DecimalFormat("#0.00").format(nettExpense)%>' />
                        <input type="hidden" name="paidAdvance" id="paidAdvance" value='<%=new DecimalFormat("#0.00").format(paidAdvance)%>' />
                        <input type="hidden" name="dieselAmount" value='<%=new DecimalFormat("#0.00").format(dieselAmount)%>' />

                        <input type="hidden" name="tripType" value='<c:out value="${tripType}" />' />
                        <input type="hidden" name="gpsKm" value='<c:out value="${gpsKm}" />' />
                        <input type="hidden" name="gpsHm" value='<c:out value="${gpsHm}" />' />


                        <c:if test = "${tripType == 1 }" >
                            <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1))  }" >
                                <br>
                                <br>
                                <div id="expenseDiv" style="display:none ">
                                    <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <td  class="text1" >Diesel Used</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(dieselUsed)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Diesel Rate / Ltr (INR)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Diesel Cost (1)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(dieselCost)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Toll Cost (2)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(tollCost)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Driver Incentive (3)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(driverIncentive)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Driver Bhatta (4)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(driverBatta)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >MIS (@kmrun*<c:out value="${miscValue}"/>*5%) (5)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(miscValue)%></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" >&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >Total Expense (System Computed)</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                        <input type="hidden" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                        </tr>
                                    </table>
                                </div>

                                <div id="hiredDiv" style="display: none;">
                                    <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <td  class="text1" >Hired Vehicle Charges</td>
                                            <td  class="text1" align="right" ><c:out value="${orderExpense}" /></td>
                                        </tr>

                                    </table>
                                    <input type="hidden" name="hiredCharges" id="hiredCharges" value="<c:out value="${orderExpense}" />">
                                </div>
                                <div id="paidExpense" style="display:none; margin:1%;" >
                                    <table border="0" style=" outline: 3px solid #5BC0DE;border-color:#5BC0DE; "  align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr >
                                            <td  class="text2" colspan="3" align="center">Trip Expense Summary</td>

                                        </tr>
                                        <tr >
                                            <td  class="text1" >Diesel Cost</td>
                                            <td  class="text1" ><b>:</b></td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(dieselAmount)%></td>
                                        </tr>
                                        <tr>
                                            <td  class="text1" >Advance  Paid</td>
                                            <td  class="text1" ><b>:</b></td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(paidAdvance)%></td>
                                        </tr>
                                        <tr>
                                            <td  class="text1" >Total  Paid</td>
                                            <td  class="text1" ><b>:</b></td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(totalPaidExpense)%></td>
                                        </tr>

                                    </table>
                                </div>
                            </c:if>

                        </c:if>
                        <script>
                                    var checkVehicleType = document.getElementById("ownership").value;
                                    //alert(checkVehicleType);
                                    if (parseInt(checkVehicleType) == 3) {
                            document.getElementById("expenseDiv").style.display = 'none';
                                    document.getElementById("hiredDiv").style.display = 'block';
                            } else {
                            document.getElementById("paidExpense").style.display = 'block';
                                    document.getElementById("expenseDiv").style.display = 'none';
                                    document.getElementById("hiredDiv").style.display = 'none';
                            }
                        </script>

                        <c:if test = "${tripType == 2 }" >
                            <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                                <br>
                                <br>
                                <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td  class="text1" >Fuel Used</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(fuelUsed)%></td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Fuel Rate / Kg (INR)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                    </tr>
                                    <c:if test="${fuelTypeId == 1003}">
                                        <tr>
                                            <td class="text1" >Pre - Cooling CNG</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td class="text1" >Fuel Cost (1)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(fuelCost)%></td>
                                    </tr>
                                    <input type="hidden" name="fuelUsed" value='<%=new DecimalFormat("#0.00").format(fuelUsed)%>' />
                                    <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                                    <input type="hidden" name="fuelCost" value='<%=new DecimalFormat("#0.00").format(fuelCost)%>' />
                                    <tr>
                                        <td class="text1" >Toll Cost (2)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(secTollCost)%></td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Addl Toll Cost (3)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(secAddlTollCost)%></td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Misc Cost (4)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(secMiscCost)%></td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Parking Cost (5)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(secParkingAmount)%></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" >&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Total Expense (System Computed)</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                    <input type="hidden" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                    </tr>
                                </table>
                            </c:if>
                        </c:if>
                        <br>
                        <br>
                        <br>

                        <c:if test = "${approveProcessHistory != null}" >
                            <br>
                            <br><br>
                            <br>
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td class="text1" colspan="9" ><center style="color:black;font-size: 14px;">
                                    Approve Process History
                                </center> </td>
                                </tr>
                                <tr id="index" height="30">
                                    <td  height="30" >Request By</td>
                                    <td  height="30" >Request On</td>
                                    <td  height="30" >Request Remarks</td>
                                    <td  height="30" >Run KM</td>
                                    <td  height="30" >Run HM</td>
                                    <td  height="30" >Status</td>
                                    <td  height="30" >Approver Remarks</td>
                                    <td  height="30" >Approver</td>
                                    <td  height="30" >Approver Processed On</td>
                                </tr>
                                <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                    <c:if test = "${approveHistory.requestType == 1}" >
                                        <tr >
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.runKm}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.runHm}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                            <td class="text1" height="30" ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                        </tr>
                                    </c:if>

                                </c:forEach >
                            </table>
                        </c:if>
                        <br>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:90px;height:35px;font-weight: bold;padding: 2px;"/></a>
                        </center>
                        <br>

                    </div>
                    <div id="closure" style="width: auto">
                        <br>
                        <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                            <div id="closureDiv" style="display:block;">
                                <table  border="1" style=" outline: 3px solid #5BC0DE;" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td class="text1" >RCM Cost</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td class="text1" >System Expense + Booked Expense</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${tripType == 2 && fuelTypeId == 1002}">
                                        <tr>
                                            <td class="text1" >System Expense + Booked Expense</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${tripType == 2 && fuelTypeId == 1003}">
                                        <tr>
                                            <td class="text1" >Pre - Cooling CNG</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" >System Expense + Booked Expense + Precooling Price</td>
                                            <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>

                                    <tr>
                                        <td class="text1" >Deviation</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float deviation = estimatedExpense - (nettExpense);
                                                        Float deviationPercentage = (deviation / estimatedExpense) * 100;
                                                        if (deviation > 0) {
                                            %>
                                            <font color="green">
                                            <%=new DecimalFormat("#0.00").format(deviation)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                            </font>
                                            <%} else {%>
                                            <font color="red">
                                            <%=new DecimalFormat("#0.00").format(deviation)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                            </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Profit Margin(%)</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float profitValue = Float.parseFloat(revenueStr) - (nettExpense);
                                                        Float profitMargin = (profitValue / Float.parseFloat(revenueStr)) * 100;
                                                        if (profitValue > 0) {
                                            %>
                                            <font color="green"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                            <%} else {%>
                                            <font color="red"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                            <%}%>
                                        </td>
                                    </tr>


                                </table>
                            </div>
                            <div id="closureHired" style="display: none;">
                                <table  border="1" style=" outline: 3px solid #5BC0DE;"  class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td class="text1" >RCM Cost</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td class="text1" >System Expense + Booked Expense</td>
                                            <td  class="text1" align="right" >
                                                <%

                                               Float exp1= Float.parseFloat((String) request.getAttribute("estimatedExpense"));
                                               Float book1=Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                               totalExpense=exp1+book1;
                                                %>
                                                <%=new DecimalFormat("#0.00").format(totalExpense)%>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td class="text1" >Deviation</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float deviation1 = estimatedExpense - (totalExpense);
                                                        Float deviationPercentage1 = (deviation1 / estimatedExpense) * 100;
                                                        if (deviation > 0) {
                                            %>
                                            <font color="green">
                                            <%=new DecimalFormat("#0.00").format(deviation1)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage1)%> &nbsp;%
                                            </font>
                                            <%} else {%>
                                            <font color="red">
                                            <%=new DecimalFormat("#0.00").format(deviation1)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage1)%> &nbsp;%
                                            </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Profit Margin(%)</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float profitValue1 = Float.parseFloat(revenueStr) - (totalExpense);
                                                        Float profitMargin1 = (profitValue1 / Float.parseFloat(revenueStr)) * 100;
                                                        if (profitValue > 0) {
                                            %>
                                            <font color="green"><%=new DecimalFormat("#0.00").format(profitMargin1)%> </font>
                                            <%} else {%>
                                            <font color="red"><%=new DecimalFormat("#0.00").format(profitMargin1)%> </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="closureNew" style="display: none;">
                                <table  border="1" style=" outline: 3px solid #5BC0DE;" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td class="text1" >RCM Cost</td>
                                        <td  class="text1" align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td class="text1" >System Expense + Booked Expense</td>
                                            <td  class="text1" align="right" >
                                                <%

                                               Float exp2= totalPaidExpense;
                                               Float book2=Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                               totalExpense=exp2+book2;
                                                %>
                                                <%=new DecimalFormat("#0.00").format(totalExpense)%>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td class="text1" >Deviation</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float deviationNew = estimatedExpense - (totalExpense);
                                                        Float deviationPercentageNew = (deviationNew / estimatedExpense) * 100;
                                                        if (deviationNew > 0) {
                                            %>
                                            <font color="green">
                                            <%=new DecimalFormat("#0.00").format(deviationNew)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentageNew)%> &nbsp;%
                                            </font>
                                            <%} else {%>
                                            <font color="red">
                                            <%=new DecimalFormat("#0.00").format(deviationNew)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentageNew)%> &nbsp;%
                                            </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text1" >Profit Margin(%)</td>
                                        <td  class="text1" align="right" >
                                            <%
                                                        Float profitValueNew = Float.parseFloat(revenueStr) - (totalExpense);
                                                        Float profitMarginNew = (profitValueNew / Float.parseFloat(revenueStr)) * 100;
                                                        if (profitValueNew > 0) {
                                            %>
                                            <font color="green"><%=new DecimalFormat("#0.00").format(profitMarginNew)%> </font>
                                            <%} else {%>
                                            <font color="red"><%=new DecimalFormat("#0.00").format(profitMarginNew)%> </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <script>
                                var checkVehicleType1 = document.getElementById("ownership").value;
                                        if (parseInt(checkVehicleType1) == 3) {
                                document.getElementById("closureDiv").style.display = 'none';
                                        document.getElementById("closureHired").style.display = 'block';
                                } else {
                                document.getElementById("closureDiv").style.display = 'none';
                                        document.getElementById("closureHired").style.display = 'none';
                                        document.getElementById("closureNew").style.display = 'block';
                                }
                            </script>

                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>

                            <%
                                        if (estActualExpDiff > 0) {
                            %>  
                            <input type="button" class="btn btn-success" name="Save" style="width:80px;height:30px;padding:1px;font-weight: bold" value="close trip" onclick="closeTrip();" />
                            <%} else {%>

                            <c:if test = "${expenseDeviationApprovalStatus == null}" >
                                <div id="expDeviationApprovalRequest" style="display:block;">
                                    <br>
                                    <br>
                                    <div align="left" class="text2" >Request Description</div>
                                    <br><textarea name="expDeviationRemarks" style="width:600px;height:80px;"></textarea>
                                    <br>
                                    <br>
                                    <input type="button" style="width:130px;" class="btn btn-success"  onclick="saveExpDeviationApprovalRequest('New');" value="request approval" />
                                </div>
                                <br>
                                <div id="expDeviationPendingApproval" style="display:none;" class="text1">
                                    <br>
                                    request has been submitted
                                    <br>
                                    <br>
                                    <b>expense deviation request is pending approval</b>
                                </div>
                            </c:if>

                            <c:if test = "${expenseDeviationApprovalStatus == 0}" >
                                <br>
                                <br>
                                <div class="text2" >Request Status: <b>request pending approval</b></div>
                                <br>
                            </c:if>
                            <c:if test = "${expenseDeviationApprovalStatus == 1}" >
                                <input type="button" class="btn btn-success" value="close trip" onclick="closeTrip();" style="width:80px;height:30px;padding:1px;font-weight: bold"/>
                            </c:if>
                            <c:if test = "${expenseDeviationApprovalStatus == 2}" >
                                <div id="expDeviationReApprovalRequest" style="display:block;">
                                    <div class="text2" >Request Status : <b>request rejected</b></div>
                                    <br>
                                    <div class="text2" >Request Description</div>
                                    <br><textarea name="expDeviationReApprovalRemarks"   style="width:600px;height:80px;"></textarea>
                                    <br>
                                    <br>
                                    <input type="button" style="width:150px;" class="button"  onclick="saveexpDeviationReApprovalRequest();" value="request re approval" />
                                </div>
                                <br>
                                <div id="expDeviationPendingReApproval" style="display:none;"  class="text1">
                                    <br>
                                    request has been submitted
                                    <br>
                                    <br>
                                    <b>expense deviation re-request is pending approval</b>
                                </div>
                            </c:if>



                            <%}%>


                            <br>
                            <br>
                            <c:if test = "${approveProcessHistory != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td class="text1" colspan="9" ><center style="color:black;font-size: 14px;">
                                        Approve Process History
                                    </center> </td>
                                    </tr>
                                    <tr id="index" height="30">
                                        <td  height="30" >Request By</td>
                                        <td  height="30" >Request On</td>
                                        <td  height="30" >Request Remarks</td>
                                        <td  height="30" >RCM Expense</td>
                                        <td  height="30" >Actual Expense</td>
                                        <td  height="30" >Status</td>
                                        <td  height="30" >Approver Remarks</td>
                                        <td  height="30" >Approver</td>
                                        <td height="30" >Approver Processed On</td>
                                    </tr>
                                    <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                        <c:if test = "${approveHistory.requestType == 2}" >
                                            <tr >
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.rcmExpense}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.nettExpense}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                                <td class="text1" height="30" ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                            </tr>
                                        </c:if>

                                    </c:forEach >
                                </table>
                            </c:if>
                        </c:if>
                        <c:if test = "${((gpsKm == 0) && (odometerApprovalStatus != 1)) }" >
                            <br>
                            <b>please pass through km usage process</b>

                        </c:if>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>

                    <div id="tripDetail">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr id="index" height="30px">
                                <td  colspan="6" >Trip Details</td>
                            </tr>

                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">


                                    <tr height="30">
                                        <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                    <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                        <td class="text1">CNote No(s)</td>
                                        <td class="text1">
                                            <c:out value="${trip.cNotes}" />
                                            <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                        </td>
                                        <td class="text1">Billing Type</td>
                                        <td class="text1">
                                            <c:out value="${trip.billingType}" />
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <!--                            <td class="text2">Customer Code</td>
                                                                    <td class="text2">BF00001</td>-->
                                        <td class="text2">Customer Name</td>
                                        <td class="text2">
                                            <c:out value="${trip.customerName}" />
                                            <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${trip.customerName}" />'>
                                            <input type="hidden" name="tripSheetId" Id="tripSheetId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                        </td>
                                        <td class="text2">Customer Type</td>
                                        <td class="text2" colspan="3" >
                                            <c:out value="${trip.customerType}" />
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1">Route Name</td>
                                        <td class="text1">
                                            <c:out value="${trip.routeInfo}" />
                                        </td>
                                        <!--                            <td class="text1">Route Code</td>
                                                                    <td class="text1" >DL001</td>-->
                                        <td class="text1">Reefer Required</td>
                                        <td class="text1" >
                                            <c:out value="${trip.reeferRequired}" />
                                        </td>
                                        <td class="text1">Order Est Weight (MT)</td>
                                        <td class="text1" >
                                            <c:out value="${trip.totalWeight}" />
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text2">Vehicle Type</td>
                                        <td class="text2">
                                            <c:out value="${trip.vehicleTypeName}" />
                                        </td>
                                        <td class="text2">Vehicle No</td>
                                        <td class="text2">
                                            <c:out value="${trip.vehicleNo}" />
                                            <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                                        </td>
                                        <td class="text2">Vehicle Capacity (MT)</td>
                                        <td class="text2">
                                            <c:out value="${trip.vehicleTonnage}" />

                                        </td>
                                    </tr>

                                    <tr height="30">
                                        <td class="text1">Veh. Cap [Util%]</td>
                                        <td class="text1">
                                            <c:out value="${trip.vehicleCapUtil}" />
                                        </td>
                                        <td class="text1">Special Instruction</td>
                                        <td class="text1">-</td>
                                        <td class="text1">Trip Schedule</td>
                                        <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                                    </tr>


                                    <tr height="30">
                                        <td class="text2">Driver </td>
                                        <td class="text2" colspan="5" >
                                            <c:out value="${trip.driverName}" />
                                        </td>

                                    </tr>
                                    <tr height="30">
                                        <td class="text1">Product Info </td>
                                        <td class="text1" colspan="5" >
                                            <c:out value="${trip.productInfo}" />
                                        </td>

                                    </tr>
                                </c:forEach>
                            </c:if>
                        </table>
                        <br/>
                        <br/>

                        <c:if test = "${expiryDateDetails != null}" >
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr id="index" height="30px">
                                    <td colspan="4" >Vehicle Compliance Check</td>
                                </tr>
                                <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                    <tr  height="30px">
                                        <td class="text2">Vehicle FC Valid UpTo</td>
                                        <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                    </tr>
                                    <tr  height="30px">
                                        <td class="text1">Vehicle Insurance Valid UpTo</td>
                                        <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                    </tr>
                                    <tr  height="30px">
                                        <td class="text2">Vehicle Permit Valid UpTo</td>
                                        <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                    </tr>
                                    <tr  height="30px">
                                        <td class="text2">Road Tax Valid UpTo</td>
                                        <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </c:if>

                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <c:if test="${tripType == 1}">
                        <div id="podDetail">
                            <c:if test="${viewPODDetails != null}">
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="tabHead" height="30px">
                                        <th width="50" >S No&nbsp;</th>
                                        <th >City Name</th>
                                        <th >POD file Name</th>
                                        <th >LR Number</th>
                                        <th >POD Remarks</th>
                                    </tr>
                                    <% int index2 = 1;%>
                                    <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                        <%
                                                    String classText3 = "";
                                                    int oddEven = index2 % 2;
                                                    if (oddEven > 0) {
                                                        classText3 = "text1";
                                                    } else {
                                                        classText3 = "text2";
                                                    }
                                        %>
                                        <tr>
                                            <td class="<%=classText3%>" ><%=index2++%></td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                            <td class="<%=classText3%>" ><a  href="JavaScript:popUp('/throttle/uploadFiles/Files/<c:out value="${viewPODDetails.podFile}"/>');"><c:out value="${viewPODDetails.podFile}"/></a></td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                        </tr>
                                    </c:forEach>

                                </table>
                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                    </c:if>
                    <div id="routeDetail">

                        <c:if test = "${tripPointDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <tr id="index" height="30px">
                                    <td height="30" >S No</td>
                                    <td  height="30" >Point Name</td>
                                    <td  height="30" >Type</td>
                                    <td  height="30" >Route Order</td>
                                    <td  height="30" >Address</td>
                                    <td  height="30" >Planned Date</td>
                                    <td  height="30" >Planned Time</td>
                                </tr>
                                <% int index2 = 1;%>
                                <c:forEach items="${tripPointDetails}" var="tripPoint">
                                    <%
                                                String classText1 = "";
                                                int oddEven = index2 % 2;
                                                if (oddEven > 0) {
                                                    classText1 = "text1";
                                                } else {
                                                    classText1 = "text2";
                                                }
                                    %>
                                    <tr>
                                        <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                        <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                    </tr>
                                </c:forEach >
                            </table>
                            <br/>

                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                        <tr>
                                            <td class="text1" width="150"> Estimated KM</td>
                                            <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                            <td class="text1" colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="text2" width="150"> Estimated Reefer Hour</td>
                                            <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                            <td class="text2" colspan="4">&nbsp;</td>
                                        </tr>

                                    </c:forEach>
                                </c:if>
                            </table>

                        </c:if>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <!--                <div id="preStart">
                    <c:if test = "${tripPreStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                        <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                                <td  colspan="4" >Pre Start Details</td>
                            </tr>
                            <tr >
                                <td class="text1" height="30" >Pre Start Date</td>
                                <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                                <td class="text1" height="30" >Pre Start Time</td>
                                <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30" >Pre Odometer Reading</td>
                                <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                            <c:if test = "${tripDetails != null}" >
                                <td class="text2">Pre Start Location / Distance</td>
                                <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                            </c:if>
                        </tr>
                        <tr>
                            <td class="text1" height="30" >Pre Start Remarks</td>
                            <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                        </tr>
                        </c:forEach >
                    </table>
                    <br/>
                    <br/>
                    <br/>
                     <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                    </center>
                    </c:if>
                    <br>
                    <br>
                </div>-->
                    <div id="startDetail">
                        <c:if test = "${tripStartDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:forEach items="${tripStartDetails}" var="startDetails">
                                    <tr id="index" height="30">
                                        <td  colspan="6" > Trip Start Details</td>
                                    </tr>
                                    <tr  height="30">
                                        <td class="text1" height="30" >Trip Planned Start Date</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Planned Start Time</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Start Reporting Date</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                    </tr>
                                    <tr  height="30">
                                        <td class="text2" height="30" >Trip Start Reporting Time</td>
                                        <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                        <td class="text2" height="30" >Trip Loading date</td>
                                        <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                        <td class="text2" height="30" >Trip Loading Time</td>
                                        <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                    </tr>
                                    <tr  height="30">
                                        <td class="text1" height="30" >Trip Loading Temperature</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Actual Start Date</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Actual Start Time</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                    </tr>
                                    <tr  height="30">
                                        <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                        <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                        <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                        <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                        <td class="text2" height="30" colspan="2" ></td>
                                    </tr>
                                </c:forEach >
                            </table>

                            <c:if test = "${tripUnPackDetails != null}" >
                                <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr id="index" height="30">
                                        <td width="20"  align="center" height="30" >Sno</td>
                                        <td  height="30" >Product/Article Code</td>
                                        <td  height="30" >Product/Article Name </td>
                                        <td  height="30" >Batch </td>
                                        <td  height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td  height="30" ><font color='red'>*</font>Uom</td>
                                        <td  height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                        <td  height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                        <td  height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                        <td  height="30" ><font color='red'>*</font>Shortage</td>
                                    </tr>


                                    <%int i1 = 1;%>
                                    <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                        <tr>
                                            <td><%=i1%></td>
                                            <td><input type="text"  class="form-control" name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly style="width:130px;height:40px;"/></td>
                                            <td><input type="text" class="form-control" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:130px;height:40px;"/>
                                                <input type="hidden" class="form-control" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>" />
                                                <input type="hidden" class="form-control" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                            </td>
                                            <td><input type="text" class="form-control" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:130px;height:40px;"   /></td>
                                            <td><input type="text" class="form-control" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:130px;height:40px;" /></td>
                                        </tr>
                                        <%i1++;%>
                                    </c:forEach>

                                    <br/>
                                    <br/>
                                    <br/>
                                </table>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>

                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </c:if>
                    </div>
                    <div id="editStartDetail">
                        <c:if test = "${tripStartDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:forEach items="${tripStartDetails}" var="startDetails">
                                    <tr id="index" height="30">
                                        <td  colspan="6" > Trip Start Details</td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" height="30" >Trip Planned Start Date</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Planned Start Time</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                        <td class="text1" height="30" >Trip Start Reporting Date</td>
                                        <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                    </tr>

                                    <tr height="30">
                                        <td class="text2" height="30" >Trip Loading Temperature</td>
                                        <td class="text2" height="30" >
                                            <input type="text" class="form-control" name="tripStartLoadTemp" id="tripStartLoadTemp"  value='<c:out value="${startDetails.loadingTemperature}" />' style="width:180px;height:40px;" />
                                            &nbsp;</td>
                                        <td class="text2" height="30" >Trip Actual Start Date</td>
                                        <td class="text2" height="30" >
                                            <input type="text" class="datepicker , form-control" name="tripStartDate" id="tripStartDate"  value='<c:out value="${startDetails.startDate}" />' style="width:180px;height:40px;"/>

                                        </td>
                                        <td class="text2" height="30" >Trip Actual Start Time</td>
                                        <td class="text2" height="30" >
                                            <c:set var="startTime" value="${startDetails.startTime}"/>
                                            <%
                                                        Object startTime = (Object) pageContext.getAttribute("startTime");
                                                        String stTime = (String) startTime;
                                                        String[] temp = null;
                                                        temp = stTime.split(":");
                                            %>
                                            HH: <select name="tripStartTimeHr" class="form-control" id="tripStartTimeHr" style="width:60px;height:40px;">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            MI: <select name="tripStartTimeMin" class="form-control" id="tripStartTimeMin" style="width:60px;height:40px;">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                                <option value='24'>24</option>
                                                <option value='25'>25</option>
                                                <option value='26'>26</option>
                                                <option value='27'>27</option>
                                                <option value='28'>28</option>
                                                <option value='29'>29</option>
                                                <option value='30'>30</option>
                                                <option value='31'>31</option>
                                                <option value='32'>32</option>
                                                <option value='33'>33</option>
                                                <option value='34'>34</option>
                                                <option value='35'>35</option>
                                                <option value='36'>36</option>
                                                <option value='37'>37</option>
                                                <option value='38'>38</option>
                                                <option value='39'>39</option>
                                                <option value='40'>40</option>
                                                <option value='41'>41</option>
                                                <option value='42'>42</option>
                                                <option value='43'>43</option>
                                                <option value='44'>44</option>
                                                <option value='45'>45</option>
                                                <option value='46'>46</option>
                                                <option value='47'>47</option>
                                                <option value='48'>48</option>
                                                <option value='49'>49</option>
                                                <option value='50'>50</option>
                                                <option value='51'>51</option>
                                                <option value='52'>52</option>
                                                <option value='53'>53</option>
                                                <option value='54'>54</option>
                                                <option value='55'>55</option>
                                                <option value='56'>56</option>
                                                <option value='57'>57</option>
                                                <option value='58'>58</option>
                                                <option value='59'>59</option>

                                            </select>
                                            <script>
                                                        document.getElementById("tripStartTimeHr").value = '<%=temp[0]%>';
                                                        document.getElementById("tripStartTimeMin").value = '<%=temp[1]%>';</script>
                                            &nbsp;</td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" height="30" >Trip Start Odometer Reading(KM)</td>
                                        <td class="text1" height="30" >
                                            <input type="text" class="form-control" name="tripStartKm" id="tripStartKm"  value='<c:out value="${startDetails.startOdometerReading}"/>' style="width:180px;height:40px;"/>
                                            &nbsp;</td>
                                        <td class="text1" height="30" >Trip Start Reefer Reading(HM)</td>
                                        <td class="text1" height="30" >
                                            <input type="text" class="form-control" name="tripStartHm" id="tripStartHm" value='<c:out value="${startDetails.startHM}"/>' style="width:180px;height:40px;" />
                                        </td>
                                        <td class="text1" height="30" colspan="2" ></td>
                                    </tr>
                                </c:forEach >
                            </table>


                            <br/>
                            <center>
                                <input type="button" class="btn btn-success" name="Save" style="width:220px;height:30px;font-weight: bold;padding:1px;" value="Update Trip Start Details" onclick="saveTripStartDetails();" />
                            </center>

                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </c:if>
                    </div>
                    <div id="endDetails">
                        <c:if test = "${tripEndDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:forEach items="${tripEndDetails}" var="endDetails">
                                    <tr id="index" height="30">
                                        <td  colspan="6" > Trip End Details</td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" height="30" >Trip Planned End Date</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                        <td class="text1" height="30" >Trip Planned End Time</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                        <td class="text1" height="30" >Trip Actual Reporting Date</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text2" height="30" >Trip Actual Reporting Time</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                        <td class="text2" height="30" >Trip Unloading Date</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                        <td class="text2" height="30" >Trip Unloading Time</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" height="30" >Trip Unloading Temperature</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                        <td class="text1" height="30" >Trip Actual End Date</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.endDate}" /></td>
                                        <td class="text1" height="30" >Trip Actual End Time</td>
                                        <td class="text1" height="30" > <c:out value="${endDetails.endTime}" /></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text2" height="30" >Trip End Odometer Reading(KM)</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                        <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                        <td class="text2" height="30" >Total Odometer Reading(KM)</td>
                                        <td class="text2" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" height="30" >Total Reefer Reading(HM)</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                        <td class="text1" height="30" >Total Duration Hours</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                        <td class="text1" height="30" >Total Days</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.totalDays}" />

                                    </tr>

                                </c:forEach >
                            </table>

                            <br/>
                            <br/>
                            <br/>

                        </c:if>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <div id="endDetail">
                        <c:if test = "${tripEndDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:forEach items="${tripEndDetails}" var="endDetails">
                                    <tr id="index" height="30">
                                        <td  colspan="4" >Enter Trip End Details</td>
                                    </tr>
                                    <tr  height="30">
                                        <td class="text1" height="30" >Planned End Date</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>

                                        <td class="text1" height="30" >Planned End Time</td>
                                        <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                    </tr>
                                    <tr class="excludeRepo"  height="30">
                                        <td class="text2"><font color="red">*</font>Vehicle Actual (Factory In) Date</td>
                                        <td class="text2"><input type="text" name="vehicleactreportdate" id="vehicleactreportdate" class="datepicker , form-control" onchange="calculateDays2();" readonly value="<c:out value="${endDetails.endReportingDate}" />" style="width:180px;height:40px;"></td>
                                        <td class="text2" height="25" ><font color="red">*</font>Vehicle Actual (Factory In) Time </td>
                                        <td class="text2" colspan="3" align="left" height="25" >
                                            HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                    </tr>
                                    <c:if test="${endDetails.endReportingDate == null}">
                                        <script>
                                                    document.tripExpense.vehicleactreportdate.value = '<%=endDate%>' / > ;</script>
                                        </c:if>
                                        <c:if test="${endDetails.endReportingTime != null}">
                                        <script>
                                                    var endtime = '<c:out value="${endDetails.endReportingTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.vehicleactreporthour.value = endtimes[0];
                                                    document.tripExpense.vehicleactreportmin.value = endtimes[1];</script>
                                        </c:if>
                                    <tr class="excludeRepo"  height="30">
                                        <td class="text1"><font color="red">*</font>Vehicle Actual (Factory Out) Date</td>
                                        <td class="text1"><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" readonly class="datepicker , form-control" onchange="calculateDays2()"  value="<c:out value="${endDetails.unLoadingDate}" />" style="width:180px;height:40px;"></td>
                                        <td class="text1" height="25" ><font color="red">*</font>Vehicle Actual (Factory Out) Time </td>
                                        <td class="text1" colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                    </tr>
                                    <c:if test="${endDetails.unLoadingDate == null}">
                                        <script>
                                                    document.tripExpense.vehicleloadreportdate.value = '<%=endDate%>' / > ;</script>
                                        </c:if>
                                        <c:if test="${endDetails.unLoadingTime != null}">
                                        <script>
                                                    var endtime = '<c:out value="${endDetails.unLoadingTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.vehicleloadreporthour.value = endtimes[0];
                                                    document.tripExpense.vehicleloadreportmin.value = endtimes[1];</script>
                                        </c:if>
                                    <tr  height="30">
                                        <td class="text2"  ><font color="red">*</font>End (ICD IN)Date  </td>
                                        <td class="text2"  >
                                            <input type="text" name="endDate" id="endDate" readonly class="datepicker , form-control" onchange="calculateDays();"  value="<c:out value="${endDetails.endDate}" />" style="width:180px;height:40px;">
                                        </td>
                                        <td class="text2"  ><font color="red">*</font>End (ICD IN )Time<c:out value="${endDetails.endTime}" /> </td>
                                        <td class="text2"  >HH:<select name="tripEndHour" id="tripEndHour" class="textbox" onchange="calculateDays();" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="tripEndMinute" id="tripEndMinute" class="textbox"onchange="calculateDays();" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                        </td>
                                    </tr>
                                    <c:if test="${endDetails.endDate == null}">
                                        <script>
                                                    document.tripExpense.endDate.value = '<%=endDate%>' / > ;</script>
                                        </c:if>
                                        <c:if test="${endDetails.endTime != null}">
                                        <script>
                                                    var endtime = '<c:out value="${endDetails.endTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.tripEndHour.value = endtimes[0];
                                                    document.tripExpense.tripEndMinute.value = endtimes[1];</script>
                                        </c:if>

                                    <tr  height="30">
                                        <td class="text1"><font color="red">*</font> Trip End Odometer Reading(KM)</td>
                                        <td class="text1"><input type="text" name="endOdometerReading" id="endOdometerReading" class="form-control" value='<c:out value="${endDetails.endOdometerReading}" />' onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalKM();" style="width:180px;height:40px;"></td>
                                        <td class="text1"><font color="red">*</font>Trip End Reefer Reading(HM)</td>
                                        <td class="text1"><input type="text" name="endHM" id="endHM" class="form-control" value='<c:out value="${endDetails.endHM}" />' onchange="calTotalHM();" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                    </tr>


                                    <c:if test="${startTripDetails != null}">
                                        <c:forEach items="${startTripDetails}" var="tripDetails">
                                            <input type="hidden" name="startKM" id="startKM" value="<c:out value="${tripDetails.startOdometerReading}"/>" />
                                            <input type="hidden" name="startHM" id="startHM" value="<c:out value="${tripDetails.startHM}"/>" />
                                            <input type="hidden" name="startDate" id="startDate" value="<c:out value="${tripDetails.startDate}"/>" />
                                            <input type="hidden" name="startTime" id="startTime" value="<c:out value="${tripDetails.startTime}"/>" />
                                        </c:forEach>
                                    </c:if>


                                    <tr  height="30">
                                        <td class="text2"> Total Odometer Reading(KM)</td>
                                        <td class="text2"><input type="text" readonly name="totalKM" id="totalKM" class="form-control" value='<c:out value="${endDetails.totalKM}" />' onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                        <td class="text2"> Total Reefer Reading(HM)</td>
                                        <td class="text2"><input type="text" readonly name="totalHrs" id="totalHrs" class="form-control" value='<c:out value="${endDetails.totalHrs}" />' onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                    </tr>
                                    <tr  height="30">

                                        <td class="text1" height="30" >Total Duration Hours</td>
                                        <td class="text1" height="30" ><input type="text" name="tripTransitHour"  id="tripTransitHour" class="form-control"  value="<c:out value="${endDetails.durationHours}" />" style="width:180px;height:40px;"></td>

                                        <td class="text1" height="30" >Total Days</td>

                                        <td class="text1"><input type="text" name="totalDays1" readonly id="totalDays1" class="form-control"  value="<c:out value="${endDetails.totalDays}" />" style="width:180px;height:40px;"></td>


                                    </tr>
                                    <tr class="excludeRepo"  height="30"> 

                                        <td class="text2" height="30" >Total Factory In to ICD In Hours</td>

                                        <td class="text2"><input type="text" name="tripFactoryToIcdHour"  id="tripFactoryToIcdHour" class="form-control"  value="<c:out value="${endDetails.tripFactoryToIcdHour}" />" style="width:180px;height:40px;"></td>
                                        <td class="text2" height="30" ><font color="red">*</font>Detain Hours</td>
                                        <td class="text2" height="30" ><input type="text" name="tripDetainHour"  id="tripDetainHour" class="form-control"  value="<c:out value="${endDetails.tripDetainHours}" />" style="width:180px;height:40px;"></td>



                                    </tr>
                                    <tr  height="30">
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <c:if test = "${trip.movementType == 2}" >
                                                <td class="boE" class="text2" height="30">Bill of Entry: </td>
                                                <td class="boE"><input type="text" id="billOfEntry" name="billOfEntry" value="<c:out value="${trip.billOfEntry}" />" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"/></td>
                                                </c:if>
                                                <c:if test = "${trip.movementType == 1}" >
                                                <td class="slN" class="text2" height="30">Shipping Bill No:</td>
                                                <td class="slN"> <input type="text" id="shipingLineNo" name="shipingLineNo" value="<c:out value="${trip.shipingLineNo}" />" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"/></td>
                                                </c:if>
                                            </c:forEach>
                                        <td class="text1" height="30" ><font color='red'>*</font>Commodity Category</td>
                                        <td><select id="commodityId" name="commodityId" style="width:180px;height:40px;" onChange="setCommaodityName(this.value);"/>
                                    <option value="0">---select-----</option>
                                    <c:forEach items="${comodityDetails}" var="com">
                                        <option value='<c:out value="${com.commodityId}" />~<c:out value="${com.commodityName}" />'><c:out value="${com.commodityName}" /></option>
                                    </c:forEach>
                                    </select>
                                    <script>
                                                document.getElementById("commodityId").value = '<c:out value="${commodityId}" />~<c:out value="${articleName}" />'
//                                                            document.getElementById("commodityId").value="0";
                                    </script>  
                                    </td>
                                    </tr>
                                    <tr  height="30" class="text2">
                                        <td>Trip start Commodity</td>
                                        <td > 
                                            <input type="text" id="commodityName" name="commodityName" style="width:180px;height:40px;"
                                                   value="" readonly class="form-control"  />
                                            <input type="hidden" id="commodityCategory" name="commodityCategory" style="width:180px;height:40px;"
                                                   value="0" class="form-control"  /></td>

                                    <script>
                                                $("#commodityName").val('<c:out value="${articleName}" />');
                                                $("#commodityCategory").val('<c:out value="${commodityId}" />');
                                                function setCommaodityName(commodity){
//                                                          alert(commodity)
                                                var temp = commodity.split('~');
                                                        $("#commodityCategory").val(temp[0]);
                                                        $("#commodityName").val(temp[1]);
                                                }
                                    </script>
                                    </tr>

                                    <table id="detentionTable" style="display:none" border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                        <tr id="index" height="30">
                                            <td  colspan="30" >Enter Trip End Details</td>
                                        </tr>
                                        <tr>
                                        </tr>

                                        <tr height="30">
                                            &ensp;&ensp;&ensp;&ensp;&ensp;
                                            <td class="text1" height="30" colspan="2" >
                                                Detention Days: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</td>

                                            <td class="text1" colspan="2"><input type="text" name="totalDays2" readonly id="totalDays2" class="form-control"  value="" style="width:180px;height:40px;"></td>

                                            <td class="text1" height="30" colspan="2" >&ensp;&ensp;Tarrif Type: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</td>

                                            <td colspan="2"><select id="tarrifType" name="tarrifType" class="form-control" onchange="detentionCalculation()" style="width:180px;height:40px;">
                                                    <option class="text1" value="0"> --Select-- </option>
                                                    <option class="text1" value="1"> General </option>
                                                    <option class="text1" value="2"> Special </option>
                                                </select></td>
                                            <td class="text1" height="30" colspan="2" >&ensp;&ensp;Amount: &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;</td>
                                            <td class="text1" colspan="2"><input type="text" name="detentionAmount" readonly id="detentionAmount" class="form-control"  value="0" style="width:180px;height:40px;"></td>

                                        </tr>
                                    </table>
                                </c:forEach >
                            </table>
                            <br/>
                        </c:if>
                        <br>
                        <%--      <c:if test = "${tripUnPackDetails != null}" >
                                  <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                      <tr  height="30">
                                          <td colspan="10"  align="center" height="30" >
                                      <center style="color:black;font-size: 14px;">
                                          Consignment Unloading Details
                                      </center> 
                                      </td>
                                      </tr>
                                      <tr id="index" height="30">
                                          <td width="30"  align="center" height="30" >Sno</td>
                                          <td  height="30" >Product/Article Code</td>
                                          <td  height="30" ><font color='red'>*</font>Product/Article Name </td>
                                              <!--                                    <td  height="30" >Batch </td>
                                                                                  <td  height="30" ><font color='red'></font>No of Packages</td>
                                                                                  <td  height="30" ><font color='red'></font>Uom</td>
                                                                                  <td  height="30" ><font color='red'></font>Total Weight (in Kg)</td>
                                                                                  <td  height="30" ><font color='red'></font>Loaded Package Nos</td>
                                                                                  <td  height="30" ><font color='red'></font>UnLoaded Package Nos</td>
                                                                                  <td  height="30" ><font color='red'></font>Shortage</td>-->
                                      </tr>


                                    <%int i2 = 1;%>
                                    <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                        <tr>
                                            <td><%=i2%></td>
                                            <td><input type="text"  style="width:240px;height:40px;" class="form-control" name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                            <td><input type="text" style="width:240px;height:40px;" class="form-control" name="productNames11" id="productNames11" value="<c:out value="${tripunpack.articleName}"/>" />
                                                <input type="hidden" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/>
                                                <input   type="hidden" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/>
                                                <input type="hidden" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/>
                                                <input type="hidden" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <input type="hidden" name="loadedpackages" id="loadedpackages<%=i2%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>

                                                <input type="hidden" name="unloadedpackages" id="unloadedpackages<%=i2%>" onblur="computeShortage(<%=i2%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    />
                                                <input type="hidden" name="shortage" id="shortage<%=i2%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                        </tr>
                                        <%i2++;%>
                                    </c:forEach>

                                    <br/>
                                </table>
                                <br/>
                                <br/>
                            </c:if>  --%>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <center>
                            <!--<input type="button" class="btn btn-success" name="Save" style="width:80px;height:30px;padding:1px;font-weight: bold" value="close trip" onclick="closeTrip();" />-->

                            <input type="button" class="btn btn-success" name="Save" style="width:200px;height:30px;font-weight: bold;padding:1px;" value="Update Trip End Details" onclick="saveTripEndDetails();" />


                        </center>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <div id="statusDetail">
                        <% int index1 = 1;%>

                        <c:if test = "${statusDetails != null}" >
                            <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <tr id="index" height="30">
                                    <td  height="30" >S No</td>
                                    <td  height="30" >Status Name</td>
                                    <td  height="30" >Remarks</td>
                                    <td  height="30" >Created User Name</td>
                                    <td  height="30" >Created Date</td>
                                </tr>
                                <c:forEach items="${statusDetails}" var="statusDetails">
                                    <%
                                                String classText = "";
                                                int oddEven1 = index1 % 2;
                                                if (oddEven1 > 0) {
                                                    classText = "text1";
                                                } else {
                                                    classText = "text2";
                                                }
                                    %>
                                    <tr height="30">
                                        <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                        <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                        <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                        <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                        <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                    </tr>
                                </c:forEach >
                            </table>
                            <br/>
                            <br/>
                            <br/>
                        </c:if>
                        <br>
                        <br>
                        <center>
                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <c:if test="${fuelTypeId == 1002}">
                        <div id="bpclDetail" style="display:none">
                            <% int index10 = 1;%>


                            <c:if test = "${bpclTransactionHistory != null}" >
                                <c:set var="totalAmount" value="0"/>
                                <table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30">
                                        <td  height="30" >S No</td>
                                        <td  height="30" >Trip Code</td>
                                        <td  height="30" >Vehicle No</td>
                                        <td  height="30" >Transaction History Id</td>
                                        <td  height="30" >BPCL Transaction Id</td>
                                        <td  height="30" >BPCL Account Id</td>
                                        <td  height="30" >Dealer Name</td>
                                        <td  height="30" >Dealer City</td>
                                        <td  height="30" >Transaction Date</td>
                                        <td  height="30" >Accounting Date</td>
                                        <td  height="30" >Transaction Type</td>
                                        <td  height="30" >Currency</td>
                                        <td  height="30" >Transaction Amount</td>
                                        <td  height="30" >Volume Document No</td>
                                        <td  height="30" >Amount Balance</td>
                                        <td  height="30" >Petromiles Earned</td>
                                        <td  height="30" >Odometer Reading</td>
                                    </tr>
                                    <c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
                                        <%
                                                    String classText10 = "";
                                                    int oddEven10 = index10 % 2;
                                                    if (oddEven10 > 0) {
                                                        classText10 = "text1";
                                                    } else {
                                                        classText10 = "text2";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td class="<%=classText10%>" height="30" ><%=index10++%></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.tripCode}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.vehicleNo}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountId}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerName}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerCity}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionDate}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountingDate}" /></td>
                                            <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionType}" /></td>
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.currency}" /></td>
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amount}" /></td>
                                            <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
                                            <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
                                        </tr>
                                    </c:forEach >

                                    <tr height="30">
                                        <td  height="30" style="text-align: right;color:black" colspan="12">Total Amount</td>
                                        <td  height="30" style="text-align: right;color:black;"  ><c:out value="${totalAmount}"/></td>
                                        <td  height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                                    </tr>
                                </table>
                                <br/>
                                <br/>
                                <br/>
                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                    </c:if>
                    <c:if test="${tripAdvanceDetails != null}">
                        <div id="advance">
                            <c:if test="${tripAdvanceDetails != null}">
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30">
                                        <td  width="30">Sno</td>
                                        <td  width="90">Advance Date</td>
                                        <td  width="90">Trip Day</td>
                                        <td  width="120">Estimated Advance</td>
                                        <td  width="120">Requested Advance</td>
                                        <td  width="90"> Type</td>
                                        <td  width="120">Requested By</td>
                                        <td  width="120">Requested Remarks</td>
                                        <td  width="120">Approved By</td>
                                        <td  width="120">Approved Remarks</td>
                                        <td  width="120">Paid Advance</td>
                                    </tr>
                                    <%int index7 = 1;%>
                                    <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                        <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                        <%
                                                    String classText7 = "";
                                                    int oddEven7 = index7 % 2;
                                                    if (oddEven7 > 0) {
                                                        classText7 = "text1";
                                                    } else {
                                                        classText7 = "text2";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td class="<%=classText7%>"><%=index7++%></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                            <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                            <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                <td class="<%=classText7%>">Adhoc</td>
                                            </c:if>
                                            <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                <td class="<%=classText7%>">Batch</td>
                                            </c:if>
                                            <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                <td class="<%=classText7%>">Manual</td>
                                            </c:if>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                            <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                        </tr>
                                    </c:forEach>

                                    <tr height="30">

                                        <td class="text1" colspan="10" align="right" style="color:black"><b>Total Advance Paid </b>&emsp;</td>
                                        <td class="text1" align="right"  style="color:black"><b><c:out value="${totalAdvancePaid}"/></b></td>
                                    </tr>
                                </table>
                                <br/>
                                <c:if test="${tripAdvanceDetailsStatus != null}">
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr  height="30">
                                            <td  colspan="13" >
                                        <center style="color:black;font-size: 14px;">
                                            Advance Approval Status Details
                                        </center>
                                        </td>
                                        </tr>
                                        <tr id="index" height="30">
                                            <td  width="30">Sno</td>
                                            <td  width="90">Request Date</td>
                                            <td  width="90">Trip Day</td>
                                            <td  width="120">Estimated Advance</td>
                                            <td  width="120">Requested Advance</td>
                                            <td  width="90"> Type</td>
                                            <td  width="120">Requested By</td>
                                            <td  width="120">Requested Remarks</td>
                                            <td  width="120">Approval Status</td>
                                            <td  width="120">Paid Status</td>
                                        </tr>
                                        <%int index13 = 1;%>
                                        <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                            <%
                                                        String classText13 = "";
                                                        int oddEven11 = index13 % 2;
                                                        if (oddEven11 > 0) {
                                                            classText13 = "text1";
                                                        } else {
                                                            classText13 = "text2";
                                                        }
                                            %>
                                            <tr height="30">

                                                <td class="<%=classText13%>"><%=index13++%></td>
                                                <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                    <td class="<%=classText13%>">Adhoc</td>
                                                </c:if>
                                                <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                    <td class="<%=classText13%>">Batch</td>
                                                </c:if>
                                                <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                    <td class="<%=classText13%>">Manual</td>
                                                </c:if>

                                                <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                <td class="<%=classText13%>">
                                                    <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                        &nbsp
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                        Request Approved
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                        Request Rejected
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                        Approval in  Pending
                                                    </c:if>
                                                    &nbsp;</td>
                                                <td class="<%=classText13%>">
                                                    <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                        Yet To Pay
                                                    </c:if>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <br/>
                                    <br/>

                                    <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr  height="30">
                                                <td  colspan="13" > 
                                            <center style="color:black;font-size: 14px;">
                                                Old Vehicle Advance Details
                                            </center></td>
                                            </tr>
                                            <tr id="index" height="30">
                                                <td  width="30">Sno</td>
                                                <td  width="90">Advance Date</td>
                                                <td  width="90">Trip Day</td>
                                                <td  width="120">Estimated Advance</td>
                                                <td  width="120">Requested Advance</td>
                                                <td  width="90"> Type</td>
                                                <td  width="120">Requested By</td>
                                                <td  width="120">Requested Remarks</td>
                                                <td  width="120">Approved By</td>
                                                <td  width="120">Approved Remarks</td>
                                                <td  width="120">Paid Advance</td>
                                            </tr>
                                            <%int index17 = 1;%>
                                            <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                <%
                                                            String classText17 = "";
                                                            int oddEven17 = index17 % 2;
                                                            if (oddEven17 > 0) {
                                                                classText17 = "text1";
                                                            } else {
                                                                classText17 = "text2";
                                                            }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText17%>"><%=index7++%></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                    <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                    <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                        <td class="<%=classText17%>">Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                        <td class="<%=classText17%>">Batch</td>
                                                    </c:if>
                                                    <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                        <td class="<%=classText17%>">Manual</td>
                                                    </c:if>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                    <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                </tr>
                                            </c:forEach>

                                            <tr height="30">


                                                <td class="text1" colspan="10" align="right"><b>Total Advance Paid </b>&emsp;</td>
                                                <td class="text1" align="right"><b><c:out value="${totalVehicleChangeAdvancePaid}"/></b></td>

                                            </tr>
                                        </table>
                                    </c:if>
                                </c:if>

                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                    </c:if>
                    <style>
                        #otherExpDetail
                        {padding-left:-2px;}
                    </style>
                    <div id="otherExpDetail" style="width: auto;padding-left: 2px;">
                        <div style="border: #ffffff solid" >
                            <%int count = 0;%>
                            <input type="hidden" name="count" id="count" value="<c:out value="${otherExpenseDetailsSize}"/>"/>
                            <c:if test="${otherExpenseDetails != null}">
                                <h4>Expense Details</h4>
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="tabHead" height="30">
                                        <th width="50" >SNo&nbsp;</th>
                                        <th >Expense Name</th>
                                        <th >Driver Name</th>
                                        <th >Expense Date</th>
                                        <th >Expense Remarks</th>
                                        <th >Expense Type</th>
                                        <th >Applicable Tax Percentage</th>
                                        <th >Expense Amount</th>
                                        <th >Total Expenses</th>
                                        <th >Document Required</th>
                                        <th style="display:none">Delete Expense</th>
                                        <th style="display:none">Edit Expense</th>

                                    </tr>
                                    <% int index5 = 1;%>
                                    <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                        <%
                                                    String classText5 = "";
                                                    int oddEven3 = index5 % 2;
                                                    if (oddEven3 > 0) {
                                                        classText5 = "text1";
                                                    } else {
                                                        classText5 = "text2";
                                                    }
                                        %>
                                        <%count++;%>
                                        <tr height="30">
                                            <td class="<%=classText5%>" ><%=index5++%></td>
                                            <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseName}"/>&nbsp;</td>
                                            <td class="<%=classText5%>" ><c:out value="${expenseDetails.employeeName}"/>&nbsp;</td>
                                            <td><input type="text" id="expenseDateNew<%=count%>" name="expenseDateNew" class="datepicker , form-control" value="<c:out value="${expenseDetails.expenseDate}"/>"/></td>

                                            <td>  <input  type="text" id="expenseRemarksEdit<%=count%>" name="expenseRemarksEdit"  class="form-control"  value="<c:out value="${expenseDetails.expenseRemarks}"/>">

                                            </td>
                                            <td><select id="expenseTypeNew<%=index5%>" name="expenseTypeNew" class="form-control">
                                                    <c:if test = "${expenseDetails.expenseType == '1'}" >
                                                        <option class="<%=classText5%>" value="1" selected>Bill To Customer </option>
                                                        <option class="<%=classText5%>" value="2">Do Not Bill To Customer </option>

                                                    </c:if>
                                                    <c:if test = "${expenseDetails.expenseType == '2'}" >
                                                        <option class="<%=classText5%>" value="1">Bill To Customer </option>
                                                        <option class="<%=classText5%>" value="2" selected>Do Not Bill To Customer </option>

                                                    </c:if>
                                                    <script>
                                                                function set(){
                                                                document.getElementById("expenseType" +<%=index5%>).value = '<c:out value="${expenseDetails.expenseType}"/>';
                                                                }
                                                    </script>
                                                    <c:if test = "${expenseDetails.expenseType == '0'}" >
                                                        <option class="<%=classText5%>" value="1">Bill To Customer </option>
                                                        <option class="<%=classText5%>" value="2">Do Not Bill To Customer </option>

                                                    </c:if>
                                                </select></td>
                                        <script>
                                            function changeFunction(sno){

                                            var expenseAmount = document.getElementById("expenseAmountvalue" + sno).value;
                                                    document.getElementById("totalExpenseAmount" + sno).value = expenseAmount;
                                            }
                                        </script>



                                        <td class="<%=classText5%>" style="text-align:center"><c:out value="${expenseDetails.taxPercentage}"/>&nbsp;</td>
                                        <td class="<%=classText5%>" >

                                            <input  type="text" id="expenseAmountvalue<%=count%>" name="expenseAmountvalue"  class="form-control" onkeyup="changeFunction(<%=count%>)"  value="<c:out value="${expenseDetails.expenseValue}"/>">


                                        </td>
                                        <td class="<%=classText5%>" >

                                            <input  type="text" id="totalExpenseAmount<%=count%>" name="totalExpenseAmount"  class="form-control"   value="<c:out value="${expenseDetails.totalExpenseAmount}"/>">


                                        </td>

                                        <td  class="<%=classText5%>" height="30" style="text-align:center"> <c:if test="${expenseDetails.documentRequired=='1'}">Yes </c:if> <c:if test="${expenseDetails.documentRequired=='0'}">No</c:if> </td>
                                        <c:if test = "${expenseDetails.expenseCategory == '1'}" >
                                            <td style="display:none" class="<%=classText5%>" height="30"><input type="checkbox"  name="delete" value="" id="delete<%=count%>" onclick="deleteExpenseDetails(<%=count%>)"/></td>
                                            <td style="display:none" class="<%=classText5%>" height="30"><input type="checkbox"  name="edit" value="" id="edit<%=count%>" onclick="modifyExpenseDetails(<%=count%>)"/></td>
                                            </c:if>
                                            <c:if test = "${expenseDetails.expenseCategory == '2'}" >
                                            <td style="display:none" class="<%=classText5%>" height="30"><input type="checkbox" name="delete" value="" id="delete<%=count%>" onclick="deleteExpenseDetails(<%=count%>)"/>
                                            <td  style="display:none" class="<%=classText5%>" height="30"><input type="checkbox" name="edit" value="" id="edit<%=count%>" onclick="modifyExpenseDetails(<%=count%>)"/>
                                            </c:if>
                                            <input type="hidden" name="expenseId" id="expenseId<%=count%>" value="<c:out value="${expenseDetails.tripExpenseId}"/>"/>
                                            <input type="hidden" name="employeeNames" id="employeeNames<%=count%>" value="<c:out value="${expenseDetails.employeeId}"/>"/>
                                            <input type="hidden" name="expenseNames" id="expenseNames<%=count%>" value="<c:out value="${expenseDetails.expenseId}"/>"/>
                                            <input type="hidden" name="expenseDates" id="expenseDates<%=count%>" value="<c:out value="${expenseDetails.expenseDate}"/>"/>
                                            <input type="hidden" name="expenseHours" id="expenseHours<%=count%>" value="<c:out value="${expenseDetails.expenseHour}"/>"/>
                                            <input type="hidden" name="expenseMinutes" id="expenseMinutes<%=count%>" value="<c:out value="${expenseDetails.expenseMinute}"/>"/>
                                            <input type="hidden" name="expenseRemarkss" id="expenseRemarkss<%=count%>" value="<c:out value="${expenseDetails.expenseRemarks}"/>"/>
                                            <input type="hidden" name="expenseTypes" id="expenseTypes<%=count%>" value="<c:out value="${expenseDetails.expenseType}"/>"/>
                                            <input type="hidden" name="passThroughStatuss" id="passThroughStatuss<%=count%>" value="<c:out value="${expenseDetails.passThroughStatus}"/>"/>
                                            <input type="hidden" name="taxPercentages" id="taxPercentages<%=count%>" value="<c:out value="${expenseDetails.taxPercentage}"/>"/>
                                            <input type="hidden" name="marginValues" id="marginValues<%=count%>" value="<c:out value="${expenseDetails.marginValue}"/>"/>
                                            <input type="hidden" name="expenseValues" id="expenseValues<%=count%>" value="<c:out value="${expenseDetails.expenseValue}"/>"/></td>
                                        <input type="hidden" name="totalExpenseAmounts" id="totalExpenseAmounts<%=count%>" value="<c:out value="${expenseDetails.totalExpenseAmount}"/>"/></td>
                                        </tr>
                                    </c:forEach>
                                    <tr height="50">
                                        <td align="center" colspan="14"> 
                                            <input type="button" class="btn btn-success" value="Update " name="Save expenses" onclick="submitExpense();" style="width:80px;height:30px;padding:1px;font-weight: bold"/>


                                        </td>
                                    </tr>
                                </table>
                            </c:if>
                            <br>
                            <script>
                                        function modifyExpenseDetails(sno) {
                                        var count = parseInt(document.getElementById("count").value);
                                                var sno1 = 0;
                                                for (var i = 1; i <= count; i++) {
                                        if (i != sno) {
                                        document.getElementById("edit" + i).checked = false;
                                        } else {
                                        document.getElementById("edit" + i).checked = true;
                                                document.getElementById('editId' + sno1).value = document.getElementById("expenseId" + i).value;
                                                document.getElementById('expenseName' + sno1).value = document.getElementById("expenseNames" + i).value;
                                                document.getElementById('employeeName' + sno1).value = document.getElementById("employeeNames" + i).value;
                                                document.getElementById('expenseDate' + sno1).value = document.getElementById("expenseDates" + i).value;
//                                                document.getElementById('expenseHour' + sno1).value = document.getElementById("expenseHours" + i).value;
//                                                document.getElementById('expenseMinute' + sno1).value = document.getElementById("expenseMinutes" + i).value;
                                                document.getElementById('expenseRemarks' + sno1).value = document.getElementById("expenseRemarkss" + i).value;
                                                document.getElementById('expenseType' + sno1).value = document.getElementById("expenseTypes" + i).value;
//                                                document.getElementById('billModeTemp' + sno1).value = document.getElementById("passThroughStatuss" + i).value;
                                                document.getElementById('taxPercentage' + sno1).value = document.getElementById("taxPercentages" + i).value;
//                                                document.getElementById('marginValue' + sno1).value = document.getElementById("marginValues" + i).value;
                                                document.getElementById('expenses' + sno1).value = document.getElementById("expenseValues" + i).value;
                                                document.getElementById('netExpense' + sno1).value = document.getElementById("totalExpenseAmounts" + i).value;
                                        }
                                        }
                                        }
                                function deleteExpenseDetails(sno) {
                                //alert("hi:"+sno);
                                var count = parseInt(document.getElementById("count").value);
//                                        alert(count);
                                        var sno1 = 0;
                                        for (var i = 1; i <= count; i++) {
                                if (i != sno) {
                                document.getElementById("delete" + i).checked = false;
                                } else {
                                document.getElementById("delete" + i).checked = true;
                                        document.getElementById('deleteId' + sno1).value = document.getElementById("expenseId" + i).value;
                                        // document.getElementById('editId' + sno1).value = document.getElementById("expenseId" + i).value;
                                        document.getElementById('expenseName' + sno1).value = document.getElementById("expenseNames" + i).value;
                                        document.getElementById('employeeName' + sno1).value = document.getElementById("employeeNames" + i).value;
                                        document.getElementById('expenseDate' + sno1).value = document.getElementById("expenseDates" + i).value;
//                                                document.getElementById('expenseHour' + sno1).value = document.getElementById("expenseHours" + i).value;
//                                                document.getElementById('expenseMinute' + sno1).value = document.getElementById("expenseMinutes" + i).value;
                                        document.getElementById('expenseRemarks' + sno1).value = document.getElementById("expenseRemarkss" + i).value;
                                        document.getElementById('expenseType' + sno1).value = document.getElementById("expenseTypes" + i).value;
//                                                document.getElementById('billModeTemp' + sno1).value = document.getElementById("passThroughStatuss" + i).value;
                                        document.getElementById('taxPercentage' + sno1).value = document.getElementById("taxPercentages" + i).value;
//                                                document.getElementById('marginValue' + sno1).value = document.getElementById("marginValues" + i).value;
                                        document.getElementById('expenses' + sno1).value = document.getElementById("expenseValues" + i).value;
                                        document.getElementById('netExpense' + sno1).value = document.getElementById("totalExpenseAmounts" + i).value;
                                }
                                }
                                }
                            </script>
                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="right">
                                <tr>
                                    <td id="availableAdvanceSpan" style="color:black;font-size: 15px;"> <b> </b></td>


                                </tr>
                                <input type="hidden" name="availableAdvance"  id="availableAdvance" value="" />
                            </table>
                            <br>

                            <table class="border" style="width: auto;font-size:13px;" border="1" cellpadding="0"  id="suppExpenseTBL" >
                                <tr id="index" height="30px;">
                                    <th width="50">S No&nbsp;</th>
                                    <th><font color='red'>*</font>Expense Name</th>
                                    <th><font color='red'>*</font>Driver Name</th>
                                    <th><font color='red'>*</font>Expense Date</th>
                                    <th>Expense Remarks</th>
                                    <th><font color='red'>*</font>Expense Type</th>
                                    <th>Applicable Tax %</th>
                                    <th><font color='red'>*</font>Expense Value</th>
                                    <th>Total Expenses</th>
                                    <th>Document Required</th>
                                </tr>

                                <tr height="50">
                                    <td align="center" colspan="14"> 
                                        <input type="hidden" class="text" name="admin" value="<c:out value="${admin}"/>" >
                                        &emsp;   <input type="button" class="btn btn-success" name="add" value="Add" onclick="addRow(<c:out value="${otherExpenseDetailsSize}"/>);" style="width:50px;height:30px;padding:1px;font-weight: bold"/>
                                        &emsp;  <input type="reset" class="btn btn-success" value="Clear" style="width:50px;height:30px;padding:1px;font-weight: bold">
                                        &emsp; <input type="button" class="btn btn-success" value="Save " name="Save expenses" onclick="submitPage();" style="width:50px;height:30px;padding:1px;font-weight: bold"/>
                                        &emsp;


                                    </td>
                                </tr>
                            </table><br><br>

                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>

                            <br/>
                            <br/>
                        </div>
                        <script>
                                    var rowCount = 1;
                                    var sno = 0;
                                    var rowCount1 = 1;
                                    var sno1 = 0;
                                    var httpRequest;
                                    var httpReq;
                                    var styl = "";
                                    function addRow(count) {
                                    //  
                                    if (parseInt(rowCount1) % 2 == 0)
                                    {
                                    styl = "text2";
                                    } else {
                                    styl = "text1";
                                    }
                                    for (var i = 1; i < count; i++) {
                                    sno1++;
                                            var tab = document.getElementById("suppExpenseTBL");
                                            //find current no of rows
                                            //  var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;

                                            var rowCountNew = parseInt(i);
                                            // rowCountNew--;
                                            var newrow = tab.insertRow(rowCountNew);
                                            cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value=''/><input type='hidden' name='deleteId' id='deleteId" + sno + "' value=''/>" + sno1 + "</td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1' height='25' style='width:90px;'><select class='form-control' style='width:110px;height:40px;' id='expenseName" + sno + "' onchange='checkExpenseType(" + sno + ");'  name='expenseName'><option  value=0>---Select---</option> <c:if test="${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"><option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /> </c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1' height='25' style='width:90px;'><select class='form-control' style='width:110px;height:40px;' id='employeeName" + sno + "'  name='employeeName'><option selected value=0>---Select---</option> <c:if test="${driverNameDetails != null}" ><c:forEach items="${driverNameDetails}" var="driver"><option selected value='<c:out value="${driver.employeeId}" />'><c:out value="${driver.employeeName}" /> </c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(3);
                                            var cell0 = "<td class='text1' height='25' style='width:70px;'><input type='hidden' name='expenseHour' id='expenseHour" + sno + "' value='00'/>\n\
                                    <input type='hidden' name='expenseMinute' id='expenseMinute" + sno + "' value='00'/><input type='text' value=''   name='expenseDate'  id='expenseDate" + sno + "'   class='datepicker , form-control' style='width:110px;height:40px;'></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            ;
                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1' height='25' style='width:120px;'><textarea rows='3' cols='20' class='form-control' name='expenseRemarks' id='expenseRemarks" + sno + "' style='width:120px;height:50px'></textarea></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(5);
                                            var cell0 = "<td class='text1' height='25' style='width:90px;'><input type='hidden'  name='marginValue' id='marginValue" + sno + "'   onchange='calTotalExpenses(" + sno + ");' style='width:110px;height:40px;' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' value = '0' ><input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' ><input type='hidden' name='billModeTemp' id='billModeTemp" + sno + "' value='0'/><input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' ><select class='form-control' id='expenseType" + sno + "'   name='expenseType' style='width:110px;height:40px;' onchange='setExpenseTypeDetails(this.value," + sno + ")'><option value='0' selected>-Select-</option><option  value='1'>Bill To Customer</option><option value='2'>Do Not Bill Customer</option></select></td>";
                                            cell.setAttribute("className", "text1");
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(6);
                                            var cell0 = "<input type='text'  name='taxPercentage' id='taxPercentage" + sno + "'   onchange='calTotalExpenses(" + sno + ");' style='width:110px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' value = '' ></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(7);
                                            var cell0 = "<td class='text1' height='10'  style='width:60px;'><input type='text'  name='expenses' id='expenses" + sno + "' class='form-control' style='width:110px;height:40px;' value=''  onKeyPress='return onKeyPressBlockCharacters(event);' onchange='checkAvailableAdvance(" + sno + ")'></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(8);
                                            var cell0 = "<td class='text1' height='25'  style='width:90px;'><input readonly type='hidden' name='currency' id='currency" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  value='0' ><input readonly type='text' name='netExpense' id='netExpense" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  class='form-control' value='' ></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(9);
                                            var cell0 = "<td class='text1' height='25'><input  type='hidden' name='activeValue' id='activeValue" + sno + "' value='0' >&emsp;<input  type='checkbox' name='documentRequired' id='documentRequired" + sno + "' onclick ='setActiveValues(" + sno + ");' value='' ></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            $(".datepicker").datepicker({
                                    dateFormat: 'dd-mm-yy',
                                            changeMonth: true, changeYear: true
                                    });
                                            rowCount1++;
                                            sno++;
                                    }
                                    }


                                </script>

                                <script type="text/javascript">
                                    function checkExpenseType(sno) {
                                    var expenses = document.getElementsByName("expenseName");
                                            var expenseNames = document.getElementsByName("expenseNames");
                                            var expenseName = document.getElementById("expenseName" + sno).value;
                                            var check = 0;
                                            for (var i = 0; i < expenseNames.length; i++) {
                                    if (expenseNames[i].value == expenseName) {
                                    check = 1;
                                    }
                                    }
                                    if (check == 1) {
                                    alert("Same expense type is already selected, Do you want to proceed?");
                                            //document.getElementById("expenseName" + sno).value = "0";
                                            //document.getElementById("expenseName" + sno).focus();
                                            return;
                                    }
                                    for (var i = 0; i <= expenses.length; i++) {
                                    if (expenseName != 0) {
                                    if (i != sno) {
                                    if (expenses[i].value == expenseName) {
                                    check = 1;
                                    }
                                    if (check == 1) {
                                    alert("Same expense type is already selected, Do you want to proceed?");
                                            //document.getElementById("expenseName" + sno).value = "0";
                                            //document.getElementById("expenseName" + sno).focus();
                                            return;
                                    }
                                    }
                                    }
                                    }


                                    }

                                    function setActiveValues(sno) {
                                    if (document.getElementById('documentRequired' + sno).checked) {

                                    document.getElementById('activeValue' + sno).value = "1";
                                    } else {

                                    document.getElementById('activeValue' + sno).value = "0";
                                    }
                                    }
                                </script>

                            </div>

                            <script>
                                $('.btnNext').click(function () {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                        $('.btnPrevious').click(function () {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                }
                                );
                            </script>

                        </div>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
            <div id="loader"></div>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
