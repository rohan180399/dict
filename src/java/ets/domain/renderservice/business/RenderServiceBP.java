/*---------------------------------------------------------------------------
 * RenderServiceBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.renderservice.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;

import ets.domain.renderservice.data.RenderServiceDAO;
import ets.domain.problemActivities.business.ProblemActivitiesTO;
import ets.domain.renderservice.business.RenderServiceTO;
import java.util.ArrayList;
import java.util.Iterator;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class RenderServiceBP {

    private RenderServiceDAO renderServiceDAO;

    public void setRenderServiceDAO(RenderServiceDAO renderServiceDAO) {
        this.renderServiceDAO = renderServiceDAO;
    }

    public RenderServiceDAO getRenderServiceDAO() {
        return renderServiceDAO;
    }

    public ArrayList getWorkTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList workTypeList = new ArrayList();
        //renderServiceDAO.testAutoCommitFalse();
        workTypeList = renderServiceDAO.getWorkTypeList();
        if (workTypeList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return workTypeList;
    }
    public ArrayList getCloseJobCardViewList(RenderServiceTO rend) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        //renderServiceDAO.testAutoCommitFalse();
        jcList = renderServiceDAO.getCloseJobCardViewList(rend);
        if (jcList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return jcList;
    }

    public ArrayList getCloseJobCardDetails(int jobCardNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getCloseJobCardDetails(jobCardNo);

        Iterator itr = jcList.iterator();
        System.out.println("itr size: " + jcList.size());
        while (itr.hasNext()) {
            RenderServiceTO rto = new RenderServiceTO();
            rto = (RenderServiceTO) itr.next();
            rto.setProblemActivityList(renderServiceDAO.getCloseJobCardPAList(jobCardNo));
            rto.setProblemCauseList(renderServiceDAO.getCloseJobCardPCList(jobCardNo));
            rto.setServiceList(renderServiceDAO.getCloseJobCardServiceList(jobCardNo));
        }

        return jcList;
    }
    public ArrayList getSections() throws FPRuntimeException, FPBusinessException{
        ArrayList sections = new ArrayList();
        sections = renderServiceDAO.getSections();
        if(sections.size()==0){

           // throw new FPBusinessException("EM-SER-14");

        }
        return sections;

    }
    public ArrayList getJobCardsBillDetails(int jobCardNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getCloseJobCardDetails(jobCardNo);

        Iterator itr = jcList.iterator();
        Float sparePercent = 0.00f;
        Float labourPercent = 0.00f;
        while (itr.hasNext()) {
            RenderServiceTO rto = new RenderServiceTO();
            rto = (RenderServiceTO) itr.next();
            //////System.out.println("B4 Labor"+rto.getLabourPercentage());
            //////System.out.println("B4 Spares"+rto.getSparesPercentage());
            sparePercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
            //labourPercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
    
            labourPercent = 1 + Float.parseFloat(rto.getLabourPercentage()) / 100;
            //////System.out.println("sparePercent"+sparePercent);
            //////System.out.println("labourPercent"+labourPercent);
            rto.setItemsList(renderServiceDAO.getJobCardItemList(jobCardNo, rto.getCustId(), sparePercent));
            rto.setProblemActivityList(renderServiceDAO.getJobCardActivityList(jobCardNo, labourPercent));
            rto.setActivityList(renderServiceDAO.getActivityList(jobCardNo));

        }

        return jcList;
    }

    public void doJobCardClosure(int userId, int jobCardId, String[] activityId, String[] qty,String[] amt, String approve, 
            String remarks, String labourCount, String labourHours, String labourExpenseAmount, String consumbalesAmount) throws FPBusinessException, FPRuntimeException {
        String problemCause[] = null;
        String problemId[] = null;
        
        //////System.out.println("handleJobCardClosure...BP");
        //////System.out.println("jobCardId: "+jobCardId);
        //////System.out.println("userId: "+userId);
        //////System.out.println("qty: "+qty);
        //////System.out.println("amt: "+amt);
        //////System.out.println("activityId: "+activityId);
        //////System.out.println("activityId.length: "+activityId.length);
        //////System.out.println("approve: "+approve);
        //////System.out.println("remarks: "+remarks);

        Float totalLabourAmount = 0.00f;
        for (int i = 0; i < activityId.length; i++) {
            //////System.out.println("activityId["+i+"]: "+activityId[i]);
            if(!activityId[i].equals("") && !"".equals(activityId[i]) && activityId[i] != null){
                //////System.out.println("activityId["+i+"]..if: "+activityId[i]);
                renderServiceDAO.insertJobCardClosure(userId, jobCardId, activityId[i], qty[i],amt[i], approve, remarks);
                totalLabourAmount = totalLabourAmount + Float.parseFloat(amt[i]);
            }
        }
        //make account entry
        renderServiceDAO.insertJobCardClosureAccounts(userId, jobCardId, totalLabourAmount+"");

        //update job card status to complete
        renderServiceDAO.updateJobCardStatus(jobCardId, "Completed", userId, remarks,
                labourCount, labourHours, labourExpenseAmount, consumbalesAmount);
    }

    public int doJobCardBilling(int userId, int jobCardId, String[] itemId, String[] quantity,String[] tax, String[] price, String[] lineItemAmount,
            String[] activityId,String[] qty, String[] activityAmount, String[] billNo, String spares, String labour, String total, String discount,
            String nett, String sparesPercent, String labourPercent, String bodyRepairTotal,String invoiceType,String labourHike,String date,
            String matlMargin,String matlMarginPercent,String laborMargin,String laborMarginPercent, String nettMargin,String nettMarginPercent,
                        String purchaseSpares,String labourExpense, String jobCost, String consumablesTotal) throws FPBusinessException, FPRuntimeException {
        //insert bill master
        int newBillNo = 0;
        try{
        newBillNo = renderServiceDAO.insertJobCardBillMaster(userId, jobCardId, spares, labour, total,
                discount, nett, sparesPercent, labourPercent, bodyRepairTotal,invoiceType,labourHike,date, consumablesTotal);
//        int newBillNo = renderServiceDAO.getLastInsertId();

        if (itemId != null) {
            for (int i = 0; i < itemId.length; i++) {
                renderServiceDAO.insertJobCardBillItems(newBillNo, itemId[i], quantity[i], tax[i], price[i], lineItemAmount[i]);
            }
        }
        if (activityId != null) {
            for (int i = 0; i < activityId.length; i++) {
                //////System.out.println("actid:"+activityId[i]);
                //////System.out.println("billNo:"+billNo[i]);
                //////System.out.println("activityAmount:"+activityAmount[i]);
                if ("0".equals(activityId[i])) {
                    if ("2".equals(billNo[i])) {
                        renderServiceDAO.insertJobCardBillService(newBillNo, activityAmount[i], activityId[i]);
                    }
                    else {
                        renderServiceDAO.insertJobCardBillBodyWorks(newBillNo, activityAmount[i], billNo[i]);
                    }
                } else {                    
// 
//  Changed By Raja On Jun 20 12:13 Pm
                     
                    if(Integer.parseInt(billNo[i]) > 5){
                        renderServiceDAO.insertJobCardBillBodyWorks(newBillNo, activityAmount[i], billNo[i]);
                    }else{
                        renderServiceDAO.insertJobCardBillLabour(newBillNo, activityId[i],qty[i], activityAmount[i]);
                    }
                }
            }
        }
        //insert data for service tax by srini 5th July 2011 start

        renderServiceDAO.insertJobCardBillServiceTax(newBillNo,labour);
        renderServiceDAO.insertJobCardBillMargin(newBillNo,labour,spares,nett,matlMargin,matlMarginPercent,laborMargin,laborMarginPercent,nettMargin,nettMarginPercent,
                        purchaseSpares,labourExpense,jobCost);
        //insert data for service tax by srini 5th July 2011 end

        //update job card status to complete
        String remarks = "";
        renderServiceDAO.updateJobCardStatus(jobCardId, "Billing Completed", userId, remarks,"0","0","0","0");
        }catch(Exception e){
            e.printStackTrace();
        }
        return newBillNo;
    }

    public ArrayList getClosedJobCardForBill(RenderServiceTO rendTO) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getClosedJobCardForBill(rendTO);

        RenderServiceTO activityTO = new RenderServiceTO();
        Iterator itr = jcList.iterator();
        String closedDetails = "";
        String temp[] = null;
        while(itr.hasNext()){
            activityTO = new RenderServiceTO();
            activityTO = (RenderServiceTO) itr.next();
            closedDetails = renderServiceDAO.getClosedDetails(activityTO.getJobCardId());
            if(closedDetails != null) {
                temp = closedDetails.split("~");
                activityTO.setClosedBy( temp[0] );
                activityTO.setClosedOn( temp[1] );
            }

        }

        if (jcList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return jcList;
    }

    public ArrayList getBillHeaderInfo(int billNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        ArrayList newJcList = new ArrayList();
        jcList = renderServiceDAO.getBillHeaderInfo(billNo);
        
        RenderServiceTO activityTO = new RenderServiceTO();
        Iterator itr = jcList.iterator();
        while(itr.hasNext()){
            activityTO = new RenderServiceTO();
            activityTO = (RenderServiceTO) itr.next();
            activityTO.setAddressSplit( addressSplit( activityTO.getAddress(),34  ) );
            newJcList.add(activityTO);    
        }
        return newJcList;    
    }

    public ArrayList getBillItemList(int billNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getBillItemList(billNo);
        return jcList;
    }
    public ArrayList getExtJobCardBillDetails(String billNo) throws FPBusinessException, FPRuntimeException {
        ArrayList extJobCardBillDetails = new ArrayList();
        extJobCardBillDetails = renderServiceDAO.getExtJobCardBillDetails(billNo);
        return extJobCardBillDetails;
    }

    public ArrayList getBillLaborCharge(int billNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        ArrayList newJcList = new ArrayList();
        jcList = renderServiceDAO.getBillLaborCharge(billNo);
        ProblemActivitiesTO rendTO = new ProblemActivitiesTO();
        Iterator itr = jcList.iterator();
        while(itr.hasNext()){
            rendTO = new ProblemActivitiesTO();
            rendTO = (ProblemActivitiesTO)itr.next();
            rendTO.setActivitySplt( addressSplit( rendTO.getProblemName(),80  ) );
            newJcList.add(rendTO);    
        }
        return newJcList;
    }

    public ArrayList getBillTax(int billNo) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getBillTax(billNo);
        return jcList;
    }
    public String getBillServiceTax(int billNo) throws FPBusinessException, FPRuntimeException {
        String serviceTax = renderServiceDAO.getBillServiceTax(billNo);
        return serviceTax;
    }
    public int saveExtJobCardBill(int userId,String jobCardId,String invoiceNo,String invoiceRemarks,String invoiceDate,
            String sparesAmt,String sparesRemarks,String consumableAmt,String consumableRemarks,
            String laborAmt,String laborRemarks,String othersAmt,String othersRemarks,String totalAmt,String vatPercent,
                       String vatAmt, String serviceTaxPercent,String serviceTaxAmt) throws FPBusinessException, FPRuntimeException {
        int billNo = renderServiceDAO.saveExtJobCardBill(userId,jobCardId,invoiceNo,invoiceRemarks,invoiceDate,sparesAmt,sparesRemarks,
                        consumableAmt,consumableRemarks,laborAmt,laborRemarks,othersAmt,othersRemarks,totalAmt,vatPercent,
                       vatAmt, serviceTaxPercent,serviceTaxAmt);
        String remarks = "";
        renderServiceDAO.updateJobCardStatus(Integer.parseInt(jobCardId), "Billing Completed", userId, remarks,"0","0","0","0");
        return billNo;
    }
    public int saveExtJobCardBillNew(int userId,String jobCardId,String invoiceNo,String invoiceRemarks,String invoiceDate) throws FPBusinessException, FPRuntimeException {
        int billNo = renderServiceDAO.saveExtJobCardBillNew(userId,jobCardId,invoiceNo,invoiceRemarks,invoiceDate);
        String remarks = "";
        renderServiceDAO.updateJobCardStatus(Integer.parseInt(jobCardId), "Billing Completed", userId, remarks,"0","0","0","0");
        return billNo;
    }
    public String getBillServiceTaxPercent(int billNo) throws FPBusinessException, FPRuntimeException {
        String serviceTax = renderServiceDAO.getBillServiceTaxPercent(billNo);
        return serviceTax;
    }
    public String getActualLaborExpense(String jcId) throws FPBusinessException, FPRuntimeException {
        String laborExpense = renderServiceDAO.getActualLaborExpense(jcId);
        return laborExpense;
    }
    public String getActualLaborExpenseForClosing(String jcId) throws FPBusinessException, FPRuntimeException {
        String laborExpense = renderServiceDAO.getActualLaborExpenseForClosing(jcId);
        return laborExpense;
    }

    public String getContractVendors(int billNo) throws FPBusinessException, FPRuntimeException {
        String vendorName = "";
        vendorName = renderServiceDAO.getContractVendors(billNo);
        return vendorName;
    }
    public String getActivityDetails(String acode,String modelId, String mfrId) throws FPBusinessException, FPRuntimeException {
        String activityDetails = "";
        activityDetails = renderServiceDAO.getActivityDetails(acode, modelId, mfrId);
        return activityDetails;
    }
    
    public String[] addressSplit(String longText,int spltSize){
            String[] splitText = null;
            int lower=0,higher;
            int splitSize = spltSize;
            int size = longText.length();
            splitText = new String[(size/splitSize)+1];
            for(int i=0;i<(size/splitSize)+1; i++){
                    if( (size-lower) < splitSize )
                    higher = (size);
                    else
                    higher = lower + splitSize;
                    splitText[i] = longText.substring(lower,higher);
                    System.out.println(splitText[i]);
                    lower = lower + splitSize;        
            }
        return splitText;
    } 

 public ArrayList getGroupList() throws FPBusinessException, FPRuntimeException {
        ArrayList groupList = new ArrayList();
        groupList = renderServiceDAO.getGroupList();
        return groupList;
    }
 public ArrayList getSubGroupList(int billGroupId) throws FPBusinessException, FPRuntimeException {
        ArrayList subGroupList = new ArrayList();
        subGroupList = renderServiceDAO.getSubGroupList(billGroupId);
        return subGroupList;
    }
 public ArrayList getVehicleList(int vehicleTypeId) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = renderServiceDAO.getVehicleList(vehicleTypeId);
        return vehicleList;
    }

   public int updateSubGroup(RenderServiceTO renderServiceTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateSubGroup = 0;
        updateSubGroup = renderServiceDAO.updateSubGroup(renderServiceTO, userId);
        return updateSubGroup;
    }

    public String checkSubGroupName(RenderServiceTO renderServiceTO) {
        String checkSubGroupName = "";
        checkSubGroupName = renderServiceDAO.checkSubGroupName(renderServiceTO);
        return checkSubGroupName;
    }


     public int saveSubGroupName(RenderServiceTO renderServiceTO, int userId) throws FPRuntimeException, FPBusinessException {
        int saveSubGroupName = 0;
        saveSubGroupName = renderServiceDAO.saveSubGroupName(renderServiceTO, userId);
        return saveSubGroupName;
    }
     public int insertJobCardBillDetails(RenderServiceTO renderServiceTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertBillDetails = 0;
        insertBillDetails = renderServiceDAO.insertJobCardBillDetails(renderServiceTO, userId);
        return insertBillDetails;
    }
      public ArrayList getServiceList() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceList = new ArrayList();
        serviceList = renderServiceDAO.getServiceList();
        return serviceList;
    }

      public ArrayList getBilledJobCardForView(RenderServiceTO rendTO) throws FPBusinessException, FPRuntimeException {
        ArrayList jcList = new ArrayList();
        jcList = renderServiceDAO.getBilledJobCardForView(rendTO);

        RenderServiceTO activityTO = new RenderServiceTO();
        Iterator itr = jcList.iterator();
        String closedDetails = "";
        String temp[] = null;
        while(itr.hasNext()){
            activityTO = new RenderServiceTO();
            activityTO = (RenderServiceTO) itr.next();
            closedDetails = renderServiceDAO.getClosedDetails(activityTO.getJobCardId());
            if(closedDetails != null) {
                temp = closedDetails.split("~");
                activityTO.setClosedBy( temp[0] );
                activityTO.setClosedOn( temp[1] );
            }

        }

        if (jcList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return jcList;
    }

      public ArrayList getBillGroupDetails(int jobCardNo) throws FPBusinessException, FPRuntimeException {
        ArrayList  billGroupDetails = new ArrayList();
         billGroupDetails = renderServiceDAO.getBilledJobCardGroupDetails(jobCardNo);

        Iterator itr =  billGroupDetails.iterator();
        Float sparePercent = 0.00f;
        Float labourPercent = 0.00f;
        while (itr.hasNext()) {
            RenderServiceTO rto = new RenderServiceTO();
            rto = (RenderServiceTO) itr.next();
            //////System.out.println("B4 Labor"+rto.getLabourPercentage());
            //////System.out.println("B4 Spares"+rto.getSparesPercentage());
//            sparePercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
            //labourPercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
            //BalaGanesh
           // labourPercent = 1 + Float.parseFloat(rto.getLabourPercentage()) / 100;
            //////System.out.println("sparePercent"+sparePercent);
            //////System.out.println("labourPercent"+labourPercent);
            rto.setItemsList(renderServiceDAO.getJobCardItemList(jobCardNo, rto.getCustId(), sparePercent));
            rto.setProblemActivityList(renderServiceDAO.getJobCardActivityList(jobCardNo, labourPercent));
            rto.setActivityList(renderServiceDAO.getActivityList(jobCardNo));

        }

        return  billGroupDetails;
    }


public ArrayList getBillDetails(int jobCardNo) throws FPBusinessException, FPRuntimeException {
        ArrayList  billDetails = new ArrayList();
         billDetails = renderServiceDAO.getBilledJobCardDetails(jobCardNo);

        Iterator itr =  billDetails.iterator();
        Float sparePercent = 0.00f;
        Float labourPercent = 0.00f;
        while (itr.hasNext()) {
            RenderServiceTO rto = new RenderServiceTO();
            rto = (RenderServiceTO) itr.next();
            //////System.out.println("B4 Labor"+rto.getLabourPercentage());
            //////System.out.println("B4 Spares"+rto.getSparesPercentage());
//            sparePercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
            //labourPercent = 1 + Float.parseFloat(rto.getSparesPercentage()) / 100;
            //BalaGanesh
           // labourPercent = 1 + Float.parseFloat(rto.getLabourPercentage()) / 100;
            //////System.out.println("sparePercent"+sparePercent);
            //////System.out.println("labourPercent"+labourPercent);
            rto.setItemsList(renderServiceDAO.getJobCardItemList(jobCardNo, rto.getCustId(), sparePercent));
            rto.setProblemActivityList(renderServiceDAO.getJobCardActivityList(jobCardNo, labourPercent));
            rto.setActivityList(renderServiceDAO.getActivityList(jobCardNo));

        }

        return  billDetails;
    }

    public int insertTyerDetails(RenderServiceTO renderServiceTO, int userId) throws FPRuntimeException, FPBusinessException {
            int insertTyerDetails = 0;
            insertTyerDetails = renderServiceDAO.insertTyerDetails(renderServiceTO, userId);
            return insertTyerDetails;
    }


}


