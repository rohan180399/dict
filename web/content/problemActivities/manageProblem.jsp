<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
if(value=='GoTo'){
var temp=document.vendorDetail.GoTo.value;       
document.vendorDetail.pageNo.value=temp;
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageProblemAct.do';   
document.vendorDetail.submit();
}else if(value == "First"){
temp ="1";
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}else if(value == "Last"){
temp =document.vendorDetail.last.value;
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageProblemAct.do';   
document.vendorDetail.submit();
}else if (value=='add')
{
document.vendorDetail.action ='/throttle/addProblemPage.do';
document.vendorDetail.submit();

}
            
}
    </script>
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="serviceplan.label.ManageProblem" text="default text"/>  </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.ManageProblem" text="default text"/></li>

        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body>
        
        <form method="post" name="vendorDetail">
            
       
 
<%@ include file="/content/common/message.jsp" %>
 
 <% int index = 0;  %>    
            
            <c:if test = "${ProblemActivities != null}" >
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                    <tr align="center">
                        <th width="33" height="30" ><div ><spring:message code="serviceplan.label.SNo" text="default text"/></div></th>
                        <th width="101" height="30" ><div ><spring:message code="serviceplan.label.ProblemName" text="default text"/></div></th>
                        <th width="100" height="30" ><div ><spring:message code="serviceplan.label.SectionName" text="default text"/></div></th>
                        <th width="161" height="30" ><div ><spring:message code="serviceplan.label.Description" text="default text"/></div></th>
                        <th width="63" height="30" ><div ><spring:message code="serviceplan.label.Status" text="default text"/></div></th>
			<th width="40" height="30" ><div >&nbsp;</div></th>
                         
                    </tr>
                    </thead>
                    <%
                    int pag = 0;
                    if(request.getAttribute("pageNo") != null) {
                        pag = (Integer)request.getAttribute("pageNo");
                    }
                    index = ((pag -1) *10) +1 ;
                    %>
                    
                    <c:forEach items="${ProblemActivities}" var="list"> 
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  > 
                             
                            
                            <td  height="30"><%= (index) %></td>
                            <td  height="30"><c:out value="${list.problemName}"/></td>
                            <td  height="30"><c:out value="${list.sectionName}"/></td>
                             
                             <td  height="30"><c:out value="${list.discription}"/></td>
                             
                            <input type="hidden" name="sectionIds" value='<c:out value="${list.sectionId}"/>'>
                               
                                
                                
                             
                             
                            <td  height="30">                                
                                <c:if test = "${list.activeInd == 'Y' }" >
                                   <spring:message code="serviceplan.label.Active" text="default text"/>  
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    <spring:message code="serviceplan.label.InActive" text="default text"/>
                                </c:if>
                            </td>
                             <td  align="left"> <a href="/throttle/alterProblemPage.do?problemId=<c:out value='${list.problemId}' />" ><spring:message code="serviceplan.label.ALTER" text="default text"/> </a> </td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>

            </c:if> 

<table align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
 <%@ include file="/content/common/pagination.jsp"%>  
 </td>
 
</tr>
</table>
 <br>
            <center>
                <input type="button" name="add" value="<spring:message code="serviceplan.label.ADD" text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success" >
                <input type="hidden" name="reqfor" value="">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

