<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : gopi
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
 <%@ page import="java.util.* "%>
 <%@ page import="java.util.Date "%>
 <%@ page import="java.text.SimpleDateFormat "%>
 <%@ page import="java.text.DateFormat "%>
 <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PrintTripSheet</title>
    </head>
    <body>
        <form name="enter" method="post">
        <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String startDate = sdf.format(today);
        String startDate1 = (String)request.getAttribute("grDate");
        String dateCheck = "01-07-2017";
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        Date oldDate = formater.parse(dateCheck);
        Date newDate = formater.parse(startDate1);
        %>
            <div id="printContent">
            <table align="center"  style="border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                /*border-color:#CD853F;width:800px;*/
                border:1px solid;
                /*opacity:0.8;*/
                font-weight:bold;">
                <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <table align="left" >
                        <tr>
                        <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                        <!--<td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>-->
                             <td style="padding-left: 180px; padding-right:42px;">
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <center><font size="1">SUBJECT TO DELHI JURISDICTION</font><br></center>
                        <font size="4"><b>Delhi International Cargo Terminal Pvt.Ltd.</b></font><br>
                        <center> <font size="2" >  
                            Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
                            E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in,Tel:0130-2203400  </font></center>
                </td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; ">
                        <table>
                            <tr>
                                <td width="40%"><font size="2">Booking Office:<u>DICT SONEPAT</u></font></td>
                        <td style="float: left;padding-left: 160px;" width="100%"><center><font size="2">CONSIGNOR COPY</font></center></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <font size="2">Consignor's Name & Address:</font> <font size="2"> <c:out value="${consignorName}"/><br><c:out value='${consignorAddress}'/></font>  <br>
                                    <br>
                                    <font size="2">Consignee's Name & Address:</font><font size="2"><c:out value="${consigneeName}"/> <br><c:out value="${consigneeAddress}"/></font>
                                    <br>
                                    <br>
                                    <br>
                                     <font size="2">Billing Party: </font><font size="2"><c:out value="${billingParty}"/></font>
                                    <br>
                                    <font size="2">Movement Type:<b><c:out value='${orderType}'/></b></font>
                                    </td>
                                    <td style="border-right:solid #888 1px;">
                                    <table width="100%">
                                        <tr>
                                            <td><font size="2">AT OWNER'S RISK</font></td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <font size="2">INSURANCE<br>
                                                The Customer has Stated That:-<br>
                                                He has not insured the consignment.&nbsp;[]<br>
                                                He has insured the consignment.&nbsp;&nbsp;[]<br>
                                                Company.........................................<br>
                                                Policy No.........................................</font><br>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                            <td>
                                <table width="100%" style="border-collapse: collapse;">
                                    <tr>
                                        <td><font size="2">G.R.NO: DICT/<b><c:out value='${grNumber}'/> </b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Date:<b><c:out value="${grDate}"/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">From:<b><c:out value="${toCityName}"/></b></font></td>
                                    </tr>


					<tr>
					<td><font size="2">To:<b><c:out value="${interamPoint}"/></b></font></td>
					</tr>
					<tr>

                                    <tr>
                                    <td><font size="2">To:<b><c:out value="${fromCityName}"/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Vehicle No:<b><c:out value='${vehicleNo}'/></b></font></td>
                                    </tr>
                                </table>
                            </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">

                        <table width='100%'>
                            <tr>
                                <td>
                                    <table style="border-collapse: collapse;" width='100%'>
                                    <tr>
                                    <td>
                                    <table border='0' style="border-collapse: collapse;border-spacing: 0px;" width='100%'>
                                    <thead>
                                    <tr style="height:20px;padding:0px;margin:0px;white-space: pre; " >
                                    <th style="border: solid #888 1px;"><h1><font size="2">No of PKG</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Description Of Goods(Said To Contain)</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Container No</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;Size&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;S. Line&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seal No&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</font> </h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Actual Weight</font></h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Charged Weight</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1> <font size="2">Rate</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Amount To Pay/Paid Rs.</font></h1></th>
                                   </tr>
                                </thead>
                                <tbody>

                                <tr style="height:20px;padding:0px;margin:0px; " >
                                <td rowspan="2" style="border: solid #888 1px; ">   </td>
                                <td rowspan="2" style="border: solid #888 1px;"> <c:out value='${articleName}'/>  </td>
                                <td style="border: solid #888 1px;">

                                    <c:out value='${containerNo}'/>
                                    <br>  <c:out value='${containerNo1}'/>
                                   </td>
                                <td style="border: solid #888 1px;  "> <c:out value='${containerTypeId}'/>   <br>  <c:out value='${containerTypeId1}'/></td>
                                <td style="border: solid #888 1px;  ">  <c:out value='${linerName}'/>  <br>  <c:out value='${linerName1}'/></td>
                                <td style="border: solid #888 1px;  "><c:out value='${sealNo}'/>  <br>  <c:out value='${sealNo1}'/> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td  style="border: solid #888 1px;"><c:out value='${grStatus}'/> </td>
                                 </tr>
                                <tr style="height:20px;padding:0px;margin:0px; " >
                                <td colspan="8" style="border: solid #888 1px; border-top: none; margin:10px;">
                                    <br>
                                    <table >
                                        <tr> <td style="padding-bottom:10px">ICD Out Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                         <tr > <td style="padding-bottom:10px">Factory In Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                        <tr > <td style="padding-bottom:10px">Factory Out Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                             <tr ><td style="padding-bottom:10px">ICD In Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                     </table>
                                </td>



                                 </tr>




                                </tbody>
                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
<!--                                <td>&nbsp;</td>-->

                            </tr>


                            <tr>
                                <td>
                                    <br>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                            <td style="border: solid #888 1px;"><table  width='100%' style="border-collapse: collapse;">
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party Invoice No & Date:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party  CST/TIN No:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Value Of Goods:Rs. </font>
                                                           <%-- <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                                                <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreight")));%>
                                                                                      <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                    </jsp:useBean></b> --%>
        </td></tr>
                                                </table></td>
                                                <td style="border: solid #888 1px;"><font sze='1' style="float: top;"> Permit No:</font></td>
                                            <td style="border: solid #888 1px;border-left: none;"><font sze='1' style="float: top;"> Remarks:</font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                        <font size="2">
                        
                      
                      <%  
                          if (newDate.after(oldDate)) {%>
                        1)GST No:06AACCB8054G1ZT,GST UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>
                        <%}else{%>
                              1)Service Tax No:AACCB8054GST001 SERVICE TAX UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>   
                              <%  }%>
                        2)PAN NO-AACCB8054G  &nbsp;&nbsp; &nbsp;&nbsp;  CIN NO:U63040MH2006PTC159885  &nbsp;&nbsp; <font size="3" width=2' style="float: right;"><b>Authorised Signature</b></font> <br>
                        3)Goods accepted for carriage on the terms & conditions printed over leaf.<br> </font>
                    </td>
                </tr>

            </table>
            <table align="center" style="border-collapse: collapse">
                   <tr>
                    <td>
                         <b> Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br>
                        <b>Branches:</b>Loni,Ludhiana,Dadri,Tughlakbad
                    </td>
                </tr>
            </table>
             <br>
             <table align="center" style="border-collapse: collapse">
                 <tr><td><center><font size="1">  Declaration </font></center></td></tr>
                    <tr><td></td></tr>
                 <tr> <td> <font size="1">1. We hereby declare that we have not available under ST Notification No.12/2003,dated 20/06/2003 of Govt.of India,Ministry of Finance
                         </font></td></tr>
                 <tr><td><font size="1">2. We hereby declare that we have not taken the credit of Excise Duty on inputs or capital goods used for providing the "Transport of Goods by road"Services under the provision of  Cenvat Credit Rules,2004
                         </font>    </td> </tr>
                 <tr><td><center><font size="1">
        Terms & Condition </font></center>
</td>

    </tr>
<tr><td><font size="1">a. Unless other wise agreed all consignment/goods are carried entirely at OWNERS risk as to packaging ,Goods contents,nature,condition and value of the Goods that are declared and are unknown to the carrier.The consignor shall indemnify the carrier against the loss resulting from inaccuracies or inadequacies of the particulars.

    </font></td>
    </tr>
    <tr><td><font size="1">b. Carrier will carry all goods on said to contain basis and will not stand liable for any internal discrepancy.

            </font> </td>
    </tr>

    <tr><td><font size="1">c. Goods to be insured by the consignor at their own cost to protect themselves against any losses and/or risk during transit by road.

            </font></td>
    </tr>
    <tr><td><font size="1">d. Carrier explicity reserves right to refusal for carriage of goods by road and such right shall be absolute.
</font>
    </td>
    </tr>
    <tr><td><font size="1">e. Transport charges are payable with in 24 Hours from loading the Goods on transport vehicle of carriers.Failure to pay transport charges prior to the arrival of Goods at destination shall attract penalty @ 2% on the transport charges.Octrol charges any other statutory government levels to be paid by consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">f. Carrier reserves right to re-measure and/or re-weight the goods of any discrepancy noted on the actual and charged weight, carrier shall impose penalty plus the rates for differential weight.
</font>
    </td>
    </tr>
    <tr><td><font size="1">g. Carrier shall not be held liable if goods are detained or security by competent authority for any misdeclaration or duty unpaid or prohibited Goods, Carrier shall not be liable for any losses, damage or deterioration to the Goods caused by such detention for examination or scrutiny.
</font>
    </td>
    </tr>
    <tr><td><font size="1">h. Carrier shall deliver the Goods within the normal transit time and shall not be liable for any delay in delivery,except where expressly agreed and such written request for timely delivery is submitted subject to force majeure conditions.
</font>
    </td>
    </tr>
    <tr><td><font size="1">i. Carrier shall make available the goods at the disposal of consignee or their agents at premises or carrier and same to be collected with in five (5) days from the date of arrival of Goods,failing which storage charges shall be payable in additional to transport charges.Carrier shall not be responsible for any damage and/or loss whatsoever to Goods stored at premises.
    </font></td>
    </tr>
    <tr><td><font size="1">j. Carrier shall have right to dispose and/or auction,the perishable goods within forty eight(48) hours of the arrival at premises if uncleared,and thirty(30)days for other goods without any notice to the consignee/ consignor.The disposal charges along with the storage charges for such period shall be payable to the consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">k. Carrier shall not be responsible for delay in transit due to unforeseen contingencies,or act of god,or due to strike,war,civil commotions,and/or,events which are beyond the control of Carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">l. Carrier shall not be liable for any loss and/or damages to goods arising out of resulting due to Act of God,war,public or state emergencies,robbery,theft,dacoity,damage due to defect in packaging materials,or due to events that are beyond the control of carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">m. Carrier shall not be responsible for damages to the inner content of the packages or Goods if the same has been delivered in same condition in which they were tendered for road transportation.Any kind of shortage has to be informed within twenty four(24)hrs of receipt of the goods with documented proof.In case of seal broken upon arrival the count of goods shall be taken jointly.
</font>
    </td>
    <tr><td><font size="1">n. No claim for compensation shall be entertained by the carrier after a period of seven(7) days from the date of delivery of the Goods along with documented/evidence and carrier has right to reject the same without assigning any reasons whatsoever.
</font>
    </td>
    </tr>
    <tr><td><font size="1">o. No suit or other legal proceeding shall be initiated,unless notice in writing has been served upon carrier before institution of suit or legal proceeding within 180 days from date receipt of Goods or from the date of booking.
</font>
    </td>
    </tr>
    <tr><td><font size="1">p. Any special instruction unless expressly written by the consignor shall not be entertained.
</font>
    </td>
    </tr>
   <tr><td><font size="1">q. The contract evidenced hereby shall be governed by and construed according to the Indian laws and any difference of opinion or dispute there under shall be settled by arbitration Delhi with each party appointing an arbitrator.
</font>
    </td>
    </tr>
     <tr><td><font size="1">r. In respect of any and/or all claims arising under this contract,The Courts of Delhi shall have exclusive jurisdiction.
</font>
    </td>
    </tr>
    <tr><td><font size="1">s. Received the Goods Specified overleap in good & proper condition.
</font>
    </td>
    </tr>
             </table>

    <br>

    <table align="left" style="border-collapse: collapse">

    <tr><td><font size="1">Signature of consignee or Agent with Rubber Stamp

</font>
    </td>
    </tr>
    <tr> <td></td></tr>
    <tr> <td></td></tr>
    <tr><td><font size="1">Date</font>


    </td>
    </tr>

   </table>

            <!-- Consignee Copy      -->


            <br>
            <br>
           <br>


            <br>
            <br>
            <br>
            <br>
            <br>
	               <br>
	               <br>
	               <br>
           <br>
	   	               <br>
	   	               <br>

              <table align="center"  style="border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                /*border-color:#CD853F;width:800px;*/
                border:1px solid;
                /*opacity:0.8;*/
                font-weight:bold;">
                <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <table align="left" >
                        <tr  >
                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <!--<td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>-->
                             <td style="padding-left: 180px; padding-right:42px;">
			       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			       <center><font size="1">SUBJECT TO DELHI JURISDICTION</font><br></center>
			    <font size="4"><b>Delhi International Cargo Terminal Pvt.Ltd.</b></font><br>
			    <center> <font size="2" >  
				Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
				E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in ,Tel:0130-2203400  </font></center>
                </td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; ">
                        <table >
                            <tr>
                                <td width="40%"><font size="2">Booking Office:<u>DICT SONEPAT</u></font></td>
                        <td style="float: left;padding-left: 160px;" width="100%"><center><font size="2">CONSIGNEE COPY</font></center></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <font size="2">Consignor's Name & Address:</font> <font size="2"> <c:out value="${consignorName}"/><br><c:out value='${consignorAddress}'/></font>  <br>
                                    <br>
                                    <font size="2">Consignee's Name & Address:</font><font size="2"><c:out value="${consigneeName}"/> <br><c:out value="${consigneeAddress}"/></font>
                                    <br>
                                    <br>
				  <font size="2">Billing Party: </font><font size="2"><c:out value="${billingParty}"/></font>
                                    <br>
                                    <font size="2">Movement Type:<b><c:out value='${orderType}'/></b></font>
                                    </td>
                                    <td style="border-right:solid #888 1px;">
                                    <table width="100%">
                                        <tr>
                                            <td><font size="2">AT OWNER'S RISK</font></td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <font size="2">INSURANCE<br>
                                                The Customer has Stated That:-<br>
                                                He has not insured the consignment.&nbsp;[]<br>
                                                He has insured the consignment.&nbsp;&nbsp;[]<br>
                                                Company.........................................<br>
                                                Policy No.........................................</font><br>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                            <td>
                                <table width="100%" style="border-collapse: collapse;">
                                    <tr>
                                        <td><font size="2">G.R.NO: DICT/<b><c:out value='${grNumber}'/>  </b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Date:<b><c:out value='${grDate}'/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">From:<b><c:out value="${toCityName}"/></b></font></td>
                                    </tr>


					<tr>
					<td><font size="2">To:<b><c:out value="${interamPoint}"/></b></font></td>
					</tr>
					<tr>

					<tr>
					<td><font size="2">To:<b><c:out value="${fromCityName}"/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Vehicle No:<b><c:out value='${vehicleNo}'/></b></font></td>
                                    </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">

                        <table width='100%'>
                            <tr>
                                <td>
                                    <table style="border-collapse: collapse;" width='100%'>
                                    <tr>
                                    <td>
                                    <table border='0' style="border-collapse: collapse;" width='100%'>
                                    <thead>
                                    <tr style="height:20px;padding:0px;margin:0px;white-space: pre; " >
                                    <th style="border: solid #888 1px;"><h1><font size="2">No of PKG</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Description Of Goods(Said To Contain)</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Container No</font></h1></th>
                                     <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;Size&nbsp;</font></h1></th>
                                     <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;S. Line&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1><font size="2">Seal No</font> </h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Actual Weight</font></h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Charged Weight</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1> <font size="2">Rate</font></h1></th>
                                    <th style="border: solid #888 1px;border-left: none;"><h1><font size="2">Amount To Pay/Paid Rs.</font></h1></th>
                                   </tr>
                                </thead>
                                <tbody>

                                <tr style="height:20px;padding:0px;margin:0px; " >
                                <td rowspan="2" style="border: solid #888 1px; ">   </td>
                                <td rowspan="2" style="border: solid #888 1px;">  <c:out value='${articleName}'/>  </td>
                                <td style="border: solid #888 1px;">

                                    <c:out value='${containerNo}'/>
                                    <br>  <c:out value='${containerNo1}'/>
                                   </td>
                                <td style="border: solid #888 1px;  "> <c:out value='${containerTypeId}'/>   <br>  <c:out value='${containerTypeId1}'/></td>
                                <td style="border: solid #888 1px;  ">  <c:out value='${linerName}'/>  <br>  <c:out value='${linerName1}'/></td>
                                <td style="border: solid #888 1px;  "><c:out value='${sealNo}'/>  <br>  <c:out value='${sealNo1}'/> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td  style="border: solid #888 1px;"><c:out value='${grStatus}'/>  </td>
                                 </tr>
                                <tr style="height:20px;padding:0px;margin:0px; " >

                                 <td colspan="8" style="border: solid #888 1px; border-top: none; margin:10px;">
                                    <br>
                                    <table >
                                        <tr> <td style="padding-bottom:10px">ICD Out Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                         <tr > <td style="padding-bottom:10px">Factory In Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                        <tr > <td style="padding-bottom:10px">Factory Out Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                             <tr ><td style="padding-bottom:10px">ICD In Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                     </table>
                                </td>



                                 </tr>





                                </tbody>
                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
<!--                                <td>&nbsp;</td>-->

                            </tr>


                            <tr>
                                <td>
                                    <br>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                            <td style="border: solid #888 1px;"><table  width='100%' style="border-collapse: collapse;">
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party Invoice No & Date:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party  CST/TIN No:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Value Of Goods:Rs. </font>
                                                       <%--         <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                                            <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreight")));%>
                                                                                      <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                    </jsp:useBean></b> --%>
                                                    </td></tr>
                                                </table></td>
                                                <td style="border: solid #888 1px;"><font sze='1' style="float: top;"> Permit No:</font></td>
                                            <td style="border: solid #888 1px;border-left: none;"><font sze='1' style="float: top;"> Remarks:</font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                     <td style="border: solid #888 1px; border-top: none; ">
                        <font size="2">
                     <%  
                          if (newDate.after(oldDate)) {%>
                        1)GST No:06AACCB8054G1ZT,GST UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>
                        <%}else{%>
                              1)Service Tax No:AACCB8054GST001 SERVICE TAX UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>   
                              <%  }%>
                        2)PAN NO-AACCB8054G &nbsp;&nbsp; &nbsp;&nbsp;  CIN NO:U63040MH2006PTC159885 &nbsp;&nbsp; <font size="3" width=2' style="float: right;"><b>Authorised Signature</b></font><br>
                        3)Goods accepted for carriage on the terms & conditions printed over leaf.<br> </font>
                    </td>
                </tr>

            </table>
                 <table align="center" style="border-collapse: collapse">
                   <tr>
                    <td>
                         <b> Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br>
                        <b>Branches:</b>Loni,Ludhiana,Dadri,Tughlakbad
                    </td>
                </tr>
            </table>
                   <br>
             <table align="center" style="border-collapse: collapse">
                 <tr><td><center><font size="1">  Declaration </font></center></td></tr>
                    <tr><td></td></tr>
                 <tr> <td> <font size="1">1. We hereby declare that we have not available under ST Notification No.12/2003,dated 20/06/2003 of Govt.of India,Ministry of Finance
                         </font></td></tr>
                 <tr><td><font size="1">2. We hereby declare that we have not taken the credit of Excise Duty on inputs or capital goods used for providing the "Transport of Goods by road"Services under the provision of  Cenvat Credit Rules,2004
                         </font>    </td> </tr>
                 <tr><td><center><font size="1">
        Terms & Condition </font></center>
</td>

    </tr>
<tr><td><font size="1">a. Unless other wise agreed all consignment/goods are carried entirely at OWNERS risk as to packaging ,Goods contents,nature,condition and value of the Goods that are declared and are unknown to the carrier.The consignor shall indemnify the carrier against the loss resulting from inaccuracies or inadequacies of the particulars.

    </font></td>
    </tr>
    <tr><td><font size="1">b. Carrier will carry all goods on said to contain basis and will not stand liable for any internal discrepancy.

            </font> </td>
    </tr>

    <tr><td><font size="1">c. Goods to be insured by the consignor at their own cost to protect themselves against any losses and/or risk during transit by road.

            </font></td>
    </tr>
    <tr><td><font size="1">d. Carrier explicity reserves right to refusal for carriage of goods by road and such right shall be absolute.
</font>
    </td>
    </tr>
    <tr><td><font size="1">e. Transport charges are payable with in 24 Hours from loading the Goods on transport vehicle of carriers.Failure to pay transport charges prior to the arrival of Goods at destination shall attract penalty @ 2% on the transport charges.Octrol charges any other statutory government levels to be paid by consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">f. Carrier reserves right to re-measure and/or re-weight the goods of any discrepancy noted on the actual and charged weight, carrier shall impose penalty plus the rates for differential weight.
</font>
    </td>
    </tr>
    <tr><td><font size="1">g. Carrier shall not be held liable if goods are detained or security by competent authority for any misdeclaration or duty unpaid or prohibited Goods, Carrier shall not be liable for any losses, damage or deterioration to the Goods caused by such detention for examination or scrutiny.
</font>
    </td>
    </tr>
    <tr><td><font size="1">h. Carrier shall deliver the Goods within the normal transit time and shall not be liable for any delay in delivery,except where expressly agreed and such written request for timely delivery is submitted subject to force majeure conditions.
</font>
    </td>
    </tr>
    <tr><td><font size="1">i. Carrier shall make available the goods at the disposal of consignee or their agents at premises or carrier and same to be collected with in five (5) days from the date of arrival of Goods,failing which storage charges shall be payable in additional to transport charges.Carrier shall not be responsible for any damage and/or loss whatsoever to Goods stored at premises.
    </font></td>
    </tr>
    <tr><td><font size="1">j. Carrier shall have right to dispose and/or auction,the perishable goods within forty eight(48) hours of the arrival at premises if uncleared,and thirty(30)days for other goods without any notice to the consignee/ consignor.The disposal charges along with the storage charges for such period shall be payable to the consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">k. Carrier shall not be responsible for delay in transit due to unforeseen contingencies,or act of god,or due to strike,war,civil commotions,and/or,events which are beyond the control of Carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">l. Carrier shall not be liable for any loss and/or damages to goods arising out of resulting due to Act of God,war,public or state emergencies,robbery,theft,dacoity,damage due to defect in packaging materials,or due to events that are beyond the control of carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">m. Carrier shall not be responsible for damages to the inner content of the packages or Goods if the same has been delivered in same condition in which they were tendered for road transportation.Any kind of shortage has to be informed within twenty four(24)hrs of receipt of the goods with documented proof.In case of seal broken upon arrival the count of goods shall be taken jointly.
</font>
    </td>
    <tr><td><font size="1">n. No claim for compensation shall be entertained by the carrier after a period of seven(7) days from the date of delivery of the Goods along with documented/evidence and carrier has right to reject the same without assigning any reasons whatsoever.
</font>
    </td>
    </tr>
    <tr><td><font size="1">o. No suit or other legal proceeding shall be initiated,unless notice in writing has been served upon carrier before institution of suit or legal proceeding within 180 days from date receipt of Goods or from the date of booking.
</font>
    </td>
    </tr>
    <tr><td><font size="1">p. Any special instruction unless expressly written by the consignor shall not be entertained.
</font>
    </td>
    </tr>
   <tr><td><font size="1">q. The contract evidenced hereby shall be governed by and construed according to the Indian laws and any difference of opinion or dispute there under shall be settled by arbitration Delhi with each party appointing an arbitrator.
</font>
    </td>
    </tr>
     <tr><td><font size="1">r. In respect of any and/or all claims arising under this contract,The Courts of Delhi shall have exclusive jurisdiction.
</font>
    </td>
    </tr>
    <tr><td><font size="1">s. Received the Goods Specified overleap in good & proper condition.
</font>
    </td>
    </tr>

    </table>
<br>

 <table align="left" style="border-collapse: collapse">

    <tr><td><font size="1">Signature of consignee or Agent with Rubber Stamp
    </font>
    </td>
    </tr>
    <tr> <td></td></tr>
    <tr> <td></td></tr>
    <tr><td><font size="1">Date </font>
    </td>
    </tr>

   </table>

                                                    <!-- office copy -->








            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
	                <br>
            <br>
            <br>
	                <br>
	                <br>

	                <br>
<br>
            <br>

            <br>
              <table align="center"  style="border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                /*border-color:#CD853F;width:800px;*/
                border:1px solid;
                /*opacity:0.8;*/
                font-weight:bold;">
                <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <table align="left" >
                        <tr>
                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <!--<td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>-->
                              <td style="padding-left: 180px; padding-right:42px;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<center><font size="1">SUBJECT TO DELHI JURISDICTION</font><br></center>
				     <font size="4"><b>Delhi International Cargo Terminal Pvt.Ltd.</b></font><br>
				     <center> <font size="2" >  
					 Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
					 E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in ,Tel:0130-2203400  </font></center>
</td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; ">
                        <table >
                            <tr>
                                <td width="40%"><font size="2">Booking Office:<u>DICT SONEPAT</u></font></td>
                        <td style="float: left;padding-left: 160px;" width="100%"><center><font size="2">OFFICE COPY</font></center></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <font size="2">Consignor's Name & Address:</font> <font size="2"> <c:out value="${consignorName}"/><br><c:out value='${consignorAddress}'/></font>  <br>
                                    <br>
                                    <font size="2">Consignee's Name & Address:</font><font size="2"><c:out value="${consigneeName}"/> <br><c:out value="${consigneeAddress}"/></font>
                                    <br>
                                    <br>
				         <font size="2">Billing Party: </font><font size="2"><c:out value="${billingParty}"/></font>
                                    <br>
                                    <font size="2">Movement Type:<b><c:out value='${orderType}'/></b></font>
                                    </td>
                                    <td style="border-right:solid #888 1px;">
                                    <table width="100%">
                                        <tr>
                                            <td><font size="2">AT OWNER'S RISK</font></td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <font size="2">INSURANCE<br>
                                                The Customer has Stated That:-<br>
                                                He has not insured the consignment.&nbsp;[]<br>
                                                He has insured the consignment.&nbsp;&nbsp;[]<br>
                                                Company.........................................<br>
                                                Policy No.........................................</font><br>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                            <td>
                                <table width="100%" style="border-collapse: collapse;">
                                    <tr>
                                        <td><font size="2">G.R.NO: DICT/<b><c:out value='${grNumber}'/>  </b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Date:<b><c:out value='${grDate}'/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">From:<b><c:out value="${toCityName}"/></b></font></td>
                                    </tr>


					<tr>
					<td><font size="2">To:<b><c:out value="${interamPoint}"/></b></font></td>
					</tr>
					<tr>

					<tr>
					<td><font size="2">To:<b><c:out value="${fromCityName}"/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Vehicle No:<b><c:out value='${vehicleNo}'/></b></font></td>
                                    </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">

                        <table width='100%'>
                            <tr>
                                <td>
                                    <table style="border-collapse: collapse;" width='100%'>
                                    <tr>
                                    <td>
                                    <table border='0' style="border-collapse: collapse;" width='100%'>
                                    <thead>
                                    <tr style="height:20px;padding:0px;margin:0px;white-space: pre; " >
                                    <th style="border: solid #888 1px;"><h1><font size="2">No of PKG</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Description Of Goods(Said To Contain)</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Container No</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;Size&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;S. Line&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1><font size="2">Seal No</font> </h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Actual Weight</font></h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Charged Weight</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1> <font size="2">Rate</font></h1></th>
                                    <th style="border: solid #888 1px;border-left: none;"><h1><font size="2">Amount To Pay/Paid Rs.</font></h1></th>
                                   </tr>
                                </thead>
                                <tbody>

                                <tr style="height:20px;padding:0px;margin:0px; " >
                                <td rowspan="2" style="border: solid #888 1px; ">   </td>
                                <td rowspan="2" style="border: solid #888 1px; "> <c:out value='${articleName}'/>   </td>
                                  <td style="border: solid #888 1px;">

                                    <c:out value='${containerNo}'/>
                                    <br>  <c:out value='${containerNo1}'/>
                                   </td>
                                <td style="border: solid #888 1px;  "> <c:out value='${containerTypeId}'/>   <br>  <c:out value='${containerTypeId1}'/></td>
                                <td style="border: solid #888 1px;  ">  <c:out value='${linerName}'/>  <br>  <c:out value='${linerName1}'/></td>
                                <td style="border: solid #888 1px;  "><c:out value='${sealNo}'/>  <br>  <c:out value='${sealNo1}'/> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td  style="border: solid #888 1px;"><c:out value='${grStatus}'/>  </td>
                                 </tr>
                                <tr style="height:20px;padding:0px;margin:0px; " >

                                <td colspan="8" style="border: solid #888 1px; border-top: none; margin:10px;">
                                    <br>
                                    <table >
                                        <tr> <td style="padding-bottom:10px">ICD Out Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                         <tr > <td style="padding-bottom:10px">Factory In Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                        <tr > <td style="padding-bottom:10px">Factory Out Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                             <tr ><td style="padding-bottom:10px">ICD In Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                     </table>
                                </td>



                                 </tr>





                                </tbody>
                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
<!--                                <td>&nbsp;</td>-->

                            </tr>


                            <tr>
                                <td>
                                    <br>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                            <td style="border: solid #888 1px;"><table  width='100%' style="border-collapse: collapse;">
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party Invoice No & Date:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party  CST/TIN No:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Value Of Goods:Rs. </font>
                                                       <%--         <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                                            <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreight")));%>
                                                                                      <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                    </jsp:useBean></b> --%>
                                                    </td></tr>
                                                </table></td>
                                                <td style="border: solid #888 1px;"><font sze='1' style="float: top;"> Permit No:</font></td>
                                            <td style="border: solid #888 1px;border-left: none;"><font sze='1' style="float: top;"> Remarks:</font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                       <font size="2">
                      <%  
                          if (newDate.after(oldDate)) {%>
                        1)GST No:06AACCB8054G1ZT,GST UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>
                        <%}else{%>
                              1)Service Tax No:AACCB8054GST001 SERVICE TAX UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>   
                              <%  }%>
                        2)PAN NO-AACCB8054G  &nbsp;&nbsp; &nbsp;&nbsp;  CIN NO:U63040MH2006PTC159885 &nbsp;&nbsp;<font size="3" width=2' style="float: right;"><b>Authorised Signature</b></font><br>
                        3)Goods accepted for carriage on the terms & conditions printed over leaf.<br> </font>
                    </td>
                </tr>

            </table>
                       <table align="center" style="border-collapse: collapse">
                   <tr>
                    <td>
                        <b> Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br>
                       <b>Branches:</b>Loni,Ludhiana,Dadri,Tughlakbad
                    </td>
                </tr>
            </table>
            <br>
             <table align="center" style="border-collapse: collapse">
                 <tr><td><center><font size="1">  Declaration </font></center></td></tr>
                    <tr><td></td></tr>
                 <tr> <td> <font size="1">1. We hereby declare that we have not available under ST Notification No.12/2003,dated 20/06/2003 of Govt.of India,Ministry of Finance
                         </font></td></tr>
                 <tr><td><font size="1">2. We hereby declare that we have not taken the credit of Excise Duty on inputs or capital goods used for providing the "Transport of Goods by road"Services under the provision of  Cenvat Credit Rules,2004
                         </font>    </td> </tr>
                 <tr><td><center><font size="1">
        Terms & Condition </font></center>
</td>

    </tr>
<tr><td><font size="1">a. Unless other wise agreed all consignment/goods are carried entirely at OWNERS risk as to packaging ,Goods contents,nature,condition and value of the Goods that are declared and are unknown to the carrier.The consignor shall indemnify the carrier against the loss resulting from inaccuracies or inadequacies of the particulars.

    </font></td>
    </tr>
    <tr><td><font size="1">b. Carrier will carry all goods on said to contain basis and will not stand liable for any internal discrepancy.

            </font> </td>
    </tr>

    <tr><td><font size="1">c. Goods to be insured by the consignor at their own cost to protect themselves against any losses and/or risk during transit by road.
    </font></td>
    </tr>
    <tr><td><font size="1">d. Carrier explicity reserves right to refusal for carriage of goods by road and such right shall be absolute.
</font>
    </td>
    </tr>
    <tr><td><font size="1">e. Transport charges are payable with in 24 Hours from loading the Goods on transport vehicle of carriers.Failure to pay transport charges prior to the arrival of Goods at destination shall attract penalty @ 2% on the transport charges.Octrol charges any other statutory government levels to be paid by consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">f. Carrier reserves right to re-measure and/or re-weight the goods of any discrepancy noted on the actual and charged weight, carrier shall impose penalty plus the rates for differential weight.
</font>
    </td>
    </tr>
    <tr><td><font size="1">g. Carrier shall not be held liable if goods are detained or security by competent authority for any misdeclaration or duty unpaid or prohibited Goods, Carrier shall not be liable for any losses, damage or deterioration to the Goods caused by such detention for examination or scrutiny.
</font>
    </td>
    </tr>
    <tr><td><font size="1">h. Carrier shall deliver the Goods within the normal transit time and shall not be liable for any delay in delivery,except where expressly agreed and such written request for timely delivery is submitted subject to force majeure conditions.
</font>
    </td>
    </tr>
    <tr><td><font size="1">i. Carrier shall make available the goods at the disposal of consignee or their agents at premises or carrier and same to be collected with in five (5) days from the date of arrival of Goods,failing which storage charges shall be payable in additional to transport charges.Carrier shall not be responsible for any damage and/or loss whatsoever to Goods stored at premises.
    </font></td>
    </tr>
    <tr><td><font size="1">j. Carrier shall have right to dispose and/or auction,the perishable goods within forty eight(48) hours of the arrival at premises if uncleared,and thirty(30)days for other goods without any notice to the consignee/ consignor.The disposal charges along with the storage charges for such period shall be payable to the consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">k. Carrier shall not be responsible for delay in transit due to unforeseen contingencies,or act of god,or due to strike,war,civil commotions,and/or,events which are beyond the control of Carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">l. Carrier shall not be liable for any loss and/or damages to goods arising out of resulting due to Act of God,war,public or state emergencies,robbery,theft,dacoity,damage due to defect in packaging materials,or due to events that are beyond the control of carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">m. Carrier shall not be responsible for damages to the inner content of the packages or Goods if the same has been delivered in same condition in which they were tendered for road transportation.Any kind of shortage has to be informed within twenty four(24)hrs of receipt of the goods with documented proof.In case of seal broken upon arrival the count of goods shall be taken jointly.
</font>
    </td>
    <tr><td><font size="1">n. No claim for compensation shall be entertained by the carrier after a period of seven(7) days from the date of delivery of the Goods along with documented/evidence and carrier has right to reject the same without assigning any reasons whatsoever.
</font>
    </td>
    </tr>
    <tr><td><font size="1">o. No suit or other legal proceeding shall be initiated,unless notice in writing has been served upon carrier before institution of suit or legal proceeding within 180 days from date receipt of Goods or from the date of booking.
 </font>
    </td>
    </tr>
    <tr><td><font size="1">p. Any special instruction unless expressly written by the consignor shall not be entertained.
</font>
    </td>
    </tr>
   <tr><td><font size="1">q. The contract evidenced hereby shall be governed by and construed according to the Indian laws and any difference of opinion or dispute there under shall be settled by arbitration Delhi with each party appointing an arbitrator.
</font>
    </td>
    </tr>
     <tr><td><font size="1">r. In respect of any and/or all claims arising under this contract,The Courts of Delhi shall have exclusive judicial.
</font>
    </td>
    </tr>
    <tr><td><font size="1">s. Received the Goods Specified overleap in good & proper condition.
</font>
    </td>
    </tr>

             </table>
           <br>
 <table align="left" style="border-collapse: collapse">

    <tr><td><font size="1">Signature of consignee or Agent with Rubber Stamp

</font>
    </td>
    </tr>
    <tr> <td></td></tr>
    <tr> <td></td></tr>
    <tr><td><font size="1">Date </font>


    </td>
    </tr>

   </table>


                                                    <!-- Driver copy-->




                                                <br>

                                                    <br>
                                                <br>
                                                <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
						                                                        <br>
						                                                        <br>
                                                    <br>
                <table align="center"  style="border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                /*border-color:#CD853F;width:800px;*/
                border:1px solid;
                /*opacity:0.8;*/
                font-weight:bold;">
                <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <table align="left" >
                        <tr  >
                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <!--<td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>-->
                             <td style="padding-left: 180px; padding-right:42px;">
		       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		       <center><font size="1">SUBJECT TO DELHI JUDICIAL</font><br></center>
		    <font size="4"><b>Delhi International Cargo Terminal Pvt.Ltd.</b></font><br>
		    <center> <font size="2" >
			Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
			E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in ,Tel:0130-2203400  </font></center>
                </td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; ">
                        <table>
                            <tr>
                                <td width="40%"><font size="2">Booking Office:<u>DICT SONEPAT</u></font></td>
                        <td style="float: left;padding-left: 160px;" width="100%"><center><font size="2">DRIVER COPY</font></center></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <font size="2">Consignor's Name & Address:</font> <font size="2"> <c:out value="${consignorName}"/><br><c:out value='${consignorAddress}'/></font>  <br>
                                    <br>
                                    <font size="2">Consignee's Name & Address:</font><font size="2"><c:out value="${consigneeName}"/> <br><c:out value="${consigneeAddress}"/></font>
                                    <br>
                                    <br>
				    <font size="2">Billing Party: </font><font size="2"><c:out value="${billingParty}"/></font>
                                    <br>
                                    <font size="2">Movement Type:<b><c:out value='${orderType}'/></b></font>
                                    </td>
                                    <td style="border-right:solid #888 1px;">
                                    <table width="100%">
                                        <tr>
                                            <td><font size="2">AT OWNER'S RISK</font></td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <font size="2">INSURANCE<br>
                                                The Customer has Stated That:-<br>
                                                He has not insured the consignment.&nbsp;[]<br>
                                                He has insured the consignment.&nbsp;&nbsp;[]<br>
                                                Company.........................................<br>
                                                Policy No.........................................</font><br>
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                            <td>
                                <table width="100%" style="border-collapse: collapse;">
                                    <tr>
                                        <td><font size="2">G.R.NO: DICT/<b><c:out value='${grNumber}'/>  </b></font></td>
                                    </tr>

                                    <tr>
                                        <td><font size="2">Date:<b><c:out value='${grDate}'/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">From:<b><c:out value="${toCityName}"/></b></font></td>
                                    </tr>


					<tr>
					<td><font size="2">To:<b><c:out value="${interamPoint}"/></b></font></td>
					</tr>
					<tr>

					<tr>
					<td><font size="2">To:<b><c:out value="${fromCityName}"/></b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="2">Vehicle No:<b><c:out value='${vehicleNo}'/></b></font></td>
                                    </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                     <td style="border: solid #888 1px; border-top: none; ">

                        <table width='100%'>
                            <tr>
                                 <td>
                                    <table style="border-collapse: collapse;" width='100%'>
                                    <tr>
                                    <td>
                                    <table border='0' style="border-collapse: collapse;" width='100%'>
                                    <thead>
                                    <tr style="height:20px;padding:0px;margin:0px;white-space: pre; " >
                                    <th style="border: solid #888 1px;"><h1><font size="2">No of PKG</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Description Of Goods(Said To Contain)</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">Container No</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;Size&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;S. Line&nbsp;</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1><font size="2">Seal No</font> </h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Actual Weight</font></h1></th>
                                    <th style="border: solid #888 1px;width:20px "><h1><font size="2">Charged Weight</font></h1></th>
                                    <th style="border: solid #888 1px; "><h1> <font size="2">Rate</font></h1></th>
                                    <th style="border: solid #888 1px;border-left: none;"><h1><font size="2">Amount To Pay/Paid Rs.</font></h1></th>
                                   </tr>
                                </thead>
                                <tbody>

                                <tr style="height:20px;padding:0px;margin:0px; " >
                                <td rowspan="2" style="border: solid #888 1px;">   </td>
                                <td rowspan="2" style="border: solid #888 1px;"><c:out value='${articleName}'/>    </td>
                                <td style="border: solid #888 1px;">

                                    <c:out value='${containerNo}'/>
                                    <br>  <c:out value='${containerNo1}'/>
                                   </td>
                                <td style="border: solid #888 1px;  "> <c:out value='${containerTypeId}'/>   <br>  <c:out value='${containerTypeId1}'/></td>
                                <td style="border: solid #888 1px;  ">  <c:out value='${linerName}'/>  <br>  <c:out value='${linerName1}'/></td>
                                <td style="border: solid #888 1px;  "><c:out value='${sealNo}'/>  <br>  <c:out value='${sealNo1}'/> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td style="border: solid #888 1px;"> </td>
                                <td  style="border: solid #888 1px;"><c:out value='${grStatus}'/>  </td>
                                 </tr>
                                <tr style="height:20px;padding:0px;margin:0px; " >

                                <td colspan="8" style="border: solid #888 1px; border-top: none; margin:10px;">
                                    <br>
                                    <table >
                                        <tr> <td style="padding-bottom:10px">ICD Out Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                         <tr > <td style="padding-bottom:10px">Factory In Date</td><td>..............................................</td><td>Time</td><td>.........................................</td></tr>
                                        <tr > <td style="padding-bottom:10px">Factory Out Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                             <tr ><td style="padding-bottom:10px">ICD In Date</td><td>...............................................</td><td>Time</td><td>.........................................</td></tr>
                                     </table>
                                </td>



                                 </tr>




                                </tbody>
                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
<!--                                <td>&nbsp;</td>-->

                            </tr>


                            <tr>
                                <td>
                                    <br>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                            <td style="border: solid #888 1px;"><table  width='100%' style="border-collapse: collapse;">
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party Invoice No & Date:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Party  CST/TIN No:</font></td></tr>
                                                    <tr><td style="border-bottom: solid  #888 1px;" ><font size="2">Value Of Goods:Rs. </font>

                                                    </td></tr>
                                                </table></td>
                                                <td style="border: solid #888 1px;"><font sze='1' style="float: top;"> Permit No:</font></td>
                                            <td style="border: solid #888 1px;border-left: none;"><font sze='1' style="float: top;"> Remarks:</font></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border: solid #888 1px; border-top: none; ">
                       <font size="2">
                      <%  
                          if (newDate.after(oldDate)) {%>
                        1)GST No:06AACCB8054G1ZT,GST UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>
                        <%}else{%>
                              1)Service Tax No:AACCB8054GST001 SERVICE TAX UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE<br>   
                              <%  }%>
                        2)PAN NO-AACCB8054G &nbsp;&nbsp; &nbsp;&nbsp; CIN NO:U63040MH2006PTC159885 &nbsp;&nbsp; <font size="3" width=2' style="float: right;"><b>Authorised Signature</b></font><br>
                        3)Goods accepted for carriage on the terms & conditions printed over leaf.<br> </font>
                    </td>
                </tr>


            </table>
                <table align="center" style="border-collapse: collapse">
                   <tr>
                    <td>
                        <b> Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br>
                        <b>Branches:</b>Loni,Ludhiana,Dadri,Tughlakbad
                    </td>
                </tr>
            </table>
              <br>
             <table align="center" style="border-collapse: collapse">
                 <tr><td><center><font size="1">  Declaration </font></center></td></tr>
                    <tr><td></td></tr>
                 <tr> <td> <font size="1">1. We hereby declare that we have not available under ST Notification No.12/2003,dated 20/06/2003 of Govt.of India,Ministry of Finance
                         </font></td></tr>
                 <tr><td><font size="1">2. We hereby declare that we have not taken the credit of Excise Duty on inputs or capital goods used for providing the "Transport of Goods by road"Services under the provision of  Cenvat Credit Rules,2004
                         </font>    </td> </tr>
                 <tr><td><center><font size="1">
        Terms & Condition </font></center>
</td>

    </tr>
<tr><td><font size="1">a. Unless other wise agreed all consignment/goods are carried entirely at OWNERS risk as to packaging ,Goods contents,nature,condition and value of the Goods that are declared and are unknown to the carrier.The consignor shall indemnify the carrier against the loss resulting from inaccuracies or inadequacies of the particulars.

    </font></td>
    </tr>
    <tr><td><font size="1">b. Carrier will carry all goods on said to contain basis and will not stand liable for any internal discrepancy.

            </font> </td>
    </tr>

    <tr><td><font size="1">c. Goods to be insured by the consignor at their own cost to protect themselves against any losses and/or risk during transit by road.

            </font></td>
    </tr>
    <tr><td><font size="1">d. Carrier explicity reserves right to refusal for carriage of goods by road and such right shall be absolute.
</font>
    </td>
    </tr>
    <tr><td><font size="1">e. Transport charges are payable with in 24 Hours from loading the Goods on transport vehicle of carriers.Failure to pay transport charges prior to the arrival of Goods at destination shall attract penalty @ 2% on the transport charges.Octrol charges any other statutory government levels to be paid by consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">f. Carrier reserves right to re-measure and/or re-weight the goods of any discrepancy noted on the actual and charged weight, carrier shall impose penalty plus the rates for differential weight.
</font>
    </td>
    </tr>
    <tr><td><font size="1">g. Carrier shall not be held liable if goods are detained or security by competent authority for any misdeclaration or duty unpaid or prohibited Goods, Carrier shall not be liable for any losses, damage or deterioration to the Goods caused by such detention for examination or scrutiny.
</font>
    </td>
    </tr>
    <tr><td><font size="1">h. Carrier shall deliver the Goods within the normal transit time and shall not be liable for any delay in delivery,except where expressly agreed and such written request for timely delivery is submitted subject to force majeure conditions.
</font>
    </td>
    </tr>
    <tr><td><font size="1">i. Carrier shall make available the goods at the disposal of consignee or their agents at premises or carrier and same to be collected with in five (5) days from the date of arrival of Goods,failing which storage charges shall be payable in additional to transport charges.Carrier shall not be responsible for any damage and/or loss whatsoever to Goods stored at premises.
    </font></td>
    </tr>
    <tr><td><font size="1">j. Carrier shall have right to dispose and/or auction,the perishable goods within forty eight(48) hours of the arrival at premises if uncleared,and thirty(30)days for other goods without any notice to the consignee/ consignor.The disposal charges along with the storage charges for such period shall be payable to the consignor.
</font>
    </td>
    </tr>
    <tr><td><font size="1">k. Carrier shall not be responsible for delay in transit due to unforeseen contingencies,or act of god,or due to strike,war,civil commotions,and/or,events which are beyond the control of Carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">l. Carrier shall not be liable for any loss and/or damages to goods arising out of resulting due to Act of God,war,public or state emergencies,robbery,theft,dacoity,damage due to defect in packaging materials,or due to events that are beyond the control of carrier.
</font>
    </td>
    </tr>
    <tr><td><font size="1">m. Carrier shall not be responsible for damages to the inner content of the packages or Goods if the same has been delivered in same condition in which they were tendered for road transportation.Any kind of shortage has to be informed within twenty four(24)hrs of receipt of the goods with documented proof.In case of seal broken upon arrival the count of goods shall be taken jointly.
</font>
    </td>
    <tr><td><font size="1">n. No claim for compensation shall be entertained by the carrier after a period of seven(7) days from the date of delivery of the Goods along with documented/evidence and carrier has right to reject the same without assigning any reasons whatsoever.
</font>
    </td>
    </tr>
    <tr><td><font size="1">o. No suit or other legal proceeding shall be initiated,unless notice in writing has been served upon carrier before institution of suit or legal proceeding within 180 days from date receipt of Goods or from the date of booking.
</font>
    </td>
    </tr>
    <tr><td><font size="1">p. Any special instruction unless expressly written by the consignor shall not be entertained.
</font>
    </td>
    </tr>
   <tr><td><font size="1">q. The contract evidenced hereby shall be governed by and construed according to the Indian laws and any difference of opinion or dispute there under shall be settled by arbitration Delhi with each party appointing an arbitrator.
</font>
    </td>
    </tr>
     <tr><td><font size="1">r. In respect of any and/or all claims arising under this contract,The Courts of Delhi shall have exclusive judicial.
</font>
    </td>
    </tr>
    <tr><td><font size="1">s. Received the Goods Specified overleap in good & proper condition.
</font>
    </td>
    </tr>

             </table>
              <br>


   <table align="left" style="border-collapse: collapse">

    <tr><td><font size="1">Signature of consignee or Agent with Rubber Stamp

</font>
    </td>
    </tr>
    <tr> <td></td></tr>
    <tr> <td></td></tr>
    <tr><td><font size="1">Date </font>


    </td>
    </tr>

   </table>


            </div>
                                <br>
                                <br>
                                <center>
                                  <%--<c:out value='${tripStatusId}'/>--%>
                                  <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>"/>
                                  <input type="hidden" name="grNo" id="grNo" value="<c:out value="${grNumber}"/>"/>
                                    <c:if test="${tripStatusId >= 10}">
                                    <input type="button" class="button"  value="Print" onClick="print('printContent');" />
                                    </c:if>
                                &nbsp;&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                                    </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                <script type="text/javascript">
//                                     function print111(val)
//            {
//                var val1 = getPrintCount();
//                alert("val11:"+val1);
//                var DocumentContainer = document.getElementById(val);
//                var WindowObject = window.open('', "TrackHistoryData",
//                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
//                WindowObject.document.writeln(DocumentContainer.innerHTML);
//                WindowObject.document.close();
//                WindowObject.focus();
//                WindowObject.print();
//                WindowObject.close();
//            }
//            
             function print(val) {
               
                var   grNo = document.getElementById('grNo').value;
                var   tripId = document.getElementById('tripId').value;
                  // alert("grNo:"+grNo);
                $.ajax({
                    url: '/throttle/getGrPrintCount.do',
                    data: {grNo: grNo},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, data) {
                         //   alert("data:"+data);
                            if(data < 5){
                           //save
                        savePrint(grNo,tripId);
                                
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close(); 
                            }else{
                               alert("you cannot take print out.Already two prints have been taken!"); 
                            }

                        });
                    }
                });

            }
            function savePrint(grNo,tripId){
//                alert("print:"+tripId);
                   $.ajax({
                    url: '/throttle/saveGrPrintCount.do',
                    data: {grNo: grNo , tripid: tripId},
                    dataType: 'json'
//                    success: function (data) {
//                        $.each(data, function (i, data) {
//                           alert("dataa:"+data); 
//                        )}
//                            
//                        };
                    });
            }

                                </script>
    </body>
</html>
