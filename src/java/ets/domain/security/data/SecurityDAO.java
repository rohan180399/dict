package ets.domain.security.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.racks.business.RackTO;
import ets.domain.security.business.SecurityTO;
//import ets.domain.vendor.business.VendorTO;
import java.util.Date;
import org.glassfish.gmbal.logex.StackTrace;

/**
 *
 * @author vidya
 */
public class SecurityDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "SecurityDAO";

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getVendorList() {
        Map map = new HashMap();
        int status = 0;
        String currentDate = "";

        // map.put("currentDate", currentDate);

        try {
            currentDate = (String) getSqlMapClientTemplate().queryForObject("security.getCurrentDate", map);

            System.out.println("i am in currentDate :" + currentDate);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return currentDate;

    }

    // getworkorderDate
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getworkorderDate(int compId) {
        Map map = new HashMap();
        ArrayList workorderDate = new ArrayList();
        try {
            map.put("compId", compId);
            System.out.println("i am in bp");
            workorderDate = (ArrayList) getSqlMapClientTemplate().queryForList("security.GetWoDetailUnique", map);

            System.out.println("i in doa workorderDate :" + workorderDate.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return workorderDate;

    }

    //getDateList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDateList() {
        Map map = new HashMap();
        ArrayList dateList = new ArrayList();
        try {
            System.out.println("i am in bp");
            dateList = (ArrayList) getSqlMapClientTemplate().queryForList("security.GetDateList", map);

            System.out.println("i in doa workorderDate :" + dateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return dateList;

    }

    //getOutList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getOutList() {
        Map map = new HashMap();
        ArrayList outList = new ArrayList();
        try {
            System.out.println("i am in bp");
            outList = (ArrayList) getSqlMapClientTemplate().queryForList("security.GetOutList", map);

            System.out.println("i in doa workorderDate :" + outList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return outList;

    }
    // InAndOut

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getInAndOut() {
        Map map = new HashMap();
        ArrayList inAndOut = new ArrayList();
        try {
            System.out.println("i am in bp");
            inAndOut = (ArrayList) getSqlMapClientTemplate().queryForList("security.InAndOut", map);

            System.out.println("i in doa workorderDate :" + inAndOut.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return inAndOut;

    }

    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProcessSaveVehicle(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            SecurityTO securityTO = null;
            while (itr.hasNext()) {    
                securityTO = (SecurityTO) itr.next();
                map.put("woId", securityTO.getWoId());
                map.put("compId", securityTO.getCompId());
                map.put("inKm", securityTO.getKm());
                map.put("inHm", securityTO.getHourMeter());
                map.put("purposeId", securityTO.getPurposeId());
                map.put("remarks", securityTO.getRemarks());
                map.put("vehicleNumber", securityTO.getVehicleNumber());
                map.put("userId", UserId);

                status = (Integer) getSqlMapClientTemplate().update("security.saveVehicle", map);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;



    }

    //doProcessGetVehicleType
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList doProcessGetVehicleType() {
        Map map = new HashMap();
        ArrayList vehicleType = new ArrayList();
        try {
            System.out.println("i am in vehicleType  dao");
            vehicleType = (ArrayList) getSqlMapClientTemplate().queryForList("security.VehicleType", map);

            System.out.println("i in doa workorderDate :" + vehicleType.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return vehicleType;

    }

    //vijay mar 16
    public int insertJobCard(int workOrderId, int km, int hm) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            map.put("woId", workOrderId);
            map.put("km", km);
            map.put("hm", hm);
            status = (Integer) getSqlMapClientTemplate().update("security.insertJobCard", map);            
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCard", sqlException);
        }
        return status;
    }


    // doProcessSaveVehicleOutTime
    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProcessSaveVehicleOutTime(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            SecurityTO securityTO = null;
            while (itr.hasNext()) {
                securityTO = (SecurityTO) itr.next();
                map.put("vioId", securityTO.getVioId());
                map.put("remarks", securityTO.getRemarks());                
                map.put("userId", UserId);
                System.out.println(" remarks" +map);
                status = (Integer) getSqlMapClientTemplate().update("security.saveVehicleOutTime", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doProcessSaveVehicleOutTime Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "doProcessSaveVehicleOutTime", sqlException);
        }
        return status;



    }

    //doSaveAddVehicleSecurity
    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doSaveAddVehicleSecurity(SecurityTO securityTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("userId", UserId);
        //  map.put("woId", securityTO.getWoId());
        map.put("vehicleNumber", securityTO.getVehicleNumber());
        map.put("purpose", securityTO.getPurpose());
        map.put("remarks", securityTO.getRemarks());



        System.out.println("securityTO.getWoId() in dao " + securityTO.getWoId());
        int status = 0;
        int mfrstatus = 0;
        int vendorId = 0;
        try {

            status = (Integer) getSqlMapClientTemplate().update("security.insertAddVehicleSecurity", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vendorId;

    }

    

    /**
     * This method used to Get vehicle in purpose list.
     *
     * @return vehicleInPurposeList - vehicleInPurposeList.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleInPurposeList() {
        Map map = new HashMap();
        ArrayList vehicleInPurposeList = new ArrayList();
        try {

            vehicleInPurposeList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getVehicleInPurposeList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleInPurposeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleInPurposeList", sqlException);
        }
        return vehicleInPurposeList;

    }
    
    public ArrayList getOpList() {
        Map map = new HashMap();
        ArrayList opList = new ArrayList();
        try {

            opList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getOpList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOpList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getOpList", sqlException);
        }
        return opList;

    }            
    
    /**
     * This method used to Get vehicle  list.
     *
     * @return vehicleList - vehicleList.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getScheduleVehicleList(int compId, SecurityTO securityTO) {
        Map map = new HashMap();
        map.put("compId", compId);
        map.put("fromDate", securityTO.getVehicleFromDate());
        map.put("toDate", securityTO.getVehicleToDate());
        map.put("purposeId", securityTO.getVehicleInPurpose());
        map.put("status", securityTO.getVehicleStatus());
        map.put("vno", securityTO.getVno());
        ArrayList vehicleList = new ArrayList();
        System.out.println("map = " + map);
        try {
             if(securityTO.getVehicleInPurpose().equals("")){
                map.put("purposeId", "0");
            }
         System.out.println(" map 2222222222isss"+map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getScheduleVehicleList", map);
        } catch (Exception sqlException) {
             sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;

    }   
  
    /**
     * This method used to Get vehicle  list.
     *
     * @return vehicleList - vehicleList.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getInVehicleList(int compId, SecurityTO securityTO) {
        Map map = new HashMap();
        map.put("compId", compId);
        map.put("fromDate", securityTO.getVehicleFromDate());
        map.put("toDate", securityTO.getVehicleToDate());
        map.put("purposeId", securityTO.getVehicleInPurpose());
         map.put("status", securityTO.getVehicleStatus());
        map.put("vno", securityTO.getVno());
        ArrayList vehicleList = new ArrayList();
        try {
             if(securityTO.getVehicleInPurpose().equals("")){
                map.put("purposeId", "0");
            }
            System.out.println(" map 111111isss"+map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getInVehicleList", map);
        } catch (Exception sqlException) {
             sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
           
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;

    }               
    /**
     * This method used to Get vehicle  list.
     *
     * @return vehicleList - vehicleList.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getOutVehicleList(int compId, SecurityTO securityTO) {
        Map map = new HashMap();
        map.put("compId", compId);
        map.put("fromDate", securityTO.getVehicleFromDate());
        map.put("toDate", securityTO.getVehicleToDate());
        map.put("purposeId", securityTO.getVehicleInPurpose());
        map.put("vno", securityTO.getVno());
         map.put("status", securityTO.getVehicleStatus());
        ArrayList vehicleList = new ArrayList();
        try {
             if(securityTO.getVehicleInPurpose().equals("")){
                map.put("purposeId", "0");
            }
         System.out.println(" map33333 isss"+map);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getOutVehicleList", map);
        } catch (Exception sqlException) {
             sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;

    }  
    /**
     * This method used to Get vehicle  list.
     *
     * @return vehicleList - vehicleList.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getInOutVehicleList(int compId, SecurityTO securityTO) {
        Map map = new HashMap();
        map.put("compId", compId);
        map.put("fromDate", securityTO.getVehicleFromDate());
        map.put("toDate", securityTO.getVehicleToDate());
        map.put("purposeId", securityTO.getVehicleInPurpose());
        map.put("vno", securityTO.getVno());
         map.put("status", securityTO.getVehicleStatus());
        ArrayList vehicleList = new ArrayList();
        try {
            System.out.println(" map555"+map);
            if(securityTO.getVehicleInPurpose().equals("")){
                map.put("purposeId", "0");
            }
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("security.getInOutVehicleList", map);
        } catch (Exception sqlException) {
             sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;

    }

    public String isThisVehicleIn(String regNo) {
         Map map = new HashMap();
        
        
        map.put("regNo", regNo);        
        System.out.println("regNo"+regNo);
        String suggestions = "";
        try {
           
            suggestions = (String) getSqlMapClientTemplate().queryForObject("security.isThisVehicleIn", map);
            System.out.println("suggestions"+suggestions);
            
            
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("isThisVehicleIn Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "isThisVehicleIn", sqlException);
        }
        return suggestions;
    }
}
