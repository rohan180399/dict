<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function calcPointKm(sno) {

        var averageKM = document.getElementById('averageKM').value;
        var travelHour = document.getElementById('travelHour' + sno).value;
        var travelMinute = document.getElementById('travelMinute' + sno).value;
        var kmMini = averageKM / 60;
        if (travelHour == '') {
            travelHour = 0;
        }
        if (travelMinute == '') {
            travelMinute = 0;
        }
        var travelHourMin = parseInt(travelHour) * 60;
        var travelPointMin = parseInt(travelMinute) + parseInt(travelHourMin);
        var kmpoint = kmMini.toFixed(2) * parseInt(travelPointMin);
        document.getElementById("travelKm" + sno).value = kmpoint.toFixed(0);
        calcTravelKm();
    }
    function calculateTransitTime(sno) {

        var averageKM = document.getElementById('averageKM').value;
        var travelKm = document.getElementById('travelKm' + sno).value;
        var travelHour = document.getElementById('travelHour' + sno).value;
        var travelMinute = document.getElementById('travelMinute' + sno).value;
        var transitTime = (parseFloat(travelKm) / parseFloat(averageKM)).toFixed(2);
        var transitTimeNew = parseInt(transitTime);
        document.getElementById('travelHour' + sno).value = transitTimeNew;
        document.getElementById('travelMinute' + sno).value = ((transitTime - transitTimeNew) * 60).toFixed(0);

        calcTravelKm();

    }
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script>

    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";

    function addRow1() {
        if (parseInt(rowCount1) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }

        sno1++;
        var tab = document.getElementById("addRoute");
        //find current no of rows
        var rowCountNew = document.getElementById('addRoute').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);
        newrow.id = 'deleteRowId' + sno;

        cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' style='width:10px;' > <label id='serialNo" + sno + "'>" + sno1 + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        if (sno1 == 1) {
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' > <input type='text' id='pointType" + sno + "' name='pointType' value='PickUp'  readonly class='form-control' /></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='hidden' id='cityId" + sno + "' name='cityId' class='form-control' ><input type='text' id='cityName" + sno + "' name='cityName' onchange='validateCityName(" + sno + ");' class='form-control'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            callAjax(sno);

            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='30' ><textarea type='text' id='pointAddresss" + sno + "' name='pointAddresss'  class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='30'><input type='text' id='pointSequence" + sno + "' name='pointSequence' class='form-control' value='" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='30'><input type='text' id='latitude" + sno + "' name='latitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell0;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='30'><input type='text' id='longitude" + sno + "' name='longitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell0 = "<td class='text1' height='30'><input type='text' id='travelKm" + sno + "' name='travelKm' onKeyUp='calculateTransitTime(" + sno + ");' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcTravelKm();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(8);
            var cell0 = "<td class='text1' height='30'><input type='text' id='travelHour" + sno + "' name='travelHour' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcTravelHour();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(9);
            var cell0 = "<td class='text1' height='30'><input typ<e='text' id='travelMinute" + sno + "' name='travelMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcTravelMinute();'>" +
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'>" +
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(10);
            var cell0 = "<td class='text1' height='30'><input type='text' id='waitMinute" + sno + "' name='waitMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcWaitMinute();calcTravelHour()'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        } else {
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' class='form-control' style='width:50px; '> <select id='pointType" + sno + "' name='pointType'><option value='PickUp'>Pickup</option><option value='Drop'>Drop</option> </select>  &nbsp;<input type='button' class='button' style='width:60px' value='Delete' name='delete' id='id='delete' onclick='return deleteRow(" + sno + "," + sno1 + "," + 1 + " );' /></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='hidden' id='cityId" + sno + "' name='cityId' class='form-control' ><input type='text' id='cityName" + sno + "' name='cityName' class='form-control'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            callAjax(sno);

            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='30' ><textarea type='text' id='pointAddresss" + sno + "' name='pointAddresss'  class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='30'><input type='text' id='pointSequence" + sno + "' name='pointSequence' class='form-control' value='" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='30'><input type='text' id='latitude" + sno + "' name='latitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell0;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='30'><input type='text' id='longitude" + sno + "' name='longitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell0 = "<td class='text1' height='30'><input type='text' id='travelKm" + sno + "' name='travelKm'  onKeyUp='calculateTransitTime(" + sno + ");'  class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelKm();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(8);
            var cell0 = "<td class='text1' height='30'><input type='text' id='travelHour" + sno + "' name='travelHour' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelHour();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(9);
            var cell0 = "<td class='text1' height='30'><input type='text' id='travelMinute" + sno + "' name='travelMinute' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelMinute();'>" +
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'>" +
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(10);
            var cell0 = "<td class='text1' height='30'><input type='text' id='waitMinute" + sno + "' name='waitMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcWaitMinute();calcTravelHour();'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            $("#freezeRoute").show();
        }
        rowCount1++;
        sno++;
    }

    function freezeRouteDetails() {
        if (parseInt(rowCount1) % 2 == 0) {
            styl = "text2";
        } else {
            styl = "text1";
        }

        sno1++;
        var tab = document.getElementById("addRoute");
        //find current no of rows
        var rowCountNew = document.getElementById('addRoute').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);
        newrow.id = 'deleteRowId' + sno;
        cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' style='width:10px;' > <label id='serialNo" + sno + "'>" + sno1 + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='30' > <input type='text' id='pointType" + sno + "' name='pointType' value='Base'  readonly class='form-control' style='width:50px;' /> &nbsp;<input type='button' class='button' style='width:80px' value='UnFreeze' name='delete' id='id='delete' onclick='return deleteRow(" + sno + "," + sno1 + "," + 2 + "  );' /></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell0 = "<td class='text1' height='30' ><input type='hidden' id='cityId" + sno + "' name='cityId' class='form-control' ><input type='text' id='cityName" + sno + "' name='cityName' class='form-control'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        callAjax(sno);

        cell = newrow.insertCell(3);
        var cell0 = "<td class='text1' height='30' ><textarea type='text' id='pointAddresss" + sno + "' name='pointAddresss'  class='form-control' ></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(4);
        var cell0 = "<td class='text1' height='30'><input type='text' id='pointSequence" + sno + "' name='pointSequence' class='form-control' value='" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(5);
        var cell0 = "<td class='text1' height='30'><input type='text' id='latitude" + sno + "' name='latitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(6);
        var cell0 = "<td class='text1' height='30'><input type='text' id='longitude" + sno + "' name='longitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(7);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelKm" + sno + "' name='travelKm'  onKeyUp='calculateTransitTime(" + sno + ");'  class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelKm();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(8);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelHour" + sno + "' name='travelHour' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelHour();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(9);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelMinute" + sno + "' name='travelMinute' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelMinute();'>" +
                "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'>" +
                "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;


        cell = newrow.insertCell(10);
        var cell0 = "<td class='text1' height='30'><input type='text' id='waitMinute" + sno + "' name='waitMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcWaitMinute();calcTravelHour();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        $("#cityId" + sno).val($("#cityId0").val());
        $("#cityName" + sno).val($("#cityName0").val());
        $("#pointAddresss" + sno).val($("#pointAddresss0").val());
        $("#latitude" + sno).val($("#latitude0").val());
        $("#longitude" + sno).val($("#longitude0").val());

        rowCount1++;
        sno++;
        $("#freezeRoute").hide();
        $("#addrow").hide();
    }

    function deleteRow(sno2, sno4, type) {
        if (!confirm('Are you sure you want to delete?')) {
            return false;
        } else {
            $('#deleteRowId' + sno2).remove();
            var rowLength = document.getElementById('addRoute').rows.length;
            var pointSequence = document.getElementsByName("pointSequence");
//            alert(rowLength);
            var sno3 = rowLength - 3;
            //serialNo
            if (rowLength > 4) {
                for (var i = 1; i < sno3; i++) {
                    var no = pointSequence[i].value;
                    no = no - 1;
                    pointSequence[i].value = i + 1;
                    $("#serialNo" + no).text(i + 1);
                    $('#cityId' + no).attr('id', 'cityId' + (i + 1));
                    $('#cityName' + no).attr('id', 'cityName' + (i + 1));
                }
                rowCount--;
                rowCount1--;
                sno1--;
            }
            if (type == 2) {
                $("#freezeRoute").show();
                $("#addrow").show();
            }
        }
        return;
    }
    function validateCityName(sno) {
        if ("#cityId" + sno == "") {
            $("#cityId" + sno).val('');
        }
    }
    function calcTravelKm() {
        var travelKm = document.getElementsByName('travelKm');
        var distance = 0;
        for (var i = 0; i < travelKm.length; i++) {
            if (travelKm[i].value != '') {
                distance += parseFloat(travelKm[i].value);
            }
        }
        document.getElementById('distance').value = distance;
        if (distance != '') {
            calcFuleCostPerKm();
        }
    }
    

   var totalhrs=0;
   function calcReferHr() {
        var travelHrs = document.getElementsByName('travelHour');
        var totalHrs = 0;
        for (var i = 0; i < travelHrs.length; i++) {
            if (travelHrs[i].value != '') {
                totalHrs += parseFloat(travelHrs[i].value);
            }
        }
        totalHrs = parseFloat(totalHrs) ;
        document.getElementById('Reeferhr').value = totalHrs ;
        
    }
    
    function calcReferMin() {

        var travelMins = document.getElementsByName('travelMinute');
        var totalMins = 0;


        for (var i = 0; i < travelMins.length; i++) {
            if (travelMins[i].value != '') {
                totalMins += parseFloat(travelMins[i].value);
            }
        }
       
        

        document.getElementById('Reefermin').value = totalMins;
        
    }
    function calcReferWaitMinute() {
        var waitMins = document.getElementsByName('waitMinute');
        var totalMins = 0;
        for (var i = 0; i < waitMins.length; i++) {
            if (waitMins[i].value != '') {
                totalMins += parseInt(waitMins[i].value);
            }
        }
        document.getElementById('totalReferWaitMinutes').value = totalMins;
       
    }
     function calcTotalRefer(){
         var totalreferhr=document.getElementById('Reeferhr').value;
         var totalrefermin=document.getElementById('Reefermin').value;
         var totalreferwait=document.getElementById('totalReferWaitMinutes').value;
       //  alert("sdbfjsdfj" +totalreferwait);
         var total1 = totalreferhr * 60  ;
         var total2 = parseInt(totalrefermin) + parseInt(totalreferwait) ;
       //  alert("total" +total2);
         document.getElementById('totalRefer').value =parseInt((total1 + total2)/2);
     }

    function calcTravelHour() {
        var travelHrs = document.getElementsByName('travelHour');
        var totalHrs = 0;
        for (var i = 0; i < travelHrs.length; i++) {
            if (travelHrs[i].value != '') {
                totalHrs += parseFloat(travelHrs[i].value);
            }
        }
        totalHrs = parseFloat(totalHrs) ;
        document.getElementById('totalHours').value = totalHrs + totalhrs ;
        
    }
    function calcTravelMinute() {

        var travelMins = document.getElementsByName('travelMinute');
        var totalMins = 0;


        for (var i = 0; i < travelMins.length; i++) {
            if (travelMins[i].value != '') {
                totalMins += parseFloat(travelMins[i].value);
            }
        }
        var temp=totalMins;
        if(totalMins >= 60)
        {
             totalhrs= Math.floor(temp / 60);
             totalMins=(totalMins % 60);
        }


        document.getElementById('totalMinutes').value = totalMins;

calcTravelHour();
    }
    function calcReeferHour() {
        var reeferHrs = document.getElementsByName('reeferHour');
        var totalHrs = 0;
        for (var i = 0; i < reeferHrs.length; i++) {
            if (reeferHrs[i].value != '') {
                totalHrs += parseInt(reeferHrs[i].value);
            }
        }
        document.getElementById('totalReeferHours').value = totalHrs;
        if (totalHrs != '') {
            calcReferHours();
        }
    }
    function calcReeferMinute() {
        var reeferMins = document.getElementsByName('reeferMinute');
        var totalMins = 0;
        for (var i = 0; i < reeferMins.length; i++) {
            if (reeferMins[i].value != '') {
                totalMins += parseInt(reeferMins[i].value);
            }
        }
        document.getElementById('totalReeferMinutes').value = totalMins;
        if (totalMins != '') {
            calcReferHours();
        }
    }
    function calcWaitMinute() {
        var waitMins = document.getElementsByName('waitMinute');
        var totalMins = 0;
        for (var i = 0; i < waitMins.length; i++) {
            if (waitMins[i].value != '') {
                totalMins += parseInt(waitMins[i].value);
            }
        }
        document.getElementById('totalWaitMinutes').value = totalMins;
    }



    function callAjax(val) {
        // Use the .autocomplete() method to compile the list based on input from user
        //alert(val);
        var pointName = 'cityName' + val;
        var pointId = 'cityId' + val;
        var address = 'pointAddresss' + val;
        var latitude = 'latitude' + val;
        var longitude = 'longitude' + val;
        //alert(prevPointId);
        $('#' + pointName).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerPoints.do",
                    dataType: "json",
                    data: {
                        cityName: request.term,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#' + pointName).val('');
                            $('#' + pointId).val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {

                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                var pointType = ui.item.pointType;
                var pointAddress = ui.item.pointAddress;
                var latitudeVal = ui.item.latitude;
                var longitudeVal = ui.item.longitude;
                //alert(latitudeVal+" : "+longitudeVal);

                $('#' + pointName).val(value);
                $('#' + pointId).val(id);
                $('#' + address).val(pointAddress);
                $('#' + latitude).val(latitudeVal);
                $('#' + longitude).val(longitudeVal);
                //validateRoute(val,value);
                //addRowFunction();
                $('#' + address).focus();
                return false;
            }

            // Format the list menu output of the autocomplete
        }).data("autocomplete")._renderItem = function(ul, item) {
            //alert(item);
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a><table width='100%' border='0'><tr><td width='60px'>" + itemVal + "</td></tr></table></a>")
                    .appendTo(ul);
        };


    }

    function addRowFunction() {

        var rowCountNew = document.getElementById('addRoute').rows.length;
        //alert(rowCountNew);
        var rkmph = document.getElementById('averageKM').value;
        if (rkmph == 0 || rkmph == '') {
            alert("please enter average km/h");
            document.getElementById('averageKM').focus();
        } else {
            var cityName = document.getElementsByName("cityName");
            var count = 0;
            var x;
            var r = confirm("Do You Want Add One More Route!");
            //alert(cityName.length);
            if (cityName.length == 1 && r == true) {
                addRow1();
            } else {
                for (var i = 0; i < cityName.length; i++) {
                    //alert(cityName[i].value);
                    if (cityName[i].value != '') {
                        count++;
                    } else {
                        count = 0;
                    }
                }
                //alert(count);
                if (count > 0 && r == true) {
                    addRow1();
                }
            }
        }
    }

    //savefunction
    function submitPage(value) {
        var count1 = 0;
        var count2 = 0;
        if ($("#routeStatus").text() !==  '') {
            alert($("#routeStatus").text());
            document.getElementById('routeName').focus();
            return;
        } else if (document.getElementById('routeValidFrom').value == '') {
            alert("please select the route valid from date");
            document.getElementById('routeValidFrom').focus();
            return;
        } else if (document.getElementById('routeValidTo').value == '') {
            alert("please select the route valid to date");
            document.getElementById('routeValidTo').focus();
            return;
        } else if (document.getElementById('routeName').value == '') {
            alert("enter the route name");
            document.getElementById('routeName').focus();
            return;
        } else if (document.getElementById('averageKM').value == '0') {
            alert("enter average km per month");
            document.getElementById('averageKM').focus();
            return;
        } else {
            count1 = validateRouteDetails();
            if (count1 == 0) {
                count2 = validateRouteExpenseDetails();
            }
        }
        if (count1 == 0 && count2 == 0) {
            document.secondaryRoute.action = '/throttle/saveSecondaryRoute.do';
            document.secondaryRoute.submit();
        }
//            document.secondaryRoute.action = '/throttle/saveSecondaryRoute.do';
//            document.secondaryRoute.submit();
    }


    function validateRouteDetails() {
        var pointType = document.getElementsByName("pointType");
        var cityId = document.getElementsByName("cityId");
        var cityName = document.getElementsByName("cityName");
        var pointAddresss = document.getElementsByName("pointAddresss");
        var pointSequence = document.getElementsByName("pointSequence");
        var latitude = document.getElementsByName("latitude");
        var longitude = document.getElementsByName("longitude");
        var travelKm = document.getElementsByName("travelKm");
        var travelHour = document.getElementsByName("travelHour");
        var travelMinute = document.getElementsByName("travelMinute");
        var reeferHour = document.getElementsByName("reeferHour");
        var reeferMinute = document.getElementsByName("reeferMinute");
        var waitMinute = document.getElementsByName("waitMinute");
        var a = 0;
        var count = 0;
        for (var i = 0; i < pointType.length; i++) {
            a = i + 1;
            if (cityId[i].value == '' && cityName[i].value == '') {
                alert("Please fill city name for row  " + a);
                cityName[i].focus();
                count = 1;
                return count;
            } else if (cityId[i].value == '' && cityName[i].value != '') {
                alert("Invalid city name for row  " + a);
                cityName[i].focus();
                count = 1;
                return count;
            } else if (pointAddresss[i].value == '') {
                alert("please fill point address for row " + a);
                pointAddresss[i].focus();
                count = 1;
                return count;
            } else if (pointSequence[i].value == '') {
                alert("please fill point sequence for row " + a);
                pointSequence[i].focus();
                count = 1;
                return count;
            } else if (latitude[i].value == '') {
                alert("please fill point latitude for row " + a);
                latitude[i].focus();
                count = 1;
                return count;
            } else if (longitude[i].value == '') {
                alert("please fill point longitude for row " + a);
                longitude[i].focus();
                count = 1;
                return count;
            } else if (travelKm[i].value == '') {
                alert("please fill point travel km for row " + a);
                travelKm[i].focus();
                count = 1;
                return count;
            } else if (travelHour[i].value == '') {
                alert("please fill point travel hour for row " + a);
                travelHour[i].focus();
                count = 1;
                return count;
            } else if (travelMinute[i].value == '') {
                alert("please fill point travel minute for row " + a);
                travelMinute[i].focus();
                count = 1;
                return count;
                /* }else if (reeferHour[i].value == '') {
                 alert("please fill point reefer hour for row " + a);
                 reeferHour[i].focus();
                 count = 1;
                 return count;
                 }else if (reeferMinute[i].value == '') {
                 alert("please fill point reefer minute for row " + a);
                 reeferMinute[i].focus();
                 count = 1;
                 return count;    */
            } else if (waitMinute[i].value == '') {
                alert("please fill point waithing minute for row " + a);
                waitMinute[i].focus();
                count = 1;
                return count;
            }
        }
        return count;

    }
    function validateRouteExpenseDetails() {
        var vehMileage = document.getElementsByName("vehMileage");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var addlTollAmounts = document.getElementsByName("addlTollAmounts");
        var parkingCost = document.getElementsByName("parkingCost");
        var miscCost = document.getElementsByName("miscCost");
        var totExpense = document.getElementsByName("totExpense");
        var a = 0;
        var count = 0;
        for (var i = 0; i < vehMileage.length; i++) {
            if (tollAmounts[i].value == '') {
                alert("please fill toll rate for vehicle type " + vehMileage[i]);
                tollAmounts[i].focus();
                count = 1;
                return count;
            } else if (addlTollAmounts[i].value == '') {
                alert("please fill fixed add toll cost for " + vehMileage[i]);
                addlTollAmounts[i].focus();
                count = 1;
                return count;
            } else if (parkingCost[i].value == '') {
                alert("please fill fixed parking cost for " + vehMileage[i]);
                parkingCost[i].focus();
                count = 1;
                return count;
            } else if (miscCost[i].value == '') {
                alert("please fill misc cost for  " + vehMileage[i]);
                miscCost[i].focus();
                count = 1;
                return count;
            }
        }
        return count;
    }



    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getSecondaryCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                $("#customerId").val(ui.item.Id);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                $('#fixedKmPerMonth').focus();
                return false;

            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };


    });

</script>

<body onload="addRow1();calcPerDayCng();">
    <form name="secondaryRoute"  method="post">
        <br>
        <br>
        <br>


        <c:set var="customerName" value="" />
        <c:set var="fromDateVal" value="" />
        <c:set var="toDateVal" value="" />

        <c:if test="${customerDetails != null}">
            <c:forEach items="${customerDetails}" var="cust">
                <c:set var="customerName" value="${cust.customerName}" />
                <c:set var="fromDateVal" value="${cust.fromDate}" />
                <c:set var="toDateVal" value="${cust.toDate}" />
            </c:forEach>
        </c:if>

        <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr class="contenthead" align="left"><td colspan="4">Add Secondary Contract Route Details</td></tr>

            <tr class="text2">
                <td><font color='red'>*</font>Customer Name</td>
                <td><input type="hidden" name="customerId" id="customerId" value='<c:out value="${customerId}"/>'/><input type="hidden" name="customerName" id="customerName" value='<c:out value="${customerName}"/>' readonly class="textbox"/><c:out value="${customerName}"/></td>
                <td colspan="2">&nbsp;</td>

            </tr>
            <input type="hidden" name="fixedKmPerMonth" id="fixedKmPerMonth" value="1000" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
            <input type="hidden" name="fixedReeferHours" id="fixedReeferHours" value="100" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
            <input type="hidden" name="fixedReeferMinutes" id="fixedReeferMinutes" value="10" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>

            <tr class="text1">
                <td><font color='red'></font>Contract Valid From</td>
                <td><input type="text" readonly  name="routeValidFrom" id="routeValidFrom" value='<c:out value="${fromDateVal}"/>' class="datepicker"/></td>
                <td><font color='red'></font>Contract Valid To</td>
                <td><input type="text" name="routeValidTo" id="routeValidTo" value='<c:out value="${toDateVal}"/>' class="datepicker"/></td>
            </tr>
            <tr class="text2">
                <td>Current Diesel Cost </td>
                <td><input type="hidden" name="dieselCost" id="dieselCost"  value="<%=request.getAttribute("currentDieselPrice")%>" readonly/><label><%=request.getAttribute("currentDieselPrice")%></label></td>
                <td>Current CNG Cost </td>
                <td><input type="hidden" name="fuelCost" id="fuelCost"  value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
            </tr>

            <tr>
                <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
            </tr>
        </table>
        <br>
        <script>
            function calcPerDayCng() {
                var fuelCost = $("#fuelCost").val();
                var perDayExp = parseFloat(fuelCost) * 1.5;
                $("#perDayAllocation").val(perDayExp);
            }


            var httpRequest;
            function checkSecondaryRouteName() {
                var routeName = document.getElementById('routeName').value;
                var customerId = document.getElementById('customerId').value;
                if (routeName != '') {
                    var url = '/throttle/checkSecondaryRouteName.do?routeName=' + routeName+'&customerId='+customerId;
                    if (window.ActiveXObject) {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } else if (window.XMLHttpRequest) {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() {
                        processRequest();
                    };
                    httpRequest.send(null);
                }
            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
//                        alert(val);
                        if (val != "" && val != 'null') {
                            $("#routeStatus").text(val);
                        } else {
                            $("#routeStatus").text('');
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }
            function routeNameEmail(){
           var name=document.getElementById("routeName").value;
           document.getElementById("routeNameEmail").value=name;
            }
            
        </script>
        <br>
        <table width="980px" align="center" cellpadding="0" cellspacing="0" id="addRoute" class="table2">
            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
            <div align="center" id="routeStatus" height="20" >&nbsp;&nbsp;</div></font>
            <tr class="text1">
                <td colspan="2" ><font color='red'>*</font>Route Name</td>
                <td colspan="1" ><input type="text" name="routeName" id="routeName" value="" class="textbox" onchange="checkSecondaryRouteName();routeNameEmail();"/></td>
                <td colspan="1" ><font color='red'>*</font>Route Ave KM/H</td>
                <td colspan="1" ><input type="text" name="averageKM" id="averageKM" class="textbox" value="0" /></td>
                <td colspan="2" >PreCoolingHrs</td>
                <td colspan="1" ><input type="text" name="preCoolingHr" id="preCoolingHr" style='width:40px;' class="textbox" value="1" readonly /></td>
                <td colspan="2" >LoadingHrs</td>
                <td colspan="1" >
                    <input type="text" name="loadingHr" id="loadingHr" style='width:40px;' class="textbox" value="0.5" readonly />
                    <input type="hidden" name="perDayAllocation" id="perDayAllocation" style='width:40px;' class="textbox" value="" readonly />
                </td>


            </tr>
            <tr>
                <td class="contenthead" height="30" style="width: 10px;">Sno</td>
                <td class="contenthead" height="30" style="width: 10px;">Point Type</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Point Name</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Address</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Point Sequence</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Latitude</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Longitude</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Km</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Hours</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Minutes</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Wait Minutes</td>
            </tr>
            <tr>
            <center>
                <br>
                <br>
                <br>
                <td colspan="4" align="right"></td>
                <td colspan="2" align="right"><input type="button" name="freezeRoute" id="freezeRoute" value="Freeze Route" class="button" style="display: none" onclick="freezeRouteDetails();calcReferHr();calcReferMin();calcReferWaitMinute();calcTotalRefer();"></td>
                <td align="right">Total</td>
                <td><input type="text" name="distance" id="distance" value="0" readonly class="textbox" style="width: 40px" onchange="totalEmail();"/></td>
                <td><input type="text" name="totalHours" id="totalHours" value="0" readonly class="textbox" style="width: 40px" onchange="totalHoursEmail();"/></td>
                <td><input type="text" name="totalMinutes" id="totalMinutes" value="0" readonly class="textbox" style="width: 40px"onchange="totalMinEmail();"/></td>
                <input type="hidden" name="totalReeferHours" id="totalReeferHours" value="0" readonly class="textbox" style="width: 40px"/>
                <input type="hidden" name="Reeferhr" id="Reeferhr" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="Reefermin" id="Reefermin" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="totalReferWaitMinutes" id="totalReferWaitMinutes" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="totalRefer" id="totalRefer" value="0" readonly class="textbox" style="width: 40px"/>
                <input type="hidden" name="totalReeferMinutes" id="totalReeferMinutes" value="0" readonly class="textbox" style="width: 40px"/>
                <td><input type="text" name="totalWaitMinutes" id="totalWaitMinutes" value="0" readonly class="textbox" style="width: 40px"onchange="totalWaitMinEmail();"/>
                </td>
            </center>
            </tr>
        </table>
        <br>
        <center><input type="button" class="button" name="addrow" id="addrow" value="AddPoint" onclick="addRowFunction();"/> </center>

        <script type="text/javascript">
            //concard value get from the addrout configDetails
            function setRCMValues() {
                var km = document.getElementById("distance").value;
                var hm=document.getElementById("totalRefer").value;
//                    var fuelDifferCost = document.getElementById("fuelDifferCost").value;
                var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                var fuelCostPerMin = document.getElementsByName('fuelCostPerMin');
                var tollAmounts = document.getElementsByName('tollAmounts');
                var addlTollAmounts = document.getElementsByName('addlTollAmounts');
                var parkingCosts = document.getElementsByName('parkingCost');
                var miscCosts = document.getElementsByName('miscCost');
                var totExpenses = document.getElementsByName('totExpense');
                var fuelTypeId = document.getElementsByName('fuelTypeId');
                var vehicleTonnage = document.getElementsByName('vehicleTonnage');
                var fuelAmnt = 0;
                var referAmnt=0;
                var increaseAmount = 0;
                var tollAmount = 0;
                var addlTollAmount = 0;
                var addfuleDifferAmount = 0;
                var miscCost = 0;
                var totExpense = 0;
                var parkingCost = 0;
                var totExpense1 = 0;
                var totExpense2 = 0;
                var j = 50;
                var k = 0;
                var A = 0;
                var perDayAllocation = $("#perDayAllocation").val();
                //alert('am here:'+fuelCostPerKms.length);
                for (var i = 0; i < fuelCostPerKms.length; i++) {
                    if (km == '') {
                        km = 0;
                    }
                    if (hm == '') {
                        hm = 0;
                    }
                    if (tollAmounts[i].value == '') {
                        tollAmount = 0;
                    } else {
                        tollAmount = tollAmounts[i].value;
                    }
                    if (addlTollAmounts[i].value == '') {
                        addlTollAmount = 0;
                    } else {
                        addlTollAmount = addlTollAmounts[i].value;
                    }
                    if (parkingCosts[i].value == '') {
                        parkingCost = 0;
                    } else {
                        parkingCost = parkingCosts[i].value;
                    }
                    if (miscCosts[i].value == '') {
                        miscCost = 0;
                    } else {
                        miscCost = miscCosts[i].value;
                    }
                    if (fuelCostPerKms[i].value != '') {
                        fuelAmnt = fuelCostPerKms[i].value * km;
                    } else {
                        fuelAmnt = 0 * km;
                    }
                    if (fuelCostPerMin[i].value != '') {
                        referAmnt = fuelCostPerMin[i].value * hm;
                    }
//                        if(fuelDifferCost=='')
//                        {
//                           increaseAmount= 0* 925;
//                           addfuleDifferAmount=increaseAmount;
//                        }
//                        else{
//                            increaseAmount= fuelDifferCost * 925;
//                            addfuleDifferAmount=increaseAmount;
//                        }
                    if (fuelTypeId[i].value == 1002) {
                        totExpense1 = parseFloat(fuelAmnt) + parseFloat(referAmnt) + parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost) + parseFloat(miscCost);
                    } else if (fuelTypeId[i].value == 1003) {
                        totExpense1 = parseFloat(fuelAmnt) + parseFloat(referAmnt)+ parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost) + parseFloat(miscCost) + parseFloat(perDayAllocation);
                    }
//                        totExpense2 = parseFloat(totExpense1) + parseFloat(addfuleDifferAmount) ;
//                        if(totExpense2<50)
//                        {
//                            totExpense=50;
//                        }  
//                        else if(totExpense2<100 && totExpense != '50')
//                        {
//                            totExpense=100;
//                        }else
//                        {
//                            totExpense2=totExpense2;
//                        }
                    k = totExpense1 % j;
//                          alert(k);
                    if (parseInt(k) < 50 && parseInt(k) != '50')
                    {
                        A = 50 - parseInt(k);
                        totExpense = totExpense1 + A;
                    }
                    else
                    {
                        totExpense = totExpense2;
                    }

                    totExpenses[i].value = parseInt(totExpense);

                }

            }




//    function setRCMValues(){
//                    var km = document.getElementById("distance").value;
//                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
//                    var tollAmounts = document.getElementsByName('tollAmounts');
//                    var addlTollAmounts = document.getElementsByName('addlTollAmounts');
//                    var parkingCosts = document.getElementsByName('parkingCost');
//                    var miscCosts = document.getElementsByName('miscCost');
//                    var totExpenses = document.getElementsByName('totExpense');
//                    var fuelAmnt = 0;
//                    var tollAmount = 0;
//                    var addlTollAmount = 0;
//                    var parkingCost = 0;
//                    var miscCost = 0;
//                    var totExpense = 0;
//                    for (var i = 0; i < fuelCostPerKms.length; i++) {
//                        if(km == ''){
//                            km = 0;
//                        }
//                        if(tollAmounts[i].value == ''){
//                            tollAmount = 0;
//                        }else{
//                            tollAmount = tollAmounts[i].value;
//                        }
//                        if(addlTollAmounts[i].value == ''){
//                            addlTollAmount = 0;
//                        }else{
//                            addlTollAmount = addlTollAmounts[i].value;
//                        }
//                        if(miscCosts[i].value == ''){
//                            miscCost = 0;
//                        }else{
//                            miscCost = miscCosts[i].value;
//                        }
//                        if(parkingCosts[i].value == ''){
//                            parkingCost = 0;
//                        }else{
//                            parkingCost = parkingCosts[i].value;
//                        }
//
//                        if (fuelCostPerKms[i].value != '') {
//                            fuelAmnt = fuelCostPerKms[i].value * km;
//                        } else {
//                            fuelAmnt = 0 * km;
//                        }
//                        totExpense = parseFloat(fuelAmnt) + parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost)+ parseFloat(miscCost);
//                        totExpenses[i].value = totExpense.toFixed(2);
//                    }
//
//                }
            function setTollCost(type) {
                if (type == 1) {
                    var km = document.getElementById("distance").value;
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var misCost = document.getElementsByName("miscCostKm");
                    var driverIncentive = document.getElementsByName("driverIncenKm");
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var factor = document.getElementsByName('factor');
                    var vehExpense = document.getElementsByName('vehExpense');
                    for (var i = 0; i < fuelCostPerKms.length; i++) {
                        misCost[i].value = document.getElementById('avgMisCost').value;
                        driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                        factor[i].value = document.getElementById('avgFactor').value;
                        if (km == '') {
                            km = 0;
                        }
                        if (fuelCostPerKms[i].value != '') {
                            var fuelAmnt = fuelCostPerKms[i].value * km;
                        } else {
                            var fuelAmnt = 0 * km;
                        }
                        if (misCost[i].value != '') {
                            var misCostPerKm = misCost[i].value * km;
                        } else {
                            var misCostPerKm = 0 * km;
                        }
                        if (driverIncentive[i].value != '') {
                            var driverIncentivePerKm = driverIncentive[i].value * km;
                        } else {
                            var driverIncentivePerKm = 0 * km;
                        }
                        if (fuelCostPerKms[i].value != '') {
                            var total = parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                            vehExpense[i].value = total.toFixed(2);
                        } else {
                            var driverIncentivePerKm = 0 * km;
                        }
                        driverIncentive[i].readOnly = true;
                        misCost[i].readOnly = true;
                        factor[i].readOnly = true;
                        calcVehExp();

                    }
                }
            }

            //calculate fuelCostPerKm
            function calcFuleCostPerKm() {
                //alert("am here");
                var km = document.getElementById("distance").value;
                var fuelCostPerKm = 0;
                var fuelCost = document.getElementById('fuelCost').value;
                var vehMileage = document.getElementsByName('vehMileage');
                var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                var factor = document.getElementsByName('factor');
                //alert(km);
                /* if (km != '') {
                 for (var i = 0; i < vehMileage.length; i++) {
                 var totFuelCost = parseFloat(km) / parseFloat(vehMileage[i].value) * parseFloat(fuelCost);
                 fuelCostPerKm = parseFloat(totFuelCost) / parseFloat(km);
                 fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                 }
                 } else {
                 for (var i = 0; i < vehMileage.length; i++) {
                 fuelCostPerKms[i].value = '';
                 }
                 }
                 */
                //setTollCost(1);
                setRCMValues();


            }
            //calculate Vehicle Expenses cost
            function calcVehExp() {
                var km = document.getElementById('distance').value;
                var tollAmounts = document.getElementsByName('tollAmounts');
                var miscCostKm = document.getElementsByName('miscCostKm');
                var driverIncenKm = document.getElementsByName('driverIncenKm');
                var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                var factorPerKm = document.getElementsByName('factor');
                var vehExpense = document.getElementsByName('vehExpense');
                var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                var reeferExpense = document.getElementsByName('reeferExpense');
                var totExpense = document.getElementsByName('totExpense');
                if (km != '') {
                    for (var i = 0; i < fuelCostPerKms.length; i++) {
                        var expCost = parseInt(km) * fuelCostPerKms[i].value;
                        if (miscCostKm[i].value != '') {
                            var misCost = miscCostKm[i].value * km;
                        } else {
                            var misCost = 0 * km;
                        }
                        if (driverIncenKm[i].value != '') {
                            var driverIncentive = driverIncenKm[i].value * km;
                        } else {
                            var driverIncentive = 0 * km;
                        }
                        var total = parseFloat(expCost.toFixed(2)) + parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                        vehExpense[i].value = total.toFixed(2);
                    }
                } else {
                    for (var i = 0; i < fuelCostPerKms.length; i++) {
                        vehExpense[i].value = '';
                        fuelCostPerHrs[i].value = '';
                        reeferExpense[i].value = '';
                        totExpense[i].value = '';
                    }
                }
                calcTotExp();
            }

            //calculate reefer hours
            function calcReferHours() {
                var hour = 0;
                var minute = 0;
                hour = document.getElementById('totalReeferHours').value;
                minute = document.getElementById('totalReeferMinutes').value;
                var refMinute = 0;
                if (hour > 0) {
                    hour = hour * 60;
                }
                if (minute != '') {
                    refMinute = parseInt(hour) + parseInt(minute);
                } else {
                    refMinute = hour;
                }
//                   alert("refMinute==="+refMinute);
                if (refMinute != 00) {
                    calcFuleCostPerHr(refMinute);
                }
            }

            //calculate fuelCostPerHr
            function calcFuleCostPerHr(refMinute) {
//  
                var fuelCost = document.getElementById('fuelCost').value;
                var reefMileage = document.getElementsByName('reefMileage');
                var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                var reeferExpense = document.getElementsByName('reeferExpense');
                for (var i = 0; i < reefMileage.length; i++) {
                    var referExp = reefMileage[i].value * fuelCost;
                    fuelCostPerHrs[i].value = referExp.toFixed(2);
                    if (refMinute != '' && refMinute != 0) {
                        var totalReefExp = fuelCostPerHrs[i].value * refMinute;
                        reeferExpense[i].value = (totalReefExp / 60).toFixed(2);
                    }
                }
                calcTotExp();
            }


            function calcTotExp() {
                var km = document.getElementById('distance').value;
                var reeferExpense = document.getElementsByName('reeferExpense');
                var vehExpense = document.getElementsByName('vehExpense');
                var totExpense = document.getElementsByName('totExpense');
                for (var i = 0; i < vehExpense.length; i++) {
                    if (vehExpense[i].value != '') {
                        var vehExp = vehExpense[i].value;
                    } else {
                        var vehExp = 0;
                    }
                    if (reeferExpense[i].value != '') {
                        var refExp = reeferExpense[i].value;
                    } else {
                        var refExp = 0;
                    }
                    var totCost = parseFloat(vehExp) + parseFloat(refExp);
                    totExpense[i].value = totCost.toFixed(2);
                }
            }

            function calcTollExp(index) {
                var tollAmounts = document.getElementById('tollAmounts' + index).value;
                var km = document.getElementById('distance').value;
                var vehExp = document.getElementById('vehExpense' + index).value;
                var reeferExp = document.getElementById('reeferExpense' + index).value;
                if (tollAmounts != '') {
                    var totExp = parseFloat(tollAmounts) + parseFloat(vehExp) + parseInt(reeferExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                }
            }
            function calcMiscExp(index) {
                var miscCostKm = document.getElementById('miscCostKm' + index).value;
                var km = document.getElementById('distance').value;
                var vehExp = document.getElementById('vehExpense' + index).value;
                //var reeferExp = document.getElementById('reeferExpense'+index).value;
                if (miscCostKm != '') {
                    var totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                }

            }
            function calcDriExp(index) {
                var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                var km = document.getElementById('distance').value;
                var vehExp = document.getElementById('vehExpense' + index).value;
                //var reeferExp = document.getElementById('reeferExpense'+index).value;
                if (driverIncenKm != '') {
                    var totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                }

            }
            function calcFactorExp(index) {
                var factorPerKm = document.getElementById('factor' + index).value;
                var km = document.getElementById('distance').value;
                var vehExp = document.getElementById('vehExpense' + index).value;
                //var reeferExp = document.getElementById('reeferExpense'+index).value;
                if (factorPerKm != '') {
                    var totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                }

            }

        </script>
        <br>
        <br>
        <br>
        <br>
        <% int count = 0;%>
        <c:if test="${mileageConfigList != null}">
            <table width="96%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="70">
                        <th><h3>S.No</h3></th>
                        <th><h3>VehicleType </h3></th>
                        <th><h3>FuelCostPerKm</h3></th>
                        <th><h3>FuelCostPerMin</h3></th>
                        <th><h3>FixedTollCost</h3></th>
                        <th><h3>FixedAddlTollCost</h3></th>
                        <th><h3>FixedParkingCost</h3></th>
                        <th><h3>FixedMiscCost</h3></th>
                        <th><h3>Total Expense Cost</h3></th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${mileageConfigList}" var="mcl">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>

                        <tr height="30">
                            <td align="left" class="<%=classText%>"><%=sno%></td>
                            <td align="left" class="<%=classText%>" style="width:200px"><input type="hidden" name="vehTypeId" id="vehTypeId<%=index%>" value="<c:out value="${mcl.vehicleTypeId}"/>" readonly/><c:out value="${mcl.vehicleTypeName}"/>&nbsp;(<c:out value="${mcl.fuelTypeName}"/>)
                                <input type="hidden" name="vehMileage" id="vehMileage<%=index%>" value="<c:out value="${mcl.vehicleMileage}"/>"/>
                                <input type="hidden" name="reefMileage" id="reefMileage<%=index%>" value="<c:out value="${mcl.reeferMileage}"/>"/>
                                <input type="hidden" name="fuelTypeId" id="fuelTypeId<%=index%>" value="<c:out value="${mcl.fuelTypeId}"/>"/>
                                <input type="hidden" name="vehicleTonnage" id="vehicleTonnage<%=index%>" value="<c:out value="${mcl.vehicleTonnage}"/>"/>
                            </td>
                            <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerKms" id="fuelCostPerKms<%=index%>" value="<fmt:formatNumber pattern="##0.00"  value="${mcl.fuelPrice / mcl.vehicleMileage }"/>"  style="width: 60px" readonly/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerMin" id="fuelCostPerMin<%=index%>" value="<fmt:formatNumber pattern="##0.00"  value="${mcl.fuelPrice / 60*mcl.reeferMileage }"/>"  style="width: 60px" readonly/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="tollAmounts" id="tollAmounts<%=index%>" value="0" style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="addlTollAmounts" id="addlTollAmounts<%=index%>" value="0" style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="parkingCost" id="parkingCost<%=index%>" value="0"   style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="miscCost" id="miscCost<%=index%>" value="0"   style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="totExpense" id="totExpense<%=index%>"  readonly style="width: 90px"/></td>
                        </tr> 
                        <%sno++;%>
                        <%index++;%>
                    </c:forEach>
                    <tr>
                        <td colspan="8" align="center"><input type="button" class="button" name="Save" value="Save" onclick="submitPage()"/></td>
                    </tr>
                <input type="hidden" name="routeNameEmail" id="routeNameEmail" value=""/>
                <input type="hidden" name="customerNameEmail" id="customerNameEmail" value="<c:out value="${customerName}"/>"/>
                <input type="hidden" name="pickup" id="Pickup" value=""/>
                <input type="hidden" name="totalEmail" id="totalHourEmail" value=""/>
                <input type="hidden" name="totalEmail" id="totalMinEmail" value=""/>
                <input type="hidden" name="totalEmail" id="totalWaitMinEmail" value=""/>
                </tbody>
            </table>
             <script language="javascript" type="text/javascript">
                  function totalEmail(){
                      document.getElementById("distance").value=  document.getElementById("totalEmail").value;
                  }
                  function totalHourEmail(){
                    document.getElementById("totalHours").value=  document.getElementById("totalHourEmail").value ; 
                  }
                  function totalMinEmail(){
                   document.getElementById("totalMinutes").value=  document.getElementById("totalMinEmail").value ;  
                  }
                  function totalWaitMinEmail(){
                   document.getElementById("totalWaitMinutes").value=  document.getElementById("totalWaitMinEmail").value ;  
                  }
                 </script>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="200">200</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </c:if>
        <br>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
