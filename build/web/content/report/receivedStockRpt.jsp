

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
         <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

       function submitPage(){
            var chek=validation();

            if(chek=='true'){  
           document.receivedStock.action='/throttle/receivedStockReport.do';
           document.receivedStock.submit();
           }
           }
             function validation(){
            if(document.receivedStock.companyId.value==0){
                alert("Please Select Data for Company Name");
                document.receivedStock.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.receivedStock.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.receivedStock.toDate,'TO Date')){
                return'false';
            }
            else if(compareDates(document.receivedStock.fromDate.value,document.receivedStock.toDate.value) ){
                alert("Please enter valid from date and to date");
                return'false';
            }    
            return 'true';
        }
        function newWindow(indx){
            var supplyId=document.getElementsByName("supplyId");
            window.open('/throttle/viewGRN.do?supplyId='+supplyId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
        }
         function setValues(){           
            if('<%=request.getAttribute("companyId")%>'!='null'){
                document.receivedStock.companyId.value='<%=request.getAttribute("companyId")%>';
                document.receivedStock.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.receivedStock.toDate.value='<%=request.getAttribute("toDate")%>';    
            }
                 if('<%=request.getAttribute("vendorId")%>'!='null'){
                document.receivedStock.vendorId.value='<%=request.getAttribute("vendorId")%>';                                
                }
                 if('<%=request.getAttribute("inVoiceId")%>'!='null'){
                document.receivedStock.inVoiceId.value='<%=request.getAttribute("inVoiceId")%>';                                
                }
                 if('<%=request.getAttribute("poId")%>'!='null'){
                document.receivedStock.poId.value='<%=request.getAttribute("poId")%>';                                
                }
                 if('<%=request.getAttribute("billType")%>'!='null'){
                document.receivedStock.billType.value='<%=request.getAttribute("billType")%>';                                
                }
            }
            
            
    function printPag()
    {       
        var DocumentContainer = document.getElementById('print');
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();   
    }                        
            
            
    </script>
    <body onload="setValues();">
        <form name="receivedStock" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>

    <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Received Stock Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
                    <td height="30"><font color="red">*</font>Location</td>
                    <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${LocationLists != null}" >
                                <c:forEach items="${LocationLists}" var="company">
                                    <c:choose>
                                        <c:when test="${company.companyTypeId==1012}" >

                                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </c:if>

                    </select></td>
                    <td >&nbsp;&nbsp;Vendor</td>
                    <td ><select name="vendorId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${VendorLists != null}" >
                                <c:forEach items="${VendorLists}" var="vendor">
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>

                    </select></td>
                    <td   height="30" >&nbsp;&nbsp;PO No </td>
                    <td   height="30"  ><input name="poId" type="text" class="form-control"  value=""  height="30"></td>
                </tr>
                <tr>
                    <td  height="30"  >&nbsp;&nbsp;Invoice No </td>
                    <td  height="30"  ><input name="inVoiceId" type="text" class="form-control"  value=""  height="30"></td>
                    <td  height="30" ><font color="red">*</font>From Date</td>
                    <td><input name="fromDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.fromDate,'dd-mm-yyyy',this)"/></span></td>
                    <td><font color="red">*</font>To Date</td>
                    <td  height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.toDate,'dd-mm-yyyy',this)"/></span></td>
                </tr>
                <tr>
                    <td  height="30"  >&nbsp;&nbsp;Bill Type</td>
                    <td  height="30"  >
                        <select name="billType" class="form-control" >
                            <option value=''> Select </option>
                            <option value='DC'> DC</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td height="30">&nbsp;</td>
                    <td height="30"><input type="button" name="" value="Search" class="button" height="30" onclick="submitPage();" ></td>
                    
                </tr> 
    </table>
    </div></div>
    </td>
    </tr>
    </table>
              <br>
                <c:set var="Amount" value="0" />
                 <c:if test = "${receivedList != null}" >
<div id="print" >
<style>    
.text1 {
font-family:Tahoma;
font-size:10px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:10px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}    
.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}
</style>    
            <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >Received Stock Report</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >Sno</td>
                    <td  class="contentsub" height="30" >Date</td>
                    <td  class="contentsub" height="30" >GRN No</td>
                    <td  class="contentsub" height="30" >Invoice No</td>
                    <td  class="contentsub" height="30" >DC No</td>
                    <td  class="contentsub" height="30" >PO/LPO No</td>
                    <td  class="contentsub" height="30" >Vendor </td>                                        
                    <td  class="contentsub" height="30" >Purchase Type</td>
                    <td  class="contentsub" height="30" >Amount</td>                    
                </tr>  
                  <% int index = 0;%> 
                    <c:forEach items="${receivedList}" var="stock"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                <tr>
                    <td class="<%=classText%>" height="30" > <%= index+1 %> </td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.billDate}"/></td>
                    <td class="<%=classText%>" height="30" ><input type="hidden" name="supplyId" value="<c:out value='${stock.supplyId}'/>"><a href="" onClick="newWindow(<%=index%>);"><c:out value="${stock.supplyId}"/></a></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.invoiceId}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.dcNo}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.poId}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.vendorName}"/></td>                                     
                       <c:choose>
                                        <c:when test="${stock.purType=='n' || stock.purType=='N'}" >
                                          <td class="<%=classText%>" height="30">Direct Purchase</td>                                           
                                        </c:when>
                                        <c:otherwise>
                                            <td class="<%=classText%>" height="30">Vendor Purchase</td>                                            
                                        </c:otherwise>    
                                    </c:choose>               
                     <td class="<%=classText%>" height="30" align="right" ><c:out value="${stock.totalAmount}"/></td>
                   <c:set var="Amount" value="${Amount + stock.totalAmount}" />   
                </tr>              
             <% index++;%>
                    </c:forEach>
                    <tr>
                        <td class="text2" height="30">&nbsp;  </td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>                        
                        <td class="text2" height="30">&nbsp;</td>                        
                        <td class="text2" height="30"><b>Total Amount</b></td>
                         <td class="text2" align="left"  height="30"> 
                             <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${Amount}" pattern="##.00"/></b>            
                        </td>                        
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        
                       
                </table>                
</div>                             
            </c:if>
            <br>

            <c:if test="${taxSummary != null}" >
            <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >Tax Summary</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >Vat (%)</td>
                    <td  class="contentsub" height="30" >Total Amount</td>
                    <td  class="contentsub" height="30" >Hiked Amount</td>
                    <td  class="contentsub" height="30" >Margin</td>
                    <td  class="contentsub" height="30" >Tax Payable</td>
                </tr> 
                
                <c:forEach items="${taxSummary}" var="tax" >                
                
                <tr>
                    <td  class="text1" height="30"  > 
                    <c:if test="${tax.tax != 0.00}" >
                    <c:out value="${tax.tax}" />(%)                     
                    </c:if>
                    <c:if test="${tax.tax == 0.00}" >
                    Exempted
                    </c:if>                    
                    </td>
                    <td  class="text1" height="30" align="right" > <c:out value="${tax.totalAmount}" /> </td>
                    <td  class="text1" height="30" align="right"  > <c:out value="${tax.hikedAmount}" /> </td>
                    <td  class="text1" height="30" align="right" > <c:out value="${tax.margin}" /> </td>
                    <td  class="text1" height="30" align="right"  > <c:out value="${tax.payableTax}" /> </td>
                </tr>
                
                </c:forEach>                
            </table>   
            <br>
<center>
    <input type="button" class="button" name="print" value="print" onClick="printPag();" >
</center>             
                </c:if>
                <br>
   
                    
                
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
