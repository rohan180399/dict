<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">???????</a>
              </span>-->


    <script type="text/javascript">


        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#tripCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getTripCode.do",
                        dataType: "json",
                        data: {
                            tripCode: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#tripId').val('');
                                $('#tripCode').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split(',');
                    $('#tripId').val(tmp[0]);
                    $('#tripCode').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split(',');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });


        function submitPage(value) {

            if (document.getElementById('fromDate').value == '') {
                alert("Please select from Date");
                document.getElementById('fromDate').focus();
            } else if (document.getElementById('toDate').value == '') {
                alert("Please select to Date");
                document.getElementById('toDate').focus();
            } else {
                if (value == "ExportExcel") {
                    document.tripGpsStatusDetails.action = '/throttle/viewGpsStatusDetais.do?param=ExportExcel';
                    document.tripGpsStatusDetails.submit();
                }
                else {
                    document.tripGpsStatusDetails.action = '/throttle/viewGpsStatusDetais.do?param=Search';
                    document.tripGpsStatusDetails.submit();
                }
            }

        }
    </script>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>View Finance Advice</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Finance</a></li>
                <li class="active">View Finance Advice</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="tripGpsStatusDetails" method="post" >

                    <br>
                    <table class="table table-bordered" id="report" >
                        <!--                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                                                </h2></td>
                                            <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="<spring:message code="operations.reports.label.ShowSearch" text="default text"/>
                        " onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="<spring:message code="operations.reports.label.Close" text="default text"/>" onClick="show_close();"  /></div></td>
                                        </tr>-->
                        <tr height="30" id="index" >
                            <td colspan="4"  style="background-color:#5BC0DE;">
                                <b><spring:message code="operations.reports.label.ViewFinanceAdvice" text="default text"/></b>
                            </td>
                        </tr>
                        <%!
                        public String NullCheck(String inputString)
                             {
                                     try
                                     {
                                             if ((inputString == null) || (inputString.trim().equals("")))
                                                             inputString = "";
                                     }
                                     catch(Exception e)
                                     {
                                                             inputString = "";
                                     }
                                     return inputString.trim();
                             }
                        %>

                        <%

                         String today="";
                         String fromday="";
                           
                         fromday = NullCheck((String) request.getAttribute("fromdate"));
                         today = NullCheck((String) request.getAttribute("todate"));

                         if(today.equals("") && fromday.equals("")){
                         Date dNow = new Date();
                         Calendar cal = Calendar.getInstance();
                         cal.setTime(dNow);
                         cal.add(Calendar.DATE, 0);
                         dNow = cal.getTime();

                         Date dNow1 = new Date();
                         Calendar cal1 = Calendar.getInstance();
                         cal1.setTime(dNow1);
                         cal1.add(Calendar.DATE, -6);
                         dNow1 = cal1.getTime();

                         SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                         today = ft.format(dNow);
                         fromday = ft.format(dNow1);
                         }

                        %>
                        <div id="first">
                            <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.TripCode" text="default text"/></td>
                                        <td height="30"><input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>" style="width:250px;height:40px;" class="form-control"><input type="text"  style="width:250px;height:40px;" name="tripCode" id="tripCode" value="<c:out value="${tripCode}"/>" class="form-control"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate" style="width:250px;height:40px;" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" style="width:250px;height:40px;" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value="<%=today%>"></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.TripStatus" text="default text"/></td>
                                        <td height="30"><select name="tripStatus" id="tripStatus" class="form-control" style="width:250px;height:40px;">
                                                <c:if test="${tripStatus ==''}">
                                                    <option value="0" selected><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="1"><spring:message code="operations.reports.label.TripStartNotUpdated" text="default text"/></option>
                                                    <option value="2"><spring:message code="operations.reports.label.TripEndNotUpdated" text="default text"/></option>
                                                    <option value="3"><spring:message code="operations.reports.label.TripDetailsNotUpdated" text="default text"/></option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='0'}">
                                                    <option value="0" selected><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="1"></option>
                                                    <option value="2"><spring:message code="operations.reports.label.TripEndNotUpdated" text="default text"/></option>
                                                    <option value="3"><spring:message code="operations.reports.label.TripDetailsNotUpdated" text="default text"/></option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='1'}">
                                                    <option value="0"><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="1" selected><spring:message code="operations.reports.label.TripStartNotUpdated" text="default text"/></option>
                                                    <option value="2"><spring:message code="operations.reports.label.TripEndNotUpdated" text="default text"/></option>
                                                    <option value="3"><spring:message code="operations.reports.label.TripDetailsNotUpdated" text="default text"/></option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='2'}">
                                                    <option value="0"><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="1"><spring:message code="operations.reports.label.TripStartNotUpdated" text="default text"/></option>
                                                    <option value="2" selected><spring:message code="operations.reports.label.TripEndNotUpdated" text="default text"/></option>
                                                    <option value="3"><spring:message code="operations.reports.label.TripDetailsNotUpdated" text="default text"/></option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='3'}">
                                                    <option value="0"><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="1"><spring:message code="operations.reports.label.TripStartNotUpdated" text="default text"/></option>
                                                    <option value="2"><spring:message code="operations.reports.label.TripEndNotUpdated" text="default text"/></option>
                                                    <option value="3" selected><spring:message code="operations.reports.label.TripDetailsNotUpdated" text="default text"/></option>
                                                </c:if>     
                                            </select> </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">
                                        &nbsp;&nbsp;<input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>"></td>
                                        
                                    </tr>
                                </table>
                        </div>
                        </div>
                        </td>
                        </tr>
                    </table>
                    <br>
                    <br>

                    <c:if test = "${tripGpsStatusDetails != null}" >
                        <table class="table table-info mb30 table-hover" id="table" class="sortable" width="100%">
                            <thead>
                                <tr height="60">
                                    <th><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripCode" text="default text"/></th>                            
                                    <th><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>                            
                                    <th><spring:message code="operations.reports.label.GPSStatus" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.CustomerRoute" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripStatus" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartInd" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartTime" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartGPSInd" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartGPSTime" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripStartAttempt" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartErrorCode" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.StartErrorMessage" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndStatus" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndTime" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndGPSStatus" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndGPSUpdateTime" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndAttempt" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndErrorCode" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.EndErrorMessage" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.DetailsGPSStatus" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripDetailsGPSUpdateTime" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripDetailsAttempt" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripDetailsErrorCode" text="default text"/></th>
                                    <th><spring:message code="operations.reports.label.TripDetailsErrorMessage" text="default text"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0,sno = 1;%>
                                <c:forEach items="${tripGpsStatusDetails}" var="fd">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr height="30">
                                        <td align="left" class="text2"><%=sno%></td>
                                        <td align="left" class="text2"><c:out value="${fd.tripCode}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.vehicleNo}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsSystem}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.routeName}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.statusName}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.tsStartInd}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.tripStartDateTime}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsStartInd}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsStartUpdateTime}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsStartAttempt}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsStartErrorCode}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsStartErrorMsg}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.tsEndInd}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.tripEndDateTime}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsEndInd}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsEndUpdateTime}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsEndAttempt}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsEndErrorCode}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsEndErrorMsg}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsInd}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsDatetime}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsAttempt}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsErrorCode}"/> </td>                                
                                        <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsErrorMsg}"/> </td>                                
                                    </tr>
                                    <%
                                               index++;
                                               sno++;
                                    %>
                                </c:forEach>

                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/>
                            </span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/>
                            <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/>
                            <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>