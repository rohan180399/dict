<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="tripGpsStatusDetails" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "tripGpsStatusDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

<c:if test = "${tripGpsStatusDetails != null}" >
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="60">
                             <th><h3>S.No</h3></th>
                            <th><h3>Trip Code</h3></th>                            
                            <th><h3>Vehicle No</h3></th>                            
                            <th><h3>GPS Status</h3></th>
                            <th><h3>Customer Route INF</h3></th>
                            <th><h3>Trip Status</h3></th>
                            <th><h3>Trip Start status</h3></th>
                            <th><h3>Trip Start Date And Time</h3></th>
                            <th><h3>Trip Start GPS Status</h3></th>
                            <th><h3>Trip Start GPS update Date And Time</h3></th>
                            <th><h3>Trip Start Attempt</h3></th>
                            <th><h3>Trip Start Error Code</h3></th>
                            <th><h3>Trip Start Error Message</h3></th>
                            <th><h3>Trip End Status</h3></th>
                            <th><h3>Trip End Date And Time</h3></th>
                            <th><h3>Trip End GPS Status</h3></th>
                            <th><h3>Trip End GPS update Date And Time</h3></th>
                            <th><h3>Trip End Attempt</h3></th>
                            <th><h3>Trip End Error Code</h3></th>
                            <th><h3>Trip End Error Message</h3></th>
                            <th><h3>Trip Details GPs Status</h3></th>
                            <th><h3>Trip Details GPs Update Date And Time</h3></th>
                            <th><h3>Trip Details Attempt</h3></th>
                            <th><h3>Trip Details Error Code</h3></th>
                            <th><h3>Trip Details Error Message</h3></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${tripGpsStatusDetails}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${fd.tripCode}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.vehicleNo}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsSystem}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.routeName}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.statusName}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.tsStartInd}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.tripStartDateTime}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsStartInd}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsStartUpdateTime}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsStartAttempt}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsStartErrorCode}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsStartErrorMsg}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.tsEndInd}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.tripEndDateTime}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsEndInd}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsEndUpdateTime}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsEndAttempt}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsEndErrorCode}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsEndErrorMsg}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsInd}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsDatetime}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsAttempt}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsErrorCode}"/> </td>                                
                                <td align="left" class="text2"><c:out value="${fd.gpsTripDetailsErrorMsg}"/> </td>   
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>
                       </c:if>

                    </tbody>
                </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>