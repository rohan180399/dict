
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#rtonamestatus").text();
        if (document.getElementById("rtoCode").value == "0") {
            errStr = "Please enter the RTO Code.\n";
            alert(errStr);
            document.getElementById("rtoCode").focus();
        }
        else if (document.getElementById("rtoName").value == "")
        {
            errStr = "Please enter the RTO name.\n";
            alert(errStr);
            document.getElementById("rtoName").focus();
        }
        else if (document.getElementById("cityId").value == "")
        {
            errStr = "please select city.\n";
            alert(errStr);
            document.getElementById("cityId").focus();
        }

        if (errStr == "") {
            document.productCategory.action = " /throttle/SaveRtoMaster.do";
            document.productCategory.method = "post";
            document.productCategory.submit();
        }

    }

    function setValues(sno, rtoId, rtoCode, rtoName, cityId, status) {

        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("rtoId").value = rtoId;
        document.getElementById("rtoCode").value = rtoCode;
        document.getElementById("rtoName").value = rtoName;
        document.getElementById("activeInd").value = status;
        document.getElementById("cityId").value = cityId;
    }



</script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">RTO Master</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="document.productCategory.rtonamemaster.focus();">


                <form name="productCategory"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">RTO Master</td></tr>
                        <!--                    <tr>
                                                <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="rtonamestatus" style="color: red"></label></td>
                                            </tr>-->
                        <tr>  <input type="hidden" name="rtoId" id="rtoId" value=""/>
                        <td >&nbsp;&nbsp;<font color="red">*</font>RTO Code :</td>
                        <td ><input type="text" name="rtoCode" id="rtoCode" class="form-control" style="width:240px;height:40px;" value=""/>
                        </td>

                        <td >&nbsp;&nbsp;<font color="red">*</font>RTO Name :</td>

                        <td ><input type="text" name="rtoName" id="rtoName" class="form-control" style="width:240px;height:40px;" value=""/>
                        </td>
                        </tr>

                        <tr>
                            <td >&nbsp;&nbsp;<font color="red">*</font>City Name:</td>
                            <td >
                                <select id="cityId" name="cityId"  class="form-control" style="width:240px;height:40px;" value="" autocomplete="off">
                                    <option value="0" selected>Select</option>
                                    <c:if test = "${Citylist != null}">
                                        <c:forEach items="${Citylist}" var="cc">
                                            <option value="<c:out value="${cc.cityId}" />"><c:out value="${cc.cityName}" /></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                            <td >
                                <select  align="center" class="form-control" style="width:240px;height:40px;" name="activeInd" id="activeInd" >
                                    <option value='Y'>Active</option>
                                    <option value='N' id="inActive" style="display: none" >In-Active</option>
                                </select>
                            </td>
                        </tr>
                        <tr><td colspan="4"><center><input type="button" class="btn btn-info" value="Save" name="Submit" onClick="submitPage()"></center></td></tr>
                    </table>
                    

                    <h2 align="center" >List Of The RTO </h2>


                    <table class="table table-info mb30 table-hover" style="width:100%" id="table">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th>S.No</th>
                        <th width="120">Rto code</th>
                        <th width="150">Rto name</th>
                        <th width="150">City name</th>
                        <th>Status</th>
                        <th>Select</th>
                        </tr>
                        </thead>
                        <tbody>

                            <% int sno = 0;%>
                            <c:if test = "${Vehicleno != null}">
                                <c:forEach items="${Vehicleno}" var="pc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=className%>"  align="center"> <%= sno%>  </td>
                                        <td class="<%=className%>"  align="center"> <c:out value="${pc.rtoCode}" /></td>
                                        <td class="<%=className%>"  align="center"> <c:out value="${pc.rtoName}" /></td>
                                        <td class="<%=className%>"  align="center"> <c:out value="${pc.cityId}" /></td>
                                        <td class="<%=className%>"  align="center"> <c:out value="${pc.status}"/></td>
                                        <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${pc.rtoId}" />', '<c:out value="${pc.rtoCode}" />', '<c:out value="${pc.rtoName}" />', '<c:out value="${pc.originCityId}" />', '<c:out value="${pc.status}" />');" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />

                    <br>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>