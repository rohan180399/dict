/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.report.business;

/**
 *
 * @author Sony
 */
public class InvoiceDetailsDTO {

      private String grNumber;
      private String grDate;
      private String destination;
      private String containerQty;
      private String commodityCategory;
      private String articleName;
      private String freightAmount;
      private String tollTax;
      private String weightmentExpense;
      private String otherExpense;
      private String detaintion;

      public String getGrNumber() {
          return grNumber;
      }

      public void setGrNumber(String grNumber) {
          this.grNumber = grNumber;
      }

      public String getGrDate() {
          return grDate;
      }

      public void setGrDate(String grDate) {
          this.grDate = grDate;
      }

      public String getDestination() {
          return destination;
      }

      public void setDestination(String destination) {
          this.destination = destination;
      }

      public String getContainerQty() {
          return containerQty;
      }

      public void setContainerQty(String containerQty) {
          this.containerQty = containerQty;
      }

      public String getCommodityCategory() {
          return commodityCategory;
      }

      public void setCommodityCategory(String commodityCategory) {
          this.commodityCategory = commodityCategory;
      }

      public String getArticleName() {
          return articleName;
      }

      public void setArticleName(String articleName) {
          this.articleName = articleName;
      }

      public String getFreightAmount() {
          return freightAmount;
      }

      public void setFreightAmount(String freightAmount) {
          this.freightAmount = freightAmount;
      }

      public String getTollTax() {
          return tollTax;
      }

      public void setTollTax(String tollTax) {
          this.tollTax = tollTax;
      }

      public String getWeightmentExpense() {
          return weightmentExpense;
      }

      public void setWeightmentExpense(String weightmentExpense) {
          this.weightmentExpense = weightmentExpense;
      }

      public String getOtherExpense() {
          return otherExpense;
      }

      public void setOtherExpense(String otherExpense) {
          this.otherExpense = otherExpense;
      }

      public String getDetaintion() {
          return detaintion;
      }

      public void setDetaintion(String detaintion) {
          this.detaintion = detaintion;
      }


  }
