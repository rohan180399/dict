/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.web;

/**
 *
 * @author Arul
 */
public class SecondaryOperationCommand {
     private String[] containerTypeId = null;
       private String[] containerQty = null;
       private String[] distnaceVehicleTypeId = null;
       private String[] fromDistance = null;
       private String[] toDistance = null;
       private String[] rateWithReeferDistance = null;
       private String[] rateWithoutReeferDistance = null;
       private String[] referenceName = null;
       private String[] loadTypeId = null;
       

     
     private String contractNo = "";
     private String contractFrom = "";
     private String contractTo = "";
     private String tariffType = "";
     private String validity = "";
     private String email = "";
    private String quotationStatus = "";
    private String salesManager = "";
    private String quotationType = "";
    private String quotationDate = "";
    private String startReading = "";
    private String trailerId = "";
        private String vehicleId = "";
        private String mfrId = "";
        private String modelId = "";
    private String extraRunKmCost = "";
     private String vehicleRegNo = "";
      private String agreedDate = "";
      private String activeInd = "";
      private String remarks = "";
      private String mfr = "";
      private String model = "";
      private String modelName = "";
      private String trailerNo = "";
    private String trailerRemarks = "";
    private String[] selectedIndex = null;
    private String billingKmCalculationId = "";
    private String maxAllowedKm = "";
    private String driverResponsibility = "";
    private String fuelResponsibility = "";
    private String startDate = "";
    private String endDate = "";
    private String vehTypeId = "";
    private String axleName = "";
    private String singleJourney = "";
    private String returnJourney = "";
    private String monthlyPass = "";
    private String localVehicleCost = "";
    private String cityName = "";
    private String latitude = "";
    private String longitude = "";
    private String googleCityName = "";
    private String zoneId = "";
    private String countryId = "";
    private String tollName = "";
    private String tollId = "";
    private String currencyCode = "";
    
    private String routeCostId = "";
    private String slabId = "";
    private String slabName = "";
    private String vendorId = "";
    private String slabRate = "";
    private String slabRateId = "";
    

    private String primaryDriver = "";
    private String custId = "";
    private String billYear = "";
    private String billMonth = "";
    private String extraKmCalculation = "";
    private String contractCngCost = "";
    private String contractDieselCost = "";
    private String rateChangeOfCng = "";
    private String rateChangeOfDiesel = "";
    private String averageKM = "";
    private String preCoolingHr = "";
    private String loadingHr = "";
    private String humanId = "";
    private String contractId = "";
    private String id = "";
    private String primaryDriverId = "";
    private String primaryDriverName = "";
    private String fromDate = "";
    private String toDate = "";
    private String noOfVehicles = "";
    private String vehicleTypeName = "";
    private String fixedKm = "";
    private String fixedKmCharge = "";
    private String extraKmCharge = "";
    private String vehicleTypeId = "";
    private String latitudePosition = "";
    private String longitudePosition = "";
    private String status = "";
    private String pointName = "";
//    private String customerId = "";
    private String pointType = "";
    private String pointAddress = "";
    private String cityId = "";
    private String pointId = "";
    private String totalKm = "";
    private String routeId = "";
    private String routeContractId = "";
    private String destination = "";
    private String customerTypeId = "0";
    private String entryType = "";
    private String consignmentNoteNo = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "0";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String origin = "";
    private String businessType = "0";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String serviceType = "0";
    private String reeferRequired = "";
    private String contractRateId = "0";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String totalPackage = "";
    private String totalWeightage = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String totFreightAmount = "";
    private String billingTypeId = "";
    private String scheduleDate = "";
    private String customerCode = "";
    private String dateName = "";
    private String customerName = "";
    private String nextWeek = "";
    private String[] secRouteId = null;
    private String[] scheduledate = null;
    private String secondaryRouteId = "";
    private String secondaryRouteCode = "";
    private String secondaryRouteName = "";
    private String routeName = "";
    private String fuelCost = "";
    private String customerId = "";
    private String fixedKmPerMonth = "";
    private String fixedReeferHours = "";
    private String fixedReeferMinutes = "";
    private String routeValidFrom = "";
    private String routeValidTo = "";
    private String distance = "";
    private String totalReeferHours = "";
    private String totalReeferMinutes = "";
    private String totalWaitMinutes = "";
    //pavi
    private String levelID = "";
    private String ledgerId = "";
    private String levelGroupName = "";
    private String expenseId = "";
    private String groupName = "";
    private String expenseName = "";
      private String dept = "";
     private String contactPerson = "";

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getLevelID() {
        return levelID;
    }

    public void setLevelID(String levelID) {
        this.levelID = levelID;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLevelGroupName() {
        return levelGroupName;
    }

    public void setLevelGroupName(String levelGroupName) {
        this.levelGroupName = levelGroupName;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }
    

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFixedKmPerMonth() {
        return fixedKmPerMonth;
    }

    public void setFixedKmPerMonth(String fixedKmPerMonth) {
        this.fixedKmPerMonth = fixedKmPerMonth;
    }

    public String getFixedReeferHours() {
        return fixedReeferHours;
    }

    public void setFixedReeferHours(String fixedReeferHours) {
        this.fixedReeferHours = fixedReeferHours;
    }

    public String getFixedReeferMinutes() {
        return fixedReeferMinutes;
    }

    public void setFixedReeferMinutes(String fixedReeferMinutes) {
        this.fixedReeferMinutes = fixedReeferMinutes;
    }

    public String getRouteValidFrom() {
        return routeValidFrom;
    }

    public void setRouteValidFrom(String routeValidFrom) {
        this.routeValidFrom = routeValidFrom;
    }

    public String getRouteValidTo() {
        return routeValidTo;
    }

    public void setRouteValidTo(String routeValidTo) {
        this.routeValidTo = routeValidTo;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalReeferHours() {
        return totalReeferHours;
    }

    public void setTotalReeferHours(String totalReeferHours) {
        this.totalReeferHours = totalReeferHours;
    }

    public String getTotalReeferMinutes() {
        return totalReeferMinutes;
    }

    public void setTotalReeferMinutes(String totalReeferMinutes) {
        this.totalReeferMinutes = totalReeferMinutes;
    }

    public String getTotalWaitMinutes() {
        return totalWaitMinutes;
    }

    public void setTotalWaitMinutes(String totalWaitMinutes) {
        this.totalWaitMinutes = totalWaitMinutes;
    }

    public String getDateName() {
        return dateName;
    }

    public void setDateName(String dateName) {
        this.dateName = dateName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getNextWeek() {
        return nextWeek;
    }

    public void setNextWeek(String nextWeek) {
        this.nextWeek = nextWeek;
    }

    public String[] getSecRouteId() {
        return secRouteId;
    }

    public void setSecRouteId(String[] secRouteId) {
        this.secRouteId = secRouteId;
    }

    public String[] getScheduledate() {
        return scheduledate;
    }

    public void setScheduledate(String[] scheduledate) {
        this.scheduledate = scheduledate;
    }

    public String getSecondaryRouteId() {
        return secondaryRouteId;
    }

    public void setSecondaryRouteId(String secondaryRouteId) {
        this.secondaryRouteId = secondaryRouteId;
    }

    public String getSecondaryRouteCode() {
        return secondaryRouteCode;
    }

    public void setSecondaryRouteCode(String secondaryRouteCode) {
        this.secondaryRouteCode = secondaryRouteCode;
    }

    public String getSecondaryRouteName() {
        return secondaryRouteName;
    }

    public void setSecondaryRouteName(String secondaryRouteName) {
        this.secondaryRouteName = secondaryRouteName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLatitudePosition() {
        return latitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        this.latitudePosition = latitudePosition;
    }

    public String getLongitudePosition() {
        return longitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        this.longitudePosition = longitudePosition;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getExtraKmCharge() {
        return extraKmCharge;
    }

    public void setExtraKmCharge(String extraKmCharge) {
        this.extraKmCharge = extraKmCharge;
    }

    public String getFixedKm() {
        return fixedKm;
    }

    public void setFixedKm(String fixedKm) {
        this.fixedKm = fixedKm;
    }

    public String getFixedKmCharge() {
        return fixedKmCharge;
    }

    public void setFixedKmCharge(String fixedKmCharge) {
        this.fixedKmCharge = fixedKmCharge;
    }

    public String getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(String noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getHumanId() {
        return humanId;
    }

    public void setHumanId(String humanId) {
        this.humanId = humanId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAverageKM() {
        return averageKM;
    }

    public void setAverageKM(String averageKM) {
        this.averageKM = averageKM;
    }

    public String getLoadingHr() {
        return loadingHr;
    }

    public void setLoadingHr(String loadingHr) {
        this.loadingHr = loadingHr;
    }

    public String getPreCoolingHr() {
        return preCoolingHr;
    }

    public void setPreCoolingHr(String preCoolingHr) {
        this.preCoolingHr = preCoolingHr;
    }

    public String getExtraKmCalculation() {
        return extraKmCalculation;
    }

    public void setExtraKmCalculation(String extraKmCalculation) {
        this.extraKmCalculation = extraKmCalculation;
    }

    public String getContractCngCost() {
        return contractCngCost;
    }

    public void setContractCngCost(String contractCngCost) {
        this.contractCngCost = contractCngCost;
    }

    public String getContractDieselCost() {
        return contractDieselCost;
    }

    public void setContractDieselCost(String contractDieselCost) {
        this.contractDieselCost = contractDieselCost;
    }

    public String getRateChangeOfCng() {
        return rateChangeOfCng;
    }

    public void setRateChangeOfCng(String rateChangeOfCng) {
        this.rateChangeOfCng = rateChangeOfCng;
    }

    public String getRateChangeOfDiesel() {
        return rateChangeOfDiesel;
    }

    public void setRateChangeOfDiesel(String rateChangeOfDiesel) {
        this.rateChangeOfDiesel = rateChangeOfDiesel;
    }
    
    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }


    public String getBillYear() {
        return billYear;
    }

    public void setBillYear(String billYear) {
        this.billYear = billYear;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(String primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public String getSlabName() {
        return slabName;
    }

    public void setSlabName(String slabName) {
        this.slabName = slabName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getSlabRate() {
        return slabRate;
    }

    public void setSlabRate(String slabRate) {
        this.slabRate = slabRate;
    }

    public String getSlabRateId() {
        return slabRateId;
    }

    public void setSlabRateId(String slabRateId) {
        this.slabRateId = slabRateId;
    }

    public String getRouteCostId() {
        return routeCostId;
    }

    public void setRouteCostId(String routeCostId) {
        this.routeCostId = routeCostId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getGoogleCityName() {
        return googleCityName;
    }

    public void setGoogleCityName(String googleCityName) {
        this.googleCityName = googleCityName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getTollName() {
        return tollName;
    }

    public void setTollName(String tollName) {
        this.tollName = tollName;
    }

    public String getTollId() {
        return tollId;
    }

    public void setTollId(String tollId) {
        this.tollId = tollId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String getAxleName() {
        return axleName;
    }

    public void setAxleName(String axleName) {
        this.axleName = axleName;
    }

    public String getSingleJourney() {
        return singleJourney;
    }

    public void setSingleJourney(String singleJourney) {
        this.singleJourney = singleJourney;
    }

    public String getReturnJourney() {
        return returnJourney;
    }

    public void setReturnJourney(String returnJourney) {
        this.returnJourney = returnJourney;
    }

    public String getMonthlyPass() {
        return monthlyPass;
    }

    public void setMonthlyPass(String monthlyPass) {
        this.monthlyPass = monthlyPass;
    }

    public String getLocalVehicleCost() {
        return localVehicleCost;
    }

    public void setLocalVehicleCost(String localVehicleCost) {
        this.localVehicleCost = localVehicleCost;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getBillingKmCalculationId() {
        return billingKmCalculationId;
    }

    public void setBillingKmCalculationId(String billingKmCalculationId) {
        this.billingKmCalculationId = billingKmCalculationId;
    }

    public String getDriverResponsibility() {
        return driverResponsibility;
    }

    public void setDriverResponsibilty(String driverResponsibility) {
        this.driverResponsibility = driverResponsibility;
    }

    public String getFuelResponsibility() {
        return fuelResponsibility;
    }

    public void setFuelResponsibility(String fuelResponsibility) {
        this.fuelResponsibility = fuelResponsibility;
    }

    public String getMaxAllowedKm() {
        return maxAllowedKm;
    }

    public void setMaxAllowedKm(String maxAllowedKm) {
        this.maxAllowedKm = maxAllowedKm;
    }

    public String getExtraRunKmCost() {
        return extraRunKmCost;
    }

    public void setExtraRunKmCost(String extraRunKmCost) {
        this.extraRunKmCost = extraRunKmCost;
    }

    public String getAgreedDate() {
        return agreedDate;
    }

    public void setAgreedDate(String agreedDate) {
        this.agreedDate = agreedDate;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getStartReading() {
        return startReading;
    }

    public void setStartReading(String startReading) {
        this.startReading = startReading;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(String quotationDate) {
        this.quotationDate = quotationDate;
    }

    public String getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(String quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public String getQuotationType() {
        return quotationType;
    }

    public void setQuotationType(String quotationType) {
        this.quotationType = quotationType;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getTariffType() {
        return tariffType;
    }
    
    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public String[] getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String[] containerQty) {
        this.containerQty = containerQty;
    }

    public String[] getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String[] containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String[] getDistnaceVehicleTypeId() {
        return distnaceVehicleTypeId;
    }

    public void setDistnaceVehicleTypeId(String[] distnaceVehicleTypeId) {
        this.distnaceVehicleTypeId = distnaceVehicleTypeId;
    }

    public String[] getFromDistance() {
        return fromDistance;
    }

    public void setFromDistance(String[] fromDistance) {
        this.fromDistance = fromDistance;
    }

    public String[] getRateWithReeferDistance() {
        return rateWithReeferDistance;
    }

    public void setRateWithReeferDistance(String[] rateWithReeferDistance) {
        this.rateWithReeferDistance = rateWithReeferDistance;
    }

    public String[] getRateWithoutReeferDistance() {
        return rateWithoutReeferDistance;
    }

    public void setRateWithoutReeferDistance(String[] rateWithoutReeferDistance) {
        this.rateWithoutReeferDistance = rateWithoutReeferDistance;
    }

    public String[] getToDistance() {
        return toDistance;
    }

    public void setToDistance(String[] toDistance) {
        this.toDistance = toDistance;
    }

    public String[] getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String[] referenceName) {
        this.referenceName = referenceName;
    }

    public String[] getLoadTypeId() {
        return loadTypeId;
    }

    public void setLoadTypeId(String[] loadTypeId) {
        this.loadTypeId = loadTypeId;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }
    
    
    
}
