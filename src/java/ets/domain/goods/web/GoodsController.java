package ets.domain.goods.web;

import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.goods.business.GoodsBP;
import ets.domain.goods.business.GoodsTO;
import ets.domain.racks.business.RackBP;
import ets.domain.security.business.SecurityBP;
import ets.domain.security.business.SecurityTO;
import ets.domain.util.ParveenErrorConstants;
//import ets.domain.vendor.business.VendorBP;
import ets.domain.vehicle.business.VehicleBP;
//import ets.domain.vendor.business.VendorTO;
import java.io.PrintWriter;
import java.util.*;

public class GoodsController extends BaseController {

    GoodsCommand goodsCommand;
    LoginBP loginBP;
    SecurityBP securityBP;
    RackBP rackBP;
    GoodsBP goodsBP;

    public GoodsBP getGoodsBP() {
        return goodsBP;
    }

    public void setGoodsBP(GoodsBP goodsBP) {
        this.goodsBP = goodsBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public RackBP getRackBP() {
        return rackBP;
    }

    public void setRackBP(RackBP rackBP) {
        this.rackBP = rackBP;
    }

    public SecurityBP getSecurityBP() {
        return securityBP;
    }

    public void setSecurityBP(SecurityBP securityBP) {
        this.securityBP = securityBP;
    }

    public GoodsCommand getGoodsCommand() {
        return goodsCommand;
    }

    public void setGoodsCommand(GoodsCommand goodsCommand) {
        this.goodsCommand = goodsCommand;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);


    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageGoodsPage(HttpServletRequest request, HttpServletResponse response, GoodsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        goodsCommand = command;
        GoodsTO goodsTO = new GoodsTO();
        String menuPath = "Manage Goods  >>  View ";
        path = "content/goods/manageGoods.jsp";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Goods-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Goods";
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String status = "BOTH";
                String fromDate = (String) session.getAttribute("currentDate");
                String toDate = (String) session.getAttribute("currentDate");
                String gdNo = "";
                String googType = "ALL";

                goodsTO.setGoodsFromDate(fromDate);
                goodsTO.setGoodsToDate(toDate);
                goodsTO.setGdNo(gdNo);
                goodsTO.setGdType(googType);
                goodsTO.setStatus(status);


                ArrayList searchGoodsList = new ArrayList();
                searchGoodsList = goodsBP.processGoodsList(goodsTO);
                if (searchGoodsList.size() != 0) {
                    request.setAttribute("SearchGoodsList", searchGoodsList);
                }
                //////System.out.println("searchGoodsList" + searchGoodsList.size());
                request.setAttribute("goodsStatus", status);
                request.setAttribute("fromDat", fromDate);
                request.setAttribute("toDate", toDate);
                request.setAttribute("gdNo", gdNo);
                request.setAttribute("gdType", googType);





            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve goods data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageGoodsSave(HttpServletRequest request, HttpServletResponse response, GoodsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        goodsCommand = command;
        String path = "";
        ModelAndView mv = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        path = "content/goods/manageGoods.jsp";
        try {

            String menuPath = "Sceurity  >>  manageGoods    ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            int index = 0;
            int modify = 0;
            String[] gdIds = goodsCommand.getGdIds();
            String[] remarks = goodsCommand.getRemarks();
            String[] in = goodsCommand.getIn();
            String[] out = goodsCommand.getOut();

            //  //////System.out.println("gdIds="+gdIds.length);
            // //////System.out.println("remarks="+remarks.length);
//            //////System.out.println("in="+in.length);
            //    //////System.out.println("out="+out.length);

            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List1 = new ArrayList();
            ArrayList List2 = new ArrayList();
            GoodsTO goodsTO = null;


            if (in != null) {
                //////System.out.println("in=" + in.length);
                for (int i = 0; i < in.length; i++) {

                    goodsTO = new GoodsTO();
                    index = Integer.parseInt(in[i]);

                    goodsTO.setGdId(gdIds[index]);
                    goodsTO.setRemark(remarks[index]);
                    List1.add(goodsTO);

                }
                String pageTitle = "Manage goods";
                request.setAttribute("pageTitle", pageTitle);
                modify = goodsBP.processSaveGoods(List1, UserId);

            }


            if (out != null) {
                //////System.out.println("out=" + out.length);
                for (int j = 0; j < out.length; j++) {
                    goodsTO = new GoodsTO();
                    index = Integer.parseInt(out[j]);
                    goodsTO.setGdId(gdIds[index]);
                    goodsTO.setRemark(remarks[index]);
                    List2.add(goodsTO);

                }
                String pageTitle = "Manage sec";
                request.setAttribute("pageTitle", pageTitle);
                modify = goodsBP.processSaveGoodsOut(List2, UserId);

            }

            // To get workorder date List

            ArrayList goodsList = new ArrayList();
            goodsList = goodsBP.processGoodsList();
            request.setAttribute("GoodsList", goodsList);



            // List to get the out time is for RC or stock transfer
            ArrayList outList = new ArrayList();
            outList = goodsBP.processGdTypeList();
            request.setAttribute("OutList", outList);



            ArrayList inList = new ArrayList();
            inList = goodsBP.processInList();
            request.setAttribute("InList", inList);

            mv = manageGoodsPage(request, response, command);


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve goods --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    //addGoods
    /**
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddGoodsPage(HttpServletRequest request, HttpServletResponse response, GoodsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        path = "content/goods/addGoods.jsp";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Goods-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                goodsCommand = command;
                String menuPath = "GOODS  >>    Add";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Add Goods";
                request.setAttribute("pageTitle", pageTitle);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //  handleAddGoods
    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddSaveGoods(HttpServletRequest request, HttpServletResponse response, GoodsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        goodsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        path = "content/goods/manageGoods.jsp";
        int userId = (Integer) session.getAttribute("userId");
        try {

            String menuPath = "GOODS  >>    Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            GoodsTO goodsTO = new GoodsTO();

            if (goodsCommand.getRefNo() != null && goodsCommand.getRefNo() != "") {
                goodsTO.setRefNo(goodsCommand.getRefNo());
            }
            if (goodsCommand.getDescription() != null && goodsCommand.getDescription() != "") {
                goodsTO.setDescription(goodsCommand.getDescription());
            }

            String pageTitle = "View Goods";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;


            insertStatus = goodsBP.processInsertGoodsDetails(goodsTO, userId);


            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Goods Added Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //   handleSearchGoods
    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleSearchGoods(HttpServletRequest request, HttpServletResponse response, GoodsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        goodsCommand = command;
        GoodsTO goodsTO = new GoodsTO();
        String menuPath = "Manage Goods  >>  View ";
        path = "content/goods/manageGoods.jsp";

        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Goods-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Goods";
                request.setAttribute("pageTitle", pageTitle);

                String status = goodsCommand.getGoodsStatus();
                String fromDat = goodsCommand.getGoodsFromDate();
                String toDate = goodsCommand.getGoodsToDate();
                String gdNo = goodsCommand.getGdNo();
                //////System.out.println("goodsCommand.getGoodType()" + goodsCommand.getGoodType());


                if (goodsCommand.getGoodsFromDate() != null && goodsCommand.getGoodsFromDate() != "") {
                    goodsTO.setGoodsFromDate(goodsCommand.getGoodsFromDate());
                }
                if (goodsCommand.getGoodsToDate() != null && goodsCommand.getGoodsToDate() != "") {
                    goodsTO.setGoodsToDate(goodsCommand.getGoodsToDate());
                }
                if (goodsCommand.getGdNo() != null && goodsCommand.getGdNo() != "") {
                    goodsTO.setGdNo(goodsCommand.getGdNo());
                }
                System.out.println(request.getParameter("goodType") + "vijay");
                goodsTO.setGdType(request.getParameter("goodType"));

                if (goodsCommand.getGoodsStatus() != null && goodsCommand.getGoodsStatus() != "") {
                    goodsTO.setStatus(goodsCommand.getGoodsStatus());
                }


                // To get workorder date List

                ArrayList searchGoodsList = new ArrayList();
                searchGoodsList = goodsBP.processGoodsList(goodsTO);
                if (searchGoodsList.size() != 0) {
                    request.setAttribute("SearchGoodsList", searchGoodsList);
                }
                //////System.out.println("searchGoodsList" + searchGoodsList.size());


                // List to get the out time is for RC or stock transfer
//                 ArrayList outList = new ArrayList();
//                 outList = goodsBP.processGdTypeList();
//                 request.setAttribute("OutList", outList);
//                //////System.out.println("OutList"+outList.size());
//
//
//                  ArrayList inList = new ArrayList();
//                 inList = goodsBP.processInList();
//                 request.setAttribute("InList", inList);
//                 //////System.out.println("InList"+inList.size());
//
                request.setAttribute("goodsStatus", goodsCommand.getGoodsStatus());
                request.setAttribute("fromDat", goodsCommand.getGoodsFromDate());
                request.setAttribute("toDate", goodsCommand.getGoodsToDate());
                request.setAttribute("gdNo", goodsCommand.getGdNo());
                request.setAttribute("gdType", goodsCommand.getGoodType());







            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve goods data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
}






