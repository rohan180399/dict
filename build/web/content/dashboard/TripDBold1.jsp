<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@page import="java.awt.image.BufferedImage"%>
<%@page import="javax.swing.JPanel"%>
<%@page import="java.awt.Dimension"%>
<%@page import="java.awt.GridLayout"%>
<%@page import="org.jfree.chart.ChartPanel"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<html lang="en">
    <head>
        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <!--<script language="JavaScript" src="FusionCharts.js"></script>-->
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
<!--        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>-->
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="css/jquery.ui.theme.css">
        <script src="js/jquery-1.4.4.js"></script>
        <script src="js/jquery.ui.core.js"></script>
        <script src="js/jquery.ui.widget.js"></script>
        <script src="js/jquery.ui.mouse.js"></script>
        <script src="js/jquery.ui.sortable.js"></script>
        <%@ page  import="java.awt.*" %>
        <%@ page  import="java.io.*" %>
        <%@ page  import="javax.swing.*" %>
        <%@ page  import="org.jfree.chart.*" %>
        <%@ page  import="org.jfree.chart.entity.*" %>
        <%@ page  import ="org.jfree.data.general.*"%>
        <%@ page import="org.jfree.chart.ChartFactory.*"%>
        <%@ page import="org.jfree.chart.ChartUtilities.*"%>
        <%@ page import="org.jfree.chart.JFreeChart.*"%>
        <%@ page import="java.io.OutputStream.*"%>
        <%@ page import="org.jfree.data.category.DefaultCategoryDataset"%>
        <%@ page import="org.jfree.chart.plot.PlotOrientation"%>
<meta http-equiv="Content-Type"
   content="text/html; charset=UTF-8" >
  <!meta  http-equiv="refresh" content="1">
        <style type="text/css">
            .link {
                font: normal 12px Arial;
                text-transform:uppercase;
                padding-left:10px;
                font-weight:bold;
            }

            .link a  {
                color:#7f8ba5;
                text-decoration:none;
            }

            .link a:hover {
                color:#7f8ba5;
                text-decoration:underline;

            }

        </style>
        <style type="text/css">
            #expand {
                width:140%;
            }
            .column { width: 435px; float: left; }
            .portlet { margin: 0 1em 1em 0; }
            .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
            .portlet-header .ui-icon { float: right; }
            .portlet-content { padding: 0.4em; }
            .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
            .ui-sortable-placeholder * { visibility: hidden; }
        </style>
        <script>
            $(function() {
                $( ".column" ).sortable({
                    connectWith: ".column"
                });

                $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
                .end()
                .find( ".portlet-content" );

                $( ".portlet-header .ui-icon" ).click(function() {
                    $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
                    $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
                });

                $( ".column" ).disableSelection();
            });
        </script>
    </head>
    <body>
        <!--Dynamic Portion-->
        <%
            Connection conn = null;
            int count = 0;
            try{

                String fileName = "jdbc_url.properties";
                Properties dbProps = new Properties();
            //The forward slash "/" in front of in_filename will ensure that
            //no package names are prepended to the filename when the Classloader
            //search for the file in the classpath

            InputStream is = getClass().getResourceAsStream("/"+fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");

            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");


            String itemCode = request.getParameter("itemCode");

            DecimalFormat df2 = new DecimalFormat("0.00");
            Class.forName(dbClassName).newInstance();
            conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

            String curMonthName = "SELECT CONCAT(MONTHNAME(CURDATE()),'-',YEAR(CURDATE())) as monthName" ;
            String ovAllTrips = "SELECT status_name as statusName, count(*) as Total FROM  ts_trip_master a,  ts_status_master b  where trip_type = 1 "
                                + "and a.trip_Status_Id = b.Status_Id and a.trip_Status_Id != 7   group by trip_status_Id";

            String statusWiseTripDetailsCurMonth  = "SELECT status_name, count(*) as Total FROM "
                                               +" ts_trip_master a, "
                                               +" ts_status_master b "
                                               +" where trip_type = 1 "
                                               +" and a.trip_Status_Id != 7 "
                                               +" and a.empty_trip_approval_status_Id != 3"
                                               +" and MONTH(a.trip_actual_start_date) =  month(now()) "
                                               +" and a.trip_Status_Id = b.Status_Id group by trip_status_Id ";
           // String fcWiseTripDetailsCurMonth = "SELECT status_name as Name, count(*) as tripCount FROM  ts_trip_master a,  ts_status_master b  where trip_type = 1 "
            //                    + "and a.trip_Status_Id = b.Status_Id and a.trip_Status_Id != 7   group by trip_status_Id";

           String fcWiseTripDetailsCurMonth = "SELECT cm.Name as Name, count(*) as tripCount FROM ts_trip_master tm, ts_trip_vehicle tv, papl_vehicle_reg_no reg, "
                                               +" papl_vehicle_operation_point vop, papl_company_master cm "
                                              + "WHERE tv.active_Ind = 'Y' and tm.Trip_Id=tv.Trip_Id "
                                               +" and tm.empty_trip_approval_status_Id != 3"
                                               +" and tm.trip_status_Id in(10,18)"
                                               +" and tv.vehicle_Id = reg.vehicle_Id and reg.active_Ind = 'Y' and tm.trip_type= 1  "
                                               +" and vop .operation_point_Id = cm.Comp_Id and vop.vehicle_Id = tv.vehicle_Id and vop.active_Ind = 'Y' and cm.comp_Id in (1024,1025,1027) "
                                               +" and MONTH(tm.trip_actual_start_date) =  month(now()) group by cm.Comp_Id";


           String routeTrips = "select RouteName,Total from ( SELECT concat(a.city_Name,' - ', b.City_Name) as RouteName, count(*) as Total FROM "
                               +" ts_trip_master ts, ts_route_master rm, ts_cities a, ts_cities b "
                               +" where "
                               +" ts.origin_Id=rm.Route_From_Id and ts.destination_Id = rm.Route_To_Id "
                               +" and rm.Route_From_Id = a.city_Id and rm.Route_To_Id = b.City_Id "
                               +" and ts.trip_type = 1 "
                               +" and ts.empty_trip_approval_status_Id != 3"
                               + " and MONTH(ts.trip_actual_start_date) =  month(now()) "
                               +" group by rm.Route_Id) as q "
                               +"where 1=1 order by Total desc limit 15 ";

           String serviceCostMfrMTD = "SELECT mfr_name,sum(nett_amount) as amt FROM papl_direct_jobcard dj, papl_jobcard_bill_master jb, papl_vehicle_master vm, "
                                     + " papl_mfr_master mf, papl_jobcard_master jm WHERE mfr_name = ? AND dj.job_card_id = jb.job_card_id AND vm.vehicle_id = dj.vehicle_id "
                                     + "AND mf.mfr_id = vm.mfr_id AND jm.job_card_id = dj.job_card_id AND "
                                     + "date_format(jm.closed_on,'%m-%Y') =  date_format(now(),'%m-%Y') AND mf.active_ind = 'Y' AND vm.active_ind = 'Y' "
                                     + "AND dj.active_ind = 'Y' AND jm.active_ind = 'Y' GROUP BY mfr_name";

            String status = "", total = "0";
            String monthName = "";
            PreparedStatement pstmMonthName = conn.prepareStatement(curMonthName);
            PreparedStatement pstmOvAllTrips = conn.prepareStatement(ovAllTrips);
            PreparedStatement pstmStatusWiseTripDetailsCurMonth = conn.prepareStatement(statusWiseTripDetailsCurMonth);
            PreparedStatement pstmFcWiseTripDetailsCurMonth = conn.prepareStatement(fcWiseTripDetailsCurMonth);
            String ovAllTripsXML = "";
            ResultSet resMonth = pstmMonthName.executeQuery();
            if(resMonth.next()) {
                 monthName = resMonth.getString(1);
            }

            
         
                ////////////////////////piechart
                    DefaultPieDataset my_pie_chart_data = new DefaultPieDataset();
        ResultSet res=pstmOvAllTrips.executeQuery();
        while(res.next()) {
//                status =   res.getString(1);
//                total = res.getString(2);
//                ovAllTripsXML = ovAllTripsXML+" <set value='"+total+"' label='"+status+"' alpha='60'/>";
                  my_pie_chart_data.setValue(res.getString(1),res.getDouble(2));
//                my_pie_chart_data.setValue(status, Total);
           }
            JFreeChart chart = ChartFactory.createPieChart(
                 "Over All Trip Details ", // Title
                 my_pie_chart_data,                    // Data
                 true,                       // Display the legend
                 true,                       // Display tool tips
                 false                       // No URLs
                 );
            if (chart != null) {
                        chart.setBorderVisible(true);
                        int width = 600;
                        int height = 400;
                        ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
                        response.setContentType("image/jpeg");
                        OutputStream out1 = response.getOutputStream();
                         BufferedImage chartImage = chart.createBufferedImage(640, 400, info);
                      //   ChartUtilities.writeBufferedImageAsJPEG(out1, chartImage);
                      //session.setAttribute("chartImage", chartImage);
                       // ChartUtilities.writeChartAsJPEG(out1, chart, width, height);
                        //   ChartUtilities.saveChartAsJPEG(new File("C:/Documents and Settings/entitle/Desktop/FSL/FSL(updated)/web/images/piechart.jpeg"),chart,600, 400);
                out1.close();
                }
                if(res != null) {
                res.close();

            }


             ////////////////////////barchart
//             DefaultCategoryDataset my_bar_chart_dataset1 = new DefaultCategoryDataset();
//             res = pstmStatusWiseTripDetailsCurMonth.executeQuery();
//             String custStatus="", tTotal="", tripCustStatusXML="";
//             while(res.next()) {
////                custStatus = res.getString(1);
////                tTotal = res.getString(2);
////                tripCustStatusXML = tripCustStatusXML+ "<set label='"+custStatus+"' value='"+tTotal+"' />";
//                 my_bar_chart_dataset1.addValue(res.getDouble(2),"Marks",res.getString(1));
//             }
//                JFreeChart chart1 = ChartFactory.createBarChart(
//                    "Status", "Trip Closure", "Trips",
//                     my_bar_chart_dataset1, PlotOrientation.VERTICAL, false, true, false);
//                chart1.setBorderVisible(true);
//
//                if (chart1 != null) {
//                        int width = 600;
//                        int height = 400;
//                        response.setContentType("image/jpeg");
//                        OutputStream out1 = response.getOutputStream();
//                        ChartUtilities.writeChartAsJPEG(out1, chart1, width, height);
//                }
///////////////////////////////////////
//             JPanel panel = new JPanel(new GridLayout(2, 2));
      
            %>

            <div id="expand">
            <div class="column" style="width:550px;">

                <div class="portlet" >
                    <div class="portlet-header"  style="width:525px;">&nbsp;&nbsp;Over All Trip Details</div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" ></table>
                    </div>
                </div>
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;">&nbsp;&nbsp;FC Wise Trip In-Progress/WFU Details For <%=monthName%></div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <img src="/throttle/content/dashboard/TripDB.jsp" alt="" usemap="#chartImage"/>
          </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>



</div>
        <script>
            $(function () {
            $('#ovrAlTrpDet').highCharts({
//        chart: {
//            type: 'column'
//        },
//        xAxis: {
//            categories: ['Trip Started/Trip In Progress', 'Trip End', 'Trip Closure']
//        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            location.href = this.options.url;
                        }
                    }
                }
            }
        },

        pie: [{
            data: [{
               name: 'Trip Freezed',
                y: 56,
                url: '/throttle/viewTripSheet.do'
            }, {
                name: 'Trip Created',
                y: 3,
                url: '/throttle/viewTripSheet.do'
            }, {
                name: 'Trip Closure',
                y: 53,
                url: '/throttle/viewTripSheet.do'
            }]
        }]
    });
});
        </script>


                                   <!--<img src="piechart.jpeg" alt="OverAllimage" WIDTH="600" HEIGHT="400" BORDER="0" />-->

<!--                                   <img src="C:/Documents and Settings/entitle/Desktop/FSL/FSL(updated)/web/images/barchart.jpeg" alt="Progress chart" WIDTH="600" HEIGHT="400" BORDER="0" />
                         -->

        <%


        }catch (FileNotFoundException fne){
            System.out.println("File Not found "+fne.getMessage());
        } catch (SQLException se){
            System.out.println("SQL Exception "+se.getMessage());
        }finally{
            if(conn == null) {
                conn.close();
            }
        }

        %>
    </body>
</html>
