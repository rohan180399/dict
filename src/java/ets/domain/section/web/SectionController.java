package ets.domain.section.web;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ets.arch.business.PaginationHelper;
import ets.arch.web.BaseController;
import ets.domain.racks.business.RackBP;
import ets.domain.racks.business.RackTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.vehicle.business.VehicleBP;
import java.io.*;
import ets.domain.racks.business.RackTO;
import ets.domain.section.business.SectionBP;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.section.business.SectionTO;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.vehicle.web.VehicleCommand;

import java.util.*;

public class SectionController extends BaseController {

    SectionCommand sectionCommand;
    VehicleCommand vehicleCommand;
    RackBP rackBP;
    LoginBP loginBP;
    VehicleBP vehicleBP;
    SectionBP sectionBP;
    PurchaseBP purchaseBP;

    public PurchaseBP getPurchaseBP() {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP) {
        this.purchaseBP = purchaseBP;
    }


    public SectionBP getSectionBP() {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP) {
        this.sectionBP = sectionBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public RackBP getRackBP() {
        return rackBP;
    }

    public void setRackBP(RackBP rackBP) {
        this.rackBP = rackBP;
    }

    public SectionCommand getSectionCommand() {
        return sectionCommand;
    }

    public void setSectionCommand(SectionCommand sectionCommand) {
        this.sectionCommand = sectionCommand;
    }

    public VehicleCommand getVehicleCommand() {
        return vehicleCommand;
    }

    public void setVehicleCommand(VehicleCommand vehicleCommand) {
        this.vehicleCommand = vehicleCommand;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);
    }

    //handlemanageSectionPage
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handlemanageSectionPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "STORES >>  Manage Section";
        path = "content/section/manageSection.jsp";
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Section-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                SectionTO sectionTO  =  new SectionTO();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Section";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);
                ArrayList serviceTypeList = new ArrayList();
                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute("serviceTypeList", serviceTypeList);

//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleaddSectionPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        sectionCommand = command;
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "STORES >> Section >>  Add Section";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SectionTO sectionTO = new SectionTO();
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Section-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                String pageTitle = "Add Section";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/section/addSection.jsp";
                ArrayList serviceTypeList = new ArrayList();
                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute("serviceTypeList", serviceTypeList);

//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //handleAddSection

    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddSection(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        sectionCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "STORES >> Section >>  Add Section";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            SectionTO sectionTO = new SectionTO();

            if (sectionCommand.getSectionName() != null && sectionCommand.getSectionName() != "") {
                sectionTO.setSectionName(sectionCommand.getSectionName());
            }
            if (sectionCommand.getSectionCode() != null && sectionCommand.getSectionCode() != "") {
                sectionTO.setSectionCode(sectionCommand.getSectionCode());
            }
            if (sectionCommand.getDescription() != null && sectionCommand.getDescription() != "") {
                sectionTO.setDescription(sectionCommand.getDescription());
            }
            if (sectionCommand.getServiceTypeId() != null && sectionCommand.getServiceTypeId() != "") {
                sectionTO.setServiceTypeId(sectionCommand.getServiceTypeId());
            }

            String pageTitle = "Add Section";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/section/manageSection.jsp";

            insertStatus = sectionBP.processInsertSectionDetails(sectionTO, userId);
            request.removeAttribute("SectionList");
            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            ArrayList serviceTypeList = new ArrayList();
            serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
            request.setAttribute("serviceTypeList", serviceTypeList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Section  Added Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //handleAlterSectionPage

    /**
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterSectionPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        path = "content/section/alterSection.jsp";

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        SectionTO sectionTO = new SectionTO();
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                sectionCommand = command;
                String menuPath = "STORES >> Section >>  Alter Section";
                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                ArrayList serviceTypeList = new ArrayList();
                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Alter  Section";
                request.setAttribute("SectionList", sectionList);
                request.setAttribute("serviceTypeList", serviceTypeList);
                request.setAttribute("pageTitle", pageTitle);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterSection(HttpServletRequest request, HttpServletResponse reponse, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        sectionCommand = command;
        String path = "";

        try {
            String menuPath = "STORES >> Section >>  Alter Section";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-Modify")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            int index = 0;
            int modify = 0;
            String[] sectionids = sectionCommand.getSectionIds();
            String[] sectionnames = sectionCommand.getSectionNames();
            String[] sectionCodes = sectionCommand.getSectionCodes();
            String[] activeStatus = sectionCommand.getActiveInds();
            String[] selectedIndex = sectionCommand.getSelectedIndex();
            String[] description = sectionCommand.getDescriptions();
            String[] serviceTypeId = sectionCommand.getServiceTypeIds();
            int UserId = (Integer) session.getAttribute("userId");
          

            ArrayList List = new ArrayList();
            SectionTO sectionTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                sectionTO = new SectionTO();
                index = Integer.parseInt(selectedIndex[i]);
              
                sectionTO.setSectionId(sectionids[index]);
                sectionTO.setSectionName(sectionnames[index]);
                sectionTO.setSectionCode(sectionCodes[index]);
                sectionTO.setActiveInd(activeStatus[index]);
                sectionTO.setDescription(description[index]);
                sectionTO.setServiceTypeId(serviceTypeId[index]);
                List.add(sectionTO);
            }
            String pageTitle = "View Manufacturers";
            path = "content/section/manageSection.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = sectionBP.processModifySectionDetails(List, UserId);
            request.removeAttribute("SectionList");
            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            ArrayList serviceTypeList = new ArrayList();
            serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
            request.setAttribute("serviceTypeList", serviceTypeList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Manufacturer Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddPartsPage 
    /**
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddPartsPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        sectionCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Stores  >> Manage Parts >>  Add";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String pageTitle = "Add Parts";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/parts/addParts.jsp";

        try {
            
//             setLocale(request, response);

//            if (!loginBP.checkAuthorisation(userFunctions, "Parts-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                ArrayList MfrList = new ArrayList();
                ArrayList uomList = new ArrayList();
                ArrayList getRackList = new ArrayList();
                ArrayList sectionList = new ArrayList();
                ArrayList vatList = new ArrayList();
                ArrayList groupList = new ArrayList();
                ArrayList venList = new ArrayList();

                MfrList = vehicleBP.processGetMfrList();
                sectionList = sectionBP.processGetSectionList();
                uomList = sectionBP.processGetUomList();
                getRackList = rackBP.processRackList();
                vatList = purchaseBP.processVatList();
                groupList = sectionBP.processGetGroupList();
                venList = sectionBP.processGetVendorList();

                ArrayList categoryList = new ArrayList();
                categoryList = sectionBP.processGetCategoryList();
                request.setAttribute("CategoryList", categoryList);
             

                request.setAttribute("MfrList", MfrList);
                request.setAttribute("vatList", vatList);
                request.setAttribute("groupList", groupList);
                request.setAttribute("SectionList", sectionList);
                request.setAttribute("UomList", uomList);
                request.setAttribute("getRackList", getRackList);
                request.setAttribute("venList", venList);

                path = "content/parts/addParts.jsp";


//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to display PartsAdd Page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddParts
    /**
     * This method used to Insert Parts Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddParts(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        ModelAndView mv = null;
        sectionCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
           
            String menuPath = "Stores  >> Manage Parts >>  Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            SectionTO sectionTO = new SectionTO();


            if (sectionCommand.getSectionId() != null && sectionCommand.getSectionId() != "") {
                sectionTO.setSectionId(sectionCommand.getSectionId());
            }
            if (sectionCommand.getMfrId() != null && sectionCommand.getMfrId() != "") {
                sectionTO.setMfrId(sectionCommand.getMfrId());
            }
            if (sectionCommand.getModelId() != null && sectionCommand.getModelId() != "") {
                sectionTO.setModelId(sectionCommand.getModelId());

            }
            if (sectionCommand.getItemCode() != null && sectionCommand.getItemCode() != "") {
                sectionTO.setItemCode(sectionCommand.getItemCode());
            }
            if (sectionCommand.getPaplCode() != null && sectionCommand.getPaplCode() != "") {
                sectionTO.setPaplCode(sectionCommand.getPaplCode());
            }
            if (sectionCommand.getItemName() != null && sectionCommand.getItemName() != "") {
                sectionTO.setItemName(sectionCommand.getItemName());
            }
            if (sectionCommand.getUomId() != null && sectionCommand.getUomId() != "") {
                sectionTO.setUomId(sectionCommand.getUomId());
            }
            if (sectionCommand.getSpecification() != null && sectionCommand.getSpecification() != "") {
                sectionTO.setSpecification(sectionCommand.getSpecification());
            }
            if (sectionCommand.getReConditionable() != null && sectionCommand.getReConditionable() != "") {
                sectionTO.setReConditionable(sectionCommand.getReConditionable());
            }
            if (sectionCommand.getScrapUomId() != null && sectionCommand.getScrapUomId() != "") {
                sectionTO.setScrapUomId(sectionCommand.getScrapUomId());
            }
            if (sectionCommand.getMaxQuandity() != null && sectionCommand.getMaxQuandity() != "") {
                sectionTO.setMaxQuandity(sectionCommand.getMaxQuandity());
            }
            if (sectionCommand.getRoLevel() != null && sectionCommand.getRoLevel() != "") {
                sectionTO.setRoLevel(sectionCommand.getRoLevel());
            }
            if (sectionCommand.getSubRackId() != null && sectionCommand.getSubRackId() != "") {
                sectionTO.setSubRackId(sectionCommand.getSubRackId());
            }
            if (sectionCommand.getCategoryId() != null && sectionCommand.getCategoryId() != "") {
                sectionTO.setCategoryId(sectionCommand.getCategoryId());
            }
            if (sectionCommand.getVatId() != null && sectionCommand.getVatId() != "") {
                sectionTO.setVatId(sectionCommand.getVatId());
            }
            if (sectionCommand.getGroupId() != null && sectionCommand.getGroupId() != "") {
                sectionTO.setGroupId(sectionCommand.getGroupId());
            }            
            sectionTO.setSellingPrice(request.getParameter("sprice"));
            sectionTO.setMrp(request.getParameter("mrp"));
            String pageTitle = "Add Parts";
            request.setAttribute("pageTitle", pageTitle);
            int insertStatus = 0;

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            path = "content/parts/manageParts.jsp";

            sectionBP.processInsertPartsDetails(sectionTO, userId);
            request.removeAttribute("PartsList");

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);
            mv = handleManagePartsPage(request,response,command);
            //  ArrayList partsList = new ArrayList();
            //   partsList = sectionBP.getpartsDetails(sectionTO);
            //  request.setAttribute("PartsList", partsList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Item Added Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    //processGetModels
    public void handleGetModels(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
      
        sectionCommand = command;
      
        String mfrId = request.getParameter("mfrId");
        String suggestions = "";

        try {
            //Hari
            if(mfrId!=null && !"".equals(mfrId))
            {
            suggestions = sectionBP.processGetModels(Integer.parseInt(mfrId));
            }
            //Hari End
            PrintWriter writer = response.getWriter();
          
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    //handleGetSubRack
    public void handleGetSubRack(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
       

        sectionCommand = command;

        String rackId = request.getParameter("rackId");
     
        String suggestions = "";
        try {
         
            suggestions = rackBP.processGetSubRack(Integer.parseInt(rackId));
            if (rackId != null && rackId != "") {
            }

            PrintWriter writer = response.getWriter();
          
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  SubRack data in Ajax --> " + exception);
        }
    }

    /**  == 99 1-st screen for parts
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleManagePartsPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        sectionCommand = command;
        String path = "";
        String menuPath = "Stores  >> Manage Parts >>  Manage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Search Parts";
        request.setAttribute("pageTitle", pageTitle);
        HttpSession session = request.getSession();
         int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
                SectionTO sectionTO = new SectionTO();
        try {
           //  setLocale(request, response);
            
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Parts-Search")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/parts/manageParts.jsp";

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            request.removeAttribute("PartsList");

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);
         
            ArrayList groupList = new ArrayList();
            groupList = sectionBP.processGetGroupListForParts();
            request.setAttribute("groupList", groupList);
/*
            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;
            int totalRecords = 0;
            PaginationHelper pagenation = new PaginationHelper();
            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            if (request.getParameter("button") != null) {
                buttonClicked = request.getParameter("button");
            }
            pagenation.setTotalRecords(totalRecords);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);
            */
             ArrayList recordsWithLimit = new ArrayList();
             recordsWithLimit = sectionBP.getpartsDetails(sectionTO,companyId);
                request.setAttribute("IndexedPartsDetail", recordsWithLimit);


//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }
    //handleSearch

    /**
     * This method caters to search Models
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleSearch(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Stores  >>  Manage Parts  >>  view";
        sectionCommand = command;
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String pageTitle = "View Item";
        request.setAttribute("pageTitle", pageTitle);
        String mfrId = request.getParameter("mfrId");
        try {
           //  setLocale(request, response);

//            if (!loginBP.checkAuthorisation(userFunctions, "Parts-Search")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/parts/manageParts.jsp";

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processGetMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList categoryList = new ArrayList();
                categoryList = sectionBP.processGetCategoryList();
                request.setAttribute("CategoryList", categoryList);

                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);

                ArrayList ModelList = new ArrayList();
                ModelList = vehicleBP.processActiveMfrList();
                request.setAttribute("ModelList", ModelList);

                ArrayList groupList = new ArrayList();
                groupList = sectionBP.processGetGroupList();
                request.setAttribute("groupList", groupList);



                SectionTO sectionTO = new SectionTO();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                ArrayList partsList = new ArrayList();

                String mrfId = sectionCommand.getMfrId();
                String modelId = sectionCommand.getModelId();                
                String itemCode = sectionCommand.getItemCode();
                String paplCode = sectionCommand.getPaplCode();
                String itemName = sectionCommand.getItemName();
                String reConditionable = sectionCommand.getReConditionable();
                String categoryId = sectionCommand.getCategoryId();
                String groupId = sectionCommand.getGroupId();
                
                System.out.println("sectionCommand.getItemCode()"+sectionCommand.getItemCode());
                System.out.println("sectionCommand.getPaplCode()"+sectionCommand.getPaplCode());
                
                if (sectionCommand.getMfrId().equals("0")) {
                    sectionTO.setMfrId("");
                } else {
                    sectionTO.setMfrId(mrfId);
                }
                if (sectionCommand.getModelId().equals("0")) {

                    sectionTO.setModelId("");
                } else {

                    sectionTO.setModelId(modelId);
                }

                if (sectionCommand.getItemCode() =="") {
                    sectionTO.setItemCode("");
                } else {
                    sectionTO.setItemCode(itemCode+"%");
                }
                if (sectionCommand.getPaplCode() == "") {

                    sectionTO.setPaplCode("");
                } else {

                    sectionTO.setPaplCode(paplCode);
                }
                if (sectionCommand.getItemName() == "") {

                    sectionTO.setItemName(null);
                } else {

                    sectionTO.setItemName(itemName);
                }
                if (sectionCommand.getReConditionable() == "") {

                    sectionTO.setReConditionable("");
                } else {

                    sectionTO.setReConditionable(reConditionable);
                }
                if (sectionCommand.getCategoryId().equals("0")) {
                    sectionTO.setCategoryId("");
                } else {
                    sectionTO.setCategoryId(categoryId);
                }

                if (sectionCommand.getGroupId().equals("0")) {
                    sectionTO.setGroupId("");
                } else {
                    sectionTO.setGroupId(groupId);
                }
                sectionTO.setSectionId(null);    

                System.out.println("sectionCommand.getPaplCode()"+sectionTO.getPaplCode());
                System.out.println("sectionCommand.getItemCode()"+sectionTO.getItemCode());
                System.out.println("sectionCommand.getGroupId()"+sectionCommand.getGroupId());
                
                request.setAttribute("sectionId", sectionCommand.getSectionId());
                request.setAttribute("itemCode", sectionCommand.getItemCode());
                request.setAttribute("paplCode", sectionCommand.getPaplCode());
                request.setAttribute("itemName", sectionCommand.getItemName());
                request.setAttribute("mfrId", sectionCommand.getMfrId());
                request.setAttribute("modelId", sectionCommand.getModelId());
                request.setAttribute("reConditionable", sectionCommand.getReConditionable());
                request.setAttribute("categoryId", sectionCommand.getCategoryId());
                request.setAttribute("groupId", sectionCommand.getGroupId());

               
                ArrayList recordsWithLimit = new ArrayList();
/*
                //pagenation start
//variables
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
//to get totalRecords
                totalRecords = sectionBP.getTotalParts(sectionTO);    //call your method
                session.setAttribute("totalRecords", totalRecords);
                PaginationHelper pagenation = new PaginationHelper();
                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                if (request.getParameter("button") != null) {
                    buttonClicked = request.getParameter("button");
                }
              
                pageNo = Integer.parseInt(request.getParameter("pageNo"));

                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                int startIndex = pagenation.getStartIndex();
                int endIndex = pagenation.getEndIndex();
                */

                //to get  records with limits
                //  partsList = sectionBP.getpartsDetails(sectionTO);
                //  request.setAttribute("PartsList", partsList);
                recordsWithLimit = sectionBP.getpartsDetails(sectionTO,companyId);//call your method                  
                if(recordsWithLimit.size() !=0){                    
                    request.setAttribute("IndexedPartsDetail", recordsWithLimit);
                }else{                    
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY,"No Records Found");
                }
//                request.setAttribute("totalPages", totalPages);
//                request.setAttribute("pageNo", pageNo);
            //pagenation end 



//            }


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    /**
     * This method caters to alter Model Page
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleAlterPartsPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        sectionCommand = command;
        VehicleTO vehicleTO= new VehicleTO();
        String menuPath = "";
        menuPath = "Stores  >> Manage Parts >>  Alter Parts";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        // String mfrId = request.getParameter("mfrId");
        String suggestions = "";
        try {
           //  setLocale(request, response);

//            if (!loginBP.checkAuthorisation(userFunctions, "Parts-Alter")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

                String pageTitle = "View Parts Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList partsDetail = new ArrayList();
                int itemId = Integer.parseInt(request.getParameter("itemId"));
                System.out.println("itemId"+itemId);
                path = "content/parts/alterParts.jsp";
                //request.removeAttribute("GetPartsDetail");
                ArrayList getPartsDetail = new ArrayList();
                ArrayList MfrList = new ArrayList();
                ArrayList uomList = new ArrayList();
                ArrayList getRackList = new ArrayList();
                ArrayList sectionList = new ArrayList();
                ArrayList ModelList = new ArrayList();
                ArrayList categoryList = new ArrayList();
                ArrayList vatList = new ArrayList();
                ArrayList groupList = new ArrayList();
                ArrayList venList = new ArrayList();

                vatList = purchaseBP.processVatList();
                groupList = sectionBP.processGetGroupList();
                categoryList = sectionBP.processGetCategoryList();
                request.setAttribute("CategoryList", categoryList);
            


                ModelList = vehicleBP.processGetModelList(vehicleTO);
                request.setAttribute("ModelList", ModelList);
                System.out.println("ModelLIst Size...."+ModelList.size());
                ArrayList subRackList = new ArrayList();
                subRackList = rackBP.processSubRackList();
                request.setAttribute("SubRackList", subRackList);

                MfrList = vehicleBP.processGetMfrList();
                sectionList = sectionBP.processGetSectionList();
                uomList = sectionBP.processGetUomList();
                getRackList = rackBP.processRackList();
                getPartsDetail = sectionBP.processGetPartsDetail(itemId);
                venList = sectionBP.processGetVendorList();

                request.setAttribute("MfrList", MfrList);
                request.setAttribute("vatList", vatList);
                request.setAttribute("groupList", groupList);
                request.setAttribute("SectionList", sectionList);
                request.setAttribute("UomList", uomList);
                request.setAttribute("getRackList", getRackList);
                request.setAttribute("GetPartsDetail", getPartsDetail);
                request.setAttribute("venList", venList);
            //  String itemCode = sectionCommand.getItemCode();
            // request.setAttribute(itemCode, itemCode);   
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to display alter page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterParts 
    /**
     * This method used to alter Parts  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterParts(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }        
        sectionCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ModelAndView mv=null;
        try {
           //  setLocale(request, response);

            String pageTitle = "Alter Parts";
            request.setAttribute("pageTitle", pageTitle);

            String menuPath = "Stores  >> Manage Parts >>  Alter Parts";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            SectionTO sectionTO = new SectionTO();


            if (sectionCommand.getCategoryId() != null && sectionCommand.getCategoryId() != "") {
                sectionTO.setCategoryId(sectionCommand.getCategoryId());

            }
            if (sectionCommand.getRackId() != null && sectionCommand.getRackId() != "") {
                sectionTO.setRackId(sectionCommand.getRackId());

            }
            if (sectionCommand.getMfrId() != null && sectionCommand.getMfrId() != "") {
                sectionTO.setMfrId(sectionCommand.getMfrId());

            }
            if (sectionCommand.getModelId() != null && sectionCommand.getModelId() != "") {
                sectionTO.setModelId(sectionCommand.getModelId());

            }
            if (sectionCommand.getMfrCode() != null && sectionCommand.getMfrCode() != "") {
                sectionTO.setMfrCode(sectionCommand.getMfrCode());

            }
            if (sectionCommand.getPaplCode() != null && sectionCommand.getPaplCode() != "") {
                sectionTO.setPaplCode(sectionCommand.getPaplCode());

            }
            if (sectionCommand.getItemName() != null && sectionCommand.getItemName() != "") {
                sectionTO.setItemName(sectionCommand.getItemName());

            }
            if (sectionCommand.getUomId() != null && sectionCommand.getUomId() != "") {
                sectionTO.setUomId(sectionCommand.getUomId());

            }
            if (sectionCommand.getSpecification() != null && sectionCommand.getSpecification() != "") {
                sectionTO.setSpecification(sectionCommand.getSpecification());

            }
            if (sectionCommand.getReConditionable() != null && sectionCommand.getReConditionable() != "") {
                sectionTO.setReConditionable(sectionCommand.getReConditionable());

            }
            if (sectionCommand.getScrapUomId() != null && sectionCommand.getScrapUomId() != "") {
                sectionTO.setScrapUomId(sectionCommand.getScrapUomId());

            }
            if (sectionCommand.getMaxQuandity() != null && sectionCommand.getMaxQuandity() != "") {
                sectionTO.setMaxQuandity(sectionCommand.getMaxQuandity());

            }
            if (sectionCommand.getRoLevel() != null && sectionCommand.getRoLevel() != "") {
                sectionTO.setRoLevel(sectionCommand.getRoLevel());

            }
            if (sectionCommand.getSubRackId() != null && sectionCommand.getSubRackId() != "") {
                sectionTO.setSubRackId(sectionCommand.getSubRackId());

            }
            if (sectionCommand.getItemId() != null && sectionCommand.getItemId() != "") {
                sectionTO.setItemId(sectionCommand.getItemId());

            }
            if(sectionCommand.getVatId()!=null && sectionCommand.getVatId()!="")
            {             
                sectionTO.setVatId(sectionCommand.getVatId());
            }           
            if(sectionCommand.getGroupId()!=null && sectionCommand.getGroupId()!="")
            {
                System.out.println("Group Id-->"+sectionCommand.getGroupId());
                sectionTO.setGroupId(sectionCommand.getGroupId());
            }
            sectionTO.setSellingPrice(request.getParameter("sprice"));
            sectionTO.setMrp(request.getParameter("mrp"));

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList categoryList = new ArrayList();
            categoryList = sectionBP.processGetCategoryList();
            request.setAttribute("CategoryList", categoryList);
        
            path = "content/parts/manageParts.jsp";
            int insertStatus = 0;
            insertStatus = sectionBP.processUpdatePartsDetails(sectionTO, userId);
            //request.removeAttribute("PartsList");
            mv=handleManagePartsPage(request,response,command);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Parts Altered Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }
    
    public void handleItemSuggestions(HttpServletRequest request, HttpServletResponse response ) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String itemName = request.getParameter("itemName");
        System.out.println("itemName" + itemName);
        String mfrId = request.getParameter("mfrId");
        System.out.println("mfrId" + mfrId);
        String modelId = request.getParameter("modelId");
        System.out.println("modelId" + modelId);
        String suggestions = sectionBP.processItemSuggests(itemName,mfrId,modelId );
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }    
    public void handleItemSuggestionsNew(HttpServletRequest request, HttpServletResponse response ) throws IOException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String itemName = request.getParameter("itemName");
        System.out.println("itemName" + itemName);
        String mfrId = request.getParameter("mfrId");
        System.out.println("mfrId" + mfrId);
        String modelId = request.getParameter("modelId");
        System.out.println("modelId" + modelId);
        String suggestions = sectionBP.processItemSuggests(itemName,mfrId,modelId );


        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
//        response.setContentType("text/xml");
//        response.setHeader("Cache-Control", "no-cache");
//        writer.println(suggestions);
//        writer.close();

        JSONArray jsonArray = new JSONArray();
            String[] temp = suggestions.split("~");
            for(int j=0; j<temp.length;j++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("itemName", temp[j]);
                //////System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            //////System.out.println("jsonArray = " + jsonArray);

            writer.print(jsonArray);


    }
    
    public void handleCheckItem(HttpServletRequest request, HttpServletResponse response ) throws IOException {
        System.out.println("i am in handleCheckItem ajax ");
        HttpSession session = request.getSession();
        String choice = request.getParameter("choice");
        String param = request.getParameter("param");
        
        String suggestions = sectionBP.processCheckItemExists(choice,param );        
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }


 //Hari Start



 public ModelAndView handleGroupPartsPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        path = "content/group/manageGroup.jsp";

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        try {
           //  setLocale(request, response);

//            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                sectionCommand = command;
                String menuPath = "STORES >> Section >>  Manage Group";
                ArrayList groupList = new ArrayList();
                groupList = sectionBP.processGetGroupList();
                if(groupList.size()==0)
                {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Group List Not Found");
                }

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Manage Group";
                request.setAttribute("groupList", groupList);
                request.setAttribute("pageTitle", pageTitle);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve GroupList data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

 //handleAddGroupPage

 public ModelAndView handleAddGroupPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Stores >>Manage Group  >> Add Group ";
        String pageTitle = "Add Group";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
           //  setLocale(request, response);

             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Rack-Add")) {
//            path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/group/addGroup.jsp";
//        }
        }catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Add Rack --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleAddGroup(HttpServletRequest request, HttpServletResponse reponse, SectionCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        ArrayList groupList = new ArrayList();
        sectionCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        SectionTO sectionTO = new SectionTO();
        String menuPath = "";
        menuPath = "Stores  >> Manage Group ";
        String pageTitle = "Manage Rack";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (sectionCommand.getGroupName() != null && sectionCommand.getGroupName() != "") {
                sectionTO.setGroupName(sectionCommand.getGroupName());
            }
            if (sectionCommand.getDescription() != null && sectionCommand.getDescription() != "") {
                sectionTO.setDescription(sectionCommand.getDescription());
            }
            path = "content/group/manageGroup.jsp";
            status = sectionBP.processInsertGroup(sectionTO, userId);
            groupList = sectionBP.processGetGroupList();
            request.setAttribute("groupList", groupList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Group Added Successfully");
            }
            else
            {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "New Group Added Failed");
            }


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }catch (FPBusinessException exception) {

        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Rack Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//handleAlterGroupPage

        public ModelAndView handleAlterGroupPage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        sectionCommand = command;
        String menuPath = "";
        menuPath = "Stores >> Manage Group  >> Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
           //  setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//             if (!loginBP.checkAuthorisation(userFunctions, "Rack-Alter")) {
//            path = "content/common/NotAuthorized.jsp";
//            } else {
            String pageTitle = "Alter Group Details";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList groupList = new ArrayList();
            path = "content/group/alterGroup.jsp";
            groupList = sectionBP.processGetGroupList();
            request.setAttribute("groupList", groupList);

//        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Alter rackList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleAlterGroup(HttpServletRequest request, HttpServletResponse reponse, SectionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        sectionCommand = command;
        String path = "";
        try {
            String menuPath = "Stores >>Manage Group";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int update = 0;
            String[] idList = sectionCommand.getGroupIdList();
            String[] nameList = sectionCommand.getGroupNameList();
            String[] descriptionList = sectionCommand.getGroupDescriptionList();
            String[] statusList = sectionCommand.getGroupStatusList();
            String[] selectedIndex = sectionCommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");
            ArrayList List = new ArrayList();
            SectionTO sectionTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                sectionTO = new SectionTO();
                index = Integer.parseInt(selectedIndex[i]);
                System.out.println("Index is"+index);
                sectionTO.setGroupId((idList[index]));
                System.out.println("GetGroupId"+sectionTO.getGroupId());
                sectionTO.setGroupName(nameList[index]);
                sectionTO.setDescription(descriptionList[index]);
                sectionTO.setActiveInd(statusList[index]);
                List.add(sectionTO);
            }
            path = "content/group/manageGroup.jsp";
            update = sectionBP.processUpdateGroup(List, userId);
            ArrayList groupList = new ArrayList();
            groupList = sectionBP.processGetGroupList();
            request.setAttribute("groupList", groupList);
            String pageTitle = "Manage Group";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Group Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleManageServicePage(HttpServletRequest request, HttpServletResponse reponse, SectionCommand command) throws IOException {
        System.out.println("ServiceTypeMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        sectionCommand = command;
        String path = "";
        //        int userId = 0;
        //                (Integer) session.getAttribute("userId");
        //        int cityMasterList = 0;
        SectionTO sectionTO = new SectionTO();
        String menuPath = "";
        menuPath = "Service Plan  >> Service Type Master ";
        String pageTitle = "ServiceTypeMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList serviceTypeList = new ArrayList();
            serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
            request.setAttribute("serviceTypeList", serviceTypeList);
            path = "content/RenderService/serviceType.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

 public void checkServiceTypeName(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        sectionCommand = command;
        String serviceTypeName = request.getParameter("serviceTypeName");
        String suggestions = "";
        try {
            suggestions = sectionBP.checkServiceTypeName(serviceTypeName);
            
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  SubRack data in Ajax --> " + exception);
        }
    }

public ModelAndView saveServiceTypeMaster(HttpServletRequest request, HttpServletResponse reponse, SectionCommand command) throws IOException {

        System.out.println("City Master.........");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        sectionCommand = command;
        String path = "";
        int userId = 0;
            userId = (Integer) session.getAttribute("userId");
        int insertServiceType = 0;
        int updateStatus = 0;
        //        int cityMasterList=0;
        SectionTO sectionTO = new SectionTO();
        String menuPath = "";
        menuPath = "Operation  >> City Master ";
        String pageTitle = "Save City Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList serviceTypeList = new ArrayList();

        try {
            if (sectionCommand.getServiceTypeName()!= null && !"".equals(sectionCommand.getServiceTypeName())) {
                sectionTO.setServiceTypeName(sectionCommand.getServiceTypeName());
            }
            if (sectionCommand.getStatus() != null && !"".equals(sectionCommand.getStatus())) {
                sectionTO.setStatus(sectionCommand.getStatus());
            }
            if (sectionCommand.getServiceTypeDesc() != null && !"".equals(sectionCommand.getServiceTypeDesc())) {
                sectionTO.setDesc(sectionCommand.getServiceTypeDesc());
            }
            if (sectionCommand.getServiceTypeId()!= null && !"".equals(sectionCommand.getServiceTypeId())) {
                sectionTO.setServiceTypeId(sectionCommand.getServiceTypeId());
                updateStatus = sectionBP.updateServiceType(sectionTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated SucessFully");
                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute("serviceTypeList", serviceTypeList);
                path = "content/RenderService/serviceType.jsp";
            }

            String checkServiceTypeName = "";
            checkServiceTypeName = sectionBP.checkServiceTypeName(sectionTO.getServiceTypeName());
            if (checkServiceTypeName == null) {
                insertServiceType = sectionBP.insertServiceType(sectionTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted SucessFully");
                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute("serviceTypeList", serviceTypeList);
                path = "content/RenderService/serviceType.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Updated SucessFully");
            }

            if (insertServiceType != 0 || updateStatus != 0) {

                serviceTypeList = sectionBP.getServiceTypeMaster(sectionTO);
                request.setAttribute("serviceTypeList", serviceTypeList);
                path = "content/RenderService/serviceType.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
public void handleGetModels1(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
       System.out.println("entered in  ajaxmodel controller");
        sectionCommand = command;

        String mfrId = request.getParameter("mfrId");
        String fleetTypeId = request.getParameter("fleetTypeId");
        String typeId = request.getParameter("typeId");
        String suggestions = "";

        try {
            //Hari
            if(mfrId!=null && !"".equals(mfrId))
            {
            System.out.println("mfrId"+mfrId);
            suggestions = sectionBP.processGetModels1(Integer.parseInt(mfrId),fleetTypeId,typeId);
            }
            //Hari End
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }



    public void handleGetModels2(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        System.out.println("entered in  ajax controller");
        sectionCommand = command;
      
        String typeId = request.getParameter("typeId");
        String suggestions = "";
        System.out.println("typeId"+typeId);
        try {
            //Hari
            if(typeId!=null && !"".equals(typeId))
            {
            suggestions = sectionBP.processGetModels2(Integer.parseInt(typeId));
            }
            //Hari End
            PrintWriter writer = response.getWriter();
          
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
    public void getTonnage(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
        System.out.println("entered in  ton Ajax controller");
        sectionCommand = command;
        String typeId = request.getParameter("typeId");
        String fleetTypeId = request.getParameter("fleetTypeId");
        System.out.println("fleetTypeId"+fleetTypeId);
        String suggestions = "";
        System.out.println("typeId"+typeId);
        try {
            //Hari
            if(typeId!=null && !"".equals(typeId)){
               if(fleetTypeId.equals("1")){
               suggestions = sectionBP.getTonnage(Integer.parseInt(typeId));
               }if(fleetTypeId.equals("2")){
               suggestions = sectionBP.getTrailerTypeTonnage(Integer.parseInt(typeId));
               }
            }
            //Hari End
            PrintWriter writer = response.getWriter();
          
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
    
   public void handleGetModelsForVendor(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
       System.out.println("entered in  ajaxVendor controller");
        sectionCommand = command;
      
//        String mfr= request.getParameter("mfr");
        String typeId = request.getParameter("typeId");
        System.out.println("typeId = " + typeId);
//        System.out.println("mfr = " + mfr);
        String suggestions = "";

        try {
            //Hari
            if( typeId!=null && !"".equals(typeId))
            {
            suggestions = sectionBP.processGetModelsForVendor(Integer.parseInt(typeId));
                System.out.println("suggestions model tyypeid= " + suggestions);
            }
            //Hari End
            PrintWriter writer = response.getWriter();
          
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
   
    public ModelAndView handleUploadParts(HttpServletRequest request, HttpServletResponse response, SectionCommand command) {
         System.out.println("i m in uplaod method");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        SectionTO empTO = new SectionTO();
        String pageTitle = "Upload Employee Details";
        menuPath = "Employee >> Upload Employe Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList employeeHistoryDetails = new ArrayList();
        String category = "", make = "", model = "", oemCode = "", companyCode = "", itemName = "";
        String uom = "", itemSpecification = "", isRecondition = "", scrapUnit = "", maxQty = "", reOrderLevel = "", rack = "";
        String subrack = "", stock = "", price = "", contractDriver = "", drivingLicenseNo = "", licenseDate = "", licenseType = "";
        String licenseState = "", addr = "", city = "", state = "", pincode = "", addr1 = "", city1 = "";
        String state1 = "", pincode1 = "", salaryType = "", basicSalary = "", esiEligible = "", pfEligible = "", bankAccountNo = "", nomineeName = "";
        String userName = "", password = "", roleName = "", userAccess = "", bankName = "", bankBranchName = "";
        int status = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
           // employeecommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
          System.out.println("isMultipart:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                       System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                     System.out.println("uploadedFileName:"+uploadedFileName);
                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            //////System.out.println("tempPath..." + tempFilePath);
                            //////System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            //////System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            //////System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            //////System.out.println("tempPath = " + tempFilePath);
                            //////System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            //////System.out.println("rows" + userId + s.getRows());

                            for (int i = 1; i < s.getRows(); i++) {

                                SectionTO sectionTO = new SectionTO();

                                category = s.getCell(1, i).getContents();
                                sectionTO.setCategoryName(category);
                                 System.out.println("category:"+category);
                                make = s.getCell(2, i).getContents();
                                sectionTO.setMfr(make);

                                model = s.getCell(3, i).getContents();
                                sectionTO.setModelName(model);

                                oemCode = s.getCell(4, i).getContents();
                                sectionTO.setItemCode(oemCode);

                                companyCode = s.getCell(5, i).getContents();
                                sectionTO.setPaplCode(companyCode);

                                itemName = s.getCell(6, i).getContents();
                                sectionTO.setItemName(itemName);
                                 System.out.println("itemName:"+itemName);
                                uom = s.getCell(7, i).getContents();
                                sectionTO.setUom(uom);

                                itemSpecification = s.getCell(8, i).getContents();
                                sectionTO.setDescription(itemSpecification);

                                isRecondition = s.getCell(9, i).getContents();
                                sectionTO.setReConditionable(isRecondition);

                                scrapUnit = s.getCell(10, i).getContents();
                                sectionTO.setScrapUomName(scrapUnit);

                                reOrderLevel = s.getCell(11, i).getContents();
                                sectionTO.setRoLevel(reOrderLevel);

                                maxQty = s.getCell(12, i).getContents();
                                sectionTO.setMaxQuandity(maxQty);

                                rack = s.getCell(13, i).getContents();
                                sectionTO.setRackName(rack);

                                subrack = s.getCell(14, i).getContents();
                                sectionTO.setSubRackName(subrack);

                                stock = s.getCell(15, i).getContents();
                                sectionTO.setStockLevel(stock);
                                
                                 price = s.getCell(17, i).getContents();
                                sectionTO.setPrice(price);
                                
                              String ids=sectionBP.getUploadIds(sectionTO, userId);
                              String temp[]=ids.split("~");
                              sectionTO.setCategoryId(temp[0]);
                              sectionTO.setMfrId(temp[1]);
                              sectionTO.setModelId(temp[2]);
                              sectionTO.setUomId(temp[3]);
                              sectionTO.setScrapUomId(temp[4]);
                              sectionTO.setSectionId("0");
                              sectionTO.setSubRackId(temp[5]);
                              sectionTO.setGroupId("0");
                              sectionTO.setVatId("0");
                              sectionTO.setSellingPrice("0");
                              sectionTO.setMrp("0");
                              sectionTO.setTypeId("ExcelUpload");
                                 sectionBP.processInsertPartsDetails(sectionTO, userId);

//                                userAccess = s.getCell(38, i).getContents();
//                                employeeTO.setUserAccess(userAccess);
//
//                                userName = s.getCell(39, i).getContents();
//                                employeeTO.setUserName(userName);
//
//                                password = s.getCell(40, i).getContents();
//                                employeeTO.setPassword(password);
//
//                                roleName = s.getCell(41, i).getContents();
//                                employeeTO.setRoleName(roleName);
                                String checkEmployeeDetails = "";
                                //////System.out.println("map.get = " + map.get("empName"));
                                String checkName = category+"~"+make+"~"+model+"~"+oemCode+"~"+companyCode+"~"+itemName+"~"+uom+"~"+itemSpecification+"~"+isRecondition+"~"+scrapUnit+"~"+reOrderLevel+"~"+maxQty+"~"+rack+"~"+subrack+"~"+stock;
                                //////System.out.println("hs = " +hs);

                                map.put("item",checkName);
                                Object mapStatus = map.get("status");
                                mapValue = map.get("item");
                                Object mapNameValue = map.get("name");
                                sectionTO.setItemCode(String.valueOf(mapValue));
                                
                                employeeHistoryDetails.add(sectionTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("employeeHistoryDetails", employeeHistoryDetails);

            path = "content/employee/employeeHistoryDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

   
   
   
   
}


  

 

