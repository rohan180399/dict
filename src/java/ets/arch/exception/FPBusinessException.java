/*----------------------------------------------------------------------------
 * FPBusinessException.java
 * Jan 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 ---------------------------------------------------------------------------*/
package ets.arch.exception;
/**
*
* Modification Log
* ---------------------------------------------------------------------
* Ver   Date              Modified By               Description
* ---------------------------------------------------------------------
* 1.0   26 Jan 2008       Srinivasan.R             Initial Version
*
***********************************************************************/

import java.util.HashMap;

import ets.arch.util.ErrorXMLUtil;
import ets.domain.util.FPLogUtils;
/**
 * FPBusinessException:All business exceptions of FP will instantiate an object of this
 * type and will throw it.
 * <br>This Class gets the details of the error thrown from exception xml.
 * @author Srinivasan.R
 * @version 1.0 26 Jan 2008
 * @since   Event Portal Iteration 0
 */

public class FPBusinessException extends Exception
{

   /*
   This will hold the error code
   */
   private String errorCode;

   /*
   This will hold the error message corresponding to the error code
   */
   private String errorMessage;

   /*
   This will hold the error cause corresponding to the error code
   */
   private String errorCause;

   /*
   This will hold the error severity corresponding to the error code
   */
   private String errorSeverity;

   /*
   This will hold the error recovery corresponding to the error code
   */
   private String errorRecovery;


  /**
   * Constructor method which will initialize all the values.
   */
   public FPBusinessException()
   {
   }

   /**
   * Constructor method which will set the value of the errorCode.
   * It will call methods to set the values of the other error attributes.
   * <br>This Class gets the details of the error thrown from xml holding
   * those values.
   * @param errorCode The Error Code representing the error that has occured.
   */
   public FPBusinessException(String errorCode)
   {
	   try {
	   HashMap errorTable = ErrorXMLUtil.getInstance().getExceptionMappings();
	   ErrorDefinition errorDefinition = (ErrorDefinition)errorTable.get(errorCode);
	   setErrorCode(errorCode);
	   setErrorMessage(errorDefinition.getErrorMessage());
	   setErrorCause(errorDefinition.getErrorCause());
	   setErrorRecovery(errorDefinition.getErrorRecovery());
	   setErrorSeverity(errorDefinition.getErrorSeverity());
	 }catch (Exception e){
		 FPLogUtils.fpDebugLog("excep in fpbusexcp:"+e.toString());
	 }

   }

   /**
   * Constructor method which will set the value of the errorCode
   * It will call methods to set the values of the other error attributes
   * It also appends the customizedErrorParameter with the Error Cause to present
   * a customized error message
   * <br>This Class gets the details of the error thrown from xml holding those values.
   * @param errorCode The Error Code representing the error that has occured.
   * @param customizedErrorParameter The input paramter for which no data exists
   */
   public FPBusinessException(String errorCode, String customizedErrorParameter)
   {

   HashMap errorTable = ErrorXMLUtil.getInstance().getExceptionMappings();
   ErrorDefinition errorDefinition = (ErrorDefinition)errorTable.get(errorCode);
   setErrorCode(errorCode);
   setErrorMessage(errorDefinition.getErrorMessage());
   setErrorCause("<![CDATA["+errorDefinition.getErrorCause()+" "+customizedErrorParameter+"]]>");
   setErrorRecovery(errorDefinition.getErrorRecovery());
   setErrorSeverity(errorDefinition.getErrorSeverity());

   }

   /**
   * @return errorCode -Returns the error code.
   */
   public String getErrorCode()
   {
      return errorCode;
   }

   /**
   * @return errorMessage -Returns the error message.
   */
   public String getErrorMessage()
   {
     return errorMessage;
   }

  /**
   * @return errorCause - the error cause.
   */
   public String getErrorCause()
   {
     return errorCause;
   }

  /**
   * @return errorRecovery -Returns the error recovery.
   */
   public String getErrorRecovery()
   {
     return errorRecovery;
   }

  /**
   * @return errorSeverity -Returns the error severity.
   */
   public String getErrorSeverity()
   {
     return errorSeverity;
   }


   // Sets the value of errorMessage field.

   private void setErrorMessage(String errorMessage)
   {
   this.errorMessage = errorMessage;
   }


   // Sets the value of errorCause field.

   private void setErrorCause(String errorCause)
   {
   this.errorCause = errorCause;
   }

   // Sets the value of errorRecovery field.

   private void setErrorRecovery(String errorRecovery)
   {
   this.errorRecovery = errorRecovery;
   }


   // Sets the value of errorSeverity field.

   private void setErrorSeverity(String errorSeverity)
   {
   this.errorSeverity = errorSeverity;
   }

   /**
   * Sets the value of errorCode attribute.
   * @param errorCode -Error Code
   */
   public void setErrorCode(String errorCode)
   {
      this.errorCode = errorCode;
   }
   /**
	 * @return errDetails
	 * author: Arivu_Selvaraj
	 */
	public String getErrorDetails()
	   {
		   String errDetails = "Error Code:"+this.getErrorCode()
		   +"Error Message:"+this.getErrorMessage()+" Error Cause:"+
		   this.getErrorCause()+"Error Recovery:"+this.getErrorRecovery();
		return errDetails;
	   }

}
