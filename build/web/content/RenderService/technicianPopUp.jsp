<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.http.*" %>
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javasckript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script>
                var rowCount=1;
                var sno=0;
                var rowIndex = 0;
                var styl='';
                function addRow()
                {
                    
                    if(parseInt(rowCount) %2==0)
                        {
                            styl="text2";
                        }else{
                        styl="text1";
                    }




                    sno++;
                    //document.stkRequest.selectedIndex.value=sno;

                    //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
                    var tab = document.getElementById("techs");
                    var iRowCount = tab.getElementsByTagName('tr').length;
                    //alert("len:"+iRowCount);
                    rowCount = iRowCount;
                    var newrow = tab.insertRow(rowCount);
                    sno = rowCount;
                    rowIndex = rowCount;
                    //alert("rowIndex:"+rowIndex);
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' > "+sno+"</td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    var cell1 = "<td class='text1' height='25'><select name='technicianId' id='technicianId"+rowIndex+"'   class='text2' style='width:150px;' ><option value='0'>--select--</option><c:if test = "${technicians != null}" ><c:forEach items="${technicians}" var="mfr"><option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" /></c:forEach ></c:if> </select></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell1;

                    cell = newrow.insertCell(2);
                    var cell2 = "<td class='text1' height='25'><input name='estimatedHrs' id='estimatedHrs"+rowIndex+"'  style='width:100px;'  class='form-control'  onchange='getOthers("+rowIndex+")'  type='text'></td>";

                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell2;

                    cell = newrow.insertCell(3);
                    cell2 = "<td class='text1' height='25'><input name='actualHrs' id='actualHrs"+rowIndex+"'  style='width:100px;'  class='form-control'  onchange='getOthers("+rowIndex+")'  type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell2;

                    var itemCode=document.getElementsByName("technicianId");
                    itemCode[rowCount-1].focus();

                    rowIndex++;
                    rowCount++;

                }

    function submitPage()   {
        document.workOrderForm.action='/throttle/saveJobCardTechnicians.do';
        document.workOrderForm.submit();
    }


        </script>

    </head>








    <body >
        <form name="workOrderForm" method="post">

            <%@ include file="/content/common/path.jsp" %>

            <!-- pointer table -->

            <!-- message table -->

<%@ include file="/content/common/message.jsp"%>

<input type="hidden" name="jobCardId" value='<%=request.getAttribute("jobCardId")%>'>
<input type="hidden" name="probId" value='<%=request.getAttribute("probId")%>'>

            <table align="center" id="techs" width="90%" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr>
                    <td>Sno
</td>
                    <td>Technician
</td>
                    <td>EstimatedHours
</td>
                    <td>ActualHours
</td>
                </tr>

                <%int index=0;%>
                <c:if test = "${existingTechnicians != null}" >
                    <c:forEach items="${existingTechnicians}" var="temp">
                    <%index++;%>
                <tr>
                    <td ><%=index%></td>
                    <td>
                        <select name="technicianId" style="width:150px;" >
                            <c:if test = "${technicians != null}" >
                                <option   value="0">--select--</option>
                                <c:forEach items="${technicians}" var="mfr">
                                    <c:choose>
                                        <c:when test="${temp.technicianId==mfr.empId}">
                                            <option selected  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                        </c:when>
                                        <c:otherwise>
                                            <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                        </c:otherwise>
                                    </c:choose>

                                </c:forEach >
                            </c:if>
                        </select>
                    </td>
                    <td><input type="text" name="estimatedHrs" style="width:100px;"  value='<c:out value="${temp.estimatedHrs}" />'></td>
                    <td><input type="text" name="actualHrs" style="width:100px;"   value='<c:out value="${temp.actualHrs}" />'></td>
                <tr>
                    </c:forEach>
                </c:if>


            </table>

<br>
<center>
    <input type="button" name="add row" value="add row" onclick="addRow();"> &nbsp;&nbsp;<input type="button" name="save" value="save & close" onclick="submitPage();" >
</center>

</html>

