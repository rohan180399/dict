<%-- 
    Document   : printVoucher
    Created on : Nov 5, 2013, 11:54:09 AM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <script>
        function submitWindow(){
            document.tripPlanning.action = '/throttle/BrattleFoods/viewTripPlanning.jsp';
            document.tripPlanning.submit();
        }
    </script>
    <body>
        <% String menuPath = "Operations >>  Voucher Print";
                    request.setAttribute("menuPath", menuPath);
        %>
        <form name="tripPlanning"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Voucher Print</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>


                                        <td width="80" height="30"><font color="red">*</font>From Date</td>
                                        <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                                            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuleprice.fromDate,'dd-mm-yyyy',this)"/></span></td>
                                        <td width="80" height="30"><font color="red">*</font>To Date</td>
                                        <td width="182"><input name="toDate" type="text" class="form-control" value="" size="20">
                                            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuleprice.toDate,'dd-mm-yyyy',this)"/></span></td>

                                        <td><input type="button"   value="Search" class="button" name="search" onClick="submitWindow()"></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <h2 align="center">Trip Planning</h2>
            <br>
            <table width="80" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Trip No</h3></th>
                        <th><h3>Vehicle NO</h3></th>
                        <th><h3>CNote NO</h3></th>
                        <th><h3>Advance Voucher Print</h3></th>
                        <th><h3>Settlement Voucher Print</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">TS/13-14/100457</td>
                        <td align="left" class="text2">TN18J1273</td>
                        <td align="left" class="text2">13,14</td>
                        <td align="left" class="text2"><a href="/throttle/BrattleFoods/printAdvanceVoucher.jsp">Advance Voucher</a></td>
                        <td align="left" class="text2"><a href="/throttle/BrattleFoods/printSettlementVoucher.jsp">Settlement Voucher</a></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">2</td>
                        <td align="left" class="text2">TS/13-14/100658</td>
                        <td align="left" class="text2">DL24G4378</td>
                        <td align="left" class="text2">8</td>
                        <td align="left" class="text2"><a href="/throttle/BrattleFoods/printAdvanceVoucher.jsp">Advance Voucher</a></td>
                        <td align="left" class="text2"><a href="/throttle/BrattleFoods/printSettlementVoucher.jsp">Settlement Voucher</a></td>
                    </tr>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>