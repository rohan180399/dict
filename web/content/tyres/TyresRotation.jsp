<%--
    Document   : TyresRotation
    Created on : 15 Mar, 2012, 8:26:55 PM
    Author     : kannan
--%>

<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
<script type="text/javascript" src="/throttle/js/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>


<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    /*            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
</style>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<!--        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>-->

<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Vehicle"  text="Vehicle"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onload="getVehAxleName('<c:out value="${axleTypeId}"/>')">

                <!--<form name="tyresRotation" action="/throttle/saveRotation.do">-->
                <form name="tyresRotation">
                    <table class="table table-info mb30 table-hover" >
                    <thead>
                    <th colspan="6"  height="30">
                    <div  align="center"><spring:message code=""  text="Tyre Rotation"/> </div>
                    </th>
                    </thead>
                    </table>
                    <table class="table table-info mb30 table-hover" >
                        <c:if test = "${vehicleList != null}" >
                            <c:forEach items="${vehicleList}" var="vd">
                                <thead>
                                <th colspan="6"  height="30">
                                <div  align="left"><spring:message code=""  text="Vehicle Details"/> </div>
                                </th>
                                </thead>
                                <tr>
                                    <td>Vehicle No</td>
                                    <td><c:out value="${vd.vehicleRegNo}" />
                                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vd.vehicleId}" />" />
                                    </td>
                                    <td>MFR Name</td>
                                    <td><c:out value="${vd.mfrName}" /></td>
                                </tr>
                                <tr>
                                    <td>Model Name</td>
                                    <td><c:out value="${vd.modelName}" /></td>
                                    <td>Last Rotation Date</td>
                                    <td></td>
                                </tr>
                            </c:forEach >
                        </c:if>
                    </table>

                    <table class="table table-info mb30 table-hover" >
                        <thead>
                        <th colspan="6"  height="30">
                        <div  align="left"><spring:message code=""  text="Tyre Rotations(New)"/> </div>
                        </th>
                        </thead>
                        <tr align="left">
                            <td>Rotation No</td>
                            <td><input type="text" name="rotationNo" id="rotationNo" class="form-control" style="width:80px;" readonly="" value="<c:out value="${tyreRotationCount}"/>"/> </td>
                            <td>Rotation Date</td>
                            <td>
                                <input type="text" name="rotationDate" id="rotationDate" class="datepicker form-control" style="width:80px;" value="<c:out value="${startDate}"/>"/>
                            </td>
                        </tr>
                        <tr align="left">
                            <td>Run KM</td>
                            <td><input type="text" name="runKm" id="runKm" class="form-control" style="width:80px;" /> </td>
                            <td>Remarks</td>
                            <td>
                                <textarea name="remarks" id="remarks" class="form-control" style="width:80px;"></textarea>
                            </td>
                        </tr>
                    </table>
                    <span id="updateStatusName" style="color: green;margin-left: 450px"></span>
                    <div id="vehicleOem">
                        <!--                <div align="center" style="font-family:Arial, Helvetica, sans-serif; color:#f5533d; font-size:12px; font-weight:bold;" id="userNameStatus">&nbsp;&nbsp;</div>-->

                        <h2 align="center">FRONT</h2>

                        <table class="table table-info mb30 table-hover" id="positionViewTable">
                            <tr>
                                <thead>
                                <th height="30">
                                <div  align="left"><spring:message code=""  text="Axle Type"/> </div>
                                </th>
                                <th height="30">
                                <div  align="left"><spring:message code=""  text="Left"/> </div>
                                </th>
                                <th height="30">
                                <div  align="left"><spring:message code=""  text="Right"/> </div>
                                </th>
                                </thead>
                            <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
                            </tr>
                        </table>

                        <script type="text/javascript">
                            function getVehAxleName(axleTypeId) {
                                var typeId = axleTypeId;
                                $("#axleTypeId").val(axleTypeId);
                                //                        var typeId=document.getElementById("typeId").value;

                                var axleTypeName = "";
                                var leftSideTyreCount = 0;
                                var rightSideTyreCount = 0;
                                var axleDetailId = 0;
                                var count = 1;

                                $.ajax({
                                    url: '/throttle/getVehAxleName.do',
                                    // alert(url);
                                    data: {typeId: typeId},
                                    dataType: 'json',
                                    success: function (data) {
                                        if (data !== '') {
                                            $.each(data, function (i, data) {
                                                axleTypeName = data.AxleTypeName;
                                                leftSideTyreCount = data.LeftSideTyreCount;
                                                rightSideTyreCount = data.RightSideTyreCount;
                                                axleDetailId = data.AxleDetailId;
                                                positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, count, axleDetailId);
                                                count++;
                                            });
                                        }
                                    }
                                });
                            }


                            function positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, rowCount, axleDetailId) {
                                var rowCount = "";
                                var style = "form-control";

                                rowCount = document.getElementById('selectedRowCount').value;
                                rowCount++;
                                var tab = document.getElementById("positionViewTable");
                                var newrow = tab.insertRow(rowCount);
                                newrow.id = 'rowId' + rowCount;

                                cell = newrow.insertCell(0);
                                var cell2 = "<td colspan='2' height='30'><input type='hidden' name='vehicleAxleDetailId' id='vehicleAxleDetailId" + rowCount + "' value='" + axleDetailId + "'  class='form-control' /><input type='hidden' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='" + axleTypeName + "'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                                //                        var cell2 = "<td class='form-control' height='30'><input type='text' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='"+ axleTypeName +"'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                                cell.setAttribute("className", style);
                                cell.innerHTML = cell2;

                                cell = newrow.insertCell(1);
                                var cell3 = "<td height='30' class='tex1' name='leftTyreIds'  id='leftTyreIds2' colspan=" + leftSideTyreCount + ">";
                                for (var i = 0; i < leftSideTyreCount; i++) {
                                    cell3 += parseInt(i + 1) + ":<input type='hidden' name='leftAxleDetailId' value='" + axleDetailId + "' /><input type='hidden' name='leftAxlePositionId' value='" + parseInt(i + 1) + "' /><select name='tyreNoLeft' id='tyreNoLeft" + axleDetailId + parseInt(i + 1) + "' class='form-control' ><option value='0'>--Select--</option><c:if test = "${tyrePossitionList != null}" ><c:forEach items="${tyrePossitionList}" var="Type"><option value='<c:out value="${Type.tyreNumber}" />'><c:out value="${Type.tyreNumber}" /></option></c:forEach > </c:if></select>Depth<input type='text' class='form-control' maxlength='3'  name='treadDepthLeft' id='treadDepthLeft" + axleDetailId + parseInt(i + 1) + "' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<span style='display:none'><select name='leftItemIds' id='tyreMfrLeft" + axleDetailId + parseInt(i + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",0," + i + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select></span><br>";
                                }
                                cell.setAttribute("className", style);
                                cell.innerHTML = cell3 + "</td>";
                                for (var i = 0; i < leftSideTyreCount; i++) {
                                    fillVehicleTyreDetails(axleDetailId, 0, i);
                                }

                                cell = newrow.insertCell(2);
                                var cell4 = "<td height='30' name='rightTyreIds' id='rightTyreIds2' colspan=" + rightSideTyreCount + ">";
                                for (var j = 0; j < rightSideTyreCount; j++) {
                                    cell4 += parseInt(j + 1) + ":<input type='hidden' name='rightAxleDetailId' value='" + axleDetailId + "' /><input type='hidden' name='rightAxlePositionId' value='" + parseInt(j + 1) + "' /><select name='tyreNoRight' id='tyreNoRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' ><option value='0'>--Select--</option><c:if test = "${tyrePossitionList != null}" ><c:forEach items="${tyrePossitionList}" var="Type"><option value='<c:out value="${Type.tyreNumber}" />'><c:out value="${Type.tyreNumber}" /></option></c:forEach > </c:if></select>Depth<input type='text'  class='form-control' maxlength='3' name='treadDepthRight' id='treadDepthRight" + axleDetailId + parseInt(j + 1) + "' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<span style='display:none'><select name='rightItemIds' id='tyreMfrRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",1," + j + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select></span><br>";
                                }
                                cell.setAttribute("className", style);
                                cell.innerHTML = cell4 + "</td>";
                                for (var j = 0; j < rightSideTyreCount; j++) {
                                    fillVehicleTyreDetails(axleDetailId, 1, j);
                                }


                                cell = newrow.insertCell(3);
                                var cell5 = "<td colspan='2' height='30'><input type='hidden' name='position' id='position'  value='" + parseInt(i) + "' ></td>";
                                cell.setAttribute("className", style);
                                cell.innerHTML = cell5;

                                document.getElementById('selectedRowCount').value = rowCount;


                            }

                            function fillVehicleTyreDetails(axleDetailId, positionName, positionNo) {
                                var vehicleId = '<c:out value="${vehicleId}"/>';
                                var axleTypeId = '<c:out value="${axleTypeId}"/>';
                                if (vehicleId != 0) {
                                    positionNo = parseInt(positionNo + 1);
                                    if (positionName == '0') {
                                        positionName = 'Left';
                                    } else {
                                        positionName = 'Right';
                                    }
                                    $.ajax({
                                        url: '/throttle/getVehAxleTyreNo.do',
                                        // alert(url);
                                        data: {axleDetailId: axleDetailId, positionName: positionName,
                                            positionNo: positionNo, vehicleId: vehicleId, axleTypeId: axleTypeId},
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== '') {
                                                $.each(data, function (i, data) {
                                                    //                                                axleTypeName = data.AxleTypeName;
                                                    document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = data.TyreNo;
                                                    document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = data.TyreDepth;
                                                    document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = data.TyreMfr;
                                                });
                                            }
                                        }
                                    });
                                }
                                //                            alert(document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value);
                                //                            document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = 100;
                            }
                            function updateVehicleTyreDetails(axleDetailId, positionName, positionNo, val, updateType) {
                                var vehicleId = $("#vehicleId").val();
                                positionNo = parseInt(positionNo + 1);
                                var url = '';
                                if (positionName == '0') {
                                    positionName = 'Left';
                                } else {
                                    positionName = 'Right';
                                }
                                if (updateType == 1) {

                                }
                                var treadDepth = document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value;
                                var tyreNo = document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value;
                                if (treadDepth == '') {
                                    alert("Please fill tread depth for " + positionName + " " + positionNo);
                                    document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).focus();
                                    document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                    return;
                                } else if (tyreNo == '') {
                                    alert("Please fill tyre no for " + positionName + " " + positionNo);
                                    document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                    document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                    return;
                                } else {
                                    url = './updateVehicleTyreNo.do';
                                    $.ajax({
                                        url: url,
                                        data: {
                                            axleDetailId: axleDetailId, positionName: positionName, positionNo: positionNo,
                                            updateValue: val, depthVal: treadDepth, updateType: updateType, vehicleId: vehicleId,
                                            tyreNo: tyreNo
                                        },
                                        type: "GET",
                                        success: function (response) {
                                            if (response.toString().trim() == 0) {
                                                document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = '';
                                                alert("Tyre No Already Mapped Please Enter Valid Tyre No");
                                                document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                            }
                                        },
                                        error: function (xhr, status, error) {
                                        }
                                    });
                                }

                            }
                        </script>

                        <br>
                        <table class="table table-info mb30 table-hover" id="oEMtable">
                            <thead>
                            <th height="30" style="width: 20px">
                            <div  align="left"><spring:message code=""  text="S.No"/> </div>
                            </th>
                            <th height="30">
                            <div  align="left"><spring:message code=""  text="Stepney/Battery"/> </div>
                            </th>
                            <th height="30">
                            <div  align="left"><spring:message code=""  text="Details"/> </div>
                            </th>
                            </thead>

                            <input type="hidden" name="oemIdCount" id="oemIdCount" value="<c:out value="${oemListSize}"/>" />
                            <!--<td><input type="button" name="AddRow" id="AddRow" value="AddRow" onclick="addOem(0, 1, '', '', '', 0, 0, 0);"/></td>-->
                            <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
                            <script>
                                function vehicleStepneyDetails(val, sno) {

                                    if (val == 1) {

                                        $("#oemMfr" + sno).show();
                                        $("#oemDepth" + sno).show();
                                        $("#oemTyreNo" + sno).show();
                                        $("#oemBattery" + sno).hide();
                                        //
                                    } else if (val == 2) {
                                        $("#oemBattery" + sno).show();
                                        $("#oemMfr" + sno).hide();
                                        $("#oemDepth" + sno).hide();
                                        $("#oemDepth" + sno).val(0);
                                        $("#oemTyreNo" + sno).hide();

                                    }
                                }

                            </script>
                            <script>

                                function insertVehicleAxle() {

                                    var vehicleId = $("#vehicleId").val();
                                    var url = '';
                                    var oemId = [];
                                    var oemDetailsId = [];
                                    var oemMfr = [];
                                    var oemDepth = [];
                                    var oemTyreNo = [];
                                    var oemBattery = [];
                                    var oemDetailsIds = document.getElementsByName("oemDetailsId");
                                    var oemMfrs = document.getElementsByName("oemMfr");
                                    var oemDepths = document.getElementsByName("oemDepth");
                                    var oemTyreNos = document.getElementsByName("oemTyreNo");
                                    var oemBatterys = document.getElementsByName("oemBattery");
                                    var oemIds = document.getElementsByName("oemId");
                                    for (var i = 0; i < oemDetailsIds.length; i++) {
                                        oemDetailsId.push(oemDetailsIds[i].value);
                                        oemMfr.push(oemMfrs[i].value);
                                        oemTyreNo.push(oemTyreNos[i].value);
                                        oemDepth.push(oemDepths[i].value);
                                        oemBattery.push(oemBatterys[i].value);
                                        oemId.push(oemIds[i].value);
                                        if (oemDetailsIds[i].value == 1) {
                                            if (oemMfrs[i].value == '') {
                                                alert('Please select MFR ');
                                                return;
                                            } else if (oemTyreNos[i].value == '') {
                                                alert('Please Enter The TyreNo ');
                                                return;
                                            } else if (oemDepths[i].value == '') {
                                                alert('Please Enter The Depth ');
                                                return;
                                            }
                                        } else if (oemDetailsIds[i].value == 2) {
                                            if (oemBattery[i].value == '') {
                                                alert('Please select battty ');
                                                return;
                                            }
                                        }

                                    }
                                    var insertStatus = 0;
                                    var oemIdResponse = "";
                                    url = './insertOemDetails.do';
                                    $("#oEmNext").hide();
                                    $.ajax({
                                        url: url,
                                        data: {oemDetailsIdVal: oemDetailsId, vehicleId: vehicleId, oemMfrVal: oemMfr, oemTyreNoVal: oemTyreNo, oemDepthVal: oemDepth,
                                            oemBatteryVal: oemBattery, oemIdVal: oemId
                                        },
                                        type: "GET",
                                        dataType: 'json',
                                        success: function (data) {
                                            var r = 0;
                                            $.each(data, function (i, data) {
                                                //                                alert(data.OemId);
                                                oemIds[r].value = data.OemId;
                                                r++;
                                            });
                                            $("#Status").text("Vehicle OEM added sucessfully ");
                                            $("#oEmNext").show();
                                        },
                                        error: function (xhr, status, error) {
                                        }
                                    });
                                    //                        $("#oEmNext").hide();
                                }

                                function addOem(sno, oemDetailsId, batteryNo, tyreNo, depth, mfrId, oemId, addType) {
                                    var rowCount = sno;
                                    var style = "form-control";
                                    rowCount = document.getElementById('rowCounted').value;
                                    rowCount++;


                                    var tab = document.getElementById("oEMtable");
                                    var newrow = tab.insertRow(rowCount);
                                    newrow.id = 'rowId' + rowCount;

                                    cell = newrow.insertCell(0);
                                    var cell0 = "<td height='25' colspan='2'><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell0;

                                    cell = newrow.insertCell(1);
                                    var cell1 = "";
                                    var oemDetailsName = "";
                                    if (oemDetailsId == 1) {
                                        oemDetailsName = "Stepney";
                                    } else {
                                        oemDetailsName = "Battery";
                                    }
                                    if (addType == 1) {
                                        cell1 = "<td height='30' colspan='2'><input type='hidden' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "'>'" + oemDetailsName + "'</td>";
                                    } else {
                                        cell1 = "<td height='30' colspan='2'><select class='form-control' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "' onchange='batteryTextHideShow(this.value," + rowCount + ");'><option value='1'>Stepney</option><option value='2'>battery</option></select></td>";
                                    }
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell1;

                                    cell = newrow.insertCell(2);
                                    var cell2 = "<td  height='30' colspan='2'><span id='batterySpan" + rowCount + "' style='display:none'>Battery:<input type='text'  name='oemBattery' id='oemBattery" + rowCount + "' maxlength='13' placeholder='BatteryNo'  size='20' class='form-control' value='" + batteryNo + "'  /></span><span id='tyreSpan" + rowCount + "'><select name='oemMfr' id='oemMfr" + rowCount + "' class='form-control' style='width:124px;display:none'  ><option value=0>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select>\n\
                                                                                                                   TyreNo:<select name='oemTyreNo' id='oemTyreNo" + rowCount + "'  class='form-control' ><option value='0'>--Select--</option><c:if test = "${tyrePossitionList != null}" ><c:forEach items="${tyrePossitionList}" var="Type"><option value='<c:out value="${Type.tyreNumber}" />'><c:out value="${Type.tyreNumber}" /></option></c:forEach > </c:if></select>Depth:<input type='text'  name='oemDepth' id='oemDepth" + rowCount + "' maxlength='13'   size='20' class='form-control' placeholder='TyreDepth' value='" + depth + "' />mm</span></td>";
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell2;
                                    $("#oemTyreNo" + rowCount).val(tyreNo);
                                    $("#oemMfr" + rowCount).val(mfrId);
                                    if (addType == 1) {
                                        if (oemDetailsId == 2) {
                                            $("#batterySpan" + rowCount).show();
                                            $("#tyreSpan" + rowCount).hide();
                                        }
                                    }
                                    cell = newrow.insertCell(3);
                                    var cell3 = "<td  height='25' colspan='2' ><input type='hidden' name='oemId' id='oemId" + rowCount + "' value='" + oemId + "'  class='form-control' /></td>";
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell3;
                                    document.getElementById('rowCounted').value = rowCount;
                                }

                                function batteryTextHideShow(value, rowCount) {
                                    if (value == 1) {
                                        $("#batterySpan" + rowCount).hide();
                                        $("#tyreSpan" + rowCount).show();
                                    } else {
                                        $("#batterySpan" + rowCount).show();
                                        $("#tyreSpan" + rowCount).hide();
                                    }
                                }

                                    </script>
                            <%int addRowCount = 1;%>
                            <c:if test="${oemList != null}">
                                <c:forEach items="${oemList}" var="item">
                                    <script>
                                        addOem('<%=addRowCount%>', '<c:out value="${item.oemDetailsId}"/>', '<c:out value="${item.oemBattery}"/>', '<c:out value="${item.oemTyreNo}"/>', '<c:out value="${item.oemDepth}"/>', '<c:out value="${item.oemMfr}"/>', '<c:out value="${item.oemId}"/>', 1);
                                    </script>
                                </c:forEach>
                            </c:if>

                        </table>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="button" value="save" id="saveButton" onclick="savetyreRotation()" />
                        </center>
                        <script>
                            function savetyreRotation() {
                                $("#saveButton").hide();
                                $("#updateStatusName").text("Processing Your request!....");
                                $('#updateStatusName').css('color', 'red');
                                if (document.getElementById("rotationNo").value == "") {
                                    alert("Rotation No should not empty");
                                    document.getElementById("rotationNo").focus();
                                    $("#saveButton").show();
                                    return;
                                } else if (document.getElementById("rotationDate").value == "") {
                                    alert("Rotation Date should not empty");
                                    document.getElementById("rotationDate").focus();
                                    $("#saveButton").show();
                                    return;
                                } else if (document.getElementById("runKm").value == "") {
                                    alert("Run Km should not empty");
                                    document.getElementById("runKm").focus();
                                    $("#saveButton").show();
                                    return;

                                }
                                var tyreNoRight = document.getElementsByName("tyreNoRight");
                                var tyreNoLeft = document.getElementsByName("tyreNoLeft");
                                var treadDepthLeft = document.getElementsByName("treadDepthLeft");
                                var treadDepthRight = document.getElementsByName("treadDepthRight");
                                var leftAxleDetailId = document.getElementsByName("leftAxleDetailId");
                                var leftAxlePositionId = document.getElementsByName("leftAxlePositionId");
                                var rightAxleDetailId = document.getElementsByName("rightAxleDetailId");
                                var rightAxlePositionId = document.getElementsByName("rightAxlePositionId");
                                //OEM
                                var oemTyreNo = document.getElementsByName("oemTyreNo");
                                var oemDepth = document.getElementsByName("oemDepth");
                                var oemId = document.getElementsByName("oemId");

                                var vehicleId = '<c:out value="${vehicleId}"/>';
                                var axleTypeId = '<c:out value="${axleTypeId}"/>';
                                var positionName = "";
                                var positionLeft = "";
                                var positionRight = "";
                                var positionStepney = "";
                                var positionArray = [];
                                var stepneyPositionArray = [];
                                var tyreLeft = "";
                                var tyreRight = "";
                                for (var i = 0; i < tyreNoLeft.length; i++) {
                                    tyreLeft = tyreNoLeft[i].value;
                                    for (var j = i + 1; j < tyreNoLeft.length; j++) {
                                        if (tyreLeft == tyreNoLeft[j].value) {
                                            alert("TyreNo : " + tyreLeft + " is mapped with other axle Left");
                                            tyreNoLeft[j].focus();
                                            $("#saveButton").show();
                                            return;
                                        }
                                    }
                                    for (var r = 0; r < tyreNoRight.length; r++) {
                                        if (tyreLeft == tyreNoRight[r].value) {
                                            alert("TyreNo : " + tyreLeft + " is mapped with other axle Right");
                                            $("#saveButton").show();
                                            tyreNoRight[r].focus();
                                            return;
                                        }
                                    }
                                    for (var s = 0; s < oemTyreNo.length; s++) {
                                        if (tyreLeft == oemTyreNo[s].value) {
                                            alert("TyreNo : " + tyreLeft + " is mapped with stepney and other axle also");
                                            $("#saveButton").show();
                                            oemTyreNo[r].focus();
                                            return;
                                        }
                                    }
                                    positionName = "Left";
                                    if (treadDepthLeft[i].value == "") {
                                        alert("Tyre tread depth should not empty");
                                        $("#saveButton").show();
                                        treadDepthLeft[i].focus();
                                        return;
                                    } else {
                                        positionLeft = leftAxleDetailId[i].value + "~" + positionName + "~" + leftAxlePositionId[i].value + "~" + vehicleId + "~" + axleTypeId + "~" + tyreLeft + "~" + treadDepthLeft[i].value;
                                        positionArray.push(positionLeft)
                                    }
                                }
                                var tyreRight = "";
                                for (var r = 0; r < tyreNoRight.length; r++) {
                                    tyreRight = tyreNoRight[r].value;
                                    for (var t = r + 1; t < tyreNoLeft.length; t++) {
                                        if (tyreRight == tyreNoRight[t].value) {
                                            alert("TyreNo : " + tyreRight + " is mapped with other axle Right");
                                            tyreNoRight[t].focus();
                                            $("#saveButton").show();
                                            return;
                                        }
                                    }
                                    for (var s = 0; s < oemTyreNo.length; s++) {
                                        if (tyreRight == oemTyreNo[s].value) {
                                            alert("TyreNo : " + tyreRight + " is mapped with Stepney");
                                            oemTyreNo[s].focus();
                                            $("#saveButton").show();
                                            return;
                                        }
                                    }
                                }
                                var tyreStepney = "";
                                for (var r = 0; r < oemTyreNo.length; r++) {
                                    tyreStepney = oemTyreNo[r].value;
                                    for (var t = r + 1; t < oemTyreNo.length; t++) {
                                        if (tyreStepney == oemTyreNo[t].value) {
                                            alert("TyreNo : " + tyreStepney + " is mapped with Stepney");
                                            oemTyreNo[t].focus();
                                            $("#saveButton").show();
                                            return;
                                        }
                                    }

                                }

                                positionName = "Right";
                                for (var r = 0; r < tyreNoRight.length; r++) {
                                    if (treadDepthRight[r].value == "") {
                                        alert("Tyre tread depth should not empty");
                                        treadDepthRight[r].focus();
                                        $("#saveButton").show();
                                        return;
                                    } else {
                                        positionRight = rightAxleDetailId[r].value + "~" + positionName + "~" + rightAxlePositionId[r].value + "~" + vehicleId + "~" + axleTypeId + "~" + tyreNoRight[r].value + "~" + treadDepthRight[r].value;
                                        positionArray.push(positionRight)
                                    }
                                }
                                for (var s = 0; s < oemTyreNo.length; s++) {
                                    if (oemDepth[s].value == "") {
                                        alert("Stepney Tyre tread depth should not empty");
                                        oemDepth[s].focus();
                                        $("#saveButton").show();
                                        return;
                                    } else {
                                        positionStepney = oemId[s].value + "~" + oemTyreNo[s].value + "~" + oemDepth[s].value + "~" + vehicleId;
                                        stepneyPositionArray.push(positionStepney)
                                    }
                                }
                                var url = '';
                                var rotationStatus = 0;
                                url = './updateTyreRotation.do';
                                var rotationNo = document.getElementById("rotationNo").value;
                                var rotationDate = document.getElementById("rotationDate").value;
                                var runKm = document.getElementById("runKm").value;
                                var remarks = document.getElementById("remarks").value;
                                $.ajax({
                                    url: url,
                                    data: {positionData: positionArray, rotationNo: rotationNo, rotationDate: rotationDate, runKm: runKm, remarks: remarks, vehicleId: vehicleId, stepneyData: stepneyPositionArray},
                                    type: "POST",
                                    dataType: 'json',
                                    success: function (response) {
                                        rotationStatus = response.toString().trim();
                                        $("#saveButton").show();
                                        if (rotationStatus == 0) {
                                            $("#updateStatusName").text("Tyre Rotation Not Done ");
                                            $('#updateStatusName').css('color', 'red');
                                            $('#rotationNo').val(rotationStatus);
                                        } else {
                                            $("#updateStatusName").text("Tyre Rotation Done Sucessfully");
                                            $('#updateStatusName').css('color', 'green');
                                            $('#rotationNo').val(rotationStatus);
                                        }
                                    },
                                    error: function (xhr, status, error) {
                                        $("#saveButton").show();
                                    }
                                });
                                //                        document.tyresRotation.action = '/throttle/saveRotation.do';
                                //                        document.tyresRotation.submit();

                            }
                        </script>
                    </div>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
