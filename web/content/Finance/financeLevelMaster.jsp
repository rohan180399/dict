
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
    <script>

        function setLevelValues(levelValue)
        {
            var temp = levelValue.split("~");
            //alert(temp[0]+":::"+temp[1]+":::"+temp[2]);
            document.add.primaryID.value = temp[0];
            document.add.levelgroupId.value = temp[1];
            document.add.fullName.value = temp[2];

        }
        function submitPage()
        {
            if (isEmpty(document.getElementById("LevelName").value)) {
                alert("Sub Level Name Should Not Be Empty");
                document.add.LevelName.focus();
                return;
            } else if (document.add.primaryLevelList.value == "0") {
                alert("Select Level type");
                document.add.primaryLevelList.focus();
                return;
            } else if (isEmpty(document.add.description.value)) {
                alert("Please Enter Description");
                document.add.description.focus();
                return;
            }
            document.add.action = '/throttle/saveNewLevel.do';
            document.add.submit();

        }


    </script>
    <style>
    #index td {
   color:white;
}
</style>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageLevelMaster"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageLevelMaster"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">  
    <body>
        <form name="add" method="post" class="form-horizontal form-bordered">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
            <table class="table table-info mb30 table-hover">
                 <tr height="30" id="index">
                        <td colspan="4" style="background-color:#5BC0DE;"><b><spring:message code="finance.label.AddLevel"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                <input type="hidden" name="primaryID" id="primaryID" />
                <input type="hidden" name="levelgroupId" id="LevelGroupID" />
                <input type="hidden" name="fullName" id="fullName"  />
                <tr height="30">
                    <td ><font color="red">*</font><spring:message code="finance.label.LevelName"  text="default text"/></td>
                    <td >
                        <input name="LevelName" type="text" class="form-control" id="LevelName" value="" style="width:260px;height:40px;">
                    </td>
                    <td  height="30"><font color="red">*</font><spring:message code="finance.label.Primary/Levelmaster"  text="default text"/></td>
                    <td  height="30">
                        <select class="form-control" name="primaryLevelList" id="primaryLevelList" style="width:260px;height:40px;" onchange="setLevelValues(this.value)">
                            <option value="0">---<spring:message code="finance.label.Select"  text="default text"/>---</option>
                            <c:if test = "${levelMasterList != null}" >
                                <c:forEach items="${levelMasterList}" var="levelM">
                                    <option value='<c:out value="${levelM.groupID}" />~<c:out value="${levelM.levelgroupId}" />~<c:out value="${levelM.levelgroupName}" />'><c:out value="${levelM.levelgroupName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font><spring:message code="finance.label.Description"  text="default text"/></td>
                    <td  height="30">
                        <input type="text" name="description" id="description" style="width:260px;height:40px;" class="form-control">
                    </td>
                    <td  height="30">&nbsp;</td>
                    <td  height="30">&nbsp;</td>
                </tr>

            </table>
            <br>
            <center>
                <input type="button"  value="<spring:message code="finance.label.Save"  text="default text"/>" onclick="submitPage();" class="btn btn-success" style="width:100px;height:35px;" />
                &emsp;<input type="reset" class="btn btn-success" style="width:100px;height:35px;" value="<spring:message code="finance.label.Clear"  text="default text"/>">
            </center>
 <br>
            <script>
                function callNextLevel(levelId) {
//                    alert(levelId);
                    var url = './getNextLevelList.do';
                    $.ajax({
                        url: url,
                        data: {levelId: levelId},
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                            $("#ul" + levelId).text("");
                            $.each(response, function (i, data) {
                                $("#ul" + levelId).append($("<li id='li" + data.groupId + "' style='border:0px;'>").append("<input type='checkbox' id='cb" + data.groupId + "' onclick='callNextLevel(" + data.groupId + ")' />").append("<label for='cb" + data.groupId + "'>" + data.groupName + "</label><span style='position: absolute;right: 500px;'>"+data.credit+"</span><span style='position: absolute;right: 230px;'>"+data.debit+"</span>").append($("<ul id='ul" + data.groupId + "'>")));
                            });
                        },
                        error: function (xhr, status, error) {
                        }
                    });
                }
            </script>
       
        <style>
    #index th {
   color:white;
}
</style>
              <table class="table table-info mb30 table-hover">
                   <thead  height="30" id="index"> 
                            <th align="left" width="40%" style="background-color:#5BC0DE;"><spring:message code="finance.label.ACCOUNTHEAD"  text="default text"/></th>
                            <th align="right" width="20%" style="background-color:#5BC0DE;"><spring:message code="finance.label.DEBITS"  text="default text"/></th>
                            <th align="center" width="18%" style="background-color:#5BC0DE;"><spring:message code="finance.label.CREDITS"  text="default text"/></th>
                        </thead>
          
            <tr>
                <td colspan="2">
                    <ul class="ul-d">
                        <c:if test="${primaryLevelMasterList != null}">
                            <c:forEach items="${primaryLevelMasterList}" var="grp">
                                <li class="li-id" id="li<c:out value="${grp.groupid}"/>" style="border:0px;">
                                    <input type="checkbox" id="cb<c:out value="${grp.groupid}"/>" onclick="callNextLevel('<c:out value="${grp.groupid}"/>')"/><label class="label-d" style="color:black" for="cb<c:out value="${grp.groupid}"/>"><c:out value="${grp.groupname}"/></label><span style="position: absolute;right: 500px;color:black"><b><c:out value="${grp.credit}"/></b></span><span style="position: absolute;right: 230px;color:black"><b><c:out value="${grp.debit}"/></b></span>
                                    <ul class="ul-d" id="ul<c:out value="${grp.groupid}"/>">
                                 
                                    </ul>
                                </li>
                            </c:forEach>
                        </c:if>
                    </ul> 
                </td>    
                <td></td>
                <!--<td></td>-->
            </tr>
        </table>    
 <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            <br>
    <style>
        ul-d {
         list-style-type:none; 
        }
        label-d{
/*            background-color: #AAAFAB;*/
            background-color: #08844e;
            border-radius: 5px;
            padding: 3px;
            padding-left: 25px;
            color: #ffffff;	
        }
        li-id { 
            margin: 1px;
            padding: 5px;
            /*border: 1px solid #ABC;*/
            border: 1px solid #ABC;
            border-radius: 5px;
        }
        input[type=checkbox] { display: none; }
        input[type=checkbox] ~ ul { 
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            white-space:nowrap;
            -webkit-transition:all 1s ease;  
            -moz-transition:all 1s ease;  
            -o-transition:all 1s ease;  
            transition:all 1s ease;  

        }
        input[type=checkbox]:checked ~ ul { 
            max-height: 100%;
            max-width: 100%;
            opacity: 1;
        }
        input[type=checkbox] + label:before{
            transform-origin:25% 50%;
            border: 8px solid transparent;
            border-width: 8px 12px;	
            border-left-color: white;
            margin-left: -20px;
            width: 0;
            height: 0;
            display: inline-block;
            text-align: center;
            content: '';
            color: #AAAFAB;
            -webkit-transition:all .5s ease;  
            -moz-transition:all .5s ease;  
            -o-transition:all .5s ease;  
            transition:all .5s ease; 
            position: absolute;
            margin-top: 1px;
        }
        input[type=checkbox]:checked + label:before {
            transform: rotate(90deg);
            /*margin-top: 6px;
      margin-left: -25px;*/
        }

    </style>
</body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


