<%-- 
    Document   : tripMapLocation
    Created on : Feb 8, 2016, 2:15:00 PM
    Author     : hp
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ page import="ets.domain.trip.business.TripTO" %>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initMap"
        async defer></script>
        <title>JSP Page</title>
    </head>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
            float: left;
            width: 63%;
            height: 100%;
        }
        #right-panel {
            float: right;
            width: 34%;
            height: 100%;
        }
        #right-panel {
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #right-panel select, #right-panel input {
            font-size: 15px;
        }

        #right-panel select {
            width: 100%;
        }

        #right-panel i {
            font-size: 12px;
        }

        .panel {
            height: 100%;
            overflow: auto;
        }
    </style>
    <body onload="initMap()">
        <h1>Last 10 Co-Ordinates From GPS Log</h1>
        <form>
            <%
                ArrayList mapInterLocationList = (ArrayList) request.getAttribute("mapInterLocationList");
                Iterator Itr = mapInterLocationList.iterator();
                TripTO tripTO = null;
            %>
            <div id="mapTab" style="width: 1380px; height: 500px; margin-top:20px;">
                <div id="map"></div>
<!--                <div id="right-panel">
                    <p>Total Distance: <span id="total"></span></p>
                </div>-->
                <script>
                    function initMap() {
                        var originLat = '<%=request.getAttribute("originLat")%>';
                        alert(originLat);
                        var originLon = '<%=request.getAttribute("originLon")%>';
                        alert(originLon);
                        var latlng = new google.maps.LatLng(originLat, originLon);
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 5,
                            center: latlng
//                            center: {lat: 34.437696, lng: -15.593918}  // Australia.
//                                            center: {lat: 21.0000, lng: 78.0000}  // Australia.
                        });

                        var directionsService = new google.maps.DirectionsService;
                        var directionsDisplay = new google.maps.DirectionsRenderer({
                            draggable: true,
                            map: map
//                                ,
//                                panel: document.getElementById('right-panel')
                        });

                        directionsDisplay.addListener('directions_changed', function() {
                            computeTotalDistance(directionsDisplay.getDirections());
                        });

                        displayRoute(originLat, originLon, directionsService,
                                directionsDisplay);
//                        displayRoute('chennai, INDIA', 'madurai, INDIA', directionsService,
//                                directionsDisplay);
                    }
                    function displayRoute(originLat, originLon, service, display) {
                        var destinationLat = '<%=request.getAttribute("destinationLat")%>';
                        //alert(originLat);
                        var destinationLon = '<%=request.getAttribute("destinationLon")%>';


                        var waypts = [];
                   
                        service.route({
//                                origin: new google.maps.LatLng(Latitude[i],-15.593918),
//                                destination: new google.maps.LatLng(Latitude[i],Longitude[i]),


                            origin: new google.maps.LatLng(originLat, originLon),
                            destination: new google.maps.LatLng(destinationLat, destinationLon),
//                            origin: {lat: 34.437696, lng: -15.593918}, // Haight.
//                            destination: {lat: 34.294471, lng: -15.774434},
//                            waypoints: waypts,
//                                 waypoints: [{location: 'Guindy, INDIA'}, {location: 'Tambaram, INDIA'}],
                            travelMode: google.maps.TravelMode.DRIVING,
                            avoidTolls: true
                        }, function(response, status) {
                            if (status === google.maps.DirectionsStatus.OK) {
                                display.setDirections(response);
                            } else {
                                //      alert('Could not display directions due to: ' + status);
                            }
                        });
                    }

                    function computeTotalDistance(result) {
                        var total = 0;
                        var myroute = result.routes[0];
                        for (var i = 0; i < myroute.legs.length; i++) {
                            total += myroute.legs[i].distance.value;
                        }
                        total = total / 1000;
//                        document.getElementById('total').innerHTML = total + ' km';
                        //   document.getElementById("distance").value = total;
                    }
                    //origin: new google.maps.LatLng(fromLat, fromLon),
                </script>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
