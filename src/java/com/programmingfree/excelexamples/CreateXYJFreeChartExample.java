/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.programmingfree.excelexamples;

/**
 *
 * @author Arul
 */
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.io.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.plot.PlotOrientation;
import java.util.Iterator;
import java.sql.*;
import java.io.*;
import java.math.BigDecimal;
//import java.util.*;
import java.util.Calendar;
import java.text.*;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import static org.apache.poi.hssf.usermodel.HeaderFooter.date;
import org.jfree.data.time.Day;
import org.jfree.data.time.Hour;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class CreateXYJFreeChartExample {

    public static void main(String[] args) throws Exception {
        String url = "jdbc:mysql://localhost:3306/";
//        String dbName = "throttlebrattle";
        String dbName = "throttlebrattle";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";
        File theDir = new File("C:/DataLoggerGraph/");

            // if the C:/tmp/ directory does not exist, create it
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    //////System.out.println("C:/DataLoggerGraph/DIR created");
                }

            }
        try {
            Calendar cal = Calendar.getInstance();
            String rangeStartDate1 = "";
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            String filename = "C:/DataLoggerGraph/" + "dataLogExcel.xls";
                //////System.out.println("filename = " + filename);
                
                //first my_sheet start
                XSSFWorkbook my_workbook1 = new XSSFWorkbook();
                String sheetName = "Report";
                Sheet my_sheet1 = (Sheet) my_workbook1.createSheet(sheetName);
                Row s1Row1 = my_sheet1.createRow((short) 23);
                s1Row1.setHeightInPoints(50); // row hight
                s1Row1.createCell((short) 0).setCellValue("S.No");
                my_sheet1.setColumnWidth((short)0,(short)4000 ); // cell width
                s1Row1.createCell((short) 1).setCellValue("Log Date");
                my_sheet1.setColumnWidth((short)1,(short)4000 ); // cell width
                s1Row1.createCell((short) 2).setCellValue("Log Time");
                my_sheet1.setColumnWidth((short)2,(short)4000 ); // cell width
                s1Row1.createCell((short) 3).setCellValue("Current Temperature");
                my_sheet1.setColumnWidth((short)3,(short)4000 ); // cell width
                s1Row1.createCell((short) 4).setCellValue("Vehicle No");
                my_sheet1.setColumnWidth((short)4,(short)4000 ); // cell width
                s1Row1.createCell((short) 5).setCellValue("Current  location");
                my_sheet1.setColumnWidth((short)5,(short)4000 ); // cell width
                s1Row1.createCell((short) 6).setCellValue("Distance travelled");
                my_sheet1.setColumnWidth((short)6,(short)4000 ); // cell width
                
                Class.forName(driver).newInstance();
                Connection conn = DriverManager.getConnection(url + dbName, userName, password);
                Statement st = conn.createStatement();
                Statement st1 = conn.createStatement();
                String logDateTime = "";
                String logDate = "";
                String logTime = "";
                String currentTemperature  = "";
                String regNo  = "";
                String location  = "";
                String dstanceTravelled  = "";
                ResultSet res1 = st1.executeQuery(" select `date` as logDateTime, \n" +
                        
                        "DATE_FORMAT(`date`,'%d-%m-%Y') as logDate, \n" +
                        "TIME_FORMAT(`date`, '%H:%i:%s' ) as logtime, \n" +
                        "current_temperature, \n" +
                        "b.reg_no,a.location,a.distance_travelled \n" +
                        "from \n" +
                        "ts_vehicle_location_log a, \n" +
                        "papl_vehicle_reg_no b\n" +
                        "where a.vehicle_Id = b.vehicle_Id\n" +
                        "and b.Active_Ind = 'Y'\n" +
                        "and a.trip_Id = 422\n" +
                        "order by `date` ");
                int cntr = 24;
                while (res1.next()) {
                    logDateTime = res1.getString("logDateTime");
                    logDate = res1.getString("logDate");
                    logTime = res1.getString("logtime");
                    currentTemperature = res1.getString("current_temperature");
                    regNo = res1.getString("reg_no");
                    location = res1.getString("location");
                    dstanceTravelled = res1.getString("distance_travelled");
                    s1Row1 = my_sheet1.createRow((short) cntr);
                    s1Row1.createCell((short) 0).setCellValue(cntr);
                    s1Row1.createCell((short) 1).setCellValue(logDateTime);
                    s1Row1.createCell((short) 2).setCellValue(currentTemperature);
                    s1Row1.createCell((short) 3).setCellValue(logDate);
                    s1Row1.createCell((short) 4).setCellValue(logTime);
                    s1Row1.createCell((short) 5).setCellValue(regNo);
                    s1Row1.createCell((short) 6).setCellValue(location);
                    s1Row1.createCell((short) 7).setCellValue(dstanceTravelled);
                    cntr++;
                }
                
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook1.write(fileOut);
            FileInputStream chart_file_input = new FileInputStream(new File("C:/DataLoggerGraph/" + "dataLogExcel.xls"));
            XSSFWorkbook my_workbook = new XSSFWorkbook(chart_file_input);
            XSSFSheet my_sheet = my_workbook.getSheetAt(0);
            DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
            //////System.out.println("conn = " + conn);
            ResultSet res = st.executeQuery(" SELECT \n"
                    + "DATE_FORMAT(`date`,'%d-%m-%Y') as logDate,\n"
                    + "TIME_FORMAT(`date`, '%H:%i:%s' ) as logtime,\n"
                    + "current_temperature FROM ts_vehicle_location_log\n"
                    + "where trip_Id = 422\n"
                    + "group by DATE_FORMAT(`date`,'%d-%m-%Y')\n"
                    + "order by `date` ");

            while (res.next()) {
                logDate = res.getString("logDate");
            }

            Iterator<Row> rowIterator = my_sheet.iterator();
            String chart_label = "";
            Number chart_data = 3;
            String seriesLabel = "";
            Day today = new Day();
            TimeSeries series = new TimeSeries( "", Hour.class );
            TimeSeriesCollection dataset=new TimeSeriesCollection();
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
            DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat m = new SimpleDateFormat("MM");
            SimpleDateFormat d = new SimpleDateFormat("dd");
            SimpleDateFormat y = new SimpleDateFormat("yyyy");
            SimpleDateFormat h = new SimpleDateFormat("HH");
            SimpleDateFormat s = new SimpleDateFormat("ss");
            SimpleDateFormat t = new SimpleDateFormat("aa");
            java.util.Date date = null; 
            int day = 0;
            int month = 0;
            int year = 0;
            int hour = 0;
            String outputText = "" ;
            
            while (rowIterator.hasNext()) {
                //Read Rows from Excel document
                Row row = rowIterator.next();
                //Read cells in Rows and get chart data
                Iterator<Cell> cellIterator = row.cellIterator();
                int columnIndex = 0;
                int checkRow = 0;
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    checkRow = cell.getRowIndex();
                    //////System.out.println("checkRow = " + checkRow);
                    if(checkRow > 23 && checkRow < 40 ){
                    columnIndex = cell.getColumnIndex();
                    //////System.out.println("columnIndex = " + columnIndex);
                    if(columnIndex == 1){
                    chart_label = cell.getStringCellValue(); 
                    date = inputFormat.parse(chart_label);
                    outputText = outputFormat.format(date);
                    //////System.out.println("outputText = " + outputText);
                    day = Integer.parseInt(d.format(date));
                    month = Integer.parseInt(m.format(date));
                    year = Integer.parseInt(y.format(date));
                    hour = Integer.parseInt(h.format(date));
                    }
                    if(columnIndex == 2){
                    chart_data =  Float.parseFloat(cell.getStringCellValue());
                    }
                    if(columnIndex == 3){
                    seriesLabel = cell.getStringCellValue();    
                    }
                    my_bar_chart_dataset.addValue(chart_data.doubleValue(), seriesLabel, outputText);
                    //////System.out.println("day = " + day);
                    //////System.out.println("month = " + month);
                    //////System.out.println("year = " + year);
                    //////System.out.println("hour = " + hour);
                    if(day != 0 && month != 0 && year != 0){
                    today =  new Day(day,month,year);
                    if(hour == 0){
                    series.add(new Hour(0,today), chart_data);
                    }else if(hour == 1){
                    series.add(new Hour(1,today), chart_data);
                    }else if(hour == 2){
                    series.add(new Hour(2,today), chart_data);
                    }else if(hour == 3){
                    series.add(new Hour(3,today), chart_data);
                    }else if(hour == 4){
                    series.add(new Hour(4,today), chart_data);
                    }else if(hour == 5){
                    series.add(new Hour(5,today), chart_data);
                    }else if(hour == 6){
                    series.add(new Hour(6,today), chart_data);
                    }else if(hour == 7){
                    series.add(new Hour(7,today), chart_data);
                    }else if(hour == 8){
                    series.add(new Hour(8,today), chart_data);
                    }
                    dataset.addSeries(series);
                    }
                    }
                    
                    
                }
              
            }
            JFreeChart BarChartObject1 = ChartFactory.createLineChart("Time Vs Temperature", "Time", "Temperature", my_bar_chart_dataset, PlotOrientation.VERTICAL, true, true, false);
            JFreeChart BarChartObject = ChartFactory.createTimeSeriesChart("User Participation Chart", "Time", "Temperature",dataset,true,true,false);
            /* Dimensions of the bar chart */
            int width = logDate.length()* 100; /* Width of the chart */

            int height = 490; /* Height of the chart */
            /* We don't want to create an intermediate file. So, we create a byte array output stream 
             and byte array input stream
             And we pass the chart data directly to input stream through this */
            /* Write chart as PNG to Output Stream */

            ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(chart_out, BarChartObject, width, height);
            /* We can now read the byte data from output stream and stamp the chart to Excel worksheet */
            int my_picture_id = my_workbook.addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);
            /* we close the output stream as we don't need this anymore */
            chart_out.close();
            /* Create the drawing container */
            XSSFDrawing drawing = my_sheet.createDrawingPatriarch();
            /* Create an anchor point */
            ClientAnchor my_anchor = new XSSFClientAnchor();
            /* Define top left corner, and we can resize picture suitable from there */
            my_anchor.setCol1(0);
            my_anchor.setRow1(0);
            /* Invoke createPicture and pass the anchor point and ID */
            XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
            /* Call resize method, which resizes the image */
            my_picture.resize();
            /* Close the FileInputStream */
            chart_file_input.close();
            /* Write changes to the workbook */
            FileOutputStream out = new FileOutputStream(new File("C:/DataLoggerGraph/" + "dataLogExcelGraph.xls"));
            my_workbook.write(out);
            out.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
