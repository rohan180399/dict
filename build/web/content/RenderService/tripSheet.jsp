<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Trip Sheet</title>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="css/box-style.css" rel="stylesheet" type="text/css">

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>

    <body onload="fillData();">


        <script>



            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';

            function addAllowanceRow()
            {
                var currentDate = new Date();
                var day = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var myDate= day + "-" + month + "-" + year;
                if(sno < 9){
                    sno++;
                    var tab = document.getElementById("allowanceTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount)-1;
                    //                    if(snumber == 1) {
                    //                        snumber = parseInt(rowCount);
                    //                    }else {
                    //                        snumber++;
                    //                    }

                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                    newrow.height="30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);

                    //cell0 = "<td class='text1'><select name='stageId' id='stageId' class='form-control' onChange='' style='width:150px;'><option value=''>Select</option><option value='Chennai'>Chennai</option><option value='Coimbatore'>Coimbatore</option><option value='Ernakulam'>Ernakulam</option><option value='Trivandrum'>Trivandrum</option><option value='Udangudi'>Udangudi</option></select></td>";
                    cell0 = "<td class='text1'><select class='form-control' style='width:100px;' id='stageId' style='width:130px'  name='stageId"+sno+"'><c:if test = "${opLocation != null}" ><c:forEach items="${opLocation}" var="opl"><option  value='<c:out value="${opl.locationId}" />'><c:out value="${opl.locationName}" /> </c:forEach ></c:if> </select></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text2'><input name='tripAllowanceDate' id='tripAllowanceDate"+snumber+"' type='text' class='datepicker' value='"+myDate+"' id='tripAllowanceDate' class='Textbox' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='tripAllowanceAmount' type='text' class='form-control' id='tripAllowanceAmount' onblur='sumAllowanceAmt()' class='Textbox' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    //                    cell0 = "<td class='text1'><select class='form-control' id='tripAllowancePaidBy' style='width:125px'  name='tripAllowancePaidBy"+snumber+"'><option selected value=0>---Select---</option><c:if test = "${paidBy != null}" ><c:forEach items="${paidBy}" var="paid"><option  value='<c:out value="${paid.issuerId}" />'><c:out value="${paid.issuerName}" /> </c:forEach ></c:if> </select></td>";
                    cell0 = "<td class='text1'><input id='tripAllowancePaidBy' name='tripAllowancePaidBy"+snumber+"' type='hidden' value='<%= session.getAttribute("userId")%>' /><input id='AllowancepaidName' name='AllowancepaidName' type='text' class='form-control' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(5);
                    cell0 = "<td class='text1'><input name='tripAllowanceRemarks' style='width:70px;' type='text' class='form-control' id='tripAllowanceRemarks' class='Textbox' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(6);
                    var cell1 = "<td alain='left' ><input type='checkbox' name='deleteItem' value='"+snumber+"'/> </td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell1;
                    // rowCount++;



                    $( ".datepicker" ).datepicker({
                        /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });

                }
            }


            function delAllowanceRow() {
                try {
                    var table = document.getElementById("allowanceTBL");
                    rowCount = table.rows.length-1;
                    for(var i=2; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                            // snumber--;
                        }
                    }sumAllowanceAmt();
                }catch(e) {
                    alert(e);
                }
            }

            function submitPage(){
                if(isEmpty(document.getElementById("gpno").value)){
                    alert("Enter G.P.No");
                    return false;
                } else if(isEmpty(document.getElementById("invoiceNo").value)){
                    alert("Enter Invoice No");
                    return false;
                }
                document.tripSheet.action='/throttle/saveTripSheet.do';
                document.tripSheet.submit();

                document.getElementById('saveButton').style.visibility='hidden';


                //action="saveTripSheet.do" method="post" onsubmit="return validateSubmit();"
            }







            //            function validateSubmit() {
            //                totalKmsFunc();
            //                sumExpenses();
            //                setBalance();
            //                if(isEmpty(document.getElementById("routeId").value)){
            //                    alert("Select Route");
            //                    return false;
            //                } else if(isEmpty(document.getElementById("vehicleId").value)){
            //                    alert("Select Vehicle");
            //                    return false;
            //                } else if(isEmpty(document.getElementById("tripCode").value)){
            //                    alert("Trip Code is not filled");
            //                    return false;
            //                }else if(isEmpty(document.getElementById("kmsOut").value)){
            //                    alert("Kms Out is not filled");
            //                    return false;
            //                }else if(isDigit(document.getElementById("kmsOut").value)){
            //                    alert("Kms Out accepts only numeric values");
            //                    return false;
            //                }else if(isEmpty(document.getElementById("kmsIn").value)){
            //                    alert("Kms Out is not filled");
            //                    return false;
            //                }else if(isDigit(document.getElementById("kmsIn").value)){
            //                    alert("Kms In accepts only numeric values");
            //                    return false;
            //                }/*else if(parseFloat(document.getElementById("kmsIn").value) == 0){
            //                    alert("Kms In Should not be 0");
            //                    return false;
            //                }*/
            //
            //            }


            var httpReq;
            function callAjax(){


                if( trim(document.tripSheet.companyId.value) == '' ){
                    alert("Please Choose Company");
                    document.tripSheet.companyId.focus();
                    return;
                }
                if( trim(document.tripSheet.routeId.value) == '' ){
                    alert("Please Choose Route");
                    document.tripSheet.routeId.focus();
                    return;
                }
                var compId=document.tripSheet.companyId.value;
                var routeId=document.tripSheet.routeId.value;
                var url='/throttle/getCompanyRouteTonnageRate.do?compId='+compId+'&routeId='+routeId;
                //alert(url);
                if ( compId != '') {
                    if (window.ActiveXObject){
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest){
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() { processAjax();};
                    httpReq.send(null);
                }

            }
            function processAjax()
            {
                //alert(httpReq.readyState);
                //alert(httpReq.status);
                if (httpReq.readyState == 4)
                {
                    if(httpReq.status == 200)
                    {
                        //alert(httpReq.responseText);
                        temp = httpReq.responseText.valueOf();


                        document.tripSheet.tonnageRate.value=temp;
                        document.tripSheet.tripRevenue.value=(document.tripSheet.tonnage.value *  document.tripSheet.tonnageRate.value).toFixed(2);

                    }
                    else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
            }

            function getPinkSlipList()
            {

                // document.location.reload(true);
                var vehicleNo = document.getElementById("regno").value;
                //var vehicleNo = document.getElementById('vehicleId').options[document.getElementById('vehicleId').selectedIndex].text;
                if(vehicleNo != "") {
                    var url = "/throttle/getPinkSlipDetail.do?vehicleNo="+vehicleNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);

                }
            }

            function processRequest()
            {
                if (httpRequest.readyState == 4)
                {
                    if(httpRequest.status == 200)
                    {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            // alert("pink slip==>"+detail);


                            if(detail != "null"){
                                // alert("in-----detail-->"+detail);
                                document.getElementById('pinkSlipStatus').style.display='none';
                                document.getElementById('pinkSlipStatus').innerHTML = "";
                                var vehicleValues = detail.split("~");
                                document.tripSheet.orderNo.value = vehicleValues[0];
                                document.tripSheet.bags.value = vehicleValues[2];
                                document.tripSheet.tonnage.value = vehicleValues[1];
                                document.tripSheet.routeId.value = vehicleValues[3];
                                document.tripSheet.tripCode.value = vehicleValues[3];
                                var routeP = vehicleValues[4]+"-"+vehicleValues[5];
                                document.tripSheet.routeName.value = routeP;
                                var routeval = document.getElementById("routeId").value;
                                document.tripSheet.vehicleId.value = vehicleValues[7];
                                var driverid = vehicleValues[8].split("-");
                                document.tripSheet.driverNameId.value = driverid[1];
                                document.tripSheet.companyId.value = vehicleValues[9];
                                document.tripSheet.companyName.value = vehicleValues[10];
                                //alert(vehicleValues[10]);
                                document.tripSheet.productId.value = vehicleValues[11];
                                document.tripSheet.productName.value = vehicleValues[12];
                                document.tripSheet.billStatus.value = vehicleValues[19];
                                document.tripSheet.tonnageRate.value = vehicleValues[20];
                                document.tripSheet.pinkSlip.value = vehicleValues[21];
                                document.tripSheet.vehicleType.value = vehicleValues[22];
                                if(document.tripSheet.vehicleType.value != 1){
                                    document.getElementById('fuelDiv').style.display='none';
                                }
                                document.tripSheet.driverType.value = vehicleValues[23];
                                document.tripSheet.vehicleVendorId.value = vehicleValues[24];
                                document.tripSheet.driverVendorId.value = vehicleValues[25];
                                document.tripSheet.tonnageRateDriverBata.value = vehicleValues[26];
                                document.tripSheet.customerType.value = vehicleValues[27];
                                document.tripSheet.selectedBillStatus.value = vehicleValues[33];
                                var twoLpsStatus = vehicleValues[29];
                                document.tripSheet.twoLpsStatus.value = twoLpsStatus;

                                if(twoLpsStatus == "Y"){
                                    var selectedTonnageRate1 = vehicleValues[28].split(",");
                                    var selectedTonnage1 = vehicleValues[30].split(",");
                                    var ownership = vehicleValues[31];
                                    var selectedTonRateMarket1 = vehicleValues[32].split(",");
                                    var tripRev = "";
                                    var tripRevenue = "";
                                    var tripRevenueTotal = 0;
                                    for(var i = 0; i<selectedTonnageRate1.length; i++){
                                        tripRev = (parseInt(selectedTonnage1[i]) * parseInt(selectedTonnageRate1[i]));
                                        tripRevenueTotal = (parseInt(selectedTonnage1[i]) * parseInt(selectedTonnageRate1[i])+parseInt(tripRevenueTotal));
                                        if(selectedTonnageRate1.length != i+1 ){
                                            tripRevenue += tripRev+",";
                                        }else{
                                            tripRevenue += tripRev;
                                        }
                                    }
                                    if(ownership == 1){
                                        var tripRevenueOne = (parseInt(selectedTonnage1[0]) * parseInt(selectedTonnageRate1[0]));
                                        var tripRevenueTwo = (parseInt(selectedTonnage1[1]) * parseInt(selectedTonnageRate1[1]));
                                        var tripRevenueOneTwo = tripRevenueOne+","+tripRevenueTwo;
                                        document.tripSheet.tripRevenueOne.value = tripRevenue;

                                        //                                        document.tripSheet.tripRevenue.value = (parseInt(selectedTonnage1[0]) * parseInt(selectedTonnageRate1[0])) + (parseInt(selectedTonnage1[1]) * parseInt(selectedTonnageRate1[1]))
                                        //                                        document.tripSheet.tripRevenueDriverBata.value = (parseInt(selectedTonnage1[0]) * parseInt(selectedTonnageRate1[0])) + (parseInt(selectedTonnage1[1]) * parseInt(selectedTonnageRate1[1]))
                                        document.tripSheet.tripRevenue.value = tripRevenueTotal;
                                        document.tripSheet.tripRevenueDriverBata.value = tripRevenueTotal;
                                    }else{
                                        //                                        document.tripSheet.tripRevenue.value = (parseInt(selectedTonnage1[0]) * parseInt(selectedTonRateMarket1[0])) + (parseInt(selectedTonnage1[1]) * parseInt(selectedTonRateMarket1[1]))
                                        //                                        document.tripSheet.tripRevenueDriverBata.value = (parseInt(selectedTonnage1[0]) * parseInt(selectedTonRateMarket1[0])) + (parseInt(selectedTonnage1[1]) * parseInt(selectedTonRateMarket1[1]))
                                        document.tripSheet.tripRevenueOne.value = tripRevenue;
                                        document.tripSheet.tripRevenue.value = tripRevenueTotal;
                                        document.tripSheet.tripRevenueDriverBata.value = tripRevenueTotal;
                                    }


                                }else{
                                    document.tripSheet.tripRevenue.value = parseInt(document.tripSheet.tonnage.value) * parseInt(document.tripSheet.tonnageRate.value);
                                    document.tripSheet.tripRevenueDriverBata.value = parseInt(document.tripSheet.tonnage.value) * parseInt(document.tripSheet.tonnageRateDriverBata.value);
                                }


                                //document.getElementById('DuesDetails').style.display='none';
                                document.getElementById('DuesDetails').innerHTML = "FC Due Day  :"+ vehicleValues[13]+"  FC Due Date : "+ vehicleValues[14]+"insurance Due Day:"+vehicleValues[15]+" insurance Due Date :"+vehicleValues[16]+"permit Due Day:"+vehicleValues[17]+" permit Due Date :"+vehicleValues[18];
                                if(routeval != "" && routeval != null)
                                {
                                    fillTripId();
                                    // fillData();
                                }
                                
                                document.tripSheet.gpno.focus();

                            }else{
                                // alert("out-----detail-->"+detail);
                                document.getElementById('pinkSlipStatus').style.display='block';
                                document.getElementById('DuesDetails').innerHTML="";
                                document.getElementById('pinkSlipStatus').innerHTML = "PinkSlip Detail Not Available";
                                document.tripSheet.orderNo.value = "";
                                document.tripSheet.bags.value = "";
                                document.tripSheet.tonnage.value = "";
                                document.tripSheet.routeId.value = "";
                                document.tripSheet.routeName.value = "";

                                document.tripSheet.vehicleId.value ="";
                                document.tripSheet.driverNameId.value = "";
                                document.tripSheet.companyId.value = "";
                                document.tripSheet.companyName.value  ="";
                                document.tripSheet.productId.value = "";
                                document.tripSheet.productName.value  ="";
                                document.tripSheet.billStatus.value ="";
                                document.tripSheet.tonnageRate.value  ="";
                                document.tripSheet.tonnageRateDriverBata.value  ="";
                                document.tripSheet.pinkSlip.value ="";
                                document.tripSheet.driverNameId.value ="0";
                                document.tripSheet.tripCode.value ="";
                                document.tripSheet.routeKm.value ="";
                                document.tripSheet.kmsOut.value ="";
                                document.tripSheet.tripRevenue.value ="";
                                document.tripSheet.tripRevenueDriverBata.value ="";
                                document.tripSheet.tripRevenueOne.value ="";
                                document.tripSheet.tripRevenueTwo.value ="";
                                document.tripSheet.selectedBillStatus.value ="";
                            }

                        }
                        //                        var  temp = httpRequest.responseText.valueOf();
                        //                        document.tripSheet.orderNo.value=temp;
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }



            function checkTripDate(){

                var one_day = 1000*60*60*24;
                var tripDate = document.getElementById('tripDate').value;
                var today = new Date();
                var hours = today.getHours();
                var minutes = today.getMinutes();
                var seconds = today.getSeconds();
                
                var dd = today.getDate();
                var mm = today.getMonth();
                var yy = today.getFullYear();

                var ddApp = tripDate.split("-")[0];
                var mmApp = (tripDate.split("-")[1]) -1;
                var yyApp = tripDate.split("-")[2];
                var appDate = new Date(yyApp, mmApp, ddApp);
                var curDate = new Date(yy, mm, dd);
                var diff = (appDate.getTime() - curDate.getTime())/one_day;
                if(diff < -1){
                    alert(" Check Trip Date ");
                    document.getElementById('tripDate').value = "";
                     
                }else if (diff == -1){
                    if(hours > 1){
                        alert(" Check Trip Date ");
                        document.getElementById('tripDate').value = "";
                    }
                }
            }
            function amountChence(val){

                if(val == 'netAmount'){
                    document.tripSheet.crossingAmount.value = "";
                }

            }
            function numericVal(s){
                if(!(/^-?\d+$/.test(s))){
                    alert("Enter Numeric Value");
                    document.tripSheet.crossingAmount.value = "";
                    document.tripSheet.crossingAmount.focus();
                    return true;
                }
                return false;
            }
        </script>
        <form name="tripSheet" method="post">
            <%@ include file="/content/common/message.jsp" %>
            <br/>
            <center><h2> Open Trip Sheet</h2></center>
            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                <div align="center" name="pinkSlipStatus" id="pinkSlipStatus" height="20" style="display: none">&nbsp;&nbsp;</div></font>
            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                <div align="center" name="DuesDetails" id="DuesDetails" height="20">&nbsp;&nbsp;</div></font>
            <div style="padding-left: 60px;">
                <table cellpadding="0" cellspacing="4" border="0" width="100%">
                    <tr>
                        <td colspan="5" align="center">
                            <table name="mainTBL" class="open" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr>
                                    <td colspan="5" class="contenthead">Trip Details </td>
                                </tr>
                                <!--                            <table name="mainTBL" class="TableMain" class="open" cellpadding="0" cellspacing="0" align="center" border="0" width="100%">-->
                                <tr class="box-grey">
                                    <td>
                                        <label><font  color="red">*</font>Vehicle No</label>
                                        <span>
                                            <input name="regno" id="regno" type="text" class="form-control" size="20" value="" onfocus="getVehicleNosPinkSlip();" onchange="getPinkSlipList();"  autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false" >
                                            <input name="vehicleId" id="vehicleId" type="hidden" value="" >
                                        </span>
                                    </td>
                                    <td>
                                        <label>Order No</label>
                                        <span>
                                            <input name="orderNo" type="text"  class="form-control"  id="orderNo" readonly />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Party Name</label>
                                        <span>
                                            <input type="hidden" id="companyId" name="companyId" class="form-control" value="">
                                            <input type="text" id="companyName" name="companyName" class="form-control" value="" readonly>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Cust Ton Rate </label>
                                        <span>
                                            <input class="form-control" name="tonnageRate" type="text" id="tonnageRate" readonly />
                                            <input class="form-control" name="tonnageRateDriverBata" type="hidden" id="tonnageRateDriverBata" />
                                            <input class="form-control" name="customerType" type="hidden" id="customerType" />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Route</label>
                                        <span>
                                            <input class="form-control" name="routeName" type="text" id="routeName" readonly />
                                            <input  type="hidden" name="routeId" type="text" id="routeId" />
                                            <c:set var="routeData" value="" />
                                            <c:if test = "${routeList != null}" >
                                                <c:forEach items="${routeList}" var="rl">
                                                    <c:set var="routeData" value="${routeData}~-${rl.routeId}-${rl.routeKm}-${rl.driveBata}-${rl.routeToll}-${rl.routeCode}" />
                                                </c:forEach >
                                            </c:if>
                                            <input type="hidden" name="routeDetails" id="routeDetails" value='<c:out value="${routeData}" />' />
                                        </span>
                                    </td>
                                </tr>
                                <tr class="box-white">
                                    <td>
                                        <label>Pink Slip NO</label>
                                        <span>
                                            <input name="pinkSlip" type="text" class="form-control"  id="pinkSlip" readonly />
                                            <input name="vehicleType" type="hidden" class="form-control"  id="vehicleType" />
                                            <input name="driverType" type="hidden" class="form-control"  id="driverType" />
                                            <input name="vehicleVendorId" type="hidden" class="form-control"  id="vehicleVendorId" />
                                            <input name="driverVendorId" type="hidden" class="form-control"  id="driverVendorId" />
                                        </span>
                                    </td>
                                    <td>
                                        <script type="text/javascript">
                                            function visibleVehicle(){
                                                var x=document.getElementById("vehicleId");
                                                x.disabled=false;
                                            }

                                            
                                            function fillTripId(){
                                                var tempRouteKm = 0;
                                                var tempDriverBata = 0;
                                                var tempRouteToll = 0;
                                                var tempTripCode = 0;
                                                var routeData = document.getElementById("routeDetails").value.split("~");
                                                //alert(document.getElementById("routeId").value.indexOf(','));
                                                if(document.getElementById("routeId").value.indexOf(',') > 0){
                                                    var tempRouteId = document.getElementById("routeId").value.split(",");
                                                    for(var n=0; n<tempRouteId.length; n++){
                                                    var selectedRouteId = "-"+tempRouteId[n]+"-";
                                                        for(var i=1; i < routeData.length; i++){
                                                        if(routeData[i].indexOf(selectedRouteId) >= 0){
                                                            var routeDetail = routeData[i].split("-");
                                                            if(tempRouteKm < routeDetail[2]){
                                                               tempRouteKm =  routeDetail[2];
                                                               tempDriverBata =  routeDetail[3];
                                                               tempRouteToll =  routeDetail[4];
                                                               tempTripCode =  routeDetail[5];
                                                            }
                                                            document.getElementById("routeKm").value = tempRouteKm;
                                                            document.getElementById("kmsOut").value = tempRouteKm;
                                                            document.getElementById("driverBata").value = tempDriverBata;
                                                            document.getElementById("routeToll").value = tempRouteToll;
                                                            document.getElementById("tripCode").value = tempTripCode;
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    var selectedRouteId = "-"+document.getElementById("routeId").value+"-";
                                                    for(var i=1; i < routeData.length; i++){
                                                        if(routeData[i].indexOf(selectedRouteId) >= 0){
                                                            var routeDetail = routeData[i].split("-");
                                                            document.getElementById("routeKm").value = routeDetail[2];
                                                            document.getElementById("kmsOut").value = routeDetail[2];
                                                            document.getElementById("driverBata").value = routeDetail[3];
                                                            document.getElementById("routeToll").value = routeDetail[4];
                                                            document.getElementById("tripCode").value = routeDetail[5];
                                                        }
                                                    }
                                                }
                                                
                                                if(document.getElementById("routeId").value == "Chennai-Ernakulam"){
                                                    document.getElementById("tripCode").value = "67247";
                                                }
                                                if(document.getElementById("routeId").value == "Chennai-Trivandrum"){
                                                    document.getElementById("tripCode").value = "66992";
                                                }
                                                if(document.getElementById("routeId").value == "Chennai-Coimbatore"){
                                                    document.getElementById("tripCode").value = "71848";
                                                }
                                                if(document.getElementById("routeId").value == "Chennai-Udangudi"){
                                                    document.getElementById("tripCode").value = "75601";
                                                }
                                            }


                                            
                                            function fillData(){
                                                //  document.getElementById("tripCode").value='T0003';
                                                var currentDate = new Date();
                                                var day = currentDate.getDate();
                                                var month = currentDate.getMonth() + 1;
                                                if(month ==1){month = "01"}
                                                if(month ==2){month = "02"}
                                                if(month ==3){month = "03"}
                                                if(month ==4){month = "04"}
                                                if(month ==5){month = "05"}
                                                if(month ==6){month = "06"}
                                                if(month ==7){month = "07"}
                                                if(month ==8){month = "08"}
                                                if(month ==9){month = "09"}
                                                var year = currentDate.getFullYear();
                                                var myDate= day + "-" + month + "-" + year;
                                                document.getElementById("departureDate").value=myDate;
                                                document.getElementById("tripDate").value=myDate;
                                                document.getElementById("arrivalDate").value=myDate;
                                                document.getElementById("tripAllowanceDate").value=myDate;
                                                document.getElementById("fuelDate").value=myDate;
                                                day = currentDate.getDate()+1;
                                                myDate= day +  "-" + month + "-" + year;
                                            }
                                        </script>
                                        <label>Route Code</label>
                                        <span>
                                            <input class="form-control" name="tripCode" type="text" id="tripCode" readonly />
                                            <input name="requestCmd" type="hidden"  id="requestCmd" value="save" />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Trip Date</label>
                                        <span>
                                            <input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate" onchange="checkTripDate();" />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Departure Date</label>
                                        <span>
                                            <input name="departureDate" type="text" class="datepicker"  readonly="readonly" id="departureDate" onchange="checkTripDate();" />

                                        </span>
                                    </td>
                                    <td>
                                        <label>Arrival Date</label>
                                        <span>
                                            <input name="arrivalDate" type="text" class="datepicker"  readonly="readonly" id="arrivalDate" onchange="checkTripDate();" />

                                        </span>
                                    </td>
                                </tr>
                                <tr class="box-grey">
                                    <td>
                                        <label>Driver Name</label>
                                        <span>
                                            <select class='form-control' style="width:123px; " id="driverNameId"  name="driverNameId">
                                                <option selected  value="0">---Select---</option>
                                                <c:if test = "${driverName != null}" >
                                                    <c:forEach items="${driverName}" var="dri">
                                                        <option  value='<c:out value="${dri.empId}" />'>
                                                            <c:out value="${dri.empName}" />
                                                        </c:forEach >
                                                    </c:if>
                                            </select>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Bill Status</label>
                                        <span>
                                            <input name="billStatus"  type="text"  class="form-control"  id="billStatus" value=""  readonly />
                                            <input type="hidden" name="selectedBillStatus"  id="selectedBillStatus" value=""/>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Status</label>
                                        <span>
                                            <select name="status" class="form-control" style="width:123px; " id="status">
                                                <option value="Open">Open</option>
                                            </select>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Route KM</label>
                                        <span>
                                            <input name="routeKm" type="text" readonly class="form-control"  id="routeKm" />
                                        </span>
                                    </td>
                                    <td>
                                        <input name="routeToll" type="hidden" readonly class="form-control"  id="routeToll" />
                                        <input name="driverBata" type="hidden" readonly class="form-control"  id="driverBata" />
                                        <label>Product</label>
                                        <span>
                                            <input name="productId" type="hidden"  id="productId" value="" class="form-control" />
                                            <input name="productName" type="text"  id="productName" value="" class="form-control" />
                                        </span>
                                    </td>
                                </tr>
                                <tr class="box-white">
                                    <td>
                                        <label>Exp Revenue from trip</label>
                                        <span>
                                            <input name="tripRevenue" type="text" readonly class="form-control"  id="tripRevenue" />
                                            <input name="tripRevenueDriverBata" type="hidden" readonly class="form-control"  id="tripRevenueDriverBata" />
                                            <input name="twoLpsStatus" type="hidden" readonly class="form-control"  id="twoLpsStatus" />
                                            <input name="tripRevenueOne" type="hidden" readonly class="form-control"  id="tripRevenueOne" />
                                            <input name="tripRevenueTwo" type="hidden" readonly class="form-control"  id="tripRevenueTwo" />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Cleaner Status</label>
                                        <span>
                                            <select name="cleanerStatus" class='form-control' style="width:123px; "  id="cleanerStatus" >
                                                <option value="1">Yes</option>
                                                <option value="0" selected>No</option>
                                            </select>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Trip Type</label>
                                        <span>
                                            <select name="tripType" class="form-control"  style="width:123px; " id="tripType">
                                                <option value="1">Loaded Trip</option>
                                                <option value="0">Empty Trip</option>
                                            </select>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Bags</label>
                                        <span>
                                            <input name="bags" type="text"  class="form-control"  id="bags" readonly />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Tonnage</label>
                                        <span>
                                            <input name="tonnage" type="text"  class="form-control"  id="tonnage" readonly />
                                        </span>
                                    </td>
                                </tr>
                                <tr class="box-grey">
                                    <td>
                                        <label>Extra Km &nbsp;&nbsp;&nbsp;&nbsp; Total Km</label>
                                        <span>
                                            <input name="extraKm" type="text" class="form-control"  id="extraKm"  value="0" onkeyup="totalKmsCalc(this.value)" onclick="resetValue(this)" style="width: 70px"/>
                                            <input name="kmsOut" type="text" class="form-control"  id="kmsOut"  value="0" style="width: 70px" readonly/>
                                            <script type="text/javascript">
                                                function resetValue(obj){
                                                    if(obj.value == 0){
                                                        obj.value = "";
                                                    }
                                                }
                                                function setValue(){
                                                    alert("obj");
                                                    //                                                    if(obj.value == ""){
                                                    //                                                    obj.value = 0;
                                                    //                                                    }
                                                }
                                                function totalKmsCalc(value){
                                                    if(value != ""){
                                                        document.getElementById('kmsOut').value=parseInt(document.getElementById('routeKm').value)+parseInt(value);
                                                    }else{
                                                        document.getElementById('kmsOut').value=parseInt(document.getElementById('routeKm').value);
                                                    }
                                                }
                                            </script>
                                        </span>
                                    </td>
                                    <td>
                                        <label>G.P.No</label>
                                        <span>
                                            <input name="gpno" type="text" class="form-control"  id="gpno" value="" />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Inovice No</label>
                                        <span>
                                            <input name="invoiceNo" type="text" class="form-control"  id="invoiceNo"   />
                                        </span>
                                    </td>
                                    <td>
                                        <label>Crossing Type</label>
                                        <span>
                                            <select name="crossingType" class="form-control" style="width:123px; " id="crossingType" onchange="amountChence(this.value);">
                                                <option value="percentage">Percentage</option>
                                                <option value="netAmount">Net Amount</option>
                                            </select>
                                        </span>
                                    </td>
                                    <td>
                                        <label>Crossing Amount</label>
                                        <span>
                                            <input name="crossingAmount" type="text" class="form-control"  id="crossingAmount" value="8"  onblur="numericVal(this.value);" />
                                        </span>
                                    </td>
                                    <!--                                    <td colspan="2">
                                                                            &nbsp;
                                                                        </td>-->
                                </tr>
                                <tr>
                                    <td height="40px"><div id="runKm"></div></td>
                                </tr>
                            </table>


                            <table class="summary" cellpadding="0" cellspacing="0" align="center" border="0" >
                                <tr>
                                    <th colspan="2">Summary</th>
                                </tr>
                                <tr class="summary-grey">
                                    <td>Total Fuel Amount</td>
                                    <td>
                                        <input name="totalFuelAmount" type="text" class="form-control" value="0" id="totalFuelAmount"  readonly  />
                                    </td>
                                </tr>
                                <tr class="summary-white">
                                    <td>Total Expense</td>
                                    <td>
                                        <input name="totalExpenses" type="text" class="form-control" value="0" id="totalExpenses" readonly  />
                                    </td>
                                </tr>
                                <tr class="summary-grey">
                                    <td>Balance Amount</td>
                                    <td>
                                        <input name="balanceAmount" type="text" class="form-control" value="0" id="balanceAmount" readonly  />
                                    </td>
                                </tr>
                            </table>

                            <table class="total" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr class="summary-grey">
                                    <td>Total Allowances </td>
                                    <td>
                                        <input name="totalAllowance" type="text" class="form-control" value="0" id="totalAllowance" readonly  />
                                    </td>
                                </tr>
                            </table>
                            <table class="total" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr class="summary-grey">
                                    <td>Total Liters</td>
                                    <td>
                                        <input name="totalFuelLtrs" type="text" class="form-control" value="0" id="totalFuelLtrs" onblur="totalKmsFunc();sumExpenses();setBalance();"  readonly  />
                                    </td>
                                </tr>
                            </table>

                            <div style="clear:both; "></div>
                            <div style="float:left; width:100%; margin-top:-15px;">
                                <div class="box-head">Trip Allowances</div>
                                <div class="scroll-y">

                                    <table style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" align="left" border="0" id="allowanceTBL" name="allowanceTBL">
                                        <tr >
                                            <th  class="contenthead">S No&nbsp;</th><th class="contenthead">Location</th><th class="contenthead">Date</th><th class="contenthead">Amount</th><th class="contenthead">Paid Person</th><th class="contenthead">Remarks</th><th class="contenthead">&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <td >1</td>
                                            <td>
                                                <select class="form-control" style="width:100px; " id="stageId"  name="stageId" >
                                                    <!--                                            <option selected  value="0">---Select---</option>-->
                                                    <c:if test = "${opLocation != null}" >
                                                        <c:forEach items="${opLocation}" var="opl">
                                                            <option  value='<c:out value="${opl.locationId}" />'> <c:out value="${opl.locationName}" />
                                                            </c:forEach >
                                                        </c:if>
                                                </select>
                                            </td>
                                            <td>
                                                <input name="tripAllowanceDate" type="text" class="datepicker"  id="tripAllowanceDate"  />
                                            </td>
                                            <td>
                                                <input name="tripAllowanceAmount" type="text" class="form-control"  value="0" id="tripAllowanceAmount"  onblur="sumAllowanceAmt();" OnKeyPress="NumericOnly()"  />
                                                <script type="text/javascript">
                                                    function sumAllowanceAmt(){
                                                        var sumAmt=0;
                                                        var RevenueAmt=0;
                                                        var totAmt=0;
                                                        var vehicleType =0;
                                                        sumAmt= document.getElementsByName('tripAllowanceAmount');
                                                        RevenueAmt= document.tripSheet.tripRevenue.value;
                                                        vehicleType = document.tripSheet.vehicleType.value;
                                                        for(i=0;i<sumAmt.length;i++){
                                                            totAmt=parseInt(totAmt)+parseInt(sumAmt[i].value);
                                                            document.getElementById('totalAllowance').value=parseInt(totAmt);
                                                        }
                                                        // alert(vehicleType);
                                                        if(vehicleType == 2)
                                                        {
                                                            var PERCENTAGE =(totAmt/RevenueAmt)*100
                                                            if(PERCENTAGE > 60)
                                                            {
                                                                alert("Advance Not More Than 60%");
                                                                document.tripSheet.tripAllowanceAmount.value=""
                                                                document.tripSheet.totalAllowance.value=""
                                                                document.tripSheet.tripAllowanceAmount.focus();
                                                            }
                                                        }
                                                    }


                                                    function sumExpenses(){
                                                        var sumAmt=0;
                                                        //sumAmt= parseInt(document.getElementById('totalAllowance').value)+parseInt(document.getElementById('totalFuelAmount').value);
                                                        sumAmt= parseInt(document.getElementById('totalFuelAmount').value);
                                                        document.getElementById('totalExpenses').value=parseInt(sumAmt);
                                                    }
                                                    function setBalance(){
                                                        var sumAmt=0;
                                                        sumAmt= parseInt(document.getElementById('totalAllowance').value)-parseInt(document.getElementById('totalFuelAmount').value);
                                                        document.getElementById('balanceAmount').value=parseInt(sumAmt);
                                                    }

                                                    function fuelPrice(rowVal)
                                                    {
                                                        //alert("----->"+rowVal);
                                                        var fuelRate = document.getElementById('bunkName'+rowVal).value;
                                                        //alert("=====>"+fuelRate);
                                                        var temp = fuelRate.split('~');
                                                        fuelRate = temp[3];
                                                        //alert(fuelRate);
                                                        
                                                        var fuelPrice = parseFloat(fuelRate);
                                                        var fuelLters = parseFloat(document.getElementById("fuelLtrs"+rowVal).value);
                                                        //  alert("fuelLters==>"+fuelLters);
                                                        var fuelAmount = parseFloat(fuelPrice * fuelLters).toFixed(2);
                                                        //  alert("fuelAmount==>"+fuelAmount);
                                                        document.tripSheet.fuelAmount.value = fuelAmount;
                                                        sumFuel();
                                                    }
                                                    function fuelPriceOLD()
                                                    {
                                                        var bunkID = document.getElementById("bunkName").value;
                                                        var temp = bunkID.split('~');
                                                        bunkID = temp[0];
                                                        // alert("bunkID===>"+bunkID);
                                                        if(bunkName != "") {
                                                            var url = "/throttle/getFuelPriceDetail.do?bunkID="+bunkID;
                                                            url = url+"&sino="+Math.random();
                                                            if (window.ActiveXObject)  {
                                                                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                                            }
                                                            else if (window.XMLHttpRequest)  {
                                                                httpRequest = new XMLHttpRequest();
                                                            }
                                                            httpRequest.open("GET", url, true);
                                                            httpRequest.onreadystatechange = function() { processRequestFuel(); } ;
                                                            httpRequest.send(null);

                                                        }
                                                    }
                                                    function processRequestFuel()
                                                    {
                                                        if (httpRequest.readyState == 4)
                                                        {
                                                            if(httpRequest.status == 200)
                                                            {
                                                                if(httpRequest.responseText.valueOf()!=""){
                                                                    var detail = httpRequest.responseText.valueOf();
                                                                    // alert(detail);
                                                                    if(detail != "null"){
                                                                        //   alert("in-----detail-->"+detail);
                                                                        var fuelPrice = parseInt(detail);
                                                                        var fuelLters = parseInt(document.getElementById("fuelLtrs").value);
                                                                        //  alert("fuelLters==>"+fuelLters);
                                                                        var fuelAmount = fuelPrice * fuelLters
                                                                        //  alert("fuelAmount==>"+fuelAmount);
                                                                        document.tripSheet.fuelAmount.value = fuelAmount;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                                                            }
                                                        }
                                                    }



                                                </script>
                                            </td>
                                            <td>

                                                <input name="AllowancepaidName" type="text" class="form-control" id="AllowancepaidName" value="<%= session.getAttribute("userName")%>" readonly/>
                                                <input type="hidden" name="tripAllowancePaidBy" id="tripAllowancePaidBy" value="<%= session.getAttribute("userId")%>"/>
                                                <%--       <select class='form-control' style="width:123px; " id='tripAllowancePaidBy' name='tripAllowancePaidBy' >
                                                                <option selected  value="0">---Select---</option>
                                                                <c:if test = "${paidBy != null}" >
                                                                    <c:forEach items="${paidBy}" var="paid">
                                                                        <option  value='<c:out value="${paid.issuerId}" />'> <c:out value="${paid.issuerName}" />
                                                                        </c:forEach >
                                                                    </c:if>
                                                            </select>--%>
                                            </td>
                                            <td>
                                                <input name="tripAllowanceRemarks" style="width:70px;" type="text" class="form-control" id="tripAllowanceRemarks"  />
                                                <input type="hidden" name="tripAllowanceId" id="tripAllowanceId" />
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="center">

                                                <input type="button" name="add" value="Add" onclick="addAllowanceRow()" id="add" class="button" />
                                                &nbsp;&nbsp;&nbsp;

                                                <input type="button" name="delete" value="Delete" onclick="delAllowanceRow()" id="delete" class="button" />

                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <div id="fuelDiv" style="display:block;">
                                <div style="float:left; width:100%; margin-top:10px;">
                                    <div class="box-head">Fuel Details</div>
                                    <div class="scroll-y">

                                        <table style="width: 100%; left: 30px;" class="border" cellpadding="0"  cellspacing="0" align="center" border="0" id="fuelTBL" name="fuelTBL">
                                            <tr >
                                                <th class="contenthead" >S No</th>
                                                <th class="contenthead" >Bunk Name</th>
                                                <!--                                    <th class="contenthead">Place</th>-->
                                                <th class="contenthead">Date</th>
                                                <th class="contenthead">Ltrs</th>
                                                <th class="contenthead">Amount</th>
                                                <th class="contenthead">Person</th>
                                                <th class="contenthead">Remarks</th>
                                                <th class="contenthead">&nbsp;</th>
                                            </tr>
                                            <tr >
                                                <td> 1</td>
                                                <td>
                                                    <!--<input name="bunkName" type="text" class="form-control" id="bunkName"  />-->
                                                    <select style="width:130px;" id='bunkName1' name='bunkName' >
                                                        <option selected  value="0">---Select---</option>
                                                        <c:if test = "${bunkList != null}" >
                                                            <c:forEach items="${bunkList}" var="bunk">
                                                                <option  value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                                </c:forEach >
                                                            </c:if>
                                                    </select>
                                                </td>
                                                <!--<td></td>-->

                                                <td>
                                                    <input name="bunkPlace" type="hidden" class="form-control" id="bunkPlace"  />
                                                    <input name="fuelDate" id="fuelDate" type="text" class="datepicker" value="" id="fuelDate"  style="width:120px;" />
                                                </td>
                                                <td>
                                                    <input name="fuelLtrs" type="text" class="form-control" value="0" id="fuelLtrs1"  onblur="sumFuel();" onchange="fuelPrice(1);" style="width:80px;" />
                                                </td>
                                                <td>
                                                    <input name="fuelAmount" readonly type="text" class="form-control" value="0" id="fuelAmount1"  onblur="sumFuel();"  style="width:80px;" readonly />
                                                </td>

                                                <td>
                                                    <input name="fuelFilledName" type="text" class="form-control" id="fuelFilledName" value="<%= session.getAttribute("userName")%>" />
                                                    <input name="fuelFilledBy" type="hidden" class="form-control" id="fuelFilledBy" value="<%= session.getAttribute("userId")%>" />
                                                </td>
                                                <td>
                                                    <input name="fuelRemarks" style="width:55px;" type="text" class="form-control" id="fuelRemarks"  />
                                                    <input type="hidden" name="HfId" id="HfId" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" align="center">
                                                    <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="button" />
                                                </td>
                                            </tr>
                                            <script>
                                                var poItems = 0;
                                                var rowCount='';
                                                var sno='';
                                                var snumber = '';

                                                function addFuelRow()
                                                {
                                                    var currentDate = new Date();
                                                    var day = currentDate.getDate();
                                                    var month = currentDate.getMonth() + 1;
                                                    var year = currentDate.getFullYear();
                                                    var myDate= day + "-" + month + "-" + year;

                                                    if(sno < 9){
                                                        sno++;
                                                        var tab = document.getElementById("fuelTBL");
                                                        var rowCount = tab.rows.length;

                                                        snumber = parseInt(rowCount)-1;
                                                        //                    if(snumber == 1) {
                                                        //                        snumber = parseInt(rowCount);
                                                        //                    }else {
                                                        //                        snumber++;
                                                        //                    }

                                                        var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                        newrow.height="30px";
                                                        // var temp = sno1-1;
                                                        var cell = newrow.insertCell(0);
                                                        var cell0 = "<td><input type='hidden'  name='fuelItemId' /> "+snumber+"</td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(1);
                                                        //                                        cell0 = "<td class='text2'><input name='bunkName' type='text' class='form-control'     id='bunkName' class='Textbox' /></td>";
                                                        cell0 = "<td class='text2'><select class='form-control' style='width:100px;' id='bunkName"+snumber+"' style='width:123px'  name='bunkName'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(2);
                                                        cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate"+snumber+"' type='text' class='datepicker' style='width:120px;' value='"+myDate+"' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(3);
                                                        cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice("+snumber+");' type='text' class='form-control' id='fuelLtrs"+snumber+"' value='0' style='width:80px;' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML     = cell0;

                                                        cell = newrow.insertCell(4);
                                                        cell0 = "<td class='text1'><input name='fuelAmount' readonly onBlur='sumFuel();' type='text' class='form-control' value='0' style='width:80px;' id='fuelAmount"+snumber+"' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;



                                                        cell = newrow.insertCell(5);
                                                        cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden' class='form-control' id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' /><input name='fuelFilledName' type='text' style='width:55px;' class='form-control' id='fuelFilledName' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(6);
                                                        cell0 = "<td class='text1'><input name='fuelRemarks' style='width:55px;' type='text' class='form-control' id='fuelRemarks' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(7);
                                                        var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell1;
                                                        // rowCount++;


                                                        $( ".datepicker" ).datepicker({
                                                            /*altField: "#alternate",
                       altFormat: "DD, d MM, yy"*/
                                                            changeMonth: true,changeYear: true
                                                        });
                                                    }
                                                }


                                                function delFuelRow() {
                                                    try {
                                                        var table = document.getElementById("fuelTBL");
                                                        rowCount = table.rows.length-1;
                                                        for(var i=2; i<rowCount; i++) {
                                                            var row = table.rows[i];
                                                            var checkbox = row.cells[7].childNodes[0];
                                                            if(null != checkbox && true == checkbox.checked) {
                                                                if(rowCount <= 1) {
                                                                    alert("Cannot delete all the rows");
                                                                    break;
                                                                }
                                                                table.deleteRow(i);
                                                                rowCount--;
                                                                i--;
                                                                sno--;
                                                                // snumber--;
                                                            }
                                                        }sumFuel();
                                                    }catch(e) {
                                                        alert(e);
                                                    }
                                                }


                                                function sumFuel(){
                                                    var totFuel=0;
                                                    var totltr=0;
                                                    var totAmount=0;
                                                    var totAmt=0;
                                                    totFuel= document.getElementsByName('fuelLtrs');

                                                    totAmount= document.getElementsByName('fuelAmount');
                                                    for(i=0;i<totFuel.length;i++){
                                                        totltr=parseInt(totltr)+parseInt(totFuel[i].value);
                                                    }
                                                    document.getElementById('totalFuelLtrs').value=parseInt(totltr);
                                                    for(i=0;i<totAmount.length;i++){
                                                        totAmt=parseInt(totAmt)+parseInt(totAmount[i].value);
                                                    }
                                                    document.getElementById('totalFuelAmount').value=parseInt(totAmt);
                                                }
                                           

                                                function totalKmsFunc(){
                                                    //document.getElementById('totalKms').value=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                                    document.getElementById('totalKms').value = 0;
                                                }
                                               

                                            </script>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both; "></div>



                        </td>
                    </tr>

                    <!--                    <tr align="center">
                                            <td align="right" class="TableColumn">
                                            </td>
                                            <td align="left" class="TableColumn">
                                            </td>
                                            <td class="TableRowNew">
                                            </td>
                                            <td align="right" class="TableColumn">
                                                Total Allowance
                                            </td>
                                            <td align="left" class="TableColumn">
                                                <input name="totalAllowance" type="text" class="form-control" value="0" id="totalAllowance" readonly  />
                                            </td>
                                            <td class="TableRowNew">
                                            </td>
                                        </tr>-->


                </table>
                <center>
                    <input type="button" id="saveButton" class="button" value="Save" onclick="submitPage();" />
                    <!--                    <input type="submit" name="save" value="Save" onclick="return Mandatory();" id="save" class="button" style="width:120px;" />-->
                </center>
            </div>
            <script type="text/javascript">
                //                function getVehicleNos(){
                //                    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
                //                }
                function getVehicleNosPinkSlip(){
                    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNoPinkSlip.do?"));

                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
