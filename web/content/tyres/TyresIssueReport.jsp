<%-- 
    Document   : TyresIssueReport
    Created on : Mar 27, 2012, 1:16:45 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link href="/throttle/css/parveen.css" rel="stylesheet" title="PAPL">
  <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>
<title></title>
</head>
<body>

<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td height="30" align="left" style="padding-right:5px; "><span class="pathText" >Tyres >> Issue Report</span> </td>
<Td width="75%">
</Td>
</tr>
</table>
 

            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Tyres Issue Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30"><font color="red">*</font>Location</td>
        <td height="30">
            <select  class="textbox" name="companyId" style="width:125px">
                <option value="">-select-</option>
                <option value="1024">ASHOK LEYLAND S.P</option>
                <option value="1019">CTS SP</option>
                <option value="1012">Egmore SP</option>
                <option value="1015">HYUNDAI SP</option>
                <option value="1011">Madhavaram SP</option>
                <option value="1017">ORAGADAM SP</option>
                <option value="1025">RCZONE SP</option>
                <option value="1022">Red Hills SP</option>
                <option value="1026">VOLVO</option>
            </select>
        </td>

        <td height="30">&nbsp;&nbsp;MFR</td>
        <td height="30"><select name="mfrId" class="textbox">
                        <option value="">-select-</option>
                        <option value="1022">Apollo</option>
                        <option value="1028">Bridgestone</option>
                        <option value="1001">MRF</option>
                    </select>
        </td>
        <td width="114" height="30">Counter No</td>
        <td width="172" height="30"><input name="counterId" type="text" class="textbox" value="" size="20">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;PAPL Code</td>
        <td height="30"><input name="paplCode" type="text" class="textbox" value="" size="20"></td>
         <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
        <td width="172" height="30"><input name="itemName" id="itemName" type="text"  class="textbox" value=""></td>
        <td>&nbsp;&nbsp;Vehicle No</td>
        <td height="30"><input name="regNo" id="regno" type="text" class="textbox" value="" size="20"></td>
        </tr>
    <tr>

        <td width="80" height="30"><font color="red">*</font>From Date</td>
        <td width="182"><input name="fromDate" type="text" class="textbox" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.fromDate,'dd-mm-yyyy',this)"/></td>
        <td width="148"><font color="red">*</font>To Date</td>
        <td width="172" height="30"><input name="toDate" type="text" class="textbox" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.toDate,'dd-mm-yyyy',this)"/></td>
        <td width="80" height="30">Workorder No</td>
        <td width="80" height="30"><input type="text" name="rcWorkorderId" class="textbox"></td>
    </tr>
    <tr>

        <td width="80" height="30">&nbsp;</td>
        <td width="182">&nbsp; </td>
        <td width="148">&nbsp;</td>
        <td width="172" height="30">&nbsp;
        </td>
        <td width="80" height="30">&nbsp;</td>
        <td width="80" height="30"><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
                <input type="hidden" value="" name="reqfor"></td>
    </tr>
    </table>


    </div></div>
    </td>
    </tr>
    </table>       
<h2>Billed</h2>
     <table width="1566" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <tr>
        <td class="contentsub">Sno</td><td class="contentsub">MFR</td><td class="contentsub">Model</td><td class="contentsub">REG NO</td><td class="contentsub">JOBCARD NO</td><td class="contentsub">RCWO NO</td><td class="contentsub">COUNTER NO</td><td class="contentsub">MRSNo</td><td class="contentsub">MANUALMRSNo</td><td class="contentsub"> 	MRSDATE</td><td class="contentsub">IssueDATE</td><td class="contentsub">PAPL CODE </td><td class="contentsub">ITEM NAME</td><td class="contentsub">Tech Name </td><td class="contentsub">User</td><td class="contentsub"> Iss Qty</td><td class="contentsub">Ret Qty</td><td class="contentsub">Net Qty </td><td class="contentsub">BuyPrice</td><td class="contentsub">SellPrice </td><td class="contentsub">Profit</td><td class="contentsub">Tax</td><td class="contentsub">NettProfit</td>
    </tr>
    <tr>
    <td class="text1"> 1</td><td class="text1">	Apollo</td><td class="text1">ALL MODEL</td><td class="text1">TN21AC 4626</td><td class="text1">1064</td><td class="text1">0</td><td class="text1">-	</td><td class="text1">1201</td><td class="text1">PO 1060</td><td class="text1">2011-05-07</td><td class="text1">2011-05-16 11:41:42.0</td><td class="text1">28011</td><td class="text1">	APOLLO TYRE 10X20</td><td class="text1">DEVENDRAN </td><td class="text1">2.00</td><td class="text1"> 	0.00</td><td class="text1">koteeswar </td><td class="text1"> 	2.00</td><td class="text1"> 	14219.00</td><td class="text1">	15475.50</td><td class="text1">2513.00</td><td class="text1">12.50</td><td class="text1">2148.62</td>
    </tr>
     </table>


<h2>Un Billed</h2>
     <table width="1566" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <tr>
        <td class="contentsub">Sno</td><td class="contentsub">MFR</td><td class="contentsub">Model</td><td class="contentsub">REG NO</td><td class="contentsub">JOBCARD NO</td><td class="contentsub">RCWO NO</td><td class="contentsub">COUNTER NO</td><td class="contentsub">MRSNo</td><td class="contentsub">MANUALMRSNo</td><td class="contentsub"> 	MRSDATE</td><td class="contentsub">IssueDATE</td><td class="contentsub">PAPL CODE </td><td class="contentsub">ITEM NAME</td><td class="contentsub">Tech Name </td><td class="contentsub">User</td><td class="contentsub"> Iss Qty</td><td class="contentsub">Ret Qty</td><td class="contentsub">Net Qty </td><td class="contentsub">BuyPrice</td><td class="contentsub">SellPrice </td><td class="contentsub">Profit</td><td class="contentsub">Tax</td><td class="contentsub">NettProfit</td>
    </tr>
    <tr>
    <td class="text1"> 1</td><td class="text1">	Apollo</td><td class="text1">ALL MODEL</td><td class="text1">TN21AC 1935</td><td class="text1">1067</td><td class="text1">0</td><td class="text1">-	</td><td class="text1">1201</td><td class="text1">PO 1062</td><td class="text1">2011-05-07</td><td class="text1">2011-05-16 11:41:42.0</td><td class="text1">28011</td><td class="text1">	APOLLO TYRE 10X20</td><td class="text1">DEVENDRAN </td><td class="text1">2.00</td><td class="text1"> 	0.00</td><td class="text1">koteeswar </td><td class="text1"> 	2.00</td><td class="text1"> 	14219.00</td><td class="text1">	15475.50</td><td class="text1">2513.00</td><td class="text1">12.50</td><td class="text1">2148.62</td>
    </tr>
     </table>

<br>
<br>
<%
String billedIssue = "<chart yAxisName='Values' caption='Billed' numberPrefix='SAR ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='Sales' value='15475.50' color='357EC7' />  "+
        "<set label='Profit' value='2513' color='F87431' /> "+
        "<set label='Net Profit' value='2148.62' color='3EA99F' /> "+
        "</chart> ";
String unBilledIssue = "<chart yAxisName='Values' caption='Un Billed' numberPrefix='SAR ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='Sales' value='15475.50' color='357EC7' />  "+
        "<set label='Profit' value='2513' color='F87431' /> "+
        "<set label='Net Profit' value='2148.62' color='3EA99F' /> "+
        "</chart> ";

String summaryIssue = "<chart yAxisName='Values' caption='Overall' numberPrefix='SAR ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='Sales' value='30951' color='357EC7' />  "+
        "<set label='Profit' value='5026' color='F87431' /> "+
        "<set label='Net Profit' value='4297.24' color='3EA99F' /> "+
        "</chart> ";
%>


<table class="table2" width="90%" cellpadding="0" cellspacing="0">
    <tr>
        <td><table width="100%" cellpadding="5">
                   <tr><td class="contentsub" align="left" colspan="2">Billed</td></tr>
                    <tr>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" > <b>SAR: 15475.50</b></td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" ><b>SAR: 2513.00</b>  </td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" ><b>SAR: 2148.62</b>  </td>
                    </tr>
</table></td>

        <td>
            <table width="100%" cellpadding="5">
                   <tr><td class="contentsub" align="left" colspan="2">Un Billed</td></tr>
                    <tr>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" > <b>SAR: 15475.50</b></td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" ><b>SAR: 2513.00</b>  </td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" ><b>SAR: 2148.62</b>  </td>
                    </tr>
        </table>
        </td>


        <td><table width="100%" cellpadding="5">
                   <tr><td class="contentsub" align="left" colspan="2">Summary</td></tr>
                    <tr>
                    <td class="text2" height="30"><b>Nett Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" > <b>SAR: 30951.00</b></td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" > <b>SAR: 5026.00</b>  </td>
                    </tr>

                    <tr>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" ><b>SAR: 4297.24</b>  </td>
                    </tr>
</table></td>
    </tr>
</table>


<table class="table2" width="90%">
    <tr>
        <td>

        </td><td>
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=billedIssue %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="280" />
                                <jsp:param name="chartHeight" value="225" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    
        </td><td>
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=unBilledIssue %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="280" />
                                <jsp:param name="chartHeight" value="225" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </td>
    <td>
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=summaryIssue %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="280" />
                                <jsp:param name="chartHeight" value="225" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </td>
    </tr>
</table>

     

    </body>
</html>

