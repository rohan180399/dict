<%-- 
    Document   : CNoteReportExcel
    Created on : Dec 25, 2018, 5:47:14 PM
    Author     : entitle
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
<%
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:MM:SS");
        Date date = new Date();
        String fDate = "";
        String tDate = "";
        if (request.getAttribute("fromDate") != null) {
            fDate = (String) request.getAttribute("fromDate");
        } else {
            fDate = dateFormat.format(date);
        }
        if (request.getAttribute("toDate") != null) {
            tDate = (String) request.getAttribute("toDate");
        } else {
            tDate = dateFormat.format(date);
        }

%>

<form name="accountReceivable" action=""  method="post">
    
<%
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
        //System.out.println("Current Date: " + ft.format(dNow));
        String curDate = ft.format(dNow);
        String expFile = "CustomerStatementReport-" + curDate + ".xls";

        String fileName = "attachment;filename=" + expFile;
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-disposition", fileName);
 %>

            <br>
            <br>
            <br>
                        <c:if test = "${customerStatementHistory != null}" >
                            <table class="table table-info mb30 table-hover" align="left"  id="table1" style="width:50%">
                                <tr><td><b>DICT - Sonepat - 131101, Haryana. INDIA</b></td></tr>
                                <tr><td><b>Customr Name  : <c:out value="${custName}"/></b></td></tr>
                                <tr><td><b>Customer Code : <c:out value="${custCode}"/></b></td></tr>
                                <tr><td><b>Printed as On : <%=tDate%></b></td></tr>                                
                            </table>   
                            <table class="table table-info mb30 table-hover"  id="table" style="width:100%">
                                <thead>
                                    <tr >
                                        <th>S.NO</th>
                                        <th>CYCLE TYPE</th>
                                        <th>DATE</th>
                                        <th>TP</th>
                                        <th>BILL NO</th>
                                        <th>DOC NO</th>
                                        <th>GR NO</th>
                                        <th>CTR NO</th>
                                        <th>CTR SIZE</th>
                                        <th>NO OF CTR</th>
                                        <th>FRT.</th>
                                        <th>TOLL.</th>
                                        <th>GREEN.</th>
                                        <th>DETENT.</th>
                                        <th>OTH</th>
                                        <th>WGT.</th>
                                        <th>DEBIT AMT</th>                                        
                                        <th>CREDIT</th>
                                        <th>TDS</th>                                        
                                        <th>IGST</th>
                                        <th>CGST</th>
                                        <th>SGST</th>
                                        <th>UGST</th>
                                        <th>BALANCE</th>
                                    </tr>
                                </thead>
                                <% int index = 0,sno = 1;%>
                                <c:forEach items="${customerStatementHistory}" var="pay">
                                    <tr>
                                        <td align="center"><%=sno++%></td>                                        
                                        <td align="center"><c:out value="${pay.transactionName}"/></td>
                                        <td align="center"><c:out value="${pay.transactionDate}"/></td>
                                        <td align="center"><c:out value="${pay.transactionType}"/></td>

                                        <c:if test = "${pay.transactionName == 'OpeningBalance'}" >
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>                                          
                                            <td align='center'>-</td>
                                            <td align='center'>0</td>
                                            <td align="center"><c:out value="${pay.creditAmount}"/></td>                                                                                   
                                            <td align='center'>-</td>                                            
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>                                        
                                        </c:if>
                                            
                                          

                                        <c:if test = "${pay.transactionName == 'Blocked'}" >
                                            <td align="center"><c:out value="${pay.consignmentNoteNo}"/></td>
                                            <td align="center">-</td>
                                            <td><c:out value="${pay.orderGrNos}"/></td>
                                            <td><c:out value="${pay.orderCntNo}"/></td>
                                            <td><c:out value="${pay.orderCntType}"/></td>
                                            <td><c:out value="${pay.orderCntSize}"/></td>
                                            <td align="center"><c:out value="${pay.freightAmount}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align="center"><c:out value="${pay.debitAmount}"/></td>
                                            <td align='center'>0</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                        </c:if>


                                        <c:if test = "${pay.transactionName == 'Invoice' || pay.transactionName == 'Released'}" >
                                            <td align="center"><c:out value="${pay.invoiceNo}"/></td>                                        
                                            <td align="center"><c:out value="${pay.consignmentNoteNo}"/></td>                                        
                                            <td><c:out value="${pay.grNo}"/></td>
                                            <td><c:out value="${pay.containerNo}"/></td>
                                            <td><c:out value="${pay.containerTypeName}"/></td>
                                            <td><c:out value="${pay.cntSize}"/></td>
                                            
                                            <c:set var="invExp" value="${pay.invExpense}"/>                                        
                                            <%
                                                String inv = (String) pageContext.getAttribute("invExp");
                                                String[] Exp = inv.split("~");
                                            %>
                                            <c:if test = "${pay.transactionName!='Released'}" >                                        
                                                <td align="center"><%=Exp[0]%></td>
                                                <td><%=Exp[1]%></td>
                                                <td><%=Exp[2]%></td>
                                                <td><%=Exp[3]%></td>
                                                <td><%=Exp[4]%></td>
                                                <td><%=Exp[5]%></td>   
                                                <td align="center"><c:out value="${pay.debitAmount}"/></td>
                                                <td align='center'>0</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                            </c:if>
                                            <c:if test = "${pay.transactionName=='Released'}" >                                        
                                                <td align="center">-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>                                                
                                                <td align='center'>-</td>
                                                <td align='center'>0</td>                                                
                                                <td align="center"><c:out value="${pay.creditAmount}"/></td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                                <td align='center'>-</td>
                                            </c:if>
                                        </c:if>

                                        <c:if test = "${pay.transactionName == 'Supp.Invoice'}" >
                                            <td align="center"><c:out value="${pay.suppInvoiceNo}"/></td>
                                            <td align="center"><c:out value="${pay.invoiceNo}"/></td>                                        
                                            <td><c:out value="${pay.suppgrNo}"/></td>
                                            <td><c:out value="${pay.suppcntNo}"/></td>
                                            <td><c:out value="${pay.suppcontTypeName}"/></td>                                            
                                            <td><c:out value="${pay.suppcntSize}"/></td>                                            
                                            <c:set var="supExpense" value="${pay.supExpense}"/>
                                            <%
                                                String spinv = (String) pageContext.getAttribute("supExpense");
                                                String[] spExp = spinv.split("~");
                                            %>
                                            <td align="center"><%=spExp[0]%></td>
                                            <td><%=spExp[1]%></td>
                                            <td><%=spExp[2]%></td>
                                            <td><%=spExp[3]%></td>
                                            <td><%=spExp[4]%></td>
                                            <td><%=spExp[5]%></td>
                                            <td align="center"><c:out value="${pay.debitAmount}"/></td>
                                            <td align='center'>0</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                        </c:if>
                                            
                                         <c:if test = "${pay.transactionName == 'CreditNote'}" >
                                            <td align="center"><c:out value="${pay.creditNo}"/></td>
                                            <td align="center"><c:out value="${pay.invoiceNo}"/></td>                                        
                                            <td><c:out value="${pay.creditGrNos}"/></td>
                                            <td><c:out value="${pay.creditCntNo}"/></td>
                                            <td><c:out value="${pay.creditCntType}"/></td>
                                            <td><c:out value="${pay.creditCntSize}"/></td>
                                            <c:set var="creditExpense" value="${pay.creditExpense}"/>
                                            <%
                                                String credit = (String) pageContext.getAttribute("creditExpense");
                                                String[] crExp = credit.split("~");
                                            %>
                                            <td align="center"><%=crExp[0]%></td>
                                            <td><%=crExp[1]%></td>
                                            <td><%=crExp[2]%></td>
                                            <td><%=crExp[3]%></td>
                                            <td><%=crExp[4]%></td>
                                            <td><%=crExp[5]%></td>
                                            <td align='center'>0</td>
                                            <td align="center"><c:out value="${pay.creditAmount}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                        </c:if>   

                                        <c:if test = "${pay.transactionName == 'SuppCreditNote'}" >
                                            <td align="center"><c:out value="${pay.suppcreditNo}"/></td>
                                            <td align="center"><c:out value="${pay.suppInvoiceNo}"/></td>                                        
                                            <td><c:out value="${pay.suppcreditGrNos}"/></td>
                                            <td><c:out value="${pay.suppcreditCntNo}"/></td>
                                            <td><c:out value="${pay.suppcreditCntType}"/></td>                                                                                        
                                            <td><c:out value="${pay.suppcreditCntSize}"/></td>                                                                                        
                                            <c:set var="supcreditExpense" value="${pay.supcreditExpense}"/>
                                            <%
                                                String spcredit = (String) pageContext.getAttribute("supcreditExpense");
                                                String[] spcrExp = spcredit.split("~");
                                            %>
                                            <td align="center"><%=spcrExp[0]%></td>
                                            <td><%=spcrExp[1]%></td>
                                            <td><%=spcrExp[2]%></td>
                                            <td><%=spcrExp[3]%></td>
                                            <td><%=spcrExp[4]%></td>
                                            <td><%=spcrExp[5]%></td>
                                            <td align='center'>0</td>
                                            <td align="center"><c:out value="${pay.freightAmount}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                        </c:if>


                                        <c:if test = "${pay.transactionName == 'Receipt' || pay.transactionName =='PdaAdjust' || pay.transactionName =='CreditAdjust' }" >
                                            <c:if test = "${pay.adjustCode != '-'}" >
                                                <td align="center"><c:out value="${pay.adjustCode}"/></td>
                                            </c:if>
                                            <c:if test = "${pay.creditadjustno != '-'}" >
                                                <td align="center"><c:out value="${pay.creditadjustno}"/></td>
                                            </c:if>
                                            <c:if test = "${pay.receiptCode != '-' && pay.receiptCode != '0' }" >
                                                <td align="center"><c:out value="${pay.receiptCode}"/></td>
                                            </c:if>
                                            <td align="center">-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align="center">-</td>
                                            <td align="center">-</td>
                                            <td align='center'>-</td>
                                            <td align="center">-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            
                                            <td align='center'><c:out value="${pay.debitAmount}"/></td>
                                            <td align="center"><c:out value="${pay.creditAmount}"/></td>
                                            
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align="center">-</td>
                                        </c:if>


                                        <%--<c:set var="recpt" value="${pay.receiptdetails}"/>--%>

                                        <%--
                                            String tds = "FALSE";
                                            String str = (String) pageContext.getAttribute("recpt");
                                            String[] recp = str.split("~");

                                            if(recp.length>1){
                                               String val = recp[0]; 
                                               if("TDS".equals(val)){
                                                 tds = "TRUE";
                                                }
                                                }
                                        %>                                        

                                        <td align="center"><c:out value="${pay.creditAmount}"/></td>     

                                        <%
                                        if(tds.equalsIgnoreCase("TRUE")){
                                        %>
                                        <td align="center"><c:out value="${pay.creditAmount}"/></td>     
                                        <%}else{%>
                                        <td align='center'>-</td>
                                        <%}--%>


                                        <c:if test = "${pay.transactionName == 'TempCredit.Expired'}" >
                                            <td align="center"><c:out value="${pay.tempCode}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align="center"><c:out value="${pay.debitAmount}"/></td>
                                            <td align='center'>0</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>  
                                        </c:if>
                                            
                                        <c:if test = "${pay.transactionName == 'TempCredit'}" >
                                            <td align="center"><c:out value="${pay.tempCode}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>0</td>
                                            <td align="center"><c:out value="${pay.creditAmount}"/></td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>
                                            <td align='center'>-</td>  
                                        </c:if>


                                        <td align="center"><c:out value="${pay.availLimit}"/> Cr</td>                                        
                                    </tr>
                                </c:forEach>
                                <%
                                    index++;
                                    sno++;
                                %>
                            </table>

                        </c:if>          
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>