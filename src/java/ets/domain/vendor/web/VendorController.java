package ets.domain.vendor.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.business.PaginationHelper;
import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.racks.business.RackBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.vendor.business.VendorBP;
import ets.domain.customer.business.CustomerBP;    
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.vendor.business.VendorTO;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.operation.business.OperationTO;
import ets.domain.section.business.SectionBP;
import java.io.File;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.PrintWriter;
import java.util.*;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

public class VendorController extends BaseController {

    VendorCommand vendorCommand;
    LoginBP loginBP;
    VendorBP vendorBP;
    CustomerBP customerBP;
    VehicleBP vehicleBP;
    RackBP rackBP;
    SectionBP sectionBP;
    OperationBP operationBP;
    private Object contractDedicateId;

    public RackBP getRackBP() {
        return rackBP;
    }

    public void setRackBP(RackBP rackBP) {
        this.rackBP = rackBP;
    }

     public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public VendorBP getVendorBP() {
        return vendorBP;
    }

    public void setVendorBP(VendorBP vendorBP) {
        this.vendorBP = vendorBP;
    }

    public VendorCommand getVendorCommand() {
        return vendorCommand;
    }

    public void setVendorCommand(VendorCommand vendorCommand) {
        this.vendorCommand = vendorCommand;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public SectionBP getSectionBP() {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP) {
        this.sectionBP = sectionBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

     protected void bind(HttpServletRequest request, Object command)
	            throws Exception {
	        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
	        ServletRequestDataBinder binder = createBinder(request, command);
	        binder.bind(request);
	        FPLogUtils.fpDebugLog("command -->" + command);
	          if(!"/throttle/updateContractApprovalVendor.do".equalsIgnoreCase(request.getRequestURI())){
	        HttpSession session = request.getSession();
	        int userId = (Integer) session.getAttribute("userId");
	        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");
	        System.out.println("userId" + userId);
	        //  System.out.println("loginRecordId" + loginRecordId);
	        String getUserFunctionsActive = "";
	        String uri = request.getRequestURI();
	//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
	        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
	            String temp[] = getUserFunctionsActive.split("~");
	            System.out.println("function Id" + temp[0]);
	            System.out.println("function Name  " + temp[0]);
	            int status1 = 0;
	            int functionId = Integer.parseInt(temp[0]);
	            String functionName = temp[1];
	//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
	        }
	        }
	        binder.closeNoCatch();
	        initialize(request);
    }
    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleManageVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        ArrayList vendorTypeList = new ArrayList();
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/vendor/manageVendor.jsp";
        String pageTitle = "Add Vendor";
        int userId = (Integer) session.getAttribute("userId");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);
            request.setAttribute("pageTitle", pageTitle);
            pageTitle = "View Vendor";

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processActiveMfrList();

            int totalRecords = 0;
            int pageNo = 0;
            int totalPages = 0;
            VendorTO vendorTO = new VendorTO();
            PaginationHelper pagenation = new PaginationHelper();
            int startIndex = 0;
            int endIndex = 0;

            ArrayList vendorList = new ArrayList();

            request.setAttribute("MfrList", MfrList);
            request.setAttribute("pageTitle", pageTitle);

            vendorTO.setStartIndex(startIndex);
            vendorTO.setEndIndex(endIndex);
            vendorList = vendorBP.processGetVendorLists(vendorTO);
            totalRecords = vendorList.size();
            pagenation.setTotalRecords(totalRecords);

            String buttonClicked = "";
            if (request.getParameter("button") != null && request.getParameter("button") != "") {
                buttonClicked = request.getParameter("button");
            }
            if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            System.out.println("pageNo" + pageNo);
            request.setAttribute("totalPages", totalPages);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            vendorTO.setStartIndex(startIndex);
            vendorTO.setEndIndex(endIndex);
            vendorList = vendorBP.processGetVendorLists(vendorTO);

            request.setAttribute("VendorList", vendorList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Add MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
   public ModelAndView handleAddVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  Add ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList vendorTypeList = new ArrayList();
        ArrayList stateList = new ArrayList();
        path = "content/vendor/addVendor.jsp";
        int userId = (Integer) session.getAttribute("userId");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");

        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-Add", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            // get all values of vendor type for user option choose in add screen

            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            stateList=customerBP.getStateList();
            request.setAttribute("stateList",stateList);

            String pageTitle = "Add Vendor";
            request.setAttribute("pageTitle", pageTitle);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", MfrList);

            //  }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    /**
     * This method used to Add Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddVendor(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageVendor.jsp";
        String pageTitle = "Add Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int vendorId = 0;

        String menuPath = "Vendor  >>  ManageVendor  >> Add";

        //file upload
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String tripSheetId1 = "";
        String vendorName1 = "";
        String tinNo1 = "";
        String vendorTypeId1 = "";
        String settlementType = request.getParameter("settlementType");
        String vendorAddress1 = "";
        String vendorPhoneNo1 = "";
        String vendorMailId1 = "";
        String priceType1 = "";
        String creditDays1 = "";
        String accountNo1 = "";
        String bankName1 = "";
        String branch1 = "";
        String branchcode1 = "";

        String contactName1 = "";
        String cstId1 = "";
        String designation1 = "";
        String dgsdId1 = "";
        String ecId1 = "";
        String eepcId1 = "";
        String emailId1 = "";
        String excisDuty1 = "";
        String faxNo1 = "";
        String gstId1 = "";
        String ifscCode1 = "";
        String micrNo1 = "";
        String mobileNo1 = "";
        String panNo1 = "";
        String rocId1 = "";
        String nstcId1 = "";
        String msmeId1 = "";
        String serviceTax1 = "";
        String vatId1 = "";
        String teleNo1 = "";
        String stateId1 = "";
        String gstNo1 = "";

        //        String[] podRemarks1 = new String[10];
        String[] fileSaved = new String[10];
        //        String[] lrNumber1 = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] city = new String[10];

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VendorTO vendorTO = new VendorTO();
            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
                        System.out.println("fPart.getFileName() = " + fPart.getFileName());

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                            System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            //                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            //                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("tripSheetId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripSheetId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("tinNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tinNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorTypeId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorTypeId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("settlementType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            settlementType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorAddress")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorAddress1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorPhoneNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorPhoneNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorMailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorMailId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("priceType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            priceType1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("creditDays")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            creditDays1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("accountNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            accountNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            bankName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("branch")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            branch1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("branchCode")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            branchcode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contactName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            contactName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cstId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            cstId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("designation")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            designation1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dgsdId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dgsdId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ecId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ecId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("eepcId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            eepcId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("emailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            emailId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("exciseDuty")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            excisDuty1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("faxNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            faxNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("gstId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            gstId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ifscCode")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ifscCode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("micrNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            micrNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("mobileNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            mobileNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("panNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            panNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("rocId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            rocId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nstcId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            nstcId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("msmeId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            msmeId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("serViceTax")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            serviceTax1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vatId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vatId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("teleNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            teleNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("stateId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            stateId1 = paramPart.getStringValue();
                             System.out.println("stateId"+stateId1);
                        }
                        if (partObj.getName().equals("gstNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            gstNo1 = paramPart.getStringValue();
                            System.out.println("gstNo"+gstNo1);
                        }

                    }
                }
            }
            int index = j;
            String[] file = new String[index];
            System.out.println("filekoik" + file);
            String remarks = "";

            String[] saveFile = new String[index];;

            int status = 0;

            //file upload
            vendorTO.setVendorName(vendorName1);
            vendorTO.setTinNo(tinNo1);
            vendorTO.setVendorTypeId(vendorTypeId1);
            vendorTO.setVendorAddress(vendorAddress1);
            vendorTO.setVendorPhoneNo(vendorPhoneNo1);
            vendorTO.setVendorMailId(vendorMailId1);
            vendorTO.setPriceType(priceType1);
            vendorTO.setCreditDays(creditDays1);

            if (settlementType == null) {
                settlementType = "";
            }
            vendorTO.setSettlementType(settlementType);
            //             String[] mfrids = vendorCommand.getMfrids();
            //             String[] mfrids = vendorCommand.getMfrids();
            //             System.out.println("mfrids222"+mfrids);
            //  String[] selectedIndex = vendorCommand.getSelectedIndex();
            String selectedIndex = "0";
            //            System.out.println("selectedIndexpp"+selectedIndex);

            vendorTO.setAccountNo(accountNo1);
            vendorTO.setBankName(bankName1);
            vendorTO.setBranch(branch1);
            vendorTO.setBranchCode(branchcode1);
            vendorTO.setContactName(contactName1);
            vendorTO.setCstId(cstId1);
            vendorTO.setDesignation(designation1);
            vendorTO.setDgsdId(dgsdId1);
            vendorTO.setEcId(ecId1);
            vendorTO.setEepcId(eepcId1);
            vendorTO.setEmailId(emailId1);
            vendorTO.setExciseDuty(excisDuty1);
            vendorTO.setFaxNo(faxNo1);
            vendorTO.setGstId(gstId1);
            vendorTO.setIfscCode(ifscCode1);
            vendorTO.setMicrNo(micrNo1);
            vendorTO.setNstcId(nstcId1);
            vendorTO.setMobileNo(mobileNo1);
            vendorTO.setPanNo(panNo1);
            vendorTO.setRocId(rocId1);
            vendorTO.setSerViceTax(serviceTax1);
            vendorTO.setVatId(vatId1);
            vendorTO.setMsmeId(msmeId1);
            vendorTO.setTeleNo(teleNo1);
            vendorTO.setStateId(stateId1);
            vendorTO.setGstNo(gstNo1);

            System.out.println("j = " + j);
            System.out.println("file = " + file.length);
            System.out.println("tempFilePath[0] = " + tempFilePath[0]);
            System.out.println("tempFilePath[1] = " + tempFilePath[1]);
            for (int x = 0; x < j; x++) {
                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
                System.out.println("saveFile[x] = " + fileSaved[x]);
                saveFile[x] = fileSaved[x];
            }
            int checkVendorName = vendorBP.checkVendorNameExists(vendorTO);
            if (checkVendorName == 0) {
                vendorId = vendorBP.processInsertVendorDetails(vendorTO, selectedIndex, userId, file, tripSheetId1, saveFile);
            }
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = handleManageVendorPage(request, response, command);
            ArrayList vendorList = new ArrayList();
            path = "content/vendor/manageVendor.jsp";
            if (checkVendorName == 0 && vendorId > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor  Added Successfully");
            } else if (checkVendorName == 0 && vendorId == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor not added please contact system admin");
            } else if (checkVendorName > 0 && vendorId == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor  already exists");
            }
        } catch (FPRuntimeException exception) {
            /*
                 * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
                 * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleAlterVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        path = "content/vendor/alterVendor.jsp";

        String pageTitle = "Add Vendor";
        request.setAttribute("pageTitle", pageTitle);
        try {
            int vendorId = Integer.parseInt(request.getParameter("vendorId"));

            ArrayList vendorDetailUnique = new ArrayList();
            vendorDetailUnique = vendorBP.processVendorGetsDetail(vendorId);
            request.setAttribute("GetVendorDetailUnique", vendorDetailUnique);

            String menuPath = "Vendor  >>  ManageVendor  >> Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            VendorTO vendorTO = new VendorTO();

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList stateList = new ArrayList();
            stateList=customerBP.getStateList();
            request.setAttribute("stateList",stateList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Alter Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterVendor(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;

        HttpSession session = request.getSession();
        ModelAndView mv = new ModelAndView();
        String path = "";
        String menuPath = "Vendor  >> Manage Vendor";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String pageTitle = "Alter Vendor";
        path = "content/vendor/manageVendor.jsp";
        request.setAttribute("pageTitle", pageTitle);
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;
        String vendorId1 = "";
        String tripSheetId1 = "";
        String vendorName1 = "";
        String tinNo1 = "";
        String vendorTypeId1 = "";
        String settlementType = request.getParameter("settlementType");
        String vendorAddress1 = "";
        String vendorPhoneNo1 = "";
        String vendorMailId1 = "";
        String priceType1 = "";
        String creditDays1 = "";
        String accountNo1 = "";
        String bankName1 = "";
        String branch1 = "";
        String branchcode1 = "";

        String contactName1 = "";
        String cstId1 = "";
        String designation1 = "";
        String dgsdId1 = "";
        String ecId1 = "";
        String eepcId1 = "";
        String emailId1 = "";
        String excisDuty1 = "";
        String faxNo1 = "";
        String gstId1 = "";
        String ifscCode1 = "";
        String micrNo1 = "";
        String mobileNo1 = "";
        String panNo1 = "";
        String rocId1 = "";
        String nstcId1 = "";
        String msmeId1 = "";
        String serviceTax1 = "";
        String vatId1 = "";
        String teleNo1 = "";
        String stateId1 = "";
        String gstNo1 = "";
        String activeStatus = "";

        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            VendorTO vendorTO = new VendorTO();
            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
                        System.out.println("fPart.getFileName() = " + fPart.getFileName());

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                            System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "\\" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "\\" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                        }

                        System.out.println("fileName..." + fileName);
                        if (tempFilePath[j] != null && !"".equals(tempFilePath[j])) {
                            j++;
                        }
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("vendorId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("tripSheetId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tripSheetId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("tinNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            tinNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorTypeId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorTypeId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("settlementType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            settlementType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorAddress")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorAddress1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorPhoneNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorPhoneNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorMailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vendorMailId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("priceType")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            priceType1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("creditDays")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            creditDays1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("accountNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            accountNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            bankName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("branch")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            branch1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("branchCode")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            branchcode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contactName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            contactName1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cstId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            cstId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("designation")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            designation1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dgsdId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            dgsdId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ecId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ecId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("eepcId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            eepcId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("emailId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            emailId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("exciseDuty")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            excisDuty1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("faxNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            faxNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("gstId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            gstId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ifscCode")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ifscCode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("micrNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            micrNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("mobileNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            mobileNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("panNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            panNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("rocId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            rocId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nstcId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            nstcId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("msmeId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            msmeId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("serViceTax")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            serviceTax1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vatId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            vatId1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("teleNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            teleNo1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("stateId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            stateId1 = paramPart.getStringValue();
                            System.out.println("stateId"+stateId1);
                        }
                        if (partObj.getName().equals("gstNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            gstNo1 = paramPart.getStringValue();
                            System.out.println("gstNo"+gstNo1);
                        }
                        if (partObj.getName().equals("activeStatus")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            activeStatus = paramPart.getStringValue();
                        }

                    }
                }
            }
            int index = j;
            String[] file = new String[index];
            System.out.println("filekoik" + file);

            String remarks = "";

            String[] saveFile = new String[index];;

            int status = 0;

            //file upload
            vendorTO.setActiveInd(activeStatus);
            vendorTO.setVendorId(vendorId1);
            vendorTO.setVendorName(vendorName1);
            vendorTO.setTinNo(tinNo1);
            vendorTO.setVendorTypeId(vendorTypeId1);
            vendorTO.setVendorAddress(vendorAddress1);
            vendorTO.setVendorPhoneNo(vendorPhoneNo1);
            vendorTO.setVendorMailId(vendorMailId1);
            vendorTO.setPriceType(priceType1);
            vendorTO.setCreditDays(creditDays1);

            if (settlementType == null) {
                settlementType = "";
            }
            vendorTO.setSettlementType(settlementType);
            String selectedIndex = "0";

            vendorTO.setAccountNo(accountNo1);
            vendorTO.setBankName(bankName1);
            vendorTO.setBranch(branch1);
            vendorTO.setBranchCode(branchcode1);
            vendorTO.setContactName(contactName1);
            vendorTO.setCstId(cstId1);
            vendorTO.setDesignation(designation1);
            vendorTO.setDgsdId(dgsdId1);
            vendorTO.setEcId(ecId1);
            vendorTO.setEepcId(eepcId1);
            vendorTO.setEmailId(emailId1);
            vendorTO.setExciseDuty(excisDuty1);
            vendorTO.setFaxNo(faxNo1);
            vendorTO.setGstId(gstId1);
            vendorTO.setIfscCode(ifscCode1);
            vendorTO.setMicrNo(micrNo1);
            vendorTO.setNstcId(nstcId1);
            vendorTO.setMobileNo(mobileNo1);
            vendorTO.setPanNo(panNo1);
            vendorTO.setRocId(rocId1);
            vendorTO.setSerViceTax(serviceTax1);
            vendorTO.setVatId(vatId1);
            vendorTO.setMsmeId(msmeId1);
            vendorTO.setTeleNo(teleNo1);
            vendorTO.setStateId(stateId1);
            vendorTO.setGstNo(gstNo1);

            System.out.println("j = " + j);
            System.out.println("tempFilePath[0] = " + tempFilePath[0]);
            System.out.println("tempFilePath[1] = " + tempFilePath[1]);
            for (int x = 0; x < j; x++) {
                System.out.println("tempFilePath[x] = " + tempFilePath[x]);
                file[x] = tempFilePath[x];
                System.out.println("saveFile[x] = " + fileSaved[x]);
                saveFile[x] = fileSaved[x];
            }

            status = vendorBP.processUpdateVendorDetails(vendorTO, selectedIndex, userId, file, tripSheetId1, saveFile);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = handleManageVendorPage(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
                 * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
                 * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    //manageVendorConfigPage
    /**
     * This method used to Add MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageVendorConfigPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Vendor  >> Config Item";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/manageConfigVendor.jsp";
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    //ajaxGetVendor
    public void ajaxGetVendor(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {

        vendorCommand = command;

        String vendorTypeId = request.getParameter("vendorTypeId");
        String suggestions = "";

        try {

            suggestions = vendorBP.processGetVendor(Integer.parseInt(vendorTypeId));
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }
    //handleConfigItemForVendorPage

    /**
     * This method used to Add MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleConfigItemForVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        String path = "";
        String menuPath = "Vendor  >> Config Item";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        try {
            path = "content/vendor/manageConfigVendor.jsp";

            //get vendorId
            int vendorId = Integer.parseInt(request.getParameter("vendorId"));
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorTypeId", request.getParameter("vendorTypeId"));

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            ArrayList itemList = new ArrayList();
            itemList = vendorBP.processGetItemList(vendorId);
            request.setAttribute("ItemList", itemList);

            ArrayList assignedList = new ArrayList();
            assignedList = vendorBP.processGetAssignedList(vendorId);
            request.setAttribute("AssignedList", assignedList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    /**
     * This method caters to save student's to a class.
     *
     * @param request - Http request object
     *
     * @param response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView saveVendorItem(HttpServletRequest request, HttpServletResponse reponse, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        String menuPath = "Dept Operatios   >> Manage class  >>  Configure Student";
        HttpSession session = request.getSession();
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        String pageTitle = "Configure Vendor Item";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageConfigVendor.jsp";
        try {
            int status = 0;
            String[] ItemIds = vendorCommand.getAssignedList();

            int vendorId = Integer.parseInt(vendorCommand.getVendorId());

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            ArrayList assignedList = new ArrayList();
            assignedList = vendorBP.processGetAssignedList(vendorId);
            request.setAttribute("AssignedList", assignedList);

            int resultAssignedList = 0;
            status = vendorBP.makeAllNoToActiveInd(vendorId);

            resultAssignedList = vendorBP.resultAssignedList(ItemIds, vendorId, userId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor Configured Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleSearchVendorCatPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vendor >> Config Vendor Category";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            path = "content/vendor/manageVendorCat.jsp";
            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);
            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);
            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleConfigCatForVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        String path = "";
        String menuPath = "Vendor  >> Config Item";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        try {
            path = "content/vendor/manageVendorCat.jsp";

            //get vendorId
            int vendorId = Integer.parseInt(request.getParameter("vendorId"));
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorTypeId", request.getParameter("vendorTypeId"));

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            ArrayList assignedList = new ArrayList();
            assignedList = vendorBP.processGetCatAssignedList(vendorId);
            request.setAttribute("AssignedList", assignedList);

            ArrayList categoryList = new ArrayList();
            categoryList = vendorBP.processGetNotAssisgnedList(vendorId);
            request.setAttribute("categoryList", categoryList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleVendorCategory(HttpServletRequest request, HttpServletResponse reponse, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        String menuPath = "Dept Operatios   >> Manage class  >>  Configure Student";
        HttpSession session = request.getSession();
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        String pageTitle = "Configure Vendor Item";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        path = "content/vendor/manageVendorCat.jsp";
        try {
            int status = 0;
            String[] catIds = vendorCommand.getAssignedList();

            int vendorId = Integer.parseInt(vendorCommand.getVendorId());
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorTypeId", request.getParameter("vendorTypeId"));

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorList();
            request.setAttribute("VendorList", vendorList);

            ArrayList vendorTypeList = new ArrayList();
            vendorTypeList = vendorBP.processGetVendorTypeList();
            request.setAttribute("VendorTypeList", vendorTypeList);

            int resultAssignedList = 0;
            status = vendorBP.processVendorCatInactive(vendorId);

            resultAssignedList = vendorBP.processVendorCatConfig(catIds, vendorId, userId);

            ArrayList assignedList = new ArrayList();
            assignedList = vendorBP.processGetCatAssignedList(vendorId);
            request.setAttribute("AssignedList", assignedList);

            ArrayList categoryList = new ArrayList();
            categoryList = vendorBP.processGetNotAssisgnedList(vendorId);
            request.setAttribute("categoryList", categoryList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor Configured Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleManageFleetVendorPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        ArrayList vendorTypeList = new ArrayList();
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/vendor/manageFleetVendor.jsp";
        String pageTitle = "Add Vendor";
        int userId = (Integer) session.getAttribute("userId");
        //     int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            try {
                vendorTypeList = vendorBP.processGetVendorTypeList();
                request.setAttribute("VendorTypeList", vendorTypeList);
                request.setAttribute("pageTitle", pageTitle);
                pageTitle = "View Vendor";

                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processActiveMfrList();

                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                VendorTO vendorTO = new VendorTO();
                PaginationHelper pagenation = new PaginationHelper();
                String vendorTypeId = String.valueOf(1001);

                ArrayList vendorList = new ArrayList();

                request.setAttribute("MfrList", MfrList);
                request.setAttribute("pageTitle", pageTitle);
                vendorTO.setVendorTypeId(vendorTypeId);
                vendorList = vendorBP.processGetVendorLists(vendorTO);
                totalRecords = vendorList.size();
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                }
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                System.out.println("pageNo" + pageNo);
                request.setAttribute("totalPages", totalPages);
                vendorTO.setVendorTypeId(vendorTypeId);
                vendorList = vendorBP.processGetVendorLists(vendorTO);

                request.setAttribute("VendorList", vendorList);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createVehicleVendorContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();

        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String vendorId = "", vendorName = "";
            vendorId = request.getParameter("vendorId");
            vendorName = request.getParameter("vendor");
            System.out.println("vendorId ===" + vendorId + " Name " + vendorName);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorName", vendorName);
            vendorTO.setVendorId(vendorId);
            String startDate = request.getParameter("startDate");
            request.setAttribute("startDate", startDate);
            String endDate = request.getParameter("endDate");
            request.setAttribute("endDate", endDate);
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            vendorTO.setVehicleTypeId(vehicleTypeId);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            System.out.println("vehicleTypeId " + vehicleTypeId);
            //ArrayList TypeList = new ArrayList();
            //  TypeList = vendorBP.getVehicleTypeList(vendorTO);
            //  request.setAttribute("TypeList", TypeList);
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);

            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            System.out.println("trailerTypeList" + trailerTypeList.size());
//                ArrayList trailerTypeList = new ArrayList();
//                trailerTypeList = vehicleBP.processTrailerList(vehicleTO);
//                request.setAttribute("trailerTypeList", trailerTypeList);
            ArrayList uomList = new ArrayList();
            uomList = vendorBP.processGetUomList();
            request.setAttribute("uomList", uomList);
            System.out.println("operationBP 1");

            ArrayList containerTypeList = new ArrayList();
            containerTypeList = operationBP.getContainerTypeList();
            request.setAttribute("containerTypeList", containerTypeList);
            //                 if (vendorCommand.getVehicleTypeId() != null && vendorCommand.getVehicleTypeId() != "") {
            //                    vendorTO.setVehic leTypeId(vendorCommand.getVehicleTypeId());
            //                }

            //              path = "content/vendor/vehicleVendorContractList.jsp";
            path = "content/vendor/createVehicleVendorContract.jsp";
            //   path = "content/vendor/editVendorContract.jsp";
            //            ArrayList vendorTypeList = new ArrayList();
            //            vendorTypeList = vendorBP.processGetVendorTypeList();
            //            request.setAttribute("VendorTypeList", vendorTypeList);
            //  }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView saveVehicleVendorContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        // int vendorId = (Integer) session.getAttribute("vendorId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/createVehicleVendorContract.jsp";
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (vendorCommand.getVendorId() != null && vendorCommand.getVendorId() != "") {
                vendorTO.setVendorId(vendorCommand.getVendorId());
            }
            if (vendorCommand.getContractTypeId() != null && vendorCommand.getContractTypeId() != "") {
                vendorTO.setContractTypeId(vendorCommand.getContractTypeId());
            }
            if (vendorCommand.getStartDate() != null && vendorCommand.getStartDate() != "") {
                vendorTO.setStartDate(vendorCommand.getStartDate());
            }
            if (vendorCommand.getEndDate() != null && vendorCommand.getEndDate() != "") {
                vendorTO.setEndDate(vendorCommand.getEndDate());
            }
            if (vendorCommand.getPaymentType() != null && vendorCommand.getPaymentType() != "") {
                vendorTO.setPaymentType(vendorCommand.getPaymentType());
            }

            //dedicate
            String[] vehicleTypeIdDedicate = request.getParameterValues("vehicleTypeIdDedicate");
            vendorTO.setVehicleTypeIdDedicates(vehicleTypeIdDedicate);
            String[] vehicleUnitsDedicate = request.getParameterValues("vehicleUnitsDedicate");
            vendorTO.setVehicleUnitsDedicates(vehicleUnitsDedicate);
            String[] contractCategory = request.getParameterValues("contractCategory");
            vendorTO.setContractCategorys(contractCategory);
            String[] fixedCost = request.getParameterValues("fixedCost");
            vendorTO.setFixedCosts(fixedCost);
            String[] fixedHrs = request.getParameterValues("fixedHrs");
            System.out.println("fixedHrs = " + fixedHrs.length);
            vendorTO.setFixedHrss(fixedHrs);
            String[] fixedMin = request.getParameterValues("fixedMin");
            vendorTO.setFixedMins(fixedMin);
            String[] totalFixedCost = request.getParameterValues("totalFixedCost");
            vendorTO.setTotalFixedCosts(totalFixedCost);
            String[] rateCost = request.getParameterValues("rateCost");
            vendorTO.setRateCosts(rateCost);
            String[] rateLimit = request.getParameterValues("rateLimit");
            vendorTO.setRateLimits(rateLimit);
            String[] maxAllowableKM = request.getParameterValues("maxAllowableKM");
            vendorTO.setMaxAllowableKMs(maxAllowableKM);
            String[] workingDays = request.getParameterValues("workingDays");
            vendorTO.setWorkingDayss(workingDays);
            String[] holidays = request.getParameterValues("holidays");
            vendorTO.setHolidayss(holidays);
            String[] addCostDedicate = request.getParameterValues("addCostDedicate");
            vendorTO.setAddCostDedicates(addCostDedicate);
            String[] iCnt = request.getParameterValues("iCnt");
            vendorTO.setiCnt(iCnt);
            String[] iCnt1 = request.getParameterValues("iCnt1");
            vendorTO.setiCnt1(iCnt1);
            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            vendorTO.setVehicleTypeIds(vehicleTypeId);
            String[] vehicleUnits = request.getParameterValues("vehicleUnits");
            vendorTO.setVehicleUnitss(vehicleUnits);
//                String[] trailerType = request.getParameterValues("trailerType");
//                String[] trailorTypeUnits = request.getParameterValues("trailorTypeUnits");
            String[] spotCost = request.getParameterValues("spotCost");
            vendorTO.setSpotCosts(spotCost);
            String[] additionalCost = request.getParameterValues("additionalCost");
            vendorTO.setAdditionalCosts(additionalCost);
            String[] marketRate = request.getParameterValues("marketRate");
            vendorTO.setMarketRate(marketRate);
            String[] loadTypeIds = request.getParameterValues("loadTypeId");
            vendorTO.setLoadTypeIds(loadTypeIds);
            String[] containerTypeIds = request.getParameterValues("containerTypeId");
            vendorTO.setContainerTypeIds(containerTypeIds);
            String[] containerQtys = request.getParameterValues("containerQty");
            vendorTO.setContainerQtys(containerQtys);

            String[] originIdFullTruck = request.getParameterValues("originIdFullTruck");
            vendorTO.setOriginIdFullTrucks(originIdFullTruck);
            String[] originNameFullTruck = request.getParameterValues("originNameFullTruck");
            vendorTO.setOriginNameFullTrucks(originNameFullTruck);
            String[] destinationIdFullTruck = request.getParameterValues("destinationIdFullTruck");
            vendorTO.setDestinationIdFullTrucks(destinationIdFullTruck);
            String[] destinationNameFullTruck = request.getParameterValues("destinationNameFullTruck");
            vendorTO.setDestinationNameFullTrucks(destinationNameFullTruck);
            String[] travelKmFullTruck = request.getParameterValues("travelKmFullTruck");
            vendorTO.setTravelKmFullTrucks(travelKmFullTruck);
            String[] travelHourFullTruck = request.getParameterValues("travelHourFullTruck");
            vendorTO.setTravelHourFullTrucks(travelHourFullTruck);
            String[] travelMinuteFullTruck = request.getParameterValues("travelMinuteFullTruck");
            vendorTO.setTravelMinuteFullTrucks(travelMinuteFullTruck);

            vendorTO.setUom(request.getParameter("uom"));
            vendorTO.setAgreedFuelPrice(request.getParameter("agreedFuelPrice"));
            vendorTO.setHikeFuelPrice(request.getParameter("hikeFuelPrice"));

            String[] pointId1 = request.getParameterValues("pointId1");
            vendorTO.setPointsId1(pointId1);
            String[] pointId2 = request.getParameterValues("pointId2");
            vendorTO.setPointsId2(pointId2);
            String[] pointId3 = request.getParameterValues("pointId3");
            vendorTO.setPointsId3(pointId3);
            String[] pointId4 = request.getParameterValues("pointId4");
            vendorTO.setPointsId4(pointId4);
            vendorTO.setVendorName(request.getParameter("vendorName"));
            System.out.println("request.getParameterbvendorName:"+request.getParameter("vendorName"));
             String[] pointName1 = request.getParameterValues("pointName1");
 vendorTO.setPointName1A(pointName1);
            int contractId = vendorBP.processInsertVehicleVendorContract(vendorTO, userId);
//
            if (contractId > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor contract added successfully");
            } else if (contractId == -100) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Organisation Route Not Defined");
            } else if (contractId == -200) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor contract added successfully, Some of the route cost is waiting for appoval");
            } else if (contractId == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Vendor contract added failed");
            }
            // }
            path = "content/vendor/manageFleetVendor.jsp";
            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorLists(vendorTO);
            request.setAttribute("VendorList", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView saveEditVehicleVendorContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        HttpSession session = request.getSession();
        String path = "";
        // int userId = (Integer) session.getAttribute("userId");
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        String menuPath = "Vendor >> View Fleet Vendor Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);
        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            if (vendorCommand.getVendorId() != null && vendorCommand.getVendorId() != "") {
                vendorTO.setVendorId(vendorCommand.getVendorId());
            }
            if (vendorCommand.getContractTypeId() != null && vendorCommand.getContractTypeId() != "") {
                vendorTO.setContractTypeId(vendorCommand.getContractTypeId());
            }
            if (vendorCommand.getStartDate() != null && vendorCommand.getStartDate() != "") {
                vendorTO.setStartDate(vendorCommand.getStartDate());
            }
            if (vendorCommand.getEndDate() != null && vendorCommand.getEndDate() != "") {
                vendorTO.setEndDate(vendorCommand.getEndDate());
            }
            if (vendorCommand.getPaymentType() != null && vendorCommand.getPaymentType() != "") {
                vendorTO.setPaymentType(vendorCommand.getPaymentType());
            }
//               if (!"".equals(endDate) && endDate != null && !"".equals(endDateold) && endDateold != null && contractTypeId != null && !endDateold.equals(endDate)) {
//                }

            String[] originNameFullTruckE = request.getParameterValues("originNameFullTruckE");
            String[] destinationNameFullTruckE = request.getParameterValues("destinationNameFullTruckE");
            String[] pointName1E = request.getParameterValues("pointName1E");
            String[] pointName2E = request.getParameterValues("pointName2E");
            String[] pointName3E = request.getParameterValues("pointName3E");
            String[] pointName4E = request.getParameterValues("pointName4E");
            String[] approvalStatusE = request.getParameterValues("approvalStatusE");
            String[] vehicleTypeE = request.getParameterValues("vehicleTypeE");
            String[] loadTypeE = request.getParameterValues("loadTypeE");
            String[] containerTypeIdE1 = request.getParameterValues("containerTypeIdE1");
            String[] containerQtyE1 = request.getParameterValues("containerQtyE1");

            vendorTO.setOriginNameFullTruckE(originNameFullTruckE);

            vendorTO.setDestinationNameFullTruckE(destinationNameFullTruckE);
            vendorTO.setPointName1E(pointName1E);
            vendorTO.setPointName2E(pointName2E);
            vendorTO.setPointName3E(pointName3E);
            vendorTO.setPointName4E(pointName4E);
            vendorTO.setApprovalStatusE(approvalStatusE);
            vendorTO.setVehicleTypeE(vehicleTypeE);
            vendorTO.setLoadTypeE(loadTypeE);
            vendorTO.setContainerTypeIdE1(containerTypeIdE1);
            vendorTO.setContainerQtyE1(containerQtyE1);

            String[] pointName1 = request.getParameterValues("pointName1");
            String[] pointName2 = request.getParameterValues("pointName2");
            String[] pointName3 = request.getParameterValues("pointName3");
            String[] pointName4 = request.getParameterValues("pointName4");
            String[] vehicleType = request.getParameterValues("vehicleTypeId");
            String[] loadTypeId = request.getParameterValues("loadTypeId");
            String[] containerQty = request.getParameterValues("containerQty");
            String[] containerType = request.getParameterValues("containerTypeId");


            vendorTO.setPointName1A(pointName1);
            vendorTO.setPointName2A(pointName2);
            vendorTO.setPointName3A(pointName3);
            vendorTO.setPointName4A(pointName4);
            vendorTO.setVehicleTypeA(vehicleType);
            vendorTO.setLoadTypeA(loadTypeId);
            vendorTO.setContainerQtyA(containerQty);
            vendorTO.setContainerTypeA(containerType);
            vendorTO.setVendorName(request.getParameter("vendorName"));

            vendorTO.setUom(request.getParameter("uom"));
            vendorTO.setAgreedFuelPrice(request.getParameter("agreedFuelPrice"));
            vendorTO.setHikeFuelPrice(request.getParameter("hikeFuelPrice"));

            String[] contractDedicateId = request.getParameterValues("contractDedicateId");
            vendorTO.setContractDedicateIds(contractDedicateId);
            String[] fixedCostE = request.getParameterValues("fixedCostE");
            vendorTO.setFixedCostE(fixedCostE);
            String[] fixedHrsE = request.getParameterValues("fixedHrsE");
            vendorTO.setFixedHrsE(fixedHrsE);
            String[] fixedMinE = request.getParameterValues("fixedMinE");
            vendorTO.setFixedMinE(fixedMinE);
            String[] totalFixedCostE = request.getParameterValues("totalFixedCostE");
            vendorTO.setTotalFixedCostE(totalFixedCostE);
            String[] rateCostE = request.getParameterValues("rateCostE");
            vendorTO.setRateCostE(rateCostE);
            String[] rateLimitE = request.getParameterValues("rateLimitE");
            vendorTO.setRateLimitE(rateLimitE);
            String[] workingDaysE = request.getParameterValues("workingDaysE");
            vendorTO.setWorkingDaysE(workingDaysE);
            String[] holidaysE = request.getParameterValues("holidaysE");
            vendorTO.setHolidaysE(holidaysE);
            String[] addCostDedicateE = request.getParameterValues("addCostDedicateE");
            vendorTO.setAddCostDedicateE(addCostDedicateE);
            String[] activeIndE = request.getParameterValues("activeIndE");
            vendorTO.setActiveIndE(activeIndE);

            String[] originIdFullTruck = request.getParameterValues("originIdFullTruck");
            vendorTO.setOriginIdFullTrucks(originIdFullTruck);
            String[] originNameFullTruck = request.getParameterValues("originNameFullTruck");
            vendorTO.setOriginNameFullTrucks(originNameFullTruck);
            String[] destinationIdFullTruck = request.getParameterValues("destinationIdFullTruck");
            vendorTO.setDestinationIdFullTrucks(destinationIdFullTruck);
            String[] destinationNameFullTruck = request.getParameterValues("destinationNameFullTruck");
            vendorTO.setDestinationNameFullTrucks(destinationNameFullTruck);
            String[] travelKmFullTruck = request.getParameterValues("travelKmFullTruck");
            vendorTO.setTravelKmFullTrucks(travelKmFullTruck);
            String[] travelHourFullTruck = request.getParameterValues("travelHourFullTruck");
            vendorTO.setTravelHourFullTrucks(travelHourFullTruck);
            String[] travelMinuteFullTruck = request.getParameterValues("travelMinuteFullTruck");
            vendorTO.setTravelMinuteFullTrucks(travelMinuteFullTruck);

            String[] vehicleTypeId = request.getParameterValues("vehicleTypeId");
            vendorTO.setVehicleTypeIds(vehicleTypeId);
            String[] vehicleUnits = request.getParameterValues("vehicleUnits");
            vendorTO.setVehicleUnitss(vehicleUnits);
            String[] trailerType = request.getParameterValues("trailerType");
            vendorTO.setTrailerTypes(trailerType);
            String[] trailorTypeUnits = request.getParameterValues("trailorTypeUnits");
            vendorTO.setTrailorTypeUnitss(trailorTypeUnits);
            String[] spotCost = request.getParameterValues("spotCost");
            vendorTO.setSpotCosts(spotCost);
            String[] additionalCost = request.getParameterValues("additionalCost");
            vendorTO.setAdditionalCosts(additionalCost);
            //dedicate
            String[] vehicleTypeIdDedicate = request.getParameterValues("vehicleTypeIdDedicate");
            vendorTO.setVehicleTypeIdDedicates(vehicleTypeIdDedicate);
            String[] vehicleUnitsDedicate = request.getParameterValues("vehicleUnitsDedicate");
            vendorTO.setVehicleUnitsDedicates(vehicleUnitsDedicate);

//                String[] trailorTypeDedicate = request.getParameterValues("trailorTypeDedicate");
//                String[] trailorUnitsDedicate = request.getParameterValues("trailorUnitsDedicate");
            String[] contractCategory = request.getParameterValues("contractCategory");
            vendorTO.setContractCategorys(contractCategory);
            String[] fixedCost = request.getParameterValues("fixedCost");
            vendorTO.setFixedCosts(fixedCost);
            String[] fixedHrs = request.getParameterValues("fixedHrs");
            vendorTO.setFixedHrss(fixedHrs);
            String[] fixedMin = request.getParameterValues("fixedMin");
            vendorTO.setFixedMins(fixedMin);
            String[] totalFixedCost = request.getParameterValues("totalFixedCost");
            vendorTO.setTotalFixedCosts(totalFixedCost);
            String[] rateCost = request.getParameterValues("rateCost");
            vendorTO.setRateCosts(rateCost);
            String[] rateLimit = request.getParameterValues("rateLimit");
            vendorTO.setRateLimits(rateLimit);
            String[] maxAllowableKM = request.getParameterValues("maxAllowableKM");
            vendorTO.setMaxAllowableKMs(maxAllowableKM);
            String[] workingDays = request.getParameterValues("workingDays");
            vendorTO.setWorkingDayss(workingDays);
            String[] holidays = request.getParameterValues("holidays");
            vendorTO.setHolidayss(holidays);
            String[] addCostDedicate = request.getParameterValues("addCostDedicate");
            vendorTO.setAddCostDedicates(addCostDedicate);
            String[] loadTypeIds = request.getParameterValues("loadTypeId");
            vendorTO.setLoadTypeIds(loadTypeIds);
            String[] containerTypeIds = request.getParameterValues("containerTypeId");
            vendorTO.setContainerTypeIds(containerTypeIds);
            String[] containerQtys = request.getParameterValues("containerQty");
            vendorTO.setContainerQtys(containerQtys);

            vendorTO.setUom(request.getParameter("uom"));
            vendorTO.setAgreedFuelPrice(request.getParameter("agreedFuelPrice"));
            vendorTO.setHikeFuelPrice(request.getParameter("hikeFuelPrice"));
            int contractId = Integer.parseInt(request.getParameter("contractId"));
            vendorTO.setVendorId(contractId + "");
//                int contractId = vendorBP.processInsertVehicleVendorContract(vendorTO, userId);
//                System.out.println("request.getParameterValues" + request.getParameterValues("originIdFullTruck"));
            String[] iCnt = request.getParameterValues("iCnt");
            vendorTO.setiCnt(iCnt);
            String[] iCnt1 = request.getParameterValues("iCnt1");
            vendorTO.setiCnt1(iCnt1);

            String[] contractHireId = request.getParameterValues("contractHireId");
            vendorTO.setContractHireIds(contractHireId);
            String[] spotCostE = request.getParameterValues("spotCostE");
            vendorTO.setSpotCostE(spotCostE);
            String[] additionalCostE = request.getParameterValues("additionalCostE");
            vendorTO.setAdditionalCostE(additionalCostE);
            String[] activeIndD = request.getParameterValues("activeIndD");
            vendorTO.setActiveIndD(activeIndD);

            String[] marketRate = request.getParameterValues("marketRate");
            vendorTO.setMarketRate(marketRate);

            String[] pointId1 = request.getParameterValues("pointId1");
            vendorTO.setPointsId1(pointId1);
            String[] pointId2 = request.getParameterValues("pointId2");
            vendorTO.setPointsId2(pointId2);
            String[] pointId3 = request.getParameterValues("pointId3");
            vendorTO.setPointsId3(pointId3);
            String[] pointId4 = request.getParameterValues("pointId4");
            vendorTO.setPointsId4(pointId4);

            if (!"".equals(vendorCommand.getEndDate()) && vendorCommand.getEndDate() != null) {
                contractId = vendorBP.updateVendorContractDetails(vendorTO, userId);
                System.out.println("updateVendorContract:" + contractId);

            }

            if (contractId > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor contract added successfully");
            } else if (contractId == -100) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Organisation Route Not Defined");
            } else if (contractId == -200) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vendor contract added successfully, Some of the route cost is waiting for appoval");
            } else if (contractId == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Vendor contract added failed");
            }
            path = "content/vendor/manageFleetVendor.jsp";
            ArrayList vendorList = new ArrayList();
            vendorList = vendorBP.processGetVendorLists(vendorTO);
            request.setAttribute("VendorList", vendorList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView viewVendorVehicleContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        ArrayList viewVendorVehicleContract = new ArrayList();
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/vendor/viewVehicleVendorContract.jsp";
        String pageTitle = "Vendor Vehicle Wise Contract Lists";
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String vendorId = "";
            vendorId = request.getParameter("vendorId");
            System.out.println("vendorId ===" + vendorId);
            request.setAttribute("vendorId", vendorId);
            vendorTO.setVendorId(vendorId);
            String contractId = "";
            contractId = request.getParameter("contractId");
            System.out.println("contractId ===" + contractId);
            request.setAttribute("contractId", contractId);
            vendorTO.setContractId(contractId);
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            vendorTO.setVehicleTypeId(vehicleTypeId);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            System.out.println("vehicleTypeId " + vehicleTypeId);
            ArrayList TypeList = new ArrayList();
            TypeList = vendorBP.getVehicleTypeList(vendorTO);
            request.setAttribute("TypeList", TypeList);
            ArrayList vehicleList = new ArrayList();
            vehicleList = vendorBP.getVehicleLists(vendorTO);
            request.setAttribute("vehicleList", vehicleList);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            viewVendorVehicleContract = vendorBP.viewVendorVehicleContract(vendorTO);
            request.setAttribute("viewVendorVehicleContract", viewVendorVehicleContract);
            System.out.println("viewVendorVehicleContract" + viewVendorVehicleContract.size());
            request.setAttribute("pageTitle", pageTitle);
            //   }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView configureVehicleTrailerPage(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();
        HttpSession session = request.getSession();
        String path = "";
        String vendorId = "";
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        String menuPath = "Vendor >>Configure Vehicle & Trailers";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vehicle";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/createVehicleTrailerContract.jsp";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            String vehicleTypeDedicate = request.getParameter("vehicleTypeDedicate");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String vehicleUnitsDedicate = request.getParameter("vehicleUnitsDedicate");
            String trailorTypeDedicate = request.getParameter("trailorTypeDedicate");
            String trailorTypeIdDedicate = request.getParameter("trailorTypeIdDedicate");
            String trailorUnitsDedicate = request.getParameter("trailorUnitsDedicate");
            String contractDedicateId = request.getParameter("contractDedicateId");
            request.setAttribute("vehicleTypeDedicate", vehicleTypeDedicate);
            request.setAttribute("vehicleUnitsDedicate", vehicleUnitsDedicate);
            request.setAttribute("trailorTypeDedicate", trailorTypeDedicate);
            request.setAttribute("trailorTypeIdDedicate", trailorTypeIdDedicate);
            request.setAttribute("trailorUnitsDedicate", trailorUnitsDedicate);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            request.setAttribute("contractDedicateId", contractDedicateId);
            vendorId = request.getParameter("vendorId");
            request.setAttribute("vendorId", vendorId);
            System.out.println("vendorId configureVehicleTrailerPage I am here" + vendorId);

//                ArrayList vehicleList = new ArrayList();
//                vehicleList = vehicleBP.processTrailerList(vehicleTO);
//                request.setAttribute("vehicleList", vehicleList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            path = "content/vendor/configureVehicleTrailerPage.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView saveVehicleTrailerContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        //    int conractId = (Integer) session.getAttribute("contractId");
        String menuPath = "Vendor >> View Vehicle trailer Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);

        int trailerStatus = 0;
        int status1 = 0;
        String vendorId = "";
        String contractDedicateId = "";
        vendorId = request.getParameter("vendorId");

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            vendorTO.setVendorId(vendorId);
            System.out.println("vendorId I am here raj " + vendorId);
            vendorTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            vendorTO.setVehicleUnits(request.getParameter("vehicleUnitsDedicate"));
            vendorTO.setTrailerUnits(request.getParameter("trailorUnitsDedicate"));
            vendorTO.setTrailerTypeAxles(request.getParameter("trailorTypeDedicate"));
            System.out.println("contractDedicateId I am here nat " + request.getParameter("contractDedicateId"));
            vendorTO.setContractDedicateId(request.getParameter("contractDedicateId"));
            if (vendorCommand.getContractId() != null && vendorCommand.getContractId() != "") {
                vendorTO.setContractId(vendorCommand.getContractId());
            }

            String[] trailerType = request.getParameterValues("trailerType");
            String[] trailerNo = request.getParameterValues("trailerNo");
            String[] trailerRemarks = request.getParameterValues("trailerRemarks");
            if (!"".equals(trailerNo) && trailerNo != null) {
                if (trailerNo.length > 0) {
                    trailerStatus = vendorBP.processInsertTrailerContract(vendorTO, trailerType, trailerNo, trailerRemarks, userId);
                    System.out.println("trailers ====" + trailerStatus);
                }
            }

            // vehicles
            if (vendorCommand.getContractId() != null && vendorCommand.getContractId() != "") {
                vendorTO.setContractId(vendorCommand.getContractId());
            }

            if (vendorCommand.getVehicleRegNo() != null && vendorCommand.getVehicleRegNo() != "") {
                vendorTO.setVehicleRegNo(vendorCommand.getVehicleRegNo());
            }
            if (vendorCommand.getAgreedDate() != null && vendorCommand.getAgreedDate() != "") {
                vendorTO.setAgreedDate(vendorCommand.getAgreedDate());
            }
            if (vendorCommand.getRemarks() != null && vendorCommand.getRemarks() != "") {
                vendorTO.setRemarks(vendorCommand.getRemarks());
            }
            if (vendorCommand.getMfr() != null && vendorCommand.getMfr() != "") {
                vendorTO.setMfr(vendorCommand.getMfr());
            }
            if (vendorCommand.getModel() != null && vendorCommand.getModel() != "") {
                vendorTO.setModel(vendorCommand.getModel());
            }
            if (vendorCommand.getActiveInd() != null && vendorCommand.getActiveInd() != "") {
                vendorTO.setActiveInd(vendorCommand.getActiveInd());
            }

            String[] checkBox = vendorCommand.getSelectedIndex();
            System.out.println("checkBox" + checkBox);
            System.out.println("vehicleRegNo1 Hi madhan i am here " + checkBox);
            String[] vehicleRegNo1 = request.getParameterValues("vehicleRegNo");
            int vehicleStatus = 0;
            if (vehicleRegNo1 != null) {
//                    status1 = vendorBP.processInsertVehicleContract(vendorTO, userId);
                System.out.println("vehicles=-=-" + status1);
                if (vehicleRegNo1.length > 0) {
                    String[] vehicleTypeIdcheck1 = request.getParameterValues("vehicleTypeIdcheck");
                    System.out.println("vehicleTypeIdcheck1" + vehicleTypeIdcheck1);
                    String[] mfr1 = request.getParameterValues("mfr");
                    String[] model1 = request.getParameterValues("model");
                    String[] agreedDate = request.getParameterValues("agreedDate");
                    String[] remarks = request.getParameterValues("remarks");
                    vehicleStatus = vendorBP.updatesaveVehicles(vendorTO, userId, vehicleRegNo1, vehicleTypeIdcheck1, mfr1, model1, agreedDate, remarks);
                    System.out.println("Status" + vehicleStatus);

                }
            }

            ArrayList vehicles = new ArrayList();
            vehicles = vendorBP.getVehicleConfig(vendorTO);
            request.setAttribute("vehicles", vehicles);

            ArrayList trailerList = new ArrayList();
            trailerList = vendorBP.getTrailerList(vendorTO);
            request.setAttribute("trailerList", trailerList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            if (vehicleStatus > 0 && trailerStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicles & Trailers Configured Successfully");
            } else if (vehicleStatus > 0 && trailerStatus == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicles Configured Successfully");
            } else if (vehicleStatus == 0 && trailerStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Trailer Configured Successfully");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Vehicles & Trailers Configured Failed, Please Contact System Admin.");
            }

            path = "content/vendor/viewVehicleTrailerContract.jsp";
//                    vendorList = vendorBP.processGetVendorLists(vendorTO);
//
//                    request.setAttribute("VendorList", vendorList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView viewVehicleTrailerContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();

//        String menuPath = "Vendor >>Configure Vehicle & Trailers";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        String pageTitle = " View Config Vehicle Trailer";
//        request.setAttribute("pageTitle", pageTitle);
//        path = "content/vendor/viewVehicleTrailerContract.jsp";
        path = "content/vendor/viewVehicleTrailerContract.jsp";
        String pageTitle = " View Config Vehicle Trailer";
        request.setAttribute("pageTitle", pageTitle);
        int userId = (Integer) session.getAttribute("userId");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String vehicleTypeIdDedicate = request.getParameter("vehicleTypeIdDedicate");
            String vehicleUnitsDedicate = request.getParameter("vehicleUnitsDedicate");
            String trailorTypeDedicate = request.getParameter("trailorTypeDedicate");
            String trailorUnitsDedicate = request.getParameter("trailorUnitsDedicate");
            String contractDedicateId = request.getParameter("contractDedicateId");
            String vendorId = request.getParameter("vendorId");
            request.setAttribute("vehicleTypeIdDedicate", vehicleTypeIdDedicate);
            request.setAttribute("vehicleUnitsDedicate", vehicleUnitsDedicate);
            request.setAttribute("trailorTypeDedicate", trailorTypeDedicate);
            request.setAttribute("trailorUnitsDedicate", trailorUnitsDedicate);
            request.setAttribute("contractDedicateId", contractDedicateId);
            request.setAttribute("vendorId", vendorId);
            vendorTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            vendorTO.setVehicleTypeIdDedicate(request.getParameter("vehicleTypeIdDedicate"));
            vendorTO.setVehicleUnits(request.getParameter("vehicleUnitsDedicate"));
            vendorTO.setTrailerType(request.getParameter("trailorTypeDedicate"));
            vendorTO.setTrailerUnits(request.getParameter("trailorUnitsDedicate"));
            vendorTO.setContractDedicateId(request.getParameter("contractDedicateId"));
            vendorTO.setVendorId(request.getParameter("vendorId"));

            ArrayList trailerList = new ArrayList();
            trailerList = vendorBP.getTrailerList(vendorTO);
            request.setAttribute("trailerList", trailerList);
            String pageTitle1 = "View Configure";
            request.setAttribute("pageTitle", pageTitle1);

            ArrayList vehicles = new ArrayList();
            vehicles = vendorBP.getVehicleConfig(vendorTO);
            request.setAttribute("vehicles", vehicles);
            String pageTitle2 = "View Configure Vehic";
            request.setAttribute("pageTitle", pageTitle2);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editVehicleTrailerContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        //public ModelAndView editVendorVehicleContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();
        //   ArrayList vendorVehicleContractLists = new ArrayList();
//        String menuPath = "Manage Vendor  >>  View ";
//        path = "content/vendor/editVehicleTrailerContract.jsp";
//        String pageTitle = "Vehicle & Trailer Configure";

        //  int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Vendor >>Edit Configure Vehicle & Trailers";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = " Edit Config Vehicle Trailer";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/vendor/editVehicleTrailerContract.jsp";
        int userId = (Integer) session.getAttribute("userId");
//        int loginRecordId = (Integer) session.getAttribute("loginRecordId");

        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            String vendorId = request.getParameter("vendorId");
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            String contractDedicateId = request.getParameter("contractDedicateId");
            String vehicleTypeDedicate = request.getParameter("vehicleTypeIdDedicate");
            String vehicleUnitsDedicate = request.getParameter("vehicleUnitsDedicate");
            String trailorTypeDedicate = request.getParameter("trailorTypeDedicate");
            String trailorUnitsDedicate = request.getParameter("trailorUnitsDedicate");
            String trailorTypeIdDedicate = request.getParameter("trailorTypeIdDedicate");
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            request.setAttribute("contractDedicateId", contractDedicateId);
            request.setAttribute("vehicleTypeDedicate", vehicleTypeDedicate);
            request.setAttribute("vehicleUnitsDedicate", vehicleUnitsDedicate);
            request.setAttribute("trailorTypeDedicate", trailorTypeDedicate);
            request.setAttribute("trailorUnitsDedicate", trailorUnitsDedicate);
            request.setAttribute("trailorTypeIdDedicate", trailorTypeIdDedicate);
            vendorTO.setTrailorTypeId(trailorTypeIdDedicate);
            vendorTO.setContractDedicateId(contractDedicateId);
            vendorTO.setVehicleTypeId(vehicleTypeId);
            vendorTO.setVendorId(vendorId);

            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);
            System.out.println("trailerTypeList" + trailerTypeList.size());

            ArrayList trailerList = new ArrayList();
            trailerList = vendorBP.getTrailerList(vendorTO);
            request.setAttribute("trailerList", trailerList);
            System.out.println("trailerList" + trailerList.size());

            ArrayList vehicles = new ArrayList();
            vehicles = vendorBP.getVehicleConfig(vendorTO);
            request.setAttribute("vehicles", vehicles);
            System.out.println("vehicles" + vehicles.size());

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            System.out.println("MfrList" + MfrList.size());

            ArrayList modelList = new ArrayList();
            modelList = vehicleBP.processGetModelList(vehicleTO);
            request.setAttribute("modelList", modelList);
            System.out.println("modelList" + modelList.size());

            // }
        } catch (FPRuntimeException exception) {

            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getCityFromList(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        System.out.println("getCityFromList -----------");
        HttpSession session = request.getSession();
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        OperationTO operTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList cityList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityFrom = "";
            //String cityToId = "";
            response.setContentType("text/html");
            cityFrom = request.getParameter("cityName");
            //String cityToId = request.getParameter("destinationIdFullTruck");
            System.out.println("cityFrom = " + cityFrom);
            //System.out.println("cityToId = " + cityToId);

            vendorTO.setCityId(cityFrom);
            //operTO.setCityId(cityToId);
            cityList = vendorBP.getCityFromList(vendorTO);
            System.out.println("userDetails.size() = " + cityList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = cityList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vendorTO = (VendorTO) itr.next();
                jsonObject.put("Value", vendorTO.getCityId() + "-" + vendorTO.getCityName());
                jsonObject.put("travelKm", vendorTO.getCityId() + "-" + vendorTO.getCityName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    /**
     * This method is used to Get City To For Ajax.
     *
     * @param request - Http request object.
     * @param response
     *
     * @param command - command object to bind the request values.
     * @throws java.io.IOException
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void getCityToList(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        System.out.println("----------------------getCityToList -----------");
        HttpSession session = request.getSession();
        vendorCommand = command;
        //VendorTO vendorTO = new VendorTO();
        OperationTO operTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList cityList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String cityTo = "";
            String originCityId = "";
            String pointId1 = "";
            String pointId2 = "";
            String pointId3 = "";
            String pointId4 = "";
            response.setContentType("text/html");
            cityTo = request.getParameter("cityName");
            //   originCityId = request.getParameter("originCityId");
            originCityId = request.getParameter("originCityId");
            pointId4 = request.getParameter("pointId4");
            if (!pointId4.equals("0")) {
                originCityId = pointId4;
            }
            pointId3 = request.getParameter("pointId3");
            if (!pointId3.equals("0")) {
                originCityId = pointId3;
            }
            pointId2 = request.getParameter("pointId2");
            if (!pointId2.equals("0")) {
                originCityId = pointId2;
            }
            pointId1 = request.getParameter("pointId1");
            if (!pointId1.equals("0")) {
                originCityId = pointId1;
            }
            System.out.println("cityTo = " + cityTo);

            operTO.setCity(cityTo);
            operTO.setOriginCityId(originCityId);
            //cityList = operationBP.getCity(operTO);
            cityList = vendorBP.getCityToList(operTO);
            System.out.println("cityList.size() = " + cityList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = cityList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operTO = (OperationTO) itr.next();
                jsonObject.put("cityId", operTO.getCityId());
                jsonObject.put("cityName", operTO.getCityToName());
                jsonObject.put("TravelHour", operTO.getTravelHour());
                jsonObject.put("TravelMinute", operTO.getTravelMinute());
                jsonObject.put("Distance", operTO.getDistance());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView viewVendorVehicleContractList(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        // ArrayList vendorVehicleContractLists = new ArrayList();
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/vendor/viewVendorVehicleWiseContract.jsp";
        String pageTitle = "Vendor Vehicle Wise Contract Lists";
        String vendorId = "", vendorName = "";
        vendorId = request.getParameter("vendorId");
        vendorName = request.getParameter("vendor");
        System.out.println("vendorId ===" + vendorId + " Name " + vendorName);
        request.setAttribute("vendorId", vendorId);
        request.setAttribute("vendorName", vendorName);
        vendorTO.setVendorId(vendorId);
        request.setAttribute("vendorId", vendorId);
        int userId = (Integer) session.getAttribute("userId");
        //    int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            String vendorContractLists = "";
            vendorContractLists = vendorBP.viewVendorVehicle(vendorTO);
            String[] cusTemp = null;
            if (vendorContractLists != null) {
                cusTemp = vendorContractLists.split("~");
                request.setAttribute("contractId", cusTemp[0]);
                vendorTO.setContrctId(Integer.parseInt(cusTemp[0]));
                request.setAttribute("contractTypeId", cusTemp[1]);
                request.setAttribute("startDate", cusTemp[2]);
                request.setAttribute("endDate", cusTemp[3]);
                request.setAttribute("paymentType", cusTemp[4]);
            }
            ArrayList dedicateList = new ArrayList();
            dedicateList = vendorBP.getDedicateList(vendorTO);
            request.setAttribute("dedicateList", dedicateList);
//              //  if (vendorTO.getContractTypeId().equals("1")) {
            ArrayList hireList = new ArrayList();
            hireList = vendorBP.getHireList(vendorTO);
            request.setAttribute("hireList", hireList);

            ArrayList containerTypeList = new ArrayList();
            containerTypeList = operationBP.getContainerTypeList();
            request.setAttribute("containerTypeList", containerTypeList);

//              //  if (vendorTO.getContractTypeId().equals("1")) {
//                    request.setAttribute("dedicateList", dedicateHireList);
//                } else if (vendorTO.getContractTypeId().equals("2")) {
//                    request.setAttribute("hireList", dedicateHireList);
//                }
            // }
            // ArrayList vendorVehicleContractLists=new ArrayList();
            // vendorVehicleContractLists = vendorBP.vendorVehicleContractList(vendorTO);
            // request.setAttribute("vendorVehicleContractLists", vendorVehicleContractLists);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editVendorVehicleContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();
        // ArrayList vendorVehicleContractLists = new ArrayList();
        String menuPath = "Manage Vendor  >>  Fleet Vendor Contract View";
        path = "content/vendor/editVendorContractDetails.jsp";
        String pageTitle = "Vendor Vehicle Wise Contract Lists";
        String vendorId = "", vendorName = "";
        vendorId = request.getParameter("vendorId");
        vendorName = request.getParameter("vendor");
        System.out.println("vendorId ===" + vendorId + " Name " + vendorName);
        request.setAttribute("menuPath", menuPath);
        request.setAttribute("vendorId", vendorId);

        request.setAttribute("vendorName", vendorName);
        vendorTO.setVendorId(vendorId);
        request.setAttribute("vendorId", vendorId);
        int userId = (Integer) session.getAttribute("userId");
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            String vendorContractLists = "";
            vendorContractLists = vendorBP.viewVendorVehicle(vendorTO);
            String[] cusTemp = null;
            if (vendorContractLists != null) {
                cusTemp = vendorContractLists.split("~");
                request.setAttribute("contractId", cusTemp[0]);
                vendorTO.setContrctId(Integer.parseInt(cusTemp[0]));
                vendorTO.setContractId(String.valueOf(Integer.parseInt(cusTemp[0])));
                request.setAttribute("contractTypeId", cusTemp[1]);
                request.setAttribute("startDate", cusTemp[2]);
                request.setAttribute("endDate", cusTemp[3]);
                request.setAttribute("paymentType", cusTemp[4]);
            }
            ArrayList dedicateList = new ArrayList();
            dedicateList = vendorBP.getDedicateList(vendorTO);
            request.setAttribute("dedicateList", dedicateList);
//              //  if (vendorTO.getContractTypeId().equals("1")) {
            ArrayList hireList = new ArrayList();
            hireList = vendorBP.getHireList(vendorTO);
            request.setAttribute("hireList", hireList);

            ArrayList fuelHike = new ArrayList();
            fuelHike = vendorBP.getFuelHike(vendorTO);
            request.setAttribute("fuelHike", fuelHike);
            System.out.println("edit fuellist-=-=-=-+++++++++++++++++++++++++++++ = " + fuelHike.size());

            vendorId = request.getParameter("vendorId");
            vendorName = request.getParameter("vendor");
            System.out.println("vendorId ===" + vendorId + " Name " + vendorName);
            request.setAttribute("vendorId", vendorId);
            request.setAttribute("vendorName", vendorName);
            vendorTO.setVendorId(vendorId);
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            vendorTO.setVehicleTypeId(vehicleTypeId);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            System.out.println("vehicleTypeId " + vehicleTypeId);
            //ArrayList TypeList = new ArrayList();
            //  TypeList = vendorBP.getVehicleTypeList(vendorTO);
            //  request.setAttribute("TypeList", TypeList);
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);

//                ArrayList vehicleList = new ArrayList();
//                vehicleList = vehicleBP.processTrailerList(vehicleTO);
//                request.setAttribute("vehicleList", vehicleList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);

            ArrayList uomList = new ArrayList();
            uomList = vendorBP.processGetUomList();
            request.setAttribute("uomList", uomList);

            ArrayList containerTypeList = new ArrayList();
            containerTypeList = operationBP.getContainerTypeList();
            request.setAttribute("containerTypeList", containerTypeList);
//              //  if (vendorTO.getContractTypeId().equals("1")) {
//                    request.setAttribute("dedicateList", dedicateHireList);
//                } else if (vendorTO.getContractTypeId().equals("2")) {
//                    request.setAttribute("hireList", dedicateHireList);
//                }
            //   }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editsaveVendorVehicleContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        VendorTO vendorTO = new VendorTO();
        VehicleTO vehicleTO = new VehicleTO();
        //  ArrayList updateVendorContract = new ArrayList();
        int updateVendorContract = 0;
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/vendor/editVendorContractDetails.jsp";
        String pageTitle = "Vendor Vehicle Wise Contract Lists";
        int userId = (Integer) session.getAttribute("userId");
        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");

        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Vendor-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            if (vendorCommand.getVendorId() != null && vendorCommand.getVendorId() != "") {
                vendorTO.setVendorId(vendorCommand.getVendorId());
                request.setAttribute("vendorId", vendorCommand.getVendorId());
            }
            if (vendorCommand.getContractTypeId() != null && vendorCommand.getContractTypeId() != "") {
                vendorTO.setContractTypeId(vendorCommand.getContractTypeId());
                request.setAttribute("contractTypeId", vendorCommand.getContractTypeId());
            }
            if (vendorCommand.getEndDateold() != null && vendorCommand.getEndDateold() != "") {
                vendorTO.setEndDateold(vendorCommand.getEndDateold());
                request.setAttribute("endDateold", vendorCommand.getEndDateold());
            }
            if (vendorCommand.getEndDate() != null && vendorCommand.getEndDate() != "") {
                vendorTO.setEndDate(vendorCommand.getEndDate());
                request.setAttribute("endDate", vendorCommand.getEndDate());
            }
            if (vendorCommand.getStartDate() != null && vendorCommand.getStartDate() != "") {
                vendorTO.setStartDate(vendorCommand.getStartDate());
                request.setAttribute("startDate", vendorCommand.getStartDate());
            }

            if (vendorCommand.getPaymentType() != null && vendorCommand.getPaymentType() != "") {
                vendorTO.setPaymentType(vendorCommand.getPaymentType());
                request.setAttribute("paymentType", vendorCommand.getPaymentType());
            }

            // String vendorId = request.getParameter("vendorId");
            String contractTypeId = request.getParameter("contractTypeId");
            String endDateold = request.getParameter("endDateold");
            String endDate = request.getParameter("endDate");
            // vendorTO.setVendorId(vendorId);
            vendorTO.setContractTypeId(contractTypeId);
            vendorTO.setEndDateold(endDateold);
            vendorTO.setEndDate(endDate);

            String startDate = request.getParameter("startDate");
            request.setAttribute("startDate", startDate);
            request.setAttribute("endDate", endDate);
            String vehicleTypeId = request.getParameter("vehicleTypeId");
            vendorTO.setVehicleTypeId(vehicleTypeId);
            request.setAttribute("vehicleTypeId", vehicleTypeId);
            System.out.println("vehicleTypeId " + vehicleTypeId);
            //ArrayList TypeList = new ArrayList();
            //  TypeList = vendorBP.getVehicleTypeList(vendorTO);
            //  request.setAttribute("TypeList", TypeList);
            ArrayList TypeList = new ArrayList();
            TypeList = vehicleBP.processGetTypeList();
            request.setAttribute("TypeList", TypeList);

//                ArrayList vehicleList = new ArrayList();
//                vehicleList = vehicleBP.processTrailerList(vehicleTO);
//                request.setAttribute("vehicleList", vehicleList);
            ArrayList trailerTypeList = new ArrayList();
            trailerTypeList = vehicleBP.processTrailerTypeList(vehicleTO);
            request.setAttribute("trailerTypeList", trailerTypeList);

            ArrayList uomList = new ArrayList();
            uomList = vendorBP.processGetUomList();
            request.setAttribute("uomList", uomList);
            ArrayList fuelHike = new ArrayList();
            fuelHike = vendorBP.getFuelHike(vendorTO);
            request.setAttribute("fuelHike", fuelHike);
            //   vendorTO.setContractTypeId(Integer.parseInt(contractTypeId));

            vendorTO.setVendorName(request.getParameter("vendorName"));
            if (!"".equals(endDate) && endDate != null && !"".equals(endDateold) && endDateold != null && contractTypeId != null && !endDateold.equals(endDate)) {
                updateVendorContract = vendorBP.updateVendorContractDetails(vendorTO, userId);
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getModel(HttpServletRequest request, HttpServletResponse response, VendorCommand command) {

        vendorCommand = command;

        String mfr = request.getParameter("mfr");
        String vehicleTypeId = request.getParameter("vehicleTypeId");
        String suggestions = "";

        try {
            //Hari
            if (mfr != null && !"".equals(mfr)) {
                suggestions = vendorBP.processGetModels(Integer.parseInt(mfr), vehicleTypeId);
            }
            System.out.println(" suggestions"+suggestions);
            //Hari End
            PrintWriter writer = response.getWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    public ModelAndView saveEditVehicleTrailerContract(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        //  int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        //    int conractId = (Integer) session.getAttribute("contractId");
        String menuPath = "Vendor >> View Vehicle trailer Contract";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Config Vendor";
        request.setAttribute("pageTitle", pageTitle);

        int status = 0;
        int status1 = 0;
        int vehicleStatus = 0;
        String vendorId = "";
        String contractDedicateId = "";
        vendorId = request.getParameter("vendorId");

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VendorConfig-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
//                if(vendorId == null){
//                    vendorTO.setVendorId(vendorCommand.getVendorId());
//                }else{
            vendorTO.setVendorId(vendorId);
//                }
            System.out.println("vendorId I am here nat " + vendorId);
            vendorTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            vendorTO.setVehicleUnits(request.getParameter("vehicleUnitsDedicate"));
            vendorTO.setTrailerType(request.getParameter("trailorTypeDedicate"));
            vendorTO.setTrailerUnits(request.getParameter("trailorUnitsDedicate"));
            vendorTO.setContractDedicateId(request.getParameter("contractDedicateId"));
            request.setAttribute("contractDedicateId", request.getParameter("contractDedicateId"));

            System.out.println("contractDedicateId I am here raj " + request.getParameter("contractDedicateId"));

            if (vendorCommand.getContractId() != null && vendorCommand.getContractId() != "") {
                vendorTO.setContractId(vendorCommand.getContractId());
            }
            if (vendorCommand.getTrailorTypeId() != null && vendorCommand.getTrailorTypeId() != "") {
                vendorTO.setTrailorTypeId(vendorCommand.getTrailorTypeId());
            }

            String[] trailerNo = request.getParameterValues("trailerNo");
            String[] trailerRemarks = request.getParameterValues("trailerRemarks");
            String[] activeIndTrailor = request.getParameterValues("activeIndTrailor");
            if (trailerNo != null) {
                status = vendorBP.processUpdateTrailerContract(vendorTO, trailerNo, activeIndTrailor, trailerRemarks, userId);
                System.out.println("vehicles ====" + status);
            }

            String[] replacementStatus = request.getParameterValues("replacementStatus");
            String[] configureId = request.getParameterValues("configureId");
            String[] configureIds = request.getParameterValues("configureIds");
            String[] replaceVehicleId = request.getParameterValues("replaceVehicle");
            String[] vehicleRegNo = request.getParameterValues("vehicleRegNo");
            String[] agreedDate = request.getParameterValues("agreedDate");
            String[] mfr = request.getParameterValues("mfr");
            String[] model = request.getParameterValues("model");
            String[] vehicleRemarks = request.getParameterValues("remarks");
            String[] activeIndVehicle = request.getParameterValues("activeIndVehicle");
            String[] existVehicle = request.getParameterValues("existVehicle");
            String[] inDate = request.getParameterValues("inDate");
            if (vehicleRegNo != null) {
                vehicleStatus = vendorBP.processUpdateVehicleContract(vendorTO, configureId, configureIds, replaceVehicleId, vehicleRegNo, agreedDate, mfr, model, vehicleRemarks, activeIndVehicle, existVehicle, inDate, replacementStatus, userId);
                System.out.println("vehicleStatus   ======== " + vehicleStatus);
            }

            ArrayList vehicles = new ArrayList();
            vehicles = vendorBP.getVehicleConfig(vendorTO);
            request.setAttribute("vehicles", vehicles);

            ArrayList trailerList = new ArrayList();
            trailerList = vendorBP.getTrailerList(vendorTO);
            request.setAttribute("trailerList", trailerList);

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            if (vehicleStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Vehicles & Trailers Updated Successfully");

            }

            path = "content/vendor/viewVehicleTrailerContract.jsp";
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public void checkVehicleRegNoExists(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException, FPBusinessException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String vehicleRegNo = request.getParameter("vehicleRegNo");
        System.out.println("vehicleRegNo" + vehicleRegNo);
        String suggestions = "";
        suggestions = vendorBP.checkVehicleRegNo(vehicleRegNo);
        System.out.println("suggestions55555" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();

    }

    public void checkTrailerExists(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException, FPBusinessException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        String trailerNo = request.getParameter("trailerNo");
        System.out.println("trailerNo" + trailerNo);
        String suggestions = "";
        suggestions = vendorBP.checkTrailerNo(trailerNo);
        System.out.println("suggestions55555" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();

    }

    public void getVendorNameDetails(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        VendorTO vendorTO = new VendorTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList vendorList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String vendorName = "";
            response.setContentType("text/html");
            vendorName = request.getParameter("vendorName");
            vendorTO.setVendorName(vendorName);
            vendorList = vendorBP.getVendorNameDetails(vendorTO);
            System.out.println("customerList.size() = " + vendorList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vendorList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vendorTO = (VendorTO) itr.next();
                jsonObject.put("Name", vendorTO.getVendorId() + "-" + vendorTO.getVendorName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void checkVendorNameExists(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        VendorTO vendorTO = new VendorTO();
//                JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            int checkVendorName = 0;
            response.setContentType("text/html");
            vendorTO.setVendorName(request.getParameter("vendorName"));
            vendorTO.setVendorTypeId(request.getParameter("vendorTypeId"));
            checkVendorName = vendorBP.checkVendorNameExists(vendorTO);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VendorCount", checkVendorName);
            pw.print(jsonObject);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getMarketHireRate(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        HttpSession session = request.getSession();
        VendorTO vendorTO = new VendorTO();
        ArrayList customerList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String marketHireRate = "";
            String checkRoute = "";
            response.setContentType("text/html");
            vendorTO.setVehicleTypeId(request.getParameter("vehicleTypeId"));
            vendorTO.setLoadTypeId(request.getParameter("loadTypeId"));
            vendorTO.setContainerTypeId(request.getParameter("containerTypeId"));
            vendorTO.setContainerQty(request.getParameter("containerQty"));
            vendorTO.setOriginIdFullTruck(request.getParameter("originIdFullTruck"));
            vendorTO.setPointId1(request.getParameter("pointId1"));
            vendorTO.setPointId2(request.getParameter("pointId2"));
            vendorTO.setPointId3(request.getParameter("pointId3"));
            vendorTO.setPointId4(request.getParameter("pointId4"));
            vendorTO.setDestinationIdFullTruck(request.getParameter("destinationIdFullTruck"));
            marketHireRate = vendorBP.getMarketHireRate(vendorTO);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("MarketHireRate", marketHireRate);
            System.out.println("jsonObject = " + jsonObject);
            pw.print(jsonObject);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getReplaceVehicle(HttpServletRequest request, HttpServletResponse response, VendorCommand command) throws IOException {
        System.out.println("getReplaceVehicle -----------");
        HttpSession session = request.getSession();
        vendorCommand = command;
        VendorTO vendorTO = new VendorTO();
        OperationTO operTO = new OperationTO();
//        JsonTO jsonTO = new JsonTO();
        ArrayList replaceVehicleList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String replaceVehicle = "";
            //String cityToId = "";
            response.setContentType("text/html");
            replaceVehicle = request.getParameter("replaceVehicle");
            //String cityToId = request.getParameter("destinationIdFullTruck");
            System.out.println("replaceVehicle = " + replaceVehicle);
            //System.out.println("cityToId = " + cityToId);

            vendorTO.setVehicleId(replaceVehicle);
            //operTO.setCityId(cityToId);
            replaceVehicleList = vendorBP.getReplaceVehicle(vendorTO);
            System.out.println("replaceVehicleList.size() = " + replaceVehicleList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = replaceVehicleList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                vendorTO = (VendorTO) itr.next();
                jsonObject.put("Name", vendorTO.getVehicleRegNo());
//                jsonObject.put("travelKm", vendorTO.getCityId() + "-" + vendorTO.getCityName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

     public ModelAndView updateContractApprovalVendor(HttpServletRequest request, HttpServletResponse reponse, VendorCommand command) throws IOException {
        System.out.println("Iam is in the update Cust Approved Status");
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }

        HttpSession session = request.getSession();
        vendorCommand = command;
        String path = "";
      //  int userId = (Integer) session.getAttribute("userId");
        int userId =1000;
        VendorTO vendorTO = new VendorTO();
        String menuPath = "";
        menuPath = "Operation  >> update Cust Approved Status ";
        String pageTitle = "update Cust Approved Status ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String contractRateId = "", status = "",approvedStatus="";

            contractRateId = request.getParameter("contractRateId");
            System.out.println("contractRateId"+contractRateId);
            status = request.getParameter("status");

            int updateStatus = vendorBP.updateVendorApprovedStatus(userId,contractRateId,status);
            System.out.println("updateStatus==" + updateStatus);

            if(updateStatus == 1){
                path = "BrattleFoods/approvedResponse.html";
            }else if(updateStatus == 3){
                path = "BrattleFoods/rejectResponse.html";
            }

//            mv = approveFuelPrice(request,response,command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert  Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }


}
