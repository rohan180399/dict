<%-- 
    Document   : manageConfigVendor
    Created on : Mar 2, 2009, 2:49:16 PM
    Author     : karudaiyar Subramaniam
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vendor.business.VendorTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        
        <script language="javascript">    
            
            
            function submitPage(value){ 
                if(value == "search"){    
                    document.addparts.action = '/throttle/configItemForVendorPage.do';
                }
                else if(value == "save"){
                    var length = document.addparts.ItemList.length;
                    var counter = document.addparts.AssignedList.length;            
                    for(var j = 0; j < counter; j++){						
                        document.addparts.AssignedList.options[j].selected = true;                   
                    } 
                    document.addparts.action = '/throttle/saveConfig.do';
                    
                }
                document.addparts.submit();
            }
            
            
            
            
            var httpReq;
            var temp = "";
            function ajaxData()
            {
                var url = "/throttle/getModels1.do?mfrId="+document.addparts.mfrId.value;    
                if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpReq = new XMLHttpRequest();
                        }
                        httpReq.open("GET", url, true);
                        httpReq.onreadystatechange = function() { processAjax(); } ;
                        httpReq.send(null);
                    }
                    
                    
                    function processAjax()
                    {
                        if (httpReq.readyState == 4)
                            {
                                if(httpReq.status == 200)
                                    {
                                        temp = httpReq.responseText.valueOf();
                                        
                                        setOptions(temp,document.addparts.modelId);
                                    }
                                    else
                                        {
                                            alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                        }
                                    }
                                }
                                
                                
                                function ajaxRackData()
                                {
                                    var url = "/throttle/getVendor.do?vendorTypeId="+document.addparts.vendorTypeId.value;    
                                    
                                    if (window.ActiveXObject)
                                        {
                                            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                        }
                                        else if (window.XMLHttpRequest)
                                            {
                                                httpReq = new XMLHttpRequest();
                                            }
                                            httpReq.open("GET", url, true);
                                            httpReq.onreadystatechange = function() { processRackAjax(); } ;
                                            httpReq.send(null);
                                        }
                                        
                                        
                                        function processRackAjax()
                                        {
                                            if (httpReq.readyState == 4)
                                                {
                                                    if(httpReq.status == 200)
                                                        {
                                                            temp = httpReq.responseText.valueOf();        
                                                            setOptions(temp,document.addparts.vendorId);
                                                            if('<%= request.getAttribute("vendorId") %>'!= 'null'){
                                                            document.addparts.vendorId.value='<%= request.getAttribute("vendorId") %>';
                                                            }
                                                        }
                                                        else
                                                            {
                                                                alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                                            }
                                                        }
                                                    }
                                                    
                                                    
                                                    
                                                    function copyAddress(ItemList)
                                                    {
                                                        var itemId = document.getElementById("itemId");
                                                        var selectedLength = 0;
                                                        if(itemId.length != 0){        
                                                            for (var i=0; i<itemId.options.length ; i++) {    
                                                                if(itemId.options[i].selected == true){        
                                                                    selectedLength++;
                                                                }    
                                                            }
                                                            
                                                            for (var j=0; j<selectedLength ; j++) {
                                                                for (var k=0; k<itemId.options.length ; k++) {
                                                                    if(itemId.options[k].selected == true){  
                                                                        var optionCounter = document.addparts.assignedList.length;
                                                                        document.addparts.assignedList.length = document.addparts.assignedList.length +1;
                                                                        document.addparts.assignedList.options[optionCounter].text =itemId.options[k].text ;
                                                                        document.addparts.assignedList.options[optionCounter].value = itemId.options[k].value ;
                                                                        itemId.options[k]=null;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            alert("Please Select any Value");
                                                        }
                                                    }
                                                    
                                                    
                                                    function copyAddress1(AssignedList)
                                                    {
                                                        
                                                        var selectedValue = document.getElementById('AssignedList');
                                                        var selectedLength = 0;
                                                        
                                                        if(selectedValue.length !=0){    
                                                            for (var i=0; i<selectedValue.options.length ; i++) {    
                                                                if(selectedValue.options[i].selected == true){
                                                                    selectedLength++;
                                                                }    
                                                            }
                                                            
                                                            
                                                            for (var j=0; j<selectedLength ; j++) {
                                                                for (var k=0; k<selectedValue.options.length ; k++) {
                                                                    if(selectedValue.options[k].selected == true){
                                                                        var optionCounter = document.addparts.itemId.length;
                                                                        document.addparts.itemId.length = document.addparts.itemId.length +1;
                                                                        document.addparts.itemId.options[optionCounter].text = selectedValue.options[k].text ;
                                                                        document.addparts.itemId.options[optionCounter].value = selectedValue.options[k].value ;
                                                                        selectedValue.options[k] = null;
                                                                        break;		
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            alert("Please Select any Value");
                                                        }
                                                    }  
                                                    
        function loadValues(val,typeId){
            if(typeId!='null'){            
                document.addparts.vendorTypeId.value = typeId;          
                ajaxRackData();
            }
        }
                                                    
                                                    
                                                    
        </script>
    </head>
    <!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
    <![endif]-->
    <body onLoad="loadValues('<%= request.getAttribute("vendorId") %>','<%= request.getAttribute("vendorTypeId") %>');" >
        <form name="addparts"  method="post" >
            
            
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                  
                    <!-- title table -->
                </div>
            </div>
            
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            
            <table  border="0" class="tdcolor" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td colspan="4" height="30" class="contenthead"><b>Search Parts</b></td>
                    <td colspan="4" height="30" class="contenthead"></td>
                </tr>
                <tr>
                    <td class="text2"  >Vendor Type</td>
                    <td class="text2"  ><select class="form-control" name="vendorTypeId" onChange="ajaxRackData();" >
                            <option value="0">---Select---</option>
                            <c:if test = "${VendorTypeList != null}" >
                                <c:forEach items="${VendorTypeList}" var="Type"> 
                                    <option value='<c:out value="${Type.vendorTypeId}" />'><c:out value="${Type.vendorTypeValue}" /></option>
                                </c:forEach >
                            </c:if>  	
                    </select></td>
                    <td class="text2"  >Vendor</td>
                    <td class="text2"  ><select class="form-control" name="vendorId"  >
                            <option value="0">---Select---</option>
                            <c:if test = "${AjaxVendorList != null}" >
                                <c:forEach items="${AjaxVendorList}" var="Type"> 
                                    <option value='<c:out value="${Type.vendorId}" />'><c:out value="${Type.vendorName}" /></option>
                                </c:forEach >
                            </c:if>  	
                    </select></td>
                </tr>
                <tr >
                    
                    
                    <td class="text1" height="30">MFR</td>
                    <td class="text1"  ><select class="form-control" name="mfrId" onChange="ajaxData();" >
                            <option value="">---Select---</option>
                            <c:if test = "${MfrList != null}" >
                                <c:forEach items="${MfrList}" var="Dept"> 
                                    <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                </c:forEach >
                            </c:if>  	
                    </select></td>
                    <td class="text1" height="30">Model</td>
                    <td class="text1"  ><select class="form-control" name="modelId" >
                            <option value="">---Select---</option>
                            <c:if test = "${ModelList != null}" >
                                <c:forEach items="${ModelList}" var="Dept"> 
                                    <option value='<c:out value="${Dept.modelId}" />'><c:out value="${Dept.modelName}" /></option>
                                </c:forEach >
                            </c:if>  	
                    </select></td>
                    
                    <td class="text1" height="30">Section</td>
                    <td class="text1"  ><select class="form-control" name="sectionId" >
                            <option value="">---Select---</option>
                            <c:if test = "${SectionList != null}" >
                                <c:forEach items="${SectionList}" var="Dept"> 
                                    <option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
                                </c:forEach >
                            </c:if>  	
                    </select></td>
                </tr>
            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)">
            </center>
            <br>
            <br>
            
            <table align="center" border="1" cellpadding="0" cellspacing="0" width="500" id="bg">
                
                <tr>
                    <td class="contenthead" valign="center" height="35" >Available Items </td>
                    <td class="contenthead" height="35"></td>
                    <td class="contenthead" valign="center" height="35">Assigned Items</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="150" height="150" cellpadding="0" cellspacing="0" border="2" align="center" id="noline">
                            <tr>  
                                <td width="150"><select style="width:200px;" multiple size="10" name="ItemList" id="itemId">
                                        <c:if test = "${ItemList != null}" >
                                            <c:forEach items="${ItemList}" var="sems"> 
                                                <option value='<c:out value="${sems.itemId}" />'><c:out value="${sems.itemId}-" /><c:out value="${sems.itemName}" /></option>
                                            </c:forEach>
                                        </c:if>
                                </select></td>
                            </tr>
                        </table>                                               
                    </td>
                    
                    <td height="100" valign="center">   
                        <table width="20" height="70" cellpadding="2" cellspacing="2" border="2" align="center" id="box">
                            <tr>
                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="form-control" value=">" onClick="copyAddress(ItemList.value)"></td></tr>
                            <tr >
                            <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="form-control" value="<" onClick="copyAddress1(AssignedList.value)"></td></tr>
                    </table>
                    </td>
                    <td valign="top">
                        
                        <table width="150" height="150" cellpadding="0" cellspacing="0" border="2" align="center" id="noline">
                            <tr>
                                <td width="150"><select style="width:200px;" multiple size="10" name="assignedList" id="AssignedList" >
                                        <c:if test = "${AssignedList != null}" >
                                            <c:forEach items="${AssignedList}" var="sem"> 
                                                <option value='<c:out value="${sem.itemId}" />'><c:out value="${sem.itemId}-" /> <c:out value="${sem.itemName}" /> </option>
                                            </c:forEach >
                                        </c:if>
                                </select></td>
                            </tr>
                    </table></td>
                </tr> 
                
            </table>
            <center>
                
                <input type="button" name="save" value="Save" onClick="submitPage(this.name)" class="button">
                
                
                <input type="hidden" value="" name="temp" >
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
</html>


