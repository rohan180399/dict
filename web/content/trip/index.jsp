<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import=" java. util. * "%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.text.DecimalFormat" %>
<link href='/throttle/fullcalendar/fullcalendar.css' rel='stylesheet' />
<script src='/throttle/fullcalendar/lib/moment.min.js'></script>
<script src='/throttle/fullcalendar/lib/jquery.min.js'></script>
<script src='/throttle/fullcalendar/fullcalendar.min.js'></script>
<script>
     var jsonVehicleData = [];
</script>

<script>
//    title: value.title,
//    start: value.start, // will be parsed
//    end: value.end, // will be parsed
//    className:clsName,
//    url: 'viewVehidleDetails.do?'
  
  jsonVehicleData.push({title: 'Chennai/Load', start: '2015-05-30',end: 'null',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Bangalore/Empty', start: '2015-06-01',end: '2015-05-05',url:'viewVehicleDetails.do?', className:'two'});
  jsonVehicleData.push({title: 'Hydrabad/Load', start: '2015-06-06',end: '2015-05-07',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Chennai/Load', start: '2015-06-09',end: '2015-06-09',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Delhi/Load', start: '2015-06-11',end: '2015-06-11',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Mumbai/Load', start: '2015-06-12',end: '2015-06-14',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Cochin/Empty', start: '2015-06-15',end: '2015-06-15',url:'viewVehicleDetails.do?', className:'two'});
  jsonVehicleData.push({title: 'Hydrabad/Load', start: '2015-06-16',end: '2015-06-17',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Delhi/Load', start: '2015-06-18',end: '2015-06-19',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Mumbai/Empty', start: '2015-06-19',end: '2015-06-20',url:'viewVehicleDetails.do?', className:'two'});
  jsonVehicleData.push({title: 'Chennai/Load', start: '2015-06-21',end: '2015-06-24',url:'viewVehicleDetails.do?', className:'one'});
  jsonVehicleData.push({title: 'Chennai/FC', start: '2015-06-24',end: '2015-06-25',url:'viewVehicleDetails.do?', className:'three'});
</script>

<script>

$(document).ready(function() {

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},


		selectable: true,
		selectHelper: true,
		select: function(start, end) {
			//var title = prompt('Event Title:');
			var title = '';
			var eventData;
			if (title) {
				eventData = {
					title: title,
					start: start,
					end: end
				};
				$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
			}

			$('#calendar').fullCalendar('unselect');
		},

		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: {

		},
		eventRender: function(event, element) {
				var dateString = moment(event.start).format('YYYY-MM-DD');
				if(event.className=="one"){
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#e8e8e8');
				}
				if(event.className=="two"){
						// $('.fc-day[data-date="'+ dateString +'"]').addClass('two');
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#fbcdcf');
				}
				if(event.className=="three"){
						$('.fc-day[data-date="'+ dateString +'"]').css('background','#73ff00');
				}
		},
		events: function(start, end, timezone, callback) {
			$('#spinner').show();
			$("#loader").addClass('ui-loader-background');
                        callback(jsonVehicleData);
		}


	});

});
</script>
<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.td.fc-day.one {
		background: #000;
	}
	.td.fc-day.two {
		background: #F00;
	}
	.td.fc-day.three {
		background: #0F0;
	}

</style>
</head>
<body>

	<div id='calendar'></div>

</body>
</html>
