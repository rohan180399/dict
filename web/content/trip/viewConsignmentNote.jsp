
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewInvoiceDetails(invId, tripId) {
//            window.open('/throttle/showinvoicedetail.do?invoiceId='+invId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        window.open('/throttle/submitBillCourierDetails.do?invoiceId=' + invId + '&tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>




<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripGeneration() {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //alert(temp);
        //alert(cntr);
        if (cntr > 0) {
            //alert("am here..");
            document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp;
            //alert("am here..1:"+document.CNoteSearch.action);
            document.CNoteSearch.submit();
            //alert("am here..2");
        } else {
            alert("Please select consignment order and proceed");
        }
    }


    function searchPage() {
        document.CNoteSearch.action = "/throttle/consignmentNoteView.do";
        document.CNoteSearch.submit();
    }
    function excelExport() {
        document.CNoteSearch.action = "/throttle/CNoteReportExcel.jsp";
        document.CNoteSearch.submit();
    }
    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }
</script>


    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Primary Operation</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
                <li><a href="general-forms.html">Report</a></li>
                <li class="active">CONSIGNMENT ORDERS</li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
<body>
                <form name="CNoteSearch" method="post" enctype="multipart">
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">CONSIGNMENT ORDERS</td></tr>
                        <tr>
                            <td >Customer Name</td>
                            <td>
                                <input type="hidden" class="form-control" name="customerId" id="customerId" style="width:180px;height:40px;" value="<c:out value="${customerId}"/>"/>
                                <input type="text" class="form-control" name="customerName" style="width:180px;height:40px;"  id="customerName" style="width: 110px" value="<c:out value="${customerName}"/>"/>
                            </td>

                            <td >Customer Order Ref No</td>
                            <td><input type="text" class="form-control" name="customerOrderReferenceNo" style="width:180px;height:40px;" id="customerOrderReferenceNo" style="width: 110px" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>


                            

<td >Consignment Order No</td>
                            <td><input type="text" class="form-control" name="consignmentOrderReferenceNo" style="width:180px;height:40px;"  id="consignmentOrderReferenceNo" style="width: 110px" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>

                        </tr>
                        
                        <tr>
<!--                            <td height="30">Status</td>
                            <td height="30" style="width:180px;height:40px;">
                                <select name="status" id="status"  style="width:150px;height:25px;" >
                                    <c:if test="${statusDetails != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <option value='<c:out value="${statusDetails.status}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>

                            </td>-->
                            

                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:40px;"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:40px;" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr align="center">
                            <!--                                    <td colspan="2"><input type="button"   value="Generate Excel" class="button" name="search" onClick="exportExcel()" style="width:150px"></td>
                                                                <td colspan="2"><input type="button"   value="Upload Trip Planning" class="button" name="search" onClick="importExcel()" style="width:200px"></td>-->
                        <center>
                            <td colspan="6"><input type="button" class="btn btn-info"   value="Search" onclick="searchPage()"></td>
                        </center>
                        </tr>
                    </table>
                    <c:if test = "${consignmentList != null}" >
                             <table class="table table-info mb30 table-hover" style="width:100%" id="table">
                            <thead height="40">
                                <tr id="tableDesingTH" height="50">
                                    <th>Sno</th>
                            <th>Order No</th>
                            <th>Consignment No</th>
                            <th>Bill of Entry </th>
                            <th>Consignment Date</th>
                            <th>Billing Party Name </th>
                            <th>Customer Name </th>
                            <th>Origin </th>
                            <th>Destination </th>
                            <th>Schedule </th>
                            <th>Order Type </th>
                            <th>Status </th>
                            <th>TripInfo</th>
                            <th>InvoiceInfo</th>
                            <th>Created By</th>
                           <th>select</th>
                            </tr>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${consignmentList}" var="cnl">
                                    <%

                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>

                                    <tr height="30">
                                        <td class="<%=classText%>" align="left" ><%=sno%></td>
                                        <td class="<%=classText%>" class="<%=classText%>" align="left" ><c:out value="${cnl.customerOrderReferenceNo}"/></td>
                                        <%--                                <td class="<%=classText%>" align="left" class="<%=classText%>"><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/><c:out value="${cnl.consignmentNoteNo}"/></td>--%>
                                        <td class="<%=classText%>" align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>

                                            <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                                        </td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.billOfEntry}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.createdOn}"/></td>

                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.customerName}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.consigneeName}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.orderType}"/></td>
                                        <td class="<%=classText%>" align="left" ><c:out value="${cnl.statusName}"/></td>
                                        <c:set var="tripCodeInfo" value="${cnl.tripCode}" />
                                        <td class="<%=classText%>" align="left" >
                                            <c:if test = "${cnl.tripCode == null}" >
                                                -
                                            </c:if>
                                            <c:if test = "${cnl.tripCode != null}" >
                                                <c:out value="${cnl.tripCode}"/>
                                            </c:if>
                                            &nbsp;
                                        </td>
                                        <c:set var="invoiceInfo" value="${cnl.invoiceNo}" />
                                        <td class="<%=classText%>" align="left" >
                                            <c:if test = "${cnl.invoiceNo == null}" >
                                                -
                                            </c:if>
                                            <c:if test = "${cnl.invoiceNo != null}" >
                                                <c:out value="${cnl.invoiceNo}"/>
                                            </c:if>

                                            &nbsp;
                                        </td>
                                        <td>
                                            <c:out value="${cnl.userName}"/>
                                            &nbsp;
                                        </td>

                                       <td>
                                            <a href='/throttle/getConsignmentDetailsForEdit.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>'">edit</a>

                                            &nbsp;
                                        </td>

                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <!--        <center>
                                <input type="button" class="button" name="trip"  value="Trip Planning" onclick="submitPageForTripGeneration()"/>
                            </center>-->
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
