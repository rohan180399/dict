<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
       <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

    <style type="text/css" media="screen">
    <!--
	body {
		font:normal 12px Arial, Helvetica, sans-serif;
	}

		UL.tabNavigation {
		    list-style: none;
		    margin: 0;
		    padding: 0;
		}

		UL.tabNavigation LI {
			display:inline;
			float:left;
			width:105px;
			height:23px;
			text-align:center;
			margin-left:5PX;

		}

		UL.tabNavigation LI A {
			display:block;
		    padding: 3px 5px;
			background:url(/throttle/images/tab_bg.png) no-repeat 0 0;
		    color: #fff;
						width:97px;
			height:23px;
		    text-decoration: none;
		}

		UL.tabNavigation LI A.selected,
		UL.tabNavigation LI A:hover {
		    background:url(/throttle/images/tab_over.png) no-repeat 0 0;
		    color: #000;
						width:97px;
			height:23px;
		    padding: 3px 5px;
		}

		UL.tabNavigation LI A:focus {
						width:97px;
			height:23px;
			outline: 0;
		}

		div.tabs > div {
			padding: 5px;
			margin-top: 22px;
		}



		#first {

		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
			padding-top:20px;
		}

		#second {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}

		#third {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}

		#fourth {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}
		#fifth {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}
		#sixth {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}
		#seventh {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}
		#eigth {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}
		#ninth {
		    
		    background-color: #f1f8ff;
			border:1px solid #6a9bcd;
		}

		#txt {
			color:#626262;
		}

		#txt a {
			color:#626262;
			text-decoration:none;
		}

		#txt a:hover {
			color:#626262;
			text-decoration:underline;
		}


    -->
    </style>


	<script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		$(function () {
			var tabContainers = $('div.tabs > div');
			tabContainers.hide().filter(':first').show();

			$('div.tabs ul.tabNavigation a').click(function () {
				tabContainers.hide();
				tabContainers.filter(this.hash).show();
				$('div.tabs ul.tabNavigation a').removeClass('selected');
				$(this).addClass('selected');
				return false;
			}).filter(':first').click();
		});



        
        function loadOTFrame(){            
            var wheight = document.getElementById("wheight").value;            
            frames['otFrame'].location.href="/throttle/content/dashboard/TyreDB.jsp";
        }
        function loadDBFrame(){            
            //frames['dbFrame'].location.href="/throttle/serviceDB.do";
            frames['dbFrame'].location.href="/throttle/content/dashboard/ServicesDB.jsp";
        }
        function loadITDBFrame(){
            //frames['itFrame'].location.href="/throttle/content/dashboard/StoresDB.jsp";
            frames['itFrame'].location.href="/throttle/content/dashboard/TripDB.jsp";
//            frames['itFrame'].location.href="./Highcharts/index.html";
        }
        
        function loadDFrame(){            
            frames['dFrame'].location.href="";
        }
        
        function loadSDFrame(){            
            frames['sdFrame'].location.href="";

        }
        function loadSuspendedFrame(){            
            frames['suspendedFrame'].location.href="";

        }
        function loadOTCFrame(){            
          var wheight = document.getElementById("wheight").value;
          frames['otcFrame'].location.href="/throttle/content/dashboard/RcDB.jsp";

        }
        function loadDCFrame(){            
            frames['dcFrame'].location.href="";

        }
        function loadSDCFrame(){            
            frames['sdcFrame'].location.href="";
        }
	function loadGovFrame(){            
            frames['govFrame'].location.href="/throttle/content/dashboard/govDB.jsp";
        }
	function loadUpdateFrame(){
            frames['govFrame'].location.href="/throttle/handleLatestUpdates.do";
        }
	</script>
<body >
    <form name="hlayout">
    <div class="tabs">
        <ul class="tabNavigation">
	    <li><a href="#veryfirst" onclick="loadGovFrame();" target="govFrame">Governance</a></li>        	
            <li><a href="#fifth" onclick="loadITDBFrame();" target="itFrame">Trip Sheet</a></li>
            <li><a href="#first" onclick="loadDBFrame();" target="dbFrame">Service</a></li>
            <li><a href="#second" onclick="loadOTFrame();" target="otFrame">Tyre</a></li>
            <li><a href="#seventh" onclick="loadOTCFrame();" target="otcFrame">RC</a></li>
	    <li><a href="#updateFrame" onclick="loadUpdateFrame();" target="govFrame">Updates</a></li>
        </ul>
<!--            <ul class="tabNavigation">
	    <li><a href="#veryfirst" onclick="loadGovFrame();" target="govFrame">Facility</a></li>
            <li><a href="#fifth" onclick="loadITDBFrame();" target="itFrame">Regional WH</a></li>
            <li><a href="#first" onclick="loadDBFrame();" target="dbFrame">State WH</a></li>
            <li><a href="#second" onclick="loadOTFrame();" target="otFrame">Admin</a></li>
            <li><a href="#seventh" onclick="loadOTCFrame();" target="otcFrame">Supplier</a></li>
        </ul>-->

        <div id="first" align="center" >
            
            <iframe width="100%"  id="dbFrame" name="dbFrame"  frameborder="0" align="middle"  height="600"  ></iframe>
            	

        </div>
        <div id="updateFrame" align="center" style="border:1px solid;">
        	<input type="hidden" id="wheight" name="wheight" value=""/>
	         <iframe width="100%" src="/throttle/handleLatestUpdates.do" id="govFrame" name="govFrame"  frameborder="0" align="middle"  height="600"  ></iframe>
	         <script type="text/javascript">
		                 function resizeIframe() {
		                 var height = document.documentElement.clientHeight;
		                 document.getElementById("wheight").value = height;
		                 //alert(document.getElementById("wheight").value);
		                 //alert("hai");
		                 height -= 55;
		                 document.getElementById('govFrame').style.height = height +"px";
		                 document.getElementById('dbFrame').style.height = height +"px";
		                 document.getElementById('otFrame').style.height = height +"px";
		                 document.getElementById('itFrame').style.height = height +"px";
		                 document.getElementById('otcFrame').style.height = height +"px";
		                 };
		                 document.getElementById('govFrame').onload = resizeIframe;
		                 window.onresize = resizeIframe;
                </script>
        </div>
        <div id="veryfirst" align="center" style="border:1px solid;">
        	<input type="hidden" id="wheight" name="wheight" value=""/>
	         <iframe width="100%" src="/throttle/content/dashboard/govDB.jsp" id="govFrame" name="govFrame"  frameborder="0" align="middle"  height="600"  ></iframe>
	         <script type="text/javascript">
		                 function resizeIframe() {
		                 var height = document.documentElement.clientHeight;
		                 document.getElementById("wheight").value = height;
		                 //alert(document.getElementById("wheight").value);
		                 //alert("hai");
		                 height -= 55;
		                 document.getElementById('govFrame').style.height = height +"px";
		                 document.getElementById('dbFrame').style.height = height +"px";
		                 document.getElementById('otFrame').style.height = height +"px";
		                 document.getElementById('itFrame').style.height = height +"px";
		                 document.getElementById('otcFrame').style.height = height +"px";
		                 };
		                 document.getElementById('govFrame').onload = resizeIframe;
		                 window.onresize = resizeIframe;
                </script>
        </div>
        <div id="fifth" align="center" style="border:1px solid;">
         <iframe width="100%"  id="itFrame" name="itFrame"  frameborder="0" align="middle" ></iframe>
        </div>
        <div id="second">
            <iframe width="100%" id="otFrame" name="otFrame"  height="600"  frameborder="0" align="middle" ></iframe>
        </div>
        

        
        
        <div id="seventh">
        <iframe width="100%"  id="otcFrame" name="otcFrame"  height="600" frameborder="0" align="middle" ></iframe>
        </div>
        

    </div>


    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
