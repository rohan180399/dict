<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.util.ThrottleConstants" %>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<script language="javascript">
    function submitPage() {
        document.editcustomer.action = '/throttle/editSaveCustomerOutstandingCredit.do';
        document.editcustomer.submit();
    }
</script>

<style>
    #index div {
        color:white;
    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Sales </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Sales"  text="Sales"/></a></li>
            <li class="active">View/Edit Customer  Credit</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body >
                <form name="editcustomer"  method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <br>  
                    <c:set var="custType" value="0" />
                    <c:set var="fixedCrLimit" value="0" />
                    <c:if test="${customerCreditLimit != null}">
                        <c:forEach items="${customerCreditLimit}" var="cudl">
                            <table class="table table-bordered  mb30 table-hover" >
                                <thead>
                                <th colspan="6"  height="30" id="index" style="background-color:#5BC0DE;">
                                    <div  align="center">View/Edit Customer</div>
                                </th>
                                </thead>
                                <tr height="35">
                                    <td width="25%"><font color="red">*</font>Customer Name</td>
                                    <td width="25%"><c:set var="custType" value="${cudl.custTypeId}"/>
                                        <input name="customerId" id="customerId" type="hidden" class="textbox" value="<c:out value="${cudl.customerId}"/>" onClick="ressetDate(this);">
                                        <input name="customerName" id="customerName" type="hidden" class="textbox" readonly value="<c:out value="${cudl.custName}"/>" style="width:200px;height:30px;">
                                    <select name="customerIds" id="customerIds" class="form-control" style="width:200px;height:40px;" onChange="setCustomerName()">
                                    <option value="0">--Select---</option>
                                    <c:forEach items="${customerList}" var="custList">
                                        <option value="<c:out value="${custList.custId}"/>~<c:out value="${custList.custName}"/>"><c:out value="${custList.custName}"/></option>
                                    </c:forEach>
                                    </select></td>
                                <script>
                                    $("#customerIds").val('<c:out value="${cudl.customerId}"/>~<c:out value="${cudl.custName}"/>');
                                    function setCustomerName(){
                                        var customerIds = $("#customerIds").val();
                                        var customerIdss = customerIds.split('~');
                                        $("#customerId").val(customerIdss[0]);
                                        $("#customerName").val(customerIdss[1]);
                                        
                                        document.editcustomer.action = "/throttle/editCustomerOutstandingCredit.do?customerId="+customerIdss[0]+"&customerName="+customerIdss[1];
                                        document.editcustomer.submit();
                                    }
                                </script>
                                    
                                    
                                    
                                    
                                    <td width="25%"><font color="red">*</font>Customer Type </td>                                   
                                    <td width="25%"><input type="hidden" id="creditType" name="creditType" value='<c:out value="${cudl.custTypeId}"/>'/>
                                        <c:if test="${cudl.custTypeId == '1'}">
                                        Credit
                                        </c:if>
                                        <c:if test="${cudl.custTypeId == '2'}">
                                        PDA
                                        </c:if>
                                    </td>     
                                    <td colspan="2">&nbsp;</td> 
                                 <tr height="35" id="creditRow">
                                     
                                     <td ><font color="red">*</font>Fixed Credit Limit</td> 
                                     <c:set var="fixedCrLimit" value="${cudl.fixedCreditLimit}" />
                                     <td > <input name="oldFixedCreditLimit" id="oldFixedCreditLimit" type="hidden" class="textbox" value="<c:out value="${cudl.fixedCreditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;">
                                         <input name="fixedCreditLimit" id="fixedCreditLimit" type="text" class="textbox" value="<c:out value="${cudl.fixedCreditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" onchange="setCreditLimit();"></td> 
                                     <td><font color="red">*</font>Avail. Credit Limit</td>
                                     <td><input name="oldCreditLimit" id="oldCreditLimit" type="hidden" class="textbox" value="<c:out value="${cudl.availLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" >
                                         <input name="creditLimit" id="creditLimit" type="text" class="textbox" value="<c:out value="${cudl.availLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" onchange="setAvailCreditLimit();"></td>
                                     <td><font color="red">*</font>Used Credit Limit</td>
                                     <td><input name="usedCreditLimit" id="usedCreditLimit" type="text" class="textbox" value="<c:out value="${cudl.usedLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" readonly></td>
                                </tr>
                                 <tr height="35" id="pdaRow">
                                     <td ><font color="red">*</font>Fixed PDA Limit</td> 
                                     <td ><input name="fixedPDALimit" id="fixedPDALimit" type="text" class="textbox" value="<c:out value="${cudl.fixedCreditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;"></td> 
                                     <td><font color="red">*</font>Avail. PDA Amount</td>
                                     <td><input name="advanceAmount" id="advanceAmount" type="text" class="textbox" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" readonly></td>
                                     <td><font color="red">*</font>Used PDA Limit</td>
                                     <td><input name="usedPDALimit" id="usedPDALimit" type="text" class="textbox" value="<c:out value="${cudl.usedLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" readonly></td>
                                </tr>
                                 <tr height="35">
                                  <td><font color="red">*</font>Credit Days</td> 
                                  <td> <input name="creditDays" id="creditDays" type="text" class="textbox" value="<c:out value="${cudl.creditDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;"></td>
                                   <td colspan="2">&nbsp;</td> 
                                 </tr>
                                <script>
                                    
                                    var creditType ='<c:out value="${cudl.custTypeId}"/>' ;
                                    if(creditType == "1"){
                                         $("#fixedPDALimit").val("0.00");
                                         $("#usedPDALimit").val("0.00");
                                         $("#advanceAmount").val("0.00");
                                          $('#fixedPDALimit').attr('readonly', true);
                                          $('#usedPDALimit').attr('readonly', true);
                                          $('#advanceAmount').attr('readonly', true);
                                          $('#fixedCreditLimit').attr('readonly', false);
                                          $('#creditLimit').attr('readonly', true);
                                         $("#creditRow").show();
                                         $("#pdaRow").hide();
                                    }else if(creditType == "2"){
                                         $("#fixedCreditLimit").val("0.00");
                                         $("#usedCreditLimit").val("0.00");
                                          $('#creditLimit').val("0.00");
                                          $('#fixedCreditLimit').attr('readonly', true);
                                          $('#usedCreditLimit').attr('readonly', true);
                                          $('#creditLimit').attr('readonly', true);
                                          $('#fixedPDALimit').attr('readonly', false);
                                          $('#advanceAmount').attr('readonly', true);
                                         $("#creditRow").hide();
                                         $("#pdaRow").show();
                                        }
                                        if($("#usedCreditLimit").val()< 0){
                                         $("#usedCreditLimit").val("0.00");
                                        }
                                       var roleId ='<c:out value="${roleId}"/>' ;  
                                        if(roleId == "1023" || roleId == "1060"){
                                           $('#fixedCreditLimit').attr('readonly', false); 
                                           $('#fixedPDALimit').attr('readonly', false); 
                                        }else{
                                           $('#fixedCreditLimit').attr('readonly', true); 
                                           $('#fixedPDALimit').attr('readonly', true); 
                                        }
                                        
                                     function setAvailCreditLimit(){
                                       var fixedCreditLimit= $("#fixedCreditLimit").val();
                                       var creditLimit= $("#creditLimit").val();
                                       if(creditLimit > fixedCreditLimit){
                                           alert("credit limit should be less than fixed credit limit");
                                           $("#creditLimit").val('0.00');
                                           $("#creditLimit").focus();
                                       }
                                       if(creditLimit == ""){
                                           $("#creditLimit").val('0.00');
                                       }
                                     }
                                     function setCreditLimit(){
                                       var oldFixedCreditLimit= $("#oldFixedCreditLimit").val();
                                       var fixedCreditLimit= $("#fixedCreditLimit").val();
                                       var creditLimit= $("#creditLimit").val();
                                       var oldCreditLimit= $("#oldCreditLimit").val();
                                        var newCreditLimit = "";
                                        var newFixedCreditLimit = "";
                                       var tempCreditLimit = oldFixedCreditLimit - oldCreditLimit;
//                                       alert(tempCreditLimit)
                                       if(oldCreditLimit == 0 || oldCreditLimit == ""){
                                        newCreditLimit = parseFloat(fixedCreditLimit);
                                        $("#creditLimit").val(newCreditLimit);
                                       }else if(oldCreditLimit > 0 || oldCreditLimit != ""){
                                        newFixedCreditLimit = parseFloat(parseFloat(fixedCreditLimit) - parseFloat(oldFixedCreditLimit));  
//                                        alert("oldCreditLimit--"+parseFloat(oldCreditLimit))
//                                        alert("newFixedCreditLimit--"+parseFloat(newFixedCreditLimit))
//                                        alert("newFixed###CreditLimit--"+parseFloat(oldCreditLimit) + parseFloat(newFixedCreditLimit))
//                                        alert(" parseFloat(parseFloat(oldCreditLimit) + parseFloat(newFixedCreditLimit))--"+ parseFloat(oldCreditLimit) + parseFloat(newFixedCreditLimit))
                                        newCreditLimit = parseFloat(oldCreditLimit) + parseFloat(newFixedCreditLimit);
//                                        alert("newCreditLimit--"+newCreditLimit)
                                        if(newCreditLimit <= 0){
                                        $("#creditLimit").val("0.00");
                                        }else{
                                        $("#creditLimit").val(newCreditLimit);
                                        }
//                                         alert(newCreditLimit);
                                       }
                                     }
                                </script>
                            </table>
                        </c:forEach >
                    </c:if>
                    
                         <%--<c:if test="${fixedCrLimit == '0.00' || fixedCrLimit == '0'}">--%>
                             <center>
                                    <input type="button" value="SAVE" name="save" id="save" class="btn btn-info" onClick="submitPage();" style="width:100px;height:35px;">
                            </center>
                         <%--</c:if>--%>
                    
                    <br/>
                    <br/>
                    <br/>
                    
                      <% int index = 1;%>
                    <c:if test="${customerCreditLimitDetails != null}">
                             <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr >
                                    <th>Sno</th>
                                    <c:if test="${custType == '1'}">
                                    <th>Credit Limit</th>
                                    </c:if>
                                    <c:if test="${custType == '2'}">
                                    <th>PDA Amount</th>
                                    </c:if>
                                    <th>Credit Days</th>
                                    <th>Created Date</th>
                                    <th>Created By</th>
                                      </tr>
                            </thead>
                            <tbody>
                        <c:forEach items="${customerCreditLimitDetails}" var="creditDetails">
                                    <tr >
                             <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                        <td  >
                                            <%=index%>
                                        </td>
                                        <c:if test="${custType == '1'}">
                                         <td  ><c:out value="${creditDetails.creditLimit}"/></td>
                                        </c:if>
                                        <c:if test="${custType == '2'}">
                                         <td  ><c:out value="${creditDetails.advanceAmount}"/></td>
                                        </c:if>
                                         <td  ><c:out value="${creditDetails.creditDays}"/></td>
                                         <td  ><c:out value="${creditDetails.createdDate}"/></td>
                                         <td  ><c:out value="${creditDetails.createdBy}"/></td>
                                        
                                </tr>
                                <%index++;%>
                        </c:forEach >
                               
                            </table>
                    </c:if>
                    <br>
                   
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
