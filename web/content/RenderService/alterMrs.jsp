<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  
</head>

<script>
 function setSelectbox(i)
    {       
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i].checked = 1;    
        
    }
    
    function submitPage(){
        var chek=validation();
        if(chek==1){           
    document.mrs.action="/throttle/updateMrsItems.do";
    document.mrs.submit();
    }
    }
    function validation(){
       var index=document.getElementsByName("selectedIndex") ;  
        var requested=document.getElementsByName("qty");
        var action=document.getElementsByName("option");
        var chec=0;
        for(i=0;i<index.length && index.length!=0;i++){
        if(index[i].checked==1){
            chec++;
            if(numberValidation(requested[i],'Requested Quantity')){
                return 0;
                }               
                if(action[i].value=="0"){
                alert("Please select Action Type");                    
                return 0;
                }
            }
            if(chec==0){
                alert("Please select Any One And then proceed");
                return 0;
                }
            }
            return 1;
        }
</script>

<body>

<form name="mrs" method="post">
    
   
<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->


<%@ include file="/content/common/message.jsp"%>

<%
    
    int index=0;
    %>

<c:if test = "${vehicleDetails != null}" >    
      <table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="400" class="border">
<tbody><tr>
<td colspan="5" class="contenthead" height="30"><div class="contenthead">Generate MRS</div></td>
</tr>

<c:forEach items="${vehicleDetails}" var="fservice"> 
  <%  
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>
<tr>
<td class="text2" height="30">Job Card No</td>
<input name="mrsJobCardNumber" type="hidden" value='<c:out value="${fservice.jobCardId}"/>'>
<td class="text2"  height="30"><c:out value="${fservice.jobCardId}"/></td>
<td class="text2" height="30">Vehicle No</td>
<td class="text2" height="30"><c:out value="${fservice.mrsVehicleNumber}"/></td>
</tr>
<tr>
<td class="text1" height="30">Service Type</td>
<td class="text1" height="30"><c:out value="${fservice.serviceTypeName}"/></td>
<td class="text1" height="30">Chasis No</td>
<td class="text1" height="30"><c:out value="${fservice.mrsVehicleChassisNumber}"/></td>
</tr>
<tr>
<td class="text2" height="30">Vehicle Type</td>
<td class="text2" height="30"><c:out value="${fservice.mrsVehicleType}"/></td>
<td class="text2" height="30">Usage Type</td>
<td class="text2" height="30"><c:out value="${fservice.mrsVehicleUsageType}"/></td>
</tr>
<!--<tr>

<td class="text1" height="30">Technician</td>
<td class="text1" height="30"><select class='form-control' id='technicianId'  name='technicianId' >
<option selected   value=0>---Select---</option>
<c:if test = "${technicians != null}" >
<c:forEach items="${technicians}" var="mfr">
<option  value='<c:out value="${mfr.empId}" />'>
<c:out value="${mfr.empName}" />
</c:forEach >
</c:if> 
</select></td>
<td class="text1" height="30">&nbsp;</td>
<td class="text1" height="30">&nbsp;</td>
</tr>-->
<%index++;%>
</c:forEach>
</tbody></table>

</c:if>
<br><br>

<br><br>
 <%
    
    int index1=0;
    %>

<c:if test = "${mrsDetails != null}" >       
    
<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="border">

<tbody><tr>
<td colspan="8" class="text2" align="center" height="30"><strong>Material Details</strong></td>
</tr>
<tr>
<td class="contenthead" height="30"><div class="contenthead">SNo</div></td>
<td class="contenthead" height="30"><div class="contenthead">Mfr Code</div></td>
<td class="contenthead" height="30"><div class="contenthead">Item Code</div></td>
<td class="contenthead" height="30"><div class="contenthead">Item Name</div></td>
<td class="contenthead" height="30"><div class="contenthead">UOM</div></td>
<td class="contenthead" height="30"><div class="contenthead">Requested Qty</div></td>
<td class="contenthead" height="30"><div class="contenthead">Action</div></td>
<td class="contenthead" height="30"><div class="contenthead">Select</div></td>
</tr>

<c:forEach items="${mrsDetails}" var="service"> 
  <%  
String classText = "";
int oddEven = index1% 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>

<tr>
<td class="<%=classText%>" height="30"><%=index1+1%></td>
<input name="itemId" class="form-control" type="hidden" value='<c:out value="${service.mrsItemId}" />'>
<input name="catId" class="form-control" type="hidden" value='<c:out value="${service.mrsItemId}" />'>
<td class="<%=classText%>" height="30"><c:out value="${service.mrsItemMfrCode}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${service.mrsPaplCode}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${service.mrsItemName}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${service.uomName}" /></td>
<c:if test = "${service.catId != 1011}" >       
<td class="<%=classText%>" height="30"><input name="qty" class="form-control" size="5"  onchange="setSelectbox(<%=index1%>)"  type="text" value='<c:out value="${service.mrsRequestedItemNumber}" />'></td>
</c:if>
<c:if test = "${service.catId == 1011}" >       
<td class="<%=classText%>" height="30"><input name="qty" type="hidden" value='0'><c:out value="${service.mrsRequestedItemNumber}" />
</c:if>
<td class="<%=classText%>" height="30">
<select class='form-control' onchange="setSelectbox(<%=index1%>)" name='option' >
<option selected   value="0">---Select---</option>
<option   value="1">Alter</option>
<option  value="2">Delete</option>
</td>
<td class="<%=classText %>" height="30"> <input type="checkbox" name="selectedindex" value='<%= index1 %>' </td>
</tr>
<%index1++;%>
</c:forEach>
</tbody></table>

<br>
<center>
<input value="Save" class="button" type="button" onclick="submitPage();">
</center>
</c:if>
<input name="mrsId"  type="hidden" value='<%=request.getParameter("mrsId")%>'>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </body>
</html>
