<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script>
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";



    //savefunction
    function submitPage(value) {
        var count1 = 0;
        var count2 = 0;
        if (document.getElementById('routeValidFrom').value == '') {
            alert("please select the route valid from date");
            document.getElementById('routeValidFrom').focus();
        } else if (document.getElementById('routeValidTo').value == '') {
            alert("please select the route valid to date");
            document.getElementById('routeValidTo').focus();
        }else{
            document.secondaryRoute.action = '/throttle/saveSecondaryContract.do';
            document.secondaryRoute.submit();
        }

    }






     $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getSecondaryCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    $("#customerId").val(ui.item.Id);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    $('#fixedKmPerMonth').focus();
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


        });

</script>

    <body>
        <form name="secondaryRoute"  method="post">
            <br>
            <br>
            <br>
            <c:set var="billType" value="${billType}" />
            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr class="contenthead" align="left"><td colspan="4">Secondary Contract Details</td></tr>
               <c:forEach items="${SecondaryCustomerList}" var="scl">
                <tr class="text2">
                    <td><font color='red'>*</font>Customer Name</td>
                    <td class="text1"><input type="hidden" name="customerId" id="customerId" class="textbox" value="<c:out value="${customerId}"/>"><c:out value="${customerName}"/></td>
                    <td><font color='red'>*</font>Extra Km Calculation</td>
                    <td>
                        <c:if test="${scl.extraKmCalculation == 1}">
                            Consolidated Run Km
                        </c:if>
                        <c:if test="${scl.extraKmCalculation == 2}">
                            Vehicle Wise Run Km
                        </c:if>
                    </td>

                </tr>

                <tr class="text1">
                    <td><font color='red'>*</font>Valid From</td>
                    <td><c:out value="${scl.fromDate}"/></td>
                    <td><font color='red'>*</font>Valid To</td>
                    <td><c:out value="${scl.toDate}"/></td>
                </tr>
                 <tr class="text2">
                    <td><font color='red'>*</font>Cost Of CNG (SAR/Kg)</td>
                    <td><c:out value="${scl.contractCngCost}"/></td>
                    <td><font color='red'>*</font>Cost Of Diesel (SAR/Ltr)</td>
                    <td><c:out value="${scl.contractDieselCost}"/></td>
                </tr>
                <tr class="text1">
                    <td><font color='red'>*</font>Rate Change For (1 SAR/Kg) Of CNG</td>
                    <td><c:out value="${scl.rateChangeOfCng}"/></td>
                    <td><font color='red'>*</font>Rate Change For (1 SAR/Ltr) Of Diesel</td>
                    <td><c:out value="${scl.rateChangeOfDiesel}"/></td>
                </tr>
              </c:forEach>
                <tr class="text2">
                    <td class="text1">Current Diesel Cost </td>
                    <td><input type="hidden" name="dieselCost" id="dieselCost"  value="<%=request.getAttribute("currentDieselPrice")%>" readonly/><%=request.getAttribute("currentDieselPrice")%></td>
                    <td class="text1">Current CNG Cost </td>
                    <td><input type="hidden" name="fuelCost" id="fuelCost"  value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><%=request.getAttribute("currentFuelPrice")%></td>
                </tr>
            </table>
            <br>
            <br>



            <br>
            <br>
            <br>
            <br>
            <% int count = 0;%>
            <c:if test="${SecondaryCustomerVehicleList != null}">
                <table width="96%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="45">
                            <th><h3>S.No</h3></th>
                            <th><h3>Vehicle Type </h3></th>
                            <th><h3>NoOfVehiclesContracted </h3></th>
                            <c:if test="${billType == 1}">
                            <th><h3>Monthly Fixed KM / Vehicle</h3></th>
                            <th><h3>Monthly Fixed Cost / Vehicle</h3></th>
                            <th><h3>Extra Km Charge / Km</h3></th>
                            </c:if>
                            <c:if test="${billType == 2}">
                            <th><h3>Rate/ Km</h3></th>
                            </c:if>
                            <c:if test="${billType == 3}">
                            <th><h3>Rate/ Drop</h3></th>
                            </c:if>
                        </tr>
                    </thead>
                    <% int index = 0;
                                int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${SecondaryCustomerVehicleList}" var="mcl">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                             <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>" style="width: 200px">
                                    <input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${mcl.vehicleTypeId}"/>" readonly/><c:out value="${mcl.vehicleTypeName}"/>
                                </td>
                                <td align="left" class="<%=classText%>"><c:out value="${mcl.noOfVehicles}"/></td>
                                <c:if test="${billType == 1}">
                                <td align="left" class="<%=classText%>"><c:out value="${mcl.fixedKm}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${mcl.fixedKmCharge}"/></td>
                                </c:if>
                                <td align="left" class="<%=classText%>"><c:out value="${mcl.extraKmCharge}"/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <c:if test="${humanResourceDetails != null}">
                <table width="96%" align="center" border="0" id="table1" class="sortable">
                    <thead>
                        <tr height="45">
                            <th><h3>S.No</h3></th>
                            <th><h3>Type </h3></th>
                            <th><h3>No.of Units </h3></th>
                            <th><h3>UniCost/Month</h3></th>
                        </tr>
                    </thead>
                    <%
                                int sno1 = 1;
                    %>
                    <tbody>
                        <c:forEach items="${humanResourceDetails}" var="humanResource">
                            <%
                                        String classText = "";
                                        int oddEven = sno1 % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                             <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno1%></td>
                                <td align="left" class="<%=classText%>" style="width: 200px">
                                    <input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${humanResource.hrId}"/>" readonly/><c:out value="${humanResource.hrName}"/>
                                </td>
                                <td align="left" class="<%=classText%>"><c:out value="${humanResource.noOfPersons}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${humanResource.fixedAmount}"/></td>
                            </tr>
                            <%sno1++;%>
                        </c:forEach>
                    </tbody>
                </table>


            </c:if>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
