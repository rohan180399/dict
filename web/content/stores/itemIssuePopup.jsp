<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script>


    var stkAvailability = 0;
    var httpReq;

    function stkValidate() {
        var temp = (document.mrs.priceIds.value).split('~');
        document.mrs.priceId.value = temp[0];
        stkAvailability = temp[1];
        document.mrs.amount.value = parseFloat(temp[2]);
        document.mrs.amount.value = parseFloat(document.mrs.issueQty.value) * parseFloat(temp[2]);
        document.mrs.nettAmount.value = document.mrs.amount.value;

    }




    function setOptions5(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
                temp1 = splt[i].split('-');
                make1 = 'INR.' + temp1[1] + '(' + temp1[2] + '/' + temp1[3] + ')';
                make2 = temp1[0] + '~' + temp1[2] + '~' + temp1[1];
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1, make2);
                variab.options[i + 1] = option0;
            }
            correctNettAmount();
        }
    }




    function setTyreOptions(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
                temp1 = splt[i].split('-');
                // make1 =   temp1[1]+'(SAR. '+temp1[3]+'/'+temp1[4]+')';
                make1 = temp1[1] + '(INR. ' + temp1[3] + '/' + temp1[4] + '/PO.NO:' + temp1[5] + ')';
                make2 = temp1[0] + '~' + temp1[2] + '~' + temp1[3];
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1, make2);
                variab.options[i + 1] = option0;

            }
        }
    }

    function setRtTyrePriceOptions(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
                //alert('Have RT WO'+splt[i]);
                temp1 = splt[i].split('-');
                make1 = temp1[2] + '(INR. ' + temp1[0] + '/WO NO' + temp1[3] + ')';
                make2 = temp1[1] + "~" + temp1[0] + "~" + temp1[3];
                //alert(make2);
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1, make2);
                variab.options[i + 1] = option0;
            }
        }
    }


    function setRcItemPriceOptions(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
                temp1 = splt[i].split('-');
                //alert("WO"+splt[i]);
                make1 = temp1[1] + '(INR. ' + temp1[0] + '/WO NO' + temp1[2] + ')';
                make2 = temp1[1] + "~" + temp1[0] + "~" + temp1[2];
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1, make2);
                variab.options[i + 1] = option0;
            }
        }
    }


    function onTyreSelect()
    {
        if (document.mrs.itemType.value == '1011') {
            var splt = (document.mrs.rcItemIds.value).split('~');
            //alert(splt);
            document.mrs.priceId.value = splt[1];
            document.mrs.tyreId.value = splt[0];
            document.mrs.rcPriceWoId.value = splt[2];
            //alert("rcPriceWoId"+document.mrs.rcPriceWoId.value);

        }
    }

    function onRcItemSelect()
    {
        var splt;
        if (document.mrs.itemType.value != '1011' && document.mrs.categoryId.value == '1011') {
            splt = (document.mrs.rcItemIds.value).split('~');
            document.mrs.rcPriceWoId.value = splt[2];
            document.mrs.price.value = splt[1];
            document.mrs.tyreId.value = splt[0];
        } else if (document.mrs.itemType.value != '1011' && document.mrs.categoryId.value != '1011') {
            splt = (document.mrs.rcItemIds.value).split('~');
            document.mrs.rcPriceWoId.value = splt[2];
            document.mrs.price.value = splt[1];
            document.mrs.rcItemId.value = splt[0];
        }

    }


    function getPriceAvailable(indx) {
    <%--alert(document.mrs.counterFlag.value);--%>
        var itemType = document.getElementsByName("itemType");
        var mrsItemId = document.mrs.itemId.value;
        var categoryId = document.mrs.categoryId.value;
        //alert(categoryId);
        var url;
        var item;
        var counter = 0;
        var caseType = '';

        //document.mrs.issueQty.readOnly=0;
        if (itemType[indx].value == '1011' && categoryId != '1011') {
            item = 'itemType=' + itemType[indx].value + "&mrsItemId=" + mrsItemId;
            url = '/throttle/itemPrice.do?' + item;
            caseType = 'case1';
            counter++;
        } else if (itemType[indx].value == '1011' && categoryId == '1011') {
            item = "mrsItemId=" + mrsItemId;
            url = '/throttle/handleNewTyresStock.do?' + item;
            caseType = 'case2';
            counter++;
        } else if (itemType[indx].value == '1012') {
            item = "mrsItemId=" + mrsItemId + "&categoryId=" + categoryId;
            url = '/throttle/getRcPriceList.do?' + item;
            caseType = 'case3';
            counter++;
        }
        if (counter > 0) {
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processPriceAjax(caseType);
            };
            httpReq.send(null);
        }
    }



    function processPriceAjax(caseType)
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                var temp = httpReq.responseText.valueOf();
                if (caseType == 'case1') {
                    setOptions5(temp, document.mrs.priceIds);
                    document.mrs.issueQty.value = "1";
                    document.mrs.issueQty.readOnly = 0;
                    document.mrs.priceIds.disabled = false;
                    document.mrs.rcItemIds.disabled = true;
                }
                else if (caseType == 'case2') {
                    //alert(temp);
                    setTyreOptions(temp, document.mrs.rcItemIds);
                    document.mrs.rcItemIds.disabled = false;
                    document.mrs.priceIds.disabled = true;
                    document.mrs.issueQty.value = "1";
                    document.mrs.issueQty.readOnly = 1;
                }
                else if (caseType == 'case3' && document.mrs.categoryId.value == '1011') {
                    //alert(caseType);
                    setRtTyrePriceOptions(temp, document.mrs.rcItemIds);
                    document.mrs.rcItemIds.disabled = false;
                    document.mrs.priceIds.disabled = true;
                    document.mrs.issueQty.value = "1";
                    document.mrs.issueQty.readOnly = 1;
                }
                else if (caseType == 'case3' && document.mrs.categoryId.value != '1011') {
                    setRcItemPriceOptions(temp, document.mrs.rcItemIds);
                    document.mrs.rcItemIds.disabled = false;
                    document.mrs.priceIds.disabled = true;
                    document.mrs.issueQty.value = "1";
                    document.mrs.issueQty.readOnly = 1;
                }
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }



    function validateStk()
    {
        var test = 'fail';
        if (document.mrs.selectedItem.checked != true) {
            test = "pass";
            return test;
        }
        if (document.mrs.itemType.value == '1011' && document.mrs.categoryId.value != '1011') {
            if (isFloat(document.mrs.issueQty.value)) {
                alert('Please Enter Valid data');
                document.mrs.issueQty.focus();
                document.mrs.issueQty.select();
                return test;
            } else if (parseFloat(stkAvailability) < parseFloat(document.mrs.issueQty.value)) {
                alert('Requested Stock is Not Available in this Price');
                document.mrs.issueQty.focus();
                return test;
            } else {
                test = "pass";
                return test;
            }
        } else {
            test = "pass";
            return test;
        }
    }


    function submitValidation()
    {
        var selectedIndx = document.getElementsByName("selectedItem");
        var retSelectedIndx = document.getElementsByName("retselectedIndex");
        var fault = document.mrs.faultQty.value;
        var index = 0;

        if (selectedIndx[0].checked == true) {
            return "pass";
        }
        for (var i = 0; i < retSelectedIndx.length; i++) {
            if (retSelectedIndx[i].checked == true) {
                index++;
            }
        }
        if (index != 0) {
            return "pass";
        }
        if (!isDigit(fault)) {
            if (fault != '0') {
                return "pass";
            } else {
                alert('Quantity Should not be zero');
                document.mrs.faultQty.focus();
                document.mrs.faultQty.select();
                return "fail";
            }
        }
        alert("Please Select Any Choice");
        return "fail";
    }


    function submitPage()
    {
        var seletcedItems = document.getElementsByName('retselectedIndex');
        if (submitValidation() == 'fail') {
            return;
        }
        //if(document.mrs.selectedItem.checked==true){
        if (validateStk() == 'fail') {
            return;
        }
        //}
        //if(seletcedItems[0].checked == true){
        //  alert('k');
        if (validateIssue() == 'fail') {
            return;
        }
        //}
    <%--var counterFlag=document.mrs.counterFlag.value;
    var counterId=document.mrs.counterId.value;
    alert("Oh God"+counterFlag);--%>
        //alert("positionId"+document.mrs.positionId.value);
        if (document.mrs.itemType.value == '1011') {
            document.mrs.actionType.value = '1011';
            document.mrs.action = '/throttle/handleMrsTyresIssue.do';
            //alert("am here 1");
            document.mrs.submit();
        } else if (document.mrs.itemType.value == '1012') {
            document.mrs.action = '/throttle/handleMrsRcItemsIssue.do';
            //alert("am here 2");
            document.mrs.submit();
        } else {
            document.mrs.action = '/throttle/handleMrsTyresIssue.do';
            //alert("am here 3");
            document.mrs.submit();
        }
    }
    function changeTotal()
    {
        if (document.mrs.issueQty.value != "") {
            //alert("am here...1");
            document.mrs.selectedItem.checked = true;
            //document.getElementById("selectedItem").checked = true;
            //alert("am here...2");
            var total = parseFloat(document.mrs.issueQty.value) * parseFloat(document.mrs.amount.value);
            document.mrs.total.value = total;
            document.mrs.nettAmount.value = total;

        }

    }
    function correctNett() {
        if (isFloat(document.mrs.discount.value)) {
            alert("Please Enter Discount");
            document.mrs.discount.focus();
            document.mrs.discount.select();
            return;
        }
        if (parseFloat(document.mrs.discount.value) > parseFloat(document.mrs.nettAmount.value)) {
            alert("Discount value cannot be greater than bill value");
            document.mrs.discount.value = "";
            document.mrs.discount.focus();
        } else {
            var result = parseFloat(document.mrs.nettAmount.value) - (parseFloat(document.mrs.nettAmount.value) * (parseFloat(document.mrs.discount.value) / 100));
            document.mrs.nettAmount.value = result;
        }
    }
    function correctNettAmount() {
        var flag = 1;
        var tax = 0;
        var hikePercentage = 0;
        var discount = 0;
        var amount = document.mrs.amount.value;
        var issueQty = document.mrs.issueQty.value
        //alert("In Correct Nett Amount");
        if (!isEmpty(document.mrs.tax.value)) {
            tax = parseFloat(document.mrs.tax.value);
        }

        if (!isEmpty(document.mrs.hikePercentage.value))
        {
            hikePercentage = parseFloat(document.mrs.hikePercentage.value);
        }
        if (!isEmpty(document.mrs.discount.value))
        {

            discount = parseFloat(document.mrs.discount.value);

        }
    <%--if(isFloat(document.mrs.tax.value) ){
        flag=0;
        alert("Please Enter Tax");
        document.mrs.tax.focus();
        document.mrs.tax.select();
        return;
    }
    else if(isFloat(document.mrs.hikePercentage.value))
        {
            flag=0;
        alert("Please Enter Hike Percentage");
        document.mrs.hikePercentage.focus();
        document.mrs.hikePercentage.select();
        return;
        }
    else if(isFloat(document.mrs.discount.value))
        {
            flag=0;
        alert("Please Enter Discount");
        document.mrs.discount.focus();
        document.mrs.discount.select();
        return;
        }--%>
        if (flag == 1)
        {

            //alert("iss"+issueQty+"tax"+tax+"hike"+hikePercentage+"disc"+discount)
            var taxAmount = parseFloat(amount) + (parseFloat(amount) * (parseFloat(tax) / 100));
            var hikeAmount = parseFloat(taxAmount) + (parseFloat(taxAmount) * (parseFloat(hikePercentage) / 100));
            var hikeTotal = parseFloat(issueQty) * parseFloat(hikeAmount);
            var discountTotal = parseFloat(hikeTotal) - (parseFloat(hikeTotal) * (parseFloat(discount) / 100));
            document.mrs.total.value = parseFloat(discountTotal);
            document.mrs.nettAmount.value = parseFloat(discountTotal).toFixed(2);
        }
    }



    function validateIssue()
    {

        var retIssuedQty1 = 0;
        var seletcedItems = document.getElementsByName('retselectedIndex');
        var retIssuedQty = document.getElementsByName("retIssuedQty");
        var retReturnedQty = document.getElementsByName("retReturnedQty");
        var issueQty = document.mrs.issueQty.value;
        var approvedQty = document.mrs.approvedQty.value;
        var totalIssued = 0;
        if (document.mrs.selectedItem.checked == true) {
            issueQty = document.mrs.issueQty.value
        } else {
            issueQty = 0;
        }

        for (var i = 0; i < retIssuedQty.length; i++) {
            retIssuedQty1 = parseFloat(retIssuedQty1) + parseFloat(retIssuedQty[i].value);
        }
        totalIssued = parseFloat(retIssuedQty1) + parseFloat(issueQty)

        if (document.mrs.selectedItem.checked == true) {
            if (parseFloat(approvedQty) < parseFloat(totalIssued)) {
                alert('Items Cannot be Issued more than Approved Quantity');
                return "fail";
            }
        }

        if (seletcedItems[0].checked == true) {
            if (retReturnedQty[0].value != '') {
                if (isFloat(retReturnedQty[0].value)) {
                    alert("Please Enter Valid Return Quantity");
                    retReturnedQty[0].focus();
                    retReturnedQty[0].select();
                    return "fail";
                }
                if (parseFloat(retIssuedQty[0].value) < parseFloat(retReturnedQty[0].value)) {
                    alert("Returned Quantity should not exceeds Issued Quantity");
                    retReturnedQty[0].focus();
                    retReturnedQty[0].select();
                    return "fail";
                }
            } else {
                alert("Please Enter Return Quantity");
                retReturnedQty[0].focus();
                retReturnedQty[0].select();
                return "fail";
            }
        }
        return "pass";
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.IssueMRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.IssueMRS"  text="MRS"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <form name="mrs" method="post" >                    


                    <!-- message table -->           
                    <%@ include file="/content/common/message.jsp" %>                              
                    <br> 

                    <% String classText = "";
                        int index = 0;%>
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                        <tr>
                            <th colspan="5" align="center"  height="30"><strong><spring:message code="stores.label.ItemName"  text="default text"/> : <%= request.getAttribute("itemName")%></strong> </th>
                        <input type="hidden" name="itemId" value="<%= request.getAttribute("itemId")%>" >
                        <input type="hidden" name="mrsId" value="<%= request.getAttribute("mrsId")%>" >
                        <input type="hidden" name="categoryId" value="<%= request.getAttribute("categoryId")%>" >
                        <input type="hidden" name="positionId" value="<%= request.getAttribute("positionId")%>" >
                        <input type="hidden" name="rcStatus" value="<%= request.getAttribute("rcStatus")%>" >
                        <input type="hidden" name="approvedQty" value="<%= request.getAttribute("approvedQty")%>" >
                        <input type="hidden" name="rcWorkId" value="<%= request.getAttribute("rcWorkId")%>" >
                        <input type="hidden" name="rcItemsId" value="<%= request.getAttribute("rcItemsId")%>" >
                        <input type="hidden" name="mrsItemId" value="<%= request.getAttribute("mrsItemId")%>" >
                        <%--<input type="hidden" name="counterFlag" value="<%= request.getAttribute("counterFlag")%>" > >
                        <input type="hidden" name="counterId" value="<%= request.getAttribute("counterId")%>" > >--%>
                        <input type="hidden" name="actionType" value="" >
                        <input type="hidden" name="priceId" value="" >
                        <input type="hidden" name="tyreId" value="" >
                        <input type="hidden" name="price" value="" >
                        <input type="hidden" name="rcPriceWoId" value="" >
                        <input type="hidden" name="rcItemId" value="" >
                        <input type="hidden" name="amount" value="" >
                        <input type="hidden" name="total" value="" >
                        </tr> 
                        <tr>
                            <th width="84"  ><spring:message code="stores.label.ItemType"  text="default text"/> </th>
                            <th width="84"  ><spring:message code="stores.label.RC/RT/TyreNoItemsList"  text="default text"/></th>
                            <th width="84"  ><spring:message code="stores.label.Price"  text="default text"/></th>
                            <th width="82" height="30"  ><spring:message code="stores.label.IssuedQty"  text="default text"/></th>
                            <%//if (request.getAttribute("counterFlag").equals("Y")) {
                            %>
                            <%--<td width="82" height="30"  >Tax</td>
                            <td width="82" height="30"  >Hike %</td>
                            <td width="82" height="30"  >Discount</td>
                            <td width="82" height="30"  >Nett Amount</td>--%>
                            <%//}%>
                            <th width="82" height="30"  ><spring:message code="stores.label.Select"  text="default text"/></th>
                        </tr>
                        </thead>


                        <tr>
                            <td width="84"  >
                                <select class="form-control" style="width:120px;height:40px;" name="itemType" onChange="getPriceAvailable('0');" >
                                    <option>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                    <option value="1011"><spring:message code="stores.label.New"  text="default text"/></option>
                                    <% if (request.getAttribute("rcStatus").equals("Y")) {%>
                                    <option value="1012"><spring:message code="stores.label.RC"  text="default text"/></option>
                                    <% }%>
                                </select>
                            </td>
                            <td width="84"  >
                                <select class="form-control" style="width:120px;height:40px;" name="rcItemIds" onChange="onTyreSelect();
                                onRcItemSelect();"  style="width:200px">
                                    <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                </select>
                            </td>
                            <td width="84"  >
                                <select class="form-control" style="width:120px;height:40px;" name="priceIds" onChange="stkValidate();"  style="width:200px" >
                                    <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                </select>
                            </td>
                            <% if (request.getAttribute("categoryId").equals("1011")) {%>
                            <td width="82" height="30" >
                                <input type="text" name="issueQty" value="1" readonly class="form-control" style="width:200px;height:40px;" > </td>
                                <% } else {%>
                            <td width="82" height="30" >
                                <input type="text" name="issueQty" value="" class="form-control" style="width:120px;height:40px;" onBlur="changeTotal();" > </td>
                                <% }
                                            //if (request.getAttribute("counterFlag").equals("Y")) {
                                %>

                            <%--<td width="82" height="30" > <select name="tax" class="form-control" onchange="correctNettAmount();">
                                    <option value="12.5">12.5</option>
                                    <option value="4.00">4.00</option>
                                    <option value="0.50">0.50</option>
                                    <option value="0.00">0.00</option>
                                </select>
                            <td width="82" height="30" > <input type="text" name="hikePercentage" value="" class="form-control" onchange="correctNettAmount();" > </td>
                            <td width="82" height="30" > <input type="text" name="discount" value="" class="form-control" onchange="correctNettAmount();" > </td>
                            <td width="82" height="30" > <input type="text" name="nettAmount" value="" class="form-control" > </td>--%>
                            <%//}%>
                            <td width="82" height="30" > 
                                <input type="checkbox" name="selectedItem" id="selectedItem" value="1" class="form-control" > </td>
                        </tr>




                    </table>
                    <br>

                    <c:if test = "${itemList != null}" >
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                            <tr>
                                <th colspan="9" align="center"  height="30"><strong><spring:message code="stores.label.Returnables"  text="default text"/></strong></th>
                            </tr>
                            <tr>
                                <th width="68" height="30"  ><spring:message code="stores.label.IssuedQty"  text="default text"/></th>
                                <th width="84"  ><spring:message code="stores.label.ItemType"  text="default text"/> </th>                        
                                <th width="84" ><spring:message code="stores.label.RTItem"  text="default text"/></th>
                                <th width="84"  ><spring:message code="stores.label.Price"  text="default text"/></th>
                                <th width="84"  ><spring:message code="stores.label.WoNo"  text="default text"/></th>
                                <th width="82" height="30"  ><spring:message code="stores.label.ReceivedQty"  text="default text"/></th>                                                                                             
                                <th width="82" height="30"  ><spring:message code="stores.label.Select"  text="default text"/></th>                                                                                             
                            </tr>
                            </thead>
                            <c:forEach items="${itemList}" var="item">
                                <% if ((index % 2) == 0) {
                                                classText = "text1";
                                            } else {
                                                classText = "text2";
                                            }
                                %>
                                <input type="hidden" name="retIssueId" value="<c:out value="${item.issueId}"/>" >
                                <input type="hidden" name="retPriceId" value="<c:out value="${item.priceId}"/>" >                        
                                <input type="hidden" name="retTyreId" value="<c:out value="${item.tyreId}"/>" >                        
                                <input type="hidden" name="retIssuedQty" value="<c:out value="${item.totalIssued}"/>" >                        
                                <tr>
                                    <td width="68" height="30"  class="<%= classText%>"> <c:out value="${item.totalIssued}"/> </td>
                                    <td width="84"  class="<%= classText%>">
                                        <input type="text" class="form-control"  size='5' readonly name="retItemType" value="<c:out value="${item.itemType}"/>" > </td>
                                    <td width="84" class="<%= classText%>">
                                        <input type="text" class="form-control"  readonly name="retRcItemId" value="<c:out value="${item.rcItemId}"/>" >
                                    </td>
                                    <td width="84"  class="<%= classText%>" >
                                        <input type="text" class="form-control"  readonly name="retPrice" value="<c:out value="${item.price}"/>" >   </td>
                                        <c:choose>
                                            <c:when test="${item.itemType=='RC'}">
                                            <td width="84"  class="<%= classText%>" >
                                                <input type="text" class="form-control"  readonly name="retRcPriceWoId" value="<c:out value="${item.rcPriceWoId}"/>" >   </td>
                                            </c:when>
                                            <c:otherwise>
                                            <td width="84"  class="<%= classText%>" > &nbsp;   </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${item.itemType=='NEW'}">
                                            <% if (request.getAttribute("categoryId").equals("1011")) {%>
                                            <td width="82" height="30"  class="<%= classText%>" >
                                                <input type="text" class="form-control" size='5' name="retReturnedQty" value="1" readonly > </td>
                                                <% } else {%>
                                            <td width="82" height="30"  class="<%= classText%>" >
                                                <input type="text" size='5'  class="form-control" name="retReturnedQty" value="" > </td>
                                                <% }%>
                                            </c:when>
                                            <c:otherwise>
                                            <td width="82" height="30" class="<%= classText%>" >
                                                <input type="text" class="form-control"  readonly size='5' name="retReturnedQty" value="1" > </td>
                                            </c:otherwise>
                                        </c:choose>
                                    <td width="82" height="30"  class="<%= classText%>" >
                                        <input type="checkbox" class="form-control"  name="retselectedIndex" value="<%= index%>" size='5' >
                                    </td>
                                </tr>   
                                <% index++;%>
                            </c:forEach>
                        </table>
                    </c:if>


                    <c:if test = "${itemList == null}" >
                        <input type="hidden" name="retIssueId" value="0" >
                        <input type="hidden" name="retPriceId" value="0" >
                        <input type="hidden" name="retTyreId" value="0" >
                        <input type="hidden" name="retIssuedQty" value="0" >


                        <input type="hidden"  size='5'  name="retItemType" value="0" >
                        <input type="hidden"   name="retRcItemId" value="0" >
                        <input type="hidden"    name="retPrice" value="0"  >
                        <input type="hidden"  size='5' name="retReturnedQty" value="0"  >
                        <div style="visibility:hidden;" ><input type="checkbox" class="form-control"  name="retselectedIndex" value="" size='5' ></div>
                        </c:if>



                    <br>
                    <% if (request.getAttribute("categoryId").equals("1011")) {%>
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                            <tr>
                                <th align="center"  height="30" ><div >Returned Tyre No</div></th>
                            </tr>
                            </thead>
                            <tr>
                                <td width="68" height="30"  >
                                    <input type="hidden" class="form-control" style="width:260px;height:40px;"  name="faultQty" value="1" >
                                    <select style="width:260px;height:40px;"  class="form-control"  name="oldTyreId">
                                        <option selected   value=0>-select-</option>
                                        <c:if test = "${vehicleTyres != null}" >
                                            <c:forEach items="${vehicleTyres}" var="sv">
                                                <option  value='<c:out value="${sv.tyreId}"/>'>
                                                <c:out value="${sv.tyreNo}" />
                                            </c:forEach >
                                        </c:if>
                                </select>
                                </td>
                            </tr>

                        </table>
                    <%}else{%>

                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                        <tr>
                            <th colspan="2" align="center"  height="30" ><div ><spring:message code="stores.label.FaultyItems-ReturnedQuantity"  text="default text"/></div></th>
                        </tr>
                        </thead>
                        <tr>
                            <td width="68" height="30"  ><spring:message code="stores.label.FaultyItems-ReturnedQuantity"  text="default text"/></td>
                            <td width="68" height="30"  ><input type="text" class="form-control" style="width:260px;height:40px;"  name="faultQty" value="0" ></td>
                        </tr>

                    </table>
                    <%}%>
                    <br>
                    <input type="hidden" name="status" value="" >
                    <br>
                    <center>
                        <input type="button" class="btn btn-success" name="Issue" value="<spring:message code="stores.label.ISSUE/RECEIVE"  text="default text"/>" onClick="submitPage();" > &nbsp;
                    </center>                
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

