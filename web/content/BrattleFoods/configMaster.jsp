
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function configsubmit()
    {
       var errStr = "";
       var nameCheckStatus = $("#parameterNameStatus").text();
        if(document.getElementById("parameterName").value == "") {
            errStr = "Please enter parameterName.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        else if(nameCheckStatus != "") {
            errStr ="parameter Name Status Already Exists.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        else if(document.getElementById("parameterValue").value == "") {
            errStr ="Please enter parameterValue.\n";
            alert(errStr);
            document.getElementById("parameterValue").focus();
        }
        else if(document.getElementById("parameterUnit").value == "") {
            errStr ="Please enter parameterUnit.\n";
            alert(errStr);
            document.getElementById("parameterUnit").focus();
        }else if(nameCheckStatus != "") {
            errStr ="parameter Name Status Already Exists.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        if(errStr == "") {
            document.configMaster.action=" /throttle/saveConfigMaster.do";
            document.configMaster.method="post";
            document.configMaster.submit();
        }






    }
    function setValues(sno,parameterName,parameterValue,parameterUnit,parameterDescription,id,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("id").value = id;
        document.getElementById("parameterDescription").value = parameterDescription;
        document.getElementById("parameterName").value = parameterName;
        document.getElementById("parameterValue").value = parameterValue;
        document.getElementById("parameterUnit").value = parameterUnit;
        document.getElementById("activeInd").value = status;
    }






    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function validateEmail(alerttoEmail){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(alerttoEmail.value) == false)
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

}
var httpRequest;
    function checkParameterName() {

        var parameterName = document.getElementById('parameterName').value;

            var url = '/throttle/checkParameterName.do?parameterName=' + parameterName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#parameterNameStatus").text('Please Check Parameter Name  :' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#parameterNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }



</script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Config Master</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="document.configMaster.parameterName.focus();">


        <form name="configMaster"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Config Master</td></tr>
                <input type="hidden" name="id" id="id" value=""  />
                
                
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Name</td>
                    <td ><input type="text" name="parameterName" id="parameterName" class="form-control" style="width:240px;height:40px;" maxlength="100" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkParameterName();" autocomplete="off"</td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Value</td>
                    <td ><input type="text" name="parameterValue" id="parameterValue" class="form-control" style="width:240px;height:40px;" onkeypress="return onKeyPressBlockCharacters(event)"</td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Unit</td>
                    <td ><input type="text" name="parameterUnit" id="parameterUnit" class="form-control" style="width:240px;height:40px;" maxlength="10" onkeypress="return onKeyPressBlockNumbers(event);"</td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>parameter Description</td>
                    <td ><input type="text" name="parameterDescription" id="parameterDescription" class="form-control" style="width:240px;height:40px;"</td>
<!--                        <select  align="center" class="textbox" name="parameterUnit" id="parameterUnit" >
                            <option value='0'>--select--</option>
                            <option value='1'>Hour</option>
                            <option value='2' >Km</option>
                            <option value='3' >Stage</option>
                            <option value='4' ></option>
                        </select>-->
<!--                    </td>-->
                </tr>
                <tr>
                    <td >Status</td>
                    <td >
                        <select  align="center" class="form-control" style="width:240px;height:40px;" name="activeInd" id="activeInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select></td>
                        <td></td>
                        <td><input type="button" class="btn btn-info" value="Save" name="Submit" onClick="configsubmit()"></td>
                </tr>
             </table>

            <h2 align="center">List The Config Details</h2>

             <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                        <th> S.No</h3></th>
                        <th> Parameter Name </h3></th>
                        <th> Parameter Value</h3></th>
                        <th> Parameter Unit</h3></th>
                        <th> Parameter Description</h3></th>
                        <th> Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${configDetailsList != null}">
                        <c:forEach items="${configDetailsList}" var="cdl">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td align="left"> <%= sno + 1%> </td>
                                <td align="left"> <c:out value="${cdl.parameterName}" /></td>
                                <td align="left"> <c:out value="${cdl.parameterValue}" /></td>
                                <td align="left"> <c:out value="${cdl.parameterUnit}"/></td>
                                <td align="left"> <c:out value="${cdl.parameterDescription}"/></td>
                                <td> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${cdl.parameterName}" />','<c:out value="${cdl.parameterValue}" />','<c:out value="${cdl.parameterUnit}" />','<c:out value="${cdl.parameterDescription}" />','<c:out value="${cdl.id}" />','<c:out value="${cdl.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>