<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.company.business.CompanyTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Fuel Price</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
<!--    <script language="javascript">
    function submitPage(value){
        if(value=='Add'){
            document.fuelPrice.action='/throttle/addFuelPage.do';
            document.fuelPrice.submit();
        }
        function deletePage(indx){
        var fuelPriceId=document.getElementsByName("fuelPriceId");
        var companyId=document.getElementsByName("companyId");
        document.fuelPrice.action='/throttle/deleteFuelPriceNew.do?fuelPriceIds='+fuelPriceId[indx].value+'&companyIds='+companyId[indx].value;
        document.fuelPrice.submit();
            }

</script>-->

    <body>
<!--        <form name="fuelPrice" method="post">-->
        <form method="post" action="/throttle/addFuelPage.do">
<%@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" %>

     <c:if test = "${fuelPriceList != null}" >
            <table align="center" width="650" border="0" cellspacing="0" cellpadding="0" class="border">

                <tr height="30">
                     <td  align="left" class="contenthead" scope="col"><b>Sno</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Company Name</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Fuel Price</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Start Date</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>End Date</b></td>
<!--                    <td  align="left" class="contenthead" scope="col"><b>Delete</b></td>-->
                </tr>
                <% int index = 0;%>
                    <c:forEach items="${fuelPriceList}" var="fP">
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text1";
            } else {
                classText = "text2";
            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>"  align="left"> <%= index + 1 %> </td>
                             <td class="<%=classText %>" align="left"><input type="hidden" name="companyId" value='<c:out value="${fp.fillingStationId}"/>'> <c:out value="${fP.fuelLocatName}" /></td>
                             <td class="<%=classText %>"  align="left"><input type="hidden" name="fuelPriceId" id="fuelPriceId" value='<c:out value="${fp.fuelPriceId}"/>'><c:out value="${fP.fuelPrice}"/> </td>
                            <td class="<%=classText %>" align="left"><input type="hidden" name="startDate" id="startDate" value='<c:out value="${fp.startDate}"/>'><c:out value="${fP.startDate}" /> </td>
                            <td class="<%=classText %>" align="left"><input type="hidden" name="endDate" id="endDate" value='<c:out value="${fp.startDate}"/>'><c:out value="${fP.endDate}" /> </td>
<!--                            <td class="<%=classText%>"><a href="" onclick="deletePage(<%=index %>)">Delete</a></td>-->
                        </tr>
                        <% index++;%>
                    </c:forEach>
              </c:if>

            </table>
            <br>
                <br>
            <center>
                <input type="submit" class="button" value="Add FuelPrice" />
<!--                <input type="button" class="button" value="Add" onclick="submitPage(this.value);">-->
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
