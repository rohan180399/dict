<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <body>
        <form name="grn" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <% int index = 0;%> 
            <c:if test = "${billDetail != null}" >        
                <table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
                    <c:forEach items="${billDetail}" var="bill"> 
                        <% if (index == 0) {%>   
                        <tr>
                            <td class="contentsub" colspan="2" align="center" height="30">Bill No&nbsp;&nbsp;: &nbsp;&nbsp;<c:out value="${bill.billId}"/></td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Vendor Name</td>
                            <td height="30"  class="text1"><c:out value="${bill.vendorName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30">Bill No</td>
                            <td height="30"  class="text2"><c:out value="${bill.billId}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" width="150"  height="30"> Bill Amount</td>
                            <td height="30"  class="text1"><c:out value="${bill.billAmount}"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" width="150"  height="30"> Received Date</td>
                            <td height="30"  class="text2"><c:out value="${bill.receivedDate}"/>
                            </td>
                        </tr>
                                          

                        <% index++;
            }%>               
                    </c:forEach>
                </table>
                <br>
                
                <br>
                
        
                <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" id="bg">
                    
                    <tr>
                        <td  height="30" class="contentsub">S No</td>
                        <td  height="30" class="contentsub">MFR Code</td>
                        <td  height="30" class="contentsub">PAPL Code</td>
                        <td  height="30" class="contentsub">Item Name</td>                        
                        <td  height="30" class="contentsub">uom</td>                        
                        <td  height="30" class="contentsub">Rc/Rt Number</td>                        
                        <td  height="30" class="contentsub">Rc Status</td>                        
                        <td  height="30" class="contentsub">Spares Amount</td>                        
                        <td  height="30" class="contentsub">Vat(%)</td>
                        <td  height="30" class="contentsub">Labor Charge</td>
                        <td  height="30" class="contentsub">Amount(SAR)</td>
                    </tr>
                    <%  index = 0;%> 
                    <c:forEach items="${billDetail}" var="bill"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr>
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.paplCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.itemName}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${bill.uomName}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${bill.rcNumber}"/></td>                                                        
                            <td class="<%=classText %>" height="30"> 
                            <c:if test="${bill.status=='Y' || bill.status=='y' }" > RC Done </c:if> 
                            <c:if test="${bill.status=='N' || bill.status=='n' }" > RC Not Done </c:if> 
                            </td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${bill.sparesAmount}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${bill.tax}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${bill.laborAmount}"/></td>           
                            <td class="<%=classText %>" height="30"><c:out value="${bill.amount}"/></td>                            
                        </tr>
                        <% index++;%>
                    </c:forEach>
                    
                </table>
            </c:if>
            <br>
                        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
    
</html>
