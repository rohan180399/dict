<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
            
        </style>
    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleWiseProfitability-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contentsub">S.No</td>
                            <td rowspan="2" class="contentsub">Vehicle No</td>
                            <td rowspan="2" class="contentsub">Vehicle Model</td>
                            <td rowspan="2" class="contentsub">MFR Name</td>
                            <td rowspan="2" class="contentsub">Trips</td>
                            <td rowspan="2" class="contentsub">Earnings</td>
                            <td colspan="5" class="contentsub" style="text-align:center">Fixed Expenses </td>
                            <td colspan="5" class="contentsub" style="text-align:center">Operation Expenses </td>
                            <td rowspan="2" class="contentsub" style="text-align:center">Per Day<br> Fixed Expenses </td>
                            <td rowspan="2" class="contentsub" style="text-align:center">Fixed Expenses <br>for the Report Period</td>
                            <td rowspan="2" class="contentsub" style="text-align:center">Operation Expenses</td>
                            <td rowspan="2" class="contentsub">Maint <br>Expenses</td>
                            <td rowspan="2" class="contentsub">Nett <br>Expenses</td>
                            <td rowspan="2" class="contentsub">Profit</td>
                            <td rowspan="2" class="contentsub">Profit %</td>
                        </tr> 
                        <tr>
                            <td class="contentsub">Insurance</td>
                            <td class="contentsub">Road Tax </td>
                            <td class="contentsub">FC Amount </td>
                            <td class="contentsub">Permit </td>
                            <td class="contentsub">EMI </td>
                            <td class="contentsub">Toll Amount</td>
                            <td class="contentsub">Fuel Amount </td>
                            <td class="contentsub">Other Exp Amount </td>
                            <td class="contentsub">Driver / Cleaner Salary </td>
                            <td class="contentsub">Driver(Incentive,<br>Bata,Misc)</td>
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:set var="totalTrip" value="${0}"/>    
                    <c:set var="totalIncome" value="${0}"/>    
                    <c:set var="totalFixedExpense" value="${0}"/>    
                    <c:set var="totalOperationExpense" value="${0}"/>    
                    <c:set var="totalNetExpense" value="${0}"/>    
                    <c:set var="totalProfitGained" value="${0}"/>   
                    <c:forEach items="${vehicleDetailsList}" var="veh">
                        <c:if test="${vehicleWiseProfitList != null}">
                            <c:forEach items="${vehicleWiseProfitList}" var="profitList">
                                <c:if test="${veh.vehicleId == profitList.vehicleId}">
                                    <c:set var="profit" value="${0}"/>    
                                    <c:set var="profitPercent" value="${0}"/>
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {%>
                                        <tbody>
                                           <c:if test="${profitType == 'All'}">
                                            <tr>
                                            <td class="text2" align="center"><%=sno++%></td>
                                            <td class="text2" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text2"  align="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text2" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>    
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                        </tr>
                                        </c:if>    
                                        <c:if test="${profitType == 'Profit' && profitList.netProfit > 0}">
                                            <tr>
                                            <td class="text2" align="center"><%=sno++%></td>
                                            <td class="text2" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text2" align="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text2" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                        </tr>
                                        </c:if>
                                        <c:if test="${profitType == 'nonProfit' && profitList.netProfit <= 0}">
                                            <tr>
                                            <td class="text2" align="center"><%=sno++%></td>
                                            <td class="text2" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text2" lign="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text2" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if> 
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                        </tr>
                                        </c:if>
                                    </tbody>
                                    <%}else{%>
                                    <tbody>
                                             <c:if test="${profitType == 'All'}">
                                            <tr>
                                            <td class="text1" align="center"><%=sno++%></td>
                                            <td class="text1" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text1"  align="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text1" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text1" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>    
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                        </tr>
                                        </c:if>    
                                        <c:if test="${profitType == 'Profit' && profitList.netProfit > 0}">
                                            <tr>
                                            <td class="text1" align="center"><%=sno++%></td>
                                            <td class="text1" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text1"  align="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text1" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text1" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                        </tr>
                                        </c:if>
                                        <c:if test="${profitType == 'nonProfit' && profitList.netProfit <= 0}">
                                            <tr>
                                            <td class="text1" align="center"><%=sno++%></td>
                                            <td class="text1" ><c:out value="${veh.regNo}"/></td>
                                            <td class="text1" align="center"><c:out value="${veh.modelName}"/></td>
                                            <td class="text1" align="center"><c:out value="${veh.mfrName}"/></td>
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="text1" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="text1" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="text1" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="text1" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if> 
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                        </tr>
                                        </c:if>
                                    <%}%>
                                </c:if>

                            </c:forEach>
                                <%
                                     index++;
                                %>
                        </c:if>
                    </c:forEach>
                                        <tr>
                                        <td colspan="4" class="contentsub" style="text-align:center">Total</td>
                                        <td class="contentsub" style="text-align:center"><c:out value="${totalTrip}"/></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalInsurance}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRoadTax}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFcAmount}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPermitAmount}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEmiAmount}" /></td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTollAmount}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFuelAmount}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOtherExpenseAmount}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverSalaryAmount}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverExpense}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpensePerDay}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalMaintainExpense}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </td>
                                         <c:set var="totalProfitPercentValue1" value="${totalProfitGained / totalIncome}"/>
                                        <c:set var="totalProfitPercentValue" value="${totalProfitPercentValue1 * 100}"/>
                                        <td class="contentsub" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercentValue}" /> %</td>
                                    </tr>     
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
             <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
                 <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                        <td width="150"><c:out value="${totalTrip}"/></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit gained</td>
                        <c:if test="${totalProfitGained lt 0 || totalProfitGained eq 0}">
                        <td width="150"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                        </c:if>
                        <c:if test="${totalProfitGained gt 0 }">
                        <td width="150"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                        </c:if>
                    </tr>
                        <c:set var="totalProfit" value="${0}"/>    
                        <c:set var="totalProfitPercent" value="${0}"/>
                        <c:if test="${totalIncome > 0}">
                        <c:set var="totalProfit" value="${totalProfitGained/totalIncome}"/>    
                        <c:set var="totalProfitPercent" value="${totalProfit*100}"/>
                        </c:if>
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit gained &nbsp;%</td>
                        <c:if test="${totalProfitPercent lt 0 || totalProfitPercent eq 0}">
                        <td width="150"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                        </c:if>
                        <c:if test="${totalProfitPercent gt 0 }">
                        <td width="150"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                        </c:if>
                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>