<%--
    Document   : vehicleWiseProfitabilityReport
    Created on : Oct 20, 2013, 6:19:33 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
         <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        <script language="javascript">
           function getDays() {
                var diff = 0;
             var one_day = 1000*60*60*24;
             var fromDate = document.getElementById("fromDate").value;
             var toDate = document.getElementById("toDate").value;
             var earDate = fromDate.split("-");
             var nexDate = toDate.split("-");
             var fD = parseFloat(earDate[0]).toFixed(0);
            var fM = parseFloat(earDate[1]).toFixed(0);
            var fY = parseFloat(earDate[2]).toFixed(0);

            var tD = parseFloat(nexDate[0]).toFixed(0);
            var tM = parseFloat(nexDate[1]).toFixed(0);
            var tY = parseFloat(nexDate[2]).toFixed(0);

             var d1 = new Date(fY,fM,fD);
            var d2 = new Date(tY,tM,tD);
            diff = (d2.getTime() - d1.getTime())/one_day;
//            alert(diff);
            document.getElementById("days").value = diff;
                }

                function getfcAmt(value)
                {

                    var amount= document.getElementById("days").value;
                    var fcc = value* amount;
                    alert(fcc);
                }
        </script>


    </head>
    <%
        String menuPath = "Report >> Trip Wise Profitability";
        request.setAttribute("menuPath", menuPath);
    %>
    <body onload="setFocus();setValues();">
        <form name="vehiclePerformance" method="post">
                <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trip Wise Profitability Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <td><input type="button" class="button"   value="Search" onclick="getDays();submitPage();"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br/>


                <table width="800" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">

                        <td rowspan="2" class="contenthead">S.No</td>
                        <td rowspan="2" class="contenthead">Customer Name</td>
                        <td rowspan="2" class="contenthead">MFR Name</td>
                        <td rowspan="2" class="contenthead">Vehicle Model</td>
                        <td rowspan="2" class="contenthead">Cons No</td>
                        <td rowspan="2" class="contenthead">Trip Id</td>
                        <td rowspan="2" class="contenthead">Route</td>
                        <td rowspan="2" class="contenthead">Billing Type</td>
                        <td rowspan="2" class="contenthead">Earnings</td>
                        <td rowspan="2" class="contenthead">Nett <br>Expenses</td>
                        <td rowspan="2" class="contenthead">Profit</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td rowspan="2" class="text1">1</td>
                        <td rowspan="2" class="text1" >M/S Bakers Circle</td>
                        <td rowspan="2" class="text1" >Ashok Leyland</td>
                        <td rowspan="2" class="text1" >2216</td>
                        <td rowspan="2"  class="text1">CN/13-14/100254</td>
                        <td rowspan="2"  class="text1">TS/13-14/100457</td>
                        <td rowspan="2"  class="text1">Kashipur - Delhi</td>
                        <td rowspan="2"  class="text1">Actual KMs</td>
                        <td rowspan="2"  class="text1">53,000.00</td>
                        <td rowspan="2"  class="text1">26,000.00</td>
                        <td rowspan="2"  class="text1">27,000.00</td>


                    </tr>
                </tbody>

                <tbody>
                <tr>
                <td rowspan="2" class="text2">2</td>
                <td rowspan="2" class="text2" >M/S Bakers Circle</td>
                <td rowspan="2" class="text2" >Ashok Leyland</td>
                <td rowspan="2" class="text2" >2216</td>
                <td rowspan="2"  class="text2">CN/13-14/100257</td>
                <td rowspan="2"  class="text2">TS/13-14/100459</td>
                <td rowspan="2"  class="text2">Kashipur - Chandigarh</td>
                <td rowspan="2"  class="text2">Actual KMs</td>
                <td rowspan="2"  class="text2">93,000.00</td>
                <td rowspan="2"  class="text2">46,000.00</td>
                <td rowspan="2"  class="text2">47,000.00</td>


                </tr>
                </tbody>
            </table>

            <br/>
            <br/>
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                    <td width="150">2</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                    <td width="150">50,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                    <td width="150">15,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                    <td width="150"><font color="green">35,000.00</font></td>
                </tr>
            </table>
            <br/>



         <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

</html>
