--------------------------------sidemenu---------------------------------------




                  <li><a href="/throttle/invoiceReceipts.do"><i class="fa fa-cog"></i>Invoice Receipts</a></li>
                  <li><a href="/throttle/invoicePendingReceipts.do"><i class="fa fa-cog"></i>Invoice Pending Receipts</a></li>
                  <li><a href="/throttle/invoiceReceived.do"><i class="fa fa-cog"></i>Invoice Received</a></li>
                  <li><a href="/throttle/handleCreditNote.do"><i class="fa fa-cog"></i>Credit Note</a></li>
                  <li><a href="/throttle/handleCreditSearch.do"><i class="fa fa-cog"></i>Credit Search</a></li>
                  <li><a href="/throttle/handleDebitNote.do"><i class="fa fa-cog"></i>Debit Note</a></li>
                  <li><a href="/throttle/handleDebitSearch.do"><i class="fa fa-cog"></i>Debit Search</a></li>
                  <li><a href="/throttle/paymentEntry.do"><i class="fa fa-cog"></i>Cash Payments</a></li>
                  <li><a href="/throttle/bankPaymentEntry.do"><i class="fa fa-cog"></i>Bank Payments</a></li>
                  <li><a href="/throttle/contraEntry.do"><i class="fa fa-cog"></i>Contra Entry</a></li>
                  <li><a href="/throttle/journalEntry.do"><i class="fa fa-cog"></i>Journal Entry</a></li>
                  <li><a href="/throttle/bankPaymentClearance.do"><i class="fa fa-cog"></i>Bank Transaction</a></li>
                  <li><a href="/throttle/bankReconciliationStatement.do"><i class="fa fa-cog"></i>Bank Reconciliation Statement</a></li>
                  <li><a href="/throttle/ledgerReport.do"><i class="fa fa-cog"></i>Ledger Report</a></li>
                  <li><a href="/throttle/dayBook.do?param=search"><i class="fa fa-cog"></i>Day Book</a></li>
                  <li><a href="/throttle/dayBookSummary.do?param=search"><i class="fa fa-cog"></i>Day Book Summary</a></li>
                  <li><a href="/throttle/trialBalanceNew.do?param=search"><i class="fa fa-cog"></i>Trial Balance</a></li>
                  <li><a href="/throttle/profitAndLoss.do?param=search"><i class="fa fa-cog"></i>P & L</a></li>
                  <li><a href="/throttle/balanceSheet.do?param=search"><i class="fa fa-cog"></i>Balance Sheet</a></li>
                  <li><a href="/throttle/entriesReport.do"><i class="fa fa-cog"></i>Entries Report</a></li>
                  <li><a href="/throttle/costCenterReport.do"><i class="fa fa-cog"></i>Cost Center Report</a></li>
                  <li><a href="/throttle/bankReconciliationReport.do?param=view"><i class="fa fa-cog"></i>BRS Report</a></li>
	</ul>
</li>



