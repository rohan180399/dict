/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.business;

import java.util.ArrayList;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripTO {

    /**
     * Creates a new instance of __NAME__
     */
    public TripTO() {
    }
    private String contractRateId = null;
    private String requestId = null;
    private String id = null;
    private String invoiceType = null;
    private String irn = null;
    private String gstType = null;
    private String signedQr = null;
    private String productId[] = null;
    private String commodityId = null;
    private String iso = null;
    private String pickupLocation = null;
    private String handling = null;
    private String cargo = null;
    private String freight20 = null;
    private String freight40 = null;
    private String todayBooking20 = null;
    private String todayBooking40 = null;
    private String phone = null;
    private String pincode = null;

    private String halfContractCheck = null;
    private String flag = null;
     private String invoiceName = null;
    private String fixedCreditAmount = null;
    private String pdaAmount = null;
    private String creditDays = null;
    private String billingState = null;
    private String gateInDate = "";
    private String requestNo = null;
    private String shippingBillDate = "";
    private String creditNoteDate = "";
    private String creditNoteNo = null;
    private String creditNoteId = null;
    private String creditAmount = null;
    private String tripPointId = null;
    private String linerId = null;
    private String expenseCategory = null;
    private String commodityCategory = null;
    private String commodityName = null;
    private String gstApplicable = null;
    private String advancerequestamt = null;

    private String requeststatus = null;
    private String tobepaidtoday = "";
    private String tripday = null;
    private String batchType = null;
    private String currencyid = null;
    private String[] trailerIds = null, trailerRemarks = null, containerTypeIds = null, containerNos = null;
    private String[] bunkNames = null, bunkPlace = null, fuelDates = null, fuelAmounts = null, fuelLtrs = null, fuelFilledBy = null, fuelRemark = null, slipNo = null, fuelUniqueId = null;
    private String remainWeight = null, sealNo = null, grStatus = null, uniqueIds = null;
    private String driverNameId = null;
    private String isRepo = null;
    private String advanceGrNo = null;
    private String excelGrNo = null;
    private String panNo = null;
    private String gstNo = null;
    private String transporter = null;
    private String tripExpenseIds = null;
    private String[] expenseTypes = null;
    private String[] expenses = null;
    private String[] tripExpensesId = null;

    private String activeInd = null;
    private String createdBy = null;
    private String usedStatus = null;
    private String grId = null;
    private String weightmentExpense = null;
    private String tollTax = null;
    private String loadType = null;
    private String mailDeliveredResponse = null;
    private String consignmentContainerId = null;
    private String movementTypeName = null;
    private String vehicleVendor = null;
    private String grHours = "";
    private String grMins = "";
    private String grNo = null;
    private String detaintion = null;
    private String containerTypeName = null;
    private String interamPoint = null;
    private String challanNo = null;
    private String challanDate = "";
    private String billOfEntry = null;
    private String shipingLineNo = null;
    private String vehicleCategory = null;
    private String movementType = null;
    private String movementTypeId = null;
    private String hireVehicleNo = "";
    private String contractTypeId = null;
    private String tripFactoryToIcdHour = "";
    private String tripContainerId = null;
    private String grDate = null;

    private String dieselAmount = null;
    private String paidExpense = null;
    private String linerName = null;
    private String tripDetainHours = "";
    private String fuelDate = "";
    private String grNumber = null;
    private String billingPartyId = null;
    private String billingParty = null;
    private String lastPointId = null;
    private String rtoId = null;
    private String rtoName = null;
    private String containerQty = null;
    private String consignmentOrderDate = "";
    //new
    private String bunkName = null;
    private String state = null;
    private String currRate = null;
    private String activeStatus = null;
    private String bunkId = null;
    private String fuelType = null;
    private String bunkStatus = null;
    private String bunkState = null;
    private String currlocation = null;
    //new
    private String consignmentNoteId = null;
    private String fromCityName = null;
    private String toCityName = null;
    private String repoId = null;
    private String uniqueId = null;
    private String containerTypeId = null;
    private String containerCapacity = null;
    private String hireCharges = null;
    private String wayBillNo = null;
    private String vendorId = null;
    private String ownerShip = null;
    private String vendorTypeId = null;
    private String vendorName = null;
    private String originCity = null;
    private String destinationCity = null;
    private String plannedDeliveryDate = "";
    private String orderGPSTrackingLocation = null;
    private String plannedPickupDate = "";
    private String pickupDate = "";
    private String deliveryDate = "";
    private String remainOrderWeight = null;
    private String containerNo = null;
    private String containerName = null;
    private String containerId = null;
    private String deattchLoc = null;
    private String orderStatus = null;
    private String latitude = null;
    private String longitude = null;
    private String countryId = null;
    private String vehicleTypeId = "";
    private String fuelCostPerKm = null;
    private String tollCostPerKm = null;
    private String miscCostKm = null;
    private String driverIncentive = null;
    private String etcCost = null;
    private String costPerKm = null;
    private String creditLimit = null;
    private String availableLimit = null;
    private String[] orderSequenceNo = null;
    private String consginmentOrderStatus = null;
    private String trailerStartKm = null;
    private String hubId = null;
    private String currencyName = null;
    private String startTrailerKM = null;
    private String endTrailerKM = null;
    private String orderSize = null;
    private String trailerId = null;
    private String trailerNo = null;
    private String deattchLocId = null;
    private String seatCapacity = null;
    private String wareHouseName = null;
    private String wareHouseId = null;
    private String wareHouseAdd = null;
    private String portName = null;
    private String portId = null;
    private String portAddress = null;
    private String sourceWId = null;
    private String destinationWId = null;
    private String orderType = null;
    private String consignmentStatus = null;
    private String consignmentStatusId = null;
    private String totalDocumentAmount = null;
    private String uploadedChallan = null;
    private String uploadedChallanAmount = null;
    private String pendingChallan = null;
    private String pendingChallanAmount = null;
    private String preCoolingCost = null;
//    private String tollCostPerKm = null;
    private String advancePaid = null;
    private String expenseHour = "";
    private String expenseMinute = "";
    private String miscRate = null;
    private String advanceReturn = null;
    private String driverChangeMinute = "";
    private String driverChangeHour = "";
    private String driverInTrip = null;
    private String documentRequiredStatus = null;
    private String unclearedAmount = null;
    private String billCopyFile = null;
    private String billCopyName = null;
    private String documentRequired = null;
    private String transactionAmount = null;
    private String transactionId = null;
    private String idleDays = null;
    private String idleBhattaId = null;
    private String bhattaAmount = null;
    private String driverLastBalance = null;
    private String settleAmount = null;
    private String payAmount = null;
    private String settlementTripsSize = null;
    private String tripAmount = null;
    private String vehicleAdvanceId = null;
    private String bhattaDate = null;
    private String advanceCode = null;
    private String endingBalance = null;
    private String vehicleMileage = null;
    private String fuelConsumption = null;
    private String fuelExpense = null;
    private String tollExpense = null;
    private String driverBhatta = null;
    private String parkingCost = null;
    private String systemExpenses = null;
    private String nextTripCountStatus = null;
    private String currentTripStartDate = "";
    private String nextTrip = "";
    private String overrideTripRemarks = null;
    private String overRideBy = null;
    private String createdOn = "";
    private String overRideOn = "";
    private String setteledKm = null;
    private String setteledHm = null;
    private String setteltotalKM="";
    private String setteltotalHrs="";
    private String remarks = null;
    private String followedBy = null;
    private String followedOn = "";
    private String ticketingDetailId = null;
    private String fileName = null;
    private String ticketingFile = null;
    private String ticketNo = "";
    private String raisedBy = null;
    private String raisedOn = "";
    private String ticketId = null;
    private String priority = null;
    private String from = null;
    private String to = null;
    private String cc = null;
    private String title = null;
    private String message = null;
    private String type = null;
    private String emptyTripPurpose = null;
    private String transitDays1 = null;
    private String transitDays2 = null;
    private String serialNumber = null;
    private String originCityName = null;
    private String destinationCityName = null;
    private String vehicleChangeCityName = null;
    private String totalHm = "";
    private String tripAdvanceId = null;
    private String tripWfuDate = "";
    private String tripWfuTime = "";
    private String usageTypeId = null;
    private String secAdditionalTollCost = null;
    private String preColingAmount = null;
    private String secondaryParkingAmount = null;
    private String totalMinutes = "";
    private String totalKm = null;
    private String totalHours = "";
    private String expense = null;
    private String emptyTripRemarks = null;
    private String cityFrom = null;
    private String cityTo = null;
    private String mailDeliveredStatusId = null;
    private String mailSendingId = null;
    private String approvalStatusId = null;
    private String outStandingAmount = null;
    private String customerCode = null;
    private String extraExpenseStatus = null;
    private String startDate = "";
    private String endDate = "";
    private String startedBy = "";
    private String endedBy = null;
    private String mailDate = "";
    private String mailIdBcc = null;
    private String mailIdCc = null;
    private String mailIdTo = null;
    private String mailContentBcc = null;
    private String mailContentCc = null;
    private String mailContentTo = null;
    private String mailSubjectBcc = null;
    private String mailSubjectCc = null;
    private String mailSubjectTo = null;
    private String mailTypeId = null;
    private String mailId = null;
    private String jobcardId = null;
    private String businessType = null;
    private String emptyTrip = null;
    private String oldVehicleNo = "";
    private String oldDriverName = null;
    private String vehicleRemarks = null;
    private String vehicleChangeDate = "";
    private String vehicleChangeMinute = "";
    private String vehicleChangeHour = "";
    private String lastVehicleEndOdometer = null;
    private String vehicleStartOdometer = null;
    private String balanceAmount = null;
    private String lastVehicleEndKm = null;
    private String vehicleStartKm = null;
    private String returnAmount = null;
    private String licenceNo = null;
    private String licenceDate = "";
    private String employeeCode = null;
    private String joiningDate = "";
    private String fuelTypeId = null;
    private String logDateTime = "";
    private String logDate = "";
    private String logTime = "";
    private String temperature = null;
    private String location = null;
    private String distance = null;
    private String courierRemarks = null;
    private String courierNo = null;
    private String weight = null;
    private String ratePerKg = null;
    private String editMode = null;
    private String discountAmount = null;
    private String billedAmount = null;
    private String billingNameAddress = null;
    private String closureTotalExpense = null;
    private String vehicleDieselUsed = null;
    private String reeferDieselUsed = null;
    private String tempApprovalStatus = null;
    private String tripGraphId = null;
    private String ratePerKm = null;
    private String emailCc = null;
    private String approvalstatus = null;
    private String infoEmailTo = null;
    private String infoEmailCc = null;
    private String extraExpenseValue = "0";
    private String driverCount = "0";
    private String tripCountStatus = null;
    private String vehicleDriverAdvanceId = null;
    private String paymentTypeId = null;
    private String paidAmount = null;
    private String driverMobile = null;
    private String currentLocation = null;
    private String totalPackages = "";
    private String vehicleRequiredDateTime = "";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredTime = "";
    private String consignmentDate = "";
    private String customerOrderReferenceNo = null;
    private String tempLogFile = null;
    private String routeNameStatus = null;
    private String route = null;
    private int routeContractId = 0;
    private String bpclTransactionId = null;
    private String secondaryTollAmount = null;
    private String secondaryAddlTollAmount = null;
    private String secondaryMiscAmount = null;
    private String transactionHistoryId = null, accountId = null, dealerName = null, dealerCity = null, transactionDate = null, accountingDate = null, transactionType = null, currency = null, amount = null, volumeDocNo = null, amoutBalance = null, petromilesEarned = null, odometerReading = null;
    private String consignorName = null;
    private String consigneeName = null;
    private String consigneeId = null;
    private String consignorAddress = null;
    private String consigneeAddress = null;
    private String consignorMobileNo = null;
    private String consigneeMobileNo = null;
    private String estimatedKM = null;
    private String emptyTripApprovalStatus = null;
    private String wfuRemarks = null;
    private String tripStartDate = "";
    private String tripStartTime = "";
    private String tripType = null;
    private String gpsKm = null;
    private String gpsHm = null;
    private String actualAdvancePaid = null;
    private String wfuUnloadingDate = "";
    private String wfuUnloadingTime = "";
    private String podStatus = null;
    private String podCount = null;
    private String paymentType = null;
    private String advanceToPayStatus = null;
    private String toPayStatus = null;
    private String currentTemperature = null;
    private String cityFromId = null;
    private String expectedArrivalDate = "";
    private String expectedArrivalTime = "";
    private String fleetCenterNo = null;
    private String fleetCenterName = null;
    private String mobileNo = null;
    private String gpsLocation = null;
    private String tripEndBalance = null;
    private String paymentMode = null;
    private String tripStartingBalance = null;
    private String tripBalance = null;
    private String zoneId = null;
    private String ZoneName = null;
    private String startReportingDate = "";
    private String startReportingTime = "";
    private String loadingTime = "";
    private String loadingDate = "";
    private String loadingTemperature = "";
    private String endReportingDate = "";
    private String endReportingTime = "";
    private String unLoadingTime = "";
    private String unLoadingDate = "";
    private String unLoadingTemperature = null;
    private String lrNumber = null;
    private String secondaryDriverIdOne = null;
    private String secondaryDriverIdTwo = null;
    private String secondaryDriverNameOne = null;
    private String secondaryDriverNameTwo = null;
    private String advanceAmount = null;
    private String advanceRemarks = null;
    private String cNotesEmail = null;
    private String vehicleNoEmail = "";
    private String tripCodeEmail = null;
    private String customerNameEmail = null;
    private String routeInfoEmail = null;
    private String stakeHolderId = null;
    private String holderName = null;
    private String invoiceNo = null;
    private String fcHeadEmail = null;
    private String customerEmail = null;
    private String smtp = null;
    private String port = null;
    private String emailId = null;
    private String password = null;
    private String tomailId = null;
    private String uomId = null;
    private String routeName = null;
    private String miscValue = null;
    private String requeston = null;
    private String requestremarks = null;
    private String totalRumHM = "";
    private String consignmentId = null;
    private String articleCode = null;
    private String articleName = null;
    private String batch = null;
    private String packageNos = null;
    private String packageWeight = null;
    private String uom = null;
    private String tripArticleid = null;
    private String loadpackageNos = null;
    private String[] tripArticleId = null;
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String[] productbatch = null;
    private String[] productuom = null;
    private String[] loadedpackages = null;
    private String[] unloadedpackages = null;
    private String[] shortage = null;
    private String vehicleactreportdate = "";
    private String vehicleactreporthour = "";
    private String vehicleactreportmin = "";
    private String vehicleloadreportdate = "";
    private String vehicleloadreporthour = "";
    private String vehicleloadreportmin = "";
    private String vehicleloadtemperature = null;
    private String approvalRequestBy = null;
    private String approvalRequestOn = "";
    private String approvalRequestRemarks = null;
    private String approvalRemarks = null;
    private String approvedBy = null;
    private String approvedOn = "";
    private String approvalStatus = null;
    private String requestType = null;
    private String systemExpense = null;
    private String rcmExpense = null;
    private String otherExpense = null;
    private String nettExpense = null;
    private String bookedExpense = null;
    private String driverBatta = "0";
    private String tollCost = "0";
    private String dieselCost = "0";
    private String dieselUsed = "0";
    private String totalDays = "0";
    private String runHm = "";
    private String runKm = "";
    private String reeferMileage = null;
    private String milleage = null;
    private String fuelPrice = null;
    private String driverIncentivePerKm = null;
    private String driverBattaPerDay = "";
    private String tollRate = null;
    private String estimatedExpense = null;
    private String permitExpiryDate = "";

    private String roleId = null;
    private String fleetCenterId = null;
    private String companyId = null;
    private String roadTaxExpiryDate = "";
    private String fcExpiryDate = "";
    private String insuranceExpiryDate = "";
    private String userName = null;
    private String productInfo = null;
    private String tripEndDate = "";
    private String expenseToBeBilledToCustomer = null;
    private String tripEndTime = "";
    private String startKm = "";
    private String startHm = "";
    private String endKm = "";
    private String endHm = "";
    private String freightAmount = null;
    private String tripPlanEndDate = "";
    private String tripPlanEndTime = "";
    private String tripclosureid = null;
    private String totalMins = "";
    private String tollAmount = "0";
//    private String driverIncentive = null;
    private String reeferConsumption = null;
    private String fuelConsumed = null;
    private String fuelCost = "0";
    private String routeExpense = null;
    private String totalExpenses = null;
    private String totalRumKM = null;
    private String totalRefeerHours = "";
    private String totalRefeerMinutes = "";
    private String battaAmount = "0";
    private String incentiveAmount = "0";
    private String routeExpenses = null;
    private String planStartDate = "";
    private String planStartHour = "";
    private String planStartMinute = "";
    private String planEndDate = "";
    private String planEndHour = "";
    private String planEndMinute = "";
    private String planStartTime = "";
    private String planEndTime = "";
    private String actionName = null;
    private String actionRemarks = null;
    private String preStartLocationId = null;
    private String preStartLocation = null;
    private String preStartLocationPlanDate = "";
    private String preStartLocationPlanTime = "";
    private String preStartLocationPlanTimeHrs = "";
    private String preStartLocationPlanTimeMins = "";
    private String preStartLocationDistance = null;
    private String preStartLocationDurationHrs = "";
    private String preStartLocationDurationMins = "";
    private String preStartLocationVehicleMileage = null;
    private String preStartLocationTollRate = null;
    private String preStartLocationRouteExpense = null;
    private String preStartLocationStatus = null;
    private String tripEndMinute = "";
    private String tripEndHour = "";
    private String tripStartMinute = "";
    private String tripStartHour = "";
    private String tripPreStartMinute = "";
    private String tripPreStartHour = "";
    private String advanceDate = "";
    private String tripDay = null;
    private String estimatedAdance = null;
    private String requestedAdvance = null;
    private String paidAdvance = null;
    private String totalFuelValue = null;
    private String fuelRemarks = null;
    private String empName = null;
    private String applicableTaxPercentage = null;
    private String totalExpenseValue = null;
    private String fuel = null;
    private String podPointId = null;
    private String startTime = "";
    private String startOdometerReading = "";
    private String startHM = "";
    //private String pointId = null;
    private String cityId = null;
    private String cityName = null;
    private String podRemarks = null;
    private String podFile = null;
    private String startTripRemarks = null;
    private String endTime = "";
    private String endHM = "";
    private String endOdometerReading = "";
    private String endTripRemarks = null;
    private String preStartHM = null;
    private String fuelLocation = null;
    private String fillDate = null;
    private String fuelLitres = null;
    private String fuelPricePerLitre = null;
    private String fuelAmount = null;
    private String fuelremarks = null;
    private String expenseId = null;
    private String expenseDate = "";
    private String expenseType = null;
    private String totalExpenseAmount = null;
    private String expenseRemarks = null;
    private String expenseName = null;
    private String driverNameex = null;
    private String employeeId = null;
    private String employeeName = null;
    private String taxPercentage = null;
    private double taxPercentageD = 0;
    private String totalValue = null;
    private String nettValue = null;
    private String taxValue = null;

    public String getDriverNameex() {
        return driverNameex;
    }

    public void setDriverNameex(String driverNameex) {
        this.driverNameex = driverNameex;
    }
    private String expenseValue = null;
    private String statusId = null;
    private String tripStatusId = null;
    private String tripExpenseId = null;
    private String tripPodId = null;
    private String durationHours = "";
    private String durationDay1 = "";
    private String durationDay2 = "";
    //Arul Start Here
    private String tripPlannedDate = "";
    private String tripPlannedStartTime = "";
    private String tripSheetDate = "";
    private String consignmentNote = null;
    private String routeInfo = null;
    private String vehicleTonnage = null;
    private String estimatedTonnage = null;
    private String vehicleUtilisation = null;
    private String tripRemarks = null;
    private String vehicleType = "";
    private String estimatedRevenue = null;
    private String totalRevenue = null;
    private String grandTotal = null;
    private String totalExpToBeBilled = null;
    private String estimatedTransitDays = "";
    private String estimatedTransitHours = "";
    private String estimatedAdvancePerDay = "";
    //Arul End Here
    private String primaryDriverId = null;
    private String secondaryDriver1Id = null;
    private String secondaryDriver2Id = null;
    private String primaryDriverName = null;
    private String secondaryDriver1Name = null;
    private String secondaryDriver2Name = null;
    private String tripDate = "";
    private String vehicleCapUtil = null;
    private String tripId = null;
    private String[] tripIds = null;
    private int userId = 0;
    private String driver1Id = null;
    private String statusName = null;
    private String profitMargin = null;
    private String passThroughStatus = null;
    private String marginValue = null;
    private String tripCode = null;
    private String invoiceCode = null;
    private String invoiceId = null;
    private String invoiceDetailId = null;
    private String tripTransitDays = "";
    private String tripTransitHours = "";
    private String advnaceToBePaidPerDay = null;
    private String[] consignmentOrderId = null;
    private String[] orderIds = null;
    private String[] pointId = null;
    private String[] pointType = null;
    private String[] pointOrder = null;
    private String[] pointAddresss = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanTime = null;
    private String[] pointPlanTimeHrs = null;
    private String[] pointPlanTimeMins = null;
    private String cNotes = null;
    private String customerName = null;
    private String customerAddress = null;
    private String billDate = "";
    private String totalWeight = null;
    private String reeferRequired = "";
    private String consginmentRemarks = "";
    private String customerType = null;
    private String billingType = null;
//    private String vehicleTypeId = null;
    private String paidStatus = null;
    private String vehicleId = "";
    private String vehicleNo = "";
    private String driverId = null;
    private String driverName = null;
    private String cleanerId = null;
    private String cleanerName = null;
    private String vehicleTypeName = "";
    private String orderExpense = null;
    private String orderRevenue = null;
    private String tripScheduleDate = "";
    private String tripScheduleTime = "";
    private String tripScheduleTimeHrs = "";
    private String tripScheduleTimeMins = "";
    private String tripScheduleTimeDB = "";
    private String point1PlannedDate = "";
    private String point1PlannedTimeHrs = "";
    private String point1PlannedTimeMins = "";
    private String point2PlannedDate = "";
    private String point2PlannedTimeHrs = "";
    private String point2PlannedTimeMins = "";
    private String point3PlannedDate = "";
    private String point3PlannedTimeHrs = "";
    private String point3PlannedTimeMins = "";
    private String point4PlannedDate = "";
    private String point4PlannedTimeHrs = "";
    private String point4PlannedTimeMins = "";
    private String finalPlannedDate = "";
    private String finalPlannedTimeMins = "";
    private String finalPlannedTimeHrs = "";
    private String orderId = null;
    private String orderNo = null;
    private String routeId = null;
    private String origin = null;
    private String originId = null;
    private String destination = null;
    private String destinationId = null;
    private String firstPointId = null;
    private String finalPointId = null;
    private String point1Id = null;
    private String point1Name = null;
    private String point2Id = null;
    private String point2Name = null;
    private String point3Id = null;
    private String point3Name = null;
    private String point4Id = null;
    private String point4Name = null;
    private String totalKM = null;
    private String totalHrs = null;
    private String totalPoints = null;
    private String interimExists = null;
    private String consignmentOrderNos = null;
    private String vehicleExpense = null;
    private String reeferExpense = null;
    private boolean doesConsignmentRouteCourseExists = false;
    private ArrayList consignmentRoutePoints = null;
//       Nithiya starts here
    private String preStartDate = "";
    private String preStartTime = "";
    private String preOdometerReading = "";
    private String preTripRemarks = null;
    private String regNo = null;
    private String tripSheetId = null;
    private String consignmentNo = null;
    private String customerId = null;
    private String noOfTrips = null;
    private String fromDate = "";
    private String toDate = "";
    private String status = null;
    private int accVehicleId = 0;
    private int reasonForChange = 0;
    private String newSlip = "";
     private String workOrderNumber = "";
    private String gpsVendor = null;
    private String gpsDeviceId = null;

//   Nithiya end here
    public String getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFirstPointId() {
        return firstPointId;
    }

    public void setFirstPointId(String firstPointId) {
        this.firstPointId = firstPointId;
    }

    public String getInterimExists() {
        return interimExists;
    }

    public void setInterimExists(String interimExists) {
        this.interimExists = interimExists;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getFinalPlannedDate() {
        return finalPlannedDate;
    }

    public void setFinalPlannedDate(String finalPlannedDate) {
        this.finalPlannedDate = finalPlannedDate;
    }

    public String getPoint1PlannedDate() {
        return point1PlannedDate;
    }

    public void setPoint1PlannedDate(String point1PlannedDate) {
        this.point1PlannedDate = point1PlannedDate;
    }

    public String getPoint2PlannedDate() {
        return point2PlannedDate;
    }

    public void setPoint2PlannedDate(String point2PlannedDate) {
        this.point2PlannedDate = point2PlannedDate;
    }

    public String getPoint3PlannedDate() {
        return point3PlannedDate;
    }

    public void setPoint3PlannedDate(String point3PlannedDate) {
        this.point3PlannedDate = point3PlannedDate;
    }

    public String getPoint4PlannedDate() {
        return point4PlannedDate;
    }

    public void setPoint4PlannedDate(String point4PlannedDate) {
        this.point4PlannedDate = point4PlannedDate;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getTripScheduleDate() {
        return tripScheduleDate;
    }

    public void setTripScheduleDate(String tripScheduleDate) {
        this.tripScheduleDate = tripScheduleDate;
    }

    public String getTripScheduleTime() {
        return tripScheduleTime;
    }

    public void setTripScheduleTime(String tripScheduleTime) {
        this.tripScheduleTime = tripScheduleTime;
    }

    public String getOrderRevenue() {
        return orderRevenue;
    }

    public void setOrderRevenue(String orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public String getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String getVehicleExpense() {
        return vehicleExpense;
    }

    public void setVehicleExpense(String vehicleExpense) {
        this.vehicleExpense = vehicleExpense;
    }

    public String getOrderExpense() {
        return orderExpense;
    }

    public void setOrderExpense(String orderExpense) {
        this.orderExpense = orderExpense;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getConsginmentRemarks() {
        return consginmentRemarks;
    }

    public void setConsginmentRemarks(String consginmentRemarks) {
        this.consginmentRemarks = consginmentRemarks;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isDoesConsignmentRouteCourseExists() {
        return doesConsignmentRouteCourseExists;
    }

    public void setDoesConsignmentRouteCourseExists(boolean doesConsignmentRouteCourseExists) {
        this.doesConsignmentRouteCourseExists = doesConsignmentRouteCourseExists;
    }

    public ArrayList getConsignmentRoutePoints() {
        return consignmentRoutePoints;
    }

    public void setConsignmentRoutePoints(ArrayList consignmentRoutePoints) {
        this.consignmentRoutePoints = consignmentRoutePoints;
    }

    public String getCleanerId() {
        return cleanerId;
    }

    public void setCleanerId(String cleanerId) {
        this.cleanerId = cleanerId;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String[] getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String[] consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getDriver1Id() {
        return driver1Id;
    }

    public void setDriver1Id(String driver1Id) {
        this.driver1Id = driver1Id;
    }

    public String[] getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String[] orderIds) {
        this.orderIds = orderIds;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointId() {
        return pointId;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public String[] getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String[] pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String[] pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String getProfitMargin() {
        return profitMargin;
    }

    public void setProfitMargin(String profitMargin) {
        this.profitMargin = profitMargin;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripTransitDays() {
        return tripTransitDays;
    }

    public void setTripTransitDays(String tripTransitDays) {
        this.tripTransitDays = tripTransitDays;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getAdvnaceToBePaidPerDay() {
        return advnaceToBePaidPerDay;
    }

    public void setAdvnaceToBePaidPerDay(String advnaceToBePaidPerDay) {
        this.advnaceToBePaidPerDay = advnaceToBePaidPerDay;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripScheduleTimeDB() {
        return tripScheduleTimeDB;
    }

    public void setTripScheduleTimeDB(String tripScheduleTimeDB) {
        this.tripScheduleTimeDB = tripScheduleTimeDB;
    }

    public String getFinalPlannedTimeHrs() {
        return finalPlannedTimeHrs;
    }

    public void setFinalPlannedTimeHrs(String finalPlannedTimeHrs) {
        this.finalPlannedTimeHrs = finalPlannedTimeHrs;
    }

    public String getFinalPlannedTimeMins() {
        return finalPlannedTimeMins;
    }

    public void setFinalPlannedTimeMins(String finalPlannedTimeMins) {
        this.finalPlannedTimeMins = finalPlannedTimeMins;
    }

    public String getPoint1PlannedTimeHrs() {
        return point1PlannedTimeHrs;
    }

    public void setPoint1PlannedTimeHrs(String point1PlannedTimeHrs) {
        this.point1PlannedTimeHrs = point1PlannedTimeHrs;
    }

    public String getPoint1PlannedTimeMins() {
        return point1PlannedTimeMins;
    }

    public void setPoint1PlannedTimeMins(String point1PlannedTimeMins) {
        this.point1PlannedTimeMins = point1PlannedTimeMins;
    }

    public String getPoint2PlannedTimeHrs() {
        return point2PlannedTimeHrs;
    }

    public void setPoint2PlannedTimeHrs(String point2PlannedTimeHrs) {
        this.point2PlannedTimeHrs = point2PlannedTimeHrs;
    }

    public String getPoint2PlannedTimeMins() {
        return point2PlannedTimeMins;
    }

    public void setPoint2PlannedTimeMins(String point2PlannedTimeMins) {
        this.point2PlannedTimeMins = point2PlannedTimeMins;
    }

    public String getPoint3PlannedTimeHrs() {
        return point3PlannedTimeHrs;
    }

    public void setPoint3PlannedTimeHrs(String point3PlannedTimeHrs) {
        this.point3PlannedTimeHrs = point3PlannedTimeHrs;
    }

    public String getPoint3PlannedTimeMins() {
        return point3PlannedTimeMins;
    }

    public void setPoint3PlannedTimeMins(String point3PlannedTimeMins) {
        this.point3PlannedTimeMins = point3PlannedTimeMins;
    }

    public String getPoint4PlannedTimeHrs() {
        return point4PlannedTimeHrs;
    }

    public void setPoint4PlannedTimeHrs(String point4PlannedTimeHrs) {
        this.point4PlannedTimeHrs = point4PlannedTimeHrs;
    }

    public String getPoint4PlannedTimeMins() {
        return point4PlannedTimeMins;
    }

    public void setPoint4PlannedTimeMins(String point4PlannedTimeMins) {
        this.point4PlannedTimeMins = point4PlannedTimeMins;
    }

    public String getTripScheduleTimeHrs() {
        return tripScheduleTimeHrs;
    }

    public void setTripScheduleTimeHrs(String tripScheduleTimeHrs) {
        this.tripScheduleTimeHrs = tripScheduleTimeHrs;
    }

    public String getTripScheduleTimeMins() {
        return tripScheduleTimeMins;
    }

    public void setTripScheduleTimeMins(String tripScheduleTimeMins) {
        this.tripScheduleTimeMins = tripScheduleTimeMins;
    }

    public String[] getPointPlanTimeHrs() {
        return pointPlanTimeHrs;
    }

    public void setPointPlanTimeHrs(String[] pointPlanTimeHrs) {
        this.pointPlanTimeHrs = pointPlanTimeHrs;
    }

    public String[] getPointPlanTimeMins() {
        return pointPlanTimeMins;
    }

    public void setPointPlanTimeMins(String[] pointPlanTimeMins) {
        this.pointPlanTimeMins = pointPlanTimeMins;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getcNotes() {
        return cNotes;
    }

    public void setcNotes(String cNotes) {
        this.cNotes = cNotes;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTripRemarks() {
        return tripRemarks;
    }

    public void setTripRemarks(String tripRemarks) {
        this.tripRemarks = tripRemarks;
    }

    public String getVehicleCapUtil() {
        return vehicleCapUtil;
    }

    public void setVehicleCapUtil(String vehicleCapUtil) {
        this.vehicleCapUtil = vehicleCapUtil;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPreOdometerReading() {
        return preOdometerReading;
    }

    public void setPreOdometerReading(String preOdometerReading) {
        this.preOdometerReading = preOdometerReading;
    }

    public String getPreStartDate() {
        return preStartDate;
    }

    public void setPreStartDate(String preStartDate) {
        this.preStartDate = preStartDate;
    }

    public String getPreStartLocation() {
        return preStartLocation;
    }

    public void setPreStartLocation(String preStartLocation) {
        this.preStartLocation = preStartLocation;
    }

    public String getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(String preStartTime) {
        this.preStartTime = preStartTime;
    }

    public String getPreTripRemarks() {
        return preTripRemarks;
    }

    public void setPreTripRemarks(String preTripRemarks) {
        this.preTripRemarks = preTripRemarks;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionRemarks() {
        return actionRemarks;
    }

    public void setActionRemarks(String actionRemarks) {
        this.actionRemarks = actionRemarks;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getSecondaryDriver1Id() {
        return secondaryDriver1Id;
    }

    public void setSecondaryDriver1Id(String secondaryDriver1Id) {
        this.secondaryDriver1Id = secondaryDriver1Id;
    }

    public String getSecondaryDriver1Name() {
        return secondaryDriver1Name;
    }

    public void setSecondaryDriver1Name(String secondaryDriver1Name) {
        this.secondaryDriver1Name = secondaryDriver1Name;
    }

    public String getSecondaryDriver2Id() {
        return secondaryDriver2Id;
    }

    public void setSecondaryDriver2Id(String secondaryDriver2Id) {
        this.secondaryDriver2Id = secondaryDriver2Id;
    }

    public String getSecondaryDriver2Name() {
        return secondaryDriver2Name;
    }

    public void setSecondaryDriver2Name(String secondaryDriver2Name) {
        this.secondaryDriver2Name = secondaryDriver2Name;
    }

    public String getConsignmentNote() {
        return consignmentNote;
    }

    public void setConsignmentNote(String consignmentNote) {
        this.consignmentNote = consignmentNote;
    }

    public String getEstimatedAdvancePerDay() {
        return estimatedAdvancePerDay;
    }

    public void setEstimatedAdvancePerDay(String estimatedAdvancePerDay) {
        this.estimatedAdvancePerDay = estimatedAdvancePerDay;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getEstimatedTonnage() {
        return estimatedTonnage;
    }

    public void setEstimatedTonnage(String estimatedTonnage) {
        this.estimatedTonnage = estimatedTonnage;
    }

    public String getEstimatedTransitDays() {
        return estimatedTransitDays;
    }

    public void setEstimatedTransitDays(String estimatedTransitDays) {
        this.estimatedTransitDays = estimatedTransitDays;
    }

    public String getEstimatedTransitHours() {
        return estimatedTransitHours;
    }

    public void setEstimatedTransitHours(String estimatedTransitHours) {
        this.estimatedTransitHours = estimatedTransitHours;
    }

    public String getTripPlannedDate() {
        return tripPlannedDate;
    }

    public void setTripPlannedDate(String tripPlannedDate) {
        this.tripPlannedDate = tripPlannedDate;
    }

    public String getTripPlannedStartTime() {
        return tripPlannedStartTime;
    }

    public void setTripPlannedStartTime(String tripPlannedStartTime) {
        this.tripPlannedStartTime = tripPlannedStartTime;
    }

    public String getTripSheetDate() {
        return tripSheetDate;
    }

    public void setTripSheetDate(String tripSheetDate) {
        this.tripSheetDate = tripSheetDate;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleUtilisation() {
        return vehicleUtilisation;
    }

    public void setVehicleUtilisation(String vehicleUtilisation) {
        this.vehicleUtilisation = vehicleUtilisation;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDurationDay1() {
        return durationDay1;
    }

    public void setDurationDay1(String durationDay1) {
        this.durationDay1 = durationDay1;
    }

    public String getDurationDay2() {
        return durationDay2;
    }

    public void setDurationDay2(String durationDay2) {
        this.durationDay2 = durationDay2;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndHM() {
        return endHM;
    }

    public void setEndHM(String endHM) {
        this.endHM = endHM;
    }

    public String getEndOdometerReading() {
        return endOdometerReading;
    }

    public void setEndOdometerReading(String endOdometerReading) {
        this.endOdometerReading = endOdometerReading;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTripRemarks() {
        return endTripRemarks;
    }

    public void setEndTripRemarks(String endTripRemarks) {
        this.endTripRemarks = endTripRemarks;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getFillDate() {
        return fillDate;
    }

    public void setFillDate(String fillDate) {
        this.fillDate = fillDate;
    }

    public String getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(String fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getFuelLitres() {
        return fuelLitres;
    }

    public void setFuelLitres(String fuelLitres) {
        this.fuelLitres = fuelLitres;
    }

    public String getFuelLocation() {
        return fuelLocation;
    }

    public void setFuelLocation(String fuelLocation) {
        this.fuelLocation = fuelLocation;
    }

    public String getFuelPricePerLitre() {
        return fuelPricePerLitre;
    }

    public void setFuelPricePerLitre(String fuelPricePerLitre) {
        this.fuelPricePerLitre = fuelPricePerLitre;
    }

    public String getFuelremarks() {
        return fuelremarks;
    }

    public void setFuelremarks(String fuelremarks) {
        this.fuelremarks = fuelremarks;
    }

    public String getPodFile() {
        return podFile;
    }

    public void setPodFile(String podFile) {
        this.podFile = podFile;
    }

    public String getPodRemarks() {
        return podRemarks;
    }

    public void setPodRemarks(String podRemarks) {
        this.podRemarks = podRemarks;
    }

    public String getPreStartHM() {
        return preStartHM;
    }

    public void setPreStartHM(String preStartHM) {
        this.preStartHM = preStartHM;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHM() {
        return startHM;
    }

    public void setStartHM(String startHM) {
        this.startHM = startHM;
    }

    public String getStartOdometerReading() {
        return startOdometerReading;
    }

    public void setStartOdometerReading(String startOdometerReading) {
        this.startOdometerReading = startOdometerReading;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTripRemarks() {
        return startTripRemarks;
    }

    public void setStartTripRemarks(String startTripRemarks) {
        this.startTripRemarks = startTripRemarks;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalExpenseAmount() {
        return totalExpenseAmount;
    }

    public void setTotalExpenseAmount(String totalExpenseAmount) {
        this.totalExpenseAmount = totalExpenseAmount;
    }

    public String getTripExpenseId() {
        return tripExpenseId;
    }

    public void setTripExpenseId(String tripExpenseId) {
        this.tripExpenseId = tripExpenseId;
    }

    public String getTripPodId() {
        return tripPodId;
    }

    public void setTripPodId(String tripPodId) {
        this.tripPodId = tripPodId;
    }

    public String getPodPointId() {
        return podPointId;
    }

    public void setPodPointId(String podPointId) {
        this.podPointId = podPointId;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public String getApplicableTaxPercentage() {
        return applicableTaxPercentage;
    }

    public void setApplicableTaxPercentage(String applicableTaxPercentage) {
        this.applicableTaxPercentage = applicableTaxPercentage;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEstimatedAdance() {
        return estimatedAdance;
    }

    public void setEstimatedAdance(String estimatedAdance) {
        this.estimatedAdance = estimatedAdance;
    }

    public String getFuelDate() {
        return fuelDate;
    }

    public void setFuelDate(String fuelDate) {
        this.fuelDate = fuelDate;
    }

    public String getFuelRemarks() {
        return fuelRemarks;
    }

    public void setFuelRemarks(String fuelRemarks) {
        this.fuelRemarks = fuelRemarks;
    }

    public String getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(String paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public String getRequestedAdvance() {
        return requestedAdvance;
    }

    public void setRequestedAdvance(String requestedAdvance) {
        this.requestedAdvance = requestedAdvance;
    }

    public String getTotalExpenseValue() {
        return totalExpenseValue;
    }

    public void setTotalExpenseValue(String totalExpenseValue) {
        this.totalExpenseValue = totalExpenseValue;
    }

    public String getTotalFuelValue() {
        return totalFuelValue;
    }

    public void setTotalFuelValue(String totalFuelValue) {
        this.totalFuelValue = totalFuelValue;
    }

    public String getTripDay() {
        return tripDay;
    }

    public void setTripDay(String tripDay) {
        this.tripDay = tripDay;
    }

    public String getTripEndHour() {
        return tripEndHour;
    }

    public void setTripEndHour(String tripEndHour) {
        this.tripEndHour = tripEndHour;
    }

    public String getTripEndMinute() {
        return tripEndMinute;
    }

    public void setTripEndMinute(String tripEndMinute) {
        this.tripEndMinute = tripEndMinute;
    }

    public String getTripPreStartHour() {
        return tripPreStartHour;
    }

    public void setTripPreStartHour(String tripPreStartHour) {
        this.tripPreStartHour = tripPreStartHour;
    }

    public String getTripPreStartMinute() {
        return tripPreStartMinute;
    }

    public void setTripPreStartMinute(String tripPreStartMinute) {
        this.tripPreStartMinute = tripPreStartMinute;
    }

    public String getTripStartHour() {
        return tripStartHour;
    }

    public void setTripStartHour(String tripStartHour) {
        this.tripStartHour = tripStartHour;
    }

    public String getTripStartMinute() {
        return tripStartMinute;
    }

    public void setTripStartMinute(String tripStartMinute) {
        this.tripStartMinute = tripStartMinute;
    }

    public String getPreStartLocationDistance() {
        return preStartLocationDistance;
    }

    public void setPreStartLocationDistance(String preStartLocationDistance) {
        this.preStartLocationDistance = preStartLocationDistance;
    }

    public String getPreStartLocationDurationHrs() {
        return preStartLocationDurationHrs;
    }

    public void setPreStartLocationDurationHrs(String preStartLocationDurationHrs) {
        this.preStartLocationDurationHrs = preStartLocationDurationHrs;
    }

    public String getPreStartLocationDurationMins() {
        return preStartLocationDurationMins;
    }

    public void setPreStartLocationDurationMins(String preStartLocationDurationMins) {
        this.preStartLocationDurationMins = preStartLocationDurationMins;
    }

    public String getPreStartLocationId() {
        return preStartLocationId;
    }

    public void setPreStartLocationId(String preStartLocationId) {
        this.preStartLocationId = preStartLocationId;
    }

    public String getPreStartLocationPlanDate() {
        return preStartLocationPlanDate;
    }

    public void setPreStartLocationPlanDate(String preStartLocationPlanDate) {
        this.preStartLocationPlanDate = preStartLocationPlanDate;
    }

    public String getPreStartLocationPlanTimeHrs() {
        return preStartLocationPlanTimeHrs;
    }

    public void setPreStartLocationPlanTimeHrs(String preStartLocationPlanTimeHrs) {
        this.preStartLocationPlanTimeHrs = preStartLocationPlanTimeHrs;
    }

    public String getPreStartLocationPlanTimeMins() {
        return preStartLocationPlanTimeMins;
    }

    public void setPreStartLocationPlanTimeMins(String preStartLocationPlanTimeMins) {
        this.preStartLocationPlanTimeMins = preStartLocationPlanTimeMins;
    }

    public String getPreStartLocationRouteExpense() {
        return preStartLocationRouteExpense;
    }

    public void setPreStartLocationRouteExpense(String preStartLocationRouteExpense) {
        this.preStartLocationRouteExpense = preStartLocationRouteExpense;
    }

    public String getPreStartLocationTollRate() {
        return preStartLocationTollRate;
    }

    public void setPreStartLocationTollRate(String preStartLocationTollRate) {
        this.preStartLocationTollRate = preStartLocationTollRate;
    }

    public String getPreStartLocationVehicleMileage() {
        return preStartLocationVehicleMileage;
    }

    public void setPreStartLocationVehicleMileage(String preStartLocationVehicleMileage) {
        this.preStartLocationVehicleMileage = preStartLocationVehicleMileage;
    }

    public String getPreStartLocationStatus() {
        return preStartLocationStatus;
    }

    public void setPreStartLocationStatus(String preStartLocationStatus) {
        this.preStartLocationStatus = preStartLocationStatus;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanEndHour() {
        return planEndHour;
    }

    public void setPlanEndHour(String planEndHour) {
        this.planEndHour = planEndHour;
    }

    public String getPlanEndMinute() {
        return planEndMinute;
    }

    public void setPlanEndMinute(String planEndMinute) {
        this.planEndMinute = planEndMinute;
    }

    public String getPlanEndTime() {
        return planEndTime;
    }

    public void setPlanEndTime(String planEndTime) {
        this.planEndTime = planEndTime;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanStartHour() {
        return planStartHour;
    }

    public void setPlanStartHour(String planStartHour) {
        this.planStartHour = planStartHour;
    }

    public String getPlanStartMinute() {
        return planStartMinute;
    }

    public void setPlanStartMinute(String planStartMinute) {
        this.planStartMinute = planStartMinute;
    }

    public String getPlanStartTime() {
        return planStartTime;
    }

    public void setPlanStartTime(String planStartTime) {
        this.planStartTime = planStartTime;
    }

    public String getBattaAmount() {
        return battaAmount;
    }

    public void setBattaAmount(String battaAmount) {
        this.battaAmount = battaAmount;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getFuelConsumed() {
        return fuelConsumed;
    }

    public void setFuelConsumed(String fuelConsumed) {
        this.fuelConsumed = fuelConsumed;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(String incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getReeferConsumption() {
        return reeferConsumption;
    }

    public void setReeferConsumption(String reeferConsumption) {
        this.reeferConsumption = reeferConsumption;
    }

    public String getRouteExpense() {
        return routeExpense;
    }

    public void setRouteExpense(String routeExpense) {
        this.routeExpense = routeExpense;
    }

    public String getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(String routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTollCost() {
        return tollCost;
    }

    public void setTollCost(String tollCost) {
        this.tollCost = tollCost;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalMins() {
        return totalMins;
    }

    public void setTotalMins(String totalMins) {
        this.totalMins = totalMins;
    }

    public String getTotalRefeerHours() {
        return totalRefeerHours;
    }

    public void setTotalRefeerHours(String totalRefeerHours) {
        this.totalRefeerHours = totalRefeerHours;
    }

    public String getTotalRefeerMinutes() {
        return totalRefeerMinutes;
    }

    public void setTotalRefeerMinutes(String totalRefeerMinutes) {
        this.totalRefeerMinutes = totalRefeerMinutes;
    }

    public String getTotalRumKM() {
        return totalRumKM;
    }

    public void setTotalRumKM(String totalRumKM) {
        this.totalRumKM = totalRumKM;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getPreStartLocationPlanTime() {
        return preStartLocationPlanTime;
    }

    public void setPreStartLocationPlanTime(String preStartLocationPlanTime) {
        this.preStartLocationPlanTime = preStartLocationPlanTime;
    }

    public String getTripPlanEndDate() {
        return tripPlanEndDate;
    }

    public void setTripPlanEndDate(String tripPlanEndDate) {
        this.tripPlanEndDate = tripPlanEndDate;
    }

    public String getTripPlanEndTime() {
        return tripPlanEndTime;
    }

    public void setTripPlanEndTime(String tripPlanEndTime) {
        this.tripPlanEndTime = tripPlanEndTime;
    }

    public String getEndHm() {
        return endHm;
    }

    public void setEndHm(String endHm) {
        this.endHm = endHm;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

    public String getRunHm() {
        return runHm;
    }

    public void setRunHm(String runHm) {
        this.runHm = runHm;
    }

    public String getRunKm() {
        return runKm;
    }

    public void setRunKm(String runKm) {
        this.runKm = runKm;
    }

    public String getStartHm() {
        return startHm;
    }

    public void setStartHm(String startHm) {
        this.startHm = startHm;
    }

    public String getStartKm() {
        return startKm;
    }

    public void setStartKm(String startKm) {
        this.startKm = startKm;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String[] getTripIds() {
        return tripIds;
    }

    public void setTripIds(String[] tripIds) {
        this.tripIds = tripIds;
    }

    public String getExpenseToBeBilledToCustomer() {
        return expenseToBeBilledToCustomer;
    }

    public void setExpenseToBeBilledToCustomer(String expenseToBeBilledToCustomer) {
        this.expenseToBeBilledToCustomer = expenseToBeBilledToCustomer;
    }

    public String getMarginValue() {
        return marginValue;
    }

    public void setMarginValue(String marginValue) {
        this.marginValue = marginValue;
    }

    public String getPassThroughStatus() {
        return passThroughStatus;
    }

    public void setPassThroughStatus(String passThroughStatus) {
        this.passThroughStatus = passThroughStatus;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTotalExpToBeBilled() {
        return totalExpToBeBilled;
    }

    public void setTotalExpToBeBilled(String totalExpToBeBilled) {
        this.totalExpToBeBilled = totalExpToBeBilled;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getNettValue() {
        return nettValue;
    }

    public void setNettValue(String nettValue) {
        this.nettValue = nettValue;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public double getTaxPercentageD() {
        return taxPercentageD;
    }

    public void setTaxPercentageD(double taxPercentageD) {
        this.taxPercentageD = taxPercentageD;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getFcExpiryDate() {
        return fcExpiryDate;
    }

    public void setFcExpiryDate(String fcExpiryDate) {
        this.fcExpiryDate = fcExpiryDate;
    }

    public String getInsuranceExpiryDate() {
        return insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(String insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }

    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }

    public String getRoadTaxExpiryDate() {
        return roadTaxExpiryDate;
    }

    public void setRoadTaxExpiryDate(String roadTaxExpiryDate) {
        this.roadTaxExpiryDate = roadTaxExpiryDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getBookedExpense() {
        return bookedExpense;
    }

    public void setBookedExpense(String bookedExpense) {
        this.bookedExpense = bookedExpense;
    }

    public String getDieselCost() {
        return dieselCost;
    }

    public void setDieselCost(String dieselCost) {
        this.dieselCost = dieselCost;
    }

    public String getDieselUsed() {
        return dieselUsed;
    }

    public void setDieselUsed(String dieselUsed) {
        this.dieselUsed = dieselUsed;
    }

    public String getDriverBattaPerDay() {
        return driverBattaPerDay;
    }

    public void setDriverBattaPerDay(String driverBattaPerDay) {
        this.driverBattaPerDay = driverBattaPerDay;
    }

    public String getDriverIncentivePerKm() {
        return driverIncentivePerKm;
    }

    public void setDriverIncentivePerKm(String driverIncentivePerKm) {
        this.driverIncentivePerKm = driverIncentivePerKm;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getTollRate() {
        return tollRate;
    }

    public void setTollRate(String tollRate) {
        this.tollRate = tollRate;
    }

    public String getSystemExpense() {
        return systemExpense;
    }

    public void setSystemExpense(String systemExpense) {
        this.systemExpense = systemExpense;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String getApprovalRequestBy() {
        return approvalRequestBy;
    }

    public void setApprovalRequestBy(String approvalRequestBy) {
        this.approvalRequestBy = approvalRequestBy;
    }

    public String getApprovalRequestOn() {
        return approvalRequestOn;
    }

    public void setApprovalRequestOn(String approvalRequestOn) {
        this.approvalRequestOn = approvalRequestOn;
    }

    public String getApprovalRequestRemarks() {
        return approvalRequestRemarks;
    }

    public void setApprovalRequestRemarks(String approvalRequestRemarks) {
        this.approvalRequestRemarks = approvalRequestRemarks;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedOn() {
        return approvedOn;
    }

    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    public String getNettExpense() {
        return nettExpense;
    }

    public void setNettExpense(String nettExpense) {
        this.nettExpense = nettExpense;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getRcmExpense() {
        return rcmExpense;
    }

    public void setRcmExpense(String rcmExpense) {
        this.rcmExpense = rcmExpense;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String[] getLoadedpackages() {
        return loadedpackages;
    }

    public void setLoadedpackages(String[] loadedpackages) {
        this.loadedpackages = loadedpackages;
    }

    public String getLoadpackageNos() {
        return loadpackageNos;
    }

    public void setLoadpackageNos(String loadpackageNos) {
        this.loadpackageNos = loadpackageNos;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String[] getProductbatch() {
        return productbatch;
    }

    public void setProductbatch(String[] productbatch) {
        this.productbatch = productbatch;
    }

    public String[] getProductuom() {
        return productuom;
    }

    public void setProductuom(String[] productuom) {
        this.productuom = productuom;
    }

    public String[] getShortage() {
        return shortage;
    }

    public void setShortage(String[] shortage) {
        this.shortage = shortage;
    }

    public String[] getTripArticleId() {
        return tripArticleId;
    }

    public void setTripArticleId(String[] tripArticleId) {
        this.tripArticleId = tripArticleId;
    }

    public String getTripArticleid() {
        return tripArticleid;
    }

    public void setTripArticleid(String tripArticleid) {
        this.tripArticleid = tripArticleid;
    }

    public String[] getUnloadedpackages() {
        return unloadedpackages;
    }

    public void setUnloadedpackages(String[] unloadedpackages) {
        this.unloadedpackages = unloadedpackages;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVehicleactreportdate() {
        return vehicleactreportdate;
    }

    public void setVehicleactreportdate(String vehicleactreportdate) {
        this.vehicleactreportdate = vehicleactreportdate;
    }

    public String getVehicleactreporthour() {
        return vehicleactreporthour;
    }

    public void setVehicleactreporthour(String vehicleactreporthour) {
        this.vehicleactreporthour = vehicleactreporthour;
    }

    public String getVehicleactreportmin() {
        return vehicleactreportmin;
    }

    public void setVehicleactreportmin(String vehicleactreportmin) {
        this.vehicleactreportmin = vehicleactreportmin;
    }

    public String getVehicleloadreportdate() {
        return vehicleloadreportdate;
    }

    public void setVehicleloadreportdate(String vehicleloadreportdate) {
        this.vehicleloadreportdate = vehicleloadreportdate;
    }

    public String getVehicleloadreporthour() {
        return vehicleloadreporthour;
    }

    public void setVehicleloadreporthour(String vehicleloadreporthour) {
        this.vehicleloadreporthour = vehicleloadreporthour;
    }

    public String getVehicleloadreportmin() {
        return vehicleloadreportmin;
    }

    public void setVehicleloadreportmin(String vehicleloadreportmin) {
        this.vehicleloadreportmin = vehicleloadreportmin;
    }

    public String getVehicleloadtemperature() {
        return vehicleloadtemperature;
    }

    public void setVehicleloadtemperature(String vehicleloadtemperature) {
        this.vehicleloadtemperature = vehicleloadtemperature;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTotalRumHM() {
        return totalRumHM;
    }

    public void setTotalRumHM(String totalRumHM) {
        this.totalRumHM = totalRumHM;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getTomailId() {
        return tomailId;
    }

    public void setTomailId(String tomailId) {
        this.tomailId = tomailId;
    }

    public String getMiscValue() {
        return miscValue;
    }

    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getFcHeadEmail() {
        return fcHeadEmail;
    }

    public void setFcHeadEmail(String fcHeadEmail) {
        this.fcHeadEmail = fcHeadEmail;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getStakeHolderId() {
        return stakeHolderId;
    }

    public void setStakeHolderId(String stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
    }

    public String getcNotesEmail() {
        return cNotesEmail;
    }

    public void setcNotesEmail(String cNotesEmail) {
        this.cNotesEmail = cNotesEmail;
    }

    public String getCustomerNameEmail() {
        return customerNameEmail;
    }

    public void setCustomerNameEmail(String customerNameEmail) {
        this.customerNameEmail = customerNameEmail;
    }

    public String getRouteInfoEmail() {
        return routeInfoEmail;
    }

    public void setRouteInfoEmail(String routeInfoEmail) {
        this.routeInfoEmail = routeInfoEmail;
    }

    public String getTripCodeEmail() {
        return tripCodeEmail;
    }

    public void setTripCodeEmail(String tripCodeEmail) {
        this.tripCodeEmail = tripCodeEmail;
    }

    public String getVehicleNoEmail() {
        return vehicleNoEmail;
    }

    public void setVehicleNoEmail(String vehicleNoEmail) {
        this.vehicleNoEmail = vehicleNoEmail;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getSecondaryDriverNameOne() {
        return secondaryDriverNameOne;
    }

    public void setSecondaryDriverNameOne(String secondaryDriverNameOne) {
        this.secondaryDriverNameOne = secondaryDriverNameOne;
    }

    public String getSecondaryDriverNameTwo() {
        return secondaryDriverNameTwo;
    }

    public void setSecondaryDriverNameTwo(String secondaryDriverNameTwo) {
        this.secondaryDriverNameTwo = secondaryDriverNameTwo;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String ZoneName) {
        this.ZoneName = ZoneName;
    }

    public String getEndReportingDate() {
        return endReportingDate;
    }

    public void setEndReportingDate(String endReportingDate) {
        this.endReportingDate = endReportingDate;
    }

    public String getEndReportingTime() {
        return endReportingTime;
    }

    public void setEndReportingTime(String endReportingTime) {
        this.endReportingTime = endReportingTime;
    }

    public String getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getLoadingTemperature() {
        return loadingTemperature;
    }

    public void setLoadingTemperature(String loadingTemperature) {
        this.loadingTemperature = loadingTemperature;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getStartReportingDate() {
        return startReportingDate;
    }

    public void setStartReportingDate(String startReportingDate) {
        this.startReportingDate = startReportingDate;
    }

    public String getStartReportingTime() {
        return startReportingTime;
    }

    public void setStartReportingTime(String startReportingTime) {
        this.startReportingTime = startReportingTime;
    }

    public String getTripBalance() {
        return tripBalance;
    }

    public void setTripBalance(String tripBalance) {
        this.tripBalance = tripBalance;
    }

    public String getTripEndBalance() {
        return tripEndBalance;
    }

    public void setTripEndBalance(String tripEndBalance) {
        this.tripEndBalance = tripEndBalance;
    }

    public String getTripStartingBalance() {
        return tripStartingBalance;
    }

    public void setTripStartingBalance(String tripStartingBalance) {
        this.tripStartingBalance = tripStartingBalance;
    }

    public String getUnLoadingDate() {
        return unLoadingDate;
    }

    public void setUnLoadingDate(String unLoadingDate) {
        this.unLoadingDate = unLoadingDate;
    }

    public String getUnLoadingTemperature() {
        return unLoadingTemperature;
    }

    public void setUnLoadingTemperature(String unLoadingTemperature) {
        this.unLoadingTemperature = unLoadingTemperature;
    }

    public String getUnLoadingTime() {
        return unLoadingTime;
    }

    public void setUnLoadingTime(String unLoadingTime) {
        this.unLoadingTime = unLoadingTime;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getFleetCenterNo() {
        return fleetCenterNo;
    }

    public void setFleetCenterNo(String fleetCenterNo) {
        this.fleetCenterNo = fleetCenterNo;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmoutBalance() {
        return amoutBalance;
    }

    public void setAmoutBalance(String amoutBalance) {
        this.amoutBalance = amoutBalance;
    }

    public String getBpclTransactionId() {
        return bpclTransactionId;
    }

    public void setBpclTransactionId(String bpclTransactionId) {
        this.bpclTransactionId = bpclTransactionId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocNo() {
        return volumeDocNo;
    }

    public void setVolumeDocNo(String volumeDocNo) {
        this.volumeDocNo = volumeDocNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeMobileNo() {
        return consigneeMobileNo;
    }

    public void setConsigneeMobileNo(String consigneeMobileNo) {
        this.consigneeMobileNo = consigneeMobileNo;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorMobileNo() {
        return consignorMobileNo;
    }

    public void setConsignorMobileNo(String consignorMobileNo) {
        this.consignorMobileNo = consignorMobileNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getPodCount() {
        return podCount;
    }

    public void setPodCount(String podCount) {
        this.podCount = podCount;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public String getEstimatedKM() {
        return estimatedKM;
    }

    public void setEstimatedKM(String estimatedKM) {
        this.estimatedKM = estimatedKM;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getAdvanceToPayStatus() {
        return advanceToPayStatus;
    }

    public void setAdvanceToPayStatus(String advanceToPayStatus) {
        this.advanceToPayStatus = advanceToPayStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getToPayStatus() {
        return toPayStatus;
    }

    public void setToPayStatus(String toPayStatus) {
        this.toPayStatus = toPayStatus;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuUnloadingDate() {
        return wfuUnloadingDate;
    }

    public void setWfuUnloadingDate(String wfuUnloadingDate) {
        this.wfuUnloadingDate = wfuUnloadingDate;
    }

    public String getWfuUnloadingTime() {
        return wfuUnloadingTime;
    }

    public void setWfuUnloadingTime(String wfuUnloadingTime) {
        this.wfuUnloadingTime = wfuUnloadingTime;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getEmptyTripApprovalStatus() {
        return emptyTripApprovalStatus;
    }

    public void setEmptyTripApprovalStatus(String emptyTripApprovalStatus) {
        this.emptyTripApprovalStatus = emptyTripApprovalStatus;
    }

    public int getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(int routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getSecondaryAddlTollAmount() {
        return secondaryAddlTollAmount;
    }

    public void setSecondaryAddlTollAmount(String secondaryAddlTollAmount) {
        this.secondaryAddlTollAmount = secondaryAddlTollAmount;
    }

    public String getSecondaryMiscAmount() {
        return secondaryMiscAmount;
    }

    public void setSecondaryMiscAmount(String secondaryMiscAmount) {
        this.secondaryMiscAmount = secondaryMiscAmount;
    }

    public String getSecondaryTollAmount() {
        return secondaryTollAmount;
    }

    public void setSecondaryTollAmount(String secondaryTollAmount) {
        this.secondaryTollAmount = secondaryTollAmount;
    }

    public String getRouteNameStatus() {
        return routeNameStatus;
    }

    public void setRouteNameStatus(String routeNameStatus) {
        this.routeNameStatus = routeNameStatus;
    }

    public String getGpsKm() {
        return gpsKm;
    }

    public void setGpsKm(String gpsKm) {
        this.gpsKm = gpsKm;
    }

    public String getGpsHm() {
        return gpsHm;
    }

    public void setGpsHm(String gpsHm) {
        this.gpsHm = gpsHm;
    }

    public String getTempLogFile() {
        return tempLogFile;
    }

    public void setTempLogFile(String tempLogFile) {
        this.tempLogFile = tempLogFile;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getVehicleRequiredDateTime() {
        return vehicleRequiredDateTime;
    }

    public void setVehicleRequiredDateTime(String vehicleRequiredDateTime) {
        this.vehicleRequiredDateTime = vehicleRequiredDateTime;
    }

    public String getVehicleDriverAdvanceId() {
        return vehicleDriverAdvanceId;
    }

    public void setVehicleDriverAdvanceId(String vehicleDriverAdvanceId) {
        this.vehicleDriverAdvanceId = vehicleDriverAdvanceId;
    }

    public String getTripCountStatus() {
        return tripCountStatus;
    }

    public void setTripCountStatus(String tripCountStatus) {
        this.tripCountStatus = tripCountStatus;
    }

    public String getDriverCount() {
        return driverCount;
    }

    public void setDriverCount(String driverCount) {
        this.driverCount = driverCount;
    }

    public String getExtraExpenseValue() {
        return extraExpenseValue;
    }

    public void setExtraExpenseValue(String extraExpenseValue) {
        this.extraExpenseValue = extraExpenseValue;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public String getInfoEmailTo() {
        return infoEmailTo;
    }

    public void setInfoEmailTo(String infoEmailTo) {
        this.infoEmailTo = infoEmailTo;
    }

    public String getInfoEmailCc() {
        return infoEmailCc;
    }

    public void setInfoEmailCc(String infoEmailCc) {
        this.infoEmailCc = infoEmailCc;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getTripGraphId() {
        return tripGraphId;
    }

    public void setTripGraphId(String tripGraphId) {
        this.tripGraphId = tripGraphId;
    }

    public String getTempApprovalStatus() {
        return tempApprovalStatus;
    }

    public void setTempApprovalStatus(String tempApprovalStatus) {
        this.tempApprovalStatus = tempApprovalStatus;
    }

    public String getVehicleDieselUsed() {
        return vehicleDieselUsed;
    }

    public void setVehicleDieselUsed(String vehicleDieselUsed) {
        this.vehicleDieselUsed = vehicleDieselUsed;
    }

    public String getReeferDieselUsed() {
        return reeferDieselUsed;
    }

    public void setReeferDieselUsed(String reeferDieselUsed) {
        this.reeferDieselUsed = reeferDieselUsed;
    }

    public String getClosureTotalExpense() {
        return closureTotalExpense;
    }

    public void setClosureTotalExpense(String closureTotalExpense) {
        this.closureTotalExpense = closureTotalExpense;
    }

    public String getBillingNameAddress() {
        return billingNameAddress;
    }

    public void setBillingNameAddress(String billingNameAddress) {
        this.billingNameAddress = billingNameAddress;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBilledAmount() {
        return billedAmount;
    }

    public void setBilledAmount(String billedAmount) {
        this.billedAmount = billedAmount;
    }

    public String getEditMode() {
        return editMode;
    }

    public void setEditMode(String editMode) {
        this.editMode = editMode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRatePerKg() {
        return ratePerKg;
    }

    public void setRatePerKg(String ratePerKg) {
        this.ratePerKg = ratePerKg;
    }

    public String getCourierRemarks() {
        return courierRemarks;
    }

    public void setCourierRemarks(String courierRemarks) {
        this.courierRemarks = courierRemarks;
    }

    public String getCourierNo() {
        return courierNo;
    }

    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    public String getLogDateTime() {
        return logDateTime;
    }

    public void setLogDateTime(String logDateTime) {
        this.logDateTime = logDateTime;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getLicenceDate() {
        return licenceDate;
    }

    public void setLicenceDate(String licenceDate) {
        this.licenceDate = licenceDate;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLastVehicleEndKm() {
        return lastVehicleEndKm;
    }

    public void setLastVehicleEndKm(String lastVehicleEndKm) {
        this.lastVehicleEndKm = lastVehicleEndKm;
    }

    public String getVehicleStartKm() {
        return vehicleStartKm;
    }

    public void setVehicleStartKm(String vehicleStartKm) {
        this.vehicleStartKm = vehicleStartKm;
    }

    public String getOldVehicleNo() {
        return oldVehicleNo;
    }

    public void setOldVehicleNo(String oldVehicleNo) {
        this.oldVehicleNo = oldVehicleNo;
    }

    public String getOldDriverName() {
        return oldDriverName;
    }

    public void setOldDriverName(String oldDriverName) {
        this.oldDriverName = oldDriverName;
    }

    public String getVehicleRemarks() {
        return vehicleRemarks;
    }

    public void setVehicleRemarks(String vehicleRemarks) {
        this.vehicleRemarks = vehicleRemarks;
    }

    public String getVehicleChangeDate() {
        return vehicleChangeDate;
    }

    public void setVehicleChangeDate(String vehicleChangeDate) {
        this.vehicleChangeDate = vehicleChangeDate;
    }

    public String getVehicleChangeMinute() {
        return vehicleChangeMinute;
    }

    public void setVehicleChangeMinute(String vehicleChangeMinute) {
        this.vehicleChangeMinute = vehicleChangeMinute;
    }

    public String getVehicleChangeHour() {
        return vehicleChangeHour;
    }

    public void setVehicleChangeHour(String vehicleChangeHour) {
        this.vehicleChangeHour = vehicleChangeHour;
    }

    public String getLastVehicleEndOdometer() {
        return lastVehicleEndOdometer;
    }

    public void setLastVehicleEndOdometer(String lastVehicleEndOdometer) {
        this.lastVehicleEndOdometer = lastVehicleEndOdometer;
    }

    public String getVehicleStartOdometer() {
        return vehicleStartOdometer;
    }

    public void setVehicleStartOdometer(String vehicleStartOdometer) {
        this.vehicleStartOdometer = vehicleStartOdometer;
    }

    public String getEmptyTrip() {
        return emptyTrip;
    }

    public void setEmptyTrip(String emptyTrip) {
        this.emptyTrip = emptyTrip;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailContentBcc() {
        return mailContentBcc;
    }

    public void setMailContentBcc(String mailContentBcc) {
        this.mailContentBcc = mailContentBcc;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailSubjectBcc() {
        return mailSubjectBcc;
    }

    public void setMailSubjectBcc(String mailSubjectBcc) {
        this.mailSubjectBcc = mailSubjectBcc;
    }

    public String getMailSubjectCc() {
        return mailSubjectCc;
    }

    public void setMailSubjectCc(String mailSubjectCc) {
        this.mailSubjectCc = mailSubjectCc;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public String getJobcardId() {
        return jobcardId;
    }

    public void setJobcardId(String jobcardId) {
        this.jobcardId = jobcardId;
    }

    public String getMailDate() {
        return mailDate;
    }

    public void setMailDate(String mailDate) {
        this.mailDate = mailDate;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public String getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(String endedBy) {
        this.endedBy = endedBy;
    }

    public String getExtraExpenseStatus() {
        return extraExpenseStatus;
    }

    public void setExtraExpenseStatus(String extraExpenseStatus) {
        this.extraExpenseStatus = extraExpenseStatus;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getOutStandingAmount() {
        return outStandingAmount;
    }

    public void setOutStandingAmount(String outStandingAmount) {
        this.outStandingAmount = outStandingAmount;
    }

    public String getApprovalStatusId() {
        return approvalStatusId;
    }

    public void setApprovalStatusId(String approvalStatusId) {
        this.approvalStatusId = approvalStatusId;
    }

    public String getMailSendingId() {
        return mailSendingId;
    }

    public void setMailSendingId(String mailSendingId) {
        this.mailSendingId = mailSendingId;
    }

    public String getMailDeliveredStatusId() {
        return mailDeliveredStatusId;
    }

    public void setMailDeliveredStatusId(String mailDeliveredStatusId) {
        this.mailDeliveredStatusId = mailDeliveredStatusId;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getEmptyTripRemarks() {
        return emptyTripRemarks;
    }

    public void setEmptyTripRemarks(String emptyTripRemarks) {
        this.emptyTripRemarks = emptyTripRemarks;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getSecondaryParkingAmount() {
        return secondaryParkingAmount;
    }

    public void setSecondaryParkingAmount(String secondaryParkingAmount) {
        this.secondaryParkingAmount = secondaryParkingAmount;
    }

    public String getPreColingAmount() {
        return preColingAmount;
    }

    public void setPreColingAmount(String preColingAmount) {
        this.preColingAmount = preColingAmount;
    }

    public String getSecAdditionalTollCost() {
        return secAdditionalTollCost;
    }

    public void setSecAdditionalTollCost(String secAdditionalTollCost) {
        this.secAdditionalTollCost = secAdditionalTollCost;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getTripWfuDate() {
        return tripWfuDate;
    }

    public void setTripWfuDate(String tripWfuDate) {
        this.tripWfuDate = tripWfuDate;
    }

    public String getTripWfuTime() {
        return tripWfuTime;
    }

    public void setTripWfuTime(String tripWfuTime) {
        this.tripWfuTime = tripWfuTime;
    }

    public String getTripAdvanceId() {
        return tripAdvanceId;
    }

    public void setTripAdvanceId(String tripAdvanceId) {
        this.tripAdvanceId = tripAdvanceId;
    }

    public String getDestinationCityName() {
        return destinationCityName;
    }

    public void setDestinationCityName(String destinationCityName) {
        this.destinationCityName = destinationCityName;
    }

    public String getOriginCityName() {
        return originCityName;
    }

    public void setOriginCityName(String originCityName) {
        this.originCityName = originCityName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String getVehicleChangeCityName() {
        return vehicleChangeCityName;
    }

    public void setVehicleChangeCityName(String vehicleChangeCityName) {
        this.vehicleChangeCityName = vehicleChangeCityName;
    }

    public String getTransitDays1() {
        return transitDays1;
    }

    public void setTransitDays1(String transitDays1) {
        this.transitDays1 = transitDays1;
    }

    public String getTransitDays2() {
        return transitDays2;
    }

    public void setTransitDays2(String transitDays2) {
        this.transitDays2 = transitDays2;
    }

    public String getEmptyTripPurpose() {
        return emptyTripPurpose;
    }

    public void setEmptyTripPurpose(String emptyTripPurpose) {
        this.emptyTripPurpose = emptyTripPurpose;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getRaisedBy() {
        return raisedBy;
    }

    public void setRaisedBy(String raisedBy) {
        this.raisedBy = raisedBy;
    }

    public String getRaisedOn() {
        return raisedOn;
    }

    public void setRaisedOn(String raisedOn) {
        this.raisedOn = raisedOn;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTicketingDetailId() {
        return ticketingDetailId;
    }

    public void setTicketingDetailId(String ticketingDetailId) {
        this.ticketingDetailId = ticketingDetailId;
    }

    public String getTicketingFile() {
        return ticketingFile;
    }

    public void setTicketingFile(String ticketingFile) {
        this.ticketingFile = ticketingFile;
    }

    public String getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(String followedBy) {
        this.followedBy = followedBy;
    }

    public String getFollowedOn() {
        return followedOn;
    }

    public void setFollowedOn(String followedOn) {
        this.followedOn = followedOn;
    }

    public String getSetteledHm() {
        return setteledHm;
    }

    public void setSetteledHm(String setteledHm) {
        this.setteledHm = setteledHm;
    }

    public String getSetteledKm() {
        return setteledKm;
    }

    public void setSetteledKm(String setteledKm) {
        this.setteledKm = setteledKm;
    }

    public String getSetteltotalHrs() {
        return setteltotalHrs;
    }

    public void setSetteltotalHrs(String setteltotalHrs) {
        this.setteltotalHrs = setteltotalHrs;
    }

    public String getSetteltotalKM() {
        return setteltotalKM;
    }

    public void setSetteltotalKM(String setteltotalKM) {
        this.setteltotalKM = setteltotalKM;
    }

    public String getOverRideBy() {
        return overRideBy;
    }

    public void setOverRideBy(String overRideBy) {
        this.overRideBy = overRideBy;
    }

    public String getOverRideOn() {
        return overRideOn;
    }

    public void setOverRideOn(String overRideOn) {
        this.overRideOn = overRideOn;
    }

    public String getOverrideTripRemarks() {
        return overrideTripRemarks;
    }

    public void setOverrideTripRemarks(String overrideTripRemarks) {
        this.overrideTripRemarks = overrideTripRemarks;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getNextTrip() {
        return nextTrip;
    }

    public void setNextTrip(String nextTrip) {
        this.nextTrip = nextTrip;
    }

    public String getCurrentTripStartDate() {
        return currentTripStartDate;
    }

    public void setCurrentTripStartDate(String currentTripStartDate) {
        this.currentTripStartDate = currentTripStartDate;
    }

    public String getNextTripCountStatus() {
        return nextTripCountStatus;
    }

    public void setNextTripCountStatus(String nextTripCountStatus) {
        this.nextTripCountStatus = nextTripCountStatus;
    }

    public String getDriverBhatta() {
        return driverBhatta;
    }

    public void setDriverBhatta(String driverBhatta) {
        this.driverBhatta = driverBhatta;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelExpense() {
        return fuelExpense;
    }

    public void setFuelExpense(String fuelExpense) {
        this.fuelExpense = fuelExpense;
    }

    public String getParkingCost() {
        return parkingCost;
    }

    public void setParkingCost(String parkingCost) {
        this.parkingCost = parkingCost;
    }

    public String getSystemExpenses() {
        return systemExpenses;
    }

    public void setSystemExpenses(String systemExpenses) {
        this.systemExpenses = systemExpenses;
    }

    public String getTollExpense() {
        return tollExpense;
    }

    public void setTollExpense(String tollExpense) {
        this.tollExpense = tollExpense;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getAdvanceCode() {
        return advanceCode;
    }

    public void setAdvanceCode(String advanceCode) {
        this.advanceCode = advanceCode;
    }

    public String getBhattaDate() {
        return bhattaDate;
    }

    public void setBhattaDate(String bhattaDate) {
        this.bhattaDate = bhattaDate;
    }

    public String getIdleBhattaId() {
        return idleBhattaId;
    }

    public void setIdleBhattaId(String idleBhattaId) {
        this.idleBhattaId = idleBhattaId;
    }

    public String getVehicleAdvanceId() {
        return vehicleAdvanceId;
    }

    public void setVehicleAdvanceId(String vehicleAdvanceId) {
        this.vehicleAdvanceId = vehicleAdvanceId;
    }

    public String getDriverLastBalance() {
        return driverLastBalance;
    }

    public void setDriverLastBalance(String driverLastBalance) {
        this.driverLastBalance = driverLastBalance;
    }

    public String getTripAmount() {
        return tripAmount;
    }

    public void setTripAmount(String tripAmount) {
        this.tripAmount = tripAmount;
    }

    public String getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(String settleAmount) {
        this.settleAmount = settleAmount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getSettlementTripsSize() {
        return settlementTripsSize;
    }

    public void setSettlementTripsSize(String settlementTripsSize) {
        this.settlementTripsSize = settlementTripsSize;
    }

    public String getBhattaAmount() {
        return bhattaAmount;
    }

    public void setBhattaAmount(String bhattaAmount) {
        this.bhattaAmount = bhattaAmount;
    }

    public String getIdleDays() {
        return idleDays;
    }

    public void setIdleDays(String idleDays) {
        this.idleDays = idleDays;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillCopyName() {
        return billCopyName;
    }

    public void setBillCopyName(String billCopyName) {
        this.billCopyName = billCopyName;
    }

    public String getDocumentRequired() {
        return documentRequired;
    }

    public void setDocumentRequired(String documentRequired) {
        this.documentRequired = documentRequired;
    }

    public String getDocumentRequiredStatus() {
        return documentRequiredStatus;
    }

    public void setDocumentRequiredStatus(String documentRequiredStatus) {
        this.documentRequiredStatus = documentRequiredStatus;
    }

    public String getBillCopyFile() {
        return billCopyFile;
    }

    public void setBillCopyFile(String billCopyFile) {
        this.billCopyFile = billCopyFile;
    }

    public String getUnclearedAmount() {
        return unclearedAmount;
    }

    public void setUnclearedAmount(String unclearedAmount) {
        this.unclearedAmount = unclearedAmount;
    }

    public String getDriverChangeHour() {
        return driverChangeHour;
    }

    public void setDriverChangeHour(String driverChangeHour) {
        this.driverChangeHour = driverChangeHour;
    }

    public String getDriverChangeMinute() {
        return driverChangeMinute;
    }

    public void setDriverChangeMinute(String driverChangeMinute) {
        this.driverChangeMinute = driverChangeMinute;
    }

    public String getDriverInTrip() {
        return driverInTrip;
    }

    public void setDriverInTrip(String driverInTrip) {
        this.driverInTrip = driverInTrip;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getAdvanceReturn() {
        return advanceReturn;
    }

    public void setAdvanceReturn(String advanceReturn) {
        this.advanceReturn = advanceReturn;
    }

    public String getMiscRate() {
        return miscRate;
    }

    public void setMiscRate(String miscRate) {
        this.miscRate = miscRate;
    }

    public String getExpenseHour() {
        return expenseHour;
    }

    public void setExpenseHour(String expenseHour) {
        this.expenseHour = expenseHour;
    }

    public String getExpenseMinute() {
        return expenseMinute;
    }

    public void setExpenseMinute(String expenseMinute) {
        this.expenseMinute = expenseMinute;
    }

    public String getPreCoolingCost() {
        return preCoolingCost;
    }

    public void setPreCoolingCost(String preCoolingCost) {
        this.preCoolingCost = preCoolingCost;
    }

    public String getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String getPendingChallan() {
        return pendingChallan;
    }

    public void setPendingChallan(String pendingChallan) {
        this.pendingChallan = pendingChallan;
    }

    public String getPendingChallanAmount() {
        return pendingChallanAmount;
    }

    public void setPendingChallanAmount(String pendingChallanAmount) {
        this.pendingChallanAmount = pendingChallanAmount;
    }

    public String getTotalDocumentAmount() {
        return totalDocumentAmount;
    }

    public void setTotalDocumentAmount(String totalDocumentAmount) {
        this.totalDocumentAmount = totalDocumentAmount;
    }

    public String getUploadedChallan() {
        return uploadedChallan;
    }

    public void setUploadedChallan(String uploadedChallan) {
        this.uploadedChallan = uploadedChallan;
    }

    public String getUploadedChallanAmount() {
        return uploadedChallanAmount;
    }

    public void setUploadedChallanAmount(String uploadedChallanAmount) {
        this.uploadedChallanAmount = uploadedChallanAmount;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredTime() {
        return vehicleRequiredTime;
    }

    public void setVehicleRequiredTime(String vehicleRequiredTime) {
        this.vehicleRequiredTime = vehicleRequiredTime;
    }

    public String getConsignmentStatusId() {
        return consignmentStatusId;
    }

    public void setConsignmentStatusId(String consignmentStatusId) {
        this.consignmentStatusId = consignmentStatusId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getOrderSize() {
        return orderSize;
    }

    public void setOrderSize(String orderSize) {
        this.orderSize = orderSize;
    }

    public String[] getOrderSequenceNo() {
        return orderSequenceNo;
    }

    public void setOrderSequenceNo(String[] orderSequenceNo) {
        this.orderSequenceNo = orderSequenceNo;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getConsginmentOrderStatus() {
        return consginmentOrderStatus;
    }

    public void setConsginmentOrderStatus(String consginmentOrderStatus) {
        this.consginmentOrderStatus = consginmentOrderStatus;
    }

    public String getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(String seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(String wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getDestinationWId() {
        return destinationWId;
    }

    public void setDestinationWId(String destinationWId) {
        this.destinationWId = destinationWId;
    }

    public String getSourceWId() {
        return sourceWId;
    }

    public void setSourceWId(String sourceWId) {
        this.sourceWId = sourceWId;
    }

    public String getEndTrailerKM() {
        return endTrailerKM;
    }

    public void setEndTrailerKM(String endTrailerKM) {
        this.endTrailerKM = endTrailerKM;
    }

    public String getStartTrailerKM() {
        return startTrailerKM;
    }

    public void setStartTrailerKM(String startTrailerKM) {
        this.startTrailerKM = startTrailerKM;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getTrailerStartKm() {
        return trailerStartKm;
    }

    public void setTrailerStartKm(String trailerStartKm) {
        this.trailerStartKm = trailerStartKm;
    }

    public String getWareHouseAdd() {
        return wareHouseAdd;
    }

    public void setWareHouseAdd(String wareHouseAdd) {
        this.wareHouseAdd = wareHouseAdd;
    }

    public String getPortAddress() {
        return portAddress;
    }

    public void setPortAddress(String portAddress) {
        this.portAddress = portAddress;
    }

    public String getAvailableLimit() {
        return availableLimit;
    }

    public void setAvailableLimit(String availableLimit) {
        this.availableLimit = availableLimit;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getEtcCost() {
        return etcCost;
    }

    public void setEtcCost(String etcCost) {
        this.etcCost = etcCost;
    }

    public String getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDeattchLoc() {
        return deattchLoc;
    }

    public void setDeattchLoc(String deattchLoc) {
        this.deattchLoc = deattchLoc;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getDeattchLocId() {
        return deattchLocId;
    }

    public void setDeattchLocId(String deattchLocId) {
        this.deattchLocId = deattchLocId;
    }

    public String getRemainOrderWeight() {
        return remainOrderWeight;
    }

    public void setRemainOrderWeight(String remainOrderWeight) {
        this.remainOrderWeight = remainOrderWeight;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderGPSTrackingLocation() {
        return orderGPSTrackingLocation;
    }

    public void setOrderGPSTrackingLocation(String orderGPSTrackingLocation) {
        this.orderGPSTrackingLocation = orderGPSTrackingLocation;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPlannedPickupDate() {
        return plannedPickupDate;
    }

    public void setPlannedPickupDate(String plannedPickupDate) {
        this.plannedPickupDate = plannedPickupDate;
    }

    public String getPlannedDeliveryDate() {
        return plannedDeliveryDate;
    }

    public void setPlannedDeliveryDate(String plannedDeliveryDate) {
        this.plannedDeliveryDate = plannedDeliveryDate;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getHireCharges() {
        return hireCharges;
    }

    public void setHireCharges(String hireCharges) {
        this.hireCharges = hireCharges;
    }

    public String getWayBillNo() {
        return wayBillNo;
    }

    public void setWayBillNo(String wayBillNo) {
        this.wayBillNo = wayBillNo;
    }

    public String getGpsDeviceId() {
        return gpsDeviceId;
    }

    public void setGpsDeviceId(String gpsDeviceId) {
        this.gpsDeviceId = gpsDeviceId;
    }

    public String getGpsVendor() {
        return gpsVendor;
    }

    public void setGpsVendor(String gpsVendor) {
        this.gpsVendor = gpsVendor;
    }

    public String getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getContainerCapacity() {
        return containerCapacity;
    }

    public void setContainerCapacity(String containerCapacity) {
        this.containerCapacity = containerCapacity;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRepoId() {
        return repoId;
    }

    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }

    public String getConsignmentNoteId() {
        return consignmentNoteId;
    }

    public void setConsignmentNoteId(String consignmentNoteId) {
        this.consignmentNoteId = consignmentNoteId;
    }

    public String getFromCityName() {
        return fromCityName;
    }

    public void setFromCityName(String fromCityName) {
        this.fromCityName = fromCityName;
    }

    public String getToCityName() {
        return toCityName;
    }

    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getBillingPartyId() {
        return billingPartyId;
    }

    public void setBillingPartyId(String billingPartyId) {
        this.billingPartyId = billingPartyId;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getBunkState() {
        return bunkState;
    }

    public void setBunkState(String bunkState) {
        this.bunkState = bunkState;
    }

    public String getBunkStatus() {
        return bunkStatus;
    }

    public void setBunkStatus(String bunkStatus) {
        this.bunkStatus = bunkStatus;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public String getCurrlocation() {
        return currlocation;
    }

    public void setCurrlocation(String currlocation) {
        this.currlocation = currlocation;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getRtoId() {
        return rtoId;
    }

    public void setRtoId(String rtoId) {
        this.rtoId = rtoId;
    }

    public String getRtoName() {
        return rtoName;
    }

    public void setRtoName(String rtoName) {
        this.rtoName = rtoName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getLastPointId() {
        return lastPointId;
    }

    public void setLastPointId(String lastPointId) {
        this.lastPointId = lastPointId;
    }

    public String getGrNumber() {
        return grNumber;
    }

    public void setGrNumber(String grNumber) {
        this.grNumber = grNumber;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getTripDetainHours() {
        return tripDetainHours;
    }

    public void setTripDetainHours(String tripDetainHours) {
        this.tripDetainHours = tripDetainHours;
    }

    public int getAccVehicleId() {
        return accVehicleId;
    }

    public void setAccVehicleId(int accVehicleId) {
        this.accVehicleId = accVehicleId;
    }

    public int getReasonForChange() {
        return reasonForChange;
    }

    public void setReasonForChange(int reasonForChange) {
        this.reasonForChange = reasonForChange;
    }

    public String getLinerName() {
        return linerName;
    }

    public void setLinerName(String linerName) {
        this.linerName = linerName;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getGrStatus() {
        return grStatus;
    }

    public void setGrStatus(String grStatus) {
        this.grStatus = grStatus;
    }

    public String getPaidExpense() {
        return paidExpense;
    }

    public void setPaidExpense(String paidExpense) {
        this.paidExpense = paidExpense;
    }

    public String getDieselAmount() {
        return dieselAmount;
    }

    public void setDieselAmount(String dieselAmount) {
        this.dieselAmount = dieselAmount;
    }

    public String getGrDate() {
        return grDate;
    }

    public void setGrDate(String grDate) {
        this.grDate = grDate;
    }

    public String getTripContainerId() {
        return tripContainerId;
    }

    public void setTripContainerId(String tripContainerId) {
        this.tripContainerId = tripContainerId;
    }

    public String getTripFactoryToIcdHour() {
        return tripFactoryToIcdHour;
    }

    public void setTripFactoryToIcdHour(String tripFactoryToIcdHour) {
        this.tripFactoryToIcdHour = tripFactoryToIcdHour;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getHireVehicleNo() {
        return hireVehicleNo;
    }

    public void setHireVehicleNo(String hireVehicleNo) {
        this.hireVehicleNo = hireVehicleNo;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getBillOfEntry() {
        return billOfEntry;
    }

    public void setBillOfEntry(String billOfEntry) {
        this.billOfEntry = billOfEntry;
    }

    public String getShipingLineNo() {
        return shipingLineNo;
    }

    public void setShipingLineNo(String shipingLineNo) {
        this.shipingLineNo = shipingLineNo;
    }

    public String getChallanDate() {
        return challanDate;
    }

    public void setChallanDate(String challanDate) {
        this.challanDate = challanDate;
    }

    public String getChallanNo() {
        return challanNo;
    }

    public void setChallanNo(String challanNo) {
        this.challanNo = challanNo;
    }

    public String getInteramPoint() {
        return interamPoint;
    }

    public void setInteramPoint(String interamPoint) {
        this.interamPoint = interamPoint;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getDetaintion() {
        return detaintion;
    }

    public void setDetaintion(String detaintion) {
        this.detaintion = detaintion;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getGrHours() {
        return grHours;
    }

    public void setGrHours(String grHours) {
        this.grHours = grHours;
    }

    public String getGrMins() {
        return grMins;
    }

    public void setGrMins(String grMins) {
        this.grMins = grMins;
    }

    public String getMovementTypeName() {
        return movementTypeName;
    }

    public void setMovementTypeName(String movementTypeName) {
        this.movementTypeName = movementTypeName;
    }

    public String getVehicleVendor() {
        return vehicleVendor;
    }

    public void setVehicleVendor(String vehicleVendor) {
        this.vehicleVendor = vehicleVendor;
    }

    public String getConsignmentContainerId() {
        return consignmentContainerId;
    }

    public void setConsignmentContainerId(String consignmentContainerId) {
        this.consignmentContainerId = consignmentContainerId;
    }

    public String getMailDeliveredResponse() {
        return mailDeliveredResponse;
    }

    public void setMailDeliveredResponse(String mailDeliveredResponse) {
        this.mailDeliveredResponse = mailDeliveredResponse;
    }

    public String getTollTax() {
        return tollTax;
    }

    public void setTollTax(String tollTax) {
        this.tollTax = tollTax;
    }

    public String getWeightmentExpense() {
        return weightmentExpense;
    }

    public void setWeightmentExpense(String weightmentExpense) {
        this.weightmentExpense = weightmentExpense;
    }

    public String getGrId() {
        return grId;
    }

    public void setGrId(String grId) {
        this.grId = grId;
    }

    public String getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(String usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String[] getExpenseTypes() {
        return expenseTypes;
    }

    public void setExpenseTypes(String[] expenseTypes) {
        this.expenseTypes = expenseTypes;
    }

    public String[] getExpenses() {
        return expenses;
    }

    public void setExpenses(String[] expenses) {
        this.expenses = expenses;
    }

    public String[] getTripExpensesId() {
        return tripExpensesId;
    }

    public void setTripExpensesId(String[] tripExpensesId) {
        this.tripExpensesId = tripExpensesId;
    }

    public String getTripExpenseIds() {
        return tripExpenseIds;
    }

    public void setTripExpenseIds(String tripExpenseIds) {
        this.tripExpenseIds = tripExpenseIds;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getNewSlip() {
        return newSlip;
    }

    public void setNewSlip(String newSlip) {
        this.newSlip = newSlip;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String[] getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String[] trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String[] getTrailerIds() {
        return trailerIds;
    }

    public void setTrailerIds(String[] trailerIds) {
        this.trailerIds = trailerIds;
    }

    public String getAdvanceGrNo() {
        return advanceGrNo;
    }

    public void setAdvanceGrNo(String advanceGrNo) {
        this.advanceGrNo = advanceGrNo;
    }

    public String getRemainWeight() {
        return remainWeight;
    }

    public void setRemainWeight(String remainWeight) {
        this.remainWeight = remainWeight;
    }

    public String getIsRepo() {
        return isRepo;
    }

    public void setIsRepo(String isRepo) {
        this.isRepo = isRepo;
    }

    public String[] getContainerTypeIds() {
        return containerTypeIds;
    }

    public void setContainerTypeIds(String[] containerTypeIds) {
        this.containerTypeIds = containerTypeIds;
    }

    public String[] getContainerNos() {
        return containerNos;
    }

    public void setContainerNos(String[] containerNos) {
        this.containerNos = containerNos;
    }

    public String getUniqueIds() {
        return uniqueIds;
    }

    public void setUniqueIds(String uniqueIds) {
        this.uniqueIds = uniqueIds;
    }

    public String[] getBunkNames() {
        return bunkNames;
    }

    public void setBunkNames(String[] bunkNames) {
        this.bunkNames = bunkNames;
    }

    public String[] getBunkPlace() {
        return bunkPlace;
    }

    public void setBunkPlace(String[] bunkPlace) {
        this.bunkPlace = bunkPlace;
    }

    public String[] getFuelDates() {
        return fuelDates;
    }

    public void setFuelDates(String[] fuelDates) {
        this.fuelDates = fuelDates;
    }

    public String[] getFuelAmounts() {
        return fuelAmounts;
    }

    public void setFuelAmounts(String[] fuelAmounts) {
        this.fuelAmounts = fuelAmounts;
    }

    public String[] getFuelLtrs() {
        return fuelLtrs;
    }

    public void setFuelLtrs(String[] fuelLtrs) {
        this.fuelLtrs = fuelLtrs;
    }

    public String[] getFuelFilledBy() {
        return fuelFilledBy;
    }

    public void setFuelFilledBy(String[] fuelFilledBy) {
        this.fuelFilledBy = fuelFilledBy;
    }

    public String[] getFuelRemark() {
        return fuelRemark;
    }

    public void setFuelRemark(String[] fuelRemark) {
        this.fuelRemark = fuelRemark;
    }

    public String[] getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String[] slipNo) {
        this.slipNo = slipNo;
    }

    public String[] getFuelUniqueId() {
        return fuelUniqueId;
    }

    public void setFuelUniqueId(String[] fuelUniqueId) {
        this.fuelUniqueId = fuelUniqueId;
    }

    public String getDriverNameId() {
        return driverNameId;
    }

    public void setDriverNameId(String driverNameId) {
        this.driverNameId = driverNameId;
    }

    public String getAdvancerequestamt() {
        return advancerequestamt;
    }

    public void setAdvancerequestamt(String advancerequestamt) {
        this.advancerequestamt = advancerequestamt;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }

    public String getTobepaidtoday() {
        return tobepaidtoday;
    }

    public void setTobepaidtoday(String tobepaidtoday) {
        this.tobepaidtoday = tobepaidtoday;
    }

    public String getTripday() {
        return tripday;
    }

    public void setTripday(String tripday) {
        this.tripday = tripday;
    }

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(String currencyid) {
        this.currencyid = currencyid;
    }

    public String getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(String expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getCommodityCategory() {
        return commodityCategory;
    }

    public void setCommodityCategory(String commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    public String getLinerId() {
        return linerId;
    }

    public void setLinerId(String linerId) {
        this.linerId = linerId;
    }

    public String getTripPointId() {
        return tripPointId;
    }

    public void setTripPointId(String tripPointId) {
        this.tripPointId = tripPointId;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreditNoteId() {
        return creditNoteId;
    }

    public void setCreditNoteId(String creditNoteId) {
        this.creditNoteId = creditNoteId;
    }

    public String getCreditNoteDate() {
        return creditNoteDate;
    }

    public void setCreditNoteDate(String creditNoteDate) {
        this.creditNoteDate = creditNoteDate;
    }

    public String getCreditNoteNo() {
        return creditNoteNo;
    }

    public void setCreditNoteNo(String creditNoteNo) {
        this.creditNoteNo = creditNoteNo;
    }

    public String getGateInDate() {
        return gateInDate;
    }

    public void setGateInDate(String gateInDate) {
        this.gateInDate = gateInDate;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getShippingBillDate() {
        return shippingBillDate;
    }

    public void setShippingBillDate(String shippingBillDate) {
        this.shippingBillDate = shippingBillDate;
    }

    public String getExcelGrNo() {
        return excelGrNo;
    }

    public void setExcelGrNo(String excelGrNo) {
        this.excelGrNo = excelGrNo;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getPdaAmount() {
        return pdaAmount;
    }

    public void setPdaAmount(String pdaAmount) {
        this.pdaAmount = pdaAmount;
    }

    public String getFixedCreditAmount() {
        return fixedCreditAmount;
    }

    public void setFixedCreditAmount(String fixedCreditAmount) {
        this.fixedCreditAmount = fixedCreditAmount;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getHalfContractCheck() {
        return halfContractCheck;
    }

    public void setHalfContractCheck(String halfContractCheck) {
        this.halfContractCheck = halfContractCheck;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getHandling() {
        return handling;
    }

    public void setHandling(String handling) {
        this.handling = handling;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTodayBooking20() {
        return todayBooking20;
    }

    public void setTodayBooking20(String todayBooking20) {
        this.todayBooking20 = todayBooking20;
    }

    public String getTodayBooking40() {
        return todayBooking40;
    }

    public void setTodayBooking40(String todayBooking40) {
        this.todayBooking40 = todayBooking40;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getFreight20() {
        return freight20;
    }

    public void setFreight20(String freight20) {
        this.freight20 = freight20;
    }

    public String getFreight40() {
        return freight40;
    }

    public void setFreight40(String freight40) {
        this.freight40 = freight40;
    }  

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String[] getProductId() {
        return productId;
    }

    public void setProductId(String[] productId) {
        this.productId = productId;
    }

    public String getIrn() {
        return irn;
    }

    public void setIrn(String irn) {
        this.irn = irn;
    }

    public String getSignedQr() {
        return signedQr;
    }

    public void setSignedQr(String signedQr) {
        this.signedQr = signedQr;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

    
    
    
}
