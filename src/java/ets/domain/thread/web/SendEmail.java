/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.thread.web;




/**
 *
 * @author vinoth
 */
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.trip.business.TripBP;
import ets.domain.report.business.ReportBP;           
import ets.domain.report.business.ReportTO;
import ets.domain.trip.business.TripTO;
import ets.domain.util.ThrottleConstants;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
//import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

//import com.itextpdf.text.pdf.security.DigestAlgorithms;
//import com.itextpdf.text.pdf.security.ExternalDigest;
//import com.itextpdf.text.pdf.security.ExternalSignature;
//import com.itextpdf.text.pdf.security.MakeSignature;
//import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
//import com.itextpdf.text.pdf.security.PrivateKeySignature;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Provider;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

//public class TestThread extends Thread {
public class SendEmail implements Runnable {

    int updateStatus = 0;
    TripBP tripBP;
    ReportBP reportBP;
    int MailSendingId = 0;
    String SMSContactNo = "";
    String SMSContent = "";
    int smsSendingStatus = 0;
    String tripId = "";
    String lastSmsDeliveredTime = "";
    String Toemailid = "";
    String CCemailid = "";
    String BCCemailid = "";
    String Frommailid = "";
    String EmailSubject = "";
    String BodyMessage = "";
    String SMTP = "";
    String Password = "";
    int updatedStatus = 0;
    int PORT = 0;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    TripTO tripTO = new TripTO();
    ReportTO reportTO = new ReportTO();

    String name;

    public SendEmail(String name) {
        this.name = name;
    }

    public SendEmail(TripBP tripBP, ReportBP reportBP, String name) {
        this.tripBP = tripBP;
        this.reportBP = reportBP;
        this.name = name;
    }

    public SendEmail() {
    }

    @Override
    public void run() {
        try {
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;
            ArrayList emailList = new ArrayList();
            emailList = tripBP.getEmailList();
            Iterator itr = emailList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                MailSendingId = Integer.parseInt(tripTO.getMailSendingId());
                EmailSubject = tripTO.getMailSubjectTo();
                BodyMessage = tripTO.getMailContentTo();
                Toemailid = tripTO.getMailIdTo();
                CCemailid = tripTO.getMailIdCc();
                BCCemailid = tripTO.getMailIdBcc();
                tripId = tripTO.getTripId();
                Properties props = System.getProperties();
                props.put("mail.smtp.starttls.enable", "true"); // added this line
                props.put("mail.smtp.host", SMTP);
                props.put("mail.smtp.user", Frommailid);
                props.put("mail.smtp.password", Password);
                props.put("mail.smtp.port", PORT);
                props.put("mail.smtp.auth", "true");
                /*
                 if ("smtp.gmail.com".equalsIgnoreCase(SMTP)) {
                 props.put("mail.smtp.user", Frommailid);
                 props.put("mail.smtp.password", Password);
                 props.put("mail.smtp.port", PORT);
                 props.put("mail.smtp.auth", "true");
                 } else {//godaddy server
                 props.put("mail.smtp.user", "");
                 props.put("mail.smtp.password", "");
                 props.put("mail.smtp.port", "25");
                 //props.put("mail.smtp.auth", "true");
                 }
                 */
                if (Toemailid.indexOf("") >= 0
                        || Toemailid.indexOf("") >= 0) {
                    if (!CCemailid.equals("") && !CCemailid.equals("-")) {
                        CCemailid = CCemailid + "," + "";
                    } else {
                        CCemailid = "";
                    }
                }

                String[] to = Toemailid.split(","); // added this line

                Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Frommailid, Password);
                    }
                });
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(Frommailid));

                InternetAddress[] toAddress = new InternetAddress[to.length];

                //System.out.println("Toemailid:"+Toemailid);
                //System.out.println("CCemailid:"+CCemailid);
                if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                    for (int i = 0; i < to.length; i++) { // changed from a while loop
                        toAddress[i] = new InternetAddress(to[i]);
                    }

                    for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                    }
                }

                ////////////// cc part////////////////////
                //            CCemailid = "";
                //CCemailid = "nithyap@entitlesolutions.com";
                if (!CCemailid.equals("-")) {
                    String[] cc = CCemailid.split(","); // added this line
                    int ccLen = cc.length;
                    InternetAddress[] ccAddress = new InternetAddress[ccLen];
                    int j = 0;
                    for (j = 0; (j < cc.length && !CCemailid.equals("")); j++) { // changed from a while loop
                        ccAddress[j] = new InternetAddress(cc[j]);
                    }
                    for (j = 0; j < ccAddress.length; j++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.CC, ccAddress[j]);
                    }
                }

                //            BCCemailid = "";
                //BCCemailid = "nithyap@entitlesolutions.com";
//                if (!BCCemailid.equals("-")) {
//                    String[] bcc = BCCemailid.split(","); // added this line
//                    int bccLen = bcc.length;
//                    InternetAddress[] bccAddress = new InternetAddress[bccLen];
//                    int j = 0;
//                    for (j = 0; (j < bcc.length && !BCCemailid.equals("")); j++) { // changed from a while loop
//                        bccAddress[j] = new InternetAddress(bcc[j]);
//                    }
//                    for (j = 0; j < bccAddress.length; j++) { // changed from a while loop
//                        message.addRecipient(Message.RecipientType.BCC, bccAddress[j]);
//                    }
//                }
                //System.out.println("Toemailid1:"+Toemailid);
                //System.out.println("CCemailid1:"+CCemailid);
                message.setSubject(EmailSubject);
                //System.out.println("BodyMessage ::::::::::::"+BodyMessage);
                message.setContent(BodyMessage, "text/html");
                Transport transport = session.getTransport("smtp");
                System.out.println("Password = " + Password);
                System.out.println("Frommailid = " + Frommailid);
                System.out.println("SMTP = " + SMTP);
                System.out.println("PORT = " + PORT);
                transport.connect(SMTP, Frommailid, Password);
                transport.sendMessage(message, message.getAllRecipients());
                int updateStatus = 0;
                tripTO.setMailDeliveredStatusId("1");
                tripTO.setMailDeliveredResponse("Delivered Sucessfully");
                updateStatus = tripBP.updateMailStatus(tripTO);
                transport.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            try {
                tripTO.setMailDeliveredStatusId("0");
                tripTO.setMailDeliveredResponse(e.toString());
                updateStatus = tripBP.updateMailStatus(tripTO);
            } catch (FPRuntimeException ex) {
                Logger.getLogger(SendSMS.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FPBusinessException ex) {
                Logger.getLogger(SendSMS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void sendExcelMail(String mailFileName, String to, String cc, String bcc, String mailContent, String bodyMsg, HSSFWorkbook hssWorkBook) {
        try {
//          Properties props = System.getProperties();
//            props.loadProperties();
//            final Properties properties = new Properties();
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;

            System.out.println("bcc ++" + bcc);
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);     
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            
            
            props.put("mail.smtp.auth", "true");     
//            Properties props = new Properties();
//            String host = props.getProperty("mail.smtp.host");
//            String port = props.getProperty("mail.smtp.port");
//            String userid = props.getProperty("mail.smtp.user.name");
//            String password1 = props.getProperty("mail.smtp.password");
//            String smtpsAuth = props.getProperty("mail.smtp.auth");

//            String SMTP_SERVER = host;
//            String SMTP_PORT = port;    
            System.out.println("SMTP_SERVER host@@@" + SMTP);
            System.out.println("SMTP_PORT port@@@s" + PORT);
            System.out.println("userid@@@" + Frommailid);
            System.out.println("password1@@@" + Password);
            System.out.println("To@@@" + to);

            final String from = Frommailid;
            final String password = Password;
            //final String from = "cgsespl@gmail.com";
            //final String password = "cgsadmin123";
            String[] recipients = null;

            String mailSubject = mailContent;
            BodyMessage = bodyMsg;
            System.out.println("BodyMessage@@@" + BodyMessage);
            // Get system properties
            //  Properties props = System.getProperties();

            // Setup mail server
//            props.put("mail.smtp.host", SMTP_SERVER);
//            props.put("mail.smtp.port", SMTP_PORT);
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.auth", smtpsAuth);
//
//
//
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", host);
//            props.put("mail.smtp.user", userid);
//            props.put("mail.smtp.password", password);
//            props.put("mail.smtp.port", port);
//            props.put("mail.smtps.auth", smtpsAuth);
            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));

            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.CC, addressCc);

            recipients = null;
            System.out.println("bcc" + bcc);
            recipients = bcc.split(",");
            System.out.println("recipients.length:" + recipients.length);
            InternetAddress[] addressBcc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressBcc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.BCC, addressBcc);

            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            message.setContent(BodyMessage, "text/html");

            // create the message part
            javax.mail.util.ByteArrayDataSource ds = null;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.out.println("01");
            hssWorkBook.write(baos);
            System.out.println("02");
            byte[] bytes = baos.toByteArray();
            try {
                ds = new ByteArrayDataSource(bytes, "application/excel");
            } catch (Exception err) {
                err.printStackTrace();
            }
            System.out.println("03");
            //fill message
//            messageBodyPart.setContent(mailContent, "text/html");
            DataHandler dh = new DataHandler(ds);
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            System.out.println("04");
            messageBodyPart.setDataHandler(dh);
            System.out.println("mailFileName in email:" + mailFileName);
            messageBodyPart.setFileName(mailFileName);
            System.out.println("2");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            System.out.println("3");
            // Put parts in message
            message.setContent(multipart);
            System.out.println("4");
            // Send the message
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            transport.connect(SMTP, PORT, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            //Transport.send(message);
            System.out.println("after send");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
        }
    }

    public void sendPDFMail(String mailFileName, String to, String cc, String bcc, String mailContent, String bodyMsg, ByteArrayOutputStream baos) {
        try {
//          Properties props = System.getProperties();
//            props.loadProperties();
//            final Properties properties = new Properties();
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;

            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");
//            Properties props = new Properties();
//            String host = props.getProperty("mail.smtp.host");
//            String port = props.getProperty("mail.smtp.port");
//            String userid = props.getProperty("mail.smtp.user.name");
//            String password1 = props.getProperty("mail.smtp.password");
//            String smtpsAuth = props.getProperty("mail.smtp.auth");

//            String SMTP_SERVER = host;
//            String SMTP_PORT = port;
            System.out.println("SMTP_SERVER host@@@" + SMTP);
            System.out.println("SMTP_PORT port@@@s" + PORT);
            System.out.println("userid@@@" + Frommailid);
            System.out.println("password1@@@" + Password);
            System.out.println("To@@@" + to);

            final String from = Frommailid;
            final String password = Password;
            //final String from = "cgsespl@gmail.com";
            //final String password = "cgsadmin123";
            String[] recipients = null;

            String mailSubject = mailContent;
            BodyMessage = bodyMsg;
            System.out.println("BodyMessage pdf@@@" + BodyMessage);
            // Get system properties
            //  Properties props = System.getProperties();

            // Setup mail server
//            props.put("mail.smtp.host", SMTP_SERVER);
//            props.put("mail.smtp.port", SMTP_PORT);
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.auth", smtpsAuth);
//
//
//
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", host);
//            props.put("mail.smtp.user", userid);
//            props.put("mail.smtp.password", password);
//            props.put("mail.smtp.port", port);
//            props.put("mail.smtps.auth", smtpsAuth);
            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));

            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.CC, addressCc);

            recipients = null;
            recipients = bcc.split(",");

            InternetAddress[] addressBcc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressBcc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.BCC, addressBcc);

            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            message.setContent(BodyMessage, "text/html");

            // create the message part
            javax.mail.util.ByteArrayDataSource ds = null;
            // ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.out.println("01");
//            PdfWriter.getInstance(document, outputStream);
//            pdfdoc.write(baos);
            System.out.println("02");
            byte[] bytes = baos.toByteArray();
            try {
                ds = new ByteArrayDataSource(bytes, "application/pdf");
            } catch (Exception err) {
                err.printStackTrace();
            }
            System.out.println("03");
            //fill message
//            messageBodyPart.setContent(mailContent, "text/html");
            DataHandler dh = new DataHandler(ds);
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            System.out.println("04");
            messageBodyPart.setDataHandler(dh);
            System.out.println("mailFileName in email:" + mailFileName);
            messageBodyPart.setFileName(mailFileName);
            System.out.println("2");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            System.out.println("3");
            // Put parts in messagel
            message.setContent(multipart);
            System.out.println("4");
            // Send the message
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            transport.connect(SMTP, PORT, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            //Transport.send(message);
            System.out.println("after send");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
        }
    }

    public void sendMultipleAttachPDFMail(String to, String cc, String bcc, String mailContent, String bodyMsg, Multipart multipart) throws Exception {
        try {
//          Properties props = System.getProperties();
//            props.loadProperties();
//            final Properties properties = new Properties();
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;

            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");
//            Properties props = new Properties();
//            String host = props.getProperty("mail.smtp.host");
//            String port = props.getProperty("mail.smtp.port");
//            String userid = props.getProperty("mail.smtp.user.name");     
//            String password1 = props.getProperty("mail.smtp.password");
//            String smtpsAuth = props.getProperty("mail.smtp.auth");

//            String SMTP_SERVER = host;
//            String SMTP_PORT = port;
            System.out.println("SMTP_SERVER host@@@" + SMTP);
            System.out.println("SMTP_PORT port@@@s" + PORT);
            System.out.println("userid@@@" + Frommailid);
            System.out.println("password1@@@" + Password);
            System.out.println("To@@@" + to);

            final String from = Frommailid;
            final String password = Password;
            //final String from = "cgsespl@gmail.com";
            //final String password = "cgsadmin123";
            String[] recipients = null;

            String mailSubject = mailContent;
            BodyMessage = bodyMsg;
            System.out.println("BodyMessage pdf@@@" + BodyMessage);
            // Get system properties
            //  Properties props = System.getProperties();

            // Setup mail server
//            props.put("mail.smtp.host", SMTP_SERVER);
//            props.put("mail.smtp.port", SMTP_PORT);
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.auth", smtpsAuth);
//
//
//
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", host);
//            props.put("mail.smtp.user", userid);
//            props.put("mail.smtp.password", password);
//            props.put("mail.smtp.port", port);
//            props.put("mail.smtps.auth", smtpsAuth);
            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));

            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.CC, addressCc);

            recipients = null;
            recipients = bcc.split(",");

            InternetAddress[] addressBcc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressBcc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.BCC, addressBcc);

            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            //  message.setContent(BodyMessage, "text/html");;
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(BodyMessage);

            // create the message part
            javax.mail.util.ByteArrayDataSource ds = null;
            // ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.out.println("01");
//            PdfWriter.getInstance(document, outputStream);
//            pdfdoc.write(baos);
            System.out.println("02");

            System.out.println("3");
            // Put parts in message
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            System.out.println("4");
            // Send the message
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            System.out.println("PORT:" + PORT);
            transport.connect(SMTP, PORT, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            //Transport.send(message);
            System.out.println("after send");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
            throw new NullPointerException("unable to send email");
        }
    }

    public Multipart addMultipleAttachments(String mailFileName, ByteArrayOutputStream baos, Multipart multipart, Provider p, PrivateKey key, java.security.cert.Certificate[] chain) throws IOException, DocumentException, GeneralSecurityException {
        javax.mail.util.ByteArrayDataSource ds = null;
        // ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.out.println("01");
//            PdfWriter.getInstance(document, outputStream);
//            pdfdoc.write(baos);
        System.out.println("02");

        byte[] bytes = baos.toByteArray();
        //adding digital sign

        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //  PdfReader reader = new PdfReader("D://ExportInvoice.pdf");
        //FileOutputStream fout = new FileOutputStream("D://signed.pdf");
        PdfReader reader = new PdfReader(bis);
        System.out.println("here1");
        Rectangle cropBox = reader.getCropBox(1);
        float width = 120;
        float height = 120;
//            PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\0');
//            System.out.println("here 2");
//            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
//              appearance.setVisibleSignature(new Rectangle(cropBox.getRight(width), cropBox.getBottom(),
//                              cropBox.getRight(), cropBox.getBottom(height)), 1, null);
//              appearance.setAcro6Layers(false);
//              System.out.println("here 3");
//               appearance.setLayer2Font(new Font(FontFamily.TIMES_ROMAN));
//                 PrivateKeySignature pks = new PrivateKeySignature(key, DigestAlgorithms.SHA256, p.getName());
//                 System.out.println("here 4");
//		        ExternalDigest digest = new BouncyCastleDigest();
//                        System.out.println("pass here");
//		       MakeSignature.signDetached(appearance, digest, pks, chain, null, null, null, 0, CryptoStandard.CMS);
//                       System.out.println("here 5");

        bytes = outputStream.toByteArray();// after adding digital sign again convert to bytes[]
        try {
            ds = new ByteArrayDataSource(bytes, "application/pdf");
        } catch (Exception err) {
            err.printStackTrace();
        }
        System.out.println("03");
        //fill message
//            messageBodyPart.setContent(mailContent, "text/html");
        DataHandler dh = new DataHandler(ds);
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        System.out.println("04");
        // Multipart multipart = new MimeMultipart();
        try {
            messageBodyPart.setDataHandler(dh);

            System.out.println("mailFileName in email:" + mailFileName);
            messageBodyPart.setFileName(mailFileName);
            System.out.println("2");

            multipart.addBodyPart(messageBodyPart);
        } catch (MessagingException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return multipart;
    }

    public int sendMultipleAttachPDFMail(String to, String cc, String bcc, String mailContent, String bodyMsg, String pdfname) throws Exception {
        int check = 0;
         System.out.println("ENTERED INSIDE");
        try {
            SMTP = "smtp.gmail.com";
            PORT = Integer.parseInt("587");
            Frommailid = ThrottleConstants.custInvFromMailId;
            Password = ThrottleConstants.custInvpassword;
            System.out.println("Frommailid--inside pdf mail---"+Frommailid);
            System.out.println("Password---inside pdf mail--"+Password);
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");

            System.out.println("SMTP_SERVER host@@@" + SMTP);
            System.out.println("SMTP_PORT port@@@s" + PORT);
            System.out.println("userid@@@" + Frommailid);
            System.out.println("password1@@@" + Password);
            System.out.println("To@@@" + to);

            final String from = Frommailid;
            final String password = Password;
            String[] recipients = null;

            String mailSubject = mailContent;
            BodyMessage = bodyMsg;
            System.out.println("BodyMessage pdf@@@" + BodyMessage);
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Frommailid, "Invoice Received"));
            //message.setFrom(new InternetAddress(from));

            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }

            message.setRecipients(Message.RecipientType.CC, addressCc);

            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            javax.mail.util.ByteArrayDataSource ds = null;

            ////////////////// new code //////////////////////////////

            Multipart multipart1 = new MimeMultipart();
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setContent(BodyMessage, "text/html");
            multipart1.addBodyPart(messageBodyPart1);

            String[] pdf = pdfname.split(",");

            for (int i = 0; i < pdf.length; i++) {
                String fileName = pdf[i];
                BodyPart messageBodyPart = new MimeBodyPart();
                String filePath = ThrottleConstants.alertReportPath;
                String fileAttachment1 = filePath + "//" + fileName;
                System.out.println("fileAttachment " + fileAttachment1);
                DataSource source1 = new FileDataSource(fileAttachment1);
                messageBodyPart.setDataHandler(new DataHandler(source1));
                String name = fileName;
                messageBodyPart.setFileName(name);
                multipart1.addBodyPart(messageBodyPart);
            }

            message.setContent(multipart1);

            ///////////////////////// new code //////////////////////////
            
            System.out.println("4");
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            System.out.println("PORT:" + PORT);
            transport.connect(SMTP, PORT, Frommailid, Password);
            System.out.println("sending");
            transport.sendMessage(message, message.getAllRecipients());
            check = 1;
            System.out.println("sent");
            transport.close();

        } catch (Exception e) {
            e.printStackTrace();
            check = 0;
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
            throw new NullPointerException("unable to send email");
        }
        return check;
    }

    public Multipart addMultipleAttachments(String mailFileName, ByteArrayOutputStream baos, Multipart multipart) {
        javax.mail.util.ByteArrayDataSource ds = null;
        byte[] bytes = baos.toByteArray();

        try {
            System.out.println("bytes len" + bytes.length);
            ds = new ByteArrayDataSource(bytes, "application/pdf");
            System.out.println("dssssssssss " + ds);
//
//            File theDir = new File(ThrottleConstants.alertReportPath+"//"+mailFileName);
//            if (!theDir.exists()) {
//                boolean result = theDir.mkdir();
//                if (result) {
//                    System.out.println(ThrottleConstants.alertReportPath + " Created");
//                }
//            }
//            
            System.out.println("filepath = " + ThrottleConstants.alertReportPath + "//" + mailFileName);

            File someFile = new File(ThrottleConstants.alertReportPath + "//" + mailFileName);
            FileOutputStream fos = new FileOutputStream(someFile);
            fos.write(bytes);
            fos.flush();
            fos.close();

        } catch (Exception err) {
            err.printStackTrace();
        }
        System.out.println("03");
        //fill message

        DataHandler dh = new DataHandler(ds);
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        System.out.println("04");
        // Multipart multipart = new MimeMultipart();
        try {
            messageBodyPart.setDataHandler(dh);
            System.out.println("dssssssssss " + messageBodyPart.hashCode());
            System.out.println("mailFileName in email:" + mailFileName);
            messageBodyPart.setFileName(mailFileName);
            System.out.println("2");
            multipart.addBodyPart(messageBodyPart);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return multipart;
    }

    public static void main(String[] args) throws Exception {
        SendEmail mail = new SendEmail();
        Multipart multipart = new MimeMultipart();
        // mail.sendMultipleAttachPDFMail("arunkumar.kanagaraj@newage-global.com", "arun@entitlesolution.com", "", "Welcome", "Hai", null);
    }
     public int sendEinvoiceAlertMail(String to, String cc, String bcc, String mailContent) throws Exception {
        int check = 0;
         System.out.println("ENTERED INSIDE");
        try {
            SMTP = "smtp.gmail.com";
            PORT = Integer.parseInt("587");
            Frommailid = "info@ampsolutions.co.in";
            Password = "hkqrpstilfbvdgdk";
            System.out.println("Frommailid--inside pdf mail---"+Frommailid);
            System.out.println("Password---inside pdf mail--"+Password);
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");

            System.out.println("SMTP_SERVER host@@@" + SMTP);
            System.out.println("SMTP_PORT port@@@s" + PORT);
            System.out.println("userid@@@" + Frommailid);
            System.out.println("password1@@@" + Password);
            System.out.println("To@@@" + to);

            final String from = Frommailid;
            final String password = Password;
            String[] recipients = null;

            String mailSubject = mailContent;
//            BodyMessage = bodyMsg;
//            System.out.println("BodyMessage pdf@@@" + BodyMessage);
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Frommailid, "Invoice Alert"));
            //message.setFrom(new InternetAddress(from));

            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }

            message.setRecipients(Message.RecipientType.CC, addressCc);

            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            javax.mail.util.ByteArrayDataSource ds = null;

            ////////////////// new code //////////////////////////////

            Multipart multipart1 = new MimeMultipart();
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setContent(BodyMessage, "text/html");
            multipart1.addBodyPart(messageBodyPart1);
//
//            String[] pdf = pdfname.split(",");
//
//            for (int i = 0; i < pdf.length; i++) {
//                String fileName = pdf[i];
//                BodyPart messageBodyPart = new MimeBodyPart();
//                String filePath = ThrottleConstants.alertReportPath;
//                String fileAttachment1 = filePath + "//" + fileName;
//                System.out.println("fileAttachment " + fileAttachment1);
//                DataSource source1 = new FileDataSource(fileAttachment1);
//                messageBodyPart.setDataHandler(new DataHandler(source1));
//                String name = fileName;
//                messageBodyPart.setFileName(name);
//                multipart1.addBodyPart(messageBodyPart);
//            }

            message.setContent(multipart1);

            ///////////////////////// new code //////////////////////////
            
            System.out.println("4");
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            System.out.println("PORT:" + PORT);
            transport.connect(SMTP, PORT, Frommailid, Password);
            System.out.println("sending");
            transport.sendMessage(message, message.getAllRecipients());
            check = 1;
            System.out.println("sent");
            transport.close();

        } catch (Exception e) {
            e.printStackTrace();
            check = 0;
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
            throw new NullPointerException("unable to send email");
        }
        return check;
    }

}
