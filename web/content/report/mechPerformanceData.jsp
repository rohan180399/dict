<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Mechanic Performance </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Reports</a></li>
          <li class="active">Mechanic Performance</li>
        </ol>
      </div>
      </div>

<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
    <form name="vehicleReport">
        
        <%@ include file="/content/common/message.jsp" %>
        
        

        <center>   
        <c:if test = "${mechPerformanceList != null}" >
<!--            <center> <input type="button" class="button" name="print" value="print" onClick="printPage();" > </center>-->
        <div id="printPage" >
            
                        <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">


                <% int cntr = 0;%> 
                <c:forEach items="${mechPerformanceList}" var="mech"> 

                    
                    <%if(cntr == 0){%>
                    <tr>
                        <td  height="30" >Technician Name: <c:out value="${mech.technician}" /></td>                        
                        <td  height="30">Location: <c:out value="${mech.companyName}" /></td>
                            
                    </tr>
                    <tr>
                        <td  height="30" >From Date: <c:out value="${fromDate}" /></td>                        
                        <td  height="30">To Date: <c:out value="${toDate}" /></td>
                            
                    </tr>
                    <%}
                    cntr++;%>
                </c:forEach>
                
            </table>

            
            
            
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">

                <tr>
                    
                    <th  height="30">Job Card No</th>
                    <th  height="30">Problem Name</th>
                    <th  height="30">Estimated Hrs</th>
                    <th  height="30">Actual Hrs</th>
                </tr>
                <% int index = 0;%> 
                <c:forEach items="${mechPerformanceList}" var="mech"> 
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
                    

                        <td class="<%=classText %>" height="30" ><c:out value="${mech.jobCardId}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.problemName}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${mech.estHrs}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.actHrs}" /></td>                        

                    </tr>
                    <% index++;%>
                </c:forEach>
                
            </table>
     
            </div>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
 </div>


        </div>
        </div>



<%@ include file="/content/common/NewDesign/settings.jsp" %>

