<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){        
            document.approve.action = '/throttle/savePaymentapproval.do';
            document.approve.submit();
        }
        
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
           
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${paymentDetails != null}" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border" >

                <a style="display: none"></a>
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Payment Approval</div></td>
                </tr>
                <c:forEach items="${paymentDetails}" var="payment">
                <tr>
                    <td class="text2" height="30" style="height:25px;border-bottom:1px solid;border-bottom-color: #f2f2f2;font-weight:normal;background:#f2f2f2;font-size:12px;">Cnote Name</td>
                    <td class="text2" height="30">
                       <input name="consignmentOrderId" id="consignmentOrderId" type="hidden" class="textbox" value="<c:out value="${payment.consignmentOrderId}"/>"><c:out value="${payment.consignmentNoteNo}"/> </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Paid Date</td>
                    <td class="text1" height="30"><c:out value="${payment.createdDate}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Payment Type</td>
                    <td class="text2" height="30">
                        <c:if test="${payment.paymentType=='1'}" >
                                    Credit
                                    </c:if>
                                  <c:if test="${payment.paymentType=='2'}" >
                                    Advance
                                    </c:if>
                                  <c:if test="${payment.paymentType=='3'}" >
                                    To Pay and Advance
                                    </c:if>
                                  <c:if test="${payment.paymentType=='4'}" >
                                    To Pay
                                    </c:if>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Payment Mode</td>
                    <td class="text1" height="30"><c:out value="${payment.paymentModeName}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Payment Reference No</td>
                    <td class="text2" height="30">
                     <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                <c:if test="${payment.paymentModeId=='6' }" >
                                   <c:out value="${payment.rtgsNo}"/>
                                    </c:if>

                                     <c:if test="${payment.paymentModeId=='2' }" >
                                   <c:out value="${payment.chequeNo}"/>
                                    </c:if>

                                    <c:if test="${payment.paymentModeId=='7' }" >
                                   <c:out value="${payment.draftNo}"/>
                                    </c:if></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Payment Remarks</td>
                    <td class="text1" height="30">
                        <td align="left" class="text2"> 
                                      <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                   <c:if test="${payment.paymentModeId=='6' }" >
                                   <c:out value="${payment.rtgsRemarks}"/>
                                   </c:if>

                                      <c:if test="${payment.paymentModeId=='7' }" >
                                   <c:out value="${payment.chequeRemarks}"/>
                                      </c:if>

                                      <c:if test="${payment.paymentModeId=='7' }" >
                                   <c:out value="${payment.draftRemarks}"/>
                                    </c:if>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Paid Amount</td>
                    <td class="text2" height="30"><c:out value="${payment.paidAmount}"/></td>
                </tr>

                <tr>
                    <td class="text1" height="30">Estimated Revenue</td>
                    <td class="text1" height="30"><c:out value="${payment.freightAmount}"/></td>
                </tr>

                
                
                <tr>
                    <td class="text1" height="30">Request On</td>
                    <td class="text1" height="30">
                        <input name="requeston" class="textbox" type="text" value='<c:out value="${fd.requestedOn}"/>' readonly></td>
                </tr>

                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Req.Advance Amt.</td>
                    <td class="text2" height="30"><input name="advancerequestamt" type="text" class="textbox" value='<c:out value="${fd.requestedadvance}"/>' readonly/></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Status</td>
                    <td class="text1" height="30"><select name="requestStatus">
                            <option value="" >-Select Any One-</option>
                            <option value="2">Approved</option>
                            <option value="3">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Approve Remarks</td>
                    <td class="text2" height="30"><textarea class="textbox" name="approveremarks"></textarea></td>
                </tr>
</c:forEach>
            </table>
                </c:if>
            <br>
            <center>
                
                <input type="button" id="mySubmit" value="Submit" class="button" onClick="submitPage();">
            </center>



        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
