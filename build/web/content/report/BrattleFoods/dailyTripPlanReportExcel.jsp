<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripPlanned-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

                    <c:if test="${tripDetails != null}">
                     <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >

                        <thead>
                       <tr height="60px;">
                           <td align="center"  class="contenthead"><h3>S.No</h3></td>
                       <td align="center" class="contenthead"><h3>Customer Name</h3></td>
                       <td align="center" class="contenthead"><h3>Billing Party</h3></td>
                        <td align="center" class="contenthead"><h3>Address</h3></td>
                       <td align="center" class="contenthead"><h3>shipping Line</h3></td>
                       <td align="center" class="contenthead"><h3>vehicle Type</h3></td>
                       <td align="center" class="contenthead"><h3>ISO/DSO</h3></td>
                       <td align="center" class="contenthead"><h3>FS/MT REPO</h3></td>
                       <td align="center" class="contenthead"><h3>PickUp Location</h3></td>
                       <td align="center" class="contenthead"><h3>To Location</h3></td>
                       <td align="center" colspan="2" class="contenthead" style="text-align:center"><h3>Total Booking</h3></td>
                       <td align="center" colspan="2" class="contenthead" style="text-align:center"><h3>Trip Start</h3></td>
                        <td align="center" class="contenthead" colspan="2"><h3>Trip End</h3></td>
                        <td align="center" class="contenthead"><h3>Consignment Order No</h3></td>
                        <!--<td align="center" class="contenthead"><h3>delete</h3></td>-->
                        <td align="center" class="contenthead"><h3>Trailer No</h3></td>
                        <td align="center" class="contenthead"><h3>Container(s)</h3></td>
                        <!--<td align="center" class="contenthead"><h3>Date</h3></td>-->
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center" >20'</td>
                            <td align="center" >40'</td>
                            <td align="center" >20'</td>
                            <td align="center" >40'</td>
                            <td align="center" >20'</td>
                            <td align="center" >40'</td>
                            <td></td>
                            <td></td>
                            <!--<td></td>-->
                        </tr>
                        </thead>
                            <c:set var="totalUnplannedTwenty" value="0"/>
                            <c:set var="totalUnplannedFourty" value="0"/>
                            <c:set var="totalPlannedTwenty" value="0"/>
                            <c:set var="totalPlannedFourty" value="0"/>
                            <c:set var="tottalStartedTwentyFtContainer" value="0"/>
                            <c:set var="totalStartedFourtyFtContainer" value="0"/>
                            <c:set var="totalEndedTwentyFtContainer" value="0"/>
                            <c:set var="totalEndedFourtyFtContainer" value="0"/>
                            <% int index = 1;%>

                            <c:forEach items="${tripDetails}" var="BPCLTD">
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>

                                <tr>
                                    <td align="center" ><%=index++%></td>
                                    <td align="center"><c:out value="${BPCLTD.customerName}"/></td>
                                    <td  align="center" ><c:out value="${BPCLTD.address}"/></td>
                                    <td align="center" ><c:out value="${BPCLTD.billingParty}"/></td>
                                    <td align="center" ><c:out value="${BPCLTD.linerName}"/></td>
                                    <td align="center"><c:out value="${BPCLTD.vehicleTypeName}"/></td>
                                    <td align="center">
                                        <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '2'|| BPCLTD.id == '3' }" >
                                            ISO
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '4'|| BPCLTD.id == '5'}">
                                            DSO
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test="${ BPCLTD.id == '3' }" >
                                           MT REPO
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '4'}">
                                            FS
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '2' || BPCLTD.id == '5'}">
                                            Import
                                        </c:if>
                                    </td>
                                    <td align="center" ><c:out value="${BPCLTD.origin}"/></td>
                                    <td  align="center" ><c:out value="${BPCLTD.destination}"/></td>
                                    <td  align="center"><c:out value="${BPCLTD.twentyFT}"/></td>
                                    <td align="center"  ><c:out value="${BPCLTD.fortyFT}"/></td>
                                    <td width="30" align="center"  ><c:out value="${BPCLTD.startedTwentyFtContainer}"/></td>
                                                <td width="30" align="center"  ><c:out value="${BPCLTD.startedFourtyFtContainer}"/></td>
                                                <c:set var="totalUnplannedTwenty" value="${totalUnplannedTwenty+ BPCLTD.twentyFT}"/>
                                                <c:set var="totalUnplannedFourty" value="${totalUnplannedFourty+ BPCLTD.fortyFT}"/>
                                                <c:set var="tottalStartedTwentyFtContainer" value="${tottalStartedTwentyFtContainer+ BPCLTD.startedTwentyFtContainer}"/>
                                                <c:set var="totalStartedFourtyFtContainer" value="${totalStartedFourtyFtContainer+ BPCLTD.startedFourtyFtContainer}"/>
                                                <c:set var="totalEndedTwentyFtContainer" value="${totalEndedTwentyFtContainer+ BPCLTD.endedTwentyFtContainer}"/>
                                                <c:set var="totalEndedFourtyFtContainer" value="${totalEndedFourtyFtContainer+ BPCLTD.endedFourtyFtContainer}"/>
                                                <td width="30" align="center"  ><c:out value="${BPCLTD.endedTwentyFtContainer}"/></td>
                                                <td width="30" align="center"  ><c:out value="${BPCLTD.endedFourtyFtContainer}"/></td>
                                        <td  align="center" ><c:out value="${BPCLTD.consignmentOrderNo}"/></td>
                                        <!--<td  align="center" ><c:out value="${BPCLTD.deleteOrder}"/></td>-->
                                        <td  align="center"  ><c:out value="${BPCLTD.vehicleNo}"/></td>
                                        <td  align="center"  ><c:out value="${BPCLTD.containerNo}"/></td>
                                        <!--<td  align="center"><c:out value="${BPCLTD.consignmentDate}"/></td>-->
                                </tr>
                            </c:forEach>
                                
                                 <tr>
                                            <td>Total</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><c:out value="${totalUnplannedTwenty}"/></td>
                                            <td><c:out value="${totalUnplannedFourty}"/></td>
                                            <td><c:out value="${tottalStartedTwentyFtContainer}"/></td>
                                            <td><c:out value="${totalStartedFourtyFtContainer}"/></td>
                                            <td><c:out value="${totalEndedTwentyFtContainer}"/></td>
                                            <td><c:out value="${totalEndedFourtyFtContainer}"/></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                 <%
                                index++;
                                   %>
                    </table>
                </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>