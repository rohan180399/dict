<%-- 
    Document   : vehicleCurrentStatus
    Created on : Sep 6, 2012, 4:01:14 PM
    Author     : Arul
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <!--table filter !-->
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <!--table filter !-->
        <!--table sort !-->
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="css/TableSortBiweekly.css"  type="text/css" />
        <!--table sort !-->


        <!--table datepicker !-->
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <!--table datepicker!-->



        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>

    </head>



    <script>
        function submitPage(value){
            /*if(isEmpty(document.currentStatus.statusDate.value)){
                alert("Please select the status date")
                document.currentStatus.statusDate.focus();
            }else{*/
            document.currentStatus.action="/throttle/vehicleCurrentStatusReport.do";
            document.currentStatus.submit();
            //}
        }
        function setFocus(){
            var statusDate='<%=request.getAttribute("statusDate")%>';
            var statusId='<%=request.getAttribute("statusId")%>';
            var regno='<%=request.getAttribute("regNo")%>';
            var locId='<%=request.getAttribute("loctId")%>';            
            if(statusDate!='null'){
                document.currentStatus.statusDate.value=statusDate;
            }if(statusId!='null'){
                document.currentStatus.statusId.value=statusId;
            }if(regno!='null'){
                document.currentStatus.regno.value=regno;
            }if(locId!='null'){                
                document.currentStatus.locationId.value=locId;
            }
        }
        window.onload = getVehicleNos;
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        }

    </script>
    <body onload="setFocus();">
        <form name="currentStatus" method="post">
            <c:if test="${menuPath != null}">
                <%@ include file="/content/common/path.jsp" %>
            </c:if>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Current Status Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td width="10%">&nbsp;</td>
                                        <td height="30">Vehicle No</td>
                                        <td>
                                            <input name="regno" id="regno" type="text" class="form-control" size="20" value="" onKeyPress="getVehicleNos();" autocomplete="off">
                                        </td>
                                        <td>Location</td>
                                        <td>
                                            <select name="locationId" id="locationId" class="selectBox" style="width:120px;">
                                                <option value="">--Select--</option>
                                                <c:if test="${locationList != null}">
                                                    <c:forEach items="${locationList}" var="ll">
                                                        <option value="<c:out value="${ll.compId}"/>"><c:out value="${ll.city}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                                <%--                                                <c:if test = "${opLocation != null}" >
                                                                                                    <c:forEach items="${opLocation}" var="vl">
                                                                                                        <option value='<c:out value="${vl.locationId}" />'><c:out value="${vl.locationName}" /></option>
                                                                                                    </c:forEach >
                                                                                                </c:if>--%>
                                            </select>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>                                        
                                        <td>Status</td>
                                        <td><select name="statusId" id="statusId" class="selectBox" style="width: 120px;">
                                                <option value="0">--Select--</option>
                                                <option value="1">Running</option>
                                                <option value="2">Halt</option>
                                            </select>
                                        </td>
                                        <td>Date</td>
                                        <td height="30">
                                            <input name="statusDate" id="statusDate" type="text" class="datepicker" size="20" style="height: 19px;"></td>
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <c:if test="${vehicleCurrentStatus == null}">
                <tr><font color="red">No Records Found</font></tr>
            </c:if>

        <br/>
        <c:if test = "${vehicleCurrentStatus != null}" >
            <%
                        int index = 0;
                        ArrayList vehicleCurrentStatus = (ArrayList) request.getAttribute("vehicleCurrentStatus");
                        if (vehicleCurrentStatus.size() != 0) {
            %>
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Trip Id</td>
                    <td class="contentsub" height="30">Trip Date</td>
                    <td class="contentsub" height="30">Vehicle No</td>
                    <td class="contentsub" height="30">Driver Name</td>
                    <td class="contentsub" height="30">Route Name</td>
                    <!--<td class="contentsub" height="30">Customer Name</td>-->
                    <td class="contentsub" height="30">Trip Type</td>
                    <td class="contentsub" height="30">In/Out Status</td>
                    <td class="contentsub" height="30">Known Location</td>
                    <td class="contentsub" height="30">In/Out DateTime</td>
                    <td class="contentsub" height="30">Trip Status</td>
                    <td class="contentsub" height="30">Current Status</td>
                </tr>
                <c:set var="vRStatus" value="${0}" />
                <c:set var="vHStatus" value="${0}" />
                <c:set var="vRHStatus" value="${0}" />

                <c:forEach items="${vehicleCurrentStatus}" var="vStatus">
                    <c:set var="total" value="${total+1}"></c:set>

                    <c:if test = "${vStatus.currentStatus == 'Running'}" >
                        <c:set var="vRStatus" value="${vRStatus + 1}" />
                    </c:if>
                    <c:if test = "${vStatus.currentStatus == 'Halt'}" >
                        <c:set var="vHStatus" value="${vHStatus + 1}" />
                    </c:if>
                    <c:set var="vRHStatus" value="${vRHStatus + 1}" />
                    <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                    %>
                    <%index++;%>
                    <tr>
                        <td class="<%=classText%>"  height="30"><%=index%></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.tripId}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.tripDate}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.regno}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.empName}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.routeName}"/></td>
                        <!--<td class="<%=classText%>"  height="30"><c:out value="${vStatus.custName}"/></td>-->
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.tripType}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.vehicleInOut}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.locationName}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.inOutDateTime}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.tripStatus}"/></td>
                        <td class="<%=classText%>"  height="30"><c:out value="${vStatus.currentStatus}"/></td>
                    </tr>
                </c:forEach>
            </table>
            <%
                        }
            %>
            <br />
            <table border="0" align="center" width="50%" cellpadding="0" cellspacing="5" class="overallSummary">
                <tr>
                    <th>Summary</th>
                    <td>Running  &nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vRStatus}"/></td>
                    <td>Halt &nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vHStatus}"/></td>
                    <td>Total &nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vRHStatus}"/></td>
                </tr>
            </table>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
