<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Fuel Price</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
         <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>


    </head>
    <script>
        function submitPage()
        {
            alert("1");
            if(document.add.bunkId.value==0){
                alert("Bunk Name should not be Empty");
                document.add.bunkId.focus();
                return;
            }
            if(textValidation(document.add.rate,'Fuel Price')){
                return;
            }
            if(textValidation(document.add.startDate,'Start Date')){
                return;
            }
            if(textValidation(document.add.endDate,'Start Date')){
                return;
            }
            alert("2");
            document.add.action='/throttle/saveFuelPage.do';
            alert("3");
            document.add.submit();
            alert("4");
        }


    </script>
    <body >
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="2" class="contenthead">Add Fuel Price</Td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Bunk Name</td>
                    <td class="text1">
                        <select name="bunkId" id="bunkId" style="width:125px">
                            <option value='0'>--Select--</option>
                            <c:if test="${BunkNameList!=null}">
                                <c:forEach items="${BunkNameList}" var="bl" >
                                    <option value='<c:out value="${bl.bunkId}"/>'> <c:out value="${bl.bunkName}"/> </option>
                                </c:forEach>
                            </c:if>
                        </select>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Fuel Price(per Ltr)</td>
                    <td class="text2"><input name="rate" id="rate" type="text" class="form-control" value="" size="20"></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Start Date</td>
                    <td class="text1">
                        <input name="startDate" id="startDate" type="text" class="datepicker" value="" size="20"></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>End Date</td>
                    <td class="text1">
                        <input name="endDate" id="endDate" type="text" class="datepicker" value="" size="20"></td>
                </tr>

            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
