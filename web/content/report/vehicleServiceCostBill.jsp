

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
       
    </head>
  
    <script>
      
function details(indx){
    billId=document.getElementsByName("billId");
    jobCardId=document.getElementsByName("jobCardId");   
      var url = '/throttle/BillDetails.do?billId='+billId[indx].value+'&jobCardId='+jobCardId[indx].value;          
           window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');    
    }

    </script>
    
    <body>        
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>                       
            <%@ include file="/content/common/message.jsp" %>
           
        
              
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            %>    
            <c:if test = "${billList != null}" >
                
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg" class="border">
                <tr>
                 <td class="contenthead" height="30" colspan="7">Service Bill Report </td>
                 </tr>
                    <tr>
                        <td class="contentsub" height="30">Vehicle No</td>
                        <td class="contentsub" height="30">Service Type</td>
                        <td class="contentsub" height="30">Company Name</td>
                        <td class="contentsub" height="30">Job Card No</td>
                        <td class="contentsub" height="30">Bill No</td>
                        <td class="contentsub" height="30">Created <br>Date/Time</td>
                        <td class="contentsub" align="right" width="70" height="30">Spare <br>Amount </td>
                        <td class="contentsub" align="right" width="70"  height="30">WO Amount </td>
                        <td class="contentsub" align="right" width="70"  height="30">Labor<br> Amount </td>
                        <td class="contentsub" align="right" width="70"  height="30">Discount</td>
                        <td class="contentsub" align="right" width="70"  height="30">Total<br>Amount </td>
                    </tr>
                    
                    <c:set var="spareAmount" value="0" />
                    <c:set var="laborAmount" value="0" />
                    <c:set var="woAmount"  value="0"/>
                    <c:set var="totalAmount"  value="0"/>
                    <c:set var="discount"  value="0"/>
                    
                    <c:forEach items="${billList}" var="bill">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        
            
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.regNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.serviceName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.companyName}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="jobCardId" value="<c:out value='${bill.jobCardId}'/>"><c:out value="${bill.jobCardId}"/></td>
                            <td class="<%=classText %>" height="30"><a href="" onclick="details(<%=index%>);"><input type="hidden" name="billId" value="<c:out value='${bill.billNo}'/>"><c:out value="${bill.billNo}"/></a></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.billDate}"/></td>
                            <td class="<%=classText %>" align="left"  width="70" height="30"><c:out value="${bill.spareAmount}"/></td>
                            <td class="<%=classText %>" align="left" width="70" height="30"><c:out value="${bill.woAmount}"/></td>
                            <td class="<%=classText %>" align="left" width="70" height="30"><c:out value="${bill.laborAmount}"/></td>
                            <td class="<%=classText %>" align="left" width="70" height="30"><c:out value="${bill.discount}"/></td>
                            <td class="<%=classText %>" align="left" width="70" height="30"><c:out value="${bill.totalAmount}"/></td>
                            <c:set var="spareAmount" value="${spareAmount + bill.spareAmount}" />
                            <c:set var="woAmount" value="${woAmount + bill.woAmount}" />
                            <c:set var="laborAmount"  value="${laborAmount + bill.laborAmount}" />
                            <c:set var="discount" value="${discount + bill.discount}" />            
                            <c:set var="totalAmount" value="${totalAmount + bill.totalAmount}" />            
                            
                        </tr>
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>
                    
                    
                    <tr>
                        <td class="text2" height="30">&nbsp;  </td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" align="left"  height="30"> 
                            <fmt:setLocale value="en_US" />SAR: <fmt:formatNumber value="${spareAmount}" pattern="##.00"/>            
                        </td>
                        <td class="text2" align="left"  height="30"> 
                            <fmt:setLocale value="en_US" />SAR: <fmt:formatNumber value="${woAmount}" pattern="##.00"/>
                        </td>
                        <td class="text2" align="left"  height="30">  
                            <fmt:setLocale value="en_US" />SAR: <fmt:formatNumber value="${laborAmount}" pattern="##.00"/>
                        </td>
                        <td class="text2"  align="left" height="30"> 
                            <fmt:setLocale value="en_US" />SAR: <fmt:formatNumber value="${discount}" pattern="##.00"/>
                        </td>
                        <td class="text2"  align="left" height="30"> 
                            <fmt:setLocale value="en_US" />SAR: <fmt:formatNumber value="${totalAmount}" pattern="##.00"/>
                        </td>    
                    </tr>        
                    
                    
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        
        
    </body>
</html>
