<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script>
    function addRates() {
//        alert("test");
            document.gstRates.action = "/throttle/addGSTRatesDetails.do";
            document.gstRates.method = "post";
            document.gstRates.submit();
    }
</script>
<script>
    function viewRateDetails() {
            document.gstRates.action = "/throttle/viewRateDetails.do";
            document.gstRates.method = "post";
            document.gstRates.submit();
    }
    </script>



<title>GST Rates Master</title>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">GST Rates Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onLoad="document.cityMaster.zoneName.focus();">
                <!--<body onload="initialize(28.3213, 77.5435, 'New Delhi')">-->
                <form name="gstRates"  method="POST">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <h2 align="center">GST Rates Details</h2>


                    <table id="table" class="table table-info mb30" style="width:100%" >
                        <input type="hidden" name="gstRateDetailId" id="gstRateDetailId" value=""  />
                        <input type="hidden" name="gstRateId" id="gstRateId" value=""  />
                        <input type="hidden" name="gstCategoryCode" id="gstCategoryCode" value=""  />
                        <input type="hidden" name="gstProductCategoryCode" id="gstProductCategoryCode" value=""  />
                        <input type="hidden" name="state" id="state" value=""  />
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th>S.No</th>
                                <th>GST Type</th>
                        <th>GST Service Category Code</th>
                        <th>GST Product Category Code</th>
                        <th>State Name</th>
                        <th>Select</th>
                        </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <c:if test = "${rateMasterList != null}">
                                <c:forEach items="${rateMasterList}" var="rat">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td align="left"> <%= sno%> </td>
                                        <td align="left">
                                        <c:if test="${rat.gstType == '1'}">
                                            Service
                                        </c:if>
                                        <c:if test="${rat.gstType == '2'}">
                                            Product
                                        </c:if>
                                        </td>
                                        <td align="left"><c:out value="${rat.sacCode}"/></td>
                                        <td align="left"><c:out value="${rat.hsnName}"/></td>
                                        <td align="left"> <c:out value="${rat.stateName}" /></td>
                                        <td align="center">
                                        <a href="/throttle/viewRateDetails.do?gstRateDetailId=<c:out value='${rat.gstRateDetailId}' />&gstCategoryCode=<c:out value="${rat.gstCategoryCode}"/>&state=<c:out value="${rat.state}"/>&gstProductCategoryCode=<c:out value="${rat.gstProductCategoryCode}"/>&gstType=<c:out value="${rat.gstType}"/>">
                                                <span class="label label-warning"><spring:message code="trucks.label.View"  text="alter"/> </span> </a>&emsp;
                                        <a href="/throttle/editRateDetails.do?gstRateDetailId=<c:out value='${rat.gstRateDetailId}' />&gstCategoryCode=<c:out value="${rat.gstCategoryCode}"/>&state=<c:out value="${rat.state}"/>&gstRateId=<c:out value="${rat.gstRateId}"/>&gstProductCategoryCode=<c:out value="${rat.gstProductCategoryCode}"/>&gstType=<c:out value="${rat.gstType}"/>">
                                                <span class="label label-warning"><spring:message code="trucks.label.Edit"  text="alter"/> </span> </a>
                                    </td>
                                    </tr>
                                    <%--<c:out value="${rat.gstCategoryCode}"/>--%>
                                    <c:out value="${rat.gstRateId}"/>
                                    <%--<c:out value="${rat.state}"/>--%>
                                    <%--<c:out value="${rat.gstProductCategoryCode}"/>--%>
                                    <%--<c:out value="${rat.gstType}"/>--%>
                                    <%--<c:out value="${rat.state}"/>--%>
                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                            <table align="center">
                        <tr >
                            <td >
                         <input type="button"  class="btn btn-info" value="ADD"  onClick="addRates()">
                            </td>
                        </tr>
                    </table>
                    </table>
                            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>