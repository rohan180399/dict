<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function checkVehicleTrailerAvalible() {
        var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
        var trailorUnits =<c:out value="${trailorUnitsDedicate}"/>;
//        alert("vehicleUnits ==== " + vehicleUnits);
//        alert("trailorUnits ==== " + trailorUnits);
        if (vehicleUnits == "" && vehicleUnits == "0") {
            $("#vehicles1").hide();
            $("#vehicles").hide();
            $("#tabs").tabs("select", 1);
            $("ul#myTab li:nth-child(2)").addClass("active");
            $("#trailers").addClass("active");
////            $("#trailers").attr('class').indexOf("active");
////            $("#trailers1").attr('class').indexOf("active");
////            $("#trailers").addClass("active");
////            $('#tabs').tabs({selected: '2'}); 
////            $( ".selector" ).tabs( "option", "active" );
//            $( ".selector" ).tabs( '2', "active" );
        }
        if (trailorUnits == "" && trailorUnits == "0") {
            $("#trailers1").hide();
            $("#trailers").hide();
            $("#tabs").tabs("select", 2);
            $("ul#myTab li:nth-child(1)").addClass("active");
            $("#vehicles").addClass("active");
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">View Fleet Vendor Vehicle Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="customerContract"  method="post" >

                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>


                    <div id="tabs">
                        <ul class="">
                            <li id="vehicles1"><a href="#fullTruck"><span>Vehicles</span></a></li>
                            <li id="trailers1"><a href="#weightBreak"><span>Trailers </span></a></li>
                        </ul>


                        <div id="fullTruck">


                            <div id="routeFullTruck">

                                <c:if test= "${vehicles != null}">
                                    <table width="990px;" align="center" border="0" id="table" >
                                        <thead>
                                            <tr id="tableDesingTD" height="30">
                                                <th align="center"><h3>S.No</h3></th>
                                        <th align="center"><h3>Vehicle RegNo</h3></th>
                                        <th align="center"><h3>Agreed Date</h3></th>
                                        <th align="center"><h3>MRF</h3></th>
                                        <th align="center"><h3>Model</h3></th>
                                        <th align="center"><h3>Remarks</h3></th>
                                        <th align="center"><h3>Active Status</h3></th>
                                        </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${vehicles}" var="vehi">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=classText%>"> <%=index++%></td>


                                            <input type="hidden" name="contractId" id="contractId<%=index%>" value=" <c:out value="${vehi.contractId}"/>"/>


                                            <td class="<%=classText%>"   > <c:out value="${vehi.vehicleRegNo}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${vehi.agreedDate}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${vehi.mfrName}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${vehi.modelName}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${vehi.remarks}"/></td>

                                            <td class="<%=classText%>"   >
                                                <c:if test="${vehi.activeInd == 'Y'}">
                                                    Active
                                                </c:if>
                                                <c:if test="${vehi.activeInd == 'N'}">
                                                    In   Active
                                                </c:if>
                                            </td>

                                            </tr>

                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <br/>

                                    <script>


                                        var container = "";
                                        function addNewRowFullTruck() {

                                            $("#AddNewRowFullTruck").hide();
                                            var iCnt = 1;
                                            var rowCnt = 1;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container = $($("#routeFullTruck")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            $(container).last().after('<table id="mainTableFullTruck" ><tr></td>\
                            <table  class="contenthead" id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr><td>Sno</td>\n\
                            <td>VehicleRegno</td>\n\
                            <td>AgreedDate</td>\n\
                            <td>MFR</td>\n\
                            <td>Model</td>\n\
                            <td>Remarks</td>\n\
                            <tr><td>' + iCnt + '</td>\n\
                            <td><input type="hidden" name="contractId" id="contractId' + iCnt + '" value="" /><input type="text" name="vehicleRegNo" id="vehicleRegNo' + iCnt + '" value="" /></td>\n\
                            <input type="text" name="agreedDate" id="agreedDate' + iCnt + '" value="" /></td>\n\
                           <td><input type="text" name="mfr" id="mfr' + iCnt + '" value="" /></td>\n\
                                     <td><input type="text" name="model" id="model' + iCnt + '" value="" /></td>\n\
                            <td><input type="text" name="remarks" id="remarks' + iCnt + '" value="" /></td>\n\
                             </tr>\n\
                            </table>\n\
                            <table border="1" width="73%"><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxFullTruck(iCnt);
                                            callDestinationAjaxFullTruck(iCnt);

                                        }

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        function addRow(val) {
                                            //alert(val);
                                            var loadCnt = val;
                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><input type="text" name="vehicleRegNo" id="vehicleRegNo' + loadCnt + '"></td><td><input type="text"  class="datepicker" name="agreedDate" id="agreedDate' + loadCnt + '"/></td><td><input type="text" name="remarks" id="remarks' + loadCnt + '" value="" /></td></tr>');
                                            loadCnt++;
                                        }
                                        function deleteRow(val) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                    </script>



                                </c:if>
                            </div>

                            <a  class="nexttab" href="#"><input type="button" class="btn btn-info" value="Next" name="Next" /></a>


                        </div>




                        <div id="weightBreak">



                            <div id="routeWeightBreak">

                                <c:if test="${trailerList != null}">
                                    <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                        <thead>
                                            <tr>
                                                <th align="center"><h3>S.No</h3></th>
                                        <th align="center"><h3>Trailer Type</h3></th>
                                        <th align="center"><h3>Trailer No</h3></th>
                                        <th align="center"><h3>Remarks</h3></th>
                                        <th align="center"><h3>Active Status</h3></th>


                                        </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${trailerList}" var="trailer">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=classText1%>"  ><%=index1++%></td>
                                            <input type="hidden" name="contractId" id="contractId<%=index1%>" value=" <c:out value="${trailer.contractId}"/>"/> 

                                            <td class="<%=classText1%>"   ><c:out value="${trailer.trailerType}"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${trailer.trailerNo}"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${trailer.trailerRemarks}"/></td>

                                            <td class="<%=classText1%>"   >
                                                <c:if test="${trailer.activeInd == 'Y'}">
                                                    Active
                                                </c:if>
                                                <c:if test="${trailer.activeInd == 'N'}">
                                                    In   Active
                                                </c:if>
                                            </td>
                                        </c:forEach>
                                        </tbody>
                                    </table>  

                                    <script>
                                        var container1 = "";
                                        function addNewRowWeightBreak(val) {
                                            $("#AddNewRowWeightBreak").hide();

                                            var iCntWeightBreak = 1;
                                            var rowCntWeightBreak = 1;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container1 = $($("#routeWeightBreak")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            $(container1).last().after('<table id="mainTableWeightBreak" ><tr></td>\
                                            <table  class="contenthead" id="routeDetailsWeightBreak' + iCntWeightBreak + '"  border="1">\n\
                                            <tr><td>Sno</td>\n\
                                            <td>TrailerType</td>\n\
                                            <td>TrailerNo</td>\n\
                                            <td align="center">Remarks</td></tr>\n\
                                           <tr><td>' + iCntWeightBreak + '</td>\n\
                                            <td><input type="hidden" name="contractDedicateId" id="contractDedicateId' + iCntWeightBreak + '" value="" /><select type="text" name="trailerType1" id="trailerType1' + iCntWeightBreak + '"><option value="0">--Select--</option><c:if test="${vehicleList  != null}"><c:forEach items="${vehicleList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                            <td><input type="text" name="trailerNo" id="trailerNo' + iCntWeightBreak + '" value="" />\n\
                                            <input type="text" name="trailerRemarks" id="trailerRemarks' + iCntWeightBreak + '" value="" />\n\
                                        </tr>\n\
                                          <table border="1" width="73%"><tr>\n\
                                            <td><input class="button" type="button" name="addRouteDetailsWeightBreak" id="addRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Add" onclick="addRowWeightBreak(' + iCntWeightBreak + ')" />\n\
                                            <input class="button" type="button" name="removeRouteDetailsWeightBreak" id="removeRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Remove"  onclick="deleteRowWeightBreak(' + iCntWeightBreak + ')" /></td>\n\
                                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxWeightBreak(iCntWeightBreak);
                                            callDestinationAjaxWeightBreak(iCntWeightBreak);

                                        }

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        function addRowWeightBreak(val) {
                                            //   alert(val);
                                            var loadCnt = val;
                                            var routeInfoSize = $('#routeDetailsWeightBreak' + loadCnt + ' tr').size();
                                            // var route=routeInfoSize+1;
                                            //   alert(route)
                                            var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                            var routeInfoDetails = "routeDetailsWeightBreak" + loadCnt;
                                            $('#routeDetailsWeightBreak' + val + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select type="text" name="trailerType" id="trailerType' + loadCnt + '"><option value="0">--Select--</option><c:if test="${vehicleList != null}"><c:forEach items="${vehicleList}" var="vehType"><option value="<c:out value="${vehType.trailerId}"/>"><c:out value="${vehType.seatCapacity}"/></option></c:forEach></c:if></select></td><td><input type="text" name="trailerNo" id="trailerNo' + loadCnt + '" value=""/></td><td><input type="text" name="remarks" id="remarks' + loadCnt + '" value="" /></td></tr>');
                                            loadCnt++;
                                        }
                                        function deleteRowWeightBreak(val) {
                                            // alert(val)
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                            var routeInfoDetails = "routeDetailsWeightBreak" + loadCnt;
                                            if ($('#routeDetailsWeightBreak' + loadCnt + ' tr').size() > 2) {
                                                $('#routeDetailsWeightBreak' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                            </script>


                                </c:if>
                                <a  class="pretab" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>
                            </div>
                            <br/>
                            <br/>
                        </div>


                        <!--                    <a  class="pretab" href="#"><input type="button" class="button" value="Save" name="Save" onclick="submitPage();"/></a>      -->
                    </div>  

                    <!--                     <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>-->

                    <script>
                        function submitPage() {
                            document.customerContract.action = "/throttle/saveEditCustomerContractDetails.do";
                            document.customerContract.method = "post";
                            document.customerContract.submit();
                        }

                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });

                    </script>



                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
