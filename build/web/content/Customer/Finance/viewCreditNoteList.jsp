<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

//            $(document).ready(function() {
//                $("#showTable").hide();
//
//            });
        </script>
        <script type="text/javascript">
            function submitPage(value) {
                document.accountReceivable.action = "/throttle/handleEcreditNoteList.do";
                document.accountReceivable.submit();
            }

 function submitBillCourierDetails(invoiceId,creditNoteId) {
//        document.billDetails.action = "" +  + "";
//        document.billDetails.submit();
        window.open('/throttle/viewCreditNotePrint.do?invoiceId=' + invoiceId+'&creditNoteId=' + creditNoteId, 'PopupPage', 'height = 600, width = 900, scrollbars = yes, resizable = yes');

    }function eInvoiceGenerate() {

        var checkIn = document.getElementsByName("selectedIndex");
        var chec = 0;
        for (var i = 0; i < checkIn.length; i++) {
            if (checkIn[i].checked) {
                chec = chec + 1;
            }
        }
        if (chec > 0) {
            document.billDetails.action = '/throttle/invoiceApproval.do';
            document.billDetails.submit();
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            checkIn[0].focus();
        }

    }
        </script>

        
        <!--</script>-->
    <style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="E-Invoice.label.CreditDetails"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="E-Invoice.label.E-CreditDetails"  text="default text"/></a></li>
            <li class="active"><spring:message code="E-Invoice.label.E-CreditDetails"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        
        
        

    <body onload="setValue();
            sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <table  class="table table-info mb30 table-hover" >
                <thead>
                <tr id="index">
                    <th colspan="6" ><spring:message code="finance.label.E-CreditDetails"  text="default text"/></th></tr>
                </thead>
                            
                                <!--<table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >-->
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="finance.label.CustomerName"  text="default text"/></td>
                                        <td height="30" >
                                            <c:if test="${customerList != null}">
                                                  <select  name="customerId" id="customerId"  class="form-control"  style="width:280px;height:40px;">
                                                      <option value="0">--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                                      <c:forEach items="${customerList}" var="cus">
                                                          <option value="<c:out value="${cus.customerId}"/>"><c:out value="${cus.customerName}"/></option>
                                                      </c:forEach>
                                                  </select>
                                            </c:if>
                                        </td>
                                        <!--<td colspan="2"></td>-->

<!--                                    </tr>
                                    <tr>-->
                                        <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                        <td height="30"><input autocomplete="off" name="fromDate" id="fromDate" type="text" class="datepicker" value="" style="width:280px;height:40px;" ></td>
                                        <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                        <td height="30"><input autocomplete="off" name="toDate" id="toDate" type="text" class="datepicker" value="" style="width:280px;height:40px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"> </td>
                                    </tr>
                                </table>
                            
                    </td>
                </tr>
            </table>

            <br>
            <br>

            <br>
            <br>
            <br>
            <div id="showTable">
                <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                    <thead>
                        <tr height="45"  >
                            <th><h3 align="center"><spring:message code="finance.label.Sno"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CustomerName"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CreditNoteNo"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Date"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CreditAmount"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Reason"  text="default text"/></h3></th>
                            <th><h3 align="center">view</h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Action"  text="default text"/></h3></th>
                            <th height="30" ><div ><spring:message code="settings.label.Details" text="Select All"/>&nbsp;<input type="checkbox" id="selectAll" onclick="selectAllMenus()" /> </div></th>
<!--                            <th><h3 align="center"><spring:message code="finance.label.Delete"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Modify"  text="default text"/></h3></th>-->
                        </tr>
                    </thead>
                    <script>
                                function selectAllMenus() {
                                    var selectAll = document.getElementById("selectAll").checked;
                                    var functionStatus = document.getElementsByName("selectedIndex");
                                    if (selectAll == true) {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = true;
                                        }
                                    } else {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = false;
                                        }
                                    }
                                }
                            </script>
                    <tbody>
                        <% int index=1;%>
                         <c:if test="${creditNoteSearchList != null}">
                        <c:forEach items="${creditNoteSearchList}" var="cn">

                        <tr height="30"  >

                            <td align="center"><%=index%></td>
                            <td align="left"><c:out value="${cn.customerName}"/></td>
                            <td align="left"><c:out value="${cn.invoiceCode}"/></td>
                            <td align="left"><c:out value="${cn.invoiceDate}"/></td>
                            <td align="left"><c:out value="${cn.creditNoteAmount}"/></td>
                            <td align="left"><c:out value="${cn.remarks}"/></td>
                            <td  height="30"><a href="#" onclick="submitBillCourierDetails('<c:out value="${cn.invoiceId}"/>','<c:out value="${cn.creditNoteId}"/>')">view</a></td>
                            <td  height="30"><input type="button" class="btn btn-success" value="E-Invoice" name="einvoice" onClick="eInvoiceGenerate();"></td>
                            <td  height="30"><input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" value="<c:out value="${cn.invoiceId}"/>" ></td>
                        </tr>
                        <% index++;%>
                        </c:forEach>
                    </c:if>
                        
                    </tbody>
                </table>
                           <center>
                            <input type="button" class="btn btn-success" value="E-Invoice Generate" name="einvoices" onClick="eInvoiceGenerate();">
                        </center>
                <center>
                    <td><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="finance.label.Print"  text="default text"/>" onclick="submitPage(this.name);"></td>
                    <td><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);"></td>
                </center>
                <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>