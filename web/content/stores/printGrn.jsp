<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.purchase.business.PurchaseTO" %>
<%@ page import="java.util.*" %>
<title>Purchase Order</title>




</head>
<body>


<script>



function print(ind)
{
var DocumentContainer = document.getElementById(ind);
var WindowObject = window.open('', "TrackHistoryData",
"width=740,height=400,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
WindowObject.document.writeln(DocumentContainer.innerHTML);
WindowObject.document.close();
WindowObject.focus();
WindowObject.print();
//WindowObject.close();
}


</script>
<body>
<form name="mpr"  method="post" >
<br>


<%
int index=0;
ArrayList supplyList = new ArrayList();
supplyList = (ArrayList) request.getAttribute("supplyList");
PurchaseTO purch = new PurchaseTO();
int listSize=10;
int itemNameLimit = 30;
int mfrCodeLimit = 10;

PurchaseTO headContent = new PurchaseTO();
System.out.println("supplyList size in jsp="+supplyList.size());
headContent = (PurchaseTO)supplyList.get(0);
String[] address = headContent.getAddress();
String itemName = "";
String mfrCode = "";


for(int i=0; i<supplyList.size(); i=i+listSize) {
%>



<div id="print<%= i %>" />
<style type="text/css">
.header {font-family:Arial;
font-size:15px;
color:#000000;
text-align:center;
padding-top:10px;
font-weight:bold;
}
.border {border:1px;
border-color:#000000;
border-style:solid;
}
.text1 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
.text2 {
font-family:Arial, Helvetica, sans-serif;
font-size:16px;
color:#000000;
}
.text3 {
font-family:Arial, Helvetica, sans-serif;
font-size:18px;
color:#000000;
}
.text4 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
</style>


<table align="center" width="400" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000000; ">
<tr>
<td colspan="2" style="border:1px solid #000000; " align="center"><strong>GRN Statement</strong></td>
</tr>
<tr>
<td width="350" style="border:1px solid #000000; "><strong>M/S&nbsp;&nbsp; <%= headContent.getVendorName() %>  </strong> </td>
<td width="350" style="border:1px solid #000000; ">


    <table width="350"  align="left" border="0" cellpadding="0" cellspacing="0" class="border" >
    <tr>
    <td colspan="2" class="text2" style="padding-left:8px;" ><strong>To</strong></td>
    </tr>
    <tr>
    <td   class="text3" align="center">
    <strong> PARVEEN AUTOMOBILES [P] LTD.</strong>
    </td>
    </tr>
    <tr>
    <td align="center" class="text2" style="padding-left:8px; ">OLD NO-25 NEW NO-3/67,PALAVAYAL,<br>
    SOTHUPAKKAM ROAD,VADAKARAI(POST),
    REDHILLS,<br>
    Chennai -600 052.<br>
    PHONE-26320982/83/084/987.<br>
    
    </td>
    </tr>
    </table>

</td>
</tr>
<tr>
<td colspan="2">
    <table width="600"  height="30" border="0" align="center" cellpadding="0" cellspacing="0" class="border">

    <tr>
    <td  style="padding-left:10px;" class="text1"> GRN NO: &nbsp <strong><span class="text3" >  <%= headContent.getSupplyId() %></span></strong> </td>
    <td  style="padding-left:10px;" class="text1"> DATE  : &nbsp <strong><span class="text3" >  <%= headContent.getCreatedDate() %></span></strong> </td>
    <td  style="padding-left:10px;" class="text1"> PO NO : &nbsp <strong><span class="text3" >  <%= headContent.getPoId() %></span></strong> </td>
    <td  style="padding-left:10px;" class="text1"> DC NO : &nbsp <strong><span class="text3" >  <%= headContent.getDcNumber() %></span></strong> </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td colspan="2">


    <table width="350"  align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">
    <tr>
    <td   height="28" valign="top" class="border" style="padding-left:3px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></td>
    <td  valign="top" style="padding-left:5px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>FOLIO NO</strong></td>
    <td   valign="top" style="padding-left:6px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>NOMENCLATURE</strong></td>
    <td  valign="top" style="padding-left:3px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></td>
    <td   valign="top" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>DISC</strong></td>
    <td   valign="top" style="padding-left:8px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>TAX</strong></td>
    <td   valign="top" style="padding-left:8px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UNIT PRICE</strong></td>
    <td   valign="top" style="padding-left:8px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>MRP</strong></td>
    <td   valign="top"  style="padding-left:1px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>QTY</strong></td>
    <td   valign="top"  style="padding-left:8px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>AMOUNT</strong></td>
    </tr>
<%
float total=0.0f;
index = 0;
    for(int j=i; ( j < (listSize+i) ) && (j<supplyList.size() ) ; j++){
    purch = new PurchaseTO();
    purch = (PurchaseTO)supplyList.get(j);
    itemName  = purch.getItemName();
    if(purch.getItemName().length() > itemNameLimit ){
    itemName = itemName.substring(0,itemNameLimit-1);
    }
    System.out.println("j=="+j);
    %>

    <tr>
    <td valign="top" HEIGHT="20" class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   > <%= j+1 %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= purch.getPaplCode() %> &nbsp; </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= purch.getUomName() %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getDiscount() %> </td>
    <td valign="top" HEIGHT="20"  class="text1" style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getTax() %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getUnitPrice() %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getMrp() %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getAcceptedQty() %> </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getItemAmount() %> </td>
    </tr>
    <%
                    String amt1 = purch.getItemAmount();
                                    float amt = Float.parseFloat(amt1);
                                    total += amt;
     index++;

}  %>
    <% while(index <= listSize ){ %>
    <tr>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  >&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">&nbsp;   </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left">&nbsp; </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">&nbsp; </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">&nbsp;  </td>
    </tr>
    <% index++; } %>
     <tr>
                    <td valign="top" HEIGHT="20"  class="text1" width="25"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="90"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>
                    <td valign="top" HEIGHT="20"  class="text1" width="400"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right">Total </td>
                    <td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <%=total%> </td>
                </tr>
    </table>



</td>
</tr>
<tr>
<td colspan="2">



    <table width="622"  height="10" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="border:1px solid #000000;">
    <Tr>
    <td colspan="2"   class="text1" height="10" style="padding-left:5px; " ><%= headContent.getRemarks() %></td>
    </Tr>
    <Tr>
    <td width="350"  class="text1" height="30" >&nbsp;  </td>
    <td width="350"  class="text1" height="30" >&nbsp;</td>
    </Tr>

    <Tr>
    <td  class="text1"   align="left"><strong>Stores Manager</strong></td>
    <td  class="text1" style="padding-left:5px;"   align="left"><strong>Purchase Department</strong></td>
    <td  class="text1"  align="right"><strong>Authorised Signatory</strong></td>
    </Tr>
    </table>

</td>
</tr>
</table>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;
    </center>
<br>
<br>
<br>
    <% } %>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

