/*---------------------------------------------------------------------------
 * FPUtil.java
 * Mar 19, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/
package ets.arch.util;

/****************************************************************************
 *
 * Modification Log:
 * Ver 		Date                               Author                Change
 * ----------------------------------------------------------------------------
 * 1.0		Mar 19, 2008                       ES			     Created
 *
 ****************************************************************************/
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import ets.arch.exception.FPResourceManager;
import ets.domain.util.FPLogUtils;

/**
*
* FPUtil: Utility class for loading property file contents.
* @author Satheeshkumar P
* @version 1.0 19 Mar 2008
* @since   event Portal Iteration 0
*/
public class FPUtil {

	/**
	 * PROPERTIES - constant to hold the property value
	 */
	private static final String PROPERTIES = "parveen";

	//constant to hold the property file path's value
	private ResourceBundle globalProperties;

	//variable to hold the value of the FPUtil.
	private static FPUtil instance;

	public FPUtil() {

	}


	/**
	 * @return  FPUtil
	 */
	public static FPUtil getInstance() {
		if(instance == null) {
			instance = new FPUtil();
			instance.loadFPProperties();
		}
		return instance;
	}

	/**
	 * <p>Loads the Global Properties</p>
	 * This method loads the properties for the given path into the properties variable.
	 *
	 */
	private void loadFPProperties() {
		try{
			globalProperties = ResourceBundle.getBundle(PROPERTIES);

		}catch (MissingResourceException mre){
			//////System.out.println("loadFPProperties");
			throw mre;

		}

	}


	/**
	 * <p>Gets property value from the file</p>
	 * @param key This is the key using which an entry in the
	 * Properties file is read.
	 * @return value The actual value to be used by the calling method.
	 */

	public String getProperty(String key) {
		if(globalProperties != null) {
			return globalProperties.getString(key);
		}else{
			return null;
		}
	}
}
