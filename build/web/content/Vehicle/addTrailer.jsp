

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">


            function CalWarrantyDate(selDate){
               
                var seleDate = selDate.split('-');
                var dd = seleDate[0];
                var mm = seleDate[1];
                var yyyy = seleDate[2];
               
                var today = new Date();
                var dd1 = today.getDate();
                var mm1 = today.getMonth()+1; //January is 0!
                var yyyy1 = today.getFullYear();
                
                //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

                var selecedDate = new Date (yyyy, mm, dd);
                var currentDate = new Date (yyyy1, mm1, dd1);
                var Days = Math.floor((selecedDate.getTime() - currentDate.getTime())/(1000*60*60*24));
                //alert("DAYS--"+Days);
                document.addVehicle.war_period.value= Days;

            }


            function submitPage(value) {

                var owner = document.getElementById("asset").value;
                document.getElementById("asset").value = owner;
                if(value == "save"){
//                    document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
                  //  document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
                    document.addVehicle.action = '/throttle/addTrailer.do';
                    document.addVehicle.submit();
                }

                if(validate()=='fail'){
                    return;
                }else if(document.addVehicle.regNo.value == ''  ){
                    alert('Please Enter Vehicle Registration Number');
                    document.addVehicle.regNo.focus();
                    return;
                }else if(document.addVehicle.regNoCheck.value == 'exists'  ){
                    alert('Vehicle RegNo already Exists');
                    return;
//                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
//                    return;
                }else if(document.addVehicle.dateOfSale.value == ''){
                    alert("Please select Registration Date");
                    document.addVehicle.dateOfSale.focus();
                    return;
                }else if(isSelect(document.addVehicle.mfrId,'Manufacturer')){
                    return;
                }else if(isSelect(document.addVehicle.modelId,'Vehicle Model')){
                    return;
                }else if(textValidation(document.addVehicle.engineNo,'Engine Number')) {
                    return;
                }else if(textValidation(document.addVehicle.chassisNo,'Chassis Number')) {
                    return;    
                }else if(textValidation(document.addVehicle.axles,'Axles Nos')) {
                    return;   
                }else if(textValidation(document.addVehicle.seatCapacity,'seatCapacity')) {
                    return;  
                }else if(textValidation(document.addVehicle.warrantyDate,'WarrantyDate')) {
                    return;    
                }else if(numberValidation(document.addVehicle.war_period,'Warranty Period')){
                    return;
                }else if(isSelect(document.addVehicle.classId,'Vehicle Class')){
                    return;
                }else if(numberValidation(document.addVehicle.kmReading,'Km Reading')){
                    return;
                }else if(numberValidation(document.addVehicle.dailyKm,'Daily Km Reading')){
                    return;
                }else if(numberValidation(document.addVehicle.dailyHm,'Daily Hm reading')){
                    return;
                }else if(isSelect(document.addVehicle.opId,'Operation Point')){
                    return;
//                }else if(textValidation(document.addVehicle.nextFCDate,'Next FC Date')) {
//                    return;
                }
                
            }


            var httpRequest;
            function getVehicleDetails() {
                if(document.addVehicle.regNo.value != ''){
                    var url='/throttle/checkTrailerExists.do?regno='+document.addVehicle.trailerNo.value;

                    if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("POST", url, true);
                    httpRequest.onreadystatechange = function() {go1(); } ;
                    httpRequest.send(null);
                }
            }


            function go1() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        var temp=response.split('-');
                        if(response!=""){
                            alert('Vehicle Already Exists');
                            document.getElementById("Status").innerHTML=httpRequest.responseText.valueOf()+" Already Exists";
                            document.addVehicle.regNo.focus();
                            document.addVehicle.regNo.select();
                            document.addVehicle.regNoCheck.value='exists';
                        }else
                        {
                            document.addVehicle.regNoCheck.value='Notexists';
                            document.getElementById("Status").innerHTML="";
                        }
                    }
                }
            }

        </script>

        <script>
            var rowCount=1;
            var sno=0;
            var httpRequest;
            var httpReq;
            var styl = "";
            <%   ArrayList tyreItems = (ArrayList) request.getAttribute("tyreItemList");

                        ArrayList positionList = (ArrayList) request.getAttribute("positionList");
                        VehicleTO veh = new VehicleTO();
                        MrsTO mrs = new MrsTO();
            %>

                function addRow(){
                    if(parseInt(rowCount) %2==0)
                    {
                        styl="text2";
                    }else{
                        styl="text1";
                    }
                    sno++;
                    var tab = document.getElementById("addTyres");
                    var newrow = tab.insertRow(rowCount);

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' > "+sno+"</td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;


                    // TyreIds
                    var cell = newrow.insertCell(1);
                    var cell0 = "<td class='text1' height='25' ><font color='red'>*</font>"+
                        "<select name='itemIds' class='form-control'    > <option value='0'>-Select-</option>"+
            <%

                        Iterator itr = tyreItems.iterator();

                        while (itr.hasNext()) {
                            veh = new VehicleTO();
                            veh = (VehicleTO) itr.next();
            %>
                    "<option value=<%= veh.getItemId()%> >  <%= veh.getItemName()%> </option>"+
            <%  }%>

                        +"</select> </td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;


                    // Positions
                    cell = newrow.insertCell(2);
                    var cell0 = "<td class='text1' height='25' >"+
                        "<font color='red'>*</font><select name='positionIds'   class='form-control' > <option value='0'>-Select-</option>"+
            <% ;
                        itr = positionList.iterator();
                        while (itr.hasNext()) {
                            mrs = new MrsTO();
                            mrs = (MrsTO) itr.next();
            %>
                    "<option value=<%= mrs.getPosId()%> >  <%= mrs.getPosName()%> </option>"+
            <% }

            %>

                        +"</select> </td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;



                    cell = newrow.insertCell(3);
                    var cell1 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreIds' onChange='checkTyreId("+rowCount+")' class='form-control' >";

                    cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
                    cell.setAttribute("className","text1");
                    cell.innerHTML = cell1;

                    cell = newrow.insertCell(4);
                    var cell2 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreDate' class='datepicker' >";

                    cell.setAttribute("className","text1");
                    cell.innerHTML = cell2;

                    rowCount++;

                    $(document).ready(function() {

                        $( "#datepicker" ).datepicker({
                            showOn: "button",
                            buttonImage: "calendar.gif",
                            buttonImageOnly: true

                        });



                    });

                    $(function() {
                        //	alert("cv");
                        $( ".datepicker" ).datepicker({

                            /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                            changeMonth: true,changeYear: true
                        });

                    });

                }

                var httpReq;
                var temp = "";
                function ajaxData()
                {
                    //alert(document.addVehicle.mfrId.value);
                    var url = "/throttle/getModels1.do?mfrId="+document.addVehicle.mfrId.value;
                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() { processAjax(); } ;
                    httpReq.send(null);
                }


                function processAjax()
                {
                    if (httpReq.readyState == 4)
                    {
                        if(httpReq.status == 200)
                        {
                            temp = httpReq.responseText.valueOf();
                            setOptions(temp,document.addVehicle.modelId);
                        }
                        else
                        {
                            alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                        }
                    }
                }





                function validate()
                {
                    var tyreIds = document.getElementsByName("tyreIds");
                    var tyreDate = document.getElementsByName("tyreDate");
                    var positionIds = document.getElementsByName("positionIds");
                    var itemIds = document.getElementsByName("itemIds");
                    var tyreExists = document.getElementsByName("tyreExists");
                    var cntr = 0;
                    for(var i=0;i<tyreIds.length;i++){
                        for(var j=0;j<tyreIds.length;j++){
                            if( (tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value==tyreIds[j].value  ) && (i != j) && (tyreIds[i].value != '')   ){
                                cntr++;
                            }
                        }
                        if( parseInt(cntr) > 0){
                            alert("Same Tyre Number should not exists twice");
                            return "fail";
                            break;
                        }
                    }

                    for(var i=0;i<positionIds.length;i++){
                        for(var j=0;j<positionIds.length;j++){
                            if( (positionIds[i].value == positionIds[j].value) && (positionIds[i].value==positionIds[j].value  ) && (i != j) && (positionIds[i].value!='0'  )  ){
                                cntr++;
                            }
                        }
                        if( parseInt(cntr) > 0){
                            alert("Tyre Positions should not be repeated");
                            return "fail";
                            break;
                        }
                    }

                    for(var i=0;i<tyreIds.length;i++){

                        if(itemIds[i].value != '0'){
                            if(positionIds[i].value == '0'){
                                alert('Please Select Position');
                                positionIds[i].focus();
                                return "fail";
                            }else if(tyreIds[i].value == ''){
                                alert('Please Enter Tyre No');
                                tyreIds[i].focus();
                                return "fail";
                            }else if(tyreExists[i].value == 'exists'){
                                alert('Tyre number '+tyreIds[i].value+' is already fitted to another vehicle');
                                tyreIds[i].focus();
                                tyreIds[i].select();
                                return "fail";
                            }
                        }
                    }

                    return "pass";
                }




                var httpRequest;
                function checkTyreId(val)
                {
                    val = val-1;
                    var tyreNo = document.getElementsByName("tyreIds");
                    var tyreExists = document.getElementsByName("tyreExists");
                    if(tyreNo[val].value != ''){
                        var url = '/throttle/checkVehicleTyreNo.do?tyreNo='+tyreNo[val].value;
                        if (window.ActiveXObject)
                        {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        else if (window.XMLHttpRequest)
                        {
                            httpRequest = new XMLHttpRequest();
                        }
                        httpRequest.open("GET", url, true);
                        httpRequest.onreadystatechange = function() { processRequest(tyreNo[val].value,val); } ;
                        httpRequest.send(null);
                    }
                }


                function processRequest(tyreNo,index)
                {
                    if (httpRequest.readyState == 4)
                    {
                        if(httpRequest.status == 200)
                        {
                            var tyreExists = document.getElementsByName("tyreExists");
                            if(httpRequest.responseText.valueOf()!=""){
                                document.getElementById("userNameStatus").innerHTML="Tyre No "+tyreNo+" Already Exists in "+httpRequest.responseText.valueOf();
                                tyreExists[index].value='exists';
                            }else {
                                document.getElementById("userNameStatus").innerHTML="";
                                tyreExists[index].value='notExists';
                            }
                        }
                        else
                        {
                            alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                        }
                    }
                }

                function openAttachedInfo(){
                    document.getElementById("asset").value="2";
                    document.getElementById("attachInfo").style.display="block";
                }
                function closeAttachedInfo(){
                    document.getElementById("asset").value="1";
                    document.getElementById("attachInfo").style.display="none";
                }
        </script>




    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="operations.label.Trailer"  text="Trailer"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.Trailer"  text="Trailer"/></a></li>
          <li class="active"><spring:message code="operations.label.Trailer"  text="Trailer"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
   
    <body onLoad="addRow()">
        <form name="addVehicle" method="post"  >
            


            <%@ include file="/content/common/message.jsp" %>

            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                <div align="center" id="Status">&nbsp;&nbsp;</div>
            </font>

            <input type="hidden" name="regNoCheck" value='exists' >
            <table border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td colspan="4" height="30" class="contenthead"><div class="contenthead">Add Trailer</div></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Trailer Number</td>
                    <td class="text1" height="30"><input maxlength='13'  name="trailerNo" type="text" class="form-control" value="" onChange="getVehicleDetails();" ></td>
                   
               
                    <td class="text2" height="30"><font color="red">*</font>Type</td>
                                <td class="text2" height="30">
                                    <select name="axles" class="form-control">
                                        <option value="1">20 ft</option>
                                        <option value="3">40 ft</option>
                                        <option value="4">Skeleton</option>
                                        <option value="5">Flatbed</option>
                                    </select>
                                </td>
                    
                </tr>
                 <tr>
                    <td class="text1" height="30"><font color="red">*</font>Warranty Expiry Date</td>
                    <td class="text1" height="30"><input name="warrantyDate" type="text" class="datepicker" value="" size="20" onchange="CalWarrantyDate(this.value)" ></td>
                    <td class="text1" height="30">Warranty (days)</td>
                    <td class="text1" height="30"><input name="war_period" type="text"  class="form-control" value="" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Trailer Cost As On Date</td>
                    <td class="text2"  ><input type="text" name="vehicleCost" class="form-control" maxlength="15" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                    <td class="text2" height="30">Depreciation(%)</td>
                    <td class="text2"  ><input type="text" name="vehicleDepreciation" class="form-control" maxlength="15" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                </tr>
                
                
               
                
               <tr>
<!--                    <td class="text1" height="30"> <font color="red">*</font>GPS Tracking System</td>
                    <td class="text1"  >
                        <select class="form-control" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)" style="width:125px;">
                            <option value='Yes' selected>Yes</option>
                            <option value='No'>No</option>
                        </select>
                        <script type="text/javascript">
                            function openGpsInfo(val) {
                                if (val == 'Yes') {
                                    document.getElementById("trckingId").style.display = "block";
                                } else {
                                    document.getElementById("trckingId").style.display = "none";
                                }
                            }
                        </script>
                    </td>-->
                    <td class="text1" height="30"> Trailer Ownership</td>
                    <td class="text1" height="30">
                        <input type="radio" name="vAsset" id="own" value="Own" checked onclick="closeAttachedInfo();"> Own
                        <input type="radio" name="vAsset" id="attach" value="Attach" onclick="openAttachedInfo();"> Lease
                        <input type="hidden" name="asset" id="asset" class="form-control" value="1">
                    </td>
                </tr>
               <tr>
                    <td colspan="2">
                        <div id="trckingId" style="display: none;">
                            <table width="100%" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td class="text1" height="30"><font color="red">*</font>GPS System Id</td>
                                    <td class="text1" align="center">
                                        <select class="form-control" name="gpsSystemId"  style="width:125px;">
                                            <option value="1" selected>0001</option>
                                            <option value="2" >0002</option>
                                            <option value="3" >0003</option>
                                        </select>
                                    </td>
                                    <td class="text1" height="30">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>

                                </tr>
                            </table>
                        </div>
                    </td>
                    <td colspan="2">
                        <div id="attachInfo" style="display: none;">
                            <table width="100%" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td class="text1" height="30">Vendor Name</td>
                                    <td class="text1">
                                        <select class="form-control" name="leasingCustId"  style="width:125px;">
                                            <option value="" checked>--Select--</option>
                                            <c:if test = "${leasingCustList != null}" >
                                                <c:forEach items="${leasingCustList}" var="LCList">
                                                    <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td class="text1" height="30">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>

                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <div align="center" style="font-family:Arial, Helvetica, sans-serif; color:#f5533d; font-size:12px; font-weight:bold;" id="userNameStatus">&nbsp;&nbsp;</div>
            <p align="center"><span class="contenthead" ><strong>&nbsp;&nbsp;OEM Details&nbsp;&nbsp;</strong></span></p>
            <table border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="addTyres">
                <tr >
                    <td width="20" class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Item</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Position</div> </td>
                    <td class="contenthead" height="30" ><div class="contenthead">Tyre Number</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Date</div></td>
                </tr>
            </table>
            <br> <br>
            <table border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td>
                    <td class="text2" height="30">Remarks</td>
                    <td class="text2" height="30">
                        <textarea class="form-control" name="description"></textarea></td>
                    </td>
                </tr>
                <tr>
                    <td>
                    <td class="text2" height="30">Is Free Services Done</td>
                    <td class="text2" height="30"><input type="checkbox" value="Y" name="selectedindex"></td>
                    </td>
                </tr>
            </table>
            <br>
            <center>
                <input type="button" class="button" value="Add" name="save" onClick="submitPage(this.name)">
                &emsp;<input type="reset" class="button" value="Clear">
                <input type="button" class="button" value="Add Row" name="save" onClick="addRow()">
            </center>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>
