

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        var httpReq;
        var temp = "";
        function ajaxData()
        {
            
            var url = "/throttle/getModels1.do?mfrId="+document.serviceReport.mfrId.value;    
            if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() { processAjax(); } ;
                    httpReq.send(null);
                }
                
                function processAjax()
                {
                    if (httpReq.readyState == 4)
                        {
                            if(httpReq.status == 200)
                                {
                                    temp = httpReq.responseText.valueOf();                 
                                    setOptions(temp,document.serviceReport.modelId);        
                                    if('<%= request.getAttribute("modelId")%>' != 'null' ){
                                        document.serviceReport.modelId.value = <%= request.getAttribute("modelId")%>;
                                    }
                                }
                                else
                                    {
                                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                    }
                                }
                            }
                            
                            
                            
                            function submitPage(){
                                var chek=validation();
                                if(chek=='true'){                              
                                    document.serviceReport.action = '/throttle/serEffReport.do';
                                    document.serviceReport.submit();
                                }
                            }
                            function validation(){
                                if(document.serviceReport.companyId.value==0){
                                    alert("Please Select Data for Company Name");
                                    document.serviceReport.companyId.focus();
                                    return 'false';
                                }           
                                else  if(textValidation(document.serviceReport.fromDate,'From Date')){
                                    return 'false';
                                }
                                else  if(textValidation(document.serviceReport.toDate,'TO Date')){
                                    return'false';
                                }
                                return 'true';
                            }
                            
                            function setValues(){
                               
                                if('<%=request.getAttribute("companyId")%>'!='null'){
                                    document.serviceReport.companyId.value='<%=request.getAttribute("companyId")%>';                                   
                                    document.serviceReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
                                    document.serviceReport.toDate.value='<%=request.getAttribute("toDate")%>';
                                }
                                
                                if('<%=request.getAttribute("mfrId")%>'!='null'){
                                    document.serviceReport.mfrId.value='<%=request.getAttribute("mfrId")%>';
                                }
                                if('<%=request.getAttribute("jobCardId")%>'!='null'){                                   
                                    document.serviceReport.jobCardId.value='<%=request.getAttribute("jobCardId")%>';
                                }
                                if('<%=request.getAttribute("modelId")%>'!='null'){
                                    ajaxData();
                                    document.serviceReport.modelId.value='<%=request.getAttribute("modelId")%>';
                                }
                                if('<%=request.getAttribute("woId")%>'!='null'){                                   
                                    document.serviceReport.woId.value='<%=request.getAttribute("woId")%>';
                                }
                                if('<%=request.getAttribute("regNo")%>'!='null'){                                  
                                    document.serviceReport.regNo.value='<%=request.getAttribute("regNo")%>';
                                }
                               
                                
                            }
                           function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }    
    </script>
    <body onload="setValues();getVehicleNos();">
        <form name="serviceReport" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>

            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Material Issue Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
            <td height="30">&nbsp;&nbsp;MFR</td>
            <td height="30"><select name="mfrId" onchange="ajaxData();" class="form-control">
                    <option value="0">-select-</option>
                    <c:if test = "${MfrLists != null}" >
                        <c:forEach items="${MfrLists}" var="mfr">
                            <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                        </c:forEach>
                    </c:if>
            </select></td>
            <td height="30">&nbsp;&nbsp;Model</td>
            <td height="30"><select name="modelId" class="form-control" style="width:125px">
                    <option value="0">-select-</option>
                    <c:if test = "${ModelLists != null}" >
                        <c:forEach items="${ModelLists}" var="model">
                            <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                        </c:forEach>
                    </c:if>
            </select></td>


            <td height="30"><font color="red">*</font>Location</td>
            <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                    <option value="0">-select-</option>
                    <c:if test = "${LocationLists != null}" >
                        <c:forEach items="${LocationLists}" var="company">
                            <c:choose>
                                <c:when test="${company.companyTypeId==1012}" >

                                    <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                    </c:if>

            </select></td>
        </tr>
        <tr>
             <td width="114" height="30">&nbsp;&nbsp;Vehicle No</td>
            <td width="172" height="30"><input name="regNo" id="regno" type="text"  class="form-control" value=""></td>
            <td width="111" height="30">&nbsp;&nbsp;Wo No</td>
            <td width="151" height="30"><input name="woId" type="text" class="form-control" value="" size="20"></td>
            <td>&nbsp;&nbsp;JC No</td>
            <td height="30"><input name="jobCardId" type="text" class="form-control" value="" size="20"></td>
        </tr>
        <tr>
            <td width="80" height="30"><font color="red">*</font>From Date</td>
            <td width="192"><input name="fromDate" type="text" class="form-control" value="" size="20">
            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
            <td width="148"><font color="red">*</font>To Date</td>
            <td width="192" height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
            <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceReport.toDate,'dd-mm-yyyy',this)"/></span></td>
            <td><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
        <input type="hidden" value="" name="reqfor"></td>
        </tr>

    </table>
    </div></div>
    </td>
    </tr>
    </table>
            <br>
           
            <c:if test = "${serviceEffList != null}" >
                <table width="886" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td width="51" class="contentsub">Sno</td>
                        <td width="102" height="30" class="contentsub">Manufacturer</td>
                        <td width="82" class="contentsub">Model</td>
                        <td width="104" height="30" class="contentsub">WO No </td>
                        <td width="82" class="contentsub">Wo Created Date</td>
                        <td width="82" height="30" class="contentsub">Scheduled Date</td>                        
                        <td width="78" height="30" class="contentsub">Vehicle Received Date</td>  
                        <td width="82" height="30" class="contentsub">JobCardId</td>
                        <blockquote>&nbsp;</blockquote>                        
                        <td width="120" height="30" class="contentsub">PCD</td>
                        <td width="120" height="30" class="contentsub">ACD</td>
                        <td width="80" height="30" class="contentsub">Days</td>
                        <td width="80" height="30" class="contentsub">Schedule Adherence</td>
                        
                    </tr>
                    <c:set var="adherence" value="0" />
                    
                    <% int index = 0;%> 
                    <c:forEach items="${serviceEffList}" var="service"> 
                    
                    
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                        <tr>                             
                            <td class="<%=classText%>" height="30"><%=index+1%></td>
                            <td class="<%=classText%>" height="30"><c:out value="${service.mfrName}"/></td>
                            <td class="<%=classText%>"><c:out value="${service.modelName}" /></td> 
                                    <td class="<%=classText%>" height="30"><c:out value="${service.woId}"/></td>
                                    <td class="<%=classText%>" height="30"><c:out value="${service.createdDate}"/></td>
                                    <td class="<%=classText%>" height="30"><c:out value="${service.schedule}"/></td> 
                                    <td class="<%=classText%>" height="30"><c:out value="${service.arrival}"/></td> 
                            <td class="<%=classText%>" height="30"><c:out value="${service.jobCardId}"/></td>                                                                                                                                   
                            <td class="<%=classText%>" height="30"><c:out value="${service.pcd}"/></td>                            
                            <td class="<%=classText%>" height="30"><c:out value="${service.acd}"/></td>                            
                            <td class="<%=classText%>" height="30"><c:out value="${service.day}"/></td>                                                                                    
                           
                         <c:set var="adherence" value="${(adherence)}" />                              
                            <td class="<%=classText%>" height="30">
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${adherence}" /> 
 </td>                  
                        </tr>  
                        <% index++;%>
                    </c:forEach>
                  
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
</html>

