




<%--This is the chart processor code which is used to customise the chart genearted by ceowlf tags below --%>


<%--From here its Displaying Fluidity Reports-- --%>


    <%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@ page import="ets.domain.report.business.ReportTO" %>
    <%@ page import="java.util.*" %>
    <script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    

    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });
    </script>
    
    
    <script language="javascript">
        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }
    </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsmis.label.JobCardReport" text="default text"/> 2</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsmis.label.FMSMIS" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsmis.label.JobCardReport" text="default text"/> 2</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onLoad="setValues()" >

                <form name="chart" method="post" >

                    <!--<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        
                        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                        </tr>
                        <tr id="exp_table" >
                        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                            <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                                <li style="background:#76b3f1">Job Card Report 2</li>
                        </ul>
                        <div id="first">-->
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="fmsmis.label.JobCardReport"  text="default text"/> 2</th></tr></thead>
                        <tr>
                            <td><spring:message code="fmsmis.label.FromDate"  text="default text"/></td>
                            <td>
                                <input type="text" class="datepicker form-control" style="width:260px;height:40px;" autocomplete="off"  name="fromDate" value="" >
                                <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.fromDate,'dd-mm-yyyy',this)"/>-->
                            </td>

                            <td><spring:message code="fmsmis.label.ToDate"  text="default text"/></td>
                            <td>
                                <input type="text" class="datepicker form-control" style="width:260px;height:40px;"  autocomplete="off" readonly name="toDate" value="" >
                                <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.toDate,'dd-mm-yyyy',this)"/>-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4"><input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="fmsmis.label.Search"  text="default text"/>" onClick="searchSubmit();" ></td>
                        </tr>
                    </table>
                    <!--    </div></div>
                        </td>
                        </tr>
                        </table>-->
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <%
                if(request.getAttribute("serviceSummary") != null){
    
                ArrayList serviceSummary = (ArrayList) request.getAttribute("serviceSummary");
                System.out.println("serviceSummary"+serviceSummary.size());

                int siz = serviceSummary.size(); 
                String[][] arrData = new String[siz][5];

                int i1 = 0;
   
                Iterator itr = serviceSummary.iterator();
                ReportTO reportTO = new ReportTO();
                while(itr.hasNext()){
                reportTO = (ReportTO) itr.next();
                arrData[i1][0] = reportTO.getCompanyName();
                arrData[i1][4] = reportTO.getBillingCompleted();
                arrData[i1][3] = reportTO.getCompleted();
                arrData[i1][1] = reportTO.getNotPlanned();
                arrData[i1][2] = reportTO.getPlanned();
                System.out.println("arrData----"+arrData[i1][0]+"-"+arrData[i1][1]+"-"+arrData[i1][2]+"-"+arrData[i1][3]+"-"+arrData[i1][3]);
                i1++;
                }


 
                String  strXML = "<graph caption='Management Report' XAxisName='CompanyName' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";

                //Initialize <categories> element - necessary to generate a stacked chart
                String strCategories = "<categories>";

                //Initiate <dataset> elements
                String strDataProdD = "<dataset seriesName='BillingCompleted' color='8BBA00'>";
                String strDataProdC = "<dataset seriesName='Completed' color='F6BD0F'>";
                String strDataProdA = "<dataset seriesName='Created' color='FF0000'>";
                String strDataProdB = "<dataset seriesName='Planned' color='AFD8F8'>";

                //Iterate through the data	
                for(int i=0;i<arrData.length;i++){
                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + arrData[i][0] + "' />";
                //Add <set value='...' /> to both the datasets
                strDataProdA += "<set value='" + arrData[i][1] + "' />";
                strDataProdB += "<set value='" + arrData[i][2] + "' />";
                strDataProdC += "<set value='" + arrData[i][3] + "' />";
                strDataProdD += "<set value='" + arrData[i][4] + "' />";
                }

                //Close <categories> element
                strCategories += "</categories>";

                //Close <dataset> elements
                strDataProdA += "</dataset>";
                strDataProdB +="</dataset>";
                strDataProdC +="</dataset>";
                strDataProdD +="</dataset>";

                //Assemble the entire XML now
                strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";

                %>           

                <table class="table table-info mb30 table-hover repcontain" >
                    <thead>
                        <c:if test = "${serviceSummary != null}" >
                            <tr>
                                <th ><spring:message code="fmsmis.label.Sno"  text="default text"/></th>
                                <th ><spring:message code="fmsmis.label.CompanyName"  text="default text"/></th>
                                <th ><spring:message code="fmsmis.label.Created"  text="default text"/></th>
                                <th ><spring:message code="fmsmis.label.Planned"  text="default text"/></th>
                                <th ><spring:message code="fmsmis.label.Completed"  text="default text"/></th>
                                <th ><spring:message code="fmsmis.label.BillingCompleted"  text="default text"/></th>
                            </tr>
                        </thead>
                        <%
                                                        int index = 1 ;
                        %>
                        <c:forEach items="${serviceSummary}" var="serviceSummary">
                            <%

                                String classText = "";

                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr>
                                <td ><%=index %></td>
                                <td ><c:out value="${serviceSummary.companyName}"/></td>
                                <td ><c:out value="${serviceSummary.notPlanned}"/></td>
                                <td ><c:out value="${serviceSummary.planned}"/></td>
                                <td ><c:out value="${serviceSummary.completed}"/></td>
                                <td ><c:out value="${serviceSummary.billingCompleted}"/></td>
                            </tr>
                            <%
                                        index++;
                            %>

                        </c:forEach> 
                    </c:if>
                </table>
                <p>
                <table align="center" style="margin-left:10px;" >
                    <tr>
                        <td >
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true"> 
                                <jsp:param name="chartSWF" value="/throttle/swf/FCF_StackedColumn2D.swf" /> 
                                <jsp:param name="strURL" value="" /> 
                                <jsp:param name="strXML" value="<%=strXML %>" /> 
                                <jsp:param name="chartId" value="productSales" /> 
                                <jsp:param name="chartWidth" value="800" /> 
                                <jsp:param name="chartHeight" value="450" />
                                <jsp:param name="debugMode" value="false" /> 	
                                <jsp:param name="registerWithJS" value="false" /> 
                            </jsp:include>
                        </td>
                    </tr>

                </table>
                </p>
                <%
                }
                %>          

            </body>
            <script>
                function searchSubmit()
                {
                    document.chart.action = "/throttle/handleServiceGraphData.do"
                    document.chart.submit();
                }

                function setValues() {
                    if ('<%= request.getAttribute("fromDate") %>' != 'null') {
                        document.chart.fromDate.value = '<%= request.getAttribute("fromDate") %>';
                    }
                    if ('<%= request.getAttribute("toDate") %>' != 'null') {
                        document.chart.toDate.value = '<%= request.getAttribute("toDate") %>';
                    }
                }

            </script> 
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


