// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseDAO.java
package ets.domain.purchase.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.purchase.business.PurchaseTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.io.PrintStream;
import java.util.*;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class PurchaseDAO extends SqlMapClientDaoSupport {

    public PurchaseDAO() {
    }

    public int doInsertMprItems(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        java.sql.ResultSet rs = null;
        try {
            map.put("mprId", Integer.valueOf(purchaseTO.getMprId()));
            for (int i = 0; i < purchaseTO.getItemIds().length; i++) {
                map.put("itemId", purchaseTO.getItemIds()[i]);
                map.put("reqQty", purchaseTO.getReqQtys()[i]);
                System.out.println((new StringBuilder()).append("purchaseTO.getReqQtys()[i]=").append(purchaseTO.getReqQtys()[i]).toString());
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertMprItems", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertMprItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doInsertMprItems", sqlException);
        }
        return status;
    }

    public int doGenerateMpr(ArrayList vendorItems, int purchaseType) {
        Map map = new HashMap();
        int mprId = 0;
        int status = 0;
        Iterator itr = vendorItems.iterator();
        PurchaseTO vend = new PurchaseTO();
        try {
            if (itr.hasNext()) {
                vend = (PurchaseTO) itr.next();
                map.put("purchaseType", Integer.valueOf(purchaseType));
                map.put("vendorId", Integer.valueOf(vend.getVendorId()));
                map.put("activeInd", "Y");
                map.put("userId", Integer.valueOf(vend.getUserId()));
                map.put("companyId", vend.getCompanyId());
                map.put("jobCardId", vend.getJobCardId());
                map.put("desc", vend.getDesc());
                mprId = ((Integer) getSqlMapClientTemplate().insert("purchase.generateMpr", map)).intValue();
                System.out.println("mprId111111"+mprId);
            }
            itr = vendorItems.iterator();
            map.put("mprId", Integer.valueOf(mprId));
            while (itr.hasNext()) {
                vend = new PurchaseTO();
                vend = (PurchaseTO) itr.next();
                map.put("itemId", Integer.valueOf(vend.getItemId()));
                map.put("reqQty", vend.getReqQty());
                map.put("buyPrice", vend.getBuyPrice());
                map.put("unitPrice", vend.getUnitPrice());
                map.put("discount", vend.getDiscount());
                map.put("vat", vend.getVat());
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertMprItems", map)).intValue();
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGenerateMpr Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doGenerateMpr", sqlException);
        }
        return mprId;
    }

    public ArrayList getVendorList(int vendorTypeId) {
        Map map = new HashMap();
        map.put("vendorType", Integer.valueOf(vendorTypeId));
        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVendorList", map);
            System.out.println((new StringBuilder()).append("vendorList size=").append(vendorList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorList;
    }
    public ArrayList purchaseReceipt() {
        Map map = new HashMap();        
        ArrayList purchaseReceiptList = new ArrayList();
        try {
            purchaseReceiptList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.purchaseReceipt", map);
            System.out.println((new StringBuilder()).append("purchaseReceipt size=").append(purchaseReceiptList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("purchaseReceipt Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "purchaseReceipt", "purchaseReceipt", sqlException);
        }
        return purchaseReceiptList;
    }

    public ArrayList getVendorItemList(String vendorId) {
        Map map = new HashMap();
        map.put("vendorId", vendorId);
        String rs = "";
        ArrayList vendorItemList = new ArrayList();
        try {
            vendorItemList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVendorItemList", map);
            //////System.out.println("vendorItemList size:" + vendorItemList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorItemList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getVendorItemList", sqlException);
        }
        return vendorItemList;
    }

    public ArrayList getMprStatusList(PurchaseTO purchaseTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mprList = new ArrayList();
        ArrayList mprStatusList = new ArrayList();
        PurchaseTO purchTO = new PurchaseTO();
        String temp[] = null;
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            mprList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getMprList", map);
            System.out.println((new StringBuilder()).append("vendorList size=").append(mprList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMprStatusList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getMprStatusList", sqlException);
        }
        return mprList;
    }

    public ArrayList getMprPoList(PurchaseTO purchaseTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mprList = new ArrayList();
        ArrayList mprStatusList = new ArrayList();
        PurchaseTO purchTO = new PurchaseTO();
        String temp[] = null;
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            mprList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getMprPoList", map);
            System.out.println((new StringBuilder()).append("MPR size=").append(mprList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMprPoList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getMprPoList", sqlException);
        }
        return mprList;
    }

    public int doCancelPo(int poId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("poId", Integer.valueOf(poId));
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.cancelPo", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doCancelPo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doCancelPo", sqlException);
        }
        return status;
    }

    public ArrayList getMprDetail(int mprId) {
        Map map = new HashMap();
        ArrayList mprList = new ArrayList();
        map.put("mprId", Integer.valueOf(mprId));
        try {
            mprList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getMprDetail", map);
            System.out.println((new StringBuilder()).append("mprList Detail ").append(mprList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMprDetail Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getMprDetail", sqlException);
        }
        return mprList;
    }

    public ArrayList getMprApproveQty(int mprId) {
        Map map = new HashMap();
        ArrayList mprList = new ArrayList();
        map.put("mprId", Integer.valueOf(mprId));
        try {
            mprList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getMprApprovedQty", map);
            System.out.println((new StringBuilder()).append("mprList Detail ").append(mprList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMprApproveQty Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getMprApproveQty", sqlException);
        }
        return mprList;
    }

    public int doMprApprove(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("status", purchaseTO.getStatus());
            map.put("mprId", Integer.valueOf(purchaseTO.getMprId()));
            map.put("desc", purchaseTO.getDesc());
            map.put("userId", Integer.valueOf(purchaseTO.getUserId()));
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertMprApproveStatus", map)).intValue();
            for (int i = 0; i < purchaseTO.getItemIds().length; i++) {
                map.put("itemId", purchaseTO.getItemIds()[i]);
                map.put("approvedQty", purchaseTO.getApprovedQtys()[i]);
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertMprApproveQty", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doMprApprove Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doMprApprove", sqlException);
        }
        return status;
    }

    public int processVendorItem(String vendorId, String itemId, String unitPrice,String vatId, String buyPrice, String discount, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("vendorId", vendorId);
            map.put("itemId", itemId);
            map.put("buyPrice", buyPrice);
            map.put("vatId", vatId);
            map.put("unitPrice", unitPrice);
            map.put("discount", discount);
            map.put("userId", userId);
            System.out.println("map:....,,,"+map);
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateVendorItemPrice", map)).intValue();
            if (status == 0) {
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertVendorItemPrice", map)).intValue();
            }


        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doMprApprove Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doMprApprove", sqlException);
        }
        return status;
    }

    public int doGeneratePo(PurchaseTO purchaseTo,String igst,String cgst,String sgst,
            String paymentDays,String shipVia,String 
            warranty,String freightTerms,String remarks) {
        Map map = new HashMap();
        int poId = 0;
        int status = 0;
        try {
            map.put("mprId", Integer.valueOf(purchaseTo.getMprId()));
            map.put("vendorId", Integer.valueOf(purchaseTo.getVendorId()));
            map.put("userId", Integer.valueOf(purchaseTo.getUserId()));
            map.put("igst",igst);
            map.put("cgst",cgst);
            map.put("sgst",sgst);
            map.put("paymentDays",paymentDays);
            map.put("shipVia",shipVia);
            map.put("warranty",warranty);
            map.put("freightTerms",freightTerms);
            map.put("remarks",remarks);
            poId = ((Integer) getSqlMapClientTemplate().insert("purchase.generatePO", map)).intValue();
            System.out.println((new StringBuilder()).append("generated PO is=").append(poId).toString());
            map.put("poId", Integer.valueOf(poId));
            for (int i = 0; i < purchaseTo.getItemIds().length; i++) {
                map.put("itemId", purchaseTo.getItemIds()[i]);
                map.put("buyPrice", purchaseTo.getBuyPrices()[i]);
                map.put("unitPrice", purchaseTo.getUnitPrices()[i]);
                map.put("discount", purchaseTo.getDiscounts()[i]);
                map.put("vat", purchaseTo.getTaxs()[i]);
                map.put("reqQty", purchaseTo.getApprovedQtys()[i]);
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertPoDetails", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGeneratePo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doGeneratePo", sqlException);
        }
        return poId;
    }
//    bala

    public String getVehicleNo(int poId) {
        Map map = new HashMap();
        String vehicleNo = null;
        try {
            map.put("poId", poId);
            //////System.out.println("B4 Vehicle");
            vehicleNo = ((String) getSqlMapClientTemplate().queryForObject("purchase.getVehicleNo", map));
            //////System.out.println("A4 Vehicle");
            //////System.out.println("vehicleno:" + vehicleNo);
            if (vehicleNo != null) {
                String[] temp;
                String delimiter = "-";
                temp = vehicleNo.split(delimiter);
                //////System.out.println("Vehicle:" + temp[0]);
                vehicleNo = temp[0];
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVehicleNo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "getVehicleNo", sqlException);
        }
        return vehicleNo;
    }

    public String getUsageType(String vehicleNo) {

        Map map = new HashMap();
        String usageType = null;
        try {
            map.put("vehicleNo", vehicleNo);
            //////System.out.println("usage type find");
            usageType = ((String) getSqlMapClientTemplate().queryForObject("purchase.getUsageType", map));
            //////System.out.println("usagetype is:" + usageType);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUsageType Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "getUsageType", sqlException);
        }
        return usageType;
    }
//    bala ends

    public ArrayList doGetPoDetail(int poId, int companyId) {
        Map map = new HashMap();
        ArrayList poDetail = new ArrayList();
        ArrayList poDetail1 = new ArrayList();
        float price = 0.0f;
        String prePrice = "";
        try {
            map.put("poId", Integer.valueOf(poId));
            map.put("companyId", Integer.valueOf(companyId));
            poDetail = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getPoDetail", map);
            //////System.out.println("Po Detail Size-->" + poDetail.size());
            //Hari
            Iterator itr23 = poDetail.iterator();
            PurchaseTO purchase = null;
            purchase = new PurchaseTO();
            while (itr23.hasNext()) {
                purchase = (PurchaseTO) itr23.next();
                map.put("vendorId", purchase.getVendorId());
                map.put("itemId", purchase.getItemId());

                /*
                prePrice = (String) getSqlMapClientTemplate().queryForObject("purchase.getLastPricePoDetail", map);
                //////System.out.println("PrePrice value-->"+prePrice);
                if(prePrice!=null)
                {
                String poPrice[] = prePrice.split("-");
                //////System.out.println("Price-->"+poPrice[0]+"Discount-->"+poPrice[1]+"Tax-->"+poPrice[2]);
                //////System.out.println("Have PrePrice");
                purchase.setPrePrice(poPrice[0]);
                purchase.setDiscount(poPrice[1]);
                purchase.setTax(poPrice[2]);
                }
                else
                {
                //////System.out.println("Have No Preprice");
                purchase.setPrePrice("0.00");
                purchase.setDiscount("0.00");
                purchase.setTax("0.00");
                }
                 */

                poDetail1.add(purchase);
            }
            //Hari End
            System.out.println((new StringBuilder()).append("after Excecuting poDetail Qury").append(poDetail.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGetPoDetail Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doGetPoDetail", sqlException);
        }
        return poDetail1;
    }
    public ArrayList processMprDetail(int mprId, int companyId) {
        Map map = new HashMap();
        ArrayList poDetail = new ArrayList();
        try {
            map.put("mprId", Integer.valueOf(mprId));
            map.put("companyId", Integer.valueOf(companyId));
            poDetail = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.processMprDetail", map);
            
            System.out.println((new StringBuilder()).append("after Excecuting poDetail Qury").append(poDetail.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGetPoDetail Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doGetPoDetail", sqlException);
        }
        return poDetail;
    }

    public ArrayList getRequiredItems(PurchaseTO compTO, int startIndex, int endIndex) {
        Map map = new HashMap();
        ArrayList itemList = new ArrayList();
        try {
            map.put("companyId", compTO.getCompanyId());
            map.put("categoryId", compTO.getCategoryId());
            map.put("mfrCode", compTO.getMfrCode());
            map.put("paplCode", compTO.getPaplCode());
            map.put("searchAll", compTO.getSearchAll());
            map.put("startIndex", Integer.valueOf(startIndex));
            map.put("endIndex", Integer.valueOf(endIndex));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getRequiredItems", map);
            System.out.println((new StringBuilder()).append("after getRequiredItems Qury sizwe=").append(itemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRequiredItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            sqlException.printStackTrace();
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "getRequiredItems", sqlException);
        }
        return itemList;
    }

    public ArrayList getVendorItemData(int vendorTypeId) {
        Map map = new HashMap();
        ArrayList itemist = new ArrayList();
        try {
            map.put("vendorTypeId", Integer.valueOf(vendorTypeId));
            itemist = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVendorItemData", map);
            System.out.println((new StringBuilder()).append("vendorItems Size=").append(itemist.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorItemData Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "getVendorItemData", sqlException);
        }
        return itemist;
    }

    public int doGenerateStRequest(PurchaseTO itemTO) {
        Map map = new HashMap();
        int status = 0;
        int requestId = 0;
        try {
            map.put("fromServiceId", itemTO.getFromSpId());
            map.put("toServiceId", itemTO.getToSpId());
            map.put("requiredDate", itemTO.getReqDate());
            map.put("remarks", itemTO.getDesc());
            map.put("userId", Integer.valueOf(itemTO.getUserId()));
            System.out.println((new StringBuilder()).append("itemTO.getFromSpId() = ").append(itemTO.getFromSpId()).toString());
            System.out.println((new StringBuilder()).append("itemTO.getReqDate() = ").append(itemTO.getReqDate()).toString());
            System.out.println((new StringBuilder()).append("itemTO.getReqDate() = ").append(itemTO.getReqDate()).toString());
            System.out.println((new StringBuilder()).append("itemTO.getDesc() = ").append(itemTO.getDesc()).toString());
            System.out.println((new StringBuilder()).append("toServiceId = ").append(itemTO.getToSpId()).toString());
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.generateStkTransferReq", map)).intValue();
            if (status > 0) {
                requestId = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.getReqId", map)).intValue();
                map.put("requestId", Integer.valueOf(requestId));
                System.out.println((new StringBuilder()).append("generated requestId is=").append(requestId).toString());
                for (int i = 0; i < itemTO.getItemIds().length; i++) {
                    map.put("itemId", itemTO.getItemIds()[i]);
                    map.put("quantity", itemTO.getReqQuant()[i]);
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStkTransferDetails", map)).intValue();
                }

            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGenerateStRequest Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doGenerateStRequest", sqlException);
        }
        return status;
    }

    public ArrayList getPOList(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("companyId", purchaseTO.getCompanyId());
        map.put("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
        ArrayList purchaseOrderList = new ArrayList();
        try {
            purchaseOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getPOList", map);
            System.out.println((new StringBuilder()).append("purchaseOrderList size=").append(purchaseOrderList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "purchaseOrderList", sqlException);
        }
        return purchaseOrderList;
    }

    public ArrayList getItemList(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
        System.out.println((new StringBuilder()).append("purchaseTO.getPoId()").append(purchaseTO.getPoId()).toString());
        ArrayList itemList = new ArrayList();
        ArrayList List = new ArrayList();
        String prePrice = "";
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getItemList", map);
            System.out.println((new StringBuilder()).append("itemList size=").append(itemList.size()).toString());
            for (Iterator itr = itemList.iterator(); itr.hasNext(); List.add(purchaseTO)) {
                purchaseTO = (PurchaseTO) itr.next();
                System.out.println((new StringBuilder()).append("itemId").append(purchaseTO.getItemId()).toString());
                map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
                map.put("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
                prePrice = (String) getSqlMapClientTemplate().queryForObject("purchase.getPrePrice", map);
                System.out.println((new StringBuilder()).append("prePrice").append(prePrice).toString());
                if (prePrice == null) {
                    int prePrices = 0;
                    purchaseTO.setPrePrice(String.valueOf(prePrices));
                } else {
                    purchaseTO.setPrePrice(prePrice);
                }
                purchaseTO.setMfrCode(purchaseTO.getMfrCode());
                purchaseTO.setPaplCode(purchaseTO.getPaplCode());
                purchaseTO.setItemId(purchaseTO.getItemId());
                purchaseTO.setItemName(purchaseTO.getItemName());
                purchaseTO.setUomName(purchaseTO.getUomName());
                purchaseTO.setApprovedQty(purchaseTO.getApprovedQty());
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "itemList", sqlException);
        }
        return List;
    }

    public int processUpdateInVoiceDiscount(PurchaseTO purchaseTO, int userId) {
        Map map = new HashMap();
        int stat = 0;
        try {

            map.put("supplyId", purchaseTO.getSupplyId());
            map.put("discount", purchaseTO.getDiscount());

            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.processUpdateInVoiceDiscount", map)).intValue();

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("processUpdateInVoiceDiscount Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "processUpdateInVoiceDiscount", "processUpdateInVoiceDiscount", sqlException);
        }
        return stat;
    }


    public int insertInVoice(PurchaseTO purchaseTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
        map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
        map.put("dcNumber", purchaseTO.getDcNumber());
        map.put("inVoiceNumber", purchaseTO.getInVoiceNumber());
        map.put("inVoiceDate", purchaseTO.getInVoiceDate());
        map.put("inVoiceAmount", purchaseTO.getInVoiceAmount());
        map.put("freight", purchaseTO.getFreight());
        map.put("dc", Float.parseFloat("0.00"));
        map.put("purchaseType", Integer.valueOf(purchaseTO.getPurchaseTypeId()));
        map.put("inventoryStatus", purchaseTO.getInventoryStatus());
        map.put("remarks", purchaseTO.getRemarks());
        int supplyId = 0;
        String supplyDetails = "";
        String[] temp = null;
        ArrayList itemList = new ArrayList();
        try {
            //check supplyId exist for this po
            //if exist getsupplyId and update details else get supplyId
            //////System.out.println("map 1= " + map);
            //supplyDetails = (String) getSqlMapClientTemplate().queryForObject("purchase.lastDCDetails", map);
            supplyDetails = null;
            if (supplyDetails != null) {
                temp = supplyDetails.split("-");
                supplyId = Integer.parseInt(temp[0]);
                map.put("supplyId", supplyId);
                map.put("Newfreight", Float.parseFloat(purchaseTO.getFreight()) + Float.parseFloat(temp[2]));
                map.put("NewdcNumber", temp[1] + "," + purchaseTO.getDcNumber());
                map.put("NewinVoiceAmount", Float.parseFloat(purchaseTO.getInVoiceAmount()) + Float.parseFloat(temp[3]));
                System.out.println("map 2= " + map);
                status = (Integer) getSqlMapClientTemplate().update("purchase.updateNewDc", map);
            } else {
                System.out.println("map 3= " + map);
                supplyId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertInVoice", map)).intValue();
                System.out.println((new StringBuilder()).append("supplyId =  ").append(supplyId).toString());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertInVoice Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "insertInVoice", sqlException);
        }
        return supplyId;
    }

    public int insertFuelFilling(ArrayList List, int userId, PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        String currentReading = "";
        String runReading = "";
        Float run = Float.valueOf(0.0F);
        int category = 0;
        int supplyId = 0;
        int priceId = 0;
        Float total = Float.valueOf(0.0F);
        String priceIdVal = "";
        map.put("userId", Integer.valueOf(userId));
        ArrayList itemList = new ArrayList();
        String balQty = "";
        try {
            Iterator itr = List.iterator();
            PurchaseTO listTO = null;
            //////System.out.println("In Invoice Insert");
            map.put("supplyId", purchaseTO.getSupplyId());
            while (itr.hasNext()) {
                listTO = new PurchaseTO();
                listTO = (PurchaseTO) itr.next();
                map.put("mrp", listTO.getMrp());
                map.put("tax", listTO.getTax());
                map.put("discount", listTO.getDiscount());
                map.put("unitPrice", listTO.getUnitPrice());
                map.put("receivedQty", listTO.getReceivedQty());
                map.put("acceptedQty", listTO.getAcceptedQty());
                map.put("itemId", Integer.valueOf(listTO.getItemId()));
                map.put("userId", Integer.valueOf(userId));
                map.put("companyId", purchaseTO.getCompanyId());
                map.put("itemAmount", listTO.getItemAmount());
                map.put("dcNumber", purchaseTO.getDcNumber());
                map.put("invoiceNumber", purchaseTO.getInVoiceNumber());
                String dat = purchaseTO.getInVoiceDate();
                String dat1[] = dat.split("-");
                String date = (new StringBuilder()).append(dat1[2]).append("-").append(dat1[1]).append("-").append(dat1[0]).toString();
                map.put("inVoiceDate", date);
                map.put("priceType", "INVOICE");
                category = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.getCategory", map)).intValue();
                currentReading = (String) getSqlMapClientTemplate().queryForObject("purchase.currentReading", map);
                if (currentReading == null) {
                    currentReading = "0";
                }
                runReading = (String) getSqlMapClientTemplate().queryForObject("purchase.getCurrentRunReading", map);
                if (runReading == null) {
                    runReading = "0";
                }
                run = Float.valueOf(Float.parseFloat(runReading) + Float.parseFloat(listTO.getAcceptedQty()));
                total = Float.valueOf(Float.parseFloat(listTO.getAcceptedQty()) + Float.parseFloat(currentReading));
                map.put("total", total);
                map.put("run", run);
                map.put("fillingId", "0");
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTankFilledDetails", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTankDetails", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertPrice", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertFuelFilling Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "insertFuelFilling", sqlException);
        }
        return status;
    }

    public int insertItem(ArrayList List, int userId, PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        String currentReading = "";
        String runReading = "";
        Float run = Float.valueOf(0.0F);
        int category = 0;
        int supplyId = 0;
        int priceId = 0;
        Float total = Float.valueOf(0.0F);
        String priceIdVal = "";
        map.put("userId", Integer.valueOf(userId));
        ArrayList itemList = new ArrayList();
        String balQty = "";
        String isExist = "";
        String isStockExist = "";
        try {
            Iterator itr = List.iterator();
            PurchaseTO listTO = null;
            //////System.out.println("In Invoice Insert");
            map.put("supplyId", purchaseTO.getSupplyId());
            for (; itr.hasNext(); System.out.println((new StringBuilder()).append("Stock Balance status ").append(status).toString())) {
                listTO = new PurchaseTO();
                listTO = (PurchaseTO) itr.next();
                map.put("mrp", listTO.getMrp());
                map.put("itemMrp", listTO.getItemMrp());
                map.put("tax", listTO.getTax());
                map.put("discount", listTO.getDiscount());
                map.put("unitPrice", listTO.getUnitPrice());
                map.put("receivedQty", listTO.getReceivedQty());
                map.put("acceptedQty", listTO.getAcceptedQty());
                map.put("itemId", Integer.valueOf(listTO.getItemId()));
                map.put("userId", Integer.valueOf(userId));
                map.put("companyId", purchaseTO.getCompanyId());
                map.put("itemAmount", listTO.getItemAmount());
                map.put("dcNumber", purchaseTO.getDcNumber());
                map.put("invoiceNumber", purchaseTO.getInVoiceNumber());
                String dat = purchaseTO.getInVoiceDate();
                String dat1[] = dat.split("-");
                String date = (new StringBuilder()).append(dat1[2]).append("-").append(dat1[1]).append("-").append(dat1[0]).toString();
                map.put("inVoiceDate", date);
                map.put("priceType", "DC");
                category = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.getCategory", map)).intValue();

                System.out.println("map:"+map);

                //////System.out.println("itemId:"+ listTO.getItemId());
                //////System.out.println("unitPrice:"+ listTO.getUnitPrice());
                //////System.out.println("tax:"+ listTO.getTax());
                //////System.out.println("mrp:"+ listTO.getMrp());
                //////System.out.println("priceType:"+ map.get("priceType"));


                priceIdVal = (String) getSqlMapClientTemplate().queryForObject("purchase.checkPrice", map);
                System.out.println((new StringBuilder()).append("priceIdVal is -->").append(priceId).toString());
                //update item selling price and mrp
                System.out.println("map 2:"+map);
                String vat = (String) getSqlMapClientTemplate().queryForObject("purchase.getItemVat", map);
                map.put("vat", vat);
                System.out.println("map 3:"+map);
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateItemMrp", map)).intValue();
                if (purchaseTO.getInventoryStatus().equalsIgnoreCase("Y")) {
                    if (priceIdVal == null) {
                        System.out.println("Create new price");
                        System.out.println("map 4:"+map);
                        priceId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertItemPrice", map)).intValue();
                        balQty = listTO.getAcceptedQty();
                        map.put("priceId", Integer.valueOf(priceId));
                        map.put("balQty", balQty);
                        System.out.println("map 5a:"+map);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStockBalance", map)).intValue();
                        //////System.out.println("New Stock balance created for price Id" + priceId + "Status is" + status);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertItem", map)).intValue();
                    } else {
                        balQty = listTO.getAcceptedQty();

                        map.put("priceId", priceIdVal);

                        map.put("balQty", balQty);
                        //////System.out.println("update existing price" + priceIdVal);
                        //////System.out.println("update Qty " + balQty);
                        System.out.println("map 5b:"+map);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateItemPrice", map)).intValue();

                        // check stock entry avaliable for this company
                        isStockExist = (String) getSqlMapClientTemplate().queryForObject("purchase.isThisCompStockExist", map);
                        if (isStockExist != null) {
                            System.out.println("map 5c:"+map);
                            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateStockBalance", map)).intValue();
                            //////System.out.println("Update Stock Balance exists-->" + status);
                        } else {
                            System.out.println("map 5d:"+map);
                            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStockBalance", map)).intValue();
                            //////System.out.println("Insert Stock Balance exists-->" + status);
                        }

                        //check this price & item for this supplyId
                        //////System.out.println("B4 Error");
                        isExist = (String) getSqlMapClientTemplate().queryForObject("purchase.isItemExist", map);
                        //////System.out.println("Dc Price Id-->" + isExist);
                        if (isExist != null) {
                            System.out.println("map 5e:"+map);
                            status = getSqlMapClientTemplate().update("purchase.updateDCItem", map);
                            //////System.out.println("Update DC Item Status-->" + status);
                        } else {
                            System.out.println("map 5f:"+map);
                            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertItem", map)).intValue();
                            //////System.out.println("New Dc Item-->" + status);
                        }
                    }



                    if (category == 1032) {
                        insertFuelFilling(List, userId, purchaseTO);
                    }
                } else {
                    if (priceIdVal == null) {
                        //////System.out.println("Create new price in direct purchase");
                        System.out.println("map 5g:"+map);
                        priceId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertItemPrice", map)).intValue();
                        balQty = listTO.getAcceptedQty();
                        map.put("priceId", Integer.valueOf(priceId));
                        map.put("balQty", balQty);
                    } else {
                        //////System.out.println("update existing price in direct purchase");
                        map.put("priceId", priceIdVal);
                    }
                    System.out.println("map 6:"+map);
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertItem", map)).intValue();
                    if (category == 1032) {
                        insertFuelFilling(List, userId, purchaseTO);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "status", sqlException);
        }
        return status;
    }

    public int insertDCItems(ArrayList List, int userId, PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        int supplyId = 0;
        int priceId = 0;
        map.put("userId", Integer.valueOf(userId));
        ArrayList itemList = new ArrayList();
        String balQty = "";
        try {
            Iterator itr = List.iterator();
            PurchaseTO listTO = null;
            //////System.out.println("in DC Items Insert");
            map.put("supplyId", purchaseTO.getSupplyId());
            for (; itr.hasNext(); System.out.println((new StringBuilder()).append("Stock Balance status ").append(status).toString())) {
                listTO = new PurchaseTO();
                listTO = (PurchaseTO) itr.next();
                map.put("mrp", listTO.getMrp());
                map.put("tax", listTO.getTax());
                map.put("discount", listTO.getDiscount());
                map.put("unitPrice", listTO.getUnitPrice());
                map.put("receivedQty", listTO.getReceivedQty());
                map.put("acceptedQty", listTO.getAcceptedQty());
                map.put("itemId", Integer.valueOf(listTO.getItemId()));
                map.put("userId", Integer.valueOf(userId));
                map.put("companyId", purchaseTO.getCompanyId());
                map.put("itemAmount", listTO.getItemAmount());
                map.put("dcNumber", purchaseTO.getDcNumber());
                map.put("invoiceNumber", purchaseTO.getInVoiceNumber());
                map.put("priceType", "DC");
                System.out.println("map:...."+map);
                if (purchaseTO.getInventoryStatus().equalsIgnoreCase("Y")) {
                    //////System.out.println("non direct purchase");
                    priceId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertItemPrice", map)).intValue();
                    balQty = listTO.getAcceptedQty();
                    map.put("priceId", Integer.valueOf(priceId));
                    map.put("balQty", balQty);
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStockBalance", map)).intValue();
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertItem", map)).intValue();
                } else {
                    //////System.out.println("direct purchase");
                    priceId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertItemPrice", map)).intValue();
                    balQty = listTO.getAcceptedQty();
                    map.put("priceId", Integer.valueOf(priceId));
                    map.put("balQty", balQty);
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertItem", map)).intValue();
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "status", sqlException);
        }
        return status;
    }

    public ArrayList getAcceptedItemsQty(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
        System.out.println((new StringBuilder()).append("purchaseTO.getPoId()").append(purchaseTO.getPoId()).toString());
        ArrayList itemList = new ArrayList();
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getItemsSuppliedForPo", map);
            System.out.println((new StringBuilder()).append("itemList size=").append(itemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getAcceptedItemsQty Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getAcceptedItemsQty", sqlException);
        }
        return itemList;
    }

    public int doPoInactive(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
        System.out.println((new StringBuilder()).append("purchaseTO.getPoId()").append(purchaseTO.getPoId()).toString());
        int status = 0;
        String activeStatus = "";
        try {
            activeStatus = (String) getSqlMapClientTemplate().queryForObject("purchase.getPOActiveStatus", map);
            System.out.println((new StringBuilder()).append("activeStatus").append(activeStatus).toString());
            if (activeStatus.equalsIgnoreCase("N")) {
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updatePurchaseOrder", map)).intValue();
            }
            System.out.println((new StringBuilder()).append("pod item updated status=").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doPoInactive Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doPoInactive", sqlException);
        }
        return status;
    }

    public int doPoItemInactive(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
        map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
        System.out.println((new StringBuilder()).append("purchaseTO.getPoId()").append(purchaseTO.getPoId()).toString());
        int status = 0;
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updatePod", map)).intValue();
            System.out.println((new StringBuilder()).append("pod PO updated status=").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doPoItemInactive Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doPoItemInactive", sqlException);
        }
        return status;
    }

    public int doMprInactive(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        map.put("mprId", Integer.valueOf(purchaseTO.getMprId()));
        System.out.println((new StringBuilder()).append("mprId set to Inactive").append(purchaseTO.getMprId()).toString());
        int status = 0;
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.doMprInactive", map)).intValue();
            System.out.println((new StringBuilder()).append("mprId set to Inactiv=").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doMprInactive Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doMprInactive", sqlException);
        }
        return status;
    }

    public ArrayList getGrnDetails(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        ArrayList supply = new ArrayList();
        int status = 0;
        try {
            map.put("grnNo", purchaseTO.getSupplyId());
            map.put("inVoice", purchaseTO.getInVoiceNumber());
            map.put("dcNo", purchaseTO.getDcNumber());
            System.out.println((new StringBuilder()).append("in dao=purchaseTO.getInVoiceNumber()").append(purchaseTO.getInVoiceNumber()).toString());
            System.out.println((new StringBuilder()).append("in dao=purchaseTO.getSupplyId()").append(purchaseTO.getSupplyId()).toString());
            System.out.println((new StringBuilder()).append("in dao=purchaseTO.getDcNumber()").append(purchaseTO.getDcNumber()).toString());
            supply = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getSupplyDetails", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getGrnDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getGrnDetails", sqlException);
        }
        return supply;
    }

    public int doGrnDetailsAlter(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        ArrayList supply = new ArrayList();
        int status = 0;
        try {
            map.put("grnId", purchaseTO.getSupplyId());
            map.put("inVoice", purchaseTO.getInVoiceNumber());
            map.put("inVoiceAmount", purchaseTO.getInVoiceAmount());
            map.put("inVoiceDate", purchaseTO.getInVoiceDate());
            map.put("dcNo", purchaseTO.getDcNumber());
            map.put("freight", purchaseTO.getFreight());
            System.out.println((new StringBuilder()).append("bill amount=").append(purchaseTO.getInVoiceAmount()).toString());
            for (int i = 0; i < purchaseTO.getItemIds().length; i++) {
                map.put("itemId", purchaseTO.getItemIds()[i]);
                map.put("itemAmount", purchaseTO.getItemAmounts()[i]);
                map.put("tax", purchaseTO.getTaxs()[i]);
                map.put("mrp", purchaseTO.getMrps()[i]);
                map.put("unitPrice", purchaseTO.getUnitPrices()[i]);
                map.put("discount", purchaseTO.getDiscounts()[i]);
                map.put("retQty", purchaseTO.getRetQtys()[i]);
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateGrn", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateGrnReturnedStk", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateItemPriceType", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getGrnDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getGrnDetails", sqlException);
        }
        return status;
    }

    public int doInsertTyreSupply(ArrayList tyreList) {
        Map map = new HashMap();
        ArrayList supply = new ArrayList();
        PurchaseTO purchaseTO = new PurchaseTO();
        int status = 0;
        try {
            for (Iterator itr = tyreList.iterator(); itr.hasNext();) {
                purchaseTO = new PurchaseTO();
                purchaseTO = (PurchaseTO) itr.next();
                map.put("companyId", purchaseTO.getCompanyId());
                map.put("supplyId", purchaseTO.getSupplyId());
                map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
                map.put("tyreNo", purchaseTO.getTyreNo());
                map.put("warranty", purchaseTO.getWarranty());
                map.put("isRt", purchaseTO.getIsRt());
                map.put("status", purchaseTO.getStatus());
                System.out.println("tyre supply map:"+map);
                status += Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTyreSupply", map)).intValue();
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertTyreSupply Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doInsertTyreSupply", sqlException);
        }
        return status;
    }

    public ArrayList getVendorMfrCatData(int vendorTypeId) {
        Map map = new HashMap();
        ArrayList itemist = new ArrayList();
        try {
            map.put("vendorTypeId", Integer.valueOf(vendorTypeId));
            itemist = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVendorMfrCat", map);
            System.out.println((new StringBuilder()).append("vendorItems mfr cat Size=").append(itemist.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorMfrCatData Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "getVendorMfrCatData", sqlException);
        }
        return itemist;
    }

    public int insertInVoiceReturnedQty(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        int supplyId = 0;
        String priceId = "";
        map.put("userId", Integer.valueOf(userId));
        ArrayList itemList = new ArrayList();
        String balQty = "";
        try {
            Iterator itr = List.iterator();
            PurchaseTO listTO = null;
            for (; itr.hasNext(); System.out.println((new StringBuilder()).append("Stock Balance status ").append(status).toString())) {
                listTO = new PurchaseTO();
                listTO = (PurchaseTO) itr.next();
                map.put("mrp", listTO.getMrp());
                map.put("acceptedQty", listTO.getReturnedQty());
                map.put("retQty", listTO.getReturnedQty());
                map.put("itemId", Integer.valueOf(listTO.getItemId()));
                map.put("grnId", listTO.getSupplyId());
                map.put("supplyId", listTO.getSupplyId());
                map.put("companyId", listTO.getCompanyId());
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateGrnUnusedStk", map)).intValue();
                if (status == 0) {
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStkBalance", map)).intValue();
                }
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateReturnedQty", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertInVoiceReturnedQty Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "insertInVoiceReturnedQty", sqlException);
        }
        return status;
    }

    public ArrayList getTyreSupplyDetails(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        ArrayList tyreList = new ArrayList();
        try {
            map.put("grnNo", purchaseTO.getSupplyId());
            map.put("inVoice", purchaseTO.getInVoiceNumber());
            map.put("dcNo", purchaseTO.getDcNumber());
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getTyreSupplyDetails", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTyreSupplyDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "getTyreSupplyDetails", sqlException);
        }
        return tyreList;
    }

    public int doInsertTyreDetail(ArrayList tyreList, int userId) {
        Map map = new HashMap();
        ArrayList supply = new ArrayList();
        PurchaseTO purchaseTO = new PurchaseTO();
        int status = 0;
        String vehicleId = "";
        String tyreId = "";
        try {
            for (Iterator itr = tyreList.iterator(); itr.hasNext(); System.out.println((new StringBuilder()).append("status tyredetail").append(status).toString())) {
                purchaseTO = new PurchaseTO();
                purchaseTO = (PurchaseTO) itr.next();
                map.put("companyId", purchaseTO.getCompanyId());
                map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
                map.put("supplyId", purchaseTO.getSupplyId());
                map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
                map.put("tyreNo", purchaseTO.getTyreNo());
                map.put("isRt", purchaseTO.getIsRt());
                map.put("status", purchaseTO.getStatus());
                map.put("positionId", purchaseTO.getPosId());
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("purchase.getVehicleId", map);
                System.out.println((new StringBuilder()).append("vehicleId").append(vehicleId).toString());
                map.put("vehicleId", vehicleId);
                map.put("userId", Integer.valueOf(userId));
                if (vehicleId != null) {
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateVehTyre", map)).intValue();
                    tyreId = (String) getSqlMapClientTemplate().queryForObject("purchase.getTyreId", map);
                    if (tyreId != null) {
                        map.put("tyreId", tyreId);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTyreDetail", map)).intValue();
                    }
                }
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertTyreDetail Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "insertTyreDetail", sqlException);
        }
        return status;
    }

    public ArrayList getVatList() {
        Map map = new HashMap();
        ArrayList MfrList = new ArrayList();
        try {
            MfrList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVatList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "MfrList", sqlException);
        }
        return MfrList;
    }

    public int doInsertVatDetails(PurchaseTO purchTO, int UserId) {
        int mfrId = 0;
        int status = 0;
        Map map = new HashMap();
        map.put("userId", Integer.valueOf(UserId));
        map.put("vat", purchTO.getVat());
        map.put("effectiveDate", purchTO.getEffectiveDate());
        map.put("desc", purchTO.getDesc());
        map.put("activeInd", "Y");
        try {
            mfrId = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertVat", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertVatDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-02", "PurchaseDAO", "doInsertVatDetails", sqlException);
        }
        return status;
    }

    public int doAlterVatDetails(ArrayList List, int UserId) {
        Map map = new HashMap();
        int status = 0;
        try {
            Iterator itr = List.iterator();
            PurchaseTO purchTO = null;
            while (itr.hasNext()) {
                purchTO = (PurchaseTO) itr.next();
                map.put("vatId", purchTO.getVatId());
                map.put("vat", purchTO.getVat());
                map.put("desc", purchTO.getDesc());
                map.put("effectiveDate", purchTO.getEffectiveDate());
                map.put("activeInd", purchTO.getActiveInd());
                map.put("userId", Integer.valueOf(UserId));
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateVat", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-02", "PurchaseDAO", "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int modifyPOAndMprHeader(PurchaseTO purch) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("mprId", Integer.valueOf(purch.getMprId()));
            map.put("poId", Integer.valueOf(purch.getPoId()));
            map.put("poDate", purch.getReqDate());
            map.put("vendorId", Integer.valueOf(purch.getVendorId()));
            map.put("remarks", purch.getRemarks());
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.modifyPoHeader", map)).intValue();
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.modifyMprHeader", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("modifyPOAndMprHeader Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "modifyPOAndMprHeader", sqlException);
        }
        return status;
    }

    public ArrayList getPurchaseorderItems(int po_id) {
        Map map = new HashMap();
        ArrayList poItems = new ArrayList();
        try {
            map.put("poId", Integer.valueOf(po_id));
            poItems = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getPoItems", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "MfrList", sqlException);
        }
        return poItems;
    }

    public int doModifyPo(int po_id, int itemId, String qty) {
        Map map = new HashMap();
        int stat = 0;
        try {
            map.put("poId", Integer.valueOf(po_id));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("qty", qty);
            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.modifyPo", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doModifyPo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "doModifyPo", sqlException);
        }
        return stat;
    }

    public int doModifyMpr(int mprId, int itemId, String qty) {
        Map map = new HashMap();
        int stat = 0;
        try {
            map.put("mprId", Integer.valueOf(mprId));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("qty", qty);
            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.modifyMpr", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doModifyPo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "doModifyPo", sqlException);
        }
        return stat;
    }

    public int doAddPoItems(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("mprId", Integer.valueOf(purchaseTO.getMprId()));
            map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
            map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
            map.put("reqQty", purchaseTO.getReqQty());
            map.put("buyPrice", purchaseTO.getBuyPrice());
            map.put("unitPrice", purchaseTO.getUnitPrice());
            map.put("discount", purchaseTO.getDiscount());
            map.put("vat", purchaseTO.getTax());
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertMprItems", map)).intValue();
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertPoDetails", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doAddPoItems", sqlException);
        }
        return status;
    }

    public void doPoModify(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        String status = "";
        try {
            map.put("mprId", Integer.valueOf(purchaseTO.getMprId()));
            map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
            map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
            map.put("reqQty", purchaseTO.getReqQty());
            status = (String) getSqlMapClientTemplate().queryForObject("purchase.checkPoItem", map);
            if (status == null) {
                doAddPoItems(purchaseTO);
            } else {
                doModifyMpr(purchaseTO.getMprId(), purchaseTO.getItemId(), purchaseTO.getReqQty());
                doModifyPo(purchaseTO.getPoId(), purchaseTO.getItemId(), purchaseTO.getReqQty());
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doAddPoItems", sqlException);
        }
    }

    public int deletePoItems(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("poId", Integer.valueOf(purchaseTO.getPoId()));
            map.put("itemId", Integer.valueOf(purchaseTO.getItemId()));
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.deletePoItems", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "deletePoItems", sqlException);
        }
        return status;
    }

    public int alterGrnHeaderInfo(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceNo", purchaseTO.getInVoiceNumber());
            map.put("dcNumber", purchaseTO.getDcNumber());
            map.put("invoiceDate", purchaseTO.getInVoiceDate());
            map.put("remarks", purchaseTO.getRemarks());
            map.put("supplyId", purchaseTO.getSupplyId());
            status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateGrnHeaderInfo", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doAddPoItems", sqlException);
        }
        return status;
    }

    public ArrayList getVendorPayments(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            map.put("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
            map.put("fromDate", purchaseTO.getFromDate());
            map.put("toDate", purchaseTO.getToDate());
            System.out.println((new StringBuilder()).append("fromDate=").append(purchaseTO.getFromDate()).toString());
            System.out.println((new StringBuilder()).append("toDate=").append(purchaseTO.getToDate()).toString());
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.vendorPayments", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doAddPoItems", sqlException);
        }
        return invoiceList;
    }

    public int addPaymentDetails(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int stat = 0;
        try {
            map.put("vendorId", Integer.valueOf(purchaseTO.getVendorId()));
            map.put("grnId", purchaseTO.getSupplyId());
            map.put("payDate", purchaseTO.getPaymentDate());
            map.put("invoiceAmnt", purchaseTO.getInVoiceAmount());
            map.put("paidAmnt", purchaseTO.getPaidAmnt());
            map.put("remarks", purchaseTO.getRemarks());
            map.put("orderType", purchaseTO.getOrderType());
            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.addVendorPayment", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("addPaymentDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "addPaymentDetails", sqlException);
        }
        return stat;
    }

    public int updatePaymentDetails(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int stat = 0;
        try {
            map.put("paymentId", purchaseTO.getPaymentId());
            map.put("payDate", purchaseTO.getPaymentDate());
            map.put("paidAmnt", purchaseTO.getPaidAmnt());
            map.put("remarks", purchaseTO.getRemarks());
            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateVendorPayment", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("addPaymentDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "addPaymentDetails", sqlException);
        }
        return stat;
    }

    public int doInvoiceDetails(PurchaseTO purchaseTO) {
        Map map = new HashMap();
        int stat = 0;
        int status = 0;
         int formId = 0;
        int voucherNo = 0;
        String temp[] = null;
        String temp2[]=null;
        String date = "";
        temp = purchaseTO.getInVoiceDate().split("-");
        date = (new StringBuilder()).append(temp[2]).append("-").append(temp[1]).append("-").append(temp[0]).toString();
        try {
            map.put("InvoiceNo", purchaseTO.getInVoiceNumber());
            map.put("InvoiceDate", date);
            map.put("InvoiceAmount", purchaseTO.getInVoiceAmount());
            map.put("supplyId", purchaseTO.getSupplyId());
            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.doInvoiceDetails", map)).intValue();
            //pavi
            map.put("paymentReferenceCode", ThrottleConstants.POcode + String.valueOf(purchaseTO.getPoId()));
            map.put("paymentReferenceId", purchaseTO.getPoId());
            map.put("paymentReferenceName", "PO");
            map.put("paymentAmount",purchaseTO.getInVoiceAmount());
            map.put("vendorNameId", purchaseTO.getVendorId());
            String vendorLedgerDetails = (String) getSqlMapClientTemplate().queryForObject("purchase.getVendorLedgerId", map);
             temp2=vendorLedgerDetails.split("~");
            String vendorLedgerId=temp2[0];
            String vendorLedgerCode=temp2[1];
            System.out.println("doInvoiceDetails"+map);
            status = (Integer) getSqlMapClientTemplate().update("purchase.insertPaymentDetails", map);
            if(status>0){
                formId = Integer.parseInt(ThrottleConstants.purchaseFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);
//                    map.put("voucherType", "%PURCHASE%");
//                    String code = "";
//                    String[] temp1;
//                    code = (String) getSqlMapClientTemplate().queryForObject("purchase.getVoucerCode", map);
//                    temp1 = code.split("-");
//                    int codeval2 = Integer.parseInt(temp1[1]);
//                    int codev2 = codeval2 + 1;
//                    String voucherCode = "PURCHASE-" + codev2;
//                    System.out.println("voucherCode = " + voucherCode);

                    String voucherCode = "PURCHASE-" + voucherNo;
                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", date);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", ThrottleConstants.stockInventoryLedgerID);
                    map.put("particularsId", ThrottleConstants.stockInventoryLedgerCode);
                    map.put("amount",  purchaseTO.getInVoiceAmount());
                    map.put("accountsType", "DEBIT");
                    map.put("narration", "PO");
                    map.put("searchCode",ThrottleConstants.POcode + String.valueOf(purchaseTO.getPoId()));
                    map.put("reference", "PO");

                    System.out.println("map1 updateClearnceDate=---------------------> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("purchase.insertAccountEntry", map);
                    System.out.println("status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("ledgerId", vendorLedgerId);
                        map.put("particularsId",vendorLedgerCode);
                        map.put("accountsType", "CREDIT");
                        System.out.println("map2 updateClearnceDate=---------------------> " + map);
                        status = (Integer) getSqlMapClientTemplate().update("purchase.insertAccountEntry", map);
                        System.out.println("status2 = " + status);
                    }
            }
        } catch (Exception sqlException) {
        sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInvoiceDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doInvoiceDetails", sqlException);
        }
        return stat;
    }

    public String getDcPriceValue(int supplyId, int itemId, int compId, float accQty, float reqQty) {
        Map map = new HashMap();
        String stat = "";
        String temp[] = null;
        float stockBal = 0.0F;
        int status = 0;
        try {
            System.out.println((new StringBuilder()).append("accQty").append(accQty).toString());
            map.put("supplyId", Integer.valueOf(supplyId));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("compId", Integer.valueOf(compId));

            stat = (String) getSqlMapClientTemplate().queryForObject("purchase.getDcPriceValue", map);
            //////System.out.println("stat" + stat);
            if (stat != null) {
                temp = stat.split("-");
                //if (Float.parseFloat(temp[1]) > accQty) {
                //  stockBal = Float.parseFloat(temp[1]) - accQty;
                //    temp[1] = String.valueOf(accQty);
                // bala before dc invoice item issued
                if (Float.parseFloat(temp[1]) < accQty) {
                    float stockBal1 = 0.0F;
                    float totQty = 0.0F;
                    //////System.out.println("reqQty in DAO" + reqQty);
                    stockBal1 = reqQty - Float.parseFloat(temp[1]);
                    totQty = accQty - stockBal1;
                    stockBal = 0.0F;
                    temp[1] = String.valueOf(totQty);
                    // bala ends
                    System.out.println((new StringBuilder()).append("in side if").append(temp[1]).toString());
                } else {
                    stockBal = 0.0F;
                }
                System.out.println((new StringBuilder()).append("stockBal").append(stockBal).toString());
                System.out.println((new StringBuilder()).append("temp[1]").append(temp[1]).toString());
                map.put("qty", Float.valueOf(stockBal));
                map.put("priceId", temp[0]);
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateDcstock", map)).intValue();
                ////////System.out.println("Update Dc Stock in getDcPriceValue"+status);
                //Hari Start
                stat = temp[0] + "-" + accQty;
                ////////System.out.println("Correct Stat value-->"+stat);
                //Hari End
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInvoiceDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doInvoiceDetails", sqlException);
        }
        return stat;
    }

    public int doInvoiceItemDetails(PurchaseTO purchaseTO, int userId, float dcStock, int compId, String mrp, String tax, String discount,
            String amount, String unitPrice, int itemId, float accQty, float unusedQty, String dcpriceId) {
        Map map = new HashMap();
        int stat = 0;
        String activeInd = "Y";
        String currentReading = "";
        String runReading = "";
        Float run = Float.valueOf(0.0F);
        int category = 0;
        int supplyId = 0;
        int priceId = 0;
        Float total = Float.valueOf(0.0F);
        String priceIdVal = "";
        int status = 0;
        //Hari
        int checkItem = 0;
        try {
            map.put("userId", Integer.valueOf(userId));
            map.put("mrp", mrp);
            map.put("tax", tax);
            map.put("discount", discount);
            map.put("unitPrice", unitPrice);
            map.put("dcQuantity", Float.valueOf(dcStock));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("userId", Integer.valueOf(userId));
            map.put("companyId", Integer.valueOf(compId));
            map.put("itemAmount", amount);
            map.put("supplyId", purchaseTO.getSupplyId());
            map.put("priceType", "INVOICE");

            category = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.getCategory", map)).intValue();
            if (category != 1032) {
                map.put("dcpriceId", dcpriceId);

                //////System.out.println("Method 2 map:"+map);

                //////System.out.println("itemId:"+ itemId);
                //////System.out.println("unitPrice:"+ unitPrice);
                //////System.out.println("tax:"+ tax);
                //////System.out.println("mrp:"+ mrp);
                //////System.out.println("priceType:"+ map.get("priceType"));



                priceIdVal = (String) getSqlMapClientTemplate().queryForObject("purchase.checkPrice", map);
                //////System.out.println("Item Id-->" + itemId + "companyId" + compId + "Stock Qty-->" + Float.valueOf(dcStock));
                //Hari
                if (priceIdVal != null) {
                    //////System.out.println("Price Id Exists" + priceIdVal);
                    map.put("priceId", priceIdVal);
                    checkItem = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.checkItemExists", map)).intValue();
                    //////System.out.println("CheckItem-->" + checkItem);
                }
                //Hari End
                if (priceIdVal == null) {
                    //////System.out.println("Create new price");
                    priceId = ((Integer) getSqlMapClientTemplate().insert("purchase.insertItemPrice", map)).intValue();
                    map.put("priceId", Integer.valueOf(priceId));
                    map.put("balQty", Float.valueOf(dcStock));
                    status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStockBalance", map)).intValue();
                } else {
                    //////System.out.println("update existing price");
                    priceId = Integer.parseInt(priceIdVal);
                    map.put("priceId", priceIdVal);
                    //Hari
                    //////System.out.println("Price Id-->" + priceIdVal);
                    if (checkItem != 0) {
                        map.put("acceptedQty", Float.valueOf(dcStock));
                        //////System.out.println("Item Exists in Stock Balance-->" + checkItem);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateItemPrice", map)).intValue();
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateStockBalance", map)).intValue();
                    } else {
                        //////System.out.println("Item Not Exists In Stock Balance-->" + checkItem);
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateItemPrice", map)).intValue();
                        map.put("balQty", Float.valueOf(dcStock));
                        status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertStockBalance", map)).intValue();
                        //////System.out.println("New Stock Inserted-->" + status);
                    }
                    //Hari End
                }
//                bala
                map.put("accQty", Float.valueOf(accQty));
                map.put("unusedQty", Float.valueOf(unusedQty));
                //////System.out.println("totQty in DAO:" + Float.valueOf(accQty));
//                bala ends
                stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.updateSupplyDetails", map)).intValue();
            } else {
                String temp[] = null;
                String invoiceDate = "";
                String InVdate = purchaseTO.getInVoiceDate();
                temp = InVdate.split("-");
                invoiceDate = (new StringBuilder()).append(temp[2]).append("-").append(temp[1]).append("-").append(temp[0]).toString();
                category = ((Integer) getSqlMapClientTemplate().queryForObject("purchase.getCategory", map)).intValue();
                currentReading = (String) getSqlMapClientTemplate().queryForObject("purchase.currentReading", map);
                if (currentReading == null) {
                    currentReading = "0";
                }
                runReading = (String) getSqlMapClientTemplate().queryForObject("purchase.getCurrentRunReading", map);
                if (runReading == null) {
                    runReading = "0";
                }
                run = Float.valueOf(Float.parseFloat(runReading) + accQty);
                total = Float.valueOf(accQty + Float.parseFloat(currentReading));
                map.put("total", total);
                map.put("run", run);
                map.put("fillingId", "0");
                map.put("inVoiceDate", invoiceDate);
                map.put("acceptedQty", Float.valueOf(accQty));
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTankFilledDetails", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertTankDetails", map)).intValue();
                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertPrice", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInvoiceItemDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doInvoiceItemDetails", sqlException);
        }
        return stat;
    }
    private final int errorStatus = 4;
    private static final String CLASS = "PurchaseDAO";

    public int doPurchaseDetails(int poId) {
        Map map = new HashMap();
        int stat = 0;
        try {

            map.put("poId", poId);


            stat = Integer.valueOf(getSqlMapClientTemplate().update("purchase.doPurchaseDetails", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doPurchaseDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doPurchaseDetails", sqlException);
        }
        return stat;
    }
    public ArrayList getBarcodePrintItemList(String poId) {
        Map map = new HashMap();
        ArrayList itemList = new ArrayList();
        try {
            map.put("poId", poId);
            System.out.println("poId:"+map);

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getBarcodePrintItemList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doAddPoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "PurchaseDAO", "doAddPoItems", sqlException);
        }
        return itemList;
    }

        public ArrayList getServiceList() {
        Map map = new HashMap();

        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("purchase.getVendorList", map);
            System.out.println((new StringBuilder()).append("vendorList size=").append(vendorList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorList;
    }
}
