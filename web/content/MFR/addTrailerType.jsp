
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
        <script type="text/javascript" src="/throttle/js/jquery-1.10.2.js"></script>
        <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>


        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            /*            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
        </style>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    </head>
    <script>
    function submitpage()
    {
        if (textValidation(document.dept.typeName, "Vehicle Type Name")) {
            return;
        }
        if (textValidation(document.dept.description, "description")) {
            return;
        }
        document.dept.action = "/throttle/handleTrailerTypeAdd.do";
        document.dept.submit();
    }


    </script>

    <script>

        var axletypeId = $("#axletypeId:selected").text();
//        alert(axletypeId);
        document.getElementById("axletypeNmae").value = axletypeId;

    </script>


    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="operations.label.Trailer"  text="Trailer"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.Trailer"  text="Trailer"/></a></li>
          <li class="active"><spring:message code="operations.label.AddTrailer"  text="Trailer"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        <form name="dept"  method="post" >
            <%@ include file="/content/common/message.jsp" %>

      <table  class="table table-info mb30 table-hover">
          <thead>
         <th colspan="6"  height="30"><spring:message code="trailers.label.Trailer" text="default text"/> </th>
          </thead>
                <tr>
                    <td><font color=red>*</font><spring:message code="trailers.label.TrailerTypeName" text="default text"/></td>
                    <td><input name="typeName" type="text" style="width:260px;height:40px;"  class="form-control" value=""></td>
                
                    <td><font color=red>*</font><spring:message code="trailers.label.AxleType" text="default text"/></td>
                    <td   width="20%"><input type="hidden" name="axleTypeName" id="axleTypeName">
                        <select style="width:260px;height:40px;"  class="form-control" name="axleTypeId" id="axleTypeId" >

                            <option value="0">---<spring:message code="trailers.label.Select" text="default text"/>---</option>
                            <c:if test = "${AxleList != null}" >
                                <c:forEach items="${AxleList}" var="Type">
                                    <option value='<c:out value="${Type.vehicleAxleId}" />'><c:out value="${Type.axleTypeName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select></td>
                </tr>
                <tr>
                    <td><font color=red>*</font><spring:message code="trailers.label.TrailerTonnage" text="default text"/></td>
                    <td><input name="tonnage" type="text" style="width:260px;height:40px;"  class="form-control" value=""></td>
                
                    <td><font color=red>*</font><spring:message code="trailers.label.TrailerCapacity(CBM)" text="default text"/></td>
                    <td><input name="capacity" type="text" style="width:260px;height:40px;"  class="form-control" value=""></td>
                </tr>
                <tr colspan="4">
                    <td><font color=red>*</font> <spring:message code="trailers.label.Description" text="default text"/></td>
                    <td><textarea style="width:260px;height:40px;"  class="form-control" name="description"></textarea></td>
                    <td ><input type="button" value="<spring:message code="trailers.label.ADD" text="default text"/>" class="btn btn-success" onclick="submitpage();">
                &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="trailers.label.CLEAR" text="default text"/>"></td>
                    <td></td>
                </tr>
                <td></td>
                <td></td>
                <td></td>
            </table>
                   
                        
                    </td>
<!--            <center>
                <input type="button" value="<spring:message code="trailers.label.ADD" text="default text"/>" class="btn btn-success" onclick="submitpage();">
                &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="trailers.label.CLEAR" text="default text"/>">
            </center>-->
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>