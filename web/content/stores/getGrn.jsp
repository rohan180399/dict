<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script>
    function submitPage(name) {
        if (name == "Search")
        {
            if (validate() == 'notSubmit') {
                return;
            }
            document.purchase.action = "/throttle/getGrnDetails.do"
            document.purchase.submit();
        }
        else
        {
            document.purchase.action = "/throttle/getGrnDetails.do?reqFor=print"
            document.purchase.submit();
        }

    }
    function validate() {
        var counter = 0;
        if (document.purchase.supplyId.value != '') {
            counter++;
            document.purchase.dcNumber.value = '';
            document.purchase.inVoiceNumber.value = '';
        }
        if (document.purchase.dcNumber.value != '') {
            counter++;
            document.purchase.supplyId.value = '';
            document.purchase.inVoiceNumber.value = '';
        }
        if (document.purchase.inVoiceNumber.value != '') {
            counter++;
            document.purchase.supplyId.value = '';
            document.purchase.dcNumber.value = '';
        }
        if (counter == 0) {
            alert("Please Enter Any Data");
            document.purchase.supplyId.focus();
            return 'notSubmit';
        }
        return 'submit'
    }

</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ModifyGRN"  text="ModifyGRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.ModifyGRN"  text="ModifyGRN"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="purchase"  method="post" >                    
                    <!-- message table -->           
                    <%@ include file="/content/common/message.jsp" %>    
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="3"><spring:message code="stores.label.ModifyGRN"  text="ModifyGRN"/></th></tr></thead>
                        <input type="hidden" name="mprId" value=<c:out value="${mpr.mprId}"/> >    
                        <tr>
                            <td ><spring:message code="stores.label.GRNNo"  text="default text"/></td>
                            <td > <input type="text" style="width:260px;height:40px;" autocomplete="off" name="supplyId" value="" class="form-control"> </td>
                            <td>
                            <input type="button" class="btn btn-success" name="Search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPage(this.name);" > &nbsp;
                            <input type="button" class="btn btn-success" name="Print" value="<spring:message code="stores.label.Print"  text="default text"/>" onClick="submitPage(this.name);" > &nbsp;
                        </td>
                        </tr>
                        <input type="hidden" name="dcNumber" value="" class="form-control">
                        <input type="hidden" name="inVoiceNumber" value="" class="form-control">
                    </table>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
