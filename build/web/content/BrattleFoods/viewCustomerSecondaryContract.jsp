<%-- 
    Document   : viewCustomerSecondaryContract
    Created on : Apr 25, 2016, 5:37:49 PM
    Author     : gulshan
--%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>    

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>




<script type="text/javascript">
    function submitPage(value) {
        document.customerContract.action = "/throttle/saveSecondaryCustomerContract.do";
        document.customerContract.submit();
    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body onload="">
        <form name="customerContract"  method="post" >
            <%@ include file="/content/common/path.jsp" %>

            <br>

            <table width="980" align="center" cellpadding="0" cellspacing="0" class="border">

                <tr>
                    <td class="contenthead" colspan="4" >Customer Secondary Contract Master</td>
                </tr>

                <tr>
                    <td class="text1">Customer Name</td>
                    <td class="text1"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="textbox"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="textbox" readonly=""><c:out value="${customerName}"/></td>
                    <td class="text1">Customer Code</td>
                    <td class="text1"><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="textbox" readonly><c:out value="${customerCode}"/></td>
                </tr>
                <tr>
                    <td class="text2">Contract From</td>
                    <td class="text2"><input type="hidden" name="contractFrom" id="contractFrom" value="<c:out value="${contractFrom}"/>" class="datepicker"><c:out value="${contractFrom}"/></td>
                    <td class="text2">Contract To</td>
                    <td class="text2"><input type="hidden" name="contractTo" id="contractTo" value="<c:out value="${contractId}"/>" class="datepicker"><c:out value="${contractTo}"/></td>
                </tr>
                <tr>
                    <td class="text1">Contract No</td>
                    <td class="text1"><input type="hidden" name="contractNo" id="contractNo" value="<c:out value="${contractNo}"/>" class="textbox" ><c:out value="${contractNo}"/></td>
                    <td class="text1">Billing Type</td>
                    <td class="text1"><input type="hidden" name="secondaryBillingTypeId" id="secondaryBillingTypeId" value="<c:out value="${secondaryBillingTypeId}"/>"/>
                        <c:if test="${billingTypeList != null}">
                            <c:forEach items="${billingTypeList}" var="btl">
                                <c:if test="${secondaryBillingTypeId == btl.billingTypeId}">
                                    <c:out value="${btl.billingTypeName}"/>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </td>
                </tr>
            </table>
            <br>

            <div id="tabs" >
                <ul>
                    <c:if test="${secondaryBillingTypeId == 1}">
                        <li id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                        </c:if>
                        <c:if test="${secondaryBillingTypeId == 1}">
                        <li id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                        </c:if>
                        <c:if test="${secondaryBillingTypeId == 1}">
                        <li id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>
                        </c:if>
                        <c:if test="${secondaryBillingTypeId == 2}">
                        <li id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                        </c:if>
                        <c:if test="${secondaryBillingTypeId == 3}">
                        <li id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                        </c:if>
                        <c:if test="${secondaryBillingTypeId == 4}">
                        <li><a href="#fkmRate"><span>Fixed KM - Based - Rate Details </span></a></li>
                        <li><a href="#fkmRoute"><span>Fixed KM - Based - Route Details </span></a></li>
                        </c:if>
                </ul>





                <c:if test="${secondaryBillingTypeId == 1}">
                    <div  style="overflow: auto">
                        <div id="ptp">
                            <div class="inpad">

                                <c:if test = "${ptpBillingList != null}">
                                    <table id="itemsTable" class="sortable">
                                        <thead>
                                            <% int sno1 = 1;%>    
                                            <tr height="110">
                                                <th><h3>S.No</h3></th>
                                        <th><h3>Vehicle Route Contract Code</h3></th>
                                        <th><h3>Vehicle Type</h3></th>
                                        <th><h3>Load Type</h3></th>
                                        <th><h3>Container Type</h3></th>
                                        <th><h3>Container Qty</h3></th>
                                        <th><h3>First Pick UP</h3></th>
                                        <th><h3>Interim Point 1</h3></th>
                                        <th><h3>Interim Point 2</h3></th>
                                        <th><h3>Interim Point 3</h3></th>
                                        <th><h3>Interim Point 4</h3></th>
                                        <th><h3>Final Drop Point</h3></th>
                                        <th><h3>Travel Km</h3></th>
                                        <th><h3>Travel Time</h3></th>
                                        <th><h3>Rate with Reefer</h3></th>
                                        <th><h3>Rate without Reefer</h3></th>
                                        <th><h3>Status</h3></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${ptpBillingList}" var="ptpl">
                                                <tr>
                                                    <td><%=sno1%></td>
                                                    <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpl.routeCode}" /></td>
                                                    <td>
                                                        <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                        <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                        <c:out value="${ptpl.vehicleTypeName}" />
                                                    </td>
                                                    <td>
                                                        <c:if test="${ptpl.loadTypeId =='1'}">
                                                            Empty Trip
                                                        </c:if>
                                                        <c:if test="${ptpl.loadTypeId =='2'}">
                                                            Load Trip
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:forEach items="${containerTypeList}" var="conType">
                                                            <c:if test="${ptpl.containerTypeId ==conType.containerId}">
                                                    <option value="<c:out value="${conType.containerId}"/>">
                                                        <c:out value="${conType.containerName}"/>
                                                    </option>
                                                </c:if>
                                            </c:forEach>
                                            </td>
                                            <td>
                                                <c:if test="${ptpl.containerQty1 =='1'}">
                                                    1
                                                </c:if>
                                                <c:if test="${ptpl.containerQty1 =='2'}">
                                                    2
                                                </c:if>
                                            </td>
                                            <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                            <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                            <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                            <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                            <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                            <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                            <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                            <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                            <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                            <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                            <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach >
                                        </tbody>
                                    </table>
                                </c:if> 
                                <br>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("itemsTable");
                                </script>
                                <div id="controls">
                                    <div id="perpage">
                                        <select onchange="sorter.size(this.value)">
                                            <option value="5" selected="selected">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span>Entries Per Page</span>
                                    </div>
                                    <div id="navigation">
                                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                    </div>
                                    <div id="text">Displaying Page 1 of 1</div>
                                </div>
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("itemsTable", 1);
                                </script>
                            </div>
                        </div>
                        <div id="pcm">
                            <div class="inpad">
                                <c:if test = "${viewpenalitycharge != null}">
                                    <table class="border" align="center" width="40%" cellpadding="0" cellspacing="0" id="POD1">
                                        <thead>
                                            <tr id="rowId0">
                                                <td class="contenthead"  height="30">S No</td>
                                                <td class="contenthead"  height="30" >Charges names</td>
                                                <td class="contenthead"  height="30" >Unit</td>
                                                <td class="contenthead" height="30" >Amount</td>
                                                <td class="contenthead" height="30" >Remark</td>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <% int sno = 0;%>
                                            <c:forEach items="${viewpenalitycharge}" var="vpc">
                                                <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=className%>"  align="left"> <%= sno%>  </td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vpc.penality}" /></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vpc.pcmunit}"/></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vpc.chargeamount}" /></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vpc.pcmremarks}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                </c:if>
                            </div>
                        </div>

                        <div id="dcm" >
                            <div class="inpad">
                                <c:if test = "${viewdetentioncharge != null}">
                                    <table class="border" align="center" width="40%" cellpadding="0" cellspacing="0" id="POD2">
                                        <thead>
                                            <tr id="rowId1">
                                                <td class="contenthead"  height="30">S No</td>
                                                <td class="contenthead"  height="30" >Vehicle type</td>
                                                <td class="contenthead"  height="30" >Time Slot</td>
                                                <td class="contenthead"  height="30" >Amount</td>
                                                <td class="contenthead"  height="30" >Remarks</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <% int sno = 0;%>
                                            <c:forEach items="${viewdetentioncharge}" var="vdc">
                                                <%
                                                    sno++;
                                                    String className = "text1";
                                                     className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=className%>"  align="left"> <%= sno%>  </td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vdc.chargeamt}" /></td>
                                                    <td class="<%=className%>"  align="left"> <c:out value="${vdc.dcmremarks}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>

                                    </table>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${secondaryBillingTypeId == 4}">
                    <div id="fkmRate">
                        <div class="inpad">

                            <c:if test = "${contractFixedRateList != null}">
                                <table id="itemsTable" class="sortable">
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <% sno1 = 1;%>
                                        <tr height="110">
                                            <th><h3>S.No</h3></th>
                                    <th><h3>Vehicle Type Name</h3></th>
                                    <th><h3>Vehicle Count</h3></th>
                                    <th><h3>Total Km</h3></th>
                                    <th><h3>Total Reefer Hm</h3></th>
                                    <th><h3>Rate with Reefer</h3></th>
                                    <th><h3>Extra Km Rate/Km with Reefer</h3></th>
                                    <th><h3>Extra Reefer Hm Rate/Hm with Reefer</h3></th>
                                    <th><h3>Rate without Reefer</h3></th>
                                    <th><h3>Extra Km Rate/Km without Reefer</h3></th>
                                    <th><h3>Status</h3></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${contractFixedRateList}" var="ptpl">
                                            <tr>
                                                <td><%=sno1%></td>
                                                <td>
                                                    <input type="hidden" name="fixedVehTypeId" id="fixedVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <c:out value="${ptpl.vehicleTypeName}" />
                                                </td>
                                                <td><input type="hidden" name="contractVehicleNos" value="<c:out value="${ptpl.contractVehicleNos}" />" class="tInput" id="contractVehicleNos"  style="width: 90px;"/><c:out value="${ptpl.contractVehicleNos}" /></td>
                                                <td><input type="hidden" name="fixedTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="fixedTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                <td><input type="hidden" name="fixedTotalHm" value="<c:out value="${ptpl.totalHm}" />" class="tInput" id="fixedTotalHm"  style="width: 90px;"/><c:out value="${ptpl.totalHm}" /></td>
                                                <td><input type="hidden" name="fixedRateWithReefer" value="<c:out value="${ptpl.rateWithReefer}" />" class="tInput" id="fixedRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                <td><input type="hidden" name="extraKmRatePerKmRateWithReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" />" class="tInput" id="extraKmRatePerKmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" /></td>
                                                <td><input type="hidden" name="extraHmRatePerHmRateWithReefer" value="<c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" />" class="tInput" id="extraHmRatePerHmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" /></td>
                                                <td><input type="hidden" name="fixedRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReefer}" />" class="tInput" id="fixedRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="extraKmRatePerKmRateWithoutReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" />" class="tInput" id="extraKmRatePerKmRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach >
                                    </tbody>
                                </table>
                            </c:if>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable", 1);
                            </script>
                        </div>
                    </div>
                    <div id="fkmRoute">
                        <div class="inpad">

                            <c:if test = "${fixedKmBillingList != null}">
                                <table id="itemsTable" class="sortable">
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <%  sno1 = 1;%>
                                        <tr height="110">
                                            <th><h3>S.No</h3></th>
                                    <th><h3>Vehicle Route Contract Code</h3></th>
                                    <th><h3>Vehicle Type</h3></th>
                                    <th><h3>First Pick UP</h3></th>
                                    <th><h3>Interim Point 1</h3></th>
                                    <th><h3>Interim Point 2</h3></th>
                                    <th><h3>Interim Point 3</h3></th>
                                    <th><h3>Interim Point 4</h3></th>
                                    <th><h3>Final Drop Point</h3></th>
                                    <th><h3>Travel Km</h3></th>
                                    <th><h3>Travel Time</h3></th>
                                    <th><h3>Rate with Reefer</h3></th>
                                    <th><h3>Rate without Reefer</h3></th>
                                    <th><h3>Status</h3></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${fixedKmBillingList}" var="ptpl">
                                            <tr>
                                                <td><%=sno1%></td>
                                                <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpl.routeCode}" /></td>
                                                <td>
                                                    <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                    <c:out value="${ptpl.vehicleTypeName}" />
                                                </td>
                                                <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach >
                                    </tbody>
                                </table>
                            </c:if>
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable", 1);
                            </script>
                        </div>
                    </div>

                </c:if>

                <c:if test="${secondaryBillingTypeId == 2}">
                    <div id="ptpw">

                        <div class="inpad">
                            <c:if test = "${ptpwBillingList != null}">
                                <table id="itemsTable1" class="sortable">
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <%  sno1 = 1;%>
                                        <tr height="60">
                                            <th><h3>S.No</h3></th>
                                    <th><h3>Vehicle Route Contract Code</h3></th>
                                    <th><h3>Vehicle Type</h3></th>
                                    <th><h3>Load Type</h3></th>
                                    <th><h3>Container Type</h3></th>
                                    <th><h3>Container Qty</h3></th>
                                    <th><h3>First Pick UP</h3></th>
                                    <th><h3>Final Drop Point</h3></th>
                                    <th><h3>Total Km</h3></th>
                                    <th><h3>Rate with Reefer / KG</h3></th>
                                    <th><h3>Rate without Reefer / KG</h3></th>
                                    <th><h3>Status</h3></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${ptpwBillingList}" var="ptpwl">
                                            <tr>
                                                <td><%=sno1%></td>
                                                <td><input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="<c:out value="${ptpwl.routeCode}" />" class="tInput" style="width:120px"/><c:out value="${ptpwl.routeCode}" /></td>
                                                <td><input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${ptpwl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.vehicleTypeName}" /></td>
                                                <td><input type="hidden" name="loadTypeId" id="loadTypeId" value="<c:out value="${ptpwl.loadTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <%--<c:out value="${ptpwl.loadTypeId}" />--%>
                                                    <c:if test="${ptpwl.loadTypeId == 1}" >
                                                        Empty Trip
                                                    </c:if>
                                                    <c:if test="${ptpwl.loadTypeId == 2}" >
                                                        Load Trip
                                                    </c:if>
                                                </td>
                                                <td><input type="hidden" name="containerTypeName" id="containerTypeName" value="<c:out value="${ptpwl.containerTypeName}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.containerTypeName}" /></td>
                                                <td><input type="hidden" name="containerQty1" id="containerQty1" value="<c:out value="${ptpwl.containerQty1}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.containerQty1}" /></td>
                                                <td><input type="hidden" name="ptpwFirstPickupId" id="ptpwFirstPickupId" value="<c:out value="${ptpwl.firstPickupId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.firstPickupName}" /></td>
                                                <td><input type="hidden" name="ptpwFinalPointId" id="ptpwFinalPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/> <c:out value="${ptpwl.finalPointName}" /></td>
                                                <td><input type="hidden" name="ptpwTotalKm" id="ptpwTotalKm" value="<c:out value="${ptpwl.totalKm}" />" class="tInput"  style="width: 120px;"/><input type="hidden" name="ptpwPointId" id="ptpwPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.finalPointKm}" /></td>
                                                <td><input type="hidden" name="ptpwRateWithReefer"  id="ptpwRateWithReefer" value="<c:out value="${ptpwl.rateWithReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithReeferPerKg}" /></td>
                                                <td><input type="hidden" name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer"  value="<c:out value="${ptpwl.rateWithoutReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithoutReeferPerKg}" /></td>
                                                <td><input type="hidden" name="status" id="status"  value="<c:out value="${ptpwl.status}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <!--                                <center>
                                                                    <a  class="nexttab" href="#" ><input type="button" class="button" value="Next" name="Save" style="width: 120px"/></a>
                                                                </center>-->
                            </c:if> 
                            <br>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable1");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable1", 1);
                            </script>

                        </div>
                        <!--                        <center>
                                                    <input type="button" class="button" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                </center>-->
                    </div>
                </c:if>



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                <c:if test="${secondaryBillingTypeId == 3}">
                    <div id="mfr">
                        <c:if test="${actualKmBillingList != null}">
                            <% int sno1 = 1;%>
                            <%  sno1 = 1;%>
                            <table width="815" align="center" border="0" id="table2" class="sortable">
                                <thead>
                                    <tr height="30">
                                        <th><h3>S.No</h3></th>
                                <th><h3>Vehicle Type (Mfr/Model)</h3></th>
                                <th><h3>Vehicle INR/Km</h3></th>
                                <th><h3>Reefer INR/Hrs</h3></th>
                                <th><h3>Select</h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${actualKmBillingList}" var="mfrList">
                                        <tr height="30">
                                            <td align="left"><%=sno1%></td>
                                            <td align="left">
                                                <input type="hidden" name="actualVehicleTypeId" value="<c:out value="${mfrList.vehicleTypeId}"/>" /><c:out value="${mfrList.vehicleTypeName}"/></td>
                                            <td align="left"><input type="hidden" name="vehicleRatePerKm" class="textbox" value="<c:out value="${mfrList.vehicleRatePerKm}"/>"><c:out value="${mfrList.vehicleRatePerKm}"/></td>
                                            <td align="left"><input type="hidden" name="reeferRatePerHour" class="textbox" value="<c:out value="${mfrList.reeferRatePerHour}"/>"><c:out value="${mfrList.reeferRatePerHour}"/></td>
                                            <td align="left"><input type="hidden" name="status" value="<c:out value="${mfrList.activeInd}"/>"/><c:out value="${mfrList.activeInd}"/></td>
                                        </tr>
                                        <%sno1++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                        <br>
                        <!--                        <center>
                                                    <input type="button" class="button" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                </center>-->
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table2");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page 1 of 1</div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table2", 1);
                        </script>
                    </div>
                </c:if>

            </div>   

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

