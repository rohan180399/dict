<%--
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage() {
        document.vehicleVendorContract.action = '/throttle/saveEditVehicleVendorContract.do';
        document.vehicleVendorContract.method = "post";
        document.vehicleVendorContract.submit();
    }
</script>
<html>
    <body>
        <%--  <%try{%>--%>

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>

            <table width="980" align="center" border="0" id="table2" >
                <tr>
                    <td class="contenthead" colspan="4" >Vendor Contract Info</td>
                </tr>
                <tr>
                    <td class="text1">Vendor Name</td>

                    <td class="text1"><input type="text" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox"><c:out value="${vendorName}"/></td>
                    <td height="30">Contract Type</td>
                    <td>
                        <select name="contractTypeId" id="contractTypeId" class="textbox" onchange="checkContractType(this.value);">
                            <!--<option value="0" selected>--Select--</option>-->
                            <option value="1" >Dedicated</option>
                            <option value="2" >Hired</option>
                            <option value="3" selected>Both</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="30">Contract From</td>
                    <td>
                        <input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker">
                        <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}" />" class="datepicker">
                    </td>

                    <td height="30">Contract To</td>
                    <td><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker"></td>
                </tr>
                <tr>
                    <td height="30">Payment Type</td>
                    <td>
                        <select name="paymentType" id="paymentType" class="textbox">
                            <option value="1" >Monthly</option>
                            <option value="2" >Trip Wise</option>
                            <option value="3" >FortNight</option>
                        </select>
                    </td>


            </table>
            <br>

            <div id="tabs">
                <ul class="">
                    <li><a href="#deD"><span>DEDICATED</span></a></li>
                    <li><a href="#fullTruck"><span>ON DEMAND</span></a></li>
                    <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                </ul>

                <div id="fullTruck">
                    <div id="routeFullTruck">

                        <c:if test="${hireList != null}">
                            <table align="center" border="0" id="ondemandTable" class="sortable" style="width:1000px;" >
                                <thead style="height: 70px;">
                                    <tr style="height: 70px;">
                                        <th align="center"><h3>S.No</h3></th>
                                <th align="center"><h3>Vehicle Type</h3></th>
                                <th align="center"><h3>Vehicle Units<br><br></h3></th>
                                <th align="center"><h3>Origin</h3></th>
                                <th align="center"><h3>Destination</h3></th>

                                <th align="center"><h3>Trailer Type</h3></th>
                                <th align="center"><h3>Trailer Units</h3></th>
                                <th align="center"><h3>Spot Cost Trip</h3></th>
                                <th align="center"><h3>Additional Cost</h3></th>
                                <th align="center"><h3>Travel Kms</h3></th>
                                <th align="center"><h3>Travel Hours</h3></th>
                                <th align="center"><h3>Travel Minutes</h3></th>
                                <th align="center"><h3>Active Status</h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${hireList}" var="route">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td class="<%=classText%>"><c:out value="${route.vehicleTypeId}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.vehicleUnits}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/></td>
                                            <td class="<%=classText%>"><c:out value="${route.destinationNameFullTruck}"/></td>

                                            <td class="<%=classText%>"><c:out value="${route.trailerType}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.trailorTypeUnits}"/></td>
                                            <td class="<%=classText%>"   >
                                                <input type="hidden" name="contractHireId" id="contractHireId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.contractHireId}"/>"/>
                                                <input type="text" name="spotCostE" id="spotCostE<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.spotCost}"/>"/></td>
                                            <td class="<%=classText%>"   ><input type="text" name="additionalCostE" id="additionalCostE<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.additionalCost}"/>"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelKmFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelHourFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelMinuteFullTruck}"/></td>
                                            <td class="<%=classText%>"   >
                                                <select name="activeIndD" id="activeIndD<%=index%>">
                                                    <c:if test="${route.activeInd == 'Y'}" >
                                                        <option value="Y" selected>Active</option>
                                                        <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${route.activeInd == 'N'}" >
                                                        <option value="N" selected>In-Active</option>
                                                        <option value="Y" >Active</option>
                                                    </c:if>
                                                </select>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <script>
                                var container = "";
                                function addRouteFullTruck() {
//                                    $('#AddRouteFullTruck').hide();
                                    var iCnt = <%=index%>;
                                    var rowCnt = <%=index%>;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    container = $($("#routeFullTruck")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(container).last().after('<table  class="contenthead" id="routeDetails' + iCnt + rowCnt + '"  border="1">\n\
                        <tr><td>Sno</td>\n\
                        <td>Origin</td>\n\
                        <td>Destination</td>\n\
                        <td>Travel Km</td>\n\
                        <td>Travel Hour</td>\n\
                        <td>Travel Min</td></tr>\n\
                        <tr><td>Route&nbsp; ' + iCnt + '</td>\n\
                        <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                        <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                        <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                        <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\</td>\n\
                        <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                        <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                        <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                        </tr>\n\
                        </table>\n\
                        <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                        <tr><td>Sno</td><td>Vehicle Type</td><td>Vehicle Units</td><td>Trailer Type</td><td>Trailer Units</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                        <tr>\n\
                        <td>' + rowCnt + '</td>\n\
                        <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                        <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                        <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList  != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                        <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                        <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                        <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                        </tr></table>\n\
                        <table border="" width="" id="routebutton' + iCnt + rowCnt + '" ><tr>\n\
                        <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                        <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                         </tr></table></td></tr></table>');
                                    callOriginAjaxFullTruck(iCnt);
                                    callDestinationAjaxFullTruck(iCnt);
                                }
                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                function addRow(val, v1) {
                                    var loadCnt = val;
                                    var loadCnt1 = v1;
                                    var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                    var ondemandTable = $('#ondemandTable tr').size()-1;
                                    
                                    alert("routeInfoSize"+routeInfoSize);
                                    alert("ondemandTable"+ondemandTable);
                                    var routeInfoSizeSno = routeInfoSize + ondemandTable;
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSno + '</td><td><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value=""/></td><td><select type="text" name="trailerType" id="trailerType' + loadCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td><td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + loadCnt + '" value=""/></td><td><input type="text" name="spotCost" id="spotCost' + loadCnt + '" value=""/></td><td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="" /></td></tr>');
                                    loadCnt++;
                                }
                                function deleteRow(val) {
                                    var loadCnt = val;

                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
//                                    alert('One row should be present in table');
                                        $('#routeDetails' + loadCnt).remove();
//                                    $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').remove();
                                        $('#routeInfoDetailsFullTruck' + loadCnt).remove();
                                        $('#routebutton' + loadCnt).remove();
                                        $('#AddRouteFullTruck').show();
                                    }
                                }
                                    </script>

                                    <script>
                                        function callOriginAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'originNameFullTruck' + val;
                                            var pointId = 'originIdFullTruck' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }


                                        //
                                        //
                                        //                                }
                                        function callDestinationAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNameFullTruck' + val;
                                            var pointIdId = 'destinationIdFullTruck' + val;
                                            var originPointId = 'originIdFullTruck' + val;
                                            var truckRouteId = 'routeIdFullTruck' + val;
                                            var travelKm = 'travelKmFullTruck' + val;
                                            var travelHour = 'travelHourFullTruck' + val;
                                            var travelMinute = 'travelMinuteFullTruck' + val;


                                            //                                    if($("#" + originPointId).val() == ""){
                                            //                                        alert("id inside condition ==")
                                            //                                    }else{
                                            //                                        alert("else inside condition ==")
                                            //                                    }
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityToList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: $("#" + pointNameId).val(),
                                                            originCityId: $("#" + originPointId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.cityName;
                                                    var id = ui.item.cityId;
                                                    //                                            alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.Distance);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.cityName;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };
                                        }
                                    </script>

                                </div>


                                <a  ><input type="button" class="button" value="Add" id="AddRouteFullTruck" name="AddRouteFullTruck" onclick="addRouteFullTruck()"/></a>
                                <a  class="pretab" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>
                        </c:if>
                </div>


                <div id="deD">
                    <div id="addDedicate">
                        <table class="contenthead" style="border-spacing: 40px 10px;" cellspacing="10" id="dedicateDetails" border="1">
                            <c:if test="${fuelHike != null}">
                                <c:forEach items="${fuelHike}" var="fuelHike">
                                    <tr><td style="padding-left:10px;padding-right:10px;">Agreed Fuel Price</td>
                                        <td style="margin-left:-200px;">
                                            <input type="text" readonly width="20" name="agreedFuelPrice" id="agreedFuelPrice" value="<c:out value="${fuelHike.agreedFuelPrice}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                        <td style="width:3px;">
                                            <select ype="text" readonly name="uom" id="uom" style="width:85px;">
                                                <option value="0">--Select--</option>
                                                <option value="1">Litre</option>
                                                <option value="2">Gallon</option>
                                                <option value="3">Kilogram</option>
                                            </select>
                                            <script>
                                                document.getElementById("uom").value =<c:out value="${fuelHike.uom}"/>;
                                            </script>
                                        </td>
                                        <td style="padding-left:10px;padding-right:10px;">Fuel Hike % for Revising Cost</td>
                                        <td><input type="text" readonly name="hikeFuelPrice" id="hikeFuelPrice" value="<c:out value="${fuelHike.fuelHikePercentage}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </table>
                        <br>

                        <c:if test="${dedicateList != null}">
                            <table align="center" border="0" id="table" class="sortable" id="mainTableFullDedicate" style="width:1000px;" >
                                <thead style="height: 100px;">
                                    <tr style="height: 100px;">
                                        <th align="center"><h3>S.No</h3></th>
                                <th align="center"><h3>Vehicle Type</h3></th>
                                <th align="center"><h3>Vehicle Units</h3></th>
                                <th align="center"><h3>Trailer Type</h3></th>
                                <th align="center"><h3>Trailer Units</h3></th>
                                <th align="center"><h3>Contract Category</h3></th>
                                <th align="center"><h3>Fixed Cost Per Vehicle & Month</h3></th>
                                <th colspan="2" align="center"><h3>Fixed Duration Per day</h3><br><br><table  class="contenthead" border="1"><tr><td width="250px;"><center>Hours</center></td><td width="212px;"><center>Minutes</center></td></tr></table></th>
                                <th align="center"><h3>Total Fixed Cost Per Month</h3></th>
                                <th align="center"><h3>Rate Per KM</h3></th>
                                <th align="center"><h3>Rate Exceeds Limit KM</h3></th>
                                <th align="center"><h3>Max Allowable KM Per Month</h3></th>
                                <!--                            <table  class="contenthead" id="routeDetails1"  border="1">
                                                                
                                                            </table>-->

                                <th align="center" colspan="2"><h3>Over Time Cost Per HR</h3><br><table  class="contenthead" border="1"><tr><td width="250px;"><center>Work Days</center></td><td width="212px;"><center>Holidays</center></td></tr></table></th>
                                <th align="center"><h3>Additional Cost</h3></th>
                                <th align="center"><h3>Active Status</h3></th>
                                <th align="center"><h3>Vehicle & Trailer Details</h3></th>

                                </tr>
                                </thead>
                                <tbody>
                                    <% int index1 = 1;%>
                                    <c:forEach items="${dedicateList}" var="weight">
                                        <%
                                            String classText1 = "";
                                            int oddEven1 = index1 % 2;
                                            if (oddEven1 > 0) {
                                                classText1 = "text2";
                                            } else {
                                                classText1 = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText1%>"  ><%=index1++%></td>
                                            <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${weight.trailorTypeDedicate}"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${weight.trailorUnitsDedicate}"/></td>
                                            <td class="<%=classText1%>"   >
                                                <c:if test="${weight.contractCategory == '1'}" >
                                                    Fixed
                                                </c:if>
                                                <c:if test="${weight.contractCategory == '2'}" >
                                                    Actual
                                                </c:if>

                                                </select></td>
                                            <td class="<%=classText1%>"   >
                                                <input type="hidden" name="contractDedicateId" id="contractDedicateId<%=index1%>"  value="<c:out value="${weight.contractDedicateId}"/>"/>
                                                <input type="text" name="fixedCostE" id="fixedCostE<%=index1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.fixedCost}"/>"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="fixedHrsE" id="fixedHrsE<%=index1%>" value="<c:out value="${weight.fixedHrs}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="fixedMinE" id="fixedMinE<%=index1%>" value="<c:out value="${weight.fixedMin}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="totalFixedCostE" id="totalFixedCostE<%=index1%>" value="<c:out value="${weight.totalFixedCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="rateCostE" id="rateCostE<%=index1%>" value="<c:out value="${weight.rateCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="rateLimitE" id="rateLimitE<%=index1%>" value="<c:out value="${weight.rateLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><c:out value="${weight.maxAllowableKM}"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="workingDaysE" id="workingDaysE<%=index1%>" value="<c:out value="${weight.workingDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="holidaysE" id="holidaysE<%=index1%>" value="<c:out value="${weight.holidays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="addCostDedicateE" id="addCostDedicateE<%=index1%>" value="<c:out value="${weight.addCostDedicate}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td class="<%=classText1%>"   >
                                                <select name="activeIndE" id="activeIndE<%=index1%>">
                                                    <c:if test="${weight.activeInd == 'Y'}" >
                                                        <option value="Y" selected>Active</option>
                                                        <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${weight.activeInd == 'N'}" >
                                                        <option value="N" selected>In-Active</option>
                                                        <option value="Y" >Active</option>
                                                    </c:if>
                                                </select></td>
                                            <td class="<%=classText1%>"   >
                                                <c:if test="${weight.existingVehicleUnit == 0 }" >
                                                    <a href="/throttle/configureVehicleTrailerPage.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>" >Configure</a>

                                                </c:if>     
                                                <c:if test="${weight.existingVehicleUnit > 0 }" >
                                                    <a href="/throttle/viewVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >View</a>
                                                </c:if> 
                                                <c:if test="${weight.existingVehicleUnit > 0}" >
                                                    <a href="/throttle/editVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >Edit</a>
                                                </c:if>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <br/>

                            <script>
                                //                              function addNewRowDedicate1() {
                                //                                alert("Hi Iam Here");
                                //                            }
                                var containerDedicate = "";
                                function addNewRowDedicate() {
                                    //                                alert("Hi Iam Here");
                                    $('#AddNewRowFullTruck').hide();
                                    var iCnt = <%=index1%>;
                                    var rowCnt = <%=index1%>;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    containerDedicate = $($("#addDedicate")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    //                                    alert(iCnt);
                                    $(containerDedicate).last().after('\
                                   <table  class="contenthead" id="routeDetails1' + iCnt + '"  border="1">\n\
                                    <tr>\n\
                                    <td>Sno</td>\n\
                                    <td><center>Vehicle Type</center></td>\n\
                                    <td><center>Vehicle Units</center></td>\n\
                                    <td><center>Trailer Type</center></td>\n\
                                    <td><center>Trailer Units</center></td>\n\
                                    <td><center>Contract Category</center></td>\n\
                                   <td><center>Fixed Cost Per Vehicle & Month</center></td>\n\
                                    <td colspan="2"><br><center>Fixed Duration <br>Per day</center><br>\n\
                                    <table class="contenthead" border="1">\n\
                                    <tr><td width="235px;"><center>Hours</center></td><td width="206px;"><center>Minutes</center></td></tr></table></td>\n\
                                    <td><center>Total Fixed Cost Per Month</center></td>\n\
                                    <td><center>Rate Per KM</center></td>\n\
                                    <td><center>Rate Exceeds Limit KM</center></td>\n\
                                    <td><center>Max Allowable KM Per Month</center></td>\n\
                                    <td colspan="2"><br><br><center>Over Time Cost Per HR</center><br><br>\n\
                                    <table class="contenthead" border="1">\n\
                                    <tr><td width="245px;"><center>Work Days</center></td><td width="216px;height="20px;"><center>Holidays</center></td></tr></table></td>\n\
                                    <td><center>Additional Cost</center></td></tr>\n\
                                     <tr>\n\
                                   <td> ' + iCnt + ' </td>\n\
                                   <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td><select ype="text" name="trailorTypeDedicate" id="trailorTypeDedicate' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList  != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="trailorUnitsDedicate" id="trailorUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><select ype="text" name="contractCategory" id="contractCategory' + iCnt + '"><option value="0">--Select--</option><option value="1">Fixed</option><option value="2">Actual</option></select></td>\n\
                                   <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td width="10px;"><select ype="text" name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox">\n\
                                              <option value="00">00</option>\n\
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                   </td>\n\
                                    <td width="260px;"><select ype="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                   </td>\n\
                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                     </tr>\n\
                                    </table>\n\
                                   <table border="" id="buttonDetails1' + iCnt + '" width=""><tr>\n\
                                    <td>\n\
                                    <input class="button" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" />\n\
                                    <input class="button" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" />\n\
                               </tr></table></td></tr></table>');
                                }
                                    </script>
                                    <script>
                                        var routeInfoSizeSub = 0;
                                        function addRows(val) {
//                                            alert(val);
                                            var loadCnt = val;
                                            //    alert(loadCnt)
                                            //alert("loadCnt");
                                            //var loadCnt1 = ve;
                                            var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
//                                            alert(routeInfoSize);
                                            if (val == 2) {
                                                routeInfoSizeSub = routeInfoSize - 1;
                                            }else if (val == 4) {
                                                routeInfoSizeSub = routeInfoSize + 1;
                                            }else {
                                                routeInfoSizeSub = routeInfoSize;
                                            }
//                                            alert("routeInfoSizeSub" + routeInfoSizeSub);
                                            var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                            var routeInfoDetails = "routeDetails1" + loadCnt;
                                            $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                                <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                           <td><select ype="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                           <td><select ype="text" name="trailorTypeDedicate" id="trailorTypeDedicate' + routeInfoSizeSub + '"><option value="0">--Select--</option><c:if test="${trailerTypeList  != null}"><c:forEach items="${trailerTypeList }" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="trailorUnitsDedicate" id="trailorUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><select ype="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '"><option value="">--Select--</option><option value="1">Fixed</option><option value="2">Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                           <td width="10px;"><select ype="text" name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td width="260px;"><select ype="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <td><input type="text" name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM"' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                          </tr>');
                                            // alert("loadCnt = "+loadCnt)
                                            loadCnt++;
                                            //   alert("loadCnt = +"+loadCnt)
                                        }
                                        function onSelectVal(val, countVal) {
                                            //                            alert("vehicleTypeIdDedicate"+val);
                                            document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                        }


                                        function deleteRows(val) {
                                            var loadCnt = val;
                                            //     alert(loadCnt);
                                            var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                            var routeInfoDetails = "routeDetails1" + loadCnt;
                                            // alert(routeInfoDetails);
                                            if ($('#routeDetails1' + loadCnt + ' tr').size() > 4) {
                                                $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                //                                               alert('One row should be present in table');
                                                $('#dedicateDetails' + loadCnt).remove();
                                                $('#routeDetails1' + loadCnt).remove();
                                                $('#buttonDetails1' + loadCnt).remove();
                                                $('#AddNewRowFullTruck').show();
                                            }
                                        }
                                    </script>

                                    <script>
                                        function callOriginAjaxdeD(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'originNamedeD' + val;
                                            var pointIdId = 'originIddeD' + val;
                                            var desPointName = 'destinationNamedeD' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                        function callDestinationAjaxdeD(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNamedeD' + val;
                                            var pointIdId = 'destinationIddeD' + val;
                                            var originPointId = 'originIddeD' + val;
                                            var truckRouteId = 'routeIddeD' + val;
                                            var travelKm = 'travelKmdeD' + val;
                                            var travelHour = 'travelHourdeD' + val;
                                            var travelMinute = 'travelMinutedeD' + val;

                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            originCityId: $("#" + originPointId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.TravelKm);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                    </script>



                        </c:if>
                        <!--</div>-->
                        <a href="#"><input type="button" class="button" value="AddNewRow" id="AddNewRowFullTruck" name="addRowDedicate" onclick="addNewRowDedicate();"/></a>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>

                    </div>
                    <script>

                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });

                    </script>

                </div>

                <center>
                    <input type="button" class="buttona" value="Save" onclick="submitPage();" />

                </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <%--   <%}catch(Exception e)
     {
            out.println(e.toString());
        }
   %>--%>
    </body>
</html>
