
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="operations.label.Trailer"  text="Trailer"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.Trailer"  text="Trailer"/></a></li>
          <li class="active"><spring:message code="operations.label.Trailer"  text="Trailer"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onLoad="getVehicleNos();
            setImages(1, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >

            <%@ include file="/content/common/message.jsp" %>


           
                              <table  class="table table-info mb30 table-hover">
                                  <thead>
    
                                  <th colspan="6"  height="30"><spring:message code="trailers.label.Trailer" text="default text"/> </th>
                                  </thead>
                                    <tr>
                                        <td><spring:message code="trailers.label.TrailerNumber" text="default text"/></td><td><input type="text" id="regno" name="regNo" value="" style="width:260px;height:40px;"  class="form-control"  ></td>
                                        <td><spring:message code="trailers.label.MFR" text="default text"/></td><td><select style="width:260px;height:40px;"  class="form-control"  name="mfrId" >
                                                <option value="0">---<spring:message code="trailers.label.Select" text="default text"/>---</option>
                                                <c:if test = "${MfrList != null}" >
                                                    <c:forEach items="${MfrList}" var="Dept">
                                                        <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                        <td><spring:message code="trailers.label.TrailerType" text="default text"/></td><td><select style="width:310px;height:40px;"  class="form-control" name="typeId" >
                                                <option value="0">---<spring:message code="trailers.label.Select" text="default text"/>---</option>
                                                <c:if test = "${TypeList != null}" >
                                                    <c:forEach items="${TypeList}" var="Type">
                                                        <option value='<c:out value="${Type.typeId}" />'><c:out value="${Type.typeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td  height="30"><spring:message code="trailers.label.Ownership" text="default text"/></td>
                                        <td >
                                            <select style="width:260px;height:40px;"  class="form-control" name="ownership" id="ownership"  >
                                                <option value="" checked>--<spring:message code="trailers.label.Select" text="default text"/>--</option>
                                                <option value="1"><spring:message code="trailers.label.Own" text="default text"/></option>
                                                <option value="2"><spring:message code="trailers.label.Dedicate" text="default text"/></option>
                                                <option value="3"><spring:message code="trailers.label.Hire" text="default text"/> </option>
                                                <option value="4"><spring:message code="trailers.label.Replacement" text="default text"/></option>
                                            </select>
                                            <script>
                                                document.getElementById("ownership").value = '<c:out value="${ownership}"/>' ;
                                            </script>
                                        </td>
                                        <td colspan="4" align="center">
                                            <input type="button" class="btn btn-success"  onclick="submitPage(this.name);" name="add" value="<spring:message code="trailers.label.ADD" text="default text"/>"> &nbsp;&nbsp;
                                            <input type="button" class="btn btn-success"  onclick="submitPage(this.name);" name="test" value="<spring:message code="trailers.label.SEARCH" text="default text"/>">
                                        </td>
                                    </tr>
                                    
  </td>
                               </table>
  </table>
                            

            <%
                        int index = 1;

            %>


            <c:if test="${vehicleList == null }" >
                <center><font color="red" size="2"> <spring:message code="trailers.label.Norecordsfound" text="default text"/> </font></center>
            </c:if>
            <c:if test="${vehicleList != null }" >
                
                    <table class="table table-info mb30 table-hover" id="table" >
                    <!--<table class="table table-bordered" id="table">-->
                    <thead>
                        <tr>
                            <th><spring:message code="trailers.label.SNo" text="default text"/></th>
                            <th><spring:message code="trailers.label.TrailerNumber" text="default text"/></th>
                            <th><spring:message code="trailers.label.TrailerType" text="default text"/></th>
                            <th><spring:message code="trailers.label.Make" text="default text"/></th>
                            <th><spring:message code="trailers.label.Model" text="default text"/></th>
                            <th><spring:message code="trailers.label.Color" text="default text"/></th>
                            <th><spring:message code="trailers.label.Ownership" text="default text"/></th>
                            <th><spring:message code="trailers.label.Action" text="default text"/></th>
                        </tr>
                    </thead>
                    
                    
                    <%
                                String style = "text1";%>
                    <c:forEach items="${vehicleList}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td  height="30" > <%= index++%> </td>
                            <td  height="30" > <c:out value="${veh.trailerNo}" /> </td>
                            <td  height="30" ><c:out value="${veh.typeName}" /></td>

                            <td  height="30" ><c:out value="${veh.mfrName}" /></td>
                            <td  height="30" ><c:out value="${veh.modelName}" /></td>
                            <td  height="30" ><c:out value="${veh.vehicleColor}" /></td>
                            <td  height="30" ><c:out value="${veh.ownership}" /></td>
                            <td  height="30" >
                                <a href='/throttle/trailerDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=1&fleetTypeId=2&listId='  ><span class="label label-warning"><spring:message code="trailers.label.Edit" text="default text"/></span> </a>
                                <a href='/throttle/trailerDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=2&listId='  ><span class="label label-info"><spring:message code="trailers.label.View" text="default text"/></span> </a>
                            </td>
                                <%--
                                <c:if test="${veh.activeInd != '0'}" >
                                    closeJobCard
                                </c:if>
                                <c:if test="${veh.activeInd == '0'}" >
                                    <a href='/throttle/createJobcard.do?vehicleId=<c:out value="${veh.vehicleId}" />'  >makeInActive </a>
                                </c:if> 
                            </td>
                            <td class="<%=style%>"  align="left"><a href="/throttle/tyresRotation.do?vehicleId=<c:out value='${veh.vehicleId}' />&axleTypeId=<c:out value='${veh.axleId}' />">Rotation</a></td>
                                --%>

                        </tr>
                    </c:forEach>
                        
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="trailers.label.EntriesPerPage" text="default text"/></span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="trailers.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="trailers.label.Of" text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addTrailerPage.do?fleetTypeId=2';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/searchTrailerPage.do?fleetTypeId=2&listId=';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }


        function getVehicleNos() {
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        }

    </script>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>