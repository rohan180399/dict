<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  
</head>

<script>
 function setSelectbox(i)
    {       
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i].checked = 1;    
        
    }
    
    function submitPage(){
    document.mrs.action="/throttle/updateMrsItems.do";
    document.mrs.submit();
    }
    
</script>

<body>

<form name="mrs" method="post">
    
       
<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->


<%@ include file="/content/common/message.jsp"%>

 <c:if test = "${rcMrsWorkOrderDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">RC Details</div></td>
</tr>

      <c:forEach items="${rcMrsWorkOrderDetails}" var="rcMrs">

<tr>
<td class="text2" height="30">Work Order No</td>
<td class="text2" height="30"><c:out value="${rcMrs.rcWorkId}"/></td>
<td class="text2" height="30">Requested Date</td>
<td class="text2" height="30"><c:out value="${rcMrs.createdDate}"/></td>
</tr>
<tr>
<td class="text1" height="30">Vendor Name</td>
<td class="text1" height="30"><c:out value="${rcMrs.vendorName}"/></td>
<td class="text1" height="30">Technician</td>
<td class="text1" height="30"><c:out value="${rcMrs.empName}"/></td>
</tr>

 </c:forEach>
 </c:if>
</table>

<br><br>

<br><br>
 <%
    
    int index1=0;
    %>

<c:if test = "${rcMrsDetails != null}" >
    
<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="border">

<tbody><tr>
<td colspan="8" class="text2" align="center" height="30"><strong>Material Details</strong></td>
</tr>
<tr>
<td class="contenthead" height="30"><div class="contenthead">SNo</div></td>
<td class="contenthead" height="30"><div class="contenthead">Mfr Code</div></td>
<td class="contenthead" height="30"><div class="contenthead">Item Code</div></td>
<td class="contenthead" height="30"><div class="contenthead">Item Name</div></td>
<td class="contenthead" height="30"><div class="contenthead">UOM</div></td>
<td class="contenthead" height="30"><div class="contenthead">Requested Qty</div></td>
<td class="contenthead" height="30"><div class="contenthead">Approved Qty</div></td>

</tr>

<c:forEach items="${rcMrsDetails}" var="rc">
  <%  
String classText = "";
int oddEven = index1% 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>

<tr>
<td class="<%=classText%>" height="30"><%=index1+1%></td>
<input name="itemId" class="form-control" type="hidden" value='<c:out value="${rc.mrsItemId}" />'>
<td class="<%=classText%>" height="30"><c:out value="${rc.mrsItemMfrCode}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${rc.mrsPaplCode}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${rc.mrsItemName}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${rc.uomName}" /></td>
<td class="<%=classText%>" height="30"><c:out value="${rc.mrsRequestedItemNumber}"/></td>
<td class="<%=classText%>" height="30"><c:out value="${rc.approvedQty}"/></td>
</tr>
<%index1++;%>
</c:forEach>
</tbody></table>
</c:if>
<br>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </body>
</html>
