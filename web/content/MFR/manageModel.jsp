
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<script language="javascript">
    function submitPage(value) {
        if (value == 'add')
        {
            document.desigDetail.action = '/throttle/handleModeladdPage.do';
        } else if (value == 'modify') {

            document.desigDetail.action = '/throttle/handleModelAlterPage.do';
        }
        document.desigDetail.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Model"  text="Model"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.FleetName"  text="Fleet"/></a></li>
            <li class="active"><spring:message code="stores.label.Model"  text="Model"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body>

                <form method="post" name="desigDetail">

                    <%@ include file="/content/common/message.jsp" %>

                    <% int index = 0; %>    

                    <c:if test = "${ModelList != null}" >
                        <!--<td colspan="4"  style="background-color:#5BC0DE;">-->
                        <table class="table table-info mb30" id="table" >
                            <thead>

                                <tr align="center">
                                    <th height="30" ><spring:message code="trucks.label.SNo"  text="default text"/></th>
                                    <th height="30" ><spring:message code="trucks.label.MFR"  text="default text"/></th>
                                    <th height="30" ><spring:message code="trucks.label.Model"  text="default text"/></th>
                                    <th height="30" >
                                        <c:if test="${fleetTypeId == 1}">
                                            <spring:message code="trucks.label.Vehicle"  text="default text"/>
                                        </c:if>
                                        <c:if test="${fleetTypeId == 2}">
                                            <spring:message code="trucks.label.Trailer"  text="default text"/>
                                        </c:if> <spring:message code="trucks.label.Type"  text="default text"/></th>
                                    <th height="30" ><spring:message code="trucks.label.FuelType"  text="default text"/></th>
                                    <th height="30" ><spring:message code="trucks.label.Description"  text="default text"/></th>
                                    <th height="30" ><spring:message code="trucks.label.Status"  text="default text"/></th>

                                </tr>
                            </thead>
                            <%
 
                            %>

                            <c:forEach items="${ModelList}" var="list"> 	
                                <%

                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>
                                <tr  width="208" height="40" > 
                                    <td><%=index + 1%></td>
                                    <td><c:out value="${list.mfrName}"/></td>
                                    <td><c:out value="${list.modelName}"/></td>
                                    <td><c:out value="${list.typeName}"/></td>
                                    <td><c:out value="${list.fuelName}"/></td>
                                    <td><c:out value="${list.description}"/></td>
                                    <td>                                
                                        <c:if test = "${list.activeInd == 'Y'}" >
                                            <spring:message code="trucks.label.Active"  text="default text"/>
                                        </c:if>
                                        <c:if test = "${list.activeInd == 'N'}" >
                                            <spring:message code="trucks.label.InActive"  text="default text"/> 
                                        </c:if>
                                    </td>

                                </tr>
                                <%
                    index++;
                                %>
                            </c:forEach >

                        </table>
                        </td>
                    </c:if> 
                    <center>
                        <br>
                        <input type="hidden" name="fleetTypeId" value="<c:out value="${fleetTypeId}"/>">
                        <input type="button" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success">
                        <c:if test = "${ModelList != null}" >
                            <input type="button" value="<spring:message code="trucks.label.Alter"  text="default text"/>" name="modify" onClick="submitPage(this.name)" class="btn btn-success">
                        </c:if>
                        <input type="hidden" name="reqfor" value="">
                    </center>
                    <br>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="trucks.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>