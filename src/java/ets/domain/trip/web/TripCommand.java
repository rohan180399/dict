/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
package ets.domain.trip.web;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
public class TripCommand {

    /** Creates a new instance of __NAME__ */
    public TripCommand() {
    }
    private String productId[] = null;
    private String commodityName = "";
    private String billOfEntry = "";
     private String shipingLineNo = "";
    private String tripFactoryToIcdHour = "";
    private String tripDetainHour = "";
    private String bunkName = "";
     private String remarks = "";
     private String location = "";
     private String state = "";
     private String currRate = "";
     private String activeStatus = "";
     private String fuelType = "";

     private String expcontractRouteOrigin = "";
     private String expcontractRouteDestination = "";
     private String billingParty = "";
     private String originId = "";
     private String trailerId = "";
     private String orderSize = "";
     private String orderType = "";
     private String startTrailerKM = "";
     private String endTrailerKM = "";
    private String overrideRemarks = "";
    private String setteledKm = "";
    private String setteledHm = "";
    private String setteltotalKM;
    private String setteltotalHrs;
    private String totalMinutes = "";
    private String totalKm = "";
    private String totalHours = "";
    private String expense = "";
    private String emptyTripRemarks = "";
    private String origin = "";
    private String destination = "";
    private String destinationId = "";
    private String cityFrom = "";
    private String cityTo = "";
    private String tripTransitHour = "";
    private String wfuRemarks = "";
    private String routeNameStatus = "";
    private String wfuUnloadingDate = "";
    private String wfuUnloadingTime = "";
    private String podStatus = "";
    private String cityFromId = "";
    private String vehicleId = "";
    private String lrNumber = "";
    private String primaryDriverId = "";
    private String secondaryDriverIdOne = "";
    private String secondaryDriverIdTwo = "";
    private String advanceDate = "";
    private String advanceAmount = "";
    private String advanceRemarks = "";
    private String stakeHolderId = "";
    private String holderName = "";
    private String statusName = "";
    private String[] assignedfunc = null;
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
     private String[] productVolume = null;
    private String[] productbatch = null;
    private String[] productuom = null;
    private String[] loadedpackages = null;
    private String[] unloadedpackages = null;
    private String[] shortage = null;
    private String[] tripArticleId = null;
    private String vehicleactreportdate = "";
    private String vehicleactreporthour = "";
    private String vehicleactreportmin = "";
    private String vehicleloadreportdate = "";
    private String vehicleloadreporthour = "";
    private String vehicleloadreportmin = "";
    private String vehicleloadtemperature = "";
    private String consignmentId = "";
    private String articleCode = "";
    private String articleName = "";
    private String batch = "";
    private String packageNos = "";
    private String packageWeight = "";
    private String uom = "";
    private String loadpackageNos = "";
    private String tripArticleid = "";
    private String planStartDate = "";
    private String planStartHour = "";
    private String planStartMinute = "";
    private String planEndDate = "";
    private String planEndHour = "";
    private String planEndMinute = "";
    private String planStartTime = "";
    private String planEndTime = "";
    private String totalMins = "";
    private String tollAmount = "";
    private String driverBatta = "";
    private String driverIncentive = "";
    private String milleage = "";
    private String reeferConsumption = "";
    private String fuelPrice = "";
    private String fuelConsumed = "";
    private String fuelCost = "";
    private String tollCost = "";
    private String routeExpense = "";
    private String totalExpenses = "";
    private String totalRumKM = "";
    private String totalRefeerHours = "";
    private String totalRefeerMinutes = "";
    private String battaAmount = "";
    private String incentiveAmount = "";
    private String vehicleTypeId = "";
    private String estimatedExpense = "";
    private String routeExpenses = "";
    private String startDate = "";
    private String startTime = "";
    private String startOdometerReading = "";
    private String startHM = "";
    private String pointId = "";
    private String cityId = "";
    private String cityName = "";
    private String podRemarks = "";
    private String podFile = "";
    private String startTripRemarks = "";
    private String endDate = "";
    private String endTime = "";
    private String endHM = "";
    private String endOdometerReading = "";
    private String endTripRemarks = "";
    private String totalKM = "";
    private String totalHrs = "";
    private String preStartHM = "";
    private String fuelLocation = "";
    private String fillDate = "";
    private String fuelLitres = "";
    private String fuelPricePerLitre = "";
    private String fuelAmount = "";
    private String fuelremarks = "";
    private String expenseId = "";
    private String expenseDate = "";
    private String expenseType = "";
    private String totalExpenseAmount = "";
    private String expenseRemarks = "";
    private String expenseName = "";
    private String employeeId = "";
    private String employeeName = "";
    private String taxPercentage = "";
    private String expenseValue = "";
    private String statusId = "";
    private String tripExpenseId = "";
    private String tripPodId = "";
    private String durationHours = "";
    private String totalDays = "";
    private String durationDay1 = "";
    private String durationDay2 = "";
    private String consignmentOrderNos = "";
//    Nithiya Starts here
    private String regNo = "";
    private String tripSheetId = "";
    private String consignmentNo = "";
    private String customerId = "";
    private String fromDate = "";
    private String toDate = "";
    private String preStartDate = "";
    private String preStartTime = "";
    private String preOdometerReading = "";
    private String preStartLocation = "";
    private String preTripRemarks = "";
    private String tripEndMinute = "";
    private String tripEndHour = "";
    private String tripStartMinute = "";
    private String tripStartHour = "";
    private String tripPreStartMinute = "";
    private String tripPreStartHour = "";

     private String gpsVendor = "";
     private String gpsDeviceId = "";
     private String tripId = "";


    //    Nithiya ends here

    public String getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPreOdometerReading() {
        return preOdometerReading;
    }

    public void setPreOdometerReading(String preOdometerReading) {
        this.preOdometerReading = preOdometerReading;
    }

    public String getPreStartDate() {
        return preStartDate;
    }

    public void setPreStartDate(String preStartDate) {
        this.preStartDate = preStartDate;
    }

    public String getPreStartLocation() {
        return preStartLocation;
    }

    public void setPreStartLocation(String preStartLocation) {
        this.preStartLocation = preStartLocation;
    }

    public String getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(String preStartTime) {
        this.preStartTime = preStartTime;
    }

    public String getPreTripRemarks() {
        return preTripRemarks;
    }

    public void setPreTripRemarks(String preTripRemarks) {
        this.preTripRemarks = preTripRemarks;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDurationDay1() {
        return durationDay1;
    }

    public void setDurationDay1(String durationDay1) {
        this.durationDay1 = durationDay1;
    }

    public String getDurationDay2() {
        return durationDay2;
    }

    public void setDurationDay2(String durationDay2) {
        this.durationDay2 = durationDay2;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndHM() {
        return endHM;
    }

    public void setEndHM(String endHM) {
        this.endHM = endHM;
    }

    public String getEndOdometerReading() {
        return endOdometerReading;
    }

    public void setEndOdometerReading(String endOdometerReading) {
        this.endOdometerReading = endOdometerReading;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTripRemarks() {
        return endTripRemarks;
    }

    public void setEndTripRemarks(String endTripRemarks) {
        this.endTripRemarks = endTripRemarks;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getFillDate() {
        return fillDate;
    }

    public void setFillDate(String fillDate) {
        this.fillDate = fillDate;
    }

    public String getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(String fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getFuelLitres() {
        return fuelLitres;
    }

    public void setFuelLitres(String fuelLitres) {
        this.fuelLitres = fuelLitres;
    }

    public String getFuelLocation() {
        return fuelLocation;
    }

    public void setFuelLocation(String fuelLocation) {
        this.fuelLocation = fuelLocation;
    }

    public String getFuelPricePerLitre() {
        return fuelPricePerLitre;
    }

    public void setFuelPricePerLitre(String fuelPricePerLitre) {
        this.fuelPricePerLitre = fuelPricePerLitre;
    }

    public String getFuelremarks() {
        return fuelremarks;
    }

    public void setFuelremarks(String fuelremarks) {
        this.fuelremarks = fuelremarks;
    }

    public String getPodFile() {
        return podFile;
    }

    public void setPodFile(String podFile) {
        this.podFile = podFile;
    }

    public String getPodRemarks() {
        return podRemarks;
    }

    public void setPodRemarks(String podRemarks) {
        this.podRemarks = podRemarks;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPreStartHM() {
        return preStartHM;
    }

    public void setPreStartHM(String preStartHM) {
        this.preStartHM = preStartHM;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHM() {
        return startHM;
    }

    public void setStartHM(String startHM) {
        this.startHM = startHM;
    }

    public String getStartOdometerReading() {
        return startOdometerReading;
    }

    public void setStartOdometerReading(String startOdometerReading) {
        this.startOdometerReading = startOdometerReading;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTripRemarks() {
        return startTripRemarks;
    }

    public void setStartTripRemarks(String startTripRemarks) {
        this.startTripRemarks = startTripRemarks;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalExpenseAmount() {
        return totalExpenseAmount;
    }

    public void setTotalExpenseAmount(String totalExpenseAmount) {
        this.totalExpenseAmount = totalExpenseAmount;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getTripExpenseId() {
        return tripExpenseId;
    }

    public void setTripExpenseId(String tripExpenseId) {
        this.tripExpenseId = tripExpenseId;
    }

    public String getTripPodId() {
        return tripPodId;
    }

    public void setTripPodId(String tripPodId) {
        this.tripPodId = tripPodId;
    }

    public String getTripEndHour() {
        return tripEndHour;
    }

    public void setTripEndHour(String tripEndHour) {
        this.tripEndHour = tripEndHour;
    }

    public String getTripEndMinute() {
        return tripEndMinute;
    }

    public void setTripEndMinute(String tripEndMinute) {
        this.tripEndMinute = tripEndMinute;
    }

    public String getTripPreStartHour() {
        return tripPreStartHour;
    }

    public void setTripPreStartHour(String tripPreStartHour) {
        this.tripPreStartHour = tripPreStartHour;
    }

    public String getTripPreStartMinute() {
        return tripPreStartMinute;
    }

    public void setTripPreStartMinute(String tripPreStartMinute) {
        this.tripPreStartMinute = tripPreStartMinute;
    }

    public String getTripStartHour() {
        return tripStartHour;
    }

    public void setTripStartHour(String tripStartHour) {
        this.tripStartHour = tripStartHour;
    }

    public String getTripStartMinute() {
        return tripStartMinute;
    }

    public void setTripStartMinute(String tripStartMinute) {
        this.tripStartMinute = tripStartMinute;
    }

    public String getBattaAmount() {
        return battaAmount;
    }

    public void setBattaAmount(String battaAmount) {
        this.battaAmount = battaAmount;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getFuelConsumed() {
        return fuelConsumed;
    }

    public void setFuelConsumed(String fuelConsumed) {
        this.fuelConsumed = fuelConsumed;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(String incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanEndHour() {
        return planEndHour;
    }

    public void setPlanEndHour(String planEndHour) {
        this.planEndHour = planEndHour;
    }

    public String getPlanEndMinute() {
        return planEndMinute;
    }

    public void setPlanEndMinute(String planEndMinute) {
        this.planEndMinute = planEndMinute;
    }

    public String getPlanEndTime() {
        return planEndTime;
    }

    public void setPlanEndTime(String planEndTime) {
        this.planEndTime = planEndTime;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanStartHour() {
        return planStartHour;
    }

    public void setPlanStartHour(String planStartHour) {
        this.planStartHour = planStartHour;
    }

    public String getPlanStartMinute() {
        return planStartMinute;
    }

    public void setPlanStartMinute(String planStartMinute) {
        this.planStartMinute = planStartMinute;
    }

    public String getPlanStartTime() {
        return planStartTime;
    }

    public void setPlanStartTime(String planStartTime) {
        this.planStartTime = planStartTime;
    }

    public String getReeferConsumption() {
        return reeferConsumption;
    }

    public void setReeferConsumption(String reeferConsumption) {
        this.reeferConsumption = reeferConsumption;
    }

    public String getRouteExpense() {
        return routeExpense;
    }

    public void setRouteExpense(String routeExpense) {
        this.routeExpense = routeExpense;
    }

    public String getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(String routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTollCost() {
        return tollCost;
    }

    public void setTollCost(String tollCost) {
        this.tollCost = tollCost;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalMins() {
        return totalMins;
    }

    public void setTotalMins(String totalMins) {
        this.totalMins = totalMins;
    }

    public String getTotalRefeerHours() {
        return totalRefeerHours;
    }

    public void setTotalRefeerHours(String totalRefeerHours) {
        this.totalRefeerHours = totalRefeerHours;
    }

    public String getTotalRefeerMinutes() {
        return totalRefeerMinutes;
    }

    public void setTotalRefeerMinutes(String totalRefeerMinutes) {
        this.totalRefeerMinutes = totalRefeerMinutes;
    }

    public String getTotalRumKM() {
        return totalRumKM;
    }

    public void setTotalRumKM(String totalRumKM) {
        this.totalRumKM = totalRumKM;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String[] getLoadedpackages() {
        return loadedpackages;
    }

    public void setLoadedpackages(String[] loadedpackages) {
        this.loadedpackages = loadedpackages;
    }

    public String getLoadpackageNos() {
        return loadpackageNos;
    }

    public void setLoadpackageNos(String loadpackageNos) {
        this.loadpackageNos = loadpackageNos;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String[] getProductbatch() {
        return productbatch;
    }

    public void setProductbatch(String[] productbatch) {
        this.productbatch = productbatch;
    }

    public String[] getProductuom() {
        return productuom;
    }

    public void setProductuom(String[] productuom) {
        this.productuom = productuom;
    }

    public String[] getShortage() {
        return shortage;
    }

    public void setShortage(String[] shortage) {
        this.shortage = shortage;
    }

    public String[] getTripArticleId() {
        return tripArticleId;
    }

    public void setTripArticleId(String[] tripArticleId) {
        this.tripArticleId = tripArticleId;
    }

    public String getTripArticleid() {
        return tripArticleid;
    }

    public void setTripArticleid(String tripArticleid) {
        this.tripArticleid = tripArticleid;
    }

    public String[] getUnloadedpackages() {
        return unloadedpackages;
    }

    public void setUnloadedpackages(String[] unloadedpackages) {
        this.unloadedpackages = unloadedpackages;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVehicleactreportdate() {
        return vehicleactreportdate;
    }

    public void setVehicleactreportdate(String vehicleactreportdate) {
        this.vehicleactreportdate = vehicleactreportdate;
    }

    public String getVehicleactreporthour() {
        return vehicleactreporthour;
    }

    public void setVehicleactreporthour(String vehicleactreporthour) {
        this.vehicleactreporthour = vehicleactreporthour;
    }

    public String getVehicleactreportmin() {
        return vehicleactreportmin;
    }

    public void setVehicleactreportmin(String vehicleactreportmin) {
        this.vehicleactreportmin = vehicleactreportmin;
    }

    public String getVehicleloadreportdate() {
        return vehicleloadreportdate;
    }

    public void setVehicleloadreportdate(String vehicleloadreportdate) {
        this.vehicleloadreportdate = vehicleloadreportdate;
    }

    public String getVehicleloadreporthour() {
        return vehicleloadreporthour;
    }

    public void setVehicleloadreporthour(String vehicleloadreporthour) {
        this.vehicleloadreporthour = vehicleloadreporthour;
    }

    public String getVehicleloadreportmin() {
        return vehicleloadreportmin;
    }

    public void setVehicleloadreportmin(String vehicleloadreportmin) {
        this.vehicleloadreportmin = vehicleloadreportmin;
    }

    public String getVehicleloadtemperature() {
        return vehicleloadtemperature;
    }

    public void setVehicleloadtemperature(String vehicleloadtemperature) {
        this.vehicleloadtemperature = vehicleloadtemperature;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String[] getAssignedfunc() {
        return assignedfunc;
    }

    public void setAssignedfunc(String[] assignedfunc) {
        this.assignedfunc = assignedfunc;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getStakeHolderId() {
        return stakeHolderId;
    }

    public void setStakeHolderId(String stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuUnloadingDate() {
        return wfuUnloadingDate;
    }

    public void setWfuUnloadingDate(String wfuUnloadingDate) {
        this.wfuUnloadingDate = wfuUnloadingDate;
    }

    public String getWfuUnloadingTime() {
        return wfuUnloadingTime;
    }

    public void setWfuUnloadingTime(String wfuUnloadingTime) {
        this.wfuUnloadingTime = wfuUnloadingTime;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getRouteNameStatus() {
        return routeNameStatus;
    }

    public void setRouteNameStatus(String routeNameStatus) {
        this.routeNameStatus = routeNameStatus;
    }

    public String getTripTransitHour() {
        return tripTransitHour;
    }

    public void setTripTransitHour(String tripTransitHour) {
        this.tripTransitHour = tripTransitHour;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getEmptyTripRemarks() {
        return emptyTripRemarks;
    }

    public void setEmptyTripRemarks(String emptyTripRemarks) {
        this.emptyTripRemarks = emptyTripRemarks;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getSetteledHm() {
        return setteledHm;
    }

    public void setSetteledHm(String setteledHm) {
        this.setteledHm = setteledHm;
    }

    public String getSetteledKm() {
        return setteledKm;
    }

    public void setSetteledKm(String setteledKm) {
        this.setteledKm = setteledKm;
    }

    public String getSetteltotalHrs() {
        return setteltotalHrs;
    }

    public void setSetteltotalHrs(String setteltotalHrs) {
        this.setteltotalHrs = setteltotalHrs;
    }

    public String getSetteltotalKM() {
        return setteltotalKM;
    }

    public void setSetteltotalKM(String setteltotalKM) {
        this.setteltotalKM = setteltotalKM;
    }

    public String getOverrideRemarks() {
        return overrideRemarks;
    }

    public void setOverrideRemarks(String overrideRemarks) {
        this.overrideRemarks = overrideRemarks;
    }

    public String getOrderSize() {
        return orderSize;
    }

    public void setOrderSize(String orderSize) {
        this.orderSize = orderSize;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String[] getProductVolume() {
        return productVolume;
    }

    public void setProductVolume(String[] productVolume) {
        this.productVolume = productVolume;
    }

    public String getEndTrailerKM() {
        return endTrailerKM;
    }

    public void setEndTrailerKM(String endTrailerKM) {
        this.endTrailerKM = endTrailerKM;
    }

    public String getStartTrailerKM() {
        return startTrailerKM;
    }

    public void setStartTrailerKM(String startTrailerKM) {
        this.startTrailerKM = startTrailerKM;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getGpsDeviceId() {
        return gpsDeviceId;
    }

    public void setGpsDeviceId(String gpsDeviceId) {
        this.gpsDeviceId = gpsDeviceId;
    }

    public String getGpsVendor() {
        return gpsVendor;
    }

    public void setGpsVendor(String gpsVendor) {
        this.gpsVendor = gpsVendor;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getExpcontractRouteDestination() {
        return expcontractRouteDestination;
    }

    public void setExpcontractRouteDestination(String expcontractRouteDestination) {
        this.expcontractRouteDestination = expcontractRouteDestination;
    }

    public String getExpcontractRouteOrigin() {
        return expcontractRouteOrigin;
    }

    public void setExpcontractRouteOrigin(String expcontractRouteOrigin) {
        this.expcontractRouteOrigin = expcontractRouteOrigin;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTripDetainHour() {
        return tripDetainHour;
    }

    public void setTripDetainHour(String tripDetainHour) {
        this.tripDetainHour = tripDetainHour;
    }

    public String getTripFactoryToIcdHour() {
        return tripFactoryToIcdHour;
    }

    public void setTripFactoryToIcdHour(String tripFactoryToIcdHour) {
        this.tripFactoryToIcdHour = tripFactoryToIcdHour;
    }

    public String getBillOfEntry() {
        return billOfEntry;
    }

    public void setBillOfEntry(String billOfEntry) {
        this.billOfEntry = billOfEntry;
    }

    public String getShipingLineNo() {
        return shipingLineNo;
    }

    public void setShipingLineNo(String shipingLineNo) {
        this.shipingLineNo = shipingLineNo;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String[] getProductId() {
        return productId;
    }

    public void setProductId(String[] productId) {
        this.productId = productId;
    }
   

}
