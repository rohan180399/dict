<%-- 
    Document   : tallyXMLData
    Created on : Oct 28, 2010, 6:25:54 PM
    Author     : Admin
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         </head>
         <script>
          function searchSubmit()
           {
               //alert(document.tallyXMLData.dataType.value);
             if(document.tallyXMLData.fromDate.value==''){
              alert("Please Enter From Date");
              return false;
           }else if(document.tallyXMLData.toDate.value==''){
            alert("Please Enter to Date");
            return false;
           }
           else if(document.tallyXMLData.reqDate.value==''){
               alert("Please Enter XMLRequired Date");
               return false;
           }
           else if(document.tallyXMLData.dataType.value==0){
               alert("Please Enter Data Type");
               return false;
           }
          else if(document.tallyXMLData.fromDate.value < document.tallyXMLData.toDate.value)
               {
                   alert("FromDate should not less than ToDate");
                   return false;
               }
          else if(document.tallyXMLData.fromDate.value < document.tallyXMLData.reqDate.value || document.tallyXMLData.toDate.value < document.tallyXMLData.reqDate.value )
               {
                   alert("Requested Date should not greater than fromDate or toDate");
                   return false;
               }
        document.tallyXMLData.action = '/throttle/tallyXMLReport.do'
        document.tallyXMLData.submit();
       }
             </script>

  <body>
        <form name="tallyXMLData"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg" class="border">
                <tr>
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">TallyXML Config</div> </td>
                </tr>
                <tr>
                    <td class="text1" height="30">From Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="form-control"  name="fromDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tallyXMLData.fromDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">TO Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="form-control"  readonly name="toDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tallyXMLData.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">XML Required Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="form-control"  readonly name="reqDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tallyXMLData.reqDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">Type Of Data</td>
                    <td class="text1" height="30">
                        <select class="form-control" name="dataType"  height="30">
                            <option value="0" selected>--Select--</option>
                            <option value="1">All Data</option>
                            <option value="2">New Data</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center" class="text2" height="30">
                        <input type="button" class="button" readonly name="Generate" value="Generate" onClick="searchSubmit();" >
                    </td>

                </tr>

                </table>

    </body>
    </html>
