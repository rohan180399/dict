/*----------------------------------------------------------------------------
 * FPRuntimeException.java
 * Jan 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietory information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 ---------------------------------------------------------------------------*/
package ets.arch.exception;

/**
*
* Modification Log
* ---------------------------------------------------------------------
* Ver   Date              Modified By               Description
* ---------------------------------------------------------------------
* 1.0   26 Jan 2008       Srinivasan.R             Initial Version
*
***********************************************************************/

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import ets.arch.util.ErrorXMLUtil;
import ets.domain.util.FPLogUtils;
/**
 * FPRuntimeException:All runtime exceptions of FP will throw an object of this type.
 * <br>This Class gets the details of the error thrown from xml
 * holding those values.
 * <br>An error is logged in the Log file with the error details.
 * @author Srinivasan.R
 * @version 1.0 26 Jan 2008
 * @since   Event Portal Iteration 0
 */
public class FPRuntimeException extends RuntimeException
{

   /*
   This will hold the error code
   */
   private String errorCode;

   /*
   This will hold the error message
   */
   private String errorMessage;

   /*
   This will hold the error cause
   */
   private String errorCause;

   /*
   This will hold the error recovery
   */
   private String errorRecovery;

   /*
   This will hold the error severity
   */
   private String errorSeverity;

   /**
    This is a constructor method which will initialize all the values.
   */
   public FPRuntimeException()
   {
   }

   /**
    * Constructor method which will set the value of the errorCode.
    * It will call methods to set the values of the other attributes.
    * <br>This Class gets the details of the error thrown from xml holding those values.
    * <br>An error is logged in the Log file with the error details.
    * @param errorCode The Error Code representing the error that has occured.
    * @param className The class here the error has occured.
    * @param methodName The method where the error has occured.
    * @param stackTrace The Stack trace of the error.
   */
   public FPRuntimeException(String errorCode, String className,
		   String methodName, Throwable stackTrace)
     {
      HashMap errortable = ErrorXMLUtil.getInstance().getExceptionMappings();
      ErrorDefinition errorDefinition = (ErrorDefinition)errortable.get(errorCode);
      setErrorCode(errorCode);
      setErrorMessage(errorDefinition.getErrorMessage());
      setErrorCause(errorDefinition.getErrorCause());
      setErrorRecovery(errorDefinition.getErrorRecovery());
      setErrorSeverity(errorDefinition.getErrorSeverity());
			logError(className, methodName,stackTrace);
     }

   /**
    * Constructor method which will set the value of the errorCode.
    * It will call methods to set the values of the other attributes.
    * <br>This Class gets the details of the error thrown from xml
holding those values.
    * <br>An error is logged in the Log file with the error details.
    * @param errorCode The Error Code representing the error that has occured.
    * @param className The class here the error has occured.
    * @param methodName The method where the error has occured.
    */
   public FPRuntimeException(String errorCode, String className, String methodName)
     {

      HashMap errortable = ErrorXMLUtil.getInstance().getExceptionMappings();
      ErrorDefinition errorDefinition = (ErrorDefinition)errortable.get(errorCode);
      setErrorCode(errorCode);
      setErrorMessage(errorDefinition.getErrorMessage());
      setErrorCause(errorDefinition.getErrorCause());
      setErrorRecovery(errorDefinition.getErrorRecovery());
      setErrorSeverity(errorDefinition.getErrorSeverity());

      logError(className, methodName);
     }


   /**
   * Returns the error code.
   * @return String errorCode
   */
   public String getErrorCode()
   {
         return errorCode;
   }

   /**
   * Returns the error message.
   * @return String errorMessage
   */
   public String getErrorMessage()
   {
         return errorMessage;
   }

   /**
   * Returns the error cause.
   * @return String errorCause
   */
   public String getErrorCause()
   {
         return errorCause;
   }

   /**
   * Returns the error recovery.
   * @return String errorRecovery
   */
   public String getErrorRecovery()
   {
       return errorRecovery;
   }

   /**
   * Returns the error severity.
   * @return String errorSeverity
   */
   public String getErrorSeverity()
   {
         return errorSeverity;
   }

   /**
   * Sets the value of errorMessage attribute.
   * @param String errorMessage
   */
   private void setErrorMessage(String errorMessage)
   {
      this.errorMessage = errorMessage;
   }

   /**
   * Sets the value of errorCause attribute.
   * @param String errorCause
   */
   private void setErrorCause(String errorCause)
   {
      this.errorCause = errorCause;
   }

   /**
   * Sets the value of errorSeverity attribute.
   * @param String errorRecovery
   */
   private void setErrorRecovery(String errorRecovery)
   {
      this.errorRecovery = errorRecovery;
   }

   /**
   * Sets the value of errorSeverity attribute.
   * @param String errorSeverity
   */
   private void setErrorSeverity(String errorSeverity)
   {
   this.errorSeverity = errorSeverity;
   }

   /**
   * Sets the value of errorCode attribute.
   * @param errorCode -Error code
   */
   public void setErrorCode(String errorCode)
   {
      this.errorCode = errorCode;
   }

  /**
   * Logs error into the log files.
   * @param className The class here the error has occured.
   * @param methodName The method where the error has occured.
   * @param stackTrace The Stack trace of the error.
   */
   public void logError(String className, String methodName, Throwable stackTrace)
  {
   //Writing into logger file.
   FPLogUtils.fpErrorLog("Run time exception in Class: "+ className +
		   " Method:"+methodName+" ", stackTrace);

  }

  /**
   * Logs error into the log files.
   * @param className The class here the error has occured.
   * @param methodName The method where the error has occured.
   */
   public void logError(String className, String methodName)
  {
   //Writing into logger file.
   FPLogUtils.fpErrorLog("Run time exception in Class: "+ className +
		   " Method:"+methodName+" ");
  }

  /**
   * Converts the stack trace into a string so that it can be logged.
   * @param stackTrace -stackTrace
   * @return String form of the stack trace
   *
   */
  public String getStackTrace(Throwable stackTrace)
    {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      stackTrace.printStackTrace(pw);
      pw.flush();
      return sw.toString();
    }
  /**
	 * @return errDetails
	 * author: Arivu_Selvaraj
	 */
	public String getErrorDetails()
	   {
		String errDetails = "Error Code:"+this.getErrorCode()+"Error Message:"+
		this.getErrorMessage()+" Error Cause:"+this.getErrorCause()+
		"Error Recovery:"+this.getErrorRecovery();
		return errDetails;
	   }
}
