<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
if(value=='GoTo'){
var temp=document.vendorDetail.GoTo.value;       
document.vendorDetail.pageNo.value=temp;
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageVendorPage.do';   
document.vendorDetail.submit();
}else if(value == "First"){
temp ="1";
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}else if(value == "Last"){
temp =document.vendorDetail.last.value;
document.vendorDetail.pageNo.value = temp; 
value='GoTo';
}
document.vendorDetail.button.value=value;
document.vendorDetail.action = '/throttle/manageVendorPage.do';   
document.vendorDetail.submit();
}else if (value=='add')
                {
                    
                    document.vendorDetail.action ='/throttle/addVendorPage.do';
                }else if(value == 'modify'){
                
                document.vendorDetail.action ='/throttle/';
            }
            document.vendorDetail.submit();
        }
    </script>
    
    <body>
        
        <form method="post" name="vendorDetail">
            <%@ include file="/content/common/path.jsp" %>
       

<%@ include file="/content/common/message.jsp" %>

            <% int index = 0;  
            int pag = (Integer)request.getAttribute("pageNo");
            index = ((pag -1) *10) +1 ;
                    %>
            <br>
            <c:if test = "${VendorList != null}" >
                <table width="90%" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

                    <tr align="center">
                        <td height="30" class="contentsub">S.No</td>
                        <td height="30" class="contentsub">Vendor Name</td>
                        <td height="30" class="contentsub">Vendor Type</td>
                        <td height="30" class="contentsub">Price Type</td>
                        <td height="30" class="contentsub">Credit Days</td>
                        <td height="30" class="contentsub">TIN NO</td>
                        <td height="30" class="contentsub">Vendor Address</td>
                        <td height="30" class="contentsub">Vendor PhoneNo</td>
                        <td height="30" class="contentsub">Vendor Mail Id</td>
                        <td height="30" class="contentsub">Vendor Is Cridatable</td>
                        <td height="30" class="contentsub">Status</td>
						<td class="contentsub"></td>
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${VendorList}" var="list"> 
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="40" > 
                            <td class="<%=classText %>" height="20"><%=index%></td>
                            <td class="<%=classText %>" height="20"><c:out value="${list.vendorName}"/></td>
                            <td class="<%=classText %>" height="20"><c:out value="${list.vendorTypeValue}"/></td>
                            <td class="<%=classText %>" height="20"><c:out value="${list.priceType}"/></td>
                            <td class="<%=classText %>" height="20"><c:out value="${list.creditDays}"/></td>
                            <td class="<%=classText %>" height="20"><c:out value="${list.tinNo}"/></td>
                             <td class="<%=classText %>" height="20"><c:out value="${list.vendorAddress}"/></td>
                              <td class="<%=classText %>" height="20"><c:out value="${list.vendorPhoneNo}"/></td>
                               <td class="<%=classText %>" height="20"><c:out value="${list.vendorMailId}"/></td>
                               
                             
                             <td class="<%=classText %>" height="20">                                
                                <c:if test = "${list.settlementType == '1' }" >
                                    No
                                </c:if>
                                <c:if test = "${list.settlementType == '2'}" >
                                    Yes
                                </c:if>
                            </td>
                            <td class="<%=classText %>" height="30">                                
                                <c:if test = "${list.activeInd == 'Y' }" >
                                    Active 
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                             <td class="<%=classText %>" align="left"> <a href="/throttle/alterVendorPage.do?vendorId=<c:out value='${list.vendorId}' />" >Alter </a> </td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 

<table align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
 <%@ include file="/content/common/pagination.jsp"%>  
 </td>
 
</tr>
</table>
            <center>
                <br>
                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">
                <c:if test = "${VendorList != null}" >
                   <!-- <input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="button">-->
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

