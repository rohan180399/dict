<%--
    Document   : VehicleAccidentDetails
    Created on : 19 Mar, 2012, 4:38:51 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.http.*" %>
        <title>Vehicle Accident Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            if(detail != "null"){
                                var vehicleValues = detail.split("~");
                                document.accidentVehicle.vehicleId.value = vehicleValues[0];
                                document.accidentVehicle.chassisNo.value = vehicleValues[1];
                                document.accidentVehicle.engineNo.value = vehicleValues[2];
                                document.accidentVehicle.vehicleMake.value = vehicleValues[4];
                                document.accidentVehicle.vehicleModel.value = vehicleValues[5];
                            }else{
                                document.accidentVehicle.vehicleId.value = "";
                                document.accidentVehicle.chassisNo.value = "";
                                document.accidentVehicle.engineNo.value =  "";
                                document.accidentVehicle.vehicleMake.value =  "";
                                document.accidentVehicle.vehicleModel.value =  "";

                            }

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }



                }
            }
            function getGpsMap()
            {
                if(document.getElementById("regno").value == ""){
                alert("Enter Regster Number:");
                return false;
                }if (document.accidentVehicle.accidentDate.value == ""){
                alert("Enter Date:");
                return false;
                }
                var regno1 = document.getElementById("regno").value;
                var accidentDate1 = document.accidentVehicle.accidentDate.value;
                var ad = accidentDate1.split("-");
                var accidentDate = ad[1]+"/"+ad[0]+"/"+ad[2];
                var rno = regno1.split(" ");
                var regno = rno[0]+"%20"+rno[1]+"%20"+rno[2];
                  var URL ="http://124.153.106.52/EILTPLUS/Panic/PanicInfoMap.aspx?Vehno="+regno+"&Veh_Panic_Date="+accidentDate+"&Veh_Panic_Time=10:10";
                NewWindow = window.open(URL,"_blank","toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=1500,Height=760") ;
                NewWindow.location = URL;
            }
            function saveToSubmit(){
                var message = "";
                if(document.getElementById("regno").value == ""){
                    message += "Enter Vehicle No \n";
                    //document.getElementById("regno").focus();
                }if (document.accidentVehicle.accidentDate.value == ""){
                    message += "Enter Accident Date \n";
                    // document.accidentVehicle.accidentDate.focus();
                }if (document.accidentVehicle.accidentSpot.value == ""){
                    message += "Enter Accident Spot \n";
                    //document.accidentVehicle.accidentSpot.focus();
                }if (document.accidentVehicle.driverName.value == ""){
                    message += "Enter Driver Name \n";
                    // document.accidentVehicle.driverName.focus();
                }if (document.accidentVehicle.policeStation.value == ""){
                    message += "Enter Police Station \n";
                    document.accidentVehicle.policeStation.focus();
                }if (document.accidentVehicle.firNo.value == ""){
                    message += "Enter FIR Number \n";
                    //document.accidentVehicle.firNo.focus();
                }if (document.accidentVehicle.firDate.value == ""){
                    message += "Enter FIR Date \n";
                    // document.accidentVehicle.firDate.focus();
                }if (document.accidentVehicle.firPreparedBy.value == ""){
                    message += "Enter FIR Prepared by \n";
                    // document.accidentVehicle.firPreparedBy.focus();
                }if (document.accidentVehicle.accidentRemarks.value == ""){
                    message += "Enter Accident Remarks \n";
                    // document.accidentVehicle.accidentRemarks.focus();
                }

                //alert(message);

                if(message == ""){
                    var saveFlag = document.getElementById("saveFlag").value;
                    if(saveFlag == "true"){
                        document.accidentVehicle.action="/throttle/saveVehicleAccident.do";
                        document.accidentVehicle.submit();
                    }  else {
                        document.accidentVehicle.action="/throttle/updateVehicleAccident.do";
                        document.accidentVehicle.submit();
                    }
                } else {
                    alert(message);
                }

            }

            function getEvents(e,val){
                var key;
                if(window.event){
                    key = window.event.keyCode;
                }else {
                    key = e.which;
                }
                if(key == 0) {
                    getVehicleDetails(val);
                }else{
                    getVehicleDetails(val);
                }
            }

        </script>

    </head>

    <body onLoad="getVehicleNos(); ">
        <form name="accidentVehicle" method="post" >

            <h2 align="center">Vehicle Accident Details</h2>

            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <c:if test="${accidentVehicleList != null}">
                    <c:forEach items="${accidentVehicleList}" var="avl">
                        <tr>
                            <td class="texttitle1">Vehicle No</td>
                            <td class="text1"><input type="text" name="regNo" id="regno" class="form-control" onblur="getVehicleDetails(this.value);" value="<c:out value="${avl.regno}"/>"/><input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${avl.vehicleId}"/>" /></td>
                            <td class="texttitle1">Make</td>
                            <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value="<c:out value="${avl.mfrName}"/>" readonly="true" disabled="true"/></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Model</td>
                            <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value="<c:out value="${avl.modelName}" />" readonly="true" disabled="true"></td>
                            <td class="texttitle2">Usage</td>
                            <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly/></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Engine No</td>
                            <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" value="<c:out value="${avl.engineNo}"/>" readonly /></td>
                            <td class="texttitle1">Chassis No</td>
                            <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" value="<c:out value="${avl.chassisNo}"/>" readonly /></td>
                        </tr>

                        <tr>
                            <td class="contenthead" colspan="4">Accident Details</td>
                        </tr>

                        <tr>
                            <td class="texttitle1">Accident Date</td>
                            <td class="text1"><input type="text" name="accidentDate" id="accidentDate" class="datepicker" value="<c:out value="${avl.accidentDate}"/>" /></td>
                            <td class="texttitle1">Accident Spot</td>
                            <td class="text1"><input type="text" name="accidentSpot" id="accidentSpot" class="form-control" value="<c:out value="${avl.accidentSpot}"/> " onclick="getVehicleDetails(regno.value);" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Driver Name</td>
                            <td class="text2"><input type="text" name="driverName" id="driverName" class="form-control" value="<c:out value="${avl.driverName}"/>"  onclick="getVehicleDetails(regno.value);"/></td>
                            <td class="texttitle2">Police Station</td>
                            <td class="text2"><input type="text" name="policeStation" id="policeStation" class="form-control" value="<c:out value="${avl.policeStation}"/>" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">FIR No</td>
                            <td class="text1"><input type="text" name="firNo" id="firNo" class="form-control" value="<c:out value="${avl.firNo}"/>" /></td>
                            <td class="texttitle1">FIR Date</td>
                            <td class="text1"><input type="text" name="firDate" id="firDate" value="<c:out value="${avl.firDate}"/>" class="datepicker" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.accidentVehicle.firDate,'dd-mm-yyyy',this)"/> --></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">FIR Prepared By</td>
                            <td class="text2"><input type="text" name="firPreparedBy" id="firPreparedBy" class="form-control" value="<c:out value="${avl.firPreparedBy}"/>" /></td>
                            <td class="texttitle2">Remarks</td>
                            <td class="text2"><input type="text" name="accidentRemarks" id="accidentRemarks" class="form-control" value="<c:out value="${avl.accidentRemarks}"/>" /><input type="hidden" name="saveFlag" id="saveFlag" value="false"></td>

                        </tr>

                    </c:forEach>
                </c:if>
                <c:if test="${accidentVehicleList == null}">

                    <tr>
                        <td class="texttitle1">Vehicle No</td>
                        <td class="text1">
                            <input type="text" name="regNo" id="regno" class="form-control" onkeypress="getEvents(event,this.value);" onclick="getVehicleDetails(this.value);" autocomplete="off" />
                            <!--                            <input type="text" name="regNo" id="regno" class="form-control"  onchange="getVehicleDetails(this.value);" onkeypress="getVehicleDetails(this.value);" autocomplete="off" />-->
                            <input type="hidden" name="vehicleId" id="vehicleId" /></td>
                        <td class="texttitle1">Make</td>
                        <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="texttitle2">Model</td>
                        <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" /></td>
                        <td class="texttitle2">Usage</td>
                        <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="texttitle1">Engine No</td>
                        <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" /></td>
                        <td class="texttitle1">Chassis No</td>
                        <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" /></td>
                    </tr>

                    <tr>
                        <td class="contenthead" colspan="4">Accident Details</td>
                    </tr>
                    <tr>
                        <td class="texttitle1">Accident Date</td>
                        <td class="text1"><input type="text" name="accidentDate" id="accidentDate" class="datepicker" />&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.accidentVehicle.accidentDate,'dd-mm-yyyy',this);getVehicleDetails(regno.value);"/></td>
                        <td class="texttitle1">Accident Spot</td>
                        <td class="text1"><input type="text" name="accidentSpot" id="accidentSpot" class="form-control" onclick="getVehicleDetails(regno.value);" /></td>
                    </tr>
                    <tr>
                        <td class="texttitle2">Driver Name</td>
                        <td class="text2"><input type="text" name="driverName" id="driverName" class="form-control" onclick="getVehicleDetails(regno.value);" /></td>
                        <td class="texttitle2">Police Station</td>
                        <td class="text2"><input type="text" name="policeStation" id="policeStation" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td class="texttitle1">FIR No</td>
                        <td class="text1"><input type="text" name="firNo" id="firNo" class="form-control" /></td>
                        <td class="texttitle1">FIR Date</td>
                        <td class="text1"><input type="text" name="firDate" id="firDate" class="datepicker" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.accidentVehicle.firDate,'dd-mm-yyyy',this)"/> --></td>
                    </tr>
                    <tr>
                        <td class="texttitle2">FIR Prepared By</td>
                        <td class="text2"><input type="text" name="firPreparedBy" id="firPreparedBy" class="form-control" /></td>
                        <td class="texttitle2">Remarks</td>
                        <td class="text2"><input type="text" name="accidentRemarks" id="accidentRemarks" class="form-control" /><input type="hidden" name="saveFlag" id="saveFlag" value="true"></td>

                    </tr>
                </c:if>
            </table>
            <br>
            <center>
                <!--            Upload Copy of Documents(if Any)&emsp;<input type="file" />
                                <br>
                                <br>-->
                <input type="submit" class="button" onclick="saveToSubmit()" value=" Update " />
                <input type="button" class="button" onclick="getGpsMap()" value=" Accident Spot " />
                <input type="reset" class="button" value=" Clear " />
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script>
        function getVehicleNos(){

            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            //getVehicleDetails(document.getElementById("regNo"));

        }

    </script>
</html>