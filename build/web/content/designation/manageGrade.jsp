<%-- 
Document   : gradeManage
Created on : Oct 23, 2008, 1:38:00 PM
Author     : vidya
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

        
        <title>Bharath</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

    
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.gradeDetail.action='/throttle/addGrade.do';
                }
                else if(value == 'modify'){
                    document.gradeDetail.action='/throttle/alterGrade.do';
                }
                document.gradeDetail.submit();
            } 
    </script>
    <script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    
    <!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Grade" text="default text"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class=""><spring:message code="hrms.label.Designation" text="default text"/> </li>
          <li class=""><spring:message code="hrms.label.Grade" text="default text"/></li>



        </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
    <body>
        
        <form  method="post" name="gradeDetail">                              
            <table class="table table-info mb30 table-hover" >
                <thead>
                <tr>
                    <th  height="30" colspan="2"><strong><spring:message code="hrms.label.DesignationName" text="default text"/></strong></th>
                    <th  height="30" colspan="2"><strong><%= request.getAttribute("desigName") %></strong></th>
                </tr>
                </thead>
                <c:if test = "${GradeList != null}" >
                    <thead>
                    <tr >
                        <th  height="30"><div ><spring:message code="hrms.label.SNo" text="default text"/></div></th>
                        <th  height="30"><div ><spring:message code="hrms.label.GradeName" text="default text"/></div></th>
                        <th  height="30"><div ><spring:message code="hrms.label.GradeDescription" text="default text"/></div></th>
                        <th  height="30"><div ><spring:message code="hrms.label.Status" text="default text"/></div></th>
                    </tr>
                    </thead>
                    <% int index = 0;

                    %>
                    <c:forEach items="${GradeList}" var="list"> 		  
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	   
                        <tr>
                            <td  height="30"><%=index + 1%></td>
                            <td  height="30"><c:out value="${list.gradeName}"/></td>
                            <td  height="30"><c:out value="${list.description}"/></td>
                            <td  height="30">
                                
                                
                                <c:if test = "${list.activeInd == 'Y' || list.activeInd == 'y'}"  >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                </c:if> 
                <input type="hidden" name="designationId" value='<%= request.getAttribute("DesigId") %>'>
            </table>
            <center>
                <input type="button" name="add" value="<spring:message code="hrms.label.NewGrade" text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success">
                <c:if test = "${GradeList != null}" >
                    <input type="button" name="modify" value="<spring:message code="hrms.label.ALTER" text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success">
                </c:if> 
                <input type="hidden" name="reqfor" value="">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
