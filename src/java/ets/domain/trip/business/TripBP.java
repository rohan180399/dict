/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.SendMail;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.data.TripDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import javax.mail.Message;
import javax.mail.MessagingException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripBP {

    /**
     * Creates a new instance of __NAME__
     */
    public TripBP() {
    }
    TripDAO tripDAO;

    public TripDAO getTripDAO() {
        return tripDAO;
    }

    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }

    public ArrayList getConsignmentList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = tripDAO.getConsignmentList(tripTO);
        return consignmentList;
    }

    public ArrayList getExpiryDateDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expiryDetails = new ArrayList();
        expiryDetails = tripDAO.getExpiryDateDetails(tripTO);
        return expiryDetails;
    }

    public int updateTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripOtherExpense(tripSheetId, vehicleId, expenseName, employeeName, expenseType, expenseDate, expenseHour, expenseMinute, taxPercentage,
                expenseRemarks, expenses, currency, netExpense, billMode, marginValue, expenseId, activeValue, userId);
        return status;
    }

    public int deleteTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteTripOtherExpense(tripSheetId, vehicleId, expenseName, employeeName, expenseType, expenseDate, expenseHour, expenseMinute, taxPercentage,
                expenseRemarks, expenses, currency, netExpense, billMode, marginValue, expenseId, activeValue, userId);
        return status;
    }

    public ArrayList getTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsToBeBilledDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsToBeBilledDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getSecTripsToBeBilledDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getSecTripsToBeBilledDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripsOtherExpenseDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceHeader(tripTO);
        return result;
    }

    public ArrayList getInvoiceDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceDetails(tripTO);
        return result;
    }

    public ArrayList getInvoiceDetailExpenses(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceDetailExpenses(tripTO);
        return result;
    }

    public ArrayList getInvoiceSupplementDetailExpenses(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getInvoiceSupplementDetailExpenses(tripTO);
        return result;
    }

    public ArrayList getTripPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPointDetails = new ArrayList();
        tripPointDetails = tripDAO.getTripPointDetails(tripTO);
        return tripPointDetails;
    }

    public ArrayList getOrderPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPointDetails = new ArrayList();
        tripPointDetails = tripDAO.getOrderPointDetails(tripTO);
        return tripPointDetails;
    }

    public ArrayList getVehicleTypeContainerTypeRates(ArrayList consignmentList, ArrayList orderPointDetails) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleTypeContainerTypeRates = new ArrayList();
        vehicleTypeContainerTypeRates = tripDAO.getVehicleTypeContainerTypeRates(consignmentList, orderPointDetails);
        return vehicleTypeContainerTypeRates;
    }

    public ArrayList getHubPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList hubPointDetails = new ArrayList();
        hubPointDetails = tripDAO.getHubPointDetails(tripTO);
        return hubPointDetails;
    }

    public ArrayList getDropPointDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList dropPointDetails = new ArrayList();
        dropPointDetails = tripDAO.getDropPointDetails(tripTO);
        return dropPointDetails;
    }

    public ArrayList getwareHouseDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList wareHouseDetails = new ArrayList();
        wareHouseDetails = tripDAO.getwareHouseDetails(tripTO);
        return wareHouseDetails;
    }

    public ArrayList getPortDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList portDetails = new ArrayList();
        portDetails = tripDAO.getPortDetails(tripTO);
        return portDetails;
    }

    public String getConsignmentOrderRevenue(String orderId) throws FPBusinessException, FPRuntimeException {
        String revenue = "";
        revenue = tripDAO.getConsignmentOrderRevenue(orderId);
        return revenue;
    }

    public String getVehicleMileageAndTollRate(String vehicleTypeId) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = tripDAO.getVehicleMileageAndTollRate(vehicleTypeId);
        return response;
    }

    public String getEstimatedTripEndDateTime(String tripId) throws FPBusinessException, FPRuntimeException {
        String response = "";
        response = tripDAO.getEstimatedTripEndDateTime(tripId);
        return response;
    }

    public String checkPreStartRoute(String preStartLocationId, String originId, String vehicleTypeId) throws FPBusinessException, FPRuntimeException {
        String info = "";
        info = tripDAO.checkPreStartRoute(preStartLocationId, originId, vehicleTypeId);
        return info;
    }

    public ArrayList getVehicleRegNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNos(tripTO);
        return vehicleNos;
    }

    public ArrayList getTrailerNos(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getTrailerNos(tripTO);
        return vehicleNos;
    }

    public ArrayList getTrailerNos() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getTrailerNos();
        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForEmptyTrip(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNosForEmptyTrip(tripTO);
        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForUpload(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVehicleRegNosForUpload(tripTO);
        return vehicleNos;
    }

    public ArrayList getDrivers(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList drivers = new ArrayList();
        drivers = tripDAO.getDrivers(tripTO);
        return drivers;
    }

    public String getConsignmentOrderExpense(String orderId) throws FPBusinessException, FPRuntimeException {
        String expense = "";
        expense = tripDAO.getConsignmentOrderExpense(orderId);
        return expense;
    }

    public int saveAction(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveAction(tripTO);
        return status;
    }

    public int saveClosureApprovalRequest(String tripId, String requestType, String remarks, int userId, String rcmExp, String nettExp) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveClosureApprovalRequest(tripId, requestType, remarks, userId, rcmExp, nettExp);
        return status;
    }

    public int saveBillingRevenueChangeApprovalRequest(String customerId, String requestType, String remarks, int userId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveBillingRevenueChangeApprovalRequest(customerId, requestType, remarks, userId);
        return status;
    }

    public int saveTripUpdate(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.saveTripUpdate(tripTO);

        //if action is freeeze update prestart plan info
        if ("1".equals(tripTO.getActionName())
                && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
            status = tripDAO.updatePreStartDetails(tripTO);
        } else if ("2".equals(tripTO.getActionName())) {//unfreeze
            //remove vehicle and driver mapping
            status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        }

        //update vehicle info
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            status = tripDAO.saveTripVehicle(tripTO);
        }
        //update driver info
        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
        }

        //if pre start route is given add one point location
        status = tripDAO.saveTripRoutePlanWhenTripUpdate(tripTO);

        return status;
    }

    public int saveTripSheetForVehicleChange(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;

        //reset vehicle and driver mapping
        status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        //update vehicle info
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            status = tripDAO.saveTripVehicleForVehicleChange(tripTO);
        }
        //update driver info
        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
        }

        return status;
    }

    public String getInvoiceCodeSequence() {
        String codeSequence = "";
        codeSequence = tripDAO.getInvoiceCodeSequence();
        // generate Invoice code
        if (codeSequence.length() == 1) {
            codeSequence = "000000" + codeSequence;
            //System.out.println("invoice no 1.." + codeSequence);
        } else if (codeSequence.length() == 2) {
            codeSequence = "00000" + codeSequence;
            //System.out.println("invoice lenght 2.." + codeSequence);
        } else if (codeSequence.length() == 3) {
            codeSequence = "0000" + codeSequence;
            //System.out.println("invoice lenght 3.." + codeSequence);
        } else if (codeSequence.length() == 4) {
            codeSequence = "000" + codeSequence;
        } else if (codeSequence.length() == 5) {
            codeSequence = "00" + codeSequence;
        } else if (codeSequence.length() == 6) {
            codeSequence = "0" + codeSequence;
        }

        return codeSequence;
    }

    public String getCnoteCodeSequence() {
        String codeSequence = "";
        codeSequence = tripDAO.getCnoteCodeSequence();
        return codeSequence;
    }

    public int saveTripSheet(TripTO tripTO) throws FPBusinessException, FPRuntimeException, IOException, Exception {
//      , trailerId, trailerRemarks, containerTypeId, containerNo, remainWeight, advanceGrNo, uniqueId  
// , String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String remainWeight, String advanceGR, String[] uniqueId
        int tripId = 0;
        int status = 0;
        String[] containerTypeId = null;
        String[] containerNo = null;
        String[] uniqueId = null;
        String sealNo[] = null;
        String grStatus[] = null;
        SqlMapClient session = tripDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            session.startTransaction();
            if (!"".equals(tripTO.getContainerTypeId())) {
                //System.out.println("if....");
                //                if((containerTypeIdTemp1.contains(","))&&(containerNoTemp1.contains(","))){
                containerTypeId = tripTO.getContainerTypeId().split(",");
                containerNo = tripTO.getContainerNo().split(",");
                uniqueId = tripTO.getUniqueId().split(",");
                sealNo = tripTO.getSealNo().split(",");
                grStatus = tripTO.getGrStatus().split(",");
                status = tripDAO.updatePlannedStatus(tripTO, containerTypeId, containerNo, uniqueId, tripTO.getIsRepo(), sealNo, grStatus, session);
            } else {
                //System.out.println("else....");
                containerTypeId = tripTO.getContainerTypeIds();
                containerNo = tripTO.getContainerNos();
            }
            String tripStatus = "";
            //generate tripcode
//            String tripCode = "TS/22-23/";
            String tripCode = "TS/23-24/";
            String tripCodeSequence = tripDAO.getTripCodeSequence(session);
            tripCode = tripCode + tripCodeSequence;
            tripTO.setTripCode(tripCode);

            //set transit days
            float transitHours = 0.00F;
            if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
                transitHours = Float.parseFloat(tripTO.getTripTransitHours());
            }
            double transitDay = (double) transitHours / 24;
            tripTO.setTripTransitDays(transitDay + "");
            //set advance to be paid per day
            float totalExpense = 0.00f;
            if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
                totalExpense = Float.parseFloat(tripTO.getOrderExpense());
            }

            int transitDayValue = (int) transitDay;
            if (transitDay > transitDayValue) {
                transitDay = transitDayValue + 1;
            }
            double advnaceToBePaidPerDay = 0;

            if (totalExpense > 0 && transitHours > 0) {
                advnaceToBePaidPerDay = totalExpense / transitDay;
            }
            tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

            //set trip status
            if ("1".equals(tripTO.getActionName())) { //freeze status
                tripTO.setStatusId("8");
                tripStatus = "Freezed";

            } else if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                tripTO.setStatusId("7");
                tripStatus = "Trip Vehicle Alloted";
            } else {
                tripTO.setStatusId("6");
                tripStatus = "Trip Created";

            }

            //create trip master
            tripId = tripDAO.saveTripSheet(tripTO, session);
            //create trip vehicles

            if (tripId > 0) {
                tripTO.setTripId(tripId + "");

                status = tripDAO.saveTripExpense(tripTO, session);

                if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                    status = tripDAO.saveTripVehicle(tripTO, session);
                }
                status = tripDAO.saveTrailerDetails(tripTO.getTrailerIds(), tripTO.getTrailerRemarks(), containerTypeId, containerNo, uniqueId, tripTO, session);

                //if action is freeeze update prestart plan info
                if ("1".equals(tripTO.getActionName())
                        && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
                    status = tripDAO.updatePreStartDetails(tripTO, session);
                }
                //create trip status details
                status = tripDAO.saveTripStatusDetails(tripTO, session);
                //create trip consignment order
                status = tripDAO.saveTripConsignments(tripTO, tripTO.getRemainWeight(), session);
                //create trip driver
                if (tripTO.getPrimaryDriverId() != null && !"".equals(tripTO.getPrimaryDriverId())) {
                    status = tripDAO.saveTripDriver(tripTO, session);
                }
                //crate trip points
                status = tripDAO.saveTripRoutePlan(tripTO, session);
                // Trip Challan Table Entry   mandatory for all type of order
                String challanNo = "";
                challanNo = tripDAO.getTripChallanSequence(session);
                String challanInsert = tripDAO.insertTripChallan(String.valueOf(tripId), tripTO.getUserId(), challanNo, session);
                // Gr Table Entry
                String grNo = "", repoGRNo = "";
                //System.out.println("tripTO.getAdvanceGrNo():" + tripTO.getAdvanceGrNo() + "tripTO.getExcelGrNo():" + tripTO.getExcelGrNo());
                if (("".equals(tripTO.getAdvanceGrNo()) || tripTO.getAdvanceGrNo() == null) && (tripTO.getExcelGrNo() == null || "".equalsIgnoreCase(tripTO.getExcelGrNo()))) {

                    if ("3".equals(tripTO.getMovementType())) {
                        if ("2".equals(tripTO.getVehicleCategory())) {

                            grNo = "0";
                        } else {
                            grNo = tripDAO.getRepoGrNoSequence(session);
                            //System.out.println("repo Gr No:" + grNo);
                            if (grNo.length() == 1) {
                                grNo = "E-000000" + grNo;
                                //System.out.println("grno 1.." + grNo);
                            } else if (grNo.length() == 2) {
                                grNo = "E-00000" + grNo;
                                //System.out.println("grno lenght 2.." + grNo);
                            } else if (grNo.length() == 3) {
                                grNo = "E-0000" + grNo;
                                //System.out.println("grno lenght 3.." + grNo);
                            } else if (grNo.length() == 4) {
                                grNo = "E-000" + grNo;
                            } else if (grNo.length() == 5) {
                                grNo = "E-00" + grNo;
                            } else if (grNo.length() == 6) {
                                grNo = "E-0" + grNo;
                            }
                        }
                    } else {
                        grNo = tripDAO.getGrNoSequence(session);
                        if (grNo.length() == 1) {
                            grNo = "000000" + grNo;
                            //System.out.println("grno 1.." + grNo);
                        } else if (grNo.length() == 2) {
                            grNo = "00000" + grNo;
                            //System.out.println("grno lenght 2.." + grNo);
                        } else if (grNo.length() == 3) {
                            grNo = "0000" + grNo;
                            //System.out.println("grno lenght 3.." + grNo);
                        } else if (grNo.length() == 4) {
                            grNo = "000" + grNo;
                        } else if (grNo.length() == 5) {
                            grNo = "00" + grNo;
                        } else if (grNo.length() == 6) {
                            grNo = "0" + grNo;
                        }
                    }
                    String tripGrNo = tripDAO.insertTripGr(String.valueOf(tripId), tripTO.getUserId(), grNo, session);
                    //System.out.println("tripGrNo:" + tripGrNo);
                } else if (tripTO.getExcelGrNo() != null && !"".equalsIgnoreCase(tripTO.getExcelGrNo())) {
                    String tripGrNo = tripDAO.insertTripUploadGr(String.valueOf(tripId), tripTO.getUserId(), tripTO.getExcelGrNo(), tripTO.getGrDate(), session);
                } else {
                    grNo = tripTO.getAdvanceGrNo();
                    int grUpdate = tripDAO.updatetripGR(String.valueOf(tripId), tripTO.getUserId(), grNo, session);
                }
                //System.out.println("gr no in Bp:" + grNo);
                //System.out.println("status...." + status);

                tripTO.setTripId(String.valueOf(tripId));
                //////Start Fuel Bank Save//////
                int flstatus = 0;
                Double totalFuel = 0.0d;
                if (tripTO.getFuelDates() != null) {
                    //System.out.println("444444");
                    //System.out.println("444444-----" + tripTO.getFuelDates().length);
                    for (int i = 0; i < tripTO.getFuelDates().length; i++) {
                        if (tripTO.getFuelAmounts()[i] != null) {
                            //System.out.println("bunkName[i]---$$$$$$$$$$$$$$$$$$$$$$$$$$$---AS = " + tripTO.getBunkNames()[i]);
                            totalFuel = totalFuel + Double.parseDouble(tripTO.getFuelAmounts()[i]);
                            //System.out.println("totalFuel:" + totalFuel);
                            flstatus = tripDAO.insertTripFuel(tripTO.getFuelType(), tripTO.getUserId(), "0", Integer.parseInt(tripTO.getTripId()), tripTO.getBunkNames()[i], tripTO.getBunkPlace()[i], tripTO.getFuelDates()[i], tripTO.getFuelAmounts()[i],
                                    tripTO.getFuelLtrs()[i], tripTO.getFuelRemark()[i], tripTO.getDriverNameId(), tripTO.getSlipNo()[i], tripTO.getFuelUniqueId()[i], tripTO.getVehicleId(), tripTO.getHireVehicleNo(), session);
                        }
                    }
                }

                int manualAdvanceRequest = 0;
                //  manualAdvanceRequest = tripDAO.manualAdvanceRequest(tripTO, tripTO.getUserId(),session);
                if (manualAdvanceRequest > 0 || flstatus > 0) {
                    String advanceVoucherNoSequence = getAdvanceVoucherNoSequence(tripTO, session);
                }
                int insertTripCash = 0;
           //     String paidCash = String.valueOf(Double.parseDouble(tripTO.getAdvancerequestamt()));
                //            //System.out.println("paidCash:" + paidCash);
//                if (!"0".equals(tripTO.getAdvancerequestamt()) && tripTO.getAdvancerequestamt() != null) {
//                    insertTripCash = tripDAO.saveTripCash(tripTO, paidCash, tripTO.getUserId(),session);
//                    //System.out.println("insertTripCash:" + insertTripCash);
//                }
                //System.out.println("manualAdvanceRequest" + manualAdvanceRequest);
            }

            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            ////System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                ////System.out.println("am here 8" + ex.getMessage());
            } finally {
                if (session.getCurrentConnection() != null) {
                    session.getCurrentConnection().close();
                }

                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return tripId;
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO, SqlMapClient session) {
        String codeSequence = "";
        codeSequence = tripDAO.getAdvanceVoucherNoSequence(tripTO, session);
        // generate Invoice code
        if (codeSequence.length() == 1) {
            codeSequence = "000000" + codeSequence;
            //System.out.println("AdvanceVoucher no 1.." + codeSequence);
        } else if (codeSequence.length() == 2) {
            codeSequence = "00000" + codeSequence;
            //System.out.println("AdvanceVoucherNo lenght 2.." + codeSequence);
        } else if (codeSequence.length() == 3) {
            codeSequence = "0000" + codeSequence;
            //System.out.println("AdvanceVoucherNo lenght 3.." + codeSequence);
        } else if (codeSequence.length() == 4) {
            codeSequence = "000" + codeSequence;
        } else if (codeSequence.length() == 5) {
            codeSequence = "00" + codeSequence;
        } else if (codeSequence.length() == 6) {
            codeSequence = "0" + codeSequence;
        }

        return codeSequence;
    }

    public int saveTripAdavnce(TripTO tripTO) throws FPBusinessException, FPRuntimeException, IOException, SQLException {
        int manualAdvanceRequest = 0;

        SqlMapClient session = tripDAO.getSqlMapClient();
        session.startTransaction();
        manualAdvanceRequest = tripDAO.manualAdvanceRequest(tripTO, tripTO.getUserId(), session);
        session.commitTransaction();
        return manualAdvanceRequest;
    }

    public int saveTripSheetOLD(TripTO tripTO, String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String remainWeight, String advanceGR, String[] uniqueId) throws FPBusinessException, FPRuntimeException, IOException {
        int status = 0;
        String tripStatus = "";
        //generate tripcode
        String tripCode = "TS/15-16/";
        String tripCodeSequence = tripDAO.getTripCodeSequence();
        tripCode = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);

        //set transit days
        float transitHours = 0.00F;
        if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
            transitHours = Float.parseFloat(tripTO.getTripTransitHours());
        }
        double transitDay = (double) transitHours / 24;
        tripTO.setTripTransitDays(transitDay + "");
        //set advance to be paid per day
        float totalExpense = 0.00f;
        if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
            totalExpense = Float.parseFloat(tripTO.getOrderExpense());
        }

        int transitDayValue = (int) transitDay;
        if (transitDay > transitDayValue) {
            transitDay = transitDayValue + 1;
        }
        double advnaceToBePaidPerDay = 0;

        if (totalExpense > 0 && transitHours > 0) {
            advnaceToBePaidPerDay = totalExpense / transitDay;
        }
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        //set trip status
        if ("1".equals(tripTO.getActionName())) { //freeze status
            tripTO.setStatusId("8");
            tripStatus = "Freezed";

        } else if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            tripTO.setStatusId("7");
            tripStatus = "Trip Vehicle Alloted";
        } else {
            tripTO.setStatusId("6");
            tripStatus = "Trip Created";

        }
        //create trip master
        int tripId = tripDAO.saveTripSheet(tripTO);
        //create trip vehicles

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                status = tripDAO.saveTripVehicle(tripTO);
            }
            status = tripDAO.saveTrailerDetails(trailerId, trailerRemarks, containerTypeId, containerNo, uniqueId, tripTO);

            //if action is freeeze update prestart plan info
            if ("1".equals(tripTO.getActionName())
                    && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
                status = tripDAO.updatePreStartDetails(tripTO);
            }
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO, remainWeight);
            //create trip driver
            if (tripTO.getPrimaryDriverId() != null && !"".equals(tripTO.getPrimaryDriverId())) {
                status = tripDAO.saveTripDriver(tripTO);
            }
            //crate trip points
            status = tripDAO.saveTripRoutePlan(tripTO);
            // Trip Challan Table Entry   mandatory for all type of order
            String challanNo = "";
            challanNo = tripDAO.getTripChallanSequence();
            String challanInsert = tripDAO.insertTripChallan(String.valueOf(tripId), tripTO.getUserId(), challanNo);
            // Gr Table Entry
            String grNo = "", repoGRNo = "";
            if ("".equals(advanceGR) || advanceGR == null) {

                if ("3".equals(tripTO.getMovementType())) {
                    if ("2".equals(tripTO.getVehicleCategory())) {

                        grNo = "0";
                    } else {
                        grNo = tripDAO.getRepoGrNoSequence();
                        //System.out.println("repo Gr No:" + grNo);
                        if (grNo.length() == 1) {
                            grNo = "E-000000" + grNo;
                            //System.out.println("grno 1.." + grNo);
                        } else if (grNo.length() == 2) {
                            grNo = "E-00000" + grNo;
                            //System.out.println("grno lenght 2.." + grNo);
                        } else if (grNo.length() == 3) {
                            grNo = "E-0000" + grNo;
                            //System.out.println("grno lenght 3.." + grNo);
                        } else if (grNo.length() == 4) {
                            grNo = "E-000" + grNo;
                        } else if (grNo.length() == 5) {
                            grNo = "E-00" + grNo;
                        } else if (grNo.length() == 6) {
                            grNo = "E-0" + grNo;
                        }
                    }
                } else {
                    grNo = tripDAO.getGrNoSequence();
                    if (grNo.length() == 1) {
                        grNo = "000000" + grNo;
                        //System.out.println("grno 1.." + grNo);
                    } else if (grNo.length() == 2) {
                        grNo = "00000" + grNo;
                        //System.out.println("grno lenght 2.." + grNo);
                    } else if (grNo.length() == 3) {
                        grNo = "0000" + grNo;
                        //System.out.println("grno lenght 3.." + grNo);
                    } else if (grNo.length() == 4) {
                        grNo = "000" + grNo;
                    } else if (grNo.length() == 5) {
                        grNo = "00" + grNo;
                    } else if (grNo.length() == 6) {
                        grNo = "0" + grNo;
                    }
                }
                String tripGrNo = tripDAO.insertTripGr(String.valueOf(tripId), tripTO.getUserId(), grNo);
                //System.out.println("tripGrNo:" + tripGrNo);
            } else {
                grNo = advanceGR;
                int grUpdate = tripDAO.updatetripGR(String.valueOf(tripId), tripTO.getUserId(), grNo);
            }
            //System.out.println("gr no in Bp:" + grNo);
            status = tripId;
            //System.out.println("status...." + status);
            //////////////////Arun Start//////////////////////////
            if ("8".equals(tripTO.getStatusId())) { // freezed
                String to = "";
                String cc = "";
                String smtp = "";
                int emailPort = 0;
                String frommailid = "";
                String password = "";
                String activitycode = "EMTRP1";

                ArrayList emaildetails = new ArrayList();
                emaildetails = getEmailDetails(activitycode);

                String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
                //System.out.println("emailString:" + emailString);
                String[] emailTemp = emailString.split("~");
                int emailTempLenth = emailTemp.length;
                //System.out.println("emailTempLenth: " + emailTempLenth);

                if (emailTempLenth > 0) {
                    to = emailTemp[0];
                }
                if (emailTempLenth > 1) {
                    cc = emailTemp[1];
                }

                Iterator itr1 = emaildetails.iterator();
                TripTO tripTO1 = null;
                if (itr1.hasNext()) {
                    tripTO1 = new TripTO();
                    tripTO1 = (TripTO) itr1.next();
                    smtp = tripTO1.getSmtp();
                    emailPort = Integer.parseInt(tripTO1.getPort());
                    frommailid = tripTO1.getEmailId();
                    password = tripTO1.getPassword();
                }
                String vehicle = tripTO.getVehicleNo();

                String emailFormat = "<html>"
                        + "<body>"
                        + "<p>Hi, <br><br>Trip Freezed For " + tripTO.getTripCode() + "</p>"
                        + "<br> Customer:" + tripTO.getCustomerName()
                        + "<br> C Note No:" + tripTO.getcNotes()
                        + "<br> Route :" + tripTO.getRouteInfo()
                        + "<br> Vehicle No :" + vehicle
                        + "<br><br> Team BrattleFoods"
                        + "</body></html>";

                String subject = "Trip Freezed for Customer " + tripTO.getCustomerName() + " Vehicle " + vehicle + " Route " + tripTO.getRouteInfo();
                int tripMail = 2;
                int consignmentId = 0;
                int routeId = 0;
                String mailType = "WOA";
                String content = emailFormat;
                if (!"".equals(to)) {
                    int mailSendingId = 0;
                    tripTO.setMailTypeId("2");
                    tripTO.setMailSubjectTo(subject);
                    tripTO.setMailSubjectCc(subject);
                    tripTO.setMailSubjectBcc("");
                    tripTO.setMailContentTo(content);
                    tripTO.setMailContentCc(content);
                    tripTO.setMailContentBcc("");
                    tripTO.setMailIdTo(to);
                    tripTO.setMailIdCc(cc);
                    tripTO.setMailIdBcc("");
//                    mailSendingId = tripDAO.insertMailDetails(tripTO, tripTO.getUserId());
                    //   new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, tripTO.getUserId()).start();
                }
            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return status;
    }

    public ArrayList getTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getStatusDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = tripDAO.getStatusDetails();
        return response;
    }

    //       Arul Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripDetails(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getTripDetails(tripSheetId);
        return tripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripInformation(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getTripInformation(tripSheetId);
        return tripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDriverCount(String tripSheetId, TripTO tripTO) {
        String driverCount = "";
        driverCount = tripDAO.getDriverCount(tripSheetId, tripTO);
        return driverCount;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripAdvance(String tripSheetId, TripTO tripTO) {
        String tripAdvance = "";
        tripAdvance = tripDAO.getTripAdvance(tripSheetId, tripTO);
        return tripAdvance;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripExpense(String tripSheetId, TripTO tripTO) {
        String tripExpense = "";
        tripExpense = tripDAO.getTripExpense(tripSheetId, tripTO);
        return tripExpense;
    }

    public String getFuelPrice(String tripSheetId, TripTO tripTO) {
        String tripExpense = "";
        tripExpense = tripDAO.getFuelPrice(tripSheetId, tripTO);
        return tripExpense;
    }

    public String getFuelExpense(String tripSheetId, TripTO tripTO) {
        String tripExpense = "";
        tripExpense = tripDAO.getFuelExpense(tripSheetId, tripTO);
        return tripExpense;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertSettlement(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int insertSettlement = 0;
        insertSettlement = tripDAO.insertSettlement(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
        return insertSettlement;
    }

    public int insertTripDriverSettlementDetails(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int insertSettlement = 0;
        insertSettlement = tripDAO.insertTripDriverSettlementDetails(tripSheetId, estimatedExpense, bpclTransactionAmount, rcmExpense, vehicleDieselConsume, reeferDieselConsume, tripStartingBalance, gpsKm, gpsHm, totalRunKm, totalDays, totalRefeerHours, milleage, tripDieselConsume, tripRcmAllocation, totalTripAdvance, totalTripMisc, totalTripBhatta, totalTripExpense, tripBalance, tripEndBalance, settlementRemarks, paymentMode, fuelPrice, tripExtraExpense, userId, tripTO);
        return insertSettlement;
    }
    // Arul Starts 12-12-2013

    public ArrayList getEndTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getEndTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getClosureEndTripSheetDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getClosureEndTripSheetDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getClosureApprovalProcessHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList result = new ArrayList();
        result = tripDAO.getClosureApprovalProcessHistory(tripTO);
        return result;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getGpsKm(String tripSheetId) {
        String tripDetails = "";
        tripDetails = tripDAO.getGpsKm(tripSheetId);
        return tripDetails;
    }

    public String getClosureApprovalStatus(String tripSheetId, String requestType) {
        String result = "";
        result = tripDAO.getClosureApprovalStatus(tripSheetId, requestType);
        return result;
    }

    public String getRevenueApprovalStatus(String customerId, String requestType) {
        String result = "";
        result = tripDAO.getRevenueApprovalStatus(customerId, requestType);
        return result;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getMiscValue(String vehicleTypeId) {
        String getMiscValue = "";
        getMiscValue = tripDAO.getMiscValue(vehicleTypeId);
        return getMiscValue;
    }

    public String getBookedExpense(String tripSheetId, TripTO tripTO) {
        String result = "";
        result = tripDAO.getBookedExpense(tripSheetId, tripTO);
        return result;
    }

    public String getTripEmails(String tripId, String statusId) {
        String result = "";
        result = tripDAO.getTripEmails(tripId, statusId);
        String[] temp = result.split("~");
        String emails = "";
        String stakeHolders = "";
        if (temp.length > 0) {
            emails = temp[0];
        }
        if (temp.length > 1) {
            stakeHolders = temp[1];
        }
        String to = "";
        String cc = "";
        int toCntr = 0;
        int ccCntr = 0;
        String[] emailTemp = emails.split(",");
        //System.out.println("emailTemp.length:" + emailTemp.length);
        //System.out.println("stakeHolders:" + stakeHolders);
        //System.out.println("1:" + stakeHolders.indexOf("1"));
        for (int i = 0; i < emailTemp.length; i++) {
            if (i == 0) {
                if (stakeHolders.indexOf("1") >= 0) {//does customer email exists
                    //System.out.println("emailTemp[i] step 1:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }

                    }
                }
            }
            if (i == 1) {
                if (stakeHolders.indexOf("2") >= 0) {//does act mgr email exists
                    //System.out.println("emailTemp[i] step 2:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }
                    }
                }
            }
            if (i == 2) {
                if (stakeHolders.indexOf("3") >= 0) {//does act mgr email exists
                    //System.out.println("emailTemp[i] step 3:" + emailTemp[i]);
                    if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                        if (toCntr == 0) {
                            to = emailTemp[i];
                            toCntr++;
                        } else {
                            to = to + "," + emailTemp[i];
                        }

                    }
                }
            }
            if (i > 2) {
                if (!"".equals(emailTemp[i]) && !"-".equals(emailTemp[i]) && (emailTemp[i].indexOf("@") >= 0)) {
                    //System.out.println("emailTemp[i] step 4:" + emailTemp[i]);
                    if (ccCntr == 0) {
                        cc = emailTemp[i];
                        ccCntr++;
                    } else {
                        cc = cc + "," + emailTemp[i];
                    }

                }
            }
        }
        result = to + "~" + cc;

        return result;
    }
//NIthya start 7 dec

    public int savePreTripSheetDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.savePreTripSheetDetails(tripTO, userId);
        return status;
    }

    public int setAdvanceAdvice(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.setAdvanceAdvice(tripTO);
        return status;
    }

    public int saveBillHeader(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillHeader(tripTO);
        return status;
    }

    public int saveBillDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetails(tripTO);
        return status;
    }

    public int saveBillDetailExpense(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveBillDetailExpense(tripTO);
        return status;
    }

    public int updateStartTripSheet(String[] containerTypeId, String[] containerNo, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStartTripSheet(tripTO, userId);
        return status;
    }

    public int createGPSLogForTripStart(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.createGPSLogForTripStart(tripId);
        return status;
    }

    public int createGPSLogForTripEnd(String tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.createGPSLogForTripEnd(tripId);
        return status;
    }

    public ArrayList getTripPODDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getTripPODDetails(tripSheetId);
        return podDetails;
    }

    public int saveTripPodDetails(String actualFilePath, String cityId1, String tripSheetId1, String podRemarks1, String lrNumber, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripPodDetails(actualFilePath, tripSheetId1, cityId1, podRemarks1, lrNumber, fileSaved, tripTO, userId);
        return status;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        vehicleList = tripDAO.getVehicleList();
        return vehicleList;
    }

    public ArrayList getDriverSettlementDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList settlementDetails = new ArrayList();
        settlementDetails = tripDAO.getDriverSettlementDetails(tripTO);
        return settlementDetails;
    }

    public ArrayList getStartTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getStartTripDetails(tripTO);
        return tripDetails;
    }

    public ArrayList getVehicleChangeTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getVehicleChangeTripDetails(tripTO);
        return tripDetails;
    }

    public int updateEndTripSheet(String[] containerTypeId, String[] containerNo, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateEndTripSheet(containerTypeId, containerNo, tripTO, userId);
        return status;
    }

    public int updateEndConsignmentSheet(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateEndConsignmentSheet(tripTO, userId);
        return status;
    }

    public int updateEndTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateEndTripSheetDuringClosure(tripTO, userId);
        return status;
    }

    public int overrideEndTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.overrideEndTripSheetDuringClosure(tripTO, userId);
        return status;
    }

    public int updateStartTripSheetDuringClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStartTripSheetDuringClosure(tripTO, userId);
        return status;
    }

//    public int updateTollAndDetaintionForBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId,
//            String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId,
//            String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = tripDAO.updateTollAndDetaintionForBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId, consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks);
//        return status;
//    }
//
//    public int updateTollAndDetaintionForSupplemetBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId,
//            String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId,
//            String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = tripDAO.updateTollAndDetaintionForSupplemetBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId, consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks);
//        return status;
//    }

    public int updateTollAndDetaintionForRepoBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId, String[] driverId,
            String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String commodityId, String commodityName, String commodityGstType) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTollAndDetaintionForRepoBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId, consignmentConatinerId, greenTax, greenTaxOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId,commodityId,commodityName,commodityGstType);
        return status;
    }

    public int updateStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateStatus(tripTO, userId);
        return status;
    }

    public ArrayList getFuelDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fuelDetails = new ArrayList();
        fuelDetails = tripDAO.getFuelDetails(tripTO);
        return fuelDetails;
    }

    public ArrayList getOtherExpenseDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList otherExpenses = new ArrayList();
        otherExpenses = tripDAO.getOtherExpenseDetails(tripTO);
        return otherExpenses;
    }

    public int insertTripFuelExpense(String tripSheetId, String location, String fillDate, String fuelLitres, String fuelAmount, String fuelRemarks, String fuelPricePerLitre, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertTripFuelExpense(tripSheetId, location, fillDate, fuelLitres, fuelAmount,
                fuelRemarks, fuelPricePerLitre, userId);
        return status;
    }

    public ArrayList getExpenseDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList expenseDetails = new ArrayList();
        expenseDetails = tripDAO.getExpenseDetails();
        return expenseDetails;
    }

    public ArrayList getExpenseDriver() throws FPRuntimeException, FPBusinessException {
        ArrayList expenseDriver = new ArrayList();
        expenseDriver = tripDAO.getExpenseDriver();
        return expenseDriver;
    }

    public ArrayList getDriverName(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverNameDetails = new ArrayList();
        driverNameDetails = tripDAO.getDriverName(tripTO);
        return driverNameDetails;
    }

    public int insertTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String activeValue, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertTripOtherExpense(tripSheetId, vehicleId, expenseName, employeeName, expenseType, expenseDate, expenseHour, expenseMinute, taxPercentage,
                expenseRemarks, expenses, currency, netExpense, billMode, marginValue, activeValue, userId);
        return status;
    }

    public int saveTripExpensePodDetailsOLD(String actualFilePath, String tripExpenseId1, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripExpensePodDetailsOLD(actualFilePath, tripExpenseId1, userId);
        return status;
    }

    public int saveTripExpensePodDetails(String fileSave, String actualFilePath, String tripExpenseId1, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripExpensePodDetails(fileSave, actualFilePath, tripExpenseId1, userId);
        return status;
    }
//NIthya END 7 dec
//Arul Start Here 07-12-2013

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripAdvanceDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetails = new ArrayList();
        tripAdvanceDetails = tripDAO.getTripAdvanceDetails(tripSheetId);
        return tripAdvanceDetails;
    }

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripFuelDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripFuelDetails = new ArrayList();
        tripFuelDetails = tripDAO.getTripFuelDetails(tripSheetId);
        return tripFuelDetails;
    }

    /**
     * This method used to get Trip Expense Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripExpenseDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripExpenseDetails = new ArrayList();
        tripExpenseDetails = tripDAO.getTripExpenseDetails(tripSheetId);
        return tripExpenseDetails;
    }

    public ArrayList getPreStartTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList preStartDetails = new ArrayList();
        preStartDetails = tripDAO.getPreStartTripDetails(tripTO);
        return preStartDetails;
    }

    public ArrayList getStartedTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList startDetails = new ArrayList();
        startDetails = tripDAO.getStartedTripDetails(tripTO);
        return startDetails;
    }

    public ArrayList getEndTripDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList endDetails = new ArrayList();
        endDetails = tripDAO.getEndTripDetails(tripTO);
        return endDetails;
    }

    public ArrayList getGPSDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gpsDetails = new ArrayList();
        gpsDetails = tripDAO.getGPSDetails(tripTO);
        return gpsDetails;
    }

    public ArrayList getTotalTripExpenseDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList totalExpenseDetails = new ArrayList();
        totalExpenseDetails = tripDAO.getTotalTripExpenseDetails(tripTO);
        return totalExpenseDetails;
    }

    public ArrayList getTotalExpenseDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList totalExpense = new ArrayList();
        ArrayList totalExpenseNew = new ArrayList();
        TripTO tripTo = new TripTO();
        totalExpense = tripDAO.getTotalExpenseDetails(tripTO);
        String driverIncentive = "0";
        String tollAmount = "0";
        String driverBatta = "0";
        String fuelPrice = "0";
        String fuelType = "0";
        String vehicleTypeId = "0";
        Iterator it = totalExpense.iterator();
        while (it.hasNext()) {
            tripTo = new TripTO();
            tripTo = (TripTO) it.next();
            if (!"2".equals(tripTO.getTripType())) {//primary
                vehicleTypeId = tripTo.getVehicleTypeId();
                driverIncentive = tripDAO.getDriverIncentiveAmount(vehicleTypeId);
                tollAmount = tripDAO.getTollAmount(vehicleTypeId);
                //System.out.println("tollAmount " + tollAmount);
                driverBatta = tripDAO.getDriverBataAmount(vehicleTypeId);
                //System.out.println("driverBatta " + driverBatta);
                fuelPrice = tripDAO.getFuelPrice(tripTo.getVehicleTypeId());
                //System.out.println("fuelPrice " + fuelPrice);
                fuelType = tripDAO.getFuelType(tripTo.getVehicleTypeId());
                //System.out.println("fuelType " + fuelType);
            } else {//secondary
                //get toll, addltoll, misc for the route 
                String secondaryRouteExpense = tripDAO.getSecondaryRouteExpense(tripTO);
                //System.out.println("secondaryRouteExpense:" + secondaryRouteExpense);

                if (secondaryRouteExpense != null) {

                    String[] tempVar = secondaryRouteExpense.split("~");

                    tripTo.setSecondaryTollAmount(tempVar[0]);
                    tripTo.setSecondaryAddlTollAmount(tempVar[1]);
                    tripTo.setSecondaryMiscAmount(tempVar[2]);
                    tripTo.setSecondaryParkingAmount(tempVar[3]);
                } else {
                    tripTo.setSecondaryTollAmount("0");
                    tripTo.setSecondaryAddlTollAmount("0");
                    tripTo.setSecondaryMiscAmount("0");
                    tripTo.setSecondaryParkingAmount("0");

                }

                //String[] tempVar = secondaryRouteExpense.split("~");
//                tripTo.setSecondaryTollAmount(tempVar[0]);
//                tripTo.setSecondaryAddlTollAmount(tempVar[1]);
//                tripTo.setSecondaryMiscAmount(tempVar[2]);
//                tripTo.setSecondaryParkingAmount(tempVar[3]);
                fuelPrice = tripDAO.getSecondaryFuelPrice(tripTo.getVehicleTypeId(), tripTO);
                fuelType = tripDAO.getSecondaryFuelType(tripTo.getVehicleTypeId(), tripTO);
            }
            tripTo.setTollAmount(tollAmount);
            tripTo.setDriverBatta(driverBatta);
            tripTo.setDriverIncentive(driverIncentive);
            tripTo.setFuelPrice(fuelPrice);
            tripTo.setFuelTypeId(fuelType);
            totalExpenseNew.add(tripTo);
            //System.out.println(" totalExpenseNew" + totalExpenseNew.size());
        }
        return totalExpenseNew;
    }

    public int saveTripClosure(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripClosure(tripTO, userId);
        return status;
    }

    public int saveTripClosureDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripClosureDetails(tripTO, userId);
        return status;
    }

    public ArrayList getTripStausDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList statusDetails = new ArrayList();
        statusDetails = tripDAO.getTripStausDetails(tripTO);
        return statusDetails;
    }

    public ArrayList getPODDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getPODDetails(tripTO);
        return podDetails;
    }

    public ArrayList getTripPackDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripPackDetails = new ArrayList();
        tripPackDetails = tripDAO.getTripPackDetails(tripTO);
        return tripPackDetails;
    }

    public ArrayList getTripUnPackDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripUnPackDetails = new ArrayList();
        tripUnPackDetails = tripDAO.getTripUnPackDetails(tripTO);
        return tripUnPackDetails;
    }

    public ArrayList viewApproveDetails(String tripid, int tripclosureid) throws FPBusinessException, FPRuntimeException {
        ArrayList viewApproveDetails = new ArrayList();
        viewApproveDetails = tripDAO.viewApproveDetails(tripid, tripclosureid);
        return viewApproveDetails;
    }

    public ArrayList viewRevenueApproveDetails(String customerId, int revenueApproveId, String tripId) throws FPBusinessException, FPRuntimeException {
        ArrayList viewApproveDetails = new ArrayList();
        viewApproveDetails = tripDAO.viewRevenueApproveDetails(customerId, revenueApproveId, tripId);
        return viewApproveDetails;
    }

    public ArrayList getEmailDetails(String activitycode) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailDetails = new ArrayList();
        EmailDetails = tripDAO.getEmailDetails(activitycode);
        return EmailDetails;
    }

    public ArrayList getmailcontactDetails(String tripid) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailcontactDetails = new ArrayList();
        EmailcontactDetails = tripDAO.getmailcontactDetails(tripid);
        return EmailcontactDetails;
    }

    public ArrayList getCityList() throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = tripDAO.getCityList();
        return cityList;
    }

    public ArrayList getTripCityList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList cityList = new ArrayList();
        cityList = tripDAO.getTripCityList(tripTO);
        return cityList;
    }

    /**
     * This method used to getTripStakeHolders .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripStakeHoldersList() throws FPRuntimeException, FPBusinessException {
        ArrayList holdersList = null;
        holdersList = tripDAO.getTripStakeHoldersList();
        return holdersList;
    }

    /**
     * This method used to getTripStakeHolders .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getstatusList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getstatusList();
        return statusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getEmailFunction(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList setStatusList = new ArrayList();
        setStatusList = tripDAO.getEmailFunction(tripTO);
        return setStatusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFunc(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunc = null;
        assignedFunc = tripDAO.getAssignedFunc(stackHolderId);
        return assignedFunc;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertEmailSettings(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertEmailSettings(assignedFunc, stackHolderId, userId);
        return status;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteStakeholder(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteStakeholder(assignedFunc, stackHolderId, userId);
        return status;
    }

    //    /**
    //     * This method used to Get  Assigned Functions.
    //     *
    //     * @param request - Http request object.
    //     *
    //     * @throws FPBusinessException - Throws when a business Exception araises
    //     *
    //     * @throws FPRuntimeException - Throws when a Runtime Exception araises
    //     */
    public ArrayList getEditStatuslist(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getEditStatuslist(stackHolderId);
        return statusList;
    }

    /**
     * This method is used to insertvehicleDriverAdvance.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertVehicleDriverAdvance(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertVehicleDriverAdvance = 0;
        insertVehicleDriverAdvance = tripDAO.insertVehicleDriverAdvance(tripTO, userId);
        return insertVehicleDriverAdvance;
    }

    public String checkVehicleDriverAdvance(String vehicleId) {
        String checkVehicleDriverAdvance = "";
        checkVehicleDriverAdvance = tripDAO.checkVehicleDriverAdvance(vehicleId);
        return checkVehicleDriverAdvance;
    }

    public ArrayList getVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        statusList = tripDAO.getVehicleDriverAdvanceList(tripTO);
        return statusList;
    }

    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        customerList = tripDAO.getCustomerList();
        return customerList;
    }

    public ArrayList getZoneList() throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = null;
        zoneList = tripDAO.getZoneList();
        return zoneList;
    }

    public ArrayList getLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList city = new ArrayList();
        city = tripDAO.getLocation(tripTO);
        return city;
    }

    public int insertBPCLTransactionHistory(ArrayList bpclTransactionList, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertBPCLTransactionHistory = 0;
        insertBPCLTransactionHistory = tripDAO.insertBPCLTransactionHistory(bpclTransactionList, tripTO, userId);
        return insertBPCLTransactionHistory;
    }

     public int insertShippingLineNoList(ArrayList shippingLineNoList, TripTO tripTO, int userId) throws FPBusinessException, FPRuntimeException, IOException, Exception {
        int status = 0;
        int insertShippingLineNoList = 0;
        SqlMapClient session = tripDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertShippingLineNoList = tripDAO.insertShippingLineNoList(shippingLineNoList,tripTO,userId,session);
            session.commitTransaction();
        } catch (Exception e) {
        }
        return status;
    }

    public ArrayList insertShippingLineNoTemp(ArrayList shippingLineNoList, int userId) throws FPRuntimeException, FPBusinessException {
        int insertshippingLineNoList = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = tripDAO.insertShippingLineNoTemp(shippingLineNoList, userId);
        return getSbillNoListTemp;
    }
    public ArrayList insertShippingLineNoTempNew(ArrayList shippingLineNoList, int userId) throws FPRuntimeException, FPBusinessException {
//        int insertShippingLineNoTempNew = 0;
        ArrayList insertShippingLineNoTempNew = new ArrayList();
        insertShippingLineNoTempNew = tripDAO.insertShippingLineNoTempNew(shippingLineNoList, userId);
        return insertShippingLineNoTempNew;
    }

    public String insertCustomerCredit(ArrayList customerDetails, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        String insertshippingLineNoList = "";
        insertshippingLineNoList = tripDAO.insertCustomerCredit(customerDetails, tripTO, userId);
        return insertshippingLineNoList;
    }

    public String checkTransactionHistoryId(TripTO tripTO) {
        String status = "";
        status = tripDAO.checkTransactionHistoryId(tripTO);
        return status;
    }

    public String getTripCodeForBpcl(TripTO tripTO) {
        String status = "";
        status = tripDAO.getTripCodeForBpcl(tripTO);
        return status;
    }

    public int getTripCodeCountForBpcl(TripTO tripTO) {
        int status = 0;
        status = tripDAO.getTripCodeCountForBpcl(tripTO);
        return status;
    }

    public ArrayList getTripWrongDataList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWrongDataList = new ArrayList();
        tripWrongDataList = tripDAO.getTripWrongDataList(tripTO);
        return tripWrongDataList;
    }

    public ArrayList getBPCLTransactionHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList bpclTransactionHistory = new ArrayList();
        bpclTransactionHistory = tripDAO.getBPCLTransactionHistory(tripTO);
        return bpclTransactionHistory;
    }

    public ArrayList getConsignmentListForUpdate(String consignmentOrderId) throws FPBusinessException, FPRuntimeException {
        ArrayList consignmentList = new ArrayList();
        consignmentList = tripDAO.getConsignmentListForUpdate(consignmentOrderId);
        return consignmentList;
    }

    public int saveEmptyTripSheet(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String remainWeight = "";
        String tripStatus = "";
        //generate tripcode
        String tripCode = "TS/13-14/";
        String tripCodeSequence = tripDAO.getTripCodeSequence();
        tripCode = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);

        //set transit days
        float transitHours = 0.00F;
        if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
            transitHours = Float.parseFloat(tripTO.getTripTransitHours());
        }
        double transitDay = (double) transitHours / 24;
        tripTO.setTripTransitDays(transitDay + "");
        //set advance to be paid per day
        float totalExpense = 0.00f;
        if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
            totalExpense = Float.parseFloat(tripTO.getOrderExpense());
        }

        int transitDayValue = (int) transitDay;
        if (transitDay > transitDayValue) {
            transitDay = transitDayValue + 1;
        }
        double advnaceToBePaidPerDay = 0;

        if (totalExpense > 0 && transitHours > 0) {
            advnaceToBePaidPerDay = totalExpense / transitDay;
        }
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        tripTO.setStatusId("8");
        tripStatus = "Trip Created";

        //create trip master
        int tripId = tripDAO.saveTripSheet(tripTO);

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");

            //if action is freeeze update prestart plan info
            /*
             if ("1".equals(tripTO.getActionName())
             && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
             status = tripDAO.updatePreStartDetails(tripTO);
             }
             */
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO, remainWeight);
            //create trip vehicles
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                status = tripDAO.saveTripVehicle(tripTO);
            }
            //create trip driver
            if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
                status = tripDAO.saveTripDriver(tripTO);
            }
            //crate trip points
            status = tripDAO.saveTripRoutePlanForEmptyTrip(tripTO);

            //////////////////Arun Start//////////////////////////
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);

            String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
            //System.out.println("emailString:" + emailString);
            String[] emailTemp = emailString.split("~");
            int emailTempLenth = emailTemp.length;
            //System.out.println("emailTempLenth: " + emailTempLenth);

            if (emailTempLenth > 0) {
                to = emailTemp[0];
            }
            if (emailTempLenth > 1) {
                cc = emailTemp[1];
            }

            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
            }
            String vehicle = tripTO.getVehicleNo();

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Empty Trip Created For " + tripTO.getTripCode() + "</p>"
                    + "<br> Customer:" + tripTO.getCustomerName()
                    + "<br> C Note No:" + tripTO.getcNotes()
                    + "<br> Route :" + tripTO.getRouteInfo()
                    + "<br> Vehicle No :" + vehicle
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = "Empty Trip Created for Vehicle " + vehicle + " Customer " + tripTO.getCustomerName() + " Route " + tripTO.getRouteInfo();

            String content = emailFormat;
            if (!"".equals(to)) {
                //new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc).start();
            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return tripId;
    }

    public ArrayList getTripAdvanceDetailsStatus(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetailsStatus = new ArrayList();
        tripAdvanceDetailsStatus = tripDAO.getTripAdvanceDetailsStatus(tripSheetId);
        return tripAdvanceDetailsStatus;
    }

    public int saveWFUTripSheet(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveWFUTripSheet(tripTO, userId);
        return status;
    }

    public ArrayList getWfuDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wfuDetails = new ArrayList();
        wfuDetails = tripDAO.getWfuDetails(tripTO);
        return wfuDetails;
    }

    public ArrayList getEmptyTripMergingList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList emptyTripList = new ArrayList();
        emptyTripList = tripDAO.getEmptyTripMergingList(tripTO);
        return emptyTripList;
    }

    public int insertEmptyTripMerging(String[] tripId, String[] tripSequence, String tripMergingRemarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertEmptyTripMerging(tripId, tripSequence, tripMergingRemarks, userId);
        return status;
    }

    public String getTripCode(int tripId) {
        String tripCode = "";
        tripCode = tripDAO.getTripCode(tripId);
        return tripCode;
    }

    public int saveEmptyTripApproval(String tripId, String approvalStatus, String userId, String mailId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveEmptyTripApproval(tripId, approvalStatus, userId, mailId);
        return status;
    }

    public int saveSecondaryTripSheet(TripTO tripTO, ArrayList orderPointDetails) throws FPBusinessException, FPRuntimeException, IOException {
        int status = 0;
        String tripStatus = "";
        String remainWeight = "";
        //generate tripcode
        String tripCode = "TS/13-14/";
        String tripCodeSequence = tripDAO.getTripCodeSequence();
        tripCode = tripCode + tripCodeSequence;
        tripTO.setTripCode(tripCode);
        tripTO.setOrderType("1");

        //set transit days
        float transitHours = 0.00F;
        if (tripTO.getTripTransitHours() != null && !"".equals(tripTO.getTripTransitHours())) {
            transitHours = Float.parseFloat(tripTO.getTripTransitHours());
        }
        double transitDay = (double) transitHours / 24;
        tripTO.setTripTransitDays(transitDay + "");
        //set advance to be paid per day
        float totalExpense = 0.00f;
        if (tripTO.getOrderExpense() != null && !"".equals(tripTO.getOrderExpense())) {
            totalExpense = Float.parseFloat(tripTO.getOrderExpense());
        }

        int transitDayValue = (int) transitDay;
        if (transitDay > transitDayValue) {
            transitDay = transitDayValue + 1;
        }
        double advnaceToBePaidPerDay = 0;

        if (totalExpense > 0 && transitHours > 0) {
            advnaceToBePaidPerDay = totalExpense / transitDay;
        }
        tripTO.setAdvnaceToBePaidPerDay(advnaceToBePaidPerDay + "");

        tripTO.setStatusId("8");
        tripStatus = "Trip Created";

        //create trip master
        tripTO.setTripType("secondary");
        int tripId = tripDAO.saveTripSheet(tripTO);

        if (tripId > 0) {
            tripTO.setTripId(tripId + "");

            //if action is freeeze update prestart plan info
            /*
             if ("1".equals(tripTO.getActionName())
             && !"1".equals(tripTO.getPreStartLocationStatus())) { //freeze status
             status = tripDAO.updatePreStartDetails(tripTO);
             }
             */
            //create trip status details
            status = tripDAO.saveTripStatusDetails(tripTO);
            //create trip consignment order
            status = tripDAO.saveTripConsignments(tripTO, remainWeight);
            //create trip vehicles
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                status = tripDAO.saveTripVehicle(tripTO);
            }
            //create trip driver
            //System.out.println("tripTO.getPrimaryDriverId()srini:" + tripTO.getPrimaryDriverId());
            if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
                status = tripDAO.saveTripDriver(tripTO);
            }
            //crate trip points
            status = tripDAO.saveTripRoutePlanForSecondaryTrip(tripTO, orderPointDetails);

            //////////////////Arun Start//////////////////////////
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);

            String emailString = getTripEmails(tripTO.getTripId(), tripTO.getStatusId());
            //System.out.println("emailString:" + emailString);
            String[] emailTemp = emailString.split("~");
            int emailTempLenth = emailTemp.length;
            //System.out.println("emailTempLenth: " + emailTempLenth);

            if (emailTempLenth > 0) {
                to = emailTemp[0];
            }
            if (emailTempLenth > 1) {
                cc = emailTemp[1];
            }

            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = tripTO1.getEmailId();
                password = tripTO1.getPassword();
            }
            String vehicle = tripTO.getVehicleNo();

            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Secondary Trip Created For " + tripTO.getTripCode() + "</p>"
                    + "<br> Customer:" + tripTO.getCustomerName()
                    + "<br> C Note No:" + tripTO.getcNotes()
                    + "<br> Route :" + tripTO.getRouteInfo()
                    + "<br> Vehicle No :" + vehicle
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = "Secondary Trip Created for Vehicle " + vehicle + " Customer " + tripTO.getCustomerName() + " Route " + tripTO.getRouteInfo();
            String content = emailFormat;
            if (!"".equals(to)) {
                int mailSendingId = 0;
                tripTO.setMailTypeId("2");
                tripTO.setMailSubjectTo(subject);
                tripTO.setMailSubjectCc(subject);
                tripTO.setMailSubjectBcc("");
                tripTO.setMailContentTo(content);
                tripTO.setMailContentCc(content);
                tripTO.setMailContentBcc("");
                tripTO.setMailIdTo(to);
                tripTO.setMailIdCc(cc);
                tripTO.setMailIdBcc("");
//                mailSendingId = tripDAO.insertMailDetails(tripTO, tripTO.getUserId());
                new SendMail(smtp, emailPort, frommailid, password, subject, content, to, cc, tripTO.getUserId()).start();
            }

            /////////////////////////////////////////////Email part///////////////////////
        }
        //send mail on trip creation to customer and account manager for contract customers
        return tripId;
    }

    public ArrayList getEmptyTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList emptyTripList = new ArrayList();
        emptyTripList = tripDAO.getEmptyTripDetails(tripTO);
        return emptyTripList;
    }

    public int saveManualEmptyTripApproval(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveManualEmptyTripApproval(tripTO);
        return status;
    }

    public int checkeEmptyTripApproval(String tripId, String approvalStatus, String userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkeEmptyTripApproval(tripId, approvalStatus, userId);
        return status;
    }

    public ArrayList getTempLogDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewTempLogDetails = new ArrayList();
        viewTempLogDetails = tripDAO.getTempLogDetails(tripTO);
        return viewTempLogDetails;
    }

    public int saveTempLogDetails(String actualFilePath, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTempLogDetails(actualFilePath, fileSaved, tripTO, userId);
        return status;
    }

    public int saveTemperatureLogApproval(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTemperatureLogApproval(tripTO, userId);
        return status;
    }

    public int checkTemperatureLogApproval(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkTemperatureLogApproval(tripTO, userId);
        return status;
    }

    public String checkConsignmentCreditLimitStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = tripDAO.checkConsignmentCreditLimitStatus(tripTO, userId);
        return status;
    }

    public int updateConsignmentOrderCreditLimitStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateConsignmentOrderCreditLimitStatus(tripTO, userId);
        return status;
    }

    public int updateConsignmentOrderApprovalStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateConsignmentOrderApprovalStatus(tripTO, userId);
        return status;
    }

    public int insertLoadingUnloadingDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertLoadingUnloadingDetails(tripTO, userId);
        return status;
    }

    public ArrayList getStatusList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = new ArrayList();
        statusList = tripDAO.getStatusList(tripTO);
        return statusList;
    }

    public ArrayList getTripVehicleNo(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getTripVehicleNo(tripTO);
        return vehicleNos;
    }

    public ArrayList getRunningTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripdetails = new ArrayList();
        tripdetails = tripDAO.getRunningTripDetails(tripTO);
        return tripdetails;
    }

    public ArrayList getConsignmentPaymentDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consignmentPaymentList = new ArrayList();
        consignmentPaymentList = tripDAO.getConsignmentPaymentDetails(tripTO);
        return consignmentPaymentList;
    }

    public ArrayList getEmailList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getEmailList = new ArrayList();
        getEmailList = tripDAO.getEmailList(tripTO);
        return getEmailList;
    }

    public int saveVehicleDriverAdvanceApproval(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveVehicleDriverAdvanceApproval(tripTO);
        return status;
    }

    public int checkVehicleDriverAdvanceApproval(String vehicleDriverAdvanceId, String approvalStatus, String userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.checkVehicleDriverAdvanceApproval(vehicleDriverAdvanceId, approvalStatus, userId);
        return status;
    }

    public ArrayList getVehicleAdvanceRequest(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList advanceRequest = new ArrayList();
        advanceRequest = tripDAO.getVehicleAdvanceRequest(tripTO);
        return advanceRequest;
    }

    public ArrayList getViewVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewVehicleDriverAdvanceList = new ArrayList();
        viewVehicleDriverAdvanceList = tripDAO.getViewVehicleDriverAdvanceList(tripTO);
        return viewVehicleDriverAdvanceList;
    }

    public int saveVehicleDriverAdvancePay(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveVehicleDriverAdvancePay(tripTO);
        return status;
    }

    public String getLastBpclTransactionDate() {
        String lastBpclTxDate = "";
        lastBpclTxDate = tripDAO.getLastBpclTransactionDate();
        return lastBpclTxDate;
    }

    public ArrayList getApprovalValueDetails(double actualval) throws FPRuntimeException, FPBusinessException {
        ArrayList getApprovalValueDetails = new ArrayList();
        getApprovalValueDetails = tripDAO.getApprovalValueDetails(actualval);
        return getApprovalValueDetails;
    }

    public ArrayList getRepairMaintenence(int configId) throws FPRuntimeException, FPBusinessException {
        ArrayList getRepairMaintenence = new ArrayList();
        getRepairMaintenence = tripDAO.getRepairMaintenence(configId);
        return getRepairMaintenence;
    }

    public String getFCLeadMailId(String vehicleId) {
        String fcLeadMailId = "";
        fcLeadMailId = tripDAO.getFCLeadMailId(vehicleId);
        return fcLeadMailId;
    }

    public String getSecondaryFCLeadMailId(String vehicleId) {
        String fcLeadMailId = "";
        fcLeadMailId = tripDAO.getSecondaryFCLeadMailId(vehicleId);
        return fcLeadMailId;
    }

    public String getSecondaryApprovalPerson(String vehicleId) {
        String approvalPersonMailId = "";
        approvalPersonMailId = tripDAO.getSecondaryApprovalPerson(vehicleId);
        return approvalPersonMailId;
    }

    public String getPreviousTripsOdometerReading(String tripSheetId) {
        String previousTripsOdometerReading = "";
        previousTripsOdometerReading = tripDAO.getPreviousTripsOdometerReading(tripSheetId);
        return previousTripsOdometerReading;
    }

    public int insertBillingGraph(String tripSheetId, String graphPath, int userId) throws IOException {
        int insertBillingGraph = 0;
        insertBillingGraph = tripDAO.insertBillingGraph(tripSheetId, graphPath, userId);
        return insertBillingGraph;
    }

    public String getTripCount(TripTO tripTO) {
        String tripCount = "";
        tripCount = tripDAO.getTripCount(tripTO);
        return tripCount;
    }

    public int updateInvoiceAmount(TripTO tripTO, int userId) throws IOException {
        int updateInvoiceAmount = 0;
        updateInvoiceAmount = tripDAO.updateInvoiceAmount(tripTO, userId);
        return updateInvoiceAmount;
    }

    public int insertCourierDetails(TripTO tripTO, int userId) throws IOException {
        int insertCourierDetails = 0;
        insertCourierDetails = tripDAO.insertCourierDetails(tripTO, userId);
        return insertCourierDetails;
    }

    public ArrayList getCourierDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList courierDetails = new ArrayList();
        courierDetails = tripDAO.getCourierDetails(tripTO);
        return courierDetails;
    }

    public int checkTempGraphApprovedStatus(TripTO tripTO, int userId) throws IOException {
        int checkTempGraphApprovedStatus = 0;
        checkTempGraphApprovedStatus = tripDAO.checkTempGraphApprovedStatus(tripTO, userId);
        return checkTempGraphApprovedStatus;
    }

    public int checkBillSubmittedStatus(TripTO tripTO, int userId) throws IOException {
        int checkBillSubmittedStatus = 0;
        checkBillSubmittedStatus = tripDAO.checkBillSubmittedStatus(tripTO, userId);
        return checkBillSubmittedStatus;
    }

    public ArrayList getVehicleLogDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleLogDetails = new ArrayList();
        vehicleLogDetails = tripDAO.getVehicleLogDetails(tripTO);
        return vehicleLogDetails;
    }

    public ArrayList getEmployeeTripDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList employeeTripDetails = new ArrayList();
        employeeTripDetails = tripDAO.getEmployeeTripDetails(tripTO);
        return employeeTripDetails;
    }

    public int updateEmployeeInTrip(TripTO tripTO, int userId) throws IOException {
        int checkBillSubmittedStatus = 0;
        checkBillSubmittedStatus = tripDAO.updateEmployeeInTrip(tripTO, userId);
        return checkBillSubmittedStatus;
    }

    public String checkEmployeeInTrip(String driverId) {
        String checkEmployeeInTrip = "";
        checkEmployeeInTrip = tripDAO.checkEmployeeInTrip(driverId);
        return checkEmployeeInTrip;
    }

    public ArrayList getVehicleChangeTripAdvanceDetails(String tripSheetId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripAdvanceDetails = new ArrayList();
        tripAdvanceDetails = tripDAO.getVehicleChangeTripAdvanceDetails(tripSheetId);
        return tripAdvanceDetails;
    }

    public int insertMailDetails(TripTO tripTO, int userId) throws IOException {
        int insertMailDetails = 0;
        insertMailDetails = tripDAO.insertMailDetails(tripTO, userId);
        return insertMailDetails;
    }

    public ArrayList getMailNotDeliveredList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList mailNotDeliveredList = new ArrayList();
        mailNotDeliveredList = tripDAO.getMailNotDeliveredList(tripTO);
        return mailNotDeliveredList;
    }

    public int updateTripOtherExpenseDoneStatus(TripTO tripTO, int userId) {
        int updateOtherExpenseDoneStatus = 0;
        updateOtherExpenseDoneStatus = tripDAO.updateTripOtherExpenseDoneStatus(tripTO, userId);
        return updateOtherExpenseDoneStatus;
    }

    public String getOtherExpenseDoneStatus(TripTO tripTO) {
        String otherExpenseDoneStatus = "";
        otherExpenseDoneStatus = tripDAO.getOtherExpenseDoneStatus(tripTO);
        return otherExpenseDoneStatus;
    }

    public String getLastUploadCustomerOutstandingDate() {
        String lastCustomerOutstandingDate = "";
        lastCustomerOutstandingDate = tripDAO.getLastUploadCustomerOutstandingDate();
        return lastCustomerOutstandingDate;
    }

    public String getPreviousOutStandingAmount(TripTO tripTO) {
        String outStandingAmount = "";
        outStandingAmount = tripDAO.getPreviousOutStandingAmount(tripTO);
        return outStandingAmount;
    }

    public int updateCustomerOutStandingAmount(String[] customerId, String[] outStandingAmount, int userId) {
        int updateCustomerOutStandingAmount = 0;
        updateCustomerOutStandingAmount = tripDAO.updateCustomerOutStandingAmount(customerId, outStandingAmount, userId);
        return updateCustomerOutStandingAmount;
    }

    public ArrayList getCustomerOutStandingList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerOutStandingList = new ArrayList();
        customerOutStandingList = tripDAO.getCustomerOutStandingList();
        return customerOutStandingList;
    }

    public ArrayList getSecondaryCustomerApprovalList() throws FPRuntimeException, FPBusinessException {
        ArrayList secondaryCustomerApprovalList = new ArrayList();
        secondaryCustomerApprovalList = tripDAO.getSecondaryCustomerApprovalList();
        return secondaryCustomerApprovalList;
    }

    public int updateSecondaryCustomerMail(TripTO tripTO) {
        int updateSecondaryCustomerMail = 0;
        updateSecondaryCustomerMail = tripDAO.updateSecondaryCustomerMail(tripTO);
        return updateSecondaryCustomerMail;
    }

    public int updateEmptyTripRoute(TripTO tripTO) {
        int updateEmptyTripRoute = 0;
        updateEmptyTripRoute = tripDAO.updateEmptyTripRoute(tripTO);
        return updateEmptyTripRoute;
    }

    public String getVehicleUsageTypeId(String vehicleId) {
        String usageTypeId = "";
        usageTypeId = tripDAO.getVehicleUsageTypeId(vehicleId);
        return usageTypeId;
    }

    public String getVehicleCurrentStatus(String tripId) {
        String vehicleCurrentStatus = "";
        vehicleCurrentStatus = tripDAO.getVehicleCurrentStatus(tripId);
        return vehicleCurrentStatus;
    }

    public int updateDeleteTrip(TripTO tripTO) {
        int updatedeletetrip = 0;
        updatedeletetrip = tripDAO.updateDeleteTrip(tripTO);
        return updatedeletetrip;

    }

    public int updateTripAdvance(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripAdvance(tripTO, userId);
        return status;
    }

    public int updateTripEnd(TripTO tripTO, int userId) {
        int updatetripend = 0;
        updatetripend = tripDAO.updateTripEnd(tripTO, userId);
        return updatetripend;

    }

    public int updateTripStartDetails(TripTO tripTO, int userId) {
        int updateTripStartDetails = 0;
        updateTripStartDetails = tripDAO.updateTripStartDetails(tripTO, userId);
        return updateTripStartDetails;
    }

    public ArrayList getTripClosureVehicleList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripClosureVehicleList = new ArrayList();
        tripClosureVehicleList = tripDAO.getTripClosureVehicleList(tripTO);
        return tripClosureVehicleList;
    }

    public ArrayList getTripSettlementVehicleList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripClosureVehicleList = new ArrayList();
        tripClosureVehicleList = tripDAO.getTripSettlementVehicleList(tripTO);
        return tripClosureVehicleList;
    }

    public ArrayList getEmptyCustomerList(TripTO tripTO) {
        ArrayList getEmptyCustomerList = new ArrayList();
        getEmptyCustomerList = tripDAO.getEmptyCustomerList(tripTO);
        return getEmptyCustomerList;
    }

    public ArrayList getTicketingStatusList(TripTO tripTO) {
        ArrayList ticketingStatusList = new ArrayList();
        ticketingStatusList = tripDAO.getTicketingStatusList(tripTO);
        return ticketingStatusList;
    }

    public ArrayList getTicketingList(TripTO tripTO) {
        ArrayList ticketingStatusList = new ArrayList();
        ticketingStatusList = tripDAO.getTicketingList(tripTO);
        return ticketingStatusList;
    }

    public ArrayList getTicketingDetailsList(TripTO tripTO) {
        ArrayList ticketingDetailsList = new ArrayList();
        ticketingDetailsList = tripDAO.getTicketingDetailsList(tripTO);
        return ticketingDetailsList;
    }

    public ArrayList getTicketingStatusDetailsList(TripTO tripTO) {
        ArrayList ticketingStatusDetailsList = new ArrayList();
        ticketingStatusDetailsList = tripDAO.getTicketingStatusDetailsList(tripTO);
        return ticketingStatusDetailsList;
    }

    public int saveTicketFile(String actualFilePath, String fileSaved, TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTicketFile(actualFilePath, fileSaved, tripTO, userId);
        return status;
    }

    public int saveTicket(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int ticketId = 0;
        ticketId = tripDAO.saveTicket(tripTO, userId);
        //System.out.println("ticketId in the tripBP = " + ticketId);
        if (ticketId > 0) {
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String priority = "";
            String type = "";
            String statusName = "";
            String raisedBy = "";
            String raisedOn = "";
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = "ticketing.brattle@gmail.com";
                password = "brattle@123";
            }
            tripTO.setTicketId(String.valueOf(ticketId));
            tripTO.setUserId(userId);
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripDAO.getTicketingList(tripTO);
            Iterator itr2 = ticketingStatusList.iterator();
            TripTO tripTO2 = null;
            if (itr2.hasNext()) {
                tripTO2 = new TripTO();
                tripTO2 = (TripTO) itr2.next();
                priority = tripTO2.getPriority();
                type = tripTO2.getType();
                statusName = tripTO2.getStatus();
                raisedBy = tripTO2.getRaisedBy();
                raisedOn = tripTO2.getRaisedOn();
            }
            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Ticket Created :</p>"
                    + "<br> Ticket No:" + "TN-" + ticketId
                    + "<br> Priority:" + priority
                    + "<br> Type :" + type
                    + "<br> Status :" + statusName
                    + "<br> Raised By :" + raisedBy
                    + "<br> Raised On :" + raisedOn
                    + "<br> Message :" + tripTO.getMessage()
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = tripTO.getTitle();
            String content = emailFormat;
            new SendMail(smtp, emailPort, frommailid, password, subject, content, tripTO.getTo(), tripTO.getCc(), tripTO.getUserId()).start();
            String mobileNo = "8122725178,9940204724,08826350111,8826437820,9790963689";
            String sMsg = "Hi Arul";
            try {
                //send sms
                sMsg = "Ticket No : " + "TN-" + ticketId + ", Type : " + type + ", Status : " + statusName + ", Priority : " + priority + ", Raised By : " + raisedBy + ", Raised On : " + raisedOn;
                //System.out.println("smsg is :" + sMsg);
                sMsg = sMsg.replaceAll(" ", "%20");
                //System.out.println("After sMsg: " + sMsg);
                //http://www.nexmoo.com/hebron/smssent.php?mno=9940117411&msg=text%20msg
                URL url = new URL("http://www.nexmoo.com/entitle/sms.php?phoneno=" + mobileNo + "&msg=" + sMsg);
                //System.out.println("url:" + url.toString());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("target=" + "getAllStopDetails");
                out.close();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String decodedString = in.readLine();
                //System.out.println(decodedString); //msg sent successfully
                if (decodedString.equals("msg sent successfully")) {
                    //System.out.println("msg sent successfully");
                }
                in.close();
            } catch (Exception e) {
                //System.out.println("Unable to send may be sms... pls check....");
                e.printStackTrace();
            }

        }
        return ticketId;
    }

    public int updateTicketStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTicketStatus(tripTO, userId);
        if (status > 0) {
            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String priority = "";
            String type = "";
            String statusName = "";
            String raisedBy = "";
            String raisedOn = "";
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            TripTO tripTO1 = null;
            if (itr1.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr1.next();
                smtp = tripTO1.getSmtp();
                emailPort = Integer.parseInt(tripTO1.getPort());
                frommailid = "ticketing.brattle@gmail.com";
                password = "brattle@123";
            }
            ArrayList ticketingStatusList = new ArrayList();
            ticketingStatusList = tripDAO.getTicketList(tripTO);
            Iterator itr2 = ticketingStatusList.iterator();
            TripTO tripTO2 = null;
            if (itr2.hasNext()) {
                tripTO2 = new TripTO();
                tripTO2 = (TripTO) itr2.next();
                priority = tripTO2.getPriority();
                type = tripTO2.getType();
                statusName = tripTO2.getStatus();
                raisedBy = tripTO2.getRaisedBy();
                raisedOn = tripTO2.getRaisedOn();
            }
            String emailFormat = "<html>"
                    + "<body>"
                    + "<p>Hi, <br><br>Ticket Updated :</p>"
                    + "<br> Ticket No:" + tripTO.getTicketNo()
                    + "<br> Priority:" + priority
                    + "<br> Type :" + type
                    + "<br> Status :" + statusName
                    + "<br> Followed By :" + raisedBy
                    + "<br> Followed On :" + raisedOn
                    + "<br> Message :" + tripTO.getMessage()
                    + "<br><br> Team BrattleFoods"
                    + "</body></html>";

            String subject = tripTO.getTitle();
            String content = emailFormat;
            new SendMail(smtp, emailPort, frommailid, password, subject, content, tripTO.getTo(), tripTO.getCc(), tripTO.getUserId()).start();
            String mobileNo = "8122725178,9940204724,08826350111,8826437820,9790963689";
            String sMsg = "Hi Arul";
            try {
                //send sms
                sMsg = "Ticket No : " + tripTO.getTicketNo() + ", Type : " + type + ", Status : " + statusName + ", Priority : " + priority + ", Followed By : " + raisedBy + ", Followed On : " + raisedOn;
                //System.out.println("smsg is :" + sMsg);
                sMsg = sMsg.replaceAll(" ", "%20");
                //System.out.println("After sMsg: " + sMsg);
                //http://www.nexmoo.com/hebron/smssent.php?mno=9940117411&msg=text%20msg
                URL url = new URL("http://www.nexmoo.com/entitle/sms.php?phoneno=" + mobileNo + "&msg=" + sMsg);
                //System.out.println("url:" + url.toString());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write("target=" + "getAllStopDetails");
                out.close();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String decodedString = in.readLine();
                //System.out.println(decodedString); //msg sent successfully
                if (decodedString.equals("msg sent successfully")) {
                    //System.out.println("msg sent successfully");
                }
                in.close();
            } catch (Exception e) {
                //System.out.println("Unable to send may be sms... pls check....");
                e.printStackTrace();
            }

        }
        return status;
    }

    public ArrayList tripsettelmentDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripsettelmentDetails = new ArrayList();
        tripsettelmentDetails = tripDAO.gettripsettelmentDetails(tripTO);
        return tripsettelmentDetails;
    }

    public String getPreviousTripEndTime(String tripSheetId) {
        String getPreviousTripEndTime = "";
        getPreviousTripEndTime = tripDAO.getPreviousTripsEndTime(tripSheetId);
        return getPreviousTripEndTime;
    }

    public ArrayList getSecFCmailcontactDetails(String customerName) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailcontactDetails = new ArrayList();
        EmailcontactDetails = tripDAO.getSecFCmailcontactDetails(customerName);
        return EmailcontactDetails;
    }

    public int updateNextTrip(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateNextTrip(tripTO, userId);
        return status;
    }

    public ArrayList getPrimaryDriverSettlementTrip(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList primaryDriverSettlementTrip = new ArrayList();
        primaryDriverSettlementTrip = tripDAO.getPrimaryDriverSettlementTrip(tripTO);
        return primaryDriverSettlementTrip;
    }

    public ArrayList getVehicleDriverAdvance(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverAdvance = new ArrayList();
        vehicleDriverAdvance = tripDAO.getVehicleDriverAdvance(tripTO);
        return vehicleDriverAdvance;
    }

    public ArrayList getDriverIdleBhatta(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverIdleBhatta = new ArrayList();
        driverIdleBhatta = tripDAO.getDriverIdleBhatta(tripTO);
        return driverIdleBhatta;
    }

    public String getDriverLastBalanceAmount(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String driverLastBalanceAmount = "";
        driverLastBalanceAmount = tripDAO.getDriverLastBalanceAmount(tripTO);
        return driverLastBalanceAmount;
    }

    public int insertPrimaryDriverSettlement(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertPrimaryDriverSettlement(tripTO, userId);
        return status;
    }

    public int insertPrimaryDriverSettlementDetails(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.insertPrimaryDriverSettlementDetails(tripTO, userId);
        return status;
    }

    public int updateIdleBhattaStatus(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateIdleBhattaStatus(tripTO, userId);
        return status;
    }

    public ArrayList getTripOtherExpenseDocumentRequired(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDocumentRequiredDetails = new ArrayList();
        tripDocumentRequiredDetails = tripDAO.getTripOtherExpenseDocumentRequired(tripTO);
        return tripDocumentRequiredDetails;
    }

    public ArrayList getOtherExpenseFileDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList podDetails = new ArrayList();
        podDetails = tripDAO.getOtherExpenseFileDetails(tripTO);
        return podDetails;
    }

    public int deleteExpenaseBillCopy(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteExpenaseBillCopy(tripTO);
        return status;
    }

    public String getTripUnclearedBalance(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String tripUnclearedBalance = "";
        tripUnclearedBalance = tripDAO.getTripUnclearedBalance(tripTO);
        return tripUnclearedBalance;
    }

    public ArrayList getTripSheetDetailsForChallan(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripSheetDetailsForChallan(tripTO);
        return tripDetails;
    }

    public String getTotalBillExpenseForDriver(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = tripDAO.getTotalBillExpenseForDriver(tripTO);
        if (status == null) {
            status = "0~0";
        }
        return status;
    }

    public ArrayList getOrderDetailsForEnd(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderDetails = new ArrayList();
        orderDetails = tripDAO.getOrderDetailsForEnd(tripTO);
        return orderDetails;
    }

    public ArrayList getVehicleForGpsCurrentLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetails = new ArrayList();
        vehicleDetails = tripDAO.getVehicleForGpsCurrentLocation(tripTO);
        return vehicleDetails;
    }

    public int getRouteExpenseDetails(String val, String vehicleTypeId, TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int expense = 0;
        expense = tripDAO.getRouteExpenseDetails(val, vehicleTypeId, tripTO);
        return expense;
    }

    public double getSecondaryRouteExpenseDetails(String totalkm, String vehicleTypeId, String originId) throws FPRuntimeException, FPBusinessException {
        double expense = 0;
        expense = tripDAO.getSecondaryRouteExpenseDetails(totalkm, vehicleTypeId, originId);
        return expense;
    }

    public String getPointKm(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos1 = new ArrayList();
        String vehicleNos = "";
        vehicleNos = tripDAO.getPointKm(tripTO);
        return vehicleNos;
    }

    public ArrayList getGoogleData(String originPointName, String destinationPointName) throws FPRuntimeException, FPBusinessException, MalformedURLException, IOException {
        ArrayList vehicleNos = new ArrayList();
        String vehicleNos1 = "";
        vehicleNos = tripDAO.getRouteDistanceCity(originPointName, destinationPointName);
        return vehicleNos;
    }

    public ArrayList getConfigurationMaster(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList configurationMaster = new ArrayList();
        String vehicleNos = "";
        configurationMaster = tripDAO.getConfigurationMaster(tripTO);
        return configurationMaster;
    }

    public int createRoute(ArrayList googleData, ArrayList country, String vehicleType, String orgin, String destination) throws FPRuntimeException, FPBusinessException {
        int createRoute = 0;
        createRoute = tripDAO.createRoute(googleData, country, vehicleType, orgin, destination);
        return createRoute;
    }

    public ArrayList getDistanceList(String[] consignmentOrderIds, String originId[], String pickupPoint) throws FPRuntimeException, FPBusinessException {
        ArrayList distanceList = new ArrayList();
        distanceList = tripDAO.getDistanceList(consignmentOrderIds, originId, pickupPoint);
        return distanceList;
    }

    public String getRouteExpenseDetailsForVehicleType(String totalkm, String vehicleTypeId,
            String vendorId, String sourceId, String destinationId, String ownership,
            String noOfContianer, String movementType,
            String point1Id, String point2Id,String point3Id,
            String vehicleCategory, String productInfo,
            String containerTypeId, String companyId, String vehicleNo, String routeContractId
    ) throws FPRuntimeException, FPBusinessException {
        String expense = "";
        expense = tripDAO.getRouteExpenseDetailsForVehicleType(totalkm, vehicleTypeId, vendorId, sourceId, destinationId,
                ownership, noOfContianer, movementType, point1Id, point2Id,point3Id, vehicleCategory, productInfo, containerTypeId, companyId, vehicleNo, routeContractId);
        return expense;
    }

    public String getOrgRouteExpenseDetailsForVehicleType(String totalkm, String vehicleTypeId,
            String vendorId, String sourceId, String destinationId, String ownership,
            String noOfContianer, String movementType,
            String point1Id, String point2Id,
            String vehicleCategory, String productInfo,
            String containerTypeId, String companyId, String vehicleNo, String routeContractId
    ) throws FPRuntimeException, FPBusinessException {
        String expense = "";
        expense = tripDAO.getOrgRouteExpenseDetailsForVehicleType(totalkm, vehicleTypeId, vendorId, sourceId, destinationId,
                ownership, noOfContianer, movementType, point1Id, point2Id, vehicleCategory, productInfo, containerTypeId, companyId, vehicleNo, routeContractId);
        return expense;
    }

    public int saveTripSheetForVehicleDeattach(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;

        //reset vehicle and driver mapping
        status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        //update vehicle info
        status = tripDAO.saveTripVehicleForVehicleDeattach(tripTO);
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
        }
        //update driver info
        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
        }

        return status;
    }

    public int saveTripSheetForVehicleAttach(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;

        //reset vehicle and driver mapping
        // status = tripDAO.clearVehicleAndDriverMapping(tripTO);
        //update vehicle info
        status = tripDAO.saveTripVehicleForVehicleAttach(tripTO);
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
        }
        //update driver info
        if (tripTO.getPrimaryDriverId() != null && !"0".equals(tripTO.getPrimaryDriverId()) && !"".equals(tripTO.getPrimaryDriverId())) {
            status = tripDAO.saveTripDriver(tripTO);
        }

        return status;
    }

    public ArrayList getTrailerList() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerList = null;
        trailerList = tripDAO.getTrailerList();
        return trailerList;
    }

    public ArrayList getContainerList() throws FPRuntimeException, FPBusinessException {
        ArrayList containerList = null;
        containerList = tripDAO.getContainerList();
        return containerList;
    }

    public ArrayList getContainerTypeDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList containerTypeDetails = new ArrayList();
        containerTypeDetails = tripDAO.getContainerTypeDetails(tripTO);
        return containerTypeDetails;
    }

    public ArrayList getcontainerType(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList container = new ArrayList();
        container = tripDAO.getcontainerType(tripTO);
        return container;
    }

    public ArrayList getVendorList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vendorList = new ArrayList();
        vendorList = tripDAO.getVendorList(tripTO);
        return vendorList;

    }

    public ArrayList getVendorVehicleType(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNos = new ArrayList();
        vehicleNos = tripDAO.getVendorVehicleType(tripTO);
        return vehicleNos;
    }

    public String getVehicleOwnerType(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        // ArrayList otherExpenses = new ArrayList();
        String vehicleOwnerType = tripDAO.getVehicleOwnerType(tripTO);
        return vehicleOwnerType;
    }

    public ArrayList getVehicleSchedule(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expiryDetails = new ArrayList();
        expiryDetails = tripDAO.getVehicleSchedule(tripTO);
        return expiryDetails;
    }

    public String getRoutePoints(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String revenue = "";
        revenue = tripDAO.getRoutePoints(tripTO);
        return revenue;
    }

    public String getContainerDetails(String ConsignmentOrderIds) throws FPRuntimeException, FPBusinessException {
        ArrayList ContainerDetails = new ArrayList();
        int counter = 0;
        TripTO rack = null;
        ContainerDetails = tripDAO.getContainerDetails(ConsignmentOrderIds);

        Iterator itr;
        String Container = "";
        if (ContainerDetails.size() == 0) {
            Container = "";
        } else {
            itr = ContainerDetails.iterator();
            while (itr.hasNext()) {
                rack = new TripTO();
                rack = (TripTO) itr.next();
                if (counter == 0) {
                    //  Container =rack.getContainerNo();
                    Container = rack.getContainerNo() + "+" + rack.getContainerName() + "+" + rack.getContainerTypeId();
                    //System.out.println("Container" + Container);
                    counter++;
                } else {
                    //  Container = Container + "~" +rack.getContainerNo();
                    Container = Container + "~" + rack.getContainerNo() + "+" + rack.getContainerName();
                    //System.out.println("Container" + Container);
                }

            }
        }
        return Container;
    }

    public ArrayList getconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        consgncontainer = tripDAO.getconsgncontainer(tripTO);
        return consgncontainer;
    }

    public ArrayList getPlannedConsgnContainerDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        consgncontainer = tripDAO.getPlannedConsgnContainerDetails(tripTO);
        return consgncontainer;
    }

    public ArrayList getrepoconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        consgncontainer = tripDAO.getrepoconsgncontainer(tripTO);
        return consgncontainer;
    }

    public ArrayList processBunkList(String bunkName) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = tripDAO.getBunkList(bunkName);
        return bunkList;
    }

    public int processInsertBunk(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("BP 1");
        status = tripDAO.doInsertBunk(tripTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-02");
        }
        return status;
    }

    public ArrayList processBunkalterList(String bunkId) throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = tripDAO.getBunkalterList(bunkId);
        if (bunkList.size() == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return bunkList;
    }

    public int processBunkUpdateList(TripTO tripTo, int userId, String bunkId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = tripDAO.doUpdateBunk(tripTo, userId, bunkId);
        if (status == 0) {
            throw new FPBusinessException("EM-CUST-01");
        }
        return status;
    }

    public ArrayList getCustomerNameSuggestionList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = tripDAO.getCustomerNameSuggestionList(tripTO);
        return customerList;
    }

    public ArrayList getInvoiceDetailsList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailsList = new ArrayList();
        invoiceDetailsList = tripDAO.getInvoiceDetailsList(tripTO);
        return invoiceDetailsList;
    }

    public ArrayList getInvoiceHeaderList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceHeaderList = new ArrayList();
        invoiceHeaderList = tripDAO.getInvoiceHeaderList(tripTO);
        return invoiceHeaderList;
    }

    public ArrayList getRtoMasterList() throws FPBusinessException, FPRuntimeException {
        ArrayList troMasterList = new ArrayList();
        troMasterList = tripDAO.getRtoMasterList();
        return troMasterList;
    }

    public ArrayList getRtoMappedVehicleList(String rtoOfficeId) throws FPBusinessException, FPRuntimeException {
        ArrayList rtoMappedVehicleList = new ArrayList();
        rtoMappedVehicleList = tripDAO.getRtoMappedVehicleList(rtoOfficeId);
        return rtoMappedVehicleList;
    }

    public int insertRtoVehicleMapping(int userId, String[] assignedVehicleId, String rtoOfficeId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = tripDAO.insertRtoVehicleMapping(userId, assignedVehicleId, rtoOfficeId);
        return insertStatus;
    }

    public int getRtoVehicleCount(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int vehicleCount = 0;
        vehicleCount = tripDAO.getRtoVehicleCount(tripTO);
        return vehicleCount;
    }

    public int updatePlannedStatus(TripTO tripTO, String[] containerTypeId, String[] containerNo, String[] uniqueId, String isRepo, String[] sealNo, String[] grStatus) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("in BP.......");
        status = tripDAO.updatePlannedStatus(tripTO, containerTypeId, containerNo, uniqueId, isRepo, sealNo, grStatus);
        return status;
    }

    public String getPlannedConatinerSize(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String status = "";
        //System.out.println("in BP.......");
        status = tripDAO.getPlannedConatinerSize(tripTO);
        return status;
    }

    public String getDistance(String origin, String destination) throws FPRuntimeException, FPBusinessException {
        String status = "";
        //System.out.println("in BP.......");
        status = tripDAO.getDistance(origin, destination);
        return status;
    }

    public ArrayList getTripList() throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetailsList = new ArrayList();
        tripDetailsList = tripDAO.getTripList();
        return tripDetailsList;
    }

    public ArrayList getPrintTripSheetDetails(String tripSheetId) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetailsList = new ArrayList();
        tripDetailsList = tripDAO.getPrintTripSheetDetails(tripSheetId);
        return tripDetailsList;
    }

    public ArrayList getPrintTripSheetDetailsNew(String tripSheetId) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetailsList = new ArrayList();
        tripDetailsList = tripDAO.getPrintTripSheetDetailsNew(tripSheetId);
        return tripDetailsList;
    }

    public ArrayList getPrintTripChallanDetails(String tripSheetId) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetailsList = new ArrayList();
        tripDetailsList = tripDAO.getPrintTripChallanDetails(tripSheetId);
        return tripDetailsList;
    }

    public ArrayList getConsignmentNoteContainer(String consignmentNoteOredrId, String tripId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerNo = new ArrayList();
        containerNo = tripDAO.getConsignmentNoteContainer(consignmentNoteOredrId, tripId);
        return containerNo;
    }

    public ArrayList getConsignmentNoteContainerNew(String consignmentNoteOredrId, String tripId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerNo = new ArrayList();
        containerNo = tripDAO.getConsignmentNoteContainerNew(consignmentNoteOredrId, tripId);
        return containerNo;
    }

    public ArrayList getConsignmentArticleList(String consignmentNoteOredrId) throws FPBusinessException, FPRuntimeException {
        ArrayList consignmentArticleList = new ArrayList();
        consignmentArticleList = tripDAO.getConsignmentArticleList(consignmentNoteOredrId);
        return consignmentArticleList;
    }

    public int getPlannedContainerCount(String consignmentOrderId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("in BP.......");
        status = tripDAO.getPlannedContainerCount(consignmentOrderId);
        return status;
    }

    public int updateShippingBillOrBillOfEntryForBilling(String consignment_order_id, String billOfEntryNo, String billing_party_id, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("in BP.......");
        status = tripDAO.updateShippingBillOrBillOfEntryForBilling(consignment_order_id, billOfEntryNo, billing_party_id, userId);
        return status;
    }

    public String getTwoTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) throws FPRuntimeException, FPBusinessException {
        String rate = "";
        //System.out.println("in BP.......");
        rate = tripDAO.getTwoTwentyRate(customerId, firstPointId, secondPointId, productCategory, movementType);
        return rate;
    }

    public String getPlannedContainerNo(String orderId) throws FPRuntimeException, FPBusinessException {
        String rate = "";
        //System.out.println("in BP.......");
        rate = tripDAO.getPlannedContainerNo(orderId);
        return rate;
    }

    public String getSingleFourtyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) throws FPRuntimeException, FPBusinessException {
        String rate = "";
        //System.out.println("in BP.......");
        rate = tripDAO.getSingleFourtyRate(customerId, firstPointId, secondPointId, productCategory, movementType);
        return rate;
    }

    public String getSingleTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType, String lastPointId, String thirdPoint) throws FPRuntimeException, FPBusinessException {
        String rate = "";
        //System.out.println("in BP.......");
        rate = tripDAO.getSingleTwentyRate(customerId, firstPointId, secondPointId, productCategory, movementType, lastPointId, thirdPoint);
        return rate;
    }

    public ArrayList getOrderInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderInvoiceHeader(tripTO);
        return result;
    }

    public ArrayList getOrderSuppInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderSuppInvoiceHeader(tripTO);
        return result;
    }

    public ArrayList getOrderInvoiceDetailsList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailsList = new ArrayList();
        invoiceDetailsList = tripDAO.getOrderInvoiceDetailsList(tripTO);
        return invoiceDetailsList;
    }

    public ArrayList getOrderSuppInvoiceDetailsList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailsList = new ArrayList();
        invoiceDetailsList = tripDAO.getOrderSuppInvoiceDetailsList(tripTO);
        return invoiceDetailsList;
    }

    public ArrayList getCreditOrderInvoiceDetailsList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailsList = new ArrayList();
        invoiceDetailsList = tripDAO.getCreditOrderInvoiceDetailsList(tripTO);
        return invoiceDetailsList;
    }

    public ArrayList getTripDieselDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        fuelDetails = tripDAO.getTripDieselDetails(tripTO);
        return fuelDetails;
    }

    public ArrayList getTripGrDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        fuelDetails = tripDAO.getTripGrDetails(tripTO);
        return fuelDetails;
    }

    public String insertTripGr(String tripId, int userId, String Grno) {
        String tripGr = "";
        tripGr = tripDAO.insertTripGr(tripId, userId, Grno);
        return tripGr;
    }

    public int updateTripOtherExpense(String tripId, String[] tripExpenseId, String[] expenses, String[] netexpenses, String[] expenseId, String[] expenseType, String totalRevenue, String[] oldExpenses, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripOtherExpense(tripId, tripExpenseId, expenses, netexpenses, expenseId, expenseType, totalRevenue, oldExpenses, userId);
        return status;
    }

    public int updateTripAddexpense(String expenseId, String expenseDate, String expensesType, String marginValue, String taxExpenses, String expenseValue, String tripSheetId, String driverNameex, String remarks, String totalExpenses, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTripAddexpense(expenseId, expenseDate, expensesType, marginValue, taxExpenses, expenseValue, tripSheetId, driverNameex, remarks, totalExpenses, userId);
        return status;
    }

    public int saveAdvanceGrNo(String noOfGr, String customerId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveAdvanceGrNo(noOfGr, customerId, userId);
        return status;
    }

    public int saveCancelledGrNo(String[] grIds, String[] activeInds, String customerId, String remarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveCancelledGrNo(grIds, activeInds, customerId, remarks, userId);
        return status;
    }

    public ArrayList getAdvanceGrDetails() throws FPBusinessException, FPRuntimeException {
        ArrayList advanceGrDetails = new ArrayList();
        advanceGrDetails = tripDAO.getAdvanceGrDetails();
        return advanceGrDetails;
    }

    public ArrayList getBlockedAdvanceGrDetails(String customerId) throws FPBusinessException, FPRuntimeException {
        ArrayList advanceGrDetails = new ArrayList();
        advanceGrDetails = tripDAO.getBlockedAdvanceGrDetails(customerId);
        return advanceGrDetails;
    }

    public ArrayList getCancelGrDetails() throws FPBusinessException, FPRuntimeException {
        ArrayList advanceGrDetails = new ArrayList();
        advanceGrDetails = tripDAO.getCancelGrDetails();
        return advanceGrDetails;
    }

    public ArrayList getAdvanceGrDetailsForPlanning(String customerId) throws FPBusinessException, FPRuntimeException {
        ArrayList advanceGrDetails = new ArrayList();
        advanceGrDetails = tripDAO.getAdvanceGrDetailsForPlanning(customerId);
        return advanceGrDetails;
    }

    public String getTotalExpense(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        String totalExpense = "";
        totalExpense = tripDAO.getTotalExpense(tripTO);
        return totalExpense;
    }

    public int saveTripCash(TripTO tripTo, String paidCash, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveTripCash(tripTo, paidCash, userId);
        return status;
    }

    public int updateBillingPartyForBilling(String[] tripId, String[] consignmentOrderIds, String[] billOfEntryNo, String billingParty, String billingPartyId, String movementType, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("in BP.......");
        status = tripDAO.updateBillingPartyForBilling(tripId, consignmentOrderIds, billOfEntryNo, billingParty, billingPartyId, movementType, userId);
        return status;
    }

    public int updateBillingPartyForRepoBilling(String[] tripId, String[] consignmentOrderIds, String billingParty, String billingPartyId, String movementType, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        //System.out.println("in BP.......");
        status = tripDAO.updateBillingPartyForRepoBilling(tripId, consignmentOrderIds, billingParty, billingPartyId, movementType, userId);
        return status;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        ArrayList pendingMailList = new ArrayList();
        pendingMailList = tripDAO.getEmailList();
        return pendingMailList;
    }

    public int updateMailStatus(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = tripDAO.updateMailStatus(tripTO);
        return updateStatus;
    }

    public int saveChangeVehicleAfterTripStart(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.saveChangeVehicleAfterTripStart(tripTO, userId);
        return status;
    }

    public int updateEstimateRevenue(TripTO tripTO, Integer userId) {
        int updateEstimateRevenue = 0;
        updateEstimateRevenue = tripDAO.updateEstimateRevenue(tripTO, userId);
        return updateEstimateRevenue;

    }

    public Integer deleteExpenseIds(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.deleteExpenseIds(tripTO, userId);
        return status;
    }

    public int updateAdvanceGrNo(String noOfGr, String grNo, String grId, String customerId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateAdvanceGrNo(noOfGr, grNo, grId, customerId, userId);
        return status;
    }

    public ArrayList getAdvanceTripExpense(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getAdvanceTripExpense = new ArrayList();
        getAdvanceTripExpense = tripDAO.getAdvanceTripExpense(tripTO);
        return getAdvanceTripExpense;
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO) {
        String codeSequence = "";
        codeSequence = tripDAO.getAdvanceVoucherNoSequence(tripTO);
        // generate Invoice code
        if (codeSequence.length() == 1) {
            codeSequence = "000000" + codeSequence;
            //System.out.println("AdvanceVoucher no 1.." + codeSequence);
        } else if (codeSequence.length() == 2) {
            codeSequence = "00000" + codeSequence;
            //System.out.println("AdvanceVoucherNo lenght 2.." + codeSequence);
        } else if (codeSequence.length() == 3) {
            codeSequence = "0000" + codeSequence;
            //System.out.println("AdvanceVoucherNo lenght 3.." + codeSequence);
        } else if (codeSequence.length() == 4) {
            codeSequence = "000" + codeSequence;
        } else if (codeSequence.length() == 5) {
            codeSequence = "00" + codeSequence;
        } else if (codeSequence.length() == 6) {
            codeSequence = "0" + codeSequence;
        }

        return codeSequence;
    }

    public ArrayList getTripExpensePrintDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = tripDAO.getTripExpensePrintDetails(tripTO);
        return tripDetails;
    }

    public int updateTransporter(TripTO tripTO, Integer userId) {
        int updateTransporter = 0;
        updateTransporter = tripDAO.updateTransporter(tripTO, userId);
        return updateTransporter;

    }

    public ArrayList getMapLocationList(String routeId) throws FPRuntimeException, FPBusinessException {
        ArrayList getMapLocationList = new ArrayList();
        getMapLocationList = tripDAO.getMapLocationList(routeId);
        return getMapLocationList;
    }

    public int getTripExistInBilling(String[] tripId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.getTripExistInBilling(tripId);
        return status;
    }

    public ArrayList getContainerAvailability(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList containerAvailability = new ArrayList();
        containerAvailability = tripDAO.getContainerAvailability(tripTO);
        return containerAvailability;
    }

    public ArrayList getContainerVisibility(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList containerVisibility = new ArrayList();
        containerVisibility = tripDAO.getContainerVisibility(tripTO);
        return containerVisibility;
    }

    public ArrayList getComodityDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList comodityDetails = new ArrayList();
        comodityDetails = tripDAO.getComodityDetails();
        return comodityDetails;
    }

    public int updateTripRoute(TripTO tripTO, Integer userId, String tripRouteCourseId, String pointType, String pointSequence, String routeInfo, String newReveneue) {
        int updateTransporter = 0;
        updateTransporter = tripDAO.updateTripRoute(tripTO, userId, tripRouteCourseId, pointType, pointSequence, routeInfo, newReveneue);
        return updateTransporter;

    }

    public int insertTripMasterLog(TripTO tripTO, Integer userId) {
        int updateTransporter = 0;
        updateTransporter = tripDAO.insertTripMasterLog(tripTO, userId);
        return updateTransporter;

    }

    public ArrayList getEmptyShippingBillNoContainerList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList emptyShippingContainerList = new ArrayList();
        emptyShippingContainerList = tripDAO.getEmptyShippingBillNoContainerList(tripTO);
        return emptyShippingContainerList;
    }

    public String getConsignmentContainerId(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String consignmentContainerId = "";
        consignmentContainerId = tripDAO.getConsignmentContainerId(tripTO);
        return consignmentContainerId;
    }

    public String getVendorId(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String consignmentContainerId = "";
        consignmentContainerId = tripDAO.getVendorId(tripTO);
        return consignmentContainerId;
    }

    public String getUploadVehicledId(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String consignmentContainerId = "";
        consignmentContainerId = tripDAO.getUploadVehicledId(tripTO);
        return consignmentContainerId;
    }

    public String getOrderId(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String consignmentContainerId = "";
        consignmentContainerId = tripDAO.getOrderId(tripTO);
        return consignmentContainerId;
    }

    public String getCityId(String cityName) throws FPRuntimeException, FPBusinessException {
        String consignmentContainerId = "";
        consignmentContainerId = tripDAO.getCityId(cityName);
        return consignmentContainerId;
    }

    public int updateStartTripSheetForUpload(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateTripStart = 0;
        updateTripStart = tripDAO.updateStartTripSheetForUpload(tripTO, userId);
        return updateTripStart;
    }

    public int updateEndTripSheetForTripUpload(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateTripStart = 0;
        updateTripStart = tripDAO.updateEndTripSheetForTripUpload(tripTO, userId);
        return updateTripStart;
    }

    public int saveTripClosureDetailsForUpload(TripTO tripTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateTripStart = 0;
        updateTripStart = tripDAO.saveTripClosureDetailsForUpload(tripTO, userId);
        return updateTripStart;
    }

    public int saveUploadTripExpense(TripTO tripTO) throws FPBusinessException, FPRuntimeException, IOException, Exception {
        int status = 0;
        SqlMapClient session = tripDAO.getSqlMapClient();
        try {
            session.startTransaction();
            tripDAO.saveUploadTripExpense(tripTO, session);
            session.commitTransaction();
        } catch (Exception e) {
        }
        return status;
    }

    public String getGrPrintCount(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String count = "";
        count = tripDAO.getGrPrintCount(tripTO);
        return count;
    }

    public String saveGrPrintCount(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        String count = "";
        count = tripDAO.saveGrPrintCount(tripTO);
        return count;
    }

    public ArrayList getSuppCreditOrderInvoiceDetailsList(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList invoiceDetailsList = new ArrayList();
        invoiceDetailsList = tripDAO.getSuppCreditOrderInvoiceDetailsList(tripTO);
        return invoiceDetailsList;
    }

    public ArrayList getSuppInvoiceDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getSuppInvoiceDetails(tripTO);
        return result;
    }

    public ArrayList getSuppInvoiceDetailExpenses(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getSuppInvoiceDetailExpenses(tripTO);
        return result;
    }

    public ArrayList getVehicleWiseBunkList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getBunkName = new ArrayList();
        getBunkName = tripDAO.getVehicleWiseBunkList(tripTO);
        return getBunkName;
    }

    public ArrayList insertOrderUpload(ArrayList shippingLineNoList, int userId) throws FPRuntimeException, FPBusinessException {        
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = tripDAO.insertOrderUpload(shippingLineNoList, userId);
        return getSbillNoListTemp;
    }
    public ArrayList getOrderUploadData(int userId) throws FPRuntimeException, FPBusinessException {        
        ArrayList getOrderUploadData = new ArrayList();
        getOrderUploadData = tripDAO.getOrderUploadData(userId);
        return getOrderUploadData;
    }
    
    
    public String checkInvoiceType(String invoiceId,String invoiceType) throws FPRuntimeException, FPBusinessException {        
        String checkInvoiceType = "";
        checkInvoiceType = tripDAO.checkInvoiceType(invoiceId,invoiceType);
        return checkInvoiceType;
    }
    
    public ArrayList getOrderEInvoiceHeader(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList result = new ArrayList();
        result = tripDAO.getOrderEInvoiceHeader(tripTO);
        return result;
    }
    
    public int updateTollAndDetaintionForBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId,
            String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId,
            String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks, String commodityId,String commodityName,String commodityGstType) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTollAndDetaintionForBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId, consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks,commodityId,commodityName,commodityGstType);
        return status;
    }
    
    
      public int updateTollAndDetaintionForSupplemetBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId,
            String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId,
            String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks,String commodityId,String commodityName, String commodityGstType) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tripDAO.updateTollAndDetaintionForSupplemetBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId, consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks,commodityId,commodityName,commodityGstType);
        return status;
    }

//    public ArrayList insertShippingLineNoTemp(ArrayList shippingLineNoList, int userId) throws FPRuntimeException, FPBusinessException {
//        int insertshippingLineNoList = 0;
//        ArrayList getSbillNoListTemp = new ArrayList();
//        getSbillNoListTemp = tripDAO.insertShippingLineNoTemp(shippingLineNoList, userId);
//        return getSbillNoListTemp;
//    }
    public int updateShipBillReq(String fromDate,String toDate,String movementType,int userId) throws FPRuntimeException, FPBusinessException {
        int updateShipBillReq = 0;
        updateShipBillReq = tripDAO.updateShipBillReq(fromDate,toDate,movementType,userId);
        return updateShipBillReq;
    }
    public ArrayList getShipBillReqList() throws FPRuntimeException, FPBusinessException {
        ArrayList getShipBillReqList = new ArrayList();
        getShipBillReqList = tripDAO.getShipBillReqList();
        return getShipBillReqList;
    }
    public ArrayList getShippingListForReq(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShippingListForReq = new ArrayList();
        getShippingListForReq = tripDAO.getShippingListForReq(tripTO);
        return getShippingListForReq;
    }
    public ArrayList getShippingListForReqErr(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShippingListForReqErr = new ArrayList();
        getShippingListForReqErr = tripDAO.getShippingListForReqErr(tripTO);
        return getShippingListForReqErr;
    }
      public int invoiceResend(TripTO tripTO) throws FPBusinessException, FPRuntimeException, IOException, Exception {
        int invoiceResend = 0;
        try {
          invoiceResend = tripDAO.invoiceResend(tripTO);
        } catch (Exception e) {
        }
        return invoiceResend;
    }  
        public ArrayList getResendMailListDetails(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
        ArrayList getResendMailListDetails = new ArrayList();
        getResendMailListDetails = tripDAO.getResendMailListDetails(tripTO);
        return getResendMailListDetails;
    }
        
          public int getNOfBillInHeader(String[] tripId) throws FPRuntimeException, FPBusinessException {
        int getNOfBillInHeader = 0;
        getNOfBillInHeader = tripDAO.getNOfBillInHeader(tripId);
        return getNOfBillInHeader;
    }
          public int getGstTypesInTrip(String[] tripId) throws FPRuntimeException, FPBusinessException {
        int getGstTypesInTrip = 0;
        getGstTypesInTrip = tripDAO.getGstTypesInTrip(tripId);
        return getGstTypesInTrip;
    }
           public void updateExpensiveItem(String expenseId, String expenseDate, String expenseType, String expenseAmountvalue, String totalExpenseAmount, int userId,String expenseRemarksEdit)
            throws FPBusinessException, FPRuntimeException {
        //System.out.println("Bp..===========================================>");
        int status = tripDAO.updateExpensiveItem(expenseId, expenseDate,expenseType, expenseAmountvalue, totalExpenseAmount, userId,expenseRemarksEdit);
    }
             public ArrayList getEmailCount() throws FPRuntimeException, FPBusinessException {
        ArrayList getEmailCount = new ArrayList();
        getEmailCount = tripDAO.getEmailCount();
        return getEmailCount;
    }
}
