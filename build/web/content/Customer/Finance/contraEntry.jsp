

     
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


        <script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>


    </head>
    <script language="javascript">


        function onKeyPressBlockCharacters(e)
        {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z]+$/;

            return !reg.test(keychar);

        }


        function bankIdVal()
        {
            var bankid1 = document.getElementsByName("bankid1");
            var bankid2 = document.getElementsByName("bankid2");
            //            alert("bankid1=== "+textValidation(bankid1[i]);
            // alert("bankid2=== "+bankid2);
        }
        function setSelectbox(i)
        {
            var selected = document.getElementsByName("selectedIndex");
            alert("selected==" + selected[i]);
            selected[i].checked = 1;

        }

       
        function submitPage1(value)
        {
            if (value == 'Save') {
                if (document.getElementById('fromDate').value == "") {
                    alert("please select contra entry date");
                    document.getElementById('fromDate').focus();
                } else {
                    var checValidate = selectedItemValidation();
                }
                //var checValidate = selectedItemValidation();
                if (checValidate == 'SubmitForm') {
                    //validattion
                    document.manufacturer.action = '/throttle/insertContraEntry.do';
                    document.manufacturer.submit();
                }
            }
        }

        function submitPage(value)
        {
            if (value == 'Save') {
                if (document.getElementById('fromDate').value == "") {
                    alert("please select contra entry date");
                    document.getElementById('fromDate').focus();
                } else if (selectedItemValidation() == 'SubmitForm') {
                    //validattion
                    var totalCreditAmt = document.getElementById('totalCreditAmt').value;
                    var totalDebitAmt = document.getElementById('totalDebitAmt').value;
                    if (parseFloat(totalDebitAmt).toFixed(2) != parseFloat(totalCreditAmt).toFixed(2)) {
                        alert("credit and debit amount mismatch");
                        return false;
                    } else {
                        document.manufacturer.action = '/throttle/insertContraEntry.do';
                        document.manufacturer.submit();
                    }
                }
            }
        }

        /* function setCheckBox(val) {
         document.getElementById("selectedIndex" + val).checked = true;
         
         }
         */

         function selectedItemValidation() {
            var bankId = document.getElementsByName("bankid1");
            var amount = document.getElementsByName("amount");
            var narration = document.getElementsByName("narration");
            var chec = 0;
            var index = 0;
            var mess = "SubmitForm";
            for (var i = 0; i < amount.length; i++) {
                index = parseInt(i) + parseInt(1);
                chec++;
                if (bankId[i].value == 0) {
                    alert("select Ledger Name for row " + index);
                    mess = 'NotSubmit';
                } else if (amount[i].value == "") {
                    alert("select amount for row " + index);
                    mess = 'NotSubmit';
                } else if (narration[i].value == "") {
                    alert("Give narration for row " + index);
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';

                }
            }
            if (mess == 'SubmitForm') {

                if (chec == 0) {
                    alert("Please click ADDROW And Then Proceed");
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';
                }
            }
            return mess;
        }
        function setValues(sno, accountEntryIds, accountsAmount, debitAmount, voucherCode, accountEntryDate, headerNarration, fullName, voucherId, accountsType) {
            //alert(fullName);
            var temp = accountEntryIds.split(",");
            var tempOne = fullName.split(",");
            var tempTwo = accountsAmount.split(",");
            // var temp3 = headerNarration.split(",");
            var tempFour = accountsType.split(",");
            var tempThree = document.getElementById('entryNarration' + sno).value.split("@");

            var index = 0;
            DeleteRow();
            var chechkbox = document.getElementsByName("edit");
            for (i = 0; i < chechkbox.length; i++) {
                document.getElementById('edit' + i).checked = false;
            }
            document.getElementById('edit' + sno).checked = true;
            if (document.getElementById('edit' + sno).checked == true) {
                for (var i = 0; i < temp.length; i++) {
                    index = parseInt(i) + parseInt(1);
                    showRow();
                    document.getElementById('amount' + index).value = tempTwo[i];
                    document.getElementById('bankId' + index).value = tempOne[i];
                    document.getElementById('narration' + index).innerHTML = tempThree[i];
                    document.getElementById('accType' + index).value = tempFour[i];
                    document.getElementById('selectedIndex' + index).checked = true;
                }
                document.getElementById('totalDebitAmt').value = debitAmount;
                document.getElementById('totalCreditAmt').value = debitAmount;
                document.getElementById('voucherCode').value = voucherCode;
                document.getElementById('voucherIdEdit').value = voucherId;
                document.getElementById('fromDate').value = accountEntryDate;

            }
        }

        var rowCount = 1;
        var sno = 0;
        var serialNo = 0;
        var style = "text2";
        function showRow()
        {
            if (rowCount % 2 == 0) {
                style = "text2";
            } else {
                style = "text1";
            }
            sno++;
            var tab = document.getElementById("addRow");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell1 = "<td class='text1' height='25' >" + sno + "</td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(1);
//            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId"+sno+"'  style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${bankLedgerList != null}" ><c:forEach items="${bankLedgerList}" var="BLL"><option  value='<c:out value="${BLL.ledgerID}" />~<c:out value="${BLL.groupCode}" />~<c:out value="${BLL.levelID}" />~<c:out value="${BLL.ledgerCode}" />'><c:out value="${BLL.ledgerName}" /></c:forEach ></c:if></select></td>";
            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId" + sno + "'  style='width:190px;height:40px;'  name='bankid1'><option selected value=0>---<spring:message code="finance.label.Select"  text="default text"/>---</option><c:if test = "${paymentLedgerList != null}" ><c:forEach items="${paymentLedgerList}" var="JLL"><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:forEach ></c:if></select></td>";
//            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId" + sno + "' onchange='setCheckBox(" + sno + ")' style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${bankLedgerList != null}" ><c:forEach items="${bankLedgerList}" var="BLL"><option  value='<c:out value="${BLL.ledgerID}" />~<c:out value="${BLL.groupCode}" />~<c:out value="${BLL.levelID}" />~<c:out value="${BLL.ledgerCode}" />'><c:out value="${BLL.ledgerName}" /></c:forEach ></c:if></select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell2;


            cell = newrow.insertCell(2);
            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount" + sno + "' maxlength='13'onkeyup='sumAmt(" + sno + ");' onclick='bankIdVal(sno-1)'  size='20' class='form-control'  style='width:190px;height:40px;'/></td>";
//            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount"+sno+"' maxlength='13' onclick='bankIdVal(sno-1)' onchange='setCheckBox("+sno+")' size='20' class='form-control' onkeypress='return onKeyPressBlockCharacters(event);' /></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(3);
//            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType"+sno+"'  class='form-control'><option value='CREDIT' selected>CREDIT</option><option value='DEBIT'>DEBIT</option></select></div> </td>";
            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType" + sno + "' onchange='sumAmt(" + sno + ");' class='form-control' style='width:190px;height:40px;'><option value='CREDIT' selected><spring:message code="finance.label.CREDIT"  text="default text"/></option><option value='DEBIT'><spring:message code="finance.label.DEBIT"  text="default text"/></option></select></div> </td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(4);
//            var cell5 = "<td class='text1' height='30'><input type='text' name='narration' id='narration"+sno+"'  size='20' class='form-control' /></td>";
            var cell5 = "<td class='text1' height='30'><textarea name='narration' id='narration" + sno + "'  size='20' class='form-control' style='width:190px;height:40px;'/></textarea></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell5;

            cell = newrow.insertCell(5);
//            var cell6 = "<td class='text1' height='30'><input type='checkbox'  name='selectedIndex' value='"+sno+"'/></td>";
            var cell6 = "<td class='text1' height='30'><input type='checkbox'  id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "'  style='width:15px;'/></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell6;

            rowCount++;
        }

        function DeleteRow() {
            try {
                var table = document.getElementById("addRow");
                rowCount = table.rows.length;
                var rCount = rowCount - 1;
                for (var i = 1; i < rowCount; i++) {
                    var row = table.rows[i];
                    var checkbox = row.cells[5].childNodes[0];
                    if (true == checkbox.checked) {
                        if (rCount < 1) {
                            alert("Cannot delete all the rows");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                        sno--;
                        // snumber--;
                    }
                }
            } catch (e) {
                alert(e);
            }
        }
        function sumAmt(val) {
            var sumAmt = 0;
            var accType = 0;
            var credit = 0;
            var debit = 0;
            sumAmt = document.getElementsByName('amount');
            accType = document.getElementsByName('accType');
            for (i = 0; i < sumAmt.length; i++) {
                if (sumAmt[i].value != "") {
                    if (accType[i].value == "CREDIT") {
                        credit += parseFloat(sumAmt[i].value);
                        document.getElementById('totalCreditAmt').value = parseFloat(credit).toFixed(2);
                        //alert(credit);
                    } else if (accType[i].value == "DEBIT") {
                        debit += parseFloat(sumAmt[i].value);
                        document.getElementById('totalDebitAmt').value = parseFloat(debit).toFixed(2);
                        //alert(credit);
                    }
                }
//                else{
//                document.getElementById('totalCreditAmt').value=0;
//                document.getElementById('totalDebitAmt').value=0;
//                }

            }
        }

        function printContraEntry(val, voucherNo) {
            document.manufacturer.action = '/throttle/printContraEntry.do?voucherCode=' + val + '&voucherNo=' + voucherNo;
            document.manufacturer.submit();
        }
        function viewCostCenterCashPayment(creditAccountEntryId, debitAccountEntryId, debitAmount, creditAmount, voucherCode, creditLedgerName, debitLedgerName) {
            var accountEntryIds = creditAccountEntryId + ',' + debitAccountEntryId;
//           alert(accountEntryID);
            document.manufacturer.action = '/throttle/viewCostCenterCashPayment.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Contra Entry&voucherCode=' + voucherCode + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName;
            document.manufacturer.submit();
        }


        function costCenterCashPaymentEditView(creditAccountEntryId, debitAccountEntryId, debitAmount, creditAmount, voucherCode, val, creditLedgerName, debitLedgerName) {
            var accountEntryIds = creditAccountEntryId + ',' + debitAccountEntryId;
//           alert(accountEntryID);
            document.manufacturer.action = '/throttle/costCenterCashPaymentEditView.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Contra Entry&voucherCode=' + voucherCode + '&Param=' + val + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName;
            document.manufacturer.submit();
        }

            </script>
            
            
            <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ContraEntryDate"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
         <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ContraEntryDate"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            
            
            
            <body onload="showRow();">
                <form name="manufacturer" method="post" >
                       <%@ include file="/content/common/message.jsp" %>
            <input type="hidden" id="voucherCode" name="voucherCode" size="20" class="form-control" value="">
            <input type="hidden" id="voucherIdEdit" name="voucherIdEdit" size="20" class="form-control" value="">
            <div align="center">
                 <table class="table table-info mb30 table-hover">
                     <thead>
                <tr id="index">
                        <th  colspan="6"  ><spring:message code="finance.label.ContraEntry"  text="default text"/></th>
                    </tr>
                     </thead>
                    <tr>
                        <td  height="30" width="150px"><spring:message code="finance.label.ContraEntryDate"  text="default text"/> </td>
                        <td  height="30"><input type="text" name="fromDate"  autocomplete="off" id="fromDate" size="20" class="datepicker" value="" style="width:260px;height:40px;">
                        <td  height="30"><spring:message code="finance.label.Credit"  text="default text"/></td>
                        <td  height="30">
                            <input type="text" id="totalCreditAmt" name="totalCreditAmt" value=""  size="20" class="form-control" onkeypress="return onKeyPressBlockCharacters(event);" readonly style="width:260px;height:40px;">
                        </td>
                        <td  height="30"><spring:message code="finance.label.Debit"  text="default text"/></td>
                        <td  height="30">
                            <input type="text" id="totalDebitAmt" name="totalDebitAmt" value=""  size="20" class="form-control" onkeypress="return onKeyPressBlockCharacters(event);" readonly style="width:260px;height:40px;">
                        </td>
                    </tr>
                </table></br></br>
                <!--<table width="1000" class="border" border="3" id="addRow" class="table table-bordered">-->
                    <table class="table table-info mb30 table-hover" id="addRow">
                        <thead>                        
                    <tr id="index">
                        <th  ><spring:message code="finance.label.Sno"  text="default text"/></th>
                        <th  ><spring:message code="finance.label.LedgerName"  text="default text"/> </th>
                        <th  ><spring:message code="finance.label.Amount"  text="default text"/></th>
                        <th  ><spring:message code="finance.label.AccountType"  text="default text"/></th>
                        <th  ><spring:message code="finance.label.Narration"  text="default text"/></th>
                        <th  >***</th>
                    </tr>
                        </thead>    
                </table>
            </div>
            <center>
                <input type="button" value="<spring:message code="finance.label.AddRow"  text="default text"/>" class="btn btn-success" onClick="showRow();" style="width:100px;height:35px;">&nbsp;&nbsp;
                <input type="button" value="<spring:message code="finance.label.DeleteRow"  text="default text"/>" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:35px;">&nbsp;&nbsp;
                <input type="button" value="<spring:message code="finance.label.Save"  text="default text"/>" class="btn btn-success" onClick="submitPage(this.value);" style="width:100px;height:35px;">
            </center>
            <br>

            <c:if test="${contraCodeList != null}">
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                    <th><spring:message code="finance.label.EntryDate"  text="default text"/></th>
                    <th><spring:message code="finance.label.CreditVoucher"  text="default text"/></th>
                    <th><spring:message code="finance.label.CreditLedger"  text="default text"/></th>
                    <th><spring:message code="finance.label.CreditAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.DebitLedger"  text="default text"/></th>
                    <th><spring:message code="finance.label.DebitAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                    <th><spring:message code="finance.label.Print"  text="default text"/></th>
                    <th><spring:message code="finance.label.Edit"  text="default text"/></th>  
                    <!--<th><h3>Cost Center</h3></th>-->
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${contraCodeList}" var="code">
                            
                            <tr height="30">
                                <td   align="left"> <%= index + 1%> </td>
                                <td   align="left"> <c:out value="${code.accountEntryDate}"/> </td>
                                <td  align="left"> 
                                    <%--<c:out value="${code.voucherCode}" />--%>
                                    <c:out value="${code.voucherNo}" />
                                </td>
                                <c:set var="totalCreditAmount" value="0"/>
                                <c:set var="creditLedger" value=""/>
                                <c:set var="totalDebitAmount" value="0"/>
                                <c:set var="debitLedger" value=""/>
                                <c:if test = "${contraEntryCreditList != null}" >
                                    <c:set var="creditCount" value="${0}"/>
                                    <c:forEach items="${contraEntryCreditList}" var="CL">
                                        <c:if test="${CL.creditVoucherCode == code.voucherCode}">
                                            <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                            <c:set var="creditLedger" value="${CL.creditLedgerName}"/>
                                            <c:set var="creditAccountEntryId" value="${CL.accountEntryId}"/>
                                            <c:set var="creditAccountEntryId" value="${CL.accountEntryId}"/>
                                            <c:set var="creditCount" value="${CL.creditCount}"/>
                                        </c:if>
                                    </c:forEach>
                                    <td  align="left"> <c:out value="${creditLedger}" /></td>
                                    <td  align="left"> <c:out value="${totalCreditAmount}" /></td>
                                </c:if>
                                <c:if test="${contraEntryDebitList != null}">
                                    <c:set var="debitCount" value="${0}"/>
                                    <c:forEach items="${contraEntryDebitList}" var="DL">
                                        <c:if test="${DL.debitVoucherCode == code.voucherCode}">
                                            <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                            <c:set var="debitLedger" value="${DL.debitLedgerName}"/>
                                            <c:set var="debitAccountEntryId" value="${DL.accountEntryId}"/>
                                            <c:set var="debitCount" value="${DL.debitCount}"/>
                                        </c:if>
                                    </c:forEach>
                                    <td  align="left"> <c:out value="${debitLedger}" /></td>
                                    <td  align="left"> <c:out value="${totalDebitAmount}" /></td>
                                </c:if>
                                <td  align="left"> <c:out value="${code.headerNarration}" /></td>
                                <td   align="left"><a href="" onclick="printContraEntry('<c:out value="${code.voucherCode}" />', '<c:out value="${code.voucherNo}" />')">Print</a></td>
                                <td  align="center"> 
                                    <input type="checkbox" name="edit" align="center" id="edit<%=index%>" onclick="setValues('<%=index%>', '<c:out value="${code.accountEntryId}" />', '<c:out value="${code.accountsAmount}"/>', '<c:out value="${totalDebitAmount}"/>', '<c:out value="${code.voucherCode}" />', ' <c:out value="${code.accountEntryDate}"/> ', '0', '<c:out value="${code.fullName}"/>', '<c:out value="${code.voucherId}"/>', '<c:out value="${code.accountsType}"/>');" />
                                    <input type="hidden" id="entryNarration<%=index%>" name="entryNarration"  value="<c:out value="${code.entryNarration}"/>">
                                </td>

<%--                                <td   align="left">
                                    <c:if test="${creditCount > 0 || debitCount > 0}" >
                                        <c:if test="${code.status == 0}" >
                                            <a href="" onclick="viewCostCenterCashPayment('<c:out value="${creditAccountEntryId}" />', '<c:out value="${debitAccountEntryId}" />', '<c:out value="${totalDebitAmount}"/>', '<c:out value="${totalCreditAmount}"/>', '<c:out value="${code.voucherCode}" />', '<c:out value="${creditLedger}"/>', '<c:out value="${debitLedger}"/>')">Create</a>
                                        </c:if>

                                        <c:if test="${code.status == 1}" >
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${creditAccountEntryId}" />', '<c:out value="${debitAccountEntryId}" />', '<c:out value="${totalDebitAmount}"/>', '<c:out value="${totalCreditAmount}"/>', '<c:out value="${code.voucherCode}" />', 'View', '<c:out value="${creditLedger}"/>', '<c:out value="${debitLedger}"/>')">View</a>&nbsp;/
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${creditAccountEntryId}" />', '<c:out value="${debitAccountEntryId}" />', '<c:out value="${totalDebitAmount}"/>', '<c:out value="${totalCreditAmount}"/>', '<c:out value="${code.voucherCode}" />', 'Edit', '<c:out value="${creditLedger}"/>', '<c:out value="${debitLedger}"/>')">Edit</a>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${creditCount == 0 && debitCount == 0}" >
                                        &nbsp;
                                    </c:if>

                                </td>    --%>
                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>