<%-- 
    Document   : FCWiseTripSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="fcWiseTripSummary" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "FCWiseTripSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            
            <c:if test = "${totalSummaryDetailsNWLoaded != null && totalSummaryDetailsNWEmpty != null}" >
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3></h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Operated</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise In-Progress</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise WFU</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Ended</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Closed</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise Settled</h3></th>
                            <th colspan="3"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;FC wise ExtraExpense</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>FC</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                            <td>Empty</td>
                            <td>Loaded</td>
                            <td>Total</td>
                        </tr>
                        <tr>
                            <td>NW</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNWEmptyExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp}"/></td>
                        </tr>
                        <tr>
                            <td>NS</td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsNSEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNSLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Gen</td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                        <tr>
                            <td>Tot</td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNSEmpty+totalSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWLoaded+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${totalSummaryDetailsNWEmpty+totalSummaryDetailsNWLoaded+totalSummaryDetailsNSEmpty+totalSummaryDetailsNSLoaded+totalSummaryDetailsGNEmpty+totalSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${inProgressSummaryDetailsNWEmpty+inProgresstotalSummaryDetailsNWLoaded+inProgressSummaryDetailsNSEmpty+inProgressSummaryDetailsNSLoaded+inProgressSummaryDetailsGNEmpty+inProgressSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${wfuSummaryDetailsNWEmpty+wfutotalSummaryDetailsNWLoaded+wfuSummaryDetailsNSEmpty+wfuSummaryDetailsNSLoaded+wfuSummaryDetailsGNEmpty+wfuSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNSEmpty+endSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${endSummaryDetailsNWLoaded+endSummaryDetailsNSLoaded+endSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${endSummaryDetailsNWEmpty+endSummaryDetailsNWLoaded+endSummaryDetailsNSEmpty+endSummaryDetailsNSLoaded+endSummaryDetailsGNEmpty+endSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNSEmpty+closedSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWLoaded+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${closedSummaryDetailsNWEmpty+closedSummaryDetailsNWLoaded+closedSummaryDetailsNSEmpty+closedSummaryDetailsNSLoaded+closedSummaryDetailsGNEmpty+closedSummaryDetailsGNLoaded}"/></td>

                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNSEmpty+settledSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWLoaded+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${settledSummaryDetailsNWEmpty+settledSummaryDetailsNWLoaded+settledSummaryDetailsNSEmpty+settledSummaryDetailsNSLoaded+settledSummaryDetailsGNEmpty+settledSummaryDetailsGNLoaded}"/></td>
                            
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNSEmpty+expSummaryDetailsGNEmpty}"/></td>
                            <td><c:out value="${expSummaryDetailsNWLoadedExp+expSummaryDetailsNSLoaded+expSummaryDetailsGNLoaded}"/></td>
                            <td><c:out value="${expSummaryDetailsNWEmptyExp+expSummaryDetailsNWLoadedExp+expSummaryDetailsNSEmpty+expSummaryDetailsNSLoaded+expSummaryDetailsGNEmpty+expSummaryDetailsGNLoaded}"/></td>
                        </tr>
                    </tbody>
                    
                </table>
            </c:if>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
       
    </body>    
</html>