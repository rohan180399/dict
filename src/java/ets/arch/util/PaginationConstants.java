/*---------------------------------------------------------------------------
 * PaginationConstants.java
 * Mar 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/
package ets.arch.util;
/**
*
* Modification Log
* ---------------------------------------------------------------------
* Ver   Date              Modified By               Description
* ---------------------------------------------------------------------
* 1.0   26 Mar 2008       Satheeshkumar P            Initial Version
*
***********************************************************************/
/**
* PaginationConstants:All runtime exceptions of FP will throw an object of this type.
* <br>This Class gets the details of the error thrown from xml
* holding those values.
* <br>An error is logged in the Log file with the error details.
* @author SatheeshKumar P
* @version 1.0 26 Mar 2008
* @since   event Portal Iteration 0
*/

public class PaginationConstants {

	static FPUtil fpUtil = FPUtil.getInstance();
	/**
	 * NO_RECORDS -No of records
	 */
	public static final int    NO_RECORDS				  = 0;
	/**
	 * NO_OF_RECORDS_PER_PAGE -No of records per page
	 */
	public static final int    NO_OF_RECORDS_PER_PAGE              =
		Integer.parseInt(fpUtil.getInstance().getProperty("NO_OF_RECORDS_PER_PAGE"));
	/**
	 * NO_OF_PAGE -No of Pages
	 */
	public static final int    NO_OF_PAGE                 = 5;
	/**
	 * SEARCH_PAGE -SEARCH_PAGE
	 */
	public static final int    SEARCH_PAGE                = - 1;
	/**
	 * DEFAULT_PAGE - DEFAULT_PAGE
	 */
	public static final int    DEFAULT_PAGE               = 1;
	/**
	 * PERCENTAGE_PAGE -PERCENTAGE_PAGE
	 */
	public static final int    PERCENTAGE_PAGE            = 100;
	/**
	 * CHECK_DISPLAY_INACTIVE-CHECK_DISPLAY_INACTIVE
	 */
	public static final int    CHECK_DISPLAY_INACTIVE     = 0;
	/**
	 * CHECK_DISPLAY_ACTIVE - CHECK_DISPLAY_ACTIVE
	 */
	public static final int    CHECK_DISPLAY_ACTIVE       = 1;
	/**
	 * CHECK_DISPLAY_HIDDEN -CHECK_DISPLAY_HIDDEN
	 */
	public static final int    CHECK_DISPLAY_HIDDEN       = 2;

/**
 * MAX_NO_OF_RECORDS -MAX_NO_OF_RECORDS
 */
	public static final int MAX_NO_OF_RECORDS = 358;
}
