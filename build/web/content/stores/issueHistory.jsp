
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRSHistory"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.MRSHistory"  text="MRS"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        
        
        <script>
            function submitPage(val){
                if(val=='approve'){
                    document.mpr.status.value="APPROVED"
                    document.mpr.action="/throttle/approveMpr.do"     
                    document.mpr.submit();
                }else if(val=='reject'){
                    document.mpr.status.value="REJECTED"                
                    document.mpr.action="/throttle/approveMpr.do"                
                    document.mpr.submit();
                }
            }            
            
        </script>
        
        <form name="mpr"  method="post" >                    
            <%--<%@ include file="/content/common/path.jsp" %>--%>            
            <!-- pointer table -->
           
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>                
            <br>
<% int index = 0; %>                

            <br>    
                <%index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${itemList != null}" >
            <table class="table table-info mb30 table-hover" id="bg" >              
                    <thead>
                <tr>
                    <th colspan="9"><center><spring:message code="stores.label.MRSIssueHistory"  text="default text"/></center> </th>
                </tr> 
                        <tr>
                            <th  height="30"><div ><spring:message code="stores.label.Sno"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.MfrCode"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.PaplCode"  text="default text"/></div></th>                       
                            <th  height="30"><div ><spring:message code="stores.label.ItemName"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.TyreNo"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.Uom"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.Quantity"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.ActionType"  text="default text"/></div></th>
                            <th  height="30"><div ><spring:message code="stores.label.IssuedOn"  text="default text"/></div></th>
                            <!--<td class="text2" height="30"><b>Fault Status</b> </td>    -->                                                                                                  
                        </tr>                
                    </thead>
                    <c:forEach items="${itemList}" var="item"> 
                        <%
            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            
                            <td class="<%=classText %>" height="30"><%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemMfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsPaplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.tyreNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.quantityRI}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.actionType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.issuedOn}"/></td>
                            <!--<td class="<%=classText %>" height="30"><c:out value="${item.faultStatus}"/></td>  --> 
                        </tr>
                        <%
                        index++;
                        %>
                    </c:forEach>                               
            </table> 
            <br>
             <center>   
            <input type="button" class="btn btn-success" name="reject" value="<spring:message code="stores.label.Print"  text="default text"/>" onClick="" >
            </center>  
        </c:if>               
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
