
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<style type="text/css">
    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }
</style>
<script>
    $(document).ready(function() {
        $('.ball, .ball1').removeClass('stop');
        $('.trigger').click(function() {
            $('.ball, .ball1').toggleClass('stop');
        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    //auto com

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getRegistrationNo.do",
                    dataType: "json",
                    data: {
                        regno: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                $('#vehicleNo').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>



<script type="text/javascript">
    function submitPage(val) {
        var fromDate = document.getElementById('fromDate').value;
        var toDate = document.getElementById('toDate').value;
        var date1 = new Date(fromDate.split("-")[2], fromDate.split("-")[1] - 1, fromDate.split("-")[0]);
        var date2 = new Date(toDate.split("-")[2], toDate.split("-")[1] - 1, toDate.split("-")[0]);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays > 185) {
            alert("Please Select Date Less Than 6 Months")
        }
        else {

            if (val == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/handleGRSummary.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/handleGRSummary.do?param=search';
                document.accountReceivable.submit();
            }
          }


    }
//    function submitPage(value) {
//        if (document.getElementById('fromDate').value == '') {
//            alert("Please select from Date");
//            document.getElementById('fromDate').focus();
//        } else if (document.getElementById('toDate').value == '') {
//            alert("Please select to Date");
//            document.getElementById('toDate').focus();
////                } else if (document.getElementById('grNo').value == '') {
////                    alert("Please select GR NO");
////                    document.getElementById('grNo').focus();
////                } else if (document.getElementById('consignorNo').value == '') {
////                    alert("Please select consignor No");
////                    document.getElementById('consignorNo').focus();
////                } else if (document.getElementById('vehicleNo').value == '') {
////                    alert("Please select vehicleNo");
////                    document.getElementById('vehicleNo').focus();
//        } else {
//            if (value == 'ExportExcel') {
//                document.accountReceivable.action = '/throttle/handleGRSummary.do?param=ExportExcel';
//                document.accountReceivable.submit();
//            } else {
//                document.accountReceivable.action = '/throttle/handleGRSummary.do?param=Search';
//                document.accountReceivable.submit();
//            }
//        }
//    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function viewCustomerProfitDetails(tripIds) {
//            alert(tripIds);
        window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">GR Summary</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="getVehicleNos();">
                <!--<body>-->
                <form name="accountReceivable" action=""  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table style="width:100%" align="center" border="0" class="tabouterborder" >
                        <tr height="30"   >
                            <td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">GR Summary</td></tr>
                        <tr height="40">
                            <td class="tabtext"><font color="red"></font>GR No</td>
                            <td  class="tabtext" height="30"><input name="grNo" id="grNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${grNo}"/>" ></td>
                            <td class="tabtext"><font color="red"></font>Container No</td>
                            <td  class="tabtext" height="30"><input name="containerNo" id="containerNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${containerNo}"/>"></td>
                            <!--                        </tr>
                                                     <tr height="40">-->
                            <td  class="tabtext"><font color="red"></font>Vehicle No</td>
                            <td  class="tabtext" height="30"><input name="vehicleNo" id="vehicleNo" type="text" class="textbox" style="width:180px;height:25px;" value="<c:out value="${vehicleNo}"/>" ></td>
                            <td  class="tabtext"><font color="red"></font>Movement Type</td>
                            <td  class="tabtext" height="30">
                                <select name="movementType" id="movementType" type="text" class="textbox" style="width:180px;height:25px;" >
                                    <option value=''>--select--</option>
                                    <option value='Import'>Import</option>
                                    <option value='Export'>Export</option>
                                    <option value='Repo'>Repo</option>
                                    <option value='Import DSO'>Import DSO</option>
                                    <option value='Export DSO'>Export DSO</option>
                                </select>
                                <script>
                                    document.getElementById("movementType").value = '<c:out value="${movementType}"/>'
                                </script>
                                <!--<input name="movementType" id="movementType" type="text" class="text" value="<c:out value="${movementType}"/>"></td>-->
                        </tr>
                        <tr height="40">
                            <td  class="tabtext" ><font color="red">*</font>From Date</td>
                            <td  class="tabtext" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:25px;color:black" value="<c:out value="${fromDate}"/>" ></td>
                            <td class="tabtext"><font color="red">*</font>To Date</td>
                            <td  class="tabtext" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:25px;color:black" value="<c:out value="${toDate}"/>"></td>
                            <!--                        </tr>
                                                    <tr height="40">-->
                            <td class="tabtext"><font color="red"></font>Trip status</td>
                            <td  class="tabtext">
                                <select class="textbox"  id="tripStatus"  name="tripStatus" style="width:180px;height:25px;" value="">
                                    <option selected  value="">---Select---</option>
                                    <c:if test = "${tripStatusList != null}" >
                                        <c:forEach items="${tripStatusList}" var="trip">
                                            <option  value='<c:out value="${trip.tripStatusId}" />'> <c:out value="${trip.tripStatusName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>

                        <script>
                            document.getElementById("tripStatus").value = '<c:out value="${tripStatus}"/>'
                        </script>
                        <td colspan="2" align="center"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;">&nbsp;&nbsp;
                            <input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    <c:if test = "${GRSummaryList == null && GRSummaryListSize != null}" >
                        <center>
                            <font color="blue">Please Wait Your Request is Processing</font>
                            <div class="container">
                                <div class="content">
                                    <div class="circle"></div>
                                    <div class="circle1"></div>
                                </div>
                            </div>
                        </center>
                    </c:if>


                    <c:if test = "${GRSummaryList != null && GRSummaryListSize == null}" >
                        <!--<table border="1" id="table" align="center" width="100%" cellpadding="0" cellspacing="1" >-->
                        <table align="center" border="0" id="table" class="table table-info mb30 table-hover" >
                            <thead>
                                <tr id="tableDesingTH">
                                    <th rowspan="2" >S.No</th>
                                    <th rowspan="2" >Booking Office</th>
                                    <th rowspan="2" >Empty Pick Up</th>
                                    <th rowspan="2" >Movement Type</th>
                                    <th rowspan="2" >G.R No.</th>
                                    <th rowspan="2" >Shipping Bill No.</th>
                                    <th rowspan="2" >BOE</th>
                                    <th rowspan="2" >Trip Start</th>
                                    <th rowspan="2" >Trip End</th>
                                    <th rowspan="2" >Trip Code</th>
                                    <th rowspan="2" >Date of Issue</th>
                                    <th rowspan="2" >GR Submission Date</th>
                                    <th rowspan="2" >Trip Status.</th>
                                    <th rowspan="2" >Trip Sheet Submit</th>
                                    <th rowspan="2" >TPT Name</th>
                                    <th rowspan="2" >Diesel Qtty.</th>
                                    <th rowspan="2" >Amount</th>
                                    <th colspan="15"  style="text-align:center">Trip Expenses</th>
                                    <th rowspan="2" >Total Other Expense</th>
                                    <th rowspan="2" >Total Trip Expense</th>


                                    <th colspan="3"  style="text-align:center">Shipper Detail</th>
                                    <th colspan="2"  style="text-align:center">Movement </td>
                                    <th colspan="6"  style="text-align:center">Conatiner Details </th>
                                    <th colspan="6"  style="text-align:center">Revenue </th>
                                    <th rowspan="2">Total Revenue</th>
                                </tr>
                                <tr id="tableDesingTH">
                                    <th  >cashDiseslAmount</th>
                                    <th  >toll</th>
                                    <th  >Tea/Food</th>
                                    <th  >R&M</th>
                                    <th  >weightment</th>
                                    <th  >octroi</th>
                                    <th  >bhati</th>
                                    <th  >Trip Expense</th>
                                    <th  >misc</th>
                                    <th  >Extra Fooding</th>
                                    <th  >Green Tax</th>
                                    <th  >Detaintion</th>
                                    <th  >Other Expense</th>
                                    <th  >Driver Batta</th>
                                    <th  >Dala</th>
                                    <th >Consignee</th>
                                    <th >Consignor </th>
                                    <th >Billing Party </th>
                                    <th >Origin </th>
                                    <th >Destination </th>
                                    <th >Conatiner No</th>
                                    <th >20' </th>
                                    <th >40' </th>
                                    <th >Type L/E </th>
                                    <th >S.Liner </th>
                                    <th >Lorry No. </th>
                                    <th >Bill No.</th>
                                    <th >Freight Charges</th>
                                    <th >Toll/GreenTax Charges</th>
                                    <th >Detention Charges Charges</th>
                                    <th >Weightment Charges</th>
                                    <th >Other Charges</th>
                                </tr>

                                </tr>
                            </thead>
                            <% int index = 0,sno = 1;%>
                            <c:set var="totalTransportationCharge" value="${0}"/>
                            <c:set var="masterId" value="${csList.masterId}"/>
                            <c:set var="detailId" value="${csList.detailId}"/>
                            <c:forEach items="${GRSummaryList}" var="csList">
                                <c:set var="routeInfo" value="${csList.origin}"/>

                                <tr>
                                    <td align="center"><%=sno%></td>
                                    <td align="left">DICT(Sonepat)</td>

                                    <%
String origin="";
                                        String   routeInformation = "" + (String)pageContext.getAttribute("routeInfo");
                                       String[] routes = null;
                                          routes = routeInformation.split("-");
                                        String   emptyPickup = "" + routes[0] ;
                                        int z= routes.length;
                                  if( z == 2){
                                          origin = "" + routes[0] ;

                                    }else{

                                          origin = "" + routes[1] ;
}
                                       
                                    %>

             
                                <td align="left" id="td1" ><%=emptyPickup%></td>
                                <td align="left"><c:out value="${csList.movementType}"/></td>

                                <td align="left"><c:out value="${csList.grNo}"/></td>
                                <td align="left"><c:out value="${csList.shippingBillNo}"/></td>
                                <td align="center" class="text1"><c:out value="${csList.billOfEntryNo}"/></td>
                                <td align="left"><c:out value="${csList.startDate}"/></td>
                                <td align="left"><c:out value="${csList.endDate}"/></td>
                                <td align="left"><c:out value="${csList.tripSheetId}"/></td>
                                <td align="left"><c:out value="${csList.grDate}"/>   </td>
                                <td align="left"><c:out value=""/></td>
                                <c:if test = "${csList.activeInd == 'Y'}">

                                    <c:if test = "${masterId > detailId}">
                                        <td align="left"><c:out value="${csList.statusName}"/></td>
                                    </c:if>
                                    <c:if test = "${detailId > masterId}">
                                        <td align="left"><c:out value="${csList.detailStatusName}"/></td>
                                    </c:if>
                                    <c:if test = "${detailId == masterId}">
                                        <td align="left"><c:out value="${csList.detailStatusName}"/></td>
                                    </c:if>

                                </c:if>
                                <c:if test = "${csList.activeInd == 'N'}">
                                    <td align="left">Trip Cancelled</td>
                                </c:if>

                                <td align="left"><c:out value=""/></td>
                                <td align="left"><c:out value="${csList.transporter}"/></td>

                                <td align="right"><c:out value="${csList.liters}"/></td>

                                <td align="right"><c:out value="${csList.tripFuelAmount}"/></td>




                                <td align="left"><c:out value="${csList.dieselCost}"/></td>
                                <td align="left"><c:out value="${csList.tollAmount}"/></td>
                                <td align="left"><c:out value="${csList.foodCost}"/></td>
                                <td align="left"><c:out value="${csList.rnmAdvance}"/></td>
                                <td align="left"><c:out value="${csList.weightment}"/></td>
                                <td align="left"><c:out value="${csList.octroiAmount}"/></td>
                                <td align="left"><c:out value="${csList.bhati}"/></td>
                                <td align="left"><c:out value="${csList.tripExpense}"/></td>
                                <td align="left"><c:out value="${csList.miscAmount}"/></td>
                                <td align="left"><c:out value="${csList.extraFoodingAmount}"/></td>
                                <td align="left"><c:out value="${csList.greenTaxAmount}"/></td>
                                <td align="left"><c:out value="${csList.detaintionAmount}"/></td>
                                <td align="left"><c:out value="${csList.otherExpense}"/></td>
                                <td align="left"><c:out value="${csList.driverBatta}"/></td>
                                <td align="left"><c:out value="${csList.dalaAmount}"/></td>
                                <td align="left"> <c:out value="${csList.dieselCost + csList.tollAmount+csList.foodCost+csList.rnmAdvance+csList.weightment+csList.octroiAmount+csList.bhati+csList.tripExpense+csList.miscAmount+csList.extraFoodingAmount+csList.greenTaxAmount+csList.detaintionAmount+csList.otherExpense+csList.driverBatta+csList.dalaAmount}"/> </td>
                                <td align="left"><c:out value="${csList.dieselCost + csList.tollAmount+csList.foodCost+csList.rnmAdvance+csList.weightment+csList.octroiAmount+csList.bhati+csList.tripExpense+csList.miscAmount+csList.extraFoodingAmount+csList.greenTaxAmount+csList.detaintionAmount+csList.otherExpense+csList.driverBatta+csList.dalaAmount +csList.tripFuelAmount}"/></td>
                                <td align="left"><c:out value="${csList.consigneeName}"/></td>
                                <td align="left"><c:out value="${csList.consignorName}"/></td>
                                <td align="left"><c:out value="${csList.billingParty}"/></td>
                                <td align="left">
                                    <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                        <c:out value="${csList.destination}"/>
                                    </c:if>
                                    <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                               <%=origin%>
                                    </c:if>
                                </td>
                                <td align="left">
                                    <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                        <%=origin%>
                                    </c:if>
                                    <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                        <c:out value="${csList.destination}"/>
                                    </c:if>
                                </td>
                                <%--<td align="left"><c:out value="${csList.destination}"/></td>--%>
                                <td align="left"><c:out value="${csList.containerNo}"/></td>
                                <td align="left"><c:out value="${csList.twentyFT}"/></td>
                                <td align="left"><c:out value="${csList.fortyFT}"/></td>
                                <%--<td align="left"><c:out value="${csList.containerType}"/></td>--%>
                                <td align="center">
                                    <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Import'|| csList.movementType == 'Export DSO'|| csList.movementType == 'Import DSO' }" >
                                        L
                                    </c:if>
                                    <c:if test = "${csList.movementType == 'Repo'}">
                                        E
                                    </c:if>
                                </td>
                                <td align="left"><c:out value="${csList.linerName}"/></td>
                                <td align="left"><c:out value="${csList.vehicleNo}"/></td>
                                <td align="left"><c:out value="${csList.invoiceCode}"/></td>


                                <td align="right"><c:out value="${csList.freightAmount}"/></td>
                                <td align="right"><c:out value="${csList.revenueTollAmount}"/></td>
                                <td align="right"><c:out value="${csList.revenueDetentionAmount}"/></td>
                                <td align="right"><c:out value="${csList.revenueWeightMent}"/></td>
                                <td align="right"><c:out value="${csList.revenueOtherExpense}"/></td>

                                <td align="right"><c:out value="${csList.freightAmount + csList.revenueTollAmount + csList.revenueDetentionAmount + csList.revenueWeightMent + csList.revenueOtherExpense}"/></td>
                                </tr>
                                <%
                           index++;
                             sno++;
                                %>

                            </c:forEach>

                        </table>
                    </c:if>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script>
                        function getVehicleNos() {
                            //onkeypress='getList(sno,this.id)'
                            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
