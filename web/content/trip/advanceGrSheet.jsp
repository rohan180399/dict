
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">


    function replaceSpecialCharacters()
    {
        var content = document.getElementById("requestremarks").value;

        //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
        // content=content.replace(/[^a-zA-Z0-9]/g,'');
        content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
        document.getElementById("requestremarks").value = content;
        //alert("Repalced");

    }

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#consignorName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('customerId').value

                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                $('#consignorName').val(value);
//                        $('#consignorPhoneNo').val(ui.item.Mobile);
//                        $('#consignorAddress').val(ui.item.Address);
                $('#customerId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>


<script language="javascript">
    function submitPage(val) { 
        if (isEmpty(document.getElementById("noOfGr").value)) {
            alert('please enter Number of GR');            
            document.getElementById("noOfGr").focus();
            return;
        }
        
         if (parseInt(document.getElementById("noOfGr").value)>10) {
            alert('Entered GR Nos are More than 10');            
            document.getElementById("noOfGr").focus();
            return;
        }
        
        if (isEmpty(document.getElementById("customerId").value)) {
            alert('please enter  Customer Name');
            document.getElementById("customerId").focus();
            return;
        }
        if (val == 'Block') {
            document.approve.action = '/throttle/saveBlockGr.do';
            document.approve.submit();
        }
        if (document.getElementById("grId").value != null && val == 'Update') { 
            document.approve.action = '/throttle/updateBlockGr.do';
            document.approve.submit();
        }
    }


    function setValues(sno, custName, customerId, grNo, grId, status) {


        var count = parseInt(document.getElementById("count").value);
        var noOfGr = grNo.split(',').length;
        
//        for (var i = 1; i <= count; i++) {
//             alert("sno:"+sno);
//            if (i == sno) {
//                document.getElementById("edit" + i).checked = true;
//                document.getElementById("block").style.display = 'none';
//                 document.getElementById("update").style.display = 'block';
//            } 
//            else {
//                alert("inmdn11");
//                document.getElementById("edit" + i).checked = false;
//                document.getElementById("update").style.display = 'block';
//                alert("inmdn2222");
//            }
  //      }
       
        if (status != 0) {
            document.getElementById("noOfGr").value = "";
            document.getElementById("consignorName").value = "";
            document.getElementById("customerId").value = "";
            document.getElementById("grId").value = "";
            document.getElementById("grNo").value = "";
             document.getElementById("edit" + sno).checked = false;
            alert("Gr No's already planned");
        } else {

            document.getElementById("noOfGr").value = noOfGr;
            document.getElementById("consignorName").value = custName;
            document.getElementById("customerId").value = customerId;
            document.getElementById("grId").value = grId;
            document.getElementById("grNo").value = grNo;
                 document.getElementById("block").style.display = 'none';
                 document.getElementById("update").style.display = 'block';
        }


    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Blocked Advance GR</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(10);">
                <form name="approve"  method="post" >
                    <%
                                request.setAttribute("menuPath", "Fuel >> Edit Fuel");
                                String tripid = request.getParameter("tripid");
                                String type = "M";
                    %>
                    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    
<!--                    <script>
                        function setValues(custId,custName,grIds){
                            document.getElementById("customerId").value=custId;
                            document.getElementById("grId").value=grIds;
                            
                        }
                    </script>-->
                     <table class="table table-info mb30 table-hover" style="width:60%">
                        <tr height="30"   ><td colSpan="5" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Advance GR Blocking</td></tr>
                        <tr>
                            <td ><font color="red">*</font>No Of GR</td>
                            <td> <input type="text" id="noOfGr" name="noOfGr" value="" class="form-control" style="width:240px;height:40px;"/></td>
                       
                            <td><font color="red">*</font>Customer Name</td>
                            <td><input type="hidden" name="customerId" id="customerId" class="form-control" style="width:240px;height:40px;" />
                            <td>
                                <input type="text" name="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" class="form-control" style="width:240px;height:40px;"  id="consignorName"/></td>
                            <td>
                                <center><input type="button" style="display:block" id="block" value="Block" onclick="submitPage(this.value)"  class="btn btn-info" /> </center>
                                <center><input type="button" style="display:none" id="update" value="Update" onclick="submitPage(this.value)"  class="btn btn-info" /> </center>

                            </td>
                        </tr>

                    </table>
                    
                    <c:if test="${advanceGRDeatils != null}">


                        <table class="table table-info mb30 table-hover" id="table"  style="width:100%">
                            
                        <thead>
                            <tr>
                            <th  >SNo</th>
                            <th  >Blocked Date</th>
                            <th >Blocked GR No </th>
                            <th >Customer Name </th>
                            <th >Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                <%int sno=0;%>
                                <c:forEach items="${advanceGRDeatils}" var="tripGr">
                                    <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                    %>


                                    <tr>
                                        <td><%=sno%></td>
                                        <td class="<%=className%>">
                                            <c:out value="${tripGr.grDate}"/></td>
                                        <td class="<%=className%>">
                                            <c:out value="${tripGr.grNo}"/>
                                        </td>
                                        <td class="<%=className%>">
                                            <c:out value="${tripGr.customerName}"/>
                                        </td>
                                        <td> 
                <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%=sno%>,'<c:out value="${tripGr.customerName}"/>','<c:out value="${tripGr.customerId}"/>','<c:out value="${tripGr.grNo}"/>','<c:out value="${tripGr.grId}"/>','<c:out value="${tripGr.usedStatus}"/>');" />
                                            <c:if test="${tripGr.usedStatus == '0'}">
                </c:if></td>

                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
               
                        <input type="hidden" id="grNo" name="grNo" value='' />
                        <input type="hidden" id="grId" name="grId" value='' />
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    </c:if>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>