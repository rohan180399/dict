<%--
    Document   : alterLPS
    Created on : 26 Nov, 2012, 02:24:35 PM
    Author     : DINESHKUMAR.S
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Parveen Auto Care</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>


        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>

    <script>

        function submitPage(){
            if(document.alter.partyName.value=='0'){
                return;
            }else if(textValidation(document.alter.lpsNumber,'LPSNumber')){
                return;
            }else if(textValidation(document.alter.lpsDate,'lpsDate')){
                return;
            }else if(textValidation(document.alter.orderNumber,'OrderNumber')){
                return;
            }else if(textValidation(document.alter.contractor,'Contractor')){
                return;
            }else if(textValidation(document.alter.destination,'Destination')){
                return;
            }else if(textValidation(document.alter.packerNumber,'PackerNumber')){
                return;
            }else if(textValidation(document.alter.quantity,'Quantity')){
                return;
            }else if(textValidation(document.alter.bags,'Bags')){
                return;
            }else if(textValidation(document.alter.productName,'ProductName')){
                return;
            }else if(textValidation(document.alter.packing,'Packing')){
                return;
            }

            else if(textValidation(document.alter.lorryNo,'lorryNo')){
                return;
            }
            else if(textValidation(document.alter.gatePassNo,'gatePassNo')){
                return;
            }
            else if(textValidation(document.alter.watchandward,'watchandward')){
                return;
            }
            else if(document.alter.clplPriority.value == 0){
                alert("Plaese Select Priority");
            }


            else{
                document.alter.action='/throttle/saveAlterLPS.do';
                document.alter.submit();
            }
        }
        function setFocus(){
            document.alter.LPSNumber.focus();
        }
        function setFocus(){
            var customerType = document.getElementById("partyId").value;
            if(customerType != null){
                if(customerType.valueOf(",")){
                    var n=customerType.split(",");
                    var techGroups = document.getElementById('partyName');
                    for(var i=0;i<techGroups.options.length;i++){
                        for(var j=0;j<n.length;j++){
                            if(techGroups.options[i].value==n[j]){
                                techGroups.options[i].selected = true;
                            }
                        }
                    }
                }else{
                    document.alter.partyName.value =customerType;
                }
            }

        }

    </script>

    <body onload="setFocus();">
        <form name="alter" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${lpsList != null}">
                <c:forEach items="${lpsList}" var="LPS">

                    <table align="center" width="700" border="0" cellspacing="0" cellpadding="0" class="border">
                        <tr height="30">
                            <Td colspan="7" class="contenthead">Edit LPS</Td>
                        </tr>
                        <tr height="30">
                            <td class="texttitle2">Party Name</td>
                            <td class="texttitle2">
                                <input type="hidden" name="partyId" id="partyId" value="<c:out value="${LPS.partyName}"/>"/>
                                <select class="textbox" name="partyName"  id="partyName" style="width:125px" multiple="multiple">
                                    <c:if test="${customerList != null}">
                                        <option value="0">---Select---</option>
                                        <c:forEach items="${customerList}" var="cus">
                                            <option value='<c:out value="${cus.custId}"/>'><c:out value="${cus.custName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td class="texttitle2" >&emsp;</td>
                            <td class="texttitle2" colspan="3" align="left">Slip No &emsp;&emsp;&emsp;&emsp;
                                <input name="lpsNumber" type="text" class="textbox" value="<c:out value="${LPS.lpsNumber}"/>" maxlength="5" size="20"><br><br>
                                Date &emsp;&emsp;&emsp;&emsp;&emsp;
                                <input name="lpsDate" type="text" class="datepicker"  readonly="readonly" id="tripDate" value="<c:out value="${LPS.lpsDate}"/>"  />&nbsp;<br><br>
                                Lorry No &emsp;&emsp;&emsp;
                                <input name="lorryNo" type="text" class="textbox" value="<c:out value="${LPS.lorryNo}"/>" size="20">
                            </td>




                            <!--                            <td class="texttitle1"><font color="red">*</font>Slip No
                                                        <input name="lpsNumber" type="text" class="textbox" value="<c:out value="${LPS.lpsNumber}"/>" size="20">
                                                        <font color="red">*</font>Date
                                                        <input name="lpsDate" type="text" class="datepicker" value="<c:out value="${LPS.lpsDate}"/>" size="20"><br><br>
                                                        <font color="red">*</font>Lorry No
                                                        <input name="lorryNo" type="text" class="textbox" value="<c:out value="${LPS.lorryNo}"/>" size="20">&nbsp;</td>-->
                        </tr>


<!--                             <td class="text1"><input name="bankName" type="text" class="textbox" value="<c:out value="${LPS.lpsID}"/>" size="20">-->
                        <td> <input type="hidden" name="lpsID" value="<c:out value="${LPS.lpsID}"/>"> </td>


                        <tr height="30">
                            <td class="texttitle1">Order Number</td>
                            <td class="texttitle1"><input name="orderNumber" type="text" class="textbox" value="<c:out value="${LPS.orderNumber}"/>" size="20"></td>
                            <td class="texttitle1">Packer Number</td>
                            <td class="texttitle1"><input name="packerNumber" type="text" class="textbox" value="<c:out value="${LPS.packerNumber}"/>" size="20"></td>
                            <td class="texttitle1">Bill Status</td>
                            <td class="texttitle1" >
                                <select class="textbox" name="billStatus" id="billStatus"  style="width:125px">
                                    <option value="0">---Select---</option>
                                    <c:if test="${LPS.billStatus == 'Paid'}">
                                        <option value='Paid' selected>Paid</option>
                                        <option value='ToPay'>To Pay</option>
                                    </c:if>
                                    <c:if test="${LPS.billStatus == 'ToPay'}">
                                        <option value='Paid'>Paid</option>
                                        <option value='ToPay' selected>To Pay</option>
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr height="30">

                            <td class="texttitle2">Contractor</td>
                            <td class="text2"><input name="contractor" type="text" class="textbox" value="<c:out value="${LPS.contractor}"/>" size="20"></td>
                            <td class="texttitle2">Quantity</td>
                            <td class="text2"><input name="quantity" type="text" class="textbox" value="<c:out value="${LPS.quantity}"/>" size="20"></td>
                            <td class="texttitle2">Bags</td>
                            <td class="text2"><input name="bags" type="text" class="textbox" value="<c:out value="${LPS.bags}"/>" size="20"></td>
                        </tr>

                        <tr height="30">
                            <td class="texttitle1">Product Name</td>
                            <td class="text1"><input name="productName" type="text" class="textbox" value="<c:out value="${LPS.productName}"/>" size="20"></td>
                            <td class="texttitle1">Packing</td>
                            <td class="text1"><input name="packing" type="text" class="textbox" value="<c:out value="${LPS.packing}"/>" size="20"></td>
                            <td class="texttitle1">Gate Pass No</td>
                            <td class="text1"><input name="gatePassNo" type="text" class="textbox" value="<c:out value="${LPS.gatePassNo}"/>" size="20"></td>
                        </tr>

                        <tr height="30">

                            <!-- <td class="texttitle2"><font color="red">*</font>Priority</td>
                            <td class="text2"><input name="priority" type="text" class="textbox" value="<c:out value="${LPS.priority}"/>" size="20"></td>-->


                            <td class="texttitle2">Priority</td>
                            <td class="text2">
                                <select class="textbox" name="clplPriority"  style="width:125px" value="<c:out value="${LPS.clplPriority}"/>"
                                        <option>---Select---</option>
                                    <option value='HIGH'>High</option>
                                    <option value='MEDIUM'>Medium</option>
                                    <option value='LOW'>Low</option>

                                    <td class="texttitle2">Destination</td>
                                    <td class="text2"><input name="destination" type="text" class="textbox" value="<c:out value="${LPS.destination}"/>" size="20"></td>
                                    <td class="texttitle2">Watch and Ward</td>
                                    <td class="text2"><input name="watchandward" type="text" class="textbox" value="<c:out value="${LPS.watchandward}"/>" size="20"></td>
                                </select>
                        </tr>

                    </table>
                </c:forEach>
            </c:if>

            <br>
            <br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
