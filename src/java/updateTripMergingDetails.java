/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
/**
 *
 * @author Arul
 */
public class updateTripMergingDetails {
     public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle_vehicle_change";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateTripId = null;
                PreparedStatement tripMergingDetails = null;
                PreparedStatement vehicleId = null;
                PreparedStatement tripId = null;
                statement = con.createStatement();

// updating records
                String sql = "delete form  ts_trip_merging_details where trip_Id = ? and trip_merging_Id != ? ";
                System.out.println("Updated successfully");
                updateTripId = con.prepareStatement(sql);
                String sql1 = "SELECT trip_merging_detail_Id FROM ts_trip_merging_details group by trip_Id ORDER BY trip_merging_detail_Id ";
                String sql2 = "SELECT trip_merging_detail_Id, trip_merging_Id,trip_Id FROM ts_trip_merging_details  WHERE trip_merging_detail_Id = ? ";
                tripMergingDetails = con.prepareStatement(sql2);
                rs = statement.executeQuery(sql1);
                int i = 0;
                System.out.println("----------------------");
                int updateStatus = 0;
                int vehicle = 0;
                while (rs.next()) {
                    int tripMergingDetailId = rs.getInt(1);
                    tripMergingDetails.setInt(1,tripMergingDetailId);
                    System.out.println("bpclTransactionId = " + tripMergingDetailId);
                    rs1 = tripMergingDetails.executeQuery();
                    while(rs1.next()){
                        System.out.println("mergingDetailId " + rs1.getString(1));
                        System.out.println("mergingId " + rs1.getString(2));
                        System.out.println("tripId" + rs1.getString(3));
                        updateTripId.setString(1,rs1.getString(3));
                        updateTripId.setString(2,rs1.getString(2));
                        updateTripId.executeUpdate();
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
