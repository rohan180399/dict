<%--
    Document   : displayDistrictWise
    Created on : Jun 10, 2013, 12:03:45 AM
    Author     : Entitle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script>
            function submitPage(value){
               if(value == 'Fetch Data'){
               var param = 'No';

               } else{
               var param = 'ExportExcel';
               }
               //alert('param is::;'+param);
                if(textValidation(document.districtwise.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.districtwise.toDate,'To Date')){
                    return;
                }
                //alert(check);
                if(document.getElementById("distSummary").checked == true){
                document.getElementById("distDetail").checked = false;
                document.districtwise.distcheck.value = document.getElementById("distSummary").value;
                document.districtwise.action="/throttle/searchDistrictWiseSummary.do?param="+param;
                document.districtwise.submit();
                }else if(document.getElementById("distDetail").checked == true){
                document.getElementById("distSummary").checked = false;
                document.districtwise.distcheck.value = document.getElementById("distDetail").value;
                document.districtwise.action="/throttle/searchDistrictWiseDetails.do?param="+param;
                document.districtwise.submit();

            }
            }
            function setFocus(){
                var district='<%=request.getAttribute("district")%>';
                var distcheck='<%=request.getAttribute("distcheck")%>';
                var ownership='<%=request.getAttribute("ownerShip")%>';
                var consignmentType='<%=request.getAttribute("consignmentType")%>';
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                if(district!='null'){
                document.districtwise.district.value=district;
                }
                if(consignmentType!='null'){
                document.districtwise.consignmentType.value=consignmentType;
                }
                if(ownership!='null'){
                document.districtwise.ownership.value=ownership;
                }
                if(distcheck!='null'){
                if(distcheck == 'distSummary') {
                    document.getElementById("distSummary").checked = true;
                }
               // alert('distcheck is:::'+distcheck);
                if(distcheck == 'distDetail') {
                    document.getElementById("distDetail").checked = true;
                }
                }
                if(sdate!='null' && edate!='null'){
                    document.districtwise.fromDate.value=sdate;
                    document.districtwise.toDate.value=edate;
                }
            }
        </script>
    </head>
    <body onload="setFocus()" >
        <form name="districtwise" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>District Wise Report</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td>Ownership : </td>
                                    <td><select class="form-control" style="width:123px; " name="ownership">
                                            <option value="0">All</option>
                                            <option value="1">Own</option>
                                            <option value="2">Attach</option>
                                        </select>
                                    </td>
                                    <td>Consignee Type</td>
                                    <td  height="30">
                                        <select class="form-control" style="width:123px;" name="consignmentType">
                                            <option selected  value="">All</option>
                                            <option  value='Paid'>Paid</option>
                                            <option  value='ToPay'>ToPay</option>
                                        </select>

                                    </td>
                                    <td  height="30">District Names:   </td>
                                    <td  height="30">
                                        <select class="form-control" style="width:123px; " name="district">
                                            <option selected  value="0">All</option>
                                            <c:if test = "${districtNameList != null}" >
                                                <c:forEach items="${districtNameList}" var="dist">
                                                    <option  value='<c:out value="${dist.district}" />'>
                                                        <c:out value="${dist.district}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="" />  </td>

                                    <td  height="30"><font color="red">*</font>To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="" /></td>
                                </tr>
                                 <tr>
                                     <td  height="30" colspan="2" align="center" >&nbsp;&nbsp;<input type="radio" name="diststatus" id="distSummary" value="distSummary" checked />Summary&nbsp;&nbsp;
                                         <input type="radio" name="diststatus" id="distDetail" value="distDetail" />Detail&nbsp;&nbsp;<input type="hidden" name="distcheck" value=""> </td>
                                     <td  height="30" colspan="2" align="left" >
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                    </td>

                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <%
            DecimalFormat df = new DecimalFormat("0.00##");
            String districtName = "",tripId = "",vehicleNo = "",invoiceNo = "",tripDate = "",destination = "",consigneeName = "",productName = "";
            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "";
            double totalFreight = 0,subTotalFreight = 0;
            double subTotalTonnage = 0,grandTotalTonnage = 0;
            String className = "text2",existTripDate = "",existDistrictName = "";
            int index = 0;
            int sno = 0;
%>
            <c:if test = "${districtSummaryList != null}" >
            <table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
            <%
            ArrayList districtSummaryList = (ArrayList) request.getAttribute("districtSummaryList");
            Iterator summaryItr = districtSummaryList.iterator();
            ReportTO repTO = null;
            subTotalFreight = 0;
            totalFreight = 0;
            if (districtSummaryList.size() != 0) {
            %>
                 <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Trip Date</td>
                    <td class="contentsub" height="30">District</td>
                    <td class="contentsub" height="30">Destination</td>
                    <td class="contentsub" height="30">Distance</td>
                    <td class="contentsub" height="30">Total Tonnage</td>
                    <td class="contentsub" height="30">CCC Freight </td>
<!--                    <td class="contentsub" height="140">Crossing</td>-->
                </tr>
                <%
                while (summaryItr.hasNext()) {
                    index++;
                    sno++;
                    
                    repTO = new ReportTO();
                    repTO = (ReportTO) summaryItr.next();
                    
                    tripDate  = repTO.getTripDate();
                    if(tripDate == null){
                        tripDate = "";
                        }
                    districtName  = repTO.getDistrict();
                    if(districtName == null){
                        districtName = "";
                        }
                    destination  = repTO.getDestination(); 
                    if(destination == null){
                        destination = "";
                        }
                    distance  = repTO.getDistance();
                    if(distance == null){
                        distance = "";
                        }
                    totalTonnage  = repTO.getTotalTonnage();
                    if(totalTonnage == null){
                        totalTonnage = "";
                        }
                    
                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                    totalFreightAmount  = repTO.getTotalFreightAmount();
                    if(totalFreightAmount == null){
                        totalFreightAmount = "";
                        }
                    
                    totalFreight += Double.parseDouble(totalFreightAmount);


                    if(existDistrictName !=""){
                    if(!existDistrictName.equalsIgnoreCase(districtName)){
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;
                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="4" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>

                        </tr>
                        <%

                        subTotalTonnage = 0;
                        subTotalFreight = 0;

                    }
                    }
                    subTotalTonnage += Double.parseDouble(totalTonnage);
                    subTotalFreight += Double.parseDouble(totalFreightAmount);
                    if(index%2 == 0){
                        className = "text2";

                        }else {
                        className = "text1";
                    }
                    %>
                        <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%></td>
                          <td class="<%=className%>"  height="30"><%=districtName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=distance%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=totalTonnage%></td>
                          <td class="<%=className%>" align="right"  height="30"><%=totalFreightAmount%></td>
                        </tr>

                <%
                 existDistrictName = districtName;
                    }
%>
                         <tr>
                            <td class="<%=className%>"  height="30" colspan="4" >&nbsp;</td>
                            <td class="<%=className%>" align="right"  height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30"><b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        
%>
                         <tr>
                            <td class="text2"  height="30" colspan="4" >&nbsp;</td>
                            <td class="text2" align="right"  height="30"><b>Grand Total</b></td>
                            <td class="text2" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="text2" align="right"  height="30"><b><%=df.format(totalFreight)%></b></td>
                        </tr>
                        <%
                       
               
            }
%>
            </table>
            </c:if>
            <c:if test = "${districtDetailsList != null}" >
                <table  border="0" class="border" align="center" width="900" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">District Name</td>
                    <td class="contentsub" height="30" width="80">Trip Id</td>
                    <td class="contentsub" height="30" width="80" >Trip Date</td>
                    <td class="contentsub" height="30" width="80" >Vehicle No</td>
                    <td class="contentsub" height="30" width="80" >Invoice No</td>
                    <td class="contentsub" height="30">Destination</td>
                    <td class="contentsub" height="30" width="100">Consignee Name</td>
                    <td class="contentsub" height="30" width="80">Material</td>
<!--                    <td class="contentsub" height="30">Departure Time</td>-->
                    <td class="contentsub" height="30" width="80">GP No</td>
                    <td class="contentsub" height="30">Total Tonnage</td>
                    <td class="contentsub" height="30">CCC Freight </td>
<!--                    <td class="contentsub" height="140">Crossing</td>-->
                </tr>
                <%
                    
                    index = 0;
                    ArrayList districtDetailsList = (ArrayList) request.getAttribute("districtDetailsList");
                    Iterator detailsItr = districtDetailsList.iterator();
                    ReportTO reportTO = null;
                    subTotalFreight = 0;
                    totalFreight = 0;
                    if (districtDetailsList.size() != 0) {
                    while (detailsItr.hasNext()) {
                    sno++;
                    reportTO = new ReportTO();
                    reportTO = (ReportTO) detailsItr.next();
                    districtName  = reportTO.getDistrict();
                    if(districtName == null){
                        districtName = "";
                        }
                    tripId  = reportTO.getTripId();
                    if(tripId == null){
                        tripId = "";
                        }
                    tripDate  = reportTO.getTripDate();
                    if(tripDate == null){
                        tripDate = "";
                        }
                    vehicleNo  = reportTO.getVehicleId();
                    if(vehicleNo == null){
                        vehicleNo = "";
                        }
                    invoiceNo  = reportTO.getInvoiceNo();
                    if(invoiceNo == null){
                        invoiceNo = "";
                        }
                    destination  = reportTO.getDestination();
                    if(destination == null){
                        destination = "";
                        }
                    consigneeName  = reportTO.getConsigneeName();
                    if(consigneeName == null){
                        consigneeName = "";
                        }
                    gpno  = reportTO.getGpno();
                    if(gpno == null){
                        gpno = "";
                        }
                    productName  = reportTO.getProductName();
                    if(productName == null){
                        productName = "";
                        }
                    totalTonnage  = reportTO.getTotalTonnage()  ;
                    if(totalTonnage == null){
                        totalTonnage = "";
                        }
                    
                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                    totalFreightAmount  = reportTO.getTotalFreightAmount();
                    if(totalFreightAmount == null){
                        totalFreightAmount = "";
                        }
                    //subTotalFreight += Double.parseDouble(totalFreightAmount);
                    totalFreight += Double.parseDouble(totalFreightAmount);
                
                    if(existDistrictName !=""){
                    if(!existDistrictName.equalsIgnoreCase(districtName)){
                        //System.out.println("className is:::"+className);
                        if(className.equalsIgnoreCase("text1")){
                            className = "text2";
                            }else{
                            className = "text1";
                            }
                            index++;
                            
                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="8" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" colspan="2" align="right" ><b><%=existDistrictName%>&nbsp;Dist Total</b></td>
                            <td class="<%=className%>" align="right"   height="30">&nbsp;<b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"   height="30">&nbsp;<b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;
                        
                    }
                    }
                
                            index++;
                            if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                            }%>
                <tr>
                <td class="<%=className%>"  height="30"><%=sno%></td>
                <td class="<%=className%>"  height="30"><%=districtName%></td>
                <td class="<%=className%>"  height="30"><%=tripId%></td>
                <td class="<%=className%>"  height="30"><%=tripDate%></td>
                <td class="<%=className%>"  height="30"><%=vehicleNo%></td>
                <td class="<%=className%>"  height="30"><%=invoiceNo%></td>
                <td class="<%=className%>"  height="30"><%=destination%></td>
                <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                <td class="<%=className%>"  height="30"><%=productName%></td>
                <td class="<%=className%>"  height="30"><%=gpno%></td>
                <td class="<%=className%>" align="right"  height="30"><%=totalTonnage%></td>
                <td class="<%=className%>" align="right"  height="30"><%=totalFreightAmount%></td>
                </tr>
                <%
                    subTotalTonnage += Double.parseDouble(totalTonnage);
                    subTotalFreight += Double.parseDouble(totalFreightAmount);
                        existDistrictName = districtName;
                        
                       }%>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="8" >&nbsp;</td>
                            <td class="<%=className%>"  height="30" colspan="2" align="right" ><b><%=existDistrictName%>&nbsp;Dist Total</b></td>
                            <td class="<%=className%>" align="right"  height="30">&nbsp;<b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right"  height="30">&nbsp;<b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                     <tr>
                            <td class="text2"  height="30" colspan="8" >&nbsp;</td>
                            <td class="text2"  height="30" colspan="2" align="right" ><b>Grand Total</b></td>
                            <td class="text2" align="right"  height="30"><b>&nbsp;<%=df.format(grandTotalTonnage)%></b></td>
                            <td class="text2" align="right"  height="30"><b>&nbsp;<%=df.format(totalFreight)%></b></td>
                            
                        </tr>
                        <%
                        }
%>
            </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
