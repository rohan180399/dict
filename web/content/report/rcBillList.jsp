

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
        <title>MRSList</title>
    </head>
    
    <script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
        function addRow(val){
            //if(document.getElementById(val).innerHTML == ""){
            document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
            //}else{
            //document.getElementById(val).innerHTML = "";
            //}
        }
        
        function showTable()
        {
            var selectedMrs = document.getElementsByName("selectedIndex");
            var counter=0;
            for(var i=0;i<selectedMrs.length;i++){
                if(selectedMrs[i].checked == 1){
                    counter++;
                    break;
                }       
            }    
            if(counter ==0){
                alert("Please Select MRS");    
            }else{
            document.bill.action = "/throttle/mrsSummary.do";
            document.bill.submit();
        }
    }
    function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    
    function submitPag(val){
        if(validate() == '0'){
            return;
        }else if(val=='purchaseOrder'){
        document.bill.purchaseType.value="1012"
        document.bill.action = '/throttle/generateMpr.do'
        document.bill.submit();
    }else if(val == 'localPurchase'){
    document.bill.purchaseType.value="1011"
    document.bill.action = '/throttle/generateMpr.do'
    document.bill.submit();    
}
}   



function searchSubmit(){
    
    if(document.bill.toDate.value==''){
        alert("Please Enter From Date");
    }else if(document.bill.fromDate.value==''){
    alert("Please Enter to Date");
} 
document.bill.action = '/throttle/handleRcBillList.do'
document.bill.submit();    

}    




function setDate(fDate,tDate,vendorId){
    if(fDate != 'null'){
        document.bill.fromDate.value=fDate;
    }
    if(tDate != 'null'){
        document.bill.toDate.value=tDate;
    }
    if(vendorId != 'null'){
        document.bill.vendorId.value=vendorId;
    }        
}


function detail(indx){
    reqId=document.getElementsByName("billNos");
    
    var url = '/throttle/handleRcBillDetail.do?billNo='+reqId[indx].value;
    window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');    
}

    </script>
    
    <body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>','<%= request.getAttribute("vendorId") %>' )">        
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
             
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">RC Bills</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
   <tr>
       <td>Vendor Name</td>
        <td>
            <select class="form-control" name="vendorId"  style="width:125px;">
                <option value="0">---Select---</option>
                <c:if test = "${vendorList != null}" >
                    <c:forEach items="${vendorList}" var="Dept">
                        <option value='<c:out value="${Dept.vendorId}" />'> <c:out value="${Dept.vendorName}" /></option>
                    </c:forEach >
                </c:if>
            </select>
        </td>
        <td>From Date</td>
        <td class="text2" height="30" width="200">
            <input type="text" class="form-control" name="fromDate" value="" >
            <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.fromDate,'dd-mm-yyyy',this)"/>
        </td>
        <td>To Date</td>
        <td class="text2" height="30" width="200">
            <input type="text" class="form-control" readonly name="toDate" value="" >
            <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/>
        </td>
        <td><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
        </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

            <br>
            
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            %>    
            <c:if test = "${billList != null}" >
                <c:set var="totalAmount" value="0"/>    
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="650" id="bg" class="border">
                    <tr>
                        <td class="contentsub" height="30">Sno</td>
                        <td class="contentsub" height="30">Invoice No</td>
                        <td class="contentsub" height="30">Bill No</td>
                        <td class="contentsub" height="30">WO No</td>
                        <td class="contentsub" height="30">Vendor Name</td>
                        <td class="contentsub" height="30">Bill Amount</td>
                        <td class="contentsub" height="30">Received Date</td>
                    </tr>
                    <c:forEach items="${billList}" var="bill">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"> <%= index + 1 %> </td>
                            <input type="hidden" name="billNos" value='<c:out value="${bill.billId}"/>' >
                            <td class="<%=classText %>" height="30"><c:out value="${bill.remarks}"/></td>                            
                            <td class="<%=classText %>" height="30"><a href="" onClick="detail(<%= index %>);" > <c:out value="${bill.billId}"/> </a> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.woId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.vendorName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.billAmount}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.receivedDate}"/></td>

                            <c:set var="totalAmount" value="${bill.billAmount + totalAmount}" /> 
                        </tr>
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>
                    <tr>
                        <td class="text2">&nbsp;</td>
                        <td class="text2" height="30">&nbsp;</td>
                        <td class="text2">&nbsp;</td>   
                        <td class="text2" height="30"><b>Total Amount</b> </td>        
                        <td class="text2" height="30">
                        <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalAmount}" pattern="##.00"/></b>  </td>     
                        <td class="text2">&nbsp;</td>
                        <td class="text2">&nbsp;</td>
                        <td class="text2">&nbsp;</td>
                        
                    </tr>           
                    
                    
                </table>
                <br>                
                <br>                
            </c:if>
            
            
            <c:if test="${billTaxSummary != null}" >
                <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                    <!--DWLayoutTable-->
                    <tr>
                        <td class="contenthead" height="30" colspan="8" align="center"  >Tax Summary</td>
                    </tr>
                    <tr>
                        <td  class="contentsub" height="30" >Vat (%)</td>
                        <td  class="contentsub" height="30" >Total Amount</td>
                        <td  class="contentsub" height="30" >Hiked Amount</td>
                        <td  class="contentsub" height="30" >Margin</td>
                        <td  class="contentsub" height="30" >Tax Payable</td>
                    </tr> 
                    
                    <c:forEach items="${billTaxSummary}" var="tax" >                
                        
                        <tr>
                            <td  class="contentsub" height="30" > 
                                <c:if test="${tax.tax != 0.00}" >
                                    <c:out value="${tax.tax}" />(%)                     
                                </c:if>
                                <c:if test="${tax.tax == 0.00}" >
                                    Exempted
                                </c:if>                    
                            </td>
                            <td  class="contentsub" height="30" > <c:out value="${tax.totalAmount}" /> </td>
                            <td  class="contentsub" height="30" > <c:out value="${tax.hikedAmount}" /> </td>
                            <td  class="contentsub" height="30" > <c:out value="${tax.margin}" /> </td>
                            <td  class="contentsub" height="30" > <c:out value="${tax.payableTax}" /> </td>
                        </tr>
                        
                    </c:forEach>                
                </table>    
            </c:if>            
            
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        
        
    </body>
</html>
