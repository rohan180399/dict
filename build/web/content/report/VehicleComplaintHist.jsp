<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<%@ page import="java.util.*" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script>

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }

    function submitPage() {
        var chek = Validation();
        if (chek == 1) {
            document.vehicleReport.action = '/throttle/vehicleComplaints.do';
            document.vehicleReport.submit();
        }
    }
    function Validation() {
        if (textValidation(document.vehicleReport.fromDate, 'From Date')) {
            return 0;
        }
        if (textValidation(document.vehicleReport.toDate, 'To Date')) {
            return 0;
        }

        return 1;
    }

    </script>
<script type="text/javascript">

     $(document).ready(function() {
                      $('#regNo').autocomplete({
                    source: function (request, response) {
//                     alert("am here...");
                        $.ajax({
                            url: "/throttle/getVehicleNos.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                     var value = ui.item.Name;
                    $('#regNo').val(value);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            // Use the .autocomplete() method to compile the list based on input from user
            


        });
        </script>
<script type="text/javascript">

//    
//    function getVehicleNos() {
//
//        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
//    }
    function setValues() {
        document.vehicleReport.regNo.focus();
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.vehicleReport.fromDate.value = '<%=request.getAttribute("fromDate")%>';
                document.vehicleReport.toDate.value = '<%=request.getAttribute("toDate")%>';
                document.vehicleReport.regNo.value = '<%=request.getAttribute("regNo")%>';

        }

        if ('<%=request.getAttribute("sectionId")%>' != 'null') {
            document.vehicleReport.sectionId.value = '<%=request.getAttribute("sectionId")%>';
        }
        if ('<%=request.getAttribute("technicianId")%>' != 'null') {
            document.vehicleReport.technicianId.value = '<%=request.getAttribute("technicianId")%>';
        }
        if (document.vehicleReport.sectionId.value != '0') {
            getProblems(document.vehicleReport.sectionId.value);
        }
        if ('<%=request.getAttribute("problemId")%>' != 'null') {
            document.vehicleReport.probId.value = '<%=request.getAttribute("problemId")%>';
        }
    }

    var httpRequest1;
    function getProblems(secId) {
        if (secId != 'null' && secId != 0) {

            var list1 = eval("document.vehicleReport.probId");

            while (list1.childNodes[0]) {
                list1.removeChild(list1.childNodes[0])
            }

            var url = '/throttle/getProblems.do?secId=' + secId;

            if (window.ActiveXObject)
            {
                httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpRequest1 = new XMLHttpRequest();
            }
            httpRequest1.open("POST", url, true);
            httpRequest1.onreadystatechange = function() {
                go();
            };
            httpRequest1.send(null);
        }
    }

    function go() {
        if (httpRequest1.readyState == 4) {
            if (httpRequest1.status == 200) {
                var response = httpRequest1.responseText;
                var list = eval("document.vehicleReport.probId");
                var details = response.split(',');
                var probId = 0;
                var probName = '--<spring:message code="service.label.Select" text="default text"/>--';
                var x = document.createElement('option');
                var name = document.createTextNode(probName);
                x.appendChild(name);
                x.setAttribute('value', probId)
                list.appendChild(x);
                for (i = 1; i < details.length; i++) {
                    temp = details[i].split('-');
                    probId = temp[0];
                    probName = temp[1];
                    x = document.createElement('option');
                    name = document.createTextNode(probName);
                    x.appendChild(name);
                    x.setAttribute('value', probId)
                    list.appendChild(x);
                }
                setTimeout("fun()", 1);
            }
        }
    }


    function fun()
    {
            var mod = '<%=request.getAttribute("probId")%>';
        if (mod != 'null') {
            document.vehicleReport.probId.value = mod;
        }
    }


    function printPage()
    {
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }



</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="service.label.VehicleComplaint"  text="Mechanic Performance"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="service.label.Service" text="default text"/></a></li>
            <li class="active"><spring:message code="service.label.VehicleComplaint" text="default text"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValues();">
                <form name="vehicleReport">

                    <%--<%@ include file="/content/common/path.jsp" %>--%>                 
                    <%@ include file="/content/common/message.jsp" %>

                    <table  class="table table-info mb30 table-hover">
                        <!--<td  colspan="4"  style="border-color:#5BC0DE;padding:16px;">-->

                        <thead>
                        <th colspan="4"><spring:message code="service.label.VehicleComplaint" text="default text"/> </th>
                        </thead>
                        <tr>
                            <td align="left" height="30"><font color="red">*</font><spring:message code="service.label.VehicleNumber" text="default text"/></td>
                            <td height="30"><input name="regNo" id="regNo" type="text" style="width:260px;height:40px;"  class="form-control" size="20"></td>
                            <td align="left" height="30"><spring:message code="service.label.Section" text="default text"/></td>
                            <td><select  name="sectionId" style="width:260px;height:40px;"  class="form-control" onchange="getProblems(this.value);" >
                                    <option value="0">---<spring:message code="service.label.Select" text="default text"/>---</option>
                                    <c:if test = "${SectionList != null}" >
                                        <c:forEach items="${SectionList}" var="sec">
                                            <option value='<c:out value="${sec.sectionId}" />'><c:out value="${sec.sectionName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                        </tr>
                        <tr>
                            <td> <spring:message code="service.label.Problem" text="default text"/> </td>
                            <td>
                                <select  style="width:260px;height:40px;"  class="form-control" id="probId"  name="probId" >
                                    <option  selectd value=''>---<spring:message code="service.label.Select" text="default text"/>---</option>
                                </select>
                            </td>
                            <td align="left" height="30">&nbsp;&nbsp;<spring:message code="service.label.Technician" text="default text"/></td>
                            <td><select style="width:260px;height:40px;"  class="form-control" name="technicianId" >
                                    <option value="0">---<spring:message code="service.label.Select" text="default text"/>---</option>
                                    <c:if test = "${TechnicianList != null}" >
                                        <c:forEach items="${TechnicianList}" var="tec">
                                            <option value='<c:out value="${tec.technicianId}" />'><c:out value="${tec.technician}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr>

                            <td align="left" height="30"><font color="red">*</font><spring:message code="service.label.FromDate" text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" style="width:260px;height:40px;"  class=" datepicker form-control " autocomplete="off" size="20"></td>
                            <td align="left" height="30"><font color="red">*</font><spring:message code="service.label.ToDate" text="default text"/> </td>
                            <td height="30"><input name="toDate" id="toDate" type="text" style="width:260px;height:40px;"  class=" datepicker form-control " autocomplete="off" size="20"></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><input type="button" class="btn btn-success" value="<spring:message code="service.label.SEARCH" text="default text"/>" onclick="submitPage();">    </td>
                        </tr>
                    </table>


                    <c:if test = "${complaintList != null}" >
<!--                        <center> <input type="button" class="btn btn-success" name="print" value="<spring:message code="service.label.Print" text="default text"/>" onClick="printPage();" > </center>-->
                        <div id="printPage" >
                            <table  class="table table-info mb30 table-hover" id="bg">
                                <thead>
<!--                                    <tr>
                                        <th height="30"  colspan="11" align="center" ><b><spring:message code="service.label.VehicleComplaint" text="default text"/> </b></th>
                                    </tr>-->
                                    <tr>
                                        <th  height="30"><spring:message code="service.label.Date" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.JobCard" text="default text"/> </th>
                                        <!--                    <td  height="30">Vehicle <br>Number</td>-->
                                        <th  height="30"><spring:message code="service.label.Section" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.Complaint" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.Activity" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.Technician" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.PCD" text="default text"/></th>                    
                                        <th  height="30"><spring:message code="service.label.ACD" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.Status" text="default text"/></th>
                                        <th  height="30"><spring:message code="service.label.Remarks" text="Remarks"/></th>
                                    </tr>
                                </thead>
                                <% int index = 0;%> 
                                <c:forEach items="${complaintList}" var="complaint"> 
                                    <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                                    %>	

                                    <td  height="30"><c:out value="${complaint.createdDate}" /></td>
                                    <td  height="30"><c:out value="${complaint.jobCardId}" /></td>
            <!--                        <td  height="30"><c:out value="${complaint.regNo}" /></td>-->
                                    <td  height="30"><c:out value="${complaint.section}" /></td>
                                    <td  height="30"><c:out value="${complaint.problem}" /></td>
                                    <td  height="30"><c:out value="${complaint.activity}" /></td>                        
                                    <td  height="30"><c:out value="${complaint.technician}" /></td>
                                    <td  height="30"><c:out value="${complaint.pcd}" /></td>                        
                                    <td  height="30"><c:out value="${complaint.acd}" /></td>
                                    <c:choose>
                                        <c:when test="${complaint.problemStatus=='C' || comp.status=='c'}" >
                                            <td  height="30"><spring:message code="service.label.Completed" text="default text"/></td>                                               
                                        </c:when>
                                        <c:otherwise>
                                            <td  height="30"><spring:message code="service.label.Pending" text="default text"/></td>                                                                                          
                                        </c:otherwise>    
                                    </c:choose>

                                    <td  height="30"><c:out value="${complaint.remarks}" /></td>
                                    </tr>
                                    <% index++;%>
                                </c:forEach>

                            </table>
                            <style>
                                .text1 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#E9FBFF;
                                    padding-left:10px;
                                }
                                .text2 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#FFFFFF;
                                    padding-left:10px;
                                }        
                                .contentsub {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:left;
                                    padding-bottom:8px;
                                }

                                .contenthead {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:center;
                                    padding-bottom:8px;
                                }

                            </style>              
                        </div>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


