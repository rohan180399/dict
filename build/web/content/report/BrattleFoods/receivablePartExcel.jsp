<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripPlanned-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

                 
                      <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead height="30">
                       
                        <tr >
                            <th >Particulars</th>
                            <th >Status</th>
                            <th >Gr status</th>
                            <th >Opening GR of the Day</th>
                            <th >GR Available for billing during the day</th>
                            <th >Invoice raised</th>
                            <th >Net GR Pending for Billing </th>
                            <th >Amount of invoices raised</th>
                            <th >Pending Revenue</th>
                            <th >Pending from < 7 Days</th>
                            
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                   
                        <c:set var="totOpenGR" value="${0}"/>    
                        <c:set var="totGRissued" value="${0}"/>    
                        <c:set var="totGRBilled" value="${0}"/>    
                        <c:set var="totNetGRPending" value="${0}"/>    
                        <c:set var="totInvAmtBillToParty" value="${0}"/>    
                        <c:set var="totInvAmtBillNotToParty" value="${0}"/>    
                        <c:set var="totPendingRevenueBillToParty" value="${0}"/>    
                        <c:set var="totPendingRevenueBillNotToParty" value="${0}"/>    
                        <c:set var="totpendingGRless7" value="${0}"/>    
                        

                            <tr  height="30">
                            <td align="center" class="text1" rowspan="10">GR Closed but not Billed</td>
                            <td align="center" class="text1" rowspan="4">Bill to Party</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grBilled']}"/></td>
                             <c:set var="netGrPending1" value="${tripMap['own-load-billing-openGR'] + tripMap['own-load-billing-issueGR'] - tripMap['own-load-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending1}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-load-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            
                            </tr>
                            <tr  height="30">
                            
                            <td align="center" class="text1">Loaded-DSO</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grBilled']}"/></td>
                             <c:set var="netGrPending2" value="${tripMap['own-dso-billing-openGR'] + tripMap['own-dso-billing-issueGR'] - tripMap['own-dso-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending2}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-dso-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            
                            </tr>
                            </tr>
                            <tr  height="30">
                            
                            <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grBilled']}"/></td>
                             <c:set var="netGrPending3" value="${tripMap['own-empty-billing-openGR'] + tripMap['own-empty-billing-issueGR'] - tripMap['own-empty-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending3}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-empty-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                            
                            </tr>
                           
                            <tr  height="30">
                            
                            <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grBilled']}"/></td>
                             <c:set var="netGrPending4" value="${tripMap['own-btt-billing-openGR'] + tripMap['own-btt-billing-issueGR'] - tripMap['own-btt-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending4}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-btt-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                            
                            </tr>
                           
                           
                            
                            <tr>
                        <c:set var="totOpenGR" value="${tripMap['own-load-billing-openGR'] + tripMap['own-dso-billing-openGR'] + tripMap['own-empty-billing-openGR'] + tripMap['own-btt-billing-openGR']}"/>   
                        <c:set var="totGRissued" value="${tripMap['own-btt-load-issueGR'] + tripMap['own-dso-billing-issueGR'] + tripMap['own-empty-billing-issueGR'] + tripMap['own-btt-billing-issueGR']}"/>   
                        <c:set var="totGRBilled" value="${tripMap['own-load-grBilled'] + tripMap['own-dso-grBilled'] + tripMap['own-empty-grBilled'] + tripMap['own-btt-grBilled']}"/>       
                        <c:set var="totNetGRPending" value="${netGrPending1 + netGrPending2 + netGrPending3+ netGrPending4}"/>       
                        <c:set var="totInvAmtBillToParty" value="${tripMap['own-load-totInvAmt'] + tripMap['own-dso-totInvAmt'] + tripMap['own-empty-totInvAmt'] + tripMap['own-btt-totInvAmt']}"/>       
                        <c:set var="totPendingRevenueBillToParty" value="${tripMap['own-load-pendingInvToParty'] + tripMap['own-dso-pendingInvToParty'] + tripMap['own-empty-pendingInvToParty'] + tripMap['own-btt-pendingInvToParty']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                        
                                 
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRBilled}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totInvAmtBillToParty}" /></td>
                            <td align="center" class="text1"><c:out value="${totPendingRevenueBillToParty}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Not to be billed to party</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grBilled']}"/></td>
                             <c:set var="netGrPending5" value="${tripMap['own-load-billing-openGR'] + tripMap['own-load-billing-issueGR'] - tripMap['own-load-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending5}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-load-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            
                            </tr>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                          <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grBilled']}"/></td>
                             <c:set var="netGrPending6" value="${tripMap['own-dso-billing-openGR'] + tripMap['own-dso-billing-issueGR'] - tripMap['own-dso-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending6}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-dso-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grBilled']}"/></td>
                             <c:set var="netGrPending7" value="${tripMap['own-empty-billing-openGR'] + tripMap['own-empty-billing-issueGR'] - tripMap['own-empty-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-empty-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                           
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                           <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grBilled']}"/></td>
                             <c:set var="netGrPending7" value="${tripMap['own-btt-billing-openGR'] + tripMap['own-btt-billing-issueGR'] - tripMap['own-btt-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-btt-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                           
                           
                            </tr>
                          <c:set var="totOpenGR2" value="${tripMap['own-load-billing-openGR'] + tripMap['own-dso-billing-openGR'] + tripMap['own-empty-billing-openGR'] + tripMap['own-btt-billing-openGR']}"/>   
                         <c:set var="totGRissued2" value="${tripMap['own-btt-load-issueGR'] + tripMap['own-dso-billing-issueGR'] + tripMap['own-empty-billing-issueGR'] + tripMap['own-btt-billing-issueGR']}"/>   
                        <c:set var="totGRBilled2" value="${tripMap['own-load-grBilled'] + tripMap['own-dso-grBilled'] + tripMap['own-empty-grBilled'] + tripMap['own-btt-grBilled']}"/>       
                        <c:set var="totNetGRPending2" value="${netGrPending5 + netGrPending6 + netGrPending7+ netGrPending8}"/>       
                       <c:set var="totInvAmtBillNotToParty" value="${tripMap['own-load-totInvAmtNotToParty'] + tripMap['own-dso-totInvAmtNotToParty'] + tripMap['own-empty-totInvAmtNotToParty'] + tripMap['own-btt-totInvAmtNotToParty']}"/>       
                        <c:set var="totPendingRevenueBillNotToParty" value="${tripMap['own-load-pendingInvNotToParty'] + tripMap['own-dso-pendingInvNotToParty'] + tripMap['own-empty-pendingInvNotToParty'] + tripMap['own-btt-pendingInvNotToParty']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRBilled2}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending2}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totInvAmtBillNotToParty}" /></td>
                            <td align="center" class="text1"><c:out value="${totPendingRevenueBillNotToParty}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                           
                            </tr>
                            
                            <c:set var="grandTotOpenGr" value="${totOpenGR }"/>       
                            <c:set var="grandTotGRissued" value="${totGRissued }"/>       
                            <c:set var="grandTotGRBilled" value="${totGRBilled }"/>       
                            <c:set var="grandtotNetGRPending" value="${totNetGRPending }"/>       
                            <c:set var="grandtotInvAmtBill" value="${totInvAmtBillNotToParty + totInvAmtBillToParty }"/>       
                            <c:set var="grandtotPendingRevenue" value="${totPendingRevenueBillNotToParty + totPendingRevenueBillToParty }"/>       
                            <c:set var="grandtottotpendingGRless7" value="${totpendingGRless7 }"/>       
                            
                             <tr  height="30">
                            <td align="center" class="text1" >Grand Total</td>
                            <td align="center" class="text1" ></td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${grandTotOpenGr}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRBilled}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotNetGRPending}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${grandtotInvAmtBill}" /></td>
                            <td align="center" class="text1"><c:out value="${grandtotPendingRevenue}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotpendingGRless7}"/></td>
                          
                            </tr>
                       
                        <%
                            index++;
                            sno++;
                        %>
                   
                </table>
               

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>