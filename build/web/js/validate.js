//Validation JavaScript Document

function isEmpty (s) {
    var whitespace = " \t\n\r";
	var i =0;
	if ((s == null) || (s.length == 0)) {
		return true;
	}
	
	for (i =0; i<s.length; i++) {
		var c = s.charAt(i);
			if(whitespace.indexOf(c) == -1) {
				return false;
			}
	}
	return true;
}


function setImages(add, edit, search, imp, exp,print){
    var x=document.getElementById('imgTable').rows
    var y=x[0].cells
    var z=x[1].cells
   
    if(add == 1) {
    
    	y[1].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/Add.png' align='middle' border='0' alt='Add' onClick=submitPage('add')></a>"
    	z[1].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=submitPage('add')>Add</a>"
    }else {
    	y[1].innerHTML="<img src='/throttle/images/Add1.png' align='middle' border='0' alt='Add'>"
    	z[1].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Add </font>";
    }
    

    if(edit == 1) {    
    	y[2].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/edit.png' align='middle' border='0' alt='Edit' onClick=submitPage('modify')></a>"
    	z[2].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=submitPage('modify')>Edit</a>"
    }else {
    	y[2].innerHTML="<img src='/throttle/images/edit1.png' align='middle' border='0' alt='Edit'>"
    	z[2].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Edit </font>";
    }


    if(search == 1) {
    
    	y[3].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/Search.png' align='middle' border='0' alt='Search' onClick=moveMe('searchAcc');searchshowhide('searchAcc')></a>"
    	z[3].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=moveMe('searchAcc');searchshowhide('searchAcc')>Search</a>"
    	
    }else {
    	y[3].innerHTML="<img src='/throttle/images/Search1.png' align='middle' border='0' alt='Search'>"
    	z[3].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Search </font>";
    }

    if(imp == 1) {
    
    	y[4].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/Import.png' align='middle' border='0' alt='Import' onClick=submitPage('import')></a>"
    	z[4].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=submitPage('import')>Import</a>"
    }else {
    	y[4].innerHTML="<img src='/throttle/images/Import1.png' align='middle' border='0' alt='Import'>"
    	z[4].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Import </font>";
    }
    
    
    if(exp == 1) {
    
    	y[5].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/Export.png' align='middle' border='0' alt='Export' onClick=submitPage('export')></a>"
    	z[5].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=submitPage('export')>Export</a>"
    }else {
    	y[5].innerHTML="<img src='/throttle/images/Export1.png' align='middle' border='0' alt='Export'>"
    	z[5].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Export </font>";
    }
    
     if(print == 1) {
    
    	y[6].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; '><img src='/throttle/images/Print.png' align='middle' border='0' alt='Print' onClick=window.print()></a>"
    	z[6].innerHTML="<a  style='font:Arial; font-size:12px; color:#333333; ' onClick=window.print()>Print</a>"
    }else {
    	y[6].innerHTML="<img src='/throttle/images/Print1.png' align='middle' border='0' alt='Print'>"
    	z[6].innerHTML="<font style='font:Arial; font-size:12px; color:#999999;'>Print </font>";
    }
    
    
}


// Checks Special Character like '," which causes db Errors

function isSpecialCharacter(val)
{
	for(var i=0;i<val.length;i++){
		if( val.charCodeAt(i)==39 || val.charCodeAt(i)==34 ){
		return true;
		break;
		}
	}
	return false;         
}
 function onKeyPressBlockCharacters(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z]+$/;

        return !reg.test(keychar);

    }
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }
function textValidation(parameter,name){
if(isEmpty(parameter.value)){
alert('please Enter Data for '+name); 
parameter.focus();
return true;
}
if(isSpecialCharacter(parameter.value)){
alert('Special Characters are Not allowed for '+name); 
parameter.focus();
parameter.select();
return true;
}
parameter.value = trim(parameter.value);
return false;
}


function numberValidation(parameter,name){
if(isDigit(parameter.value)){
alert('please Enter Valid Data for '+name); 
parameter.focus();
parameter.select();
return true;
}
return false;
}

function dateFormat(parameter){
    var temp = parameter.value.split('-');
    var temp1;
    if(temp[2].length == 4){
    temp1 = temp[2]+'-'+temp[1]+'-'+temp[0];    
    }else{
    temp1 = temp[0]+'-'+temp[1]+'-'+temp[2];    
    }    
    return temp1;
}


function compareDates(fromDate,toDate)
{
var fDate = new Date(fromDate);
var tDate = new Date(toDate);


if ( Date.parse(fDate) > Date.parse(tDate) ) {    
return true;
}

if (  Date.parse(fDate) <= Date.parse(tDate)  ) {    
return false;
}


}



// This function is used to check the value for digits

function isDigit(s){
	if(!(/^-?\d+$/.test(s))){
		return true;
	}
return false;
}


//This function is used to check of the entered value contains any character other than
// 'A-Z','a-z',' ','*' ,'0-9'
	function isName(fieldValue)
	{
		

		var len=fieldValue.length;
		for(i=0;i<len;i++)
		{
			var str=fieldValue.charAt(i);
			var code=str.charCodeAt(0);
			if(!((64<code && code<91)|| (96<code && code<123) || (code==32) || (code==42)|| (47<code && code <58)))
			{
				return false;

			}

		}
		return true;
	}
	

//This function is used to check of the entered value contains any character other than
// ' ','*' ,'0-9'
	function isNonNumeric(fieldValue)
	{
		

		var len=fieldValue.length;
		for(i=0;i<len;i++)
		{
			var str=fieldValue.charAt(i);
			var code=str.charCodeAt(0);
			if(!((code==32) || (code==42)|| (47<code && code <58)))
			{
				return false;

			}

		}
		return true;
	}
	
	
// This function is used to trim the value
	
	
	function trim(TRIM_VALUE){
		if(TRIM_VALUE.length < 1){
			return"";
			}
				TRIM_VALUE = RTrim(TRIM_VALUE);
				TRIM_VALUE = LTrim(TRIM_VALUE);
			if(TRIM_VALUE==""){
			return "";
			}
			else{
				return TRIM_VALUE;
		}
	} //End Function

	function RTrim(VALUE){
		var w_space = String.fromCharCode(32);
		var v_length = VALUE.length;
		var strTemp = "";
		if(v_length < 0){
		return"";
		}
	
		var iTemp = v_length -1;
	
		while(iTemp > -1){
			if(VALUE.charAt(iTemp) == w_space){
			}
			else{
			strTemp = VALUE.substring(0,iTemp +1);
			break;
			}
			iTemp = iTemp-1;
	
		} //End While
	return strTemp;

	} //End Function

	function LTrim(VALUE){
		var w_space = String.fromCharCode(32);		
		if(v_length < 1){
			return"";
		}
		var v_length = VALUE.length;
		var strTemp = "";
		
		var iTemp = 0;
		
		while(iTemp < v_length){
			if(VALUE.charAt(iTemp) == w_space){
			}
			else{
			strTemp = VALUE.substring(iTemp,v_length);
			break;
			}
			iTemp = iTemp + 1;
		} //End While
		return strTemp;
	} //End Function
	
	
	
// This function is used to check for valid email address

function checkEmail(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        alert('Please provide a valid email address');
        return true;
    }else{
        return false;
    }
 }


function isEmail (strng) {
var error="";

var emailFilter=/^.+@.+\..{2,3}$/
if (!(emailFilter.test(strng))) 
{
   error = "Please enter a valid email address.";
   //alert(error);
   return error;
  
}
else 
{
//test email for illegal characters
   var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/
   if (strng.match(illegalChars)) {
         error = "The email address contains illegal characters.";
//		 alert(error);
   return error;
		
		 }
}
   return error;    
}  

/*
start and end date validation
*/

function fnDateStartEndValidate(startDate, endDate)
{
        
	if (startDate.length < 11)
	{
		startDate = "0"+startDate;
	}
	
	if (endDate.length < 11)
	{
		endDate = "0"+endDate;
	}
	
	var startYear = startDate.substring(7,11);
	var endYear = endDate.substring(7,11);
         
	if (startYear > endYear)
	{
		return false;
	}

	if (startYear == endYear)
	{
		var mon = new Array();
		mon[0]='JAN';
		mon[1]='FEB';
		mon[2]='MAR';
		mon[3]='APR';
		mon[4]='MAY';
		mon[5]='JUN';
		mon[6]='JUL';
		mon[7]='AUG';
		mon[8]='SEP';
		mon[9]='OCT';
		mon[10]='NOV';
		mon[11]='DEC';
		var startMonth = startDate.substring(3,6).toUpperCase();
                 
		var endMonth = endDate.substring(3,6).toUpperCase();
                
		var endMonthIndex;
		for (var i=0;i<12;i++)
		{
			if (endMonth == mon[i])
			{
				endMonthIndex = i;
				break;
			}
		}
                
		for (var i=endMonthIndex+1;i<12;i++)
		{
			if (startMonth == mon[i])
			{
				return false;
			}
		}
	
		var startDay = startDate.substring(0,2);
		var endDay = endDate.substring(0,2);
                
		if (startDay > endDay)
		{
			return false;
		}
	}
	return true;              
}

	//Called when the check box is selected.
	function selectedRow(checkBoxObj, index)
	{
		var obj = document.getElementById('TR'+index);
		if( obj != null )
		{	
			if( checkBoxObj.checked )
			{
				obj.bgColor = '#EEEEBB';
			}
			else
			{
				if( index%2 > 0 )
				{
					obj.bgColor = '#d5d5d5';				
				}
				else
				{
	
					obj.bgColor = '#f5f5f5';
				}
			}
		}
	}

//vijay

function floatValidation(parameter,name){    
if(isFloat(parameter.value)){
alert('please Enter Valid Data for '+name); 
parameter.focus();
return true;
}
return false;
}


//is select
function isSelect(parameter,name){
    if(parameter.value=='0'){
        alert('please select '+name); 
        parameter.focus();
        return true;
    }
return false;    
}


         // is float
function isFloat(s){
if(!(/^((\d+(\.\d*)?)|((\d*\.)?\d+))$/.test(s))){				
return true;
}
return false;
}                

//to set max length for text area
   function maxlength(field, maxlen) {       
if (field.value.length > maxlen){
field.value = field.value.substring(0, maxlen);

}
} 



document.attachEvent("onkeydown", my_onkeydown_handler); 



//  Ctrl + R , Ctrl + N  will Not work

document.attachEvent("onkeydown", my_onkeydown_handler); 
function my_onkeydown_handler() 
{
switch (event.keyCode) 
{ 

case 116 : // 'F5' 
alert("Page Cannot be refreshed");
event.returnValue = false; 
event.keyCode = 0; 
window.status = "disabled F5"; 

break; 
case 122 : // 'F11' 
alert("Page Cannot be refreshed");
event.returnValue = false; 
event.keyCode = 0; 
window.status = "We have disabled F11"; 
break; 

} 

if ((event.ctrlKey && (event.keyCode == 78 || event.keyCode == 82)))
{
 alert("Page Cannot be refreshed");
return false; }
 
} 

//Right Click Disabled below




//var isNS = (navigator.appName == "Netscape") ? 1 : 0;
//if(navigator.appName == "Netscape")
//document.captureEvents(Event.MOUSEDOWN||Event.MOUSEUP);
//function mischandler()
//{
//
//return false;
//}
//function mousehandler(e)
//{
//
//var myevent = (isNS) ? e : event;
//var eventbutton = (isNS) ? myevent.which : myevent.button;
//if((eventbutton==2)||(eventbutton==3))
// alert('Right click disabled');
//return false;
// }
//
// document.oncontextmenu = mischandler;
// document.onmousedown = mousehandler;
// document.onmouseup = mousehandler;  
 










