<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<html>
<head>
        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a  {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:140%;
}
	.column { width: 250px; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>



<%
    Connection conn = null;
    int count = 0;
    try{

        String fileName = "jdbc_url.properties";
        Properties dbProps = new Properties();
    //The forward slash "/" in front of in_filename will ensure that
    //no package names are prepended to the filename when the Classloader
    //search for the file in the classpath

    InputStream is = getClass().getResourceAsStream("/"+fileName);
    dbProps.load(is);//this may throw IOException
    String dbClassName = dbProps.getProperty("jdbc.driverClassName");

    String dbUrl = dbProps.getProperty("jdbc.url");
    String dbUserName = dbProps.getProperty("jdbc.username");
    String dbPassword = dbProps.getProperty("jdbc.password");
   

    String itemCode = request.getParameter("itemCode");

    DecimalFormat df2 = new DecimalFormat("0.00");
    Class.forName(dbClassName).newInstance();
    conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

    String fleetTypeQry = " SELECT vehicle_type_name,count(*) as tot FROM papl_model_master mm, "
                         + "papl_vehicle_type_master tm, papl_vehicle_master vm WHERE mm.vehicle_type_id = "
                            + "tm.vehicle_type_id AND  vm.model_id = mm.model_id AND tm.active_ind = 'Y' "
                           + "AND mm.active_ind = 'Y' AND vm.active_ind = 'Y' GROUP BY vehicle_type_name ";
    String fleetDistribution = "SELECT mfr_name,count(*) as cnt FROM papl_vehicle_master vm, papl_mfr_master mf"
                                 + " WHERE mf.mfr_id = vm.mfr_id  AND mf.active_ind = 'Y' AND vm.active_ind = 'Y' GROUP BY mfr_name ORDER BY mfr_name";

    String serviceCostMTD = "SELECT sum(total_amount) FROM papl_jobcard_master jm, papl_jobcard_bill_master jb"
                            + " WHERE jm.job_card_id = jb.job_card_id AND "
                            + "date_format(closed_on,'%m-%Y') = date_format(now(),'%m-%Y') AND jm.active_ind = 'Y' ";

    String serviceCostYTD = " SELECT sum(total_amount) FROM papl_jobcard_master jm, papl_jobcard_bill_master jb"
                             + " WHERE jm.job_card_id = jb.job_card_id AND "
                             + "date_format(closed_on,'%Y') = date_format(now(),'%Y') AND jm.active_ind = 'Y'";
  
   String serviceCostMfrYTD = "SELECT mfr_name,sum(nett_amount) as amt FROM papl_direct_jobcard dj, papl_jobcard_bill_master jb, papl_vehicle_master vm, "
                             + " papl_mfr_master mf, papl_jobcard_master jm WHERE dj.job_card_id = jb.job_card_id AND vm.vehicle_id = dj.vehicle_id "
                             + "AND mf.mfr_id = vm.mfr_id AND jm.job_card_id = dj.job_card_id AND "
                             + "date_format(jm.closed_on,'%Y') =  date_format(now(),'%Y') AND mf.active_ind = 'Y' AND vm.active_ind = 'Y' "
                             + "AND dj.active_ind = 'Y' AND jm.active_ind = 'Y' GROUP BY mfr_name";
  
   String serviceCostMfrMTD = "SELECT mfr_name,sum(nett_amount) as amt FROM papl_direct_jobcard dj, papl_jobcard_bill_master jb, papl_vehicle_master vm, "
                             + " papl_mfr_master mf, papl_jobcard_master jm WHERE mfr_name = ? AND dj.job_card_id = jb.job_card_id AND vm.vehicle_id = dj.vehicle_id "
                             + "AND mf.mfr_id = vm.mfr_id AND jm.job_card_id = dj.job_card_id AND "
                             + "date_format(jm.closed_on,'%m-%Y') =  date_format(now(),'%m-%Y') AND mf.active_ind = 'Y' AND vm.active_ind = 'Y' "
                             + "AND dj.active_ind = 'Y' AND jm.active_ind = 'Y' GROUP BY mfr_name";


    String fleetType = "", total = "0";
    double serviceCostYtd = 0, serviceCostMtd = 0;
    PreparedStatement pstmFleetType = conn.prepareStatement(fleetTypeQry);
    PreparedStatement pstmFleetDist = conn.prepareStatement(fleetDistribution);
    PreparedStatement pstmScYtd = conn.prepareStatement(serviceCostMTD);
    PreparedStatement pstmScMtd = conn.prepareStatement(serviceCostYTD);    
    PreparedStatement pstmMfrScYtd = conn.prepareStatement(serviceCostMfrYTD);    
    PreparedStatement pstmMfrScMtd = conn.prepareStatement(serviceCostMfrMTD);    
    String fleetTypeXml = "";    
    ResultSet res = pstmFleetType.executeQuery();
    while(res.next()) {        
         fleetType = res.getString(1);
         total = res.getString(2);
         fleetTypeXml = fleetTypeXml+" <set value='"+total+"' label='"+fleetType+"' alpha='60'/>";
    }
    res = pstmScYtd.executeQuery();
    while(res.next()) {
        serviceCostYtd = res.getDouble(1);
    }
     res = pstmScMtd.executeQuery();
     while(res.next()) {
        serviceCostMtd = res.getDouble(1);
     }     
     ArrayList mrfList = new ArrayList();
     ArrayList ytdList = new ArrayList();
     ArrayList mtdList = new ArrayList();
     String category = "";
     String dataSet1 = "";
     String dataSet2 = "";    
     // mfr wise YTD
   //  out.print(pstmMfrScYtd);
     res = pstmMfrScYtd.executeQuery();
     while(res.next()) {       
       mrfList.add(res.getString("mfr_name"));
       ytdList.add(res.getString("amt"));
       category = category + "<category label='"+res.getString("mfr_name")+"' />";
       dataSet1 = dataSet1 + " <set value='"+res.getString("amt")+"' /> ";
     }
     category = "<categories>"+category+"</categories>";
     dataSet1 = "<dataset seriesName='YTD' color='3EA99F' showValues='0'>"+dataSet1+"</dataset>";
     
     //mfr wise month to date
     String mfrName = "";
     double cost = 0;
       if(mrfList != null) {
        for(int i = 0; i < mrfList.size(); i++)     {
            mfrName = "";
            cost = 0;
            pstmMfrScMtd.setString(1, mrfList.get(i).toString());
            
            res = pstmMfrScMtd.executeQuery();
            while(res.next()) {       
                mfrName = res.getString("mfr_name");
                cost = res.getDouble("amt");                
            }
            if(mfrName != null && mfrName.length() > 0){
                 dataSet2 = dataSet2 + " <set value='"+cost+"' /> ";
            } else{
                dataSet2 = dataSet2 + " <set value='0' /> ";
            }
         }    
    }
     dataSet2 = "<dataset seriesName='MTD' color='F87431' showValues='0'>"+dataSet2+"</dataset>";  
     String fleetDistCategory = "", fleetData1 = "";
     res = pstmFleetDist.executeQuery();
     while(res.next()) {        
       fleetDistCategory = fleetDistCategory+" <set label='"+res.getString("mfr_name")+"' value='"+res.getString("cnt")+"' />  ";// color='3EA99F'
     } 
%>



<%


// used dash board codes

fleetTypeXml =  "<chart caption='Fleet Types' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' "
        + "isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+fleetTypeXml+"</chart>";



String fleetDistributionXml = "<chart palette='2' caption='Fleet Distribution' showLabels='1' showvalues='0'  numberPrefix='' showSum='1' decimals='0' useRoundEdges='1' legendBorderAlpha='0'> "+
" <categories><category label='Ashok Leyland' /><category label='Eicher' /><category label='Volvo' /><category label='Benz' /></categories> "+
" <dataset seriesName='1Yr' color='F535AA' showValues='0'><set value='2' /><set value='86' /><set value='94' /><set value='1' /></dataset> "+
" <dataset seriesName='2Yr' color='357EC7' showValues='0'><set value='3' /><set value='12' /><set value='115' /><set value='2' /></dataset> "+
" <dataset seriesName='3Yr' color='F87431' showValues='0'><set value='3' /><set value='22' /><set value='25' /><set value='1' /></dataset> "+
" <dataset seriesName='>3Yr' color='99CC00' showValues='0'><set value='3' /><set value='20' /><set value='54' /><set value='0' /></dataset> "+
" </chart> ";


String serviceCost = "<chart yAxisName='Values' caption='Service Cost / Fleet' numberPrefix='SAR ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' showBorder='0'> "+
	" <set label='YTD' value='"+serviceCostYtd+"' color='3EA99F' />  "+
        "<set label='MTD' value='"+serviceCostYtd+"' color='F87431' /> "+
        "</chart> ";
String fleetDistCategoryXml = "<chart yAxisName='Values' caption='Fleet Distribution' numberPrefix=' ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' "
        + "showBorder='0'> "+fleetDistCategory+" </chart> ";
        
   /*     
        
String fleetServiceCost = "<chart palette='2' caption='Service Cost / Fleet' showLabels='1' showvalues='1' decimals='0' numberPrefix='SAR.'> " +
" <categories><category label='Ashok Leyland' /><category label='Eicher' /><category label='Volvo' /><category label='Benz' /></categories>  "+
" <dataset seriesName='YTD' color='3EA99F' showValues='0'> "+
" <set value='2601.34' /> "+
" <set value='2148.82' /> "+
" <set value='1372.76' /> "+
" <set value='3407.15' /> "+
" </dataset> "+
" <dataset seriesName='MTD' color='F87431' showValues='0'> "+
" <set value='2000.65' /> "+
" <set value='1835.76' /> "+
" <set value='1222.18' /> "+
" <set value='2557.31' /> "+
" </dataset> "+
" </chart> ";       
*/



String fleetServiceCost = "<chart palette='2' caption='Service Cost / Fleet' showLabels='1' showvalues='1' decimals='0' numberPrefix='SAR.'>"
        + " " +category+""+dataSet1+""+dataSet2+" </chart> ";        
%>

<div id="expand">
<div class="column" style="width:430px;">
	
	
        
        <div class="portlet" >
            <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Fleet Types</div>
                <div class="portlet-content">
                <table align="center"   cellspacing="0px" >
                <tr>
                    <td>
                        <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                            <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                            <jsp:param name="strURL" value="" />
                            <jsp:param name="strXML" value="<%=fleetTypeXml %>" />
                            <jsp:param name="chartId" value="productSales" />
                            <jsp:param name="chartWidth" value="400" />
                            <jsp:param name="chartHeight" value="250" />
                            <jsp:param name="debugMode" value="false" />
                            <jsp:param name="registerWithJS" value="false" />
                        </jsp:include>
                    </td>
                </tr>
                </table>
                </div>
	</div>
    
<div class="portlet" >
                <div class="portlet-header" style="width:405px;">&nbsp;&nbsp;Service Cost Overall </div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=serviceCost %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="400" />
                                <jsp:param name="chartHeight" value="450" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>
</div>
<div class="column" style="width:630px;">
	
    <div class="portlet" >
                <div class="portlet-header" style="width:605px;">&nbsp;&nbsp;Fleet Distribution</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=fleetDistCategoryXml %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="600" />
                                <jsp:param name="chartHeight" value="250" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
	</div>
    <div class="portlet" >
                    <div class="portlet-header" style="width:605px;">&nbsp;&nbsp;Service Cost Makewise </div>
                        <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                        <tr>
                            <td>
                                <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                    <jsp:param name="chartSWF" value="/throttle/swf/MSBar3D.swf" />
                                    <jsp:param name="strURL" value="" />
                                    <jsp:param name="strXML" value="<%=fleetServiceCost %>" />
                                    <jsp:param name="chartId" value="productSales" />
                                    <jsp:param name="chartWidth" value="600" />
                                    <jsp:param name="chartHeight" value="450" />
                                    <jsp:param name="debugMode" value="false" />
                                    <jsp:param name="registerWithJS" value="false" />
                                </jsp:include>
                            </td>
                        </tr>
                        </table>
                        </div>
	</div>

</div>

   
    


</div><!-- End demo -->

<%
 if(pstmScYtd != null) {
        pstmScYtd.close();
    }
    if(res != null) {
        res.close();
    }

}catch (FileNotFoundException fne){
    System.out.println("File Not found "+fne.getMessage());
} catch (SQLException se){
    System.out.println("SQL Exception "+se.getMessage());
}finally{
    if(conn == null) {
        conn.close();
    }
}

%>

</body>
</html>
