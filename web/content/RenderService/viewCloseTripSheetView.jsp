<%--
    Document   : viewCloseTripSheet
    Created on : Jan 12, 2013, 8:45:49 AM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.net.*,java.io.*,java.util.*,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Trip Sheet</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript">

            function setRevenue(sno){
                var toLocation = document.getElementById('toLocation'+sno).value;
                var deliveredTon = document.getElementById('deliveredTon'+sno).value;
                if(toLocation != ''){
                    var temp = toLocation.split('^');
                    document.getElementById('returnAmount'+sno).value = parseFloat(parseFloat(deliveredTon) * parseFloat(temp[3])).toFixed(2);
                }

            }

            //                alert(sno);
            function getFromLocation(sno){
               var oTextbox = new AutoSuggestControl(document.getElementById("fromLocation"+sno),new ListSuggestions("fromLocation"+sno,"/throttle/getFromLocationSuggestion.do?sno="+sno+"&"));
            }
            function getToLocation(sno){
                // alert(sno);
                var returnTripType = document.getElementById('returnTripType'+sno).value;
                var customer = document.getElementById('customer'+sno).value;
                var fromLocation = document.getElementById('fromLocation'+sno).value;

                if(returnTripType == 'loaded') {
                    if(customer == ''){
                        alert("please select customer");
                    }else if(fromLocation == ''){
                        alert("please select fromLocation");
                    }else{
                        var temp = customer.split('^');
                        customer = temp[1];
                        temp = fromLocation.split('^');
                        fromLocation = temp[1];
                        var oTextbox = new AutoSuggestControl(document.getElementById("toLocation"+sno),new ListSuggestions("toLocation"+sno,"/throttle/getToLocationSuggestion.do?sno="+sno+"&fromLocation="+fromLocation+"&customer="+customer+"&"));
                    }
                }else {
                    var oTextbox = new AutoSuggestControl(document.getElementById("toLocation"+sno),new ListSuggestions("toLocation"+sno,"/throttle/getToLocationSuggestion.do?sno="+sno+"&fromLocation=0&customer=0&"));
                }

            }
            function getCustomers(sno){
                //alert(sno);
                //alert("Hi...cust::"+document.getElementById('customer'+sno).value);
                //alert("Hi...cust::"+document.getElementById('customer1').value);
                var oTextbox = new AutoSuggestControl(document.getElementById("customer"+sno),new ListSuggestions("customer"+sno,"/throttle/getCustomerSuggestion.do?sno="+sno+"&"));
                //                var oTextbox = new AutoSuggestControl(document.getElementById("customer1"),new ListSuggestions("customer1","/throttle/getCustomerSuggestion.do?sno="+sno+"&"));
            }
            function getProducts(sno){
                //alert(sno);
                //alert("Hi...prod::"+document.getElementById('returnProductName'+sno).value);
                //alert("Hi...prod::"+document.getElementById('returnProductName1').value);
                var oTextbox = new AutoSuggestControl(document.getElementById("returnProductName"+sno),new ListSuggestions("returnProductName"+sno,"/throttle/getProductSuggestion.do?sno="+sno+"&"));
                //                var oTextbox = new AutoSuggestControl(document.getElementById("returnProductName1"),new ListSuggestions("returnProductName1","/throttle/getProductSuggestion.do?sno="+sno+"&"));
            }
            function newWO(val){
                var tripSheetIdParam = document.tripSheet.tripSheetIdParam.value;
                var driverVendorId = document.getElementById('driverVendorId').value;
                var driverNameId = document.getElementById('driverNameId').value;
                var driverType = document.getElementById('driverType').value;
                //driverNameId,driverType, driverVendorId
                //alert('editReturnTripSheet.do?returnTripId='+val+'&tripSheetIdParam='+tripSheetIdParam);
                window.open('editReturnTripSheet.do?reqFor=view&returnTripId='+val+'&driverType='+driverType+'&driverNameId='+driverNameId+'&driverVendorId='+driverVendorId+'&tripSheetIdParam='+tripSheetIdParam, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
            }

            var httpRequest1;
            function saveReturnMovment(sno) {
                //alert(sno);
                var tripCode = document.getElementById('tripCode').value;
                var tripDate1 = document.getElementById('tripDate').value;
                var temp = tripDate1.split(" ");
                var tripDate =temp[0];

                var customer = document.getElementById('customer'+sno).value;
                //alert(customer);
                var fromLocation = document.getElementById('fromLocation'+sno).value;
                var toLocation = document.getElementById('toLocation'+sno).value;
                var returnTripType = document.getElementById('returnTripType'+sno).value;
                var vehicleType = document.getElementById('vehicleType').value;

                var returnProductName = document.getElementById('returnProductName'+sno).value;
                var returnLoadedDate = document.getElementById('returnLoadedDate'+sno).value;
                var returnDeliveredDate = document.getElementById('returnDeliveredDate'+sno).value;
                //                var returnTon = document.getElementById('returnTon'+sno).value;
                var outKM = document.getElementById('outKM'+sno).value;
                var inKM = document.getElementById('inKM'+sno).value;
                var returnKM = document.getElementById('returnKM'+sno).value;
                var returnAmount = document.getElementById('returnAmount'+sno).value;

                var loadedTon = document.getElementById('loadedTon'+sno).value;
                var deliveredTon = document.getElementById('deliveredTon'+sno).value;
                var shortageTon = document.getElementById('shortageTon'+sno).value;
                var loadedSlipNo = document.getElementById('loadedSlipNo'+sno).value;
                var deliveredDANo = document.getElementById('deliveredDANo'+sno).value;

                var vehicleVendorId = document.getElementById('vehicleVendorId').value;
                var driverVendorId = document.getElementById('driverVendorId').value;
                var driverNameId = document.getElementById('driverNameId').value;
                var driverType = document.getElementById('driverType').value;
                //driverNameId,driverType, driverVendorId
                //deliveredDANo loadedSlipNo shortageTon deliveredTon loadedTon

                var url='/throttle/saveReturnMovement.do?fromLocation='+fromLocation+'&vehicleType='+vehicleType+'&vehicleVendorId='+vehicleVendorId+'&driverNameId='+driverNameId+'&driverVendorId='+driverVendorId+'&vehicleVendorId='+vehicleVendorId+'&customer='+customer+'&vehicleType='+vehicleType+'&driverType='+driverType+'&tripCode='+tripCode+'&loadedTon='+loadedTon+'&deliveredTon='+deliveredTon+'&shortageTon='+shortageTon+'&loadedSlipNo='+loadedSlipNo+'&deliveredDANo='+deliveredDANo+'&tripDate='+tripDate+'&toLocation='+toLocation+'&returnTripType='+returnTripType+'&returnProductName='+returnProductName+'&returnLoadedDate='+returnLoadedDate+'&returnDeliveredDate='+returnDeliveredDate+'&outKM='+outKM+'&inKM='+inKM+'&returnKM='+returnKM+'&returnAmount='+returnAmount;
                //alert(url);
                //                document.getElementById('row'+sno).innerHTML = "<a href='#'>hello</a>";

                if (window.ActiveXObject)
                {
                    httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest1 = new XMLHttpRequest();
                }
                httpRequest1.open("POST", url, true);
                httpRequest1.onreadystatechange = function() {go(sno); } ;
                httpRequest1.send(null);

            }

            function go(sno) {
                if (httpRequest1.readyState == 4) {
                    if (httpRequest1.status == 200) {
                        var response = httpRequest1.responseText;
                        //alert(response);
                        if(response != null && response !=''){
                            document.getElementById('row'+sno).innerHTML = "<a href='#' onClick='newWO("+response+");'>edit</a>";
                            document.getElementById('fromLocation'+sno).readOnly =true;
                            document.getElementById('toLocation'+sno).readOnly =true;
                            document.getElementById('returnTripType'+sno).disabled  =true;
                            document.getElementById('returnProductName'+sno).readOnly =true;
                            document.getElementById('returnLoadedDate'+sno).readOnly =true;
                            document.getElementById('returnDeliveredDate'+sno).readOnly =true;
                            document.getElementById('returnTon'+sno).readOnly =true;
                            document.getElementById('outKM'+sno).readOnly =true;
                            document.getElementById('inKM'+sno).readOnly =true;
                            document.getElementById('returnKM'+sno).readOnly =true;
                            document.getElementById('returnAmount'+sno).readOnly =true;
                        }
                    }
                }
                window.location.reload();

            }




            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function bataCalculation(sno){
                var inKm = 0;
                var outKm = 0;
                var totKm = 0;
                if(document.getElementById("stageIdExp"+sno).value == "Bata") {
                    if(document.getElementById("kmsIn").value != ""){
                        inKm = parseInt(document.getElementById("kmsIn").value);
                    }
                    if(document.getElementById("kmsOut").value != ""){
                        outKm = parseInt(document.getElementById("kmsOut").value);
                    }
                }
                totKm = (inKm - outKm);
                document.getElementById("tripExpensesAmount"+sno).value = (totKm * 0.75).toFixed(0);
                calculateAmount();

            }
            function calculateAmount(){
                
                var totalAllowance = document.getElementById('totalAllowance').value;
                var expExpAmtExist = document.getElementById('expExpAmtExist').value;
                var expExpAmt = document.getElementById('expExpAmt').value;
                var totalExpenses = document.getElementById('existExpenses').value;
                var totalKm = document.getElementById('totalKms').value;
                var totalFuelLtrs = document.getElementById('totalFuelLtrs').value;
                var toPayAmount = document.getElementById("totalToPayAmt").value;
                totalExpenses = parseFloat(totalExpenses) - parseFloat(expExpAmtExist) + parseFloat(expExpAmt);
                //var allowanceRowSize = document.getElementsByName("tripAllowanceAmount");
                var expenseRowSize = document.getElementsByName("tripExpensesAmount");
                //                alert("totalAllowance:"+totalAllowance);
                //                alert("expenseRowSize.length:"+expenseRowSize.length);
                //                alert("totalExpenses"+totalExpenses);
                //                alert("totalFuelAmount"+document.getElementById("totalFuelAmount").value);
                //                for(var i=0; i<allowanceRowSize.length;i++) {
                //                        totalAllowance=parseInt(totalAllowance)+parseInt(allowanceRowSize[i].value);
                //
                //                }
                for(var i=0; i<expenseRowSize.length;i++) {
                    totalExpenses=parseInt(totalExpenses)+parseInt(expenseRowSize[i].value);
                }
                //alert(totalExpenses);

                //                if(document.getElementById("totalFuelAmount").value != ""){
                //                    totalExpenses = parseFloat(totalExpenses) + parseFloat(document.getElementById("totalFuelAmount").value);
                //                }


                if(parseFloat(totalKm) >0 && parseFloat(totalFuelLtrs) >0 ){
                    document.getElementById("totMileage").value = parseFloat(parseFloat(totalKm) / parseFloat(totalFuelLtrs)).toFixed(2);
                }else{
                    document.getElementById("totMileage").value = 0.00;
                }
                document.getElementById("totalExpenses").value = parseFloat(totalExpenses).toFixed(2);

                document.getElementById("totalAllowance").value = parseFloat(totalAllowance).toFixed(2);
                document.getElementById("nettAllowance").value = parseFloat(totalAllowance).toFixed(2);
                document.getElementById("balanceAmount").value = parseFloat(totalAllowance  - totalExpenses).toFixed(2);
                /*
                if(parseFloat(totalAllowance) > parseFloat(totalExpenses) ){
                    document.getElementById("balanceAmount").value = parseFloat(totalAllowance - totalExpenses).toFixed(2);
                }else{
                    if( parseFloat(totalAllowance) > 0){
                        document.getElementById("balanceAmount").value = parseFloat(totalAllowance - totalExpenses).toFixed(2);
                    }else {
                        document.getElementById("balanceAmount").value = parseFloat(totalExpenses - totalAllowance).toFixed(2);
                    }
                }
                */
                var totalExpense = document.getElementById('totalExpenses').value;
                var balanceAmount = document.getElementById('balanceAmount').value;
                document.getElementById("settlementAmount").value = parseFloat(parseFloat(balanceAmount) + parseFloat(toPayAmount) ).toFixed(2);
                /*
                if(parseFloat(toPayAmount) > 0){
                    if(parseFloat(balanceAmount) > 0){
                        //document.getElementById("settlementAmount").value = parseFloat(parseFloat(balanceAmount) - parseFloat(toPayAmount) ).toFixed(2);
                        document.getElementById("settlementAmount").value = parseFloat((-1 *  parseFloat(toPayAmount)) + parseFloat(balanceAmount)  ).toFixed(2);
                    }else{
                        document.getElementById("settlementAmount").value = parseFloat((-1 *  parseFloat(toPayAmount)) - parseFloat(balanceAmount)  ).toFixed(2);
                    }
                }else {
                    document.getElementById("settlementAmount").value = parseFloat(parseFloat(balanceAmount) - parseFloat(toPayAmount) ).toFixed(2);
                }
                */


                //return values
                var returnTotalAllowance = document.getElementById('returnTotalAllowance').value;
                var returnTotalExpense = document.getElementById('returnTotalExpense').value;
                var returnTotalKm = document.getElementById('returnTotalKm').value;

                /*
                if(parseFloat(returnTotalAllowance) > parseFloat(returnTotalExpense) ){
                    var returnBalanceAmount = parseFloat(parseFloat(returnTotalAllowance) - parseFloat(returnTotalExpense)).toFixed(2);
                }else{
                    var returnBalanceAmount = parseFloat(parseFloat(returnTotalExpense) - parseFloat(returnTotalAllowance)).toFixed(2);
                }
                document.getElementById("returnBalanceAmount").value = parseFloat(returnBalanceAmount).toFixed(2);
                */
               document.getElementById("returnBalanceAmount").value = parseFloat(parseFloat(returnTotalAllowance ) - parseFloat(returnTotalExpense)).toFixed(2);
                //nett values
                var returnBalanceAmount = document.getElementById("returnBalanceAmount").value;
                var nettTotalAllowance = document.getElementById('nettTotalAllowance').value;
                var nettExpense = document.getElementById('nettExpenses').value;
                var nettKm = document.getElementById('nettKm').value;
                var nettFuelLtrs = document.getElementById('nettFuelLtrs').value;
                var nettBalanceAmount = document.getElementById('nettBalanceAmount').value;
                //reset values
                document.getElementById('nettTotalAllowance').value = parseFloat(parseFloat(totalAllowance) + parseFloat(returnTotalAllowance)).toFixed(2);
                document.getElementById('nettExpenses').value = parseFloat(parseFloat(totalExpense) + parseFloat(returnTotalExpense)).toFixed(2);
                document.getElementById('nettKm').value = parseFloat(totalKm).toFixed(2);
                document.getElementById('nettBalanceAmount').value = parseFloat(parseFloat(balanceAmount) + parseFloat(returnBalanceAmount)).toFixed(2);
                if(parseFloat(totalKm) >0 && parseFloat(nettFuelLtrs) >0 ){
                    document.getElementById('nettMileage').value = parseFloat(parseFloat(totalKm) / parseFloat(nettFuelLtrs)).toFixed(2);
                }else{
                    document.getElementById('nettMileage').value = 0.00;
                }
                var nettBalanceAmount = document.getElementById('nettBalanceAmount').value;
                /*
                if(parseFloat(toPayAmount) > 0){
                    if(parseFloat(nettBalanceAmount) > 0){
                        //document.getElementById("nettSettlementAmount").value = parseFloat(parseFloat(nettBalanceAmount) - parseFloat(toPayAmount) ).toFixed(2);
                        document.getElementById("nettSettlementAmount").value = parseFloat((-1 *  parseFloat(toPayAmount)) + parseFloat(nettBalanceAmount)  ).toFixed(2);
                    }else{
                        document.getElementById("nettSettlementAmount").value = parseFloat((-1 *  parseFloat(toPayAmount)) - parseFloat(nettBalanceAmount)  ).toFixed(2);
                    }
                }else {
                    document.getElementById("nettSettlementAmount").value = parseFloat(parseFloat(nettBalanceAmount) - parseFloat(toPayAmount)).toFixed(2);
                }
                */
               document.getElementById("nettSettlementAmount").value = parseFloat(parseFloat(toPayAmount) + parseFloat(nettBalanceAmount)).toFixed(2);

            }
            function calculateAmountOLD(){
                var totalAllowance = 0;
                var totalExpenses = 0;
                var balanceAmount = 0;
                var expEtmTotal = 0;
                //                CLPL Trip amount stats
                var billAmt = 0;
                var totalExpensesRe = 0;
                var balanceAmountRe = 0;
                if(document.getElementById("tripReceivedAmount").value != "") {
                    billAmt = parseFloat(document.getElementById("tripReceivedAmount").value);
                }

                if(document.getElementById('returnTrip').checked == true){
                    balanceAmountRe = parseFloat(document.getElementById("returnAmount").value);
                    totalExpensesRe = parseFloat(document.getElementById("returnExpenses").value);

                }
                //                CLPL Trip amount end


                if(document.getElementById("expEtmTotal").value != "") {
                    expEtmTotal = parseFloat(document.getElementById("expEtmTotal").value);
                }
                for(var i=0; i<11;i++)
                    if(document.getElementById("tripAllowanceAmount"+i) && document.getElementById("tripAllowanceAmount"+i).value != ""){
                        totalAllowance += parseFloat(document.getElementById("tripAllowanceAmount"+i).value);
                    }
                for(var i=0; i<11;i++)
                    if(document.getElementById("tripExpensesAmount"+i) && document.getElementById("tripExpensesAmount"+i).value != ""){
                        totalExpenses += parseFloat(document.getElementById("tripExpensesAmount"+i).value);
                    }
                if(document.getElementById("totalFuelAmount").value != ""){
                    totalExpenses += parseFloat(document.getElementById("totalFuelAmount").value);
                }
                totalExpenses += expEtmTotal;
                balanceAmount = (totalAllowance - totalExpenses );
                //                CLPL Trip amt start
                if(billAmt > 0){
                    balanceAmount += billAmt;
                }


                //                CLPL Trip amt end
                document.getElementById("balanceAmount").value = balanceAmount.toFixed(2);
                document.getElementById("totalExpenses").value = totalExpenses.toFixed(2);
                document.getElementById("totalAllowance").value = totalAllowance.toFixed(2);
                if(totalExpensesRe >0)
                {
                    totalExpenses +=totalExpensesRe;
                    document.getElementById("totalExpensesReturn").value = totalExpenses.toFixed(2);
                }
                if(balanceAmountRe >0)
                {
                    if(totalExpensesRe =="NaN"){
                        //alert("ll");
                        totalExpensesRe = 0;
                    }
                    balanceAmount = (balanceAmount - totalExpensesRe );
                    balanceAmount +=balanceAmountRe;
                    document.getElementById("balanceAmountRetuen").value = balanceAmount.toFixed(2);
                }
            }

            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';
            function addAllowanceRow() {
                if(sno < 9){
                    sno++;
                    var tab = document.getElementById("allowanceTBL");
                    var rowCount = tab.rows.length;
                    snumber = parseInt(rowCount)-1;
                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                    newrow.height="30px";

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><select class='form-control' id='stageId' style='width:130px'  name='stageId"+sno+"'><option selected value=0>---Select---</option><c:if test = "${opLocation != null}" ><c:forEach items="${opLocation}" var="opl"><option  value='<c:out value="${opl.locationId}" />'><c:out value="${opl.locationName}" /> </c:forEach ></c:if> </select></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text2'><input name='tripAllowanceDate' id='tripAllowanceDate"+snumber+"' type='text' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='tripAllowanceAmount' id='tripAllowanceAmount"+snumber+"' type='text' class='form-control' onkeyup='calculateAmount();' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    cell0 = "<td class='text1'><select class='form-control' id='tripAllowancePaidBy' style='width:125px'  name='tripAllowancePaidBy"+sno+"'><option selected value=0>---Select---</option><c:if test = "${paidBy != null}" ><c:forEach items="${paidBy}" var="paid"><option  value='<c:out value="${paid.issuerId}" />'><c:out value="${paid.issuerName}" /> </c:forEach ></c:if> </select></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(5);
                    cell0 = "<td class='text1'><input name='tripAllowanceRemarks' type='text' class='form-control' id='tripAllowanceRemarks' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(6);
                    var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                    cell.innerHTML = cell1;

                    $( ".datepicker" ).datepicker({
                        /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });
                }
            }
            function delAllowanceRow() {
                try {
                    var table = document.getElementById("allowanceTBL");
                    rowCount = table.rows.length-1;
                    for(var i=2; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }sumAllowanceAmt();
                }catch(e) {
                    alert(e);
                }
            }
            function submitPage() {
                if(isEmpty(document.getElementById("routeId").value)){
                    alert("Select Route");
                    document.getElementById("routeId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("vehicleId").value)){
                    alert("Select Vehicle");
                    document.getElementById("vehicleId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("tripCode").value)){
                    alert("Trip Code is not filled");
                    document.getElementById("tripCode").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsOut").value)){
                    alert("Kms Out is not filled");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isDigit(document.getElementById("kmsOut").value)){
                    alert("Kms Out accepts only numeric values");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsIn").value)){
                    document.getElementById("kmsIn").focus();
                    alert("Kms Out is not filled");
                    return false;
                }else if(isDigit(document.getElementById("kmsIn").value)){
                    alert("Kms In accepts only numeric values");
                    document.getElementById("kmsIn").focus();
                    return false;
                }/*else if(parseFloat(document.getElementById("kmsIn").value) == 0){
                    alert("Kms In Should not be 0");
                    document.getElementById("kmsIn").focus();
                    return false;
                }*/else if(isEmpty(document.getElementById("arrivalDate").value)){
                    alert("Arrival Date is not filled");
                    document.getElementById("arrivalDate").focus();
                    return false;
                }
                /*
                else if(isEmpty(document.getElementById('fromLocation').value)){
                    alert("fromLocation is not filled");
                    document.getElementById("fromLocation").focus();
                    return false;
                }else if(isEmpty(document.getElementById('toLocation').value)){
                    alert("toLocation is not filled");
                    document.getElementById("toLocation").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnLoadedTonnage').value)){
                    alert("returnLoadedTonnage is not null");
                    document.getElementById("returnLoadedTonnage").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnLoadedDate').value)){
                    alert("returnLoadedDate is not null");
                    document.getElementById("returnLoadedDate").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnDeliveredTonnage').value)){
                    alert("returnDeliveredTonnage is not null");
                    document.getElementById("returnDeliveredTonnage").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnDeliveredDate').value)){
                    alert("returnDeliveredDate is not null");
                    document.getElementById("returnDeliveredDate").focus();
                    return false;
                }else if(isEmpty(document.getElementById('productName').value)){
                    alert("productName is not null");
                    document.getElementById("productName").focus();
                    return false;
                }else if(isEmpty(document.getElementById('shortageTonnage').value)){
                    alert("shortageTonnage is not null");
                    document.getElementById("shortageTonnage").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnExpenses').value)){
                    alert("returnExpenses is not null");
                    document.getElementById("returnExpenses").focus();
                    return false;
                }else if(isEmpty(document.getElementById('returnAmount').value)){
                    alert("ReturnAmount is not null");
                    document.getElementById("returnAmount").focus();
                    return false;
                }
                 */

                document.tripSheet.action='/throttle/updateCloseTripSheet.do';
                document.tripSheet.submit();

            }

            function fillData(){
                //  document.getElementById("tripCode").value='T0003';
                var currentDate = new Date();
                var day = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var myDate= day + "-" + month + "-" + year;
                //                document.getElementById("departureDate").value=myDate;
                //                document.getElementById("tripDate").value=myDate;
                //                day = currentDate.getDate()+1;
                //                myDate= day +  "-" + month + "-" + year;
                var date = document.getElementById("aDate").value
                var kmsIn = document.getElementById("kmsIn").value

                if(kmsIn == 'null' || kmsIn == ""){
                    document.getElementById("kmsIn").value=0;
                }
                if(date == 'null' || date == ""){
                    document.getElementById("arrivalDate").value=myDate;
                }else{
                    document.getElementById("arrivalDate").value=date;
                }
                totalKmsFunc();
                calculateAmount();
            }
            function show(){
                if(document.getElementById("billStatus").value == "Paid") {
                    //            alert("123");
                    document.getElementById("tripAM").style.display="none";

                }else{
                    //          alert("456");
                    document.getElementById("tripReceivedAmount").value =document.getElementById("tripRevenue").value;
                    document.getElementById("totalToPayAmt").value =document.getElementById("tripRevenue").value;
                    document.getElementById("nettToPayAmt").value =document.getElementById("tripRevenue").value;
                    document.getElementById("tripAM").style.display="table-Row";
                    calculateAmount();
                }
            }
            var xmlhttp;


            function gpsDetails(){
                var tripCode = document.getElementById("tripCode").value;

                var arrivalDate1 = document.getElementById("arrivalDate").value;
                var temp = arrivalDate1.split("-");
                var arrivalDate =temp[0]+"/"+temp[1]+"/"+temp[2];
                var selecedDate = document.getElementById("selecedDate").value;
                document.tripSheet.action="/throttle/viewCloseTripSheet.do?tripCode="+tripCode+"arrivalDate="+arrivalDate+"selecedDate="+selecedDate;
                document.tripSheet.submit();
            }


        </script>


    </head>
    <body onload="fillData();">
<!--        <form name="tripSheet" action="updateCloseTripSheet.do" method="post" onsubmit="return validateSubmit();" >-->
<form name="tripSheet" method="post" >
            <%@ include file="/content/common/message.jsp" %>
            <%
                        OperationTO operationTo = new OperationTO();
                        ArrayList tripDetailsAL = (ArrayList) request.getAttribute("tripDetails");
                        if (tripDetailsAL != null && tripDetailsAL.size() > 0) {
                            operationTo = (OperationTO) tripDetailsAL.get(0);
                            String vehileId = operationTo.getTripVehicleId();

            %>
            <center><h2> View Trip Sheet</h2><input type="hidden" name="tripSheetIdParam" value="<%=request.getParameter("tripSheetIdParam")%>" /></center>
            <div style="padding-left: 60px;">
                <table cellpadding="0" cellspacing="4" border="0" width="100%">
                    <tr>
                        <td class="contenthead">Trip Sheet Details </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td  class="texttitle1"> Route</td>
                                    <td  class="text1">
                                        <input name="routeId" id="routeId" type="hidden" class="form-control" value="<%=operationTo.getTripRouteId()%>"  />
                                        <input name="vehicleType" id="vehicleType" type="hidden" class="form-control" value="<%=operationTo.getOwnership()%>"  />
                                        <input name="driverType" id="driverType" type="hidden" class="form-control" value="<%=operationTo.getContractor()%>"  />
                                        <input name="vehicleVendorId" id="vehicleVendorId" type="hidden" class="form-control" value="<%=operationTo.getVehicleVendorId()%>"  />
                                        <input name="driverVendorId" id="driverVendorId" type="hidden" class="form-control" value="<%=operationTo.getDriverVendorId()%>"  />
                                        <input name="customerType" id="customerType" type="hidden" class="form-control" value="<%=operationTo.getCustomertypeId()%>"  />
                                        <input name="settlementType" id="settlementType" type="hidden" class="form-control" value="<%=operationTo.getSettlementType()%>"  />
                                        <input name="customerId" id="customerId" type="hidden" class="form-control" value="<%=operationTo.getCustId()%>"  />
                                        <input name="routeName" type="text" class="form-control" value="<%=operationTo.getRouteName()%>" readonly="true" id="routeName"  />
                                    </td>
                                    <td class="texttitle1">
                                        Vehicle
                                    </td>
                                    <td  class="text1">
                                        <input name="vno" type="hidden" class="form-control" value="" id="vno"  />
                                        <input name="vehicleId" type="hidden" class="form-control" value="<%=operationTo.getTripVehicleId()%>" id="vehicleId"  />
                                        <select name="vehicleId1"  id="vehicleId1" class="form-control" style="width:120px;" disabled>
                                            <option value=""> -Select- </option>
                                            <c:if test = "${vehicleRegNos != null}" >
                                                <c:forEach items="${vehicleRegNos}" var="vd">
                                                    <option value='<c:out value="${vd.vehicleId}" />'><c:out value="${vd.regNo}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td  class="texttitle1">
                                    </td>

                                <script type="text/javascript">
                                    function visibleVehicle(){
                                        var x=document.getElementById("vehicleId");
                                        x.disabled=false;
                                    }
                                    document.getElementById('routeId').value = "<%=operationTo.getTripRouteId()%>";
                                    document.getElementById('vehicleId1').value = "<%=operationTo.getTripVehicleId()%>";
                                </script>

                                    <td class="texttitle2">
                                        Trip Id
                                    </td>
                                    <td class="text2">
                                        <input name="tripCode" type="text" class="form-control" value="<%=operationTo.getTripSheetId()%>" readonly="readonly" id="tripCode"  />
                                        <input name="requestCmd" type="hidden"  id="requestCmd" value="save" />
                                    </td>
                                    <td class="texttitle2">Trip Date</td>
                                    <td class="text2">
                                        <input name="tripDate" type="text" class="form-control" value="<%=operationTo.getTripDate()%>"  readonly="readonly" id="tripDate" />
                                    </td>
                                    <td class="texttitle2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1"> Departure Date</td>
                                    <td class="text1">
                                        <input name="departureDate" type="text" class="form-control"  value="<%=operationTo.getTripDepartureDate()%>" id="departureDate" readonly="readonly" />
                                        <select class="form-control" name="dHours" id="dHours" style="width:45px;" >
                                            <option value="00">00</option>
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                            <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                            <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                            <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                            <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                            <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                            <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                            <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                            <option value="24">24</option>
                                        </select>
                                        <script type="text/javascript">
                                            document.getElementById('dHours').value = "<%=operationTo.getDepHours()%>";
                                        </script>
                                        <select class="form-control" name="dMints" id="dMints" style="width:45px;" >
                                            <option value="00">00</option>
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                            <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                            <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                            <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                            <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                            <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                            <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                            <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                            <option value="24">24</option><option value="25">25</option><option value="26">26</option>
                                            <option value="27">27</option><option value="28">28</option><option value="29">29</option>
                                            <option value="30">30</option><option value="31">31</option><option value="32">32</option>
                                            <option value="33">33</option><option value="34">34</option><option value="35">35</option>
                                            <option value="36">36</option><option value="37">37</option><option value="38">38</option>
                                            <option value="39">39</option><option value="40">40</option><option value="41">41</option>
                                            <option value="42">42</option><option value="43">43</option><option value="44">44</option>
                                            <option value="45">45</option><option value="46">46</option><option value="47">47</option>
                                            <option value="48">48</option><option value="49">49</option><option value="50">50</option>
                                            <option value="51">51</option><option value="52">52</option><option value="53">53</option>
                                            <option value="54">54</option><option value="55">55</option><option value="56">56</option>
                                            <option value="57">57</option><option value="58">58</option><option value="59">59</option>
                                        </select>
                                        <script type="text/javascript">
                                            document.getElementById('dMints').value = "<%=operationTo.getDepMints()%>";
                                        </script>
                                    </td>
                                    <td class="texttitle1"><font color="red">*</font>Arrival Date</td>
                                    <td class="text1">
                                        <input name="aDate" type="hidden" class="form-control" value="<c:out value="${selectedDate}"/>" id="aDate" />
                                        <input name="selecedDate" type="hidden"  id="selecedDate" value="Y" />
                                        <input name="arrivalDate" type="text" class="datepicker" value="" onchange="gpsDetails();" id="arrivalDate" readonly="true"  />
                                        <select class="form-control" name="aHours" id="aHours" style="width:45px;" >
                                            <option value="00">00</option>
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                            <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                            <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                            <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                            <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                            <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                            <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                            <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                            <option value="24">24</option>
                                        </select>
                                        <script type="text/javascript">
                                            //      document.getElementById('aHours').value = "<%=operationTo.getArrHours()%>";
                                        </script>
                                        <select class="form-control" name="aMints" id="aMints" style="width:45px;" >
                                            <option value="00">00</option>
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                            <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                            <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                            <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                            <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                            <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                            <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                            <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                            <option value="24">24</option><option value="25">25</option><option value="26">26</option>
                                            <option value="27">27</option><option value="28">28</option><option value="29">29</option>
                                            <option value="30">30</option><option value="31">31</option><option value="32">32</option>
                                            <option value="33">33</option><option value="34">34</option><option value="35">35</option>
                                            <option value="36">36</option><option value="37">37</option><option value="38">38</option>
                                            <option value="39">39</option><option value="40">40</option><option value="41">41</option>
                                            <option value="42">42</option><option value="43">43</option><option value="44">44</option>
                                            <option value="45">45</option><option value="46">46</option><option value="47">47</option>
                                            <option value="48">48</option><option value="49">49</option><option value="50">50</option>
                                            <option value="51">51</option><option value="52">52</option><option value="53">53</option>
                                            <option value="54">54</option><option value="55">55</option><option value="56">56</option>
                                            <option value="57">57</option><option value="58">58</option><option value="59">59</option>
                                        </select>
                                        <script type="text/javascript">
                                            //document.getElementById('aMints').value = "<%=operationTo.getArrMints()%>";
                                        </script>
                                    </td>
                                    <td class="texttitle1">
                                    </td>

                                    <td class="texttitle2">Kms Out</td>
                                    <td class="text2">
                                        <input name="kmsOut" type="text" class="form-control"  value="<%=operationTo.getTripKmsOut()%>" id="kmsOut"  OnKeyPress="NumericOnly();" readonly />
                                    </td>

                                    <td class="texttitle2">
                                        Driver Name
                                    </td>
                                    <td class="texttitle2">
                                        <input name="driverNameId" type="hidden" class="form-control" value="<%=operationTo.getTripDriverId()%>" id="driverNameId"  />
                                        <select class='form-control' style="width:123px; " id="driverNameId1"  name="driverNameId1" disabled>
                                            <option selected  value="0">---Select---</option>
                                            <c:if test = "${driverName != null}" >
                                                <c:forEach items="${driverName}" var="dri">
                                                    <option  value='<c:out value="${dri.empId}" />'>
                                                        <c:out value="${dri.empName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>
                                        <script type="text/javascript">
                                            document.getElementById('driverNameId1').value = "<%=operationTo.getTripDriverId()%>";
                                        </script>
                                    </td>
                                    <td class="texttitle2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1"><font color="red">*</font>
                                        Km In
                                    </td>
                                    <td class="texttitle1">
                                        <!--<input name="kmsIn" type="text" class="form-control"  value="<%=operationTo.getTripKmsIn()%>" id="kmsIn" onblur="totalKmsFunc();setBalance();bataCalculation(1)" OnKeyPress="NumericOnly();" />-->
                                        <input name="kmsIn" readonly type="text" class="form-control"  value="<%=operationTo.getTripKmsIn()%>" id="kmsIn" onblur="totalKmsFunc();calculateAmount();" OnKeyPress="NumericOnly();" />
                                    </td>

                                    <td class="texttitle1">
                                        Bill Type
                                    </td>
                                    <td class="texttitle1">
                                        <input name="billStatus" type="text" class="form-control"  value="<%=operationTo.getBillStatus()%>" id="billStatus" readonly />
                                    </td>
                                    <td class="texttitle1">
                                    </td>

                                    <td class="texttitle1">
                                        GPS Km
                                    </td>

                                    <td class="texttitle1">
                                        <input name="gpskm" type="text" class="form-control" value="<c:out value="${totalGPSKM}"/>" id="gpskm"  />
                                    </td>

                                    <td class="texttitle1">
                                        Status
                                    </td>
                                    <td class="text1">
                                        <select name="status" class="form-control"  id="status">
                                            <option value="Close">Close</option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">
                                    </td>
                                </tr>
                                <tr>
                                    <input name="driverBata" type="hidden" readonly class="form-control"  id="driverBata" />
                                        <input name="routeToll" type="hidden" readonly class="form-control"  id="routeToll" />
<!--                                    <td class="texttitle2">
                                        Driver Bata / Ton
                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                    <td class="texttitle2">
                                        Toll Fee
                                    </td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">
                                    </td>-->

                                    <td class="texttitle1">
                                        Freight Rate / Ton
                                    </td>
                                    <td class="texttitle1">
                                        <input name="tonnage" type="text" onChange="callAjax();"  class="form-control"  id="tonnage" readonly value="<%=operationTo.getTonnageRate()%>" />
                                    </td>

                                    <td class="texttitle1">
                                        Expected Revenue from this Trip
                                    </td>
                                    <td class="texttitle1">
                                        <input name="tripRevenue" type="text" readonly class="form-control"  id="tripRevenue" value="<%=operationTo.getRevenue()%>" />
                                    </td>
                                    <td class="text1">
                                    </td>

                                    <td class="texttitle2">
                                        Loaded Tonnage
                                    </td>
                                    <td class="texttitle2">
                                        <input name="loadedTonnage" type="text" class="form-control"  id="loadedTonnage" value="<%=operationTo.getTotalTonnage()%>" readonly />
                                    </td>

                                    <td class="texttitle2">
                                        <font color="red">*</font>Delivered Tonnage
                                    </td>
                                    <td class="texttitle2">
                                        <input name="deliveredTonnage" type="text" class="form-control"  id="deliveredTonnage" value="<%=operationTo.getTotalTonnage()%>" />
                                    </td>
                                    <td class="texttitle2">
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="texttitle1">
                                        Cleaner Status
                                    </td>
                                    <td class="texttitle1">
                                        <select name="cleanerStatus" class="form-control"  id="cleanerStatus" disabled>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <input name="cleanerStatusVal" type="hidden" class="form-control"  id="cleanerStatusVal" value="<%=operationTo.getCleanerStatus()%>" />
                                        <script type="text/javascript">
                                            document.getElementById('cleanerStatus').value = "<%=operationTo.getCleanerStatus()%>";
                                        </script>
                                    </td>

                                    <td class="texttitle1">
                                        Trip Type
                                    </td>
                                    <td class="texttitle1">
                                        <select name="tripType" class="form-control"  id="tripType" disabled>
                                            <option value="1">Loaded Trip</option>
                                        </select>
                                        <input type="hidden" name="tripTypeVal" id="tripTypeVal" value="<%=operationTo.getTripType()%>"/>
                                        <script type="text/javascript">
                                            document.getElementById('tripType').value = "<%=operationTo.getTripType()%>";
                                        </script>
                                    </td>
                                    <td class="texttitle1">
                                    </td>

                                    <td class="texttitle2">
                                        Vehicle IN
                                    </td>
                                    <td class="texttitle2">
                                        <select name="vehicleInOut" id="vehicleInOut" class="form-control" style="width:120px;" disabled>
                                            <option value="0" selected>In</option>

                                        </select>
                                        <script type="text/javascript">
                                            document.getElementById('vehicleInOut').value = "<%=operationTo.getInOutIndication()%>";
                                        </script>
                                    </td>

                                    <td class="texttitle2">
                                        Vehicle In Location
                                    </td>
                                    <td class="texttitle2">
                                        <select name="vehicleInOutLoc" id="vehicleInOutLoc" class="form-control" style="width:120px;" disabled>
                                            <option value="Karikkali" selected>Karikkali</option>
                                        </select>
                                        <script type="text/javascript">
                                            //                                            document.getElementById('vehicleInOutLoc').value = "<%=operationTo.getLocationId()%>";
                                        </script>
                                    </td>
                                    <td class="texttitle2">
                                    </td>
                                     </tr>
                                <tr>
                                    <td class="texttitle1"> Pink Slip :</td>
                                    <td class="texttitle1">
                                        <input name="pinkSlip" type="text"  class="form-control"  id="pinkSlip" value="<%=operationTo.getPinkSlipID()%>" readonly />
                                    </td>
                                    <td class="texttitle1"> LPS  : </td>
                                    <td class="texttitle1">
                                        <input name="tonnage" type="text"  class="form-control"  id="tonnage" value="<%=operationTo.getOrderNo()%>" readonly />
                                    </td>
                                    <td class="texttitle1">
                                    </td>

                                    <td class="texttitle2"> Bags :</td>
                                    <td class="texttitle2">
                                        <input name="bags" type="text"  class="form-control"  id="bags" value="<%=operationTo.getBags()%>" readonly />
                                    </td>

                                    <td class="texttitle2">&nbsp;
                                    </td>
                                    <td class="texttitle2">&nbsp;
                                    </td>
                                </tr>

                                <!--                                <div >-->
                                <tr id="tripAM" style="display: none;">
                                    <td class="texttitle1"><font color="red">*</font> Trip Received Amount</td>
                                    <td class="text1">
                                        <input name="tripReceivedAmount" type="text"  class="form-control" readonly  id="tripReceivedAmount" value="0" onkeyup="calculateAmount();" OnKeyPress="NumericOnly()"/>
                                    </td>
                                    <td class="texttitle1">
                                        &nbsp;
                                    </td>
                                    <td class="texttitle1">
                                        &nbsp;
                                    </td>
                                    <td class="texttitle1">
                                    </td>
                                </tr>
                            </table>
                                    <table>
                                <!--                                </div>-->


                                <tr style="height: 10px;">
                                    <td align="right" class="TableColumn" colspan="5">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="center"  colspan="5" rowspan="1">
                                        <h2>Payment Details</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="allowanceTBL" name="allowanceTBL">
                                                <tr style="width:50px;">
                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                    <th class="contenthead">Location</th>
                                                    <th class="contenthead">Date</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Paid Person</th>
                                                    <th class="contenthead">Remarks</th>
                                                    <th class="contenthead">Delete</th>
                                                </tr>
                                                <%
                                                                            int snoAllo = 0;
                                                                            float totalAllowanceGiven = 0.00F;

                                                                            TripAllowanceTO allowanceTo = null;
                                                                            ArrayList allowanceAL = (ArrayList) request.getAttribute("tripAllowanceDetails");
                                                                            if (allowanceAL != null) {
                                                                                Iterator it = allowanceAL.iterator();
                                                                                while (it.hasNext()) {
                                                                                    allowanceTo = new TripAllowanceTO();
                                                                                    allowanceTo = (TripAllowanceTO) it.next();
                                                                                    snoAllo++;
                                                                                    totalAllowanceGiven = totalAllowanceGiven + Float.parseFloat(allowanceTo.getTripAllowanceAmount());
                                                %>
                                                <tr>
                                                    <td><%=snoAllo%></td>
                                                    <td>

                                                        <select class='form-control' style="width:130px; " id='stageId<%=snoAllo%>'  name='stageId' disabled >
                                                            <option selected  value="0">---Select---</option>
                                                            <c:if test = "${opLocation != null}" >
                                                                <c:forEach items="${opLocation}" var="opl">
                                                                    <option  value='<c:out value="${opl.locationId}" />'> <c:out value="${opl.locationName}" />
                                                                    </c:forEach >
                                                                </c:if>
                                                        </select>
                                                        <script type="text/javascript">
                                                            document.getElementById("stageId<%=snoAllo%>").value = '<c:out value="${opl.locationId}"/>';
                                                        </script>
                                                    </td>
                                                    <td>
                                                        <input name="tripAllowanceDate" type="text" class="form-control"  id="tripAllowanceDate" value ="<%=allowanceTo.getTripAllowanceDate()%>"  readonly />
                                                    </td>
                                                    <td>
                                                        <input name="tripAllowanceAmount" type="text" class="form-control" id="tripAllowanceAmount<%=snoAllo%>" value ="<%=allowanceTo.getTripAllowanceAmount()%>" onkeyup="calculateAmount();"  OnKeyPress="NumericOnly()" readonly />
                                                    </td>
                                                    <td>
                                                        <script type="text/javascript">
                                                            document.getElementById('stageId<%=snoAllo%>').value = "<%=allowanceTo.getTripStageId()%>";
                                                            function sumAllowanceAmt(){
                                                                var sumAmt=0;
                                                                var totAmt=0;
                                                                sumAmt= document.getElementsByName('tripAllowanceAmount');
                                                                for(i=0;i<sumAmt.length;i++){
                                                                    totAmt=parseInt(totAmt)+parseInt(sumAmt[i].value);
                                                                    document.getElementById('totalAllowance').value=parseInt(totAmt);
                                                                }
                                                            }
                                                            function sumExpenses(){
                                                                var sumAmt=0;
                                                                sumAmt= parseInt(document.getElementById('totalAllowance').value)+parseInt(document.getElementById('totalFuelAmount').value);
                                                                document.getElementById('totalExpenses').value=parseInt(sumAmt);
                                                            }
                                                            function setBalance(){
                                                                var sumAmt=0;
                                                                sumAmt= parseInt(document.getElementById('totalAllowance').value)-parseInt(document.getElementById('totalFuelAmount').value);
                                                                document.getElementById('balanceAmount').value=parseInt(sumAmt);
                                                            }
                                                        </script>
                                                        <select class='form-control' style="width:123px; " id='tripAllowancePaidBy<%=snoAllo%>' name='tripAllowancePaidBy' disabled >
                                                            <option selected  value="0">---Select---</option>
                                                            <c:if test = "${paidBy != null}" >
                                                                <c:forEach items="${paidBy}" var="paid">
                                                                    <option  value='<c:out value="${paid.issuerId}" />'> <c:out value="${paid.issuerName}" />
                                                                    </c:forEach >
                                                                </c:if>
                                                        </select>
                                                        <script type="text/javascript">
                                                            document.getElementById("tripAllowancePaidBy<%=snoAllo%>").value = "<%=allowanceTo.getTripAllowancePaidById()%>" ;
                                                        </script>
                                                    </td>
                                                    <td>
                                                        <input name="tripAllowanceRemarks" type="text" class="form-control" id="tripAllowanceRemarks" value ="<%=allowanceTo.getTripAllowanceRemarks()%>"  readonly />
                                                        <input type="hidden" name="tripAllowanceId" id="tripAllowanceId" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <%
                                                                                }

                                                                            }
                                                %>
                                                <!--                                                <tr>
                                                                                                    <td colspan="6" align="center">
                                                                                                        <input type="button" name="add" value="Add"  onclick="addAllowanceRow()" id="add" class="button" />
                                                                                                        &nbsp;&nbsp;&nbsp;

                                                                                                        <input type="button" name="delete" value="Delete" onclick="delAllowanceRow()" id="delete" class="button" />

                                                                                                    </td>
                                                                                                    <td>&nbsp;</td>
                                                                                                </tr>-->
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right" class="TableColumn">
                                    </td>
                                    <td align="left" class="TableColumn">
                                    </td>

                                    <td align="right" class="TableColumn">
                                        Total
                                    </td>
                                    <td align="left" class="TableColumn">
                                        <input name="totalAllowance" type="text" class="form-control" value="<%=totalAllowanceGiven%>" id="totalAllowance" readonly  />
                                    </td>
                                    <td class="TableRowNew">
                                    </td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_Tr1" align="center">
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1">
                                        <h2>Fuel Details</h2>
                                    </td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_Tr2">
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" class="border" id="fuelTBL" name="fuelTBL" style="width: 100%; left: 30px;" class="border">

                                                <tr style="width:40px;">
                                                    <th class="contenthead">S No</th>
                                                    <th class="contenthead">Bunk Name</th>
                                                    <th class="contenthead">Place</th>
                                                    <th class="contenthead">Date</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Ltrs</th>
                                                    <!--<th class="contenthead">Person</th>-->
                                                    <th class="contenthead">Remarks</th>
                                                    <th class="contenthead">Delete</th>
                                                </tr>
                                                <%
                                                                            int snoFuel = 0;
                                                                            float totalLitres = 0.00F;
                                                                            float totalFuelAmount = 0.00F;
                                                                            TripFuelDetailsTO fuelTo = new TripFuelDetailsTO();
                                                                            ArrayList fuelAL = (ArrayList) request.getAttribute("fuelDetails");
                                                                            if (fuelAL != null) {
                                                                                Iterator itFuel = fuelAL.iterator();
                                                                                while (itFuel.hasNext()) {
                                                                                    fuelTo = new TripFuelDetailsTO();
                                                                                    fuelTo = (TripFuelDetailsTO) itFuel.next();
                                                                                    snoFuel++;
                                                                                    totalLitres = totalLitres + Float.parseFloat(fuelTo.getTripFuelLtrs());
                                                                                    totalFuelAmount = totalFuelAmount + Float.parseFloat(fuelTo.getTripFuelAmounts());
                                                %>
                                                <tr>

                                                    <td> <%=snoFuel%> </td>

                                                    <td>
                                                        <%=fuelTo.getTripBunkName()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripBunkPlace()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelDate()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelAmounts()%>
                                                    </td><td>
                                                        <%=fuelTo.getTripFuelLtrs()%>
                                                    </td>
                                                    <td>
                                                        <%=fuelTo.getTripFuelRemarks()%>
                                                        <input type="hidden" name="HfId" id="HfId" />

                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <%
                                                                                }

                                                                            }
                                                %>
                                                <!--                                                <tr>
                                                                                                    <td colspan="8" align="center">
                                                                                                        <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />
                                                                                                        &nbsp;&nbsp;&nbsp;
                                                                                                        <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="button" />
                                                                                                    </td>
                                                                                                    <td>&nbsp;</td>
                                                                                                </tr>-->

                                            </table>
                                        </div>
                                        <script type="text/javascript">
                                            var poItems = 0;
                                            var rowCount='';
                                            var sno='';
                                            var snumber = '';

                                            function addFuelRow()
                                            {
                                                if(sno < 9){
                                                    sno++;
                                                    var tab = document.getElementById("fuelTBL");
                                                    var rowCount = tab.rows.length;

                                                    snumber = parseInt(rowCount)-1;
                                                    //                    if(snumber == 1) {
                                                    //                        snumber = parseInt(rowCount);
                                                    //                    }else {
                                                    //                        snumber++;
                                                    //                    }

                                                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                    newrow.height="30px";
                                                    // var temp = sno1-1;
                                                    var cell = newrow.insertCell(0);
                                                    var cell0 = "<td><input type='hidden'  name='fuelItemId' /> "+snumber+"</td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(1);
                                                    cell0 = "<td class='text2'><select class='form-control' name='bunkName' style='width:170px'  id='bunkName"+sno+"'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(2);
                                                    cell0 = "<td class='text2'><input name='bunkPlace' type='text' class='form-control' id='bunkPlace' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(3);
                                                    cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate"+snumber+"' type='text' class='datepicker' style='width:80px;' id='fuelDate' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(4);
                                                    cell0 = "<td class='text1'><input name='fuelAmount' onBlur='sumFuel();' type='text' class='form-control' value='0' style='width:80px;' id='fuelAmount' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(5);
                                                    cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' type='text' class='form-control' id='fuelLtrs' value='0' style='width:80px;' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    /*cell = newrow.insertCell(6);
                       cell0 = "<td class='text1'><input name='fuelFilledBy' type='text' class='form-control' id='fuelFilledBy' class='Textbox' /></td>";
                       //cell.setAttribute(cssAttributeName,"text1");
                       cell.innerHTML = cell0;*/

                                                    cell = newrow.insertCell(6);
                                                    cell0 = "<td class='text1'><input name='fuelRemarks' type='text' class='form-control' id='fuelRemarks' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(7);
                                                    var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell1;
                                                    // rowCount++;


                                                    $( ".datepicker" ).datepicker({
                                                        /*altField: "#alternate",
                           altFormat: "DD, d MM, yy"*/
                                                        changeMonth: true,changeYear: true
                                                    });
                                                }
                                            }


                                            function delFuelRow() {
                                                try {
                                                    var table = document.getElementById("fuelTBL");
                                                    rowCount = table.rows.length-1;
                                                    for(var i=2; i<rowCount; i++) {
                                                        var row = table.rows[i];
                                                        var checkbox = row.cells[7].childNodes[0];
                                                        if(null != checkbox && true == checkbox.checked) {
                                                            if(rowCount <= 1) {
                                                                alert("Cannot delete all the rows");
                                                                break;
                                                            }
                                                            table.deleteRow(i);
                                                            rowCount--;
                                                            i--;
                                                            sno--;
                                                            // snumber--;
                                                        }
                                                    }sumFuel();
                                                }catch(e) {
                                                    alert(e);
                                                }
                                            }


                                            function sumFuel(){
                                                var totFuel=0;
                                                var totltr=0;
                                                var totAmount=0;
                                                var totAmt=0;
                                                totFuel= document.getElementsByName('fuelLtrs');

                                                totAmount= document.getElementsByName('fuelAmount');
                                                for(i=0;i<totFuel.length;i++){
                                                    totltr=parseInt(totltr)+parseInt(totFuel[i].value);
                                                    document.getElementById('totalFuelLtrs').value=parseInt(totltr);
                                                }
                                                for(i=0;i<totAmount.length;i++){
                                                    totAmt=parseInt(totAmt)+parseInt(totAmount[i].value);
                                                    document.getElementById('totalFuelAmount').value=parseInt(totAmt);
                                                }
                                            }
                                            function NumericOnly(){

                                            }

                                            function totalKmsFunc(){
                                                var kmDiff=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                                if(kmDiff<0){kmDiff=0;}
                                                document.getElementById('totalKms').value=kmDiff;
                                            }

                                        </script>
                                        <script type="text/javascript">
                                            var poItems = 0;
                                            var rowCount='0';
                                            var sno='0';
                                            var snumber = '0';

                                            function addExpensesRow()
                                            {
                                                if(sno < 9){
                                                    sno++;
                                                    var tab = document.getElementById("expensesTBL");
                                                    var rowCount = tab.rows.length;

                                                    snumber = parseInt(rowCount)-1;
                                                    //                    if(snumber == 1) {
                                                    //                        snumber = parseInt(rowCount);
                                                    //                    }else {
                                                    //                        snumber++;
                                                    //                    }

                                                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                    newrow.height="30px";
                                                    // var temp = sno1-1;
                                                    var cell = newrow.insertCell(0);
                                                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(1);
                                                    cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='form-control' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-Select-</option><option>Fuel</option><option>Bata</option><option>Toll</option><option>Spares</option><option>Consumables</option><option>Mechanic Charges</option><option>Mammul Loading Charges</option><option>Parking Charges</option><option>Others</option></select></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(2);
                                                    cell0 = "<td class='text2'><input name='tripExpensesDate' id='tripExpensesDate"+snumber+"' type='text' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(3);
                                                    cell0 = "<td class='text1'><input name='tripExpensesAmount' id='tripExpensesAmount"+snumber+"' type='text' class='form-control'  onkeyup='calculateAmount();' class='Textbox' /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(4);
                                                    cell0 = "<td class='text1'><input name='tripExpensesPaidBy' id='tripExpensesPaidBy"+snumber+"' type='text' class='form-control'  /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(5);
                                                    cell0 = "<td class='text1'><input name='tripExpensesRemarks'id='tripExpensesRemarks"+snumber+"' type='text' class='form-control'  /></td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell0;

                                                    cell = newrow.insertCell(6);
                                                    var cell1 = "<td><input type='checkbox' name='deleteItemExp' value='"+snumber+"'   /> </td>";
                                                    //cell.setAttribute(cssAttributeName,"text1");
                                                    cell.innerHTML = cell1;
                                                    // rowCount++;



                                                    $( ".datepicker" ).datepicker({
                                                        /*altField: "#alternate",
                                                                        altFormat: "DD, d MM, yy"*/
                                                        changeMonth: true,changeYear: true
                                                    });

                                                }
                                            }


                                            function delExpensesRow() {
                                                try {
                                                    var table = document.getElementById("expensesTBL");
                                                    rowCount = table.rows.length-1;
                                                    for(var i=2; i<rowCount; i++) {
                                                        var row = table.rows[i];
                                                        var checkbox = row.cells[6].childNodes[0];
                                                        if(null != checkbox && true == checkbox.checked) {
                                                            if(rowCount <= 1) {
                                                                alert("Cannot delete all the rows");
                                                                break;
                                                            }
                                                            table.deleteRow(i);
                                                            rowCount--;
                                                            i--;
                                                            sno--;
                                                            // snumber--;
                                                        }
                                                    }sumAllowanceAmt();
                                                }catch(e) {
                                                    alert(e);
                                                }
                                            }
                                            function returnTripDetail(){
                                                if(document.getElementById('returnTrip').checked == true){
                                                    document.getElementById("ReturnDetail").style.display="block";
                                                }else{
                                                    document.getElementById("ReturnDetail").style.display="none";
                                                }
                                            }

                                        </script>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="right" class="TableColumn">
                                        Total Liters
                                    </td>
                                    <td align="left" class="TableColumn">
                                        <input name="totalFuelLtrs" type="text" class="form-control"  value="<%=totalLitres%>" id="totalFuelLtrs" readonly  />
                                    </td>

                                    <td align="right" class="TableColumn">
                                        Total
                                    </td>
                                    <td align="left" class="TableColumn">
                                        <input name="totalFuelAmount" type="text" class="form-control" value="<%=totalFuelAmount%>" id="totalFuelAmount"  readonly  />
                                    </td>
                                    <td class="TableRowNew">
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td align="center"  colspan="5" rowspan="1">
                                        <h2>Trip Expenses (Fixed)</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th width="50" class="contenthead">S No&nbsp;</th><th class="contenthead">Expenses</th><th class="contenthead">Amount</th><th class="contenthead">Liters(if Fuel)</th><th class="contenthead">Date</th><th class="contenthead">Remarks</th><th class="contenthead">Mode</th>
                                                </tr>
                                                <%
                                                                            double expTotal = 0;
                                                                            int expSno = 0;
                                                                            TripFuelDetailsTO expTo = new TripFuelDetailsTO();
                                                                            ArrayList expAL = (ArrayList) request.getAttribute("etmExpDetails");
                                                                            if (expAL != null) {
                                                                                Iterator itExp = expAL.iterator();
                                                                                while (itExp.hasNext()) {
                                                                                    expSno++;
                                                                                    expTo = new TripFuelDetailsTO();
                                                                                    expTo = (TripFuelDetailsTO) itExp.next();
                                                                                    expTotal += Double.parseDouble(expTo.getAmount());
                                                %>
                                                <tr style="width:50px;">
                                                    <td width="50"><%=expSno%></td>
                                                    <td><%=expTo.getCategory()%></td>
                                                    <td>
                                                        <input type="hidden" name="expExpAmtExist" id="expExpAmtExist" value="<%=expTo.getAmount()%>" />
                                                        <input type="text" readonly name="expExpAmt" id="expExpAmt" onChange="calculateAmount();" value="<%=expTo.getAmount()%>" />
                                                        <input type="hidden" readonly name="expExpId" id="expExpId" value="<%=expTo.getTripExpenseId()%>" />
                                                    </td>
                                                    <td><%=expTo.getLiter()%></td>
                                                    <td><%=expTo.getTdate()%></td>
                                                    <td><%=expTo.getRemark()%></td>
                                                    <td><%=expTo.getMode()%></td>
                                                </tr>
                                                <%


                                                                                }
                                                                            }

                                                %>
                                                <tr style="width:50px;">
                                                    <td width="50">&nbsp;</td><td><input type="hidden" name="expEtmTotal" id="expEtmTotal" value="<%=expTotal%>" />&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                                </tr>
                                                <tr style="width:50px;">
                                                    <td width="50">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <%
                                            if("1".equals(operationTo.getOwnership())){
                                            %>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                                <tr align="center">
                                                    <td align="center"  colspan="6" rowspan="1">
                                                        <h2>Trip Expenses</h2>
                                                    </td>
                                                </tr>
                                                <script type="text/javascript">
                                                    var poItems = 0;
                                                    var rowCount='0';
                                                    var sno='0';
                                                    var snumber = '0';

                                                    function addExpensesRow()
                                                    {
                                                        if(sno < 9){
                                                            sno++;
                                                            var tab = document.getElementById("expensesTBL");
                                                            var rowCount = tab.rows.length;

                                                            snumber = parseInt(rowCount)-1;
                                                            //                    if(snumber == 1) {
                                                            //                        snumber = parseInt(rowCount);
                                                            //                    }else {
                                                            //                        snumber++;
                                                            //                    }

                                                            var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                            newrow.height="30px";
                                                            // var temp = sno1-1;
                                                            var cell = newrow.insertCell(0);
                                                            var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(1);
                                                            //cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='form-control' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-Select-</option><option value='Bata'>Bata</option><option value='Toll'>Toll</option><option value='Coolie'>Coolie</option><option value='Mamool'>Mamool Loading Charges</option><option value='Parking Charges'>Parking Charges</option><option value='Loading and Unloading'>Loading and Unloading</option><option value='Others'>Others</option></select></td>";
                                                            cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='form-control' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-select-</option><c:if test = "${expensesList != null}" ><c:forEach items="${expensesList}" var="elist"><option  value='<c:out value="${elist.expenseId}" />~<c:out value="${elist.expenseName}" />'><c:out value="${elist.expenseName}" /> </c:forEach ></c:if> </select></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(2);
                                                            cell0 = "<td class='text2'><input name='tripExpensesDate' id='tripExpensesDate"+snumber+"' type='text' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(3);
                                                            cell0 = "<td class='text1'><input name='tripExpensesAmount' id='tripExpensesAmount"+snumber+"' type='text' class='form-control' value='0'  onBlur='calculateAmount();' class='Textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(4);
                                                            cell0 = "<td class='text1'><input name='tripExpensesPaidBy' id='tripExpensesPaidBy"+snumber+"' type='text' class='form-control'  /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(5);
                                                            cell0 = "<td class='text1'><input name='tripExpensesRemarks'id='tripExpensesRemarks"+snumber+"' type='text' class='form-control'  /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(6);
                                                            var cell1 = "<td><input type='checkbox' name='deleteItemExp' value='"+snumber+"'   /> </td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell1;
                                                            // rowCount++;



                                                            $( ".datepicker" ).datepicker({
                                                                /*altField: "#alternate",
                                                                        altFormat: "DD, d MM, yy"*/
                                                                changeMonth: true,changeYear: true
                                                            });

                                                        }
                                                    }


                                                    function delExpensesRow() {
                                                        try {
                                                            var table = document.getElementById("expensesTBL");
                                                            rowCount = table.rows.length-1;
                                                            for(var i=2; i<rowCount; i++) {
                                                                var row = table.rows[i];
                                                                var checkbox = row.cells[6].childNodes[0];
                                                                if(null != checkbox && true == checkbox.checked) {
                                                                    if(rowCount <= 1) {
                                                                        alert("Cannot delete all the rows");
                                                                        break;
                                                                    }
                                                                    table.deleteRow(i);
                                                                    rowCount--;
                                                                    i--;
                                                                    sno--;
                                                                    // snumber--;
                                                                }
                                                            }sumAllowanceAmt();
                                                        }catch(e) {
                                                            alert(e);
                                                        }
                                                    }
                                                    function returnTripDetail(){
                                                        if(document.getElementById('returnTrip').checked == true){
                                                            document.getElementById("ReturnDetail").style.display="block";
                                                        }else{
                                                            document.getElementById("ReturnDetail").style.display="none";
                                                        }
                                                    }

                                                </script>
                                                <tr>
                                                    <td align="center" class="TableColumn" colspan="11" rowspan="1" style="height: 0%">
                                                        <div>
                                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="expensesTBL" name="expensesTBL">
                                                                <tr>
                                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                                    <th class="contenthead">Expenses</th>
                                                                    <th class="contenthead">Date</th>
                                                                    <th class="contenthead">Amount</th>
                                                                    <th class="contenthead">Paid Person</th>
                                                                    <th class="contenthead">Remarks</th>
                                                                    <th class="contenthead">&nbsp;</th>
                                                                </tr>
<!--                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>

                                                                        <select name="stageIdExp" id="stageIdExp<%=snoAllo%>" class="form-control"  style="width:150px;">
                                                                            <option value=""> -select- </option>
                                                                            <c:if test = "${expensesList != null}" >
                                                                            <c:forEach items="${expensesList}" var="elist">
                                                                            <option value='<c:out value="${elist.expenseId}" />~<c:out value="${elist.expenseName}" />'><c:out value="${elist.expenseName}" /></option>
                                                                            </c:forEach >
                                                                            </c:if>
                                                                        </select>

                                                                    </td>
                                                                    <td>
                                                                        <input name="tripExpensesDate" type="text" class="datepicker"  id="tripExpensesDate"   />
                                                                    </td>

                                                                    <td>
                                                                        <input name="tripExpensesAmount" type="text" class="form-control" id="tripExpensesAmount<%=snoAllo%>" onBlur="calculateAmount();" value="0" OnKeyPress="NumericOnly()"  />
                                                                    </td>


                                                                    <td>
                                                                        <input name="tripExpensesPaidBy" type="text" class="form-control" id="tripExpensesPaidBy" onkeyup="PaidPerson(this);" />
                                                                    </td>
                                                                    <td>
                                                                        <input name="tripExpensesRemarks" type="text" class="form-control" id="tripExpensesRemarks"   />
                                                                        <input type="hidden" name="tripExpensesId" id="tripExpenses" />
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>-->
<!--                                                                <tr>
                                                                    <td colspan="6" align="center">
                                                                        <input type="button" name="add" value="Add" onclick="addExpensesRow()" id="add" class="button" />
                                                                        &nbsp;&nbsp;&nbsp;

                                                                        <input type="button" name="delete" value="Delete" onclick="delExpensesRow()" id="delete" class="button" />

                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>-->
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                           <%
                                            }
                                            %>
                                            <table>
                                                <tr align="left">


                                                    <td align="right" class="TableColumn">
                                                        Total Advances
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="nettAllowance" type="text" class="form-control" value="<%=totalAllowanceGiven%>" id="nettAllowance" readonly  />
                                                    </td>
                                                    <td align="right" class="TableColumn">
                                                        Total Expenses
                                                    </td>

                                                    <td align="left" class="TableColumn">

                                                        <input name="totalExpenses" type="text" class="form-control" value="<%=expTotal %>" id="totalExpenses" readonly  />
                                                        <input name="existExpenses"  id="existExpenses" type="hidden" class="form-control" value="<%=expTotal %>" readonly  />

                                                    </td>
                                                </tr>

                                                <tr id="TrBalance" align="left">

                                                    <td align="right" class="TableColumn">
                                                        Total Kms
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="totalKms" type="text" class="form-control" value="<%=operationTo.getTripTotalKms()%>" id="totalKms" readonly  />
                                                    </td>


                                                    <td align="right" class="TableColumn">

                                                        Fuel Liters

                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="totFuelLtrs" type="text" class="form-control"  value="<%=totalLitres%>" id="totFuelLtrs" readonly  />
                                                    </td>

                                                </tr>
                                                <tr id="TrBalance" align="left">

                                                    <td align="right" class="TableColumn">
                                                        Mileage (kmpl)
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="totMileage" type="text" class="form-control"  value="0" id="totMileage" readonly  />
                                                    </td>


                                                    <td align="right" class="TableColumn">

                                                        Balance Amount

                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="balanceAmount" type="text" class="form-control" value="<%=totalAllowanceGiven - (expTotal)%>" id="balanceAmount" readonly  />
                                                    </td>

                                                </tr>
                                                <tr id="TrBalance" align="left">

                                                    <td align="right" class="TableColumn">
                                                        To Pay Amount to be Collected (if any)
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="totalToPayAmt" type="text" class="form-control"  value="0" id="totalToPayAmt" readonly  />
                                                    </td>


                                                    <td align="right" class="TableColumn">

                                                        Settlement Amount

                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="settlementAmount" type="text" class="form-control" value="<%=totalAllowanceGiven - (expTotal )%>" id="settlementAmount" readonly  />
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            

                                <tr>
                                    <td style="width: 189px; height: 19px">
                                    </td>
                                    <td align="center" colspan="2">
                                    </td>
                                    <td align="center" style="height: 19px; width: 76px;">
                                    </td>
                                    <td align="center" style="width: 76px; height: 19px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                    <%
                    if("1".equals(operationTo.getOwnership())){
                    %>
                    <table cellpadding="0" cellspacing="4" border="0" width="60%">
                    <tr>

                        <td align="left" class="TableColumn">
                            <input type="checkbox" name="returnTrip" id="returnTrip" value="" onclick="returnTripDetail();"><b> Return Movements </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="ReturnDetail" style="display: none;">
                                <table cellpadding="0" cellspacing="0" style="width: 55%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                    <tr align="center">
                                        <td align="center"  colspan="6" rowspan="1">
                                            <h2>Return Movements</h2>
                                        </td>
                                    </tr>
                                    <script type="text/javascript">
                                        var poItems = 0;
                                        var rowCount='0';
                                        var sno='0';
                                        var snumber = '0';

                                        function addReturnRow()
                                        {
                                            if(sno < 9){
                                                sno++;
                                                var tab = document.getElementById("returnMON");
                                                var rowCount = tab.rows.length;

                                                snumber = parseInt(rowCount)-1;
                                                //                    if(snumber == 1) {
                                                //                        snumber = parseInt(rowCount);
                                                //                    }else {
                                                //                        snumber++;
                                                //                    }

                                                var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                newrow.height="30px";
                                                // var temp = sno1-1;
                                                //alert(snumber);


                                                var cell = newrow.insertCell(0);
                                                var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(1);
                                                cell0 = "<td class='text1'><select name='returnTripType' id='returnTripType"+snumber+"'  onchange='emptyField(this.value);' class='form-control'><option value='loaded'>Loaded Trip</option><option value='empty'>Empty Trip </option></select></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(2);
                                                cell0 = "<td class='text1'><input name='customer' id='customer"+snumber+"' onFocus='getCustomers("+snumber+");' type='text' class='form-control' autocomplete='off' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;


                                                cell = newrow.insertCell(3);
                                                cell0 = "<td class='text1'><input name='fromLocation' id='fromLocation"+snumber+"' onFocus='getFromLocation("+snumber+");' type='text' class='form-control' autocomplete='off' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(4);
                                                cell0 = "<td class='text2'><input name='toLocation' id='toLocation"+snumber+"' type='text' onFocus='getToLocation("+snumber+");' class='form-control' autocomplete='off' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;



                                                cell = newrow.insertCell(5);
                                                cell0 = "<td class='text1'><input name='returnProductName' value='' id='returnProductName"+snumber+"' type='text' class='form-control' onFocus='getProducts("+snumber+");'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(6);
                                                cell0 = "<td class='text2'><input name='returnLoadedDate' value=''  id='returnLoadedDate"+snumber+"' type='text' class='datepicker' id='returnLoadedDate' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(7);
                                                cell0 = "<td class='text2'><input name='inKM' id='inKM"+snumber+"' type='hidden' class='form-control'  class='Textbox' /><input name='outKM' id='outKM"+snumber+"' type='hidden' class='form-control'  class='Textbox' /><input name='returnDeliveredDate' id='returnDeliveredDate"+snumber+"' type='text' class='datepicker' id='returnDeliveredDate' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                /*
                                                cell = newrow.insertCell(8);
                                                cell0 = "<td class='text1'><input name='outKM' id='outKM"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(9);
                                                cell0 = "<td class='text1'><input name='inKM' id='inKM"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                */
                                                cell = newrow.insertCell(8);
                                                cell0 = "<td class='text1'><input name='returnKM' id='returnKM"+snumber+"'  value='' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                //                                                cell = newrow.insertCell(11);
                                                //                                                cell0 = "<td class='text1'><input name='returnTon' id='returnTon"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //                                                //cell.setAttribute(cssAttributeName,"text1");
                                                //                                                cell.innerHTML = cell0;
                                                //deliveredDANo loadedSlipNo shortageTon deliveredTon loadedTon
                                                cell = newrow.insertCell(9);
                                                cell0 = "<td class='text1'><input name='loadedTon' value=''  id='loadedTon"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                cell = newrow.insertCell(10);
                                                cell0 = "<td class='text1'><input name='deliveredTon' value=''  id='deliveredTon"+snumber+"' onblur='setRevenue("+snumber+");' type='text' class='form-control'/></td>";

                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                cell = newrow.insertCell(11);
                                                cell0 = "<td class='text1'><input name='shortageTon' value=''  id='shortageTon"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                cell = newrow.insertCell(12);
                                                cell0 = "<td class='text1'><input name='loadedSlipNo' value=''  id='loadedSlipNo"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                cell = newrow.insertCell(13);
                                                cell0 = "<td class='text1'><input name='deliveredDANo' value=''  id='deliveredDANo"+snumber+"' type='text' class='form-control'  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(14);
                                                cell0 = "<td class='text1'><input name='returnAmount' value=''  id='returnAmount"+snumber+"' type='text' value='0' readonly  class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(15);
                                                var cell1 = "<td id='row"+snumber+"'><input type='buton' name='savereturnmovement' value='save' class='button' onClick='saveReturnMovment("+snumber+");' /> </td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell1;
                                                cell = newrow.insertCell(16);
                                                var cell1 = "<td><input type='checkbox' name='deleteItemExp' value='"+snumber+"'   /> </td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell1;
                                                // rowCount++;



                                                $( ".datepicker" ).datepicker({
                                                    /*altField: "#alternate",
                                                            altFormat: "DD, d MM, yy"*/
                                                    changeMonth: true,changeYear: true
                                                });

                                            }
                                        }


                                        function delReturnRow() {
                                            try {
                                                var table = document.getElementById("returnMON");
                                                rowCount = table.rows.length-1;
                                                for(var i=2; i<rowCount; i++) {
                                                    var row = table.rows[i];
                                                    var checkbox = row.cells[16].childNodes[0];
                                                    if(null != checkbox && true == checkbox.checked) {
                                                        if(rowCount <= 1) {
                                                            alert("Cannot delete all the rows");
                                                            break;
                                                        }
                                                        table.deleteRow(i);
                                                        rowCount--;
                                                        i--;
                                                        sno--;
                                                        // snumber--;
                                                    }
                                                }sumAllowanceAmt();
                                            }catch(e) {
                                                alert(e);
                                            }
                                        }


                                        function emptyField(tripType){
                                            //alert(tripType);
                                            if(tripType == 'empty'){
                                                document.tripSheet.customer.disabled=true;
                                                document.tripSheet.returnProductName.disabled=true;
                                                document.tripSheet.returnLoadedDate.disabled=true;
                                                document.tripSheet.returnDeliveredDate.disabled=true;
                                                document.tripSheet.loadedTon.disabled=true;
                                                document.tripSheet.deliveredTon.disabled=true;
                                                document.tripSheet.shortageTon.disabled=true;
                                                document.tripSheet.loadedSlipNo.disabled=true;
                                                document.tripSheet.deliveredDANo.disabled=true;

                                            }else{
                                                document.tripSheet.customer.disabled=false;
                                                document.tripSheet.returnProductName.disabled=false;
                                                document.tripSheet.returnLoadedDate.disabled=false;
                                                document.tripSheet.returnDeliveredDate.disabled=false;
                                                document.tripSheet.loadedTon.disabled=false;
                                                document.tripSheet.deliveredTon.disabled=false;
                                                document.tripSheet.shortageTon.disabled=false;
                                                document.tripSheet.loadedSlipNo.disabled=false;
                                                document.tripSheet.deliveredDANo.disabled=false;

                                            }
                                        }
                                    </script>
                                    <tr>
                                        <td align="center" class="TableColumn" colspan="11" rowspan="1" style="height: 0%">
                                            <div>
                                                <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="returnMON" name="returnMON">
                                                    <tr>
                                                        <th width="50" class="contenthead">S No&nbsp;</th>
                                                        <th class="contenthead">Return Trip Type</th>
                                                        <th class="contenthead">Customer</th>
                                                        <th class="contenthead">From Loc</th>
                                                        <th class="contenthead">To Loc</th>
                                                        <th class="contenthead">Product</th>
                                                        <th class="contenthead">Movements Start Date</th>
                                                        <th class="contenthead">Movements End Date</th>
<!--                                                        <th class="contenthead">OutKm</th>
                                                        <th class="contenthead">InKm</th>-->
                                                        <th class="contenthead">Distance(KM)</th>
                                                        <th class="contenthead">Loaded Ton</th>
                                                        <th class="contenthead">Delivered Ton</th>
                                                        <th class="contenthead">Shortage Ton</th>
                                                        <th class="contenthead">Loaded Slip No</th>
                                                        <th class="contenthead">Delivered DA No</th>
                                                        <th class="contenthead">Revenue</th>
                                                    </tr>
                                                    <% int cntr = 1;%>
                                                    <c:if test = "${returnTripList != null}" >
                                                        <script type="text/javascript">
                                                            document.getElementById('returnTrip').checked = true;
                                                            document.getElementById("ReturnDetail").style.display="block";

                                                        </script>

                                                        <c:forEach items="${returnTripList}" var="returnTrip">

                                                            <tr>
                                                                <td><%=cntr%></td>
                                                                <td class="texttitle1">
                                                                    <c:out value="${returnTrip.returnTripType}" />
                                                                </td>
                                                                <td class="texttitle1">
                                                                    <c:out value="${returnTrip.customerId}" />
                                                                </td>
                                                                <td class="texttitle1">
                                                                    <c:out value="${returnTrip.returnFromLocation}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.returnToLocation}" />
                                                                </td>

                                                                <td>
                                                                    <c:out value="${returnTrip.returnProductName}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.openDateTime}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.closedDate}" />
                                                                </td>
<!--                                                                <td>
                                                                    <c:out value="${returnTrip.outKM}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.inKM}" />
                                                                </td>-->
                                                                <td>
                                                                    <c:out value="${returnTrip.returnKM}" />
                                                                </td>



                                                                <td>
                                                                    <c:out value="${returnTrip.loadedTonnage}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.deliveredTonnage}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.shortageTonnage}" />
                                                                </td>
                                                                
                                                                
                                                                <td>
                                                                    <c:out value="${returnTrip.deliveredDANo}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.loadedSlipNo}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.returnAmount}" />
                                                                </td>
                                                                <td >

                                                                    <a href='#' onClick='newWO(<c:out value="${returnTrip.returnTripId}" />);'>view</a>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <% cntr++;%>
                                                        </c:forEach >
                                                    </c:if>

<!--                                                    <tr>
                                                        <td><%=cntr%></td>
                                                        <td class="texttitle1">
                                                            <select name="returnTripType" class="form-control"  id="returnTripType<%=cntr%>" onchange="emptyField(this.value);">
                                                                <option value="loaded">Loaded Trip</option>
                                                                <option value="empty">Empty Trip</option>
                                                            </select>
                                                        </td>
                                                        <td class="texttitle1">
                                                            <input name="customer" id="customer<%=cntr%>" onFocus='getCustomers(<%=cntr%>);' type="text" class="form-control" autocomplete="off"  />

                                                        </td>

                                                        <td class="texttitle1">
                                                            <input name="fromLocation" id="fromLocation<%=cntr%>" onFocus='getFromLocation(<%=cntr%>);' type="text" class="form-control"  autocomplete="off" />
                                                        </td>

                                                        <td>
                                                            <input name="toLocation" type="text" class="form-control"  id="toLocation<%=cntr%>" onFocus='getToLocation(<%=cntr%>);'  autocomplete="off" />
                                                        </td>

                                                        <td>
                                                            <input name="returnProductName" type="text"  value='' class="form-control" onFocus='getProducts(<%=cntr%>);' id="returnProductName<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="returnLoadedDate" type="text" class="datepicker"  id="returnLoadedDate<%=cntr%>"   />
                                                        </td>
                                                        <td>
                                                            <input name="returnDeliveredDate" type="text" value=''  class="datepicker"  id="returnDeliveredDate<%=cntr%>"   />
                                                        </td>
                                                        <input name="inKM" type="hidden" class="form-control" id="inKM<%=cntr%>" />
                                                        <input name="outKM" type="hidden" class="form-control" id="outKM<%=cntr%>" />
                                                        <td>

                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            <input name="returnKM" type="text" value=''  class="form-control" id="returnKM<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="returnTon" type="text" class="form-control" id="returnTon<%=cntr%>" />
                                                        </td>
                                                        
                                                        <td>
                                                            <input name="loadedTon" type="text"  value='' class="form-control" id="loadedTon<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="deliveredTon" type="text"  value='' class="form-control" id="deliveredTon<%=cntr%>" onblur='setRevenue(<%=cntr%>);' />
                                                        </td>
                                                        <td>
                                                            <input name="shortageTon" type="text" value=''  class="form-control" id="shortageTon<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="loadedSlipNo" type="text" value=''  class="form-control" id="loadedSlipNo<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="deliveredDANo" type="text" value=''  class="form-control" id="deliveredDANo<%=cntr%>" />
                                                        </td>
                                                        <td>
                                                            <input name="returnAmount" type="text"  value='' class="form-control" id="returnAmount<%=cntr%>" value="0"  readonly />
                                                        </td>

                                                        <td id='row<%=cntr%>'><input type='buton' name='saveretmvt' value='save' class="button" onClick='saveReturnMovment(<%=cntr%>);' /></td>
                                                        <td>&nbsp;</td>
                                                    </tr>-->
<!--                                                    <tr>
                                                        <td colspan="18" align="center">
                                                            <input type="button" name="add" value="Add" onclick="addReturnRow()" id="add" class="button" />
                                                            &nbsp;&nbsp;&nbsp;

                                                            <input type="button" name="delete" value="Delete" onclick="delReturnRow()" id="delete" class="button" />

                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>-->
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr align="center">


                                        <td align="right" class="TableColumn">
                                            Return Advances
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="returnTotalAllowance" type="text" class="form-control" value='<%=request.getAttribute("returnAdvanceTotal")%>' id="returnTotalAllowance" readonly  />
                                        </td>
                                         <td align="right" class="TableColumn">
                                            Return Expenses
                                        </td>

                                        <td align="left" class="TableColumn">

                                            <input name="returnTotalExpense" type="text" class="form-control" value='<%=request.getAttribute("returnExpenseTotal")%>' id="returnTotalExpense" readonly  />

                                        </td>

                                        <td class="TableRowNew">
                                        </td>
                                        <td class="TableRowNew">
                                        </td>
                                    </tr>
                                    <tr id="TrTotalDetails" align="center">

                                        <td align="right" class="TableColumn">
                                            Return KMs
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="returnTotalKm" type="text" class="form-control" value='<%=request.getAttribute("returnKMTotal")%>' id="returnTotalKm" readonly  />
                                        </td>


                                        <td align="right" class="TableColumn">
                                            Return Fuel Ltrs
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="returnTotalFuelLtrs" type="text" class="form-control" value='<%=request.getAttribute("returnFuelLtrsTotal")%>' id="returnTotalFuelLtrs" readonly  />
                                        </td>

                                        <td class="TableRowNew">
                                        </td>

                                        <td class="TableRowNew">
                                            <input type="hidden" name="hfETMTotal" id="hfETMTotal" value="0" />
                                        </td>

                                    </tr>
                                    <tr id="TrBalance" align="center">
                                        <td class="TableRowNew">

                                        </td>
                                        <td class="TableRowNew">

                                        </td>


                                        <td align="right" class="TableColumn">

                                            Return Balance

                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="returnBalanceAmount" type="text" class="form-control" value='0' id="returnBalanceAmount" readonly  />
                                        </td>

                                    </tr>
                                    <tr  align="center">
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                    </tr>
                                    <tr  align="center">
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                        <td class="TableRowNew">&nbsp;
                                        </td>
                                    </tr>

                                    <tr align="center">


                                        <td align="right" class="TableColumn">
                                            NETT Advances
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="nettTotalAllowance" type="text" class="form-control" value='<%=request.getAttribute("returnAdvanceTotal")%>' id="nettTotalAllowance" readonly  />
                                        </td>
                                        <td align="right" class="TableColumn">
                                            NETT Expenses
                                        </td>

                                        <td align="left" class="TableColumn">

                                            <input name="nettExpenses" type="text" class="form-control" value='<%=request.getAttribute("returnExpenseTotal")%>' id="nettExpenses" readonly  />

                                        </td>

                                        <td class="TableRowNew">
                                        </td>
                                        <td class="TableRowNew">
                                        </td>
                                    </tr>
                                    <tr id="TrTotalDetails" align="center">

                                        <td align="right" class="TableColumn">
                                            NETT Kms
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="nettKm" type="text" class="form-control" value='<%=request.getAttribute("returnKMTotal")%>' id="nettKm" readonly  />
                                        </td>


                                        <td align="right" class="TableColumn">
                                            NETT Fuel Ltrs
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <%
                                                totalLitres = totalLitres + Float.parseFloat((String)request.getAttribute("returnFuelLtrsTotal"));
                                            %>
                                            <input name="nettFuelLtrs" type="text" class="form-control" value='<%=totalLitres %>' id="nettFuelLtrs" readonly  />
                                        </td>

                                        <td class="TableRowNew">
                                        </td>

                                        <td class="TableRowNew">
                                            <input type="hidden" name="hfETMTotal" id="hfETMTotal" value="0" />
                                        </td>

                                    </tr>
                                    <tr id="TrBalance" align="center">

                                        <td align="right" class="TableColumn">
                                            NETT Mileage
                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="nettMileage" type="text" class="form-control" value='0' id="nettMileage" readonly  />
                                        </td>

                                        <td align="right" class="TableColumn">

                                            NETT Balance Amount

                                        </td>

                                        <td align="left" class="TableColumn">
                                            <input name="nettBalanceAmount" type="text" class="form-control" value='0' id="nettBalanceAmount" readonly  />
                                        </td>


                                        <td class="TableRowNew">

                                        </td>
                                        <td class="TableRowNew">

                                        </td>

                                    </tr>
                                    <tr id="TrBalance" align="left">

                                                    <td align="right" class="TableColumn">
                                                        To Pay Amount to be Collected (if any)
                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="nettToPayAmt" type="text" class="form-control"  value="0" id="nettToPayAmt" readonly  />
                                                    </td>


                                                    <td align="right" class="TableColumn">

                                                        Nett Settlement Amount

                                                    </td>

                                                    <td align="left" class="TableColumn">
                                                        <input name="nettSettlementAmount" type="text" class="form-control" value="0" id="nettSettlementAmount" readonly  />
                                                    </td>

                                                </tr>
                                </table>
                            </div>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>
                                &nbsp;

                        </td>
                    </tr>
                </table>

            </div>
            <%
                        }else{
            %>
            <input name="returnTotalAllowance" type="hidden" class="form-control" value='0' id="returnTotalAllowance" readonly  />
            <input name="returnTotalExpense" type="hidden" class="form-control" value='0' id="returnTotalExpense" readonly  />
            <input name="returnTotalKm" type="hidden" class="form-control" value='0' id="returnTotalKm" readonly  />
            <input name="returnTotalFuelLtrs" type="hidden" class="form-control" value='0' id="returnTotalFuelLtrs" readonly  />
            <input name="returnBalanceAmount" type="hidden" class="form-control" value='0' id="returnBalanceAmount" readonly  />
            <input name="nettTotalAllowance" type="hidden" class="form-control" value='0' id="nettTotalAllowance" readonly  />
            <input name="nettExpenses" type="hidden" class="form-control" value='0' id="nettExpenses" readonly  />
            <input name="nettKm" type="hidden" class="form-control" value='0' id="nettKm" readonly  />
            <input name="nettFuelLtrs" type="hidden" class="form-control" value='0' id="nettFuelLtrs" readonly  />
            <input name="nettMileage" type="hidden" class="form-control" value='0' id="nettMileage" readonly  />
            <input name="nettBalanceAmount" type="hidden" class="form-control" value='0' id="nettBalanceAmount" readonly  />
            <input name="nettToPayAmt" type="hidden" class="form-control"  value="0" id="nettToPayAmt" readonly  />
            <input name="nettSettlementAmount" type="hidden" class="form-control" value="0" id="nettSettlementAmount" readonly  />

            <%
                        }
            %>
            
            

            <%
                        }
            %>
            <script type="text/javascript">
                calculateAmount();
                show();
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
