<%--
    Document   : addVehicle
    Created on : Feb 27, 2009, 6:46:00 PM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>


        <script language="javascript">


            function CalWarrantyDate(selDate){

                var seleDate = selDate.split('-');
                var dd = seleDate[0];
                var mm = seleDate[1];
                var yyyy = seleDate[2];

                var today = new Date();
                var dd1 = today.getDate();
                var mm1 = today.getMonth()+1; //January is 0!
                var yyyy1 = today.getFullYear();

                //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

                var selecedDate = new Date (yyyy, mm, dd);
                var currentDate = new Date (yyyy1, mm1, dd1);
                var Days = Math.floor((currentDate.getTime() - selecedDate.getTime())/(1000*60*60*24));
                // alert("DAYS--"+Days);
                document.addVehicle.war_period.value= Days;

            }

            function submitPage(value){
                if(validate()=='fail'){
                    return;
                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
                    return;
                }else if(numberValidation(document.addVehicle.war_period,'Warranty Period')){
                    return;
                }else if(isSelect(document.addVehicle.mfrId,'Manufacturer')){
                    return;
//                }else if(isSelect(document.addVehicle.classId,'Vehicle Class')){
//                    return;
                }else if(isSelect(document.addVehicle.modelId,'Vehicle Model')){
                    return;
                }else if( document.addVehicle.dateOfSale.value==''  ){
                    alert("Please select Date");
                    return;
                }else if(isSelect(document.addVehicle.opId,'Operation Point')){
                    return;
//                }else if(isSelect(document.addVehicle.groupId,'Group')){
//                    return;
                }else if(numberValidation(document.addVehicle.kmReading,'Km Reading')){
                    return;
//                }else if(numberValidation(document.addVehicle.hmReading,'Hm reading')){
//                    return;
                }else if(numberValidation(document.addVehicle.dailyKm,'Daily Km Reading')){
                    return;
//               
                }else if(textValidation(document.addVehicle.regNo,'Registration Number')){
                    return;
                }else if(textValidation(document.addVehicle.engineNo,'Engine Number')) {
                    return;
                }else if(textValidation(document.addVehicle.chassisNo,'Chassis Number')) {
                    return;
                }/*else if(textValidation(document.addVehicle.nextFCDate,'Next FC Date')) {
                    return;
                }*/else if(textValidation(document.addVehicle.seatCapacity,'Chassis Number')) {
                    return;
                }else if(textValidation(document.addVehicle.description,'Remarks')) {
                    return;
                }if(value == "save"){
                    //alert("1a");
                    if(document.addVehicle.fservice.checked==true ){
                    //alert("1b");
                        document.addVehicle.fservice.value='Y';
                    }else{
                    //alert("1c");
                        document.addVehicle.fservice.value='N';
                    }
                    //alert("1");
                    document.addVehicle.nextFCDate.value = "00-00-0000";
                    //alert("2");
                    document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
                    //alert("3");
                    document.addVehicle.action = '/throttle/alterVehicle.do';
                    document.addVehicle.submit();
                }
            }


            function submitPage1(){
                document.addVehicle.action  = '/throttle/searchVehiclePage.do';
                document.addVehicle.submit();
            }


        </script>

        <script>
            var rowCount=0;
            var sno=0;

            var httpReq;
            var styl = "";

            function addRow()
            {
                rowCount= parseInt(document.getElementsByName("itemIds").length)+1 ;
                sno++;
                if( parseInt(rowCount) >= 20 ){
                    alert("Tyre Numbers Should Not Exceeds Maximum Limit");
                    return;
                }
                if(parseInt(rowCount) %2==0)
                {
                    styl="text1";
                }else{
                    styl="text2";
                }
                var tab = document.getElementById("addTyres");
                var newrow = tab.insertRow(rowCount);

                var cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='30' width='50'  > "+(rowCount)+"</td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;


                // Positions
                cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='30'> <select class='form-control' name='itemIds'  ><option  selected value='0'>---Select---</option>"+
                    "<c:if test = "${positionList != null}" ><c:forEach items="${tyreItemList}" var="sec"><option  value='<c:out value="${sec.itemId}" />'><c:out value="${sec.itemName}" /></c:forEach ></c:if>"+
                    "</select></td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;

                // Positions
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='30'> <select class='form-control' name='posIds'  style='width:150px;'><option  selected value='0'>---Select---</option>"+
                    "<c:if test = "${positionList != null}" ><c:forEach items="${positionList}" var="sec"><option  value='<c:out value="${sec.posId}" />'><c:out value="${sec.posName}" /></c:forEach ></c:if>"+
                    "</select></td>";
                cell.setAttribute("className",styl);
                cell.innerHTML = cell0;



                cell = newrow.insertCell(3);
                var cell1 =  "<td class='text1'  width='50'  height='30' > <input type='hidden' name='tyreIds' value='0' >"+
                    "<input type='text' name='tyreNos' onChange='checkTyreId("+rowCount+")' class='form-control'  > "
                cell.setAttribute("className",styl);
                cell.innerHTML = cell1;

                cell = newrow.insertCell(4);
                var cell2 =  "<td class='text1'  width='50'  height='30' > <input type='text' name='tyreDate' class='datepicker'  > "
                cell.setAttribute("className",styl);
                cell.innerHTML = cell2;

                rowCount++;

                $(document).ready(function() {
                    //alert('hai');
                    $( "#datepicker" ).datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });



                });

                $(function() {
                    //	alert("cv");
                    $( ".datepicker" ).datepicker({

                        /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });

                });

            }

        </script>



        <script>



            function setTyreOptions(text,variab){
                variab.options.length=0;
                option0 = new Option("--select--",'0');
                variab.options[0] = option0;

                if(text != ""){
                    var splt = text.split('^');
                    var temp1;
                    variab.options[0] = option0;
                    for(var i=0;i<splt.length;i++){
                        temp1 = splt[i].split('~');
                        option0 = new Option(temp1[1],temp1[0])
                        variab.options[i+1] = option0;
                    }
                }
            }

            function validate()
            {
                var tyreIds = document.getElementsByName("tyreIds");
                var positionIds = document.getElementsByName("posIds");
                var tyreDate = document.getElementsByName("tyreDate");
                var itemIds = document.getElementsByName("itemIds");
                var tyreNos = document.getElementsByName("tyreNos");
                var cntr = 0;

                for(var i=0;i<tyreNos.length;i++){
                    for(var j=0;j<tyreNos.length;j++){
                        if( (tyreNos[i].value == tyreNos[j].value) && (tyreNos[i].value==tyreNos[j].value  ) && (i != j) && (tyreNos[i].value!='' )   ){
                            cntr++;
                        }
                    }
                    if( parseInt(cntr) > 0){
                        alert("Tyre Nos Should not be repeated");
                        return "fail";
                        break;
                    }
                }

                for(var i=0;i<positionIds.length;i++){
                    if(itemIds[i].value != '0'){
                        for(var j=0;j<positionIds.length;j++){
                            if( (positionIds[i].value == positionIds[j].value) && (positionIds[i].value==positionIds[j].value  ) && (i != j) && (positionIds[i].value!='0' )  ){
                                cntr++;
                            }
                        }
                        if( parseInt(cntr) > 0){
                            alert("Tyre Positions should not be repeated");
                            return "fail";
                            break;
                        }
                    }
                }

                for(var i=0;i<tyreIds.length;i++){
                    if(itemIds[i].value != '0'){
                        if(positionIds[i].value == '0'){
                            alert('Please Select Position');
                            positionIds[i].focus();
                            return "fail";
                        }else if(tyreNos[i].value == ''){
                            alert('Please Enter Tyre Number');
                            tyreIds[i].focus();
                            return "fail";
                        }
                    }
                }
                return "pass";
            }

            var httpReq;
                var temp = "";
                function getModelAndTonnage(str){ //alert(str);
                    $.ajax({
                                       url: "/throttle/getModels1.do",
                                       dataType: "text",
                                       data: {
                                          typeId:document.addVehicle.typeId.value,
                                           mfrId:document.addVehicle.mfrId.value
                                       },
                                       success: function(temp) {
                                          // alert(data);
                                           if (temp != '') {

                                       setOptions1(temp,document.addVehicle.modelId);
                                           }else{
                                               setOptions1(temp,document.addVehicle.modelId);
                                       alert('There is no model based on Vehicle Type Please add first');
                                           }
                                       }
                                   });
                             // for tonnage now

                      $.ajax({
                                       url: "/throttle/getTonnage.do",
                                       dataType: "text",
                                       data: {
                                          typeId:document.addVehicle.typeId.value
                                          },
                                       success: function(temp) {
//                                           alert(data);
                                           if (temp != '') {

                                       setOptions2(temp,document.addVehicle.seatCapacity);
                                           }else{
                                       alert('Tonnage is not there please set first in vehicle type');
                                           }
                                       }
                                   });
               }
                                   function setOptions1(text,variab) {
                                  //alert("setOptions on page")
                               // alert("text = "+text+ "variab = "+variab)
                                        variab.options.length = 0;
                                        //                                alert("1")
                                        var option0 = new Option("--select--", '0');
                                        //                                alert("2")
                                        variab.options[0] = option0;
                                        //                                alert("3")


                                        if (text != "") {
                                            //                                    alert("inside the condition")
                                            var splt = text.split('~');
                                            var temp1;
                                            variab.options[0] = option0;
                                            for (var i = 0; i < splt.length; i++) {
                                                //                                    alert("splt.length ="+splt.length)
                                                //                                    alert("for loop ="+splt[i])
                                                temp1 = splt[i].split('-');
                                                option0 = new Option(temp1[1], temp1[0])
                                                //alert("option0 ="+option0)
                                                variab.options[i + 1] = option0;
                                            }
                                        }
                                    }

                                function setOptions2(text) {
//                                alert("setOptions on page")
//                               alert("text = "+text)
                                        if (text != "") {
//                                        alert(text);
                                        document.getElementById('seatCapacity').value=text;

                                    }
                                    }


            // raja

            var httpRequest;
            function checkTyreId(val)
            {
                var tyreNo = document.getElementsByName("tyreNos");
                if(tyreNo[val].value != ''){
                    var url = '/throttle/checkVehicleTyreNo.do?tyreNo='+tyreNo[val].value;
                    if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(tyreNo[val].value); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest(tyreNo)
            {
                if (httpRequest.readyState == 4)
                {
                    if(httpRequest.status == 200)
                    {
                        if(httpRequest.responseText.valueOf()!=""){
                            document.getElementById("userNameStatus").innerHTML="Tyre No "+tyreNo+" Already Exists in "+httpRequest.responseText.valueOf();

                        }else {
                            document.getElementById("userNameStatus").innerHTML="";
                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }






        </script>



    </head>
    <!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
        <![endif]-->
    <body onLoad="" >
        <form name="addVehicle" method="post"  >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>


            <br>
            <c:if test="${vehicleDetail != null}" >

                <table  border="0" class="border" align="center" width="80%" cellpadding="0" cellspacing="0" id="bg">
                    <c:forEach items="${vehicleDetail}" var="veh" >
                        <tr>
                            <td colspan="4" height="30" class="contenthead"><div class="contenthead">Alter Vehicle</div></td>
                        </tr>

                        <tr>
                            <td class="text2" height="30">Vehicle Number</td>
                            <td class="text2" height="30"><input name="regNo" maxlength='14' class='form-control' type="text"  value='<c:out value="${veh.regNo}" />' ></td>
                            <td class="text2" height="30">Registration Date</td>
                            <td class="text2" height="30"><input name="dateOfSale" type="text" class="datepicker" value='<c:out value="${veh.dateOfSale}" />' size="20" ><!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVehicle.dateOfSale,'dd-mm-yyyy',this)"/>-->
                            </td>
                        </tr>
                        <tr>
                             <input type="hidden" name="vehicleId" value="<c:out value="${veh.vehicleId}" />" >
                        <td class="text1" height="30">Usage</td>
                        <td class="text1"  ><select class="form-control" name="usageId"  style="width:125px;">
                                <option value="0">---Select---</option>
                                <c:if test = "${UsageList != null}" >
                                    <c:forEach items="${UsageList}" var="Dept">
                                        <c:choose>
                                            <c:when test="${Dept.usageId == veh.usageId}" >
                                                <option selected value='<c:out value="${Dept.usageId}" />'><c:out value="${Dept.usageName}" /></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value='<c:out value="${Dept.usageId}" />'><c:out value="${Dept.usageName}" /></option>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach >
                                </c:if>
                            </select></td>
                        
                           
                            <td class="text2" height="30"><font color="red">*</font>Warranty Date</td>
                            <td class="text2" height="30"><input name="warrantyDate" type="text" class="datepicker" value="<c:out value="${veh.warrantyDate}"/>" size="20" onchange="CalWarrantyDate(this.value)" >
                        </tr>
                        <tr>

                        <tr>
                         <td class="text2" height="30">MFRs</td>
                            <td class="text2"  ><select class="form-control" name="mfrId"    style="width:125px;">
                                    <option value="0">---Select---</option>
                                    <c:if test = "${
                                          MfrList != null}" >
                                        <c:forEach items="${MfrList}" var="Dept">
                                            <c:choose>
                                                <c:when test="${Dept.mfrId == veh.mfrId}" >
                                                    <option selected value=<c:out value="${Dept.mfrId}" /> ><c:out value="${Dept.mfrName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=<c:out value="${Dept.mfrId}" /> ><c:out value="${Dept.mfrName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                       
                        <td class="text1" height="30">Warranty (Days)</td>
                        <td class="text1" height="30"><input name="war_period" type="text" maxlength="2" class="form-control" value='<c:out value="${veh.war_period}" />' onKeyPress="return onKeyPressBlockCharacters(event);" size="20"></td>
                        </tr>

                        <tr>
                            
                             <td class="text1" height="30">Vehicle Type</td>
                            <td class="text1" height="30"><select class="form-control" name="typeId" onchange="getModelAndTonnage(this.value);" style="width:125px;">
                                    <option value="1">Own</option>
                                    <c:if test = "${TypeList != null}" >
                                        <c:forEach items="${TypeList}" var="type">
                                            <c:choose>
                                                <c:when test="${type.typeId == veh.typeId}">
                                                    <option selected value=<c:out value="${type.typeId}" /> > <c:out value="${type.typeName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=<c:out value="${type.typeId}" /> > <c:out value="${type.typeName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                               
                            </td>
                            <td class="text2" height="30">Vehicle Class</td>
                            <td class="text2"  ><select class="form-control" name="classId"  style="width:125px;">
                                    <option value="0">---Select---</option>
                                    <c:if test = "${ClassList != null}" >
                                        <c:forEach items="${ClassList}" var="Dept">
                                            <c:choose>
                                                <c:when test="${Dept.classId == veh.classId}" >
                                                    <option selected value=<c:out value="${Dept.classId}" /> ><c:out value="${Dept.className}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=<c:out value="${Dept.classId}" /> ><c:out value="${Dept.className}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">Model </td>
                            <td class="text1" height="30"><select readonly class="form-control" name="modelId"  style="width:125px;">
                                    <c:if test = "${modelList != null}" >
                                        <c:forEach items="${modelList}" var="Dept">

                                            <c:if test="${Dept.modelId == veh.modelId}" >
                                                <option selected value=<c:out value="${Dept.modelId}" /> ><c:out value="${Dept.modelName}" /></option>
                                            </c:if>


                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td class="text1" height="30">Tonnage</td>
                            <td class="text1" height="30"><input maxlength='10' name="seatCapacity" id="seatCapacity" type="text" class="form-control" value='<c:out value="${veh.seatCapacity}" />' size="20" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td class="text2" height="30">Engine No</td>
                            <td class="text2" height="30"><input maxlength='20' name="engineNo" type="text" class="form-control" value='E<c:out value="${veh.engineNo}" />' size="20"></td>
                            <td class="text2" height="30">Chassis No</td>
                            <td class="text2" height="30"><input maxlength='20' type="text" name="chassisNo" size="20" class="form-control" value='C<c:out value="${veh.chassisNo}" />'> </td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">Operation Point </td>
                            <td class="text1"  ><select class="form-control" name="opId"  style="width:125px;">
                                    <option value="0">---Select---</option>
                                    <c:if test = "${OperationPointList != null}" >
                                        <c:forEach items="${OperationPointList}" var="Dept">
                                            <c:choose>
                                                <c:when test="${Dept.opId == veh.opId}" >
                                                    <option selected value=<c:out value="${Dept.opId}" /> > <c:out value="${Dept.opName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=<c:out value="${Dept.opId}" /> > <c:out value="${Dept.opName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                             <td class="text2" height="30"> GPS Tracking System</td>
                            <td class="text2"  >
                                <select class="form-control" name="gpsSystem" id="gpsSystem"  style="width:125px;">
                                    <option value="0">---Select---</option>
                                    <c:choose>
                                        <c:when test="${veh.gpsSystem == 'Yes' }" >
                                            <option value='Yes' selected>Yes</option>
                                            <option value='No'>No</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='Yes' >Yes</option>
                                            <option value='No' selected>No</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </td>
<!--                            <td class="text1" height="30">Vehicle Group</td>
                        <input type="hidden" name="custId" value="0"/>
                       <td class="text1" height="30"><select class="form-control" name="groupId" style="width:125px;">
                                <option value="0">---Select---</option>
                                <c:if test = "${groupList != null}" >
                                    <c:forEach items="${groupList}" var="grp">
                                        <c:choose>
                                            <c:when test="${grp.groupId == veh.groupId}" >
                                                <option selected value=<c:out value="${grp.groupId}" /> > <c:out value="${grp.groupName}" /></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value=<c:out value="${grp.groupId}" /> > <c:out value="${grp.groupName}" /></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </td>-->

                        <!--                    <td class="text1" height="30">Next FC Date</td>
                                            <td class="text1" height="30"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVehicle.nextFCDate,'dd-mm-yyyy',this)"/></td>                    -->
                        <input name="nextFCDate" type="hidden" class="form-control" value='<c:out value="${veh.nextFCDate}" />' size="20">
                        </tr>

                        <tr>
                            <td class="text2" height="30"> KM Reading</td>
                            <td class="text2"  ><input name="kmReading" maxlength='15' type="text" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value='<c:out value="${veh.kmReading}" />' size="20"></td>
                            <!--                    <td class="text2" height="30">HM Reading</td>
                                                <td class="text2" height="30"></td>-->
                        <input name="hmReading" maxlength='20' type="hidden" class="form-control" value='<c:out value="${veh.hmReading}" />' size="20">

                        <td class="text2" height="30">Daily Run KM</td>
                        <td class="text2" height="30"> <input name="dailyKm" maxlength='20' type="text" class="form-control" value='<c:out value="${veh.dailyKm}" />' size="20" onKeyPress="return onKeyPressBlockCharacters(event);"> </td>
                        <!--                    <td class="text1" height="30"> Daily Run HM</td>
                                            <td class="text1"  > </td>-->
                        <input name="dailyHm" type="hidden" maxlength='20' class="form-control" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" size="20">
                        </tr>

                        <tr>


                           
                           
                            <td class="text1" height="30">Ownership</td>
                            <td class="text1" height="30">
                                <c:if test = "${veh.ownership == '1'}" >
                                    Own
                                </c:if>
                                <c:if test = "${veh.ownership == '2'}" >
                                    Dedicate
                                </c:if>
                                <c:if test = "${veh.ownership == '3'}" >
                                    Hire
                                </c:if>
                                <c:if test = "${veh.ownership == '4'}" >
                                    Replacement
                                </c:if>
                                    <input type="hidden" name="ownership" id="ownership"  value="<c:out value="${veh.ownership}" />"/>
                                
                            </td>
                            
                      
                                    <c:if test = "${veh.ownership != '1'}" >
                            
                                  <td class="text1" height="30">Vendor Name</td>
                            <td class="text1" height="30"><select class="form-control" name="leasingCustId" style="width:125px;">
                                    <c:if test = "${leasingCustList != null}" >
                                        <c:forEach items="${leasingCustList}" var="lcl">
                                            <c:choose>
                                                <c:when test="${lcl.vendorId == veh.vendorId}">
                                                    <option selected value=<c:out value="${lcl.vendorId}" /> > <c:out value="${lcl.vendorName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=<c:out value="${lcl.vendorId}" /> > <c:out value="${lcl.vendorName}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                             </tr>
                                    </c:if>
                        <tr>  
                                     <td class="text1" height="30">Status </td>
                            <td class="text1"  ><select class="form-control" name="activeInd"   style="width:125px;">
                                    <option value="0">---Select---</option>
                                    <c:choose>
                                        <c:when test="${veh.activeInd == 'Y'}" >
                                            <option selected value='Y' > Active </option>
                                            <option value='N' > InActive </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option  value='Y' > Active </option>
                                            <option selected value='N' > InActive </option>
                                        </c:otherwise>
                                    </c:choose>
                                </select></td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <br>



            <div align="center" style="font-family:Arial, Helvetica, sans-serif; color:#f5533d; font-size:12px; font-weight:bold;" id="userNameStatus">&nbsp;&nbsp;</div>
            <p align="center"><span  ><strong><strong>&nbsp;&nbsp;OEM Details&nbsp;&nbsp;</strong></strong></span></p>
            <c:if test="${vehTyreList != null}" >
                <% int index = 0;
                            String classText = "";
                %>
                <table border="0" class="border" align="center" width="80%" cellpadding="0" cellspacing="0" id="addTyres">
                    <tr >
                        <td  class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                        <td  class="contenthead" height="30" ><div class="contenthead">Item</div></td>
                        <td  class="contenthead" height="30" ><div class="contenthead">Position</div> </td>
                        <td  class="contenthead" height="30" ><div class="contenthead">Tyre Number</div></td>
                        <td  class="contenthead" height="30" ><div class="contenthead">Date</div></td>
                    </tr>


                    <c:forEach items="${vehTyreList}" var="veh">
                        <%

                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr>
                            <td class="<%=classText%>" height="30" > <%= index + 1%> </td>

                            <td class="<%=classText%>" height="30" >
                                <select name="itemIds" class="form-control"   >
                                    <option value='0' > --select-- </option>
                                    <c:if test="${tyreItemList != null}" >
                                        <c:forEach items="${tyreItemList}" var="item">
                                            <c:choose>

                                                <c:when test="${item.itemId == veh.itemId }" >
                                                    <option selected value='<c:out value="${item.itemId}" />' >  <c:out value="${item.itemName}" /> </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${item.itemId}" />'  > <c:out value="${item.itemName}" /></option>
                                                </c:otherwise>

                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>


                            <td class="<%=classText%>" height="30" >
                                <select name="posIds" class="form-control"   >
                                    <option value='0' > --select-- </option>
                                    <c:if test="${positionList != null}" >
                                        <c:forEach items="${positionList}" var="item">
                                            <c:choose>
                                                <c:when test="${item.posId == veh.posId }" >
                                                    <option selected value='<c:out value="${item.posId}" />' > <c:out value="${item.posName}" /> </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${item.posId}" />' > <c:out value="${item.posName}" /></option>
                                                </c:otherwise>
                                            </c:choose>

                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td class="<%=classText%>" height="30" >
                                <input type="text" class="form-control" onChange="checkTyreId(<%= index%>)" name="tyreNos" value='<c:out value="${veh.tyreNo}" />'>
                                <input type="hidden" name="tyreIds" value='<c:out value="${veh.tyreId}" />' >
                            </td>
                            <td class="<%=classText%>" height="30" >
                                <input type="text" class="datepicker" name="tyreDate" value='<c:out value="${veh.tyreD}" />'>
                            </td>


                        </tr>
                        <% index++;%>
                    </c:forEach>
                </table>
            </c:if>
            <br> <br>

            <c:if test="${vehicleDetail != null}" >
                <c:forEach items="${vehicleDetail}" var="veh" >
                    <table border="0" class="border" align="center" width="80%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td>
                            <td class="text2" height="30">Remarks</td>
                            <td class="text2" height="30"><textarea maxlength='100' class="form-control" name="description"><c:out value="${veh.description}" /></textarea></td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <td class="text2" height="30">Is Free Services Done</td>
                            <td class="text2" height="30">
                                <c:if test="${veh.fservice=='Y'}" >
                                    <input type="checkbox" checked value="Y" name="fservice">
                                </c:if>
                                <c:if test="${veh.fservice!='Y'}" >
                                    <input type="checkbox" value="N" name="fservice">
                                </c:if>
                            </td>
                        </tr>
                    </table>
                </c:forEach>
            </c:if>


            <p>&nbsp;</p>

            <% int index = 0;
                        String classText = "";%>



            <p>&nbsp;</p>
            <center>
                <input type="button" class="button" value="Save" name="save" onClick="submitPage(this.name)">
                <input type="button" class="button" value="Add Row" name="Add Row" onClick="addRow()">
                <input type="button" class="button" value="Back" name="Back" onClick="submitPage1()">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>

