  <%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });


            function submitPage(date,active,type,tripType){
              document.customerWise.action = '/throttle/handlefinanceAdviceReportExcel.do?dateval='+date+"&active="+active+"&type="+type+"&tripType="+tripType;
              document.customerWise.submit();  
            }
        </script>
    </head>
    <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
                String tripType = request.getParameter("tripType");
    %>
 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Advance Payment </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Leasing Operation</a></li>
          <li class="">Finanace Advice</li>
          <li class="active">Pay</li>
        </ol>
      </div>
      </div
      <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body onload="sorter.size(50)">
        <form name="customerWise" method="post">
           
            <%@ include file="/content/common/message.jsp" %>

            <br><br>
            Advice Date: <%=dateval%>
            <br>
            <br>


               <c:if test = "${financeAdviceDetails != null}" >
                   <center>  <input class="button" type="button" name="ExportToExcel" id="ExportToExcel" value="ExportToExcel" onclick="submitPage('<%=dateval%>','<%=active%>','<%=type%>','<%=tripType%>')" /></center>
               <table class="table table-info mb30 table-hover" id="table">
               <thead>                   
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>VehicleNo</h3></th>
                        <th><h3>VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>Route</h3></th>
                        <th><h3>Driver</h3></th>
                        <th><h3>TripStart</h3></th>
                        <th><h3>Freight Rate</h3></th>
                        <th><h3>Nett Expenses</h3></th>
                        <th><h3>Profit</h3></th>
                        <th><h3>Already PaidAmount</h3></th>
                        <th><h3>Transit Days</h3></th>
                        <th><h3>Journey Day</h3></th>
                        <th><h3>ToBePaid Today</h3></th>
                        <th><h3>Paid</h3></th>
                        <th><h3>action</h3></th>
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${financeAdviceDetails}" var="FD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }


                            %>
                            <tr height="30">
                                <td class="<%=classText%>" align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>" ><c:out value="${FD.batchType}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.customerName}"/></td>
                                <td class="<%=classText%>">
                                    <c:out value="${FD.cnoteName}"/>
                                </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripcode}" /></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.regNo}" /></td>
                                <td class="<%=classText%>" ><c:out value="${FD.vehicleTypeName}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.routeName}"/></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.driverName}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.planneddate}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.freightcharges}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedexpense}"/> </td>
                                <td class="<%=classText%>"  align="left"> 
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${FD.freightcharges-FD.estimatedexpense}" />
                                </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.actualadvancepaid}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedtransitday}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripday}" /></td>


                                <td class="<%=classText%>" align="left">
                                    <c:if test="${FD.approvalstatus==0 || FD.approvalstatus==3}">
                                    <c:out value="${FD.estimatedadvance}" />
                                    </c:if>
                                    <c:if test="${FD.approvalstatus==1}">
                                    <c:out value="${FD.requestedadvance}" />
                                    </c:if>

                                </td>
                                
                                <%if(active.equals("1")){%>
                                <c:if test="${FD.approvalstatus==0}">
                                <td>-</td>
                                <td class="<%=classText%>" align="left">Waiting for Approval</td>
                                </c:if>



                                
                                <%--c:if test="${FD.approvalstatus==3 && FD.paidstatus=='N'}">
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetApprove.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Request</a>&nbsp;&nbsp;</td>                                
                                </c:if--%>
                                
                                
                                


                                <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                    <%if(!type.equalsIgnoreCase("M")){%>
                                    <td>-</td>
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0&dateval=<c:out value="${dateval}"/>&type=<c:out value="${type}"/>&active=<c:out value="${active}"/>&tripType=<c:out value="${tripType}"/>">pay</a></td>
                                                                           <%}%>
                                 </c:if>
                                 <c:if test="${(FD.approvalstatus==1 && FD.paidstatus=='N')}">
                                     <%if(!type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1&dateval=<c:out value="${dateval}"/>&type=<c:out value="${type}"/>&active=<c:out value="${active}"/>&tripType=<c:out value="${tripType}"/>">pay</a></td>
                                     <%}%>
                                 </c:if>

                                 <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                 <%if(type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1&dateval=<c:out value="${dateval}"/>&type=<c:out value="${type}"/>&active=<c:out value="${active}"/>&tripType=<c:out value="${tripType}"/>">pay</a></td>
                                 <%}%>
                                 </c:if>

                                <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                    <td>-</td>
                                <td class="<%=classText%>" align="left">
                                    <c:if test="${(FD.approvalstatus ==2)}">
                                    Rejected
                                    </c:if>
                                    <c:if test="${(FD.approvalstatus ==1)}">
                                    Approved
                                    </c:if>
                                </td>
                                </c:if>


                                                                           

                                <c:if test="${FD.paidstatus=='Y' && FD.approvalstatus != 0}">                                
                                <td class="<%=classText%>" align="left"><c:out value="${FD.paidamt}"/></td>
                                <td class="<%=classText%>" align="left">Already Paid</td>
                                </c:if>

                                <%}else{%>


                                <c:if test="${FD.paidstatus=='Y'}">
                                <td class="<%=classText%>" align="left"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                <td class="<%=classText%>" align="left"><font color="green">Paid</font></td>
                                </c:if>
                                <c:if test="${FD.paidstatus == 'N'}">
                                    <td class="<%=classText%>" align="center"><font color="red">-</font></td>
                                    <td class="<%=classText%>" align="center"><font color="red">Not Paid</font></td>
                                </c:if>

                                

                                <%}%>
                                <%--
                                <td class="<%=classText%>" align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                
                                </td>
                                --%>


                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>


            </tbody>
            </table>

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50" selected>50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>