<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Trip_Planning_"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test = "${consignmentList != null}" >
            <table style="width: 1100px" align="center" border="0">
                <thead>
                    <tr height="80">
                        <th class="contentsub"><h3><h3>Sno</h3></th>
                        <th class="contentsub"><h3>Consignment No</h3></th>
                        <th class="contentsub"><h3>Consignment Date</h3></th>
                        <th class="contentsub"><h3>Customer Name </h3></th>
                        <th class="contentsub"><h3>Customer Mobile </h3></th>
                        <th class="contentsub"><h3>Origin </h3></th>
                        <th class="contentsub"><h3>Destination </h3></th>
                        <th class="contentsub"><h3>Revenue </h3></th>
                        <th class="contentsub"><h3>RCM Expense </h3></th>
                        <th class="contentsub"><h3>Profitability </h3></th>
                        <th class="contentsub"><h3>Vehicle Required On</h3></th>
                        <th class="contentsub"><h3>Time To Start</h3></th>
                        <th class="contentsub"><h3>Vehicle No</h3></th>
                    </tr>
                </thead>
                <% int index = 0;int sno = 1;%>
                <tbody>
                    <c:forEach items="${consignmentList}" var="cnl">
                        <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <% if (oddEven > 0) { %>
                        <tr height="30">
                            <td align="left" class="text2" ><%=sno%></td>
                            <td align="left" class="text2" ><c:out value="${cnl.consignmentNoteNo}"/></a></td>
                            <td align="left" class="text2" ><c:out value="${cnl.consignmentOrderDate}"/></a></td>
                            <td align="left" class="text2" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.customerMobile}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.consigmentDestination}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.freightAmount}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.rcmexpense}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.freightAmount - cnl.rcmexpense}"/></td>
                            <td align="left" class="text2" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                            <c:if test="${cnl.timeLeftValue > 0}">
                                <td align="left" class="text2"> <font color="green"><c:out value="${cnl.timeLeft}"/></font></td>
                                    </c:if>
                                    <c:if test="${cnl.timeLeftValue <= 0}">
                                <td align="left" class="text2"> <font color="red"><c:out value="${cnl.timeLeft}"/></font></td>
                            </c:if>
                            <td align="left" class="text2" ></td>
                        </tr>
                         <%}else{%>
                        <tr height="30">
                            <td align="left" class="text1" ><%=sno%></td>
                            <td align="left" class="text1" ><c:out value="${cnl.consignmentNoteNo}"/></a></td>
                            <td align="left" class="text1" ><c:out value="${cnl.consignmentOrderDate}"/></a></td>
                            <td align="left" class="text1" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.customerMobile}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.consigmentDestination}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.freightAmount}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.rcmexpense}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.freightAmount - cnl.rcmexpense}"/></td>
                            <td align="left" class="text1" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                            <c:if test="${cnl.timeLeftValue > 0}">
                                <td align="left" class="text1"> <font color="green"><c:out value="${cnl.timeLeft}"/></font></td>
                                    </c:if>
                                    <c:if test="${cnl.timeLeftValue <= 0}">
                                <td align="left" class="text1"> <font color="red"><c:out value="${cnl.timeLeft}"/></font></td>
                            </c:if>
                            <td align="left" class="text1" ></td>
                        </tr>
                        <%}%>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
