/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fuelManagement.web;

/**
 *
 * @author vinoth
 */
import ets.arch.business.PaginationHelper;
import ets.arch.web.BaseController;
import ets.domain.fuelManagement.business.FuelManagementBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.operation.web.OperationCommand;
import ets.domain.fuelManagement.business.FuelManagementTO;
import ets.domain.vehicle.business.VehicleTO;     
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import java.io.*;
import java.net.UnknownHostException;
import ets.domain.operation.business.OperationTO;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class FuelManagementController extends BaseController {

    FuelManagementCommand fuelCommand;
    FuelManagementBP fuelBP;
    LoginBP loginBP;
    VehicleBP vehicleBP;
    CustomerBP customerBP;
    OperationBP operationBP;
    OperationCommand operationCommand;

    public OperationCommand getOperationCommand() {
        return operationCommand;
    }

    public void setOperationCommand(OperationCommand operationCommand) {
        this.operationCommand = operationCommand;
    }
    
    
    public FuelManagementBP getFuelBP() {
        return fuelBP;
    }

    public void setFuelBP(FuelManagementBP fuelBP) {
        this.fuelBP = fuelBP;
    }

    public FuelManagementCommand getFuelCommand() {
        return fuelCommand;
    }

    public void setFuelCommand(FuelManagementCommand fuelCommand) {
        this.fuelCommand = fuelCommand;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }
    

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);


    }

    public ModelAndView handleViewVehicleCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        VehicleTO vehicleTO = new VehicleTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Manage Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList vehicleCompList = new ArrayList();
                ArrayList typeList = new ArrayList();
                path = "content/fuelManagement/viewVehicleCompany.jsp";
                typeList = vehicleBP.processActiveTypeList(vehicleTO);
                request.setAttribute("TypeList", typeList);

                if (request.getParameter("vehicleNo") != null) {
                    fuelTO.setRegNo(request.getParameter("vehicleNo"));
                }
                if (request.getParameter("owningCompId") != null) {
                    fuelTO.setOwningCompId(request.getParameter("owningCompId"));
                }
                if (request.getParameter("typeId") != null) {
                    fuelTO.setVehTypeId(request.getParameter("typeId"));
                }
                if (request.getParameter("usingCompId") != null) {
                    fuelTO.setServicePtId(request.getParameter("usingCompId"));
                }
                ArrayList usageList = new ArrayList();
                ArrayList MfrList = new ArrayList();

                MfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", MfrList);
                usageList = vehicleBP.processActiveUsageList();
                request.setAttribute("usageList", usageList);
                ArrayList owningCompanyList = new ArrayList();
                ArrayList usingCompanyList = new ArrayList();
                 owningCompanyList = customerBP.processActiveCustomerList();
                request.setAttribute("OwningCompanyList", owningCompanyList);
                usingCompanyList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompanyList", usingCompanyList);
                request.setAttribute("regNo", request.getParameter("vehicleNo"));
                request.setAttribute("companyId", request.getParameter("companyId"));
                request.setAttribute("typeId", request.getParameter("typeId"));
                request.setAttribute("servicePtId", request.getParameter("servicePtId"));
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewUsingCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Using Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/fuelManagement/viewUsingComp.jsp";

                ArrayList usingCompList = new ArrayList();
                usingCompList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompList", usingCompList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddUsingCompanyPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Using Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/fuelManagement/addUsingComp.jsp";

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddUsingCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Using Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/fuelManagement/addUsingComp.jsp";
                fuelTO.setCompanyName(request.getParameter("companyName"));
                fuelTO.setActiveInd(fuelCommand.getActiveInd());
                int status = fuelBP.processAddUsingComp(fuelTO);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Using Company Added Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterUsingCompanyPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Using Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                fuelTO.setUsingCompId(fuelCommand.getUsingCompId());
                path = "content/fuelManagement/alterUsingComp.jsp";
                ArrayList usingCompList = new ArrayList();
                usingCompList = fuelBP.processUsingCompanyList(fuelTO);
                request.setAttribute("UsingCompList", usingCompList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterUsingCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Using Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                path = "content/fuelManagement/viewUsingComp.jsp";
                fuelTO.setCompanyName(request.getParameter("companyName"));
                fuelTO.setActiveInd(fuelCommand.getActiveInd());
                fuelTO.setUsingCompId(fuelCommand.getUsingCompId());
                int status = fuelBP.processAlterUsingComp(fuelTO);
                ArrayList usingCompList = new ArrayList();
                usingCompList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompList", usingCompList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Using Company Altered Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVehicleCompanyPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Add Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Add Vehicle Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                ArrayList mfrList = new ArrayList();
                ArrayList modelList = new ArrayList();
                path = "content/fuelManagement/addVehicleCompany.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                mfrList = vehicleBP.processActiveMfrList();
                modelList = vehicleBP.processActiveModelList();
                request.setAttribute("MfrLists", mfrList);
                request.setAttribute("ModelLists", modelList);
                ArrayList usingCompList = new ArrayList();
                usingCompList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompList", usingCompList);
                ArrayList customerList = new ArrayList();
                customerList = customerBP.processActiveCustomerList();
                request.setAttribute("customerList", customerList);
                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processGetUsageList();
                request.setAttribute("UsageList", usageList);
                //////System.out.println("regno=" + fuelCommand.getVehNo());
                request.setAttribute("vehicle", fuelCommand.getVehNo());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewAddCompany --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVehicleCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View  Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {

                String pageTitle = "View Vehicle Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                ArrayList vehicleCompList = new ArrayList();
                path = "content/fuelManagement/viewVehicleCompany.jsp";
                int status = 0;
                fuelTO.setVehTypeId(fuelCommand.getVehTypeId());
                fuelTO.setVehicleId(fuelCommand.getVehicleId());
                fuelTO.setOwningCompId(fuelCommand.getOwningCompId());
                fuelTO.setUsingCompId(fuelCommand.getCompanyId());
                //////System.out.println("fuelCommand.getUsingCompId()" + fuelCommand.getCompanyId());
                //////System.out.println("fuelTO.setUsingCompId" + fuelTO.getUsingCompId());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setDate(fuelCommand.getDate());
                fuelTO.setMfrId(fuelCommand.getMfrId());
                fuelTO.setModelId(fuelCommand.getModelId());
                fuelTO.setUsageTypeId(fuelCommand.getUsageTypeId());
                fuelTO.setActiveInd(fuelCommand.getActiveInd());
                status = fuelBP.processInsertVehicleCompany(fuelTO, userId);
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);
                VehicleTO vehicleTO = new VehicleTO();
                typeList = vehicleBP.processActiveTypeList(vehicleTO);
                request.setAttribute("TypeList", typeList);
                fuelTO.setRegNo(fuelCommand.getRegNo());
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);
                String companyId = (String) session.getAttribute("companyId");
                ArrayList MfrList = new ArrayList();
                MfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", MfrList);

                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processActiveUsageList();
                request.setAttribute("usageList", usageList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Company Added Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterVehicleCompanyPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String rollNumber = "";
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Alter Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Alter Vehicle Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList owningCompanyList = new ArrayList();
                ArrayList usingCompanyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                ArrayList vehicleCompList = new ArrayList();
                fuelTO.setRegNo(fuelCommand.getRegNo());
                path = "content/fuelManagement/alterVehicleCompany.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                ArrayList modelList = new ArrayList();
                 modelList = vehicleBP.processActiveModelList();
                request.setAttribute("ModelLists", modelList);
                ArrayList mfrList = new ArrayList();
                mfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrLists", mfrList);
                ArrayList usageList = new ArrayList();
                usageList = vehicleBP.processActiveUsageList();
                request.setAttribute("usageList", usageList);
                owningCompanyList = customerBP.processActiveCustomerList();
                request.setAttribute("OwningCompanyList", owningCompanyList);
                usingCompanyList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompanyList", usingCompanyList);
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterVehicleCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;

        String menuPath = "";
        menuPath = "Fuel Management >>View Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else

            String pageTitle = "View Vehicle Company";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList companyList = new ArrayList();
            ArrayList vehicleCompList = new ArrayList();
            int status = 0;
            path = "content/fuelManagement/viewVehicleCompany.jsp";
            ArrayList List = new ArrayList();

            FuelManagementTO fuelTO = new FuelManagementTO();

            fuelTO.setVehicleId(fuelCommand.getVehicleId());
            fuelTO.setVehTypeId(fuelCommand.getVehTypeId());
            fuelTO.setRegNo(fuelCommand.getRegNo());
            fuelTO.setOwningCompId(fuelCommand.getOwningCompId());
            fuelTO.setUsingCompId(fuelCommand.getUsingCompId());
            //////System.out.println("fuelCommand.getUsingCompId()" + fuelCommand.getCompanyId());
            //////System.out.println("fuelTO.setUsingCompId" + fuelTO.getUsingCompId());
            fuelTO.setDate(fuelCommand.getDate());
            fuelTO.setMfrId(fuelCommand.getMfrId());
            fuelTO.setModelId(fuelCommand.getModelId());
            fuelTO.setUsageTypeId(fuelCommand.getUsageTypeId());
            fuelTO.setActiveInd(fuelCommand.getActiveInd());
            status = fuelBP.processAlterVehComp(fuelTO, userId);
            VehicleTO vehicleTO = new VehicleTO();
            ArrayList typeList = new ArrayList();
            typeList = vehicleBP.processActiveTypeList(vehicleTO);
            request.setAttribute("TypeList", typeList);
            fuelTO.setRegNo(fuelCommand.getRegNo());
            vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
            request.setAttribute("VehicleCompList", vehicleCompList);
            String companyId = (String) session.getAttribute("companyId");
            ArrayList ModelList = new ArrayList();
            ModelList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", ModelList);
            ArrayList usageList = new ArrayList();
            usageList = vehicleBP.processActiveUsageList();
            request.setAttribute("usageList", usageList);
            companyList = fuelBP.processGetCompanyList();
            request.setAttribute("CompanyList", companyList);
            vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
            request.setAttribute("VehicleCompList", vehicleCompList);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Company Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Vehicle Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDeleteVehicleCompanyPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String rollNumber = "";
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Delete Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleCompany-Delete")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Delete Vehicle Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                ArrayList vehicleCompList = new ArrayList();
                path = "content/fuelManagement/deleteVehicleCompany.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Delete Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDeleteVehicleCompany(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        String menuPath = "";

        menuPath = "Fuel Management >>View Vehicle Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "View Vehicle Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList vehicleCompList = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/viewVehicleCompany.jsp";
                String[] vehicleIds = fuelCommand.getVehicleIds();
                String[] typeIds = fuelCommand.getVehTypeIds();
                String[] compIds = fuelCommand.getCompanyIds();
                String[] serviceIds = fuelCommand.getServicePtIds();
                String[] regNos = fuelCommand.getRegNos();
                String[] selectedIndex = fuelCommand.getSelectedIndex();


                ArrayList List = new ArrayList();
                int index = 0;
                FuelManagementTO fuelTO = null;
                for (int i = 0; i < selectedIndex.length; i++) {
                    fuelTO = new FuelManagementTO();
                    index = Integer.parseInt(selectedIndex[i]);
                    fuelTO.setVehicleId((vehicleIds[index]));

                    List.add(fuelTO);
                }
                status = fuelBP.processDeleteVehComp(List, userId);
                request.setAttribute("CompanyList", companyList);
                vehicleCompList = fuelBP.processVehicleCompList(fuelTO);
                request.setAttribute("VehicleCompList", vehicleCompList);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Vehicle Company Deleted Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to delete Company --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewTankDetails(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>View Tank Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "TankDetail-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                FuelManagementTO fuelTO = new FuelManagementTO();
                String pageTitle = "Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                ArrayList companyList = new ArrayList();
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                path = "content/fuelManagement/viewTankDetails.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                tankDetails = fuelBP.processTankDetails(fuelTO);
                request.setAttribute("TankDetails", tankDetails);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchTankDetails(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Tank Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "TankDetail-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                ArrayList companyList = new ArrayList();
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                path = "content/fuelManagement/viewTankDetails.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                tankDetails = fuelBP.processTankDetails(fuelTO);
                request.setAttribute("TankDetails", tankDetails);
                request.setAttribute("companyId", fuelCommand.getCompanyId());
                request.setAttribute("fromDate", fuelCommand.getFromDate());
                request.setAttribute("toDate", fuelCommand.getToDate());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddTankDetailsPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Add Tank Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "TankDetail-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Add Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                ArrayList companyList = new ArrayList();
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                path = "content/fuelManagement/tankDetails.jsp";


            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddTankDetails(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Tank Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();

                path = "content/fuelManagement/viewTankDetails.jsp";
                int status = 0;
                fuelTO.setDate(fuelCommand.getDate());
                fuelTO.setFuelFilled(fuelCommand.getFuelFilled());
                fuelTO.setPrevious(fuelCommand.getPrevious());
                fuelTO.setPresent(fuelCommand.getPresent());
                fuelTO.setRate(fuelCommand.getRate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelTO.setMinLevel(fuelCommand.getMinLevel());

                status = fuelBP.processInsertTankDetails(fuelTO, userId);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                tankDetails = fuelBP.processTankDetails(fuelTO);
                request.setAttribute("TankDetails", tankDetails);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tank Details Added Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterTankDetailsPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Alter Tank Details  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "TankDetail-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Alter Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                path = "content/fuelManagement/alterTankDetails.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                tankDetails = fuelBP.processTankDetails(fuelTO);
                request.setAttribute("TankDetails", tankDetails);
                ArrayList companyList = new ArrayList();
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterTankDetail(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Tank Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Tank Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList tankDetails = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/viewVehicleCompany.jsp";
                String[] dats = fuelCommand.getDats();
                String[] present = fuelCommand.getPresentReadings();
                String[] compIds = fuelCommand.getCompanyIds();
                String[] fuel = fuelCommand.getFuels();
                String[] rate = fuelCommand.getRates();
                String[] service = fuelCommand.getServicePtIds();
                String[] selectedIndex = fuelCommand.getSelectedIndex();

                ArrayList List = new ArrayList();

                int index = 0;
                FuelManagementTO fuelTO = null;
                for (int i = 0; i < selectedIndex.length; i++) {
                    fuelTO = new FuelManagementTO();
                    index = Integer.parseInt(selectedIndex[i]);
                    fuelTO.setDate((dats[index]));
                    fuelTO.setPresent(present[index]);
                    fuelTO.setCompanyId(compIds[index]);
                    fuelTO.setFuelFilled(fuel[index]);
                    fuelTO.setRate(rate[index]);
                    fuelTO.setServicePtId(service[index]);
                    List.add(fuelTO);
                }
                status = fuelBP.processAlterTankDetails(List, userId);

                tankDetails = fuelBP.processTankDetails(fuelTO);
                request.setAttribute("TankDetails", tankDetails);

                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Tank Detaails Modified Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View  Tank Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddFuelFillingPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Filling ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelFilling-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Filling";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankReading = new ArrayList();
                String runReading = "";
                path = "content/fuelManagement/fuelFillingIn.jsp";
                tankReading = fuelBP.processTankReading(companyId);
                request.setAttribute("TankReading", tankReading);
                runReading = fuelBP.processRunReading(companyId);
                request.setAttribute("runReading", runReading);
                float fuelPrice = fuelBP.processFuelPrice(companyId);
                request.setAttribute("fuelPrice", fuelPrice);
                String regNo = fuelBP.processRegNo(companyId);
                request.setAttribute("regNo", regNo);
                //////System.out.println("tankReading" + tankReading.size());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Fuel Filling --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddFuelFilling(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Filling  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelFilling-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String runReading = "";
                String pageTitle = "Fuel Filling";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                ArrayList tankReading = new ArrayList();
                path = "content/fuelManagement/fuelFillingIn.jsp";
                int status = 0;
                fuelTO.setDate(fuelCommand.getDate());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setCouponNo(fuelCommand.getCouponNo());
                fuelTO.setFuelFilled(fuelCommand.getFuelFilled());
                fuelTO.setAmount(fuelCommand.getAmount());
                fuelTO.setPresent(fuelCommand.getPresent());
                fuelTO.setFuel(fuelCommand.getFuel());
                fuelTO.setOutFillKm(fuelCommand.getOutFillKm());
                fuelTO.setTotalKm(fuelCommand.getTotalKm());
                fuelTO.setAvgKm(fuelCommand.getAvgKm());
                fuelTO.setTankReading(fuelCommand.getTankReading());
                fuelTO.setOutFill(fuelCommand.getOutFill());
                fuelTO.setRunReading(fuelCommand.getRunReading());
                status = fuelBP.processAddFuelFilling(fuelTO, userId, companyId);
                tankReading = fuelBP.processTankReading(companyId);
                request.setAttribute("TankReading", tankReading);
                runReading = fuelBP.processRunReading(companyId);
                request.setAttribute("runReading", runReading);
                float fuelPrice = fuelBP.processFuelPrice(companyId);
                request.setAttribute("fuelPrice", fuelPrice);
                String regNo = fuelBP.processRegNo(companyId);
                request.setAttribute("regNo", regNo);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Fuel Filling Added  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Fuel Filling --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleOutFillFuelPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Filling";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelFilling-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/fuelManagement/fuelFillingOut.jsp";
                //////System.out.println("request.getParameter()" + request.getParameter("regNo"));
                String pageTitle = "Fuel Filling";
                request.setAttribute("regNo", request.getParameter("regNo"));
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Fuel Filling --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleOutFillFuel(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = null;
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Filling  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Fuel Filling";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList tankDetails = new ArrayList();
                ArrayList tankReading = new ArrayList();
                ArrayList List = new ArrayList();
                path = "content/fuelManagement/refresh.jsp";
                int status = 0;
                String[] dats = fuelCommand.getDats();
                String[] regNos = fuelCommand.getRegNos();
                String[] fuels = fuelCommand.getFuels();
                String[] amount = fuelCommand.getRates();
                String[] outKm = fuelCommand.getOutFillKms();
                String[] places = fuelCommand.getPlaces();
                for (int i = 0; i < regNos.length; i++) {
                    fuelTO = new FuelManagementTO();
                    fuelTO.setDate(dats[i]);
                    //////System.out.println("date" + fuelTO.getDate());
                    fuelTO.setRegNo(regNos[i]);
                    fuelTO.setAmount(amount[i]);
                    fuelTO.setFuel(fuels[i]);
                    fuelTO.setOutFillKm(outKm[i]);
                    fuelTO.setPlace(places[i]);
                    List.add(fuelTO);
                }

                status = fuelBP.processAddOutFillFuel(List, userId, companyId);
                tankReading = fuelBP.processTankReading(companyId);
                request.setAttribute("TankReading", tankReading);
                float fuelPrice = fuelBP.processFuelPrice(companyId);
                request.setAttribute("fuelPrice", fuelPrice);
                request.setAttribute("regNo", fuelCommand.getRegNo());
                //////System.out.println("fuelCommand.getRegNo()" + fuelCommand.getRegNo());
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Out Fill Added  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Fuel Filling --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleVehDetails(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) throws IOException {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        String regNo = request.getParameter("regNo");
        String sugessions = "";
        sugessions = fuelBP.processVehDetails(regNo, companyId);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        //////System.out.println("sugessions " + sugessions);
        writer.print(sugessions);
        writer.close();

    }

    public void handleAjaxVehComp(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) throws IOException {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        String regNo = request.getParameter("regNo");
        String sugessions = "";
        sugessions = fuelBP.processVehComp(regNo);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        //////System.out.println("sugessions " + sugessions);
        writer.print(sugessions);
        writer.close();

    }

    public void handleChekMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) throws IOException {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        String mfrId = request.getParameter("mfrId");
        String modelId = request.getParameter("modelId");
        String fromYear = request.getParameter("fromYear");
        String toYear = request.getParameter("toYear");
        String sugessions = "";
        sugessions = fuelBP.processChekMilleage(mfrId, modelId, fromYear, toYear);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        //////System.out.println("sugessions " + sugessions);
        writer.print(sugessions);
        writer.close();

    }

    public ModelAndView handleSearchFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Manage Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                ArrayList companyList = new ArrayList();
                path = "content/fuelManagement/fuelPrice.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getServicePtId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);
                request.setAttribute("fromDate", fuelCommand.getFromDate());
                request.setAttribute("toDate", fuelCommand.getToDate());
                request.setAttribute("companyId", fuelCommand.getServicePtId());
                request.setAttribute("loginedId", companyId);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Manage Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                ArrayList companyList = new ArrayList();
                path = "content/fuelManagement/fuelPrice.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddFuelPricePage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Add Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Add Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                path = "content/fuelManagement/addFuelPrice.jsp";
                String dat = request.getParameter("date");
                String comp = request.getParameter("companyId");
                //////System.out.println("dat" + dat);
                //////System.out.println("comp=" + comp);
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                request.setAttribute("dates", dat);
                request.setAttribute("companyIds", comp);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/fuelPrice.jsp";
                fuelTO.setDate(fuelCommand.getDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelTO.setRate(fuelCommand.getRate());
                status = fuelBP.processInsertPrice(fuelTO, userId);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handleAlterFuelPricePage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//
//        String path = "";
//        HttpSession session = request.getSession();
//        int userId = (Integer) session.getAttribute("userId");
//        String companyId = (String) session.getAttribute("companyId");
//        fuelCommand = command;
//        FuelManagementTO fuelTO = new FuelManagementTO();
//        String menuPath = "";
//        menuPath = "Fuel Management >>Alter Fuel Price  ";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
//            {
//                String pageTitle = "Alter Fuel Price ";
//                request.setAttribute("pageTitle", pageTitle);
//                path = "content/fuelManagement/alterFuelPrice.jsp";
//                request.setAttribute("date", request.getParameter("dates"));
//                request.setAttribute("companyId", request.getParameter("companyIds"));
//                request.setAttribute("rate", request.getParameter("rates"));
//                ArrayList companyList = new ArrayList();
//                companyList = fuelBP.processGetCompanyList();
//                request.setAttribute("CompanyList", companyList);
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
////        } catch (FPBusinessException exception) {
////            /*
////             * run time exception has occurred. Directed to error page.
////             */
////            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
////            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
////                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to View Alter Fuel Price  --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//
//    public ModelAndView handleAlterFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//
//        String path = "";
//        HttpSession session = request.getSession();
//        int userId = (Integer) session.getAttribute("userId");
//        String companyId = (String) session.getAttribute("companyId");
//        fuelCommand = command;
//        FuelManagementTO fuelTO = new FuelManagementTO();
//        String menuPath = "";
//        menuPath = "Fuel Management >>Fuel Price ";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
////            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
////            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
////                path = "content/common/NotAuthorized.jsp";
////            } else
//            {
//                String pageTitle = "Fuel Price ";
//                request.setAttribute("pageTitle", pageTitle);
//                ArrayList fuelPriceList = new ArrayList();
//                int status = 0;
//                path = "content/fuelManagement/fuelPrice.jsp";
//                fuelTO.setDate(fuelCommand.getDate());
//                fuelTO.setCompanyId(fuelCommand.getCompanyId());
//                fuelTO.setRate(fuelCommand.getRate());
//                status = fuelBP.processUpdateFuelPrice(fuelTO, userId);
//                fuelPriceList = fuelBP.processFuelPriceList(companyId);
//                request.setAttribute("FuelPriceList", fuelPriceList);
//
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView handleDeleteFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-Delete")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/fuelPrice.jsp";
                fuelTO.setDate(request.getParameter("dates"));
                fuelTO.setCompanyId(request.getParameter("companyIds"));

                status = fuelBP.processDeleteFuelPrice(fuelTO, userId);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Deleted  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //Fuel Management Report
    public ModelAndView handleFuelReportPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelReport-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                String pageTitle = "Fuel Report";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList owningCompanyList = new ArrayList();
                ArrayList usingCompanyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                path = "content/fuelManagement/fuelReport.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                owningCompanyList = customerBP.processActiveCustomerList();
                request.setAttribute("OwningCompanyList", owningCompanyList);
                usingCompanyList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompanyList", usingCompanyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                request.setAttribute("pageNo", pageNo);
                request.setAttribute("totalPages", totalPages);
                session.setAttribute("totalRecords", totalRecords);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFuelReport(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelReport-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Fuel Report";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelFillingList = new ArrayList();
                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                PaginationHelper pagenation = new PaginationHelper();
                path = "content/fuelManagement/fuelReport.jsp";
                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                int startIndex = 0;
                int endIndex = 0;
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setFromKm(fuelCommand.getFromKm());
                fuelTO.setToKM(fuelCommand.getToKM());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setCouponNo(fuelCommand.getCouponNo());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelTO.setTypeId(fuelCommand.getVehTypeId());
                fuelTO.setOutFill(fuelCommand.getOutFill());
                fuelTO.setOwningCompId(fuelCommand.getOwningCompId());
                fuelTO.setUsingCompId(fuelCommand.getUsingCompId());
                //////System.out.println("fuelTO.setowCompId" + fuelCommand.getOwningCompId());
                //////System.out.println("todate" + fuelCommand.getToDate());
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                typeList = vehicleBP.processGetTypeList();
                request.setAttribute("TypeList", typeList);
                request.setAttribute("outFill", fuelTO.getOutFill());
                request.setAttribute("vehTypeId", fuelTO.getTypeId());
                request.setAttribute("companyId", fuelTO.getCompanyId());
                request.setAttribute("toDate", fuelTO.getToDate());
                request.setAttribute("fromDate", fuelTO.getFromDate());
                request.setAttribute("toKm", fuelTO.getToKM());
                request.setAttribute("fromKm", fuelTO.getFromKm());
                request.setAttribute("regNo", fuelTO.getRegNo());
                request.setAttribute("couponNo", fuelCommand.getCouponNo());
                request.setAttribute("owningCompId", fuelCommand.getOwningCompId());
                request.setAttribute("usingCompId", fuelCommand.getUsingCompId());
                totalRecords = fuelBP.processTotalRecords(fuelTO);
                session.setAttribute("totalRecords", totalRecords);
                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                if (request.getParameter("button") != null) {
                    buttonClicked = request.getParameter("button");
                }
                ArrayList owningCompanyList = new ArrayList();
                ArrayList usingCompanyList = new ArrayList();
                owningCompanyList = customerBP.processActiveCustomerList();
                request.setAttribute("OwningCompanyList", owningCompanyList);
                usingCompanyList = fuelBP.processUsingCompanyList();
                request.setAttribute("UsingCompanyList", usingCompanyList);
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
                //////System.out.println("buttonClicked" + buttonClicked);
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                totalPages = totalRecords / 50;
                request.setAttribute("pageNo", pageNo);
                request.setAttribute("totalPages", totalPages);
                startIndex = pagenation.getStartIndex();
                endIndex = 40 + pagenation.getEndIndex();
                //////System.out.println("ebdIndex" + endIndex);
                fuelFillingList = fuelBP.processFuelFillingList(fuelTO, userId, startIndex, endIndex);
                request.setAttribute("FuelFillingList", fuelFillingList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewFuel Report --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Milleage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MilleageConfig-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Vehicle Milleage";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList milleageList = new ArrayList();
                ArrayList vehicleTypeList = new ArrayList();
                path = "content/fuelManagement/milleageConfig.jsp";
                fuelTO.setVehicleTypeId(fuelCommand.getVehicleTypeId());
                vehicleTypeList = vehicleBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
                request.setAttribute("milleageList", milleageList);
                request.setAttribute("vehicleTypeId", fuelTO.getVehicleTypeId());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Vehicle Milleage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleSaveMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        int userId = (Integer) session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Milleage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MilleageConfig-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Vehicle Milleage";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList milleageList = new ArrayList();
                ArrayList vehicleTypeList = new ArrayList();
                path = "content/fuelManagement/milleageConfig.jsp";
                fuelTO.setVehicleTypeId(fuelCommand.getVehicleTypeId());
                String[] vehicleTypeIds = request.getParameterValues("vehicleTypeIds");
                String[] modelIds = request.getParameterValues("modelIds");
                String[] vehicleMileage = request.getParameterValues("vehicleMileage");
                String[] reeferMileage = request.getParameterValues("reeferMileage");
                String[] selected = request.getParameterValues("selectedIndex");
                int updateMileage = 0;
                updateMileage = fuelBP.updateMileage(vehicleTypeIds,vehicleMileage,reeferMileage,selected,fuelTO,userId);
                vehicleTypeList = vehicleBP.getVehicleTypeList();
                request.setAttribute("vehicleTypeList", vehicleTypeList);
                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
                request.setAttribute("milleageList", milleageList);
                request.setAttribute("vehicleTypeId", fuelTO.getVehicleTypeId());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Vehicle Milleage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleDeleteVehicleMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Milleage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MilleageConfig-Delete")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Vehicle Milleage";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList milleageList = new ArrayList();
                ArrayList mfrList = new ArrayList();
                path = "content/fuelManagement/milleageConfig.jsp";
                fuelTO.setMfrId(fuelCommand.getMfrId());
                fuelTO.setModelId(fuelCommand.getModelId());
                fuelTO.setFromYear(fuelCommand.getFromYear());
                fuelTO.setToYear(fuelCommand.getToYear());
                int status = fuelBP.processDeleteMilleage(fuelTO);
                mfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", mfrList);
                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
                request.setAttribute("milleageList", milleageList);
                request.setAttribute("mfrId", fuelTO.getMfrId());
                request.setAttribute("modelId", fuelTO.getModelId());
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Deleted Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Vehicle Milleage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVehicleMilleagePage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Add Vehicle  Milleage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MilleageConfig-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Add Vehicle Milleage";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList mfrList = new ArrayList();

                path = "content/fuelManagement/addMilleage.jsp";
                mfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrLists", mfrList);


            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add Vehicle milleage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddVehicleMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String companyId = (String) session.getAttribute("companyId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>View Milleage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Vehicle Milleage";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList milleageList = new ArrayList();
                ArrayList mfrList = new ArrayList();
                path = "content/fuelManagement/milleageConfig.jsp";
                fuelTO.setMfrId(fuelCommand.getMfrId());
                fuelTO.setModelId(fuelCommand.getModelId());
                fuelTO.setFromYear(fuelCommand.getFromYear());
                fuelTO.setToYear(fuelCommand.getToYear());
                fuelTO.setMilleage(fuelCommand.getMilleage());
                int status = fuelBP.processAddMilleage(fuelTO);
                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
                request.setAttribute("milleageList", milleageList);
                mfrList = vehicleBP.processActiveMfrList();
                request.setAttribute("MfrList", mfrList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View Milleage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handleAlterVehicleMilleagePage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//
//        String path = "";
//        HttpSession session = request.getSession();
//        String companyId = (String) session.getAttribute("companyId");
//        fuelCommand = command;
//        FuelManagementTO fuelTO = new FuelManagementTO();
//        String menuPath = "";
//        menuPath = "Fuel Management >>Alter Milleage";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
////            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
////            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
////                path = "content/common/NotAuthorized.jsp";
////            } else
//            {
//                String pageTitle = "Alter Vehicle Milleage";
//                request.setAttribute("pageTitle", pageTitle);
//                ArrayList milleageList = new ArrayList();
//                path = "content/fuelManagement/alterMilleage.jsp";
//                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
//                request.setAttribute("milleageList", milleageList);
//
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Alter Milleage --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//
//    public ModelAndView handleAlterVehicleMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//
//        String path = "";
//        HttpSession session = request.getSession();
//        String companyId = (String) session.getAttribute("companyId");
//        fuelCommand = command;
//        FuelManagementTO fuelTO = new FuelManagementTO();
//        String menuPath = "";
//        menuPath = "Fuel Management >>View Milleage";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
////            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
////            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
////                path = "content/common/NotAuthorized.jsp";
////            } else
//            {
//                String pageTitle = "Vehicle Milleage";
//                request.setAttribute("pageTitle", pageTitle);
//                ArrayList milleageList = new ArrayList();
//                path = "content/fuelManagement/milleageConfig.jsp";
//                fuelTO.setMfrId(fuelCommand.getMfrId());
//                fuelTO.setModelId(fuelCommand.getModelId());
//                fuelTO.setFromYear(fuelCommand.getFromYear());
//                fuelTO.setToYear(fuelCommand.getToYear());
//                fuelTO.setMilleage(fuelCommand.getMilleage());
//                int status = fuelBP.processUpdateMilleage(fuelTO);
//                milleageList = fuelBP.processMilleageList(fuelTO, companyId);
//                request.setAttribute("milleageList", milleageList);
//
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to Alter Milleage --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView handleMilleageReportPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Milleage  Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MilleageReport-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Milleage  Report";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                path = "content/fuelManagement/milleageReport.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMilleageReport(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        int userId = 0;
        HttpSession session = request.getSession();
        //int userId = (Integer)session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Milleage  Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {
                String pageTitle = "Milleage  Report";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList milleageReport = new ArrayList();
                ArrayList companyList = new ArrayList();
                path = "content/fuelManagement/milleageReport.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelTO.setOutFill("0");
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                request.setAttribute("companyId", fuelTO.getCompanyId());
                request.setAttribute("toDate", fuelTO.getToDate());
                request.setAttribute("fromDate", fuelTO.getFromDate());
                request.setAttribute("regNo", fuelTO.getRegNo());
                milleageReport = fuelBP.processMilleageReport(fuelTO, userId);
                request.setAttribute("milleageReport", milleageReport);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Milleage Report --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleReportPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Vehicle  Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "VehicleReport-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                path = "content/fuelManagement/vehicleMonthlyReport.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleVehicleReport(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        int userId = 0;
        HttpSession session = request.getSession();
        //int userId = (Integer)session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Vehicle  Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {

                ArrayList vehicleReport = new ArrayList();
                ArrayList companyList = new ArrayList();
                path = "content/fuelManagement/vehicleMonthlyReport.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);
                request.setAttribute("toDate", fuelTO.getToDate());
                request.setAttribute("fromDate", fuelTO.getFromDate());
                request.setAttribute("regNo", fuelTO.getRegNo());
                vehicleReport = fuelBP.processVehicleMonthlyReport(fuelTO, userId);
                request.setAttribute("VehicleReport", vehicleReport);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Milleage Report --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMonthlyAvgReportPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel Management >>Monthly Avg Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "MonthlyAvgReport-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                ArrayList companyList = new ArrayList();
                ArrayList typeList = new ArrayList();
                path = "content/fuelManagement/monthlyAvgRept.jsp";
                companyList = fuelBP.processGetCompanyList();
                request.setAttribute("CompanyList", companyList);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleMonthlyAvgReport(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = request.getSession();
        //int userId = (Integer)session.getAttribute("userId");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Vehicle  Report ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else
            {

                ArrayList monthlyAvgReport = new ArrayList();
                ArrayList companyList = new ArrayList();
                ArrayList monthArray = new ArrayList();
                path = "content/fuelManagement/monthlyAvgRept.jsp";
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setRegNo(fuelCommand.getRegNo());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                String[] dat = (fuelTO.getFromDate().split("-"));
                String[] dat1 = (fuelTO.getToDate().split("-"));
                int from = Integer.parseInt(dat[0]);
                int to = Integer.parseInt(dat1[0]);
                request.setAttribute("from", from);
                request.setAttribute("to", to);
                if (from > to) {
                    from = Integer.parseInt(dat[0]);
                    to = 12 + Integer.parseInt(dat1[0]);
                }
                //////System.out.println("from" + from);
                //////System.out.println("to" + to);
                int i = 0;
                for (int j = from; j <= to; j++) {
                    if (j > 12 && j != 12) {
                        i = j - 12;
                        //////System.out.println("i" + i);
                    } else {
                        i = j;
                    }
                    //////System.out.println("i" + i);
                    if (i == 1) {
                        monthArray.add(monthArray.size(), "Jan");
                    }
                    if (i == 2) {
                        monthArray.add(monthArray.size(), "Feb");
                    } else if (i == 3) {
                        monthArray.add(monthArray.size(), "Mar");
                    } else if (i == 4) {
                        monthArray.add(monthArray.size(), "Apirl");
                    } else if (i == 5) {
                        monthArray.add(monthArray.size(), "May");
                    } else if (i == 6) {
                        monthArray.add(monthArray.size(), "June");
                    } else if (i == 7) {
                        monthArray.add(monthArray.size(), "July");
                    } else if (i == 8) {
                        monthArray.add(monthArray.size(), "Aug");
                    } else if (i == 9) {
                        monthArray.add(monthArray.size(), "Sep");
                    } else if (i == 10) {
                        monthArray.add(monthArray.size(), "Oct");
                    } else if (i == 11) {
                        monthArray.add(monthArray.size(), "Nov");
                    } else if (i == 12) {
                        monthArray.add(monthArray.size(), "Dec");
                    }
                }
                request.setAttribute("MonthArray", monthArray);
                request.setAttribute("regNo", fuelTO.getRegNo());
                monthlyAvgReport = fuelBP.processMonthlyAvgReport(fuelTO, userId);
                request.setAttribute("MonthlyAvgReport", monthlyAvgReport);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Milleage Report --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //To Excel
    public void handleFuelReportExcel(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        OutputStream out = null;
        try {
            String[] date = request.getParameterValues("dates");
            String[] companyName = request.getParameterValues("companyNames");
            String[] couponNo = request.getParameterValues("couponNos");
            String[] OutFill = request.getParameterValues("outFills");
            String[] regNo = request.getParameterValues("regNos");
            String[] mfr = request.getParameterValues("mfrs");
            String[] model = request.getParameterValues("models");
            String[] usageType = request.getParameterValues("usageTypes");
            String[] owningComp = request.getParameterValues("owningComps");
            String[] comp = request.getParameterValues("usingComps");
            String[] fuelFilled = request.getParameterValues("fuels");
            String[] amount = request.getParameterValues("amounts");
            String[] present = request.getParameterValues("presents");
            String[] previous = request.getParameterValues("previous");
            String[] totalKm = request.getParameterValues("totalKms");
            String[] avgKm = request.getParameterValues("avgKms");
            String[] run = request.getParameterValues("runReadings");
            String[] capacity = request.getParameterValues("tankReadings");


            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + "Fuel Report" + ".xls");

            WritableWorkbook w =
                    Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("Fuel Filling Report", 0);

            s.addCell(new Label(0, 0, "Date"));
            s.addCell(new Label(1, 0, "company Name"));
            s.addCell(new Label(2, 0, "coupon No"));
            s.addCell(new Label(3, 0, "Filled Status"));
            s.addCell(new Label(4, 0, "Vehicle No"));
            s.addCell(new Label(5, 0, "Mfr"));
            s.addCell(new Label(6, 0, "Model"));
            s.addCell(new Label(7, 0, "Usdage Type"));
            s.addCell(new Label(8, 0, "Owning Company"));
            s.addCell(new Label(9, 0, "Using Company"));
            s.addCell(new Label(10, 0, "Fuel Filled"));
            s.addCell(new Label(11, 0, "Amount"));
            s.addCell(new Label(12, 0, "Present Reading"));
            s.addCell(new Label(13, 0, "Previous Reading"));
            s.addCell(new Label(14, 0, "Total Km"));
            s.addCell(new Label(15, 0, "Average Km"));
            s.addCell(new Label(16, 0, "Tank Reading"));
            s.addCell(new Label(17, 0, "Capacity"));

            for (int i = 0; i < companyName.length; i++) {
                s.addCell(new Label(0, i + 1, date[i]));
                s.addCell(new Label(1, i + 1, companyName[i]));
                s.addCell(new Label(2, i + 1, couponNo[i]));
                s.addCell(new Label(3, i + 1, OutFill[i]));
                s.addCell(new Label(4, i + 1, regNo[i]));
                s.addCell(new Label(5, i + 1, mfr[i]));
                s.addCell(new Label(6, i + 1, model[i]));
                s.addCell(new Label(7, i + 1, usageType[i]));
                s.addCell(new Label(8, i + 1, owningComp[i]));
                s.addCell(new Label(9, i + 1, comp[i]));
                s.addCell(new Label(10, i + 1, fuelFilled[i]));
                s.addCell(new Label(11, i + 1, amount[i]));
                s.addCell(new Label(12, i + 1, present[i]));
                s.addCell(new Label(13, i + 1, previous[i]));
                s.addCell(new Label(14, i + 1, totalKm[i]));
                s.addCell(new Label(15, i + 1, avgKm[i]));
                s.addCell(new Label(16, i + 1, run[i]));
                s.addCell(new Label(17, i + 1, capacity[i]));

                //////System.out.println("inside sno");

            }

            w.write();
            w.close();
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewReports  data --> " + exception);

        }


    }

    public void handleMilleageReportExcel(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        OutputStream out = null;
        try {

            String[] date = request.getParameterValues("dates");
            String[] companyName = request.getParameterValues("companyNames");
            String[] servicePtName = request.getParameterValues("servicePtNames");
            String[] regNo = request.getParameterValues("regNos");
            String[] milleage = request.getParameterValues("milleages");
            String[] avgKm = request.getParameterValues("avgKms");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + "Milleage Report" + ".xls");

            WritableWorkbook w =
                    Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("Milleage Report", 0);

            s.addCell(new Label(0, 0, "Date"));
            s.addCell(new Label(1, 0, "Vehicle Number"));
            s.addCell(new Label(2, 0, "Owning Company"));
            s.addCell(new Label(3, 0, "Using Company"));
            s.addCell(new Label(4, 0, "Milleage"));
            s.addCell(new Label(5, 0, "Actual Milleage"));


            for (int i = 0; i < companyName.length; i++) {
                s.addCell(new Label(0, i + 1, date[i]));
                s.addCell(new Label(1, i + 1, regNo[i]));
                s.addCell(new Label(2, i + 1, companyName[i]));
                s.addCell(new Label(3, i + 1, servicePtName[i]));
                s.addCell(new Label(4, i + 1, milleage[i]));
                s.addCell(new Label(5, i + 1, avgKm[i]));


                //////System.out.println("inside sno");

            }

            w.write();
            w.close();
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewReports  data --> " + exception);

        }


    }

    public void handleVehicleReportExcel(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        String companyName = (String) session.getAttribute("companyName");
        try {

            String[] regNo = request.getParameterValues("regNos");
            String[] type = request.getParameterValues("types");
            String[] usage = request.getParameterValues("usageTypes");
            String[] fuel = request.getParameterValues("fuels");
            String[] outFill = request.getParameterValues("outFills");
            String[] totFill = request.getParameterValues("totFills");
            String[] km = request.getParameterValues("totalKms");
            String[] avg = request.getParameterValues("avgs");
            String[] rate = request.getParameterValues("rates");
            String amount = request.getParameter("amount");
            String litr = request.getParameter("litres");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + "Vehicle Monthly  Report" + ".xls");

            WritableWorkbook w =
                    Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("Vehicle Monthly  Report", 0);

            s.addCell(new Label(2, 0, "PARVEEN  AUTOMOBILES  (P)  LTD.,"));
            s.addCell(new Label(2, 1, "FUEL  STATION," + companyName + ", CHENNAI  600 110."));
            s.addCell(new Label(2, 2, "MONTHLY KMPL STATEMENT "));

            s.addCell(new Label(0, 4, "Vehicle Number"));
            s.addCell(new Label(1, 4, "Vehicle Type"));
            s.addCell(new Label(2, 4, "Usage Type"));
            s.addCell(new Label(3, 4, "Fuel Filled In"));
            s.addCell(new Label(4, 4, "Fuel Filled Out"));
            s.addCell(new Label(5, 4, "Total Filled "));
            s.addCell(new Label(6, 4, "Total Km"));
            s.addCell(new Label(7, 4, "Average"));
            s.addCell(new Label(8, 4, "Amount"));

            for (int i = 0; i < regNo.length; i++) {
                s.addCell(new Label(0, i + 5, regNo[i]));
                s.addCell(new Label(1, i + 5, type[i]));
                s.addCell(new Label(2, i + 5, usage[i]));
                s.addCell(new Label(3, i + 5, fuel[i]));
                s.addCell(new Label(4, i + 5, outFill[i]));
                s.addCell(new Label(5, i + 5, totFill[i]));
                s.addCell(new Label(6, i + 5, km[i]));
                s.addCell(new Label(7, i + 5, avg[i]));
                s.addCell(new Label(8, i + 5, rate[i]));

                //////System.out.println("inside vehicle report");

            }
            s.addCell(new Label(2, rate.length + 6, "Total Amount"));
            s.addCell(new Label(4, rate.length + 6, amount));
            s.addCell(new Label(2, fuel.length + 7, "Total Litres"));
            s.addCell(new Label(4, fuel.length + 7, litr));
            w.write();
            w.close();
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to vehicle Reports  data --> " + exception);

        }


    }

    public void handleAvgReportExcel(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        OutputStream out = null;
        HttpSession session = request.getSession();
        fuelCommand = command;
        String companyName = (String) session.getAttribute("companyName");
        try {

            String[] regNo = request.getParameterValues("regNos");
            String[] mfr = request.getParameterValues("mfrs");
            String[] model = request.getParameterValues("models");
            String[] usage = request.getParameterValues("usageTypes");
            String[] avgName = request.getParameterValues("avgNames");
            String[] inFillName = request.getParameterValues("inFillNames");
            String[] outFillName = request.getParameterValues("outFillNames");
            String[][] avg = fuelCommand.getAvgs();
            String[][] inFill = fuelCommand.getInFills();
            String[][] outFill = fuelCommand.getOutFills();
            String[] km = request.getParameterValues("totalKms");


            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + "Vehicle Monthly  Report" + ".xls");

            WritableWorkbook w =
                    Workbook.createWorkbook(response.getOutputStream());
            WritableSheet s = w.createSheet("Vehicle Monthly  Report", 0);

            s.addCell(new Label(2, 0, "PARVEEN  AUTOMOBILES  (P)  LTD.,"));
            s.addCell(new Label(2, 1, "FUEL  STATION," + companyName + ", CHENNAI  600 110."));
            s.addCell(new Label(2, 2, "MONTHLY KMPL STATEMENT "));

            s.addCell(new Label(0, 4, "Vehicle Number"));
            s.addCell(new Label(1, 4, "Mfr"));
            s.addCell(new Label(2, 4, "Model"));
            s.addCell(new Label(3, 4, "Usage Type"));
            for (int i = 0; i < avgName.length; i++) {
                s.addCell(new Label(i + 4, 4, avgName[i]));
            }
            for (int i = 0; i < inFillName.length; i++) {
                s.addCell(new Label(i + 4 + avgName.length, 4, inFillName[i]));
            }
            for (int i = 0; i < outFillName.length; i++) {
                s.addCell(new Label(i + 4 + inFillName.length + avgName.length, 4, outFillName[i]));
            }

            s.addCell(new Label(4 + outFillName.length + inFillName.length + avgName.length, 4, "Total Km"));

            int x = 0;
            for (int i = 0; i < regNo.length; i++) {
                s.addCell(new Label(0, i + 5, regNo[i]));
                s.addCell(new Label(1, i + 5, mfr[i]));
                s.addCell(new Label(2, i + 5, model[i]));
                s.addCell(new Label(3, i + 5, usage[i]));
                for (int j = 0; j < avgName.length; j++) {

                    s.addCell(new Label(j + 4, i + 5, avg[j + x][0]));
                    //////System.out.println("avg[" + (j + x) + "][0])" + avg[j + x][0]);
                }
                for (int j = 0; j < inFillName.length; j++) {
                    s.addCell(new Label(j + 4 + avgName.length, i + 5, inFill[j + x][0]));
                }
                for (int j = 0; j < outFillName.length; j++) {
                    s.addCell(new Label(j + 4 + inFillName.length + avgName.length, i + 5, outFill[j][0]));
                }

                s.addCell(new Label(4 + outFillName.length + inFillName.length + avgName.length, i + 5, km[i]));
                x = x + avgName.length;

                //////System.out.println("inside vehicle report");

            }

            w.write();
            w.close();
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to vehicle Reports  data --> " + exception);

        }
    }

    public ModelAndView viewFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String rollNumber = "";
        String path = "";
        HttpSession session = request.getSession();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel >>Fuel Price";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Manage Fuel";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                path = "content/fuelManagement/fuelPrice.jsp";
                fuelPriceList = fuelBP.getFuelPriceNew();
                //////System.out.println("fuelPriceList =======> " + fuelPriceList.size());
                request.setAttribute("fuelPriceList", fuelPriceList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addFuelPage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        //////System.out.println("22222222222222222222 =====> ");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList BunkNameList = new ArrayList();
        fuelCommand = command;
        String menuPath = "";
        menuPath = "Fuel >> Add Fuel Price";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Add Fule Price";
                request.setAttribute("pageTitle", pageTitle);
                path = "content/fuelManagement/addFuelPrice.jsp";
//                BunkNameList = fuelBP.GetBunk();
//                request.setAttribute("companyTypes", BunkNameList);
                BunkNameList = fuelBP.getBunkList();
                //////System.out.println("bunkList.size() in cont==== => " + BunkNameList.size());
                request.setAttribute("BunkNameList", BunkNameList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveFuelPage(HttpServletRequest request, HttpServletResponse reponse, FuelManagementCommand command) throws IOException {

        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        fuelCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList BunkNameList = new ArrayList();
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "HRMS >>Company  ";
        String pageTitle = "Manage Company";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (request.getParameter("bunkId") != null) {
                fuelTO.setBunkId(request.getParameter("bunkId"));
            }
            if (request.getParameter("rate") != null) {
                fuelTO.setRate(request.getParameter("rate"));
            }
            if (request.getParameter("startDate") != null) {
                fuelTO.setStartDate(request.getParameter("startDate"));
            }
            if (request.getParameter("endDate") != null) {
                fuelTO.setEndDate(request.getParameter("endDate"));
            }
            path = "content/fuelManagement/fuelPrice.jsp";
            status = fuelBP.insertNewfuelPrice(fuelTO, userId);

            ArrayList fuelPriceList = new ArrayList();
            fuelPriceList = fuelBP.getFuelPriceNew();
                //////System.out.println("fuelPriceList =======> " + fuelPriceList.size());
                request.setAttribute("fuelPriceList", fuelPriceList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Fuel Price Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Company Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

     public ModelAndView deleteFuelPrice(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        String fuelPriceIds = (String) session.getAttribute("fuelPriceIds");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-Delete")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/fuelPrice.jsp";
                fuelTO.setDate(request.getParameter("dates"));
                fuelTO.setCompanyId(request.getParameter("companyIds"));

                status = fuelBP.processDeleteFuelPrice(fuelTO, userId);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Deleted  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


     //Brattle Foods Starts Here
     /**
     * This method used to handle Fuel Consumption Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */

     public ModelAndView handleFuelConsumption(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String companyId = (String) session.getAttribute("companyId");
        String fuelPriceIds = (String) session.getAttribute("fuelPriceIds");
        fuelCommand = command;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Fuel Management >>Fuel Price ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "FuelPrice-Delete")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String pageTitle = "Fuel Price ";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList fuelPriceList = new ArrayList();
                int status = 0;
                path = "content/fuelManagement/fuelPrice.jsp";
                fuelTO.setDate(request.getParameter("dates"));
                fuelTO.setCompanyId(request.getParameter("companyIds"));

                status = fuelBP.processDeleteFuelPrice(fuelTO, userId);
                fuelTO.setFromDate(fuelCommand.getFromDate());
                fuelTO.setToDate(fuelCommand.getToDate());
                fuelTO.setCompanyId(fuelCommand.getCompanyId());
                fuelPriceList = fuelBP.processFuelPriceList(fuelTO);
                request.setAttribute("FuelPriceList", fuelPriceList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Deleted  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

     public ModelAndView vehicleTypeOperationgCostMaster(HttpServletRequest request, HttpServletResponse response,       FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId ="";
        fuelCommand = command;
        FuelManagementTO fuelTO =null;
        String menuPath = "";
        String countryId = "";
        String pageTitle = "VehicleType Operating cost  ";
        try {
            userId = (Integer) session.getAttribute("userId");
            companyId = (String) session.getAttribute("companyId");
            fuelTO = new FuelManagementTO();
            menuPath = "Fuel Management >>Vehicle Type Operating Cost ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

             ArrayList countryList=new ArrayList();
            countryList=fuelBP.getCountryList();
            request.setAttribute("countryList", countryList);
            if(request.getParameter("countryId") != null){
                countryId=request.getParameter("countryId");
            }
            fuelTO.setCountryId(countryId);
            request.setAttribute("countryId", countryId);


            ArrayList vehicleTypeCostList=new ArrayList();
            vehicleTypeCostList=fuelBP.getVehicleTypeCostList(fuelTO);
            //////System.out.println("vehicleTypeCostList size in COntroller :::"+vehicleTypeCostList.size());
            if(vehicleTypeCostList.size()>0){
            request.setAttribute("vehicleTypeCostList", vehicleTypeCostList);
            }else{
                request.setAttribute("errorMessage", "No Records Found");
            }

            path = "content/fuelManagement/vehicleTypeCostMaster.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
             /**
     * This method used to handle addvehicleTypeOperatingCost
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */

     public ModelAndView addvehicleTypeOperatingCost(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId ="";
        fuelCommand = command;
        FuelManagementTO fuelTO =null;
        String menuPath = "";
        String pageTitle = "Fuel Price ";
        String fetchStatus="";
        String countryId="";
        String vehicleTypeCostId="";
        try {
            userId = (Integer) session.getAttribute("userId");
            companyId = (String) session.getAttribute("companyId");
            fuelTO = new FuelManagementTO();
            menuPath = "Fuel Management >>Vehicle Type Operating Cost ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);


            if(request.getParameter("fetchStatus") != null)
            fetchStatus=request.getParameter("fetchStatus");

            if(request.getParameter("vehicleTypeCostId") != null)
            vehicleTypeCostId=request.getParameter("vehicleTypeCostId");
            fuelTO.setVehicleTypeId(vehicleTypeCostId);
           
            countryId=request.getParameter("countryId");
            
            //////System.out.println("My country ID is "+countryId);
            request.setAttribute("countryId", countryId);

            ArrayList countryList=new ArrayList();
            countryList=fuelBP.getCountryList();
            request.setAttribute("countryList", countryList);

            if("Y".equals(fetchStatus)){
          // fetch vehicle and cost
             ArrayList checkFuelList=new ArrayList();
             checkFuelList=fuelBP.getCheckFuelList(countryId);
             //////System.out.println("checkFuelList size :::"+checkFuelList.size());
             if(checkFuelList.size()>0){
             ArrayList fuelConfigVehicleTypeList=new ArrayList();
             fuelConfigVehicleTypeList=fuelBP.getFuelConfigVehicleTypeList(countryId);
             request.setAttribute("fuelConfigVehicleTypeList", fuelConfigVehicleTypeList);
             //////System.out.println("fuelConfigVehicleTypeList size is :::"+fuelConfigVehicleTypeList.size());
             }else{
              request.setAttribute("errorMessage", "No Fuel Price Found ,Please Add");
             }


            }




            path = "content/fuelManagement/addVehicleTypeCostMaster.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     public ModelAndView editvehicleTypeOperatingCost(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId ="";
        fuelCommand = command;
        FuelManagementTO fuelTO =null;
        String menuPath = "";
        String pageTitle = "Edit VehicleType Cost";
        String fetchStatus="";
        String countryId="";
        String vehicleTypeCostId="";
        try {
            userId = (Integer) session.getAttribute("userId");
            companyId = (String) session.getAttribute("companyId");
            fuelTO = new FuelManagementTO();
            menuPath = "Fuel Management >>Edit Vehicle Type Operating Cost ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);




            if(request.getParameter("vehicleTypeCostId") != null)
            vehicleTypeCostId=request.getParameter("vehicleTypeCostId");
            fuelTO.setVehicleTypeId(vehicleTypeCostId);

            if(request.getParameter("countryId") != null){
            countryId=request.getParameter("countryId");
            }
            request.setAttribute("countryId", countryId);

            ArrayList countryList=new ArrayList();
            countryList=fuelBP.getCountryList();
            request.setAttribute("countryList", countryList);

             ArrayList editVehicleTypeList=new ArrayList();
             editVehicleTypeList=fuelBP.editVehicleTypeList(fuelTO);
             request.setAttribute("fuelConfigVehicleTypeList", editVehicleTypeList);
             //////System.out.println("editVehicleTypeList size is ::::"+editVehicleTypeList.size());




            path = "content/fuelManagement/addVehicleTypeCostMaster.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     /**
     * This method used to handle savevehicleTypeOperatingCost
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ModelAndView savevehicleTypeOperatingCost(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int userId = 0;
        String companyId ="";
        fuelCommand = command;
        FuelManagementTO fuelTO =null;
        String menuPath = "";
        String pageTitle = "Fuel Price ";
        String fetchStatus="";
        String countryId="";
        int insertStatus=0;
        try {
            userId = (Integer) session.getAttribute("userId");
            companyId = (String) session.getAttribute("companyId");
            fuelTO = new FuelManagementTO();
            menuPath = "Fuel Management >>Vehicle Type Operating Cost ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            fuelTO.setUserId(String.valueOf(userId));
            countryId=request.getParameter("countryId");
            fuelTO.setCountryId(countryId);

            if( fuelCommand.getVehicleTypeCostId()!=null && !"".equals(fuelCommand.getVehicleTypeCostId()) ){
                fuelTO.setVehicleTypeOperatingCostId( fuelCommand.getVehicleTypeCostId());
            }
            if( fuelCommand.getVehicleTypeIds()!=null && !"".equals(fuelCommand.getVehicleTypeIds()) ){
                fuelTO.setVehicletypeid( fuelCommand.getVehicleTypeIds());
            }
            if( fuelCommand.getCostPerKm()!=null && !"".equals(fuelCommand.getCostPerKm()) ){
                fuelTO.setCostPerKm( fuelCommand.getCostPerKm());
            }
            if( fuelCommand.getTollCostPerKm()!=null && !"".equals(fuelCommand.getTollCostPerKm()) ){
                fuelTO.setTollGateCostPerKm( fuelCommand.getTollCostPerKm());
            }
            if( fuelCommand.getMiscAmt()!=null && !"".equals(fuelCommand.getMiscAmt()) ){
                fuelTO.setMiscAmt( fuelCommand.getMiscAmt());
            }
            if( fuelCommand.getDriverIncentive()!=null && !"".equals(fuelCommand.getDriverIncentive()) ){
                fuelTO.setDriverVehicleIncentive( fuelCommand.getDriverIncentive());
            }
            if( fuelCommand.getEtcAmount()!=null && !"".equals(fuelCommand.getEtcAmount()) ){
                fuelTO.setEtcAmount( fuelCommand.getEtcAmount());
            }
            insertStatus=fuelBP.saveVehicleTypeOperatingCost(fuelTO);
            if(insertStatus>0){
                request.setAttribute("successMessage", "Record Saved SuccessFully");
            }else{
                    request.setAttribute("errorMessage", "Record Saved Not Saved");
            }
            ArrayList countryList=new ArrayList();
            countryList=fuelBP.getCountryList();
            request.setAttribute("countryList", countryList);


            ArrayList vehicleTypeCostList=new ArrayList();
            vehicleTypeCostList=fuelBP.getVehicleTypeCostList(fuelTO);
            //////System.out.println("vehicleTypeCostList size in COntroller :::"+vehicleTypeCostList.size());
            if(vehicleTypeCostList.size()>0){
            request.setAttribute("vehicleTypeCostList", vehicleTypeCostList);
            }else{
                request.setAttribute("errorMessage", "No Records Found");
            }

            path = "content/fuelManagement/vehicleTypeCostMaster.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Alter Fuel Price  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

     
     public ModelAndView viewMfrMilleageAgeing(HttpServletRequest request, HttpServletResponse reponse, FuelManagementCommand command) throws IOException {
//         //////System.out.println("FuelPriceMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        fuelCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int savefuelPriceMaster = 0;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Operation  >> fuelPriceMaster ";
        String pageTitle = "Save fuelpriceMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            //////System.out.println("fuelCommand.getMfrId() = " + fuelCommand.getMfrId());
            if(fuelCommand.getMfrId() != "0"){
            fuelTO.setMfrId( fuelCommand.getMfrId());
            ArrayList mfrMilleageAgeing = new ArrayList();
            mfrMilleageAgeing = fuelBP.viewMfrMilleageAgeing(fuelTO);
            request.setAttribute("mfrMilleageAgeing", mfrMilleageAgeing);
            path = "content/BrattleFoods/viewMfrMilleageDetails.jsp";
            }else{
            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processActiveMfrList();
            request.setAttribute("MfrList", MfrList);
            path = "content/BrattleFoods/mfrMilleageConfig.jsp";
            }
            

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }
     
    public void saveMfrMilleageAgeing(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        fuelCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        int madhavaramSp = 1011;
        FuelManagementTO fuelTO = new FuelManagementTO();
        try {
            if (fuelCommand.getAgeingId() != null && !"".equals(fuelCommand.getAgeingId())) {
                fuelTO.setAgeingId(fuelCommand.getAgeingId());
            }
            if (fuelCommand.getMfrId() != null && !"".equals(fuelCommand.getMfrId())) {
                fuelTO.setMfrId(fuelCommand.getMfrId());
            }
            if (fuelCommand.getMfrAge()!= null && fuelCommand.getMfrAge() != "") {
                fuelTO.setMfrAge(fuelCommand.getMfrAge());
            }
            if (fuelCommand.getMfrToAge()!= null && fuelCommand.getMfrToAge() != "") {
                fuelTO.setMfrToAge(fuelCommand.getMfrToAge());
            }
            if (fuelCommand.getStatus()!= null && fuelCommand.getStatus() != "") {
                fuelTO.setStatus(fuelCommand.getStatus());
            }
            int updateStatus = 0;
            updateStatus = fuelBP.saveMfrMilleageAgeing(fuelTO,userId);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        } 
    }
    
   public ModelAndView viewMfrMilleageConfiguration(HttpServletRequest request, HttpServletResponse reponse, FuelManagementCommand command) throws IOException {
//         //////System.out.println("FuelPriceMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        fuelCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int savefuelPriceMaster = 0;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Operation  >> fuelPriceMaster ";
        String pageTitle = "Save fuelpriceMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            //////System.out.println("fuelCommand.getMfrId() = " + fuelCommand.getMfrId());
            if(fuelCommand.getMfrId() != "0"){
            fuelTO.setMfrId( fuelCommand.getMfrId());
            ArrayList viewMfrModelList = new ArrayList();
            viewMfrModelList = fuelBP.viewMfrModelList(fuelTO);
            request.setAttribute("viewMfrModelList", viewMfrModelList);
            path = "content/BrattleFoods/viewMfrModelList.jsp";
            }else{
            ArrayList modelList = new ArrayList();
            VehicleTO vehicleTO = new VehicleTO();
            vehicleTO.setFleetTypeId("1");
            modelList = vehicleBP.processGetModelList(vehicleTO);
            request.setAttribute("MfrList", modelList);
            path = "content/fuelManagement/mfrMilleageConfig.jsp";
            }
            

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    } 
   
   public ModelAndView viewMfrMilleageConfigurationDetails(HttpServletRequest request, HttpServletResponse reponse, FuelManagementCommand command) throws IOException {
//         //////System.out.println("FuelPriceMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        fuelCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int savefuelPriceMaster = 0;
        FuelManagementTO fuelTO = new FuelManagementTO();
        String menuPath = "";
        menuPath = "Operation  >> fuelPriceMaster ";
        String pageTitle = "Save fuelpriceMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            if(fuelCommand.getMfrId() != "0"){
            fuelTO.setMfrId( fuelCommand.getMfrId());
            }
            if(fuelCommand.getModelId() != "0"){
            fuelTO.setModelId( fuelCommand.getModelId());
            }
            if(fuelCommand.getVehicleTypeId() != "0"){
            fuelTO.setVehicleTypeId( fuelCommand.getVehicleTypeId());
            }
            ArrayList viewMfrMilleageConfigDetails = new ArrayList();
            viewMfrMilleageConfigDetails = fuelBP.viewMfrMilleageConfigDetails(fuelTO);
            request.setAttribute("viewMfrMilleageConfigDetails", viewMfrMilleageConfigDetails);
            path = "content/BrattleFoods/milleageDetailUpdate.jsp";
            

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    } 
   
   
   public void upateMfrMilleage(HttpServletRequest request, HttpServletResponse response, FuelManagementCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        fuelCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");
        int companyType = (Integer) session.getAttribute("companyTypeId");
        String companyId = (String) session.getAttribute("companyId");
        int madhavaramSp = 1011;
        FuelManagementTO fuelTO = new FuelManagementTO();
        try {
            if (fuelCommand.getKmPerLitter()!= null && fuelCommand.getKmPerLitter() != "") {
                fuelTO.setKmPerLitter(fuelCommand.getKmPerLitter());
            }
            if (fuelCommand.getKmPerLitterLoaded()!= null && fuelCommand.getKmPerLitterLoaded() != "") {
                fuelTO.setKmPerLitterLoaded(fuelCommand.getKmPerLitterLoaded());
            }
            if (fuelCommand.getAgeingId()!= null && fuelCommand.getAgeingId() != "") {
                fuelTO.setAgeingId(fuelCommand.getAgeingId());
            }
            if (fuelCommand.getVehicleTypeId() != "0") {
                fuelTO.setVehicleTypeId(fuelCommand.getVehicleTypeId());
            }
            if (fuelCommand.getModelId()!= "0") {
                fuelTO.setModelId(fuelCommand.getModelId());
            }
            if (fuelCommand.getMfrId()!= "0") {
                fuelTO.setMfrId(fuelCommand.getMfrId());
            }
            int updateStatus = 0;
            updateStatus = fuelBP.updateMfrMilleage(fuelTO,userId);
            response.setContentType("text/css");
            if (updateStatus > 0) {
                response.getWriter().println(updateStatus);
            } else {
                response.getWriter().println(0);
            }
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        } 
    }
 public ModelAndView fuelPriceApprovalList(HttpServletRequest request, HttpServletResponse reponse, OperationCommand command) throws IOException {

//         System.out.println("FuelPriceMaster...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        operationCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        int savefuelPriceMaster = 0;
        OperationTO operationTO = new OperationTO();
        String menuPath = "";
        menuPath = "Operation  >> fuelPriceMaster ";
        String pageTitle = "Save fuelpriceMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            int status = 0;
            ArrayList viewfuelPriceMaster = new ArrayList();
            viewfuelPriceMaster = operationBP.getFuelPriceApprovalList();
            System.out.println("viewfuelPriceMaster" + viewfuelPriceMaster);
            request.setAttribute("viewfuelPriceMaster", viewfuelPriceMaster);
            path = "content/BrattleFoods/fuelPriceApprovalList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }
   
}



