<%-- 
    Document   : invoiceReopen
    Created on : Mar 10, 2021, 4:20:13 PM
    Author     : hp
--%>




<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>


<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('invoiceNo').value == '') {
            alert("Please select Invoice No.");
            document.getElementById('invoiceNo').focus();
        } 
         else if (value == "Update") {
            document.BPCLTransaction.action = '/throttle/handleUpdateInvoiceReopen.do?param=Update';
            document.BPCLTransaction.submit();
        }
          else if (value == "search") {
         
            document.BPCLTransaction.action = '/throttle/handleUpdateInvoiceReopen.do?param=search';
            document.BPCLTransaction.submit();


    }}
function submit2(value) {
          if (value == "creditUpdate") {
             alert("hiiiiii");
            document.BPCLTransaction.action = '/throttle/handleUpdateInvoiceReopen.do?param=creditUpdate';
            document.BPCLTransaction.submit();


    }}
//    function submit1(value) {
//        alert(value);
//        if (document.getElementById('invoiceNo').value == '') {
//            alert("Please select Invoice No.");
//            document.getElementById('invoiceNo').focus();
//        } 
//         else if (value == "search") {
//             alert("hiiiiii");
//            document.BPCLTransaction.action = '/throttle/handleUpdateInvoiceReopen.do?param=search';
//            document.BPCLTransaction.submit();
//        }
//
//
//    }
</script> 

<script>
    $(document).ready(function() {
        $('#invoiceNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getInvoiceNo.do",
                    dataType: "json",
                    data: {
                        invoiceNo: request.term,invoiceType:$("#invoiceType").val()
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#invoiceNo').val('');
                            $('#customerId').val('');
                            $('#invoiceId').val('');
                            $('#creditNoteId').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#invoiceNo').val(tmp[0]);
                $('#customerId').val(tmp[1]);
                $('#custName').val(tmp[2]);
                $('#invoiceId').val(tmp[3]);
                $('#creditNoteId').val(tmp[4]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>   





<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Invoice Reopen</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Billing</a></li>
            <li class="active">Invoice Reopen</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="BPCLTransaction" method="post">
                    <%@ include file="/content/common/message.jsp"%>

                    <table  class="table table-info mb30 table-hover" style="width: 100%">
                        <tr> 
                            <td><font color="red">*</font>Invoice Type</td>
                                <td><select name="invoiceType" id="invoiceType" class="form-control" style="width:240px;height:40px;">
                                    
                                        <option value="0">---Select---</option>
                                        <option value="1">Main Invoice</option>
                                        <option value="2">supplementary</option>
                                </select> 
                            </td>
                        <script>
                            document.getElementById("invoiceType").value=<c:out value="${invoiceType}" />;
                            </script>
                            <td><font color="red">*</font>Invoice No</td>
                            <td height="30">
                                <input name="invoiceNo" id="invoiceNo" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${invoiceno}" />">
                            </td>

                            <td >Billing Party</td>
                            <td height="30" >
                                <input readonly name="custName" id="custName" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${custName}" />">
                                <input name="customerId" id="customerId" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${customerId}" />"> 
                                <input name="invoiceId" id="invoiceId" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${invoiceId}" />"> 
                                <input name="creditNoteId" id="creditNoteId" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${creditNoteId}" />"> 
                            </td>          

                            <td style="display:none"><font color="red">*</font>Change In Billing Party</td>
                            <td style="display:none"><select name="billingParty" id="billingParty" class="form-control" style="width:240px;height:40px;">
                                    <c:if test="${billingParty != null}">
                                        <option value="">---select-----</option>
                                        <c:forEach items="${billingParty}" var="cust">
                                            <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                        </c:forEach> 
                                    </c:if>
                                </select> 
                            </td> 

                        </tr>

                    </table>

                    <center>
                        <input type="button" class="btn btn-info" name="update" onclick="submitPage(this.value);" value="Update">  
                        <input type="button" class="btn btn-info" name="search" onclick="submitPage(this.value);" value="search">  
                    </center>


            </body>
               <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                            <thead >
                                <tr>
                                    <th>S.No</th>
                            <th>Trip Id</th>
                            <th>Gr. No.</th>
                            <th>Gr. Date</th>
                            <th>Customer Id</th>
                            <th>Customer Name</th>
                           
                            <th height="30" ><div ><spring:message code="settings.label.Details" text="Select All"/>&nbsp;<input type="checkbox" id="selectAll" onclick="selectAllMenus()" /> </div></th>
                            </tr>
                            </thead>
                            <script>
                                function selectAllMenus() {
                                    var selectAll = document.getElementById("selectAll").checked;
                                    var functionStatus = document.getElementsByName("selectedIndex");
                                    if (selectAll == true) {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = true;
                                        }
                                    } else {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = false;
                                        }
                                    }
                                    
                                }
                                    function selectedIds(sno)
                                    {  
                                        var z=1;
                                        var x=0;
                                        var sno=sno;
                                        var check=document.getElementById("checkedIndexs"+sno).value;
                                        if(check == 0){
                                            document.getElementById("checkedIndexs"+sno).value=z;
                                        }
                                        if(check == 1){
                                            document.getElementById("checkedIndexs"+sno).value = x;
                                        }
                                    }
                            </script>
                            <tbody>
                                <%int sno=1;
                                int index =0;
                                String classText="";
                                %>
                                <c:forEach items="${invoicecreditList}" var="closedBillList">
                                    <%int oddEven = index % 2;
                                  if (oddEven > 0) {
                                             classText = "text2";
                                         } else {
                                             classText = "text1";
                                         }
                                    %>
                                    <tr>
                                        <td  height="30"><%=sno++%></td>
                                   
                                        <td  height="30"><c:out value="${closedBillList.tripId}"/>
                                <input name="tripId" id="tripId<%=index%>" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${closedBillList.tripId}" />"></td>
                                        <td  height="30"><c:out value="${closedBillList.grNo}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.grDate}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.customerId}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.customerName}"/></td>
                                        <td height="30" ><input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" onchange="selectedIds(<%=index%>)" value="<c:out value="${closedBillList.tripId}"/>"/>
                                <input name="checkedIndexs" id="checkedIndexs<%=index%>" type="text" class=" form-control" style="width:240px;height:40px;" value="0"></td>
                                   
                                        
                                        
                                          
                                        
                                      
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                            <center>    <input type="button" class="btn btn-info" name="creditUpdate" onclick="submit2(this.value);" value="creditUpdate"></center>  
                </form>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
