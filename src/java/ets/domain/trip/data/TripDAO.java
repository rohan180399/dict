/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException; 

import ets.arch.exception.FPRuntimeException;

import ets.domain.trip.business.TripTO;
import ets.domain.trip.business.PointTO;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.programmingfree.excelexamples.calculateDistance;
//
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;   
import org.json.simple.parser.JSONParser;
import org.json.*;

//
import ets.domain.util.FPLogUtils;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.Iterator;

import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

//import com.Service;
//import com.ServiceSoap;
//import com.UpdateOrderStatusResponse.UpdateOrderStatusResult;
//import com.CreateTMSAccountDetailResponse.CreateTMSAccountDetailResult;
//import com.ArrayOfTMSModel;
//import com.TMSModel;
import java.text.SimpleDateFormat;
import java.util.List;

//import com.efsapi.Service;
//import com.efsapi.ServiceSoap;
//import com.efsapi.UpdateOrderStatusResponse.UpdateOrderStatusResult;
import java.util.List;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripDAO extends SqlMapClientDaoSupport {

    /**
     * Creates a new instance of __NAME__
     */
    public TripDAO() {
    }
    private final static String CLASS = "TripDAO";

    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentList(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList consignmentList = new ArrayList();
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentList", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public ArrayList getTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map:---" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getVehicleTypeContainerTypeRates(ArrayList consignmentList, ArrayList orderPointDetails) {
        Map map = new HashMap();
        String movementType = "";
        String productCategory = "";
        String contractId = "";
        Iterator itr = consignmentList.iterator();
        TripTO tripTONew = new TripTO();
        while (itr.hasNext()) {
            tripTONew = new TripTO();
            tripTONew = (TripTO) itr.next();
            movementType = tripTONew.getMovementType();
            productCategory = tripTONew.getProductInfo();
            contractId = tripTONew.getRouteContractId() + "";
        }
        map.put("movementType", movementType);
        map.put("productCategory", productCategory);
        map.put("contractId", contractId);
        String firstPointId = "";
        String finalPointId = "";
        String point1Id = "";
        String point2Id = "";
        String point3Id = "";
        String point4Id = "";
        itr = orderPointDetails.iterator();
        int cntr = 0;
        int size = orderPointDetails.size();
        size--;
        System.out.println("size:" + size);
        PointTO pointTO = new PointTO();
        while (itr.hasNext()) {
            System.out.println("size:" + size + "  cntr:" + cntr);
            pointTO = new PointTO();
            pointTO = (PointTO) itr.next();
            if (cntr == 0) {
                firstPointId = pointTO.getPointId();
            } else if (cntr == size) {
                finalPointId = pointTO.getPointId();
            } else {
                if (cntr == 1) {
                    point1Id = pointTO.getPointId();
                }
                if (cntr == 2) {
                    point2Id = pointTO.getPointId();
                }
                if (cntr == 3) {
                    point3Id = pointTO.getPointId();
                }
                if (cntr == 4) {
                    point4Id = pointTO.getPointId();
                }
            }
            cntr++;
        }
        map.put("firstPointId", firstPointId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("point3Id", point3Id);
        map.put("point4Id", point4Id);
        map.put("finalPointId", finalPointId);
        ArrayList vehicleTypeContainerTypeRates = new ArrayList();

        System.out.println("map:---" + map);
        //vehicleTypeId~containerTypeId~containerQty~loadedType
        String inputType = "1058~1~1~1,1059~2~1~1,1059~1~2~1,1058~1~1~2,1059~2~1~2,1059~1~2~2";
        String[] temp = inputType.split(",");
        String[] temp1 = null;
        String rate = "";
        try {
            for (int i = 0; i < temp.length; i++) {
                tripTONew = new TripTO();
                temp1 = temp[i].split("~");
                map.put("vehicleTypeId", temp1[0]);
                map.put("containerTypeId", temp1[1]);
                map.put("containerQty", temp1[2]);
                map.put("loadTypeId", temp1[3]);
                tripTONew.setVehicleTypeId(temp1[0]);
                tripTONew.setContainerTypeId(temp1[1]);
                tripTONew.setContainerQty(temp1[2]);
                tripTONew.setLoadType(temp1[3]);
                System.out.println("map:" + map);
                Object rateObj = getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates", map);
                System.out.println("rateObj:" + rateObj);
                if (rateObj != null) {
                    rate = (String) rateObj;
                    System.out.println("rate:" + rate);
                    tripTONew.setCurrRate(rate);
                    vehicleTypeContainerTypeRates.add(tripTONew);
                }
            }
            System.out.println("vehicleTypeContainerTypeRates size=" + vehicleTypeContainerTypeRates.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeContainerTypeRates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeContainerTypeRates List", sqlException);
        }

        return vehicleTypeContainerTypeRates;
    }

    public ArrayList getTripsToBeBilledDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsToBeBilledDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getSecTripsToBeBilledDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecTripsToBeBilledDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetails", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetails List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetailExpenses(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetailExpenses", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetailExpenses List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceSupplementDetailExpenses(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceSupplementDetailExpenses", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetailExpenses List", sqlException);
        }

        return result;
    }

    public ArrayList getTripPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripPointDetails = new ArrayList();
//        try {
//            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetails", map);
//            System.out.println("getTripPointDetails size=" + tripPointDetails.size());
        System.out.println("map for point:" + map);
        try {

            if ("2".equals(tripTO.getTripType())) {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetailsSecondary", map);
                System.out.println("getTripPointDetails arun  1111  size=" + tripPointDetails.size());
            } else {

                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetails", map);
                System.out.println("getTripPointDetails  arun  222  size=" + tripPointDetails.size());
            }

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getOrderPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        System.out.println("map getOrderPointDetailss" + map);
        ArrayList tripPointDetails = new ArrayList();
        try {

//          if(tripTO.getOrderType().equals("2") && tripTO.getOrderStatus().equals("25")){
//
//            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderPointDetailsForFCL", map);
//            System.out.println("getOrderPointDetails size=" + tripPointDetails.size());
//          }else{
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderPointDetails", map);
            System.out.println("getOrderPointDetails size=" + tripPointDetails.size());
//          }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getHubPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList tripPointDetails = new ArrayList();
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHubPointDetails", map);
            System.out.println("gethubPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getDropPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList tripPointDetails = new ArrayList();
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDropPointDetails", map);
            System.out.println("getDropPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return tripPointDetails;
    }

    public ArrayList getwareHouseDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList wareHouseDetail = new ArrayList();
        try {
            wareHouseDetail = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getwareHouseDetail", map);
            System.out.println("wareHouseDetail size=" + wareHouseDetail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return wareHouseDetail;
    }

    public ArrayList getPortDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList portDetail = new ArrayList();
        try {
            portDetail = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPortDetail", map);
            System.out.println("wareHouseDetail size=" + portDetail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return portDetail;
    }

    public ArrayList getVehicleRegNos(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", "1");
            map.put("vendorId", "");
        } else {
            map.put("vendorId", tripTO.getVendorId());
            map.put("ownerShip", "");
        }
        if (tripTO.getVehicleTypeId() != null && !"".equals(tripTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId().split("-")[0]);
        } else {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        }

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNos", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getTrailerNos(TripTO tripTO) {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList trailerNos = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerNos", map);
            System.out.println("trailerNos size=" + trailerNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return trailerNos;
    }

    public ArrayList getTrailerNos() {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList trailerNos = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerNos", map);
            System.out.println("trailerNos size=" + trailerNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return trailerNos;
    }

    public ArrayList getVehicleRegNosForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForEmptyTrip", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForUpload(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo() + "%";
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleNo", vehicleNo);
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            String tripStatus = "";
            String jobcardStatus = "";

            tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleRegNosForUploadStatus", map);
            System.out.println("vehicleStatus = " + tripStatus);
            if (tripStatus == null) {
                map.put("tripStatus", 0);
                jobcardStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getJobcardForUploadStatus", map);
                if (jobcardStatus == null) {
                    map.put("tripStatus", 2);
                } else {
                    map.put("tripStatus", 0);
                }
            } else {
                map.put("tripStatus", 1);
            }

            System.out.println("map = " + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForUpload", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNosForUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNosForUpload List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getDrivers(TripTO tripTO) {
        Map map = new HashMap();
        String driverName = tripTO.getDriverName() + "%";
        map.put("driverName", driverName);

        ArrayList drivers = new ArrayList();
        try {
            drivers = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDrivers", map);
            System.out.println("drivers size=" + drivers.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return drivers;
    }

    public String getConsignmentOrderRevenue(String orderId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String revenue = "";
        try {
            map.put("orderId", orderId);
            System.out.println("map = " + map);
            revenue = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderRevenue", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("revenue=" + revenue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderRevenue", sqlException);
        }
        return revenue;
    }

    public String getVehicleMileageAndTollRate(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map...:" + map);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleMileageAndTollRate", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMileageAndTollRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleMileageAndTollRate", sqlException);
        }
        return response;
    }

    public String getEstimatedTripEndDateTime(String tripId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("tripId", tripId);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getEstimatedTripEndDateTime", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEstimatedTripEndDateTime Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEstimatedTripEndDateTime", sqlException);
        }
        return response;
    }

    public String checkPreStartRoute(String preStartLocationId, String originId, String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String info = "";
        try {
            map.put("preStartLocationId", preStartLocationId);
            map.put("originId", originId);
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            info = (String) getSqlMapClientTemplate().queryForObject("trip.checkPreStartRoute", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("info=" + info);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkPreStartRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkPreStartRoute", sqlException);
        }
        return info;
    }

    public String getConsignmentOrderExpense(String orderId) {
        Map map = new HashMap();
        String expense = "";
        try {
            map.put("orderId", orderId);
            System.out.println("map:" + map);
            expense = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderExpense", map);
            System.out.println("expense=" + expense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderExpense", sqlException);
        }
        return expense;
    }

    public String getTripCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getTripCodeSequence", map);
            System.out.println("tripCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripCodeSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        String tripCodeSequences = "";
        try {
            tripCodeSequence = (Integer) session.insert("trip.getTripCodeSequence", map);
            tripCodeSequences = tripCodeSequence + "";
            if (tripCodeSequences.length() == 1) {
                tripCodeSequences = "00000" + tripCodeSequences;
                System.out.println("invoice no 1.." + tripCodeSequences);
            } else if (tripCodeSequences.length() == 2) {
                tripCodeSequences = "0000" + tripCodeSequences;
                System.out.println("invoice lenght 2.." + tripCodeSequences);
            } else if (tripCodeSequences.length() == 3) {
                tripCodeSequences = "000" + tripCodeSequences;
                System.out.println("invoice lenght 3.." + tripCodeSequences);
            } else if (tripCodeSequences.length() == 4) {
                tripCodeSequences = "00" + tripCodeSequences;
            } else if (tripCodeSequences.length() == 5) {
                tripCodeSequences = "0" + tripCodeSequences;
            }
            System.out.println("tripCodeSequencestripCodeSequences=" + tripCodeSequences);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequences;
    }

    public String getGrNoSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getGrNoSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getRepoGrNoSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getRepoGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getRepoGrNoSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getRepoGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripChallanSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getChallanNoSequence", map);
            System.out.println("challan No=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripChallanSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getChallanNoSequence", map);
            System.out.println("challan No=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getCnoteCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getCnoteCodeSequence", map);
            System.out.println("getCnoteCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getInvoiceCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getInvoiceCodeSequence", map);
            System.out.println("getInvoiceCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public int saveTripSheet(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int tripId = 0;
        int status = 0;
        try {
            map.put("orgMarketRate", tripTO.getExpense());
            map.put("shipingLineNo", tripTO.getShipingLineNo());
            map.put("billOfEntry", tripTO.getBillOfEntry());
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("customerId", tripTO.getCustomerId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", tripTO.getOrderExpense());
            map.put("orderRevenue", tripTO.getOrderRevenue());
            map.put("halfRateFlag", tripTO.getHalfContractCheck());
            map.put("profitMargin", tripTO.getProfitMargin());
            map.put("hireCharge", tripTO.getHireCharges());

            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            //map.put("pointPlanDate", pointPlanDate[0]);
            //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }

            //map.put("tripScheduleTime", tripTO.getTripScheduleTimeHrs()+":"+tripTO.getTripScheduleTimeMins());
            map.put("statusId", tripTO.getStatusId());
            map.put("transitHours", tripTO.getTripTransitHours());
            map.put("transitDay", tripTO.getTripTransitDays());
            map.put("advnaceToBePaidPerDay", tripTO.getAdvnaceToBePaidPerDay());
            map.put("userId", tripTO.getUserId());
            map.put("emptyTripPurpose", tripTO.getEmptyTripPurpose());
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", tripTO.getReeferRequired());
            map.put("totalWeight", tripTO.getTotalWeight());
            map.put("preStartLocationStatus", tripTO.getPreStartLocationStatus());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("plannedEndDate", tripTO.getTripPlanEndDate());
            map.put("plannedEndTime", tripTO.getTripPlanEndTime());
            map.put("vehicleVendorId", tripTO.getVendorId());

            if (!"".equals(tripTO.getEstimatedKM())) {
                map.put("estimatedKm", tripTO.getEstimatedKM());
            } else {
                map.put("estimatedKm", "0");
            }

            String tripType = tripTO.getTripType();
            if (tripType != null && !"".equals(tripType)) {
                if ("secondary".equalsIgnoreCase(tripType)) {
                    map.put("tripType", "2");
                } else {
                    map.put("tripType", "1");
                }
            } else {
                map.put("tripType", "1");
            }
            System.out.println("map value is:" + map);
            if ("1040".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            tripId = (Integer) session.insert("trip.saveTripSheet", map);
            System.out.println("tripId=" + tripId);

            map.put("tripId", tripId);
            System.out.println("map = " + map);
            status = (Integer) session.update("trip.saveHalfContractRateLog", map);
            System.out.println("status=" + status);

            //int vehicleAvailUpdate= (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int saveTripSheet(TripTO tripTO) {
        Map map = new HashMap();
        int tripId = 0;
        try {
            map.put("shipingLineNo", tripTO.getShipingLineNo());
            map.put("billOfEntry", tripTO.getBillOfEntry());
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("customerId", tripTO.getCustomerId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", tripTO.getOrderExpense());
            map.put("orderRevenue", tripTO.getOrderRevenue());
            map.put("profitMargin", tripTO.getProfitMargin());
            map.put("hireCharge", tripTO.getHireCharges());
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            //map.put("pointPlanDate", pointPlanDate[0]);
            //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }

            //map.put("tripScheduleTime", tripTO.getTripScheduleTimeHrs()+":"+tripTO.getTripScheduleTimeMins());
            map.put("statusId", tripTO.getStatusId());
            map.put("transitHours", tripTO.getTripTransitHours());
            map.put("transitDay", tripTO.getTripTransitDays());
            map.put("advnaceToBePaidPerDay", tripTO.getAdvnaceToBePaidPerDay());
            map.put("userId", tripTO.getUserId());
            map.put("emptyTripPurpose", tripTO.getEmptyTripPurpose());
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());
            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", tripTO.getReeferRequired());
            map.put("totalWeight", tripTO.getTotalWeight());
            map.put("preStartLocationStatus", tripTO.getPreStartLocationStatus());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("plannedEndDate", tripTO.getTripPlanEndDate());
            map.put("plannedEndTime", tripTO.getTripPlanEndTime());
            map.put("vehicleVendorId", tripTO.getVendorId());

            if (!"".equals(tripTO.getEstimatedKM())) {
                map.put("estimatedKm", tripTO.getEstimatedKM());
            } else {
                map.put("estimatedKm", "0");
            }

            String tripType = tripTO.getTripType();
            if (tripType != null && !"".equals(tripType)) {
                if ("secondary".equalsIgnoreCase(tripType)) {
                    map.put("tripType", "2");
                } else {
                    map.put("tripType", "1");
                }
            } else {
                map.put("tripType", "1");
            }
            System.out.println("map value is:" + map);
            if ("1040".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            System.out.println("map = " + map);
            tripId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripSheet", map);
            System.out.println("tripId=" + tripId);

            //int vehicleAvailUpdate= (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int updatePreStartDetails(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("preStartLocationId", tripTO.getPreStartLocationId());
            map.put("preStartLocationPlanDate", tripTO.getPreStartLocationPlanDate());
            map.put("preStartLocationPlanTimeHrs", tripTO.getPreStartLocationPlanTimeHrs());
            map.put("preStartLocationPlanTimeMins", tripTO.getPreStartLocationPlanTimeMins());
            map.put("preStartLocationDistance", tripTO.getPreStartLocationDistance());
            map.put("preStartLocationDurationHrs", tripTO.getPreStartLocationDurationHrs());
            map.put("preStartLocationDurationMins", tripTO.getPreStartLocationDurationMins());
            map.put("preStartLocationVehicleMileage", tripTO.getPreStartLocationVehicleMileage());
            map.put("preStartLocationTollRate", tripTO.getPreStartLocationTollRate());
            map.put("preStartLocationRouteExpense", tripTO.getPreStartLocationRouteExpense());

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.updatePreStartDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int updatePreStartDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("preStartLocationId", tripTO.getPreStartLocationId());
            map.put("preStartLocationPlanDate", tripTO.getPreStartLocationPlanDate());
            map.put("preStartLocationPlanTimeHrs", tripTO.getPreStartLocationPlanTimeHrs());
            map.put("preStartLocationPlanTimeMins", tripTO.getPreStartLocationPlanTimeMins());
            map.put("preStartLocationDistance", tripTO.getPreStartLocationDistance());
            map.put("preStartLocationDurationHrs", tripTO.getPreStartLocationDurationHrs());
            map.put("preStartLocationDurationMins", tripTO.getPreStartLocationDurationMins());
            map.put("preStartLocationVehicleMileage", tripTO.getPreStartLocationVehicleMileage());
            map.put("preStartLocationTollRate", tripTO.getPreStartLocationTollRate());
            map.put("preStartLocationRouteExpense", tripTO.getPreStartLocationRouteExpense());

            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreStartDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripStatusDetails(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            map.put("userId", tripTO.getUserId());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripStatusDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            map.put("userId", tripTO.getUserId());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripVehicle(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());

//            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
//            map.put("trailerId", tripTO.getTrailerId());
//            map.put("seatCapacity", tripTO.getSeatCapacity());
            if (tripTO.getVehicleCapUtil() != "") {
                map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            } else {
                map.put("vehicleCapUtil", "0");
            }
            if (tripTO.getTrailerId() != "") {
                map.put("trailerId", tripTO.getTrailerId());
            } else {
                map.put("trailerId", "0");
            }
            if (tripTO.getSeatCapacity() != "") {
                map.put("seatCapacity", tripTO.getSeatCapacity());
            } else {
                map.put("seatCapacity", "0");
            }
            if ("0".equals(tripTO.getVehicleId())) {
                map.put("hireVehilceNo", tripTO.getHireVehicleNo());
            }

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);

            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();

            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }
            map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
            map.put("originId", tripTO.getOriginId());
            System.out.println("map for Avail:");
            int vehicleAvailUpdate = (Integer) session.update("trip.updateVehicleAvailabilityForFreeze", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicle(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());

//            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
//            map.put("trailerId", tripTO.getTrailerId());
//            map.put("seatCapacity", tripTO.getSeatCapacity());
            if (tripTO.getVehicleCapUtil() != "") {
                map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            } else {
                map.put("vehicleCapUtil", "0");
            }
            if (tripTO.getTrailerId() != "") {
                map.put("trailerId", tripTO.getTrailerId());
            } else {
                map.put("trailerId", "0");
            }
            if (tripTO.getSeatCapacity() != "") {
                map.put("seatCapacity", tripTO.getSeatCapacity());
            } else {
                map.put("seatCapacity", "0");
            }
            if ("0".equals(tripTO.getVehicleId())) {
                map.put("hireVehilceNo", tripTO.getHireVehicleNo());
            }

            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);

            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();

            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }
            map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
            map.put("originId", tripTO.getOriginId());
            System.out.println("map for Avail:");
            int vehicleAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicleForVehicleChange(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "22");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicleForVehicleChange", map);
            if (status > 0) {
                if (tripTO.getReasonForChange() == 2) {
                    map.put("accVehicleNo", tripTO.getAccVehicleId());
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateAccidentDetail", map);
                }
            }
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleChangeTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleChangeTripAdvanceDetails", map);
            System.out.println("getVehicleChangeTripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int clearVehicleAndDriverMapping(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            if ("".equals(tripTO.getLastVehicleEndKm()) || tripTO.getLastVehicleEndKm() == null) {
                map.put("endKM", 0);
            } else {
                map.put("endKM", tripTO.getLastVehicleEndKm());
            }
            if ("".equals(tripTO.getLastVehicleEndOdometer()) || tripTO.getLastVehicleEndOdometer() == null) {
                map.put("endHM", 0);
            } else {
                map.put("endHM", tripTO.getLastVehicleEndOdometer());
            }
            if (!"".equals(tripTO.getVehicleChangeDate())) {
                map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            }
            if (!"".equals(tripTO.getVehicleChangeHour()) && !"".equals(tripTO.getVehicleChangeMinute())) {
                map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            }
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripVehicle", map);
            System.out.println("removeTripVehicle=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripDriver", map);
            System.out.println("removeTripDriver=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "clearVehicleAndDriverMapping", sqlException);
        }
        return status;
    }

    public int saveTripDriver(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("primaryDriverName", tripTO.getPrimaryDriverName());
            map.put("type", "P");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripDriver", map);
//            if (!"0".equals(tripTO.getSecondaryDriver1Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver1Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }
//            if (!"0".equals(tripTO.getSecondaryDriver2Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver2Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }

            System.out.println("saveTripDriver=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int saveTripDriver(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("primaryDriverName", tripTO.getPrimaryDriverName());
            map.put("type", "P");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            if (!"0".equals(tripTO.getSecondaryDriver1Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver1Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }
//            if (!"0".equals(tripTO.getSecondaryDriver2Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver2Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }

            System.out.println("saveTripDriver=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int saveTripConsignments(TripTO tripTO, String remainWeight, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        String statusID = "";
        String repoId = "";
        int newRepoId = 0;
        int plannedstatus = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("vehicleId", tripTO.getVehicleId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            int j = 1;
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("orderSequence", j);
                j++;
                System.out.println("map value is:" + map);
                statusID = (String) session.queryForObject("trip.getConsignmentStatus", map);
                repoId = (String) session.queryForObject("trip.getConsignmentRepoId", map);
                plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
                System.out.println("statusID:" + statusID + " order id:" + tripTO.getOrderType());
                if ("1".equals(tripTO.getRepoId()) || "1".equals(repoId)) {
                    if (plannedstatus > 0) {
                        // newRepoId= Integer.parseInt(repoId)+ Integer.parseInt(tripTO.getRepoId());
                        newRepoId = 1;
                    } else {
                        newRepoId = 2;
                    }
                } else {
                    newRepoId = Integer.parseInt(tripTO.getRepoId());
                }
//                if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("5")) {
//                    System.out.println("statusID:1");
//                    map.put("consignmentStatusId", "23");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("25")) {
//
//                    map.put("consignmentStatusId", "26");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("28")) {
//                    map.put("consignmentStatusId", "29");
//                } else {
                map.put("consignmentStatusId", "8");
                map.put("remainWeight", remainWeight);
                map.put("repoId", newRepoId);
                System.out.println("statusID:2");
//                }
                System.out.println("map2 value is:" + map);
                status = (Integer) session.update("trip.saveTripConsignments", map);
                status = (Integer) session.update("trip.updateConsignmentOrderStatus", map);
            }
            System.out.println("saveTripConsignments=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }

    public int saveTripConsignments(TripTO tripTO, String remainWeight) {
        Map map = new HashMap();
        int status = 0;
        String statusID = "";
        String repoId = "";
        int newRepoId = 0;
        int plannedstatus = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("vehicleId", tripTO.getVehicleId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            int j = 1;
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("orderSequence", j);
                j++;
                System.out.println("map value is:" + map);
                statusID = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentStatus", map);
                repoId = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentRepoId", map);
                plannedstatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainer", map);
                System.out.println("statusID:" + statusID + " order id:" + tripTO.getOrderType());
                if ("1".equals(tripTO.getRepoId()) || "1".equals(repoId)) {
                    if (plannedstatus > 0) {
                        // newRepoId= Integer.parseInt(repoId)+ Integer.parseInt(tripTO.getRepoId());
                        newRepoId = 1;
                    } else {
                        newRepoId = 2;
                    }
                } else {
                    newRepoId = Integer.parseInt(tripTO.getRepoId());
                }
//                if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("5")) {
//                    System.out.println("statusID:1");
//                    map.put("consignmentStatusId", "23");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("25")) {
//
//                    map.put("consignmentStatusId", "26");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("28")) {
//                    map.put("consignmentStatusId", "29");
//                } else {
                map.put("consignmentStatusId", "8");
                map.put("remainWeight", remainWeight);
                map.put("repoId", newRepoId);
                System.out.println("statusID:2");
//                }
                System.out.println("map2 value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripConsignments", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderStatus", map);
            }
            System.out.println("saveTripConsignments=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }

    public int saveAction(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveAction", map);
                System.out.println("saveAction=" + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveClosureApprovalRequest(String tripId, String requestType, String remarks, int userId, String rcmExp, String nettExp) {
        Map map = new HashMap();
        int status = 0;
        try {
            if (nettExp == null || "".equals(nettExp)) {
                nettExp = "0";
            }
            if (rcmExp == null || "".equals(rcmExp)) {
                rcmExp = "0";
            }
            map.put("tripId", tripId);
            map.put("requestType", requestType);
            map.put("remarks", remarks);
            map.put("userId", userId);
            map.put("rcmExp", rcmExp);
            map.put("nettExp", nettExp);
            String kmHmInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getTripKmHm", map);
            //String rcmSystemExpInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getRcmSystemExpense", map);
            String[] temp = kmHmInfo.split("-");
            map.put("kmRun", temp[0]);
            map.put("hmRun", temp[1]);
            map.put("aprovedStatus", "1");
            System.out.println("map in the trip dao = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveClosureApprovalRequest", map);
            System.out.println("saveClosureApprovalRequest=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveClosureApprovalRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveClosureApprovalRequest", sqlException);
        }
        return status;
    }

    public int saveBillingRevenueChangeApprovalRequest(String customerId, String requestType, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("userId", userId);
            map.put("customerId", customerId);
            map.put("requestType", requestType);
            map.put("remarks", remarks);

            System.out.println("map in the trip dao = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillingRevenueApprovalRequest", map);
            System.out.println("saveBillingRevenueApprovalRequest=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveClosureApprovalRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveClosureApprovalRequest", sqlException);
        }
        return status;
    }

    public int saveTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());

            map.put("tripId", tripTO.getTripId());
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);

            //update trip master
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
//            updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            //       updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            System.out.println("saveAction=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlan(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < pointId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                map.put("pointAddresss", pointAddresss[i]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("Route map value is:" + map);
                status = (Integer) session.update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }
            System.out.println("");
            if (tripTO.getActionName() != null && "1".equals(tripTO.getActionName()) && !"".equals(tripTO.getPreStartLocationId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("route map value is:" + map);
                    status = (Integer) session.update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }
//                 updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            status = (Integer) session.update("trip.updateContainerConsignmentId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlan(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < pointId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                map.put("pointAddresss", pointAddresss[i]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("Route map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }
            System.out.println("");
            if (tripTO.getActionName() != null && "1".equals(tripTO.getActionName()) && !"".equals(tripTO.getPreStartLocationId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("route map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }
//                 updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            status = (Integer) getSqlMapClientTemplate().update("trip.updateContainerConsignmentId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlanWhenTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("tripSheetId", tripTO.getTripCode());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());

        map.put("userId", tripTO.getUserId());
        System.out.println("map ...." + map);
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        System.out.println("map =-------------------------------- " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getStatusDetails() {
        Map map = new HashMap();
        System.out.println("map = " + map);
        ArrayList response = new ArrayList();
        try {
            response = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusMaster", map);
            System.out.println("response size=" + response.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusDetails List", sqlException);
        }

        return response;
    }

    //       Arul Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripDetails(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripDetail", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripInformation(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripInformation", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDriverCount(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String driverCount = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            driverCount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverCount", map);
            System.out.println("driverCount=" + driverCount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverCount", sqlException);
        }
        return driverCount;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripAdvance(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripAdvance = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.getTripAdvance", map);
            System.out.println("tripAdvance=" + tripAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvance", sqlException);
        }
        return tripAdvance;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripExpense(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpense", map);
            System.out.println("tripExpense=" + tripExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    public String getFuelPrice(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);

            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getDieselPrice", map);
            System.out.println("getFuelPrice=" + tripExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    public String getFuelExpense(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);

            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelExpense", map);
            System.out.println("getFuelExpense=" + tripExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertSettlement(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        int insertSettlement = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        try {
            insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertSettlement", map);
            closureSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getClosureSize", map);
            settlementSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getSettlementSize", map);
            if (insertSettlement > 0 && closureSize == settlementSize) {
                updateSetttlementMaster = (Integer) getSqlMapClientTemplate().update("trip.updateSetttlementMaster", map);
                // updateOrderStatusToEFS(tripSheetId, "14");
                updateSettlementDetail = (Integer) getSqlMapClientTemplate().update("trip.updateSettlementDetail", map);
                int status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
                //      updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertSettlement;
    }

    public int insertTripDriverSettlementDetails(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        int insertTripDriverSettlementDetails = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        int driverChangeStatus = 0;
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        ArrayList driverClosureList = new ArrayList();
        try {
            driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
            System.out.println("driverChangeStatus = " + driverChangeStatus);
            Float totalKm = 0.0f;
            Float totalHm = 0.0f;
            Float advancePaid = 0.0f;
            int totalTripDays = 0;
            String driverInTrip = "";
            String[] tempDriverInTrip = null;
            int driverCount = 0;
            if (driverChangeStatus > 0) {
                // Driver Cnage Case
                inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
                Iterator itr1 = inActiveVehicleDriverlist.iterator();
                TripTO tpTO = new TripTO();
                while (itr1.hasNext()) {
                    tpTO = new TripTO();
                    tpTO = (TripTO) itr1.next();
                    if (tpTO.getDriverInTrip().contains(",")) {
                        tempDriverInTrip = driverInTrip.split(",");
                        for (int i = 0; i < tempDriverInTrip.length; i++) {
                            map.put("driverId", tempDriverInTrip[i]);
                            driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                            Iterator itr = driverClosureList.iterator();
                            TripTO trpTO = new TripTO();
                            while (itr.hasNext()) {
                                trpTO = new TripTO();
                                trpTO = (TripTO) itr.next();
                                map.put("tripId", tripSheetId);
                                map.put("tripSheetId", tripSheetId);
                                map.put("driverId", trpTO.getDriverId());
                                map.put("estimatedExpense", trpTO.getEstimatedExpense());
                                map.put("bpclTransactionAmount", 0);
                                map.put("rcmExpense", trpTO.getEstimatedExpense());
                                map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                                map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                                map.put("tripStartingBalance", 0);
                                if (!"".equals(gpsHm) && gpsHm != null) {
                                    map.put("gpsHm", gpsHm);
                                } else {
                                    map.put("gpsHm", "0");
                                }
                                if (!"".equals(gpsKm)) {
                                    map.put("gpsKm", gpsKm);
                                } else {
                                    map.put("gpsKm", "0");
                                }
                                if (!"".equals(trpTO.getRunKm())) {
                                    map.put("totalRunKm", trpTO.getRunKm());
                                } else {
                                    map.put("totalRunKm", "0");
                                }
                                if (!"".equals(trpTO.getTripTransitDays())) {
                                    map.put("totalDays", trpTO.getTripTransitDays());
                                } else {
                                    map.put("totalDays", "0");
                                }
                                if (!"".equals(trpTO.getRunHm())) {
                                    map.put("totalRefeerHours", trpTO.getRunHm());
                                } else {
                                    map.put("totalRefeerHours", "0");
                                }
                                if (!"".equals(trpTO.getVehicleMileage())) {
                                    map.put("milleage", trpTO.getVehicleMileage());
                                } else {
                                    map.put("milleage", "0");
                                }
                                if (!"".equals(trpTO.getFuelCost())) {
                                    map.put("fuelPrice", trpTO.getFuelCost());
                                } else {
                                    map.put("fuelPrice", "0");
                                }
                                if (!"".equals(trpTO.getFuelConsumption())) {
                                    map.put("tripDieselConsume", trpTO.getFuelConsumption());
                                } else {
                                    map.put("tripDieselConsume", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                                } else {
                                    map.put("tripRcmAllocation", "0");
                                }
                                if (!"".equals(tpTO.getAdvancePaid())) {
                                    map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length);
                                } else {
                                    map.put("totalTripAdvance", "0");

                                }
                                if (!"".equals(trpTO.getMiscValue())) {
                                    map.put("totalTripMisc", trpTO.getMiscValue());
                                } else {
                                    map.put("totalTripMisc", "0");
                                }
                                if (!"".equals(trpTO.getDriverBhatta())) {
                                    map.put("totalTripBhatta", trpTO.getDriverBhatta());
                                } else {
                                    map.put("totalTripBhatta", "0");
                                }
                                String startDate = tpTO.getStartDate();
                                String endDate = tpTO.getEndDate();
                                map.put("tripStartDate", startDate);
                                map.put("tripEndDate", endDate);
                                String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                                if (!"".equals(expenseValue)) {
                                    map.put("tripExtraExpense", Float.parseFloat(expenseValue) / tempDriverInTrip.length);
                                } else {
                                    map.put("tripExtraExpense", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                                } else {
                                    map.put("totalTripExpense", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripEndBalance)) {
                                    map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripEndBalance", "0");
                                }

                                map.put("settlementRemarks", settlementRemarks);
                                map.put("paymentMode", paymentMode);
                                map.put("userId", userId);
                                map.put("vehicleId", tripTO.getVehicleId());
                                System.out.println("the insertSettlement" + map);
                                insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                                System.out.println("insert status = " + insertTripDriverSettlementDetails);
                            }
                        }
                    } else {
                        driverInTrip = tpTO.getDriverInTrip();
                        map.put("driverId", driverInTrip);
                        driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                        Iterator itr = driverClosureList.iterator();
                        TripTO trpTO = new TripTO();
                        while (itr.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr.next();
                            map.put("tripId", tripSheetId);
                            map.put("tripSheetId", tripSheetId);
                            map.put("driverId", trpTO.getDriverId());
                            map.put("estimatedExpense", trpTO.getEstimatedExpense());
                            map.put("bpclTransactionAmount", 0);
                            map.put("rcmExpense", trpTO.getEstimatedExpense());
                            map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                            map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                            map.put("tripStartingBalance", 0);
                            if (!"".equals(gpsHm) && gpsHm != null) {
                                map.put("gpsHm", gpsHm);
                            } else {
                                map.put("gpsHm", "0");
                            }
                            if (!"".equals(gpsKm)) {
                                map.put("gpsKm", gpsKm);
                            } else {
                                map.put("gpsKm", "0");
                            }
                            if (!"".equals(trpTO.getRunKm())) {
                                map.put("totalRunKm", trpTO.getRunKm());
                            } else {
                                map.put("totalRunKm", "0");
                            }
                            if (!"".equals(trpTO.getTripTransitDays())) {
                                map.put("totalDays", trpTO.getTripTransitDays());
                            } else {
                                map.put("totalDays", "0");
                            }
                            if (!"".equals(trpTO.getRunHm())) {
                                map.put("totalRefeerHours", trpTO.getRunHm());
                            } else {
                                map.put("totalRefeerHours", "0");
                            }
                            if (!"".equals(trpTO.getVehicleMileage())) {
                                map.put("milleage", trpTO.getVehicleMileage());
                            } else {
                                map.put("milleage", "0");
                            }
                            if (!"".equals(trpTO.getFuelCost())) {
                                map.put("fuelPrice", trpTO.getFuelCost());
                            } else {
                                map.put("fuelPrice", "0");
                            }
                            if (!"".equals(trpTO.getFuelConsumption())) {
                                map.put("tripDieselConsume", trpTO.getFuelConsumption());
                            } else {
                                map.put("tripDieselConsume", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                            } else {
                                map.put("tripRcmAllocation", "0");
                            }
                            if (!"".equals(tpTO.getAdvancePaid())) {
                                map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()));
                            } else {
                                map.put("totalTripAdvance", "0");

                            }
                            if (!"".equals(trpTO.getMiscValue())) {
                                map.put("totalTripMisc", trpTO.getMiscValue());
                            } else {
                                map.put("totalTripMisc", "0");
                            }
                            if (!"".equals(trpTO.getDriverBhatta())) {
                                map.put("totalTripBhatta", trpTO.getDriverBhatta());
                            } else {
                                map.put("totalTripBhatta", "0");
                            }
                            String startDate = tpTO.getStartDate();
                            String endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                            if (!"".equals(expenseValue)) {
                                map.put("tripExtraExpense", Float.parseFloat(expenseValue));
                            } else {
                                map.put("tripExtraExpense", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                            } else {
                                map.put("totalTripExpense", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripEndBalance)) {
                                map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripEndBalance", "0");
                            }

                            map.put("settlementRemarks", settlementRemarks);
                            map.put("paymentMode", paymentMode);
                            map.put("userId", userId);
                            map.put("vehicleId", tripTO.getVehicleId());
                            System.out.println("the insertSettlement" + map);
                            insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                            System.out.println("insert status = " + insertTripDriverSettlementDetails);
                        }
                    }

                }

                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    map.put("driverId", "");
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tTO = new TripTO();
                    while (itr.hasNext()) {
                        tTO = new TripTO();
                        tTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tTO.getDriverId());
                        map.put("estimatedExpense", tTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", 0);
                        map.put("rcmExpense", tTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(tTO.getRunKm()) / Float.parseFloat(tTO.getVehicleMileage()));
                        map.put("reeferDieselConsume", Float.parseFloat(tTO.getRunHm()) * Float.parseFloat(tTO.getReeferMileage()));
                        map.put("tripStartingBalance", 0);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tTO.getRunKm())) {
                            map.put("totalRunKm", tTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(tTO.getTripTransitDays())) {
                            map.put("totalDays", tTO.getTripTransitDays());
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tTO.getRunHm())) {
                            map.put("totalRefeerHours", tTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tTO.getVehicleMileage())) {
                            map.put("milleage", tTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(tTO.getFuelCost())) {
                            map.put("fuelPrice", tTO.getFuelCost());
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) - Float.parseFloat(tpTO.getAdvancePaid()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tTO.getMiscValue())) {
                            map.put("totalTripMisc", tTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        String startDate = tpTO.getStartDate();
                        String endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                        if (!"".equals(expenseValue)) {
                            map.put("tripExtraExpense", Float.parseFloat(expenseValue) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(tTO.getEstimatedExpense()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }

                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            } else {
                //Driver Not Change Case
                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tpTO.getDriverId());
                        map.put("estimatedExpense", tpTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", Float.parseFloat(bpclTransactionAmount) / vehicleDriverCount);
                        map.put("rcmExpense", tpTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(vehicleDieselConsume) / vehicleDriverCount);
                        map.put("reeferDieselConsume", Float.parseFloat(reeferDieselConsume) / vehicleDriverCount);
                        map.put("tripStartingBalance", tripStartingBalance);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tpTO.getRunKm())) {
                            map.put("totalRunKm", tpTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(totalDays)) {
                            map.put("totalDays", totalDays);
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tpTO.getRunHm())) {
                            map.put("totalRefeerHours", tpTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tpTO.getVehicleMileage())) {
                            map.put("milleage", tpTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(fuelPrice)) {
                            map.put("fuelPrice", fuelPrice);
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tpTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tpTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tpTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tpTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tpTO.getMiscValue())) {
                            map.put("totalTripMisc", tpTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tpTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tpTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        if (!"".equals(tripExtraExpense)) {
                            map.put("tripExtraExpense", Float.parseFloat(tripExtraExpense) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(totalTripExpense) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", Float.parseFloat(tripEndBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertTripDriverSettlementDetails;
    }

    // Arul Starts 12-12-2013
    public ArrayList getEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());

        System.out.println("map in the end trip sheet details= " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());
        int tripVehicleCount = 0;
        System.out.println("map in the end trip sheet details=---------------xxxxxx " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount = " + tripVehicleCount);
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetails", map);
            //hididng vehicle Change logic for DICT as it is not applicable for now 02/04/2016
//            if (tripVehicleCount == 1) {
//            } else {
//                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetailsForVehicleChange", map);
//            }
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureApprovalProcessHistory(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureApprovalProcessHistory", map);
            System.out.println("getClosureApprovalProcessHistory result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalProcessHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalProcessHistory List", sqlException);
        }

        return result;
    }

    public String getGpsKm(String tripSheetId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String gpsKm = "";
        try {
            map.put("tripSheetId", tripSheetId);
            gpsKm = (String) getSqlMapClientTemplate().queryForObject("trip.getGpsKm", map);
            System.out.println("gpsKm in the dao test by arul = " + gpsKm);
            if (gpsKm == null) {
                gpsKm = "0-0";
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("gpsKm in dao =" + gpsKm);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGpsKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGpsKm", sqlException);
        }
        return gpsKm;
    }

    public String getClosureApprovalStatus(String tripSheetId, String requestType) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("requestType", requestType);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getClosureApprovalStatus", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalStatus", sqlException);
        }
        return result;
    }

    public String getRevenueApprovalStatus(String customerId, String requestType) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("customerId", customerId);
            map.put("requestType", requestType);
            System.out.println("map for approve status:" + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getRevenueApprovalStatus", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalStatus", sqlException);
        }
        return result;
    }

    public String getMiscValue(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String getMiscValue = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map = " + map);
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue3118", map);
            } else {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("revenue=" + getMiscValue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMiscValue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMiscValue", sqlException);
        }
        return getMiscValue;
    }

    public String getBookedExpense(String tripSheetId, TripTO tripTO) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getBookedExpense", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBookedExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBookedExpense", sqlException);
        }
        return result;
    }
//Nithya Start 7 Dec 

    public int savePreTripSheetDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("preStartDate", tripTO.getPreStartDate());
        map.put("preStarttime", tripTO.getTripPreStartHour() + ":" + tripTO.getTripPreStartMinute() + ":00");
        map.put("preOdometerReading", tripTO.getPreOdometerReading());
        map.put("preTripRemarks", tripTO.getPreTripRemarks());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("preStartHM", tripTO.getPreStartHM());
        map.put("statusId", tripTO.getStatusId());
        map.put("userId", userId);
        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPreTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreTripSheetDetails", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            System.out.println("savePreTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int setAdvanceAdvice(TripTO tripTO) {
        Map map = new HashMap();
        map.put("estimatedAdvancePerDay", tripTO.getEstimatedAdvancePerDay());
        map.put("userId", tripTO.getUserId());
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("the setAdvanceAdvice" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripAdvance", map);
            System.out.println("setAdvanceAdvice size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillHeader(TripTO tripTO) {
        Map map = new HashMap();

        map.put("billingPartyId", tripTO.getCustomerId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", tripTO.getNoOfTrips());
        map.put("grandTotal", tripTO.getGrandTotal());
        map.put("totalRevenue", tripTO.getTotalRevenue());
        map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
        System.out.println("the saveBillHeader" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceDetailId", tripTO.getInvoiceDetailId());
        map.put("tripId", tripTO.getTripId());
        map.put("expenseId", tripTO.getExpenseId());
        map.put("expenseName", tripTO.getExpenseName() + "; " + tripTO.getExpenseRemarks());
        map.put("marginValue", tripTO.getMarginValue());
        map.put("taxPercentage", tripTO.getTaxPercentage());
        map.put("expenseValue", tripTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (tripTO.getMarginValue() == null || "".equals(tripTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = tripTO.getMarginValue();
        }
        if (tripTO.getTaxPercentage() == null || "".equals(tripTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = tripTO.getTaxPercentage();
        }
        if (tripTO.getExpenseValue() == null || "".equals(tripTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = tripTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

//   public int updateStartTripSheet(String[] containerTypeId,String[] containerNo,TripTO tripTO, int userId) {
//        Map map = new HashMap();
//        map.put("userId", userId);
//        map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
//        map.put("planStartDate", tripTO.getPlanStartDate());
//        map.put("startDate", tripTO.getStartDate());
//        map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
//        map.put("startOdometerReading", tripTO.getStartOdometerReading());
//        map.put("startHM", tripTO.getStartHM());
//        map.put("tripSheetId", tripTO.getTripSheetId());
//        map.put("tripId", tripTO.getTripSheetId());
//        map.put("startTripRemarks", tripTO.getStartTripRemarks());
//        map.put("statusId", tripTO.getStatusId());
//        map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
//        map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
//        map.put("lrNumber", tripTO.getLrNumber());
//        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
//        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
//        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
//        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
//        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
//        map.put("startTrailerKM", tripTO.getStartTrailerKM());
//
//        System.out.println("the updatetripsheetdetails" + map);
//        int status = 0;
//        int update = 0;
//        String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
//        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
//        String[] ConsignmentId = ConsignmentIdList.split(",");
//        for (int i = 0; i < ConsignmentId.length; i++) {
//            map.put("consignmentId", ConsignmentId[i]);
//            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
//
//            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
////            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("23")) {
////                map.put("consignmentStatus", "24");
////            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("26")) {
////                map.put("consignmentStatus", "27");
////            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("29")) {
////                map.put("consignmentStatus", "30");
////            } else {
////                map.put("consignmentStatus", "10");
////            }
//            map.put("consignmentStatus", "10");
//            System.out.println("the updated map:" + map);
//
//            update = (Integer) getSqlMapClientTemplate().update("trip.updateStartconsignmentSheet", map);
//            System.out.println("the update:" + update);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//            System.out.println("the status1:" + status);
//        }
//        ArrayList tripDetails = new ArrayList();
//        try {
//            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripSheetDetails", map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheet", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
//            if(containerTypeId != null){
//                 for(int i=1;i< containerTypeId.length;i++){
//                 map.put("containerTypeId",containerTypeId[i]) ;
//                 map.put("containerNo",containerNo[i]) ;
//                 status = (Integer) getSqlMapClientTemplate().update("trip.insertContainerDetails", map);
//                }
//            }
//
////            int maxVehicleSequence = 0;
////            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
////            map.put("vehicleSequence", maxVehicleSequence);
////            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
//
//            int insertConsignmentArticle = 0;
//            map.put("consignmentId", tripTO.getConsignmentId());
//            System.out.println("the inner" + map);
//            String[] productCodes = null;
//            String[] productNames = null;
//            String[] packageNos = null;
//            String[] weights = null;
//            String[] productbatch = null;
//            String[] productuom = null;
//            String[] loadedpackages = null;
//            productCodes = tripTO.getProductCodes();
//            productNames = tripTO.getProductNames();
//            packageNos = tripTO.getPackagesNos();
//            weights = tripTO.getWeights();
//            productbatch = tripTO.getProductbatch();
//            productuom = tripTO.getProductuom();
//            loadedpackages = tripTO.getLoadedpackages();
//            for (int i = 0; i < productCodes.length; i++) {
//                map.put("productCode", productCodes[i]);
//                map.put("producName", productNames[i]);
//                map.put("packageNos", packageNos[i]);
//                map.put("weights", weights[i]);
//                map.put("productbatch", productbatch[i]);
//                map.put("productuom", productuom[i]);
//                map.put("loadedpackages", loadedpackages[i]);
//                System.out.println("the full" + map);
//                if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
//                    insertConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
//                }
//
//            }
//            String tripRevenueDetails = "";
//            String tripTemp[] = null;
//            String customerId = "";
//            String tripType = "";
//            String estimatedRevenue = "";
//            String emptyTrip = "";
//            tripRevenueDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRevenueDetails", map);
//            System.out.println("tripRevenueDetails = " + tripRevenueDetails);
//            if (!"".equals(tripRevenueDetails)) {
//                tripTemp = tripRevenueDetails.split("~");
//                customerId = tripTemp[0];
//                tripType = tripTemp[1];
//                estimatedRevenue = tripTemp[2];
//                emptyTrip = tripTemp[3];
//            }
//            /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
//            String code2 = "";
//            String[] temp = null;
//            int insertStatus = 0;
//            map.put("userId", userId);
//            map.put("DetailCode", "1");
//            map.put("voucherType", "%SALES%");
//            code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
//            temp = code2.split("-");
//            int codeval2 = Integer.parseInt(temp[1]);
//            int codev2 = codeval2 + 1;
//            String voucherCode = "SALES-" + codev2;
//            System.out.println("voucherCode = " + voucherCode);
//            map.put("voucherCode", voucherCode);
//            map.put("mainEntryType", "VOUCHER");
//            map.put("entryType", "SALES");
//
//            //get ledger info
//            map.put("customer", customerId);
//            String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
//            System.out.println("ledgerInfo:" + ledgerInfo);
//            temp = ledgerInfo.split("~");
//            String ledgerId = temp[0];
//            String particularsId = temp[1];
//            map.put("ledgerId", ledgerId);
//            map.put("particularsId", particularsId);
//
//            map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
//            //  map.put("amount", crossingAmount);
//            map.put("Accounts_Type", "DEBIT");
//            map.put("Remark", "Freight Charges");
//            map.put("Reference", "Trip");
//            //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//            //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//            System.out.println("tripId = " + tripTO.getTripSheetId());
//            map.put("SearchCode", tripTO.getTripSheetId());
//            System.out.println("map1 =---------------------> " + map);
//            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
//            System.out.println("status1 = " + insertStatus);
//            //--------------------------------- acc 2nd row start --------------------------
//            if (insertStatus > 0) {
//            map.put("DetailCode", "2");
//            map.put("ledgerId", "51");
//            map.put("particularsId", "LEDGER-39");
//            map.put("Accounts_Type", "CREDIT");
//            System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
//            System.out.println("map2 =---------------------> " + map);
//            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
//            System.out.println("status2 = " + insertStatus);
//            }
//
//            }
//             */
//
//            System.out.println("updateStartTripSheet size=" + tripDetails.size());
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
//        }
//
//        return status;
//    }
    public ArrayList getTripPODDetails(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPODDetails", map);
            System.out.println("getTripPODDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPODDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveTripPodDetails(String actualFilePath, String cityId1, String tripSheetId1, String podRemarks1, String lrNumber1, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripSheetId1 != null && tripSheetId1 != "") {
                tripId = Integer.parseInt(tripSheetId1);
                map.put("tripSheetId", tripId);
            }

            if (cityId1 != null && cityId1 != "") {
                cityId = Integer.parseInt(cityId1);
                map.put("cityId", cityId);
            }
            map.put("podRemarks", podRemarks1);
            map.put("fileName", fileSaved);
            map.put("lrNumber", lrNumber1);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] podFile = new byte[(int) file.length()];
            System.out.println("podFile = " + podFile);
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripPodDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripPodDetails", map);
            map.put("consignorName", tripTO.getConsignorName());
            map.put("consigneeName", tripTO.getConsigneeName());
            map.put("consignorMobileNo", tripTO.getConsignorMobileNo());
            map.put("consigneeMobileNo", tripTO.getConsigneeMobileNo());
            map.put("consignorAddress", tripTO.getConsignorAddress());
            map.put("consigneeAddress", tripTO.getConsigneeAddress());
            map.put("consignmentNote", tripTO.getConsignmentNote());
            System.out.println("the consignorDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignorDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        Map map = new HashMap();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getDriverSettlementDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverSettlementDetails", map);
            System.out.println("getDriverSettlementDetails.size() = " + settlementDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverSettlementDetails List", sqlException);
        }
        return settlementDetails;
    }

    public int createGPSLogForTripStart(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripStart" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripStart", map);
            System.out.println("createGPSLogForTripStart size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripStart Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripStart List", sqlException);
        }

        return status;
    }

    public int createGPSLogForTripEnd(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripEnd" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripEnd", map);
            System.out.println("createGPSLogForTripEnd =" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripEnd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripEnd List", sqlException);
        }

        return status;
    }

    public ArrayList getStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        try {
//            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
//            if(tripVehicleCount == 1){
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetails", map);
//            }else{
//            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetailsForVehicleChange", map);
//            }
            System.out.println("getStartTripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getPreStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        System.out.println("getPreStartTripDetails" + tripTO.getTripId());
        ArrayList tripPreStartDetails = new ArrayList();
        try {
            tripPreStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPreStartTripDetails", map);
            System.out.println("getPreStartTripDetails size=" + tripPreStartDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPreStartTripDetails List", sqlException);
        }

        return tripPreStartDetails;
    }

    public ArrayList getStartedTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
            System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getVehicleChangeTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount in DAO = " + tripVehicleCount);
            if (tripVehicleCount == 1) {
                System.out.println("tripVehicleCount in DAO if = " + tripVehicleCount);
                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
                System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            } else {
                System.out.println("tripVehicleCount in DAO else = " + tripVehicleCount);
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
                map.put("vehicleId", vehicleId);
                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetailsForVehicleChange", map);
                System.out.println("tripVehicleCount in DAO else size = " + tripStartDetails.size());
            }
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getEndTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripEndDetails = new ArrayList();
        int tripVehicleCount = 0;
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            if (tripVehicleCount == 1) {
                System.out.println(" 2222222222");
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetailsForVehicleChange", map);
            } else {
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetails", map);
                System.out.println(" 111111111111");
            }
            System.out.println("getEndTripDetails size=" + tripEndDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEndTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEndTripDetails List", sqlException);
        }

        return tripEndDetails;
    }

    public ArrayList getGPSDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList gpsDetails = new ArrayList();
        try {
            gpsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getGPSDetails", map);
            System.out.println("getGPSDetails size=" + gpsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGPSDetails List", sqlException);
        }

        return gpsDetails;
    }

    public ArrayList getTotalExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList expenseDetails = new ArrayList();
        String vehicleId = "";
        String modelId = "";
        String vehicleTypeId = "";
        vehicleId = tripTO.getVehicleId();
        try {

            if ("".equals(vehicleId) && vehicleId == null && "0".equals(vehicleId)) {
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleId", map);
            }
            map.put("vehicleId", vehicleId);
            modelId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleModelId", map);
            map.put("modelId", modelId);
            if (!"0".equals(vehicleId)) {
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeId", map);
            } else {
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeIdFromTrip", map);
            }
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map expenseDetails-----------------------------------------------------------------------------------------------------" + map);
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTotalExpenseDetails", map);
            System.out.println("getTotalExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getTotalExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public String getTollAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        System.out.println("map = " + map);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount3118", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            }
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getSecondaryRouteExpense(TripTO tripTO) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripTO.getTripId());
            System.out.println("map = " + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryRouteExpense", map);
            System.out.println("getSecondaryRouteExpense size=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryRouteExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryRouteExpense List", sqlException);
        }

        return result;
    }

    public String getDriverIncentiveAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            }
            System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return tollAmount;
    }

    public String getDriverBataAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverBatta", map);
            System.out.println("getDriverBataAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverBataAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverBataAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFuelPrice(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getFuelType(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelType", map);
            System.out.println("getFuelType size=" + fuelType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelType List", sqlException);
        }

        return fuelType;
    }

    public String getSecondaryFuelPrice(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getSecondaryFuelType(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String secondaryFuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            secondaryFuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelType", map);
            System.out.println("getSecondaryFuelType size=" + secondaryFuelType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryFuelType List", sqlException);
        }

        return secondaryFuelType;
    }

    public String getTripEmails(String tripId, String statusId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        map.put("statusId", statusId);
        String result = "";
        try {
            System.out.println("map:" + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getTripEmails", map);
            System.out.println("getTripEmails value=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripEmails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripEmails List", sqlException);
        }

        return result;
    }

    public int saveTripClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getTotalRumKM());
        map.put("reeferHours", tripTO.getTotalRefeerHours());
        map.put("reeferMinutes", tripTO.getTotalRefeerMinutes());
        map.put("mileage", tripTO.getMilleage());
        map.put("reeferConsumption", tripTO.getReeferConsumption());
        map.put("fuelConsumed", tripTO.getFuelConsumed());
        map.put("fuelAmount", tripTO.getFuelAmount());
        map.put("fuelCost", tripTO.getFuelCost());
        map.put("tollAmount", tripTO.getTollAmount());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getIncentiveAmount());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("battaAmount", tripTO.getBattaAmount());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("routeExpense", tripTO.getRouteExpense());
        map.put("totalExpense", tripTO.getTotalExpenses());
        map.put("userId", userId);
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripClosure", map);
            System.out.println("saveTripClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int saveTripClosureDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getRunKm());
        map.put("reeferHours", tripTO.getRunHm());
        map.put("gpsKm", tripTO.getGpsKm());
        map.put("gpsHm", tripTO.getGpsHm());
        map.put("reeferMinutes", "0");
        map.put("mileage", tripTO.getMilleage());
        map.put("reeferConsumption", tripTO.getReeferMileage());
        map.put("fuelConsumed", tripTO.getDieselUsed());
        map.put("fuelAmount", tripTO.getFuelPrice());
        map.put("fuelCost", tripTO.getDieselCost());
        map.put("tollAmount", tripTO.getTollRate());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("miscValue", tripTO.getMiscValue());
        map.put("battaAmount", tripTO.getDriverBattaPerDay());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("systemExpenses", tripTO.getSystemExpense());
        map.put("routeExpense", tripTO.getBookedExpense());
        map.put("totalExpense", tripTO.getTotalExpenses());
        map.put("hireCharges", tripTO.getHireCharges());
        map.put("dieselAmount", tripTO.getDieselAmount());
        map.put("userId", userId);

        if (tripTO.getSecondaryParkingAmount().equals("")) {
            map.put("secParkingAmount", "0");
        } else {
            map.put("secParkingAmount", tripTO.getSecondaryParkingAmount());
        }
        if (tripTO.getPreColingAmount().equals("")) {
            map.put("preColingAmount", "0");
        } else {
            map.put("preColingAmount", tripTO.getPreColingAmount());
        }
        if (tripTO.getSecAdditionalTollCost().equals("")) {
            map.put("secAddlTollCost", "0");
        } else {
            map.put("secAddlTollCost", tripTO.getSecAdditionalTollCost());
        }
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        int driverChangeStatus = 0;
        int driverSettelment = 0;
        ArrayList tripDetails = new ArrayList();
        ArrayList vehicleDriverlist = new ArrayList();
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        try {

            System.out.println("starts");
            updateEndTripSheetDuringClosure(tripTO, userId);
            System.out.println("ends");

            status = (Integer) getSqlMapClientTemplate().insert("trip.saveTripClosure", map);
            System.out.println("status tripClosureId =" + status);
            if (status > 0) {
                map.put("tripClosureId", status);
                String driver = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleDriverId", map);
                if (!"".equals(driver) && driver != null) {
                    map.put("driver", driver);
                } else {
                    map.put("driver", 0);
                }
                if (!"".equals(tripTO.getUnclearedAmount()) && tripTO.getUnclearedAmount() != null) {
                    map.put("unclearedAmount", tripTO.getUnclearedAmount());
                } else {
                    map.put("unclearedAmount", 0);
                }

                System.out.println(" map for Driver Settelment:" + map);
                driverSettelment = (Integer) getSqlMapClientTemplate().update("trip.updateTripDriverSettelement", map);
                driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
                System.out.println("driverChangeStatus = " + driverChangeStatus);
                if (driverChangeStatus > 0) {
                    // Driver Cnage Case
                    inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
                    Iterator itr1 = inActiveVehicleDriverlist.iterator();
                    TripTO tpTO = new TripTO();
                    Float runKm = 0.0f;
                    Float totalRunKm = 0.0f;
                    Float runHm = 0.0f;
                    Float totalRunHm = 0.0f;
                    Float milleage = 0.0f;
                    Float reeferMileage = 0.0f;
                    Float dieselUsed = 0.0f;
                    Float driverBatta = 0.0f;
                    Float miscRate = 0.0f;
                    Float extraExpenseValue = 0.0f;
                    Float fuelPrice = 0.0f;
                    Float tollRate = 0.0f;
                    Float driverIncentivePerKm = 0.0f;
                    Float nettExpense = 0.0f;
                    Float bookedExpense = 0.0f;
                    Float tempEstimatedExpense = 0.0f;
                    Float estimatedExpense = 0.0f;
                    Float driverBattaPerDay = 100.0f;
                    String startDate = "";
                    String endDate = "";
                    String expenseValue = "";
                    int totalDays = 0;
                    int totalDaysSum = 0;
                    int driverCount = 0;
                    int driverCountSum = 0;
                    while (itr1.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr1.next();
                        String[] tempDriverId = null;
                        driverCount = Integer.parseInt(tpTO.getDriverCount());
                        driverCountSum += Integer.parseInt(tpTO.getDriverCount());
                        totalDays = Integer.parseInt(tpTO.getTotalDays());
                        totalDaysSum += Integer.parseInt(tpTO.getTotalDays());
                        tempDriverId = tpTO.getDriverInTrip().split(",");
                        runKm = Float.parseFloat((String) tpTO.getTotalKm());
                        totalRunKm += Float.parseFloat((String) tpTO.getTotalKm());
                        runHm = Float.parseFloat((String) tpTO.getTotalHm());
                        totalRunHm += Float.parseFloat((String) tpTO.getTotalHm());
                        milleage = Float.parseFloat((String) tripTO.getMilleage());
                        reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                        dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                        driverBatta = totalDays * driverBattaPerDay * driverCount;
                        miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                        startDate = tpTO.getStartDate();
                        endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                        extraExpenseValue = Float.parseFloat((String) expenseValue);
                        fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                        tollRate = Float.parseFloat((String) tripTO.getTollRate());
                        driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                        bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                        Float dieselCost = dieselUsed * fuelPrice;
                        Float tollCost = runKm * tollRate;
                        Float driverIncentive = runKm * driverIncentivePerKm;
                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                        nettExpense = systemExpense + bookedExpense;
                        tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                        estimatedExpense = tempEstimatedExpense * runKm;

                        if (tempDriverId.length > 0) {
                            for (int i = 0; i < tempDriverId.length; i++) {
                                map.put("driverId", tempDriverId[i]);
                                map.put("estimatedExpense", estimatedExpense / driverCount);
                                map.put("totalDays", totalDays);
                                map.put("runKM", runKm / driverCount);
                                map.put("reeferHours", runHm / driverCount);
                                map.put("gpsKm", 0);
                                map.put("gpsHm", 0);
                                map.put("reeferMinutes", "0");
                                map.put("mileage", milleage);
                                map.put("reeferConsumption", reeferMileage);
                                map.put("fuelConsumed", dieselUsed / driverCount);
                                map.put("fuelAmount", tripTO.getFuelPrice());
                                map.put("fuelCost", dieselCost / driverCount);
                                map.put("tollAmount", tripTO.getTollRate());
                                map.put("tollCost", tollCost / driverCount);
                                map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                                map.put("driverIncentive", driverIncentive / driverCount);
                                map.put("miscValue", miscValue);
                                map.put("battaAmount", tripTO.getDriverBattaPerDay());
                                map.put("driverBatta", driverBatta / driverCount);
                                map.put("systemExpenses", systemExpense / driverCount);
                                map.put("routeExpense", bookedExpense / driverCount);
                                map.put("totalExpense", nettExpense / driverCount);
                                System.out.println("final Map = " + map);
                                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                            }
                        }
                    }

                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr2 = vehicleDriverlist.iterator();
                        TripTO trpTO = new TripTO();
                        vehicleDriverCount = vehicleDriverCount - driverCountSum;
                        totalDays = totalDays - totalDaysSum;
                        while (itr2.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr2.next();
                            driverId = trpTO.getDriverId();
                            System.out.println("driverId = " + driverId);
                            map.put("driverId", driverId);
                            runKm = (Float.parseFloat((String) tripTO.getRunKm()) - totalRunKm) / vehicleDriverCount;
                            runHm = (Float.parseFloat((String) tripTO.getRunHm()) - totalRunHm) / vehicleDriverCount;
                            milleage = Float.parseFloat((String) tripTO.getMilleage());
                            reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                            dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                            driverBatta = totalDays * driverBattaPerDay * vehicleDriverCount;
                            miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                            startDate = tpTO.getStartDate();
                            endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                            extraExpenseValue = Float.parseFloat((String) expenseValue);
                            fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                            tollRate = Float.parseFloat((String) tripTO.getTollRate());
                            driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                            bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                            Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                            Float dieselCost = dieselUsed * fuelPrice;
                            Float tollCost = runKm * tollRate;
                            Float driverIncentive = runKm * driverIncentivePerKm;
                            Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                            nettExpense = systemExpense + bookedExpense;
                            tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                            estimatedExpense = tempEstimatedExpense * runKm;
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            System.out.println("final Map = " + map);
                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                        }
                    }

                } else {
                    // Driver not Change Case
                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr1 = vehicleDriverlist.iterator();
                        TripTO tpTO = new TripTO();
                        while (itr1.hasNext()) {
                            tpTO = new TripTO();
                            tpTO = (TripTO) itr1.next();
                            driverId = tpTO.getDriverId();
                            System.out.println("driverId = " + driverId);
                            map.put("driverId", driverId);
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            System.out.println("final Map = " + map);
                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
                        }
                    }

                }
                //account entry details start

                //fuel cost
                String code2 = "";
                String[] temp = null;
                int insertStatus = 0;
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);

                if ("3".equals(tripTO.getOwnerShip())) {
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 1582);
                    map.put("particularsId", "LEDGER-1562");

                    map.put("amount", tripTO.getDieselCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Hire Charge");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                } else {
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 37);
                    map.put("particularsId", "LEDGER-29");

                    map.put("amount", tripTO.getDieselCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Fuel Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //toll cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 41);
                    map.put("particularsId", "LEDGER-33");

                    map.put("amount", tripTO.getTollCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Toll Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //driver Bata cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 34);
                    map.put("particularsId", "LEDGER-27");

                    map.put("amount", tripTO.getDriverBatta());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Driver Bhatta Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //driver Incentive cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 34);
                    map.put("particularsId", "LEDGER-27");

                    map.put("amount", tripTO.getDriverIncentive());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Driver Incentive Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }
                    //Misc cost

                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 40);
                    map.put("particularsId", "LEDGER-32");

                    map.put("amount", tripTO.getMiscValue());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Misc Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                }
                //updateTripCloserToEFS(tripTO.getTripId());
                //account entry details end

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheet(String[] containerTypeId, String[] containerNo, TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("endDate", tripTO.getEndDate());
        map.put("planEndDate", tripTO.getPlanEndDate());
        String plannedEndMinute = "00";
        String plannedEndHour = "00";
        String tripEndMinute = "00";
        String tripEndHour = "00";
        if (tripTO.getPlanEndHour() != null && !"".equals(tripTO.getPlanEndHour())) {
            plannedEndHour = tripTO.getPlanEndHour();
        }
        if (tripTO.getPlanEndMinute() != null && !"".equals(tripTO.getPlanEndMinute())) {
            plannedEndMinute = tripTO.getPlanEndMinute();
        }
        if (tripTO.getTripEndHour() != null && !"".equals(tripTO.getTripEndHour())) {
            tripEndHour = tripTO.getTripEndHour();
        }
        if (tripTO.getTripEndMinute() != null && !"".equals(tripTO.getTripEndMinute())) {
            tripEndMinute = tripTO.getTripEndMinute();
        }
        map.put("planEndTime", plannedEndHour + ":" + plannedEndMinute + ":00");
        map.put("endTime", tripEndHour + ":" + tripEndMinute + ":00");
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("durationDay1", tripTO.getDurationDay1());
        map.put("durationDay2", tripTO.getDurationDay2());
        map.put("endTrailerKM", tripTO.getEndTrailerKM());
        System.out.println("the updateEndTripSheet" + map);

        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        //map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
        map.put("destinationId", tripTO.getDestinationId());
        map.put("originId", tripTO.getOriginId());
        map.put("vehicleId", tripTO.getVehicleId());

        if (tripTO.getEndReportingTime() != null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getEndReportingTime());
        }
        if (tripTO.getEndReportingTime() == null && "".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        }

        int status = 0;
        int update = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            System.out.println("map for End:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheet", map);
            int vehicleAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForEnd", map);
            // updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            String trailerIds = tripTO.getTrailerId();
            String trailer[] = trailerIds.split(",");
            for (int k = 0; k < trailer.length; k++) {
                map.put("trailerId", trailer[k]);
                System.out.println("trailer availability map:" + map);
                int traileAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateTrailerAvailabiltyForEnd", map);
                System.out.println("traileAvailUpdate = " + traileAvailUpdate);
            }

            // update consignment status for pick up trip end
            String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int j = 0; j < ConsignmentId.length; j++) {
                map.put("consignmentId", ConsignmentId[j]);
                String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
                System.out.println("the ConsignmentDetailsList size:" + ConsignmentDetailsList.length());
                String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
                if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("24")) {
                    map.put("consignmentStatus", "25");
                } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("27")) {
                    map.put("consignmentStatus", "28");
                } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("30")) {
                    map.put("consignmentStatus", "31");
                } else {
                    map.put("consignmentStatus", "12");
                }
                String orderTripDetails = null;
                orderTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderTripDetails", map);
                System.out.println("details length is" + ConsignmentDetails[2]);
//                if(orderTripDetails!=null || Integer.parseInt(ConsignmentDetails[2])>0){
//                  map.put("consignmentStatus", "25");
//                  }
//                if(Integer.parseInt(ConsignmentDetails[2])==0 && orderTripDetails==null){
//                map.put("consignmentStatus", "26");
//                }
                System.out.println("the updated map:" + map);
                update = (Integer) getSqlMapClientTemplate().update("trip.updateEndconsignmentSheet", map);
                System.out.println("the update:" + update);

                //   status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripSheetDetails", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);

                //pick up trip end
//                  updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
                int updateConsignmentArticle = 0;
                map.put("consignmentId", tripTO.getConsignmentId());
                String[] productCodes = null;
                String[] unloadedpackages = null;
                String[] shortage = null;
                String[] articleId = null;
                String[] articleName = null;
                articleName = tripTO.getProductNames();
                productCodes = tripTO.getProductCodes();
                unloadedpackages = tripTO.getUnloadedpackages();
                articleId = tripTO.getTripArticleId();
                shortage = tripTO.getShortage();
                if (tripTO.getProductCodes() != null && !"".equals(tripTO.getProductCodes()) && !"".equals(tripTO.getConsignmentId()) && tripTO.getConsignmentId() != null) {
                    for (int i = 0; i < articleName.length; i++) {
                        map.put("productCode", productCodes[i]);
                        map.put("loadedpackages", unloadedpackages[i]);
                        map.put("shortage", shortage[i]);
                        map.put("tripArticleId", articleId[i]);
                        map.put("articleName", tripTO.getArticleName());
                        map.put("productId", tripTO.getCommodityId());
                        System.out.println("the inner product update" + map);
                        updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentArticle", map);
                    }
                }

                System.out.println("updateEndTripSheet size=" + tripDetails.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public int updateEndConsignmentSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        //map.put("consignmentStatus", tripTO.getConsignmentStatusId());
        map.put("endDate", tripTO.getEndDate());
        map.put("planEndDate", tripTO.getPlanEndDate());
        String plannedEndMinute = "00";
        String plannedEndHour = "00";
        String tripEndMinute = "00";
        String tripEndHour = "00";
        if (tripTO.getPlanEndHour() != null && !"".equals(tripTO.getPlanEndHour())) {
            plannedEndHour = tripTO.getPlanEndHour();
        }
        if (tripTO.getPlanEndMinute() != null && !"".equals(tripTO.getPlanEndMinute())) {
            plannedEndMinute = tripTO.getPlanEndMinute();
        }
        if (tripTO.getTripEndHour() == null && !"".equals(tripTO.getTripEndHour())) {
            tripEndHour = tripTO.getTripEndHour();
        }
        if (tripTO.getTripEndMinute() == null && !"".equals(tripTO.getTripEndMinute())) {
            tripEndMinute = tripTO.getTripEndMinute();
        }
        map.put("planEndTime", plannedEndHour + ":" + plannedEndMinute + ":00");
        map.put("endTime", tripEndHour + ":" + tripEndMinute + ":00");
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("durationDay1", tripTO.getDurationDay1());
        map.put("durationDay2", tripTO.getDurationDay2());
        System.out.println("the updateEndTripSheet" + map);

        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        //map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());

        if (tripTO.getEndReportingTime() != null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getEndReportingTime());
        }
        if (tripTO.getEndReportingTime() == null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        }

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            int update = 0;

            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
            System.out.println("the ConsignmentDetailsList size:" + ConsignmentDetailsList.length());
            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("24")) {
                map.put("consignmentStatus", "25");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("27")) {
                map.put("consignmentStatus", "28");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("30")) {
                map.put("consignmentStatus", "31");
            } else {
                map.put("consignmentStatus", "12");
            }
            System.out.println("the updated map:" + map);
            update = (Integer) getSqlMapClientTemplate().update("trip.updateEndconsignmentSheet", map);
            System.out.println("the update:" + update);

            //   status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);

            int updateConsignmentArticle = 0;
            String[] productCodes = null;
            String[] unloadedpackages = null;
            String[] shortage = null;
            String[] articleId = null;
            productCodes = tripTO.getProductCodes();
            unloadedpackages = tripTO.getUnloadedpackages();
            articleId = tripTO.getTripArticleId();
            shortage = tripTO.getShortage();
            if (tripTO.getProductCodes() != null && !"".equals(tripTO.getProductCodes()) && !"".equals(tripTO.getConsignmentId()) && tripTO.getConsignmentId() != null) {
                for (int i = 0; i < productCodes.length; i++) {
                    map.put("productCode", productCodes[i]);
                    map.put("loadedpackages", unloadedpackages[i]);
                    map.put("shortage", shortage[i]);
                    map.put("tripArticleId", articleId[i]);
                    System.out.println("the inner" + map);
                    //         updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentArticle", map);
                }
            }

            System.out.println("updateEndTripSheet size=" + tripDetails.size());
            System.out.println("status=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getEmptyTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("tripId", tripTO.getTripId());
        try {
            emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripDetails", map);
            System.out.println("getEmptyTripDetails.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripDetails List", sqlException);
        }
        return emptyTripList;
    }

    public int saveManualEmptyTripApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int updateStartTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startKm", tripTO.getStartKm());
        map.put("startHm", tripTO.getStartHM());
        map.put("tripStartLoadTemp", tripTO.getLoadingTemperature());
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        map.put("userId", userId);
        System.out.println("the updateStartTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheetDuringClosure", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripVehicleDuringClosure", map);
            System.out.println("updateStartTripSheetDuringClosure =" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
//            nilesh 17 10 2015 Start
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
//            nilesh 17 10 2015 End
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("userId", userId);
        map.put("statusId", 12);
        map.put("tripDetainHours", tripTO.getTripDetainHours());
        if (!"NaN".equals(tripTO.getTripFactoryToIcdHour()) || (tripTO.getTripFactoryToIcdHour()) != null) {
            map.put("tripFactoryToIcdHours", tripTO.getTripFactoryToIcdHour());
        } else {
            map.put("tripFactoryToIcdHours", 0.0);

        }
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        if (!"".equals(tripTO.getBillOfEntry())) {
            map.put("billOfEntry", tripTO.getBillOfEntry());
        }
        if (!"".equals(tripTO.getShipingLineNo())) {
            map.put("shipingLineNo", tripTO.getShipingLineNo());
        }
        if (!"".equals(tripTO.getCommodityCategory())) {
            map.put("commodityCategory", tripTO.getCommodityCategory());
        }
        System.out.println("the updateEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheetDuringClosure", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripStatusInDetail", map);
            
            System.out.println("update commodity in trip master---" + status);
            if (!"".equals(tripTO.getBillOfEntry())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForBoE", map);
                System.out.println("BoE updated .." + status);
            }
            if (!"".equals(tripTO.getShipingLineNo())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForSlN", map);
                System.out.println("sln updated .." + status);
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripVehicleDuringClosure", map);
            System.out.println("updateEndTripSheetDuringClosure size=" + tripDetails.size());
            // update product
            int updateConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
            String[] productCodes = null;
            String[] unloadedpackages = null;
            String[] shortage = null;
            String[] articleId = null;
            String[] articleName = null;
            articleName = tripTO.getProductNames();
            productCodes = tripTO.getProductCodes();
            unloadedpackages = tripTO.getUnloadedpackages();
            articleId = tripTO.getTripArticleId();
            shortage = tripTO.getShortage();
            map.put("consignmentId", 0);
            map.put("productCode", tripTO.getCommodityCategory());
            map.put("productId", tripTO.getCommodityCategory());
            map.put("productbatch", 0);
            map.put("packageNos", 0);
            map.put("weights", 0);
            map.put("productuom", 1);
            map.put("loadedpackages", 0);
            map.put("shortage", 0);
            map.put("tripArticleId", 0);
            map.put("producName", tripTO.getCommodityName());
            System.out.println("map for product..." + map);
            int article = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripArticleDeatils", map);
            System.out.println("article---" + article);
            if (article == 0) {
                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
                System.out.println("insertConsignmentArticle----" + updateConsignmentArticle);
            } else {
                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticle", map);
                System.out.println("updateTripArticle----" + updateConsignmentArticle);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int overrideEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("overrideTripRemarks", tripTO.getOverrideTripRemarks());
        map.put("userId", userId);
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        System.out.println("the overrideEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.overrideEndTripSheetDuringClosure", map);
            System.out.println("overrideEndTripSheetDuringClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("overrideEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "overrideEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        System.out.println("the updateEndTripSheet" + map);
        int status = 0;
        int statusInsert = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        String tripStatus = "";
        try {
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
            statusInsert = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getTripStatusId", map);
            System.out.println("tripStatus********" + tripStatus);
            if (!"16".equalsIgnoreCase(tripStatus)) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
            }
            System.out.println("statusstatusstatusstatusstatus " + status);
            System.out.println("statusInsert:" + statusInsert + "status:" + status);
            int insertTripExpenseCount = 0;
            if (status != 0) {
                if (!"0".equals(tripTO.getVehicleId())) {
                    insertTripExpenseCount = (Integer) getSqlMapClientTemplate().update("trip.insertTripExpenseReportCount", map);
                }
            }
            System.out.println("insertTripExpenseCount:" + insertTripExpenseCount);
            String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int i = 0; i < ConsignmentId.length; i++) {
                map.put("consignmentId", ConsignmentId[i]);
                map.put("consignmentStatus", "13");
                System.out.println("the updated map:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
                System.out.println("the status1:" + status);
            }
//             updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            //           updateTripCloserToEFS(tripTO.getTripId());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getFuelDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFuelDetails", map);
            System.out.println("getFuelDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", "0");
        }

        ArrayList tripDetails = new ArrayList();
        try {
            System.out.println(" mapiss" + map);
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseDetails", map);
            System.out.println("getOtherExpenseDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public int insertTripFuelExpense(String tripSheetId, String location, String fillDate, String fuelLitres, String fuelAmount, String fuelRemarks, String fuelPricePerLitre, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("fillDate", fillDate);
        map.put("location", location);
        map.put("fuelPricePerLitre", fuelPricePerLitre);
        map.put("fuelLitres", fuelLitres);
        map.put("fuelAmount", fuelAmount);
        map.put("fuelRemarks", fuelRemarks);
        map.put("userId", userId);
        System.out.println("the insertTripFuel" + map);
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripFuel", map);
            System.out.println("insertTripFuel size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpenseDetails() {
        Map map = new HashMap();

        ArrayList expenseDetails = new ArrayList();
        try {
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpenseDetails", map);
            System.out.println("getExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public ArrayList getExpenseDriver() {
        Map map = new HashMap();

        ArrayList expenseDriver = new ArrayList();
        try {
            expenseDriver = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpenseDriver", map);
            System.out.println("getExpenseDriver size=" + expenseDriver.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpenseDetails List", sqlException);
        }

        return expenseDriver;
    }

    public ArrayList getDriverName(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList driverNameDetails = new ArrayList();
        System.out.println("map getDriverName = " + map);
        try {
            driverNameDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverName", map);
            System.out.println("getDriverName size=" + driverNameDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName List", sqlException);
        }

        return driverNameDetails;
    }

    public int insertTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String activeValue, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }

        map.put("vehicleId", vehicleId);
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("employeeName", employeeName);
        map.put("activeValue", activeValue);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":" + "00");
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("userId", userId);
        System.out.println("the insertTripOtherExpense" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
            System.out.println("insertTripOtherExpense size=");

            //Acct Entry
            String code2 = "";
            temp = null;
            int insertStatus = 0;
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");

            code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
            temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            //select ledger details for the expense;
            map.put("expenseId", expenseName);
            String ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseLedgerInfo", map);

            String[] tempVar = ledgerDetails.split("~");
            map.put("ledgerId", tempVar[0]);
            map.put("particularsId", tempVar[1]);

            map.put("amount", expenses);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Other Expenses");
            map.put("Reference", "Trip");

            System.out.println("tripId = " + tripSheetId);
            map.put("SearchCode", tripSheetId);
            System.out.println("map1 =---------------------> " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
            System.out.println("status1 = " + insertStatus);
            //--------------------------------- acc 2nd row start --------------------------
            if (insertStatus > 0) {
                map.put("DetailCode", "2");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("Accounts_Type", "CREDIT");
                System.out.println("map2 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status2 = " + insertStatus);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpiryDateDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList expiryDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            expiryDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpiryDateDetails", map);
            System.out.println("getExpiryDateDetails.size() = " + expiryDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpiryDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpiryDateDetails List", sqlException);
        }
        return expiryDetails;
    }

    public int updateTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int expenseType1 = 0;
        int billMode1 = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("vehicleId", vehicleId);
        if (billMode != null && billMode != "") {
            billMode1 = Integer.parseInt(billMode);
            map.put("billMode", billMode1);
        }
        if (expenseType != null && expenseType != "") {
            expenseType1 = Integer.parseInt(expenseType);
            map.put("expenseType", expenseType1);
        }
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("expenseId", expenseId);
        map.put("employeeName", employeeName);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":00");
        map.put("expenseRemarks", expenseRemarks);
        map.put("activeValue", activeValue);
        map.put("userId", userId);
        System.out.println("the updateTripOtherExpense" + map);
        try {
//            status = (Integer) getSqlMapClientTemplate().update("trip.deleteOtherExpense", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpense", map);
            System.out.println("updateTripOtherExpense size=");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public int deleteTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int expenseType1 = 0;
        int billMode1 = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("vehicleId", vehicleId);
        if (billMode != null && billMode != "") {
            billMode1 = Integer.parseInt(billMode);
            map.put("billMode", billMode1);
        }
        if (expenseType != null && expenseType != "") {
            expenseType1 = Integer.parseInt(expenseType);
            map.put("expenseType", expenseType1);
        }
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("expenseId", expenseId);
        map.put("employeeName", employeeName);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":00");
        map.put("expenseRemarks", expenseRemarks);
        map.put("activeValue", activeValue);
        map.put("userId", userId);
        System.out.println("the deleteTripOtherExpense" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.deleteOtherExpense", map);

            System.out.println("deleteTripOtherExpense size=");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetailsOLD(String actualFilePath, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetailsOLD", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetails(String actualFilePath, String fileSave, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            map.put("fileSave", fileSave);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetails", map);
            status1 = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpenseDoumentUploadedDetails", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }
//Nithya End 7 Dec
//Arul Start Here 07-12-2013

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetails", map);
            if (tripAdvanceDetails.size() == 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsOld", map);
            }
            System.out.println("tripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripDieselDetails(TripTO tripTo) {
        Map map = new HashMap();
        ArrayList tripFuelDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTo.getTripSheetId());
            System.out.println(" Trip  diesel map is::" + map);
            tripFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDieselDetails", map);
            System.out.println("tripFuelDetails.size() = " + tripFuelDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDieselDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDieselDetails List", sqlException);
        }
        return tripFuelDetails;
    }

    public ArrayList getTripGrDetails(TripTO tripTo) {
        Map map = new HashMap();
        ArrayList tripFuelDetails = new ArrayList();
        try {
            map.put("tripId", tripTo.getTripId());
            map.put("grNumber", tripTo.getGrNumber());

            System.out.println(" Trip  gr map is::" + map);

            tripFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripGrDetails", map);
            System.out.println("tripGrDetails.size() = " + tripFuelDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDieselDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDieselDetails List", sqlException);
        }
        return tripFuelDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripExpenseDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripExpenseDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripExpenseDetails", map);
            System.out.println("tripExpenseDetails.size() = " + tripExpenseDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpenseDetails List", sqlException);
        }
        return tripExpenseDetails;
    }

    public ArrayList getTripStausDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("closed Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStatusDetails", map);
            System.out.println("getTripStausDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripStausDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripStausDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getPODDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPODDetails", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getTripPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPackDetails", map);
            System.out.println("getTripPackDetails.size() = " + tripPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPackDetails List", sqlException);
        }
        return tripPackDetails;
    }

    public ArrayList getTripUnPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripUnPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripUnPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripUnPackDetails", map);
            System.out.println("tripUnPackDetails.size() = " + tripUnPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripUnPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripUnPackDetails List", sqlException);
        }
        return tripUnPackDetails;
    }

    public ArrayList viewApproveDetails(String tripid, int tripclosureid) {
        Map map = new HashMap();
        ArrayList viewApproveDetails = new ArrayList();
        map.put("tripid", tripid);
        map.put("tripclosureid", tripclosureid);

        try {
            viewApproveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewApproveDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewApproveDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewApproveDetails", sqlException);
        }
        return viewApproveDetails;
    }

    public ArrayList viewRevenueApproveDetails(String customerId, int revenueApproveId, String tripId) {
        Map map = new HashMap();
        ArrayList viewApproveDetails = new ArrayList();
        map.put("customerId", customerId);
        map.put("revenueApproveId", revenueApproveId);
        map.put("tripId", tripId);
        System.out.println("map for Email content:" + map);
        try {
            viewApproveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewRevenueApproveDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewApproveDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewApproveDetails", sqlException);
        }
        return viewApproveDetails;
    }

    public ArrayList getEmailDetails(String activitycode) {
        ArrayList EmailDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            EmailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailDetails", map);
            System.out.println("EmailDetails size=" + EmailDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("EmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "EmailDetails List", sqlException);
        }

        return EmailDetails;
    }

    public ArrayList getTripCityList(TripTO tripTO) {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripCityList", map);
            System.out.println("getTripCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCityList List", sqlException);
        }

        return cityList;
    }

    public ArrayList getCityList() {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCityList", map);
            System.out.println("getCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCityList List", sqlException);
        }

        return cityList;
    }

    public ArrayList getmailcontactDetails(String tripid) {
        ArrayList getmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripid", tripid);
            getmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getmailContactDetails", map);
            System.out.println("getmailcontactDetails size=" + getmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getmailcontactDetails;
    }

    public ArrayList getTripStakeHoldersList() throws FPRuntimeException, FPBusinessException {
        ArrayList holdersList = new ArrayList();
        Map map = new HashMap();
        try {

            holdersList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStakeHolders", map);
            System.out.println("Size in holdersList " + holdersList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return holdersList;
    }

    public ArrayList getstatusList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getstatusList", map);
            System.out.println("Size in statusList " + statusList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getEmailFunction(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList setStatusList = new ArrayList();
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("stakeHolderId", tripTO.getStakeHolderId());
            map.put("holderName", tripTO.getHolderName());
            map.put("statusId", tripTO.getStatusId());
            map.put("statusName", tripTO.getStatusName());
            System.out.print("map----" + map);
            setStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return setStatusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFunc(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunc = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            assignedFunc = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunc;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFucntions(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunctions = new ArrayList();
        Map map = new HashMap();
        //String sqlQuery = "select Function_Id from Role_function where Role_Id="+roleId+"";

        try {
            map.put("statusId", tripTO.getStatusId());

            assignedFunctions = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAssignedFunctions", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunctions;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertEmailSettings(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        int updateStatus = 0;
        int insertStatus = 0;
        try {
            map.put("statusId", stackHolderId);
            System.out.println("stake holder List" + map + "length assigned func" + assignedFunc.length);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
            if (assignedFunc != null) {
                for (int i = 0; i < assignedFunc.length; i++) {
                    map.put("statusId", stackHolderId);
                    map.put("userId", userId);
                    map.put("assignedFuncId", assignedFunc[i]);
                    System.out.println("insert map" + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("trip.insertEmailFunction", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return insertStatus;
    }

    /**
     * This method used to Modify Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteStakeholder(String[] assignedFunc, String stakeHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        try {

            map.put("statusId", stakeHolderId);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public ArrayList getEditStatuslist(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEditStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    public int insertVehicleDriverAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
//        map.put("userId", userId);
        try {

            map.put("vehicleId", tripTO.getVehicleId());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("advanceDate", tripTO.getAdvanceDate());
            map.put("advanceRemarks", tripTO.getAdvanceRemarks());
            map.put("primaryDriverId", tripTO.getPrimaryDriverId());
            map.put("secondaryDriverIdOne", tripTO.getSecondaryDriverIdOne());
            map.put("secondaryDriverIdTwo", tripTO.getSecondaryDriverIdTwo());
            map.put("expenseType", tripTO.getExpenseType());
            map.put("userId", userId);
            System.out.println("map value is:" + map);
            insertVehicleDriverAdvance = (Integer) getSqlMapClientTemplate().insert("trip.insertVehicleDriverAdvance", map);
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertVehicleDriverAdvance", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

    public String checkVehicleDriverAdvance(String vehicleId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("vehicleId", vehicleId);
        String checkVehicleDriverAdvance = "";
        System.out.println("map = " + map);
        try {
            checkVehicleDriverAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvance", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverAdvance", sqlException);
        }
        return checkVehicleDriverAdvance;

    }

    public ArrayList getVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        map.put("usageTypeId", tripTO.getUsageTypeId());
        System.out.println("map = " + map);
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvanceList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDriverAdvanceList", sqlException);
        }
        return statusList;
    }

    public ArrayList getCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        Map map = new HashMap();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getZoneList() throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = null;
        Map map = new HashMap();
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getZoneList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return zoneList;
    }

    public ArrayList getLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("city", tripTO.getCityId() + "%");
        map.put("zoneId", tripTO.getZoneId());
        System.out.println("map = " + map);

        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLocation", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCity", sqlException);
        }
        return cityList;
    }

    public int insertBPCLTransactionHistory(ArrayList bpclTransactionList, TripTO tripTO1, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);
            map.put("transactionHistoryId", tripTO1.getTransactionHistoryId());
            Iterator itr = bpclTransactionList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("tripId", tripTO.getTripId());
                map.put("vehicleId", tripTO.getVehicleId());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("accountId", tripTO.getAccountId());
                map.put("dealerName", tripTO.getDealerName());
                map.put("dealerCity", tripTO.getDealerCity());
                String[] temp = null;
                String date = "";
                String time = "";
                temp = tripTO.getTransactionDate().split(" ");
                String[] temp1 = temp[0].split("-");
                date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
                time = temp[1];
                String transactionDateTime = date + " " + time;
                map.put("transactionDate", transactionDateTime);

                String[] accTemp = null;
                String accDate = "";
                String accTime = "";
                accTemp = tripTO.getAccountingDate().split(" ");
                String[] accDateTemp = accTemp[0].split("-");
                accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
                accTime = accTemp[1];
                String accDateTime = accDate + " " + accTime;
                map.put("accountingDate", accDateTime);

                map.put("transactionType", tripTO.getTransactionType());
                map.put("currency", tripTO.getCurrency());
                map.put("amount", tripTO.getAmount());
                map.put("volumeDocNo", tripTO.getVolumeDocNo());
                map.put("amountBalance", tripTO.getAmoutBalance());
                map.put("petromilesEarned", tripTO.getPetromilesEarned());
                map.put("odometerReading", tripTO.getOdometerReading());
                System.out.println("map value is:" + map);
                insertVehicleDriverAdvance += (Integer) getSqlMapClientTemplate().update("trip.insertBPCLTransactionHistory", map);
            }
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertBPCLTransactionHistory", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

//    public int insertShippingLineNoList(ArrayList shippingLineNoList, TripTO tripTO1, int userId) {
//        Map map = new HashMap();
//        int insertShippingLineNo = 0;
//        int status = 0;
//        TripTO tripTO = new TripTO();
//        try {
//            map.put("userId", userId);
//            Iterator itr = shippingLineNoList.iterator();
//            while (itr.hasNext()) {
//                tripTO = (TripTO) itr.next();
//                map.put("containerNo", tripTO.getContainerNo());
//                map.put("containerSize", tripTO.getContainerTypeId());
//                map.put("containerType", tripTO.getContainerTypeName());
//                map.put("shippingBillNo", tripTO.getShipingLineNo());
//                String shippingDate = tripTO.getShippingBillDate();
//                String gateInDate = tripTO.getGateInDate();
//                System.out.println("shippingDate---" + shippingDate);
//                System.out.println("gateInDate---" + gateInDate);
//                map.put("shippingBillDate", shippingDate);
//                map.put("gateInDate", gateInDate);
//                System.out.println("accTemp-----" + tripTO.getGateInDate());
//                map.put("requestNo", tripTO.getRequestNo());
//                map.put("vehicleNo", tripTO.getVehicleNo());
//                map.put("containerQty", tripTO.getContainerQty());
//                System.out.println("map value is:" + map);
//
//                if ("0".equals(tripTO.getFlag())) {
//                    int shippingNo = (Integer) getSqlMapClientTemplate().queryForObject("trip.getContainerExistsForShippingBillNo", map);
//                    System.out.println("shippingNo----" + shippingNo);
//
//                    if (shippingNo == 0) {
//
//                        String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripIdForShippingBillNo", map);
//                        System.out.println("map value is:tripIdtripId" + tripId);
//                        if (!"".equals(tripId) && tripId != null) {
//                            String[] temp = tripId.split("~");
//                            String[] tempTripIds = temp[0].split(",");
//                            for (int i = 0; i < tempTripIds.length; i++) {
//
//                                map.put("tripId", tempTripIds[i]);
//                                map.put("statusCount", temp[1]);
//                                System.out.println("map value is:" + map);
//                                insertShippingLineNo += (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineNoList", map);
//                                status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoForTrip", map);
//                                if (status == 1) {
//                                    status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoCount", map);
//                                }
//
//                            }
//                        }
//                        System.out.println(" sttausiss" + status);
//                        status = 0;
//                    }
//                }
//            }
//            System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
//        }
//        return insertShippingLineNo;
//    }
    public String insertCustomerCredit(ArrayList customerDetails, TripTO tripTO1, int userId) {
        Map map = new HashMap();
        int update = 0;
        //  String update = "";
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);
            Iterator itr = customerDetails.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("customerCode", tripTO.getCustomerCode());
                map.put("customerName", tripTO.getCustomerName());
                if (tripTO.getCreditAmount() == null || "".equals(tripTO.getCreditAmount())) {
                    map.put("creditAmount", "0");
                } else {
                    map.put("creditAmount", tripTO.getCreditAmount());
                }
                if (tripTO.getCreditDays() == null || "".equals(tripTO.getCreditDays())) {
                    map.put("creditDays", "0");
                } else {
                    map.put("creditDays", tripTO.getCreditDays());
                }
                if (tripTO.getPdaAmount() == null || "".equals(tripTO.getPdaAmount())) {
                    map.put("pdaAmount", "0");
                } else {
                    map.put("pdaAmount", tripTO.getPdaAmount());
                }
                if (tripTO.getFixedCreditAmount() == null || "".equals(tripTO.getFixedCreditAmount())) {
                    map.put("fixedCreditAmount", "0");
                } else {
                    map.put("fixedCreditAmount", tripTO.getFixedCreditAmount());
                }

                System.out.println("map value is:" + map);

                int customerId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getCustomerId", map);
                if (customerId != 0) {
                    map.put("customerId", customerId);
                    System.out.println("inside map value is:" + map);
                    update = (Integer) getSqlMapClientTemplate().update("trip.updateCustomerCredit", map);

                    System.out.println(" update" + update);

                }
            }
            //   System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return String.valueOf(update);
    }

    public String checkTransactionHistoryId(TripTO tripTO) {
        Map map = new HashMap();
        map.put("transactionHistoryId", tripTO.getTransactionHistoryId());
        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("accountId", tripTO.getAccountId());
        map.put("dealerName", tripTO.getDealerName());
        map.put("dealerCity", tripTO.getDealerCity());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);

        String[] accTemp = null;
        String accDate = "";
        String accTime = "";
        accTemp = tripTO.getAccountingDate().split(" ");
        String[] accDateTemp = accTemp[0].split("-");
        accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
        accTime = accTemp[1];
        String accDateTime = accDate + " " + accTime;
        map.put("accountingDate", accDateTime);
        String status = "";
        System.out.println("map = " + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkTransactionHistoryId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTransactionHistoryId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTransactionHistoryId", sqlException);
        }
        return status;

    }

    public String getTripCodeForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        String tripId = "";
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripIdForBpcl", map);
                if (tripId != null) {
                    tripId = tripId + "~" + vehicleId;
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripIdForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripIdForBpcl", sqlException);
        }
        return tripId;

    }

    public int getTripCodeCountForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripCodeCountForBpcl", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeCountForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCodeCountForBpcl", sqlException);
        }
        return tripCount;

    }

    public ArrayList getTripWrongDataList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tripWrongDataList = new ArrayList();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);

        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripWrongDataList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripWrongDataList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWrongDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripWrongDataList", sqlException);
        }
        return tripWrongDataList;
    }

    public ArrayList getBPCLTransactionHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList bpclTransactionHistory = new ArrayList();
        ArrayList bpclTransactionHistoryStartDate = new ArrayList();
        ArrayList bpclTransactionHistoryEndDate = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        String vehicleId = "";
        map.put("tripId", tripTO.getTripId());
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", tripTO.getVehicleId());
        } else {
            map.put("tripSheetId", tripTO.getTripId());
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            map.put("vehicleId", vehicleId);
        }
        System.out.println("map = " + map);
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        String startDate = "";
        String currentTripStartDate = "";
        String endDate = "";
        try {
//            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryDate", map);
            Iterator itr = bpclTransactionHistoryStartDate.iterator();
            if (itr.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr.next();
                startDate = tripTO1.getStartDate();
                currentTripStartDate = tripTO1.getCurrentTripStartDate();
                endDate = tripTO1.getEndDate();
            }
//            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryStartDate", map);
//            Iterator itr = bpclTransactionHistoryStartDate.iterator();
//
//            if(itr.hasNext()){
//                tripTO1 = new TripTO();
//                tripTO1 = (TripTO) itr.next();
//                startDate = tripTO1.getStartDate();
//            }
//            bpclTransactionHistoryEndDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryEndDate", map);
//            Iterator itr1 = bpclTransactionHistoryEndDate.iterator();
//            if(itr1.hasNext()){
//                tripTO2 = new TripTO();
//                tripTO2 = (TripTO) itr1.next();
//                endDate = tripTO2.getEndDate();
//            }
            map.put("startDate", startDate);
            map.put("currentTripStartDate", currentTripStartDate);
            map.put("endDate", endDate);
            System.out.println("map = " + map);
//            bpclTransactionHistory = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistory", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBPCLTransactionHistory", sqlException);
        }
        return bpclTransactionHistory;
    }

    public ArrayList getConsignmentListForUpdate(String consignmentOrderId) {
        Map map = new HashMap();
        map.put("consignmentOrderId", consignmentOrderId);

        ArrayList consignmentList = new ArrayList();
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentListForUpdate", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public int saveTripRoutePlanForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();
            String routeId = "0";
            map.put("orderId", consignmentId);
            map.put("pointId", originId);
            map.put("pointType", "Start Point");
            map.put("pointOrder", "1");
            map.put("pointAddresss", origin);
            map.put("pointPlanDate", plannedDate);
            map.put("pointPlanTime", plannedHour + ":" + plannedMinutes);
            map.put("routeId", routeId);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            System.out.println("saveTripRoutePlan=" + status);
            routeId = tripTO.getRouteId();
            map.put("orderId", consignmentId);
            map.put("pointId", destinationId);
            map.put("pointType", "End Point");
            map.put("pointOrder", "2");
            map.put("pointAddresss", destination);
            map.put("pointPlanDate", "00-00-0000");
            map.put("pointPlanTime", "00" + ":" + "00");
            map.put("routeId", routeId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripAdvanceDetailsStatus(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatus", map);
            if (tripAdvanceDetails.size() > 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatusOld", map);
            }
            System.out.println("getTripAdvanceDetailsStatus.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetailsStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetailsStatus List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int saveWFUTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("wfuRemarks", tripTO.getWfuRemarks());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveWFUTripSheet", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), "18");
            System.out.println("updateEndTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getWfuDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getWfuDetails", map);
            System.out.println("getWfuDetails.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWfuDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWfuDetails List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getEmptyTripMergingList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("customerId", tripTO.getCustomerId());
        map.put("tripCode", tripTO.getTripCode());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        if (tripTO.getVehicleId().equals("")) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        map.put("tripId", tripTO.getTripId());
        System.out.println("tripId=" + tripTO.getTripId() + "");
        try {
            if (tripTO.getTripId() != null && !"".equals(tripTO.getTripId())) {
                String[] tripIdNos = tripTO.getTripId().split(",");
                List tripIds = new ArrayList(tripIdNos.length);
                for (int i = 0; i < tripIdNos.length; i++) {
                    System.out.println("value:" + tripIdNos[i]);
                    tripIds.add(tripIdNos[i]);
                }
                map.put("tripIds", tripIds);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingList", map);
            } else if (tripTO.getTripId() == null) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            } else if ("".equals(tripTO.getTripId())) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            }
            System.out.println("getEmptyTripMergingList.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return emptyTripList;
    }

    public int insertEmptyTripMerging(String[] tripId, String[] tripSequence, String tripMergingRemarks, int userId) {
        Map map = new HashMap();
        int mergeId = 0;
        int mergeCount = 0;
        try {
            map.put("userId", userId);
            map.put("tripMergingRemarks", tripMergingRemarks);
            mergeId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripMergeMaster", map);
            if (mergeId > 0) {
                map.put("mergeId", mergeId);
                for (int i = 0; i < tripId.length; i++) {
                    map.put("tripId", tripId[i]);
                    if ("".equals(tripSequence[i])) {
                        map.put("tripSequence", 0);
                    } else {
                        map.put("tripSequence", tripSequence[i]);
                    }
                    mergeCount += (Integer) getSqlMapClientTemplate().update("trip.saveTripMergeDetails", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripMergeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripMergeDetails", sqlException);
        }
        return mergeId;
    }

    public String getTripCode(int tripId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("tripId", tripId);
        String tripCode = "";
        System.out.println("map = " + map);
        try {
            tripCode = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCode", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCode", sqlException);
        }
        return tripCode;

    }

    public int saveEmptyTripApproval(String tripId, String approvalStatus, String userId, String mailId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        map.put("mailId", mailId);
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int saveTripRoutePlanForSecondaryTrip(TripTO tripTO, ArrayList orderPointDetails) {
        Map map = new HashMap();
        int status = 0;
        try {
            SecondaryOperationTO operationTO = null;
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();
            String routeId = "0";
            map.put("orderId", consignmentId);
            Iterator itr = orderPointDetails.iterator();
            while (itr.hasNext()) {
                operationTO = new SecondaryOperationTO();
                operationTO = (SecondaryOperationTO) itr.next();
                map.put("pointId", operationTO.getPointId());
                map.put("pointType", operationTO.getPointType());
                map.put("pointOrder", operationTO.getPointSequence());
                map.put("pointAddresss", operationTO.getPointAddresss());
                map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                map.put("pointPlanTime", "00:00");
                map.put("routeId", 0);
                System.out.println("map value is dsdssdsdsdsdsdsdsdsdsdsds:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int checkeEmptyTripApproval(String tripId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkeEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getTempLogDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList viewTempLogDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            viewTempLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTempLogDetails", map);
            System.out.println("viewTempLogDetails.size() = " + viewTempLogDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return viewTempLogDetails;
    }

    public int saveTempLogDetails(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripTO.getTripId() != null && tripTO.getTripId() != "") {
                tripId = Integer.parseInt(tripTO.getTripId());
                map.put("tripSheetId", tripId);
            }

            map.put("fileName", fileSaved);
            map.put("userId", userId);

            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] tempFile = new byte[(int) file.length()];
            fis.read(tempFile);
            fis.close();
            map.put("tempFile", tempFile);
            System.out.println("the saveTripTempLogDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTempLogDetails", map);
            System.out.println("saveTripTempLogDetails123455 size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public int saveTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTemperatureLogApproval", map);
            System.out.println("saveTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int checkTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTemperatureLogApproval", map);
            System.out.println("checkeTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public String checkConsignmentCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        String status = "";
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkConsignmentCreditLimitStatus", map);
            System.out.println("ConsignmentCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ConsignmentCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ConsignmentCreditLimitStatus List", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the updateConsignmentOrderCreditLimitStatus" + map);
        try {
            if (tripTO.getApprovalStatus().equals("1")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitApprovalStatus", map);
            } else if (tripTO.getApprovalStatus().equals("2")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitCancelStatus", map);
            }
            System.out.println("updateConsignmentOrderCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderCreditLimitStatus", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderApprovalStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        map.put("statusId", tripTO.getStatusId());
        map.put("approvalStatusId", tripTO.getApprovalStatusId());
        map.put("userId", userId);
        System.out.println("the updateConsignmentOrderApprovalStatus" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderApprovalStatus", map);
            map.put("updateType", "System Update");
            map.put("remarks", "Credit Limit Update Status");
            status = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentStatus", map);
            System.out.println("updateConsignmentOrderApprovalStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderApprovalStatus List", sqlException);
        }

        return status;
    }

    public int insertLoadingUnloadingDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            SecondaryOperationTO operationTO = null;
            map.put("userId", userId);
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
            map.put("tripRouteCourseId", tripTO.getRouteId());
            System.out.println("map value is" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveLoadingUnloadingDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getStatusList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statuList = new ArrayList();
        try {
            statuList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusList", map);
            System.out.println("getStatusList.size() = " + statuList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusList List", sqlException);
        }
        return statuList;
    }

    public ArrayList getTripVehicleNo(TripTO tripTO) {
        Map map = new HashMap();
        if (tripTO.getStatusId().toString().equals("222")) {
            System.out.println("i am in if " + tripTO.getStatusId().toString());
            map.put("statusId", "10");

        } else {
            System.out.println("i am in else " + tripTO.getStatusId().toString());
            map.put("statusId", tripTO.getStatusId());

        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleNo", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getRunningTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println(tripTO.getConsignmentStatusId().toString());
        ArrayList tripDetails = new ArrayList();
        try {
            if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("5")) {
                map.put("orderStatus", "24");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFirstLeginProgress", map);
            } else if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("28")) {
                map.put("orderStatus", "30");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getThirdLeginProgress", map);
            } else if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("25")) {
                map.put("orderStatus", "27");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecondLeginProgress", map);
            }
            System.out.println("trip details size=" + tripDetails.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }
        return tripDetails;
    }

    public ArrayList getConsignmentPaymentDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentPaymentList = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripId());
            map.put("paymentType", tripTO.getPaymentType());
            System.out.println(" Trip map is::" + map);
            consignmentPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentPaymentDetails", map);
            System.out.println("consignmentPaymentList.size() = " + consignmentPaymentList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentPaymentDetails List", sqlException);
        }
        return consignmentPaymentList;
    }

    public ArrayList getEmailList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getEmailList = new ArrayList();
        try {
            map.put("mailId", tripTO.getEmailId());
            System.out.println(" Trip map is::" + map);
            getEmailList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailList", map);
            System.out.println("getEmailList.size() = " + getEmailList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmailList List", sqlException);
        }
        return getEmailList;
    }

    public int saveVehicleDriverAdvanceApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the saveVehicleDriverAdvanceApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvanceApproval", map);
            System.out.println("saveVehicleDriverAdvanceApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvanceApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvanceApproval List", sqlException);
        }

        return status;
    }

    public int checkVehicleDriverAdvanceApproval(String vehicleDriverAdvanceId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", vehicleDriverAdvanceId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvanceApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate ttro the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleAdvanceRequest(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("fromdate", tripTO.getFromDate());
            map.put("todate", tripTO.getToDate());
            map.put("vehicleAdvanceId", tripTO.getVehicleDriverAdvanceId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceRequest", map);
            System.out.println("getVehicleAdvanceRequest.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAdvanceRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleAdvanceRequest List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getViewVehicleDriverAdvanceList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList advanceDetails = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map is::" + map);
            String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTripId", map);
            if (tripId != null) {
                map.put("tripId", tripId);
                System.out.println(" Trip map is::" + map);
                advanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceDetailsList", map);
                System.out.println("getVehicleAdvanceDetailsList.size() = " + advanceDetails.size());
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewVehicleDriverAdvanceList List", sqlException);
        }
        return advanceDetails;
    }

    public int saveVehicleDriverAdvancePay(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("paidAdvance", tripTO.getPaidAdvance());
        map.put("paidStatus", tripTO.getPaidStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveVehicleDriverAdvancePay" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvancePay", map);
            System.out.println("saveVehicleDriverAdvancePay size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvancePay Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvancePay List", sqlException);
        }

        return status;
    }

    public String getLastBpclTransactionDate() {
        Map map = new HashMap();
        String lastBpclTxDate = "";
        System.out.println("map = " + map);
        try {
            lastBpclTxDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastBpclTransactionDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastBpclTransactionDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastBpclTransactionDate", sqlException);
        }
        return lastBpclTxDate;

    }

    public ArrayList getApprovalValueDetails(double actualval) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actualval", actualval);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getApprovalValueDetails", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getApprovalValueDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getApprovalValueDetails List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public ArrayList getRepairMaintenence(int configId) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("configId", configId);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRepairMaintenence", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRepairMaintenence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRepairMaintenence List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public String getFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryApprovalPerson(String vehicleId) {
        Map map = new HashMap();
        String approvalPersonMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            approvalPersonMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return approvalPersonMailId;

    }

    public String getPreviousTripsOdometerReading(String tripSheetId) {
        Map map = new HashMap();
        String previousTripsOdometerReading = "";
        String tripVehicleId = "";
        String previousTripDetails = "";
        String previousTripEndKm = null;
        String previousTripEndHm = null;
        String previousTripType = "";
        String reeferRequired = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                previousTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndKm", map);
                System.out.println("previousTripDetails = " + previousTripDetails);
                if (previousTripDetails != null) {
                    temp = previousTripDetails.split("~");
                    previousTripEndKm = temp[0];
                    previousTripType = temp[2];
                    reeferRequired = temp[3];
                    if (previousTripType.equals("0") && reeferRequired.equals("Yes")) {
                        previousTripEndHm = temp[1];
                    } else {
                        previousTripEndHm = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndHm", map);
                        System.out.println("previousTripEndHm = " + previousTripEndHm);
                    }
                }
                if (previousTripEndKm != null && previousTripEndHm != null) {
                    tripEndDetails = previousTripEndKm + "-" + previousTripEndHm;
                } else {
                    tripEndDetails = 0 + "-" + 0;
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripEndDetails;

    }

    public int insertBillingGraph(String tripSheetId, String graphPath, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        int insertStatus = 0;
        int updateStatus = 0;
        FileInputStream fis = null;
        File file = new File(graphPath);
        System.out.println("file = " + file);
        fis = new FileInputStream(file);
        System.out.println("fis = " + fis);
        byte[] graphFile = new byte[(int) file.length()];
        System.out.println("podFile = " + graphFile);
        fis.read(graphFile);
        fis.close();
        map.put("graphFile", graphFile);
        map.put("fileName", "dataLogExcel" + tripSheetId + ".xls");
        map.put("userId", userId);
        try {
            String status = (String) getSqlMapClientTemplate().queryForObject("trip.checkGraphStatus", map);
            if (status == null) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);
            } else {
                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateStatusBillingGraph", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return insertStatus;

    }

    public String getTripCount(TripTO tripTO) {
        Map map = new HashMap();
        String tripVehicleId = "";
        String tripCount = "";
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripStartDate", tripTO.getTripScheduleDate());
        map.put("tripEndDate", tripTO.getTripEndDate());
        System.out.println("map = " + map);
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripVehicleId", map);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                tripCount = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripCount", map);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripCount;

    }

    public int updateInvoiceAmount(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("editMode", tripTO.getEditMode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {

            map.put("billedAmount", tripTO.getBilledAmount());
            if (tripTO.getWeight() != null && !"".equals(tripTO.getWeight())) {
                map.put("weight", tripTO.getWeight());
            } else {
                map.put("weight", 0);
            }
            if (tripTO.getRatePerKg() != null && !"".equals(tripTO.getRatePerKg())) {
                map.put("ratePerKg", tripTO.getRatePerKg());
            } else {
                map.put("ratePerKg", 0);
            }
            if (tripTO.getDiscountAmount() != null && !"".equals(tripTO.getDiscountAmount())) {
                map.put("discountAmount", tripTO.getDiscountAmount());
            } else {
                map.put("discountAmount", 0);
            }
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountHeader", map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountDetail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return updateStatus;

    }

    public int insertCourierDetails(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int insertCourierDetails = 0;
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("courierNo", tripTO.getCourierNo());
        map.put("courierRemarks", tripTO.getCourierRemarks());
        map.put("userId", userId);
        try {
            insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.insertCourierDetails", map);
            if (insertCourierDetails > 0) {
                insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.updateCourierDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return insertCourierDetails;

    }

    public ArrayList getCourierDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCourierDetails", map);
            System.out.println("getCourierDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCourierDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int checkTempGraphApprovedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkTempGraphApprovedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("userId", userId);
        try {
            checkTempGraphApprovedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTempGraphApprovedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTempGraphApprovedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTempGraphApprovedStatus", sqlException);
        }
        return checkTempGraphApprovedStatus;

    }

    public int checkBillSubmittedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkBillSubmittedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {
            checkBillSubmittedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkBillSubmittedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkBillSubmittedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkBillSubmittedStatus", sqlException);
        }
        return checkBillSubmittedStatus;

    }

    public ArrayList getVehicleLogDetails(TripTO tripTO) {
        ArrayList vehicleLogDetails = new ArrayList();
        ArrayList vehicleLogDetailss = new ArrayList();
        Map map = new HashMap();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        try {
            String tripId = tripTO.getTripId();
            System.out.println("map:" + map);
            map.put("tripId", tripId);
            String startDate = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleLogDate", map);
            System.out.println("tripIdtripIdtripId:" + tripId);
            System.out.println("startDatestartDate:" + startDate);
            int transitHours = Integer.parseInt(tripTO.getTripTransitHours());
            System.out.println("transitHourstransitHours:" + transitHours);
            int totalHrs = Integer.parseInt(tripTO.getTotalHrs());
            System.out.println("totalHrstotalHrs:" + totalHrs);
            int j = 0;
            int k = transitHours / totalHrs;
            for (int i = 0; i < k; i++) {
                tripTO2 = new TripTO();
                j = j + k;
                map.put("time", j);
                map.put("startDate", startDate);
                map.put("tripId", tripId);
                System.out.println("map:" + map);

                vehicleLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLogDetails", map);
                System.out.println("tripTO1.vehicleLogDetails()" + vehicleLogDetails.size());
                if (vehicleLogDetails.size() > 0) {
                    Iterator itr4 = vehicleLogDetails.iterator();
                    while (itr4.hasNext()) {
                        tripTO1 = (TripTO) itr4.next();
                        tripTO2.setLogDateTime(tripTO1.getLogDateTime());
                        System.out.println("tripTO1.getLogDateTime()" + tripTO.getLogDateTime());
                        tripTO2.setLogDate(tripTO1.getLogDate());
                        tripTO2.setLogTime(tripTO1.getLogTime());
                        System.out.println("tripTO1.getLogTime()" + tripTO.getLogTime());
                        tripTO2.setLocation(tripTO1.getLocation());
                        tripTO2.setTemperature(tripTO1.getTemperature());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setDistance(tripTO1.getDistance());
                        System.out.println("tripTO1.getDistance()" + tripTO.getDistance());
                    }

                    vehicleLogDetailss.add(tripTO2);
                }
            }

            System.out.println("vehicleLogDetailss size=" + vehicleLogDetailss.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleLogDetails List", sqlException);
        }

        return vehicleLogDetailss;
    }

    public ArrayList getEmployeeTripDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("employeeId", tripTO.getEmployeeId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmployeeTripDetails", map);
            System.out.println("getEmployeeTripDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmployeeTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmployeeTripDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int updateEmployeeInTrip(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int status = 0;
        map.put("startDate", tripTO.getStartDate());
        map.put("endDate", tripTO.getEndDate());
        String driverChangeHour = "00";
        String driverChangeMinute = "00";
        if (tripTO.getDriverChangeHour() != null && !"".equals(tripTO.getDriverChangeHour())) {
            driverChangeHour = "00";
        } else {
            driverChangeHour = tripTO.getDriverChangeHour();
        }
        if (tripTO.getDriverChangeMinute() != null && !"".equals(tripTO.getDriverChangeMinute())) {
            driverChangeMinute = "00";
        } else {
            driverChangeMinute = tripTO.getDriverChangeMinute();
        }
        map.put("endTime", driverChangeHour + ":" + driverChangeMinute + ":00");
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("totalKm", tripTO.getTotalKM());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("balanceAmount", tripTO.getBalanceAmount());
        map.put("returnAmount", tripTO.getReturnAmount());
        map.put("advancePaid", tripTO.getActualAdvancePaid());
        map.put("staffId", tripTO.getEmployeeId());
        map.put("remarks", tripTO.getActionRemarks());
        map.put("activeInd", tripTO.getStatus());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("driverId", tripTO.getPrimaryDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startHm", tripTO.getStartHM());
        map.put("endHm", tripTO.getEndHM());
        map.put("totalHm", tripTO.getTotalHm());
        map.put("remarks", tripTO.getRemarks());
        map.put("cityId", tripTO.getCityId());
        map.put("driverCount", tripTO.getDriverCount());
        map.put("driverInTrip", tripTO.getDriverInTrip());
        map.put("userId", userId);
        String driverType = "";
        System.out.println("map update driver on a trip = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEmployeeInTrip", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.inactiveTripDriver", map);
            driverType = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverType", map);
            if (driverType.equals("P")) {
                map.put("type", "P");
            } else if (driverType.equals("S")) {
                map.put("type", "S");
            } else if (driverType.equals("T")) {
                map.put("type", "S");
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            status = (Integer) getSqlMapClientTemplate().update("employee.saveEmployeeStatus", map);
            System.out.println("updateNewDriverVehicleDriverMapping" + map);
            String vehicleid = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForNewDriver", map);
            if (vehicleid != null) {
                map.put("oldVehicleId", vehicleid);
                map.put("oldDriverId", "0");
                if (driverType.equals("P")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingPrimary", map);
                } else if (driverType.equals("S")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingSecondary", map);
                } else if (driverType.equals("T")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingTeritory", map);
                }
                System.out.println("updateOldDriverVehicleDriverMapping" + map);
//                  status = (Integer) getSqlMapClientTemplate().update("trip.updateOldDriverVehicleDriverMapping", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return status;

    }

    public String checkEmployeeInTrip(String driverId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("staffId", driverId);
        String checkEmployeeInTrip = "";
        System.out.println("map = " + map);
        try {
            checkEmployeeInTrip = (String) getSqlMapClientTemplate().queryForObject("employee.checkDriverStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeInTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeInTrip", sqlException);
        }
        return checkEmployeeInTrip;

    }

    public int insertMailDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", tripTO.getMailTypeId());
        map.put("mailSubjectTo", tripTO.getMailSubjectTo());
        map.put("mailSubjectCc", tripTO.getMailSubjectCc());
        map.put("mailSubjectBcc", tripTO.getMailSubjectBcc());
        map.put("mailContentTo", tripTO.getMailContentTo());
        map.put("mailContentCc", tripTO.getMailContentCc());
        map.put("mailContentBcc", tripTO.getMailContentBcc());
        map.put("mailTo", tripTO.getMailIdTo());
        map.put("mailCc", tripTO.getMailIdCc());
        map.put("mailBcc", tripTO.getMailIdBcc());
        map.put("status", tripTO.getStatus());
        map.put("userId", userId);
        map.put("contractRateId", tripTO.getContractRateId());
        System.out.println("map####### = " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("trip.insertMailDetails", map);
            int updateInContract = (Integer) getSqlMapClientTemplate().update("trip.updateInContract", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        return insertMailDetails;

    }

    public ArrayList getMailNotDeliveredList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList mailNotDeliveredList = new ArrayList();
        map.put("mailSendingId", tripTO.getMailSendingId());
        map.put("mailDeliveredStatusId", tripTO.getMailDeliveredStatusId());
        try {
            System.out.println("closed Trip map is::" + map);
            mailNotDeliveredList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMailNotDeliveredList", map);
            System.out.println("getMailNotDeliveredList.size() = " + mailNotDeliveredList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMailNotDeliveredList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMailNotDeliveredList List", sqlException);
        }
        return mailNotDeliveredList;
    }

    public int updateTripOtherExpenseDoneStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int updateOtherExpenseDoneStatus = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            updateOtherExpenseDoneStatus = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateOtherExpenseDoneStatus", sqlException);
        }
        return updateOtherExpenseDoneStatus;

    }

    public String getOtherExpenseDoneStatus(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        String otherExpenseDoneStatus = "";
        System.out.println("map = " + map);
        try {
            otherExpenseDoneStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOtherExpenseDoneStatus", sqlException);
        }
        return otherExpenseDoneStatus;

    }

    public String getLastUploadCustomerOutstandingDate() {
        Map map = new HashMap();
        String lastCustomerOutstandingDate = "";
        System.out.println("map = " + map);
        try {
            lastCustomerOutstandingDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastUploadCustomerOutstandingDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastUploadCustomerOutstandingDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastUploadCustomerOutstandingDate", sqlException);
        }
        return lastCustomerOutstandingDate;

    }

    public String getPreviousOutStandingAmount(TripTO tripTO) {
        Map map = new HashMap();
        map.put("customerCode", tripTO.getCustomerCode());
        map.put("customerName", tripTO.getCustomerName());
        map.put("userId", tripTO.getUserId());
        System.out.println("map = " + map);
        String previousOutStandingAmount = "";
        int updateCustomerOutStanding = 0;
        try {
            previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            System.out.println("previousOutStandingAmount = " + previousOutStandingAmount);
            if (previousOutStandingAmount == null) {
                updateCustomerOutStanding = (Integer) getSqlMapClientTemplate().update("trip.insertCustomerOutStandingAmount", map);
                System.out.println("updateCustomerOutStanding = " + updateCustomerOutStanding);
                previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousOutStandingAmount", sqlException);
        }
        return previousOutStandingAmount;

    }

    public int updateCustomerOutStandingAmount(String[] customerId, String[] outStandingAmount, int userId) {
        Map map = new HashMap();
        int updateCustomerOutStandingAmount = 0;
        double creditLimit = 0.0;
        map.put("userId", userId);
        try {
            for (int i = 0; i < customerId.length; i++) {
                map.put("customerId", customerId[i]);
                map.put("amount", outStandingAmount[i]);
                creditLimit = (Double) getSqlMapClientTemplate().queryForObject("trip.getCustomerCreditLimit", map);
                int retVal = Double.compare(creditLimit, Double.parseDouble(outStandingAmount[i]));
                if (retVal >= 0) {
                    map.put("cnoteCount", "0");
                } else if (retVal < 0) {
                    map.put("cnoteCount", "1");
                }
                System.out.println("map = " + map);
                updateCustomerOutStandingAmount += (Integer) getSqlMapClientTemplate().update("trip.updateCustomerOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCustomerOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateCustomerOutStandingAmount", sqlException);
        }
        return updateCustomerOutStandingAmount;

    }

    public ArrayList getCustomerOutStandingList() {
        Map map = new HashMap();
        ArrayList customerOutStandingList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            customerOutStandingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerOutStandingList", map);
            System.out.println("customerOutStandingList.size() = " + customerOutStandingList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerOutStandingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerOutStandingList List", sqlException);
        }
        return customerOutStandingList;
    }

    public ArrayList getSecondaryCustomerApprovalList() {
        Map map = new HashMap();
        ArrayList secondaryCustomerApprovalList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            secondaryCustomerApprovalList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecondaryCustomerApprovalList", map);
            System.out.println("getSecondaryCustomerApprovalList.size() = " + secondaryCustomerApprovalList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerApprovalList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryCustomerApprovalList List", sqlException);
        }
        return secondaryCustomerApprovalList;
    }

    public int updateSecondaryCustomerMail(TripTO tripTO) {
        Map map = new HashMap();
        int updateSecondaryCustomerMail = 0;
        try {
            map.put("customerId", tripTO.getCustomerId());
            map.put("emailId", tripTO.getEmailId());
            System.out.println("map = " + map);
            updateSecondaryCustomerMail = (Integer) getSqlMapClientTemplate().update("trip.updateSecondaryCustomerMail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateSecondaryCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateSecondaryCustomerMail;

    }

    public int updateEmptyTripRoute(TripTO tripTO) {
        Map map = new HashMap();
        int updateEmptyTripRoute = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("originId", tripTO.getOrigin());
            map.put("destinationId", tripTO.getDestination());
            map.put("cityFrom", tripTO.getCityFrom());
            map.put("cityTo", tripTO.getCityTo());
            map.put("routeInfo", tripTO.getCityFrom() + "-" + tripTO.getCityTo());
            map.put("expense", tripTO.getExpense());
            map.put("totalKm", tripTO.getTotalKm());
            map.put("totalHrs", tripTO.getTotalHours());
            map.put("totalMinutes", tripTO.getTotalMinutes());
            map.put("emptyTripRemarks", tripTO.getEmptyTripRemarks());
            map.put("routeId", tripTO.getRouteId());
            //set transit days
            float transitHours = 0.00F;
            if (tripTO.getTotalHours() != null && !"".equals(tripTO.getTotalHours())) {
                transitHours = Float.parseFloat(tripTO.getTotalHours());
            }
            double transitDay = (double) transitHours / 24;
            tripTO.setTripTransitDays(transitDay + "");
            //set advance to be paid per day
            float totalExpense = 0.00f;
            if (tripTO.getExpense() != null && !"".equals(tripTO.getExpense())) {
                totalExpense = Float.parseFloat(tripTO.getExpense());
            }

            int transitDayValue = (int) transitDay;
            if (transitDay > transitDayValue) {
                transitDay = transitDayValue + 1;
            }
            double advnaceToBePaidPerDay = 0;

            if (totalExpense > 0 && transitHours > 0) {
                advnaceToBePaidPerDay = totalExpense / transitDay;
            }
            map.put("transitDay", transitDay);
            map.put("transitHours", transitHours);
            map.put("advnaceToBePaidPerDay", advnaceToBePaidPerDay);

            System.out.println("map = " + map);
            updateEmptyTripRoute = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripMaster", map);
            System.out.println("updateEmptyTripRoute = " + updateEmptyTripRoute);
            int updateEmptyTripRouteCourse = 0;
            int updateEmptyTripConsignment = 0;
            if (updateEmptyTripRoute > 0) {
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseOrigin", map);
                System.out.println("updateEmptyTripRouteCourse one= " + updateEmptyTripRouteCourse);
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseDestination", map);
                System.out.println("updateEmptyTripRouteCourse two= " + updateEmptyTripRouteCourse);
                updateEmptyTripConsignment = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripConsignment", map);
                System.out.println("updateEmptyTripConsignment two= " + updateEmptyTripConsignment);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEmptyTripRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateEmptyTripRoute", sqlException);
        }
        return updateEmptyTripRoute;

    }

    public String getVehicleUsageTypeId(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        String usageTypeId = "";
        try {
            usageTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleUsageTypeId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleUsageTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleUsageTypeId", sqlException);
        }
        return usageTypeId;

    }

    public String getVehicleCurrentStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        System.out.println("map = " + map);
        String tripStatusId = "";
        try {
            tripStatusId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleCurrentStatus", sqlException);
        }
        return tripStatusId;

    }

    public int updateDeleteTrip(TripTO tripTO) {
        Map map = new HashMap();
        int updatedeletetrip = 0;
        int updateGr = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("tripStatusId", tripTO.getStatusId());
            map.put("remarks", tripTO.getRemarks());
            String vehicleCurrentStatus = "";
            System.out.println("map = " + map);
            if (tripTO.getStatusId().equals("3")) {
                System.out.println("i m in cancel");
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTrip", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripStatusDetails", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripAdvance", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripFuel", map);
                updateGr = (Integer) getSqlMapClientTemplate().update("trip.cancelTripGR", map);
            } else if (tripTO.getStatusId().equals("8")) {
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTrip", map);
//                updateOrderStatusToEFS(tripTO.getTripId(), "8");
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripStatusDetails", map);
            } else if (tripTO.getStatusId().equals("10")) {
                vehicleCurrentStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);
                if (vehicleCurrentStatus == null) {
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripEnd", map);
//                    updateOrderStatusToEFS(tripTO.getTripId(), "10");
                    //updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripWfu", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripVehicle", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripStatusDetails", map);
                }
            }
//            else if (tripTO.getStatusId().equals("3")) {
//                     updateGr= (Integer) getSqlMapClientTemplate().update("trip.cancelTripGR", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatedeletetrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updatedeletetrip", sqlException);
        }
        return updatedeletetrip;

    }

    public int updateTripAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("tripAdvanceId", tripTO.getTripAdvanceId());
        map.put("reqAmount", tripTO.getAdvanceAmount());

        map.put("userId", userId);
        System.out.println("the updateTripAdance" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripAdvance", map);
            System.out.println("updateTripAdvance=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripAdvance" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripAdvance", sqlException);
        }

        return status;
    }

    public int updateTripEnd(TripTO tripTO, int userId) {
        Map map = new HashMap();
        // map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
        //  map.put("startDate", tripTO.getStartDate());
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        //map.put("endOdometerReading", tripTO.getEndOdometerReading());
        //map.put("endHM", tripTO.getEndHM());
        map.put("tripId", tripTO.getTripId());
        // map.put("tripSheetId", tripTO.getTripSheetId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        // map.put("totalKM", tripTO.getTotalKM());
        // map.put("totalHrs", tripTO.getTotalHrs());
        // map.put("userId", userId);
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateEndTrip Details in the DAO " + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTrip", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripStartDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripId", tripTO.getTripId());

        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateStartTrip Details in the DAO" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripDetails", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public ArrayList getTripClosureVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripClosureVehicle = new ArrayList();
        ArrayList tripClosureVehicleList = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            System.out.println("closed Trip map is::" + map);
            tripClosureVehicle = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripClosureVehicleList", map);
            if (tripClosureVehicle.size() > 0) {
                Iterator itr = tripClosureVehicle.iterator();
                while (itr.hasNext()) {
                    tripTO2 = new TripTO();
                    tripTO1 = (TripTO) itr.next();
                    serialNo = Integer.parseInt(tripTO1.getSerialNumber());
                    tripTO2.setSerialNumber(tripTO1.getSerialNumber());
                    if (serialNo == 1) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getOriginCityName());
                        tripTO2.setStartDate(tripTO1.getStartDate());
                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays1());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    } else if (serialNo > 1 && serialNo != tripClosureVehicle.size()) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    } else if (serialNo > 1 && serialNo == tripClosureVehicle.size()) {
                        tripTO2.setTripCode(tripTO1.getTripCode());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
                        tripTO2.setEndDate(tripTO1.getEndDate());
                        tripTO2.setStartKm(tripTO1.getStartKm());
                        tripTO2.setEndKm(tripTO1.getEndKm());
                        tripTO2.setTotalKm(tripTO1.getTotalKm());
                        tripTO2.setStartHm(tripTO1.getStartHm());
                        tripTO2.setEndHm(tripTO1.getEndHm());
                        tripTO2.setTotalHm(tripTO1.getTotalHm());
                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
                        tripTO2.setVehicleId(tripTO1.getVehicleId());
                    }
                    tripClosureVehicleList.add(tripTO2);
                }
            }
            System.out.println("getTripClosureVehicleList.size() = " + tripClosureVehicleList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripClosureVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripClosureVehicleList List", sqlException);
        }
        return tripClosureVehicleList;
    }

    public ArrayList getTripSettlementVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSettlementVehicleList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSettlementVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripSettlementVehicleList", sqlException);
        }
        return customer;

    }

    public ArrayList getEmptyCustomerList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyCustomerList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getEmptyCustomerList", sqlException);
        }
        return customer;

    }

    public ArrayList getTicketingStatusList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList ticketingStatusList = new ArrayList();

        try {
            ticketingStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusList", sqlException);
        }
        return ticketingStatusList;

    }

    public ArrayList getTicketingList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        map.put("roleId", tripTO.getRoleId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingDetailsList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingStatusDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusDetailsList", sqlException);
        }
        return ticketingList;
    }

    public int saveTicketFile(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("fileName", fileSaved);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] ticketFile = new byte[(int) file.length()];
            System.out.println("ticketFile = " + ticketFile);
            fis.read(ticketFile);
            fis.close();
            map.put("ticketFile", ticketFile);
            map.put("ticketId", tripTO.getTicketId());
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int saveTicket(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("status", tripTO.getStatus());
            map.put("type", tripTO.getType());
            map.put("from", tripTO.getFrom());
            map.put("to", tripTO.getTo());
            map.put("cc", tripTO.getCc());
            map.put("title", tripTO.getTitle());
            map.put("message", tripTO.getMessage());
            map.put("priority", tripTO.getPriority());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().insert("trip.saveTicket", map);
            map.put("ticketId", ticketId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int updateTicketStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("ticketId", tripTO.getTicketId());
            map.put("statusId", tripTO.getStatusId());
            map.put("message", tripTO.getMessage());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().update("trip.updateTicketMaster", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTicketMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTicketMaster List", sqlException);
        }

        return ticketId;
    }

    public ArrayList gettripsettelmentDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripsettelmentDetails = new ArrayList();
        System.out.println(" gettripsettelmentDetails map:" + map);
        try {

            tripsettelmentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.gettripsettelmentDetails", map);

            System.out.println(" gettripsettelmentDetails size=" + tripsettelmentDetails.size());
            System.out.println(" gettripsettelmentDetails elements=" + tripsettelmentDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog(" gettripsettelmentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripsettelmentDetails List", sqlException);
        }

        return tripsettelmentDetails;
    }

    public String getPreviousTripsEndTime(String tripSheetId) {
        Map map = new HashMap();

        String tripVehicleId = "";
        String tripVehicleId1 = "";
        String previousTripEndDetails = "";
        String previousTripType = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        String previousTripEndDate = null;
        String previousTripEndTime = null;
        ArrayList tripClosureVehicle = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        String PreviousTripId = "";
        try {

            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);

            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null && tripVehicleId != "") {
                map.put("tripVehicleId", tripVehicleId);

                PreviousTripId = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripId", map);
                System.out.println("tripVehicleId = " + PreviousTripId);
                if (PreviousTripId != null && PreviousTripId != "") {
                    map.put("tripId", PreviousTripId);
                    previousTripEndDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndTime", map);
                    System.out.println("previousTripEndTimeDetails = " + previousTripEndDetails);
                }
            }
            if (previousTripEndDetails != null && previousTripEndDetails != "") {
                System.out.println("previousTripEndDetails = " + previousTripEndDetails);
                temp = previousTripEndDetails.split("~");
                previousTripEndDate = temp[0];
                previousTripEndTime = temp[1];
            }
            System.out.println("previousTripEndDate = " + previousTripEndDate);
            System.out.println("previousTripEndTime = " + previousTripEndTime);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return previousTripEndDetails;

    }

    public String getTripVehicleStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        String tripVehicleStatus = "";
        try {

            tripVehicleStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripVehicleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripVehicleStatus List", sqlException);
        }

        return tripVehicleStatus;
    }

    public ArrayList getSecFCmailcontactDetails(String customerName) {
        ArrayList getSecFCmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("customerName", customerName);
            getSecFCmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecFCmailContactDetails", map);
            System.out.println("getSecFCmailcontactDetails size=" + getSecFCmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getSecFCmailcontactDetails;
    }

    public int updateNextTrip(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("nextTrip", tripTO.getNextTrip());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateNextTrip", map);
            System.out.println("updateNextTrip =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateNextTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateNextTrip List", sqlException);
        }

        return ticketId;
    }

    public ArrayList getPrimaryDriverSettlementTrip(TripTO tripTO) {
        ArrayList primaryDriverSettlementTrip = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            System.out.println("map getPrimaryDriverSettlementTrip = " + map);
            primaryDriverSettlementTrip = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrimaryDriverSettlementTrip", map);
            System.out.println("getPrimaryDriverSettlementTrip size=" + primaryDriverSettlementTrip.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverSettlementTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverSettlementTrip List", sqlException);
        }

        return primaryDriverSettlementTrip;
    }

    public ArrayList getVehicleDriverAdvance(TripTO tripTO) {
        ArrayList vehicleDriverAdvance = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            vehicleDriverAdvance = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvance", map);
            System.out.println("getSecFCmailcontactDetails size=" + vehicleDriverAdvance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDriverAdvance List", sqlException);
        }

        return vehicleDriverAdvance;
    }

    public ArrayList getDriverIdleBhatta(TripTO tripTO) {
        ArrayList driverIdleBhatta = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverIdleBhatta = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverIdleBhatta", map);
            System.out.println("getDriverIdleBhatta size=" + driverIdleBhatta.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIdleBhatta Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIdleBhatta List", sqlException);
        }

        return driverIdleBhatta;
    }

    public String getDriverLastBalanceAmount(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverLastBalanceAmount", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverLastBalanceAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverLastBalanceAmount List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public int insertPrimaryDriverSettlement(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            map.put("driverLastBalance", tripTO.getDriverLastBalance());
            map.put("tripAmount", tripTO.getTripAmount());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("settleAmount", tripTO.getSettleAmount());
            map.put("payAmount", tripTO.getPayAmount());
            map.put("paymentMode", tripTO.getPaymentMode());
            map.put("balanceAmount", tripTO.getBalanceAmount());
            map.put("settlementTripSize", tripTO.getSettlementTripsSize());
            map.put("bhattaAmount", tripTO.getBhattaAmount());
            map.put("idleDays", tripTO.getIdleDays());
            map.put("settlementRemarks", tripTO.getRemarks());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.insertPrimaryDriverSettlement", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int insertPrimaryDriverSettlementDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverSettlementId", tripTO.getStatus());
            map.put("transactionType", tripTO.getTransactionType());
            map.put("transactionId", tripTO.getTransactionId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("driverId", tripTO.getDriverId());
            map.put("transactionAmount", tripTO.getTransactionAmount());
            map.put("driverCount", tripTO.getDriverCount());
            map.put("settlementAmount", tripTO.getAmount());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPrimaryDriverSettlementDetails", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int updateIdleBhattaStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("idleBhattaId", tripTO.getIdleBhattaId());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateIdleBhattaStatus", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getTripOtherExpenseDocumentRequired(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("map in other expense document:=" + map);
        ArrayList getDocumentRequiredDetails = new ArrayList();
        try {

            getDocumentRequiredDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOtherExpenseDocumentRequired", map);
            System.out.println("getDocumentRequiredDetails.size() = " + getDocumentRequiredDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDocumentRequiredDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDocumentRequiredDetails", sqlException);
        }

        return getDocumentRequiredDetails;
    }

    public ArrayList getOtherExpenseFileDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseFile", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public int deleteExpenaseBillCopy(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        int updateStatus1 = 0;
        try {

            map.put("expenseId", tripTO.getExpenseId());
            System.out.println("map for expense delete:" + map);

            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteExpenseBillCopy", map);
            updateStatus1 = (Integer) getSqlMapClientTemplate().update("trip.updateTripExpenseForBillDelete", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public String getTripUnclearedBalance(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        try {
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTripUnclearedBalance", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripUnclearedBalance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripUnclearedBalance List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public ArrayList getTripSheetDetailsForChallan(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetailsForChallan", map);
            System.out.println("getTripSheetDetails for challan size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetailsforchallan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetailsforchallan List", sqlException);
        }

        return tripDetails;
    }

    public String getTotalBillExpenseForDriver(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        String billExpense = "";
        try {

            billExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTotalBillExpenseForDriver", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalBillExpenseForDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalBillExpenseForDriver List", sqlException);
        }

        return billExpense;
    }

    public ArrayList getOrderDetailsForEnd(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList orderDetails = new ArrayList();
        try {
            orderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderDetailsForEnd", map);
            System.out.println("getTripSheetDetails for challan size=" + orderDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderDetailsForEnd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderDetailsForEnd", sqlException);
        }

        return orderDetails;
    }

    public ArrayList getVehicleForGpsCurrentLocation(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList vehicleDetails = new ArrayList();
        try {
            vehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleForGpsCurrentLocation", map);
            System.out.println("getVehicleForGpsCurrentLocation for challan size=" + vehicleDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleForGpsCurrentLocation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleForGpsCurrentLocation", sqlException);
        }

        return vehicleDetails;
    }

    public int getRouteExpenseDetails(String val, String vehicleTypeId, TripTO tripTO) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();

        int routeExpense = 0;
        int update = 0;
        //  int OriginHubId=0;
        // int DestinationHubId=0;
        List ConsignmentNos = new ArrayList();
        List hubId = new ArrayList();
        int Expense = 0;
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            String[] pointIds = val.split(":");
            System.out.println(pointIds.length);
            for (int i = 1; i <= pointIds.length - 1; i++) {
                String[] consignmentList = pointIds[i].split("~");
                System.out.println(consignmentList.length);
                for (int k = 0; k < (consignmentList.length - 1); k++) {
                    map.put("consignmentId", consignmentList[k]);
                    map.put("sequenceId", consignmentList[k + 1]);
                    System.out.println("C-note id map:" + map);
                    update = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentExecutionSequence", map);
                    System.out.println("updated:" + update);
                    ConsignmentNos.add(consignmentList[k]);
                }

                System.out.println("outer loop:");
            }
            map.put("ConsignmentNos", ConsignmentNos);
            System.out.println("C-note id map with:" + ConsignmentNos);
            hubId = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHubId", map);
            System.out.println("hubId with:" + hubId.size());
            for (int j = 0; j < hubId.size() - 1; j++) {
                TripTO OriginHubId = null;
                TripTO DestinationHubId = null;
                OriginHubId = (TripTO) hubId.get(j);
                if (hubId.get(j + 1) != hubId.get(j)) {
                    DestinationHubId = (TripTO) hubId.get(j + 1);
                }
                map.put("OriginHubId", OriginHubId.getHubId());
                map.put("DestinationHubId", DestinationHubId.getHubId());
                System.out.println("last map with:" + map);
                Expense = Expense + (Integer) getSqlMapClientTemplate().queryForObject("trip.getRouteExpense", map);
            }

            System.out.println("routeExpense = " + Expense);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return Expense;
    }

    public double getSecondaryRouteExpenseDetails(String totalkm, String vehicleTypeId, String originId) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();
        int routeExpense = 0;
        Double expense = 0.0;
        try {
            String vehmilegfuil;
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("originId", originId);
            System.out.println("fuel query map" + map);
            //System.out.println("closed Trip map is::" + map);
            vehmilegfuil = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleMileg", map);
            System.out.println(vehmilegfuil);
            String Fuel = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelRate", map);
            System.out.println(Fuel);
            expense = ((Integer.parseInt(totalkm) / Double.parseDouble(vehmilegfuil)) * Double.parseDouble(Fuel));

            System.out.println(totalkm + "routeExpense = " + expense);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return expense;
    }

    public String getPointKm(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("originId", tripTO.getOrigin());
        map.put("destinationId", tripTO.getDestination());
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);

        String pointkm = "";
        ArrayList pointkm1 = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            pointkm = (String) getSqlMapClientTemplate().queryForObject("trip.getPointKM", map);
            System.out.println("pointkm size=" + pointkm);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return pointkm;
    }

    public ArrayList getConfigurationMaster(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList configurationMaster = new ArrayList();

        try {
            System.out.println("map: in the dao" + map);
            configurationMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConfigurationMaster", map);
            System.out.println("getConfigurationMaster size=" + configurationMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return configurationMaster;
    }
    //get google data

    public ArrayList getRouteDistanceCity(String origin, String destination) throws MalformedURLException, IOException {
        int status = 0;
        if (origin.contains(",")) {
            origin = origin.replace(",", "");
        }
        if (destination.contains(",")) {
            destination = destination.replace(",", "");
        }
        URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin.replace(" ", "") + "&destinations=" + destination.replace(" ", ""));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        String line, outputString = "";
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        while ((line = reader.readLine()) != null) {
            outputString += line;
        }
        System.out.println(outputString);
        String jsonText = outputString;
//String status[]=null;
        String error = "";
        ArrayList finalList = new ArrayList();
        try {
            JSONParser parser = new JSONParser();
            String str1 = "";
            ArrayList ar = new ArrayList();
            ArrayList ar1 = new ArrayList();

            KeyFinder finder = new KeyFinder();
            finder.setMatchKey("status");
            while (!finder.isEnd()) {
                parser.parse(jsonText, finder, true);
                if (finder.isFound()) {
                    finder.setFound(false);
                    System.out.println("RND " + finder.getValue());
                    ar.add(finder.getValue().toString());
                }
            }
            String str[] = new String[ar.size()];
            String splitkm[] = new String[5];
            String traveltime[] = new String[5];
            int indexArr = 0;
            Iterator itr = ar.iterator();
            while (itr.hasNext()) {
                str[indexArr] = itr.next().toString();
                ++indexArr;
                System.out.println(itr.next());
            }
            System.out.println("my 0 position " + str[0]);
            if (str[0].equalsIgnoreCase("OK")) {
                error = "ok";
                JSONParser parser1 = new JSONParser();
                KeyFinder finder1 = new KeyFinder();
                finder1.setMatchKey("text");
                while (!finder1.isEnd()) {
                    parser1.parse(jsonText, finder1, true);
                    if (finder1.isFound()) {
                        finder1.setFound(false);
                        System.out.println("RND " + finder1.getValue());
                        ar1.add(finder1.getValue().toString());
                    }
                }
                System.out.println("size" + ar1.size());
                Iterator it1 = ar1.iterator();
                String forkm[] = new String[ar1.size()];
                int index1 = 0;
                while (it1.hasNext()) {
                    forkm[index1] = it1.next().toString();
                    ++index1;
                }
                System.out.println("fddd" + forkm[0] + "  dfsdf" + forkm[1]);
                splitkm = forkm[0].split(" ");
                traveltime = forkm[1].split(" ");
                System.out.println("finalList" + finalList.size());
                finalList.add(splitkm[0].replace(",", ""));
                System.out.println("lenth of travel time" + traveltime.length);
                for (int j = 0; j < traveltime.length; j++) {
                    System.out.println("data" + traveltime[j]);
                    finalList.add(traveltime[j]);
                    System.out.println("finalList" + finalList.size());
                }
            } else {
                error = "not";
                System.out.println("wrong origin destination" + error);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalList;
    }

    public int createRoute(ArrayList googleData, ArrayList country, String vehicleType, String origin, String destination) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList configurationMaster = new ArrayList();
        int routeId = 0;
        try {
            String param[] = new String[6];
            String countryI[] = new String[5];
            int i = 0;
            i = 0;
            map.put("countryId", "2");
            Iterator itr = googleData.iterator();
            while (itr.hasNext()) {
                param[i] = itr.next().toString();
                i++;
            }
            System.out.println("param lenth is " + param.length);
            String val = "", val1 = "";
            int prlength = param.length;
            int totalHrs = 0, totalmint = 0;
            double travelkm = 0.0;
            for (int j = 1; j < param.length; j++) {
                System.out.println("j " + j);
                if (param[j + 1].equalsIgnoreCase("day")) {
                    totalHrs = Integer.parseInt(param[j]) * 24;
                    totalHrs = totalHrs + Integer.parseInt(param[j + 2]);
                    totalmint = 0;
                    //Integer.parseInt(param[j+2])*60;
                } else if (param[j + 1].equalsIgnoreCase("hours")) {
                    totalHrs = Integer.parseInt(param[j]);
                    totalmint = Integer.parseInt(param[j + 2]);
                } else if (param[j + 1].equalsIgnoreCase("mins")) {
                    totalHrs = 0;
                    totalmint = Integer.parseInt(param[j]);
                }
                break;
            }
            travelkm = Double.parseDouble(param[0]);
            System.out.println("totalhrs" + totalHrs + "total minuts" + totalmint + "travel km" + travelkm);

            System.out.println("For source and destination ID" + origin + " " + destination);
            String nextRouteCode = (String) getSqlMapClientTemplate().queryForObject("trip.getNextRouteCode", map);
            System.out.println("next Route Code" + nextRouteCode);
            if (nextRouteCode != null) {
                if (nextRouteCode.length() == 3) {
                    nextRouteCode = "RC" + nextRouteCode;
                } else if (nextRouteCode.length() == 2) {
                    nextRouteCode = "RC0" + nextRouteCode;
                } else {
                    nextRouteCode = "RC00" + nextRouteCode;
                }
            } else {
                nextRouteCode = "RC001";
            }
            System.out.println("next Route Code after concat" + nextRouteCode);
            System.out.println("map for fuel:" + map);
            String fuelprice = (String) getSqlMapClientTemplate().queryForObject("trip.getCurrentFuelPrice", map);
            int flag = 0;
            System.out.println("map for getConfigurationMaster: " + map);
            configurationMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConfigurationMaster", map);
            Iterator itr1 = configurationMaster.iterator();
            TripTO tpTO = new TripTO();
            while (itr1.hasNext()) {
                tpTO = new TripTO();
                tpTO = (TripTO) itr1.next();
                String vehicleExpesne = String.valueOf(Double.parseDouble(tpTO.getFuelCostPerKm()) * travelkm);
                map.put("vehicleTypeId", tpTO.getVehicleTypeId());
                map.put("fuelCostPerKm", tpTO.getFuelCostPerKm());
                map.put("tollCostPerKm", tpTO.getTollCostPerKm());
                map.put("miscCostKm", tpTO.getMiscCostKm());
                map.put("driverIncentive", tpTO.getDriverIncentive());
                map.put("routeCode", nextRouteCode);
                map.put("totalHours", totalHrs);
                map.put("totalMins", totalmint);
                map.put("travelKm", travelkm);
                map.put("roadType", "NH");
                map.put("routeToId", destination);
                map.put("routeFromId", origin);
                map.put("fuelPrice", fuelprice);
                map.put("vehicleExpesne", vehicleExpesne);
                System.out.println("map for route Master:" + map);
                if (flag == 0) {

                    routeId = (Integer) getSqlMapClientTemplate().insert("trip.inserInMaster", map);
                }
                flag = 1;
                int maxid = (Integer) getSqlMapClientTemplate().queryForObject("trip.getlastRouteId", map);
                map.put("routeId", maxid);

                System.out.println("map for route cost:" + map);
                int insertcost = (Integer) getSqlMapClientTemplate().insert("trip.inserInCostMaster", map);
                System.out.println("insertcost" + insertcost);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return routeId;
    }

    public ArrayList getDistanceList(String[] consignmentOrderIds, String[] originId, String pickupPoint) {
        Map map = new HashMap();
        ArrayList distanceList = new ArrayList();
        try {
            calculateDistance cd = new calculateDistance();
            TripTO tripTO = new TripTO();
            String originLatLong = "";
            map.put("consignmentOrderIds", consignmentOrderIds);
            String origins = (String) getSqlMapClientTemplate().queryForObject("trip.getDistinctOrigins", map);
            String[] distinctOrigins = origins.split(",");
            System.out.println("consignmentOrderIds.length...:" + consignmentOrderIds.length);

            System.out.println("originId.length " + originId.length);
            System.out.println("distict originid length" + distinctOrigins.length);

            for (int j = 0; j < distinctOrigins.length; j++) {

                System.out.println("i m in first loop");
                System.out.println("distinctOrigins[j] before loop:" + distinctOrigins[j]);
                System.out.println("pickupPoint before loop:" + pickupPoint);
                if (!distinctOrigins[j].equals(pickupPoint)) {
                    System.out.println("pickupPoint in main loop " + pickupPoint);
                    System.out.println("distict originId in main loop " + distinctOrigins[j]);
                    double lat1 = 0.00;
                    double long1 = 0.00;
                    double lat2 = 0.00;
                    double long2 = 0.00;
                    double distance = 0.00;
                    int cntr = 0;
                    String temp1 = "";
                    String temp2 = "";
                    tripTO = new TripTO();
                    map.put("origin", pickupPoint);
                    originLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                    if (!"".equals(originLatLong) && originLatLong != null) {
                        String temp[] = originLatLong.split("~");
                        lat1 = Double.parseDouble(temp[0]);
                        long1 = Double.parseDouble(temp[1]);
                    }
                    for (int i = 0; i < distinctOrigins.length; i++) {
                        System.out.println("i m in second loop");
                        String orgin = "";
                        if (!distinctOrigins[i].equals(pickupPoint)) {
                            orgin = distinctOrigins[i];
                            System.out.println("orgin " + orgin);
                            System.out.println("originId[i] " + distinctOrigins[i]);
                            System.out.println("pickupPoint " + pickupPoint);
                            map.put("origin", distinctOrigins[i]);
                            System.out.println("inside ids map" + map);
                            String destinationLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                            if (!"".equals(destinationLatLong) && destinationLatLong != null) {
                                String temp[] = destinationLatLong.split("~");
                                lat2 = Double.parseDouble(temp[0]);
                                long2 = Double.parseDouble(temp[1]);
                            }
                            distance = cd.distance(lat1, long1, lat2, long2, 'K');
                            if (cntr == 0) {
                                temp1 = String.valueOf(distance);
                                temp2 = distinctOrigins[i];
                                System.out.println("distance 111..:" + distance);
                            } else {
                                temp1 = temp1 + "~" + String.valueOf(distance);
                                temp2 = temp2 + "~" + distinctOrigins[i];
                                System.out.println("distance array :" + temp1);
                            }
                            cntr++;
                        }

                    }
                    System.out.println("distinctOrigins 111..lat2:" + lat2);
                    System.out.println("distinctOrigins 111..lat1:" + lat1);
                    System.out.println("distinctOrigins 111..long2:" + long2);
                    System.out.println("distinctOrigins 111..long1:" + long1);

                    String[] tempVal = null;
                    tempVal = temp1.split("~");
                    String[] tempVal2 = temp2.split("~");
                    double smallValue = Double.parseDouble(tempVal[0]);
                    System.out.println("tempVal.length..:" + tempVal.length);
                    System.out.println("tempVal2.length..:" + tempVal2.length);
                    for (int i = 0; i < tempVal.length; i++) {
                        if (tempVal.length > 1) {
                            if (Double.parseDouble(tempVal[i]) < smallValue) {
                                smallValue = Double.parseDouble(tempVal[i]);
                                pickupPoint = tempVal2[i];

                                System.out.println("Shortest Distance:" + smallValue);
                                System.out.println("nearest Point:" + pickupPoint);
                                System.out.println("new pickup point :" + pickupPoint);
                            }
                        } else if (Double.parseDouble(tempVal[i]) == smallValue) {
                            smallValue = Double.parseDouble(tempVal[i]);
                            pickupPoint = tempVal2[i];
                            System.out.println("nearest Point else:" + pickupPoint);
                            System.out.println("shortest lenght else:" + smallValue);
                            System.out.println("new pickup point else:" + pickupPoint);
                        }
                    }
                    map.put("origin", pickupPoint);
                    System.out.println("map of nearest Point:" + map);
                    originLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                    if (!"".equals(originLatLong) && originLatLong != null) {
                        String temp[] = originLatLong.split("~");
                        lat1 = Double.parseDouble(temp[0]);
                        long1 = Double.parseDouble(temp[1]);
                    }
                    tripTO.setOriginId(pickupPoint);
                    tripTO.setDistance(String.valueOf(smallValue));
                    tripTO.setLatitude(String.valueOf(lat1));
                    tripTO.setLongitude(String.valueOf(long1));
                    distanceList.add(tripTO);
                    System.out.println("tripTO.pickupPoint" + pickupPoint);
                    System.out.println("tripTO.setOriginId" + tripTO.getOriginId());
                    System.out.println("tripTO.setDistance" + tripTO.getDistance());
                    System.out.println("tripTO.setLatitude" + tripTO.getLatitude());
                    System.out.println("tripTO.setLongitude" + tripTO.getLongitude());

                }

            }

            System.out.println("map: in the dao" + map);
            System.out.println("distanceList size=" + distanceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDistanceList List", sqlException);
        }

        return distanceList;
    }

    public String getRouteExpenseDetailsForVehicleType(String totalkm, String vehicleTypeId, String vendorId,
            String sourceId, String destinationId, String ownership, String noOfContianer, String movementType,
            String point1Id, String point2Id, String point3Id,
            String vehicleCategory, String productInfo,
            String containerTypeId, String companyId, String vehicleNo, String routeContractId) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();
        String routeExpense = "";
        Double expense = 0.00d;
        String returnValue = "";
        try {
            String vehmilegfuil;
            String costPerKm = "";
            String expense1 = "";
            ArrayList configurationMaster = new ArrayList();
            map.put("countryId", "2");
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("totalkm", totalkm);
            map.put("vendorId", vendorId);
            map.put("sourceId", sourceId);
            map.put("point1Id", point1Id);
            map.put("point2Id", point2Id);
            map.put("point3Id", point3Id);
            map.put("point4Id", "");
            map.put("firstPointId", point1Id);
            map.put("destinationId", destinationId);
            map.put("ownership", ownership);
            map.put("noOfConatiner", noOfContianer);
            map.put("movementType", movementType);
            map.put("productInfo", productInfo);
            map.put("companyId", companyId);
            map.put("vehicleNo", vehicleNo);

            map.put("firstPointId", sourceId);
            map.put("finalPointId", destinationId);
            map.put("containerQty", noOfContianer);
            map.put("containerTypeId", containerTypeId);
            map.put("contractId", routeContractId);

            int loadType = Integer.parseInt(movementType);
            System.out.println("loadType#######" + loadType);
            if (loadType == 1 || loadType == 2 || loadType == 4 || loadType == 5 || loadType == 6) {
                map.put("movementType", "2");
                map.put("loadTypeId", "2");
            } else {
                map.put("movementType", "1");
                map.put("loadTypeId", "1");
            }

            //  expense = ((Integer.parseInt(totalkm) / Double.parseDouble(vehmilegfuil)) * Double.parseDouble(Fuel));
            // if( !"".equals(vendorId.trim())|| vendorId.trim()!=null){
            System.out.println(" map is::" + map);
            returnValue = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleExpense", map);
            if (returnValue == null) {
                returnValue = "0";
            }
            String approvalStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates1", map);

            String totalFuel = (String) getSqlMapClientTemplate().queryForObject("trip.getTripFuelForVehicle", map);
            System.out.println("totalFuel " + totalFuel);
            if (totalFuel == null) {
                totalFuel = "0.00~0.00";
            }
            System.out.println("approvalStatus = " + approvalStatus);
            System.out.println("totalFuel = " + totalFuel);
            System.out.println("returnValue = " + returnValue);

            returnValue = approvalStatus + "#" + totalFuel + "@" + returnValue;
            System.out.println("returnValue--1---" + returnValue);
            if (!"3".equals(ownership) || "1".equals(vehicleCategory)) {

            } else {
                System.out.println("else....");
                System.out.println(" map is::" + map);
                expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForVendorVehicle", map);
                System.out.println("expense1 " + expense1);
                if (expense1 == null) {
                    expense1 = "0.00";
                }

                returnValue = returnValue + "-" + expense1;

            }
            System.out.println("returnValue-----2------------" + returnValue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return returnValue;
    }

    public String getOrgRouteExpenseDetailsForVehicleType(String totalkm, String vehicleTypeId, String vendorId,
            String sourceId, String destinationId, String ownership, String noOfContianer, String movementType,
            String point1Id, String point2Id,
            String vehicleCategory, String productInfo,
            String containerTypeId, String companyId, String vehicleNo, String routeContractId) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();
        String routeExpense = "";
        Double expense = 0.00d;
        String returnValue = "";
        String expense1 = "";
        try {
            String vehmilegfuil;
            String costPerKm = "";
            ArrayList configurationMaster = new ArrayList();
            map.put("countryId", "2");
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("totalkm", totalkm);
            map.put("vendorId", vendorId);
            map.put("sourceId", sourceId);
            map.put("point1Id", point1Id);
            map.put("point2Id", point2Id);
            map.put("point3Id", "");
            map.put("point4Id", "");
            map.put("firstPointId", point1Id);
            map.put("destinationId", destinationId);
            map.put("ownership", ownership);
            map.put("noOfConatiner", noOfContianer);
            map.put("movementType", movementType);
            map.put("productInfo", productInfo);
            map.put("companyId", companyId);
            map.put("vehicleNo", vehicleNo);

            map.put("firstPointId", sourceId);
            map.put("finalPointId", destinationId);
            map.put("containerQty", noOfContianer);
            map.put("containerTypeId", containerTypeId);
            map.put("contractId", routeContractId);

            int loadType = Integer.parseInt(movementType);

            if (loadType == 1 || loadType == 2 || loadType == 4 || loadType == 5 || loadType == 6) {
                map.put("movementType", "2");
                map.put("loadTypeId", "2");
            } else {
                map.put("movementType", "1");
                map.put("loadTypeId", "1");
            }

            System.out.println("getOrgRouteExpenseDetailsForVehicleType  map is::" + map);
            expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripOrgExpenseForVehicle", map);
            System.out.println("OrgRouteExpense " + expense1);
            if (expense1 == null) {
                expense1 = "0.00";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return expense1;
    }

    public int saveTripVehicleForVehicleDeattach(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "23");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripSheetForVehicleDeattach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicleForVehicleDeattach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForVehicleDeattach", map);
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            //  maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            //  map.put("vehicleSequence", maxVehicleSequence);
            //   updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicleForVehicleAttach(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("originId", tripTO.getOriginId());
            map.put("expectedArrivalDate", tripTO.getTripPlanEndDate());
            map.put("expectedArrivalTime", tripTO.getTripPlanEndTime());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "10");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripSheetForVehicleAttach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicleForVehicleAttach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForVehicleAttach", map);
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTrailerDetails(String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String[] uniqueId, TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("originId", tripTO.getOriginId());
        //update trailer availability
        String[] pointPlanDate = tripTO.getPointPlanDate();
        String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
        String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
        //map.put("pointPlanDate", pointPlanDate[0]);
        //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
        if (pointPlanDate != null) {
            map.put("tripScheduleDate", pointPlanDate[0]);
        } else {
            map.put("tripScheduleDate", "00-00-0000");
        }
        if (pointPlanTimeHrs != null) {
            map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0] + ":" + "00");
        } else {
            map.put("tripScheduleTime", "00" + ":" + "00" + ":" + "00");
        }
        map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
        //

        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        try {

//                System.out.println("trailerId.length:"+trailerId.length);
//                for(int i = 0; i<trailerId.length;i++ ){
//                   map.put("trailerId",trailerId[i]) ;
//                   map.put("trailerRemarks",trailerRemarks[i]) ;
//                    System.out.println("trailerInsert map:"+map);
//                   status = (Integer) getSqlMapClientTemplate().update("trip.insertTrailerDetails", map);
//                    int traileAvailUpdate=(Integer) getSqlMapClientTemplate().update("trip.updateTrailerAvailabiltyForFreeze", map);
//                   System.out.println("traileAvailUpdate = " + traileAvailUpdate);
//                }
            for (int i = 0; i < containerTypeId.length; i++) {
                map.put("containerTypeId", containerTypeId[i]);
                map.put("containerNo", containerNo[i]);
                map.put("consignmentContainerId", uniqueId[i]);
                System.out.println("the container map" + map);
                status = (Integer) session.update("trip.insertContainerDetails", map);
                System.out.println("container Status..." + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int saveTrailerDetails(String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String[] uniqueId, TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("originId", tripTO.getOriginId());
        //update trailer availability
        String[] pointPlanDate = tripTO.getPointPlanDate();
        String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
        String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
        //map.put("pointPlanDate", pointPlanDate[0]);
        //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
        if (pointPlanDate != null) {
            map.put("tripScheduleDate", pointPlanDate[0]);
        } else {
            map.put("tripScheduleDate", "00-00-0000");
        }
        if (pointPlanTimeHrs != null) {
            map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0] + ":" + "00");
        } else {
            map.put("tripScheduleTime", "00" + ":" + "00" + ":" + "00");
        }
        map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
        //

        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        try {

//                System.out.println("trailerId.length:"+trailerId.length);
//                for(int i = 0; i<trailerId.length;i++ ){
//                   map.put("trailerId",trailerId[i]) ;
//                   map.put("trailerRemarks",trailerRemarks[i]) ;
//                    System.out.println("trailerInsert map:"+map);
//                   status = (Integer) getSqlMapClientTemplate().update("trip.insertTrailerDetails", map);
//                    int traileAvailUpdate=(Integer) getSqlMapClientTemplate().update("trip.updateTrailerAvailabiltyForFreeze", map);
//                   System.out.println("traileAvailUpdate = " + traileAvailUpdate);
//                }
            for (int i = 0; i < containerTypeId.length; i++) {
                map.put("containerTypeId", containerTypeId[i]);
                map.put("containerNo", containerNo[i]);
                map.put("consignmentContainerId", uniqueId[i]);
                System.out.println("the container map" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertContainerDetails", map);
                System.out.println("container Status..." + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getcontainerType(TripTO tripTO) {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList container = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            container = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getcontainerType", map);
            System.out.println("container size=" + container.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("containerType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return container;
    }

    public ArrayList getTrailerList() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerList = null;
        Map map = new HashMap();
        try {
            trailerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return trailerList;
    }

    public ArrayList getContainerList() throws FPRuntimeException, FPBusinessException {
        ArrayList containerList = null;
        Map map = new HashMap();
        try {
            containerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return containerList;
    }

    public ArrayList getContainerTypeDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList containerTypeDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerTypeDetails", map);
            System.out.println("getContainerTypeDetails.size() = " + containerTypeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return containerTypeDetails;
    }

//  update order efs
//public void updateOrderStatusToEFS(String tripId, String tripStatusId) {
//
//        Map map = new HashMap();
//        try {
//            String orderStatus = "", driverName = "", orderGPSTrackingLocation = "", orderNo = "", pickupDate = "", tripCode = "", tripPlannedStartDateTime = "", tripStartDateTime = "", tripPlannedEndDateTime = "",
//                    tripEndDateTime = "", vehicleNo = "",wayBillNo="";
//            map.put("tripId", tripId);
//            map.put("statusId", tripStatusId);
//            System.out.println("map:::::"+map);
//            String  orderReferenceNoTemp[]=null;
//            String orderReferenceNo = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderReferenceNoForTripId", map);
//            if(!"".equals(orderReferenceNo)){
//             orderReferenceNoTemp=orderReferenceNo.split(",");
//            }
//            System.out.println("orderReferenceNoTemp.length"+orderReferenceNoTemp.length);
//            for(int i=0;i<orderReferenceNoTemp.length;i++){
//            map.put("orderReferenceNo", orderReferenceNoTemp[i]);
//
//
//            // String tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getStausName", map);
//            System.out.println("map::::" + map);
//            ArrayList orderDeatils = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderDetails", map);
//            Iterator itr1 = orderDeatils.iterator();
//            TripTO tpTO = new TripTO();
//            while (itr1.hasNext()) {
//                tpTO = new TripTO();
//                tpTO = (TripTO) itr1.next();
//                orderStatus = tpTO.getStatusName();
//                driverName = tpTO.getDriverName();
//                orderGPSTrackingLocation = tpTO.getOrderGPSTrackingLocation();
//                orderNo = tpTO.getCustomerOrderReferenceNo();
//                pickupDate = tpTO.getLoadingDate();
//                tripCode = tpTO.getTripCode();
//                tripPlannedStartDateTime = tpTO.getPlannedPickupDate();
//                tripStartDateTime = tpTO.getPickupDate();
//                tripPlannedEndDateTime = tpTO.getDeliveryDate();
//                tripEndDateTime = tpTO.getDeliveryDate();
//                vehicleNo = tpTO.getVehicleNo();
//                wayBillNo=tpTO.getWayBillNo();
//                System.out.println("orderStatus" + orderStatus + "driverName" + driverName + "orderGPSTrackingLocation" + orderGPSTrackingLocation);
//                System.out.println("orderNo" + orderNo + "pickupDate" + pickupDate + "tripCode" + tripCode + "tripPlannedStartDateTime" + tripPlannedStartDateTime);
//                System.out.println("tripStartDateTime" + tripStartDateTime + "tripPlannedEndDateTime" + tripPlannedEndDateTime + "tripEndDateTime" + tripEndDateTime + "vehicleNo" + vehicleNo+"wayBillNo:"+wayBillNo);
//            }
//            // TODO code application logic here
//
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            Date dNow = new Date();
//            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
//            String today = ft.format(dNow);
//
//            //UpdateOrderStatusResult result = srvs.updateOrderStatus(orderReferenceNo, tripStatusId, "04-05-2015 21:12:12");
//
//
//        //    String[] temp = orderReferenceNo.split(",");
//            UpdateOrderStatusResult result = null;
//        //    for (int i = 0; i < temp.length; i++) {
//             //   System.out.println(temp[i]);
//                result = srvs.updateOrderStatus(orderStatus, driverName, orderGPSTrackingLocation, orderNo, pickupDate, tripCode, tripPlannedStartDateTime, tripStartDateTime, tripPlannedEndDateTime, tripEndDateTime, vehicleNo,wayBillNo);
//                System.out.println("result.getContent() = " + result.getContent());
//                List resultList = result.getContent();
//                for (int k = 0; k < resultList.size(); k++) {
//                    System.out.println(resultList.get(k));
//                }
//            }
//          //  }
//
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
//// update trip closer data
// public void updateTripCloserToEFS(String tripId) {
//
//        Map map = new HashMap();
//        try {
//            String orderno[] = null, accountentrydate[] = null, partycode[] = null, partytype[] = null, chargecode[] = null, accountstype[] = null, chargeamount[] = null, narration[] = null, jobcardtype[] = null;
//            String orderReferenceNoTemp[]=null;
//
//            map.put("tripId", tripId);
//
//            String orderReferenceNo = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderReferenceNoForTripId", map);
//            // String tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getStausName", map);
//            if(!"".equals(orderReferenceNo)){
//             orderReferenceNoTemp=orderReferenceNo.split(",");
//            }
//            System.out.println("orderReferenceNoTemp.length"+orderReferenceNoTemp.length);
//            int weightFactor=orderReferenceNoTemp.length;
//            for(int i=0;i<orderReferenceNoTemp.length;i++){
//            map.put("orderReferenceNo", orderReferenceNoTemp[i]);
//            map.put("weightFactor", weightFactor);
//           // map.put("orderReferenceNo", orderReferenceNo);
//            System.out.println("map::::" + map);
//            ArrayList tripAccountDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAccountDetails", map);
//            ArrayOfTMSModel ar = new ArrayOfTMSModel();
//         //   ar.getTMSModel().removeAll(tripAccountDetails);
//            ar.getTMSModel().addAll(tripAccountDetails);
//            System.out.println("tmsmodel Size:"+ar.getTMSModel().size()) ;
//              Iterator itr1 = tripAccountDetails.iterator();
//            TMSModel tpTO = new TMSModel();
//            while (itr1.hasNext()) {
//                tpTO = new TMSModel();
//                tpTO = (TMSModel) itr1.next();
//               System.out.println("tpTO.getOrder()"+tpTO.getOrderno());
//               System.out.println("tpTO.getAccountentrydate()"+tpTO.getAccountentrydate());
//               System.out.println("tpTO.getAccountstype()"+tpTO.getAccountstype());
//               System.out.println("tpTO.getPartycode()"+tpTO.getPartycode());
//               System.out.println("tpTO.getPartytype()"+tpTO.getPartytype());
//               System.out.println("tpTO.getNarration()"+tpTO.getNarration());
//               System.out.println("tpTO.getChargecode()"+tpTO.getChargecode());
//               System.out.println("tpTO.getChargeamount()"+tpTO.getChargeamount());
//               System.out.println("tpTO.getJobcardtype()"+tpTO.getJobcardtype());
//               System.out.println("tpTO.getExpensetype()"+tpTO.getExpensetype());
//
//          }
//            //int i = 0;
//
////          ArrayList Acc=new ArrayList();
////
////                partycode[i]=tpTO.getEmpCode();
////                partytype[i]="E";
////                chargecode[i]=tpTO.getChargeCode();
////                accountstype[i]=tpTO.getAccountsType();
////                chargeamount[i]=tpTO.getAccountsAmount();
////                narration[i]=tpTO.getNarration();
////                jobcardtype[i]="";
////                System.out.println("accountentrydate"+accountentrydate+"partycode"+partycode+"chargecode"+chargecode+"accountstype"+accountstype+"chargeamount"+chargeamount+"narration"+narration);
////              i++;
////            }
////             // TODO code application logic here
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            //UpdateOrderStatusResult result = srvs.updateOrderStatus(orderReferenceNo, tripStatusId, "04-05-2015 21:12:12");
//            String[] temp = orderReferenceNo.split(",");
//            CreateTMSAccountDetailResult result = null;
//           // for (int j = 0; j < temp.length; j++) {
//             //   System.out.println(temp[j]);
//                result = srvs.createTMSAccountDetail(ar);
//                System.out.println("result.getContent() = " + result.getContent());
//                List resultList = result.getContent();
//                for (int k = 0; k < resultList.size(); k++) {
//                    System.out.println(resultList.get(k));
//                }
//            }
//           // }
//
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
//
    public ArrayList getVendorList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        try {
            try {
                System.out.println("i am here ");
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorList", map);
                System.out.println("result=" + result.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVendorList", sqlException);
        }
        return result;
    }

    public ArrayList getVendorVehicleType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("vendorId", tripTO.getVendorId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", 1);
        } else {
            map.put("ownerShip", "");
        }
        System.out.println("map = " + map);
        ArrayList vehicleTypeList = new ArrayList();
        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorVehicleType", map);
            System.out.println("vehicleTypeList size=" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeList List", sqlException);
        }

        return vehicleTypeList;
    }

    public String getVehicleOwnerType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());

        // ArrayList tripDetails = new ArrayList();
        String vehicleOwnerType = "";
        try {
            if (!"0".equals(tripTO.getVehicleId())) {
                vehicleOwnerType = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleOwnerType", map);
//                System.out.println("getOtherExpenseDetails size=" + vehicleOwnerType.length());
            } else {
                vehicleOwnerType = "3";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return vehicleOwnerType;
    }

    public int updateStartTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        Map map2 = new HashMap();

        map.put("userId", userId);
        map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
        map.put("planStartDate", tripTO.getPlanStartDate());
        map.put("startDate", tripTO.getStartDate());
        map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("startHM", tripTO.getStartHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("startTripRemarks", tripTO.getStartTripRemarks());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
        map.put("startTrailerKM", tripTO.getStartTrailerKM());

        map.put("regNo", tripTO.getRegNo());
        map1.put("tripId", tripTO.getTripId());
        map2.put("userId", userId);
        map2.put("tripId", tripTO.getTripId());
        map2.put("gpsVendor", tripTO.getGpsVendor());
        map2.put("gpsDeviceId", tripTO.getGpsDeviceId());

        System.out.println("the updatetripsheetdetails" + map);
        int status = 0;
        int update = 0;
        String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
        String[] ConsignmentId = ConsignmentIdList.split(",");
        for (int i = 0; i < ConsignmentId.length; i++) {
            map.put("consignmentId", ConsignmentId[i]);
            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);

            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("23")) {
                map.put("consignmentStatus", "24");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("26")) {
                map.put("consignmentStatus", "27");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("29")) {
                map.put("consignmentStatus", "30");
            } else {
                map.put("consignmentStatus", "10");
            }
            System.out.println("the updated map:" + map);

            update = (Integer) getSqlMapClientTemplate().update("trip.updateStartconsignmentSheet", map);
            System.out.println("the update:" + update);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            System.out.println("the status1:" + status);
        }
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripSheetDetails", map);
            System.out.println(" start trip status= " + status);
//            if (tripTO.getRegNo() != null) {
//
//                status = (Integer) getSqlMapClientTemplate().update("trip.updateStartVehicle", map);
//            }
            System.out.println("vehicle regNo status= " + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripGpsdetails", map2);

            System.out.println("vehicle gps status= " + status);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheet", map);

//             updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            //            int maxVehicleSequence = 0;
            //            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            //            map.put("vehicleSequence", maxVehicleSequence);
            //            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            int insertConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
            System.out.println("the inner" + map);
            String[] productCodes = null;
            String[] productNames = null;
            String[] productId = null;
            String[] packageNos = null;
            String[] weights = null;
            String[] productbatch = null;
            String[] productuom = null;
            String[] loadedpackages = null;
            productCodes = tripTO.getProductCodes();
            productNames = tripTO.getProductNames();
            productId = tripTO.getProductId();
            packageNos = tripTO.getPackagesNos();
            weights = tripTO.getWeights();
            productbatch = tripTO.getProductbatch();
            productuom = tripTO.getProductuom();
            loadedpackages = tripTO.getLoadedpackages();
            for (int i = 0; i < productNames.length; i++) {
                map.put("productCode", productCodes[i]);
                map.put("producName", productNames[i]);
                map.put("productId", productId[i]);
                map.put("packageNos", packageNos[i]);
                map.put("weights", weights[i]);
                map.put("productbatch", productbatch[i]);
                map.put("productuom", productuom[i]);
                map.put("loadedpackages", loadedpackages[i]);
                System.out.println("The product Details:" + map);
                if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
                    insertConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
                }

            }
            String tripRevenueDetails = "";
            String tripTemp[] = null;
            String customerId = "";
            String tripType = "";
            String estimatedRevenue = "";
            String emptyTrip = "";
            tripRevenueDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRevenueDetails", map);
            System.out.println("tripRevenueDetails = " + tripRevenueDetails);
            if (!"".equals(tripRevenueDetails)) {
                tripTemp = tripRevenueDetails.split("~");
                customerId = tripTemp[0];
                tripType = tripTemp[1];
                estimatedRevenue = tripTemp[2];
                emptyTrip = tripTemp[3];
            }
            /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
             String code2 = "";
             String[] temp = null;
             int insertStatus = 0;
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%SALES%");
             code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
             temp = code2.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "SALES-" + codev2;
             System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "SALES");

             //get ledger info
             map.put("customer", customerId);
             String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
             System.out.println("ledgerInfo:" + ledgerInfo);
             temp = ledgerInfo.split("~");
             String ledgerId = temp[0];
             String particularsId = temp[1];
             map.put("ledgerId", ledgerId);
             map.put("particularsId", particularsId);

             map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
             //  map.put("amount", crossingAmount);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Freight Charges");
             map.put("Reference", "Trip");
             //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
             //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
             System.out.println("tripId = " + tripTO.getTripSheetId());
             map.put("SearchCode", tripTO.getTripSheetId());
             System.out.println("map1 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "51");
             map.put("particularsId", "LEDGER-39");
             map.put("Accounts_Type", "CREDIT");
             System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
             System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status2 = " + insertStatus);
             }

             }
             */

            System.out.println("updateStartTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleSchedule(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList vehicleSchedule = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map is::" + map);
            vehicleSchedule = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleSchedule", map);
            System.out.println("getVehicleSchedule.size() = " + vehicleSchedule.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpiryDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpiryDateDetails List", sqlException);
        }
        return vehicleSchedule;
    }

    public String getRoutePoints(TripTO tripTO) {
        Map map = new HashMap();
        String routePoints = "";
        try {
            map.put("consignmentOrderId", tripTO.getConsignmentOrderNos());
            routePoints = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentRoutepointsForGoogleMap", map);
            System.out.println("routePoints=" + routePoints);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return routePoints;
    }

    public ArrayList getContainerDetails(String ConsignmentOrderIds) {
        Map map = new HashMap();
        ArrayList ContainerDetails = new ArrayList();
        map.put("ConsignmentOrderIds", ConsignmentOrderIds);
        try {
            System.out.println("map====ContainerDetailst" + map);
            ContainerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerDetails", map);
            System.out.println("ContainerDetails===" + ContainerDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return ContainerDetails;
    }

    public ArrayList getconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        try {
            consgncontainer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getconsgnContainerDetails", map);
            System.out.println("getconsgnContainerDetails.size() = " + consgncontainer.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return consgncontainer;
    }

    public ArrayList getPlannedConsgnContainerDetails(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        try {
            consgncontainer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPlannedConsgnContainerDetails", map);
            System.out.println("getPlannedConsgnContainerDetails.size() = " + consgncontainer.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return consgncontainer;
    }

    public ArrayList getrepoconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        try {
            consgncontainer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getrepoconsgnContainerDetails", map);
            System.out.println("getrepoconsgnContainerDetails.size() = " + consgncontainer.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return consgncontainer;
    }

    public int updatePlannedStatus(TripTO tripTO, String[] containerTypeId, String[] containerNo, String[] uniqueId, String isRepo, String sealNo[], String grStatus[]) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            if ("1".equals(tripTO.getRepoId())) {
                map.put("repoId", 1);
            } else if ("2".equals(tripTO.getRepoId())) {
                map.put("repoId", 2);

            } else {
                map.put("repoId", 0);
            }
            if (containerNo != null) {
                for (int i = 0; i < containerTypeId.length; i++) {

                    map.put("containerTypeId", containerTypeId[i]);
                    map.put("containerNo", containerNo[i]);
                    map.put("uniqueId", uniqueId[i]);
                    if ("".equals(sealNo[i]) || sealNo[i] == null) {

                        map.put("sealNo", "NA");
                    } else {
                        map.put("sealNo", sealNo[i]);
                    }

                    map.put("grStatus", grStatus[i]);
                    map.put("consignmentOrderIds", consignmentOrderIds[0]);
                    System.out.println("updatePlannedStatus" + map);
                    if ("Y".equals(isRepo)) {

                        lastStatus = (Integer) getSqlMapClientTemplate().update("trip.updateLastPlannedStatus", map);
                        System.out.println("lastStatus:" + lastStatus);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.updatePlannedStatus", map);
                        System.out.println("updatePlannedStatus1111111111:" + status);
                    }
                }
                if ("2".equals(tripTO.getRepoId())) {
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public int updatePlannedStatus(TripTO tripTO, String[] containerTypeId, String[] containerNo, String[] uniqueId, String isRepo, String sealNo[], String grStatus[], SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            if ("1".equals(tripTO.getRepoId())) {
                map.put("repoId", 1);
            } else if ("2".equals(tripTO.getRepoId())) {
                map.put("repoId", 2);

            } else {
                map.put("repoId", 0);
            }
            System.out.println("containerTypeId.length" + containerTypeId.length);
            if (containerNo != null) {
                for (int i = 0; i < containerTypeId.length; i++) {

                    map.put("containerTypeId", containerTypeId[i]);
                    map.put("containerNo", containerNo[i]);
                    map.put("uniqueId", uniqueId[i]);
                    if ("".equals(sealNo[i]) || sealNo[i] == null) {

                        map.put("sealNo", "NA");
                    } else {
                        map.put("sealNo", sealNo[i]);
                    }

                    map.put("grStatus", grStatus[i]);
                    map.put("consignmentOrderIds", consignmentOrderIds[0]);
                    System.out.println("updatePlannedStatus" + map);
                    if ("Y".equals(isRepo)) {

                        lastStatus = (Integer) session.update("trip.updateLastPlannedStatus", map);
                        System.out.println("lastStatus:" + lastStatus);
                    } else {
                        status = (Integer) session.update("trip.updatePlannedStatus", map);
                        System.out.println("updatePlannedStatus1111111111:" + status);
                    }
                }
                if ("2".equals(tripTO.getRepoId())) {
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public String getPlannedConatinerSize(TripTO tripTO) {
        Map map = new HashMap();
        String plannedConatiner = "";
        try {
            map.put("orderId", tripTO.getConsignmentOrderNos());
            System.out.println("map for size:" + map);
            plannedConatiner = (String) getSqlMapClientTemplate().queryForObject("trip.getPlannedConatinerNos", map);
            System.out.println("plannedConatiner size=" + plannedConatiner);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return plannedConatiner;
    }

    public String getDistance(String origin, String destination) {
        Map map = new HashMap();
        String plannedConatiner = "";
        try {
            map.put("origin", origin);
            map.put("destination", destination);
            System.out.println("map for size:" + map);
            plannedConatiner = (String) getSqlMapClientTemplate().queryForObject("trip.getDistance", map);
            System.out.println("plannedConatiner size=" + plannedConatiner);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return plannedConatiner;
    }

    public ArrayList getTripList() {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripList", map);
            System.out.println("getTripList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getPrintTripSheetDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetails", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getPrintTripSheetDetailsNew(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetailsNew", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getPrintTripChallanDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripChallanDetails", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getConsignmentNoteContainer(String consignmentNoteOredrId, String tripId) {
        Map map = new HashMap();
        ArrayList containerNo = new ArrayList();
        try {
            map.put("consignmentNoteOredrId", consignmentNoteOredrId);
            map.put("tripId", tripId);
            System.out.println("map for size:" + map);
            containerNo = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentNoteContainer", map);
            System.out.println("consignmentNoteOredrId  size=" + containerNo.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentNoteContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentNoteContainer List", sqlException);
        }

        return containerNo;
    }

    public ArrayList getConsignmentNoteContainerNew(String consignmentNoteOredrId, String tripId) {
        Map map = new HashMap();
        ArrayList containerNo = new ArrayList();
        try {
            map.put("consignmentNoteOredrId", consignmentNoteOredrId);
            map.put("tripId", tripId);
            System.out.println("map for size:" + map);
            containerNo = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentNoteContainerNew", map);
            System.out.println("consignmentNoteOredrId  size=" + containerNo.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentNoteContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentNoteContainer List", sqlException);
        }

        return containerNo;
    }

    public ArrayList getConsignmentArticleList(String consignmentNoteOredrId) {
        Map map = new HashMap();
        ArrayList consignmentArticleList = new ArrayList();
        try {

            map.put("consignmentNoteOredrId", consignmentNoteOredrId);
            System.out.println("map for size:" + map);
            consignmentArticleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentArticleList", map);
            System.out.println("consignmentArticleList DAO size=" + consignmentArticleList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentArticleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentArticleList List", sqlException);
        }

        return consignmentArticleList;
    }

    public ArrayList getBunkList(String bunkName) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            System.out.println("bunkName.." + bunkName);
            if (bunkName != null && !"".equals(bunkName)) {
                map.put("bunkname", bunkName);
            }

            System.out.println("map.size().." + map.size());
            if (map.size() > 0) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getselBunk", map);
            } else {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBunkList", map);
            }
            System.out.println("bunkselList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doInsertBunk(TripTO tripTO, int userId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        int bunkid = 0;
        int id = 0;

        try {
            id = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaxIDList", mapmaster);
            String FuelLocateId = tripTO.getBunkName().substring(0, 1) + id;

            System.out.println("FuelLocateId" + FuelLocateId);

            mapmaster.put("FuelLocateId", FuelLocateId);
            mapmaster.put("bunkname", tripTO.getBunkName());
            mapmaster.put("location", tripTO.getCurrlocation());
            mapmaster.put("state", tripTO.getBunkState());
            mapmaster.put("act_ind", tripTO.getBunkStatus());
            mapmaster.put("createdby", userId);

            System.out.println("test1" + status);
            int bunkId = (Integer) getSqlMapClientTemplate().insert("trip.insertBunkmaster", mapmaster);
            System.out.println("bunkId master is" + bunkId);

            //Ledger Code start
            if (bunkId != 0) {
                String code = (String) getSqlMapClientTemplate().queryForObject("trip.getLedgerCode", mapmaster);
                String[] temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                mapmaster.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("trip.accYearVal", mapmaster);
                System.out.println("accYear:" + accYear);
                mapmaster.put("accYear", accYear);
                //current year end

                String bunkName = tripTO.getBunkName() + "-" + bunkId;
                System.out.println("bunkName =====> " + bunkName);

                int VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("trip.insertVendorLedger", mapmaster);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    mapmaster.put("ledgerId", VendorLedgerId);
                    mapmaster.put("bunkId", bunkId);
                    System.out.println("map update ::::=> " + mapmaster);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkInfo", mapmaster);
                }
            }
            //Ledger Code end

            //bunkid = (Integer) getSqlMapClientTemplate().queryForObject("customer.getBunkselectedList", mapmaster);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", tripTO.getFuelType());
            mapdetail.put("currentrate", tripTO.getCurrRate());
            mapdetail.put("bunkremarks", tripTO.getRemarks());
            mapdetail.put("act_ind", tripTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("test2" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkalterList(String bunkId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (bunkId != null) {
                map.put("bunkid", bunkId);
            }
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getalterBunk", map);

            System.out.println("bunkList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doUpdateBunk(TripTO tripTO, int userId, String bunkId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        mapmaster.put("bunkname", tripTO.getBunkName());
        mapmaster.put("location", tripTO.getCurrlocation());
        mapmaster.put("state", tripTO.getBunkState());
        mapmaster.put("act_ind", tripTO.getBunkStatus());
        mapmaster.put("createdby", userId);
        mapmaster.put("bunkid", bunkId);

        try {
            System.out.println("test1=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkmaster", mapmaster);
            System.out.println("status master is=" + status);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", tripTO.getFuelType());
            mapdetail.put("currentrate", tripTO.getCurrRate());
            mapdetail.put("bunkremarks", tripTO.getRemarks());
            mapdetail.put("act_ind", tripTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("\nbunkid.." + bunkId);
            System.out.println("\ncustomerTO.getFuelType()=" + tripTO.getFuelType());
            System.out.println("\ncustomerTO.getCurrRate()=" + tripTO.getCurrRate());
            System.out.println("\ncustomerTO.getRemarks()=" + tripTO.getRemarks());
            System.out.println("\ncustomerTO.getBunkStatus()=" + tripTO.getBunkStatus());
            System.out.println("\ncreatedby=" + userId);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getCustomerNameSuggestionList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            map.put("customerName", tripTO.getCustomerName() + "%");
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerNameSuggestionList", map);

            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerNameSuggestionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerNameSuggestionList", sqlException);
        }
        return customerList;
    }

    public ArrayList getInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetailsList", map);

            System.out.println("invoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getInvoiceHeaderList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceHeaderList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceHeaderList", map);

            System.out.println("invoiceHeaderList size=" + invoiceHeaderList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceHeaderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceHeaderList", sqlException);
        }
        return invoiceHeaderList;
    }

    public ArrayList getRtoMasterList() {
        Map map = new HashMap();
        ArrayList troMasterList = new ArrayList();
        try {
            troMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRtoMasterList", map);

            System.out.println("troMasterList size=" + troMasterList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoMasterList", sqlException);
        }
        return troMasterList;
    }

    public ArrayList getRtoMappedVehicleList(String rtoOfficeId) {
        Map map = new HashMap();
        ArrayList rtoMappedVehicleList = new ArrayList();
        try {
            if (rtoOfficeId != null && !"".equalsIgnoreCase(rtoOfficeId)) {
                map.put("rtoOfficeId", rtoOfficeId);
            }
            rtoMappedVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRtoMappedVehicleList", map);

            System.out.println("rtoMappedVehicleList size=" + rtoMappedVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoMappedVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoMappedVehicleList", sqlException);
        }
        return rtoMappedVehicleList;
    }

    public int insertRtoVehicleMapping(int userId, String[] assignedVehicleId, String rtoOfficeId) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            if (assignedVehicleId != null && assignedVehicleId.length > 0) {
                map.put("rtoOfficeId", rtoOfficeId);
                insertStatus = (Integer) getSqlMapClientTemplate().delete("trip.deleteRtoMapping", map);
                for (int i = 0; i < assignedVehicleId.length; i++) {
                    map.put("userId", userId);
                    map.put("vehicleId", assignedVehicleId[i]);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("trip.insertRtoVehicleMapping", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertRtoVehicleMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "insertRtoVehicleMapping", sqlException);
        }
        return insertStatus;
    }

    public int getRtoVehicleCount(TripTO tripTO) {
        Map map = new HashMap();
        int vehicleCount = 0;
        try {
            map.put("rtoOfficeId", tripTO.getRtoId());
            System.out.println("map===" + map);
            vehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getRtoVehicleCounttrip", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoVehicleCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoVehicleCount", sqlException);
        }
        return vehicleCount;
    }

    public int getPlannedContainerCount(String consignmentOrderId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("consignmentOrderId", consignmentOrderId);
            System.out.println("map fro:" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainerCount", map);
            System.out.println("count::" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return status;
    }

    public String getTwoTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getTwoTwentyRate", map);
            System.out.println("two twenty rate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("two twenty rate After:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public String getSingleFourtyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            System.out.println("map for Fourty:" + map);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRate", map);
            System.out.println("single fourty rate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("single fourty rate After:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public String getSingleTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType, String lastPointId, String thirdPoint) {
        Map map = new HashMap();
        String rate = "";
        String fourtyRate = "";
        String twoTwentyRate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("lastPointId", lastPointId);
            map.put("thirdPoint", thirdPoint);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            System.out.println("map:" + map);
            if (!"1".equals(productCategory)) {
                System.out.println("i m in frozen/chiller");
                fourtyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRateWithReefer", map);
            } else {
                System.out.println("i m in ambient");
                fourtyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRateWithoutReefer", map);
            }
            System.out.println("single fourty rate:" + fourtyRate);
            if (fourtyRate == null) {
                fourtyRate = "0.0";
            }
            System.out.println("singleFourtyRate After:" + fourtyRate);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleTwentyRate", map);
            System.out.println("singleTwentyRate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("singleTwentyRate After:" + rate);

            twoTwentyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getTwoTwentyRate", map);
            System.out.println("two twenty rate:" + twoTwentyRate);
            if (twoTwentyRate == null) {
                twoTwentyRate = "0.0";
            }
            System.out.println("two twenty rate after:" + twoTwentyRate);

            rate = rate + "~" + fourtyRate + "~" + twoTwentyRate;
            System.out.println("final Rate tariff:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public ArrayList getOrderInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getOrderSuppInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderSuppInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getOrderInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceDetailsList", map);

            System.out.println("invoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getOrderSuppInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderSuppInvoiceDetailsList", map);

            System.out.println("invoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getCreditOrderInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            map.put("creditNoteId", tripTO.getCreditNoteId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCreditOrderInvoiceDetailsList", map);

            System.out.println("getCreditOrderInvoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getTripFuelDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripFuelDetails", map);

            System.out.println("getTripFuelDeatils size=" + fuelDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return fuelDetails;
    }

    public String insertTripGr(String tripId, int userId, String tripGr, SqlMapClient session) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("tripGr", tripGr);
            tripGrNo = (Integer) session.insert("trip.insertTripGr", map);
            System.out.println("tripGrNo in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripUploadGr(String tripId, int userId, String tripGr, String tripGrDate, SqlMapClient session) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("tripGr", tripGr);
            map.put("tripGrDate", tripGrDate + " 08:00:00");
            tripGrNo = (Integer) session.insert("trip.insertTripUploadGr", map);
            System.out.println("tripGrNo in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripGr(String tripId, int userId, String tripGr) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("tripGr", tripGr);
            tripGrNo = (Integer) getSqlMapClientTemplate().insert("trip.insertTripGr", map);
            System.out.println("tripGrNo in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripChallan(String tripId, int userId, String challanNo, SqlMapClient session) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("challanNo", challanNo);
            tripGrNo = (Integer) session.insert("trip.insertTripChallan", map);
            System.out.println("insertTripChallan in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripChallan(String tripId, int userId, String challanNo) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("challanNo", challanNo);
            tripGrNo = (Integer) getSqlMapClientTemplate().insert("trip.insertTripChallan", map);
            System.out.println("insertTripChallan in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String getPlannedContainerNo(String orderId) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("orderId", orderId);

            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainerNo", map);
            System.out.println("getPlannedContainerNo:" + rate);
            if (rate == null) {
                rate = "";
            }
            System.out.println("getPlannedContainerNot:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public int updateTollAndDetaintionForBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId, String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks, String commodityId, String commodityName, String commodityGstType) {
        Map map = new HashMap();
        String tempContainer[] = null;
        String tempTripContainerId[] = null;
        String tempConsignmentContainerId[] = null;
        map.put("userId", userId);
        String dest = "";
        String billEntry = "";
        String shipNo = "";
        String containrNo = "";
        String grenTax = "";
        String tollTax = "";
        String otherExpenses = "";
        String weightmentCharge = "";
        String billParty = "";

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {

                map.put("tripId", tripId[i]);
                map.put("toll", toll[i]);
                map.put("detaintion", detaintion[i]);
                map.put("greenTax", greenTax[i]);
                map.put("grNo", grNo[i]);
                map.put("otherExpense", otherExpense[i]);
                map.put("weightmentCharge", weightment[i]);
                map.put("remarks", detaintionRemarks);
                map.put("expenseType", expenseType[i]);

                map.put("commodityId", commodityId);
                map.put("commodityName", commodityName);
                map.put("commodityGstType", commodityGstType);

                System.out.println("map@" + map);

                if (!"".equals(commodityId) ) {
                    int updateTripCommodityDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripCommodityDetails", map);
                    System.out.println("updateTripCommodityDetails=" + updateTripCommodityDetails);

                    int updateTripArticleDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticleDetails", map);
                    System.out.println("updateTripArticleDetails=" + updateTripArticleDetails);
                }
                // check otherExpense
                int otherExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseForBilling", map);
                System.out.println("otherExpenseCheck" + otherExpenseCheck);
                if (!"0.00".equals(otherExpense[i])) {

                    if (otherExpenseCheck != 0) {
                        map.put("otherExpense", otherExpense[i]);
                        System.out.println("%%%%%%%%%%%%otherExpense[i]" + otherExpense[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherExpenseForBilling", map);
                    }
                }

                // check detaiontion
                int detaintioncheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDetaiontionForBilling", map);
                System.out.println("detaintioncheck" + detaintioncheck);
                if (!"0.00".equals(detaintion[i])) {

                    if (detaintioncheck != 0) {
                        map.put("detaintion", detaintion[i]);
                        System.out.println("%%%%%%%%%%%%detaintion[i]" + detaintion[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateDetaintionForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionForBilling", map);
                    }
                }
                //check for Grenn Tax
                int greentax = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGreenTaxForBilling", map);
                System.out.println("greentax" + greentax);
                if (!"0.00".equals(greenTax[i]) && !"0".equals(greenTax[i])) {

                    if (greentax != 0) {
                        map.put("greenTax", greenTax[i]);
                        System.out.println("%%%%%%%%%%%%greenTax[i]" + greenTax[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateGreenTaxForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxForBilling", map);
                    }
                }

                //check toll
                int tollcheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTollForBilling", map);
                System.out.println("tollcheck" + tollcheck);
                if (!"0.00".equals(toll[i])) {
                    if (tollcheck != 0) {
                        map.put("toll", toll[i]);
                        System.out.println("%%%%%%%%%%%toll[i]" + toll[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTollForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollForBilling", map);
                    }
                }

                //check weightment
                int weightmentCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getWeightmentForBilling", map);
                System.out.println("weightmentCharge" + weightmentCheck);
                map.put("expenseType", expenseType[i]);
//         if( !"0.00".equals(weightment[i])){
                map.put("weightmentCharge", weightment[i]);
                if (weightmentCheck != 0) {
                    System.out.println("%%%%%%%%%%%toll[i]" + weightment[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateWeightmentForBilling", map);
                } else if (!"0".equals(weightment[i])) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentForBilling", map);
                }

                //update trip container and consignment container
                if (containerNoOld[i].equals(containerNo[i])) {
                    containrNo = "0";
                    System.out.println("enterecd action");
                    map.put("containerNo", "0");
                    map.put("containerNoOld", "0");
                } else {
                    containrNo = containerNo[i];
                    map.put("containerNoOld", containerNoOld[i]);
                    map.put("containerNo", containerNo[i]);
                }

                System.out.println("the update toll and detaintion" + map);
                // for double twenty  contanier,we need to  split the container for each trip id
                tempContainer = containerNo[i].split(",");
                tempTripContainerId = tripContainerId[i].split(",");
                tempConsignmentContainerId = consignmentConatinerId[i].split(",");
                for (int j = 0; j < tempContainer.length; j++) {
                    map.put("tempContainer", tempContainer[j]);
                    map.put("tempTripContainerId", tempTripContainerId[j]);
                    map.put("tempConsignmentContainerId", tempConsignmentContainerId[j]);
                    System.out.println("the container map111" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainer", map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainer", map);
                }

                //update shipping bill and BOE
                if ("1".equals(movementType)) {
                    if (!"0".equals(shippingBillNo[i]) && !"".equals(shippingBillNo[i]) && shippingBillNo[i] != null) {
                        map.put("shippingBillNo", shippingBillNo[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoForTrip", map);
                        System.out.println("status for shipping billno" + status);
                    }
                } else if (!"0".equals(billOfEntry[i]) && !"".equals(billOfEntry[i]) && billOfEntry[i] != null) {
                    map.put("billOfEntry", billOfEntry[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateBillOfEntryForTrip", map);
                    System.out.println("status for shipping billno" + status);
                }

                // insert charges for invoice edit log (BILLING)
                if (detaintionOld[i].equals(detaintion[i])) {
                    System.out.println("enterecd action");
                    dest = "0";
                    map.put("detaintion", "0");
                    map.put("detaintionOld", "0");
                } else {
                    dest = detaintion[i];
                    map.put("detaintionOld", detaintionOld[i]);
                    map.put("detaintion", detaintion[i]);
                }
                if (greenTaxOld[i].equals(greenTax[i])) {
                    grenTax = "0";
                    map.put("greenTax", "0");
                    map.put("greenTaxOld", "0");
                } else {
                    grenTax = greenTax[i];
                    map.put("greenTaxOld", greenTaxOld[i]);
                    map.put("greenTax", greenTax[i]);
                }
                if (tollOld[i].equals(toll[i])) {
                    tollTax = "0";
                    map.put("toll", "0");
                    map.put("tollOld", "0");
                } else {
                    tollTax = toll[i];
                    map.put("tollOld", tollOld[i]);
                    map.put("toll", toll[i]);
                }
                if (otherExpenseOld[i].equals(otherExpense[i])) {
                    otherExpenses = "0";
                    map.put("otherExpense", "0");
                    map.put("otherExpenseOld", "0");
                } else {
                    otherExpenses = otherExpense[i];
                    map.put("otherExpenseOld", otherExpenseOld[i]);
                    map.put("otherExpense", otherExpense[i]);
                }
                if (weightmentOld[i].equals(weightment[i])) {
                    weightmentCharge = "0";
                    map.put("weightment", "0");
                    map.put("weightmentOld", "0");
                } else {
                    weightmentCharge = toll[i];
                    map.put("weightmentOld", weightmentOld[i]);
                    map.put("weightment", weightment[i]);
                }
                System.out.println("##################" + map);

                if ("1".equals(movementType)) {
                    map.put("billOfEntry", "0");
                    map.put("billOfEntryOld", "0");
                    billEntry = "0";
                    if (shippingBillNoOld[i].equals(shippingBillNo[i])) {
                        System.out.println("shippingBillNo000000" + shippingBillNo[i] + "," + shippingBillNoOld[i]);
                        System.out.println("enterecd action");
                        shipNo = "0";
                        map.put("shippingBillNo", "0");
                        map.put("shippingBillNoOld", "0");
                    } else {
                        shipNo = shippingBillNo[i];

                        map.put("shippingBillNoOld", shippingBillNoOld[i]);
                        map.put("shippingBillNo", shippingBillNo[i]);

                        map.put("billOfEntry", "0");
                        map.put("billOfEntryOld", "0");
                    }
                } else {
                    map.put("shippingBillNo", "0");
                    map.put("shippingBillNoOld", "0");
                    shipNo = "0";
                    if (billOfEntry[i].equals(billOfEntryOld[i])) {
                        System.out.println("billOfEntry000000" + billOfEntry[i] + "," + billOfEntryOld[i]);
                        billEntry = "0";
                        map.put("billOfEntry", "0");
                        map.put("billOfEntryOld", "0");
                    } else {
                        billEntry = billOfEntry[i];
                        map.put("billOfEntryOld", billOfEntryOld[i]);
                        map.put("billOfEntry", billOfEntry[i]);

                        map.put("shippingBillNo", "0");
                        map.put("shippingBillNoOld", "0");
                    }
                }

                if (billingParty.equals(billingPartyOld)) {
                    System.out.println("billingParty" + billingParty + "," + billingParty);
                    billParty = "0";
                    map.put("billingParty", "0");
                    map.put("billingPartyOld", "0");
                } else {
                    billParty = billingParty;
                    map.put("billingPartyOld", billingPartyOld);
                    map.put("billingParty", billingParty);
                }

                System.out.println("map end=" + map);
                System.out.println("map containerNo[i]=" + containrNo);
                System.out.println("map shippingBillNo[i]=" + shipNo);
                System.out.println("map billOfEntry[i]=" + billEntry);
                System.out.println("map detaintion[i]=" + dest);
                System.out.println("map greenTax[i]=" + grenTax);
                System.out.println("map toll[i]=" + tollTax);
                System.out.println("map weightment[i]=" + weightment);
                System.out.println("map billList=" + billList);
                System.out.println("map billParty=" + billParty);

                if (("0".equals(shipNo)) && ("0".equals(billEntry)) && "0".equals(containrNo) && "0".equals(dest) && "0".equals(grenTax) && "0".equals(tollTax) && "0".equals(otherExpenses) && "0".equals(weightmentCharge) && "0".equals(billParty)) {
                    System.out.println("entered equal to Zero");
                } else {
                    System.out.println("BILLLING");
                    map.put("billType", "Billing");
                    map.put("movementType", movementType);
                    System.out.println("entered  equal to Zero");
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceEditLog", map);
                    System.out.println("the update toll and detaintion =" + status);
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTollAndDetaintionForSupplemetBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion,
            String[] detaintionOld, String[] vehicleId, String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId,
            String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld,
            String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty,
            String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String detaintionRemarks, String commodityId, String commodityName, String commodityGstType) {
        Map map = new HashMap();
        String tempContainer[] = null;
        String tempTripContainerId[] = null;
        String tempConsignmentContainerId[] = null;
        map.put("userId", userId);

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {

                map.put("tripId", tripId[i]);
                map.put("toll", toll[i]);
                map.put("detaintion", detaintion[i]);
                map.put("greenTax", greenTax[i]);
                map.put("grNo", grNo[i]);
                map.put("otherExpense", otherExpense[i]);
                map.put("weightmentCharge", weightment[i]);
                map.put("remarks", detaintionRemarks);
                map.put("expenseType", expenseType[i]);
                map.put("billParty", billingParty);
                map.put("commodityId", commodityId);
                map.put("commodityName", commodityName);
                map.put("commodityGstType", commodityGstType);
                System.out.println("map@::::::" + map);

                if (!"".equals(commodityId)) {
                    int updateTripCommodityDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripCommodityDetails", map);
                    System.out.println("updateTripCommodityDetails=" + updateTripCommodityDetails);

                    int updateTripArticleDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticleDetails", map);
                    System.out.println("updateTripArticleDetails=" + updateTripArticleDetails);
                }

                double tollchr = Double.parseDouble(toll[i]);
                double detaintionchr = Double.parseDouble(detaintion[i]);
                double greenTaxchr = Double.parseDouble(greenTax[i]);
                double otherExpensechr = Double.parseDouble(otherExpense[i]);
                double weightmentchr = Double.parseDouble(weightment[i]);

                double total = tollchr + detaintionchr + greenTaxchr + otherExpensechr + weightmentchr;

                if (total > 0) {

                    int otherExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseForSuppBilling", map);
//                System.out.println("otherExpenseCheck" + otherExpenseCheck);
//                if (!"0.00".equals(otherExpense[i])) {

                    if (otherExpenseCheck != 0) {
                        // map.put("otherExpense", otherExpense[i]);
                        System.out.println("%%%%%%%%%%%%otherExpense[i]" + otherExpense[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseForSuppBilling", map);
                    } else {
//                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherExpenseForBilling", map);
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherSupplementExpenseForBilling", map);
                    }
//                }

                    // check detaiontion
                    int detaintioncheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDetaiontionForSuppBilling", map);
                    System.out.println("detaintioncheck" + detaintioncheck);
//                if (!"0.00".equals(detaintion[i])) {
//
                    if (detaintioncheck != 0) {
                        // map.put("detaintion", detaintion[i]);
                        System.out.println("%%%%%%%%%%%%detaintion[i]" + detaintion[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateDetaintionForSuppBilling", map);
                    } else {
//                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionForBilling", map);
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionSupplementForBilling", map);
                    }
//                }
                    //check for Grenn Tax
                    int greentax = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGreenTaxForSuppBilling", map);
                    System.out.println("greentax" + greentax);
//                if (!"0.00".equals(greenTax[i]) && !"0".equals(greenTax[i])) {
//
                    if (greentax != 0) {
                        // map.put("greenTax", greenTax[i]);
                        System.out.println("%%%%%%%%%%%%greenTax[i]" + greenTax[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateGreenTaxForSuppBilling", map);
                    } else {
//                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxForBilling", map);
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxSupplementForBilling", map);
                    }
//                }

                    //check toll
                    int tollcheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTollForSuppBilling", map);
                    System.out.println("tollcheck" + tollcheck);
//                if (!"0.00".equals(toll[i])) {
                    if (tollcheck != 0) {
//                        map.put("toll", toll[i]);
                        System.out.println("%%%%%%%%%%%toll[i]" + toll[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTollForSuppBilling", map);
                    } else {
//                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollForBilling", map);
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollSupplementForBilling", map);
                    }
//                }

                    //check weightment
                    int weightmentCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getWeightmentForSuppBilling", map);
                    System.out.println("weightmentCharge" + weightmentCheck);
//                map.put("expenseType", expenseType[i]);
////         if( !"0.00".equals(weightment[i])){
//                map.put("weightmentCharge", weightment[i]);
                    if (weightmentCheck != 0) {
                        System.out.println("%%%%%%%%%%% if wgt [i]" + weightment[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateWeightmentForSuppBilling", map);
                    } else {
//                    status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentForBilling", map);
                        System.out.println("%%%%%%%%%%% else wgt [i]" + weightment[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentSupplementForBilling", map);
                    }
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTollAndDetaintionForRepoBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId, String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId, String commodityId, String commodityName, String commodityGstType) {
        System.out.println("RAJDAO");
        Map map = new HashMap();
        String tempContainer[] = null;
        String tempTripContainerId[] = null;
        String tempConsignmentContainerId[] = null;
        map.put("userId", userId);
        String dest = "";
        String billEntry = "";
        String shipNo = "";
        String containrNo = "";
        String grenTax = "";
        String tollTax = "";
        String otherExpenses = "";
        String weightmentCharge = "";
        String billParty = "";

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {

                map.put("tripId", tripId[i]);
                map.put("toll", toll[i]);
                map.put("detaintion", detaintion[i]);
                map.put("greenTax", greenTax[i]);
                map.put("grNo", grNo[i]);
                map.put("otherExpense", otherExpense[i]);
                map.put("expenseType", expenseType[i]);

                map.put("commodityId", commodityId);
                map.put("commodityName", commodityName);
                map.put("commodityGstType", commodityGstType);

                System.out.println("map@" + map);

                if (!"".equals(commodityId) ) {
                    int updateTripCommodityDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripCommodityDetails", map);
                    System.out.println("updateTripCommodityDetails=" + updateTripCommodityDetails);

                    int updateTripArticleDetails = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticleDetails", map);
                    System.out.println("updateTripArticleDetails=" + updateTripArticleDetails);
                }
                // check otherExpense
                int otherExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseForBilling", map);
                System.out.println("otherExpenseCheck" + otherExpenseCheck);
                if (!"0.00".equals(otherExpense[i])) {

                    if (otherExpenseCheck != 0) {
                        map.put("otherExpense", otherExpense[i]);
                        System.out.println("%%%%%%%%%%%%otherExpense[i]" + otherExpense[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherExpenseForBilling", map);
                    }
                }

                //check toll
                int tollcheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTollForBilling", map);
                if (!"0.00".equals(toll[i])) {
                    if (tollcheck != 0) {
                        map.put("toll", toll[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTollForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollForBilling", map);
                    }
                }
// check detaiontion
                int detaintioncheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDetaiontionForBilling", map);
                if (!"0.00".equals(detaintion[i])) {

                    if (detaintioncheck != 0) {
                        map.put("detaintion", detaintion[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateDetaintionForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionForBilling", map);
                    }
                }
                //check for Grenn Tax
                int greentax = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGreenTaxForBilling", map);
                if (!"0.00".equals(greenTax[i])) {

                    if (greentax != 0) {
                        map.put("greenTax", greenTax[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateGreenTaxForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxForBilling", map);
                    }
                }

//check weightment
                int weightmentCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getWeightmentForBilling", map);
                System.out.println("weightmentCharge" + weightmentCheck);
                map.put("expenseType", expenseType[i]);
//         if( !"0.00".equals(weightment[i])){
                map.put("weightmentCharge", weightment[i]);
                if (weightmentCheck != 0) {
                    System.out.println("%%%%%%%%%%%toll[i]" + weightment[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateWeightmentForBilling", map);
                } else if (!"0".equals(weightment[i])) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentForBilling", map);
                }

// update container
                if (containerNoOld[i].equals(containerNo[i])) {
                    containrNo = "0";
                    System.out.println("enterecd action");
                    map.put("containerNo", "0");
                    map.put("containerNoOld", "0");
                } else {
                    containrNo = containerNo[i];
                    map.put("containerNoOld", containerNoOld[i]);
                    map.put("containerNo", containerNo[i]);
                }

                // for double twenty  contanier,we need to  split the container for each trip id
                tempContainer = containerNo[i].split(",");
                tempTripContainerId = tripContainerId[i].split(",");
                tempConsignmentContainerId = consignmentConatinerId[i].split(",");
                for (int j = 0; j < tempContainer.length; j++) {
                    map.put("tempContainer", tempContainer[j]);
                    map.put("tempTripContainerId", tempTripContainerId[j]);
                    map.put("tempConsignmentContainerId", tempConsignmentContainerId[j]);
                    System.out.println("the container map111" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainer", map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainer", map);
                }

                //insert charges for invoice edit log (REPO BILLING)
                if (detaintionOld[i].equals(detaintion[i])) {
                    System.out.println("enterecd action");
                    dest = "0";
                    map.put("detaintion", "0");
                    map.put("detaintionOld", "0");
                } else {
                    dest = detaintion[i];
                    map.put("detaintionOld", detaintionOld[i]);
                    map.put("detaintion", detaintion[i]);
                }
                if (greenTaxOld[i].equals(greenTax[i])) {
                    grenTax = "0";
                    map.put("greenTax", "0");
                    map.put("greenTaxOld", "0");
                } else {
                    grenTax = greenTax[i];
                    map.put("greenTaxOld", greenTaxOld[i]);
                    map.put("greenTax", greenTax[i]);
                }
                if (tollOld[i].equals(toll[i])) {
                    tollTax = "0";
                    map.put("toll", "0");
                    map.put("tollOld", "0");
                } else {
                    tollTax = toll[i];
                    map.put("tollOld", tollOld[i]);
                    map.put("toll", toll[i]);
                }

                if ("RepoBilling".equals(billList)) {
                    shipNo = "0";
                    map.put("shippingBillNo", "0");
                    map.put("shippingBillNoOld", "0");
                    billEntry = "0";
                    map.put("billOfEntry", "0");
                    map.put("billOfEntryOld", "0");
                }
                if (billingParty.equals(billingPartyOld)) {
                    billParty = "0";
                    map.put("billingParty", "0");
                    map.put("billingPartyOld", "0");
                } else {
                    billParty = billingParty;
                    map.put("billingPartyOld", billingPartyOld);
                    map.put("billingParty", billingParty);
                }

                if (otherExpenseOld[i].equals(otherExpense[i])) {
                    otherExpenses = "0";
                    map.put("otherExpense", "0");
                    map.put("otherExpenseOld", "0");
                } else {
                    otherExpenses = otherExpense[i];
                    map.put("otherExpenseOld", otherExpenseOld[i]);
                    map.put("otherExpense", otherExpense[i]);
                }
                if (weightmentOld[i].equals(weightment[i])) {
                    weightmentCharge = "0";
                    map.put("weightment", "0");
                    map.put("weightmentOld", "0");
                } else {
                    weightmentCharge = weightment[i];
                    map.put("weightmentOld", weightmentOld[i]);
                    map.put("weightment", weightment[i]);
                }

                System.out.println("the update toll and detaintion" + map);

                if ("RepoBilling".equals(billList)) {

                    if (("0".equals(shipNo)) && ("0".equals(billEntry)) && "0".equals(containrNo) && "0".equals(dest) && "0".equals(grenTax) && "0".equals(otherExpenses) && "0".equals(weightmentCharge) && "0".equals(billParty)) {
                        System.out.println("entered equal to Zero");
                    } else {
                        System.out.println("REPOBILLLING");
                        map.put("billType", "RepoBilling");
                        map.put("movementType", movementType);
                        System.out.println("entered not equal to Zero");
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceEditLog", map);
                        System.out.println("the update toll and detaintion =" + status);
                    }
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateShippingBillOrBillOfEntryForBilling(String consignment_order_id, String billOfEntryNo, String billingPartyId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {

            map.put("consignmentOrderId", consignment_order_id);
            map.put("billOfEntryNo", billOfEntryNo);
            //    map.put("shippingLineNo", shippingLineNo);
            map.put("billingPartyId", billingPartyId);

            System.out.println("the update bill of entry or shipping bill no" + map);

            if (!"0".equals(billOfEntryNo) && !"".equals(billOfEntryNo) && billOfEntryNo != null) {

                status = (Integer) getSqlMapClientTemplate().update("trip.updateBillOfEntryNo", map);
            }
//        if( !"0".equals(shippingLineNo) && !"".equals(shippingLineNo) && shippingLineNo!=null ){
//        status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNo", map);
//        }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripOtherExpense(String tripId, String[] tripExpenseId, String[] expenses, String[] netexpenses, String[] expenseId, String[] expenseType, String totalRevenue, String[] oldExpenses, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);
        map.put("tripSheetId", tripId);
        String totalRevenues = "";
        String oldExpense = "";
        String expense = "";
        int status = 0;
        try {
            for (int i = 0; i < tripExpenseId.length; i++) {
                map.put("tripExpenseId", tripExpenseId[i]);
                map.put("expenseId", expenseId[i]);
                map.put("oldExpense", oldExpenses[i]);
                map.put("expenses", expenses[i]);
                map.put("netexpenses", netexpenses[i]);

                System.out.println("the updateTripOtherExpense:" + map);
                // for double twenty  contanier,we need to  split the container for each trip id

                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseAfterClosure", map);
                System.out.println("updateTripOtherExpense" + status);

            }

            System.out.println("the expesne updated=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripAddexpense(String expenseId, String expenseDate, String expensesType, String marginValue, String taxExpenses, String expenseValue, String tripSheetId, String driverNameex, String remarks, String totalExpenses, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {

            map.put("expenseId", expenseId);
            map.put("expenseDate", expenseDate);
            map.put("expensesType", expensesType);
            map.put("marginValue", "0");
            map.put("taxExpenses", "0");
            map.put("expenseValue", expenseValue);
            map.put("driverNameex", "0");
            map.put("tripSheetId", tripSheetId);
            map.put("totalExpenses", totalExpenses);
            map.put("remarks", remarks);
            String[] temp = null;
            temp = expenseDate.split("-");
            map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " 00:00:00");
            System.out.println("the updateTripAddexpense:" + map);
            // for double twenty  contanier,we need to  split the container for each trip id
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripAddExpenseAfterClosure", map);

            System.out.println("the expesne updated=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripAddExpenseAfterClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int saveAdvanceGrNo(String noOfGr, String customerId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);
        int loopcount = Integer.parseInt(noOfGr);

        int tempGrNo = 0;
        int insertTripGr = 0;
        String grNo = "";
        try {
            for (int i = 0; i < loopcount; i++) {

                System.out.println("the getGrNoSequence:" + map);

                tempGrNo = (Integer) getSqlMapClientTemplate().insert("trip.getGrNoSequence", map);
                System.out.println("tempGrNo" + tempGrNo);
                grNo = String.valueOf(tempGrNo);
                if (grNo.length() == 1) {
                    grNo = "000000" + grNo;
                    System.out.println("grno 1.." + grNo);
                } else if (grNo.length() == 2) {
                    grNo = "00000" + grNo;
                    System.out.println("grno lenght 2.." + grNo);
                } else if (grNo.length() == 3) {
                    grNo = "0000" + grNo;
                    System.out.println("grno lenght 3.." + grNo);
                } else if (grNo.length() == 4) {
                    grNo = "000" + grNo;
                } else if (grNo.length() == 5) {
                    grNo = "00" + grNo;
                } else if (grNo.length() == 6) {
                    grNo = "0" + grNo;
                }
                map.put("tripGr", grNo);
                map.put("customerId", customerId);
                map.put("blockStatus", "Y");
                map.put("used", "N");
                System.out.println("map:" + map);
                insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.insertTripGr", map);

            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public int saveCancelledGrNo(String[] grIds, String[] activeInds, String customerId, String remarks, int userId) {
        Map map = new HashMap();
        int insertTripGr = 0;
        map.put("userId", userId);
        map.put("customerId", customerId);
        map.put("remarks", remarks);

        try {
            System.out.println(" activeInds.length" + activeInds.length);
            for (int i = 0; i < activeInds.length; i++) {
                if (activeInds[i].equals("Y")) {
                    map.put("grId", grIds[i]);
                    System.out.println("map valuei is " + map);
                    insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.saveCancelledGrNo", map);
                }
            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public ArrayList getAdvanceGrDetails() {
        Map map = new HashMap();

        ArrayList advanceGrDetails = new ArrayList();
        try {

            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceGrDetails", map);

            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return advanceGrDetails;
    }

    public ArrayList getBlockedAdvanceGrDetails(String customerId) {
        Map map = new HashMap();

        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("customerId", customerId);
            System.out.println(" map is" + map);
            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBlockedAdvanceGrDetails", map);

            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBlockedAdvanceGrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBlockedAdvanceGrDetails", sqlException);
        }
        return advanceGrDetails;
    }

    public ArrayList getCancelGrDetails() {
        Map map = new HashMap();

        ArrayList cancelGrDetails = new ArrayList();
        try {

            cancelGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCancelGrDetails", map);

            System.out.println("getCancelGrDetails size=" + cancelGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return cancelGrDetails;
    }

    public ArrayList getAdvanceGrDetailsForPlanning(String customerId) {
        Map map = new HashMap();
        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("customerId", customerId);
            String[] customerIds = customerId.split(",");
            List customerIdVal = new ArrayList(customerIds.length);
            for (int i = 0; i < customerIds.length; i++) {
                System.out.println("value:" + customerIds[i]);
                customerIdVal.add(customerIds[i]);
            }
            map.put("customerId", customerIdVal);

            System.out.println("map:" + map);
            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceGrDetailsForPlanning", map);
            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return advanceGrDetails;
    }

    public int updatetripGR(String tripId, int userId, String grNo, SqlMapClient session) {
        Map map = new HashMap();
        int update = 0;
        map.put("userId", userId);
        map.put("tripId", tripId);
        map.put("grNo", grNo);
        System.out.println("map for advance GR:" + map);
        try {
            update = (Integer) session.update("trip.updateTripAdvanceGr", map);
            System.out.println("update" + update);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return update;
    }

    public int updatetripGR(String tripId, int userId, String grNo) {
        Map map = new HashMap();
        int update = 0;
        map.put("userId", userId);
        map.put("tripId", tripId);
        map.put("grNo", grNo);
        System.out.println("map for advance GR:" + map);
        try {
            update = (Integer) getSqlMapClientTemplate().update("trip.updateTripAdvanceGr", map);
            System.out.println("update" + update);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return update;
    }

    public String getTotalExpense(TripTO tripTO) {
        Map map = new HashMap();
        String totalExpense = "";
        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            totalExpense = (String) getSqlMapClientTemplate().queryForObject("trip.totalExpesne", map);

            System.out.println("totalExpense =" + totalExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return totalExpense;
    }

    public int saveTripCash(TripTO tripTo, String paidCash, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("paidCash", paidCash);
            map.put("tripId", tripTo.getTripId());
            String GrNo = (String) getSqlMapClientTemplate().queryForObject("trip.getGrNo", map);
            map.put("grNo", GrNo);
            System.out.println("map for cash:" + map);

            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripCash", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateCashBalance", map);

            System.out.println("the saveTripCash inserted=" + status);

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int saveTripCash(TripTO tripTo, String paidCash, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("paidCash", paidCash);
            map.put("tripId", tripTo.getTripId());
            String GrNo = (String) session.queryForObject("trip.getGrNo", map);
            map.put("grNo", GrNo);
            System.out.println("map for cash:" + map);

            status = (Integer) session.update("trip.insertTripCash", map);
            status = (Integer) session.update("trip.updateCashBalance", map);

            System.out.println("the saveTripCash inserted=" + status);

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateBillingPartyForBilling(String[] tripId, String[] consignmentOrderIds, String[] billOfEntryNo, String billingParty, String billingPartyId, String movementType, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("billingParty", billingParty);
                map.put("billingPartyId", billingPartyId);

                System.out.println("the update bill of entry or shipping bill no" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingPartyForTrip", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateBillingPartyForRepoBilling(String[] tripId, String[] consignmentOrderIds, String billingParty, String billingPartyId, String movementType, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("billingParty", billingParty);
                map.put("billingPartyId", billingPartyId);

                System.out.println("the update bill of entry or shipping bill no" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingPartyForTrip", map);
                System.out.println("updateShippingBillOrBillOfEntryForBilling=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList emailList = new ArrayList();
        try {
            emailList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailListDetails", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmailList", sqlException);
        }
        return emailList;
    }

    public int updateMailStatus(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("mailSendingId", tripTO.getMailSendingId());
        map.put("mailDeliveredRespone", tripTO.getMailDeliveredResponse());
        map.put("mailDeliveredStatus", tripTO.getMailDeliveredStatusId());
        try {
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateMailStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateMailStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMailStatus", sqlException);
        }
        return updateStatus;
    }

    public int saveChangeVehicleAfterTripStart(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int updateTripMaster = 0;
        int updateDriverDetails = 0;
        int insertDriverDetails = 0;
        int updateTripVehicle = 0;

        map.put("tripId", tripTO.getTripId());
//        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleType", tripTO.getVehicleType());
        map.put("startKm", tripTO.getOdometerReading());
        map.put("startHm", 0.00);

        if ("".equals(tripTO.getVehicleCapUtil())) {
            map.put("vehicleCapUtil", 0.00);
        } else {
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
        }

        map.put("vehicleTonnage", tripTO.getVehicleTonnage());
        map.put("vendorId", tripTO.getVendorId());
        map.put("driverName", tripTO.getDriverName());
        map.put("driverId", tripTO.getDriver1Id());
        map.put("userId", userId);

        System.out.println("map ###= " + map);
        try {

            String veh = tripTO.getVehicleNo();
            System.out.println("vehicleNO###########" + veh);
            if (tripTO.getVehicleNo().contains("~")) {
                map.put("vehicleNo", "");
            } else {
                map.put("vehicleNo", tripTO.getVehicleNo());
            }
            updateTripVehicle = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicle", map);
            System.out.println("updateTripVehicle###==" + updateTripVehicle);

            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripVehicleDetails", map);
            System.out.println("status###==" + status);

            String[] tempVendorId = tripTO.getVendorId().split("~");
            map.put("vendorId", tempVendorId[0]);

            updateTripMaster = (Integer) getSqlMapClientTemplate().update("trip.updateTripMaster", map);
            System.out.println("updateTripMaster###==" + updateTripMaster);

//            map.put("vehicleId", tripTO.getVehicleId());
//            map.put("driverName", tripTO.getDriverName());
//            map.put("driverId", tripTO.getDriver1Id());
            System.out.println("updateDriverDetailsMAP@@@" + map);
            updateDriverDetails = (Integer) getSqlMapClientTemplate().update("trip.updateDriverDetails", map);
            System.out.println("updateDriverDetails###==" + updateDriverDetails);

            insertDriverDetails = (Integer) getSqlMapClientTemplate().update("trip.insertDriverDetails", map);
            System.out.println("insertDriverDetails###==" + insertDriverDetails);
            int updateFuel = 0;
            if ("Y".equals(tripTO.getNewSlip())) {
                updateFuel = (Integer) getSqlMapClientTemplate().update("trip.updateFuelDetails", map);
                updateFuel = (Integer) getSqlMapClientTemplate().update("trip.updateFuelSNo", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "status", sqlException);
        }
        return status;

    }

    public int updateEstimateRevenue(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("estimateRevenue", tripTO.getEstimatedRevenue());

        map.put("userId", userId);
        System.out.println("the updateTripAdance" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEstimateRevenue", map);
            System.out.println("updateEstimateRevenue=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEstimateRevenue" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEstimateRevenue", sqlException);
        }

        return status;
    }

    public Integer deleteExpenseIds(TripTO tripTO, int userId) {
        Map map = new HashMap();

        System.out.println("map@@@" + tripTO.getTripExpenseIds());
        String[] tripExpenses1 = tripTO.getTripExpensesId();
        String[] expenseType = tripTO.getExpenseTypes();
        String[] expenses = tripTO.getExpenses();

        String[] tripExpenses = tripTO.getTripExpenseIds().split(",");
        List tripExpensesList = new ArrayList(tripExpenses.length);
        for (int i = 0; i < tripExpenses.length; i++) {
            System.out.println("value:" + tripExpenses[i]);
            tripExpensesList.add(tripExpenses[i]);
        }
        map.put("tripExpensesList", tripExpensesList);
        Integer status = 0;
        try {
            // Delete invoiceDetail & invoice header For Bill Not To Customer
            status = (Integer) getSqlMapClientTemplate().update("trip.deleteExpenseIds", map);
            System.out.println("status@@@" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "status List", sqlException);
        }

        return status;
    }

    public int updateAdvanceGrNo(String noOfGr, String grNos, String grId, String customerId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);
        int loopcount = Integer.parseInt(noOfGr);
        System.out.println("loopcount@@@" + loopcount);
        int tempGrNo = 0;
        int insertTripGr = 0;
//        String grNo = "";
        try {
            for (int i = 0; i < loopcount; i++) {
                String[] grIds = grId.split(",");
                String[] grNo = grNos.split(",");

                map.put("grId", grIds[i]);
                map.put("tripGr", grNo[i]);
                map.put("customerId", customerId);
                map.put("blockStatus", "Y");
                map.put("used", "N");
                map.put("userId", userId);
                System.out.println("map:" + map);
                insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.updateAdvanceTripGr", map);

            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public ArrayList getAdvanceTripExpense(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getAdvanceTripExpense = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripId());
            System.out.println("closed Trip map is::" + map);
            getAdvanceTripExpense = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceTripExpense", map);
            System.out.println("getAdvanceTripExpense.size() = " + getAdvanceTripExpense.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvanceTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvanceTripExpense List", sqlException);
        }
        return getAdvanceTripExpense;
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO) {
        Map map = new HashMap();
        int advanceVoucherNoSequence = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        try {
            advanceVoucherNoSequence = (Integer) getSqlMapClientTemplate().insert("trip.advanceVoucherNoSequence", map);
            System.out.println("advanceVoucherNoSequence=" + advanceVoucherNoSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("advanceVoucherNoSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "advanceVoucherNoSequence", sqlException);
        }
        return advanceVoucherNoSequence + "";
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int advanceVoucherNoSequence = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        try {
            advanceVoucherNoSequence = (Integer) session.insert("trip.advanceVoucherNoSequence", map);
            System.out.println("advanceVoucherNoSequence=" + advanceVoucherNoSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("advanceVoucherNoSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "advanceVoucherNoSequence", sqlException);
        }
        return advanceVoucherNoSequence + "";
    }

    public ArrayList getTripExpensePrintDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("tripId", tripTO.getTripId());

        System.out.println("map for print Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripExpensePrintDetails", map);
            System.out.println("getTripExpensePrintDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpensePrintDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpensePrintDetails List", sqlException);
        }

        return tripDetails;
    }

    public int updateTransporter(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("vendorId", tripTO.getTransporter());

        map.put("userId", userId);
        System.out.println("the updateTransporter" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTransporter", map);
            System.out.println("updateTransporter=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return status;
    }

    public ArrayList getMapLocationList(String routeId) {
        Map map = new HashMap();

        ArrayList tripClosureDetails = new ArrayList();
        map.put("routeId", routeId);
        System.out.println("getMapLocationList" + map);
        try {
            tripClosureDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMaplocation", map);
            System.out.println("getMapLocationList size=" + tripClosureDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return tripClosureDetails;
    }

    public int getTripExistInBilling(String[] tripId) {
        Map map = new HashMap();
        int status = 0;
        try {
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getTripExistInBilling map" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripExistInBilling", map);
            System.out.println("status size is ::::" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExistInBilling Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExistInBilling", sqlException);
        }
        return status;
    }

    public int insertTripFuel(String fuelType, int userId, String returnTripId, int tripId, String bunkName, String bunkPlace, String fuelDate, String fuelAmount,
            String fuelLtrs, String fuelRemarks, String driverId, String slipNo, String tripFuelId, String vehicleId, String hireVehicleNo, SqlMapClient session) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            String[] tempBunk = bunkName.split("~");
//            System.out.println("tempBunk[0] = " + tempBunk[0]);
//            System.out.println("tempBunk[1] = " + tempBunk[1]);
//            System.out.println("tempBunk[2] = " + tempBunk[2]);
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            map.put("bunkName", tempBunk[0]);
            map.put("bunkPlace", bunkPlace);
            map.put("fuelType", fuelType);
            map.put("fuelDate", fuelDate);
            map.put("fuelAmount", fuelAmount);
            map.put("fuelLtrs", fuelLtrs);
            map.put("fuelRemarks", fuelRemarks);
            map.put("createdBy", userId);
            map.put("slipNo", slipNo);
            map.put("tripFuelId", tripFuelId);
            map.put("vehicleId", vehicleId);
            if ("0".equals(vehicleId)) {
                map.put("hireVehilceNo", hireVehicleNo);
            }
            System.out.println("map in tripFuel " + map);
            if (tripFuelId == null || "".equals(tripFuelId)) {
                lastInsertedId = (Integer) session.update("operation.insertTripFuel", map);
                System.out.println("insertTripFuel -->" + lastInsertedId);

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                String code2 = (String) session.queryForObject("operation.getTripVoucherCode", map);
                String[] temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", "37");
                map.put("particularsId", "LEDGER-29");
                map.put("amount", fuelAmount);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Diesel Expense");
                map.put("Reference", returnTripId);
                map.put("SearchCode", tripId);
                map.put("driverId", driverId);

                System.out.println("map1 =---------------------> " + map);
                int status1 = (Integer) session.update("operation.insertTripAccountEntry", map);
                System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                if (status1 > 0) {
                    //identify debit account
                    //if own driver, debit account is driver account
                    //if contract driver, the debit account is contract vendor account
                    String ledgerId = tempBunk[1];
                    String particularsId = tempBunk[2];

                    map.put("DetailCode", "2");
                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    int status2 = (Integer) session.update("operation.insertTripAccountEntry", map);
                    System.out.println("status2 = " + status2);
                }
            } else {
                lastInsertedId = (Integer) session.update("operation.updateTripFuel", map);
                System.out.println("update:" + lastInsertedId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int manualAdvanceRequest(TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("advancerequestamt", tripTO.getAdvancerequestamt());
            map.put("requeston", tripTO.getRequeston());
            map.put("requestremarks", tripTO.getRequestremarks());
            map.put("requeststatus", tripTO.getRequeststatus());
            map.put("tripid", tripTO.getTripId());
            map.put("tobepaidtoday", tripTO.getTobepaidtoday());
            map.put("batchType", tripTO.getBatchType());
            map.put("tripday", tripTO.getTripday());
            map.put("currencyId", tripTO.getCurrencyid());
            map.put("userId", userId);
            System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) session.insert("operation.manualAdvanceRequest", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public int saveTripExpense(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripSheetId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("expenseType", "2");
            map.put("billMode", "0");
            map.put("employeeName", "0");
            map.put("activeValue", "0");
            map.put("taxPercentage", "0");
            map.put("expenseRemarks", "system Update");
            map.put("currency", "1");
            map.put("marginValue", "0");

//            Toll Update
            map.put("netExpense", tripTO.getTollAmount());
            map.put("expenses", tripTO.getTollAmount());
            map.put("expenseName", "1011");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
//            Driver Batta Update
            map.put("netExpense", tripTO.getDriverBatta());
            map.put("expenses", tripTO.getDriverBatta());
            map.put("expenseName", "1024");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
//            Misc  Update
            map.put("netExpense", tripTO.getMiscValue());
            map.put("expenses", tripTO.getMiscValue());
            map.put("expenseName", "1018");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
//            Dala Update
            map.put("netExpense", tripTO.getOtherExpense());
            map.put("expenses", tripTO.getOtherExpense());
            map.put("expenseName", "1025");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            
            map.put("netExpense",0);
            map.put("expenses", 0);
            map.put("expenseName", "1010");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            map.put("netExpense",0);
            map.put("expenses", 0);
            map.put("expenseName", "1023");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);

            System.out.println("saveTripExpense=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpense", sqlException);
        }
        return status;
    }

    public ArrayList getContainerAvailability(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList containerAvailibility = new ArrayList();
        map.put("containerNo", tripTO.getContainerNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        System.out.println("getMapLocationList" + map);
        try {
            if (tripTO.getContainerNo() != null && !"".equals(tripTO.getContainerNo())) {
                containerAvailibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerAvailabilitySearch", map);
            } else {
                containerAvailibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerAvailability", map);
            }
            System.out.println("getContainerAvailability size=" + containerAvailibility.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return containerAvailibility;
    }

    public ArrayList getContainerVisibility(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList containerVisibility = new ArrayList();
        map.put("containerNo", tripTO.getContainerNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        System.out.println("getMapLocationList" + map);
        try {

            containerVisibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerVisibility", map);

            System.out.println("getContainerAvailability size=" + containerVisibility.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return containerVisibility;
    }

    public ArrayList getComodityDetails() {
        Map map = new HashMap();

        ArrayList comodityDetails = new ArrayList();

        try {

            comodityDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getComodityDetails", map);

            System.out.println("getComodityDetails size=" + comodityDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return comodityDetails;
    }

    public int updateTripRoute(TripTO tripTO, int userId, String tripRouteCourseId, String pointType, String pointSequence, String routeInfo, String newReveneue) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("tripId", tripTO.getTripId());
            map.put("tripRouteCourseId", tripRouteCourseId);
            map.put("pointId", tripTO.getTripPointId());
            map.put("pointType", pointType);
            map.put("pointOrder", pointSequence);
            map.put("pointAddresss", "");
            map.put("pointPlanDate", "00-00-0000");
            map.put("pointPlanTime", "00:00:00");
            map.put("routeInfo", routeInfo);
            map.put("newRevenue", newReveneue);

            map.put("userId", userId);
            System.out.println("map val" + map);
            if (!"".equalsIgnoreCase(tripRouteCourseId) && !"0".equalsIgnoreCase(tripRouteCourseId)) {
                manualAdvanceRequest = (Integer) getSqlMapClientTemplate().update("trip.updateTripRoute", map);
            } else if ("0".equalsIgnoreCase(tripRouteCourseId)) {
                int update = (Integer) getSqlMapClientTemplate().update("trip.updateTripRouteSequence", map);
                System.out.println("update" + update);
                if (update > 0) {
                    int insert = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
                }
            }
            int routeUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateRouteInfo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public int insertTripMasterLog(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("tripId", tripTO.getTripId());

            if (tripTO.getStatusId().equals("3")) {
                map.put("tripType", "3");
            } else {
                map.put("tripType", "1");
            }
            map.put("userId", userId);
            System.out.println("map val" + map);

            int routeUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripMasterLog", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public ArrayList getTotalTripExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList totalExpenseDetails = new ArrayList();
        try {
            totalExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOtherExpenseDetails", map);
            System.out.println("getGPSDetails size=" + totalExpenseDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalTripExpenseDetails List", sqlException);
        }

        return totalExpenseDetails;
    }

    public ArrayList getEmptyShippingBillNoContainerList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList emptyShippingContainerList = new ArrayList();
        try {
            emptyShippingContainerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyShippingBillNoContainerList", map);
            System.out.println("getEmptyShippingBillNoContainerList size=" + emptyShippingContainerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyShippingBillNoContainerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalTripExpenseDetails List", sqlException);
        }

        return emptyShippingContainerList;
    }

    public String getConsignmentContainerId(TripTO tripTO) {
        Map map = new HashMap();
        String consignmentContainerId = "";

        try {
            map.put("containerNo", tripTO.getContainerNo());
            map.put("consignmentOrderId", tripTO.getConsignmentOrderNos());
            System.out.println(" getConsignmentContainerId map is::" + map);
            consignmentContainerId = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentContainerId", map);
            System.out.println("consignmentContainerId.() = " + consignmentContainerId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return consignmentContainerId;
    }

    public String getVendorId(TripTO tripTO) {
        Map map = new HashMap();
        String vendorId = "";

        try {
            map.put("vendorName", "%" + tripTO.getTransporter() + "%");

            System.out.println(" getVendorId map is::" + map);
            vendorId = (String) getSqlMapClientTemplate().queryForObject("trip.getVendorId", map);
            System.out.println("getVendorId.() = " + vendorId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return vendorId;
    }

    public String getUploadVehicledId(TripTO tripTO) {
        Map map = new HashMap();
        String consignmentContainerId = "";

        try {
            map.put("vehicleNo", "%" + tripTO.getVehicleNo() + "%");

            System.out.println(" getUploadVehicleId map is::" + map);
            consignmentContainerId = (String) getSqlMapClientTemplate().queryForObject("trip.getUploadVehicleId", map);
            System.out.println("getUploadVehicleId.() = " + consignmentContainerId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return consignmentContainerId;
    }

    public String getOrderId(TripTO tripTO) {
        Map map = new HashMap();
        String consignmentContainerId = "";

        try {
            map.put("consignmentOrderNo", tripTO.getConsignmentNo());

            System.out.println(" getOrderId map is::" + map);
            consignmentContainerId = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderId", map);
            System.out.println("getOrderId.() = " + consignmentContainerId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return consignmentContainerId;
    }

    public String getCityId(String cityName) {
        Map map = new HashMap();
        String consignmentContainerId = "";

        try {
            map.put("cityName", cityName);

            System.out.println(" getCityId map is::" + map);
            consignmentContainerId = (String) getSqlMapClientTemplate().queryForObject("trip.getCityId", map);
            System.out.println("getCityId.() = " + consignmentContainerId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return consignmentContainerId;
    }

    public int updateStartTripSheetForUpload(TripTO tripTO, int userId) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        Map map2 = new HashMap();

        map.put("userId", userId);
        map.put("planStartTime", tripTO.getPlanStartHour());
        map.put("planStartDate", tripTO.getPlanStartDate());
        map.put("startDate", tripTO.getStartDate());
        map.put("startTime", tripTO.getTripStartHour());
        map.put("startOdometerReading", "0");
        map.put("startHM", "0");
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("startTripRemarks", "trip uploaded");
        map.put("statusId", "10");
        map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
        map.put("lrNumber", "0");
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour());
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour());
        map.put("vehicleloadtemperature", "0");
        map.put("startTrailerKM", "0");

        map.put("regNo", tripTO.getRegNo());
        map1.put("tripId", tripTO.getTripId());
        map2.put("userId", userId);
        map2.put("tripId", tripTO.getTripId());
        map2.put("gpsVendor", tripTO.getGpsVendor());
        map2.put("gpsDeviceId", tripTO.getGpsDeviceId());

        System.out.println("the updatetripsheetdetails" + map);
        int status = 0;
        int update = 0;
        //  String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
        //System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
        // String[] ConsignmentId = ConsignmentIdList.split(",");
//        for (int i = 0; i < ConsignmentId.length; i++) {
//            map.put("consignmentId", ConsignmentId[i]);
//            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
//
//            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
//            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("23")) {
//                map.put("consignmentStatus", "24");
//            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("26")) {
//                map.put("consignmentStatus", "27");
//            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("29")) {
//                map.put("consignmentStatus", "30");
//            } else {
//                map.put("consignmentStatus", "10");
//            }
//            System.out.println("the updated map:" + map);
//
//            update = (Integer) getSqlMapClientTemplate().update("trip.updateStartconsignmentSheet", map);
//            System.out.println("the update:" + update);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//            System.out.println("the status1:" + status);
//        }
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripSheetDetails", map);
            System.out.println(" start trip status= " + status);
//            if (tripTO.getRegNo() != null) {
//
//                status = (Integer) getSqlMapClientTemplate().update("trip.updateStartVehicle", map);
//            }
            System.out.println("vehicle regNo status= " + status);
            //   status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripGpsdetails", map2);

            System.out.println("vehicle gps status= " + status);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheet", map);

//             updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            //            int maxVehicleSequence = 0;
            //            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            //            map.put("vehicleSequence", maxVehicleSequence);
            //            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
//            int insertConsignmentArticle = 0;
//            map.put("consignmentId", tripTO.getConsignmentId());
//            System.out.println("the inner" + map);
//            String[] productCodes = null;
//            String[] productNames = null;
//            String[] packageNos = null;
//            String[] weights = null;
//            String[] productbatch = null;
//            String[] productuom = null;
//            String[] loadedpackages = null;
//            productCodes = tripTO.getProductCodes();
//            productNames = tripTO.getProductNames();
//            packageNos = tripTO.getPackagesNos();
//            weights = tripTO.getWeights();
//            productbatch = tripTO.getProductbatch();
//            productuom = tripTO.getProductuom();
//            loadedpackages = tripTO.getLoadedpackages();
//            for (int i = 0; i < productNames.length; i++) {
//                map.put("productCode", productCodes[i]);
//                map.put("producName", productNames[i]);
//                map.put("packageNos", packageNos[i]);
//                map.put("weights", weights[i]);
//                map.put("productbatch", productbatch[i]);
//                map.put("productuom", productuom[i]);
//                map.put("loadedpackages", loadedpackages[i]);
//                System.out.println("The product Details:" + map);
//                if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
//                    insertConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
//                }
//
//            }
//            String tripRevenueDetails = "";
//            String tripTemp[] = null;
//            String customerId = "";
//            String tripType = "";
//            String estimatedRevenue = "";
//            String emptyTrip = "";
//            tripRevenueDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRevenueDetails", map);
//            System.out.println("tripRevenueDetails = " + tripRevenueDetails);
//            if (!"".equals(tripRevenueDetails)) {
//                tripTemp = tripRevenueDetails.split("~");
//                customerId = tripTemp[0];
//                tripType = tripTemp[1];
//                estimatedRevenue = tripTemp[2];
//                emptyTrip = tripTemp[3];
//            }
            /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
             String code2 = "";
             String[] temp = null;
             int insertStatus = 0;
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%SALES%");
             code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
             temp = code2.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "SALES-" + codev2;
             System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "SALES");

             //get ledger info
             map.put("customer", customerId);
             String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
             System.out.println("ledgerInfo:" + ledgerInfo);
             temp = ledgerInfo.split("~");
             String ledgerId = temp[0];
             String particularsId = temp[1];
             map.put("ledgerId", ledgerId);
             map.put("particularsId", particularsId);

             map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
             //  map.put("amount", crossingAmount);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Freight Charges");
             map.put("Reference", "Trip");
             //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
             //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
             System.out.println("tripId = " + tripTO.getTripSheetId());
             map.put("SearchCode", tripTO.getTripSheetId());
             System.out.println("map1 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "51");
             map.put("particularsId", "LEDGER-39");
             map.put("Accounts_Type", "CREDIT");
             System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
             System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status2 = " + insertStatus);
             }

             }
             */
            System.out.println("updateStartTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheetForTripUpload(TripTO tripTO, int userId) {
        Map map = new HashMap();
//            nilesh 17 10 2015 Start
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour());
//            nilesh 17 10 2015 End
        map.put("endTime", tripTO.getTripEndHour());
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", "1");
        map.put("tripTransitHours", "20");
        map.put("endOdometerReading", "0");
        map.put("endHM", "0");
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", "90");
        map.put("totalHrs", "6");
        map.put("userId", userId);
        map.put("statusId", "12");
        map.put("tripDetainHours", "0");
        if (!"NaN".equals(tripTO.getTripFactoryToIcdHour()) || (tripTO.getTripFactoryToIcdHour()) != null) {
            map.put("tripFactoryToIcdHours", tripTO.getTripFactoryToIcdHour());
        } else {
            map.put("tripFactoryToIcdHours", 0.0);

        }
//        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
//            map.put("setteledKm", tripTO.getSetteledKm());
//        }
//        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
//            map.put("setteledHm", tripTO.getSetteledHm());
//        }
//        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
//            map.put("setteltotalKM", tripTO.getSetteltotalKM());
//        }
//        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
//            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
//        }
        if (!"".equals(tripTO.getBillOfEntry())) {
            map.put("billOfEntry", tripTO.getBillOfEntry());
        }
        if (!"".equals(tripTO.getShipingLineNo())) {
            map.put("shipingLineNo", tripTO.getShipingLineNo());
        }
        System.out.println("the updateEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheetDuringClosure", map);
            if (!"".equals(tripTO.getBillOfEntry())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForBoE", map);
                System.out.println("BoE updated .." + status);
            }
            if (!"".equals(tripTO.getShipingLineNo())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForSlN", map);
                System.out.println("sln updated .." + status);
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripVehicleDuringClosure", map);
            System.out.println("updateEndTripSheetDuringClosure size=" + tripDetails.size());
            // update product
            int updateConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
//            String[] productCodes = null;
//            String[] unloadedpackages = null;
//            String[] shortage = null;
//            String[] articleId = null;
//            String[] articleName = null;
//            articleName = tripTO.getProductNames();
//            productCodes = tripTO.getProductCodes();
//            unloadedpackages = tripTO.getUnloadedpackages();
//            articleId = tripTO.getTripArticleId();
//            shortage = tripTO.getShortage();
//            map.put("consignmentId", 0);
//            map.put("productCode", tripTO.getCommodityCategory());
//            map.put("productbatch", 0);
//            map.put("packageNos", 0);
//            map.put("weights", 0);
//            map.put("productuom", 1);
//            map.put("loadedpackages", 0);
//            map.put("shortage", 0);
//            map.put("tripArticleId", 0);
//            map.put("producName", tripTO.getCommodityName());
//            System.out.println("map for product..." + map);
//            int article = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripArticleDeatils", map);
//            if (article == 0) {
//                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
//            } else {
//                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticle", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int saveTripClosureDetailsForUpload(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("tripId", tripTO.getTripId());
        map.put("statusId", "13");
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", "1");
        map.put("runKM", "50");
        map.put("reeferHours", "0");
        map.put("gpsKm", "0");
        map.put("gpsHm", "0");
        map.put("reeferMinutes", "0");
        map.put("mileage", "3.5");
        map.put("reeferConsumption", "2.75");
        map.put("fuelConsumed", "0");
        map.put("fuelAmount", "0");
        map.put("fuelCost", "0");
        map.put("tollAmount", "0");
        map.put("tollCost", "0");
        map.put("incentiveAmount", "0");
        map.put("driverIncentive", "0");
        map.put("miscValue", "0");
        map.put("battaAmount", "0");
        map.put("driverBatta", "0");
        map.put("systemExpenses", "0");
        map.put("routeExpense", "0");
        map.put("totalExpense", "0");
        map.put("hireCharges", "0");
        map.put("dieselAmount", "0");
        map.put("userId", userId);

        if (tripTO.getSecondaryParkingAmount().equals("")) {
            map.put("secParkingAmount", "0");
        } else {
            map.put("secParkingAmount", tripTO.getSecondaryParkingAmount());
        }
        if (tripTO.getPreColingAmount().equals("")) {
            map.put("preColingAmount", "0");
        } else {
            map.put("preColingAmount", tripTO.getPreColingAmount());
        }
        if (tripTO.getSecAdditionalTollCost().equals("")) {
            map.put("secAddlTollCost", "0");
        } else {
            map.put("secAddlTollCost", tripTO.getSecAdditionalTollCost());
        }
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        int driverChangeStatus = 0;
        int driverSettelment = 0;
        ArrayList tripDetails = new ArrayList();
        ArrayList vehicleDriverlist = new ArrayList();
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveTripClosure", map);
            int statusInsert = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
            System.out.println("status tripClosureId =" + status);
            int insertTripExpenseCount = 0;
            if (status != 0) {
                if (!"0".equals(tripTO.getVehicleId())) {
                    insertTripExpenseCount = (Integer) getSqlMapClientTemplate().update("trip.insertTripExpenseReportCount", map);
                }
            }
//            if (status > 0) {
//                map.put("tripClosureId", status);
//                String driver = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleDriverId", map);
//                if (!"".equals(driver) && driver != null) {
//                    map.put("driver", driver);
//                } else {
//                    map.put("driver", 0);
//                }
//                if (!"".equals(tripTO.getUnclearedAmount()) && tripTO.getUnclearedAmount() != null) {
//                    map.put("unclearedAmount", tripTO.getUnclearedAmount());
//                } else {
//                    map.put("unclearedAmount", 0);
//                }
//
//                System.out.println(" map for Driver Settelment:" + map);
//                driverSettelment = (Integer) getSqlMapClientTemplate().update("trip.updateTripDriverSettelement", map);
//                driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
//                System.out.println("driverChangeStatus = " + driverChangeStatus);
//                if (driverChangeStatus > 0) {
//                    // Driver Cnage Case
//                    inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
//                    Iterator itr1 = inActiveVehicleDriverlist.iterator();
//                    TripTO tpTO = new TripTO();
//                    Float runKm = 0.0f;
//                    Float totalRunKm = 0.0f;
//                    Float runHm = 0.0f;
//                    Float totalRunHm = 0.0f;
//                    Float milleage = 0.0f;
//                    Float reeferMileage = 0.0f;
//                    Float dieselUsed = 0.0f;
//                    Float driverBatta = 0.0f;
//                    Float miscRate = 0.0f;
//                    Float extraExpenseValue = 0.0f;
//                    Float fuelPrice = 0.0f;
//                    Float tollRate = 0.0f;
//                    Float driverIncentivePerKm = 0.0f;
//                    Float nettExpense = 0.0f;
//                    Float bookedExpense = 0.0f;
//                    Float tempEstimatedExpense = 0.0f;
//                    Float estimatedExpense = 0.0f;
//                    Float driverBattaPerDay = 100.0f;
//                    String startDate = "";
//                    String endDate = "";
//                    String expenseValue = "";
//                    int totalDays = 0;
//                    int totalDaysSum = 0;
//                    int driverCount = 0;
//                    int driverCountSum = 0;
//                    while (itr1.hasNext()) {
//                        tpTO = new TripTO();
//                        tpTO = (TripTO) itr1.next();
//                        String[] tempDriverId = null;
//                        driverCount = Integer.parseInt(tpTO.getDriverCount());
//                        driverCountSum += Integer.parseInt(tpTO.getDriverCount());
//                        totalDays = Integer.parseInt(tpTO.getTotalDays());
//                        totalDaysSum += Integer.parseInt(tpTO.getTotalDays());
//                        tempDriverId = tpTO.getDriverInTrip().split(",");
//                        runKm = Float.parseFloat((String) tpTO.getTotalKm());
//                        totalRunKm += Float.parseFloat((String) tpTO.getTotalKm());
//                        runHm = Float.parseFloat((String) tpTO.getTotalHm());
//                        totalRunHm += Float.parseFloat((String) tpTO.getTotalHm());
//                        milleage = Float.parseFloat((String) tripTO.getMilleage());
//                        reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
//                        dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
//                        driverBatta = totalDays * driverBattaPerDay * driverCount;
//                        miscRate = Float.parseFloat((String) tripTO.getMiscRate());
//                        startDate = tpTO.getStartDate();
//                        endDate = tpTO.getEndDate();
//                        map.put("tripStartDate", startDate);
//                        map.put("tripEndDate", endDate);
//                        expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
//                        extraExpenseValue = Float.parseFloat((String) expenseValue);
//                        fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
//                        tollRate = Float.parseFloat((String) tripTO.getTollRate());
//                        driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
//                        bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
//                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
//                        Float dieselCost = dieselUsed * fuelPrice;
//                        Float tollCost = runKm * tollRate;
//                        Float driverIncentive = runKm * driverIncentivePerKm;
//                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
//                        nettExpense = systemExpense + bookedExpense;
//                        tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
//                        estimatedExpense = tempEstimatedExpense * runKm;
//
//                        if (tempDriverId.length > 0) {
//                            for (int i = 0; i < tempDriverId.length; i++) {
//                                map.put("driverId", tempDriverId[i]);
//                                map.put("estimatedExpense", estimatedExpense / driverCount);
//                                map.put("totalDays", totalDays);
//                                map.put("runKM", runKm / driverCount);
//                                map.put("reeferHours", runHm / driverCount);
//                                map.put("gpsKm", 0);
//                                map.put("gpsHm", 0);
//                                map.put("reeferMinutes", "0");
//                                map.put("mileage", milleage);
//                                map.put("reeferConsumption", reeferMileage);
//                                map.put("fuelConsumed", dieselUsed / driverCount);
//                                map.put("fuelAmount", tripTO.getFuelPrice());
//                                map.put("fuelCost", dieselCost / driverCount);
//                                map.put("tollAmount", tripTO.getTollRate());
//                                map.put("tollCost", tollCost / driverCount);
//                                map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
//                                map.put("driverIncentive", driverIncentive / driverCount);
//                                map.put("miscValue", miscValue);
//                                map.put("battaAmount", tripTO.getDriverBattaPerDay());
//                                map.put("driverBatta", driverBatta / driverCount);
//                                map.put("systemExpenses", systemExpense / driverCount);
//                                map.put("routeExpense", bookedExpense / driverCount);
//                                map.put("totalExpense", nettExpense / driverCount);
//                                System.out.println("final Map = " + map);
//                                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
//                            }
//                        }
//                    }
//
//                    int vehicleDriverCount = 0;
//                    String driverId = "";
//                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
//                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
//                    if (vehicleDriverCount > 0) {
//                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
//                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
//                        Iterator itr2 = vehicleDriverlist.iterator();
//                        TripTO trpTO = new TripTO();
//                        vehicleDriverCount = vehicleDriverCount - driverCountSum;
//                        totalDays = totalDays - totalDaysSum;
//                        while (itr2.hasNext()) {
//                            trpTO = new TripTO();
//                            trpTO = (TripTO) itr2.next();
//                            driverId = trpTO.getDriverId();
//                            System.out.println("driverId = " + driverId);
//                            map.put("driverId", driverId);
//                            runKm = (Float.parseFloat((String) tripTO.getRunKm()) - totalRunKm) / vehicleDriverCount;
//                            runHm = (Float.parseFloat((String) tripTO.getRunHm()) - totalRunHm) / vehicleDriverCount;
//                            milleage = Float.parseFloat((String) tripTO.getMilleage());
//                            reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
//                            dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
//                            driverBatta = totalDays * driverBattaPerDay * vehicleDriverCount;
//                            miscRate = Float.parseFloat((String) tripTO.getMiscRate());
//                            startDate = tpTO.getStartDate();
//                            endDate = tpTO.getEndDate();
//                            map.put("tripStartDate", startDate);
//                            map.put("tripEndDate", endDate);
//                            expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
//                            extraExpenseValue = Float.parseFloat((String) expenseValue);
//                            fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
//                            tollRate = Float.parseFloat((String) tripTO.getTollRate());
//                            driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
//                            bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
//                            Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
//                            Float dieselCost = dieselUsed * fuelPrice;
//                            Float tollCost = runKm * tollRate;
//                            Float driverIncentive = runKm * driverIncentivePerKm;
//                            Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
//                            nettExpense = systemExpense + bookedExpense;
//                            tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
//                            estimatedExpense = tempEstimatedExpense * runKm;
//                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
//                            map.put("totalDays", tripTO.getTotalDays());
//                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
//                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
//                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
//                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
//                            map.put("reeferMinutes", "0");
//                            map.put("mileage", tripTO.getMilleage());
//                            map.put("reeferConsumption", tripTO.getReeferMileage());
//                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
//                            map.put("fuelAmount", tripTO.getFuelPrice());
//                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
//                            map.put("tollAmount", tripTO.getTollRate());
//                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
//                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
//                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
//                            map.put("miscValue", tripTO.getMiscValue());
//                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
//                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
//                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
//                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
//                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
//                            System.out.println("final Map = " + map);
//                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
//                        }
//                    }
//
//                } else {
//                    // Driver not Change Case
//                    int vehicleDriverCount = 0;
//                    String driverId = "";
//                    vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
//                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
//                    if (vehicleDriverCount > 0) {
//                        vehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleDriver", map);
//                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
//                        Iterator itr1 = vehicleDriverlist.iterator();
//                        TripTO tpTO = new TripTO();
//                        while (itr1.hasNext()) {
//                            tpTO = new TripTO();
//                            tpTO = (TripTO) itr1.next();
//                            driverId = tpTO.getDriverId();
//                            System.out.println("driverId = " + driverId);
//                            map.put("driverId", driverId);
//                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
//                            map.put("totalDays", tripTO.getTotalDays());
//                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
//                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
//                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
//                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
//                            map.put("reeferMinutes", "0");
//                            map.put("mileage", tripTO.getMilleage());
//                            map.put("reeferConsumption", tripTO.getReeferMileage());
//                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
//                            map.put("fuelAmount", tripTO.getFuelPrice());
//                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
//                            map.put("tollAmount", tripTO.getTollRate());
//                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
//                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
//                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
//                            map.put("miscValue", tripTO.getMiscValue());
//                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
//                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
//                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
//                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
//                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
//                            System.out.println("final Map = " + map);
//                            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriverClosure", map);
//                        }
//                    }
//
//                }
            //account entry details start

            //fuel cost
            String code2 = "";
            String[] temp = null;
            int insertStatus = 0;
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");
            code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
            temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            if ("3".equals(tripTO.getOwnerShip())) {
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 1582);
                map.put("particularsId", "LEDGER-1562");

                map.put("amount", tripTO.getDieselCost());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Hire Charge");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

            } else {
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 37);
                map.put("particularsId", "LEDGER-29");

                map.put("amount", tripTO.getDieselCost());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Fuel Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //toll cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 41);
                map.put("particularsId", "LEDGER-33");

                map.put("amount", tripTO.getTollCost());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Toll Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //driver Bata cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 34);
                map.put("particularsId", "LEDGER-27");

                map.put("amount", tripTO.getDriverBatta());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Driver Bhatta Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //driver Incentive cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 34);
                map.put("particularsId", "LEDGER-27");

                map.put("amount", tripTO.getDriverIncentive());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Driver Incentive Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }
                //Misc cost

                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 40);
                map.put("particularsId", "LEDGER-32");

                map.put("amount", tripTO.getMiscValue());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Misc Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripTO.getTripId());
                map.put("SearchCode", tripTO.getTripId());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

            }
            //updateTripCloserToEFS(tripTO.getTripId());
            //account entry details end

            //}
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int saveUploadTripExpense(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripSheetId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("expenseType", "2");
            map.put("billMode", "0");
            map.put("employeeName", "0");
            map.put("activeValue", "0");
            map.put("taxPercentage", "0");
            map.put("expenseRemarks", "system Update");
            map.put("currency", "1");
            map.put("marginValue", "0");

//            tripExpense Update
            map.put("netExpense", tripTO.getExpense());
            map.put("expenses", tripTO.getExpense());
            map.put("expenseName", "1017");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);

            System.out.println("saveTripExpense=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpense", sqlException);
        }
        return status;
    }

    public String getGrPrintCount(TripTO tripTO) {
        Map map = new HashMap();
        String count = "";
        String grNo = tripTO.getGrNo();
        map.put("grNo", grNo);

        try {
            count = (String) getSqlMapClientTemplate().queryForObject("trip.getGrPrintCount", map);
            System.out.println("count=" + count);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return count;
    }

    public String saveGrPrintCount(TripTO tripTO) {
        Map map = new HashMap();

        String grNo = tripTO.getGrNo();
        map.put("grNo", grNo);
        map.put("tripId", tripTO.getTripId());
        map.put("userId", tripTO.getUserId());

        System.out.println("map  for save:" + map);
        try {
            int count = (Integer) getSqlMapClientTemplate().insert("trip.saveGrPrintDetails", map);
            int count1 = (Integer) getSqlMapClientTemplate().update("trip.updatePrintCount", map);
            System.out.println("count-=" + count);
            System.out.println("count1=" + count1);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return "";
    }

    public ArrayList getSuppCreditOrderInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            map.put("creditNoteId", tripTO.getCreditNoteId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSuppCreditOrderInvoiceDetailsList", map);

            System.out.println("getCreditOrderInvoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getSuppInvoiceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSuppInvoiceDetails", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetails List", sqlException);
        }

        return result;
    }

    public ArrayList getSuppInvoiceDetailExpenses(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSuppInvoiceDetailExpenses", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetailExpenses List", sqlException);
        }

        return result;
    }

//    public ArrayList insertShippingLineNoTemp(ArrayList shippingLineNoList, int userId) {
//        Map map = new HashMap();
//        int insertShippingLineNo = 0;
//        int deleteSBillTemp = 0;
//        int status = 0;
//        String tripId = "";
//        int existingContainers = 0;
//        ArrayList getSbillNoListTemp = new ArrayList();
//        TripTO tripTO = new TripTO();
//        try {
//            map.put("userId", userId);
//
//            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("trip.deleteShippingLineTemp", map);
//            System.out.println("deleteSBillTemp------" + deleteSBillTemp);
//
//            Iterator itr = shippingLineNoList.iterator();
//            while (itr.hasNext()) {
//                tripTO = (TripTO) itr.next();
//                map.put("containerNo", tripTO.getContainerNo());
//                existingContainers = (Integer) getSqlMapClientTemplate().queryForObject("trip.existingContainers", map);
//                System.out.println("existingContainers---" + existingContainers);
//                map.put("containerSize", tripTO.getContainerTypeName());
//                map.put("containerType", tripTO.getContainerTypeId());
//                map.put("shippingBillNo", tripTO.getShipingLineNo());
//                String shippingDate = tripTO.getShippingBillDate();
//                String gateInDate = tripTO.getGateInDate();
//                System.out.println("shippingDate---" + shippingDate);
//                System.out.println("gateInDate---" + gateInDate);
//                map.put("shippingBillDate", shippingDate);
//                map.put("gateInDate", gateInDate);
//                System.out.println("accTemp-----" + tripTO.getGateInDate());
//                map.put("requestNo", tripTO.getRequestNo());
//                map.put("vehicleNo", tripTO.getVehicleNo());
//                map.put("containerQty", tripTO.getContainerQty());
//                System.out.println("map value is:" + map);
//
//                if (existingContainers > 0) {
//                    tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus", map);
//                    System.out.println("map value is:tripIdtripId" + tripId);
//                    if (!"".equals(tripId) && tripId != null) {
//                        map.put("flag", "0");
//                        map.put("status", "Ready to add");
//                        insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
//                        System.out.println("insertShippingLineNo1--" + insertShippingLineNo);
//                    } else {
//                        tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus1", map);  // except Movemnet
//                        System.out.println("except Movemnet-----" + tripId);
//                        if (!"".equals(tripId) && tripId != null) {
//                            map.put("flag", "1");
//                            map.put("status", "Not Export Movement");
//                            insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
//                            System.out.println("insertShippingLineNo2--" + insertShippingLineNo);
//                        } else {
//                            tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus2", map);  // cancel GR
//                            System.out.println("cancel GR-----" + tripId);
//                            if (!"".equals(tripId) && tripId != null) {
//                                map.put("flag", "1");
//                                map.put("status", "cancelled Gr");
//                                insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
//                                System.out.println("insertShippingLineNo3--" + insertShippingLineNo);
//                            } else {
//                                tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus3", map);  // invoice already generated
//                                System.out.println("invoice already generated-----" + tripId);
//                                if (!"".equals(tripId) && tripId != null) {
//                                    map.put("flag", "1");
//                                    map.put("status", "invoice already generated");
//                                    insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
//                                    System.out.println("insertShippingLineNo4--" + insertShippingLineNo);
//                                } else {
//                                    tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus4", map);  // Sbill already exist
//                                    System.out.println("Sbill already exis----" + tripId);
//                                    if (!"".equals(tripId) && tripId != null) {
//                                        map.put("flag", "1");
//                                        map.put("status", "Sbill already exist");
//                                        insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
//                                        System.out.println("insertShippingLineNo5--" + insertShippingLineNo);
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//                } else {
//                    map.put("flag", "1");
//                    map.put("status", "Container Not exist");
//                    insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map); // Container Not exist
//                    System.out.println("insertShippingLineNo5--" + insertShippingLineNo);
//                }
//            }
//            getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSbillNoListTemp", map);
//            System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());
//
//            System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
//        }
//        return getSbillNoListTemp;
//    }
    public ArrayList getVehicleWiseBunkList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getVehicleWiseBunkList = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("ownership", tripTO.getOwnerShip());
            if ("0".equals(tripTO.getOwnerShip())) {
                getVehicleWiseBunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAllBunkList", map);
                System.out.println("getVehicleWiseBunkList size=" + getVehicleWiseBunkList.size());
            } else {
                getVehicleWiseBunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleWiseBunkList", map);
                System.out.println("getVehicleWiseBunkList size=" + getVehicleWiseBunkList.size());

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return getVehicleWiseBunkList;
    }

    public ArrayList insertOrderUpload(ArrayList shippingLineNoList, int userId) {
        Map map = new HashMap();
        int deleteSBillTemp = 0;
        int status = 0;
        int insertOrder = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        TripTO tripTO = new TripTO();

        try {

            map.put("userId", userId);
            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("trip.deleteOrderUpload", map);
            System.out.println("deleteSBillTemp------" + deleteSBillTemp);

            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("customerName", tripTO.getCustomerName());
                map.put("billingParty", tripTO.getBillingParty());
                map.put("consignee", tripTO.getConsigneeName());

                map.put("vehicleType", tripTO.getVehicleType());
                System.out.println("vehicleType----xxxx---------------------" + tripTO.getVehicleType());
                map.put("iso_dso", tripTO.getIso());
                map.put("fs_ms_repo", tripTO.getIsRepo());
                map.put("handling", tripTO.getHandling());
                map.put("cargo", tripTO.getCargo());

                map.put("pickupLocation", tripTO.getPickupLocation());
                map.put("point1", tripTO.getPoint1Name());
                map.put("point2", tripTO.getPoint2Name());
                map.put("point3", tripTO.getPoint3Name());
                map.put("point4", tripTO.getPoint4Name());
                map.put("toLocation", tripTO.getToCityName());
                map.put("finalPoint", tripTO.getToCityName());

                map.put("todays_booking_20", tripTO.getTodayBooking20());
                map.put("todays_booking_40", tripTO.getTodayBooking40());
                map.put("flag", "0");
                int Id = 0;
                String Id2 = null;
                String Id3 = null;
                String Id4 = "";
                String Id5 = "";
                String Id6 = "";
                String Id7 = "";
                String Id8 = "";
                String Id9 = "";
                String custId = "";
                String customerId = "0";
                String contractId = "0";
                String FreightChrg20 = "0";
                String FreightChrg40 = "0";

                map.put("vehicleTypeId", "%" + tripTO.getVehicleType() + "%");
                System.out.println("map------------------------------" + map);
                String vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderVehicleTypeId", map);
                map.put("vehicleTypeIds", vehicleTypeId);
                System.out.println("vehicleTypeId----------------------" + vehicleTypeId);
                if (vehicleTypeId == null) {
                    map.put("flag", "7");
                    map.put("vehicleTypeId", 0);
                } else {
                    map.put("vehicleTypeId", vehicleTypeId);
                }

                map.put("movementType", tripTO.getMovementType());
                String movementypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderMovementTypeId", map);
                System.out.println("movementypeId : " + movementypeId);

                if (movementypeId == null) {
                    map.put("flag", "8");
                    map.put("movementypeId", 0);
                } else {
                    map.put("movementypeId", movementypeId);
                    String loadType = "";
                    if (movementypeId.equalsIgnoreCase("1") || movementypeId.equalsIgnoreCase("2")) {
                        loadType = "2";
                    } else {
                        loadType = "1";
                    }
                    tripTO.setLoadType(loadType);
                }

                map.put("shippingLine", tripTO.getShipingLineNo());
                String linerId = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderLinerId", map);
                System.out.println("linerId : " + linerId);

                if (linerId == null) {
                    map.put("flag", "9");
                    map.put("linerId", 0);
                } else {
                    map.put("linerId", linerId);
                }

                map.put("custName", tripTO.getCustomerName());
                custId = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerStatus", map);

                if (custId == null) {
                    map.put("flag", "1");//customer name is not available
                } else {
                    customerId = custId;
                    Id = Integer.parseInt(custId);
                    map.put("custName", tripTO.getBillingParty());
                    Id2 = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerStatus", map);
                    System.out.println("billingpartyId===========" + Id2);

                    if (Id2 == null) {
                        map.put("flag", "2");// billing party not matches to customer                                           
                        map.put("billingPartyId", "0");
                    } else {
                        map.put("billingPartyId", Id2);
                        map.put("custName", tripTO.getConsigneeName());
                        Id3 = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerStatus", map);
                        System.out.println("consigneeId" + Id3);

                        if (Id3 == null) {
                            map.put("flag", "3");
                            map.put("consigneeId", "0");

                        } else {
                            map.put("consigneeId", Id3);
                            map.put("location", tripTO.getPickupLocation());
                            Id4 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                            System.out.println("pickuplocationid :" + Id4);

                            if (Id4 == null) {
                                map.put("flag", "4");
                                Id4 = "";
                            } else {
                                map.put("pickupLocationId", Id4);
                                map.put("location", tripTO.getPoint1Name());
                                Id5 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                                System.out.println("point1 location :" + Id5);

                                if (Id5 == null) {
                                    map.put("flag", "5");// drop location was not registered(there is no contract in these routes)
                                    Id5 = "";
                                } else {
                                    map.put("point1Id", Id5);
                                    if (!tripTO.getPoint2Name().equals("")) {
                                        map.put("location", tripTO.getPoint2Name());
                                        Id6 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                                    } else {
                                        Id6 = "";
                                    }

                                    if (!tripTO.getPoint3Name().equals("")) {
                                        map.put("location", tripTO.getPoint3Name());
                                        Id7 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                                    } else {
                                        Id7 = "";
                                    }

                                    if (!tripTO.getPoint4Name().equals("")) {
                                        map.put("location", tripTO.getPoint4Name());
                                        Id8 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                                    } else {
                                        Id8 = "";
                                    }

                                    if (!tripTO.getToCityName().equals("")) {
                                        map.put("location", tripTO.getToCityName());
                                        Id9 = (String) getSqlMapClientTemplate().queryForObject("trip.getFromCityId", map);
                                    } else {
                                        Id9 = "";
                                    }

                                    map.put("point2d", Id6);
                                    map.put("point3Id", Id7);
                                    map.put("point4Id", Id8);
                                    map.put("finalPointId", Id9);

                                    map.put("pickupLocation", tripTO.getPickupLocation());
                                    map.put("point1", tripTO.getPoint1Name());
                                    map.put("point2", tripTO.getPoint2Name());
                                    map.put("point3", tripTO.getPoint3Name());
                                    map.put("point4", tripTO.getPoint4Name());
                                    map.put("finalPoint", tripTO.getToCityName());

                                    map.put("custId", Id);
                                    map.put("loadTypeId", tripTO.getLoadType());
                                    map.put("vehicleTypeId", vehicleTypeId);

                                    String Id20 = "0";
                                    String Id40 = "0";

                                    if (!tripTO.getTodayBooking20().equals("") && !tripTO.getTodayBooking20().equals("0")) {
                                        map.put("containerType", "1");
                                        System.out.println("map for Contract : " + map);
                                        String contrct20 = (String) getSqlMapClientTemplate().queryForObject("trip.getContractId", map);
                                        System.out.println("20 feet contract value" + contrct20);

                                        if (contrct20 != null) {
                                            String[] cntr = contrct20.split("~");
                                            Id20 = cntr[0];
                                            FreightChrg20 = cntr[1];
                                            contractId = Id20;
                                        } else if (contrct20 == null) {
                                            map.put("flag", "6");
                                        }
                                    }

                                    if (!tripTO.getTodayBooking40().equals("") && !tripTO.getTodayBooking40().equals("0")) {
                                        map.put("containerType", "2");
                                        System.out.println("map for Contract : " + map);
                                        String contrct40 = (String) getSqlMapClientTemplate().queryForObject("trip.getContractId", map);
                                        System.out.println("40 feet contract value" + contrct40);

                                        if (contrct40 != null) {
                                            String[] cntr = contrct40.split("~");
                                            Id40 = cntr[0];
                                            FreightChrg40 = cntr[1];
                                            contractId = Id40;
                                        } else if (contrct40 == null) {
                                            map.put("flag", "10");
                                        }

                                    }

                                    if (vehicleTypeId == null) {
                                        map.put("flag", "7");
                                    } else if (movementypeId == null) {
                                        map.put("flag", "8");
                                    } else if (linerId == null) {
                                        map.put("flag", "9");
                                    }
                                }
                            }
                        }
                    }
                }

                map.put("custId", customerId);
                map.put("contractId", contractId);
                map.put("freight20", FreightChrg20);
                map.put("freight40", FreightChrg40);

                if (Id4.equals("")) {
                    map.put("pickupLocationId", 0);
                }
                if (Id5.equals("")) {
                    map.put("point1d", 0);
                }
                if (Id6.equals("")) {
                    map.put("point2d", 0);
                }
                if (Id7.equals("")) {
                    map.put("point3Id", 0);
                }
                if (Id8.equals("")) {
                    map.put("point4Id", 0);
                }
                if (Id9.equals("")) {
                    map.put("finalPointId", 0);
                }

                System.out.println("map value is: after flag set" + map);
                insertOrder = (Integer) getSqlMapClientTemplate().update("trip.insertOrderUpload", map);
                System.out.println("insertOrder--" + insertOrder);
            }

            if (insertOrder > 0) {
                getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getinsertOrderUpload", map);
                System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public ArrayList getOrderUploadData(int userId) {
        Map map = new HashMap();
        ArrayList getOrderUploadData = new ArrayList();
        try {
            map.put("userId", userId);
            getOrderUploadData = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getValidOrderUpload", map);
            System.out.println("getOrderUploadData size=" + getOrderUploadData.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return getOrderUploadData;
    }

    public String checkInvoiceType(String invoiceId, String invoiceType) {
        Map map = new HashMap();
        String checkInvoiceType = "";
        try {
            map.put("invoiceId", invoiceId);
            map.put("invoiceType", invoiceType);

            System.out.println("map:" + map);

            checkInvoiceType = (String) getSqlMapClientTemplate().queryForObject("trip.checkInvoiceType", map);
            System.out.println("checkInvoiceType----" + checkInvoiceType);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("chechInvoiceType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "chechInvoiceType List", sqlException);
        }
        return checkInvoiceType;
    }

    public ArrayList getOrderEInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("invoiceType", tripTO.getInvoiceType());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            if ("1".equals(tripTO.getInvoiceType())) {
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderEInvoiceHeader", map);
                System.out.println("getOrderEInvoiceHeader=" + result.size());
            } else if ("2".equals(tripTO.getInvoiceType())) {
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderESuppInvoiceHeader", map);
                System.out.println("getOrderESuppInvoiceHeader=" + result.size());
            } else if ("3".equals(tripTO.getInvoiceType())) {
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderECreditInvoiceHeader", map);
                System.out.println("getOrderECreditInvoiceHeader=" + result.size());
            } else if ("4".equals(tripTO.getInvoiceType())) {
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderECreditSuppInvoiceHeader", map);
                System.out.println("getOrderECreditSuppInvoiceHeader=" + result.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList insertShippingLineNoTemp(ArrayList shippingLineNoList, int userId) {
        Map map = new HashMap();
        int insertShippingLineNo = 0;
        int deleteSBillTemp = 0;
        int status = 0;
        String tripId = "";
        String commodityCheck = "";
        int existingContainers = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);

            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("trip.deleteShippingLineTemp", map);
            System.out.println("deleteSBillTemp------" + deleteSBillTemp);
            System.out.println("shippingLineNoList.size()------" + shippingLineNoList.size());

            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("containerNo", tripTO.getContainerNo());
                existingContainers = (Integer) getSqlMapClientTemplate().queryForObject("trip.existingContainers", map);
                System.out.println("existingContainers---" + existingContainers);
                map.put("containerSize", tripTO.getContainerTypeName());
                map.put("containerType", tripTO.getContainerTypeId());
                map.put("shippingBillNo", tripTO.getShipingLineNo());
                String shippingDate = tripTO.getShippingBillDate();
                String gateInDate = tripTO.getGateInDate();
                System.out.println("shippingDate---" + shippingDate);
                System.out.println("gateInDate---" + gateInDate);
                map.put("shippingBillDate", shippingDate);
                map.put("gateInDate", gateInDate);
                System.out.println("accTemp-----" + tripTO.getGateInDate());
                map.put("requestNo", tripTO.getRequestNo());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("containerQty", tripTO.getContainerQty());
                map.put("commodityId", tripTO.getCommodityId());
                map.put("commodityName", tripTO.getCommodityName());
                System.out.println("map value is:" + map);
                String gstType = (String) getSqlMapClientTemplate().queryForObject("trip.getCommodityGstType", map); // 
                map.put("gstType", gstType);

                if (existingContainers > 0) {
                    tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus", map); // 
                    System.out.println("map value is:tripIdtripId" + tripId);
                    if (!"".equals(tripId) && tripId != null) {
                        map.put("flag", "0");
                        map.put("status", "Ready to add");
                        insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                        System.out.println("insertShippingLineNo1--" + insertShippingLineNo);
                        System.out.println("Ready to add--");
                    } else {
                        tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus1", map);  // except Movemnet
                        System.out.println("except Movemnet-----" + tripId);
                        if (!"".equals(tripId) && tripId != null) {
                            map.put("flag", "1");
                            map.put("status", "Not Export Movement");
                            insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                            System.out.println("Not Export Movement--");
                            System.out.println("insertShippingLineNo2--" + insertShippingLineNo);
                        } else {
                            tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus2", map);  // cancel GR
                            System.out.println("cancel GR-----" + tripId);
                            if (!"".equals(tripId) && tripId != null) {
                                map.put("flag", "1");
                                map.put("status", "cancelled Gr");
                                insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                                System.out.println("cancelled Gr--");
                                System.out.println("insertShippingLineNo3--" + insertShippingLineNo);
                            } else {
                                tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus3", map);  // invoice already generated
                                System.out.println("invoice already generated-----" + tripId);
                                if (!"".equals(tripId) && tripId != null) {
                                    map.put("flag", "1");
                                    map.put("status", "invoice already generated");
                                    insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                                    System.out.println("invoice already generated--");
                                    System.out.println("insertShippingLineNo4--" + insertShippingLineNo);
                                } else {
                                    tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus4", map);  // Sbill already exist
                                    System.out.println("Sbill already exis----" + tripId);
                                    if (!"".equals(tripId) && tripId != null) {
                                        map.put("flag", "1");
                                        map.put("status", "Sbill already exist");
                                        insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                                        System.out.println("insertShippingLineNo5--" + insertShippingLineNo);
                                        System.out.println("Sbill already exist--");
                                    } else {
                                        tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getShippingBillNoStatus5", map);  // Commodity not exist
                                        System.out.println("Commodity Not Exist----" + tripId);
                                        if (!"".equals(tripId) && tripId != null) {
                                            map.put("flag", "1");
                                            map.put("status", "Commodity Not Exist");
                                            insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                                            System.out.println("insertShippingLineNo6--" + insertShippingLineNo);
                                            System.out.println("Commodity Not Exist--");
                                        } else {
                                            map.put("flag", "1");
                                            map.put("status", "Mismatch found");
                                            insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
                                            System.out.println("insertShippingLineNo7--" + insertShippingLineNo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    map.put("flag", "1");
                    map.put("status", "Container Not exist");
                    insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map); // Container Not exist
                    System.out.println("insertShippingLineNo8--" + insertShippingLineNo);
                }
            }
            getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSbillNoListTemp", map);
            System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());

            System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public int insertShippingLineNoList(ArrayList shippingLineNoList, TripTO tripTO1, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertShippingLineNo = 0;
        int status = 0;
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);
            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("containerNo", tripTO.getContainerNo());
                map.put("containerSize", tripTO.getContainerTypeId());
                map.put("containerType", tripTO.getContainerTypeName());
                map.put("shippingBillNo", tripTO.getShipingLineNo());
                String shippingDate = tripTO.getShippingBillDate();
                String gateInDate = tripTO.getGateInDate();
                System.out.println("shippingDate---" + shippingDate);
                System.out.println("gateInDate---" + gateInDate);
                map.put("shippingBillDate", shippingDate);
                map.put("gateInDate", gateInDate);
                System.out.println("accTemp-----" + tripTO.getGateInDate());
                map.put("requestNo", tripTO.getRequestNo());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("containerQty", tripTO.getContainerQty());
                map.put("commodityId", tripTO.getCommodityId());
                map.put("commodityName", tripTO.getCommodityName());
                System.out.println("map AAAA value is:" + map);

                if ("0".equals(tripTO.getFlag())) {
                    int shippingNo = (Integer) session.queryForObject("trip.getContainerExistsForShippingBillNo", map);
                    System.out.println("shippingNo----" + shippingNo);

                    if (shippingNo == 0) {

                        String tripId = (String) session.queryForObject("trip.getTripIdForShippingBillNo", map);
                        System.out.println("map value is:tripIdtripId---------------------------" + tripId);
                        if (!"".equals(tripId) && tripId != null) {
                            String[] temp = tripId.split("~");
                            String[] tempTripIds = temp[0].split(",");
                            System.out.println("map value is:tempTripIds---------------------------" + tempTripIds);

                            for (int i = 0; i < tempTripIds.length; i++) {

                                map.put("tripId", tempTripIds[i]);
                                map.put("statusCount", temp[1]);
                                System.out.println("map value is:" + map);
                                insertShippingLineNo += (Integer) session.update("trip.insertShippingLineNoList", map);
                                System.out.println("insertShippingLineNo value is:" + status);
                                status = (Integer) session.update("trip.updateShippingBillNoForTrip", map);
                                System.out.println("updateShippingBillNoForTrip value is:" + status);

                                status = (Integer) session.update("trip.updateTripMasterforShippingLineNo", map);
                                System.out.println("updateTripMasterforShippingLineNo value is:" + status);

                                status = (Integer) session.update("trip.updateTripArticleforShippingLineNo", map);
                                System.out.println("updateTripArticleforShippingLineNo value is:" + status);

                                if (status == 1) {
                                    status = (Integer) session.update("trip.updateShippingBillNoCount", map);
                                    System.out.println("updateShippingBillNoCount value is:" + status);
                                }
                            }
                        }
                        System.out.println(" sttausiss" + status);
                        status = 0;
                    }
                }
            }
            System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return insertShippingLineNo;
    }

    public ArrayList insertShippingLineNoTempNew(ArrayList shippingLineNoList, int userId) {
        Map map = new HashMap();
        int insertShippingLineNo = 0;
        int deleteSBillTemp = 0;
        int status = 0;
        String tripId = "";
        String commodityCheck = "";
        int existingContainers = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);

            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("trip.deleteShippingLineTemp", map);
            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("containerNo", tripTO.getContainerNo());
                map.put("containerSize", tripTO.getContainerTypeName());
                map.put("containerType", tripTO.getContainerTypeId());
                map.put("shippingBillNo", tripTO.getShipingLineNo());
                String shippingDate = tripTO.getShippingBillDate();
                String gateInDate = tripTO.getGateInDate();
                map.put("shippingBillDate", shippingDate);
                map.put("gateInDate", gateInDate);
                map.put("requestNo", tripTO.getRequestNo());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("containerQty", tripTO.getContainerQty());
                map.put("commodityId", tripTO.getCommodityId());
                map.put("commodityName", tripTO.getCommodityName());
                map.put("gstType", 'Y');
                System.out.println("hello----------------------------------" + map);
                map.put("flag", "0");
                map.put("status", "Ready to add");
                insertShippingLineNo = (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineTemp", map);
            }
            getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSbillNoListTemp", map);
            System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());

            System.out.println("insertShippingLineNoList=" + insertShippingLineNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public int updateShipBillReq(String fromDate, String toDate, String movementType, int userId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("movementType", movementType);
        map.put("userId", userId);
        System.out.println("the updateShipBillReq-----------------------" + map);
        int status = 0;
        try {
            int check = (Integer) getSqlMapClientTemplate().queryForObject("trip.getExistingRequest", map);
            System.out.println("check--------------" + check);
            if (check == 0) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateShipBillReq", map);
                System.out.println("updateTransporter=" + status);
                status = 1;
            } else {
                status = 3;
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return status;
    }

    public ArrayList getShipBillReqList() {
        Map map = new HashMap();

        ArrayList getShipBillReqList = new ArrayList();
        try {
            getShipBillReqList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getShipBillReqList", map);
            System.out.println("getShipBillReqList----------" + getShipBillReqList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return getShipBillReqList;
    }

    public ArrayList getShippingListForReq(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList getShippingListForReq = new ArrayList();
        try {
            map.put("requestNo", tripTO.getRequestNo());
            getShippingListForReq = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getShippingListForReq", map);
            System.out.println("getShippingListForReq----------" + getShippingListForReq.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return getShippingListForReq;
    }

    public ArrayList getShippingListForReqErr(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList getShippingListForReqErr = new ArrayList();
        try {
            map.put("requestNo", tripTO.getRequestNo());
            getShippingListForReqErr = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getShippingListForReqErr", map);
            System.out.println("getShippingListForReqErr----------" + getShippingListForReqErr.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return getShippingListForReqErr;
    }

    public int invoiceResend(TripTO tripTO1) {
        Map map = new HashMap();
        int invoiceResend = 0;
        int status = 0;
        int check = 0;
        TripTO tripTO = new TripTO();
        try {
            map.put("fromDate", tripTO1.getFromDate());
            map.put("toDate", tripTO1.getToDate());
            map.put("customerId", tripTO1.getCustomerId());
            map.put("userId", tripTO1.getUserId());
            System.out.println("map AAAA value is:" + map);

            check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkMailDetailsExist", map);
            if (check == 0) {

                invoiceResend = (Integer) getSqlMapClientTemplate().update("trip.invoiceResend", map);
                System.out.println("insertShippingLineNo value is:" + invoiceResend);
                System.out.println(" sttausiss" + status);
            } else {
                invoiceResend = 111;
            }
            status = 0;
            System.out.println("insertShippingLineNoList=" + invoiceResend);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return invoiceResend;
    }

    public ArrayList getResendMailListDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList getResendMailListDetails = new ArrayList();
        try {
            map.put("requestNo", tripTO.getRequestNo());
            getResendMailListDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getResendMailListDetails", map);
            System.out.println("getResendMailListDetails----------" + getResendMailListDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return getResendMailListDetails;
    }
    
     public int getNOfBillInHeader(String[] tripId) {
        Map map = new HashMap();
        int getNOfBillInHeader = 0;
        try {
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getNOfBillInHeader map" + map);
            getNOfBillInHeader = (Integer) getSqlMapClientTemplate().queryForObject("trip.getNOfBillInHeader", map);
            System.out.println("getNOfBillInHeader size is ::::" + getNOfBillInHeader);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getNOfBillInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getNOfBillInHeader", sqlException);
        }
        return getNOfBillInHeader;
    }
     public int getGstTypesInTrip(String[] tripId) {
        Map map = new HashMap();
        int getGstTypesInTrip = 0;
        try {
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getNOfBillInHeader map" + map);
            getGstTypesInTrip = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGstTypesInTrip", map);
            System.out.println("getGstTypesInTrip size is ::::" + getGstTypesInTrip);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getNOfBillInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getNOfBillInHeader", sqlException);
        }
        return getGstTypesInTrip;
    }

      public int updateExpensiveItem(String expenseId, String expenseDate, String expenseType, String expenseAmountvalue, String totalExpenseAmount, int userId,String expenseRemarksEdit) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("expenseId", expenseId);
            map.put("expenseDate", expenseDate);
            map.put("expenseType", expenseType);
            map.put("expenseRemarksEdit", expenseRemarksEdit);
            if (expenseAmountvalue != null && !"".equals(expenseAmountvalue)) {
            map.put("expenseAmountvalue", expenseAmountvalue);
            }else{
                  map.put("expenseAmountvalue", 0);
            }
            if (totalExpenseAmount != null && !"".equals(totalExpenseAmount)) {
            map.put("totalExpenseAmount", expenseAmountvalue);
            }else{
                  map.put("totalExpenseAmount", 0);
            }
          
            map.put("userId", userId);
            status = Integer.valueOf(getSqlMapClientTemplate().update("trip.updateExpenseItem", map)).intValue();
//            if (status == 0) {
//                status = Integer.valueOf(getSqlMapClientTemplate().update("purchase.insertVendorItemPrice", map)).intValue();
//            }


        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doMprApprove Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "doMprApprove", sqlException);
        }
        return status;
    }
      
       public ArrayList getEmailCount() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getEmailCount = new ArrayList();
        try {
            getEmailCount = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailCount", map);
System.out.println("getEmailCount==========================================================>>>>>"+getEmailCount.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString()); 
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return getEmailCount;
    }


}
