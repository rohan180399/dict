<%-- 
    Document   : viewWorkOrderSubmission
    Created on : 17 Aug, 2023, 11:49:37 AM
    Author     : Dell
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Primary Billing</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Primary Billing</a></li>
            <li class="active">View Bill</li>
        </ol>
    </div>
</div>
            <script>
               function submitPage() {
        document.workOrder.action = "/throttle/saveWorkOrderDetailsForSubmit.do";
        document.workOrder.submit();
    }
                </script>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="sorter.size(10);setTollCost();">
        <form name="workOrder"  method="post">
            <c:if test="${viewWorkOrderSubmission != null}">
            <table class="table table-info mb30 table-hover" style="width:100%">
                <c:forEach items="${viewWorkOrderSubmission}" var="rl">
                <tr>
                    <td  colspan="12" style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">View Bill Details</td>
                </tr>
                <tr>
                    <td>
                        Work Order Number
                    </td>
                    <td>
                        <input type="text" id="workOrderNumber" name="workOrderNumber" class="form-control" style="width:240px;height:40px;" value="<c:out value="${rl.workOrderNumber}"/>">
                        <input type="hidden" id="invoiceId" name="invoiceId" value="<c:out value="${invoiceId}"/>">
                    </td>
                
                    <td >Bill.No</td>
                    <td ><c:out value="${rl.invoiceCode}"/></td>
                    </tr>
                <tr>
                    <td >Bill Date</td>
                    <td >
                        <c:out value="${rl.createddate}"/>
                    </td>
                
                    <td >Billing Party</td>
                    <td ><c:out value="${rl.billingParty}"/></td>
                    </tr>
                <tr>
                    <td >Customer</td>
                    <td ><c:out value="${rl.invoicecustomer}"/></td>
               
                    <td >GR Nos</td>
                    <td ><c:out value="${rl.grNo}"/></td>
                     </tr>
                <tr>
                    <td >Container Nos</td>
                    <td ><c:out value="${rl.containerNo}"/></td>
                
                    <td >Amount</td>
                    <td ><c:out value="${rl.grandTotal}"/></td>
                    
                </tr>
                         <tr>
                            <td colspan="4" align="center">
                                <input type="button" class="btn btn-info"   value="Save" onclick="submitPage();">&nbsp;&nbsp;
                               
                        </tr>
               
               
                
                </c:forEach>
            </table>
            </c:if>
           
            <br>
            <br>
            

            <br>
            <br>
           
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

