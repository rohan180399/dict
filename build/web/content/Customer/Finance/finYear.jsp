
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script>
    function submitPage()
    {
        if (textValidation(document.enter.finYear, 'financial Year')) {
            return;
        }
        if (textValidation(document.enter.fromDate, 'FromDate')) {
            return;
        }
        if (textValidation(document.enter.toDate, 'ToDate')) {
            return;
        }
        if (textValidation(document.enter.description, 'description')) {
            return;
        }

        document.enter.action = '/throttle/saveFinYear.do';
        document.enter.submit();
    }

    function setValues(sno, accountYear, fromDate, toDate, discription, statusName, accountYearId) {

        var count = parseInt(document.getElementById("count").value);

        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }

        if (statusName == 'Active' || statusName == 'active') {
            statusName = 'Y';
        } else {
            statusName = 'N';
        }

        document.getElementById("finYear").value = accountYear;
        document.getElementById("fromDate").value = fromDate;
        document.getElementById("toDate").value = toDate;
        document.getElementById("description").value = discription;
        document.getElementById("active_ind").value = statusName;
        document.getElementById("accountYearId").value = accountYearId;

    }
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


    var httpRequest;
    function checkFinYear() {

        var finYear = document.getElementById('finYear').value;
        var url = '/throttle/checkFinYear.do?finYear=' + finYear;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function () {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#finyearname").text('Please Check Financial Year  :' + val + ' is Already Exists');
                    document.getElementById("finYear").value = "";
                    document.getElementById("finYear").focus();
                } else {
                    $("#nameStatus").hide();
                    $("#finyearname").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>
<style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageFinancialYear"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageFinancialYear"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form method="post" name="enter" class="form-horizontal form-bordered">
                     <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover">
                         <tr height="30" id="index">
                             <td colspan="6"  style="background-color:#5BC0DE;"><b><spring:message code="finance.label.AddFinancialYear"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                        <!--                <tr height="30">
                                         <td colspan="2" class="contenthead">Financial Year</Td>&nbsp;&nbsp;&nbsp;                 
                                        </tr>-->
                        <tr>
                            <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="finyearname" style="color: red"></label></td>
                        </tr>
                        <tr height="30">
                            <td><font color="red">*</font><spring:message code="finance.label.FinancialYear"  text="default text"/></td>
                            <td><input name="finYear" id="finYear" type="text" class="form-control" value=""  onchange="checkFinYear()" style="width:260px;height:40px;"></td>
<!--                        </tr>
                        <tr height="30">-->
                            <td ><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                            <td> <input name="fromDate" id="fromDate" type="textbox" class="datepicker form-control" value="" style="width:260px;height:40px;"></td>
<!--                        </tr>
                        <tr height="30">-->
                            <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                            <td> <input type="textbox" name="toDate" id="toDate"   value="" style="width:260px;height:40px;" class="datepicker form-control"></td>
                        </tr>
                        <tr height="30">
                            <td ><font color="red">*</font><spring:message code="finance.label.Description"  text="default text"/></td>
                            <td>
                                <input id="description" name="description" type="text" class="form-control" value=""   style="width:260px;height:40px;">
                                <input name="accountYearId" id="accountYearId" type="hidden" class="form-control" value=""  >
                            </td>
<!--                        </tr>
                        <tr height="30">-->
                            <td><font color="red">*</font><spring:message code="finance.label.Status"  text="default text"/></td>
                            <td>
                                <select name="active_ind" id="active_ind" class="form-control" style="width:260px;height:40px;">
                                    <option value="Y"><spring:message code="finance.label.Active"  text="default text"/></option>
                                    <option value="N"><spring:message code="finance.label.InActive"  text="default text"/></option>
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                  
                    <center> 
                        <input type="button" class="btn btn-success" value="<spring:message code="finance.label.Save"  text="default text"/>" onclick="submitPage();" style="width:100px;height:35px;"/>
                        &emsp;<input type="reset"  value="<spring:message code="finance.label.Clear"  text="default text"/>" class="btn btn-success" style="width:100px;height:35px;">
                    </center>
                    <br>

                    <c:if test = "${financeYearList != null}" >
                       <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.FinancialYear"  text="default text"/></th>
                            <th><spring:message code="finance.label.FromDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.ToDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.Description"  text="default text"/></th>
                            <th><spring:message code="finance.label.Status"  text="default text"/></th>
                            <th><spring:message code="finance.label.Check"  text="default text"/></th>
                            </thead>
                            <tbody>

                                <%                        int index = 0;
                                    int sno = 0;
                                %>
                                <c:forEach items="${financeYearList}" var="year">
                                    <% sno++;%>
                                    <tr>
                                        <td  align="left"> <%= index + 1%> </td>
                                        <td align="left"> <c:out value="${year.accountYear}" /></td>
                                        <td align="left"> <c:out value="${year.fromDate}" /></td>
                                        <td align="left"> <c:out value="${year.toDate}" /></td>
                                        <td  align="left"> <c:out value="${year.discription}"/> </td>
                                        <td  align="left"> <c:out value="${year.statusName}"/> </td>                                                       
                                        <td  align="left">

                                            <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%=sno%>, '<c:out value="${year.accountYear}" />', '<c:out value="${year.fromDate}"/>', '<c:out value="${year.toDate}"/>', '<c:out value="${year.discription}"/>', '<c:out value="${year.statusName}"/>', '<c:out value="${year.accountYearId}"/>');"/>

                                        </td>                              
                                    </tr>
                                    <% index++;%>
                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                            </table>
                        </c:if>
                    <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
                    
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

