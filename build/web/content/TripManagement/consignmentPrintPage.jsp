<%--
    Document   : consignmentPrintPage
    Created on : Mar 26, 2013, 11:12:08 PM
    Author     : Entitle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Consignment Print</title>
    </head>

    <script>



        function print(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }


    </script>


    <body>

        <%
                    String[] OrderNo;
                    String[] Bags;
                    String[] Tonnage;
                    String[] RouteName;
                    String[] TripRevenueOne;
                    String[] BillStatus;
                    String[] SelectedProductName;
                    String[] Packing;
                    String[] InvoiceNo;
                    String[] CustName;
                    OperationTO operationTo = new OperationTO();
                    ArrayList tripDetailsAL = (ArrayList) request.getAttribute("consignmentTripDetails");


                    if (tripDetailsAL != null && tripDetailsAL.size() > 0) {
                        operationTo = (OperationTO) tripDetailsAL.get(0);

                        String orderNo = operationTo.getOrderNo();
                        OrderNo = orderNo.split(",");
                        String bags = operationTo.getBags();
                        Bags = bags.split(",");
                        String tonnage = operationTo.getTonnage();
                        Tonnage = tonnage.split(",");

                        String custName = operationTo.getCustName();
                        CustName = custName.split(",");

                        String routeVal = operationTo.getSelectedRouteName();
                        RouteName = routeVal.split(",");

                        String tripRevenueOne = operationTo.getTripRevenueOne();
                        TripRevenueOne = tripRevenueOne.split(",");

                        String billStatus = operationTo.getSelectedBillStatus();
                        BillStatus = billStatus.split(",");

                        String packing = operationTo.getSelectedPacking();
                        Packing = packing.split(",");

                        String sproductName = operationTo.getSelectedProductName();
                        SelectedProductName = sproductName.split(",");
                        
                        String invoiceNo = operationTo.getInvoiceNo();
                        InvoiceNo = invoiceNo.split(",");

                        for (int i = 0; i < OrderNo.length; i++) {
                            
                            System.out.println("OrderNo[i] = " + OrderNo[i]);
                            System.out.println("Bags[i] = " + Bags[i]);
                            System.out.println("Tonnage[i] = " + Tonnage[i]);
                            System.out.println("CustName[i] = " + CustName[i]);
                           // System.out.println("InvoiceNo[i] = " + InvoiceNo[i]);





        %>
        <form name="suggestedPS" method="post">
            <div id="print<%= i%>" >

                <br>
                <br>
                <br>
                <table width="740" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                    <c:if test="${consignmentTripDetails != null}">
                        <c:forEach items="${consignmentTripDetails}" var="td">
                            <c:set var="routeName" value="${td.routeName}"/>
                            <c:set var="tolocation" value="${td.tolocation}"/>
                            <c:set var="tripVehicleId" value="${td.tripVehicleId}"/>
                            <c:set var="depHours" value="${td.depHours}"/>
                            <c:set var="depMints" value="${td.depMints}"/>
                            <c:set var="tripDate" value="${td.tripDate}"/>
                            <c:set var="tripdateP" value="${td.tripdateP}"/>
                            <c:set var="tripSheetId" value="${td.tripSheetId}"/>
                            <c:set var="custId" value="${td.custId}"/>
                            <c:set var="custName" value="${td.custName}"/>
                            <c:set var="orderNumber" value="${td.orderNumber}"/>
                            <c:set var="productId" value="${td.productId}"/>
                            <c:set var="bags" value="${td.bags}"/>
                            <c:set var="tonnage" value="${td.tonnage}"/>
                            <c:set var="totalBags" value="${td.totalBags}"/>
                            <c:set var="totalTonnage" value="${td.totalTonnage}"/>
                            <c:set var="tripTotalKms" value="${td.tripTotalKms}"/>
                            <c:set var="GPSKm" value="${td.GPSKm}"/>
                            <c:set var="billStatus" value="${td.billStatus}"/>
                            <c:set var="tripTotalLitres" value="${td.tripTotalLitres}"/>
                            <c:set var="tripTotalExpenses" value="${td.tripTotalExpenses}"/>
                            <c:set var="tripTotalAllowances" value="${td.tripTotalAllowances}"/>
                            <c:set var="tripFuelAmount" value="${td.tripFuelAmount}"/>
                            <c:set var="tripBalanceAmount" value="${td.tripBalanceAmount}"/>
                            <c:set var="gpno" value="${td.gpno}"/>
                            <c:set var="invoiceNo" value="${td.invoiceNo}"/>
                            <c:set var="productName" value="${td.productName}"/>
                            <c:set var="regno" value="${td.regno}"/>
                            <c:set var="revenue" value="${td.revenue}"/>
                        </c:forEach>
                    </c:if>

                    <tr>
                        <td height="190" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>

                        <td width="50%"><span style="padding-top:10px; display:block; padding-bottom:10px; padding-left:130px; vertical-align:middle;"><c:out value="${tripDate}"/></span></td>
                        <td width="50%"><span style="padding-top:3px; display:block; padding-bottom:3px; padding-right:20px; text-align:right; vertical-align:middle;"><span style="font-size:14px; "><c:out value="${tripSheetId}"/><br><c:out value="${tripDate}"/></span></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center; ">&nbsp;</td>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center;">&nbsp;</td>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center;"><c:out value="${tripTotalLitres}"/></td>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center;"><c:out value="${tripTotalAllowances}"/></td>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center;"><c:out value="${tripTotalKms}"/></td>
                                    <td width="16%" height="35" style="padding-top:15px; text-align:center;">0</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="115" style="text-align:left; vertical-align:middle; padding-left:15px;"><p><%=CustName[i]%><br><%=RouteName[i]%></p></td>
                        <%--<td height="115" style="text-align:left; vertical-align:middle; padding-left:15px;"><p><c:out value="${custName}"/><br><c:out value="${tolocation}"/></p></td>--%>
                        <td height="115" style="text-align:left; vertical-align:middle; padding-left:15px;"><p><%=RouteName[i]%></p></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
                                <tr>
                                    <td colspan="7" height="25"><span style="padding-left:60px; "><c:out value="${gpno}"/>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<%=SelectedProductName[i]%>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <%=Packing[i]%></span></td>
                                </tr>
                                <tr>
                                    <td width="12%" height="35" style="padding-top:25px; text-align:center; font-size:12px; "><%=InvoiceNo[i]%></td>
                                    <td width="12%" height="35" style="padding-top:25px; text-align:center; font-size:12px; "><%=OrderNo[i]%></td>
                                    <td width="14%" height="35" style="padding-top:25px; text-align:center; font-size:16px;"><%=Bags[i]%></td>
                                    <td width="14%" height="35" style="padding-top:25px; text-align:center; font-size:12px;"><c:out value="${regno}"/></td>
                                    <td width="14%" height="35" style="padding-top:25px; text-align:center; font-size:12px;">&nbsp;</td>
                                    <td width="14%" height="35" style="padding-top:25px; text-align:center; font-size:12px;"><%=Tonnage[i]%></td>
                                    <% String BS = BillStatus[i]; 
                                    if(BS.equals("Paid")){
                                     %>
                                            <td width="12%" height="35" style="padding-top:25px; text-align:center; font-size:12px;">T0 BE BILLED</td>

                                    <%}else{%>

                                    <c:choose>
                                        <c:when test="${TripRevenueOne[i] > 0}">
                                            <td width="12%" height="35" style="padding-top:25px; text-align:center; font-size:12px;"><%=TripRevenueOne[i]%></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td width="12%" height="35" style="padding-top:25px; text-align:center; font-size:12px;"><c:out value="${revenue}"/> </td>
                                        </c:otherwise>
                                    </c:choose>

                                    <%}%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="55"><span style="padding-left:120px; font-size:16px;"><%=Bags[i]%></span></td>
                    </tr>
                    <tr>
                        <td style="height:130px; padding-left:120px; line-height:24px;  "><c:out value="${regno}"/><br></td>
                    </tr>
                </table>

            </div>
            <table align="center" width="850" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                <br>
                <br>
                <br>
                <br>
                <tr align="center">
                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i%>');" > &nbsp;
                    </td>
                </tr>
            </table>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

        <%}
                                }%>
    </body>
</html>
