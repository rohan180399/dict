<!DOCTYPE html>
<!--<html>-->
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script>
    function openLevel2Report(statusName, statusId, day) {
         document.nodReport.action = '/throttle/nodReportLevel2.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
         document.nodReport.submit();
    }
    
    function openLevel3Report(statusName, statusId, day) {
         document.nodReport.action = '/throttle/nodReportLevel3.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
         document.nodReport.submit();
    }
    
</script>

<body>
<center>
    <%@include file="/content/common/message.jsp"%>
</center>
<form name="nodReport"  method="post">

    <c:set var="zeroDayTotal" value="0"/>
    <c:set var="oneDayTotal" value="0"/>
    <c:set var="twoDayTotal" value="0"/>
    <c:set var="threeDayTotal" value="0"/>
    <c:set var="fourDayTotal" value="0"/>
    <c:set var="fiveDayTotal" value="0"/>
    <c:set var="sixDayTotal" value="0"/>
    <c:set var="sevenDayTotal" value="0"/>
    <c:set var="moreDayTotal" value="0"/>
    
    <c:set var="zeroDayTotals" value="0"/>
    <c:set var="oneDayTotals" value="0"/>
    <c:set var="twoDayTotals" value="0"/>
    <c:set var="threeDayTotals" value="0"/>
    <c:set var="fourDayTotals" value="0"/>
    <c:set var="fiveDayTotals" value="0"/>
    <c:set var="sixDayTotals" value="0"/>
    <c:set var="sevenDayTotals" value="0"/>
    <c:set var="moreDayTotals" value="0"/>

    <c:set var="orderCreatedTotal" value="0"/>
    <c:set var="tripCreatedTotal" value="0"/>
    <c:set var="tripFreezedTotal" value="0"/>
    <c:set var="tripStartTotal" value="0"/>
    <c:set var="tripEndTotal" value="0"/>
    <c:set var="tripClosureTotal" value="0"/>
    <c:set var="tripSettledTotal" value="0"/>
    <c:set var="billCreatedTotal" value="0"/>
    <c:set var="billSubmittedTotal" value="0"/>
    <c:set var="summaryTotal" value="0"/>

    <c:if test="${nodList != null}">

        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.NOD Report" text="NOD Report"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Report"/></a></li>
                    <li class=""><spring:message code="hrms.label.NOD Report" text="NOD Report"/></li>

                </ol>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <center><b>NOD Report</b></center>
                    <br>
                    <table class="table table-info mb30 table-bordered"  >
                        <thead >
                        <th >&emsp; &emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Status </th>
                        <th>0 Day</th>
                        <th>1 Day</th>
                        <th>2 Days</th>
                        <th>3 Days</th>
                        <th >4 Days</th>
                        <th>5 Days</th>
                        <th>6 Days</th>
                        <th>7 Days</th>
                        <th> >7 Days</th>
                        <th>Total</th>             
                        </thead>
                        
                        <tbody>
                            
                            <% int sno = 0;%>
                            <%
                            sno++;
                            String className = "text1";
                            if ((sno % 1) == 0) {
                            className = "text1";
                            } else {
                            className = "text2";
                            }
                            int checkBillType=0;
                            %>
                             <c:if test="${nodLists != null}">
                            <c:forEach items="${nodLists}" var="lists">
                                <tr>
                                    <td  bgcolor="#31DFD7" align="center" > <font color="#050404"><c:out value="${lists.statusName}"/></td>

                                    <td bgcolor="#15c6b7" align="center" > <a  href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 0);" >
                                            <font color="#050404"> <c:out value="${lists.zeroDay}"/>
                                            <c:set var="zeroDayTotals" value="${zeroDayTotal+lists.zeroDay}"/></a></td>
                                    <td bgcolor="#18baac" align="center" > <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 1);" >
                                            <font color="#050404">  <c:out value="${lists.oneDay}"/>
                                            <c:set var="oneDayTotals" value="${oneDayTotal+lists.oneDay}"/>
                                        </a> </td>
                                    <td bgcolor="#f9f900" align="center" > <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 2);" >
                                            <font color="#050404"> <c:out value="${lists.twoDay}"/>
                                            <c:set var="twoDayTotals" value="${twoDayTotal+list.twoDay}"/>
                                        </a> </td>
                                    <td bgcolor="#dbdb0f" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 3);" >
                                            <font color="#050404"> <c:out value="${lists.threeDay}"/> <c:set var="threeDayTotals" value="${threeDayTotal+list.threeDay}"/></a> </td>
                                    <td bgcolor="#afaf18" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 4);" >
                                            <font color="#050404">    <c:out value="${lists.fourDay}"/>  <c:set var="fourDayTotals" value="${fourDayTotal+lists.fourDay}"/> </a></td>
                                    <td bgcolor="#f91702" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 5);" >
                                            <font color="#050404">  <c:out value="${lists.fiveDay}"/>  <c:set var="fiveDayTotals" value="${fiveDayTotal+lists.fiveDay}"/> </a></td>
                                    <td bgcolor="#d31f0e" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 6);" >
                                            <font color="#050404">   <c:out value="${lists.sixDay}"/>  <c:set var="sixDayTotals" value="${sixDayTotal+lists.sixDay}"/></a></td>
                                    <td bgcolor="#ff8000" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 7);" >
                                            <font color="#050404">  <c:out value="${lists.sevenDay}"/>  <c:set var="sevenDayTotals" value="${sevenDayTotal+lists.sevenDay}"/></a></td>
                                    <td bgcolor="#ff8000" align="center"> <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 8);" >
                                            <font color="#050404">  <c:out value="${lists.moreThanSevenDay}"/> <c:set var="moreDayTotals" value="${moreDayTotal+lists.moreThanSevenDay}"/> </a></td>
                                    <td bgcolor="#DA542D" style="text-align: center"><font color="#050404"><c:out value="${lists.zeroDay+oneDayTotal+lists.twoDay+lists.threeDay+lists.fourDay+lists.fiveDay+lists.sixDay+lists.sevenDay+lists.moreThanSevenDay}"/></font></td>
                                </tr>
                            </c:forEach>
                            </c:if>
                            <c:forEach items="${nodList}" var="list">
                                <tr>
                                    <td  bgcolor="#31DFD7" align="center" > <font color="#050404"><c:out value="${list.statusName}"/></td>

                                    <td bgcolor="#15c6b7" align="center" > <a  href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 0);" >
                                            <font color="#050404"> <c:out value="${list.zeroDay}"/>
                                            <c:set var="zeroDayTotal" value="${zeroDayTotal+list.zeroDay}"/></a></td>
                                    <td bgcolor="#18baac" align="center" > <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 1);" >
                                            <font color="#050404">  <c:out value="${list.oneDay}"/>
                                            <c:set var="oneDayTotal" value="${oneDayTotal+list.oneDay}"/>
                                        </a> </td>
                                    <td bgcolor="#f9f900" align="center" > <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 2);" >
                                            <font color="#050404"> <c:out value="${list.twoDay}"/>
                                            <c:set var="twoDayTotal" value="${twoDayTotal+list.twoDay}"/>
                                        </a> </td>
                                    <td bgcolor="#dbdb0f" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 3);" >
                                            <font color="#050404"> <c:out value="${list.threeDay}"/> <c:set var="threeDayTotal" value="${threeDayTotal+list.threeDay}"/></a> </td>
                                    <td bgcolor="#afaf18" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 4);" >
                                            <font color="#050404">    <c:out value="${list.fourDay}"/>  <c:set var="fourDayTotal" value="${fourDayTotal+list.fourDay}"/> </a></td>
                                    <td bgcolor="#f91702" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 5);" >
                                            <font color="#050404">  <c:out value="${list.fiveDay}"/>  <c:set var="fiveDayTotal" value="${fiveDayTotal+list.fiveDay}"/> </a></td>
                                    <td bgcolor="#d31f0e" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 6);" >
                                            <font color="#050404">   <c:out value="${list.sixDay}"/>  <c:set var="sixDayTotal" value="${sixDayTotal+list.sixDay}"/></a></td>
                                    <td bgcolor="#ff8000" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 7);" >
                                            <font color="#050404">  <c:out value="${list.sevenDay}"/>  <c:set var="sevenDayTotal" value="${sevenDayTotal+list.sevenDay}"/></a></td>
                                    <td bgcolor="#ff8000" align="center"> <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 8);" >
                                            <font color="#050404">  <c:out value="${list.moreThanSevenDay}"/> <c:set var="moreDayTotal" value="${moreDayTotal+list.moreThanSevenDay}"/> </a></td>

                                    <td bgcolor="#DA542D" style="text-align: center"><font color="#050404">
                                        <c:if test="${list.statusId == '5'}">
                                            <c:set var="orderCreatedTotal" value="${orderCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${orderCreatedTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '6'}">
                                            <c:set var="tripCreatedTotal" value="${tripCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripCreatedTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '8'}">
                                            <c:set var="tripFreezedTotal" value="${tripFreezedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripFreezedTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '10'}">
                                            <c:set var="tripStartTotal" value="${tripStartTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripStartTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '12'}">
                                            <c:set var="tripEndTotal" value="${tripEndTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripEndTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '13'}">
                                            <c:set var="tripClosureTotal" value="${tripClosureTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripClosureTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '14'}">
                                            <c:set var="tripSettledTotal" value="${tripSettledTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${tripSettledTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '16'}">
                                            <c:set var="billCreatedTotal" value="${billCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${billCreatedTotal}"/>
                                        </c:if>
                                        <c:if test="${list.statusId == '17'}">
                                            <c:set var="billSubmittedTotal" value="${billSubmittedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                            <c:out value="${billSubmittedTotal}"/>
                                        </c:if>
                                        <c:set var="summaryTotal" value="${list.moreThanSevenDay+orderCreatedTotal+tripCreatedTotal+tripFreezedTotal+tripStartTotal+tripEndTotal+tripClosureTotal+tripSettledTotal+billCreatedTotal+billSubmittedTotal}"/>
                                        </font>
                                    </td>
                                </tr>
                            </c:forEach>
                           
                            <tr>

                                <td bgcolor="#31DFD7" style="text-align: center"><font color="#050404">Total</font></td>
                                <td bgcolor="#15c6b7"  style="text-align: center"><font color="#050404">
                                    
                                        <font color="#050404"><c:out value="${zeroDayTotal+zeroDayTotals}"/></font>
                                    </font></td>
                                <td bgcolor="#18baac" style="text-align: center"> <font color="#050404"><c:out value="${oneDayTotal+oneDayTotals}"/></font></td>
                                <td bgcolor="#f9f900" style="text-align: center"> <font color="#050404"><c:out value="${twoDayTotal+twoDayTotals}"/></font></td>
                                <td bgcolor="#dbdb0f"  style="text-align: center"> <font color="#050404"><c:out value="${threeDayTotal+threeDayTotals}"/></font></td>
                                <td bgcolor="#afaf18" style="text-align: center"> <font color="#050404"><c:out value="${fourDayTotal+fourDayTotals}"/></font></td>
                                <td bgcolor="#f91702" style="text-align: center"> <font color="#050404"><c:out value="${fiveDayTotal+fiveDayTotals}"/></font></td>
                                <td bgcolor="#d31f0e" style="text-align: center"> <font color="#050404"><c:out value="${sixDayTotal+sixDayTotals}"/></font></td>
                                <td bgcolor="#ff8000" style="text-align: center"> <font color="#050404"><c:out value="${sevenDayTotal+sevenDayTotals}"/></font></td>
                                <td bgcolor="#ff8000" style="text-align: center">
                                    
                                        <font color="#050404"> <c:out value="${moreDayTotal+moreDayTotals}"/></font>
                                    
                                </td>
                                <td bgcolor="#DA542D" style="text-align: center"><font color="#050404"><c:out value="${zeroDayTotal+oneDayTotal+twoDayTotal+threeDayTotal+fourDayTotal+fiveDayTotal+sixDayTotal+sevenDayTotal+moreDayTotal+zeroDayTotals+oneDayTotals+twoDayTotals+threeDayTotals+fourDayTotals+fiveDayTotals+sixDayTotals+sevenDayTotals+moreDayTotals}"/></font></td>
                            </tr>
                        </tbody>
                    </c:if>
                </table>
                    <br>
                    <br>
                    <br>

                <c:if test="${nodList == null}">
                    <center>
                        <br>
                        <br>
                        <br>
                        <b style="color: red">No Records Found</b>
                    </center>
                </c:if>

            </div>
        </div>
    </div>
</body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>