<%-- 
    Document   : vehiclePerformance
    Created on : Sep 6, 2012, 4:02:15 PM
    Author     : Arul
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
         <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>

    </head>



    <script>
        function submitPage(value){
            /*if(document.vehiclePerformance.vehicleId.value == 0){
                alert("Please select the vehicle")
                document.vehiclePerformance.vehicleId.focus();
            }else if(isEmpty(document.vehiclePerformance.fromDate.value)){
                alert("Please select the from date")
                document.vehiclePerformance.fromDate.focus();
            }else if(isEmpty(document.vehiclePerformance.toDate.value)){
                alert("Please select the to date")
                document.vehiclePerformance.toDate.focus();
            }else{*/
                document.vehiclePerformance.action="/throttle/vehiclePerformanceReport.do";
                document.vehiclePerformance.submit();
            //}
        }
        function setFocus(){
            var regno='<%=request.getAttribute("regNo")%>';
            var fromDate='<%=request.getAttribute("fromDate")%>';
            var toDate='<%=request.getAttribute("toDate")%>';
            if(vehicleId!='null' && fromDate!='null' && toDate!='null'){
                document.vehiclePerformance.regno.value=regno;
            }if(fromDate!='null'){
                document.vehiclePerformance.fromDate.value=fromDate;
            }if(toDate!='null'){
                document.vehiclePerformance.toDate.value=toDate;
            }
        }
        window.onload = getVehicleNos;
        function getVehicleNos(){
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        }

    </script>
    <body onload="setFocus();">
        <form name="vehiclePerformance" method="post">
                <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Performance Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td>Vehicle No</td>
                                        <td><input name="regno" id="regno" type="text" class="form-control" size="20" value="" onKeyPress="getVehicleNos();" autocomplete="off"></td>                                        
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <c:if test="${vehiclePerformance == null}">
                <tr><font color="red">No Records Found</font></tr>
            </c:if>


            <br/>
            <c:if test = "${vehiclePerformance != null}" >
                <%
                    int index = 0;
                    ArrayList vehiclePerformance = (ArrayList) request.getAttribute("vehiclePerformance");
                    if (vehiclePerformance.size() != 0) {
                %>
                <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                    <td class="contentsub" height="30">S.No</td>                    
                    <td class="contentsub" height="30">Trip Date</td>                    
                    <td class="contentsub" height="30">Driver Name</td>
                    <td class="contentsub" height="30">Current Status</td>
                    <td class="contentsub" height="30">Route Name</td>                    
                    <td class="contentsub" height="30">Trip Type</td>
                    <td class="contentsub" height="30">toll Expense</td>
                    <td class="contentsub" height="30">Diesel Expense</td>
                    <td class="contentsub" height="30">Total Expense</td>
                    <td class="contentsub" height="30">Total Amount</td>
                    <td class="contentsub" height="30">Total Revenue</td>
                </tr>
                    <c:set var="vRStatus" value="${0}" />
                    <c:set var="vHStatus" value="${0}" />
                    <c:set var="vRHStatus" value="${0}" />
                    <c:set var="vTotExp" value="${0}" />
                    <c:set var="vTotAmt" value="${0}" />
                    <c:set var="vTotRev" value="${0}" />

                    <c:forEach items="${vehiclePerformance}" var="vPerform">
                        <c:set var="total" value="${total+1}"></c:set>

                        <c:if test = "${vPerform.currentStatus == 'Running'}" >
                            <c:set var="vRStatus" value="${vRStatus + 1}" />
                        </c:if>
                        <c:if test = "${vPerform.currentStatus == 'Halt'}" >
                            <c:set var="vHStatus" value="${vHStatus + 1}" />
                        </c:if>
                        <c:set var="vRHStatus" value="${vRHStatus + 1}" />
                        <c:set var="vTotExp" value="${vTotExp + vPerform.totalExpense}" />
                        <c:set var="vTotAmt" value="${vTotAmt + vPerform.totalTonAmount}" />

                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <%index++;%>
                        <tr>
                            <td class="<%=classText%>"  height="30"><%=index%></td>                            
                            <td class="<%=classText%>"  height="30"><c:out value="${vPerform.tripDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${vPerform.empName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${vPerform.currentStatus}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${vPerform.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${vPerform.tripType}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${vPerform.tollExpense}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${vPerform.dieselExpense}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${vPerform.totalExpense}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${vPerform.totalTonAmount}"/></td>
                            <td class="<%=classText%>"  height="30"><fmt:formatNumber pattern="##.00" value="${vTotAmt - vTotExp}"/></td>
                        </tr>
                    </c:forEach>
                        <c:set var="vTotRev" value="${vTotAmt - vTotExp}" />
                </table>
                <%
                            }
                %>
                <br />
                <table border="0" align="center" width="80%" cellpadding="0" cellspacing="5" class="overallSummary">
                    <tr>
                        <th>Summary</th>
                        <td>Running  &nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vRStatus}"/></td>
                        <td>Halt &nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vHStatus}"/></td>
                        <td>Total Trips&nbsp;&nbsp; = &nbsp;&nbsp;<c:out value="${vRHStatus}"/></td>
                        <td>Net Expenses&nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${vTotExp}"/></td>
                        <td>Net Amount&nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${vTotAmt}"/></td>
                        <td>Net Revenue&nbsp;&nbsp; = &nbsp;&nbsp;<fmt:formatNumber pattern="##.00" value="${vTotRev}"/></td>
                    </tr>                    
                </table>
            </c:if>             
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
