
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.business;

import java.util.ArrayList;
import java.io.*;

/**
 * chni
 *
 * @author
 */
public class OperationTO implements Serializable {

    private String terifType = null;
    private String timeSlotTo = null;            

    private String invoiceNo = null;
    private String invoiceDate = null;
    private String creditNoteId = null;
    private String productDesc = null;
    private String pincode = null;
    private String billlingLocation = null;
    private String apiErrorMsg = null;

    private String roundoff = null;
    private String upload_status = null;
    private String documentDate = "";
    private String documentNumber = "";
    private String invType = null;
    private String supTyp = null;
    private String billingLegalName = null;
    private String billingTradeName = null;
    private String billingGstin = null;
    private String placeOfSupply = null;
    private String billlingAddress = null;
    private String billingState = null;
    private String placeOfState = null;
    private String billingPin = null;
    private String snos = null;
    private String itemDescription = null;
    private String gorS = null;
    private String hsnCd = null;
    private String qty = null;
    private String unit = null;
    private String itemPrice = null;
    private String grossAmount = null;
    private String itemDiscount = null;
    private String itemTaxableValue = null;
    private String gstPercent = null;
    private String igst = null;
    private String cgst = null;
    private String sgst = null;
    private String cessValue = null;
    private String statecessValue = null;
    private String netamount = null;
    private String totalTaxableValue = null;
    private String totalIgst = null;
    private String totalCgst = null;
    private String totalSgst = null;
    private String totalNetAmount = null;
    private String compLegalName = null;
    private String compGstin = null;
    private String compAddress = null;
    private String compLocation = null;
    private String compState = null;
    private String compPin = null;
    private String isServc = null;
    private String prdDesc = null;
    private String hsnCode = null;
    private String unitPrice = null;
    private String totAmount = null;
    private String assAmt = null;
    private String cgstPercent = null;
    private String sgstPercent = null;
    private String igstPercent = null;
    private String totalValue = null;
    private String expenseDesc = null;

    private String billingCustId = null;
    private String sellerDetails = null;
    private String invoiceId = null;
    private String invoiceCode = null;
    private String billingCustName = null;
    private String billingAddress = null;
    private String billingPanNo = null;
    private String billingPincode = null;
    private String billingPos = null;
    private String billingLocation = null;
    private String grandTotal = null;

    private String commodityId=null;
    private String commodityName=null;
    private String gstApplicable=null;
    private String commodityDesc=null;
    private String usedLimit=null;
    private String availLimit=null;
    private String bankId=null;
    private String bankName=null;
    private String fixedCreditLimit=null;
    private String custTypeId=null;
    private String organizationId=null;
    private String gstNo=null;
    private String stateId=null;
    private String organizationName=null;
    private String stateName=null;
    private String panNo = null;
    private String categoryCode = null;
    private String categoryName = null;
    private String sacCode = null;
    private String sacDescription = null;
    private String gstCategoryId = null;
    private String description = null;
    private String gstCategoryName = null;
    private String categoryId = null;
    private String gstCodeMasterId = null;
    private String gstName = null;
    private String state = null;
    private String gstCode = null;
    private String gstPercentage = null;
    private String validFrom = "";
    private String validTo = "";
    private String gstRateDetailId = null;
    private String gstCategoryCode = null;
    private String gstRateId = null;
    private String hsnName = null;
    private String hsnDescription = null;
    private String gstProductId = null;
    private String gstType = null;
    private String gstProductCategoryCode = null;

    private String marketHireWithReefer = "";
    private String effectiveHour = "";
    private String effectiveMin = "";
    private String orgWithoutReeferRate = "";
    private String orgWithReeferRate = "";
    private String marketHire = "";
    private String approvalStatus = "";
    private String fuelVehicle = null;
    private String fuelDg = null;
    private String toll = null;
    private String driverBachat = null;
    private String dala = null;
    private String misc = null;
    private String containerNo = null;
    private String movementTypeName = null;
    private String tripTypeId = null;
    private String tripTypeName = null;
    private String tripConfigId = null;
    private String tripConfigInd = null;
    private String movementTypeIds[];
    private String tripConfigInds[];
    private String tripConfigIds[];
    private String tripTypeIds[];

    private String[] vehTypeIdValue = null;
    private String[] containerTypeValue = null;
    private String[] containerName1 = null;
    private String[] containerLinerName = null;
    private String[] containerFreightCharges = null;
    private String[] consignmentContainerIds = null;
    private String[] containerType = null;
    private String[] containerNO = null;
    private String[] containerQty = null;
    private String ContainerNo = null;
    private String companyID1 = null;
    private String companyName = null;
    private String insId = null;
    private String contactNo = null;
    private String agentName = null;
    private String agentCode = null;
    private String agentNo = null;
    private String movementTypeId = null;

    private String parameterType = null;
    private String emailBcc = null;
    private String rtoChargeId = null;
    private String rtoVehicleId = null;
    private String rtoId = null;
    private String rtoName = null;
    private String rtoCode = null;
    private String amounts = null;
    private String menuname = null;
    private String menuId = null;
    private String functionmenu = null;
    private String submodule = null;
    private String module = null;
    private String function = null;
    private String functionuri = null;
    private String entrymenu = null;
    private String entrytype = null;
    private String secondaryBillingTypeId = null;
    private String trailerTypeName = null;

//    13/05/16
    private String custAddressTwo = null;
    private String custAddressthree = null;
    private String shippingLineTwo = null;
    private String orderNo = null;

    private String fuelWOReefer[] = null;
    private String fuelWithReefer[] = null;
    private String fuelWOReeferReq = null;
    private String fuelWithReeferReq = null;

    private String expcontractRouteOrigin = null;
    private String expcontractRouteDestination = null;
    private String contractRouteInteream = null;

    private String[] fuelCostPerKmsEmpCon = null;
    private String[] fuelCostPerKms20ftCon = null;
    private String[] fuelCostPerKms2_20ftCon = null;
    private String[] fuelCostPerKms40ftCon = null;
    private String[] vehExpenseEmpCon = null;
    private String[] vehExpense20ftCon = null;
    private String[] vehExpense2_20ftCon = null;
    private String[] vehExpense40ftCon = null;
    //
    private String fuelCostPerKmEmpCon = null;
    private String fuelCostPerKm20ftCon = null;
    private String fuelCostPerKm2_20ftCon = null;
    private String fuelCostPerKm40ftCon = null;
    private String vehExpensesEmpCon = null;
    private String vehExpenses20ftCon = null;
    private String vehExpenses2_20ftCon = null;
    private String vehExpenses40ftCon = null;
    //
    private String emptyContainerMilleage = null;
    private String container_20ft_Milleage = null;
    private String container_2_20ft_Milleage = null;
    private String container_40ft_Milleage = null;

    private String containerQuantity = null;
    private String loadTypeId = null;
    private String containerQty1 = null;

    private String icdLocation = null;
    private String[] tripIds = null;
    private String miscStatus = null;
    private String incentiveStatus = null;
    private String tollStatus = null;
    private String oldFuelPriceId = null;
    private String approveStatus = null;
    private String billingPartyId = null;
    private String billingPartyName = null;
    private String grNo = null;
    private String billOfEntry = null;
    private String cash = null;
    private String cashDate = "";
    private String shipingLineNo = null;
    private String secCustId = null;
    private String timeSlot = null;
    private String plannedStatus = null;
    private String consignmentContainerId = null;
    private String containerLinerId = null;
    private String containerFreightCharge = null;
    private String containerFreightRate = null;

    private String linerId = "";
    private String linerName = null;
    private String loadType = null;
    private String containerQtys = null;
    private String containerTypes = null;
    private String penality = null;
    private String chargeamount = null;
    private String detention = null;
    private String chargeamt = null;
    private String destatus = null;
    private String pcmunit = null;
    private String dcmunit = null;
    private String pcmremarks = null;
    private String dcmremarks = null;

    private String consigneeId = null;
    private String consignorId = null;
    private String billingParty = null;
    private String movementType = null;
    private String linerid = null;
    private String linername = null;

    private String longitude = null;

    private String speed = null;
    private String vehicleUnit = null;
    private String quotationId = null;
    private String containerTypeId = null;
    private String tollId = null;
    private String orderId = null;
    private String onwardOrderId = null;
    private String returnOrderId = null;
    private String onwardOrderIdTrips = null;
    private String returnOrderIdTrips = null;
    private String doDistance1 = null;
    private String doDistance2 = null;

    private String tollName = null;
    private String vehicleMileageEmpty = null;
    private String[] routeBorderCityIds = null;
    private String[] dcVehicleTypeId = null;
    private String[] dcRate = null;
    private String routeBorderCityId = null;
    private String freezeInDate = "";
    private String freezeInTime = "";
    private String freezeOutDate = "";
    private String freezeOutTime = "";
    private String incidentId = null;
    private String[] borderCityId = null;
    private String[] borderHours = null;
    private String[] borderMinutes = null;
    private String routeSellingCost = null;
    private String routeSellingCostId = null;
    private String borderCrossingStatus = null;
    private String borderCount = null;
    private String borderCheckingHour = "";
    private String borderCheckingMinute = "";
    private String detentionHour = "";
    private String detentionMinute = "";
    private String[] sellVehTypeId = null;
    private String[] vehModelId = null;
    private String[] vehMfrId = null;
    private String[] vehAgeingId = null;
    private String borderStatus = null;
    private String ageingId = null;
    private String age = null;
    private String toAge = null;
    private String googleCityName = null;

    private String bookingPersonInfo = null;
    private String multilpeContainerFlag = null;
    private String segmentName = null;
    private String latitude = null;
    private String longitute = null;
    private String cityCode = null;
    private String customerOrderReferenceNo = "";
    private String orderVolume = null;
    private String tripAdvanceId = null;
    private String pickupType = null;
    private String currencyCode = null;
    private String requestDate = "";
    private String costPerKm = null;
    private String destinationType = null;
    private String vehicleCapacity = null;
    private String totalVolume = null;
    private String originLat = null;
    private String originLong = null;
    private String destLat = null;
    private String destLong = null;
    private String pickupStartDate = "";
    private String pickupEndDate = "";
    private String arrivalStartDate = "";
    private String arrivalEndDate = "";
    private String destinationRadius = null;
    private String pickupRadius = null;
    private String originCityId = null;
    private String pointPlanTimeTemp = null;
    private String containerTypeName = null;
    private String billEntryNo = "";
    private String billEntryDate = "";
    private String dutyPaymentDate = "";
    private String occDate = "";
    private String cfsPersonName = null;
    private String cfsPersonNo = null;
    private String containerName = null;
    private String containerId = null;
    private String consginmentOrderStatus = null;
    private String hubId = null;
    private String hubName = null;
    private String wareHouseName = null;
    private String wareHouseId = null;
    private String jobCardTypeNew = null;
    private String jobCardCode = null;
    private String jobCardRemarks = null;
    private String jobCardIssueDate = "";
    private String[] routeCostIds = null;
    private String[] fixedKmStatus = null;
    private String[] editId = null;
    private String[] fixedKmvehicleTypeId = null;
    private String[] vehicleNos = null;
    private String[] fixedTotalKm = null;
    private String[] fixedTotalHm = null;
    private String[] fixedRateWithReefer = null;
    private String[] fixedExtraKmRateWithReefer = null;
    private String[] fixedExtraHmRateWithReefer = null;
    private String[] fixedRateWithoutReefer = null;
    private String[] fixedExtraKmRateWithoutReefer = null;
    private String outStandingDate = null;
    private String routeCostId = null;
    private String emptyTripPurpose = null;
    private String contractFixedRateId = null;
    private String contractVehicleNos = null;
    private String extraKmRatePerKmRateWithReefer = "";
    private String extraHmRatePerHmRateWithReefer = "";
    private String extraKmRatePerKmRateWithoutReefer = "";
    private String totalHm = "";
    private String extraKmRateWithReefer = "";
    private String extraHmRateWithReefer = "";
    private String extraKmRateWithoutReefer = "";
    private String tripEndHm = "";
    private String serviceVendorId = null;
    private String priorityId = null;
    private String reqHour = "";
    private String reqMinute = "";
    private String reqSeconds = "";
    private String km = null;
    private String jobCardType = null;
    private String sectionId = null;
    private String sectionName = null;
    private String serviceTypeName = null;
    private String serviceTypeId = null;
    private String usageTypeId = null;
    private String closedBy = null;
    private String actualCompletionDate = "";
    private String plannedCompleteHours = "";
    private String plannedCompleteDays = "";
    private String jobcardHours = "";
    private String jobcardDays = "";
    private double creditLimitAmount = 0;
    private double outStanding = 0;
    private double deviateAmount = 0;
    private double tripRevenue = 0;
    private double paidAdvance = 0;
    private String wfuDay = null;
    private String tripStatusId = null;
    private String approvedBy = null;
    private String amountExceed = null;
    private String estimatedExpense = null;
    private String totalRequestAmount = null;
    private String tripPod = null;
    private String nextStatusId = null;
    private String userName = null;
    private String timeApprovalStatus = "";
    private String emailTo = null;
    private String emailCc = null;
    private String infoEmptyTo = null;
    private String infoEmailCc = null;
    private String paymentTypeId = null;
    private String orderType = null;
    private String productCategory = null, productCategoryUnchecked = null, tempMinimumTemp = null, tempMaximumTemperature = null, tempReeferRequired = null,
            temp1MinimumTemp = null, temp1MaximumTemperature = null, temp1ReeferRequired = null;
    private double factors = 0;
    private String bpclTransactionId = null;
    private String fuelTypeId = null;
    private String fuelTypeName = null;
    private String fuelUnite = null;

    private String infoEmailTo = null;

    private String fuelType = null;
    private String advanceStatus = null;
    private String advanceToPayStatus = null;
    private String paidAmount = null;
    private String paymentModeId = null;
    private String paymentModeName = null;
    private String paymentType = null;
    private String rtgsNo = null;
    private String rtgsRemarks = null;
    private String chequeNo = null;
    private String chequeRemarks = null;
    private String draftNo = null;
    private String draftRemarks = null;
    private float totalRevenue = 0;
    private float totalExp = 0;
    private String custCode = null;
    private String custType = null;
    private String custContactPerson = null;
    private String accountManagerId = null;
    private String accountManager = null;
    private String creditLimit = null;
    private String creditDays = null;
    private String fleetCenterContactNo = null;
    private String mobileNo = null;
    private String[] articleId = null;
    private String consignmentArticleId = null;
    private String batchName = null;
    private String unitOfMeasurement = null;
    private String contractVehicleTypeId = null;
    private String distanceTravelled = null;
    private String tripAdvaceId = null;
    private String currentTemperature = null;
    private String actualKmVehicleTypeId = null;
    private String smtp = null;
    private String port = null;
    private String emailId = null;
    private String password = null;
    private String tomailId = null;
    //Senthil 10-12-2013
    private String consignmentRefNo = null;
    private String pickupdate = null;
    private String pickupTime = null;
    private String consignmentStatus = null;
    private String consignmentVehicleId = null;
    private String consignmentVehicleStatus = null;
    private String configId = null;
    private String requesttype = null;
    private String[] batchCode = null;
    private String[] uom = null;
    private String totalrunkm = null;
    private String totalrunhm = null;
    private String rcmexpense = null;
    private String systemexpense = null;
    private String fleetCenterId = null;
    private String totalWeights = null;
    private String tripPlanningRemark = null;
    private String consigmentId = null;
    private String invoicecustomer = null;
    private String totalDrivers = null;
    private String totFreightCharges = null;
    private String totalkmrun = null;
    private double totalTripFreightAmt = 0.0;
    private double totalExpenseAmt = 0.0;
    private int totaldrivers = 0;
    private double totalTripKmRun = 0;
    private double totalFreightAmt = 0;
    //Senthil 10-12-2013
    //Mathan 10-12-2013
    private String productCategoryName = null;
    private String id = null;
    private String reeferMinimumTemperature = null;
    private String reeferMaximumTemperature = null;
    private String parameterName = null;
    private String parameterValue = null;
    private String parameterUnit = null;
    private String parameterDescription = null;
    private String frequencyId = null;
    private String frequencyName = null;
    private String alertName = null;
    private String alertDes = null;
    private String alertBased = null;
    private String alertRaised = null;
    private String alerttoEmail = null;
    private String alertccEmail = null;
    private String alertsemailSub = null;
    private String alertsfrequently = null;
    private String alertRaised1 = null;
    private String escalationtoEmail1 = null;
    private String escalationccEmail1 = null;
    private String escalationemailSub1 = null;
    private String repeat = null;
    private String alertid = null;
    private String alertstatus = null;
    private String checkListName = null;
    private String checkListStage = null;
    private String checkListDate = "";
    private String checkListId = null;
    private String checkListStatus = null;
    private String stage = null;
    private String stageName = null;
    private String StageId = null;
    private String cityName = null;
    private String cityState = null;
    private String zoneid = null;
    private String zoneName = null;
    private String zoneId = null;
    //Mathan 10-12-2013
    private String approvestatus = null;
    private String statusName = null;
    private String approveremarks = null;
    private String requestedadvance = null;
    private String batchType = null;
    private String advicedate = "";
    private String isactive = null;
    private String pointOrder = null;
    private String contractRouteOrigin = null;
    private String expectedArrivalTime = "";
    private String expectedArrivalDate = "";
    private String tripStartDate = "";
    private String tripstartkm = null;
    private String tripendkm = null;
    private String freightcharges = null;
    private String customerContractId = null;
    private String consignmentOrderRefRemarks = null;
    private String customerPincode = null;
    private String customerMobile = null;
    private String customerPhone = null;
    private String customerEmail = null;
    private String customerBillingType = null;
    private String budinessType = null;
    private String consigmentInstruction = null;
    private String vehicleRequiredTime = "";
    private String consignorMobile = null;
    private String consigneeMobile = null;
    private String invoiceType = null;
    private String kmRate = null;
    private String kgRate = null;
    private String fixedRate = null;
    private String totalPackages = null;
    private String totalWeight = null;
    private String totalDistance = null;
    private String freightCharges = null;
    private String totalStatndardCharges = null;
    private String consigmentOrderStatus = null;
    private String consignmentPointId = null;
    private String consignmentPointType = null;
    private String consignmentPointAddress = null;
    private String pointSequence = null;
    private String pointPlanTime = "";
    private String pointDate = "";
    private String pointAddress = null;
    private String consignmentOrderNo = null;
    private String articleCode = null;
    private String articleName = null;
    private String packageNos = null;
    private String packageWeight = null;
    //Vehicle Driver Mapping
    private String mappingId = null;
    private String regNo = null;
    private String primaryDriverId = null;
    private String primaryDriverName = null;
    private String secondaryDriverIdOne = null;
    private String secondaryDriverNameOne = null;
    private String secondaryDriverIdTwo = null;
    private String secondaryDriverNameTwo = null;
    private String approvalstatus = null;
    private String estimatedadvance = null;
    private String cnoteName = null;
    private String actualadvancepaid = null;
    private String priority1 = null;
    private String mfrId = null;
    private int servicetypeId = 0;
    private int compId = 0;
    private int cnoteCount = 0;
    private String servicetypeName = null;
    private String driverVendorId = null;
    private String vehicleVendorId = null;
    private String compName = null;
    private String bayNo = null;
    private String technicianId = null;
    private String technicianName = null;
    private String estimatedHrs = "";
    private String actualHrs = "";
    private String roleId = null;
    private int userId = 0;
    private int workOrderId = 0;
    private String regno = null;
    private ArrayList activity = new ArrayList();
    private ArrayList periodicServiceList = new ArrayList();
    private int priority = 0;
    private String activityName = null;
    private String[] pointRouteId = null;
    private int elapsedDays = 0;
    private String reqDate = "";
    private String hour = "";
    private String minute = "";
    private String sec = null;
    private String remarks = null;
    private String cRemarks = null;
    private int kmReading = 0;
    private int vehicleId = 0;
    private int serviceLocationId = 0;
    private String driverName = null;
    private String serviceLocation = null;
    private String dateOfIssue = "";
    private String vehicleTypeName = null;
    private String chassNo = null;
    private String usageName = null;
    private String engineNo = null;
    private String mfrName = null;
    private String modelName = null;
    private String secName = null;
    private String probName = null;
    private String desc = null;
    private String saleDate = null;
    int severity = 0;
    private String symptoms = null;
    private String scheduledDeleivery = "";
    private String scheduledDate = "";
    private String status = null;
    private String serviceName = null;
    private String cust = null;
    private int jobCardId = 0;
    private String jcMYFormatNo = null;
    private int serviceId = 0;
    private int balanceKm = 0;
    private int balanceHm = 0;
    private int probId = 0;
    private int empId = 0;
    private int secId = 0;
    private int vendorId = 0;
    private int model = 0;
    private float totalAmount = 0;
    private String vendorName = null;

    private String identifiedby = null;
    private String servicedDate = null;
    private String closedDate = null;
    private String lastServicedKm = null;
    private String custName = null;
    private String custAddress = null;
    private String custCity = null;
    private String custState = null;
    private String custPhone = null;
    private String custMobile = null;
    private String custEmail = null;
    private int check = 0;
    private int currentKM = 0;
    private int hourMeter = 0;
    private int currentHM = 0;
    private int flag = 0;
    private int mrsId = 0;
    private int itemId = 0;
    private int reqQty = 0;
    private int issQty = 0;
    private int woId = 0;
    private String vendorAddress = null;
    private String woComp = null;
    private String problemName = null;
    private String phone = null;
    private String createdDate = "";
    private String pcd = null;
    private String intime = "";
    private String totalKm = "0";
    private String[] address = null;
    private String[] problemNameSplit = null;
    private String[] symptomsSplit = null;
    //Hari
    private int billNo = 0;
//   bala
    private int customertypeId = 0;
    private String customertypeName = null;
    private String settlementType = null;
    //---------- Trip details ------
    private String tripSheetId = null;
    private String tripCode = null;
    private String tripRouteId = null;
    private String tripVehicleId = null;
    private String tripScheduleId = null;
    private String tripScheduleDateTime = "";
    private String tripScheduleDate = "";
    private String tripScheduleTime = "";
    private String timeLeft = null;
    private String timeLeftValue = null;
    private String tripDriverId = null;
    private String tripDate = "";
    private String tripDepartureDate = "";
    private String tripArrivalDate = "";
    private String tripKmsOut = null;
    private String tripKmsIn = null;
    private String tripTotalLitres = null;
    private String tripFuelAmount = null;
    private String tripTotalAllowances = null;
    private String tripTotalExpenses = null;
    private String expenseName = null;
    private String expenseId = null;
    private String tripTotalKms = null;
    private String tripBalanceAmount = null;
    private String tripStatus = null;
    //tripId,identityNo,deviceId,routeName,outKM,driverName,outDateTime,inKM,location,inDateTime,status
    private String tripId = null;
    private String returnTripId = null;
    private String identityNo = null;
    private String deviceId = null;
    private String routeName = null;
    private String outKM = null;
    private String outDateTime = "";
    private String inKM = null;
    private String location = null;
    private String inDateTime = "";
    private String settlementFlag = null;
    private String issuerName = null;
    private String designation = null;
    private String amount = null;
    private String advDatetime = "";
    private String fuelName = null;
    private String bunkName = null;
    private String liters = null;
    private String fuelDatetime = null;
    private String inOutIndication = null;
    private String inOutDateTime = "";
    private String locationName = null;
    private String tonnageRate = "0";
    private String totalTonnage = null;
    private String deliveredTonnage = null;
    private String shortage = null;
    private String empName = null;
    private String vehicleNo = null;
    private String embarkDate = "";
    private String alightDate = "";
    private String alightStatus = null;
    private String createdOn = null;
    private String expensesDesc = null;
    private String serviceVendor = null;
    private String locationId = null;
    private String custId = null;
    private String tripType = null;
    private String totalTonAmount = null;
    private String issuerId = null;
    private String bunkId = null;
    private String driverId = null;
    private String cleanerStatus = null;
    private String depHours = null;
    private String depMints = null;
    private String arrHours = null;
    private String arrMints = null;
    private String vehicleTypeId = null;
    private String flagString = null;
    private String totalFuel = null;
    private String totalFuelAmount = null;
    private String mileage = null;
//    CLPL Trip sheet start
    private String stageId = null;
    private String tonnage = null;
    private String bags = null;
//    private String orderNo = null;
    private String pinkSlip = null;
    private String tripdateP = null;
    private String tolocation = null;
//    CLPL Trip sheet end
    // CLPL  trip management
    private String lpsID = null;
    private String lpsNumber = null;
    private String partyName = null;
    private String lpsDate = "";
    private String billStatus = null;
    private String orderNumber = null;
    private String contractor = null;
    private String packerNumber = null;
    private String route = null;
    private String quantity = null;
    private String destination = null;
    private String productName = null;
    private String packing = null;
    private String clplPriority = null;
    private String active_ind = null;
    private String productId = null;
    private String productCode = null;
    private String lorryNo = null;
    private String gatePassNo = null;
    private String watchandward = null;
    private String date = null;
    private String lpsNo = null;
    private String returnAmt = null;
    private String returnExp = null;
    private String totalBalAmtRt = null;
    private String totalExpensesAmtRt = null;
    //Pink slip
    private String registerNo = null;
    private String driName = null;
    private String routeId = null;
    private String selectedRouteId = null;
    private String selectedLpsIds = null;
    private String pinkSlipID = null;
    private String routeID = null;
    private String vehicleID = null;
    private String driverID = null;
    private String vehicleTonnage = null;
    private String ownership = null;
    private String selectedBillStatus = null;
    //CLPL Invoice
    private String openDateTime = "";
    private String customerId = null;
    private String customerName = null;
    private String closeDateTime = "";
    private String totalallowance = null;
    private String totalamount = null;
    private String totalexpenses = null;
    private String balanceamount = null;
    private String totalkms = null;
    private String revenue = null;
    private String act_Ind = null;
    private String cust_Name = null;
    private String emp_Name = null;
    private String fromDate = "";
    private String toDate = "";
    private String remark = null;
    private String totalInvAmount = null;
    private String noOfTrip = null;
    private String taxAmount = null;
    private String vehicleid = null;
    private String invRefCode = null;
    private String invoiceStatus = null;
    private String numberOfTri = null;
    private String invoiceFor = null;
    private String invoiceDetailId = null;
    private String tripNo = null;
    private String freightAmount = null;
    private String ledgerName = null;
    private String ledgerId = null;
    private String summary = null;
    //Market Vehicle Settlement
    private String totalDeliveredTonnage = null;
    private String commissionPercentage = null;
    private String totalTannage = null;
    private String commissionAmount = null;
    private String settlementAmount = null;
    private String totalshortage = null;
    private String totalregno = null;
    private String LedgerCode = null;
    private String settlementid = null;
    private String numberOfTrip = null;
    private String commisssionAmount = null;
    private String totalAmountL = null;
    private String createddate = null;
    private String voucherCode = null;
    private String totalamaount = null;
    private String productname = "";
    //clpl trip edit
    private String GPSKm = null;
    private String returnTripProduct = null;
    private String returnFromLocation = null;
    private String returnToLocation = null;
    private String returnDate = "";
    private String returnTonnage = null;
    private String returnLoadedTonnage = null;
    private String returnLoadedDate = null;
    private String returnDeliveredTonnage = null;
    private String returnDeliveredDate = null;
    private String shortageTonnage = null;
    private String returnAmount = null;
    private String returnExpenses = null;
    private String totalBalAmtRetuen = null;
    private String totalExpensesAmtReturn = null;
//        clpl close trip
    private String tripReceivedAmount = null;
    private String fromLocationid = null;
    private String toLocationid = null;
    private String fromLocation = null;
    private String toLocation = null;
    private String returnTripType = null;
    private String returnProductName = null;
    private String returnKM = null;
    private String returnTon = null;
    private String deliveredDANo = null;
    private String loadedSlipNo = null;
    private String loadedTonnage = null;
    private String fLocationName = null;
    private String fLocationId = null;
    private String tLocationName = null;
    private String tLocationId = null;
    private String voucherNo = null;
    private String vDate = null;
    private String accountDetail = null;
    private String accountEntryID = null;
    private String accountEntryDate = null;
    private String accountsAmount = null;
    private String accountsType = null;
    private String narration = null;
    private String ledgerID = null;
    private String detailCode = null;
    private String gpno = null;
    private String branchLedgerName = null;
    private String driverLedgerName = null;
    private int sno = 0;
    private String selectedRow = null;
    private String crossingType = null;
    private String crossingAmount = null;
    private String tonnageRateMarket = null;
    private String twoLpsStatus = null;
    private String tripRevenueOne = null;
    private String tripRevenueTwo = null;
    private String selectedRouteName = null;
    private String selectedProductName = null;
    private String selectedPacking = null;
    //Print page
    // fro currency Master
    private String fromcurrency = null;
    private String fromcurrencyId = null;
    private String currencyid = null;

    private String fromPlace = null;
    private String fromLat = null;
    private String fromLong = null;
    private String toPlace = null;
    private String toLat = null;
    private String toLong = null;
    private String noOfTrips = null;

    private String fromcurrencyvalue = null;
    private String tocurrency = null;
    private String tocurrencyId = null;
    private String tocurrencyvalue = null;
    private String fromdate = "";
    private String todate = "";

    private String totalBags = null;
    private String billStatusNew = null;
//    Brattle Foods
    private String modelId = null;
    private String vehicleMileage = null;
    private String reeferMileage = null;
    private String routeCode = null;
    private String travelTime = "";
    private String distance = null;
    private String reeferRunning = null;
    private String roadType = null;
    private String tollAmount = null;
    private String fuelCost = null;
    private String[] vehRouteCostId = null;
    private String[] vehMileageEmpty = null;
    private String[] mfrIds = null;
    private String[] modelIds = null;
    private String[] vehTypeId = null;
    private String[] vehMileage = null;
    private String[] vehExpense = null;
    private String[] vehExpenseEmpty = null;
    private String[] reeferConsump = null;
    private String[] reeferExpense = null;
    private String[] varRate = null;
    private String[] varCost = null;
    private String[] totExpense = null;
    private String[] totExpenseEmpty = null;
    private String routeIdFrom = null;
    private String routeIdTo = null;
    private String editRouteId = null;
    private String fuelPrice = null;
    private String routeFrom = null;
    private String routeTo = null;
    private String vehicleExpense = null;
    private String reeferExpenses = null;
    private String variableCostPerKm = null;
    private String variableExpense = null;
    private String totalExpense = null;
    private String effectiveDate = "";
    private String cityFromId = null;
    private String cityFromName = null;
    private String cityToId = null;
    private String cityToName = null;
    private String travelHour = "";
    private String travelMinute = "";
    private String reeferHour = "";
    private String reeferMinute = "";
    private String tollAmountType = null;
    private String avgTollAmount = null;
    private String avgMisCost = null;
    private String avgDriverIncentive = null;
    private String avgFactor = null;
    private String[] fuelCostPerKm = null;
    private String[] fuelCostPerHr = null;
    private String[] tollAmounts = null;
    private String[] miscCostKm = null;
    private String[] driverIncenKm = null;
    private String[] factor = null;
    private String[] varExpense = null;
    private String city = null;
    private String[] fuelCostPerKms = null;
    private String[] fuelCostPerKmsEmpty = null;
    private String[] fuelCostPerHrs = null;
    private String[] reefMileage = null;
    private String fuelCostKm = null;
    private String fuelCostKmEmpty = null;
    private String fuelCostHr = null;
    private String tollAmountperkm = null;
    private String miscCostperkm = null;
    private String driverIncentperkm = null;
    private String variExpense = null;
    private String vehiExpense = null;
    private String vehiExpenseEmpty = null;
    private String reefeExpense = null;
    private String totaExpense = null;
    private String totaExpenseEmpty = null;
    private String sellCost = null;
    private String effectivDate = null;
    private String stChargeName = null;
    private String stChargeDesc = null;
    private String stChargeUnit = null;
    private String stChargeId = null;
    private String unitName = null;
    private String billingTypeId = null;
    private String billingTypeName = null;
    private String elementName = null;
    private String elementValue = null;
    private String rowCount = null;
    private String ptpPickupPoint = null;
    private String interimPoint = null;
    private String contractNo = null;
    private String customerCode = null;
    private String contractFrom = null;
    private String contractTo = null;
    private String[] ptpRouteContractCode = null;
    private String[] ptpMfrIds = null;
    private String[] ptpPickupPoints = null;
    private String[] interimPoint1 = null;
    private String[] interimPointId1 = null;
    private String[] interimPoint1Km = null;
    private String[] interimPoint2 = null;
    private String[] interimPointId2 = null;
    private String[] interimPoint2Km = null;
    private String[] interimPoint3 = null;
    private String[] interimPointId3 = null;
    private String[] interimPoint3Km = null;
    private String[] interimPoint4 = null;
    private String[] interimPointId4 = null;
    private String[] interimPoint4Km = null;
    private String[] ptpDropPoint = null;
    private String[] ptpDropPointId = null;
    private String[] ptpDropPointKm = null;
    private String[] ptpTotalKm = null;
    private String[] ptpRateWithReefer = null;
    private String[] ptpRateWithoutReefer = null;
    private String[] ptpwRouteContractCode = null;
    private String[] ptpwMfrIds = null;
    private String[] ptpwPickupPoint = null;
    private String[] ptpwDropPoint = null;
    private String[] ptpwPointId = null;
    private String[] ptpwTotalKm = null;
    private String[] ptpwRateWithReefer = null;
    private String[] ptpwRateWithoutReefer = null;
    private int contractId = 0;
    private String customerDetails = null;
    private String firstPickup = null;
    private String interimPickup1 = null;
    private String point1Id = null;
    private String point1Type = null;
    private String point2Type = null;
    private String point3Type = null;
    private String point4Type = null;
    private String point1Km = null;
    private String interimPickup2 = null;
    private String point2Id = null;
    private String point2Km = null;
    private String interimPickup3 = null;
    private String point3Id = null;
    private String point3Km = null;
    private String interimPickup4 = null;
    private String point4Id = null;
    private String point4Km = null;
    private String finalDrop = null;
    private String finalPointId = null;
    private String finalPointKm = null;
    private String rateWithReeferPerKm = null;
    private String rateWithoutReeferPerKm = null;
    private String rateWithReeferPerKg = null;
    private String rateWithoutReeferPerKg = null;
    private String reeferRupeesPerHour = "";
    private String vehicleRupeesPerKm = "";
    private String routeBillingId = null;
    private int routeContractId = 0;
    private String customerTypeId = null;
    private String customerTypeName = null;
    private String routeContractCode = null;
    private String interimPointName = null;
    private String cityId = null;
    private String countryId = null;
    private String countryName = null;
    private String currencySymbol = null;
    private String endDate = "";
    private String lastPrice = null;
    private String priceDiff = null;
    private String fuelPriceId = null;
    private String accountManagerName = null;
    private String routeFromId = null;
    private String routeToId = null;

    /* Consignment Note */
    private String entryType = null;
    private String consignmentNoteNo = null;
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = null;
    private String productCategoryId = null;
    private String tripclosureid = null;
    private String customerAddress = null;
    private String customerMobileNo = null;
    private String mailId = null;
    private String customerPhoneNo = null;
    private String walkinCustomerName = null;
    private String walkinCustomerCode = null;
    private String walkinCustomerAddress = null;
    private String walkinPincode = null;
    private String walkinCustomerMobileNo = null;
    private String walkinMailId = null;
    private String walkinCustomerPhoneNo = null;
    private String origin = null;
    private String businessType = null;
    private String multiPickup = null;
    private String multiDelivery = null;
    private String consignmentOrderInstruction = null;
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] productVolume = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String serviceType = null;
    private String reeferRequired = null;
    private String contractRateId = null;
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = null;
    private String consignorName = null;
    private String consignorPhoneNo = null;
    private String consignorAddress = null;
    private String consigneeName = null;
    private String consigneePhoneNo = null;
    private String consigneeAddress = null;
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String totalPackage = null;
    private String totalWeightage = "";
    private String totalVolumes = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String totalPoints = null;
    private String totFreightAmount = null;
    private String subTotal = null;
    private String totalCharges = null;
    private String docCharges = null;
    private String odaCharges = null;
    private String multiPickupCharge = null;
    private String multiDeliveryCharge = null;
    private String handleCharges = null;
    private String otherCharges = null;
    private String unloadingCharges = null;
    private String loadingCharges = null;
    private String standardChargeRemarks = null;
    private String walkInBillingTypeId = null;
    private String walkinFreightWithReefer = null;
    private String walkinFreightWithoutReefer = null;
    private String walkinRateWithReeferPerKg = null;
    private String walkinRateWithoutReeferPerKg = null;
    private String walkinRateWithReeferPerKm = null;
    private String walkinRateWithoutReeferPerKm = null;

    /* Consignment Note */
    /*Contract Edit */
    private String firstPickupId = null;
    private String firstPickupName = null;
    private String point1Name = "";
    private String point1Hrs = "";
    private String point1Minutes = "";
    private String point1RouteId = "";
    private String point2Name = "";
    private String point2Hrs = "";
    private String point2Minutes = "";
    private String point2RouteId = null;
    private String point3Name = null;
    private String point3Hrs = "";
    private String point3Minutes = "";
    private String point3RouteId = null;
    private String point4Name = null;
    private String point4Hrs = "";
    private String point4Minutes = "";
    private String point4RouteId = null;
    private String finalPointName = null;
    private String prevPointId = null;
    private String finalPointHrs = "";
    private String finalPointMinutes = "";
    private String finalPointRouteId = null;
    private String vehicleRatePerKm = "";
    private String reeferRatePerHour = "";
    private String activeInd = null;
    /*Contract Edit */
    private String consignmentOrderId = null;
    private String consignmentOrderDate = "";
    private String customerType = null;
    private String consigmentOrigin = null;
    private String consigmentDestination = null;
    private String[] pointId = null;
    private String[] pointName = null;
    private String[] pointType = null;
    private String[] order = null;
    private String[] pointAddresss = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanHour = null;
    private String[] pointPlanMinute = null;
    private String[] multiplePointRouteId = null;
    private String endPointId = null;
    private String finalRouteId = null;
    private String endPointName = null;
    private String endPointType = null;
    private String endOrder = null;
    private String endPointAddresss = "";
    private String endPointPlanDate = "";
    private String endPointPlanHour = "";
    private String endPointPlanMinute = "";
    private String tripcode = null;
    private String planneddate = "";
    private String estimatedrevenue = "";
    private String estimatedtransitday = "";
    private String estimatedadvanceperday = "";
    private String tobepaidtoday = "";
    private String advancerequestamt = null;
    private String requeststatus = null;
    private String requeston = null;
    private String requestby = null;
    private String requestremarks = null;
    private String tripid = null;
    private String tripday = null;
    private String paidamt = "";
    private String paidstatus = "";
    private String estimatedexpense = null;
    private String TripActualAmount = null;
    private String[] sellingCost = null;

    private String trailerId = null;
    private String trailerNo = null;
    private String jobCardFor = "";

//     gulshan-10/12/15
    private String currencyName = null;
    private String currencyId = null;

    private String slabName = null;
    private String slabId = null;
    private String maxTonnage = null;
    private String countryCode = null;

    private String alertTime = null;
    private String escalationAlertTime = null;
    private String vendorTypeId = "";
    //pavi
    private String dept = "";
    private String contactPerson = "";
    private String contractApprovalStatus = "";
    private String blockedAmount = "";
    private String advanceAmount = "";
    //kova
    private String version = "";
    private String igstOnIntra = "";
    private String regRev = "";
    private String taxSch = "";
    private String itemAssAount = "";
     private String workOrderNumber = "";

    public String getBlockedAmount() {
        return blockedAmount;
    }

    public void setBlockedAmount(String blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    public String getContractApprovalStatus() {
        return contractApprovalStatus;
    }

    public void setContractApprovalStatus(String contractApprovalStatus) {
        this.contractApprovalStatus = contractApprovalStatus;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

// over
    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getTripActualAmount() {
        return TripActualAmount;
    }

    public void setTripActualAmount(String TripActualAmount) {
        this.TripActualAmount = TripActualAmount;
    }

    public String getEstimatedexpense() {
        return estimatedexpense;
    }

    public void setEstimatedexpense(String estimatedexpense) {
        this.estimatedexpense = estimatedexpense;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getPaidstatus() {
        return paidstatus;
    }

    public void setPaidstatus(String paidstatus) {
        this.paidstatus = paidstatus;
    }

    public String getTripday() {
        return tripday;
    }

    public void setTripday(String tripday) {
        this.tripday = tripday;
    }

    public String getTripid() {
        return tripid;
    }

    public void setTripid(String tripid) {
        this.tripid = tripid;
    }

    public String getAdvancerequestamt() {
        return advancerequestamt;
    }

    public void setAdvancerequestamt(String advancerequestamt) {
        this.advancerequestamt = advancerequestamt;
    }

    public String getRequestby() {
        return requestby;
    }

    public void setRequestby(String requestby) {
        this.requestby = requestby;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }

    public String getTobepaidtoday() {
        return tobepaidtoday;
    }

    public void setTobepaidtoday(String tobepaidtoday) {
        this.tobepaidtoday = tobepaidtoday;
    }

    public String getEstimatedadvanceperday() {
        return estimatedadvanceperday;
    }

    public void setEstimatedadvanceperday(String estimatedadvanceperday) {
        this.estimatedadvanceperday = estimatedadvanceperday;
    }

    public String getEstimatedtransitday() {
        return estimatedtransitday;
    }

    public void setEstimatedtransitday(String estimatedtransitday) {
        this.estimatedtransitday = estimatedtransitday;
    }

    public String getEstimatedrevenue() {
        return estimatedrevenue;
    }

    public void setEstimatedrevenue(String estimatedrevenue) {
        this.estimatedrevenue = estimatedrevenue;
    }

    public String getPlanneddate() {
        return planneddate;
    }

    public void setPlanneddate(String planneddate) {
        this.planneddate = planneddate;
    }

    public String getTripcode() {
        return tripcode;
    }

    public void setTripcode(String tripcode) {
        this.tripcode = tripcode;
    }

//    Brattle Foods
    public String getServiceVendor() {
        return serviceVendor;
    }

    public void setServiceVendor(String serviceVendor) {
        this.serviceVendor = serviceVendor;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getInDateTime() {
        return inDateTime;
    }

    public void setInDateTime(String inDateTime) {
        this.inDateTime = inDateTime;
    }

    public String getInKM() {
        return inKM;
    }

    public void setInKM(String inKM) {
        this.inKM = inKM;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOutDateTime() {
        return outDateTime;
    }

    public void setOutDateTime(String outDateTime) {
        this.outDateTime = outDateTime;
    }

    public String getOutKM() {
        return outKM;
    }

    public void setOutKM(String outKM) {
        this.outKM = outKM;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public int getCustomertypeId() {
        return customertypeId;
    }

    public void setCustomertypeId(int customertypeId) {
        this.customertypeId = customertypeId;
    }

    public String getCustomertypeName() {
        return customertypeName;
    }

    public void setCustomertypeName(String customertypeName) {
        this.customertypeName = customertypeName;
    }
//   bala ends

    public int getBillNo() {
        return billNo;
    }

    public void setBillNo(int billNo) {
        this.billNo = billNo;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getIssQty() {
        return issQty;
    }

    public void setIssQty(int issQty) {
        this.issQty = issQty;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getMrsId() {
        return mrsId;
    }

    public void setMrsId(int mrsId) {
        this.mrsId = mrsId;
    }

    public int getReqQty() {
        return reqQty;
    }

    public void setReqQty(int reqQty) {
        this.reqQty = reqQty;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getBalanceHm() {
        return balanceHm;
    }

    public void setBalanceHm(int balanceHm) {
        this.balanceHm = balanceHm;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getServicedDate() {
        return servicedDate;
    }

    public void setServicedDate(String servicedDate) {
        this.servicedDate = servicedDate;
    }

    public int getCurrentHM() {
        return currentHM;
    }

    public void setCurrentHM(int currentHM) {
        this.currentHM = currentHM;
    }

    public int getHourMeter() {
        return hourMeter;
    }

    public void setHourMeter(int hourMeter) {
        this.hourMeter = hourMeter;
    }

    public int getCurrentKM() {
        return currentKM;
    }

    public void setCurrentKM(int currentKM) {
        this.currentKM = currentKM;
    }

    public String getIdentifiedby() {
        return identifiedby;
    }

    public void setIdentifiedby(String identifiedby) {
        this.identifiedby = identifiedby;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public int getSecId() {
        return secId;
    }

    public void setSecId(int secId) {
        this.secId = secId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getProbId() {
        return probId;
    }

    public void setProbId(int probId) {
        this.probId = probId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getBalanceKm() {
        return balanceKm;
    }

    public void setBalanceKm(int balanceKm) {
        this.balanceKm = balanceKm;
    }

    public int getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(int jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getScheduledDeleivery() {
        return scheduledDeleivery;
    }

    public void setScheduledDeleivery(String scheduledDeleivery) {
        this.scheduledDeleivery = scheduledDeleivery;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProbName() {
        return probName;
    }

    public void setProbName(String probName) {
        this.probName = probName;
    }

    public String getSecName() {
        return secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getChassNo() {
        return chassNo;
    }

    public void setChassNo(String chassNo) {
        this.chassNo = chassNo;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public int getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(int elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public int getServicetypeId() {
        return servicetypeId;
    }

    public void setServicetypeId(int servicetypeId) {
        this.servicetypeId = servicetypeId;
    }

    public String getServicetypeName() {
        return servicetypeName;
    }

    public void setServicetypeName(String servicetypeName) {
        this.servicetypeName = servicetypeName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getKmReading() {
        return kmReading;
    }

    public void setKmReading(int kmReading) {
        this.kmReading = kmReading;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public int getServiceLocationId() {
        return serviceLocationId;
    }

    public void setServiceLocationId(int serviceLocationId) {
        this.serviceLocationId = serviceLocationId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPriority1() {
        return priority1;
    }

    public void setPriority1(String priority1) {
        this.priority1 = priority1;
    }

    public String getJcMYFormatNo() {
        return jcMYFormatNo;
    }

    public void setJcMYFormatNo(String jcMYFormatNo) {
        this.jcMYFormatNo = jcMYFormatNo;
    }

    public String getLastServicedKm() {
        return lastServicedKm;
    }

    public void setLastServicedKm(String lastServicedKm) {
        this.lastServicedKm = lastServicedKm;
    }

    public int getWoId() {
        return woId;
    }

    public void setWoId(int woId) {
        this.woId = woId;
    }

    public String[] getAddress() {
        return address;
    }

    public void setAddress(String[] address) {
        this.address = address;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getWoComp() {
        return woComp;
    }

    public void setWoComp(String woComp) {
        this.woComp = woComp;
    }

    public ArrayList getActivity() {
        return activity;
    }

    public void setActivity(ArrayList activity) {
        this.activity = activity;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String[] getProblemNameSplit() {
        return problemNameSplit;
    }

    public void setProblemNameSplit(String[] problemNameSplit) {
        this.problemNameSplit = problemNameSplit;
    }

    public String[] getSymptomsSplit() {
        return symptomsSplit;
    }

    public void setSymptomsSplit(String[] symptomsSplit) {
        this.symptomsSplit = symptomsSplit;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getServiceLocation() {
        return serviceLocation;
    }

    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public String getcRemarks() {
        return cRemarks;
    }

    public void setcRemarks(String cRemarks) {
        this.cRemarks = cRemarks;
    }

    public String getBayNo() {
        return bayNo;
    }

    public void setBayNo(String bayNo) {
        this.bayNo = bayNo;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getActualHrs() {
        return actualHrs;
    }

    public void setActualHrs(String actualHrs) {
        this.actualHrs = actualHrs;
    }

    public String getEstimatedHrs() {
        return estimatedHrs;
    }

    public void setEstimatedHrs(String estimatedHrs) {
        this.estimatedHrs = estimatedHrs;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public String getTripArrivalDate() {
        return tripArrivalDate;
    }

    public void setTripArrivalDate(String tripArrivalDate) {
        this.tripArrivalDate = tripArrivalDate;
    }

    public String getTripBalanceAmount() {
        return tripBalanceAmount;
    }

    public void setTripBalanceAmount(String tripBalanceAmount) {
        this.tripBalanceAmount = tripBalanceAmount;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripDepartureDate() {
        return tripDepartureDate;
    }

    public void setTripDepartureDate(String tripDepartureDate) {
        this.tripDepartureDate = tripDepartureDate;
    }

    public String getTripDriverId() {
        return tripDriverId;
    }

    public void setTripDriverId(String tripDriverId) {
        this.tripDriverId = tripDriverId;
    }

    public String getTripFuelAmount() {
        return tripFuelAmount;
    }

    public void setTripFuelAmount(String tripFuelAmount) {
        this.tripFuelAmount = tripFuelAmount;
    }

    public String getTripKmsIn() {
        return tripKmsIn;
    }

    public void setTripKmsIn(String tripKmsIn) {
        this.tripKmsIn = tripKmsIn;
    }

    public String getTripKmsOut() {
        return tripKmsOut;
    }

    public void setTripKmsOut(String tripKmsOut) {
        this.tripKmsOut = tripKmsOut;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripScheduleId() {
        return tripScheduleId;
    }

    public void setTripScheduleId(String tripScheduleId) {
        this.tripScheduleId = tripScheduleId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripTotalExpenses() {
        return tripTotalExpenses;
    }

    public void setTripTotalExpenses(String tripTotalExpenses) {
        this.tripTotalExpenses = tripTotalExpenses;
    }

    public String getTripTotalKms() {
        return tripTotalKms;
    }

    public void setTripTotalKms(String tripTotalKms) {
        this.tripTotalKms = tripTotalKms;
    }

    public String getTripTotalLitres() {
        return tripTotalLitres;
    }

    public void setTripTotalLitres(String tripTotalLitres) {
        this.tripTotalLitres = tripTotalLitres;
    }

    public String getTripVehicleId() {
        return tripVehicleId;
    }

    public void setTripVehicleId(String tripVehicleId) {
        this.tripVehicleId = tripVehicleId;
    }

    public String getTripTotalAllowances() {
        return tripTotalAllowances;
    }

    public void setTripTotalAllowances(String tripTotalAllowances) {
        this.tripTotalAllowances = tripTotalAllowances;
    }

    public String getAdvDatetime() {
        return advDatetime;
    }

    public void setAdvDatetime(String advDatetime) {
        this.advDatetime = advDatetime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFuelDatetime() {
        return fuelDatetime;
    }

    public void setFuelDatetime(String fuelDatetime) {
        this.fuelDatetime = fuelDatetime;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String getInOutDateTime() {
        return inOutDateTime;
    }

    public void setInOutDateTime(String inOutDateTime) {
        this.inOutDateTime = inOutDateTime;
    }

    public String getInOutIndication() {
        return inOutIndication;
    }

    public void setInOutIndication(String inOutIndication) {
        this.inOutIndication = inOutIndication;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getLiters() {
        return liters;
    }

    public void setLiters(String liters) {
        this.liters = liters;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getSettlementFlag() {
        return settlementFlag;
    }

    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }

    public String getDeliveredTonnage() {
        return deliveredTonnage;
    }

    public void setDeliveredTonnage(String deliveredTonnage) {
        this.deliveredTonnage = deliveredTonnage;
    }

    public String getTotalTonnage() {
        return totalTonnage;
    }

    public void setTotalTonnage(String totalTonnage) {
        this.totalTonnage = totalTonnage;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getAlightDate() {
        return alightDate;
    }

    public void setAlightDate(String alightDate) {
        this.alightDate = alightDate;
    }

    public String getAlightStatus() {
        return alightStatus;
    }

    public void setAlightStatus(String alightStatus) {
        this.alightStatus = alightStatus;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getExpensesDesc() {
        return expensesDesc;
    }

    public void setExpensesDesc(String expensesDesc) {
        this.expensesDesc = expensesDesc;
    }

    public String getEmbarkDate() {
        return embarkDate;
    }

    public void setEmbarkDate(String embarkDate) {
        this.embarkDate = embarkDate;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getTotalTonAmount() {
        return totalTonAmount;
    }

    public void setTotalTonAmount(String totalTonAmount) {
        this.totalTonAmount = totalTonAmount;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(String issuerId) {
        this.issuerId = issuerId;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCleanerStatus() {
        return cleanerStatus;
    }

    public void setCleanerStatus(String cleanerStatus) {
        this.cleanerStatus = cleanerStatus;
    }

    public String getArrHours() {
        return arrHours;
    }

    public void setArrHours(String arrHours) {
        this.arrHours = arrHours;
    }

    public String getArrMints() {
        return arrMints;
    }

    public void setArrMints(String arrMints) {
        this.arrMints = arrMints;
    }

    public String getDepHours() {
        return depHours;
    }

    public void setDepHours(String depHours) {
        this.depHours = depHours;
    }

    public String getDepMints() {
        return depMints;
    }

    public void setDepMints(String depMints) {
        this.depMints = depMints;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public ArrayList getPeriodicServiceList() {
        return periodicServiceList;
    }

    public void setPeriodicServiceList(ArrayList periodicServiceList) {
        this.periodicServiceList = periodicServiceList;
    }

    public String getFlagString() {
        return flagString;
    }

    public void setFlagString(String flagString) {
        this.flagString = flagString;
    }

    public String getTotalFuel() {
        return totalFuel;
    }

    public void setTotalFuel(String totalFuel) {
        this.totalFuel = totalFuel;
    }

    public String getTotalFuelAmount() {
        return totalFuelAmount;
    }

    public void setTotalFuelAmount(String totalFuelAmount) {
        this.totalFuelAmount = totalFuelAmount;
    }

    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPinkSlip() {
        return pinkSlip;
    }

    public void setPinkSlip(String pinkSlip) {
        this.pinkSlip = pinkSlip;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getActive_ind() {
        return active_ind;
    }

    public void setActive_ind(String active_ind) {
        this.active_ind = active_ind;
    }

    public String getClplPriority() {
        return clplPriority;
    }

    public void setClplPriority(String clplPriority) {
        this.clplPriority = clplPriority;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getGatePassNo() {
        return gatePassNo;
    }

    public void setGatePassNo(String gatePassNo) {
        this.gatePassNo = gatePassNo;
    }

    public String getLorryNo() {
        return lorryNo;
    }

    public void setLorryNo(String lorryNo) {
        this.lorryNo = lorryNo;
    }

    public String getLpsDate() {
        return lpsDate;
    }

    public void setLpsDate(String lpsDate) {
        this.lpsDate = lpsDate;
    }

    public String getLpsID() {
        return lpsID;
    }

    public void setLpsID(String lpsID) {
        this.lpsID = lpsID;
    }

    public String getLpsNo() {
        return lpsNo;
    }

    public void setLpsNo(String lpsNo) {
        this.lpsNo = lpsNo;
    }

    public String getLpsNumber() {
        return lpsNumber;
    }

    public void setLpsNumber(String lpsNumber) {
        this.lpsNumber = lpsNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPackerNumber() {
        return packerNumber;
    }

    public void setPackerNumber(String packerNumber) {
        this.packerNumber = packerNumber;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getWatchandward() {
        return watchandward;
    }

    public void setWatchandward(String watchandward) {
        this.watchandward = watchandward;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getPinkSlipID() {
        return pinkSlipID;
    }

    public void setPinkSlipID(String pinkSlipID) {
        this.pinkSlipID = pinkSlipID;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getSelectedLpsIds() {
        return selectedLpsIds;
    }

    public void setSelectedLpsIds(String selectedLpsIds) {
        this.selectedLpsIds = selectedLpsIds;
    }

    public String getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getAct_Ind() {
        return act_Ind;
    }

    public void setAct_Ind(String act_Ind) {
        this.act_Ind = act_Ind;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }

    public String getCloseDateTime() {
        return closeDateTime;
    }

    public void setCloseDateTime(String closeDateTime) {
        this.closeDateTime = closeDateTime;
    }

    public String getCust_Name() {
        return cust_Name;
    }

    public void setCust_Name(String cust_Name) {
        this.cust_Name = cust_Name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEmp_Name() {
        return emp_Name;
    }

    public void setEmp_Name(String emp_Name) {
        this.emp_Name = emp_Name;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getNoOfTrip() {
        return noOfTrip;
    }

    public void setNoOfTrip(String noOfTrip) {
        this.noOfTrip = noOfTrip;
    }

    public String getOpenDateTime() {
        return openDateTime;
    }

    public void setOpenDateTime(String openDateTime) {
        this.openDateTime = openDateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTotalInvAmount() {
        return totalInvAmount;
    }

    public void setTotalInvAmount(String totalInvAmount) {
        this.totalInvAmount = totalInvAmount;
    }

    public String getTotalallowance() {
        return totalallowance;
    }

    public void setTotalallowance(String totalallowance) {
        this.totalallowance = totalallowance;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotalexpenses() {
        return totalexpenses;
    }

    public void setTotalexpenses(String totalexpenses) {
        this.totalexpenses = totalexpenses;
    }

    public String getTotalkms() {
        return totalkms;
    }

    public void setTotalkms(String totalkms) {
        this.totalkms = totalkms;
    }

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getLedgerCode() {
        return LedgerCode;
    }

    public void setLedgerCode(String LedgerCode) {
        this.LedgerCode = LedgerCode;
    }

    public String getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(String commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(String commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getTotalDeliveredTonnage() {
        return totalDeliveredTonnage;
    }

    public void setTotalDeliveredTonnage(String totalDeliveredTonnage) {
        this.totalDeliveredTonnage = totalDeliveredTonnage;
    }

    public String getTotalTannage() {
        return totalTannage;
    }

    public void setTotalTannage(String totalTannage) {
        this.totalTannage = totalTannage;
    }

    public String getTotalregno() {
        return totalregno;
    }

    public void setTotalregno(String totalregno) {
        this.totalregno = totalregno;
    }

    public String getTotalshortage() {
        return totalshortage;
    }

    public void setTotalshortage(String totalshortage) {
        this.totalshortage = totalshortage;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceFor() {
        return invoiceFor;
    }

    public void setInvoiceFor(String invoiceFor) {
        this.invoiceFor = invoiceFor;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTri() {
        return numberOfTri;
    }

    public void setNumberOfTri(String numberOfTri) {
        this.numberOfTri = numberOfTri;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getTripNo() {
        return tripNo;
    }

    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    public String getCommisssionAmount() {
        return commisssionAmount;
    }

    public void setCommisssionAmount(String commisssionAmount) {
        this.commisssionAmount = commisssionAmount;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getSettlementid() {
        return settlementid;
    }

    public void setSettlementid(String settlementid) {
        this.settlementid = settlementid;
    }

    public String getTotalAmountL() {
        return totalAmountL;
    }

    public void setTotalAmountL(String totalAmountL) {
        this.totalAmountL = totalAmountL;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getTotalamaount() {
        return totalamaount;
    }

    public void setTotalamaount(String totalamaount) {
        this.totalamaount = totalamaount;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getGPSKm() {
        return GPSKm;
    }

    public void setGPSKm(String GPSKm) {
        this.GPSKm = GPSKm;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnFromLocation() {
        return returnFromLocation;
    }

    public void setReturnFromLocation(String returnFromLocation) {
        this.returnFromLocation = returnFromLocation;
    }

    public String getReturnToLocation() {
        return returnToLocation;
    }

    public void setReturnToLocation(String returnToLocation) {
        this.returnToLocation = returnToLocation;
    }

    public String getReturnTonnage() {
        return returnTonnage;
    }

    public void setReturnTonnage(String returnTonnage) {
        this.returnTonnage = returnTonnage;
    }

    public String getReturnTripProduct() {
        return returnTripProduct;
    }

    public void setReturnTripProduct(String returnTripProduct) {
        this.returnTripProduct = returnTripProduct;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getReturnExpenses() {
        return returnExpenses;
    }

    public void setReturnExpenses(String returnExpenses) {
        this.returnExpenses = returnExpenses;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getSelectedBillStatus() {
        return selectedBillStatus;
    }

    public void setSelectedBillStatus(String selectedBillStatus) {
        this.selectedBillStatus = selectedBillStatus;
    }

    public String getSelectedRouteId() {
        return selectedRouteId;
    }

    public void setSelectedRouteId(String selectedRouteId) {
        this.selectedRouteId = selectedRouteId;
    }

    public String getTripReceivedAmount() {
        return tripReceivedAmount;
    }

    public void setTripReceivedAmount(String tripReceivedAmount) {
        this.tripReceivedAmount = tripReceivedAmount;
    }

    public String getReturnDeliveredDate() {
        return returnDeliveredDate;
    }

    public void setReturnDeliveredDate(String returnDeliveredDate) {
        this.returnDeliveredDate = returnDeliveredDate;
    }

    public String getReturnDeliveredTonnage() {
        return returnDeliveredTonnage;
    }

    public void setReturnDeliveredTonnage(String returnDeliveredTonnage) {
        this.returnDeliveredTonnage = returnDeliveredTonnage;
    }

    public String getReturnLoadedDate() {
        return returnLoadedDate;
    }

    public void setReturnLoadedDate(String returnLoadedDate) {
        this.returnLoadedDate = returnLoadedDate;
    }

    public String getReturnLoadedTonnage() {
        return returnLoadedTonnage;
    }

    public void setReturnLoadedTonnage(String returnLoadedTonnage) {
        this.returnLoadedTonnage = returnLoadedTonnage;
    }

    public String getShortageTonnage() {
        return shortageTonnage;
    }

    public void setShortageTonnage(String shortageTonnage) {
        this.shortageTonnage = shortageTonnage;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getTripdateP() {
        return tripdateP;
    }

    public void setTripdateP(String tripdateP) {
        this.tripdateP = tripdateP;
    }

    public String getTolocation() {
        return tolocation;
    }

    public void setTolocation(String tolocation) {
        this.tolocation = tolocation;
    }

    public String getTotalBalAmtRetuen() {
        return totalBalAmtRetuen;
    }

    public void setTotalBalAmtRetuen(String totalBalAmtRetuen) {
        this.totalBalAmtRetuen = totalBalAmtRetuen;
    }

    public String getTotalExpensesAmtReturn() {
        return totalExpensesAmtReturn;
    }

    public void setTotalExpensesAmtReturn(String totalExpensesAmtReturn) {
        this.totalExpensesAmtReturn = totalExpensesAmtReturn;
    }

    public String getReturnAmt() {
        return returnAmt;
    }

    public void setReturnAmt(String returnAmt) {
        this.returnAmt = returnAmt;
    }

    public String getReturnExp() {
        return returnExp;
    }

    public void setReturnExp(String returnExp) {
        this.returnExp = returnExp;
    }

    public String getTotalBalAmtRt() {
        return totalBalAmtRt;
    }

    public void setTotalBalAmtRt(String totalBalAmtRt) {
        this.totalBalAmtRt = totalBalAmtRt;
    }

    public String getTotalExpensesAmtRt() {
        return totalExpensesAmtRt;
    }

    public void setTotalExpensesAmtRt(String totalExpensesAmtRt) {
        this.totalExpensesAmtRt = totalExpensesAmtRt;
    }

    public String getTonnageRate() {
        return tonnageRate;
    }

    public void setTonnageRate(String tonnageRate) {
        this.tonnageRate = tonnageRate;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getReturnKM() {
        return returnKM;
    }

    public void setReturnKM(String returnKM) {
        this.returnKM = returnKM;
    }

    public String getReturnProductName() {
        return returnProductName;
    }

    public void setReturnProductName(String returnProductName) {
        this.returnProductName = returnProductName;
    }

    public String getReturnTon() {
        return returnTon;
    }

    public void setReturnTon(String returnTon) {
        this.returnTon = returnTon;
    }

    public String getReturnTripType() {
        return returnTripType;
    }

    public void setReturnTripType(String returnTripType) {
        this.returnTripType = returnTripType;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getReturnTripId() {
        return returnTripId;
    }

    public void setReturnTripId(String returnTripId) {
        this.returnTripId = returnTripId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFromLocationid() {
        return fromLocationid;
    }

    public void setFromLocationid(String fromLocationid) {
        this.fromLocationid = fromLocationid;
    }

    public String getToLocationid() {
        return toLocationid;
    }

    public void setToLocationid(String toLocationid) {
        this.toLocationid = toLocationid;
    }

    public String getDeliveredDANo() {
        return deliveredDANo;
    }

    public void setDeliveredDANo(String deliveredDANo) {
        this.deliveredDANo = deliveredDANo;
    }

    public String getLoadedSlipNo() {
        return loadedSlipNo;
    }

    public void setLoadedSlipNo(String loadedSlipNo) {
        this.loadedSlipNo = loadedSlipNo;
    }

    public String getLoadedTonnage() {
        return loadedTonnage;
    }

    public void setLoadedTonnage(String loadedTonnage) {
        this.loadedTonnage = loadedTonnage;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getDriverVendorId() {
        return driverVendorId;
    }

    public void setDriverVendorId(String driverVendorId) {
        this.driverVendorId = driverVendorId;
    }

    public String getVehicleVendorId() {
        return vehicleVendorId;
    }

    public void setVehicleVendorId(String vehicleVendorId) {
        this.vehicleVendorId = vehicleVendorId;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getfLocationId() {
        return fLocationId;
    }

    public void setfLocationId(String fLocationId) {
        this.fLocationId = fLocationId;
    }

    public String getfLocationName() {
        return fLocationName;
    }

    public void setfLocationName(String fLocationName) {
        this.fLocationName = fLocationName;
    }

    public String gettLocationId() {
        return tLocationId;
    }

    public void settLocationId(String tLocationId) {
        this.tLocationId = tLocationId;
    }

    public String gettLocationName() {
        return tLocationName;
    }

    public void settLocationName(String tLocationName) {
        this.tLocationName = tLocationName;
    }

    public String getAccountDetail() {
        return accountDetail;
    }

    public void setAccountDetail(String accountDetail) {
        this.accountDetail = accountDetail;
    }

    public String getAccountEntryDate() {
        return accountEntryDate;
    }

    public void setAccountEntryDate(String accountEntryDate) {
        this.accountEntryDate = accountEntryDate;
    }

    public String getAccountEntryID() {
        return accountEntryID;
    }

    public void setAccountEntryID(String accountEntryID) {
        this.accountEntryID = accountEntryID;
    }

    public String getAccountsAmount() {
        return accountsAmount;
    }

    public void setAccountsAmount(String accountsAmount) {
        this.accountsAmount = accountsAmount;
    }

    public String getAccountsType() {
        return accountsType;
    }

    public void setAccountsType(String accountsType) {
        this.accountsType = accountsType;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getvDate() {
        return vDate;
    }

    public void setvDate(String vDate) {
        this.vDate = vDate;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getGpno() {
        return gpno;
    }

    public void setGpno(String gpno) {
        this.gpno = gpno;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getBranchLedgerName() {
        return branchLedgerName;
    }

    public void setBranchLedgerName(String branchLedgerName) {
        this.branchLedgerName = branchLedgerName;
    }

    public String getDriverLedgerName() {
        return driverLedgerName;
    }

    public void setDriverLedgerName(String driverLedgerName) {
        this.driverLedgerName = driverLedgerName;
    }

    public String getTotalBags() {
        return totalBags;
    }

    public void setTotalBags(String totalBags) {
        this.totalBags = totalBags;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(String selectedRow) {
        this.selectedRow = selectedRow;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public String getCrossingAmount() {
        return crossingAmount;
    }

    public void setCrossingAmount(String crossingAmount) {
        this.crossingAmount = crossingAmount;
    }

    public String getCrossingType() {
        return crossingType;
    }

    public void setCrossingType(String crossingType) {
        this.crossingType = crossingType;
    }

    public String getTonnageRateMarket() {
        return tonnageRateMarket;
    }

    public void setTonnageRateMarket(String tonnageRateMarket) {
        this.tonnageRateMarket = tonnageRateMarket;
    }

    public String getTwoLpsStatus() {
        return twoLpsStatus;
    }

    public void setTwoLpsStatus(String twoLpsStatus) {
        this.twoLpsStatus = twoLpsStatus;
    }

    public String getTripRevenueOne() {
        return tripRevenueOne;
    }

    public void setTripRevenueOne(String tripRevenueOne) {
        this.tripRevenueOne = tripRevenueOne;
    }

    public String getTripRevenueTwo() {
        return tripRevenueTwo;
    }

    public void setTripRevenueTwo(String tripRevenueTwo) {
        this.tripRevenueTwo = tripRevenueTwo;
    }

    public String getSelectedRouteName() {
        return selectedRouteName;
    }

    public void setSelectedRouteName(String selectedRouteName) {
        this.selectedRouteName = selectedRouteName;
    }

    public String getSelectedPacking() {
        return selectedPacking;
    }

    public void setSelectedPacking(String selectedPacking) {
        this.selectedPacking = selectedPacking;
    }

    public String getSelectedProductName() {
        return selectedProductName;
    }

    public void setSelectedProductName(String selectedProductName) {
        this.selectedProductName = selectedProductName;
    }

    public String getBillStatusNew() {
        return billStatusNew;
    }

    public void setBillStatusNew(String billStatusNew) {
        this.billStatusNew = billStatusNew;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getReeferRunning() {
        return reeferRunning;
    }

    public void setReeferRunning(String reeferRunning) {
        this.reeferRunning = reeferRunning;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getModelIds() {
        return modelIds;
    }

    public void setModelIds(String[] modelIds) {
        this.modelIds = modelIds;
    }

    public String[] getVehMileage() {
        return vehMileage;
    }

    public void setVehMileage(String[] vehMileage) {
        this.vehMileage = vehMileage;
    }

    public String[] getVehExpense() {
        return vehExpense;
    }

    public void setVehExpense(String[] vehExpense) {
        this.vehExpense = vehExpense;
    }

    public String[] getReeferConsump() {
        return reeferConsump;
    }

    public void setReeferConsump(String[] reeferConsump) {
        this.reeferConsump = reeferConsump;
    }

    public String[] getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String[] reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String[] getVarRate() {
        return varRate;
    }

    public void setVarRate(String[] varRate) {
        this.varRate = varRate;
    }

    public String[] getVarCost() {
        return varCost;
    }

    public void setVarCost(String[] varCost) {
        this.varCost = varCost;
    }

    public String[] getTotExpense() {
        return totExpense;
    }

    public void setTotExpense(String[] totExpense) {
        this.totExpense = totExpense;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public String getRouteIdFrom() {
        return routeIdFrom;
    }

    public void setRouteIdFrom(String routeIdFrom) {
        this.routeIdFrom = routeIdFrom;
    }

    public String getRouteIdTo() {
        return routeIdTo;
    }

    public void setRouteIdTo(String routeIdTo) {
        this.routeIdTo = routeIdTo;
    }

    public String getEditRouteId() {
        return editRouteId;
    }

    public void setEditRouteId(String editRouteId) {
        this.editRouteId = editRouteId;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getRouteFrom() {
        return routeFrom;
    }

    public void setRouteFrom(String routeFrom) {
        this.routeFrom = routeFrom;
    }

    public String getRouteTo() {
        return routeTo;
    }

    public void setRouteTo(String routeTo) {
        this.routeTo = routeTo;
    }

    public String getVehicleExpense() {
        return vehicleExpense;
    }

    public void setVehicleExpense(String vehicleExpense) {
        this.vehicleExpense = vehicleExpense;
    }

    public String getReeferExpenses() {
        return reeferExpenses;
    }

    public void setReeferExpenses(String reeferExpenses) {
        this.reeferExpenses = reeferExpenses;
    }

    public String getVariableCostPerKm() {
        return variableCostPerKm;
    }

    public void setVariableCostPerKm(String variableCostPerKm) {
        this.variableCostPerKm = variableCostPerKm;
    }

    public String getVariableExpense() {
        return variableExpense;
    }

    public void setVariableExpense(String variableExpense) {
        this.variableExpense = variableExpense;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getStChargeName() {
        return stChargeName;
    }

    public void setStChargeName(String stChargeName) {
        this.stChargeName = stChargeName;
    }

    public String getStChargeDesc() {
        return stChargeDesc;
    }

    public void setStChargeDesc(String stChargeDesc) {
        this.stChargeDesc = stChargeDesc;
    }

    public String getStChargeUnit() {
        return stChargeUnit;
    }

    public void setStChargeUnit(String stChargeUnit) {
        this.stChargeUnit = stChargeUnit;
    }

    public String getStChargeId() {
        return stChargeId;
    }

    public void setStChargeId(String stChargeId) {
        this.stChargeId = stChargeId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementValue() {
        return elementValue;
    }

    public void setElementValue(String elementValue) {
        this.elementValue = elementValue;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public String getInterimPoint() {
        return interimPoint;
    }

    public void setInterimPoint(String interimPoint) {
        this.interimPoint = interimPoint;
    }

    public String getPtpPickupPoint() {
        return ptpPickupPoint;
    }

    public void setPtpPickupPoint(String ptpPickupPoint) {
        this.ptpPickupPoint = ptpPickupPoint;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getContractFrom() {
        return contractFrom;
    }

    public void setContractFrom(String contractFrom) {
        this.contractFrom = contractFrom;
    }

    public String getContractTo() {
        return contractTo;
    }

    public void setContractTo(String contractTo) {
        this.contractTo = contractTo;
    }

    public String[] getPtpRouteContractCode() {
        return ptpRouteContractCode;
    }

    public void setPtpRouteContractCode(String[] ptpRouteContractCode) {
        this.ptpRouteContractCode = ptpRouteContractCode;
    }

    public String[] getPtpMfrIds() {
        return ptpMfrIds;
    }

    public void setPtpMfrIds(String[] ptpMfrIds) {
        this.ptpMfrIds = ptpMfrIds;
    }

    public String[] getInterimPoint1() {
        return interimPoint1;
    }

    public void setInterimPoint1(String[] interimPoint1) {
        this.interimPoint1 = interimPoint1;
    }

    public String[] getInterimPointId1() {
        return interimPointId1;
    }

    public void setInterimPointId1(String[] interimPointId1) {
        this.interimPointId1 = interimPointId1;
    }

    public String[] getInterimPoint1Km() {
        return interimPoint1Km;
    }

    public void setInterimPoint1Km(String[] interimPoint1Km) {
        this.interimPoint1Km = interimPoint1Km;
    }

    public String[] getInterimPoint2() {
        return interimPoint2;
    }

    public void setInterimPoint2(String[] interimPoint2) {
        this.interimPoint2 = interimPoint2;
    }

    public String[] getInterimPointId2() {
        return interimPointId2;
    }

    public void setInterimPointId2(String[] interimPointId2) {
        this.interimPointId2 = interimPointId2;
    }

    public String[] getInterimPoint2Km() {
        return interimPoint2Km;
    }

    public void setInterimPoint2Km(String[] interimPoint2Km) {
        this.interimPoint2Km = interimPoint2Km;
    }

    public String[] getInterimPoint3() {
        return interimPoint3;
    }

    public void setInterimPoint3(String[] interimPoint3) {
        this.interimPoint3 = interimPoint3;
    }

    public String[] getInterimPointId3() {
        return interimPointId3;
    }

    public void setInterimPointId3(String[] interimPointId3) {
        this.interimPointId3 = interimPointId3;
    }

    public String[] getInterimPoint3Km() {
        return interimPoint3Km;
    }

    public void setInterimPoint3Km(String[] interimPoint3Km) {
        this.interimPoint3Km = interimPoint3Km;
    }

    public String[] getInterimPoint4() {
        return interimPoint4;
    }

    public void setInterimPoint4(String[] interimPoint4) {
        this.interimPoint4 = interimPoint4;
    }

    public String[] getInterimPointId4() {
        return interimPointId4;
    }

    public void setInterimPointId4(String[] interimPointId4) {
        this.interimPointId4 = interimPointId4;
    }

    public String[] getInterimPoint4Km() {
        return interimPoint4Km;
    }

    public void setInterimPoint4Km(String[] interimPoint4Km) {
        this.interimPoint4Km = interimPoint4Km;
    }

    public String[] getPtpDropPoint() {
        return ptpDropPoint;
    }

    public void setPtpDropPoint(String[] ptpDropPoint) {
        this.ptpDropPoint = ptpDropPoint;
    }

    public String[] getPtpDropPointId() {
        return ptpDropPointId;
    }

    public void setPtpDropPointId(String[] ptpDropPointId) {
        this.ptpDropPointId = ptpDropPointId;
    }

    public String[] getPtpDropPointKm() {
        return ptpDropPointKm;
    }

    public void setPtpDropPointKm(String[] ptpDropPointKm) {
        this.ptpDropPointKm = ptpDropPointKm;
    }

    public String[] getPtpTotalKm() {
        return ptpTotalKm;
    }

    public void setPtpTotalKm(String[] ptpTotalKm) {
        this.ptpTotalKm = ptpTotalKm;
    }

    public String[] getPtpRateWithReefer() {
        return ptpRateWithReefer;
    }

    public void setPtpRateWithReefer(String[] ptpRateWithReefer) {
        this.ptpRateWithReefer = ptpRateWithReefer;
    }

    public String[] getPtpRateWithoutReefer() {
        return ptpRateWithoutReefer;
    }

    public void setPtpRateWithoutReefer(String[] ptpRateWithoutReefer) {
        this.ptpRateWithoutReefer = ptpRateWithoutReefer;
    }

    public String[] getPtpwRouteContractCode() {
        return ptpwRouteContractCode;
    }

    public void setPtpwRouteContractCode(String[] ptpwRouteContractCode) {
        this.ptpwRouteContractCode = ptpwRouteContractCode;
    }

    public String[] getPtpwMfrIds() {
        return ptpwMfrIds;
    }

    public void setPtpwMfrIds(String[] ptpwMfrIds) {
        this.ptpwMfrIds = ptpwMfrIds;
    }

    public String[] getPtpwPickupPoint() {
        return ptpwPickupPoint;
    }

    public void setPtpwPickupPoint(String[] ptpwPickupPoint) {
        this.ptpwPickupPoint = ptpwPickupPoint;
    }

    public String[] getPtpwDropPoint() {
        return ptpwDropPoint;
    }

    public void setPtpwDropPoint(String[] ptpwDropPoint) {
        this.ptpwDropPoint = ptpwDropPoint;
    }

    public String[] getPtpwTotalKm() {
        return ptpwTotalKm;
    }

    public void setPtpwTotalKm(String[] ptpwTotalKm) {
        this.ptpwTotalKm = ptpwTotalKm;
    }

    public String[] getPtpwRateWithReefer() {
        return ptpwRateWithReefer;
    }

    public void setPtpwRateWithReefer(String[] ptpwRateWithReefer) {
        this.ptpwRateWithReefer = ptpwRateWithReefer;
    }

    public String[] getPtpwRateWithoutReefer() {
        return ptpwRateWithoutReefer;
    }

    public void setPtpwRateWithoutReefer(String[] ptpwRateWithoutReefer) {
        this.ptpwRateWithoutReefer = ptpwRateWithoutReefer;
    }

    public String[] getPtpPickupPoints() {
        return ptpPickupPoints;
    }

    public void setPtpPickupPoints(String[] ptpPickupPoints) {
        this.ptpPickupPoints = ptpPickupPoints;
    }

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }

    public String getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(String customerDetails) {
        this.customerDetails = customerDetails;
    }

    public String[] getPtpwPointId() {
        return ptpwPointId;
    }

    public void setPtpwPointId(String[] ptpwPointId) {
        this.ptpwPointId = ptpwPointId;
    }

    public String getFirstPickup() {
        return firstPickup;
    }

    public void setFirstPickup(String firstPickup) {
        this.firstPickup = firstPickup;
    }

    public String getInterimPickup1() {
        return interimPickup1;
    }

    public void setInterimPickup1(String interimPickup1) {
        this.interimPickup1 = interimPickup1;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Km() {
        return point1Km;
    }

    public void setPoint1Km(String point1Km) {
        this.point1Km = point1Km;
    }

    public String getInterimPickup2() {
        return interimPickup2;
    }

    public void setInterimPickup2(String interimPickup2) {
        this.interimPickup2 = interimPickup2;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint2Km() {
        return point2Km;
    }

    public void setPoint2Km(String point2Km) {
        this.point2Km = point2Km;
    }

    public String getInterimPickup3() {
        return interimPickup3;
    }

    public void setInterimPickup3(String interimPickup3) {
        this.interimPickup3 = interimPickup3;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint3Km() {
        return point3Km;
    }

    public void setPoint3Km(String point3Km) {
        this.point3Km = point3Km;
    }

    public String getInterimPickup4() {
        return interimPickup4;
    }

    public void setInterimPickup4(String interimPickup4) {
        this.interimPickup4 = interimPickup4;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getPoint4Km() {
        return point4Km;
    }

    public void setPoint4Km(String point4Km) {
        this.point4Km = point4Km;
    }

    public String getFinalDrop() {
        return finalDrop;
    }

    public void setFinalDrop(String finalDrop) {
        this.finalDrop = finalDrop;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFinalPointKm() {
        return finalPointKm;
    }

    public void setFinalPointKm(String finalPointKm) {
        this.finalPointKm = finalPointKm;
    }

    public String getRateWithReeferPerKm() {
        return rateWithReeferPerKm;
    }

    public void setRateWithReeferPerKm(String rateWithReeferPerKm) {
        this.rateWithReeferPerKm = rateWithReeferPerKm;
    }

    public String getRateWithoutReeferPerKm() {
        return rateWithoutReeferPerKm;
    }

    public void setRateWithoutReeferPerKm(String rateWithoutReeferPerKm) {
        this.rateWithoutReeferPerKm = rateWithoutReeferPerKm;
    }

    public String getRateWithReeferPerKg() {
        return rateWithReeferPerKg;
    }

    public void setRateWithReeferPerKg(String rateWithReeferPerKg) {
        this.rateWithReeferPerKg = rateWithReeferPerKg;
    }

    public String getRateWithoutReeferPerKg() {
        return rateWithoutReeferPerKg;
    }

    public void setRateWithoutReeferPerKg(String rateWithoutReeferPerKg) {
        this.rateWithoutReeferPerKg = rateWithoutReeferPerKg;
    }

    public String getReeferRupeesPerHour() {
        return reeferRupeesPerHour;
    }

    public void setReeferRupeesPerHour(String reeferRupeesPerHour) {
        this.reeferRupeesPerHour = reeferRupeesPerHour;
    }

    public String getVehicleRupeesPerKm() {
        return vehicleRupeesPerKm;
    }

    public void setVehicleRupeesPerKm(String vehicleRupeesPerKm) {
        this.vehicleRupeesPerKm = vehicleRupeesPerKm;
    }

    public String getRouteBillingId() {
        return routeBillingId;
    }

    public void setRouteBillingId(String routeBillingId) {
        this.routeBillingId = routeBillingId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public String getRouteContractCode() {
        return routeContractCode;
    }

    public void setRouteContractCode(String routeContractCode) {
        this.routeContractCode = routeContractCode;
    }

    public String getInterimPointName() {
        return interimPointName;
    }

    public void setInterimPointName(String interimPointName) {
        this.interimPointName = interimPointName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getPriceDiff() {
        return priceDiff;
    }

    public void setPriceDiff(String priceDiff) {
        this.priceDiff = priceDiff;
    }

    public String getFuelPriceId() {
        return fuelPriceId;
    }

    public void setFuelPriceId(String fuelPriceId) {
        this.fuelPriceId = fuelPriceId;
    }

    public int getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(int routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getAccountManagerName() {
        return accountManagerName;
    }

    public void setAccountManagerName(String accountManagerName) {
        this.accountManagerName = accountManagerName;
    }

    public String getRouteFromId() {
        return routeFromId;
    }

    public void setRouteFromId(String routeFromId) {
        this.routeFromId = routeFromId;
    }

    public String getRouteToId() {
        return routeToId;
    }

    public void setRouteToId(String routeToId) {
        this.routeToId = routeToId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCityToId() {
        return cityToId;
    }

    public void setCityToId(String cityToId) {
        this.cityToId = cityToId;
    }

    public String getTravelHour() {
        return travelHour;
    }

    public void setTravelHour(String travelHour) {
        this.travelHour = travelHour;
    }

    public String getTravelMinute() {
        return travelMinute;
    }

    public void setTravelMinute(String travelMinute) {
        this.travelMinute = travelMinute;
    }

    public String getReeferHour() {
        return reeferHour;
    }

    public void setReeferHour(String reeferHour) {
        this.reeferHour = reeferHour;
    }

    public String getReeferMinute() {
        return reeferMinute;
    }

    public void setReeferMinute(String reeferMinute) {
        this.reeferMinute = reeferMinute;
    }

    public String getTollAmountType() {
        return tollAmountType;
    }

    public void setTollAmountType(String tollAmountType) {
        this.tollAmountType = tollAmountType;
    }

    public String getAvgTollAmount() {
        return avgTollAmount;
    }

    public void setAvgTollAmount(String avgTollAmount) {
        this.avgTollAmount = avgTollAmount;
    }

    public String getAvgMisCost() {
        return avgMisCost;
    }

    public void setAvgMisCost(String avgMisCost) {
        this.avgMisCost = avgMisCost;
    }

    public String getAvgDriverIncentive() {
        return avgDriverIncentive;
    }

    public void setAvgDriverIncentive(String avgDriverIncentive) {
        this.avgDriverIncentive = avgDriverIncentive;
    }

    public String getAvgFactor() {
        return avgFactor;
    }

    public void setAvgFactor(String avgFactor) {
        this.avgFactor = avgFactor;
    }

    public String[] getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String[] vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String[] getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String[] fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String[] getFuelCostPerHr() {
        return fuelCostPerHr;
    }

    public void setFuelCostPerHr(String[] fuelCostPerHr) {
        this.fuelCostPerHr = fuelCostPerHr;
    }

    public String[] getTollAmounts() {
        return tollAmounts;
    }

    public void setTollAmounts(String[] tollAmounts) {
        this.tollAmounts = tollAmounts;
    }

    public String[] getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String[] miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String[] getDriverIncenKm() {
        return driverIncenKm;
    }

    public void setDriverIncenKm(String[] driverIncenKm) {
        this.driverIncenKm = driverIncenKm;
    }

    public String[] getFactor() {
        return factor;
    }

    public void setFactor(String[] factor) {
        this.factor = factor;
    }

    public String[] getVarExpense() {
        return varExpense;
    }

    public void setVarExpense(String[] varExpense) {
        this.varExpense = varExpense;
    }

    public String getCityFromName() {
        return cityFromName;
    }

    public void setCityFromName(String cityFromName) {
        this.cityFromName = cityFromName;
    }

    public String getCityToName() {
        return cityToName;
    }

    public void setCityToName(String cityToName) {
        this.cityToName = cityToName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getWalkinCustomerName() {
        return walkinCustomerName;
    }

    public void setWalkinCustomerName(String walkinCustomerName) {
        this.walkinCustomerName = walkinCustomerName;
    }

    public String getWalkinCustomerCode() {
        return walkinCustomerCode;
    }

    public void setWalkinCustomerCode(String walkinCustomerCode) {
        this.walkinCustomerCode = walkinCustomerCode;
    }

    public String getWalkinCustomerAddress() {
        return walkinCustomerAddress;
    }

    public void setWalkinCustomerAddress(String walkinCustomerAddress) {
        this.walkinCustomerAddress = walkinCustomerAddress;
    }

    public String getWalkinPincode() {
        return walkinPincode;
    }

    public void setWalkinPincode(String walkinPincode) {
        this.walkinPincode = walkinPincode;
    }

    public String getWalkinCustomerMobileNo() {
        return walkinCustomerMobileNo;
    }

    public void setWalkinCustomerMobileNo(String walkinCustomerMobileNo) {
        this.walkinCustomerMobileNo = walkinCustomerMobileNo;
    }

    public String getWalkinMailId() {
        return walkinMailId;
    }

    public void setWalkinMailId(String walkinMailId) {
        this.walkinMailId = walkinMailId;
    }

    public String getWalkinCustomerPhoneNo() {
        return walkinCustomerPhoneNo;
    }

    public void setWalkinCustomerPhoneNo(String walkinCustomerPhoneNo) {
        this.walkinCustomerPhoneNo = walkinCustomerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getTotalPackage() {
        return totalPackage;
    }

    public void setTotalPackage(String totalPackage) {
        this.totalPackage = totalPackage;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTotalCharges() {
        return totalCharges;
    }

    public void setTotalCharges(String totalCharges) {
        this.totalCharges = totalCharges;
    }

    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    public String getOdaCharges() {
        return odaCharges;
    }

    public void setOdaCharges(String odaCharges) {
        this.odaCharges = odaCharges;
    }

    public String getMultiPickupCharge() {
        return multiPickupCharge;
    }

    public void setMultiPickupCharge(String multiPickupCharge) {
        this.multiPickupCharge = multiPickupCharge;
    }

    public String getMultiDeliveryCharge() {
        return multiDeliveryCharge;
    }

    public void setMultiDeliveryCharge(String multiDeliveryCharge) {
        this.multiDeliveryCharge = multiDeliveryCharge;
    }

    public String getHandleCharges() {
        return handleCharges;
    }

    public void setHandleCharges(String handleCharges) {
        this.handleCharges = handleCharges;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(String unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public String getLoadingCharges() {
        return loadingCharges;
    }

    public void setLoadingCharges(String loadingCharges) {
        this.loadingCharges = loadingCharges;
    }

    public String getWalkInBillingTypeId() {
        return walkInBillingTypeId;
    }

    public void setWalkInBillingTypeId(String walkInBillingTypeId) {
        this.walkInBillingTypeId = walkInBillingTypeId;
    }

    public String getWalkinFreightWithReefer() {
        return walkinFreightWithReefer;
    }

    public void setWalkinFreightWithReefer(String walkinFreightWithReefer) {
        this.walkinFreightWithReefer = walkinFreightWithReefer;
    }

    public String getWalkinFreightWithoutReefer() {
        return walkinFreightWithoutReefer;
    }

    public void setWalkinFreightWithoutReefer(String walkinFreightWithoutReefer) {
        this.walkinFreightWithoutReefer = walkinFreightWithoutReefer;
    }

    public String getWalkinRateWithReeferPerKg() {
        return walkinRateWithReeferPerKg;
    }

    public void setWalkinRateWithReeferPerKg(String walkinRateWithReeferPerKg) {
        this.walkinRateWithReeferPerKg = walkinRateWithReeferPerKg;
    }

    public String getWalkinRateWithoutReeferPerKg() {
        return walkinRateWithoutReeferPerKg;
    }

    public void setWalkinRateWithoutReeferPerKg(String walkinRateWithoutReeferPerKg) {
        this.walkinRateWithoutReeferPerKg = walkinRateWithoutReeferPerKg;
    }

    public String getWalkinRateWithReeferPerKm() {
        return walkinRateWithReeferPerKm;
    }

    public void setWalkinRateWithReeferPerKm(String walkinRateWithReeferPerKm) {
        this.walkinRateWithReeferPerKm = walkinRateWithReeferPerKm;
    }

    public String getWalkinRateWithoutReeferPerKm() {
        return walkinRateWithoutReeferPerKm;
    }

    public void setWalkinRateWithoutReeferPerKm(String walkinRateWithoutReeferPerKm) {
        this.walkinRateWithoutReeferPerKm = walkinRateWithoutReeferPerKm;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getStandardChargeRemarks() {
        return standardChargeRemarks;
    }

    public void setStandardChargeRemarks(String standardChargeRemarks) {
        this.standardChargeRemarks = standardChargeRemarks;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getFirstPickupId() {
        return firstPickupId;
    }

    public void setFirstPickupId(String firstPickupId) {
        this.firstPickupId = firstPickupId;
    }

    public String getFirstPickupName() {
        return firstPickupName;
    }

    public void setFirstPickupName(String firstPickupName) {
        this.firstPickupName = firstPickupName;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint1Hrs() {
        return point1Hrs;
    }

    public void setPoint1Hrs(String point1Hrs) {
        this.point1Hrs = point1Hrs;
    }

    public String getPoint1Minutes() {
        return point1Minutes;
    }

    public void setPoint1Minutes(String point1Minutes) {
        this.point1Minutes = point1Minutes;
    }

    public String getPoint1RouteId() {
        return point1RouteId;
    }

    public void setPoint1RouteId(String point1RouteId) {
        this.point1RouteId = point1RouteId;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint2Hrs() {
        return point2Hrs;
    }

    public void setPoint2Hrs(String point2Hrs) {
        this.point2Hrs = point2Hrs;
    }

    public String getPoint2Minutes() {
        return point2Minutes;
    }

    public void setPoint2Minutes(String point2Minutes) {
        this.point2Minutes = point2Minutes;
    }

    public String getPoint2RouteId() {
        return point2RouteId;
    }

    public void setPoint2RouteId(String point2RouteId) {
        this.point2RouteId = point2RouteId;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint3Minutes() {
        return point3Minutes;
    }

    public void setPoint3Minutes(String point3Minutes) {
        this.point3Minutes = point3Minutes;
    }

    public String getPoint3RouteId() {
        return point3RouteId;
    }

    public void setPoint3RouteId(String point3RouteId) {
        this.point3RouteId = point3RouteId;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getPoint4Hrs() {
        return point4Hrs;
    }

    public void setPoint4Hrs(String point4Hrs) {
        this.point4Hrs = point4Hrs;
    }

    public String getPoint4Minutes() {
        return point4Minutes;
    }

    public void setPoint4Minutes(String point4Minutes) {
        this.point4Minutes = point4Minutes;
    }

    public String getPoint4RouteId() {
        return point4RouteId;
    }

    public void setPoint4RouteId(String point4RouteId) {
        this.point4RouteId = point4RouteId;
    }

    public String getFinalPointName() {
        return finalPointName;
    }

    public void setFinalPointName(String finalPointName) {
        this.finalPointName = finalPointName;
    }

    public String getFinalPointHrs() {
        return finalPointHrs;
    }

    public void setFinalPointHrs(String finalPointHrs) {
        this.finalPointHrs = finalPointHrs;
    }

    public String getFinalPointMinutes() {
        return finalPointMinutes;
    }

    public void setFinalPointMinutes(String finalPointMinutes) {
        this.finalPointMinutes = finalPointMinutes;
    }

    public String getFinalPointRouteId() {
        return finalPointRouteId;
    }

    public void setFinalPointRouteId(String finalPointRouteId) {
        this.finalPointRouteId = finalPointRouteId;
    }

    public String getVehicleRatePerKm() {
        return vehicleRatePerKm;
    }

    public void setVehicleRatePerKm(String vehicleRatePerKm) {
        this.vehicleRatePerKm = vehicleRatePerKm;
    }

    public String getReeferRatePerHour() {
        return reeferRatePerHour;
    }

    public void setReeferRatePerHour(String reeferRatePerHour) {
        this.reeferRatePerHour = reeferRatePerHour;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String[] getFuelCostPerKms() {
        return fuelCostPerKms;
    }

    public void setFuelCostPerKms(String[] fuelCostPerKms) {
        this.fuelCostPerKms = fuelCostPerKms;
    }

    public String[] getFuelCostPerHrs() {
        return fuelCostPerHrs;
    }

    public void setFuelCostPerHrs(String[] fuelCostPerHrs) {
        this.fuelCostPerHrs = fuelCostPerHrs;
    }

    public String[] getReefMileage() {
        return reefMileage;
    }

    public void setReefMileage(String[] reefMileage) {
        this.reefMileage = reefMileage;
    }

    public String getFuelCostKm() {
        return fuelCostKm;
    }

    public void setFuelCostKm(String fuelCostKm) {
        this.fuelCostKm = fuelCostKm;
    }

    public String getFuelCostHr() {
        return fuelCostHr;
    }

    public void setFuelCostHr(String fuelCostHr) {
        this.fuelCostHr = fuelCostHr;
    }

    public String getTollAmountperkm() {
        return tollAmountperkm;
    }

    public void setTollAmountperkm(String tollAmountperkm) {
        this.tollAmountperkm = tollAmountperkm;
    }

    public String getMiscCostperkm() {
        return miscCostperkm;
    }

    public void setMiscCostperkm(String miscCostperkm) {
        this.miscCostperkm = miscCostperkm;
    }

    public String getDriverIncentperkm() {
        return driverIncentperkm;
    }

    public void setDriverIncentperkm(String driverIncentperkm) {
        this.driverIncentperkm = driverIncentperkm;
    }

    public String getVariExpense() {
        return variExpense;
    }

    public void setVariExpense(String variExpense) {
        this.variExpense = variExpense;
    }

    public String getVehiExpense() {
        return vehiExpense;
    }

    public void setVehiExpense(String vehiExpense) {
        this.vehiExpense = vehiExpense;
    }

    public String getReefeExpense() {
        return reefeExpense;
    }

    public void setReefeExpense(String reefeExpense) {
        this.reefeExpense = reefeExpense;
    }

    public String getTotaExpense() {
        return totaExpense;
    }

    public void setTotaExpense(String totaExpense) {
        this.totaExpense = totaExpense;
    }

    public String getEffectivDate() {
        return effectivDate;
    }

    public void setEffectivDate(String effectivDate) {
        this.effectivDate = effectivDate;
    }

    public String getPoint3Hrs() {
        return point3Hrs;
    }

    public void setPoint3Hrs(String point3Hrs) {
        this.point3Hrs = point3Hrs;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getConsigmentOrigin() {
        return consigmentOrigin;
    }

    public void setConsigmentOrigin(String consigmentOrigin) {
        this.consigmentOrigin = consigmentOrigin;
    }

    public String getConsigmentDestination() {
        return consigmentDestination;
    }

    public void setConsigmentDestination(String consigmentDestination) {
        this.consigmentDestination = consigmentDestination;
    }

    public String[] getPointId() {
        return pointId;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public String[] getPointName() {
        return pointName;
    }

    public void setPointName(String[] pointName) {
        this.pointName = pointName;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String[] getOrder() {
        return order;
    }

    public void setOrder(String[] order) {
        this.order = order;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanHour() {
        return pointPlanHour;
    }

    public void setPointPlanHour(String[] pointPlanHour) {
        this.pointPlanHour = pointPlanHour;
    }

    public String[] getPointPlanMinute() {
        return pointPlanMinute;
    }

    public void setPointPlanMinute(String[] pointPlanMinute) {
        this.pointPlanMinute = pointPlanMinute;
    }

    public String getEndPointId() {
        return endPointId;
    }

    public void setEndPointId(String endPointId) {
        this.endPointId = endPointId;
    }

    public String getFinalRouteId() {
        return finalRouteId;
    }

    public void setFinalRouteId(String finalRouteId) {
        this.finalRouteId = finalRouteId;
    }

    public String getEndPointName() {
        return endPointName;
    }

    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    public String getEndPointType() {
        return endPointType;
    }

    public void setEndPointType(String endPointType) {
        this.endPointType = endPointType;
    }

    public String getEndOrder() {
        return endOrder;
    }

    public void setEndOrder(String endOrder) {
        this.endOrder = endOrder;
    }

    public String getEndPointAddresss() {
        return endPointAddresss;
    }

    public void setEndPointAddresss(String endPointAddresss) {
        this.endPointAddresss = endPointAddresss;
    }

    public String getEndPointPlanDate() {
        return endPointPlanDate;
    }

    public void setEndPointPlanDate(String endPointPlanDate) {
        this.endPointPlanDate = endPointPlanDate;
    }

    public String getEndPointPlanHour() {
        return endPointPlanHour;
    }

    public void setEndPointPlanHour(String endPointPlanHour) {
        this.endPointPlanHour = endPointPlanHour;
    }

    public String getEndPointPlanMinute() {
        return endPointPlanMinute;
    }

    public void setEndPointPlanMinute(String endPointPlanMinute) {
        this.endPointPlanMinute = endPointPlanMinute;
    }

    public String[] getMultiplePointRouteId() {
        return multiplePointRouteId;
    }

    public void setMultiplePointRouteId(String[] multiplePointRouteId) {
        this.multiplePointRouteId = multiplePointRouteId;
    }

    public String[] getPointRouteId() {
        return pointRouteId;
    }

    public void setPointRouteId(String[] pointRouteId) {
        this.pointRouteId = pointRouteId;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getTimeLeftValue() {
        return timeLeftValue;
    }

    public void setTimeLeftValue(String timeLeftValue) {
        this.timeLeftValue = timeLeftValue;
    }

    public String getTripScheduleDateTime() {
        return tripScheduleDateTime;
    }

    public void setTripScheduleDateTime(String tripScheduleDateTime) {
        this.tripScheduleDateTime = tripScheduleDateTime;
    }

    public String getTripScheduleDate() {
        return tripScheduleDate;
    }

    public void setTripScheduleDate(String tripScheduleDate) {
        this.tripScheduleDate = tripScheduleDate;
    }

    public String getTripScheduleTime() {
        return tripScheduleTime;
    }

    public void setTripScheduleTime(String tripScheduleTime) {
        this.tripScheduleTime = tripScheduleTime;
    }

    public String getActualadvancepaid() {
        return actualadvancepaid;
    }

    public void setActualadvancepaid(String actualadvancepaid) {
        this.actualadvancepaid = actualadvancepaid;
    }

    public String getCnoteName() {
        return cnoteName;
    }

    public void setCnoteName(String cnoteName) {
        this.cnoteName = cnoteName;
    }

    public String getEstimatedadvance() {
        return estimatedadvance;
    }

    public void setEstimatedadvance(String estimatedadvance) {
        this.estimatedadvance = estimatedadvance;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBudinessType() {
        return budinessType;
    }

    public void setBudinessType(String budinessType) {
        this.budinessType = budinessType;
    }

    public String getConsigmentInstruction() {
        return consigmentInstruction;
    }

    public void setConsigmentInstruction(String consigmentInstruction) {
        this.consigmentInstruction = consigmentInstruction;
    }

    public String getConsigmentOrderStatus() {
        return consigmentOrderStatus;
    }

    public void setConsigmentOrderStatus(String consigmentOrderStatus) {
        this.consigmentOrderStatus = consigmentOrderStatus;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getConsignmentOrderRefRemarks() {
        return consignmentOrderRefRemarks;
    }

    public void setConsignmentOrderRefRemarks(String consignmentOrderRefRemarks) {
        this.consignmentOrderRefRemarks = consignmentOrderRefRemarks;
    }

    public String getConsignmentPointAddress() {
        return consignmentPointAddress;
    }

    public void setConsignmentPointAddress(String consignmentPointAddress) {
        this.consignmentPointAddress = consignmentPointAddress;
    }

    public String getConsignmentPointId() {
        return consignmentPointId;
    }

    public void setConsignmentPointId(String consignmentPointId) {
        this.consignmentPointId = consignmentPointId;
    }

    public String getConsignmentPointType() {
        return consignmentPointType;
    }

    public void setConsignmentPointType(String consignmentPointType) {
        this.consignmentPointType = consignmentPointType;
    }

    public String getConsignorMobile() {
        return consignorMobile;
    }

    public void setConsignorMobile(String consignorMobile) {
        this.consignorMobile = consignorMobile;
    }

    public String getCustomerBillingType() {
        return customerBillingType;
    }

    public void setCustomerBillingType(String customerBillingType) {
        this.customerBillingType = customerBillingType;
    }

    public String getCustomerContractId() {
        return customerContractId;
    }

    public void setCustomerContractId(String customerContractId) {
        this.customerContractId = customerContractId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPincode() {
        return customerPincode;
    }

    public void setCustomerPincode(String customerPincode) {
        this.customerPincode = customerPincode;
    }

    public String getFixedRate() {
        return fixedRate;
    }

    public void setFixedRate(String fixedRate) {
        this.fixedRate = fixedRate;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getKgRate() {
        return kgRate;
    }

    public void setKgRate(String kgRate) {
        this.kgRate = kgRate;
    }

    public String getKmRate() {
        return kmRate;
    }

    public void setKmRate(String kmRate) {
        this.kmRate = kmRate;
    }

    public String getMappingId() {
        return mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointDate() {
        return pointDate;
    }

    public void setPointDate(String pointDate) {
        this.pointDate = pointDate;
    }

    public String getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getSecondaryDriverNameOne() {
        return secondaryDriverNameOne;
    }

    public void setSecondaryDriverNameOne(String secondaryDriverNameOne) {
        this.secondaryDriverNameOne = secondaryDriverNameOne;
    }

    public String getSecondaryDriverNameTwo() {
        return secondaryDriverNameTwo;
    }

    public void setSecondaryDriverNameTwo(String secondaryDriverNameTwo) {
        this.secondaryDriverNameTwo = secondaryDriverNameTwo;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getTotalStatndardCharges() {
        return totalStatndardCharges;
    }

    public void setTotalStatndardCharges(String totalStatndardCharges) {
        this.totalStatndardCharges = totalStatndardCharges;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getVehicleRequiredTime() {
        return vehicleRequiredTime;
    }

    public void setVehicleRequiredTime(String vehicleRequiredTime) {
        this.vehicleRequiredTime = vehicleRequiredTime;
    }

    public String getFreightcharges() {
        return freightcharges;
    }

    public void setFreightcharges(String freightcharges) {
        this.freightcharges = freightcharges;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripendkm() {
        return tripendkm;
    }

    public void setTripendkm(String tripendkm) {
        this.tripendkm = tripendkm;
    }

    public String getTripstartkm() {
        return tripstartkm;
    }

    public void setTripstartkm(String tripstartkm) {
        this.tripstartkm = tripstartkm;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getContractRouteOrigin() {
        return contractRouteOrigin;
    }

    public void setContractRouteOrigin(String contractRouteOrigin) {
        this.contractRouteOrigin = contractRouteOrigin;
    }

    public String getPrevPointId() {
        return prevPointId;
    }

    public void setPrevPointId(String prevPointId) {
        this.prevPointId = prevPointId;
    }

    public String getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String getAdvicedate() {
        return advicedate;
    }

    public void setAdvicedate(String advicedate) {
        this.advicedate = advicedate;
    }

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getRequestedadvance() {
        return requestedadvance;
    }

    public void setRequestedadvance(String requestedadvance) {
        this.requestedadvance = requestedadvance;
    }

    public String getApproveremarks() {
        return approveremarks;
    }

    public void setApproveremarks(String approveremarks) {
        this.approveremarks = approveremarks;
    }

    public String getApprovestatus() {
        return approvestatus;
    }

    public void setApprovestatus(String approvestatus) {
        this.approvestatus = approvestatus;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getAlertBased() {
        return alertBased;
    }

    public void setAlertBased(String alertBased) {
        this.alertBased = alertBased;
    }

    public String getAlertDes() {
        return alertDes;
    }

    public void setAlertDes(String alertDes) {
        this.alertDes = alertDes;
    }

    public String getAlertName() {
        return alertName;
    }

    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    public String getAlertRaised() {
        return alertRaised;
    }

    public void setAlertRaised(String alertRaised) {
        this.alertRaised = alertRaised;
    }

    public String getAlertRaised1() {
        return alertRaised1;
    }

    public void setAlertRaised1(String alertRaised1) {
        this.alertRaised1 = alertRaised1;
    }

    public String getAlertccEmail() {
        return alertccEmail;
    }

    public void setAlertccEmail(String alertccEmail) {
        this.alertccEmail = alertccEmail;
    }

    public String getAlertid() {
        return alertid;
    }

    public void setAlertid(String alertid) {
        this.alertid = alertid;
    }

    public String getAlertsemailSub() {
        return alertsemailSub;
    }

    public void setAlertsemailSub(String alertsemailSub) {
        this.alertsemailSub = alertsemailSub;
    }

    public String getAlertsfrequently() {
        return alertsfrequently;
    }

    public void setAlertsfrequently(String alertsfrequently) {
        this.alertsfrequently = alertsfrequently;
    }

    public String getAlertstatus() {
        return alertstatus;
    }

    public void setAlertstatus(String alertstatus) {
        this.alertstatus = alertstatus;
    }

    public String getAlerttoEmail() {
        return alerttoEmail;
    }

    public void setAlerttoEmail(String alerttoEmail) {
        this.alerttoEmail = alerttoEmail;
    }

    public String getCheckListDate() {
        return checkListDate;
    }

    public void setCheckListDate(String checkListDate) {
        this.checkListDate = checkListDate;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public String getCheckListName() {
        return checkListName;
    }

    public void setCheckListName(String checkListName) {
        this.checkListName = checkListName;
    }

    public String getCheckListStage() {
        return checkListStage;
    }

    public void setCheckListStage(String checkListStage) {
        this.checkListStage = checkListStage;
    }

    public String getCheckListStatus() {
        return checkListStatus;
    }

    public void setCheckListStatus(String checkListStatus) {
        this.checkListStatus = checkListStatus;
    }

    public String getCityState() {
        return cityState;
    }

    public void setCityState(String cityState) {
        this.cityState = cityState;
    }

    public String getConsigmentId() {
        return consigmentId;
    }

    public void setConsigmentId(String consigmentId) {
        this.consigmentId = consigmentId;
    }

    public String getEscalationccEmail1() {
        return escalationccEmail1;
    }

    public void setEscalationccEmail1(String escalationccEmail1) {
        this.escalationccEmail1 = escalationccEmail1;
    }

    public String getEscalationemailSub1() {
        return escalationemailSub1;
    }

    public void setEscalationemailSub1(String escalationemailSub1) {
        this.escalationemailSub1 = escalationemailSub1;
    }

    public String getEscalationtoEmail1() {
        return escalationtoEmail1;
    }

    public void setEscalationtoEmail1(String escalationtoEmail1) {
        this.escalationtoEmail1 = escalationtoEmail1;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }

    public String getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(String frequencyName) {
        this.frequencyName = frequencyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoicecustomer() {
        return invoicecustomer;
    }

    public void setInvoicecustomer(String invoicecustomer) {
        this.invoicecustomer = invoicecustomer;
    }

    public String getParameterDescription() {
        return parameterDescription;
    }

    public void setParameterDescription(String parameterDescription) {
        this.parameterDescription = parameterDescription;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterUnit() {
        return parameterUnit;
    }

    public void setParameterUnit(String parameterUnit) {
        this.parameterUnit = parameterUnit;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getReeferMaximumTemperature() {
        return reeferMaximumTemperature;
    }

    public void setReeferMaximumTemperature(String reeferMaximumTemperature) {
        this.reeferMaximumTemperature = reeferMaximumTemperature;
    }

    public String getReeferMinimumTemperature() {
        return reeferMinimumTemperature;
    }

    public void setReeferMinimumTemperature(String reeferMinimumTemperature) {
        this.reeferMinimumTemperature = reeferMinimumTemperature;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getTotFreightCharges() {
        return totFreightCharges;
    }

    public void setTotFreightCharges(String totFreightCharges) {
        this.totFreightCharges = totFreightCharges;
    }

    public String getTotalDrivers() {
        return totalDrivers;
    }

    public void setTotalDrivers(String totalDrivers) {
        this.totalDrivers = totalDrivers;
    }

    public double getTotalExpenseAmt() {
        return totalExpenseAmt;
    }

    public void setTotalExpenseAmt(double totalExpenseAmt) {
        this.totalExpenseAmt = totalExpenseAmt;
    }

    public double getTotalFreightAmt() {
        return totalFreightAmt;
    }

    public void setTotalFreightAmt(double totalFreightAmt) {
        this.totalFreightAmt = totalFreightAmt;
    }

    public double getTotalTripFreightAmt() {
        return totalTripFreightAmt;
    }

    public void setTotalTripFreightAmt(double totalTripFreightAmt) {
        this.totalTripFreightAmt = totalTripFreightAmt;
    }

    public double getTotalTripKmRun() {
        return totalTripKmRun;
    }

    public void setTotalTripKmRun(double totalTripKmRun) {
        this.totalTripKmRun = totalTripKmRun;
    }

    public String getTotalWeights() {
        return totalWeights;
    }

    public void setTotalWeights(String totalWeights) {
        this.totalWeights = totalWeights;
    }

    public int getTotaldrivers() {
        return totaldrivers;
    }

    public void setTotaldrivers(int totaldrivers) {
        this.totaldrivers = totaldrivers;
    }

    public String getTotalkmrun() {
        return totalkmrun;
    }

    public void setTotalkmrun(String totalkmrun) {
        this.totalkmrun = totalkmrun;
    }

    public String getTripPlanningRemark() {
        return tripPlanningRemark;
    }

    public void setTripPlanningRemark(String tripPlanningRemark) {
        this.tripPlanningRemark = tripPlanningRemark;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneid() {
        return zoneid;
    }

    public void setZoneid(String zoneid) {
        this.zoneid = zoneid;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getRcmexpense() {
        return rcmexpense;
    }

    public void setRcmexpense(String rcmexpense) {
        this.rcmexpense = rcmexpense;
    }

    public String getRequesttype() {
        return requesttype;
    }

    public void setRequesttype(String requesttype) {
        this.requesttype = requesttype;
    }

    public String getSystemexpense() {
        return systemexpense;
    }

    public void setSystemexpense(String systemexpense) {
        this.systemexpense = systemexpense;
    }

    public String getTotalrunhm() {
        return totalrunhm;
    }

    public void setTotalrunhm(String totalrunhm) {
        this.totalrunhm = totalrunhm;
    }

    public String getTotalrunkm() {
        return totalrunkm;
    }

    public void setTotalrunkm(String totalrunkm) {
        this.totalrunkm = totalrunkm;
    }

    public String[] getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String[] batchCode) {
        this.batchCode = batchCode;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getConsignmentVehicleId() {
        return consignmentVehicleId;
    }

    public void setConsignmentVehicleId(String consignmentVehicleId) {
        this.consignmentVehicleId = consignmentVehicleId;
    }

    public String getConsignmentVehicleStatus() {
        return consignmentVehicleStatus;
    }

    public void setConsignmentVehicleStatus(String consignmentVehicleStatus) {
        this.consignmentVehicleStatus = consignmentVehicleStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getConsignmentRefNo() {
        return consignmentRefNo;
    }

    public void setConsignmentRefNo(String consignmentRefNo) {
        this.consignmentRefNo = consignmentRefNo;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupdate() {
        return pickupdate;
    }

    public void setPickupdate(String pickupdate) {
        this.pickupdate = pickupdate;
    }

    public String getPoint1Type() {
        return point1Type;
    }

    public void setPoint1Type(String point1Type) {
        this.point1Type = point1Type;
    }

    public String getPoint2Type() {
        return point2Type;
    }

    public void setPoint2Type(String point2Type) {
        this.point2Type = point2Type;
    }

    public String getPoint3Type() {
        return point3Type;
    }

    public void setPoint3Type(String point3Type) {
        this.point3Type = point3Type;
    }

    public String getPoint4Type() {
        return point4Type;
    }

    public void setPoint4Type(String point4Type) {
        this.point4Type = point4Type;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getTomailId() {
        return tomailId;
    }

    public void setTomailId(String tomailId) {
        this.tomailId = tomailId;
    }

    public String getActualKmVehicleTypeId() {
        return actualKmVehicleTypeId;
    }

    public void setActualKmVehicleTypeId(String actualKmVehicleTypeId) {
        this.actualKmVehicleTypeId = actualKmVehicleTypeId;
    }

    public String[] getArticleId() {
        return articleId;
    }

    public void setArticleId(String[] articleId) {
        this.articleId = articleId;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getConsignmentArticleId() {
        return consignmentArticleId;
    }

    public void setConsignmentArticleId(String consignmentArticleId) {
        this.consignmentArticleId = consignmentArticleId;
    }

    public String getContractVehicleTypeId() {
        return contractVehicleTypeId;
    }

    public void setContractVehicleTypeId(String contractVehicleTypeId) {
        this.contractVehicleTypeId = contractVehicleTypeId;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getTripAdvaceId() {
        return tripAdvaceId;
    }

    public void setTripAdvaceId(String tripAdvaceId) {
        this.tripAdvaceId = tripAdvaceId;
    }

    public String getFleetCenterContactNo() {
        return fleetCenterContactNo;
    }

    public void setFleetCenterContactNo(String fleetCenterContactNo) {
        this.fleetCenterContactNo = fleetCenterContactNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public double getFactors() {
        return factors;
    }

    public void setFactors(double factors) {
        this.factors = factors;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public float getTotalExp() {
        return totalExp;
    }

    public void setTotalExp(float totalExp) {
        this.totalExp = totalExp;
    }

    public float getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(float totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getProductCategoryUnchecked() {
        return productCategoryUnchecked;
    }

    public void setProductCategoryUnchecked(String productCategoryUnchecked) {
        this.productCategoryUnchecked = productCategoryUnchecked;
    }

    public String getTemp1MaximumTemperature() {
        return temp1MaximumTemperature;
    }

    public void setTemp1MaximumTemperature(String temp1MaximumTemperature) {
        this.temp1MaximumTemperature = temp1MaximumTemperature;
    }

    public String getTemp1MinimumTemp() {
        return temp1MinimumTemp;
    }

    public void setTemp1MinimumTemp(String temp1MinimumTemp) {
        this.temp1MinimumTemp = temp1MinimumTemp;
    }

    public String getTemp1ReeferRequired() {
        return temp1ReeferRequired;
    }

    public void setTemp1ReeferRequired(String temp1ReeferRequired) {
        this.temp1ReeferRequired = temp1ReeferRequired;
    }

    public String getTempMaximumTemperature() {
        return tempMaximumTemperature;
    }

    public void setTempMaximumTemperature(String tempMaximumTemperature) {
        this.tempMaximumTemperature = tempMaximumTemperature;
    }

    public String getTempMinimumTemp() {
        return tempMinimumTemp;
    }

    public void setTempMinimumTemp(String tempMinimumTemp) {
        this.tempMinimumTemp = tempMinimumTemp;
    }

    public String getTempReeferRequired() {
        return tempReeferRequired;
    }

    public void setTempReeferRequired(String tempReeferRequired) {
        this.tempReeferRequired = tempReeferRequired;
    }

    public String getBpclTransactionId() {
        return bpclTransactionId;
    }

    public void setBpclTransactionId(String bpclTransactionId) {
        this.bpclTransactionId = bpclTransactionId;
    }

    public String getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(String advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public String getAdvanceToPayStatus() {
        return advanceToPayStatus;
    }

    public void setAdvanceToPayStatus(String advanceToPayStatus) {
        this.advanceToPayStatus = advanceToPayStatus;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeRemarks() {
        return chequeRemarks;
    }

    public void setChequeRemarks(String chequeRemarks) {
        this.chequeRemarks = chequeRemarks;
    }

    public String getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(String draftNo) {
        this.draftNo = draftNo;
    }

    public String getDraftRemarks() {
        return draftRemarks;
    }

    public void setDraftRemarks(String draftRemarks) {
        this.draftRemarks = draftRemarks;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentModeId() {
        return paymentModeId;
    }

    public void setPaymentModeId(String paymentModeId) {
        this.paymentModeId = paymentModeId;
    }

    public String getPaymentModeName() {
        return paymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        this.paymentModeName = paymentModeName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRtgsNo() {
        return rtgsNo;
    }

    public void setRtgsNo(String rtgsNo) {
        this.rtgsNo = rtgsNo;
    }

    public String getRtgsRemarks() {
        return rtgsRemarks;
    }

    public void setRtgsRemarks(String rtgsRemarks) {
        this.rtgsRemarks = rtgsRemarks;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelUnite() {
        return fuelUnite;
    }

    public void setFuelUnite(String fuelUnite) {
        this.fuelUnite = fuelUnite;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getFuelTypeName() {
        return fuelTypeName;
    }

    public void setFuelTypeName(String fuelTypeName) {
        this.fuelTypeName = fuelTypeName;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public String getInfoEmailCc() {
        return infoEmailCc;
    }

    public void setInfoEmailCc(String infoEmailCc) {
        this.infoEmailCc = infoEmailCc;
    }

    public String getInfoEmailTo() {
        return infoEmailTo;
    }

    public void setInfoEmailTo(String infoEmailTo) {
        this.infoEmailTo = infoEmailTo;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getInfoEmptyTo() {
        return infoEmptyTo;
    }

    public void setInfoEmptyTo(String infoEmptyTo) {
        this.infoEmptyTo = infoEmptyTo;
    }

    public String getTimeApprovalStatus() {
        return timeApprovalStatus;
    }

    public void setTimeApprovalStatus(String timeApprovalStatus) {
        this.timeApprovalStatus = timeApprovalStatus;
    }

    public String getNextStatusId() {
        return nextStatusId;
    }

    public void setNextStatusId(String nextStatusId) {
        this.nextStatusId = nextStatusId;
    }

    public String getTripPod() {
        return tripPod;
    }

    public void setTripPod(String tripPod) {
        this.tripPod = tripPod;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getAmountExceed() {
        return amountExceed;
    }

    public void setAmountExceed(String amountExceed) {
        this.amountExceed = amountExceed;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getTotalRequestAmount() {
        return totalRequestAmount;
    }

    public void setTotalRequestAmount(String totalRequestAmount) {
        this.totalRequestAmount = totalRequestAmount;
    }

    public double getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(double paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public double getTripRevenue() {
        return tripRevenue;
    }

    public void setTripRevenue(double tripRevenue) {
        this.tripRevenue = tripRevenue;
    }

    public String getWfuDay() {
        return wfuDay;
    }

    public void setWfuDay(String wfuDay) {
        this.wfuDay = wfuDay;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public double getDeviateAmount() {
        return deviateAmount;
    }

    public void setDeviateAmount(double deviateAmount) {
        this.deviateAmount = deviateAmount;
    }

    public double getOutStanding() {
        return outStanding;
    }

    public void setOutStanding(double outStanding) {
        this.outStanding = outStanding;
    }

    public double getCreditLimitAmount() {
        return creditLimitAmount;
    }

    public void setCreditLimitAmount(double creditLimitAmount) {
        this.creditLimitAmount = creditLimitAmount;
    }

    public String getJobcardDays() {
        return jobcardDays;
    }

    public void setJobcardDays(String jobcardDays) {
        this.jobcardDays = jobcardDays;
    }

    public String getJobcardHours() {
        return jobcardHours;
    }

    public void setJobcardHours(String jobcardHours) {
        this.jobcardHours = jobcardHours;
    }

    public String getPlannedCompleteDays() {
        return plannedCompleteDays;
    }

    public void setPlannedCompleteDays(String plannedCompleteDays) {
        this.plannedCompleteDays = plannedCompleteDays;
    }

    public String getPlannedCompleteHours() {
        return plannedCompleteHours;
    }

    public void setPlannedCompleteHours(String plannedCompleteHours) {
        this.plannedCompleteHours = plannedCompleteHours;
    }

    public String getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(String actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getJobCardType() {
        return jobCardType;
    }

    public void setJobCardType(String jobCardType) {
        this.jobCardType = jobCardType;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getReqHour() {
        return reqHour;
    }

    public void setReqHour(String reqHour) {
        this.reqHour = reqHour;
    }

    public String getReqMinute() {
        return reqMinute;
    }

    public void setReqMinute(String reqMinute) {
        this.reqMinute = reqMinute;
    }

    public String getReqSeconds() {
        return reqSeconds;
    }

    public void setReqSeconds(String reqSeconds) {
        this.reqSeconds = reqSeconds;
    }

    public String getServiceVendorId() {
        return serviceVendorId;
    }

    public void setServiceVendorId(String serviceVendorId) {
        this.serviceVendorId = serviceVendorId;
    }

    public String getTripEndHm() {
        return tripEndHm;
    }

    public void setTripEndHm(String tripEndHm) {
        this.tripEndHm = tripEndHm;
    }

    public String getExtraHmRateWithReefer() {
        return extraHmRateWithReefer;
    }

    public void setExtraHmRateWithReefer(String extraHmRateWithReefer) {
        this.extraHmRateWithReefer = extraHmRateWithReefer;
    }

    public String getExtraKmRateWithReefer() {
        return extraKmRateWithReefer;
    }

    public void setExtraKmRateWithReefer(String extraKmRateWithReefer) {
        this.extraKmRateWithReefer = extraKmRateWithReefer;
    }

    public String getExtraKmRateWithoutReefer() {
        return extraKmRateWithoutReefer;
    }

    public void setExtraKmRateWithoutReefer(String extraKmRateWithoutReefer) {
        this.extraKmRateWithoutReefer = extraKmRateWithoutReefer;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String[] getFixedKmvehicleTypeId() {
        return fixedKmvehicleTypeId;
    }

    public void setFixedKmvehicleTypeId(String[] fixedKmvehicleTypeId) {
        this.fixedKmvehicleTypeId = fixedKmvehicleTypeId;
    }

    public String[] getFixedRateWithReefer() {
        return fixedRateWithReefer;
    }

    public void setFixedRateWithReefer(String[] fixedRateWithReefer) {
        this.fixedRateWithReefer = fixedRateWithReefer;
    }

    public String[] getFixedRateWithoutReefer() {
        return fixedRateWithoutReefer;
    }

    public void setFixedRateWithoutReefer(String[] fixedRateWithoutReefer) {
        this.fixedRateWithoutReefer = fixedRateWithoutReefer;
    }

    public String[] getFixedTotalHm() {
        return fixedTotalHm;
    }

    public void setFixedTotalHm(String[] fixedTotalHm) {
        this.fixedTotalHm = fixedTotalHm;
    }

    public String[] getFixedTotalKm() {
        return fixedTotalKm;
    }

    public void setFixedTotalKm(String[] fixedTotalKm) {
        this.fixedTotalKm = fixedTotalKm;
    }

    public String[] getVehicleNos() {
        return vehicleNos;
    }

    public void setVehicleNos(String[] vehicleNos) {
        this.vehicleNos = vehicleNos;
    }

    public String[] getFixedExtraHmRateWithReefer() {
        return fixedExtraHmRateWithReefer;
    }

    public void setFixedExtraHmRateWithReefer(String[] fixedExtraHmRateWithReefer) {
        this.fixedExtraHmRateWithReefer = fixedExtraHmRateWithReefer;
    }

    public String[] getFixedExtraKmRateWithReefer() {
        return fixedExtraKmRateWithReefer;
    }

    public void setFixedExtraKmRateWithReefer(String[] fixedExtraKmRateWithReefer) {
        this.fixedExtraKmRateWithReefer = fixedExtraKmRateWithReefer;
    }

    public String[] getFixedExtraKmRateWithoutReefer() {
        return fixedExtraKmRateWithoutReefer;
    }

    public void setFixedExtraKmRateWithoutReefer(String[] fixedExtraKmRateWithoutReefer) {
        this.fixedExtraKmRateWithoutReefer = fixedExtraKmRateWithoutReefer;
    }

    public String getContractFixedRateId() {
        return contractFixedRateId;
    }

    public void setContractFixedRateId(String contractFixedRateId) {
        this.contractFixedRateId = contractFixedRateId;
    }

    public String getContractVehicleNos() {
        return contractVehicleNos;
    }

    public void setContractVehicleNos(String contractVehicleNos) {
        this.contractVehicleNos = contractVehicleNos;
    }

    public String getExtraHmRatePerHmRateWithReefer() {
        return extraHmRatePerHmRateWithReefer;
    }

    public void setExtraHmRatePerHmRateWithReefer(String extraHmRatePerHmRateWithReefer) {
        this.extraHmRatePerHmRateWithReefer = extraHmRatePerHmRateWithReefer;
    }

    public String getExtraKmRatePerKmRateWithReefer() {
        return extraKmRatePerKmRateWithReefer;
    }

    public void setExtraKmRatePerKmRateWithReefer(String extraKmRatePerKmRateWithReefer) {
        this.extraKmRatePerKmRateWithReefer = extraKmRatePerKmRateWithReefer;
    }

    public String getExtraKmRatePerKmRateWithoutReefer() {
        return extraKmRatePerKmRateWithoutReefer;
    }

    public void setExtraKmRatePerKmRateWithoutReefer(String extraKmRatePerKmRateWithoutReefer) {
        this.extraKmRatePerKmRateWithoutReefer = extraKmRatePerKmRateWithoutReefer;
    }

    public String[] getEditId() {
        return editId;
    }

    public void setEditId(String[] editId) {
        this.editId = editId;
    }

    public String[] getFixedKmStatus() {
        return fixedKmStatus;
    }

    public void setFixedKmStatus(String[] fixedKmStatus) {
        this.fixedKmStatus = fixedKmStatus;
    }

    public String getEmptyTripPurpose() {
        return emptyTripPurpose;
    }

    public void setEmptyTripPurpose(String emptyTripPurpose) {
        this.emptyTripPurpose = emptyTripPurpose;
    }

    public String getRouteCostId() {
        return routeCostId;
    }

    public void setRouteCostId(String routeCostId) {
        this.routeCostId = routeCostId;
    }

    public String[] getRouteCostIds() {
        return routeCostIds;
    }

    public void setRouteCostIds(String[] routeCostIds) {
        this.routeCostIds = routeCostIds;
    }

    public String getOutStandingDate() {
        return outStandingDate;
    }

    public void setOutStandingDate(String outStandingDate) {
        this.outStandingDate = outStandingDate;
    }

    public int getCnoteCount() {
        return cnoteCount;
    }

    public void setCnoteCount(int cnoteCount) {
        this.cnoteCount = cnoteCount;
    }

    public String getJobCardCode() {
        return jobCardCode;
    }

    public void setJobCardCode(String jobCardCode) {
        this.jobCardCode = jobCardCode;
    }

    public String getJobCardIssueDate() {
        return jobCardIssueDate;
    }

    public void setJobCardIssueDate(String jobCardIssueDate) {
        this.jobCardIssueDate = jobCardIssueDate;
    }

    public String getJobCardRemarks() {
        return jobCardRemarks;
    }

    public void setJobCardRemarks(String jobCardRemarks) {
        this.jobCardRemarks = jobCardRemarks;
    }

    public String getJobCardTypeNew() {
        return jobCardTypeNew;
    }

    public void setJobCardTypeNew(String jobCardTypeNew) {
        this.jobCardTypeNew = jobCardTypeNew;
    }

    public String[] getProductVolume() {
        return productVolume;
    }

    public void setProductVolume(String[] productVolume) {
        this.productVolume = productVolume;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getConsginmentOrderStatus() {
        return consginmentOrderStatus;
    }

    public void setConsginmentOrderStatus(String consginmentOrderStatus) {
        this.consginmentOrderStatus = consginmentOrderStatus;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getFromcurrency() {
        return fromcurrency;
    }

    public void setFromcurrency(String fromcurrency) {
        this.fromcurrency = fromcurrency;
    }

    public String getFromcurrencyvalue() {
        return fromcurrencyvalue;
    }

    public void setFromcurrencyvalue(String fromcurrencyvalue) {
        this.fromcurrencyvalue = fromcurrencyvalue;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTocurrency() {
        return tocurrency;
    }

    public void setTocurrency(String tocurrency) {
        this.tocurrency = tocurrency;
    }

    public String getTocurrencyvalue() {
        return tocurrencyvalue;
    }

    public void setTocurrencyvalue(String tocurrencyvalue) {
        this.tocurrencyvalue = tocurrencyvalue;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(String currencyid) {
        this.currencyid = currencyid;
    }

    public String getFromcurrencyId() {
        return fromcurrencyId;
    }

    public void setFromcurrencyId(String fromcurrencyId) {
        this.fromcurrencyId = fromcurrencyId;
    }

    public String getTocurrencyId() {
        return tocurrencyId;
    }

    public void setTocurrencyId(String tocurrencyId) {
        this.tocurrencyId = tocurrencyId;
    }

    public String getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(String wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getBillEntryDate() {
        return billEntryDate;
    }

    public void setBillEntryDate(String billEntryDate) {
        this.billEntryDate = billEntryDate;
    }

    public String getBillEntryNo() {
        return billEntryNo;
    }

    public void setBillEntryNo(String billEntryNo) {
        this.billEntryNo = billEntryNo;
    }

    public String getCfsPersonName() {
        return cfsPersonName;
    }

    public void setCfsPersonName(String cfsPersonName) {
        this.cfsPersonName = cfsPersonName;
    }

    public String getCfsPersonNo() {
        return cfsPersonNo;
    }

    public void setCfsPersonNo(String cfsPersonNo) {
        this.cfsPersonNo = cfsPersonNo;
    }

    public String getDutyPaymentDate() {
        return dutyPaymentDate;
    }

    public void setDutyPaymentDate(String dutyPaymentDate) {
        this.dutyPaymentDate = dutyPaymentDate;
    }

    public String getOccDate() {
        return occDate;
    }

    public void setOccDate(String occDate) {
        this.occDate = occDate;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getPointPlanTimeTemp() {
        return pointPlanTimeTemp;
    }

    public void setPointPlanTimeTemp(String pointPlanTimeTemp) {
        this.pointPlanTimeTemp = pointPlanTimeTemp;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getOriginCityId() {
        return originCityId;
    }

    public void setOriginCityId(String originCityId) {
        this.originCityId = originCityId;
    }

    public String getDestinationRadius() {
        return destinationRadius;
    }

    public void setDestinationRadius(String destinationRadius) {
        this.destinationRadius = destinationRadius;
    }

    public String getPickupRadius() {
        return pickupRadius;
    }

    public void setPickupRadius(String pickupRadius) {
        this.pickupRadius = pickupRadius;
    }

    public String getPickupStartDate() {
        return pickupStartDate;
    }

    public void setPickupStartDate(String pickupStartDate) {
        this.pickupStartDate = pickupStartDate;
    }

    public String getPickupEndDate() {
        return pickupEndDate;
    }

    public void setPickupEndDate(String pickupEndDate) {
        this.pickupEndDate = pickupEndDate;
    }

    public String getArrivalStartDate() {
        return arrivalStartDate;
    }

    public void setArrivalStartDate(String arrivalStartDate) {
        this.arrivalStartDate = arrivalStartDate;
    }

    public String getArrivalEndDate() {
        return arrivalEndDate;
    }

    public void setArrivalEndDate(String arrivalEndDate) {
        this.arrivalEndDate = arrivalEndDate;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getVehicleCapacity() {
        return vehicleCapacity;
    }

    public void setVehicleCapacity(String vehicleCapacity) {
        this.vehicleCapacity = vehicleCapacity;
    }

    public String getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(String destinationType) {
        this.destinationType = destinationType;
    }

    public String getPickupType() {
        return pickupType;
    }

    public void setPickupType(String pickupType) {
        this.pickupType = pickupType;
    }

    public String getTotalVolumes() {
        return totalVolumes;
    }

    public void setTotalVolumes(String totalVolumes) {
        this.totalVolumes = totalVolumes;
    }

    public String getDestLat() {
        return destLat;
    }

    public void setDestLat(String destLat) {
        this.destLat = destLat;
    }

    public String getDestLong() {
        return destLong;
    }

    public void setDestLong(String destLong) {
        this.destLong = destLong;
    }

    public String getOriginLat() {
        return originLat;
    }

    public void setOriginLat(String originLat) {
        this.originLat = originLat;
    }

    public String getOriginLong() {
        return originLong;
    }

    public void setOriginLong(String originLong) {
        this.originLong = originLong;
    }

    public String getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String getTripAdvanceId() {
        return tripAdvanceId;
    }

    public void setTripAdvanceId(String tripAdvanceId) {
        this.tripAdvanceId = tripAdvanceId;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getSellCost() {
        return sellCost;
    }

    public void setSellCost(String sellCost) {
        this.sellCost = sellCost;
    }

    public String[] getSellingCost() {
        return sellingCost;
    }

    public void setSellingCost(String[] sellingCost) {
        this.sellingCost = sellingCost;
    }

    public String getOrderVolume() {
        return orderVolume;
    }

    public void setOrderVolume(String orderVolume) {
        this.orderVolume = orderVolume;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getMultilpeContainerFlag() {
        return multilpeContainerFlag;
    }

    public void setMultilpeContainerFlag(String multilpeContainerFlag) {
        this.multilpeContainerFlag = multilpeContainerFlag;
    }

    public String getBookingPersonInfo() {
        return bookingPersonInfo;
    }

    public void setBookingPersonInfo(String bookingPersonInfo) {
        this.bookingPersonInfo = bookingPersonInfo;
    }

    public String getIcdLocation() {
        return icdLocation;
    }

    public void setIcdLocation(String icdLocation) {
        this.icdLocation = icdLocation;
    }

    public String getGoogleCityName() {
        return googleCityName;
    }

    public void setGoogleCityName(String googleCityName) {
        this.googleCityName = googleCityName;
    }

    public String getAgeingId() {
        return ageingId;
    }

    public void setAgeingId(String ageingId) {
        this.ageingId = ageingId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getToAge() {
        return toAge;
    }

    public void setToAge(String toAge) {
        this.toAge = toAge;
    }

    public String[] getVehModelId() {
        return vehModelId;
    }

    public void setVehModelId(String[] vehModelId) {
        this.vehModelId = vehModelId;
    }

    public String[] getVehMfrId() {
        return vehMfrId;
    }

    public void setVehMfrId(String[] vehMfrId) {
        this.vehMfrId = vehMfrId;
    }

    public String[] getVehAgeingId() {
        return vehAgeingId;
    }

    public void setVehAgeingId(String[] vehAgeingId) {
        this.vehAgeingId = vehAgeingId;
    }

    public String[] getSellVehTypeId() {
        return sellVehTypeId;
    }

    public void setSellVehTypeId(String[] sellVehTypeId) {
        this.sellVehTypeId = sellVehTypeId;
    }

//    gulshan-10/12/15
    public String getSlabName() {
        return slabName;
    }

    public void setSlabName(String slabName) {
        this.slabName = slabName;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public String getMaxTonnage() {
        return maxTonnage;
    }

    public void setMaxTonnage(String maxTonnage) {
        this.maxTonnage = maxTonnage;
    }

    //    over
    public String getBorderStatus() {
        return borderStatus;
    }

    public void setBorderStatus(String borderStatus) {
        this.borderStatus = borderStatus;
    }

    public String getBorderCrossingStatus() {
        return borderCrossingStatus;
    }

    public void setBorderCrossingStatus(String borderCrossingStatus) {
        this.borderCrossingStatus = borderCrossingStatus;
    }

    public String getBorderCount() {
        return borderCount;
    }

    public void setBorderCount(String borderCount) {
        this.borderCount = borderCount;
    }

    public String getBorderCheckingHour() {
        return borderCheckingHour;
    }

    public void setBorderCheckingHour(String borderCheckingHour) {
        this.borderCheckingHour = borderCheckingHour;
    }

    public String getBorderCheckingMinute() {
        return borderCheckingMinute;
    }

    public void setBorderCheckingMinute(String borderCheckingMinute) {
        this.borderCheckingMinute = borderCheckingMinute;
    }

    public String getDetentionHour() {
        return detentionHour;
    }

    public void setDetentionHour(String detentionHour) {
        this.detentionHour = detentionHour;
    }

    public String getDetentionMinute() {
        return detentionMinute;
    }

    public void setDetentionMinute(String detentionMinute) {
        this.detentionMinute = detentionMinute;
    }

    public String getRouteSellingCostId() {
        return routeSellingCostId;
    }

    public void setRouteSellingCostId(String routeSellingCostId) {
        this.routeSellingCostId = routeSellingCostId;
    }

    public String getRouteSellingCost() {
        return routeSellingCost;
    }

    public void setRouteSellingCost(String routeSellingCost) {
        this.routeSellingCost = routeSellingCost;
    }

    public String[] getBorderCityId() {
        return borderCityId;
    }

    public void setBorderCityId(String[] borderCityId) {
        this.borderCityId = borderCityId;
    }

    public String[] getBorderHours() {
        return borderHours;
    }

    public void setBorderHours(String[] borderHours) {
        this.borderHours = borderHours;
    }

    public String[] getBorderMinutes() {
        return borderMinutes;
    }

    public void setBorderMinutes(String[] borderMinutes) {
        this.borderMinutes = borderMinutes;
    }

    public String getRouteBorderCityId() {
        return routeBorderCityId;
    }

    public void setRouteBorderCityId(String routeBorderCityId) {
        this.routeBorderCityId = routeBorderCityId;
    }

    public String getFreezeInDate() {
        return freezeInDate;
    }

    public void setFreezeInDate(String freezeInDate) {
        this.freezeInDate = freezeInDate;
    }

    public String getFreezeInTime() {
        return freezeInTime;
    }

    public void setFreezeInTime(String freezeInTime) {
        this.freezeInTime = freezeInTime;
    }

    public String getFreezeOutDate() {
        return freezeOutDate;
    }

    public void setFreezeOutDate(String freezeOutDate) {
        this.freezeOutDate = freezeOutDate;
    }

    public String getFreezeOutTime() {
        return freezeOutTime;
    }

    public void setFreezeOutTime(String freezeOutTime) {
        this.freezeOutTime = freezeOutTime;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String[] getDcRate() {
        return dcRate;
    }

    public void setDcRate(String[] dcRate) {
        this.dcRate = dcRate;
    }

    public String[] getDcVehicleTypeId() {
        return dcVehicleTypeId;
    }

    public void setDcVehicleTypeId(String[] dcVehicleTypeId) {
        this.dcVehicleTypeId = dcVehicleTypeId;
    }

    public String[] getVehExpenseEmpty() {
        return vehExpenseEmpty;
    }

    public void setVehExpenseEmpty(String[] vehExpenseEmpty) {
        this.vehExpenseEmpty = vehExpenseEmpty;
    }

    public String getVehicleMileageEmpty() {
        return vehicleMileageEmpty;
    }

    public void setVehicleMileageEmpty(String vehicleMileageEmpty) {
        this.vehicleMileageEmpty = vehicleMileageEmpty;
    }

    public String getFuelCostKmEmpty() {
        return fuelCostKmEmpty;
    }

    public void setFuelCostKmEmpty(String fuelCostKmEmpty) {
        this.fuelCostKmEmpty = fuelCostKmEmpty;
    }

    public String getVehiExpenseEmpty() {
        return vehiExpenseEmpty;
    }

    public void setVehiExpenseEmpty(String vehiExpenseEmpty) {
        this.vehiExpenseEmpty = vehiExpenseEmpty;
    }

    public String getTotaExpenseEmpty() {
        return totaExpenseEmpty;
    }

    public void setTotaExpenseEmpty(String totaExpenseEmpty) {
        this.totaExpenseEmpty = totaExpenseEmpty;
    }

    public String[] getVehRouteCostId() {
        return vehRouteCostId;
    }

    public void setVehRouteCostId(String[] vehRouteCostId) {
        this.vehRouteCostId = vehRouteCostId;
    }

    public String[] getVehMileageEmpty() {
        return vehMileageEmpty;
    }

    public void setVehMileageEmpty(String[] vehMileageEmpty) {
        this.vehMileageEmpty = vehMileageEmpty;
    }

    public String[] getTotExpenseEmpty() {
        return totExpenseEmpty;
    }

    public void setTotExpenseEmpty(String[] totExpenseEmpty) {
        this.totExpenseEmpty = totExpenseEmpty;
    }

    public String[] getFuelCostPerKmsEmpty() {
        return fuelCostPerKmsEmpty;
    }

    public void setFuelCostPerKmsEmpty(String[] fuelCostPerKmsEmpty) {
        this.fuelCostPerKmsEmpty = fuelCostPerKmsEmpty;
    }

    public String[] getRouteBorderCityIds() {
        return routeBorderCityIds;
    }

    public void setRouteBorderCityIds(String[] routeBorderCityIds) {
        this.routeBorderCityIds = routeBorderCityIds;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getTollId() {
        return tollId;
    }

    public void setTollId(String tollId) {
        this.tollId = tollId;
    }

    public String getTollName() {
        return tollName;
    }

    public void setTollName(String tollName) {
        this.tollName = tollName;
    }

    public String getAlertTime() {
        return alertTime;
    }

    public void setAlertTime(String alertTime) {
        this.alertTime = alertTime;
    }

    public String getEscalationAlertTime() {
        return escalationAlertTime;
    }

    public void setEscalationAlertTime(String escalationAlertTime) {
        this.escalationAlertTime = escalationAlertTime;
    }

    public String getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getJobCardFor() {
        return jobCardFor;
    }

    public void setJobCardFor(String jobCardFor) {
        this.jobCardFor = jobCardFor;
    }

    public String getFromLat() {
        return fromLat;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromLong() {
        return fromLong;
    }

    public void setFromLong(String fromLong) {
        this.fromLong = fromLong;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getToLat() {
        return toLat;
    }

    public void setToLat(String toLat) {
        this.toLat = toLat;
    }

    public String getToLong() {
        return toLong;
    }

    public void setToLong(String toLong) {
        this.toLong = toLong;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDoDistance1() {
        return doDistance1;
    }

    public void setDoDistance1(String doDistance1) {
        this.doDistance1 = doDistance1;
    }

    public String getDoDistance2() {
        return doDistance2;
    }

    public void setDoDistance2(String doDistance2) {
        this.doDistance2 = doDistance2;
    }

    public String getOnwardOrderId() {
        return onwardOrderId;
    }

    public void setOnwardOrderId(String onwardOrderId) {
        this.onwardOrderId = onwardOrderId;
    }

    public String getReturnOrderId() {
        return returnOrderId;
    }

    public void setReturnOrderId(String returnOrderId) {
        this.returnOrderId = returnOrderId;
    }

    public String getOnwardOrderIdTrips() {
        return onwardOrderIdTrips;
    }

    public void setOnwardOrderIdTrips(String onwardOrderIdTrips) {
        this.onwardOrderIdTrips = onwardOrderIdTrips;
    }

    public String getReturnOrderIdTrips() {
        return returnOrderIdTrips;
    }

    public void setReturnOrderIdTrips(String returnOrderIdTrips) {
        this.returnOrderIdTrips = returnOrderIdTrips;
    }

    public String getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    public String getVehicleUnit() {
        return vehicleUnit;
    }

    public void setVehicleUnit(String vehicleUnit) {
        this.vehicleUnit = vehicleUnit;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLinerid() {
        return linerid;
    }

    public void setLinerid(String linerid) {
        this.linerid = linerid;
    }

    public String getLinername() {
        return linername;
    }

    public void setLinername(String linername) {
        this.linername = linername;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getShippingLineTwo() {
        return shippingLineTwo;
    }

    public void setShippingLineTwo(String shippingLineTwo) {
        this.shippingLineTwo = shippingLineTwo;
    }

    public String getCustAddressthree() {
        return custAddressthree;
    }

    public void setCustAddressthree(String custAddressthree) {
        this.custAddressthree = custAddressthree;
    }

    public String getCustAddressTwo() {
        return custAddressTwo;
    }

    public void setCustAddressTwo(String custAddressTwo) {
        this.custAddressTwo = custAddressTwo;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getBillOfEntry() {
        return billOfEntry;
    }

    public void setBillOfEntry(String billOfEntry) {
        this.billOfEntry = billOfEntry;
    }

    public String getBillingPartyId() {
        return billingPartyId;
    }

    public void setBillingPartyId(String billingPartyId) {
        this.billingPartyId = billingPartyId;
    }

    public String getBillingPartyName() {
        return billingPartyName;
    }

    public void setBillingPartyName(String billingPartyName) {
        this.billingPartyName = billingPartyName;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCashDate() {
        return cashDate;
    }

    public void setCashDate(String cashDate) {
        this.cashDate = cashDate;
    }

    public String getChargeamount() {
        return chargeamount;
    }

    public void setChargeamount(String chargeamount) {
        this.chargeamount = chargeamount;
    }

    public String getChargeamt() {
        return chargeamt;
    }

    public void setChargeamt(String chargeamt) {
        this.chargeamt = chargeamt;
    }

    public String getConsignmentContainerId() {
        return consignmentContainerId;
    }

    public void setConsignmentContainerId(String consignmentContainerId) {
        this.consignmentContainerId = consignmentContainerId;
    }

    public String getContainerFreightCharge() {
        return containerFreightCharge;
    }

    public void setContainerFreightCharge(String containerFreightCharge) {
        this.containerFreightCharge = containerFreightCharge;
    }

    public String getContainerFreightRate() {
        return containerFreightRate;
    }

    public void setContainerFreightRate(String containerFreightRate) {
        this.containerFreightRate = containerFreightRate;
    }

    public String getContainerLinerId() {
        return containerLinerId;
    }

    public void setContainerLinerId(String containerLinerId) {
        this.containerLinerId = containerLinerId;
    }

    public String getContainerQtys() {
        return containerQtys;
    }

    public void setContainerQtys(String containerQtys) {
        this.containerQtys = containerQtys;
    }

    public String getContainerTypes() {
        return containerTypes;
    }

    public void setContainerTypes(String containerTypes) {
        this.containerTypes = containerTypes;
    }

    public String getDcmremarks() {
        return dcmremarks;
    }

    public void setDcmremarks(String dcmremarks) {
        this.dcmremarks = dcmremarks;
    }

    public String getDcmunit() {
        return dcmunit;
    }

    public void setDcmunit(String dcmunit) {
        this.dcmunit = dcmunit;
    }

    public String getDestatus() {
        return destatus;
    }

    public void setDestatus(String destatus) {
        this.destatus = destatus;
    }

    public String getDetention() {
        return detention;
    }

    public void setDetention(String detention) {
        this.detention = detention;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getIncentiveStatus() {
        return incentiveStatus;
    }

    public void setIncentiveStatus(String incentiveStatus) {
        this.incentiveStatus = incentiveStatus;
    }

    public String getLinerId() {
        return linerId;
    }

    public void setLinerId(String linerId) {
        this.linerId = linerId;
    }

    public String getLinerName() {
        return linerName;
    }

    public void setLinerName(String linerName) {
        this.linerName = linerName;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getMiscStatus() {
        return miscStatus;
    }

    public void setMiscStatus(String miscStatus) {
        this.miscStatus = miscStatus;
    }

    public String getOldFuelPriceId() {
        return oldFuelPriceId;
    }

    public void setOldFuelPriceId(String oldFuelPriceId) {
        this.oldFuelPriceId = oldFuelPriceId;
    }

    public String getPcmremarks() {
        return pcmremarks;
    }

    public void setPcmremarks(String pcmremarks) {
        this.pcmremarks = pcmremarks;
    }

    public String getPcmunit() {
        return pcmunit;
    }

    public void setPcmunit(String pcmunit) {
        this.pcmunit = pcmunit;
    }

    public String getPenality() {
        return penality;
    }

    public void setPenality(String penality) {
        this.penality = penality;
    }

    public String getPlannedStatus() {
        return plannedStatus;
    }

    public void setPlannedStatus(String plannedStatus) {
        this.plannedStatus = plannedStatus;
    }

    public String getSecCustId() {
        return secCustId;
    }

    public void setSecCustId(String secCustId) {
        this.secCustId = secCustId;
    }

    public String getShipingLineNo() {
        return shipingLineNo;
    }

    public void setShipingLineNo(String shipingLineNo) {
        this.shipingLineNo = shipingLineNo;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTollStatus() {
        return tollStatus;
    }

    public void setTollStatus(String tollStatus) {
        this.tollStatus = tollStatus;
    }

    public String[] getTripIds() {
        return tripIds;
    }

    public void setTripIds(String[] tripIds) {
        this.tripIds = tripIds;
    }

    public String getContainerQty1() {
        return containerQty1;
    }

    public void setContainerQty1(String containerQty1) {
        this.containerQty1 = containerQty1;
    }

    public String getContainerQuantity() {
        return containerQuantity;
    }

    public void setContainerQuantity(String containerQuantity) {
        this.containerQuantity = containerQuantity;
    }

    public String getContainer_20ft_Milleage() {
        return container_20ft_Milleage;
    }

    public void setContainer_20ft_Milleage(String container_20ft_Milleage) {
        this.container_20ft_Milleage = container_20ft_Milleage;
    }

    public String getContainer_2_20ft_Milleage() {
        return container_2_20ft_Milleage;
    }

    public void setContainer_2_20ft_Milleage(String container_2_20ft_Milleage) {
        this.container_2_20ft_Milleage = container_2_20ft_Milleage;
    }

    public String getContainer_40ft_Milleage() {
        return container_40ft_Milleage;
    }

    public void setContainer_40ft_Milleage(String container_40ft_Milleage) {
        this.container_40ft_Milleage = container_40ft_Milleage;
    }

    public String getEmptyContainerMilleage() {
        return emptyContainerMilleage;
    }

    public void setEmptyContainerMilleage(String emptyContainerMilleage) {
        this.emptyContainerMilleage = emptyContainerMilleage;
    }

    public String getFuelCostPerKm20ftCon() {
        return fuelCostPerKm20ftCon;
    }

    public void setFuelCostPerKm20ftCon(String fuelCostPerKm20ftCon) {
        this.fuelCostPerKm20ftCon = fuelCostPerKm20ftCon;
    }

    public String getFuelCostPerKm2_20ftCon() {
        return fuelCostPerKm2_20ftCon;
    }

    public void setFuelCostPerKm2_20ftCon(String fuelCostPerKm2_20ftCon) {
        this.fuelCostPerKm2_20ftCon = fuelCostPerKm2_20ftCon;
    }

    public String getFuelCostPerKm40ftCon() {
        return fuelCostPerKm40ftCon;
    }

    public void setFuelCostPerKm40ftCon(String fuelCostPerKm40ftCon) {
        this.fuelCostPerKm40ftCon = fuelCostPerKm40ftCon;
    }

    public String getFuelCostPerKmEmpCon() {
        return fuelCostPerKmEmpCon;
    }

    public void setFuelCostPerKmEmpCon(String fuelCostPerKmEmpCon) {
        this.fuelCostPerKmEmpCon = fuelCostPerKmEmpCon;
    }

    public String[] getFuelCostPerKms20ftCon() {
        return fuelCostPerKms20ftCon;
    }

    public void setFuelCostPerKms20ftCon(String[] fuelCostPerKms20ftCon) {
        this.fuelCostPerKms20ftCon = fuelCostPerKms20ftCon;
    }

    public String[] getFuelCostPerKms2_20ftCon() {
        return fuelCostPerKms2_20ftCon;
    }

    public void setFuelCostPerKms2_20ftCon(String[] fuelCostPerKms2_20ftCon) {
        this.fuelCostPerKms2_20ftCon = fuelCostPerKms2_20ftCon;
    }

    public String[] getFuelCostPerKms40ftCon() {
        return fuelCostPerKms40ftCon;
    }

    public void setFuelCostPerKms40ftCon(String[] fuelCostPerKms40ftCon) {
        this.fuelCostPerKms40ftCon = fuelCostPerKms40ftCon;
    }

    public String[] getFuelCostPerKmsEmpCon() {
        return fuelCostPerKmsEmpCon;
    }

    public void setFuelCostPerKmsEmpCon(String[] fuelCostPerKmsEmpCon) {
        this.fuelCostPerKmsEmpCon = fuelCostPerKmsEmpCon;
    }

    public String getLoadTypeId() {
        return loadTypeId;
    }

    public void setLoadTypeId(String loadTypeId) {
        this.loadTypeId = loadTypeId;
    }

    public String[] getVehExpense20ftCon() {
        return vehExpense20ftCon;
    }

    public void setVehExpense20ftCon(String[] vehExpense20ftCon) {
        this.vehExpense20ftCon = vehExpense20ftCon;
    }

    public String[] getVehExpense2_20ftCon() {
        return vehExpense2_20ftCon;
    }

    public void setVehExpense2_20ftCon(String[] vehExpense2_20ftCon) {
        this.vehExpense2_20ftCon = vehExpense2_20ftCon;
    }

    public String[] getVehExpense40ftCon() {
        return vehExpense40ftCon;
    }

    public void setVehExpense40ftCon(String[] vehExpense40ftCon) {
        this.vehExpense40ftCon = vehExpense40ftCon;
    }

    public String[] getVehExpenseEmpCon() {
        return vehExpenseEmpCon;
    }

    public void setVehExpenseEmpCon(String[] vehExpenseEmpCon) {
        this.vehExpenseEmpCon = vehExpenseEmpCon;
    }

    public String getVehExpenses20ftCon() {
        return vehExpenses20ftCon;
    }

    public void setVehExpenses20ftCon(String vehExpenses20ftCon) {
        this.vehExpenses20ftCon = vehExpenses20ftCon;
    }

    public String getVehExpenses2_20ftCon() {
        return vehExpenses2_20ftCon;
    }

    public void setVehExpenses2_20ftCon(String vehExpenses2_20ftCon) {
        this.vehExpenses2_20ftCon = vehExpenses2_20ftCon;
    }

    public String getVehExpenses40ftCon() {
        return vehExpenses40ftCon;
    }

    public void setVehExpenses40ftCon(String vehExpenses40ftCon) {
        this.vehExpenses40ftCon = vehExpenses40ftCon;
    }

    public String getVehExpensesEmpCon() {
        return vehExpensesEmpCon;
    }

    public void setVehExpensesEmpCon(String vehExpensesEmpCon) {
        this.vehExpensesEmpCon = vehExpensesEmpCon;
    }

    public String getContractRouteInteream() {
        return contractRouteInteream;
    }

    public void setContractRouteInteream(String contractRouteInteream) {
        this.contractRouteInteream = contractRouteInteream;
    }

    public String getExpcontractRouteDestination() {
        return expcontractRouteDestination;
    }

    public void setExpcontractRouteDestination(String expcontractRouteDestination) {
        this.expcontractRouteDestination = expcontractRouteDestination;
    }

    public String getExpcontractRouteOrigin() {
        return expcontractRouteOrigin;
    }

    public void setExpcontractRouteOrigin(String expcontractRouteOrigin) {
        this.expcontractRouteOrigin = expcontractRouteOrigin;
    }

    public String[] getFuelWOReefer() {
        return fuelWOReefer;
    }

    public void setFuelWOReefer(String[] fuelWOReefer) {
        this.fuelWOReefer = fuelWOReefer;
    }

    public String getFuelWOReeferReq() {
        return fuelWOReeferReq;
    }

    public void setFuelWOReeferReq(String fuelWOReeferReq) {
        this.fuelWOReeferReq = fuelWOReeferReq;
    }

    public String[] getFuelWithReefer() {
        return fuelWithReefer;
    }

    public void setFuelWithReefer(String[] fuelWithReefer) {
        this.fuelWithReefer = fuelWithReefer;
    }

    public String getFuelWithReeferReq() {
        return fuelWithReeferReq;
    }

    public void setFuelWithReeferReq(String fuelWithReeferReq) {
        this.fuelWithReeferReq = fuelWithReeferReq;
    }

    public String[] getContainerNO() {
        return containerNO;
    }

    public void setContainerNO(String[] containerNO) {
        this.containerNO = containerNO;
    }

    public String[] getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String[] containerQty) {
        this.containerQty = containerQty;
    }

    public String[] getContainerType() {
        return containerType;
    }

    public void setContainerType(String[] containerType) {
        this.containerType = containerType;
    }

    public String getEntrymenu() {
        return entrymenu;
    }

    public void setEntrymenu(String entrymenu) {
        this.entrymenu = entrymenu;
    }

    public String getEntrytype() {
        return entrytype;
    }

    public void setEntrytype(String entrytype) {
        this.entrytype = entrytype;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getFunctionmenu() {
        return functionmenu;
    }

    public void setFunctionmenu(String functionmenu) {
        this.functionmenu = functionmenu;
    }

    public String getFunctionuri() {
        return functionuri;
    }

    public void setFunctionuri(String functionuri) {
        this.functionuri = functionuri;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getSubmodule() {
        return submodule;
    }

    public void setSubmodule(String submodule) {
        this.submodule = submodule;
    }

    public String getTrailerTypeName() {
        return trailerTypeName;
    }

    public void setTrailerTypeName(String trailerTypeName) {
        this.trailerTypeName = trailerTypeName;
    }

    public String getAmounts() {
        return amounts;
    }

    public void setAmounts(String amounts) {
        this.amounts = amounts;
    }

    public String getEmailBcc() {
        return emailBcc;
    }

    public void setEmailBcc(String emailBcc) {
        this.emailBcc = emailBcc;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getRtoChargeId() {
        return rtoChargeId;
    }

    public void setRtoChargeId(String rtoChargeId) {
        this.rtoChargeId = rtoChargeId;
    }

    public String getRtoCode() {
        return rtoCode;
    }

    public void setRtoCode(String rtoCode) {
        this.rtoCode = rtoCode;
    }

    public String getRtoId() {
        return rtoId;
    }

    public void setRtoId(String rtoId) {
        this.rtoId = rtoId;
    }

    public String getRtoName() {
        return rtoName;
    }

    public void setRtoName(String rtoName) {
        this.rtoName = rtoName;
    }

    public String getRtoVehicleId() {
        return rtoVehicleId;
    }

    public void setRtoVehicleId(String rtoVehicleId) {
        this.rtoVehicleId = rtoVehicleId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(String agentNo) {
        this.agentNo = agentNo;
    }

    public String getCompanyID1() {
        return companyID1;
    }

    public void setCompanyID1(String companyID1) {
        this.companyID1 = companyID1;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getInsId() {
        return insId;
    }

    public void setInsId(String insId) {
        this.insId = insId;
    }

    public String getContainerNo() {
        return ContainerNo;
    }

    public void setContainerNo(String ContainerNo) {
        this.ContainerNo = ContainerNo;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    public String[] getMovementTypeIds() {
        return movementTypeIds;
    }

    public void setMovementTypeIds(String[] movementTypeIds) {
        this.movementTypeIds = movementTypeIds;
    }

    public String getMovementTypeName() {
        return movementTypeName;
    }

    public void setMovementTypeName(String movementTypeName) {
        this.movementTypeName = movementTypeName;
    }

    public String getTripConfigId() {
        return tripConfigId;
    }

    public void setTripConfigId(String tripConfigId) {
        this.tripConfigId = tripConfigId;
    }

    public String[] getTripConfigIds() {
        return tripConfigIds;
    }

    public void setTripConfigIds(String[] tripConfigIds) {
        this.tripConfigIds = tripConfigIds;
    }

    public String getTripConfigInd() {
        return tripConfigInd;
    }

    public void setTripConfigInd(String tripConfigInd) {
        this.tripConfigInd = tripConfigInd;
    }

    public String[] getTripConfigInds() {
        return tripConfigInds;
    }

    public void setTripConfigInds(String[] tripConfigInds) {
        this.tripConfigInds = tripConfigInds;
    }

    public String getTripTypeId() {
        return tripTypeId;
    }

    public void setTripTypeId(String tripTypeId) {
        this.tripTypeId = tripTypeId;
    }

    public String[] getTripTypeIds() {
        return tripTypeIds;
    }

    public void setTripTypeIds(String[] tripTypeIds) {
        this.tripTypeIds = tripTypeIds;
    }

    public String getTripTypeName() {
        return tripTypeName;
    }

    public void setTripTypeName(String tripTypeName) {
        this.tripTypeName = tripTypeName;
    }

    public String getDala() {
        return dala;
    }

    public void setDala(String dala) {
        this.dala = dala;
    }

    public String getDriverBachat() {
        return driverBachat;
    }

    public void setDriverBachat(String driverBachat) {
        this.driverBachat = driverBachat;
    }

    public String getFuelDg() {
        return fuelDg;
    }

    public void setFuelDg(String fuelDg) {
        this.fuelDg = fuelDg;
    }

    public String getFuelVehicle() {
        return fuelVehicle;
    }

    public void setFuelVehicle(String fuelVehicle) {
        this.fuelVehicle = fuelVehicle;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public String getToll() {
        return toll;
    }

    public void setToll(String toll) {
        this.toll = toll;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getMarketHire() {
        return marketHire;
    }

    public void setMarketHire(String marketHire) {
        this.marketHire = marketHire;
    }

    public String getOrgWithoutReeferRate() {
        return orgWithoutReeferRate;
    }

    public void setOrgWithoutReeferRate(String orgWithoutReeferRate) {
        this.orgWithoutReeferRate = orgWithoutReeferRate;
    }

    public String getMarketHireWithReefer() {
        return marketHireWithReefer;
    }

    public void setMarketHireWithReefer(String marketHireWithReefer) {
        this.marketHireWithReefer = marketHireWithReefer;
    }

    public String getOrgWithReeferRate() {
        return orgWithReeferRate;
    }

    public void setOrgWithReeferRate(String orgWithReeferRate) {
        this.orgWithReeferRate = orgWithReeferRate;
    }

    public String[] getVehTypeIdValue() {
        return vehTypeIdValue;
    }

    public void setVehTypeIdValue(String[] vehTypeIdValue) {
        this.vehTypeIdValue = vehTypeIdValue;
    }

    public String[] getContainerTypeValue() {
        return containerTypeValue;
    }

    public void setContainerTypeValue(String[] containerTypeValue) {
        this.containerTypeValue = containerTypeValue;
    }

    public String[] getContainerName1() {
        return containerName1;
    }

    public void setContainerName1(String[] containerName1) {
        this.containerName1 = containerName1;
    }

    public String[] getContainerLinerName() {
        return containerLinerName;
    }

    public void setContainerLinerName(String[] containerLinerName) {
        this.containerLinerName = containerLinerName;
    }

    public String[] getContainerFreightCharges() {
        return containerFreightCharges;
    }

    public void setContainerFreightCharges(String[] containerFreightCharges) {
        this.containerFreightCharges = containerFreightCharges;
    }

    public String[] getConsignmentContainerIds() {
        return consignmentContainerIds;
    }

    public void setConsignmentContainerIds(String[] consignmentContainerIds) {
        this.consignmentContainerIds = consignmentContainerIds;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGstCategoryCode() {
        return gstCategoryCode;
    }

    public void setGstCategoryCode(String gstCategoryCode) {
        this.gstCategoryCode = gstCategoryCode;
    }

    public String getGstCategoryId() {
        return gstCategoryId;
    }

    public void setGstCategoryId(String gstCategoryId) {
        this.gstCategoryId = gstCategoryId;
    }

    public String getGstCategoryName() {
        return gstCategoryName;
    }

    public void setGstCategoryName(String gstCategoryName) {
        this.gstCategoryName = gstCategoryName;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstCodeMasterId() {
        return gstCodeMasterId;
    }

    public void setGstCodeMasterId(String gstCodeMasterId) {
        this.gstCodeMasterId = gstCodeMasterId;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getGstRateDetailId() {
        return gstRateDetailId;
    }

    public void setGstRateDetailId(String gstRateDetailId) {
        this.gstRateDetailId = gstRateDetailId;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public String getSacDescription() {
        return sacDescription;
    }

    public void setSacDescription(String sacDescription) {
        this.sacDescription = sacDescription;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getGstRateId() {
        return gstRateId;
    }

    public void setGstRateId(String gstRateId) {
        this.gstRateId = gstRateId;
    }

    public String getEffectiveHour() {
        return effectiveHour;
    }

    public void setEffectiveHour(String effectiveHour) {
        this.effectiveHour = effectiveHour;
    }

    public String getEffectiveMin() {
        return effectiveMin;
    }

    public void setEffectiveMin(String effectiveMin) {
        this.effectiveMin = effectiveMin;
    }

    public String getHsnName() {
        return hsnName;
    }

    public void setHsnName(String hsnName) {
        this.hsnName = hsnName;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getHsnDescription() {
        return hsnDescription;
    }

    public void setHsnDescription(String hsnDescription) {
        this.hsnDescription = hsnDescription;
    }

    public String getGstProductId() {
        return gstProductId;
    }

    public void setGstProductId(String gstProductId) {
        this.gstProductId = gstProductId;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getGstProductCategoryCode() {
        return gstProductCategoryCode;
    }

    public void setGstProductCategoryCode(String gstProductCategoryCode) {
        this.gstProductCategoryCode = gstProductCategoryCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getCustTypeId() {
        return custTypeId;
    }

    public void setCustTypeId(String custTypeId) {
        this.custTypeId = custTypeId;
    }

    public String getFixedCreditLimit() {
        return fixedCreditLimit;
    }

    public void setFixedCreditLimit(String fixedCreditLimit) {
        this.fixedCreditLimit = fixedCreditLimit;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getUsedLimit() {
        return usedLimit;
    }

    public void setUsedLimit(String usedLimit) {
        this.usedLimit = usedLimit;
    }

    public String getAvailLimit() {
        return availLimit;
    }

    public void setAvailLimit(String availLimit) {
        this.availLimit = availLimit;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getCommodityDesc() {
        return commodityDesc;
    }

    public void setCommodityDesc(String commodityDesc) {
        this.commodityDesc = commodityDesc;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }

    public String getSupTyp() {
        return supTyp;
    }

    public void setSupTyp(String supTyp) {
        this.supTyp = supTyp;
    }

    public String getBillingLegalName() {
        return billingLegalName;
    }

    public void setBillingLegalName(String billingLegalName) {
        this.billingLegalName = billingLegalName;
    }

    public String getBillingTradeName() {
        return billingTradeName;
    }

    public void setBillingTradeName(String billingTradeName) {
        this.billingTradeName = billingTradeName;
    }

    public String getBillingGstin() {
        return billingGstin;
    }

    public void setBillingGstin(String billingGstin) {
        this.billingGstin = billingGstin;
    }

    public String getPlaceOfSupply() {
        return placeOfSupply;
    }

    public void setPlaceOfSupply(String placeOfSupply) {
        this.placeOfSupply = placeOfSupply;
    }

    public String getBilllingAddress() {
        return billlingAddress;
    }

    public void setBilllingAddress(String billlingAddress) {
        this.billlingAddress = billlingAddress;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getPlaceOfState() {
        return placeOfState;
    }

    public void setPlaceOfState(String placeOfState) {
        this.placeOfState = placeOfState;
    }

    public String getBillingPin() {
        return billingPin;
    }

    public void setBillingPin(String billingPin) {
        this.billingPin = billingPin;
    }

    public String getSnos() {
        return snos;
    }

    public void setSnos(String snos) {
        this.snos = snos;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getGorS() {
        return gorS;
    }

    public void setGorS(String gorS) {
        this.gorS = gorS;
    }

    public String getHsnCd() {
        return hsnCd;
    }

    public void setHsnCd(String hsnCd) {
        this.hsnCd = hsnCd;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(String itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public String getItemTaxableValue() {
        return itemTaxableValue;
    }

    public void setItemTaxableValue(String itemTaxableValue) {
        this.itemTaxableValue = itemTaxableValue;
    }

    public String getGstPercent() {
        return gstPercent;
    }

    public void setGstPercent(String gstPercent) {
        this.gstPercent = gstPercent;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getCessValue() {
        return cessValue;
    }

    public void setCessValue(String cessValue) {
        this.cessValue = cessValue;
    }

    public String getStatecessValue() {
        return statecessValue;
    }

    public void setStatecessValue(String statecessValue) {
        this.statecessValue = statecessValue;
    }

    public String getNetamount() {
        return netamount;
    }

    public void setNetamount(String netamount) {
        this.netamount = netamount;
    }

    public String getTotalTaxableValue() {
        return totalTaxableValue;
    }

    public void setTotalTaxableValue(String totalTaxableValue) {
        this.totalTaxableValue = totalTaxableValue;
    }

    public String getTotalIgst() {
        return totalIgst;
    }

    public void setTotalIgst(String totalIgst) {
        this.totalIgst = totalIgst;
    }

    public String getTotalCgst() {
        return totalCgst;
    }

    public void setTotalCgst(String totalCgst) {
        this.totalCgst = totalCgst;
    }

    public String getTotalSgst() {
        return totalSgst;
    }

    public void setTotalSgst(String totalSgst) {
        this.totalSgst = totalSgst;
    }

    public String getTotalNetAmount() {
        return totalNetAmount;
    }

    public void setTotalNetAmount(String totalNetAmount) {
        this.totalNetAmount = totalNetAmount;
    }

    public String getCompLegalName() {
        return compLegalName;
    }

    public void setCompLegalName(String compLegalName) {
        this.compLegalName = compLegalName;
    }

    public String getCompGstin() {
        return compGstin;
    }

    public void setCompGstin(String compGstin) {
        this.compGstin = compGstin;
    }

    public String getCompAddress() {
        return compAddress;
    }

    public void setCompAddress(String compAddress) {
        this.compAddress = compAddress;
    }

    public String getCompLocation() {
        return compLocation;
    }

    public void setCompLocation(String compLocation) {
        this.compLocation = compLocation;
    }

    public String getCompState() {
        return compState;
    }

    public void setCompState(String compState) {
        this.compState = compState;
    }

    public String getCompPin() {
        return compPin;
    }

    public void setCompPin(String compPin) {
        this.compPin = compPin;
    }

    public String getIsServc() {
        return isServc;
    }

    public void setIsServc(String isServc) {
        this.isServc = isServc;
    }

    public String getPrdDesc() {
        return prdDesc;
    }

    public void setPrdDesc(String prdDesc) {
        this.prdDesc = prdDesc;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getTotAmount() {
        return totAmount;
    }

    public void setTotAmount(String totAmount) {
        this.totAmount = totAmount;
    }

    public String getAssAmt() {
        return assAmt;
    }

    public void setAssAmt(String assAmt) {
        this.assAmt = assAmt;
    }

    public String getCgstPercent() {
        return cgstPercent;
    }

    public void setCgstPercent(String cgstPercent) {
        this.cgstPercent = cgstPercent;
    }

    public String getSgstPercent() {
        return sgstPercent;
    }

    public void setSgstPercent(String sgstPercent) {
        this.sgstPercent = sgstPercent;
    }

    public String getIgstPercent() {
        return igstPercent;
    }

    public void setIgstPercent(String igstPercent) {
        this.igstPercent = igstPercent;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getExpenseDesc() {
        return expenseDesc;
    }

    public void setExpenseDesc(String expenseDesc) {
        this.expenseDesc = expenseDesc;
    }

    public String getBillingCustId() {
        return billingCustId;
    }

    public void setBillingCustId(String billingCustId) {
        this.billingCustId = billingCustId;
    }

    public String getSellerDetails() {
        return sellerDetails;
    }

    public void setSellerDetails(String sellerDetails) {
        this.sellerDetails = sellerDetails;
    }

    public String getBillingCustName() {
        return billingCustName;
    }

    public void setBillingCustName(String billingCustName) {
        this.billingCustName = billingCustName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingPanNo() {
        return billingPanNo;
    }

    public void setBillingPanNo(String billingPanNo) {
        this.billingPanNo = billingPanNo;
    }

    public String getBillingPincode() {
        return billingPincode;
    }

    public void setBillingPincode(String billingPincode) {
        this.billingPincode = billingPincode;
    }

    public String getBillingPos() {
        return billingPos;
    }

    public void setBillingPos(String billingPos) {
        this.billingPos = billingPos;
    }

    public String getBillingLocation() {
        return billingLocation;
    }

    public void setBillingLocation(String billingLocation) {
        this.billingLocation = billingLocation;
    }

    public String getUpload_status() {
        return upload_status;
    }

    public void setUpload_status(String upload_status) {
        this.upload_status = upload_status;
    }

    public String getRoundoff() {
        return roundoff;
    }

    public void setRoundoff(String roundoff) {
        this.roundoff = roundoff;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIgstOnIntra() {
        return igstOnIntra;
    }

    public void setIgstOnIntra(String igstOnIntra) {
        this.igstOnIntra = igstOnIntra;
    }

    public String getRegRev() {
        return regRev;
    }

    public void setRegRev(String regRev) {
        this.regRev = regRev;
    }

    public String getTaxSch() {
        return taxSch;
    }

    public void setTaxSch(String taxSch) {
        this.taxSch = taxSch;
    }

    public String getItemAssAount() {
        return itemAssAount;
    }

    public void setItemAssAount(String itemAssAount) {
        this.itemAssAount = itemAssAount;
    }

    public String getBilllingLocation() {
        return billlingLocation;
    }

    public void setBilllingLocation(String billlingLocation) {
        this.billlingLocation = billlingLocation;
    }

    public String getApiErrorMsg() {
        return apiErrorMsg;
    }

    public void setApiErrorMsg(String apiErrorMsg) {
        this.apiErrorMsg = apiErrorMsg;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getCreditNoteId() {
        return creditNoteId;
    }

    public void setCreditNoteId(String creditNoteId) {
        this.creditNoteId = creditNoteId;
    }

    public String getTerifType() {
        return terifType;
    }

    public void setTerifType(String terifType) {
        this.terifType = terifType;
    }

    public String getTimeSlotTo() {
        return timeSlotTo;
    }

    public void setTimeSlotTo(String timeSlotTo) {
        this.timeSlotTo = timeSlotTo;
    }

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

}
