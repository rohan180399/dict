
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

       function submitPage(){
           var chek='true';
           if(chek=='true'){
                document.maxProblem.action='/throttle/topProblemGraph.do';
                document.maxProblem.submit();
           }
       }
       function validation(){
               if(textValidation(document.maxProblem.fromDate,'From Date')){
                   document.maxProblem.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.maxProblem.toDate,'To Date')){
                   document.maxProblem.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           if(a!='null'){
               
                document.maxProblem.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.maxProblem.toDate.value='<%=request.getAttribute("toDate")%>';
           }
       }
    </script>
    <body>
        <form  name="maxProblem" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
 <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Top Problem</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
    <td>From Date</td>
    <td><input name="fromDate" class="form-control" type="text" value="" size="20">
    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.maxProblem.fromDate,'dd-mm-yyyy',this)"/></td>
   <td height="25">To Date</td>
    <td><input name="toDate" class="form-control" type="text" value="" size="20">
    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.maxProblem.toDate,'dd-mm-yyyy',this)"/></td>
<td height="25">Mfr</td>
<td height="25">
    <select name="mfr" style="width:125px"><option value="0">All</option>
        <%if(request.getAttribute("ManufacturerList")!=null){
            ArrayList manufacturerList = (ArrayList)request.getAttribute("ManufacturerList");
            Iterator itr = manufacturerList.iterator();
            ReportTO repTO = new ReportTO();
            while(itr.hasNext()){
                repTO = (ReportTO) itr.next();
            %>
            <option value = '<%=repTO.getMfrId()%>'><%=repTO.getMfrName()%></option>
            <%
            }
        }
            %>
 </select>
</td>
 </tr>
<tr>
<td height="25">Usage Type</td>
<td height="25">
    <select name="usageType" style="width:125px"><option value="0">All</option>
        <%if(request.getAttribute("UsageTypeList")!=null){
            ArrayList UsageTypeList = (ArrayList)request.getAttribute("UsageTypeList");
            Iterator itr1 = UsageTypeList.iterator();
            ReportTO repTO1 = new ReportTO();
            while(itr1.hasNext()){
                repTO1 = (ReportTO) itr1.next();
            %>
            <option value = '<%=repTO1.getUsageTypeId()%>'><%=repTO1.getUsageType()%></option>
            <%
            }
        }

            %>
 </select>
</td>

<td height="25">Age</td>
<td height="25">
    <select name="age" class="form-control">
    <option value="0">All</option>
    <option value="1"><1</option>
    <option value="2"><2</option>
    <option value="3"><3</option>
    <option value="4"><4</option>
    <option value="5"><5</option>
</select>
</td>
<!--
<td height="25">Vehicle Type</td>
<td class="text1" height="25"><select name="age" class="form-control">
    <option value="0">All</option>
    <option value="1011">HEAVY VEHICLE</option>
    <option value="1012">LMV</option>
    <option value="1013">CARGO</option>
</select></td>   -->
<td colspan="2"><input type="button" class="button" value="Search" onclick="submitPage();" /></td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>
           
            
    <%

            ArrayList topProblemGraph= (ArrayList) request.getAttribute("topProblemGraph");
            

            if(request.getAttribute("topProblemGraph")!=null)
                {
            if(topProblemGraph.size()!=0 ){
            System.out.println("inside jsp");
            String dataset= "";
            String set="";
            String  strXML = "<graph caption='Top Problem'  hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='0' showvalues='0' numdivlines='3' numVdivlines='0'   rotateNames='0'>";
           
             String categories="<categories >";
            Iterator monthName=topProblemGraph.iterator();
            ReportTO monthNameTO=new ReportTO();
            
            while(monthName.hasNext()){
                  monthNameTO=(ReportTO) monthName.next();
                  String temp=monthNameTO.getProblemName();                  
                  categories+="<category name='"+temp+"' />";                  
                 }
            categories+="</categories>";


              
            dataset="<dataset seriesName='No of Times' color='8BBA09' anchorBorderColor='yellow' anchorBgColor='yellow'>" ;
                Iterator probitr=topProblemGraph.iterator();
            ReportTO probTO=new ReportTO();
                 while(probitr.hasNext()){
                  probTO=(ReportTO) probitr.next();
             set+="<set value='"+probTO.getTotalQty()+"' />";
            }
 String contentSet=dataset+set;
 contentSet+="</dataset>";
  contentSet+="</graph>";
    strXML+=categories+contentSet;      
         
         

            %>

            <p>
                  <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_MSBar2D.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML %>" />
                <jsp:param name="chartId" value="productSales" />
                <jsp:param name="chartWidth" value="800" />
                <jsp:param name="chartHeight" value="550" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                </td>
            </tr>

        </table>
            </p>
            <%}}%>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
