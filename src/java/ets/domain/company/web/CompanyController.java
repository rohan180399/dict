/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.company.web;

/**
 *
 * @author vinoth
 */
import ets.arch.web.BaseController;
import ets.domain.company.business.CompanyBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.company.business.CompanyTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import java.io.*;

public class CompanyController extends BaseController {

    CompanyCommand companycommand;
    CompanyBP companyBP;
    LoginBP loginBP;
    CustomerBP customerBP;

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public CompanyCommand getCompanycommand() {
        return companycommand;
    }

    public void setCompanycommand(CompanyCommand companycommand) {
        this.companycommand = companycommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

public ModelAndView handleAddCompPage(HttpServletRequest request, HttpServletResponse response, CompanyCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList companyTypes = new ArrayList();
        companycommand = command;
        String menuPath = "";
        menuPath = "HRMS >> Company >> Add Company ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Add Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                path = "content/Company/newCompany.jsp";
                companyTypes = companyBP.processGetCompanyTypes();
                request.setAttribute("companyTypes", companyTypes);
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);

                ArrayList organizationList = new ArrayList();
                organizationList = customerBP.getOrganizationList();
                request.setAttribute("organizationList",organizationList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    
    

    public ModelAndView handleAddNewCompany(HttpServletRequest request, HttpServletResponse reponse, CompanyCommand command) throws IOException {
    
            HttpSession session = request.getSession();
            int status = 0;
            String path = "";
            companycommand = command;
            int userId = (Integer) session.getAttribute("userId");
            ArrayList companyTypes = new ArrayList();
            ArrayList companyList = new ArrayList();
            CompanyTO companyTO = new CompanyTO();
            String menuPath = "";
            menuPath = "HRMS >>Company  ";
            String pageTitle = "Manage Company";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            try {
                if (companycommand.getName() != null && companycommand.getName() != "") {
                    companyTO.setName(companycommand.getName());
                }
                if (companycommand.getAddress() != null && companycommand.getAddress() != "") {
                    companyTO.setAddress(companycommand.getAddress());
                }
                if (companycommand.getCompanyTypeId() != null && companycommand.getCompanyTypeId() != "") {
                    companyTO.setCompanyTypeId(Integer.parseInt(companycommand.getCompanyTypeId()));
                }
                if (companycommand.getCity() != null && companycommand.getCity() != "") {
                    companyTO.setCity(companycommand.getCity());
                }
                if (companycommand.getDistrict() != null && companycommand.getDistrict() != "") {
                    companyTO.setDistrict(companycommand.getDistrict());
                }
    //            if (companycommand.getState() != null && companycommand.getState() != "") {
    //                companyTO.setState(companycommand.getState());
    //            }
                if (companycommand.getPinCode() != null && companycommand.getPinCode() != "") {
                    companyTO.setPinCode(companycommand.getPinCode());
                }
                if (companycommand.getPhone1() != null && companycommand.getPhone1() != "") {
                    companyTO.setPhone1(companycommand.getPhone1());
                }
                if (companycommand.getPhone2() != null && companycommand.getPhone2() != "") {
                    companyTO.setPhone2(companycommand.getPhone2());
                }
                if (companycommand.getFax() != null && companycommand.getFax() != "") {
                    companyTO.setFax(companycommand.getFax());
                }
                if (companycommand.getEmail() != null && companycommand.getEmail() != "") {
                    companyTO.setEmail(companycommand.getEmail());
                }
                if (companycommand.getWeb() != null && companycommand.getWeb() != "") {
                    companyTO.setWeb(companycommand.getWeb());
                }
                if (companycommand.getStateId() != null && companycommand.getStateId() != "") {
                    companyTO.setStateId(companycommand.getStateId());
                    System.out.println("getStateId"+companycommand.getStateId());
                }
                if (companycommand.getOrganizationId() != null && companycommand.getOrganizationId() != "") {
                    companyTO.setOrganizationId(companycommand.getOrganizationId());
                    System.out.println("getOrganizationId"+companycommand.getOrganizationId());
                }
                if (companycommand.getGstNo() != null && companycommand.getGstNo() != "") {
                    companyTO.setGstNo(companycommand.getGstNo());
                    System.out.println("getGstNo"+companycommand.getGstNo());
                }
                path = "content/Company/manageCompany.jsp";
                status = companyBP.processInsertNewCompany(companyTO, userId);
                companyList = companyBP.processGetCompanyList();
                request.setAttribute("companyLists", companyList);
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Company Added Successfully");
                }
    
            } catch (FPRuntimeException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Run time exception --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            } catch (FPBusinessException exception) {
                /*
                 * run time exception has occurred. Directed to error page.
                 */
                FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
                request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                        exception.getErrorMessage());
            } catch (Exception exception) {
                exception.printStackTrace();
                FPLogUtils.fpErrorLog("Failed to insert Company Details --> " + exception);
                return new ModelAndView("content/common/error.jsp");
            }
            return new ModelAndView(path);
    }
    

    public ModelAndView handleViewCompany1(HttpServletRequest request, HttpServletResponse response, CompanyCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String rollNumber = "";
        String path = "";
        HttpSession session = request.getSession();
        companycommand = command;
        String menuPath = "";
        menuPath = "HRMS >>Company  ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Manage Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyList = new ArrayList();
                path = "content/Company/manageCompany.jsp";
                companyList = companyBP.processGetCompanyList();
                request.setAttribute("companyLists", companyList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleViewCompanyDetail(HttpServletRequest request, HttpServletResponse response, CompanyCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        companycommand = command;
        String menuPath = "";
        menuPath = "HRMS >>Company >> Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Company";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyDetail = new ArrayList();
                path = "content/Company/viewCompany.jsp";
                int companyId = Integer.parseInt(request.getParameter("cmpId"));
                companyDetail = companyBP.processGetCompanyDetail(companyId);
                request.setAttribute("companyDetail", companyDetail);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }



public ModelAndView handleViewAlterScreen(HttpServletRequest request, HttpServletResponse response, CompanyCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        companycommand = command;
        String menuPath = "";
        menuPath = "HRMS >>Company   >> Details >>Alter ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Company-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "View Company Details";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList companyDetail = new ArrayList();
                ArrayList companyTypes = new ArrayList();
                path = "content/Company/alterCompany.jsp";
                //int companyId = Integer.parseInt(request.getParameter("cmpId"));
                companyDetail = companyBP.processGetCompanyDetail(companycommand.getCmpId());
                companyTypes = companyBP.processGetCompanyTypes();
                request.setAttribute("companyDetail", companyDetail);
                request.setAttribute("companyTypes", companyTypes);
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);

                ArrayList organizationList = new ArrayList();
                organizationList = customerBP.getOrganizationList();
                request.setAttribute("organizationList", organizationList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCompany1 --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleUpdateCompany(HttpServletRequest request, HttpServletResponse response, CompanyCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        companycommand = command;
        int userId = (Integer) session.getAttribute("userId");
        CompanyTO companyTO = new CompanyTO();
        String menuPath = "";
        menuPath = "HRMS >>Company ";
        String pageTitle = "Manage Company";
        request.setAttribute("pageTitle", pageTitle);
        System.out.print("UpdateCompany-------------->");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            companyTO.setCmpId(companycommand.getCmpId());
            if (companycommand.getName() != null && companycommand.getName() != "") {
                companyTO.setName(companycommand.getName());
            }
            if (companycommand.getAddress() != null && companycommand.getAddress() != "") {
                companyTO.setAddress(companycommand.getAddress());
            }
            if (companycommand.getCompanyTypeId() != null && companycommand.getCompanyTypeId() != "") {
                companyTO.setCompanyTypeId(Integer.parseInt(companycommand.getCompanyTypeId()));
            }
            if (companycommand.getCity() != null && companycommand.getCity() != "") {
                companyTO.setCity(companycommand.getCity());
            }
            if (companycommand.getDistrict() != null && companycommand.getDistrict() != "") {
                companyTO.setDistrict(companycommand.getDistrict());
            }
            if (companycommand.getState() != null && companycommand.getState() != "") {
                companyTO.setState(companycommand.getState());
            }
            if (companycommand.getPinCode() != null && companycommand.getPinCode() != "") {
                companyTO.setPinCode(companycommand.getPinCode());
            }
            if (companycommand.getPhone1() != null && companycommand.getPhone1() != "") {
                companyTO.setPhone1(companycommand.getPhone1());
            }
            if (companycommand.getPhone2() != null && companycommand.getPhone2() != "") {
                companyTO.setPhone2(companycommand.getPhone2());
            }
            if (companycommand.getFax() != null && companycommand.getFax() != "") {
                companyTO.setFax(companycommand.getFax());
            }
            if (companycommand.getEmail() != null && companycommand.getEmail() != "") {
                companyTO.setEmail(companycommand.getEmail());
            }
            if (companycommand.getWeb() != null && companycommand.getWeb() != "") {
                companyTO.setWeb(companycommand.getWeb());
            }
            if (companycommand.getStatus() != null && companycommand.getStatus() != "") {
                companyTO.setStatus(companycommand.getStatus());
            }
            if (companycommand.getStateId() != null && companycommand.getStateId() != "") {
                companyTO.setStateId(companycommand.getStateId());
            }
            if (companycommand.getOrganizationId() != null && companycommand.getOrganizationId() != "") {
                companyTO.setOrganizationId(companycommand.getOrganizationId());
            }
            if (companycommand.getGstNo() != null && companycommand.getGstNo() != "") {
                companyTO.setGstNo(companycommand.getGstNo());
            }
            path = "content/Company/manageCompany.jsp";
            status = companyBP.processUpdateCompany(companyTO, userId);
            ArrayList companyList = new ArrayList();
            companyList = companyBP.processGetCompanyList();
            request.setAttribute("companyLists", companyList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Company Details Modified Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Company Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }    
    
}
