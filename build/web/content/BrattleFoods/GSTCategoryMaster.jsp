<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script>
    function gstsubmit() {
//        alert("test");
        var errStr = "";
        //document.getElementById("categoryCode").value == ""
        //alert("document.getElementById(categoryCode -------   "+document.getElementById("gstType").value );
        var gstType=document.getElementById("gstType").value;
        if(gstType==1){
                if (document.getElementById("sacCode").value == "") {
                errStr = "Please select valid sac Code.\n";
                alert(errStr);
                document.getElementById("sacCode").focus();
                }
            if (document.getElementById("sacDescription").value == "") {
                errStr = "Please select valid sac Description.\n";
                alert(errStr);
                document.getElementById("sacDescription").focus();
                }
        }
        else{
                if (document.getElementById("hsnCode").value == "") {
                errStr = "Please select valid HSN Code.\n";
                alert(errStr);
                document.getElementById("sacCode").focus();

            }
            if (document.getElementById("hsnCode").value == "") {
                errStr = "Please select valid HSN Code.\n";
                alert(errStr);
                document.getElementById("hsnCode").focus();
            }
         }
         if (document.getElementById("categoryCode").value == "") {
            errStr = "Please select valid Category Code.\n";
            alert(errStr);
            document.getElementById("categoryCode").focus();
        }        
        else if (document.getElementById("categoryName").value == "") {
            errStr = "Please select valid Category Name.\n";
            alert(errStr);
            document.getElementById("categoryName").focus();
        }
        var gstType=document.getElementById("gstType1").value;
        if (errStr == "") {
            document.gstMaster.action = "/throttle/saveGSTCategoryMaster.do?gstType1="+gstType;
            document.gstMaster.method = "post";
            document.gstMaster.submit();
        }
    }
</script>
<script>
    function setValues(sno, gstCategoryId, categoryCode, categoryName, sacCode, sacDescription,gstType,hsnCode,hsnDescription,activeInd) {
//        alert(gstCategoryId)
        var count = parseInt(document.getElementById("count").value);
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("gstCategoryId").value = gstCategoryId;
        document.getElementById("categoryCode").value = categoryCode;
        document.getElementById("categoryName").value = categoryName;
        document.getElementById("sacCode").value = sacCode;
        document.getElementById("sacDescription").value = sacDescription;
        document.getElementById("gstType").value = gstType;
        document.getElementById("gstType1").value = gstType;
        
        document.getElementById("hsnCode").value = hsnCode;
        document.getElementById("hsnDescription").value = hsnDescription;
        document.getElementById("activeInd").value = activeInd;
        $('#gstType').attr("disabled", true); 
        //        $("#cityName").val().readOnly;
    }
</script>
<script>
    function gstName(val) {
//        alert(val);
        if (val == '0') {
            $("#service").hide();
            $("#product").hide();
            
        }else if(val == '1'){
            $("#service").show();
            $("#product").hide();
    }else{
            $("#service").hide();
            $("#product").show();
        }
    }
    
    function hideValue(val){
//        alert(val)
        if(val == '0'){
            $("#service").hide();
            $("#product").hide();
        }else if (val == '1'){
            $("#service").show();
            $("#product").hide();
        }else{
            $("#service").hide();
            $("#product").show();
        }
    }
    
    function hideValue1(){
        $("#service").hide();
            $("#product").hide();
    }
</script>


<title>GST Category Master</title>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">GST Category Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onLoad="hideValue1();">
                <!--<body onload="initialize(28.3213, 77.5435, 'New Delhi')">-->
                <form name="gstMaster"  method="POST">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                        <input type="hidden" name="gstCategoryId" id="gstCategoryId" value=""  />
                        <tr>
                        <table class="table table-info mb30 table-hover" style="width:100%">
                            <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">GST Category Master</td></tr>
                            <tr>
                                <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>GST Type</td>
                                <td  >
                                    <input type="hidden" id="gstType1" value="" />
                                    <select class="form-control" name="gstType" id="gstType" required style="width:240px;height:40px" onchange="gstName(this.value)">
                                        <option value="0" >--Select--</option>
                                        <option value="1" >Service</option>
                                        <option value="2" >Product</option>
                                    </select>
                                    <!--<textarea name="googleCityName" id="googleCityName" class="textbox" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();"   maxlength="50" placeholder="Enter a location"  style="width:800px"></textarea>-->
                                </td>
                                <td >Status</td>
                        <td >
                            <select  align="center"  name="activeInd" id="activeInd" class="form-control" style="width:240px;height:40px">
                                <option value='Y'>Active</option>
                                <option value='N'>In-Active</option>
                            </select>
                        </td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Category Code</td>
                                <td  >
                                    <input type="text" name="categoryCode" id="categoryCode" class="form-control" style="width:240px;height:40px"    maxlength="50"  style="width:500px;height: 30px;" value="">
                                    <!--<textarea name="googleCityName" id="googleCityName" class="textbox" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();"   maxlength="50" placeholder="Enter a location"  style="width:800px"></textarea>-->
                                </td>
                                <td ><font color="red">*</font>Category Name</td>
                                <td ><input type="text" name="categoryName" id="categoryName" class="form-control" style="width:240px;height:40px" maxlength="50"></td>
                            </tr>
                            <tr id="service">
                                <td ><font color="red">*</font>SAC Code</td>
                                <td ><input type="text" name="sacCode" id="sacCode" class="form-control" style="width:240px;height:40px"  maxlength="50" ></td>
                                <td ><font color="red">*</font>SAC Description</td>
                                <td >
                                    <!--<input type="text" name="sacDescription" id="sacDescription"  class="form-control" style="width:240px;height:40px">-->
                                    <textarea name="sacDescription" id="sacDescription" class="form-control"  style="width:240px;height:40px"></textarea>
                                </td>
                            </tr>
                            <tr id="product">
                                <td ><font color="red">*</font>HSN Code</td>
                                <td ><input type="text" name="hsnCode" id="hsnCode" class="form-control" style="width:240px;height:40px"  maxlength="50" ></td>
                                <td ><font color="red">*</font>HSN Description</td>
                                <td ><textarea name="hsnDescription" id="hsnDescription" class="form-control"  style="width:240px;height:40px"></textarea>
                                </td>
                            </tr>
                        </table>
                        </tr>
                        <tr>
                            <td>
                        <center>
                            <input type="button"  class="btn btn-info" value="Save"  onClick="gstsubmit()">
                        </center>
                        </td>
                        </tr>
                    </table>
                    <h2 align="center">GST Category Master List</h2>


                    <table  id="table" class="table table-info mb30" style="width:100%" >
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th>S.No</th>
                        <th>Category Code</th>
                        <th>Category Name </th>
                        <th>SAC Code </th>
                        <th>SAC Descriptions</th>
                        <th>GST Type</th>
                        <th>HSN Code</th>
                        <th>HSN Descriptions</th>
                        <th>Status</th>
                        <th>Select</th>
                        </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <c:if test = "${GSTCategoryMasterList != null}">
                                <c:forEach items="${GSTCategoryMasterList}" var="cat">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td   align="left"> <%= sno%> </td>
                                        <td   align="left"><c:out value="${cat.categoryCode}"/></td>
                                        <td   align="left"> <c:out value="${cat.categoryName}" /></td>
                                        <td   align="left"> <c:out value="${cat.sacCode}" /></td>
                                        <td   align="left"><c:out value="${cat.sacDescription}"/></td>
                                        <td   align="left">
                                            <c:if test = "${cat.gstType == '1'}" >
                                            Service
                                        </c:if>
                                        <c:if test = "${cat.gstType == '2'}" >
                                           Product
                                           </c:if>
                                            </td>
                                        <td   align="left"><c:out value="${cat.hsnCode}"/></td>
                                        <td   align="left"><c:out value="${cat.hsnDescription}"/></td>
                                        <td   align="left"><c:out value="${cat.activeInd}"/></td>
                                        <td align="center"> <input type="checkbox" id="edit<%=sno%>" 
onclick="setValues(<%= sno%>, '<c:out value="${cat.gstCategoryId}" />', '<c:out value="${cat.categoryCode}" />', '<c:out value="${cat.categoryName}" />', '<c:out value="${cat.sacCode}" />', '<c:out value="${cat.sacDescription}" />','<c:out value="${cat.gstType}"/>','<c:out value="${cat.hsnCode}"/>','<c:out value="${cat.hsnDescription}"/>','<c:out value="${cat.activeInd}"/>');hideValue(<c:out value="${cat.gstType}"/>);" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                    </table>
                            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>