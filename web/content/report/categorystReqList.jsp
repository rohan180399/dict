
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">
       function submitPage(){
           var chek=validation();
           if(chek=='true'){
               
               if(document.salesBillTrend.itemType.value=='1')
                   {
                       
                document.salesBillTrend.action='/throttle/categoryReport.do';
                document.salesBillTrend.submit();
                }
                else
                    {
                        
                        document.salesBillTrend.action='/throttle/categoryNewReport.do';
                document.salesBillTrend.submit();
                        }
           }
       }
       function validation(){
               if(textValidation(document.salesBillTrend.fromDate,'From Date')){
                   document.salesBillTrend.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.salesBillTrend.toDate,'To Date')){
                   document.salesBillTrend.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           var b='<%=request.getAttribute("itemType")%>';
           if(a!='null'){
               
                document.salesBillTrend.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.salesBillTrend.toDate.value='<%=request.getAttribute("toDate")%>';
           }
           if(b!='null'){
               document.salesBillTrend.itemType.value=b;
               }
       }
    </script>
    <body>
        <form  name="salesBillTrend" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
           
          
                
                <table width='50%'  border='0' cellpadding='5' cellspacing='0' class='repcontain' align="center">


                          <tr >
                    <Td colspan="4" class="contenthead">Categorywise Report</Td>
                </tr>

                <c:set var="tot" value="0"></c:set>
            <c:if test = "${categoryRcReport != null}" >
                <tr>
                    <td class="contentsub">S.No</td>
                    <td class="contentsub">Category Name</td>
                    <td class="contentsub">Stock Worth</td>
                </tr>
                <%
					int index = 1 ;
                    %>
                <c:forEach items="${categoryRcReport}" var="pd">
                          <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                    <tr>  
                       <td class="<%=classText %>"><%=index%></td>
                    <td class="<%=classText %>"><c:out value="${pd.categoryName}"/></td>
                    <td class="<%=classText %>"><c:out value="${pd.amount}"/></td>
                    <c:set var="tot" value="${tot+pd.amount}"></c:set>
                    </tr>
                       <%
            index++;
                        %>
                    
                </c:forEach>
                        <tr>
                            <td colspan="2" class="contenthead">Total</td>
                    <td class="contenthead"><fmt:formatNumber value="${tot}" pattern="##.00"/></td>
</tr>
            </c:if>
        </table>
            
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
