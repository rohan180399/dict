<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
        <title>Throttle ::: Accelerate Your Enterprise</title>
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="/throttle/css/login/style.css" />
        <!-- Google Font -->
        <link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core CSS -->
        <!--<link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery Library -->
        <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>-->
        <!-- Bootstrap Core JS -->
        <!--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>
    <!--    <script>
            $(document).ready(function () {
                $("#login").validationEngine();
            });
    
        </script>-->
    <style>
        .errorClass { 
            border:  1px solid red; 
        }
        .noErrorClass { 
            border:  1px solid black; 
        }
    </style>
    <script type="text/javascript">
        var httpRequest;
        function getUser(userName)
        {
            var url = '/throttle/getUserName.do?userName=' + userName;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }

            httpRequest.open("GET", url, true);

            httpRequest.onreadystatechange = function () {
                processRequest();
            };

            httpRequest.send(null);
        }


        function processRequest()
        {
            if (httpRequest.readyState == 4)
            {
                if (httpRequest.status == 200)
                {
                    if (httpRequest.responseText.valueOf() != "") {
                        document.login.password.focus();
                        document.getElementById("userNameStatus").innerHTML = "";
                        document.login.spass.value = httpRequest.responseText.valueOf();
                    } else {
                        document.getElementById("userNameStatus").innerHTML = "Invalid User Name";
                        document.login.userName.focus();
                        document.login.userName.select();
                    }
                }
                else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }




        var whitespace = " \t\n\r";
        function isEmpty(s) {
            var i = 0;
            if ((s == null) || (s.length == 0)) {
                return true;
            }
            for (i = 0; i < s.length; i++) {
                var c = s.charAt(i);
                if (whitespace.indexOf(c) == -1) {
                    return false;
                }
            }
            return true;
        }
        function qty(field, event)
        {

            if (window.event.keyCode == 13 && field.value != "")
            {
                document.login.password.focus();
            }
        }
        function qty1(field, event)
        {

            if (window.event.keyCode == 13 && field.value != "")
            {
                getlogin();
            }
        }
        function userName(uName) {

            document.login.uname.focus();
            if (uName != 'null') {
                document.login.uname.value = uName;
            }
        }
        function focuslogin() {
            document.login.about_us.focus();
        }

    </script>
    <body>
        <!--<form name="login" method="post" id ="login" action ="/throttle/login.do"   >-->
        <section class="container" >
            <section class="login-form">
                <form method="post" action="" role="login" name="login" id ="login">
                    <div>
                        <img src="images/Throttle_Logo.png" alt="Throttle" style="height:30px;width:150px;margin-left:-01px;"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <!--<img src="images/DICT_Logo11.png" alt="DICT" style="height:120px;width:120px;margin-left: 0px;"/>-->
                        <h4>Login</h4>
                    </div>			
                     <span id="userNameStatus" style="text-align: center;color: red"></span>
                    <input type="userName" id="userName" name="userName" placeholder="User Name" class="form-control input-lg" style="text-align: center;color: black"/>
                    <input type="password" name="password" id="password" placeholder="Password"  class="form-control input-lg" style="text-align: center;color: black"/>
                    <select name="branchId" id="branchId" style="display: none" class="form-control input-lg">
                        <option value=''>--Select--</option>
                    </select>
                    <!--required-->
                    <button type="button" name="go" id="submitButton" class="btn btn-lg btn-block btn-info" style="margin-top: 20px;" onclick="userLogin()">Sign in</button>
                    <div>
<!--                        <a  id="forgotLink">reset password</a>-->
                    </div>
                </form>
            </section>
        </section>

        <script>
            (function () {
                var username = document.getElementById('userName');
                username.addEventListener('keypress', function (event) {
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        if (username != '') {
                            onSubmit();
                        } else {
                            document.getElementById('userName').focus();
                        }
                    }
                });
                
            }());
            (function () {
                var password = document.getElementById('password');
                password.addEventListener('keypress', function (event) {
                    if (event.keyCode == 13 || event.keyCode == 9) {
                        event.preventDefault();
                        if (password != '') {
                        onSubmit();
                        }else{
                        document.getElementById('branchId').focus(); 
                    }
                    }
                });
            }());
        </script>
        <script type="text/javascript">

            function onSubmit() {
                var ret  = 0;
                var userName = document.getElementById("userName").value;
                var password = document.getElementById("password").value;
                if (userName == '') {
                    $("#userNameStatus").text("Fill UserName");
                    $("#userName").removeClass("noErrorClass");
                    $("#userName").addClass("errorClass");
                    document.getElementById("userName").focus();
                    return;
                } else if (userName != '' && password == '') {
                    $("#userNameStatus").text("Fill Password");
                    $("#userName").removeClass("errorClass");
                    $("#userName").addClass("noErrorClass");
                    $("#password").removeClass("noErrorClass");
                    $("#password").addClass("errorClass");
                    document.getElementById("password").focus();
                    return;
                } else if (userName != '' && password != '') {
                    $("#password").removeClass("errorClass");
                    $("#password").addClass("noErrorClass");
                    $("#userNameStatus").text("");
                    document.login.action = '/throttle/login.do';
                    document.login.submit();
                }
                return ret;
            }


            function userLogin() {
                var userName = document.getElementById("userName").value;
                var password = document.getElementById("password").value;
                if (userName == '') {
                    $("#userNameStatus").text("Fill Credentials");
                    $("#userName").removeClass("noErrorClass");
                    $("#userName").addClass("errorClass");
                    document.getElementById("userName").focus();
                    return;
                } else if (password == '') {
                    $("#userNameStatus").text("Fill Credentials");
                    $("#password").removeClass("noErrorClass");
                    $("#password").addClass("errorClass");
                    document.getElementById("password").focus();
                    return;
                } else if (userName != '' && password != '') {
                    $("#userName").removeClass("errorClass");
                    $("#password").removeClass("errorClass");
                    $("#userName").addClass("noErrorClass");
                    $("#password").addClass("noErrorClass");
                    $("#userNameStatus").text("");
                    document.login.action = '/throttle/login.do';
                    document.login.submit();
                    document.getElementById("userName").disabled = "disabled";
                    document.getElementById("password").disabled = "disabled";
                    document.getElementById("submitButton").disabled = "disabled";
                    document.getElementById("forgotLink").removeAttribute('href');
                }
            }


            function forgotUserPass() {
                document.login.action = "viewforgotUserPass.do";
                document.login.method = "post";
                document.login.submit();
            }

            document.onClick = function () {
                if (window.event.keyCode == '13') {
                    onSubmit();
                }
            }


            function clearSelectBox() {
                $('#branchId')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="">--Select--</option>')
                        ;

            }
        </script>  
        <!--</form>-->
    </body>
</html>
