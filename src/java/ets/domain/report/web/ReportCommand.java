/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.web;
 /**
 *
 * @author sabreesh
 */
public class ReportCommand {

    //ReportCommand
    // Brattle Foods starts
    //Arul starts
    private String expenseName = "";
    private String expenseValue = "";
    private String sbBillBoeBill = "";
    private String movTypeId = "";
    private String reportTypeId = "";
    private String containerType = "";
    private String billingParty = "";
    private String orderTypeId = "";
    private String linerNameId = "";
    
    private String billedPartyname = "";
    private String frieghtAmount = "";
    private String detentionAmount = "";
    private String weightMent = ""; 
    private String creatorName = "";

    private String containerNo = "";
    private String movementType = "";

    private String[] entityParseValue = null;
    private String[] qBfilterFunction = null;
    private String[] qBfilterFunctionEnd = null;
    private String[] qBOuterSyntax = null;
    private String[] qBAggregateFunction = null;
    private String[] qBClosingSyntax = null;
    private String reportDesc = "";
    private String duration = "";
    private String [] weekdaysValue = null;
    private String messageRefNo = "";
    private String content = "";
    private String referenceNo = "";
    private String date = "";
    private String messageContent = "";
    private String monthId = "";
//    private String status = "";
    private String messageDate = "";
//    private String customerName = "";
    private String supplierName = "";
    private String auctionRefNo = "";
    private String mode = "";
    private String contactInfo = "";
    private String grNo = "";
    private String grId = "";
    private String message = "";
    private String entityId = "";
    private String schedularOption = "";
    private String schedularDateTime = "";
    private String reportName = "";
    private String fileExtn = "";
    private String frequency = "";
    private String emailId = "";
    private String scheduleEmailStart = "";
    private String scheduleEmailEnd = "";
    private String emailAttach = "";
    private String supplierId = "";
    private String queryName = "";
    public String[] assignedfunc = null;
    public String[] condition = null;
    public String[] conditionColumnName = null;
    public String[] operatorValue = null;
    public String[] userValue = null;
    private String selectionList = "";
    private String entityColumnName = "";
    private String entityTableName = "";
    private String entityDataType = "";
    private String entityDisplayName = "";
    private String supplierCode = "";
    private String entityName = "";
    public String[] viewColList = null;
    public String[] displayName = null;
//    public String[] entityName = null;
    public String[] tableName = null;
    private String columnDataTypeName = "";
    private String tableList = "";
    private String queryId = "";
    private String reportMode = "";
    public String[] activeInd ;
    public String[] openSyntax ;
    private String createdBy = "";
    private String createdOn = "";
    private String entityIdFilter = "";
    private String constructedQuery = "";
    private String[] entityDetailsId = null ;
    private String trailerId = "";
    private String trailerNo = "";
    private String not = "";
    private String tripStatusId = "";
    private String vehicleTypeId = "";
    private String fleetCenterId = "";
    private String tripStatusIdTo = "";
    private String startDateFrom = "";
    private String startDateTo = "";
    private String endDateFrom = "";
    private String endDateTo = "";
    private String closedDateFrom = "";
    private String closedDateTo = "";
    private String settledDateFrom = "";
    private String settledDateTo = "";
    private String podStatus = "";
    private String tripSheetId = "";
    private String tripStatus = "";
    private String tripCode = "";
    private String transactionHistoryId = "";
    private String bPCLAccountId = "";
    private String dealerName = "";
    private String dealerCity = "";
    private String transactionDate = "";
    private String accountingDate = "";
    private String transactionType = "";
    private String Currency = "";
    private String transactionAmount = "";
    private String volumeDocumentNo = "";
    private String amountBalance = "";
    private String petromilesEarned = "";
    private String odometerReading = "";
    private double profitValue = 0;
    private double profitPercent = 0;
    private String approvalstatus = "";
    private String consignmentNoteNo = "";
    private String estimatedRevenue = "";
    private String customerId = "";
    private String customerName = "";
    //Arul Ends
    //Arul
    // Brattle Foods  end
    String invoiceType = "";
    String opId = "";
    String spId = "";
    String serviceType = "";
    private String jobCardId = "";
    private String lastProblem = "";
    private String lastTech = "";
    private String lastStatus = "";
    private String lastKm = "";
    private String lastRemarks = "";
    String regNo = "";
    String problem = "";
    String problemStatus = "";
    String createdDate = "";
    String pcd = "";
    String acd = "";
    String type = "";
    String technician = "";
    String remarks = "";
    String sectionId = "";
    String companyId = "0";
    String vendorId = "0";
    String fromDate = "";
    String toDate = "";
    String mfrCode = "";
    String paplCode = "";
    String mfrId = "";
    String modelId = "";
    String itemName = "";
    String poId = "";
    String processId = "";
    String purType = "";
    String woId = "";
    String categoryId = "";
    String inVoiceId = "";
    String fromId = "0";
    String toId = "0";
    String reqId = "";
    String fromServiceName = "";
    String toServiceName = "";
    String gdId = "";
    String usageType = "";
    String status = "";
    String technicianId = "";
    String rcId = "";
    String tyreNo = "";
    String orderType = "";
    String billType = "";
    String purpose = "";
    String custId = "";
    String vat = "";
    //shankar
    String itemCode = "";
    //rajesh
    String vendorName = "";
    String mfr = "";
    String usage = "";
    String age = "";
    String vehicleType = "";
    String itemType = "";
    //Hari
    private String reportType = null;
    private String year = null;
    private String rcWorkorderId = null;
    private String counterId = null;
//  bala
    String customerType = "";
    String reqDate = "";
    String dataType = null;
    private String driName = "";
    private String regno = "";
    private String settlementId = "";
    private String vehicleId = "";
    private String statusDate = "";
    private String statusId = "";
    private String locationId = "";
    private String dueIn = "";
    //CLPL Report
    private String lpsId = "";
    private String city = "";
    private String pandL = "";
    private String ownership = "";
    private String district = "";
    private String consignmentType = "";
    private String consignmentName = "";
    private String tripId = "";
    private String vehicleNo = "";

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
//  bala ends

    public String getCounterId() {
        return counterId;
    }

    public void setCounterId(String counterId) {
        this.counterId = counterId;
    }

    public String getRcWorkorderId() {
        return rcWorkorderId;
    }

    public void setRcWorkorderId(String rcWorkorderId) {
        this.rcWorkorderId = rcWorkorderId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
    //end rajesh

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    // end shankar
    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getRcId() {
        return rcId;
    }

    public void setRcId(String rcId) {
        this.rcId = rcId;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getInVoiceId() {
        return inVoiceId;
    }

    public void setInVoiceId(String inVoiceId) {
        this.inVoiceId = inVoiceId;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getOpId() {
        return opId;
    }

    public void setOpId(String opId) {
        this.opId = opId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getAcd() {
        return acd;
    }

    public void setAcd(String acd) {
        this.acd = acd;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getProblemStatus() {
        return problemStatus;
    }

    public void setProblemStatus(String problemStatus) {
        this.problemStatus = problemStatus;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getPurType() {
        return purType;
    }

    public void setPurType(String purType) {
        this.purType = purType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getFromServiceName() {
        return fromServiceName;
    }

    public void setFromServiceName(String fromServiceName) {
        this.fromServiceName = fromServiceName;
    }

    public String getToServiceName() {
        return toServiceName;
    }

    public void setToServiceName(String toServiceName) {
        this.toServiceName = toServiceName;
    }

    public String getGdId() {
        return gdId;
    }

    public void setGdId(String gdId) {
        this.gdId = gdId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getLastKm() {
        return lastKm;
    }

    public void setLastKm(String lastKm) {
        this.lastKm = lastKm;
    }

    public String getLastProblem() {
        return lastProblem;
    }

    public void setLastProblem(String lastProblem) {
        this.lastProblem = lastProblem;
    }

    public String getLastRemarks() {
        return lastRemarks;
    }

    public void setLastRemarks(String lastRemarks) {
        this.lastRemarks = lastRemarks;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getLastTech() {
        return lastTech;
    }

    public void setLastTech(String lastTech) {
        this.lastTech = lastTech;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDueIn() {
        return dueIn;
    }

    public void setDueIn(String dueIn) {
        this.dueIn = dueIn;
    }

    public String getLpsId() {
        return lpsId;
    }

    public void setLpsId(String lpsId) {
        this.lpsId = lpsId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPandL() {
        return pandL;
    }

    public void setPandL(String pandL) {
        this.pandL = pandL;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getConsignmentType() {
        return consignmentType;
    }

    public void setConsignmentType(String consignmentType) {
        this.consignmentType = consignmentType;
    }

    public String getConsignmentName() {
        return consignmentName;
    }

    public void setConsignmentName(String consignmentName) {
        this.consignmentName = consignmentName;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public double getProfitPercent() {
        return profitPercent;
    }

    public void setProfitPercent(double profitPercent) {
        this.profitPercent = profitPercent;
    }

    public double getProfitValue() {
        return profitValue;
    }

    public void setProfitValue(double profitValue) {
        this.profitValue = profitValue;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(String amountBalance) {
        this.amountBalance = amountBalance;
    }

    public String getbPCLAccountId() {
        return bPCLAccountId;
    }

    public void setbPCLAccountId(String bPCLAccountId) {
        this.bPCLAccountId = bPCLAccountId;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocumentNo() {
        return volumeDocumentNo;
    }

    public void setVolumeDocumentNo(String volumeDocumentNo) {
        this.volumeDocumentNo = volumeDocumentNo;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getClosedDateFrom() {
        return closedDateFrom;
    }

    public void setClosedDateFrom(String closedDateFrom) {
        this.closedDateFrom = closedDateFrom;
    }

    public String getClosedDateTo() {
        return closedDateTo;
    }

    public void setClosedDateTo(String closedDateTo) {
        this.closedDateTo = closedDateTo;
    }

    public String getEndDateFrom() {
        return endDateFrom;
    }

    public void setEndDateFrom(String endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    public String getEndDateTo() {
        return endDateTo;
    }

    public void setEndDateTo(String endDateTo) {
        this.endDateTo = endDateTo;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getSettledDateFrom() {
        return settledDateFrom;
    }

    public void setSettledDateFrom(String settledDateFrom) {
        this.settledDateFrom = settledDateFrom;
    }

    public String getSettledDateTo() {
        return settledDateTo;
    }

    public void setSettledDateTo(String settledDateTo) {
        this.settledDateTo = settledDateTo;
    }

    public String getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(String startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public String getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(String startDateTo) {
        this.startDateTo = startDateTo;
    }

    public String getTripStatusIdTo() {
        return tripStatusIdTo;
    }

    public void setTripStatusIdTo(String tripStatusIdTo) {
        this.tripStatusIdTo = tripStatusIdTo;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getNot() {
        return not;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String[] getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String[] activeInd) {
        this.activeInd = activeInd;
    }

    public String[] getAssignedfunc() {
        return assignedfunc;
    }

    public void setAssignedfunc(String[] assignedfunc) {
        this.assignedfunc = assignedfunc;
    }

    public String getAuctionRefNo() {
        return auctionRefNo;
    }

    public void setAuctionRefNo(String auctionRefNo) {
        this.auctionRefNo = auctionRefNo;
    }

    public String getBilledPartyname() {
        return billedPartyname;
    }

    public void setBilledPartyname(String billedPartyname) {
        this.billedPartyname = billedPartyname;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getColumnDataTypeName() {
        return columnDataTypeName;
    }

    public void setColumnDataTypeName(String columnDataTypeName) {
        this.columnDataTypeName = columnDataTypeName;
    }

    public String[] getCondition() {
        return condition;
    }

    public void setCondition(String[] condition) {
        this.condition = condition;
    }

    public String[] getConditionColumnName() {
        return conditionColumnName;
    }

    public void setConditionColumnName(String[] conditionColumnName) {
        this.conditionColumnName = conditionColumnName;
    }

    public String getConstructedQuery() {
        return constructedQuery;
    }

    public void setConstructedQuery(String constructedQuery) {
        this.constructedQuery = constructedQuery;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDetentionAmount() {
        return detentionAmount;
    }

    public void setDetentionAmount(String detentionAmount) {
        this.detentionAmount = detentionAmount;
    }

    public String[] getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String[] displayName) {
        this.displayName = displayName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEmailAttach() {
        return emailAttach;
    }

    public void setEmailAttach(String emailAttach) {
        this.emailAttach = emailAttach;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEntityColumnName() {
        return entityColumnName;
    }

    public void setEntityColumnName(String entityColumnName) {
        this.entityColumnName = entityColumnName;
    }

    public String getEntityDataType() {
        return entityDataType;
    }

    public void setEntityDataType(String entityDataType) {
        this.entityDataType = entityDataType;
    }

    public String[] getEntityDetailsId() {
        return entityDetailsId;
    }

    public void setEntityDetailsId(String[] entityDetailsId) {
        this.entityDetailsId = entityDetailsId;
    }

    public String getEntityDisplayName() {
        return entityDisplayName;
    }

    public void setEntityDisplayName(String entityDisplayName) {
        this.entityDisplayName = entityDisplayName;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityIdFilter() {
        return entityIdFilter;
    }

    public void setEntityIdFilter(String entityIdFilter) {
        this.entityIdFilter = entityIdFilter;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String[] getEntityParseValue() {
        return entityParseValue;
    }

    public void setEntityParseValue(String[] entityParseValue) {
        this.entityParseValue = entityParseValue;
    }

    public String getEntityTableName() {
        return entityTableName;
    }

    public void setEntityTableName(String entityTableName) {
        this.entityTableName = entityTableName;
    }

    public String getFileExtn() {
        return fileExtn;
    }

    public void setFileExtn(String fileExtn) {
        this.fileExtn = fileExtn;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFrieghtAmount() {
        return frieghtAmount;
    }

    public void setFrieghtAmount(String frieghtAmount) {
        this.frieghtAmount = frieghtAmount;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getLinerNameId() {
        return linerNameId;
    }

    public void setLinerNameId(String linerNameId) {
        this.linerNameId = linerNameId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageRefNo() {
        return messageRefNo;
    }

    public void setMessageRefNo(String messageRefNo) {
        this.messageRefNo = messageRefNo;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMonthId() {
        return monthId;
    }

    public void setMonthId(String monthId) {
        this.monthId = monthId;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String[] getOpenSyntax() {
        return openSyntax;
    }

    public void setOpenSyntax(String[] openSyntax) {
        this.openSyntax = openSyntax;
    }

    public String[] getOperatorValue() {
        return operatorValue;
    }

    public void setOperatorValue(String[] operatorValue) {
        this.operatorValue = operatorValue;
    }

    public String getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(String orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public String[] getqBAggregateFunction() {
        return qBAggregateFunction;
    }

    public void setqBAggregateFunction(String[] qBAggregateFunction) {
        this.qBAggregateFunction = qBAggregateFunction;
    }

    public String[] getqBClosingSyntax() {
        return qBClosingSyntax;
    }

    public void setqBClosingSyntax(String[] qBClosingSyntax) {
        this.qBClosingSyntax = qBClosingSyntax;
    }

    public String[] getqBOuterSyntax() {
        return qBOuterSyntax;
    }

    public void setqBOuterSyntax(String[] qBOuterSyntax) {
        this.qBOuterSyntax = qBOuterSyntax;
    }

    public String[] getqBfilterFunction() {
        return qBfilterFunction;
    }

    public void setqBfilterFunction(String[] qBfilterFunction) {
        this.qBfilterFunction = qBfilterFunction;
    }

    public String[] getqBfilterFunctionEnd() {
        return qBfilterFunctionEnd;
    }

    public void setqBfilterFunctionEnd(String[] qBfilterFunctionEnd) {
        this.qBfilterFunctionEnd = qBfilterFunctionEnd;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public String getReportMode() {
        return reportMode;
    }

    public void setReportMode(String reportMode) {
        this.reportMode = reportMode;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getSchedularDateTime() {
        return schedularDateTime;
    }

    public void setSchedularDateTime(String schedularDateTime) {
        this.schedularDateTime = schedularDateTime;
    }

    public String getSchedularOption() {
        return schedularOption;
    }

    public void setSchedularOption(String schedularOption) {
        this.schedularOption = schedularOption;
    }

    public String getScheduleEmailEnd() {
        return scheduleEmailEnd;
    }

    public void setScheduleEmailEnd(String scheduleEmailEnd) {
        this.scheduleEmailEnd = scheduleEmailEnd;
    }

    public String getScheduleEmailStart() {
        return scheduleEmailStart;
    }

    public void setScheduleEmailStart(String scheduleEmailStart) {
        this.scheduleEmailStart = scheduleEmailStart;
    }

    public String getSelectionList() {
        return selectionList;
    }

    public void setSelectionList(String selectionList) {
        this.selectionList = selectionList;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getTableList() {
        return tableList;
    }

    public void setTableList(String tableList) {
        this.tableList = tableList;
    }

    public String[] getTableName() {
        return tableName;
    }

    public void setTableName(String[] tableName) {
        this.tableName = tableName;
    }

    public String[] getUserValue() {
        return userValue;
    }

    public void setUserValue(String[] userValue) {
        this.userValue = userValue;
    }

    public String[] getViewColList() {
        return viewColList;
    }

    public void setViewColList(String[] viewColList) {
        this.viewColList = viewColList;
    }

    public String[] getWeekdaysValue() {
        return weekdaysValue;
    }

    public void setWeekdaysValue(String[] weekdaysValue) {
        this.weekdaysValue = weekdaysValue;
    }

    public String getWeightMent() {
        return weightMent;
    }

    public void setWeightMent(String weightMent) {
        this.weightMent = weightMent;
    }

    public String getGrId() {
        return grId;
    }

    public void setGrId(String grId) {
        this.grId = grId;
    }

    public String getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(String reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public String getSbBillBoeBill() {
        return sbBillBoeBill;
    }

    public void setSbBillBoeBill(String sbBillBoeBill) {
        this.sbBillBoeBill = sbBillBoeBill;
    }

    public String getMovTypeId() {
        return movTypeId;
    }

    public void setMovTypeId(String movTypeId) {
        this.movTypeId = movTypeId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }
    
    
}
