<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value) {
            if (value == "add") {
                document.manufacturer.action = '/throttle/handleViewSecondaryCustomer.do';
                document.manufacturer.submit();
            } else if (value == 'alter') {
                document.manufacturer.action = '/throttle/handleViewAlter.do';
                document.manufacturer.submit();
            }
        }
    </script>
    <body>
        <form name="manufacturer" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Secondary Trip Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td height="30"><font color="red">*</font>Customer Code</td>
                                        <td><input name="custId" id="custId" type="text" class="textbox"  onclick="ressetDate(this);"></td>
                                        <td height="30"><font color="red">*</font>Customer Name</td>
                                        <td><input name="custId" id="custId" type="text" class="textbox" onClick="ressetDate(this);"></td>

                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button"   value="Search" class="button" name="search" onClick="submitWindow('reqFor')">&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <c:if test = "${secondaryTripCustomerList != null}" >
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>
                            <th><h3>Customer Code</h3></th>
                            <th><h3>Customer Name</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Secondary<br>Schedule</h3></th>
                        </tr>
                    </thead>
                     <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${secondaryTripCustomerList}" var="customer">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno++%></td>
                                <td align="left" class="text2"><c:out value="${customer.customerCode}"/> </td>
                                <td align="left" class="text2"><c:out value="${customer.custName}"/> </td>
                                <td align="left" class="text2">
                                    <c:if test="${(customer.custStatus=='n') || (customer.custStatus=='N')}" >
                                    InActive
                                    </c:if>
                                    <c:if test="${(customer.custStatus=='y') || (customer.custStatus=='Y')}" >
                                    Active
                                    </c:if>
                                </td>
                                <td align="left" class="text2"> &nbsp;
                                        <c:if test="${customer.secondaryTripscheduleSatus =='0'}" >
                                        <a href="/throttle/secondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>">Schedule</a>
                                        </c:if>
                                        <c:if test="${customer.secondaryTripscheduleSatus =='1'}" >
                                        <a href="/throttle/handleViewSecondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>&billingTypeId=<c:out value="${customer.billingTypeId}"/>">View</a>
                                        &nbsp;&nbsp;
                                        <a href="/throttle/editSecondaryTripSchedule.do?customerId=<c:out value="${customer.customerId}"/>&customerName=<c:out value="${customer.custName}"/>&customerCode=<c:out value="${customer.customerCode}"/>">Edit</a>
                                        </c:if>
                                </td>
                            </tr>  
                        </c:forEach>
                     </tbody>      
                </table>
            </c:if>
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


</html>
