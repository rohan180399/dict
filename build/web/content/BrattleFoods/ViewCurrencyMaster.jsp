<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
//    $(document).ready(function() {
//
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
//
//
//
//    });
//
//    $(function() {
//        //	alert("cv");
//        $(".datepicker").datepicker({
//            /*altField: "#alternate",
//             altFormat: "DD, d MM, yy"*/
//            changeMonth: true, changeYear: true
//        });
//
//    });
    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#productCategoryNameStatus").text();
        if(document.getElementById("fromcurrency").value == "0") {
            errStr = "Please Select From Currency.\n";
            alert(errStr);
            document.getElementById("fromcurrency").focus();
        }
         else if(document.getElementById("tocurrency").value == "0")
        {
            errStr ="Please select to currncy Name.\n";
            alert(errStr);
            document.getElementById("tocurrency").focus();
        }
        else if(document.getElementById("fromcurrencyvalue").value == "")
        {
            errStr ="Please enter from currency value\n";
            alert(errStr);
            document.getElementById("fromcurrencyvalue").focus();
        } else if(document.getElementById("tocurrencyvalue").value == "")
        {
            errStr ="Please enter To currency value\n";
            alert(errStr);
            document.getElementById("tocurrencyvalue").focus();
        }
        else if(document.getElementById("fromdate").value == "")
        {
            errStr ="Please select from date\n";
            alert(errStr);
            document.getElementById("fromdate").focus();
        }else if(document.getElementById("todate").value == "")
        {
            errStr ="Please  To date\n";
            alert(errStr);
            document.getElementById("todate").focus();
        }

        if(errStr == "") {
            document.productCategory.action=" /throttle/saveCurrencyMaster.do";
            document.productCategory.method="post";
            document.productCategory.submit();
        }



    }
     function setValues(sno,fromcurrency,fromcurrencyvalue,tocurrency,tocurrencyvalue,fromdate,todate,status,currencyid){
    
     var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("fromcurrency").value = fromcurrency;
        document.getElementById("fromcurrencyvalue").value = fromcurrencyvalue;
        document.getElementById("tocurrency").value = tocurrency;
        document.getElementById("tocurrencyvalue").value = tocurrencyvalue;
        document.getElementById("fromdate").value = fromdate;
        document.getElementById("todate").value = todate;
        document.getElementById("activeInd").value = status;
        document.getElementById("currencyId").value = currencyid;
           }

  function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
{
	var temp = obj.value;

	// avoid changing things if already formatted correctly
	var reg0Str = '[0-9]*';
	if (decimalPlaces > 0) {
		reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
	} else if (decimalPlaces < 0) {
		reg0Str += '\\.?[0-9]*';
	}
	reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
	reg0Str = reg0Str + '$';
	var reg0 = new RegExp(reg0Str);
	if (reg0.test(temp)) return true;

	// first replace all non numbers
	var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
	var reg1 = new RegExp(reg1Str, 'g');
	temp = temp.replace(reg1, '');

	if (allowNegative) {
		// replace extra negative
		var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
		var reg2 = /-/g;
		temp = temp.replace(reg2, '');
		if (hasNegative) temp = '-' + temp;
	}

	if (decimalPlaces != 0) {
		var reg3 = /\./g;
		var reg3Array = reg3.exec(temp);
		if (reg3Array != null) {
			// keep only first occurrence of .
			//  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
			var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
			reg3Right = reg3Right.replace(reg3, '');
			reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
			temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
		}
	}

	obj.value = temp;
}
function blockNonNumbers(obj, e, allowDecimal, allowNegative)
{
	var key;
	var isCtrl = false;
	var keychar;
	var reg;

	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}

	if (isNaN(key)) return true;

	keychar = String.fromCharCode(key);

	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || isCtrl)
	{
		return true;
	}

	reg = /\d/;
	var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

	return isFirstN || isFirstD || reg.test(keychar);
}

 var httpRequest;
    function checkproductCategoryName() {

        var productCategoryName = document.getElementById('productCategoryName').value;

            var url = '/throttle/checkProductCategory.do?productCategoryName=' + productCategoryName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#productCategoryNameStatus").text('Please Check Product Category Name: ' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#productCategoryNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }
</script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                     yearRange: '1900:' + new Date().getFullYear(),
                    changeMonth: true, changeYear: true
                });
            });
            function hideSelect(str){
             var selectobject=document.getElementById("tocurrency");
             for (var i=0; i<selectobject.length; i++){
             var c=selectobject.options[i].value;
             if (selectobject.options[i].value == str ){
             $("select[name='tocurrency'] option[value='"+str+"']").hide();}
            else{
            $("select[name='tocurrency'] option[value='"+c+"']").show();
             }
           }
         }
        </script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Currency Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="document.productCategory.productCategoryName.focus();">


        <form name="productCategory"  method="post" >
           
            <table class="table table-info mb30 table-hover" style="width:100%">
               <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Currency Master</td></tr>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="productCategoryNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>From Currency</td>
                    <td class="text1">
                        <select id="fromcurrency" name="fromcurrency"  class="form-control" style="width:180px;height:40px;" onchange="hideSelect(this.value)">
                            <option value="0" selected>Select..</option>
                                <c:if test = "${countryList != null}">
                                 <c:forEach items="${countryList}" var="cc">
                                     <option value="<c:out value="${cc.countryId}" />"><c:out value="${cc.currencySymbol}" /></option>
                                 </c:forEach>
                             </c:if>
                        </select>

                    </td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>From Currency Value</td>
                   
                   <td class="text1"><input type="text" name="fromcurrencyvalue" id="fromcurrencyvalue" class="form-control" style="width:180px;height:40px;" autocomplete="off"/></td>

                </tr>

                <tr>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Currency &nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                     <select id="tocurrency" name="tocurrency" class="form-control" style="width:180px;height:40px;">
                            <option value="0" selected>Select..</option>
                                <c:if test = "${countryList != null}">
                                 <c:forEach items="${countryList}" var="cc">
                                     <option id="forHide" value="<c:out value="${cc.countryId}" />"><c:out value="${cc.currencySymbol}" /></option>
                                 </c:forEach>
                             </c:if>
                     </select>
                    </td>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Currency &nbsp;&nbsp;&nbsp;&nbsp;Value</td>
                    <td class="text2"><input type="text" name="tocurrencyvalue" id="tocurrencyvalue" class="form-control" style="width:180px;height:40px;" autocomplete="off" /></td>

                </tr>
                  <tr>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>From Date&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="text2"><input type="text" name="fromdate" id="fromdate" class="datepicker"  style="width:180px;height:40px;"/></td>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>To Date &nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="text2"><input type="text" name="todate" id="todate" class="datepicker"  style="width:180px;height:40px;"/></td>

                </tr>
                <tr>
                     <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:180px;height:40px;" name="activeInd" id="activeInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>


            <h2 align="center">List Of The Currency</h2>


            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">
                        <th><h3>S.No</h3></th>
                        <th width="120"><h3>From Currency</h3></th>
                        <th width="150"><h3>From Currency Value</h3></th>
                        <th><h3>To Currency </h3></th>
                        <th width="150"><h3>To Currency Value</h3></th>
                        <th><h3>From Date</h3></th>
                        <th><h3>To Date</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${currencyList != null}">
                        <c:forEach items="${currencyList}" var="pc">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.fromcurrency}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.fromcurrencyvalue}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.tocurrency}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.tocurrencyvalue}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.fromdate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.todate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.status}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${pc.fromcurrencyId}" />','<c:out value="${pc.fromcurrencyvalue}" />','<c:out value="${pc.tocurrencyId}" />','<c:out value="${pc.tocurrencyvalue}" />','<c:out value="${pc.fromdate}" />','<c:out value="${pc.todate}" />','<c:out value="${pc.status}" />','<c:out value="${pc.currencyid}" />');" /></td>
                            </tr>

                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
                    <input type="hidden" name="currencyId" id="currencyId" value=""/>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>