<%--
    Document   : pendingGrInvoice
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>


        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    
    <body>
        <form name="pendingGrInvoice" method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "pendingGrInvoice-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
           %>
           
            <br>
            <c:if test="${invoiceReport != null}">
                <table class="table table-info mb30 table-hover"  id="table" style="width:100%">
                <!--<table align="center" border="0" id="table" class="sortable" width="100%" >-->

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                             <th align="center"><h3>Bill of Entry</h3></th>
                                    <th align="center"><h3>Shipping Bill No</h3></th>
                            <th align="center"><h3>GR No</h3></th>
                            <th align="center"><h3>GR Date</h3></th>
                            <th align="center"><h3>Movement Type</h3></th>
                            <th align="center"><h3>Invoice No</h3></th>
                            <th align="center"><h3>Invoice Date</h3></th>
                            <th align="center"><h3>Billed Party Name</h3></th>
				 <th align="center"><h3>Route </h3></th>
                            <th align="center"><h3>Container No</h3></th>
                            <th align="center"><h3>Container Size</h3></th>
                            <th align="center"><h3>Freight Amount</h3></th>
                            <th align="center"><h3>Detention Amount</h3></th>
                            <th align="center"><h3>Toll Tax</h3></th>
                            <th align="center"><h3>Weightment</h3></th>
                            <th align="center"><h3>other Expense</h3></th>
                            <th align="center"><h3>Total Amount</h3></th>
                            <th align="center"><h3>Creation Date</h3></th>
                            <th align="center"><h3>Creator Name</h3></th>
                            <th align="center"><h3>Commodity</h3></th>
                            <th align="center"><h3>GST Applicable</h3></th>
                             <th align="center"><h3>Pending GR Remarks</h3></th>
                                                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${invoiceReport}" var="Invoice">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                             <tr height="40" >
                                <td class="<%=classText%>"><%=index%></td>
                                  <td  width="30" class="<%=classText%>"><c:out value="${Invoice.billOfEntryNo}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.shipingBillNo}"/></td>
                                <td  class="<%=classText%>"><c:out value="${Invoice.grNo}"/></td>
                                <td   class="<%=classText%>"><c:out value="${Invoice.grDate}"/></td>
                                <td   class="<%=classText%>"><c:out value="${Invoice.movementType}"/></td>
                              <td  class="<%=classText%>"><c:out value="${Invoice.invoiceCode}"/>
                                <td  class="<%=classText%>"><c:out value="${Invoice.invoiceDate}"/></td>
                                <td  class="<%=classText%>" ><c:out value="${Invoice.billedPartyname}"/></td>
				<td  width="30" class="<%=classText%>"><c:out value="${Invoice.routeInfo}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.containerNo}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.containerSize}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.frieghtAmount}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${Invoice.detentionAmount}"/>
                                  </td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.tollAmount}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${Invoice.weightMent}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${Invoice.otherExpense}"/></td>
                                
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.otherExpense + Invoice.tollAmount + Invoice.detentionAmount + Invoice.frieghtAmount}"/></td>
                                <td class="<%=classText%>"  ><c:out value="${Invoice.createdDate}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.creatorName}"/></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.commodityName}" /></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.gstType}" /></td>
                                <td  class="<%=classText%>"  ><c:out value="${Invoice.remarks}"/></td>
                                </tr>
                                <%index++;%>                                    
                        </c:forEach>
                    </tbody>
                </table>

               
            </c:if>
            <br>
            <br>
    </form>
    </body>
</html>

