<%-- 
    Document   : paymentEntry
    Created on : Mar 21, 2013, 11:58:48 AM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
   <script type="text/javascript">
        function submitPage(){
               document.manufacturer.action = 'content/Finance/paymentReport.jsp';
                document.manufacturer.submit();
        }
       
       
    </script>
    <body>

        <br>
        finance > purchase > purchase payments
        <form name="manufacturer" method="post" >
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                       &nbsp;
                    </td></tr></table>
            <br/>
          
            <br/>
           
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bg" class="border">
                <tr height="30">
                    <td colspan="9" class="contenthead" height="30">
                        <div class="contenthead" align="center">Purchase Payment Details</div></td>
                </tr>
                <tr >
                     <tr height = "30" class="text2">
                                <td align="center" colspan ="4" >Customer Name</td>
                                <td colspan ="5"> B S TRADERS</td>
                        </tr>
                        <tr height = "30"class="text1">
                              <td >&nbsp;</td>
                         <td ><font color="red">*</font>Voucher No</td>
                            <td ><input type="text" name="voucherNo" id="voucherNo" class="form-control" value="CAS/13-14/12514" >
                         <td  >&nbsp;</td>
                         <td ><font color="red">*</font>Voucher Date</td>
                           <td >&nbsp;</td>
                            <td ><input type="text" name="voucherDate" id="voucherDate" class="datepicker" value="03-03-2014" >
                            </td>
                              <td colspan="2">&nbsp;</td></tr>
                        </tr>
                        <tr height = "30" class="text2">
                              <td >&nbsp;</td>
                         <td ><font color="red">*</font>Amount</td>
                         <td ><input type="text" name="amount" id="amount" class="form-control" value="0" ></td>
                            <td >&nbsp;</td>
                         <td width="80" >Amount Type </td>
                         <td >&nbsp;</td>
                           <td width="90" > <select name="amountType" id="amountType" class="form-control" style="height:20px; width:122px;" onchange="getLocation();" >
                                                    <option value="1">Cash</option>
                                                    <option value="1">Bank</option>
                                            </select></td>
                               <td colspan="2" align="left"><b>Cr.</b></td></tr>
                        
                        <tr height = "30" class="text1">
                              <td >&nbsp;</td>
                         <td ><font color="red">*</font>Bank Name</td>
                            <td ><input type="text" name="bankName" id="bankName" class="form-control" value="" >
                              <td >&nbsp;</td>
                            <td ><font color="red">*</font>Cheque Date</td>
                            <td >&nbsp;</td>
                            <td ><input type="text" name="chequeDate" id="chequeDate" class="datepicker" value="" >
                            </td>
                          <td colspan="2">&nbsp;</td></tr>
                        <tr>
                        <tr height = "30" class="text2">
                            <td >&nbsp;</td>
                         <td ><font color="red">*</font>Bank Charges</td>
                            <td ><input type="text" name="bankCharge" id="bankCharge" class="form-control" value="0" >&nbsp;<b>Dr.</b>
                            </td>
                            <td  colspan="6">&nbsp;</td>
                        </tr>
<!--                        <tr height = "30" class="text1">
                         <td width="120">Total advance</td>
                            <td ><input type="text" name="totalAdvance" id="totalAdvance" class="form-control" value="" > </td>
                         <td >Ready To Pay</td>
                            <td ><input type="text" name="pay" id="pay" class="form-control" value="" > </td>
                         <td >Advance</td>
                            <td ><input type="text" name="advance" id="advance" class="form-control" value="" > </td>
                         <td >Balance</td>
                            <td ><input type="text" name="balance" id="balance" class="form-control" value="" > </td>
                        </tr>-->
                        <tr height = "30" class="text2">
                         <td width="120">Purchase No</td>
                            <td ><input type="text" name="purchaseNo" id="purchaseNo" class="form-control" value="" ></td>
                         <td >Purchase Amount</td>
                            <td ><input type="text" name="purchaseAmount" id="purchaseAmount" class="form-control" value="" ></td>
                         <td >Balance</td>
                            <td ><input type="text" name="balanceAmount" id="balanceAmount" class="form-control" value="" > </td>
                         <td >Ready To Pay</td>
                         <td ><input type="text" name="readyToPay" id="readyToPay" class="form-control" value="" ></td><td><input type="Button" name="Click" value =" Add "   class="button" /></td>
                        </tr>
            </table>
            <br>
            <table width="400">
                    <tr height="30"> 
                        <td align="left" class="contenthead">Purchase No</td>
                        <td align="left"  class="contenthead">Amount</td>
                        <td align="left"  class="contenthead">Delete</td>
            </tr>
            </table>
            <br/>
            <br/>
            <center>
                        <td align="center" colspan="9"><input type="Button" name="Click" value =" Save "   class="button" onClick="submitPage();"/>
            </center>   
    </body>
</html>