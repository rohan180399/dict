/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
package ets.domain.recondition.business;

import ets.domain.recondition.data.ReconditionDAO;
import ets.domain.recondition.business.ReconditionTO;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.purchase.business.PurchaseTO;
import ets.domain.purchase.data.PurchaseDAO;
import java.util.*;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.vehicle.data.VehicleDAO;
import java.util.ArrayList;
import java.util.Iterator;

public class ReconditionBP {

    private ReconditionDAO reconditionDAO;
    private PurchaseDAO purchaseDAO;
    private VehicleDAO vehicleDAO;

    public ReconditionDAO getReconditionDAO() {
        return reconditionDAO;
    }

    public void setReconditionDAO(ReconditionDAO reconditionDAO) {
        this.reconditionDAO = reconditionDAO;
    }

    public PurchaseDAO getPurchaseDAO() {
        return purchaseDAO;
    }

    public void setPurchaseDAO(PurchaseDAO purchaseDAO) {
        this.purchaseDAO = purchaseDAO;
    }

    public VehicleDAO getVehicleDAO() {
        return vehicleDAO;
    }

    public void setVehicleDAO(VehicleDAO vehicleDAO) {
        this.vehicleDAO = vehicleDAO;
    }

    public ArrayList processRcQueue(ReconditionTO reconditionTO) throws FPBusinessException, FPRuntimeException {
        ArrayList rcQueue = new ArrayList();
        ArrayList tyreNos = new ArrayList();
        Date dat = new Date();
        //////System.out.println("process rc queue");
        //////System.out.println("start=" + dat);
        rcQueue = reconditionDAO.getRcQueue(reconditionTO);
        ReconditionTO rcTO = new ReconditionTO();
        Iterator itr=rcQueue.iterator();
        while(itr.hasNext()){
            rcTO=new ReconditionTO();
            rcTO = (ReconditionTO) itr.next();
            //if the rcqueue item is a tyre and the rc queue entry came via mrs issue
            // then fetch all vehicle tyrenos as option to choose rc id
            //////System.out.println("am here 1");
            if("1011".equals(rcTO.getCategoryId()) && rcTO.getMrsId() != 0){
                tyreNos = reconditionDAO.getRcQueueTyreNos(rcTO.getRcQueueId());
                rcTO.setTyreNos(tyreNos);
                //////System.out.println("am here 2");
            }
            //////System.out.println("am here 3");
        }
        //////System.out.println("am here 4");
        dat = new Date();
        //////System.out.println("end=" + dat);
        if (rcQueue.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcQueue;
    }

    public int processScrapRc(ArrayList scrapItems, int companyId, int userId) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = reconditionDAO.doInsertScrapRc(scrapItems, companyId, userId);
        if (status == 0) {
            // throw new FPBusinessException("EM-MRS1-04");
        }
        return status;
    }

    public void processGenerateRcWo(ArrayList rcItems,String jobCardId,String customerType,String customerName,String customerAddress,String mobileNo, int companyId, int userId, ReconditionTO reconTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        //////System.out.println("Selected Rc items  size is=" + rcItems.size());
        status = reconditionDAO.doGenerateWo(rcItems,jobCardId,customerType,customerName,customerAddress,mobileNo, companyId, userId, reconTO);
        //////System.out.println("generated WO is=" + status);
    }

// Vinoth
    public String processWoList(ReconditionTO stocktransferTO) throws FPBusinessException, FPRuntimeException {
        ArrayList workOrderList = new ArrayList();
        String woList = "";
        int counter = 0;
        Iterator itr;

        workOrderList = reconditionDAO.getWOList(stocktransferTO);

        if (workOrderList.size() == 0) {
            woList = "NotFound";
        } else {
            itr = workOrderList.iterator();
            while (itr.hasNext()) {
                stocktransferTO = new ReconditionTO();
                stocktransferTO = (ReconditionTO) itr.next();
                if (counter == 0) {
                    //////System.out.println(" purchaseTO.getWoId()" + stocktransferTO.getWoId());
                    woList = (stocktransferTO.getWoId());
                    counter++;
                } else {
                    woList = woList + "~" + (stocktransferTO.getWoId());
                }
            }
        }
        return woList;
    }

    public ArrayList processItemList(ReconditionTO stocktransferTO) throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();

        itemList = reconditionDAO.getRcItemList(stocktransferTO);

        //if(stocktransferTO.getVendorId().equalsIgnoreCase("1232"))
        {
        ReconditionTO rcTO=null;
        Iterator itr=itemList.iterator();
        while(itr.hasNext()){
            rcTO=new ReconditionTO();
            rcTO=(ReconditionTO)itr.next();
            System.out.println(stocktransferTO.getItemId()+"vijay"+rcTO.getRcItemId());

            rcTO.setSparesAmount(reconditionDAO.getRCSpareAmount(stocktransferTO.getWoId(),rcTO.getItemId()));
            //////System.out.println("rcTO.getSparesAmount() Hari"+rcTO.getSparesAmount());
        }
        }


        if (itemList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public int processInsertItem(ArrayList List, int userId, ReconditionTO stocktransferTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        //////System.out.println("list size = " + List.size());
        status = reconditionDAO.insertRcItem(List, userId, stocktransferTO);
        if (status == 0) {
            throw new FPBusinessException("EM-MPR-02");
        }
        return status;
    }

    public ArrayList processAvailableRcItems(int companyId) throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = reconditionDAO.getAvailableRcItems(companyId);
        return itemList;
    }

    public ArrayList getWoList(ReconditionTO reconditionTO) throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = reconditionDAO.getWOList(reconditionTO);
        return itemList;
    }

    public void processAvailableRcItems(int rcItemId, int queueId) throws FPBusinessException, FPRuntimeException {
        int stat = 0;
        stat = reconditionDAO.getAvailableRcItems(rcItemId, queueId);
    }

    public int processGenerateRcItemId(ReconditionTO rec, int userId) throws FPBusinessException, FPRuntimeException {
        int rcId = 0;
        rcId = reconditionDAO.doGenerateRcItemId(rec, userId);
        if (rcId == 0) {
            //throw new FPBusinessException("EM-MPR-02");
        }
        return rcId;
    }

    public ArrayList processVehicleRcRtItems(int companyId) throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        ArrayList newList = new ArrayList();
        ArrayList rcDurat = new ArrayList();
        ArrayList finalList = new ArrayList();
        ArrayList woRaisedList = new ArrayList();
        int condit = 0;
        //////System.out.println("in processVehicleRcRtItems");
        Date dat = new Date();
        //////System.out.println("start=" + dat);

        ReconditionTO rec, durTO, woRaisedTO;
        Iterator itr;
        Iterator durItr;
        Iterator durWoRaisedItr;
        itemList = reconditionDAO.getVehicleRcRtItems(companyId);
        woRaisedList = reconditionDAO.getWoRaisedItems();
        itr = itemList.iterator();
        /*
        while(itr.hasNext()){
        rec = new ReconditionTO();
        rec = (ReconditionTO) itr.next();
        ///////////////////////////////////
        rcDurat = reconditionDAO.getRcDuration(rec.getItemId(),Integer.parseInt(rec.getRcItemId()) );
        durItr = rcDurat.iterator();
        if(durItr.hasNext()){
        durTO = new  ReconditionTO();
        durTO = (ReconditionTO) durItr.next();
        rec.setRcDuration(durTO.getRcDuration());
        }
        ////////////////////////////////////
        newList.add(rec);
        }
         */

// Remove Items Which are already raised for rc workorder        

        itr = itemList.iterator();
        while (itr.hasNext()) {
            rec = new ReconditionTO();
            rec = (ReconditionTO) itr.next();
            condit = 0;
            durWoRaisedItr = woRaisedList.iterator();
            while (durWoRaisedItr.hasNext()) {
                woRaisedTO = new ReconditionTO();
                woRaisedTO = (ReconditionTO) durWoRaisedItr.next();
                if (woRaisedTO.getItemId() == rec.getItemId() && woRaisedTO.getRcItemId().equalsIgnoreCase(rec.getRcItemId())) {
                    condit++;
                    break;
                }
            }
            if (condit == 0) {
                finalList.add(rec);
            }
        }

        dat = new Date();
        //////System.out.println("end=" + dat);

        return finalList;
    }

    public int processVehicleRcRtItems(ReconditionTO rec) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = reconditionDAO.doUpdateIsRtTyre(rec);
        return status;
    }

    public String processTyreNo(String tyreId, int companyId) throws FPBusinessException, FPRuntimeException {
        String tyreNo = "";
        tyreNo = reconditionDAO.getTyreNo(tyreId, companyId);
        if (tyreNo == null) {
            tyreNo = "";
        }
        return tyreNo;
    }

    public int processUpdateRcPrice(ArrayList List) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        //////System.out.println(" vijay list size = " + List.size());
        status = reconditionDAO.doUpdateRcPrice(List);
        if (status == 0) {
            //throw new FPBusinessException("EM-MPR-02");
        }
        return status;
    }

    public int processRcTyreUpdate(ReconditionTO tyreTO, int UserId, int companyId) {
        int status = 0;
        status = reconditionDAO.doInsertVehicleTyre(tyreTO, UserId, companyId);
        if (status == 0) {
            // throw new FPBusinessException("EM-MRS1-04");
        }
        return status;
    }

    public ArrayList processRcGoodsTransfer(int companyId) throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = reconditionDAO.getRcGoodsTransfer(companyId);
        return itemList;
    }

    public int processSenderStatus(int rcQueueId, String sentStatus) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = reconditionDAO.doSenderStatus(rcQueueId, sentStatus);
        return status;
    }

    public int processFaultItems(int itemId, int qty, int woId, int companyId, int userId) {
        int status = 0;

        //insert
        status = reconditionDAO.doInsertFaultItems(itemId, qty, woId, companyId, userId);
        return status;
    }

    public int processRcWoNo(int companyId, int userId, String vendorId,String jobCardId,String customerType,String customerName,String customerAddress,String mobileNo,String remarks) {
        int status = 0;
        status = reconditionDAO.doGenerateRcWO(companyId, userId, vendorId,jobCardId,customerType,customerName,customerAddress,mobileNo, remarks);
        return status;
    }

    public String getItemSuggestions(String mfrCode, String itemCode, String itemName, int companyId) {
        String values = "";
        String[] temp = null;
        String Quandity = "";
        int itemId = 0;
        String rcCount = "";
        String localCount = "";
        float totlQty = 0;
        if (mfrCode != "" && mfrCode != null) {
            values = reconditionDAO.getMfrItemId(mfrCode);
        } else if (itemCode != "" && itemCode != null) {
            values = reconditionDAO.getCodeItemId(itemCode);
        } else if (itemName != "" && itemName != null) {
            values = reconditionDAO.getNameItemId(itemName);
        }
        if (values != null) {

            temp = values.split("~");
            itemId = Integer.parseInt(temp[0]);
            Quandity = temp[0] + "~" + String.valueOf(totlQty) + "~" + temp[1] + "~" + temp[2] + "~" + temp[3] + "~" + temp[4] + "~" + temp[5] + "~" + temp[6];
            //////System.out.println("values" + Quandity);
        }

        return Quandity;
    }

    public String getItemSuggestionsToScrap(String itemCode, String itemName) {
        //////System.out.println("In reconditionBP");
        String values = "";
        String[] temp = null;
        String Quandity = "";
        int itemId = 0;
        String rcCount = "";
        String localCount = "";
        float totlQty = 0;
        if (itemCode != "" && itemCode != null) {
            values = reconditionDAO.getNameItemIdToScrap("", itemCode);
        } else if (itemName != "" && itemName != null) {
            values = reconditionDAO.getNameItemIdToScrap(itemName, "");
        }
        //////System.out.println("In reconditionBP 1");
        if (values != null) {
            //////System.out.println("In reconditionBP 2");
            temp = values.split("~");
            itemId = Integer.parseInt(temp[0]);
            Quandity = temp[0] + "~" + temp[1] + "~" + temp[2] + "~" + temp[3] + "~" + temp[4] + "~" + temp[5] + "~" + temp[6];
            //////System.out.println("values" + Quandity);
        }

        return Quandity;
    }

    public int addScrapItems(int itemId, int qty, int priceId, int userId) {
        int status = 0;
        //insert
        status = reconditionDAO.addScrapItems(itemId, qty, priceId, userId);
        return status;
    }

    public ArrayList processRCWOItems(ReconditionTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reconditionDAO.getRCWOItems(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public int updateRcWo(ReconditionTO repTO) throws FPRuntimeException, FPBusinessException {
        int stat = 0;
        stat = reconditionDAO.updateRcWo(repTO);
        return stat;
    }

    public ArrayList doFalutTyreItems(String[] regNos, String[] tyreIds, String[] positionIds, String[] itemIds, int companyId, int userId) {
        int insertStatus = 0;
        ArrayList TyreList = new ArrayList();
        VehicleTO vehicleTO = new VehicleTO();
        vehicleTO.setItemIds(itemIds);
        vehicleTO.setPositionId(positionIds);
        vehicleTO.setTyreIds(tyreIds);
        vehicleTO.setRegNos(regNos);
        vehicleTO.setVehicleId("0");

        if (itemIds != null) {
            if (!itemIds[0].equalsIgnoreCase("0")) {
                TyreList = vehicleDAO.doInsertVehicleTyre1(vehicleTO, userId, companyId);
            }
        }
        return TyreList;
    }
//Hari



    public ArrayList getRcWorkOrderList(int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList rcWorkOrder = new ArrayList();
        rcWorkOrder = reconditionDAO.getRcWorkOrderList(companyId);
        if (rcWorkOrder.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcWorkOrder;
    }
    public ArrayList getRcWorkOrderDetails(int rcWorkId,int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList rcWorkOrderDetails = new ArrayList();
        rcWorkOrderDetails = reconditionDAO.getRcWorkOrderDetails(rcWorkId,companyId);
        if (rcWorkOrderDetails.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcWorkOrderDetails;
    }
    public ArrayList getRcMrsWorkOrderDetails(int rcWorkId,int companyId,int mrsId) throws FPRuntimeException, FPBusinessException {

        ArrayList rcMrsWorkOrderDetails = new ArrayList();
        rcMrsWorkOrderDetails = reconditionDAO.getRcMrsWorkOrderDetails(rcWorkId,companyId,mrsId);
        if (rcMrsWorkOrderDetails.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcMrsWorkOrderDetails;
    }


    public ArrayList getCompanyEmployee(int companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList compTechnician = new ArrayList();
        compTechnician = reconditionDAO.getCompanyEmployee(companyId);
        if (compTechnician.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return compTechnician;
    }

public ArrayList getRcMrsList(int jobcardId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList rcMrsList = new ArrayList();
        rcMrsList = reconditionDAO.getRcMrsList(jobcardId);
        if (rcMrsList.size() != 0);
        return rcMrsList;
    }

public ArrayList getRcWOItems(int jobcardId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList rcWOItems = new ArrayList();
        rcWOItems = reconditionDAO.getRcWOItems(jobcardId);
        if (rcWOItems.size() != 0);
        return rcWOItems;
    }

public ArrayList getWorkorderItems(int poId)
        throws FPRuntimeException, FPBusinessException
    {
        ArrayList poItems = new ArrayList();
        poItems = reconditionDAO.getWorkorderItems(poId);
        return poItems;
    }


public int modifyWOHeader(ReconditionTO recondition)
        throws FPRuntimeException, FPBusinessException
    {
        int status = reconditionDAO.modifyWOHeader(recondition);
        return status;
    }


  
public int insertRCNewItem(ReconditionTO recondition)
        throws FPRuntimeException, FPBusinessException
    {
        int status = reconditionDAO.insertRCNewItem(recondition);
        return status;
    }

public int deleteWoItems(ReconditionTO recondition)
        throws FPRuntimeException, FPBusinessException
    {
    int status=0;
    status=reconditionDAO.deleteWoItems(recondition);
    return status;
    }
//Rc Zone
public ArrayList getJobCardList() throws FPRuntimeException, FPBusinessException {

        ArrayList jobCardList = new ArrayList();
        jobCardList = reconditionDAO.getJobCardList();
        if (jobCardList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return jobCardList;
    }
public ArrayList getCustomerTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList customerTypeList = new ArrayList();
        customerTypeList = reconditionDAO.getCustomerTypeList();
        if (customerTypeList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return customerTypeList;
    }
}  

