<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script>
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";



    //savefunction
    function submitPage(value) {
        var count1 = 0;
        var count2 = 0;
        if (document.getElementById('extraKmCalculation').value == '0') {
            alert("please select extra km calculation");
            document.getElementById('extraKmCalculation').focus();
        } else if (document.getElementById('routeValidFrom').value == '') {
            alert("please select the route valid from date");
            document.getElementById('routeValidFrom').focus();
        } else if (document.getElementById('routeValidTo').value == '') {
            alert("please select the route valid to date");
            document.getElementById('routeValidTo').focus();
        } else if (document.getElementById('contractCngCost').value == '') {
            alert("please fill cost of cng");
            document.getElementById('contractCngCost').focus();
        } else if (document.getElementById('contractDieselCost').value == '') {
            alert("please fill cost of diesel");
            document.getElementById('contractDieselCost').focus();
        } else if (document.getElementById('rateChangeOfCng').value == '') {
            alert("please fill rate change of cng");
            document.getElementById('rateChangeOfCng').focus();
        } else if (document.getElementById('rateChangeOfDiesel').value == '') {
            alert("please fill rate change of diesel");
            document.getElementById('rateChangeOfDiesel').focus();
        } else {
            document.editSecondaryRoute.action = '/throttle/saveEditSecondaryContract.do';
            document.editSecondaryRoute.submit();
        }

    }






    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getSecondaryCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                $("#customerId").val(ui.item.Id);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                $('#fixedKmPerMonth').focus();
                return false;

            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };


    });

</script>

<body>
    <form name="editSecondaryRoute"  method="post">
        <br>
        <br>
        <br>
        <c:set var="billType" value="${billType}" />
        <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr class="contenthead" align="left"><td colspan="4">Secondary Contract Details</td></tr>
          
                <tr class="text2">
                    <td><font color='red'>*</font>Vendor Name</td>
                    <td><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>"/><input type="text" name="VendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="form-control" readonly /></td>
                    <td><font color='red'>*</font>Extra Km Calculation</td>
                    <td><select name="extraKmCalculation" id="extraKmCalculation" class="form-control">
                            <option value="0" selected>--Select--</option>
                            <option value="1">Consolidated Run Km</option>
                            <option value="2">Vehicle Wise Run Km</option>
                        </select>
                        <script>
                           //document.getElementById("extraKmCalculation").value = '';-->
                        </script>
                    </td>
                </tr>

                <input type="hidden" name="contractId" id="contractId" value="<c:out value="${scl.contractId}"/>" readonly/>
                <tr class="text1">
                    <td><font color='red'>*</font>Valid From</td>
                    <td><input type="text" name="routeValidFrom" id="routeValidFrom" value="<c:out value="${scl.fromDate}"/>" class="datepicker"/></td>
                    <td><font color='red'>*</font>Valid To</td>
                    <td><input type="text" name="routeValidTo" id="routeValidTo" value="<c:out value="${scl.toDate}"/>" class="datepicker"/></td>
                </tr>
          </table>
        <br>
        <br><br>
        <br>
        <br>
        <br>
        <% int count = 0;%>
        <c:if test="${SecondaryCustomerVehicleList != null}">
            <table width="100%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="45">
                        <th><h3>S.No</h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>NoOfVehiclesContracted </h3></th>
                        <c:if test="${billType == 1}">
                        <th><h3>Monthly Fixed KM / Vehicle</h3></th>
                        <th><h3>Monthly Fixed Cost / Vehicle</h3></th>
                        <th><h3>Extra Km Charge / Km</h3></th>
                        </c:if>
                        <c:if test="${billType == 2}">
                            <th><h3>Rate/ Km</h3></th>
                        </c:if>
                        <c:if test="${billType == 3}">
                            <th><h3>Rate/ Drop</h3></th>
                        </c:if>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${SecondaryCustomerVehicleList}" var="mcl">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>

                        <tr height="30">
                            <td align="left" class="<%=classText%>"><%=sno%></td>
                            <td align="left" class="<%=classText%>" style="width: 200px">
                                <input type="hidden" name="id" id="id" value="<c:out value="${mcl.id}"/>" readonly/>
                                <input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${mcl.vehicleTypeId}"/>" readonly/><c:out value="${mcl.vehicleTypeName}"/>
                            </td>
                            <td align="left" class="<%=classText%>"><input type="text" name="noOfVehicle" id="noOfVehicle<%=index%>" value="<c:out value="${mcl.noOfVehicles}"/>"  style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <c:if test="${billType == 1}">
                            <td align="left" class="<%=classText%>"><input type="text" name="fixedKm" id="fixedKm<%=index%>"  value="<c:out value="${mcl.fixedKm}"/>" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            <td align="left" class="<%=classText%>"><input type="text" name="fixedKmCharge" id="fixedKmCharge<%=index%>"  value="<c:out value="${mcl.fixedKmCharge}"/>" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                            </c:if>
                            <c:if test="${billType == 2 || billType == 3  }">
                                <input type="hidden" name="fixedKm" id="fixedKm<%=index%>"  value="0" />
                                <input type="hidden" name="fixedKmCharge" id="fixedKmCharge<%=index%>"  value="0" />
                            </c:if>
                            <td align="left" class="<%=classText%>"><input type="text" name="extraKmCharge" id="extraKmCharge<%=index%>"   value="<c:out value="${mcl.extraKmCharge}"/>" style="width: 90px" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                        </tr>
                        <%sno++;%>
                        <%index++;%>
                    </c:forEach>

                </tbody>
            </table>
            <br/>
            <br/>
            <br/>
            <table width="100%" align="center" cellpadding="0" cellspacing="0" id="addHumanResource" class="table2">
                <thead>
                <td class="contenthead" height="30" style="width: 10px;">S No</td>
                <td class="contenthead" height="30" style="width: 10px;">Type</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>No of Unit</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>UnitCost/Month</td>
                </thead>
                <tbody>
                    <%
                                int sno1 = 1;
                    %>

                    <c:if test="${humanResourceDetails != null}">
                        <c:forEach items="${humanResourceDetails}" var="human">
                            <tr>
                                <td><%=sno1%><input type="hidden" id="updateValue" name="updateValue" value="0" >
                                    <input type="hidden" name="pointId" id="pointId"  value="<c:out value="${human.humanId}"/>" />
                                    <input type="hidden" name="humanResourceDetailsSize" id="humanResourceDetailsSize"  value="<c:out value="${humanResourceDetailsSize}"/>" />
                                    <input type="hidden" name="humanId" id="humanId" value="<c:out value="${human.humanId}"/>"/>
                                </td>
                                <td>
                                    <select name="hrId" class='form-control' style='width:125px'>
                                        <option>---Select---</option>
                                        <c:if test="${humanResourceList != null}" >
                                            <c:forEach items="${humanResourceList}" var="hrList">
                                                <c:choose>
                                                    <c:when test="${hrList.hrId==human.hrId}">
                                                        <option selected value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                        <option value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" /> </option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach >
                                        </c:if>
                                    </select>
                                </td>
                                <td><input type="text" name="noOfPersons" id="noOfPersons" value="<c:out value="${human.noOfPersons}"/>"  class="form-control" style='width:120px' /></td>
                                <td><input type="text" name="fixedAmount" id="fixedAmount" value="<c:out value="${human.fixedAmount}"/>"  class="form-control" style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                            </tr>
                            <%sno1++;%>
                        </c:forEach>
                    </c:if>
                    <c:if test="${humanResourceDetails == null}">
                        <tr>
                            <td>
                                <%=sno1%><input type="hidden" id="updateValue" name="updateValue" value="1" >
                                <input type="hidden" name="pointId" id="pointId"  value="0" />
                                <input type="hidden" name="humanResourceDetailsSize" id="humanResourceDetailsSize"  value="<c:out value="${humanResourceDetailsSize}"/>" />
                                <input type="hidden" name="humanId" id="humanId" value="0"/>
                            </td>
                            <td>
                                <select name="hrId" class='form-control' style='width:125px'>
                                    <c:if test="${humanResourceList != null}" >
                                        <option value="0">---Select---</option>
                                        <c:forEach items="${humanResourceList}" var="hrList">
                                            <option value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" /> </option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td><input type="text" name="noOfPersons" id="noOfPersons" value="0"  class="form-control" style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                            <td><input type="text" name="fixedAmount" id="fixedAmount" value="0"  class="form-control" style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>
                        </tr>
                    </c:if>
                <center>
                    <tr>
                    <br>
                    <br>
                    <br>
                    <td colspan="4" align="center">
                        <input type="button" class="button" name="add" value="add" onclick="addRow1();"/>
                    </td>
                    </tr>
                </center>
                </tbody>
            </table>

            <br/>
            <br/>
            <br/>
            <center>
                <input type="button" class="button" name="Save" value="Save" onclick="submitPage()"/>
            </center>

            <script type="text/javascript">
                //            var sno = 1;
                //            var httpRequest;
                //            var httpReq;
                //            var styl = "";
                var rowCount = 1;
                var sno = 0;
                var rowCount1 = 1;
                var sno1 = 0;
                var httpRequest;
                var httpReq;
                var styl = "";
                var routeSize = 0;

                function addRow1() {
                    var routeSize1 = document.getElementById('humanResourceDetailsSize').value;
                    if (routeSize1 > 0) {
                        sno1 = parseInt(routeSize1);
                        routeSize = parseInt(routeSize1);
                    }
                    else {
                        routeSize1++;
                        sno1 = parseInt(routeSize1);
                    }
                    if (parseInt(rowCount1) % 2 == 0)
                    {
                        styl = "text2";
                    } else {
                        styl = "text1";
                    }
                    sno1++;

                    var tab = document.getElementById("addHumanResource");
                    var rowCountNew = document.getElementById('addHumanResource').rows.length;
                    rowCountNew--;

                    var newrow = tab.insertRow(rowCountNew);

                    cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' style='width:10px;'> " + sno1 + "</td>";
                    cell.setAttribute("className", styl);
                    cell.innerHTML = cell0;
                    if (sno1 == 1) {
                        cell = newrow.insertCell(1);
                        cell0 = "<td class='text1' height='25'><select class='form-control' id='hrId" + sno + "' style='width:125px'  name='hrId'><option selected value=0>---Select---</option><c:if test="${humanResourceList != null}" ><c:forEach items="${humanResourceList}" var="hrList"><option  value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" /> </c:forEach > </c:if> </select></td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(2);
                        var cell0 = "<td class='text1' height='25' ><input class='form-control' type='text' value='' style='width:120px;'  name='noOfPersons'  id='noOfPersons" + sno + "'   value='<c:out value="${human.noOfPersons}"/>' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(3);
                        var cell0 = "<td class='text1' height='25' ><input class='form-control' type='text' value='' style='width:120px;'  name='fixedAmount'  id='fixedAmount" + sno + "'   value='<c:out value="${human.fixedAmount}"/>' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
                        cell.innerHTML = cell0;
                    }else {
                        cell = newrow.insertCell(1);
                        cell0 = "<td class='text1' height='30' ><input type='hidden' id='updateValue" + sno + "' name='updateValue' value='1' ><td class='text1' height='25'><select class='form-control' id='hrId" + sno + "' style='width:125px'  name='hrId'><option selected value=0>---Select---</option><c:if test="${humanResourceList != null}" ><c:forEach items="${humanResourceList}" var="hrList"><option  value='<c:out value="${hrList.hrId}" />'><c:out value="${hrList.hrName}" /> </c:forEach > </c:if> </select></td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(2);
                        var cell0 = "<td class='text1' height='25' ><input class='form-control' type='text' value='' style='width:120px;'  name='noOfPersons'  id='noOfPersons" + sno + "'   value='<c:out value="${human.noOfPersons}"/>' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
                        cell.innerHTML = cell0;

                        cell = newrow.insertCell(3);
                        var cell0 = "<td class='text1' height='25' ><input class='form-control' type='text' value='' style='width:120px;'  name='fixedAmount'  id='fixedAmount" + sno + "'   value='<c:out value="${human.fixedAmount}"/>' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
                        cell.innerHTML = cell0;
                    }

                    rowCount1++;
                    sno++;
                    document.getElementById('humanResourceDetailsSize').value++;

                }
            </script>

        </c:if>
        <br>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
