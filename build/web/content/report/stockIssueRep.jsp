

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        var httpReq;
        var temp = "";
        function ajaxData()
        {
           if(document.IssueReport.mfrId.value !='0'){
            var url = "/throttle/getModels1.do?mfrId="+document.IssueReport.mfrId.value;
            if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() { processAjax(); } ;
                    httpReq.send(null);
                }
                }
                function processAjax()
                {
                    if (httpReq.readyState == 4)
                        {
                            if(httpReq.status == 200)
                                {
                                    temp = httpReq.responseText.valueOf();
                                    setOptions(temp,document.IssueReport.modelId);
                                    if('<%= request.getAttribute("modelId")%>' != 'null' ){
                                        document.IssueReport.modelId.value = <%= request.getAttribute("modelId")%>;
                                    }
                                }
                                else
                                    {
                                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                    }
                                }
                            }



                            function submitPage(){
                                var chek=validation();
                                if(chek=='true'){
                                    document.IssueReport.action = '/throttle/stockIssueReport.do';
                                    document.IssueReport.submit();
                                }
                            }
                            function validation(){
                                if(document.IssueReport.companyId.value==0){
                                    alert("Please Select Data for Company Name");
                                    document.IssueReport.companyId.focus();
                                    return 'false';
                                }
                                else  if(textValidation(document.IssueReport.fromDate,'From Date')){
                                    return 'false';
                                }
                                else  if(textValidation(document.IssueReport.toDate,'TO Date')){
                                    return'false';
                                }
                                return 'true';
                            }

                            function setValues(){
                                var poId='<%=request.getAttribute("poId")%>';
                                if('<%=request.getAttribute("companyId")%>'!='null'){
                                    document.IssueReport.companyId.value='<%=request.getAttribute("companyId")%>';
                                    document.IssueReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
                                    document.IssueReport.toDate.value='<%=request.getAttribute("toDate")%>';
                                }
                                if('<%=request.getAttribute("categoryId")%>'!='null'){
                                    document.IssueReport.categoryId.value='<%=request.getAttribute("categoryId")%>';
                                }
                                if('<%=request.getAttribute("mfrId")%>'!='null'){
                                    document.IssueReport.mfrId.value='<%=request.getAttribute("mfrId")%>';
                                }
                                if('<%=request.getAttribute("modelId")%>'!='null'){
                                    ajaxData();
                                    document.IssueReport.modelId.value='<%=request.getAttribute("modelId")%>';
                                }
                                if('<%=request.getAttribute("mfrCode")%>'!='null'){
                                    document.IssueReport.mfrCode.value='<%=request.getAttribute("mfrCode")%>';
                                }
                                if('<%=request.getAttribute("paplCode")%>'!='null'){
                                    document.IssueReport.paplCode.value='<%=request.getAttribute("paplCode")%>';
                                }
                                if('<%=request.getAttribute("itemName")%>'!='null'){
                                    document.IssueReport.itemName.value='<%=request.getAttribute("itemName")%>';
                                }
                                if('<%=request.getAttribute("regNo")%>'!='null'){
                                    document.IssueReport.regNo.value='<%=request.getAttribute("regNo")%>';
                                }
                                if('<%=request.getAttribute("rcWorkorderId")%>'!='null'){
                                    document.IssueReport.rcWorkorderId.value='<%=request.getAttribute("rcWorkorderId")%>';
                                }
                                if('<%=request.getAttribute("counterId")%>'!='null'){
                                    document.IssueReport.counterId.value='<%=request.getAttribute("counterId")%>';
                                }

                            }
                            function getItemNames(){
                                var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));

                            }
function getVehicleNos(){
    //onkeypress='getList(sno,this.id)'
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}

function toexcel(){

         document.IssueReport.action='/throttle/handleStockIssueExcelRepNew.do';
        document.IssueReport.submit();
    }
    </script>
    <body onload="setValues();getItemNames();getVehicleNos();">
        <form name="IssueReport" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Material Issue Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30"><font color="red">*</font>Location</td>
        <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                <option value="">-select-</option>
                <c:if test = "${LocationLists != null}" >
                    <c:forEach items="${LocationLists}" var="company">
                        <c:choose>
                            <c:when test="${company.companyTypeId==1012}" >

                                <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </c:if>

        </select></td>

        <td height="30">&nbsp;&nbsp;MFR</td>
        <td height="30"><select name="mfrId" class="form-control">
                <option value="">-select-</option>
                <c:if test = "${MfrLists != null}" >
                    <c:forEach items="${MfrLists}" var="mfr">
                        <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                    </c:forEach>
                </c:if>
        </select></td>
        <td width="114" height="30">&nbsp;&nbsp;Category</td>
        <td width="172" height="30"><select name="categoryId" class="form-control" style="width:125px">
                <option value="">-select-</option>
                <c:if test = "${categoryList != null}" >
                    <c:forEach items="${categoryList}" var="category">
                        <option value="<c:out value="${category.categoryId}"/>"><c:out value="${category.categoryName}"/></option>
                    </c:forEach>
                </c:if>

        </select></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;PAPL Code</td>
        <td height="30"><input name="paplCode" type="text" class="form-control" value="" size="20"></td>
         <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
        <td width="172" height="30"><input name="itemName" id="itemName" type="text"  class="form-control" value=""></td>
        <td>&nbsp;&nbsp;Vehicle No</td>
        <td height="30"><input name="regNo" id="regno" type="text" class="form-control" value="" size="20"></td>
        </tr>
    <tr>

        <td width="80" height="30"><font color="red">*</font>From Date</td>
        <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.fromDate,'dd-mm-yyyy',this)"/></td>
        <td width="148"><font color="red">*</font>To Date</td>
        <td width="172" height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.toDate,'dd-mm-yyyy',this)"/></td>
        <td width="80" height="30">Workorder No</td>
        <td width="80" height="30"><input type="text" name="rcWorkorderId" class="form-control"></td>
    </tr>
    <tr>

        <td width="80" height="30">Counter No</td>
        <td width="182"><input name="counterId" type="text" class="form-control" value="" size="20">
        </td>
        <td width="148">&nbsp;</td>
        <td width="172" height="30">&nbsp;
        </td>
        <td width="80" height="30">&nbsp;</td>
        <td width="80" height="30"><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
                <input type="hidden" value="" name="reqfor"></td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
            <br>
                    <c:set var="totalValue" value="0"/>
                    <c:set var="totalProfit" value="0"/>
                    <c:set var="totalNettProfit" value="0"/>
                    <c:set var="totalUnbilledValue" value="0"/>
                    <c:set var="totalUnbilledProfit" value="0"/>
                    <c:set var="totalUnbilledNettProfit" value="0"/>
                    <c:set var="totalQty" value="0"/>
            <c:if test = "${stokIssueList != null}" >


                    <% int index = 0; String type="Billed";%>
                    <c:forEach items="${stokIssueList}" var="issue">

                        <c:if test = "${issue.type == 'unbilled'}" >
                            <% 
                            if(type.equals("Billed") ){
                                index = 0; type="UnBilled";
                                %></table><%
                            }
                            %>

                        </c:if>
                    <%if (index == 0){%>

                    <table width="1200" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                        <tr><td colspan="23" align="left" ><%=type%></td></tr>
                    <tr class="contenthead">
                        <td class="contentsub">Sno</td>
                        <td height="30" class="contentsub">MFR</td>
                        <td height="30" class="contentsub">Model</td>
                        <td height="30" class="contentsub">REG NO</td>
                        <td height="30" class="contentsub">JOBCARD NO</td>
                        <td height="30" class="contentsub">RCWO NO</td>
                        <td height="30" class="contentsub">COUNTER NO</td>
                        <td height="30" class="contentsub">MRSNo </td>
                        <td height="30" class="contentsub">MANUALMRSNo </td>
                        <td height="30" class="contentsub">MRSDATE</td>
                        <td height="30" class="contentsub">IssueDATE</td>
                        <td class="contentsub">PAPL CODE</td>
                        <td height="30" class="contentsub">ITEM NAME</td>
                        <td class="contentsub">Tech Name</td>
                        <td class="contentsub">User</td>
                        <td height="30" class="contentsub">Iss Qty</td>
                        <td height="30" class="contentsub">Ret Qty</td>
                        <td height="30" class="contentsub">Net Qty</td>
                        <td height="30" class="contentsub">BuyPrice</td>
                        <td height="30" class="contentsub">SellPrice</td>
                        <td height="30" class="contentsub">Profit</td>
                        <td height="30" class="contentsub">Tax</td>
                        <td height="30" class="contentsub">NettProfit</td>

                    </tr>

            <%
            }
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>

                        <tr>
                            <%--//For Generate Excel--%>
                        <input type="hidden" name="sno" value="<%=index+1%>">
                        <input type="hidden" name="mfrName" value="<c:out value="${issue.mfrName}"/>">
                        <input type="hidden" name="regNos" value="<c:out value="${issue.regNo}" />">
                        <input type="hidden" name="jobCardId" value="<c:out value="${issue.jobCardId}"/>">
                        <input type="hidden" name="rcWorkorderIds" value="<c:out value="${issue.rcWorkorderId}"/>">
<!--                        <input type="hidden" name="counterIds" value="<c:out value="${issue.counterId}"/>">-->
                        <input type="hidden" name="counterIds" value="-">
                        <input type="hidden" name="mrsId" value="<c:out value="${issue.mrsId}"/>">
                        <input type="hidden" name="manualMrsNo" value="<c:out value="${issue.manualMrsNo}"/>">
                        <input type="hidden" name="createdDate" value="<c:out value="${issue.createdDate}"/>">
                        <input type="hidden" name="manualMrsDate" value="<c:out value="${issue.manualMrsDate}"/>">
                        <input type="hidden" name="paplCodes" value="<c:out value="${issue.paplCode}" />">
                        <input type="hidden" name="itemNames" value="<c:out value="${issue.itemName}"/>">
                        <input type="hidden" name="categoryName" value="<c:out value="${issue.categoryName}"/>">
                        <input type="hidden" name="itemType" value="<c:out value="${issue.itemType}"/>">
                        <input type="hidden" name="itemPrice" value="<c:out value="${issue.itemPrice}"/>">
                        <input type="hidden" name="itemQty" value="<c:out value="${issue.itemQty}"/>">

                            <td class="<%=classText%>" height="30"><%=index+1%></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.mfrName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.modelName}"/></td>
                            <td class="<%=classText%>"><c:out value="${issue.regNo}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.jobCardId}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.rcWorkorderId}"/></td>
<!--                            <td class="<%=classText%>" height="30"><c:out value="${issue.counterId}"/></td>-->
                            <td class="<%=classText%>" height="30">-</td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.mrsId}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.manualMrsNo}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.manualMrsDate}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.issueDate}"/></td>
                            <td class="<%=classText%>"><c:out value="${issue.paplCode}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemName}"/></td>

                            <td class="<%=classText%>" height="30"><c:out value="${issue.empName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.user}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.issueQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.retQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.itemQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.buyPrice}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.sellPrice}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.profit}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.tax}"/></td>
                            
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.nettProfit}"/></td>

<!--                         <td class="<%=classText%>" height="30"><c:out value="${issue.categoryName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemType}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemPrice}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemQty}"/></td>-->

                            <c:if test = "${issue.type == 'billed'}" >
                              <c:set var="totalValue" value="${(issue.sellPrice * issue.itemQty) + totalValue}" />
                              <c:set var="totalProfit" value="${issue.profit + totalProfit}" />
                              <c:set var="totalNettProfit" value="${issue.nettProfit + totalNettProfit}" />
                            </c:if>
                              <c:if test = "${issue.type == 'unbilled'}" >
                                  <c:set var="totalUnbilledValue" value="${(issue.sellPrice * issue.itemQty) + totalUnbilledValue}" />
                                  <c:set var="totalUnbilledProfit" value="${issue.profit + totalUnbilledProfit}" />
                                  <c:set var="totalUnbilledNettProfit" value="${issue.nettProfit + totalUnbilledNettProfit}" />
                              </c:if>
                              <c:set var="totalQty" value="${issue.itemQty + totalQty}" />
                        </tr>
                        <% index++;%>
                    </c:forEach>
               <table>
                   <tr><td colspan="23" align="left" >Billed</td></tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalValue}" pattern="##.00"/></b></td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalNettProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr><td colspan="23" align="left" >UnBilled</td></tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalUnbilledValue}" pattern="##.00"/></b></td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalUnbilledProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>


                    <tr><td colspan="23" align="left" >Summary</td></tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Nett Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalValue + totalUnbilledValue}" pattern="##.00"/></b></td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalProfit + totalUnbilledProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                    <tr>
                    <td class="text2" colspan="14" >&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalNettProfit + totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>
                    <td class="text2">&nbsp;</td>

                    </tr>
                </table>
    <br>
<center>
<input class="button" type="button" value="ExportToExcel" onClick="toexcel();">
</center>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>

