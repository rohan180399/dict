<%--
    Document   : invoiceEditLogDetails
    Created on : Jun 8, 2016, 4:37:58 PM
    Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script>
            function submitPage(){
               document.log.action = '/throttle/searchInvoiceEditLog.do';
               document.log.submit();
            }
            </script>
    </head>
    <body onload="setValue();
            sorter.size(10);">
        <form name="log" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>
            <table width="1000" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="10" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Invoice Edit Log Details</li>
                            </ul>
                            <div id="first">
                                <table width="950px" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr colspan="2">
                                        <td  height="30"><font color="red">*</font>GR No :</td>
                                        <td  height="30"><input type="text" name="grNo" id="grNo" class="text" value="<c:out value="${grNo}"/>" />  </td>


                                    <!--<td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>-->
                                    <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage();"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>

              <table align="center" border="0" id="table"  class="sortable" width="90%">
                <thead>
                    <tr  height="80" width="180" >
                        <th><h4>S.No</h4></th>
                        <th><h4>GR No</h4></th>
                        <th><h4>Container No. Old</h4></th>
                        <th><h4>Container No. New</h4></th>
                        <th><h4>Shipping No. Old</h4></th>
                        <th><h4>Shipping No. New</h4></th>
                        <th><h4>Bill of  Entry Old</h4></th>
                        <th><h4>Bill of Entry New</h4></th>
                        <th><h4>Detention Charge Old</h4></th>
                        <th><h4>Detention Charge New </h4></th>
                        <th><h4>Green Tax Charge Old </h4></th>
                        <th><h4>Green Tax Charge New</h4></th>
                        <th><h4>Toll Tax Old</h4></th>
                        <th><h4>Toll Tax New </h4></th>
                        <th><h4>Other Expense New </h4></th>
                        <th><h4>Other Expense Old </h4></th>
                        <th><h4>weightment New </h4></th>
                        <th><h4>weightment Old </h4></th>
                        <th><h4>Billing Party Old </h4></th>
                        <th><h4>Billing Party New </h4></th>
                        <th><h4>Movement Type </h4></th>
                        <th><h4>Billing Type</h4></th>
                        <th><h4>User Name</h4></th>
                        <th><h4>Time</h4></th>

                    </tr>
                </thead>
                <tbody>


                    <% int index = 1,sno = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>

                    <c:forEach items="${invoiceEditLogDetails}" var="log">


                        <tr height="50" width="150px;">
                            <td align="center" class="<%=classText%>"><%=sno%></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.grNo}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.containerNoOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.containerNo}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.shipBillNoOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.shipBillNo}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.billOfEntryOld}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.billOfEntry}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.detentionChargeOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.detentionCharge}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.greenTaxOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.greenTax}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.tollTaxOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.tollTax}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.otherExpenseOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.otherExpense}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.weightmentOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.weightment}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.billingPartyIdOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.billingPartyIdNew}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.movementType}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.billType}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.userName}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.modifiedTime}"/></td>



                        </tr>
                   <%
                   index++;
                     sno++;
                        %>
                    </c:forEach>

                </tbody>


            </table>
            <br/>
            <br/>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <br/>
            <br/>
            <br/>
            <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",0);
        </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>