<%-- 
    Document   : PhysicalStockEntry3
    Created on : Nov 1, 2012, 7:15:27 PM
    Author     : admin
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
         <script>

            function submitpage(){
                var errMsg = "";
                if(document.stockEntry2.existPrice.value == "" && document.stockEntry2.newPrice.value == "") {
                    errMsg = errMsg+"Select Existing price Or Fill New Price";
                }

                if(errMsg == "") {
                    document.stockEntry2.action = "PhysicalStockEntry4.jsp";
                    document.stockEntry2.method = "post";
                    document.stockEntry2.submit();
                } else {
                    alert(errMsg);
                }
            }
            function enableNewPrice(){

                if(document.stockEntry2.existPrice.value !=  "") {
                    document.stockEntry2.newPrice.value = "";
                    document.stockEntry2.newPrice.readOnly = true;
                } else {
                    document.stockEntry2.newPrice.readOnly = false;
                    document.stockEntry2.existPrice.value !=  ""
                }

            }

        </script>

    </head>
    <body>
        <form name="stockEntry2">

        <%
        Connection conn = null;
        int count = 0;
        try{

            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();

            InputStream is = getClass().getResourceAsStream("/"+fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");

            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            /*out.println(dbClassName);
            out.println(dbUserName+"<br>");
            out.println(dbPassword+"<br>");*/

            String itemCode = request.getParameter("itemCode");
            String itemName = request.getParameter("itemName");
            String uomName = request.getParameter("uomName");
            String itemId = request.getParameter("itemId");
            String newStock = request.getParameter("newStock");
            if(newStock == null) {
            newStock = "0";
            } else {
            newStock = newStock.trim();
            }

            DecimalFormat df2 = new DecimalFormat("0.00");
            Class.forName(dbClassName).newInstance();
            conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            String query = " SELECT price_id, concat(price,' (',price_type,')') as price FROM papl_item_price_master "
                        + "WHERE item_id = ?";
            PreparedStatement pstm = conn.prepareStatement(query);
            pstm.setString(1, itemId);
            //  out.print(pstm);
            String itemPrice = "";
            int itemPriceId = 0;

        
        
        
        %>
          <table width="450" cellpadding="0" cellspacing="0" height="250" align="center" class="table2">
                <tr><td colspan="2" align="center" class="contenthead">Stock Manual Entry- Step:3</td>
                </tr>
                <tr>
                    <td>Item Code</td>
                    <td><%=itemCode%>
                        <input type="hidden" name="itemCode" id="itemCode" value="<%=itemCode%>" />
                        <input type="hidden" name="itemId" id="itemId" value="<%=itemId%>" />
                    </td>
                </tr>
                <tr>
                    <td>Item Name</td>
                    <td><%=itemName%></td>
                </tr>
                <tr>
                    <td>UOM</td>
                    <td><%=uomName%></td>
                </tr>
                <tr>
                    <td>New Stock</td>
                    <td><%=newStock%><input type="hidden" name="newStock" id="newStock" value="<%=newStock%>" readonly /></td>
                </tr>
                <tr>
                    <td>Existing Price<br> (without Tax)</td>
                    <td>
                        <select name="existPrice" id="existPrice" onchange="enableNewPrice();">
                            <option value="">-select-</option>
                            <%
                            ResultSet res = pstm.executeQuery();
                            while(res.next()) {
                               itemPriceId = res.getInt("price_id");
                               itemPrice = res.getString("price");
                                %>
                                   <option value="<%=itemPriceId%>"><%=itemPrice%></option>
                                <%
                            }
                            if(pstm != null) {
                                pstm.close();
                            }
                            if(res != null) {
                                res.close();
                            }
                        %>
                        </select>
                    </td>
                    </tr>

                <tr>
                    <td>New Price<br> (without Tax)</td>
                    <td><input type="text" name="newPrice" id="newPrice" maxlength="10" onblur="enableNewPrice();" onchange="enableNewPrice();" /><br>Default Tax@ 14.5 (%) </td>
               </tr>

                
                <tr><td colspan="2" align="center">
                        <input type="button" name="proceed" value=" Save " onclick="submitpage();" />
                    </td></tr>
            </table>
        <%


        }
        catch (IOException ioe){
            System.err.println("Properties loading failed in AppConfig");

        }finally{


            if(conn != null) {
                conn.close();
            }

        }

        %>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
