
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
$("#datepicker").datepicker({
showOn: "button",
buttonImage: "calendar.gif",
buttonImageOnly: true

});
});

$(function() {
//alert("cv");
$(".datepicker").datepicker({
/*altField: "#alternate",
altFormat: "DD, d MM, yy"*/
changeMonth: true, changeYear: true
});
});



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#tabs").tabs();
});
</script>


<script type="text/javascript" language="javascript">




function getDriverName() {
var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

}
</script>
<script language="">
function print(val)
{
var DocumentContainer = document.getElementById(val);
var WindowObject = window.open('', "TrackHistoryData",
"width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
WindowObject.document.writeln(DocumentContainer.innerHTML);
WindowObject.document.close();
WindowObject.focus();
WindowObject.print();
WindowObject.close();
}

function popUp(url) {
var http = new XMLHttpRequest();
http.open('HEAD', url, false);
http.send();
if (http.status != 404) {
popupWindow = window.open(
url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
}
else {
var url1 = "/throttle/content/trip/fileNotFound.jsp";
popupWindow = window.open(
url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
}
}

</script>

<!--<div class="pageheader">
<h2><i class="fa fa-edit"></i>Fleet Vendor Contract</h2>
<div class="breadcrumb-wrapper">
<span class="label">You are here:</span>
<ol class="breadcrumb">
<li><a href="index.html">Home</a></li>
<li><a href="general-forms.html">Vendors</a></li>
<li class="active">Fleet Vendor Contract</li>
</ol>
</div>
</div>--> <style>
#index td {
color:white;
font-weight: bold;
}
</style>
<div class="contentpanel">
<div class="panel panel-default">
<div class="panel-body">

<body>

<form name="trip" method="post">
<br>
<%@ include file="/content/common/message.jsp" %>
<style>
body {
    font:13px verdana;
    font-weight:normal;
}
</style>

<%
int desigId = (Integer)session.getAttribute("DesigId");
if(desigId != 1046){
%>


<table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

<tr id="exp_table" >
    <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:300;">

            <div id="first">
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                            <tr id="exp_table" >
                                <td><font color="white"><b>Expected Revenue:</b></font></td>
                                <td><font color="white"><b> <c:out value="${trip.orderRevenue}" /></b></font></td>

                            </tr>
                            <tr id="exp_table" >
                                <c:if test="${tripDetails != null}">
                                    <c:forEach items="${tripDetails}" var="tripDetails">
                                        <c:if test="${tripDetails.statusId >= 12}">
                                            <td> <font color="white"><b>Actual Expense:</b></font></td>
                                            </c:if>
                                            <c:if test="${tripDetails.statusId < 12}">
                                            <td> <font color="white"><b>Projected Expense:</b></font></td>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                <td><font color="white"><b> <c:out value="${trip.orderExpense}" /></b></font></td>
                            </tr>
                            <c:set var="profitMargin" value="" />
                            <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                            <c:set var="orderExpense" value="${trip.orderExpense}" />
                            <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                            <%
                            String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                            String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                            float profitPercentage = 0.00F;
                            if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                            }


                            %>
                            <tr id="exp_table" >
                                <td> <font color="white"><b>Profit Margin:</b></font></td>
                                <td> <font color="white"><b> <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>
                                    <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                <td>
                                <td>
                            </tr>
                        </table>
                    </c:forEach>
                </c:if>
            </div>
        </div>
    </td>
</tr>
</table>
<br>
<%}%>
<br>
<br>
<br>
<br>
<style>
#index td {
    color:white;
    font-weight: bold;
}
</style>
<table class="tableborder" style="width:100%;">
<% int loopCntr = 0;%>
<c:if test = "${tripDetails != null}" >
    <c:forEach items="${tripDetails}" var="trip">
        <% if(loopCntr == 0) {%>
        <tr height="30" id="index" style="background-color:#5BC0DE;font-size:14px;">
            <td  >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
            <td >Trip Code: <c:out value="${trip.tripCode}"/></td>
            <td >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
            <td >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
            <td >Status: <c:out value="${trip.status}"/></td>
        </tr>
        <% }%>
        <% loopCntr++;%>
    </c:forEach>
</c:if>
</table>

<div id="tabs" >
<ul class="nav nav-tabs">

    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 6 || tripDetails.statusId == 7 || tripDetails.statusId == 8 }">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>
            </c:forEach></c:if>
        <c:if test="${tripDetails != null}">
            <c:forEach items="${tripDetails}" var="tripDetails">
                <c:if test="${tripDetails.statusId == 9}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>
                <!--                     <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 10}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance /Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 18}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance/ Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 12}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance/Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                <li data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 13}">
                <li  class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance/Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                <li data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                <li data-toggle="tab"><a href="#expenseDetail"><span>Expense Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 14}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance / Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                <li data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                <li data-toggle="tab"><a href="#expenseDetail"><span>Expense Details</span></a></li>
                <li data-toggle="tab"><a href="#settlementDetail"><span>Settlement Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 15}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance /Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                <li data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                <li data-toggle="tab"><a href="#expenseDetail"><span>Expense Details</span></a></li>
                <li data-toggle="tab"><a href="#settlementDetail"><span>Settlement Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <c:if test="${tripDetails != null}">
        <c:forEach items="${tripDetails}" var="tripDetails">
            <c:if test="${tripDetails.statusId == 16}">
                <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                <li data-toggle="tab"><a href="#advance"><span>Advance /Fuel</span></a></li>
                <li data-toggle="tab" style="display:none"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                <!--                     <li data-toggle="tab"><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Details</span></a></li>
                <li data-toggle="tab"><a href="#endDetail"><span>Trip End Details</span></a></li>
                <li data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                <li data-toggle="tab"><a href="#expenseDetail"><span>Expense Details</span></a></li>
                <li data-toggle="tab"><a href="#settlementDetail"><span>Settlement Details</span></a></li>
                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                </c:if>

        </c:forEach>
    </c:if>
    <!--
    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
    <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
    <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
</ul>

<div id="tripDetail" class="tab-pane active">
    <table class="table table-info mb30 table-hover" style="width:100%">
        <thead>
        <th colspan="6" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Trip Details</th>
        </thead>
        <c:if test = "${tripDetails != null}" >
            <c:forEach items="${tripDetails}" var="trip">


                <tr>
                    <!--                            <td class="text1">Trip Sheet Date</td>
                                                <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                    <td class="text1">Consignment No(s)</td>
                    <td class="text1">
                        <c:out value="${trip.cNotes}" />
                    </td>
                    <td class="text1">Billing Type</td>
                    <td class="text1">
                        <c:out value="${trip.billingType}" />
                    </td>
                </tr>
                <tr>
                    <!--                            <td class="text2">Customer Code</td>
                                                <td class="text2">BF00001</td>-->
                    <td class="text2">Customer Name</td>
                    <td class="text2">
                        <c:out value="${trip.customerName}" />
                        <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                    </td>
                    <td class="text2">Customer Type</td>
                    <td class="text2" colspan="3" >
                        <c:out value="${trip.customerType}" />
                    </td>
                </tr>
                <tr>
                    <td class="text1">Route Name</td>
                    <td class="text1">
                        <c:out value="${trip.routeInfo}" />
                    </td>
                    <!--                            <td class="text1">Route Code</td>
                                                <td class="text1" >DL001</td>-->
                    <td class="text1">Reefer Required</td>
                    <td class="text1" >
                        <c:out value="${trip.reeferRequired}" />
                    </td>
                    <td class="text1">Order Est Weight (MT)</td>
                    <td class="text1" >
                        <c:out value="${trip.totalWeight}" />
                    </td>
                </tr>
                <tr>
                    <td class="text2">Vehicle Type</td>
                    <td class="text2">
                        <c:out value="${trip.vehicleTypeName}" />
                    </td>
                    <td class="text2">Vehicle No</td>
                    <td class="text2">
                        <c:out value="${trip.vehicleNo}" />
                        <br>
                <fonnt color="red"><c:out value="${trip.oldVehicleNo}" /></font>
                    </td>
                    <td class="text2">Vehicle Capacity (MT)</td>
                    <td class="text2">
                        <c:out value="${trip.vehicleTonnage}" />

                    </td>
                    </tr>

                    <tr>
                        <td class="text1">Veh. Cap [Util%]</td>
                        <td class="text1">
                            <c:out value="${trip.vehicleCapUtil}" />
                        </td>
                        <td class="text1">Special Instruction</td>
                        <td class="text1">-</td>
                        <td class="text1">Trip Schedule</td>
                        <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                    </tr>


                    <tr>
                        <td class="text2">Driver </td>
                        <td class="text2" colspan="5" >
                            <c:out value="${trip.driverName}" />
                            <br>
                    <fonnt color="red"><c:out value="${trip.oldDriverName}" /></font>
                        </td>

                        </tr>
                        <tr>
                            <td class="text1">Product Info </td>
                            <td class="text1" colspan="5" >
                                <c:out value="${trip.productInfo}" />
                            </td>

                        </tr>
                    </c:forEach>
                </c:if>
                </table>
                <br/>

                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7  && tripDetails.statusId != 8 && tripDetails.statusId != 9 || tripDetails.statusId ==10 || tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16 || tripDetails.statusId ==18}">
                            <c:if test = "${tripStartDetails != null}" >
                                <table class="tableborder table-bordered mb30 table-hover" style="width:100%;display: none">

                                    <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                    <c:forEach items="${tripStartDetails}" var="startDetails">
                                        <thead>
                                        <th colspan="6" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Trip Start Details</th>
                                        </thead>
                                        <tr>
                                            <td class="text1" height="30" >Trip Planned Start Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Planned Start Time</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Start Reporting Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                        </tr>
                                        <tr>
                                            <td class="text2" height="30" >Trip Start Reporting Time</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Loading date</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Loading Time</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td class="text1" height="30" >Trip Loading Temperature</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Actual Start Date</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                            <td class="text1" height="30" >Trip Actual Start Time</td>
                                            <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                            <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                            <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                            <td class="text2" height="30" colspan="2" ></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                            </c:if>
                            <br/>
                            <br/>
                        </c:if>
                    </c:forEach></c:if>
                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  && tripDetails.statusId != 9 && tripDetails.statusId != 10 && tripDetails.statusId !=18 || tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16 }">
                            <c:if test = "${tripEndDetails != null}" >
                                <table class="tableborder table-bordered mb30 table-hover" style="width:100%">
                                    <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                    <c:forEach items="${tripEndDetails}" var="endDetails">
                                        <thead>
                                        <th colspan="6" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Trip End Details</th>
                                        </thead>
                                        <tr>
                                            <td class="text1" height="30" >Trip Planned End Date</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                            <td class="text1" height="30" >Trip Planned End Time</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                            <td class="text1" height="30" >Trip Actual Reporting Date</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text2" height="30" >Trip Actual Reporting Time</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                            <td class="text2" height="30" >Trip Unloading Date</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                            <td class="text2" height="30" >Trip Unloading Time</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" height="30" >Trip Unloading Temperature</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                            <td class="text1" height="30" >Trip Actual End Time</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endTime}" /></td>
                                            <td class="text1" height="30" >Trip End Odometer Reading(KM)</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                            <td class="text2" height="30" >Total Odometer Reading(KM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                            <td class="text2" height="30" >Total Reefer Reading(HM)</td>
                                            <td class="text2" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="text1" height="30" >Total Duration Hours</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                            <td class="text1" height="30" >Total Days</td>
                                            <td class="text1" height="30" ><c:out value="${endDetails.totalDays}" />
                                            <td class="text1" height="30" colspan ="4"></td>
                                        </tr>

                                    </c:forEach >
                                </table>
                            </c:if>
                            <br/>
                            <br/>
                        </c:if>
                    </c:forEach></c:if>
                    <br/>

                <c:if test = "${expiryDateDetails != null}" >
                    <!--                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                                                </tr>-->
                    <table class="tableborder table-bordered mb30 table-hover" style="width:100%;display: none">
                        <thead>
                        <th colspan="2" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Vehicle Compliance Check</th>
                        </thead>
                        <c:forEach items="${expiryDateDetails}" var="expiryDate">
                            <tr>
                                <td class="text2">Vehicle FC Valid UpTo</td>
                                <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                            </tr>
                            <tr>
                                <td class="text1">Vehicle Insurance Valid UpTo</td>
                                <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                            </tr>
                            <tr>
                                <td class="text2">Vehicle Permit Valid UpTo</td>
                                <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                            </tr>
                            <tr>
                                <td class="text2">Road Tax Valid UpTo</td>
                                <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                            </tr>
                        </c:forEach>
                    </table>
                    <br/>
                    <br/>

                </c:if>
                <center>
                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                </center>
                </div>
                <div id="routeDetail" class="tab-pane">

                    <c:if test = "${tripPointDetails != null}" >
                        <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                        <table class="table table-info mb30 table-hover " style="width:100%;"  id="table2" >
                            <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                <tr height="30">
                                    <th>S No</th>
                                    <th>Point Name</th>
                                    <th>Type</th>
                                    <th>Route Order</th>
                                    <th>Address</th>
                                    <th>Planned Date</th>
                                    <th>Planned Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index2 = 1;%>
                                <c:forEach items="${tripPointDetails}" var="tripPoint">
                                    <%
                                                String classText = "";
                                                int oddEven1 = index2 % 2;
                                                if (oddEven1 > 0) {
                                                    classText = "text1";
                                                } else {
                                                    classText = "text2";
                                                }
                                    %>
                                    <tr height="20">
                                        <td class="<%=classText%>"  ><%=index2++%></td>
                                        <td class="<%=classText%>"  ><c:out value="${tripPoint.pointName}" /></td>
                                        <td class="<%=classText%>"><c:out value="${tripPoint.pointType}" /></td>
                                        <td class="<%=classText%>"><c:out value="${tripPoint.pointSequence}" /></td>
                                        <td class="<%=classText%>"><c:out value="${tripPoint.pointAddress}" /></td>
                                        <td class="<%=classText%>"><c:out value="${tripPoint.pointPlanDate}" /></td>
                                        <td class="<%=classText%>"><c:out value="${tripPoint.pointPlanTime}" /></td>
                                    </tr>

                                </c:forEach >
                            </tbody>
                        </table>
                        <br>
                        <table border="0" class="border" style="width:30%;display: none" align="left" width="60%" cellpadding="0" cellspacing="0" >
                            <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr style="background-color:#5BC0DE;width:100%;height:25px;color:white">
                                        <td  width="200"> Estimated KM</td>
                                        <td  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                    </tr>
                                    <tr style="background-color:#5BC0DE;width:100%;height:25px;color:white">
                                        <td width="200"> Estimated Reefer Hour</td>
                                        <td  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </c:if>
                    <br>
                    <center>
                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                    </center>
                    <br>

                </div>
                <div id="statusDetail" class="tab-pane">
                    <% int index1 = 1;%>

                    <c:if test = "${statusDetails != null}" >
                        <!--<table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                        <table class="table table-info mb30 table-hover" style="width:100%;"   id="bg" >
                            <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                <tr height="30">
                                    <th height="30" >S No</th>
                                    <th  height="30" >Status Name</th>
                                    <th  height="30" >Remarks</th>
                                    <th height="30" >Created User Name</th>
                                    <th  height="30" >Created Date</th>
                                </tr>

                            </thead>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                <%
                                            String classText = "";
                                            int oddEven1 = index1 % 2;
                                            if (oddEven1 > 0) {
                                                classText = "text1";
                                            } else {
                                                classText = "text2";
                                            }
                                %>
                                <tr height="25">
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                    </c:if>
                    <center>
                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                    </center>
                </div>

                <c:if test="${tripDetails != null}">
                    <%int count=1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  || tripDetails.statusId ==9 || tripDetails.statusId ==10 || tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16 || tripDetails.statusId ==18}">
                            <!--                    <div id="preStart">
                            <c:if test = "${tripPreStartDetails != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                                    <tr>
                                        <td class="contenthead" colspan="4" >Pre Start Details</td>
                                    </tr>
                                    <tr >
                                        <td class="text1" height="30" >Pre Start Date</td>
                                        <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                                        <td class="text1" height="30" >Pre Start Time</td>
                                        <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="text2" height="30" >Pre Odometer Reading</td>
                                        <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                    <c:if test = "${tripDetails != null}" >
                                        <td class="text2">Pre Start Location / Distance</td>
                                        <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:if>
                                </tr>
                                <tr>
                                    <td class="text1" height="30" >Pre Start Remarks</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                                </tr>
                                </c:forEach >
                            </table>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                             <center>
                                <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                            </center>
                            </c:if>
                            <br>
                            <br>
                        </div>-->


                            <div id="advance" class="tab-pane">

                                <c:if test="${tripAdvanceDetails != null}">

                                    <center><b style="font-size:13px;">Trip Advance Details</b></center>
                                    <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                                    <table class="table table-info mb30 table-hover" style="width:100%;"   id="bg" >
                                        <thead style="background-color:#5BC0DE;width:100%;color:white;font-size: 13px;">
                                            <tr height="30">
                                                <th width="30">Sno</th>
                                                <th width="90">Advance Date</th>
                                                <th width="90">Trip Day</th>
                                                <th width="120">Estimated Advance</th>
                                                <th width="120">Requested Advance</th>
                                                <th width="120">Currency</th>
                                                <th width="90"> Type</th>
                                                <th width="120">Requested By</th>
                                                <th width="120">Requested Remarks</th>
                                                <th width="120">Approved By</th>
                                                <th width="120">Approved Remarks</th>
                                                <th width="120">Paid Advance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%int index7=1;%>
                                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                <%
                                                       String classText7 = "";
                                                       int oddEven7 = index7 % 2;
                                                       if (oddEven7 > 0) {
                                                           classText7 = "text1";
                                                       } else {
                                                           classText7 = "text2";
                                                       }
                                                %>
                                                <tr>
                                                    <td class="<%=classText7%>"><%=index7++%></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                                    <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.currencyName}"/></td>
                                                    <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                        <td class="<%=classText7%>">Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                        <td class="<%=classText7%>">Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                        <td class="<%=classText7%>">Manual</td>
                                                    </c:if>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                    <td class="<%=classText7%>" align="right"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                </tr>
                                            </c:forEach>

                                            <tr>

                                                <td class="text1" colspan="11" align="right"><b>Total Advance Paid </b>&emsp;</td>
                                                <td class="text1" align="right"><b><c:out value="${totalAdvancePaid}"/></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <c:if test="${tripAdvanceDetailsStatus != null}">
                                        <center><b style="font-size:13px;">Advance Approval Status Details</b></center>
                                        <table class="table table-info mb30 table-hover" style="width:100%;"   id="bg" >
                                            <thead style="background-color:#5BC0DE;width:100%;color:white;font-size: 13px;">
                                                <tr height="30">
                                                    <th width="30">Sno</td>
                                                    <th width="90">Request Date</td>
                                                    <th width="90">Trip Day</td>
                                                    <th width="120">Estimated Advance</td>
                                                    <th width="120">Requested Advance</td>
                                                    <th width="90"> Type</td>
                                                    <th width="120">Requested By</td>
                                                    <th width="120">Requested Remarks</td>
                                                    <th width="120">Approval Status</td>
                                                    <th width="120">Paid Status</td>
                                                </tr>
                                                <%int index13=1;%>
                                                <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                    <%
                                                           String classText13 = "";
                                                           int oddEven11 = index13 % 2;
                                                           if (oddEven11 > 0) {
                                                               classText13 = "text1";
                                                           } else {
                                                               classText13 = "text2";
                                                           }
                                                    %>
                                                    <tr>

                                                        <td class="<%=classText13%>"><%=index13++%></td>
                                                        <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                        <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                        <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                        <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                            <td class="<%=classText13%>">Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                            <td class="<%=classText13%>">Batch</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                            <td class="<%=classText13%>">Manual</td>
                                                        </c:if>

                                                        <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                        <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                        <td class="<%=classText13%>">
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                                &nbsp
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                                Request Approved
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                                Request Rejected
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                                Approval in  Pending
                                                            </c:if>
                                                            &nbsp;</td>
                                                        <td class="<%=classText13%>">
                                                            <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                                Yet To Pay
                                                            </c:if>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                        </table>
                                        <br/>
                                        <br/>
                                        <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                            <center><b style="font-size:13px;">Vehicle Change Advance Details</b></center>
                                            <table class="table table-info mb30 table-hover" style="width:100%;"   id="bg" >
                                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size: 13px;">
                                                    <tr height="30">
                                                        <th width="30">Sno</td>
                                                        <th width="90">Advance Date</td>
                                                        <th width="90">Trip Day</td>
                                                        <th width="120">Estimated Advance</td>
                                                        <th width="120">Requested Advance</td>
                                                        <th width="90"> Type</td>
                                                        <th width="120">Requested By</td>
                                                        <th width="120">Requested Remarks</td>
                                                        <th width="120">Approved By</td>
                                                        <th width="120">Approved Remarks</td>
                                                        <th width="120">Paid Advance</td>
                                                    </tr>
                                                </thead>
                                                <%int index17=1;%>
                                                <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                    <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                    <%
                                                           String classText17 = "";
                                                           int oddEven17 = index17 % 2;
                                                           if (oddEven17 > 0) {
                                                               classText17 = "text1";
                                                           } else {
                                                               classText17 = "text2";
                                                           }
                                                    %>
                                                    <tr>
                                                        <td class="<%=classText17%>"><%=index7++%></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                        <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                            <td class="<%=classText17%>">Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                            <td class="<%=classText17%>">Batch</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                            <td class="<%=classText17%>">Manual</td>
                                                        </c:if>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                    </tr>
                                                </c:forEach>
                                                <tr>

                                                    <td class="text1" colspan="10" align="right"><b>Total Advance Paid </b>&emsp;</td>
                                                    <td class="text1" align="right"><b><c:out value="${totalVehicleChangeAdvancePaid}"/></b></td>
                                                </tr>
                                                <!--                                                                            <tr></tr>
                                                                                                                            <tr>

                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1">&nbsp;</td>
                                                                                                                                <td class="text1"></td>
                                                                                                                                <td class="text1"></td>
                                                                                                                                <td class="text1" colspan="2">Total Advance Paid</td>
                                                                                                                                <td class="text1"><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                                                                                                            </tr>-->
                                            </table>
                                        </c:if>
                                    </c:if>
                                </c:if>
                                <br/>
                                <br>
                                <c:if test="${tripDieselDetails != null}">
                                    <center><b style="font-size:13px;">Trip Diesel Details</b></center>
                                    <table class="table table-info mb30 table-hover" style="width:100%;"   id="bg" >
                                        <thead style="background-color:#5BC0DE;width:100%;color:white;font-size: 13px;">
                                            <tr height="30">
                                                <th width="30">Sno</td>
                                                <th width="90"> Date</td>
                                                <th width="90">Fuel(ltr)</td>
                                                <th width="120">Amount</td>
                                                <th width="120">Slip NO</td>
                                                <th width="120">Bunk Name</td>
                                                <th width="120">Given By</td>

                                            </tr>
                                        </thead>
                                        <%int index18=1;%>
                                        <c:forEach items="${tripDieselDetails}" var="tripDiesel">

                                            <%
                                                   String classText18 = "";
                                                   int oddEven18 = index18 % 2;
                                                   if (oddEven18 > 0) {
                                                       classText18 = "text1";
                                                   } else {
                                                       classText18 = "text2";
                                                   }
                                            %>
                                            <tr>
                                                <td class="<%=classText18%>"><%=index18++%></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.fuelDate}"/></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.dieselUsed}"/></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.dieselCost}"/></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.ticketNo}"/></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.bunkName}"/></td>
                                                <td class="<%=classText18%>"><c:out value="${tripDiesel.userName}"/></td>

                                            </tr>
                                        </c:forEach>


                                    </table>


                                </c:if>
                                <br>
                                <br>

                                <%if(count==1){%>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                <%}%>
                            </div>
                        </c:if>
                        <%count++;%>
                    </c:forEach>
                </c:if>
                <c:if test="${tripDetails != null}">
                    <%int count1=1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7  && tripDetails.statusId != 8  && tripDetails.statusId != 9 || tripDetails.statusId ==10 || tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16 || tripDetails.statusId ==18}">
                            <div id="startDetail" class="tab-pane">
                                <c:if test = "${tripStartDetails != null}" >
                                    <table class="table table-info mb30 table-hover" style="width:100%">
                                        <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                        <c:forEach items="${tripStartDetails}" var="startDetails">
                                            <thead>
                                            <th colspan="6" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Trip Start Details</th>
                                            </thead>
                                            <tr>
                                                <td class="text1" height="30" >Trip Planned Start Date</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                                <td class="text1" height="30" >Trip Planned Start Time</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                                <td class="text1" height="30" >Trip Start Reporting Date</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                            </tr>
                                            <tr>
                                                <td class="text2" height="30" >Trip Start Reporting Time</td>
                                                <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                                <td class="text2" height="30" >Trip Loading date</td>
                                                <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                                <td class="text2" height="30" >Trip Loading Time</td>
                                                <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                            </tr>
                                            <tr >
                                                <td class="text1" height="30" >Trip Loading Temperature</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                                <td class="text1" height="30" >Trip Actual Start Date</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                                <td class="text1" height="30" >Trip Actual Start Time</td>
                                                <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                                <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                                <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                                <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                                <td class="text2" height="30" colspan="2" ></td>
                                            </tr>
                                        </c:forEach >
                                    </table>

                                    <c:if test = "${tripUnPackDetails != null}" >
                                        <table class="table table-info mb30 " align="left" style="width:100%">
                                            <!--<table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">-->
                                            <tr style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size: 13px;">
                                                <th width="20" align="center" height="30" >Sno</td>
                                                <th height="30" >Product/Article Code</td>
                                                <th height="30" >Product/Article Name </td>
                                                <th height="30" >Batch </td>
                                                <th height="30" ><font color='red'>*</font>No of Packages</td>
                                                <th height="30" ><font color='red'>*</font>Uom</td>
                                                <th height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                                <th height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                                <th height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                                <th height="30" ><font color='red'>*</font>Shortage</td>
                                            </tr>


                                            <%int i1=1;%>
                                            <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                <tr>
                                                    <td><%=i1%></td>
                                                    <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> "  readonly style="width:120px;height:20px;"/>
                                                    <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:120px;height:20px;"/>
                                                        <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>" />
                                                        <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                    </td>
                                                    <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:120px;height:20px;"  /></td>
                                                    <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:120px;height:20px;"/></td>
                                                </tr>
                                                <%i1++;%>
                                            </c:forEach>

                                            <br/>
                                            <br/>
                                            <br/>
                                        </table>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>

                                    </c:if>
                                    <br>
                                    <br>
                                </c:if>

                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>

                            </div>


                            <div id="bpclDetail" class="tab-pane" >
                                <% int index10 = 1;%>

                                <c:if test = "${bpclTransactionHistory == null}" >
                                    <center><font color="red">No Records Found</font></center>
                                    </c:if>
                                    <c:if test = "${bpclTransactionHistory != null}" >
                                        <c:set var="totalAmount" value="0"/>
                                    <table class="tableborder table-bordered mb30 table-hover" style="width:100%;" border="1"  id="bg" cellpadding="0" cellspacing="0">
                                        <thead style="background-color:#5BC0DE;width:100%;color:white;font-style:13px;">
                                            <tr height="30">
                                                <!--<table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                                <!--<tr>-->
                                                <th height="30" >S No</td>
                                                <th height="30" >Trip Code</td>
                                                <th height="30" >Vehicle No</td>
                                                <th height="30" >Transaction History Id</td>
                                                <th height="30" >BPCL Transaction Id</td>
                                                <th height="30" >BPCL Account Id</td>
                                                <th height="30" >Dealer Name</td>
                                                <th height="30" >Dealer City</td>
                                                <th height="30" >Transaction Date</td>
                                                <th height="30" >Accounting Date</td>
                                                <th height="30" >Transaction Type</td>
                                                <th height="30" >Currency</td>
                                                <th height="30" >Transaction Amount</td>
                                                <th height="30" >Volume Document No</td>
                                                <th height="30" >Amount Balance</td>
                                                <th height="30" >Petromiles Earned</td>
                                                <th height="30" >Odometer Reading</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
                                                <%
                                                            String classText10 = "";
                                                            int oddEven10 = index10 % 2;
                                                            if (oddEven10 > 0) {
                                                                classText10 = "text1";
                                                            } else {
                                                                classText10 = "text2";
                                                            }
                                                %>
                                                <tr>
                                                    <td class="<%=classText10%>" height="30" ><%=index10++%></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.tripCode}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.vehicleNo}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountId}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerName}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerCity}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionDate}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountingDate}" /></td>
                                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionType}" /></td>
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.currency}" /></td>
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amount}" /></td>
                                                    <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
                                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
                                                </tr>
                                            </c:forEach >

                                            <tr >
                                                <th height="30" style="text-align: right" colspan="12">Total Amount</td>
                                                <th height="30" style="text-align: right"  ><c:out value="${totalAmount}"/></td>
                                                <th height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                </c:if>
                                <br>
                                <br>
                                <%if(count1==1){%>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                <%}%>
                            </div>

                        </c:if>
                        <%count1++;%>
                    </c:forEach>
                    <center>


                    </center>
                </c:if>





                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  && tripDetails.statusId != 9 && tripDetails.statusId != 10 && tripDetails.statusId != 18 || tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16}">
                            <div id="endDetail" class="tab-pane">
                                <c:if test = "${tripEndDetails != null}" >
                                    <c:if test = "${tripEndDetails != null}" >
                                        <table class="tableborder table-bordered mb30 table-hover" style="width:100%">
                                            <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                            <c:forEach items="${tripEndDetails}" var="endDetails">
                                                <thead>
                                                <th colspan="6" style="background-color:#5BC0DE;width:100%;height:25px;color:white">Trip End Details</th>
                                                </thead>
                                                <tr>
                                                    <td class="text1" height="30" >Trip Planned End Date</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                                    <td class="text1" height="30" >Trip Planned End Time</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                                    <td class="text1" height="30" >Trip Actual Reporting Date</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text2" height="30" >Trip Actual Reporting Time</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                                    <td class="text2" height="30" >Trip Unloading Date</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                                    <td class="text2" height="30" >Trip Unloading Time</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text1" height="30" >Trip Unloading Temperature</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                                    <td class="text1" height="30" >Trip Actual End Date</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.endDate}" /></td>
                                                    <td class="text1" height="30" >Trip Actual End Time</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.endTime}" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text2" height="30" >Trip End Odometer Reading(KM)</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                                    <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                                    <td class="text2" height="30" >Total Odometer Reading(KM)</td>
                                                    <td class="text2" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text1" height="30" >Total Reefer Reading(HM)</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                                    <td class="text1" height="30" >Total Duration Hours</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                                    <td class="text1" height="30" >Total Days</td>
                                                    <td class="text1" height="30" ><c:out value="${endDetails.totalDays}" />

                                                </tr>

                                            </c:forEach >
                                        </table>
                                    </c:if>
                                    <c:if test = "${tripUnPackDetails != null}" >
                                        <table class="table table-info mb30" id="addTyres1">
                                            <tr  height="30">
                                                <td colspan="10"  align="center" height="30" >
                                            <center style="color:black;font-size: 14px;">
                                                Consignment Unloading Details
                                            </center>
                                            </td>
                                            </tr>
                                            <tr id="tableDesingTD" height="30">
                                                <td width="20" align="center" height="30" >Sno</td>
                                                <td height="30" >Product/Article Code</td>
                                                <td height="30" >Product/Article Name </td>
                                                <td height="30" >Batch </td>
                                                <td height="30" ><font color='red'>*</font>No of Packages</td>
                                                <td height="30" ><font color='red'>*</font>Uom</td>
                                                <td height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                                <td height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                                <td height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                                <td  height="30" ><font color='red'>*</font>Shortage</td>
                                            </tr>


                                            <%int i2=1;%>
                                            <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                <tr>
                                                    <td><%=i2%></td>
                                                    <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly style="width:120px;height:20px;"/></td>
                                                    <td><input type="text" name="loadedpackages" id="loadedpackages<%=i2%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:120px;height:20px;"/>
                                                        <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                        <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                    </td>
                                                    <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i2%>" onblur="computeShortage(<%=i2%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:120px;height:20px;"  /></td>
                                                    <td><input type="text" name="shortage" id="shortage<%=i2%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:120px;height:20px;"/></td>
                                                </tr>
                                                <%i2++;%>
                                            </c:forEach>

                                            <br/>
                                        </table>
                                        <br/>
                                        <br/>
                                    </c:if>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>

                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                        </c:if></c:forEach>
                        <center>


                        </center>

                </c:if>
                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  && tripDetails.statusId != 9 && tripDetails.statusId != 10 && tripDetails.statusId != 18 && tripDetails.statusId ==12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16}">
                            <div id="podDetail" class="tab-pane">
                                <c:if test="${viewPODDetails != null}">
                                    <table class="table table-info mb30" id="bg" >
                                        <thead>
                                        <tr>
                                            <th width="50" class="contenthead">S No&nbsp;</th>
                                            <th class="contenthead">City Name</th>
                                            <th class="contenthead">POD file Name</th>
                                            <th class="contenthead">LR Number</th>
                                            <th class="contenthead">POD Remarks</th>
                                        </tr>
                                        </thead>
                                        <% int index11 = 1;%>
                                        <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                            <%
                                               String classText3 = "";
                                               int oddEven11 = index11 % 2;
                                               if (oddEven11 > 0) {
                                                   classText3 = "text1";
                                               } else {
                                                   classText3 = "text2";
                                               }
                                            %>
                                            <tr>
                                                <td class="<%=classText3%>" ><%=index11++%></td>
                                                <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                                <td class="<%=classText3%>" >
                                                    <a onclick="viewPODFiles('<c:out value="${viewPODDetails.tripPodId}"/>')" href="#"><c:out value="${viewPODDetails.podFile}"/></a>
                                                </td>
                                                <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                                <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                            </tr>
                                        </c:forEach>

                                    </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                </c:if>
                                <script>
                                    function viewPODFiles(tripPodId) {
                                        window.open('/throttle/content/trip/displayBlobData.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                                    }
                                </script>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                        </c:if></c:forEach></c:if>
                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  && tripDetails.statusId != 9 && tripDetails.statusId != 10  && tripDetails.statusId != 18 && tripDetails.statusId !=12 && tripDetails.statusId !=13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16}">
                            <div id="settlementDetail" class="tab-pane">
                                <div id="printDiv">
                                    <script>



                                        function print()
                                        {
                                            var DocumentContainer = document.getElementById("printDiv");
                                            var WindowObject = window.open('', "TrackHistoryData",
                                                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                                            WindowObject.document.close();
                                            WindowObject.focus();
                                            WindowObject.print();
                                            WindowObject.close();
                                        }

                                    </script>
                                    <c:if test = "${settlementDetails != null}" >
                                        <table width="860" border="1" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                                            <c:forEach items="${settlementDetails}" var="settlementDetails">
                                                <tr>
                                                    <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">Freight Systems Private Limited - Delhi<br>
                                                <center><u>Driver Settlement Voucher</u></center>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp;</td>
                                                    <td  height="30" height="30" style="text-align:left;">Trip Start Date</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startDate}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">C Note No</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label id="cNoteNo"><c:out value="${cNotes}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Trip Start Time in IST</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startTime}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp</td>
                                                    <td  height="30" height="30" style="text-align:left;">Trip End Date</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripEndDate}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp</td>
                                                    <td  height="30" height="30" style="text-align:left;">Trip End Time in IST</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripEndTime}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Truck Number</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${vehicleNo}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Trip No</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripCode}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Driver 1</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${driverName1}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Rate/Km For Trip</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label id="ratePerKm"></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Driver 2</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${driverName2}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Diesel Reference Rate</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${fuelPrice}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Stop No 1</td>
                                                    <td  height="30" height="30" style="text-align:left;">Pickup</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${origin}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Stop No 2</td>
                                                    <td  height="30" height="30" style="text-align:left;">Drop</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${destination}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Odometer Starting</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startKm}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Reefer Hours Starting</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${startHm}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Odometer Ending</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${endKm}"/></label></td>
                                                    <td  height="30" height="30" style="text-align:left;">Reefer Hours Ending</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label><c:out value="${endHm}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;">Odometer Km</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.runKm}" /></td>
                                                    <td height="30" height="30" style="text-align:left;">Reefer Hours</td>
                                                    <td height="30" height="30" style="text-align:left;" ><c:out value="${settlementDetails.runHm}" /></td>
                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;" >Trip Settlement Date</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.fromDate}" /></td>
                                                    <td  height="30" height="30" style="text-align:left;">GPS  Km</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.gpsKm}"/></td>
                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;">Fuel Price</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.fuelPrice}" /></td>
                                                    <td height="30" height="30" style="text-align:left;" >Diesel Used</td>
                                                    <td height="30" height="30" style="text-align:left;" ><c:out value="${settlementDetails.fuelConsumed}" /></td>
                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;">RCM</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.estimatedExpense}"/></td>
                                                    <td  height="30" height="30" style="text-align:left;">Extra Expenses</td>
                                                    <td  height="30" height="30" style="text-align:left;"><label id="extraExpense"><c:out value="${settlementDetails.expenseValue}"/></label></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Actual Expense</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${estimatedExpense}"/>
                                                    </td>
                                                    <td  height="30" height="30" style="text-align:left;">Diesel (Vehicle/Reefer)</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.vehicleDieselUsed}"/>/<c:out value="${settlementDetails.reeferDieselUsed}"/></td>

                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Advance Paid</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.totalValue}"/>
                                                    </td>

                                                    <td  height="30" height="30" style="text-align:left;">Mis(Total Km*<c:out value="${miscValue}"/>*5%)</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.otherExpense}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Total Expenses</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.totalExpenses}" />
                                                    </td>
                                                    <td  height="30" height="30" style="text-align:left;">Bhatta</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.driverBatta}" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td  height="30" height="30" style="text-align:left;">Balance</td>
                                                    <td  height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.tripBalance}" />
                                                    <td height="30" height="30" style="text-align:left;">BPCL Transaction</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.actualAdvancePaid}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;">Ending Balance</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${settlementDetails.tripEndBalance}" /></td>
                                                    <td height="30" height="30" style="text-align:left;">Uncleared Balance</td>
                                                    <td height="30" height="30" style="text-align:left;"><c:out value="${tripUnclearedBalance}" /></td>

                                                </tr>
                                                <tr>
                                                    <td height="30" height="30" style="text-align:left;" >Payment Mode</td>
                                                    <td height="30" height="30" style="text-align:left;" ><c:out value="${settlementDetails.paymentMode}" /></td>
                                                    <td height="30" height="30" style="text-align:left;" >Remarks</td>
                                                    <td height="30" height="30" style="text-align:left;" ><c:out value="${settlementDetails.tripRemarks}" /></td>
                                                </tr>
                                                <tr>
                                                    <td height="90" colspan="2" valign="bottom">Prepared By</td>
                                                    <td height="90" colspan="2" valign="bottom">Passed By</td>
                                                </tr>

                                            </c:forEach >
                                        </table>
                                        <br/>
                                        <br/>
                                        <center>


                                        </center>
                                    </c:if>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </c:if></c:forEach></c:if>
                <c:if test="${tripDetails != null}">
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <c:if test="${tripDetails.statusId != 6 && tripDetails.statusId != 7 && tripDetails.statusId != 8  && tripDetails.statusId != 9 && tripDetails.statusId != 10   && tripDetails.statusId != 18 && tripDetails.statusId !=12 || tripDetails.statusId ==13 || tripDetails.statusId ==14 || tripDetails.statusId ==15 || tripDetails.statusId ==16}">
                            <div id="expenseDetail" class="tab-pane">
                                <table  class="table table-info mb30 table-hover" id="bg">
                                    <c:if test="${tripExpenseDetails != null}">
                                        <!--                                                                <tr>
                                                                                                            <td  colspan="10" >Other Expense Details</td>
                                                                                                        </tr>-->
                                        <tr>
                                            <td class="text1" colspan="10" ><center style="color:black;font-size: 14px;">
                                            Other Expense Details
                                        </center> </td>
                                        </tr>
                                        <tr id="tableDesingTD" height="30">
                                            <td >S.No</td>
                                            <td >Expense Name</td>
                                            <td >Expense By</td>
                                            <td >Expense Date</td>
                                            <td >Expense Type</td>
                                            <td >Bill Mode</td>
                                            <td >Margin Value</td>
                                            <td >TAX</td>
                                            <td >Expense Value</td>
                                            <td >Total Expense</td>
                                            <td >Remarks</td>
                                        </tr>
                                        <% int index9 = 1; %>
                                        <c:forEach items="${tripExpenseDetails}" var="tripExp">
                                            <%
                                                        String classText9 = "";
                                                        int oddEven8 = index9 % 2;
                                                        if (oddEven8 > 0) {
                                                            classText9 = "text1";
                                                        } else {
                                                            classText9 = "text2";
                                                        }
                                            %>
                                            <tr>
                                                <td class="<%=classText9%>"><%=index9++%></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.expenseName}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.empName}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.expenseDate}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.expenseType}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.passThroughStatus}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.marginValue}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.applicableTaxPercentage}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.expenseValue}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.totalExpenseValue}"/></td>
                                                <td class="<%=classText9%>"><c:out value="${tripExp.expenseRemarks}"/></td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br/>
                                <br/>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                        </c:if></c:forEach>
                        <center>


                        </center>
                </c:if>




                <script>
                    //                    $(".nexttab").click(function() {
                    //                        var selected = $("#tabs").tabs("option", "selected");
                    //                        $("#tabs").tabs("option", "selected", selected + 1);
                    //                    });
                    $('.btnNext').click(function() {
                        $('.nav-tabs > .active').next('li').find('a').trigger('click');
                    });
                    $('.btnPrevious').click(function() {
                        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                    });
                </script>

                </div>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
                </div>
                </div>
                </div>
                <%@ include file="/content/common/NewDesign/settings.jsp" %>