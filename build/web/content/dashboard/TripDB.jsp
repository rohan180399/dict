<%--
    Document   : StoresDB
    Created on : Feb 3, 2012, 1:01:43 PM
    Author     : Administrator
--%>

<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<html lang="en">
    <head>
        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="css/jquery.ui.theme.css">
        <script src="js/jquery-1.4.4.js"></script>
        <script src="js/jquery.ui.core.js"></script>
        <script src="js/jquery.ui.widget.js"></script>
        <script src="js/jquery.ui.mouse.js"></script>
        <script src="js/jquery.ui.sortable.js"></script>
        <style type="text/css">
            .link {
                font: normal 12px Arial;
                text-transform:uppercase;
                padding-left:10px;
                font-weight:bold;
            }

            .link a  {
                color:#7f8ba5;
                text-decoration:none;
            }

            .link a:hover {
                color:#7f8ba5;
                text-decoration:underline;

            }

        </style>
        <style type="text/css">
            #expand {
                width:140%;
            }
            .column { width: 435px; float: left; }
            .portlet { margin: 0 1em 1em 0; }
            .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
            .portlet-header .ui-icon { float: right; }
            .portlet-content { padding: 0.4em; }
            .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
            .ui-sortable-placeholder * { visibility: hidden; }
        </style>
        <script>
            $(function() {
                $( ".column" ).sortable({
                    connectWith: ".column"
                });

                $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
                .end()
                .find( ".portlet-content" );

                $( ".portlet-header .ui-icon" ).click(function() {
                    $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
                    $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
                });

                $( ".column" ).disableSelection();
            });
            
            function myJS(myVar){
                window.alert(myVar);
            }
        </script>
    </head>
    <body>
        <!--Dynamic Portion-->
        <%
            Connection conn = null;
            int count = 0;
            try{

                String fileName = "jdbc_url.properties";
                Properties dbProps = new Properties();
            //The forward slash "/" in front of in_filename will ensure that
            //no package names are prepended to the filename when the Classloader
            //search for the file in the classpath

            InputStream is = getClass().getResourceAsStream("/"+fileName);
            dbProps.load(is);//this may throw IOException
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");

            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");


            String itemCode = request.getParameter("itemCode");

            DecimalFormat df2 = new DecimalFormat("0.00");
            Class.forName(dbClassName).newInstance();
            conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

            String curMonthName = "SELECT CONCAT(MONTHNAME(CURDATE()),'-',YEAR(CURDATE())) as monthName" ;
            String ovAllTrips = "SELECT status_name, count(*),Status_Id as Total FROM  ts_trip_master a,  ts_status_master b  where trip_type = 1 "
                                + "and a.trip_Status_Id = b.Status_Id and a.trip_Status_Id != 7   group by trip_status_Id";

            String statusWiseTripDetailsCurMonth  = "SELECT status_name, count(*),Status_Id as Total FROM "
                                            +" ts_trip_master a, "
                                            +" ts_status_master b "
                                            +" where trip_type = 1 "
                                            +" and a.trip_Status_Id != 7 "
                                            +" and a.empty_trip_approval_status_Id != 3"
                                            +" and MONTH(a.trip_actual_start_date) =  month(now()) "
                                            +" and a.trip_Status_Id = b.Status_Id group by trip_status_Id ";
                                    

            String fcWiseTripDetailsCurMonth = "SELECT cm.Name, count(*) as tripCount FROM ts_trip_master tm, ts_trip_vehicle tv, papl_vehicle_reg_no reg, "
                                            +" papl_vehicle_operation_point vop, papl_company_master cm "
                                            + "WHERE tv.active_Ind = 'Y' and tm.Trip_Id=tv.Trip_Id "
                                            +" and tm.empty_trip_approval_status_Id != 3"
                                            +" and tm.trip_status_Id in(10,18)"
                                            +" and tv.vehicle_Id = reg.vehicle_Id and reg.active_Ind = 'Y' and tm.trip_type= 1  "
                                            +" and vop .operation_point_Id = cm.Comp_Id and vop.vehicle_Id = tv.vehicle_Id and vop.active_Ind = 'Y' and cm.comp_Id in (1024,1025,1027) "
                                            +" and MONTH(tm.trip_actual_start_date) =  month(now()) group by cm.Comp_Id";

           String routeTrips = "select RouteName,Total,Route_Id from ( SELECT concat(a.city_Name,' - ', b.City_Name) as RouteName, count(*) as Total,rm.Route_Id FROM "
                               +" ts_trip_master ts, ts_route_master rm, ts_cities a, ts_cities b "
                               +" where "
                               +" ts.origin_Id=rm.Route_From_Id and ts.destination_Id = rm.Route_To_Id "
                               +" and rm.Route_From_Id = a.city_Id and rm.Route_To_Id = b.City_Id "
                               +" and ts.trip_type = 1 "
                               +" and ts.empty_trip_approval_status_Id != 3"
                               + " and MONTH(ts.trip_actual_start_date) =  month(now()) "
                               +" group by rm.Route_Id) as q "
                               +"where 1=1 order by Total desc limit 15 ";

           String serviceCostMfrMTD = "SELECT mfr_name,sum(nett_amount) as amt FROM papl_direct_jobcard dj, papl_jobcard_bill_master jb, papl_vehicle_master vm, "
                                     + " papl_mfr_master mf, papl_jobcard_master jm WHERE mfr_name = ? AND dj.job_card_id = jb.job_card_id AND vm.vehicle_id = dj.vehicle_id "
                                     + "AND mf.mfr_id = vm.mfr_id AND jm.job_card_id = dj.job_card_id AND "
                                     + "date_format(jm.closed_on,'%m-%Y') =  date_format(now(),'%m-%Y') AND mf.active_ind = 'Y' AND vm.active_ind = 'Y' "
                                     + "AND dj.active_ind = 'Y' AND jm.active_ind = 'Y' GROUP BY mfr_name";

          String topCustomer="select customerName,count(trip_id),sum(estimated_revenue) as totalRevenue"
                                   +"from ts_trip_master"
                                   +"where MONTH(trip_actual_start_date) =  month(now())"
                                   +"group by customername"
                                  +"order by totalRevenue desc limit 10";


            String status = "", total = "0", statusId = "0";
            String monthName = "";
            PreparedStatement pstmMonthName = conn.prepareStatement(curMonthName);
            PreparedStatement pstmOvAllTrips = conn.prepareStatement(ovAllTrips);
            PreparedStatement pstmFcWiseTripDetailsCurMonth = conn.prepareStatement(fcWiseTripDetailsCurMonth);
            PreparedStatement pstmStatusWiseTripDetailsCurMonth = conn.prepareStatement(statusWiseTripDetailsCurMonth);
            PreparedStatement pstmRouteTrips = conn.prepareStatement(routeTrips);
            PreparedStatement pstmMfrScMtd = conn.prepareStatement(serviceCostMfrMTD);
            PreparedStatement pstmTopCust = conn.prepareStatement(topCustomer);
            
            String ovAllTripsXML = "";
            String link = "";
            String topCustomerXML="";
            ResultSet resMonth = pstmMonthName.executeQuery();
            if(resMonth.next()) {
                 monthName = resMonth.getString(1);
            }
            ResultSet res = pstmOvAllTrips.executeQuery();
            while(res.next()) {
                 status = res.getString(1);
                 total = res.getString(2);
                 statusId = res.getString(3);
                 link = "P-detailsWin,width=600, height=600, toolbar=no, scrollbars=yes, resizable=yes-/throttle/viewTripSheets.do?statusId=" + statusId;
                 ovAllTripsXML = ovAllTripsXML+" <set value='"+total+"' link='" + link + "' alpha='60' />";

            }

            res = pstmFcWiseTripDetailsCurMonth.executeQuery();
            String curStatus="", vehTotal="", tripVehStatusXML="",tripVehStatusURL = "";
            while(res.next()) {
                curStatus = res.getString(1);
                vehTotal = res.getString(2);
                link = "P-detailsWin,width=600, height=600, toolbar=no, scrollbars=yes, resizable=yes-/throttle/viewTripSheets.do?statusId=" + statusId;
                tripVehStatusXML = tripVehStatusXML+ "<set label='"+curStatus+"' link='" + link + "' value='"+vehTotal+"' />";
            }
             res = pstmStatusWiseTripDetailsCurMonth.executeQuery();
             String custStatus="", tTotal="", tripCustStatusXML="",tripCustStatusURL="";
             statusId = "0";
             while(res.next()) {
                custStatus = res.getString(1);
                tTotal = res.getString(2);
                statusId = res.getString(3);
                link = "P-detailsWin,width=600, height=600, toolbar=no, scrollbars=yes, resizable=yes-/throttle/viewTripSheets.do?statusId=" + statusId;
                tripCustStatusXML = tripCustStatusXML+ "<set label='"+custStatus+"' link='" + link + "' value='"+tTotal+"' />";
             }
          
          


             ArrayList mrfList = new ArrayList();
             ArrayList ytdList = new ArrayList();
             ArrayList mtdList = new ArrayList();
             String category = "";
             String dataSet1 = "";
             String dataSet2 = "";
             // mfr wise YTD
           //  out.print(pstmMfrScYtd);
             res = pstmRouteTrips.executeQuery();
             String routeStatus="", rTotal="", tripRouteStatusXML="", routeId = "0",tripRouteStatusURL="";
             while(res.next()) {
               routeStatus = res.getString(1);
                rTotal = res.getString(2);
                routeId = res.getString(3);
                link = "P-detailsWin,width=600, height=600, toolbar=no, scrollbars=yes, resizable=yes-/throttle/viewTripSheets.do?statusId=" + statusId;
                tripRouteStatusXML = tripRouteStatusXML+ "<set label='"+routeStatus+"' value='"+rTotal+"' />";
             }
             category = "<categories>"+category+"</categories>";
             dataSet1 = "<dataset seriesName='YTD' color='3EA99F' showValues='0'>"+dataSet1+"</dataset>";

             //mfr wise month to date
             String mfrName = "";
             double cost = 0;
               if(mrfList != null) {
                for(int i = 0; i < mrfList.size(); i++)     {
                    mfrName = "";
                    cost = 0;
                    pstmMfrScMtd.setString(1, mrfList.get(i).toString());

                    res = pstmMfrScMtd.executeQuery();
                    while(res.next()) {
                        mfrName = res.getString("mfr_name");
                        cost = res.getDouble("amt");
                    }
                    if(mfrName != null && mfrName.length() > 0){
                         dataSet2 = dataSet2 + " <set value='"+cost+"' /> ";
                    } else{
                        dataSet2 = dataSet2 + " <set value='0' /> ";
                    }
                 }
            }
             dataSet2 = "<dataset seriesName='MTD' color='F87431' showValues='0'>"+dataSet2+"</dataset>";             
        %>




        <%
        // used dash board codes
                    ovAllTripsXML = "<chart caption='' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' "
                            + "isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>"+ovAllTripsXML+"</chart>";

                    tripVehStatusXML = "<chart caption='' bgColor='FFFFFF,CCCCCC' showPercentageValues='0' plotBorderColor='FFFFFF' numberPrefix='' "
                            + "isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'> "+tripVehStatusXML+ "</chart> ";

                    tripCustStatusXML = "<chart yAxisName='Trips' caption='Status' numberPrefix=' ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' "
                            + "showBorder='0'> "+tripCustStatusXML+ "</chart> ";
                    
                    tripRouteStatusXML = "<chart yAxisName='Trips' caption='Route' numberPrefix=' ' useRoundEdges='1' "
                            + "bgColor='FFFFFF,FFFFFF' showBorder='0'> "+tripRouteStatusXML+ "</chart> ";
                
        %>

        <div id="expand">
            <div class="column" style="width:550px;">
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;">&nbsp;&nbsp;Over All Trip Details</div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                        <jsp:param name="strURL" value="<%=link%>" />
                                        <jsp:param name="strXML" value="<%=ovAllTripsXML%>" />
                                        <jsp:param name="chartId" value="productSales" />
                                        <jsp:param name="chartWidth" value="500" />
                                        <jsp:param name="chartHeight" value="250" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;">&nbsp;&nbsp;FC Wise Trip In-Progress/WFU Details For <%=monthName%></div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Pie2D.swf" />
                                        <jsp:param name="strURL" value="" />
                                        <jsp:param name="strXML" value="<%=tripVehStatusXML%>" />
                                        <jsp:param name="chartId" value="productSales" />
                                        <jsp:param name="chartWidth" value="500" />
                                        <jsp:param name="chartHeight" value="250" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="column" style="width:550px;">
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;">&nbsp;&nbsp;Status Wise Trip Details For <%=monthName%> </div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                        <jsp:param name="strURL" value="" />
                                        <jsp:param name="strXML" value="<%=tripCustStatusXML%>" />
                                        <jsp:param name="chartId" value="productSales" />
                                        <jsp:param name="chartWidth" value="400" />
                                        <jsp:param name="chartHeight" value="250" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;">&nbsp;&nbsp;Route Wise Trip Details For <%=monthName%></div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                        <jsp:param name="strURL" value="" />
                                        <jsp:param name="strXML" value="<%=tripRouteStatusXML%>" />
                                        <jsp:param name="chartId" value="productSales" />
                                        <jsp:param name="chartWidth" value="520" />
                                        <jsp:param name="chartHeight" value="250" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End demo -->
        <%         
            if(res != null) {
                res.close();
            }

        }catch (FileNotFoundException fne){
            System.out.println("File Not found "+fne.getMessage());
        } catch (SQLException se){
            System.out.println("SQL Exception "+se.getMessage());
        }finally{
            if(conn == null) {
                conn.close();
            }
        }

        %>
    </body>
</html>
