<%-- 
    Document   : secondaryCustomerApprovalList
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.update.action = '/throttle/saveSecondaryCustomerApprovalMail.do';
            document.update.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>    
    <body>
        <form name="update" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
   
            <br>
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${secondaryCustomerApprovalList != null}">
                <table align="center">
                    <tr>
                        <td class="contenthead">S No</td>
                        <td class="contenthead">Customer Code</td>
                        <td class="contenthead">Customer Name</td>
                        <td class="contenthead">Approval Mail Id</td>
                    </tr>
                    <c:forEach items="${secondaryCustomerApprovalList}" var="mail">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green">
                                        <input type="hidden" name="customerId" id="customerId" value="<c:out value="${mail.customerId}"/>" />
                                        <input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${mail.customerCode}"/>" /><c:out value="${mail.customerCode}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${mail.customerName}"/>" /><c:out value="${mail.customerName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="text" name="approvalMailId" id="approvalMailId" value="<c:out value="${mail.emailId}"/>" style="width: 250px" /></font></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>