<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
      <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
     
    <body>

        <form name="tripSheet" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripSheetDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>
        
      

            <br>
            <br>
            <br>

<br><br>
            
            <br>
            <br>
              <c:if test="${tripDetails != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="50">
                        <th><h3>Sno</h3></th>
                      <!--  <th><h3>Mail</h3></th> -->
                        <c:if test="${tripType == 1}">
                            <th><h3>POD</h3></th>
                        </c:if>
                      <!--  <th><h3>Alert on Trip</h3></th> -->
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Trip Code</h3></th>
                       <!-- <th><h3>Reefer </h3></th>  -->
                        <th><h3>Fleet Center with Contact</h3></th>
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Customer Type </h3></th>
                       <!-- <th><h3>Billing Type </h3></th>  -->
                        <th><h3>Route </h3></th>
                        <th><h3>Driver Name with Contact </h3></th>
                        <th><h3>Vehicle Type </h3></th>
                       <!-- <th><h3>Trip Schedule</h3></th>
                        <th ><h3>Expected Arrival Date and time</h3></th> -->
                        <th><h3>Trip Status</h3></th>
                    <!--    <th><h3>C-Note Edit</h3></th>   
                        <th><h3>select</h3></th>
                        <c:if test="${roleId == '1023'}">
                            <th><h3>Admin Action</h3></th>
                        </c:if>
                        <th><h3>Link To Map</h3></th>  -->
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=className%>" width="40" align="left">
                                <%=index%>
                            </td>
                     <!--       <td class="<%=className%>" width="40" align="left">
                                <img src="images/mailSendingImage.jpg" alt="Mail Alert"   title="Mail Alert" style="width: 30px;height: 30px" onclick="mailTriggerPage('<c:out value="${tripDetails.tripSheetId}"/>');"/>
                            </td>  -->
                         <c:if test="${tripType == 1}">
                                <td class="<%=className%>" align="center">
                                    <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                        <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                            <c:if test="${tripDetails.podCount == '0'}">
                                              <font color="red">       Pod Pending </font>
                                                </c:if>
                                                <c:if test="${tripDetails.podCount != '0'}">
                                                    <font color="green">       Pod uploaded
                                                    </font>
                                                </c:if>
                                            </c:if>
                                        </c:if>
                                </td>
                            </c:if>   
                    <!--        <td class="<%=className%>" align="center" width="120">
                                <c:if test="${tripDetails.currentTemperature < '-10'  && tripDetails.currentTemperature > '-18' }">
                                    <img src="images/reefer_green.png" alt="reefer temp deviation"   title="reefer temp deviation"/>
                                </c:if>
                                <c:if test="${tripDetails.currentTemperature > '-10'  && tripDetails.currentTemperature < '-18' }">
                                    <img src="images/reefer_green.png" alt="reefer temp is within specified limit"   title="reefer temp is within specified limit"/>
                                </c:if>


                            </td>  -->
                            <td class="<%=className%>" width="120">
                                <c:out value="${tripDetails.vehicleNo}"/><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font></td>
                            <td class="<%=className%>" width="120" >
                                <c:out value="${tripDetails.tripCode}"/>
                            </td>
                         <!--   <td class="<%=className%>" align="center">
                                <c:if test="${tripDetails.reeferRequired == 'YES' || tripDetails.reeferRequired == 'Yes' }">
                                    <img src="images/ActiveRefer.png" alt="Yes"   title="Yes"/>
                                </c:if>
                                <c:if test="${tripDetails.reeferRequired == 'NO' || tripDetails.reeferRequired == 'No' }">
                                    <img src="images/DeactiveRefer.png" alt="No"   title="No"/>
                                </c:if>
                            </td>  -->
                            <td class="<%=className%>" width="150"><c:out value="${tripDetails.fleetCenterName}"/><br/><c:out value="${tripDetails.fleetCenterNo}"/></td>


                            <td  align="center" class="<%=className%>" width="150">
                                <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                    <c:out value="${tripDetails.customerName}"/>
                                </c:if>
                               <!-- <c:if test="${tripDetails.customerName == 'Empty Trip'}">
                                    <img src="images/emptytrip.png" alt="EmptyTrip"   title="Empty Trip"/>
                                </c:if></td>  -->

                           <!-- <td class="<%=className%>" align="center">
                                <c:if test="${tripDetails.customerType == 'Contract'}">
                                    <img src="images/contract.png" alt="Y"   title="contract customer"/>
                                </c:if>
                                <c:if test="${tripDetails.customerType == 'Walk In'}">
                                    <img src="images/walkin.gif" alt="Y"  title="walkin customer"/>
                                </c:if></td>  -->
                            <td class="<%=className%>" ><c:out value="${tripDetails.billingType}"/></td>
                            <td class="<%=className%>" >                                
                                <c:if test="${tripDetails.routeNameStatus > '2'}">
                                    <c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                                <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                    <c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                            </td>  

                            <td class="<%=className%>" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                                        <td class="<%=className%>" >
                                <c:if test="${tripDetails.extraExpenseStatus == 0}">
                                <c:out value="${tripDetails.status}"/>
                                </c:if>
                                <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                    <c:if test="${tripDetails.statusId == 14}">
                                    <c:out value="${tripDetails.status}"/>
                                    </c:if>
                                    <c:if test="${tripDetails.statusId != 14}">
                                    FC Closure
                                    </c:if>
                                </c:if>
                            </td>


                           <!-- <c:if test="${roleId != '1030'}">

                                <td class="<%=className%>" >
                                    <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo != 'Empty Trip' && roleId != '1032' }">
                                        <a href="getConsignmentDetailsForEditAfterTripStart.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>"><c:out value="${tripDetails.cNotes}"/></a>
                                    </c:if>
                                    <c:if test="${tripDetails.statusId >= 7 && tripDetails.statusId <= 10 && tripDetails.customerOrderReferenceNo == 'Empty Trip'}">
                                        &nbsp;
                                    </c:if>
                                </td>
                                <td class="<%=className%>" >

                                    <!--   <c:if test="${tripDetails.statusId == 8}">
                                           <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>">UnFreeze</a>
                                           &nbsp;<a href="viewPreTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>">PreStart</a>
                                    </c:if> -->
                 <!--                   <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                        Empty Trip Waiting For Approval
                                    </c:if>

                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                        <c:if test="${tripDetails.statusId == 6}">
                                            <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">AllotVehicle</a>


                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 7}">
                                            <c:if test="${RoleId != 1033}">
                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>
                                            </c:if>
                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 8}">
                                            <c:if test="${RoleId != 1033}">
                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">UnFreeze</a>
                                            </c:if>
                                            <c:if test="${tripDetails.tripCountStatus == 0}">
                                                &nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                            </c:if>
                                            <c:if test="${tripDetails.tripCountStatus == 1}">
                                                &nbsp;Started
                                            </c:if>
                                            <c:if test="${RoleId == 1023 || RoleId == 1033}">

                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                    </c:if>
                                                </c:if>


                                            </c:if>

                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 10}">
                                            <c:if test="${tripDetails.routeNameStatus > '2'}">
                                                <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Loading & Unloading</a>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 10}">
                                            <c:if test="${tripDetails.emptyTrip == 0}">
                                                <a href="viewWFUTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">WFU</a>
                                                &nbsp;
                                                <a href="viewTripSheetForVehicleChange.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">ChangeVehicle</a>
                                                &nbsp;
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                            </c:if>
                                          <%--  <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&freightAmount=<c:out value="${tripDetails.orderRevenue}"/>&paymentType=<c:out value="${tripDetails.paymentType}"/>&nextPage=2'>Proceed To Pay</a>
                                            </c:if> --%>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <%-- <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&freightAmount=<c:out value="${tripDetails.orderRevenue}"/>&paymentType=<c:out value="${tripDetails.paymentType}"/>&nextPage=2'>Proceed To Pay</a>
                                            </c:if> --%>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            &nbsp;

                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032}">  

                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                    </c:if>
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                        <b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                </c:if>
                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                        &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                        <b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                    </c:if>
                                                </c:if>

                                                <c:if test="${tripDetails.tripType != 2}">
                                                    <a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                                </c:if>


                                            </c:if>
                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 18}">
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 1}">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                &nbsp;
                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                            </c:if>
                                            &nbsp;
                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                <a href="manualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${tripDetails.statusId == 12 && tripDetails.oldVehicleNo == null}">
                                            <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">closure</a>
                                            &nbsp;
                                            <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                            </c:if>

                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 12 && tripDetails.oldVehicleNo != null}">
                                            <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">closure</a>
                                            &nbsp;
                                            <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                            </c:if>

                                        </c:if>
                                        <c:if test="${tripDetails.statusId == 13}">
                                            <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>
                                        </c:if>
                                    </c:if>
                                </td>
                            </c:if>
                            <c:if test="${roleId == '1030'}">
                                <td class="<%=className%>" width="150" align="left">&nbsp;</td>
                                <td class="<%=className%>" width="150" align="left">&nbsp;</td>
                            </c:if>
                            <c:if test="${roleId == '1023'}">
                                <td class="<%=className%>" width="450" align="left">
                                    <c:if test="${tripDetails.customerName == 'Empty Trip' && tripDetails.emptyTrip < 3 && tripDetails.emptyTrip > 0 && tripDetails.statusId <= 12 && tripType == 1}">
                                        <a href="changeEmptyTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Change Route</a>
                                        &nbsp;
                                    </c:if>
                                    <c:if test="${tripDetails.statusId > 8 && tripDetails.statusId <= 18}">
                                        <a href="handleDeleteTripSheet.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripDetails.tripType}"/>&statusId=<c:out value="${statusId}"/>&tripStatusId=<c:out value="${tripDetails.statusId}"/>">Update Trip</a>
                                        &nbsp;
                                    </c:if>
                                </td>
                            </c:if>
                            <td class="<%=className%>" width="450" align="left">
                                <c:if test="${tripDetails.statusId <= 10}">
                                    <c:if test="${tripDetails.gpsLocation == 'NA'}">
                                        NA
                                    </c:if>
                                    <c:if test="${tripDetails.gpsLocation != 'NA'}">
                                        <a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.vehicleNo}" />');"><c:out value="${tripDetails.gpsLocation}" /></a>
                                    </c:if>
                                </c:if>
                            </td>




                        </tr>   -->

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>