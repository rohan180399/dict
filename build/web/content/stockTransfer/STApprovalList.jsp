<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>    
 <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>       
</head>
<script>
    
    function submitPage(value){              
       requestPtId=document.stockTransfer.servicePoint.value; 
        if(document.stockTransfer.servicePoint.value==0){
            alert("Please Select Service Point name");
             document.stockTransfer.servicePoint.focus();
             return;
             }
        document.stockTransfer.action='/throttle/request.do?servicePoint='+value;
        document.stockTransfer.submit();
        }        
function ApproveStock(reqId)
{    
    document.stockTransfer.requestId.value=reqId;
    url = '/throttle/approvalScreen.do';        
    document.stockTransfer.action=url;
    document.stockTransfer.submit();    
}    
    
function setRequestPtId(){  
    document.stockTransfer.servicePoint.focus();
    if('<%= request.getAttribute("requestPtId") %>' != 'null'){
        document.stockTransfer.servicePoint.value = '<%= request.getAttribute("requestPtId") %>';
    }    
}    
</script>
    
    <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
    
    
<body onLoad="setRequestPtId();">
<form name="stockTransfer"  method="post" >

<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table align="center" width="390" border="0" cellspacing="0" cellpadding="0" class="border">
    <tr>
    <td class="text1" height="30"><spring:message code="stores.label.FromServicePoint"  text="default text"/>
</td>  
    <td class="text1"><select class="form-control"  name="servicePoint" style="width:125px">
    <option>-Select-</option>  
     <c:if test = "${servicePointList != null}" >
    <c:forEach items="${servicePointList}" var="company"> 
    <option value="<c:out value="${company.companyId}"/>"><c:out value="${company.companyName}"/></option>     
    </c:forEach>  
</c:if>  
</select></td>
<td class="text1">
<input type="button" align="center" name="search" class="button" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onclick="submitPage(servicePoint.value);"></td>
 </tr>
</table>
<br>
    <br>
         <% int index = 0;%>
        <c:if test = "${requestList != null}" >
          
    <table align="center" width="400" border="0" cellspacing="0" cellpadding="0" class="border">

<tr>
<td class="contentsub" align="left" height="30"><div class="contentsub"><spring:message code="stores.label.RequestNo"  text="default text"/></div></td>
<td class="contentsub" align="left" height="30"><div class="contentsub"><spring:message code="stores.label.ToServicePoint"  text="default text"/></div></td>
<td class="contentsub" align="left" height="30"><div class="contentsub"><spring:message code="stores.label.RequiredDate"  text="default text"/></div></td>
<td class="contentsub" align="left" height="30"><div class="contentsub"><spring:message code="stores.label.Approve"  text="default text"/></div></td>
</tr>
 
<c:forEach items="${requestList}" var="request"> 
      <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
<tr>
<td class="<%=classText %>" height="30"><c:out value="${request.requestId}"/></td>
<td class="<%=classText %>"  height="30"><c:out value="${request.companyName}"/></td>
<td class="<%=classText %>" height="30"> <c:out value="${request.requiredDate}"/> </td>
<td class="<%=classText %>" height="30"> <a href="" onClick="ApproveStock(<c:out value="${request.requestId}"/>);" >Approve</a></td>
</tr>
   <%
  index++;
  %>
</c:forEach>
<input type="hidden" name="requestId" value="" >
</table>
<br>
    <br>
</c:if>

<center>
</center>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>


</html>

