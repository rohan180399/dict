<%--
    Document   : generateCounterBill
    Created on : Jun 23, 2010, 7:04:28 PM
    Author     : Hari
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions1.js"></script>


        <%@ page import="ets.domain.mrs.business.MrsTO" %>
    </head>



    <SCRIPT>

        var httpRequest1;


        function getOthers(sno)
        {

            var temp=0;
            var mfrCode=eval("document.counterBill.mfrCode"+sno).value;
            var itemCode=eval("document.counterBill.itemCode"+sno).value;
            var itemName=eval("document.counterBill.itemName"+sno).value;
            if(mfrCode!=""){
                temp=eval("document.counterBill.mfrCode"+sno);
            }else if(itemCode!=""){
                temp=eval("document.counterBill.itemCode"+sno);
            }else if(itemName!=""){
                temp=eval("document.counterBill.itemName"+sno);
            }


            var url='/throttle/getQuandity.do?mfrCode='+mfrCode+'&itemCode='+itemCode+'&itemName='+itemName;
            if (window.ActiveXObject)
            {
                httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpRequest1 = new XMLHttpRequest();
            }
            httpRequest1.open("POST", url, true);
            httpRequest1.onreadystatechange = function() {
                go(sno,temp);
            } ;
            httpRequest1.send(null);
        }


        function go(sno,temp) {
            if (httpRequest1.readyState == 4) {
                if (httpRequest1.status == 200) {
                    var response = httpRequest1.responseText;
                    var uom=eval("document.counterBill.uom"+sno);
                    var stock=eval("document.counterBill.stock"+sno);
                    var itemId=eval("document.counterBill.itemId"+sno);
                    var mfrCode=eval("document.counterBill.mfrCode"+sno);
                    var itemCode=eval("document.counterBill.itemCode"+sno);
                    var itemName=eval("document.counterBill.itemName"+sno);
                    var qty=eval("document.counterBill.qty"+sno);

                    if(response!=""){
                        var details=response.split('~');


                        itemName.value=details[5];
                        itemCode.value=details[4];
                        mfrCode.value=details[3];
                        uom.value=details[2];
                        stock.value=details[1];
                        itemId.value=details[0];
                        qty.focus();

                        if(details[6]==1011){
                            qty.value=1;
                            document.getElementById("posId"+sno).style.visibility="visible";
                        }else{
                            qty.value="";
                            qty.focus();
                            document.getElementById("posId"+sno).style.visibility="hidden";
                        }


                    }else
                    {
                        alert("Invalid value");
                        temp.value="";
                        temp.focus();
                    }
                }
            }
        }



        function itemValidate()
        {
            var rows = document.getElementsByName('selectedindex');
            var qty = 0;
            var itemName = '';
            var cntr = 0;
            for(var i=1;i<rows.length+1;i++){
                if(eval("document.counterBill.itemId"+i).value!='' ){
                    cntr++;
                    if( eval("document.counterBill.itemName"+i).value ==''  ){
                        alert("Please Enter Item Name");
                        eval("document.counterBill.itemName"+i).focus();
                        return 'fail';
                    }
                    if( isFloat(eval("document.counterBill.qty"+i).value) ){
                        alert("Please Enter Correct Quantity");
                        eval("document.counterBill.qty"+i).focus();
                        eval("document.counterBill.qty"+i).select();
                        return 'fail';
                    }
                }
            }
            if(cntr==0){
                alert("Please enter any item");
                return 'fail'
            }
            return 'pass';
        }



        function clearFieldValues(val)
        {
            getItemNames(val);
            var uom=eval("document.counterBill.uom"+val);
            var stock=eval("document.counterBill.stock"+val);
            var itemId=eval("document.counterBill.itemId"+val);
            var mfrCode=eval("document.counterBill.mfrCode"+val);
            var itemCode=eval("document.counterBill.itemCode"+val);
            var itemName=eval("document.counterBill.itemName"+val);
            var qty=eval("document.counterBill.qty"+val);
            var selectedInd=eval("document.counterBill.selectedInd"+val);
            if(selectedInd.checked == true){
                itemName.value="";
                itemCode.value="";
                mfrCode.value="";
                uom.value="";
                stock.value="";
                itemId.value="";
                qty.value="";
                document.getElementById("posId"+val).style.visibility="hidden";
            }
        }







        var rowCount=2;
        var sno=0;
        var httpRequest;
        var httpReq;
        function addRow()
        {

            sno++;
            var tab = document.getElementById("items");
            var newrow = tab.insertRow(rowCount);
            var temp=sno-1;
            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='hidden' name='selectedindex' value='"+temp+"'><input type='hidden' name=hiddenElement1 value='itemId"+sno+"' ><input type='hidden'  name='itemId"+sno+"' > "+sno+"</td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell0;



            var cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><input name='mfrCode"+sno+"' class='form-control'   onchange='getOthers("+sno+")'  type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell0;

            cell = newrow.insertCell(2);
            var cell1 = "<td class='text1' height='25'><input name='itemCode"+sno+"' class='form-control'  onchange='getOthers("+sno+")'  type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(3);
            var cell2 = "<td class='text1' height='25'><input id='itemNames"+sno+"' name='itemName"+sno+"' value='' class='form-control'  onFocusOut='getOthers("+sno+")'  type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell2;

            cell = newrow.insertCell(4);
            var cell3 = " <td class='text1' height='25'><input name='uom"+sno+"' size='5' readonly class='form-control'  type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell3;

            cell = newrow.insertCell(5);
            var cell4 = " <td class='text1' height='25'><input type='hidden'  name=hiddenElement2 value='qty"+sno+"' ><input name='qty"+sno+"' size='5' onchange='setSelectbox(sno-1)'  class='form-control'  type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell4;

            cell = newrow.insertCell(6);
            var cell5 = " <td class='text1' height='25'><input name='stock"+sno+"' size='5' readonly class='form-control' value='' type='text'></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell5;

            cell = newrow.insertCell(7);
            var cell1 = "<td class='text1'   height='25'><input type='hidden' name=hiddenElement3 value='posId"+sno+"' ><select class='form-control'  style='visibility:hidden;' id='posId"+sno+"'  name='posId"+sno+"' ><option  selected value='0'>---Select---</option><c:if test = "${tyrePostions != null}" ><c:forEach items="${tyrePostions}" var="sec"><option  value='<c:out value="${sec.posId}" />'><c:out value="${sec.posName}" /></c:forEach ></c:if> </select></td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(8);
            var cell1 = "<td class='text1' height='25'><input type='checkbox' name='selectedInd"+sno+"' onClick='clearFieldValues("+sno+");' > </td>";
            cell.setAttribute("className","text2");
            cell.innerHTML = cell1;

            rowCount++;
            getItemNames(sno);
        }


        function setSelectbox(i)
        {

            var selected=document.getElementsByName("selectedindex") ;
            selected[i].checked = 1;
        }

        function selectedItemValidation(){

            var index = document.getElementsByName("selectedindex");

            var chec=0;
            var mess = "SubmitForm";
            var j=1;
            for(var i=0;(i<index.length && index.length!=0);i++){
                var qty =eval("document.counterBill.qty"+j);
                if(index[i].checked){
                    chec++;

                    if(numberValidation(qty,'Quantity')){
                        return 'notSubmit';
                    }
                }
                j++;
            }
            if(chec == 0){
                //alert("Please Select Any One And Then Proceed");
                //km[0].focus();
                return 'notSubmit';
            }
            return 'SubmitForm';
        }



        function generateCounterMRS()
        {
         
            if(isEmpty(document.counterBill.customerName.value)){
                alert("Enter Customer Name");
                document.counterBill.customerName.focus();
                return;
            }
            else if(isEmpty(document.counterBill.address.value)){
                alert("Enter Customer Address");
                document.counterBill.address.focus();
                return;
            }
            else if(isEmpty(document.counterBill.mobileNo.value)){
                alert("Enter Customer Mobile Number");
                document.counterBill.mobileNo.focus();
                return;
            }
            else if(document.counterBill.mobileNo.value.length < 10){
                alert("Enter Valid Customer Mobile No ")  ;
                document.counterBill.mobileNo.focus();
                return ;
            }
            else if(isDigit(document.counterBill.mobileNo.value)){
                alert("Enter Valid Customer Mobile No")  ;
                document.counterBill.mobileNo.focus();
                return ;
            }
            if(document.counterBill.date.value=="" ){
                alert("Please Enter Manual Counter Bill Date");
                document.counterBill.date.focus();
                return;
            }
            if(textValidation(document.counterBill.remarks,'Remarks')){
                document.counterBill.remarks.focus();
                return;
            }

            if(itemValidate()!='fail' ){
                if(confirm("Are you sure to submit")){
                    document.getElementById("buttonStyle").style.visibility = "hidden";
                    document.counterBill.action="/throttle/generateMrs.do?reqFor=Counter";
                    document.counterBill.submit();
                }
            }
        }

        function getItemNames(val){
            var itemTextBox = 'itemNames'+ val;
            var oTextbox = new AutoSuggestControl(document.getElementById(itemTextBox),new ListSuggestions(itemTextBox,"/throttle/handleItemSuggestions.do?"));
            //getVehicleDetails(document.getElementById("regno"));
        }



    </SCRIPT>


<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>

    <body onLoad="addRow();">
        <form name="counterBill" method="post">

            <%@ include file="/content/common/path.jsp" %>

            <!-- pointer table -->


            <%@ include file="/content/common/message.jsp"%>


            <%

                        int index = 0;
            %>


            <table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="550" class="border">
                <tbody><tr>
                        <td colspan="5" class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.GenerateCounter"  text="default text"/>
</div></td>
                    </tr>


                    <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text1";
                                } else {
                                    classText = "text2";
                                }
                    %>
                    <tr>
                        <td class="text1" height="30"><font color="red">*</font><spring:message code="stores.label.CustomerName"  text="default text"/>
</td>
                        <td class="text1"  height="30"><input type="text" name="customerName" class="form-control"S></td>
                        <td class="text1" height="30"><font color="red">*</font><spring:message code="stores.label.CustomerMobileNo"  text="default text"/>
</td>
                        <td class="text1"  height="30"><input type="text" name="mobileNo" class="form-control" maxlength="10"></td>

                    </tr>
                    <tr>

                        <td class="text2" height="30"><font color="red">*</font><spring:message code="stores.label.CustomerAddress"  text="default text"/>
</td>
                        <td class="text2"  height="30"><textarea name="address" class="form-control"></textarea></td>
                        <td class="text2" height="30"><font color="red">*</font><spring:message code="stores.label.CounterBillCreatedDate"  text="default text"/>
</td>
                        <td class="text2" height="30"><input class="form-control" type="text" readonly name="date" size="10">
                            <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.counterBill.date,'dd-mm-yyyy',this)"/></td>
                    </tr>


                <input name="mrsJobCardNumber" type="hidden" value='0'/>
                <input name="technicianId" type="hidden" value='0'/>


                </tbody></table>

            <br><br>


            <table id="items" align="center" border="0" cellpadding="0" cellspacing="0" width="650" class="border">

                <tbody><tr>
                        <td colspan="8" align="center" class="text2" height="30"><strong><spring:message code="stores.label.MaterialDetails"  text="default text"/></strong></td>
                    </tr>
                    <tr>
                        <td class="contentsub" height="30"><spring:message code="stores.label.SNo"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.MFRItemcode"  text="default text"/>  
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.ItemCode"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.ItemName"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.uom"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Quantity"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.StockAvailability"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.TyrePostion"  text="default text"/>
</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Clear"  text="default text"/>
</td>
                    </tr>
                    <tr>
                    </tr>


                </tbody></table>
            <br>
            <center><div class="text2" align="center" > <spring:message code="stores.label.RackNameRemarks"  text="default text"/>
 &nbsp;&nbsp;:&nbsp;&nbsp; <textarea class="form-control" name="remarks" rows="2" cols="15" ></textarea> </center>
            <br>
            <br>
            <br>
            <div id="buttonStyle" align="center" style="visibility:visible;" >
                <input value="<spring:message code="stores.label.ADDROW"  text="default text"/>" class="button" type="button" onClick="addRow();">
                <input type="button" value="<spring:message code="stores.label.GenerateCounter"  text="default text"/>" class="button" onClick="generateCounterMRS()">
            </div>
            <br>




        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>