<%-- 
    Document   : billingdetail
    Created on : Dec 10, 2013, 2:58:42 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script>
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function goback(){
                history.go(-1);
            }
        </script>
    </head>
    <body>
        <form name="viewbillGeneration" method="post">
            <div id="print">
                <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <h1 align="center">Invoice</h1>
                    <br>
                    <tr>
                        <td valign="top">
                            <h4>Freight Systems Pvt Ltd.</h4>
                            <h3><p>15/1, Asaf Ali Road</p>
                                <p>New Delhi</p>
                                <p>110002</p></h3>
                        </td>
                        <td>

                            <table width="100%" height="200" border="0">
                                <tr>
                                     <td width="50%">Invoice No :<b><c:out value="${operatioTo.invoiceCode}"/></b></td>
                                    <td>Dated :<c:out value="${operatioTo.createddate}"/></b></td>
                                </tr>
                                <tr>
                                    <td>Delivery Note:<b><c:out value="${operatioTo.invRefCode}"/></b></td>
                                    <td>Mode/Terms Of Pament</td>
                                </tr>
                                <tr>
                                    <td>Suppliers's Ref. <b><c:out value="${operatioTo.invRefCode}"/></b></td>
                                    <td>Other Reference(s)</td>
                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td valign="top">
                            <h3>Buyer</h3>
                            <h4><c:out value="${operatioTo.invoicecustomer}"/></h4>
                            <h3><p><c:out value="${operatioTo.custAddress}"/></p>

                        </td>
                        <td>
                            <table width="100%" height="200" border="0">
                                <tr>
                                    <td>Buyer's Order No :<b></b></td>
                                    <td>Dated :<b><c:out value="${operatioTo.createddate}"/></b></td>
                                </tr>
                                <tr>
                                    <td width="50%">Delivery Document No:<b>HR 55 Q 7013</b></td>
                                    <td>Dated :<b><c:out value="${operatioTo.createddate}"/></b></td>
                                </tr>
                                <tr>
                                    <td>Despatched through. <b>14086</b></td>
                                    <td>Destination :Mumbai <b></b></td>
                                </tr>

                            </table>
                        </td>


                    </tr>
                </table>

                <br>
                <br>
                    <c:if test="${invoicedetailList !=null}">
                <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" class="border" >
                    <tr>

                        <td  >S.No</td>
                        <td >Description Of Goods</td>
                        <td >No of Packages</td>
                        <td >Total Package Weight</td>
                        <td align='right' >Amount</td>

                    </tr>
                    <%int sno=0;%>
                    <c:forEach items="${invoicedetailList}" var="invoicedetailList">
                    <tr>
                        <td><%=sno++%></td>
                        <td><c:out value="${invoicedetailList.routeName}"/></td>
                        <td><c:out value="${invoicedetailList.totalPackage}"/></td>
                        <td><c:out value="${invoicedetailList.totalWeightage}"/></td>
                        <td align='right'><c:out value="${invoicedetailList.freightAmount}"/></td>
                    </tr>
                    </c:forEach>
                    <tr><td colspan="4" style="text-align: right;">Total Charges</td><td><c:out value="${operatioTo.grandTotal}"/></td>
                    </tr>

</table>
                    </c:if>

                <table width="95%" align="center" cellpadding="0" cellspacing="0" class="border">
                    <tr>
                        <td><lable>Amount Chargeable (in words)</lable><br>
                    <b>Rupees:</b><%
                    OperationTO grandTotal=(OperationTO)request.getAttribute("operatioTo");
                       String grandTot=grandTotal.getGrandTotal();
                            float spareTotal = 0.00F;
                             spareTotal = Float.parseFloat(grandTot);
                        %>
                        <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                            <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>
                            <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                            <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                            <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />Only</b>
                        </jsp:useBean>
                    <br>
                    <lable>Remarks:</lable><br>
                    <lable>Being Amount provided for the Freight charges from Rajpura-</lable><br>
                    <lable>Cochin vide vehicle no. HR 55 Q 7013/14086</lable><br>
                    <lable>Company service Tax No : <b>AABCL5755FSD002</b> </lable><br>
                    <lable>Company's PAN : <b>AABCL5755F</b> </lable><br>
                    <lable>Declaration: </lable><br>
                    <lable>Service Tax payable by Consignor / Consignee</lable><br>
                    <lable>This is to certify and confirm that we have not availed</lable><br>
                    <lable>Cenvat credit of duty on inputs and capital ggodsand</lable><br>
                    <lable>Providing transport service to you under the provision of </lable><br>
                    <lable>the cenvat credit rules 2004 and also have not availed</lable><br>
                    <lable>benifit of notification no. 12/2003 Service tax dated june</lable><br>
                    <lable>2003 (ide Notification no. 32/2004 Service tax dated on 03/12/2004)</lable><br>
                        </td>
                    </tr>
                    <tr><td align="right"> for Freight Systems Pvt Ltd.</td></tr>
                    <tr><td align="right"> Authorised .</td></tr>

</table>
            </div>
            <br>
            <br>

            <center>
                <input type="button" class="button"  value="Print" onClick="print('print');" >&nbsp;&nbsp;&nbsp;<input type="button" class="button"  value="Back" onClick="goback();" >
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>