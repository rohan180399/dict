<%--
    Document   : suppInvoicegenerate
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('customerName').value == '') {
            document.getElementById('customerId').value = "";
        }
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/grSuppInvoiceGenerator.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            } else {
                document.BPCLTransaction.action = '/throttle/grSuppInvoiceGenerator.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
</script>
<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('customerId').value

                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                $('#customerName').val(value);
//                        $('#consignorPhoneNo').val(ui.item.Mobile);
//                        $('#consignorAddress').val(ui.item.Address);
                $('#customerId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  GR Supp.Invoice Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Billing</a></li>
            <li class="active">GR Supplement Invoice Generator</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="BPCLTransaction" method="post">
                    <%@ include file="/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover" style="width: 50%">
                        <tr>
                            <td  height="30">Filter by</td> 
                            <td height="30">
                                <select name="reportTypeId" id="reportTypeId" style="height:35px;width:250px;" >
                                    <option value="1">GR</option>
                                    <option value="2">Invoice</option>
                                </select> 
                            </td>
                            <td>Party Name</td>
                            <td><input type="hidden" name="customerId" id="customerId" class="textbox" value="<c:out value="${customerId}"/>"/>
                                <input type="text" class="form-control" name="customerName"   id="customerName" value="<c:out value="${customerName}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:250px;"/></td>
                       
                        </tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="form-control datepicker" value="<c:out value="${fromDate}"/>" style="width:250px;"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="form-control datepicker" value="<c:out value="${toDate}"/>"  style="width: 250px;"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><input type="button" class="btn btn-info" name="search" onclick="submitPage(this.name);" value="Search"></td>
                            <td><input type="button" class="btn btn-info" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                            <td></td>
                        </tr>
                        <script>
                            document.getElementById("reportTypeId").value ='<c:out value="${reportTypeId}"/>';
                        </script>
                            
                    </table>
                    <br>
                    <br>
                    <c:if test="${invoiceReport != null}">
                        <table class="table table-info mb30 table-hover"  id="table" style="width:100%">

                            <thead>
                                <tr>
                                    <th align="center">S.No</th>
                                    <th align="center">GR No</th>
                                    <th align="center">GR Date</th>
                                    <th align="center">Movement Type</th>
                                    <th align="center">Route</th>
                                    <th align="center">Supplementary Invoice No</th>
                                    <th align="center">Supplementary Invoice Date</th>
                                    <th align="center">Billed Party Name</th>
                                    <th align="center">Container No</th>
                                    <th align="center">Container Size</th>
                                    <th align="center">Freight Amount</th>
                                    <th align="center">Detention Amount</th>
                                    <th align="center">Toll Tax</th>
                                    <th align="center">Green Tax</th>
                                    <th align="center">Weighment</th>
                                    <th align="center">other Expense</th>
                                    <th align="center">Total Amount</th>
                                    <th align="center">Creation Date</th>
                                    <th align="center">Creator Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>

                                <c:forEach items="${invoiceReport}" var="Invoice">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr>
                                        <td class="<%=classText%>"><%=index%></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.grNo}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.grDate}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.movementType}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.routeInfo}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${Invoice.invoiceCode}"/>
                                        <td width="30" class="<%=classText%>"><c:out value="${Invoice.invoiceDate}"/></td>
                                        <td width="30" class="<%=classText%>" ><c:out value="${Invoice.billedPartyname}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.containerNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.containerSize}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.frieghtAmount}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.detentionAmount}"/>
                                        </td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.tollAmount}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.greenTaxAmount}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.weightMent}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.otherExpense}"/></td>


<td width="30" class="<%=classText%>"  ><c:out value="${Invoice.otherExpense + Invoice.tollAmount + Invoice.detentionAmount + Invoice.frieghtAmount +Invoice.greenTaxAmount +Invoice.weightMent}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.createdDate}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${Invoice.creatorName}"/></td>
                                    </tr>
                                    <%index++;%>

                                </c:forEach>
                            </tbody>
                        </table>


                    </c:if>
                    <br>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                   </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
