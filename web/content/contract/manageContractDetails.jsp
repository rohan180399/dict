
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
    </head>
    <body>
        <form name="manufacturer" method="post">
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <!-- pointer table -->

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td>
                        <%@ include file="/content/common/message.jsp" %>
                    </td></tr></table>
                    <c:if test = "${contractDetails != null}" >
                <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

                    <tr>
                        <td width="40" height="30"  class="contentsub">S.NO</td>
                        <td width="100" height="30" class="contentsub">Customer Name</td>
                        <td width="100" height="30" class="contentsub">Contract End Date</td>
                        <td width="66" height="30" class="contentsub">Status</td>
                        <td width="30" height="30" class="contentsub"></td>
                    </tr>
                    <tr>
                        <%
                                    int index = 0;
                        %>

                        <c:forEach items="${contractDetails}" var="contract">
                            <%
                                        String classText = "";
                                        String style = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                            style = "background-color:#FFFFFF";

                                        } else {
                                            classText = "text1";
                                            style = "background-color:#D8F1F5";
                                        }
                            %>
                        
                            <td class="<%=classText%>" height="30"><%=index + 1%></td>
                            <input type="hidden" name="custId" value='<c:out value="${contract.custId}"/>'>
                            <td class="<%=classText%>" height="30"><c:out value="${contract.custName}"/> </td>
                            <td class="<%=classText%>" height="30"><c:out value="${contract.edate}"/> </td>
                            <td class="<%=classText%>" height="30">
                                <c:if test="${(contract.status=='n') || (contract.status=='N')}" >
                                    <option value="N">InActive</option>
                                </c:if>
                                <c:if test="${(contract.status=='y') || (contract.status=='Y')}" >
                                    <option value="Y" >Active</option>
                                </c:if>
                            </td>
                            <td  height="30"  > <div class="<%=classText%>" >  <a href="/throttle/alterCustContract.do?custId=<c:out value="${contract.custId}"/>"> alter  </a> </div>  </td>
                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach>

                </table>
            </c:if>
            <br>
            <center>
                <input type="submit" class="button" value="Add" name="add" onClick="submitPage(this.name)">

            </center>
            <input type="hidden" value="" name="reqfor">

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script language="javascript">
        function submitPage(value){
            if(value == "add"){
                document.manufacturer.action = '/throttle/addCustContract.do';
                document.manufacturer.submit();
            }
        }
    </script>

</html>
