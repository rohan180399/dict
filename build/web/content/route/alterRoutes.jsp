<%--
    Document   : addLecture
    Created on : Nov 7, 2008, 11:34:04 AM
    Author     : vijay
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script language="javascript">


            function getFLocation()
            {
                var oTextbox = new AutoSuggestControl(document.getElementById("fromLocation"),new ListSuggestions("fromLocation","/throttle/handleFLocation.do?"));
            }
            function getTLocation()
            {
                var oTextbox = new AutoSuggestControl(document.getElementById("toLocation"),new ListSuggestions("toLocation","/throttle/handleTLocation.do?"));
            }
            //            function clearTData()
            //            {
            //
            //                document.getElementById("toLocation").value = "";
            //            }
            //            function clearFData()
            //            {
            //                var cusId1 = document.getElementById("customerId").value;
            //                var cusId=cusId1.split("-");
            //                if(cusId[0] == 1000){
            //                    document.getElementById("fromLocation").value = "Karikali^1000";
            //                    document.getElementById('fromLocation').disabled=true;
            //                }else{
            //                document.getElementById("fromLocation").value = "";
            //                }
            //            }
            //
            function getFromLoc(){
                var cusId1 = document.getElementById("customerId").value;
                var cusId=cusId1.split("-");
                if(cusId[0] == 1000){
                    document.getElementById("ProductList").style.display="none";
                }else{
                    document.getElementById("ProductList").style.display="block";
                }
            }

            function submitPage(value){
                if(value == "save"){
                    var temp1 = document.addRoute.fromLocation.value;
                    var temp = temp1.split('^');
                    document.addRoute.floc.value = temp[0];
                    document.addRoute.flocID.value = temp[1];
                    var temp1 = document.addRoute.toLocation.value;
                    var temp = temp1.split('^');
                    document.addRoute.tloc.value = temp[0];
                    document.addRoute.tlocID.value = temp[1];

                    var temp1 = document.addRoute.customerId.value;
                    var temp = temp1.split('-');
                    document.addRoute.custID.value = temp[0];


                    //alert('Hi im here....1');
                    if(textValidation(document.addRoute.routeCode,'Route Code')){
                        return;
                    }else if(textValidation(document.addRoute.fromLocation,'From Location')){
                        return;
                    }else if(textValidation(document.addRoute.toLocation,'To Location')){
                        return;
                    }else if(textValidation(document.addRoute.viaRoute,'Via Route')){
                        return;
                    }else if(textValidation(document.addRoute.km,'KM')){
                        return;
                    }else if(textValidation(document.addRoute.tollAmount,'Toll Amount')){
                        return;
                    }else if(textValidation(document.addRoute.eligibleTrip,'Eligible Trip')){
                        return;
                    }else if(textValidation(document.addRoute.driverBata,'Driver Bata')){
                        return;
                    }else if(textValidation(document.addRoute.description,'Description')){
                        return;
                    }else if(textValidation(document.addRoute.tonnageRateMarket,'Market tonnage Rate ')){
                        return;
                    }
                    //alert('Hi im here....2');
                    document.addRoute.action = '/throttle/alterRouteInfo.do';
                    document.addRoute.submit();
                }
            }
        </script>
    </head>

    <!--[if lte IE 7]>
    <style type="text/css">

    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->

    <body onload="getFromLoc();">
        <form name="addRoute" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${routeDetails != null}">
                <c:forEach items="${routeDetails}" var="route">
                    <table  border="0" class="border" align="center" width="569" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td colspan="4" height="30" class="contenthead"><div class="contenthead">Alter Route</div></td>
                        </tr>
                        <tr>
                            <td class="text1" height="25"><font color=red>*</font>CLPL Customer</td>
                            <td class="text1" height="25">

                                <select class="form-control" name="customerId" id="customerId" style="width:275px" onchange="getFromLoc();">
                                    <c:if test="${cuslist != null}">
                                        <c:forEach items="${cuslist}" var="cl">
                                            <c:choose>
                                                <c:when test="${cl.custId == route.custId}" >
                                                    <option value='<c:out value="${cl.custId}"/>-<c:out value="${cl.custName}"/>' selected><c:out value="${cl.custName}"/></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${cl.custId}"/>-<c:out value="${cl.custName}"/>' ><c:out value="${cl.custName}"/></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" height="25"><font color=red>*</font>From Location</td>
                            <td class="text1" height="25"><input type="text" name="fromLocation"  id="fromLocation" size="20" value="<c:out value="${route.fromLocation}"/>"  onclick="clearFData();" onfocus="getFLocation();" class="form-control" autocomplete="off" ></td>
                        </tr>
                        <tr>
                            <td class="text1" height="25"><font color=red>*</font>To Location</td>
                            <td class="text1" height="25"><input type="text" name="toLocation" id="toLocation" value="<c:out value="${route.toLocation}"/>" size="20"  class="form-control" onclick="clearTData();" onfocus="getTLocation();"  autocomplete="off"  ></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="ProductList" style="display: none;">
                                    <tr>
                                        <td class="text1"><font color=red>*</font>Product Name</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&emsp;</td>
                                        <td class="text1">&nbsp;</td>
                                        <td class="text1">
                                            <select class="form-control" name="productId" id="productId" style="width:125px">
                                                <c:if test="${PList != null}">
                                                    <option value="0">---Select---</option>
                                                    <c:forEach items="${PList}" var="proList">
                                                        <c:choose>
                                                            <c:when test="${route.productId == proList.productId}">
                                                                <option value='<c:out value="${proList.productId}"/>' selected><c:out value="${proList.productname}"/></option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value='<c:out value="${proList.productId}"/>'><c:out value="${proList.productname}"/></option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2"><font color=red>*</font>Route Code</td>
                            <td class="text2">
                                <input type="hidden" name="routeId" size="20" class="form-control" value="<c:out value="${route.routeId}"/>" />
                                <input type="text" name="routeCode" size="20" class="form-control" value="<c:out value="${route.routeCode}"/>" autocomplete="off" />
                            </td>
                        </tr>
                        <!--                <tr>
                                            <td class="text1" height="25"><font color=red>*</font>From Location</td>
                                            <td class="text1" height="25"><input type="text" name="fromLocation" size="20"  value="<c:out value="${route.fromLocation}"/>" autocomplete="off" class="form-control" readonly ></td>
                                        </tr>-->
                        <tr>
                            <td class="text1">Depot</td>
                            <td class="text1">
                                <select class="form-control" name="Depot"  style="width:125px">
                                    <c:choose>
                                        <c:when test="${route.depotVal =='Yes'}">
                                            <option value='Yes' selected>Yes</option>
                                            <option value='No'>No</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value='No' selected>No</option>
                                            <option value='Yes'>Yes</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2"><font color=red>*</font>State</td>
                            <td class="text2"><input type="text" name="state" size="20" value="<c:out value="${route.state}"/>"  class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color=red>*</font>District</td>
                            <td class="text1"><input type="text" name="toCity" size="20"  value="<c:out value="${route.toCity}"/>" class="form-control" /></td>
                        </tr>

                        <!--                        <tr>
                                                    <td class="text2" height="25"><font color=red>*</font>To Location</td>
                                                    <td class="text2" height="25"><input type="text" name="toLocation" size="20"  value="<c:out value="${route.toLocation}"/>" autocomplete="off" class="form-control" onfocus="getTLocation();" ></td>
                                                </tr>-->
                        <tr>
                            <td class="text1"><font color=red>*</font>Via Route</td>
                            <td class="text1"><input type="text" name="viaRoute" size="20"  value="<c:out value="${route.viaRoute}"/>" autocomplete="off" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color=red>*</font>Freight Tonnage Rate </td>
                            <td class="text1"><input type="text" name="tonnageRate" size="20" value="<c:out value="${route.tonnageRate}"/>"   class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color=red>*</font>Market Vehicl Freight Tonnage Rate </td>
                            <td class="text1"><input type="text" name="tonnageRateMarket" size="20" value="<c:out value="${route.tonnageRateMarket}"/>" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color=red>*</font>Market Vehicle Margin %age </td>
                            <td class="text2"><input type="text" name="marketVehiclePercentage" size="20" value="<c:out value="${route.marketVehiclePercentage}"/>"  class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color=red>*</font>KM</td>
                            <td class="text1"><input type="text" name="km" size="20"  value="<c:out value="${route.km}"/>" autocomplete="off" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color=red>*</font>SLA (in hrs)</td>
                            <td class="text2"><input type="text" name="sla" size="20" value="<c:out value="${route.sla}"/>"   class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color=red>*</font>Toll Amount</td>
                            <td class="text2"><input type="text" name="tollAmount" size="20"  value="<c:out value="${route.tollAmount}"/>" autocomplete="off" class="form-control" /></td>
                        </tr>
                        <!--                <tr>
                                            <td class="text1"><font color=red>*</font>Eligible Trip</td>
                                            <td class="text1"><input type="text" name="eligibleTrip" size="20"  value="<c:out value="${route.eligibleTrip}"/>" autocomplete="off" class="form-control" /></td>
                                        </tr>-->
                        <input type="hidden" name="eligibleTrip" size="20"  value="<c:out value="${route.eligibleTrip}"/>" autocomplete="off" class="form-control" />
                        <tr>
                            <td class="text1"><font color=red>*</font>Driver Bata/per ton</td>
                            <td class="text1"><input type="text" name="driverBata" size="20"  value="<c:out value="${route.driverBata}"/>" autocomplete="off" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><font color=red>*</font>Description</td>
                            <td class="text2" height="30"><textarea name="description" style="width:123px;"></textarea></td>
                        </tr>
                    </table>
                </c:forEach>
            </c:if>
            <br>
            <center>
                <input type="button" class="button" value="Update" name="save" onClick="submitPage(this.name)">

                <input type="hidden" name="floc" value=""  class="form-control" />
                <input type="hidden" name="flocID" value=""  class="form-control" />
                <input type="hidden" name="tloc" value=""  class="form-control" />
                <input type="hidden" name="tlocID" value=""  class="form-control" />
                <input type="hidden" name="custID" value=""  class="form-control" />
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
