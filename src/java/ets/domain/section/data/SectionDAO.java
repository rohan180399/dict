package ets.domain.section.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.section.business.SectionTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author vidya
 */
public class SectionDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "SectionDAO";

    //getSectionList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getSectionList() {
        Map map = new HashMap();
        ArrayList sectionList = new ArrayList();
        try {

            sectionList = (ArrayList) getSqlMapClientTemplate().queryForList("section.SectionList", map);
        // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "SectionList", sqlException);
        }
        return sectionList;

    }
    // getCategoryList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCategoryList() {
        Map map = new HashMap();
        ArrayList categoryList = new ArrayList();
        try {

            categoryList = (ArrayList) getSqlMapClientTemplate().queryForList("section.CategoryList", map);
        // System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "SectionList", sqlException);
        }
        return categoryList;

    }

    /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doSectionDetails(SectionTO sectionTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("sectionName", sectionTO.getSectionName());
        map.put("sectionCode", sectionTO.getSectionCode());
        map.put("description", sectionTO.getDescription());
        if(!"".equals(sectionTO.getServiceTypeId())){
        map.put("serviceTypeId", sectionTO.getServiceTypeId());
        }else{
        map.put("serviceTypeId", "0");
        }

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("section.insertSection", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doSectionDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            SectionTO sectionTO = null;
            while (itr.hasNext()) {
                sectionTO = (SectionTO) itr.next();
                map.put("sectionId", sectionTO.getSectionId());
                map.put("sectionName", sectionTO.getSectionName());
                map.put("sectionCode", sectionTO.getSectionCode());
                map.put("description", sectionTO.getDescription());
                map.put("serviceTypeId", sectionTO.getServiceTypeId());
                map.put("activeInd", sectionTO.getActiveInd());
                map.put("userId", UserId);
                System.out.println("map = " + map);
                status = (Integer) getSqlMapClientTemplate().update("section.updateSection", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS, "updateSection", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUomList() {
        Map map = new HashMap();
        ArrayList uomList = new ArrayList();
        try {

            uomList = (ArrayList) getSqlMapClientTemplate().queryForList("section.UomList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "UomList", sqlException);
        }
        return uomList;

    }

    // parts starts here...
    /**
     * This method used to Insert Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doPartsDetails(SectionTO sectionTO, int UserId) {

        Map map = new HashMap();
        int itemId = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("sectionId", sectionTO.getSectionId());
        map.put("mfrId", sectionTO.getMfrId());
        map.put("modelId", sectionTO.getModelId());
        map.put("itemCode", sectionTO.getItemCode());
        map.put("paplCode", sectionTO.getPaplCode());
        map.put("itemName", sectionTO.getItemName());
        map.put("uom", sectionTO.getUomId());
        map.put("specification", sectionTO.getSpecification());
        map.put("reConditionable", sectionTO.getReConditionable());
        map.put("scrapUomId", sectionTO.getScrapUomId());
        map.put("vatId", sectionTO.getVatId());
        map.put("groupId", sectionTO.getGroupId());
        map.put("maxQuandity", sectionTO.getMaxQuandity());
        map.put("roLevel", sectionTO.getRoLevel());
        map.put("subRackId", sectionTO.getSubRackId());
        map.put("categoryId", sectionTO.getCategoryId());
        map.put("sprice", sectionTO.getSellingPrice());
        map.put("mrp", sectionTO.getMrp());        
        System.out.println("add parts map is="+map);


        int status = 0;
        try {
            //get the category code
            String categoryCode = (String) getSqlMapClientTemplate().queryForObject("section.getCategoryCode", map);
            //get the part sequence no
            int partSeqNo = (Integer) getSqlMapClientTemplate().insert("section.insertPartSequenceNo", map);

            String companyPartCode = categoryCode + partSeqNo;
            map.put("paplCode", companyPartCode);

            itemId = (Integer) getSqlMapClientTemplate().insert("section.insertParts", map);
            System.out.println("itemId"+itemId);
        //itemId = (Integer) getSqlMapClientTemplate().queryForObject("section.getItemId", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doPartsDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "doPartsDetails", sqlException);
        }
        return itemId;

    }

    /**
     * This method used to Insert Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doInsertStock(int spId, int itemId) {

        Map map = new HashMap();

        map.put("spId", spId);
        map.put("itemId", itemId);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("section.insertNewStock", map);
            status = (Integer) getSqlMapClientTemplate().update("section.insertRcStock", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertStock Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "doInsertStock", sqlException);
        }
        return status;

    }

    /**
     * This Ajax method used to Get  ModelList.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAjaxModelList(int mfrId) {
        Map map = new HashMap();
        ArrayList ajaxModelList = new ArrayList();
        map.put("mfrId", mfrId);
        try {

            ajaxModelList = (ArrayList) getSqlMapClientTemplate().queryForList("section.AjaxModelList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return ajaxModelList;

    }

    /**
     * This ajax method used to Get SubRack List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAjaxSubRackList(int rackId) {
        Map map = new HashMap();
        ArrayList ajaxSubRackList = new ArrayList();
        map.put("rackId", rackId);
        System.out.println("rackId in DAO" + rackId);
        try {

            ajaxSubRackList = (ArrayList) getSqlMapClientTemplate().queryForList("section.AjaxSubRackList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxSubRackList", sqlException);
        }
        return ajaxSubRackList;

    }

    /**
     * This  method used to Get Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getPartsDetails(SectionTO sectionTO) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        String itemName = sectionTO.getItemName();
        System.out.println("itemNae="+itemName);
        if(itemName != null ){
        itemName = "%" + itemName + "%";    
        }
        map.put("mfrId", sectionTO.getMfrId());
        map.put("modelId", sectionTO.getModelId());
        map.put("sectionId", sectionTO.getSectionId());
        map.put("itemCode", sectionTO.getItemCode());
        map.put("paplCode", sectionTO.getPaplCode());
        map.put("itemName", itemName );
        map.put("reConditionable", sectionTO.getReConditionable());
        try {

            List = (ArrayList) getSqlMapClientTemplate().queryForList("section.PartsList", map);
            System.out.println("List.size=" + List.size());


        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "PartsList", sqlException);
        }
        return List;

    }

    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAlterPartsDetail(int itemId) {
        Map map = new HashMap();
        map.put("itemId", itemId);
        System.out.println("partsId in dao" + itemId);
        ArrayList partsDetail = null;
        try {
            partsDetail = (ArrayList) getSqlMapClientTemplate().queryForList("section.GetPartsDetail", map);
            System.out.println("company size in dao " + partsDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return partsDetail;
    }

    /**
     * This method used to Update Parts  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doPartsUpdateDetails(SectionTO sectionTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("categoryId", sectionTO.getCategoryId());
        map.put("mfrId", sectionTO.getMfrId());
        map.put("sprice", sectionTO.getSellingPrice());
        map.put("mrp", sectionTO.getMrp());
        map.put("modelId", sectionTO.getModelId());
        map.put("mfrCode", sectionTO.getMfrCode());
        map.put("paplCode", sectionTO.getPaplCode());
        map.put("itemName", sectionTO.getItemName());
        map.put("uomId", sectionTO.getUomId());
        map.put("specification", sectionTO.getSpecification());
        map.put("reConditionable", sectionTO.getReConditionable());
        map.put("scrapUomId", sectionTO.getScrapUomId());
        map.put("groupId", sectionTO.getGroupId());
        map.put("vatId", sectionTO.getVatId());
        map.put("maxQuandity", sectionTO.getMaxQuandity());
        map.put("roLevel", sectionTO.getRoLevel());
        map.put("subRackId", sectionTO.getSubRackId());
        map.put("rackId", sectionTO.getRackId());
        map.put("itemId", sectionTO.getItemId());        

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("section.updateParts", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateParts", sqlException);
        }
        return status;

    }
    //getTotalParts
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int getTotalParts(SectionTO sectionTO) {
        Map map = new HashMap();
        String itemName = sectionTO.getItemName();
        
        if(itemName != null){
            itemName = "%"+itemName+"%";
        }
        
        map.put("mfrId", sectionTO.getMfrId());
        map.put("modelId", sectionTO.getModelId());
        map.put("sectionId", sectionTO.getSectionId());
        map.put("itemCode", sectionTO.getItemCode());
        map.put("paplCode", sectionTO.getPaplCode());
        map.put("itemName", itemName );
        map.put("reConditionable", sectionTO.getReConditionable());
        map.put("categoryId", sectionTO.getCategoryId());
        int totalRecords = 0;
        try {
            //get count
            totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("section.getTotalParts", map);
            System.out.println("totalRecords :" + totalRecords);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "SectionList", sqlException);
        }
        return totalRecords;

    }
    //getPartsByPage(startIndex,endIndex)
    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getPartsByPage(SectionTO sectionTO) {
        ArrayList indexedPartsDetail = new ArrayList();
        Map map = new HashMap();
        String  itemName = sectionTO.getItemName();
        String  groupId = sectionTO.getGroupId();
        
        if(groupId == null){
            groupId = "";
        }
        if(itemName != null){
            itemName = "%" + itemName + "%" ;
        }else{
            itemName = "";
        }

        //map.put("startIndex", startIndex);
        //map.put("endIndex", endIndex);
        map.put("mfrId", sectionTO.getMfrId());
        map.put("modelId", sectionTO.getModelId());
        map.put("itemCode", sectionTO.getItemCode());
        map.put("paplCode", sectionTO.getPaplCode());
        map.put("itemName", itemName );
        map.put("reConditionable", sectionTO.getReConditionable());
        map.put("categoryId", sectionTO.getCategoryId());
        map.put("groupId", groupId);
        System.out.println("sectionTO.getPaplCode(): "+sectionTO.getPaplCode());
        //System.out.println("startIndex" + startIndex);
        //System.out.println("endIndex" + endIndex);
        
        System.out.println("map:"+map);
        try {
            //search parts with first and last index
            indexedPartsDetail = (ArrayList) getSqlMapClientTemplate().queryForList("section.IndexedPartsDetail", map);
            System.out.println("indexed parts size in dao " + indexedPartsDetail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return indexedPartsDetail;
    }

    public float getItemStock(int itemId, int companyId) {
        Map map = new HashMap();
        Float qty;
        float quantity = 0;
        map.put("itemId", itemId);
        map.put("companyId", companyId);
        try {
            //search parts with first and last index            
            qty = (Float) getSqlMapClientTemplate().queryForObject("section.localSpStkBalance", map);            
            if (qty != null) {
                quantity = qty;
            } else {
                quantity = 0;
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return quantity;
    }

    //Hari Start
    public float getItemStockLastIssued(int itemId, int companyId) {
        Map map = new HashMap();
        Float qty;
        float quantity = 0;
        map.put("itemId", itemId);
        map.put("companyId", companyId);
        try {
            //search parts with first and last index
            qty = (Float) getSqlMapClientTemplate().queryForObject("section.getItemStockLastIssued", map);
            System.out.println("getItemStockLastIssued"+qty);
            if (qty != null) {
                quantity = qty;
            } else {
                quantity = 0;
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return quantity;
    }
    public float getItemStockLastDelivered(int itemId, int companyId) {
        Map map = new HashMap();
        Float qty;
        float quantity = 0;
        map.put("itemId", itemId);
        map.put("companyId", companyId);
        try {
            //search parts with first and last index
            qty = (Float) getSqlMapClientTemplate().queryForObject("section.getItemStockLastDelivered", map);
            System.out.println("getItemStockLastDelivered"+qty);
            if (qty != null) {
                quantity = qty;
            } else {
                quantity = 0;
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return quantity;
    }

    public String getItemStockLastPurchased(int itemId, int companyId) {
        Map map = new HashMap();
        Float qty;
        float quantity = 0;
        String value="";
        String price="";
        ArrayList lastPurchased = new ArrayList();
        map.put("itemId", itemId);
        map.put("companyId", companyId);
        try {
            //search parts with first and last index
            System.out.println("In getItemStockLastPurchased");
            lastPurchased = (ArrayList) getSqlMapClientTemplate().queryForList("section.getItemStockLastPurchased", map);
            System.out.println("getItemStockLastPurchased"+lastPurchased.size());
            Iterator itr= lastPurchased.iterator();
            SectionTO section=null;
            if (lastPurchased.size() != 0) {
                section=(SectionTO)itr.next();
                value=section.getLastPurchasedQty()+"-"+section.getPrice();
                System.out.println("Have value-->"+value);
            } else {
                price = (String)getSqlMapClientTemplate().queryForObject("section.getItemPrice", map);
                System.out.println("From Price Master-->"+price);
                value=0.00+"-"+price;
                System.out.println("Have No Value-->"+value);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return value;
    }
//Hari End
    public String getItemSuggests(String itemName,String mfrId, String modelId) {
        Map map = new HashMap();
        map.put("itemName", itemName);
        System.out.println("mfrId:"+mfrId+"modelId:"+modelId);
        if("".equals(mfrId) || mfrId == null){
            mfrId="0";
        }
        if("".equals(modelId) || modelId == null){
            modelId="0";
        }
        System.out.println("mfrId:"+mfrId+"modelId:"+modelId);
        map.put("mfrId", mfrId);
        map.put("modelId", modelId);
        String suggestions = "";
        SectionTO secTO = new SectionTO();
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("section.getItemNames", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                secTO = new SectionTO();
                secTO = (SectionTO) itr.next();
                suggestions = secTO.getItemName() + "~" + suggestions;
            }
            if("".equals(suggestions)){
                suggestions="no match found";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return suggestions;
    }

    public String checkItemExists(String choice, String param) {
        Map map = new HashMap();
        String suggestions = "";

        try {
            map.put("choice", choice);
            map.put("param", param);
            System.out.println("choice="+choice);
            System.out.println("param="+param);
            System.out.println("my map value here "+map);
            if("mfr".equals(choice)){
            suggestions = (String) getSqlMapClientTemplate().queryForObject("section.checkMFRItemExists", map);
            }else if("papl".equals(choice)){
            suggestions = (String) getSqlMapClientTemplate().queryForObject("section.checkPAPLItemExists", map);
            }
            System.out.println("item name="+suggestions);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkItemExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkItemExists", sqlException);
        }
        return suggestions;
    }

 //Hari Start
    //getGroupList
public ArrayList getGroupList() {
        Map map = new HashMap();
        ArrayList groupList = new ArrayList();
        try {
            System.out.println("In Group List Dao");
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("section.groupList", map);
            System.out.println("GropList Size-->"+groupList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "groupList", sqlException);
        }
        return groupList;

    }

public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList venList = new ArrayList();
        try {
            System.out.println("In Group List Dao");
            venList = (ArrayList) getSqlMapClientTemplate().queryForList("section.vendorList", map);
            System.out.println("GropList Size-->"+venList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "groupList", sqlException);
        }
        return venList;

    }

public ArrayList getGroupListForParts() {
        Map map = new HashMap();
        ArrayList groupList = new ArrayList();
        try {
            System.out.println("In Group List Dao");
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("section.groupListForParts", map);
            System.out.println("GropList Size-->"+groupList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "groupList", sqlException);
        }
        return groupList;

    }


/**
     * This method used to Insert Group Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doGroupInsertDetails(SectionTO sectionTO, int UserId) {

        Map map = new HashMap();
        int itemId = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("groupName", sectionTO.getGroupName());
        map.put("groupDescription", sectionTO.getDescription());
                int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("section.insertGroup", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doGroupInsertDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "doGroupInsertDetails", sqlException);
        }
        return status;

    }


public int doUpdateGroup(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
          try {
            Iterator itr = List.iterator();
            SectionTO sectionTO = null;
            while (itr.hasNext()) {
            sectionTO = (SectionTO) itr.next();
            System.out.println("getStatus is-->" + sectionTO.getActiveInd());
            map.put("userId", userId);
            map.put("groupId", sectionTO.getGroupId());
            map.put("groupName", sectionTO.getGroupName());
            map.put("groupDescription", sectionTO.getDescription());
            map.put("activeInd", sectionTO.getActiveInd());
            status = (Integer) getSqlMapClientTemplate().update("section.updateGroup", map);
            System.out.println("status is" + status);
        }
          }catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-03", CLASS, "doUpdateCompany", sqlException);
        }
        return status;
    }

public ArrayList getServiceTypeMaster(SectionTO sectionTO) {
        Map map = new HashMap();
        ArrayList serviceTypeList = new ArrayList();

        System.out.println("map = " + map);
        try {
            System.out.println("this is City Master");
            serviceTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("section.getServiceTypeMaster", map);
            System.out.println(" serviceTypeList =" + serviceTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypeMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypeMaster List", sqlException);
        }

        return serviceTypeList;
    }

public String checkServiceTypeName(String serviceTypeName) {
        Map map = new HashMap();
        String suggestions = "";

        try {
            map.put("serviceTypeName", serviceTypeName);
            suggestions = (String) getSqlMapClientTemplate().queryForObject("section.checkServiceTypeName", map);
            System.out.println("item name="+suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkItemExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkItemExists", sqlException);
        }
        return suggestions;
    }

public int insertServiceType(SectionTO sectionTO,int userId) {
        Map map = new HashMap();
        int insertServiceType = 0;

        try {
            map.put("serviceTypeName", sectionTO.getServiceTypeName());
            map.put("desc", sectionTO.getDesc());
            map.put("status", sectionTO.getStatus());
            map.put("userId", userId);
            insertServiceType = (Integer) getSqlMapClientTemplate().update("section.insertServiceType", map);
            System.out.println("item name="+insertServiceType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertServiceType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertServiceType", sqlException);
        }
        return insertServiceType;
    }

public int updateServiceType(SectionTO sectionTO,int userId) {
        Map map = new HashMap();
        int updateServiceType = 0;

        try {
            map.put("serviceTypeId", sectionTO.getServiceTypeId());
            map.put("serviceTypeName", sectionTO.getServiceTypeName());
            map.put("desc", sectionTO.getDesc());
            map.put("status", sectionTO.getStatus());
            map.put("userId", userId);
            updateServiceType = (Integer) getSqlMapClientTemplate().update("section.updateServiceType", map);
            System.out.println("item name="+updateServiceType);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateServiceType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateServiceType", sqlException);
        }
        return updateServiceType;
    }
public ArrayList getAjaxModelList1(int mfrId,String fleetTypeId, String typeId) {
                Map map = new HashMap();
                ArrayList ajaxModelList = new ArrayList();
               map.put("mfrId", mfrId);
               if(fleetTypeId == null || "".equals(fleetTypeId)){
                   fleetTypeId = "0";
               }
               if(typeId == null || "".equals(typeId)){
                   typeId = "0";
               }
               map.put("fleetTypeId", fleetTypeId);
                map.put("typeId", typeId);
                System.out.println("new map:"+map);
                try {

                    ajaxModelList = (ArrayList) getSqlMapClientTemplate().queryForList("section.AjaxModelList1", map);

                } catch (Exception sqlException) {
                    /*
                     * Log the exception and propagate to the calling class
                     */
                    FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
                    FPLogUtils.fpErrorLog("sqlException" + sqlException);
                    throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
                }
                return ajaxModelList;

        }


         public ArrayList getAjaxModelList2(int typeId) {
            Map map = new HashMap();
            ArrayList ajaxModelList = new ArrayList();
            map.put("typeId", typeId);
            try {
    
                ajaxModelList = (ArrayList) getSqlMapClientTemplate().queryForList("section.AjaxModelList2", map);
    
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
            }
            return ajaxModelList;
    
        }
         
        public ArrayList getTonnage(int typeId) {
        Map map = new HashMap();
        ArrayList tonList = new ArrayList();
        map.put("typeId", typeId);
        try {

            tonList = (ArrayList) getSqlMapClientTemplate().queryForList("section.getTonnage", map);
            System.out.println("tonList"+tonList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "tonList", sqlException);
        }
        return tonList;

    }
        
    public ArrayList getTrailerTypeTonnage(int typeId) {
        Map map = new HashMap();
        ArrayList tonList = new ArrayList();
        map.put("typeId", typeId);
        try {

            tonList = (ArrayList) getSqlMapClientTemplate().queryForList("section.getTrailerTypeTonnage", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerTypeTonnage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getTrailerTypeTonnage", sqlException);
        }
        return tonList;

    }
          
         public ArrayList processGetModelsForVendor(int typeId) {
	            Map map = new HashMap();
	            ArrayList processGetModelsForVendor = new ArrayList();
	//           map.put("mfr", mfr);
	            map.put("typeId", typeId);
	            try {
	    
	                processGetModelsForVendor = (ArrayList) getSqlMapClientTemplate().queryForList("section.processGetModelsForVendor", map);
	    
	            } catch (Exception sqlException) {
	                /*
	                 * Log the exception and propagate to the calling class
	                 */
	                FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
	                FPLogUtils.fpErrorLog("sqlException" + sqlException);
	                throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
	            }
	            return processGetModelsForVendor;
	    
        }
         
         
           public String getUploadIds(SectionTO sectionTO,int userId) {
        Map map = new HashMap();
        String ids = "";

        try {
            map.put("categoryName", sectionTO.getCategoryName());
            map.put("uomName", sectionTO.getUom());
            map.put("scrapUomName", sectionTO.getScrapUomName());
            map.put("modelName", sectionTO.getModelName());
            map.put("mfrName", sectionTO.getMfr());
            map.put("subRackName", sectionTO.getSubRackName());

           System.out.println ("map in ids:::::"+map);
            String mfrId = (String) getSqlMapClientTemplate().queryForObject("section.getMFRId", map);
            String modelId = (String) getSqlMapClientTemplate().queryForObject("section.getModelId", map);
            String uomId = (String) getSqlMapClientTemplate().queryForObject("section.getUOMId", map);
            String scrapuomId = (String) getSqlMapClientTemplate().queryForObject("section.getScrapUOMId", map);
            String categoryId = (String) getSqlMapClientTemplate().queryForObject("section.getCategoryId", map);
            String subrackId = (String) getSqlMapClientTemplate().queryForObject("section.getSubrackId", map);
            ids=categoryId+'~'+mfrId+'~'+modelId+'~'+uomId+'~'+scrapuomId+'~'+subrackId;
            System.out.println("ids="+ids);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateServiceType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateServiceType", sqlException);
        }
        return ids;
    }

           
             public int insertItemPrice(SectionTO sectionTO) {
        Map map = new HashMap();
        int insertprice = 0;

        try {
            map.put("itemId", sectionTO.getItemId());
            map.put("tax", "0");
            map.put("mrp", sectionTO.getPrice());
            map.put("unitPrice", sectionTO.getPrice());
            map.put("priceType", "DC");
            System.out.println("map item="+map);
            insertprice = (Integer) getSqlMapClientTemplate().update("section.insertItemPrice", map);
            System.out.println("item insertprice="+insertprice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertServiceType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertServiceType", sqlException);
        }
        return insertprice;
    }
          
}
