<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {
        if (value == 'ExportExcel') {
            document.fcWiseTripSummary.action = '/throttle/handleAccountMgrPerformanceReportExcel.do?param=ExportExcel';
            document.fcWiseTripSummary.submit();
        } else {
            document.fcWiseTripSummary.action = '/throttle/handleAccountMgrPerformanceReportExcel.do?param=Search';
            document.fcWiseTripSummary.submit();
        }
    }
</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">العربية</a>
              </span>-->

    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.AccountMgrPerformanceSummary" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="header.label.Reports" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.AccountMgrPerformanceSummary" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="fcWiseTripSummary" method="post">
                        <table class="table table-bordered" id="report" >


                            <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td  height="30"><input style="width:260px;height:40px;"  class="datepicker" name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td  height="30"><input style="width:260px;height:40px;"  class="datepicker" name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">
                                            <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>"></td>
                                    </tr>

                                </table>
                            </td>
                        </table>

                        <c:if test="${AccountMgrPerformanceReportDetails != null}">
                            <table class="table table-info mb30 table-hover" id="table" class="sortable"  >

                                <thead>
                                <th> <spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                <th ><spring:message code="operations.reports.label.AccountManagerName" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.Totaltrips" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.TotalSales" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.TotalKms" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.Revenue/km" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.cost/km" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.Margin/km" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.WFUDays" text="default text"/></th>
                                <th><spring:message code="operations.reports.label.TotalCost" text="default text"/></th>

                                <th><spring:message code="operations.reports.label.TotalReferHrs" text="default text"/></th>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="marginKm1" value="${0}"/>
                                    <c:set var="marginKm2" value="${0}"/>
                                    <c:forEach items="${AccountMgrPerformanceReportDetails}" var="account">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td width="30"  ><%=index++%></td>
                                            <td  width="30" ><c:out value="${account.empName}"/></td>
                                            <td  width="30" ><c:out value="${account.tripNos}"/></td>
                                            <td  width="30" ><c:out value="${account.revenue}"/></td>
                                            <td  width="30" ><c:out value="${account.totalkms}"/></td>
                                            <td  width="30" >
                                                <c:if test="${account.totalkms > 0.0}">
                                                    <c:set var="marginKm1" value="${account.revenue/account.totalkms}"/>
                                        <fmt:formatNumber pattern="##0.00" value="${account.revenue/account.totalkms}"/>
                                    </c:if>
                                    <c:if test="${account.totalkms == 0.0}">
                                        0.00
                                    </c:if>
                                    </td>
                                    <td  width="30" >
                                        <c:if test="${account.totalkms > 0.0}">
                                            <c:set var="marginKm2" value="${account.totalCost/account.totalkms}"/>
                                        <fmt:formatNumber pattern="##0.00" value="${account.totalCost/account.totalkms}"/>
                                    </c:if>
                                    <c:if test="${account.totalkms == 0.0}">
                                        0.00
                                    </c:if>
                                    </td>
                                    <td  width="30" ><fmt:formatNumber pattern="##0.00" value="${marginKm1-marginKm2}"/></td>
                                    <td  width="30" ><fmt:formatNumber pattern="##0.00" value="${(account.wfuHours/24)}"/></td>

                                    <td  width="30" ><c:out value="${account.totalCost}"/></td>

                                    <td  width="30" ><c:out value="${account.totalHms}"/></td>


                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>


                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" >5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50" selected="selected">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="operations.reports.label.of" text="default text"/>  <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                        </c:if>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

