<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>

<script>
    function submitPage()
    {
        if (document.add.companyTypeId.value == 0) {
            alert("CompanyType should not be Empty");
            document.add.companyTypeId.focus();
            return;
        }
        if (textValidation(document.add.name, 'Name')) {
            return;
        }
        if (textValidation(document.add.address, 'address')) {
            return;
        }
        if (textValidation(document.add.city, 'city')) {
            return;
        }
        if (textValidation(document.add.district, 'district')) {
            return;
        }
        if (textValidation(document.add.stateId, 'state')) {
            return;
        }
        if (numberValidation(document.add.pinCode, 'PinCode')) {
            return;
        }
        if (numberValidation(document.add.phone1, 'phone1')) {
            return;
        }
        if (numberValidation(document.add.organizationId, 'Organization Id')) {
            return;
        }
        if (numberValidation(document.add.gstNo, 'Gst No')) {
            return;
        }
        if (document.add.phone2.value != "") {
            if (numberValidation(document.add.phone2, 'Phone2')) {
                return;
            }
        } else {
            document.add.phone2.value = parseInt(0);
        }
        if (document.add.fax.value != "") {
            if (isSpecialCharacter(document.add.fax.value)) {
                alert('Special Characters are Not allowed for Fax');
                document.add.fax.focus();
                return;
            }
        } else {
            document.add.fax.value = parseInt(0);
        }

        if (document.add.email.value != "") {
            if (isEmail(document.add.email.value)) {
                document.add.email.focus();
                return;
            }
            if (isSpecialCharacter(document.add.email.value)) {
                alert('Special Characters are Not allowed for Email');
                document.add.email.focus();
                return;
            }
        } else {
            document.add.email.value = "-";
        }
        if (document.add.web.value != "") {

            if (isSpecialCharacter(document.add.web.value)) {
                alert('Special Characters are Not allowed for Web');
                document.add.web.focus();
                return;
            }
        } else {
            document.add.web.value = "-";
        }
        document.add.action = '/throttle/addNewCompany.do';
        document.add.submit();
    }
    function isEmail(s)
    {
        if (/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
            return false;
        alert("Email not in valid form!");
        return true;
    }
    function setFocus() {
        document.add.companyTypeId.focus();
    }

</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AddCompany" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class=""><spring:message code="hrms.label.Company" text="default text"/></li>
                <li class="Active"><spring:message code="hrms.label.AddCompany" text="default text"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setFocus();">
                    <form name="add" method="post">


                        <%--<%@ include file="/content/common/message.jsp" %>--%>

                        <table class="table table-info mb30 table-hover" >
                            <!--                            <thead>
                                                            <tr height="30">
                                                                <th colspan="2" ><spring:message code="hrms.label.AddCompany" text="default text"/></th>
                                                            </tr>
                                                        </thead>-->
                            <tr height="30">
                                <td ><font color="red">*</font><spring:message code="hrms.label.CompanyType" text="default text"/></td>
                                <td ><select class="form-control" name="companyTypeId" style="width:260px;height:40px;">
                                        <option value='0'>--<spring:message code="hrms.label.Select" text="default text"/>--</option>
                                        <c:if test="${companyTypes!=null}">                                        
                                            <c:forEach items="${companyTypes}" var="compType" >  
                                                <option value='<c:out value="${compType.companyTypeId}"/>'> <c:out value="${compType.companyType}"/> </option>
                                            </c:forEach>                                           
                                        </c:if>
                                        <!--                            </tr>
                                                                    <tr height="30">-->
                                        <td ><font color="red">*</font><spring:message code="hrms.label.Name" text="default text"/></td>
                                        <td ><input name="name" type="text" class="form-control" style="width:260px;height:40px;" value="" size="20"></td>
                            </tr>
                            <tr height="30">
                                <td ><font color="red">*</font><spring:message code="hrms.label.Address" text="default text"/></td>
                                <td ><textarea class="form-control" cols="15" rows="2" size="20" name="address" style="width:260px;height:40px;"></textarea></td>
                                <!--                            </tr> 
                                                            <tr height="30">-->
                                <td ><font color="red">*</font><spring:message code="hrms.label.City" text="default text"/></td>
                                <td ><input name="city" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20"></td>
                            </tr>
                            <tr height="30">
                                <td ><font color="red">*</font><spring:message code="hrms.label.District" text="default text"/></td>
                                <td ><input name="district" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20"></td>
                                <!--                            </tr>
                                                            <tr height="30">-->
<!--                                <td ><font color="red">*</font><spring:message code="hrms.label.State" text="default text"/></td>
                                <td ><input name="state" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20"></td>-->
                                <td><font color="red">*</font>Billing State</td>
                            <td>
                                <input name="stateIdTemp" id="stateIdTemp"  type="hidden" class="form-control" style="width:260px;height:40px;">
                                <select name="stateId" id="stateId" class="form-control" required data-placeholder="Choose One" style="width:260px;height:40px;">
                                    <option value="">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            </tr>
                            <tr height="30">
                                <td ><font color="red">*</font><spring:message code="hrms.label.Phone" text="default text"/>1</td>
                                <td ><input name="phone1" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20" maxlength="12"></td>
                                <!--                            </tr>
                                                            <tr height="30">-->

                                <td ><font color="red">*</font><spring:message code="hrms.label.Pincode" text="default text"/></td>
                                <td ><input name="pinCode"  type="text" style="width:260px;height:40px;" class="form-control" value="" size="20" maxlength="6"></td>
                            </tr>
                            <tr height="30">
                                <td >&nbsp;&nbsp;<spring:message code="hrms.label.Phone" text="default text"/>2</td>
                                <td ><input name="phone2" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20" maxlength="12"></td>
                                <!--                            </tr>
                                                            <tr height="30">-->
                                <td >&nbsp;&nbsp;<spring:message code="hrms.label.Fax" text="default text"/></td>
                                <td ><input name="fax" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20" maxlength="20" ></td>
                            </tr>
                            <tr height="30">
                                <td >&nbsp;&nbsp;<spring:message code="hrms.label.Email" text="default text"/></td>
                                <td ><input name="email" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20"></td>
                                <!--                            </tr>
                                                            <tr height="30">-->
                                <td >&nbsp;&nbsp;<spring:message code="hrms.label.Web" text="default text"/></td>
                                <td ><input name="web" type="text" style="width:260px;height:40px;" class="form-control" value="" size="20"></td>
                            </tr>
                            <tr>
                            <td >Organization Name</td>
                            <td>
                                <select class="form-control" name="organizationId" id="organizationId" style="width:260px;height:40px;">
                                    <c:if test="${organizationList != null}">
                                        <option value="">--Select--</option>
                                        <c:forEach items="${organizationList}" var="org">
                                            <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td><font color="red">*</font>GST No</td>
                            <td>
                                <input name="gstNo" id="gstNo"  type="text" class="form-control" style="width:260px;height:40px;">
                            </td>
                            </tr>
                            <tr>
                        </tr>
                        </table>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="btn btn-success" value="<spring:message code="hrms.label.SAVE" text="default text"/>" onclick="submitPage();" />
                            &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="hrms.label.CLEAR" text="default text"/>">
                        </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
