<%-- 
    Document   : viewParts
    Created on : Mar 4, 2009, 5:56:27 PM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            document.desigDetail.submit();
        }
        
    
    </script>
    
    <body>
        
        <form method="post" name="desigDetail">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                    <!-- title table -->
                    <table width="875" cellpadding="0" cellspacing="0" align="center" border="0">
                        <tr>
                            <td height="20"><%@ include file="/content/common/pageTitle.jsp" %></td>
                        </tr>
                    </table>
                    <!-- title table -->
                </div>
            </div>
            <br>
            <br>
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            
                    <tr>
 
 <table  border="0" class="tdcolor" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
 <tr>
<td colspan="4" height="30" class="contenthead"><b>Search Parts</b></td>
<td colspan="4" height="30" class="contenthead"></td>
</tr>
 


<tr>
 <td class="text2" height="30">Item Code</td>
<td class="text2" height="30"><input name="itemCode" type="text" class="form-control" value=<c:out value="${itemCode}" />></td>
<td class="text2" height="30">PAPL Code</td>
<td class="text2" height="30"><input name="paplCode" type="text" class="form-control" value=""></td>
<td class="text2" height="30">Item Name</td>
<td class="text2" height="30"><input name="itemName" type="text" class="form-control" value=""></td>



</tr>
<tr >
 
<td class="text1" height="30">Section</td>
<td class="text1"  ><select class="form-control" name="sectionId" >
<option value="">---Select---</option>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="Dept"> 
<option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
</c:forEach >
</c:if>  	
</select></td>
<td class="text1" height="30">MFR</td>
<td class="text1"  ><select class="form-control" name="mfrId" onchange="ajaxData();" >
<option value="">---Select---</option>
<c:if test = "${MfrList != null}" >
<c:forEach items="${MfrList}" var="Dept"> 
<option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
</c:forEach >
</c:if>  	
</select></td>
<td class="text1" height="30">Model</td>
<td class="text1"  ><select class="form-control" name="modelId" >
<option value="">---Select---</option>
<c:if test = "${ModelList != null}" >
<c:forEach items="${ModelList}" var="Dept"> 
<option value='<c:out value="${Dept.modelId}" />'><c:out value="${Dept.modelName}" /></option>
</c:forEach >
</c:if>  	
</select></td>
</tr>
<tr>

<td class="text2" height="30">RC Status</td>
<td class="text2" height="30"><select name="reConditionable" class="form-control" >
<option value="">--Select--</option>
<option>Y</option>
<option>N</option>
</select></td>
</tr>

 
</table>     
<!-- copy there  end -->
 <% int index = 0;  %>    
            <br>
            <c:if test = "${PartsList != null}" >
                <table width="800" align="center" cellpadding="0" cellspacing="0" id="bg">
 	 	 	 	 	 	 	 	 	 	 	 	                    
                    <tr align="center">
                        <td height="30" class="contentsub">S.No</td>
                        <td height="30" class="contentsub">Manufacturer</td>
                        <td height="30" class="contentsub">Model</td>
                        <td height="30" class="contentsub">MfrItemCode</td>
                        <td height="30" class="contentsub">PAPL Code</td>
                        <td height="30" class="contentsub">Item Name</td>
                        <td height="30" class="contentsub">Category</td>
                        <td height="30" class="contentsub">RC state</td>
                        <td height="30" class="contentsub">Scrap out</td>
                        <td height="30" class="contentsub">Max Qty</td>
                        <td height="30" class="contentsub">Ro Level</td>
                        <td height="30" class="contentsub">Rack</td>
                        <td height="30" class="contentsub">Sub-Rack</td>
                        <td height="30" class="contentsub">itemId</td>
                         <td height="30" class="contentsub">Details</td>
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${PartsList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="40" > 
                            
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.mfrName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.modelName}"/></td>
                             <td class="<%=classText %>" height="30"><c:out value="${list.mfrCode}"/></td>
                              <td class="<%=classText %>" height="30"><c:out value="${list.paplCode}"/></td>
                               <td class="<%=classText %>" height="30"><c:out value="${list.itemName}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${list.sectionName}"/></td>
                                 <td class="<%=classText %>" height="30"><c:out value="${list.reConditionable}"/></td>
                                  <td class="<%=classText %>" height="30"><c:out value="${list.uomName}"/></td>
                                   <td class="<%=classText %>" height="30"><c:out value="${list.maxQuandity}"/></td>
                                   <td class="<%=classText %>" height="30"><c:out value="${list.roLevel}"/></td>
                                   <td class="<%=classText %>" height="30"><c:out value="${list.rackName}"/></td>
                                   <td class="<%=classText %>" height="30"><c:out value="${list.subRackName}"/></td>
                                   <td class="<%=classText %>" height="30"><c:out value="${list.itemId}"/></td>
                                   <td class="<%=classText %>" align="left"> <a href="/throttle/alterParts.do?itemId=<c:out value='${list.itemId}' />" >Details </a> </td>  
                               
                             
                             
                        </tr>
                        <%
            index++;
             
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                 <input type="hidden" name=" " value="itemId">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
