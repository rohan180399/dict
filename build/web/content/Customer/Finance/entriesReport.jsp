
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
//        $(document).ready(function () {
//                $('#voucherNo').autocomplete({
//                    source: function (request, response) {
//                        $.ajax({
//                            url: "/throttle/getVoucherNo.do",
//                            dataType: "json",
//                            data: {
//                                voucherNo: request.term
//                            },
//                            success: function (data, textStatus, jqXHR) {
//                                if (data == '') {
//                                   
//                                }
//                                var items = data;
//                                response(items);
//                            },
//                            error: function (data, type) {
//
//                            }
//                        });
//                    },
//                    minLength: 1,
//                    select: function (event, ui) {
//                        var value = ui.item.Name;
//                        $('#voucherNo').val(value);
//
//                        return false;
//                    }
//
//                    // Format the list menu output of the autocomplete
//                }).data("autocomplete")._renderItem = function (ul, item) {
//                    var itemVal = item.Name;
//                    itemVal = '<font color="green">' + itemVal + '</font>';
//                    return $("<li></li>")
//                            .data("item.autocomplete", item)
//                            .append("<a>" + itemVal + "</a>")
//                            .appendTo(ul);
//                };
//
//            });
    $(document).ready(function() {
        $('#ledgerName').autocomplete({
            source: function(request, response) {
//                    alert("Hi I am Here");
                $.ajax({
                    url: "/throttle/getLedger.do",
                    dataType: "json",
                    data: {
                        ledgerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        if (data == '') {

                        }
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {

                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#ledgerId').val(tmp[0]);
                $('#ledgerName').val(tmp[1]);

                return false;
            }

            // Format the list menu output of the autocomplete
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
</script>  
<script>
    function submitPage(value) {
//                  var param = '';
//            if (value == 'Search') {
//                param = 'No';
//            } else {
//                param = 'ExportExcel';
//            }
        if (textValidation(document.entriesReport.fromDate, 'From Date')) {
            return;

        } else if (textValidation(document.entriesReport.toDate, 'To Date')) {
            return;
        }

        if (document.getElementById("distSummary").checked == true) {
            document.getElementById("distDetail").checked = false;
            document.entriesReport.distcheck.value = document.getElementById("distSummary").value;
            document.entriesReport.action = "/throttle/searchEntriesDetails.do?Param=" + value;
            document.entriesReport.submit();
        } else if (document.getElementById("distDetail").checked == true) {
            document.getElementById("distSummary").checked = false;
            document.entriesReport.distcheck.value = document.getElementById("distDetail").value;
            document.entriesReport.action = "/throttle/searchEntriesDetails.do?Param=" + value;
            document.entriesReport.submit();

        }

    }
    function setFocus() {
        var entryType = '<%=request.getAttribute("entryType")%>';
//            alert(entryType);
        var distcheck = '<%=request.getAttribute("distcheck")%>';
        var ledgerId = '<%=request.getAttribute("ledgerId")%>';
        var ledgerName = '<%=request.getAttribute("ledgerName")%>';
        if (entryType != 'null') {
            document.entriesReport.entryType.value = entryType;
        }

        if (ledgerId != 'null') {
            document.entriesReport.ledgerId.value = ledgerId;
        }
        if (ledgerName != 'null') {
            document.entriesReport.ledgerName.value = ledgerName;
        }

        if (distcheck != 'null') {
            if (distcheck == 'distSummary') {
                document.getElementById("distSummary").checked = true;
            }
            // alert('distcheck is:::'+distcheck);
            if (distcheck == 'distDetail') {
                document.getElementById("distDetail").checked = true;
            }
        }

    }
</script>
<%
          DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
          Date date = new Date();
          String fDate = "";
          String tDate = "";
          if (request.getAttribute("fromDate") != null) {
              fDate = (String) request.getAttribute("fromDate");
          } else {
              fDate = dateFormat.format(date);
          }
          if (request.getAttribute("toDate") != null) {
              tDate = (String) request.getAttribute("toDate");
          } else {
              tDate = dateFormat.format(date);
          }
%>


<style>
    #index td {
        color:white;
    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.EntriesReport"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.EntriesReport"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="setFocus();" >
                <form name="entriesReport" action=""  method="post">
                    <!--            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                                    <tr>
                                        <td >
                                        </td></tr>
                                </table>-->


                    <br/>

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr id="index" >
                                <th colspan="4" ><b><spring:message code="finance.label.EntriesReport"  text="default text"/></b></th>
                            </tr>
                        </thead>
                        <!--                    <tr id="exp_table"  style="display: block;">
                                                <td colspan="2" style="padding:15px;" align="right">-->

                        <!--<table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">-->
                        <tr>
                            <td  height="30"><spring:message code="finance.label.LedgerName"  text="default text"/></td>
                            <td  height="30">
                                <input type="hidden" name="ledgerId" id="ledgerId" class="form-control" value="">
                                <input type="text" name="ledgerName" id="ledgerName" class="form-control" value="" style="width:260px;height:40px;">
                            </td>
                            <td  height="30"><spring:message code="finance.label.EntryType"  text="default text"/></td>
                            <td  height="30">
                                <select class="form-control" name="entryType" id="entryType" style="width:260px;height:40px;">
                                    <option value=''>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                    <option value='CP'><spring:message code="finance.label.CashPayment"  text="default text"/></option>
                                    <option value='CR'><spring:message code="finance.label.CashReceipt"  text="default text"/></option>
                                    <option value='BP'><spring:message code="finance.label.BankPayment"  text="default text"/></option>
                                    <option value='BR'><spring:message code="finance.label.BankReceipt"  text="default text"/></option>
                                    <option value='CE'><spring:message code="finance.label.ContraEntry"  text="default text"/></option>
                                    <option value='JE'><spring:message code="finance.label.JournalEntry"  text="default text"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/> :</td>
                            <td  height="30"><input type="text"  autocomplete="off" name="fromDate" id="fromDate" class="datepicker" value="<%=fDate%>" style="width:260px;height:40px;"/>  </td>
                            <td  height="30"><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/> : </td>
                            <td  height="30"><input type="text"  autocomplete="off" name="toDate" id="toDate" class="datepicker" value="<%=tDate%>" style="width:260px;height:40px;"/></td>
                        </tr>

                        <tr>
                            <td height="30"> <spring:message code="finance.label.Detail"  text="default text"/> <input type="radio" name="diststatus" id="distDetail" value="distDetail" style="align:left"/><input type="hidden" name="distcheck" value="" > </td>
                            <td height="30"><spring:message code="finance.label.Summary"  text="default text"/><input type="radio" name="diststatus" id="distSummary" value="distSummary" checked style="align:left"/></td>
                            <td  height="30" colspan="2" >
                                <input type="button"  value="<spring:message code="finance.label.FetchData"  text="default text"/>"  class="btn btn-success" name="fetchData" onClick="submitPage(this.name)" style="width:100px;height:35px;">

                                <input type="button"  value="<spring:message code="finance.label.ExportToExcel"  text="default text"/>"  class="btn btn-success" name="ExportExcel" onClick="submitPage(this.name)" style="width:115px;height:35px;"> 
                            </td>
                            <!--                                        <td>
                                                                    </td>-->

                        </tr>

                    </table>

                    </td>
                    </tr>
                    </table>


                    <c:if test = "${entriesReportList != null && distcheck == 'distDetail'}" >

                        <br>
                        <!--                <table align="center" width="100%" border="0" id="table" class="sortable">
                                            <thead>-->
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.EntryDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.EntryType"  text="default text"/></th>
                            <th><spring:message code="finance.label.VoucherCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.CreditLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.CreditAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                            </thead>
                            <% int index = 0;%>
                            <% int sno = 0;%>
                            <c:set var="accountEntryDate" value=""/>
                            <c:set var="entryStatus" value=""/>
                            <c:set var="totalDebit" value="${0.00}"/>
                            <c:set var="totalCredit" value="${0.00}"/>
                            <c:forEach items="${entriesReportList}" var="PL">

                                <tr height="30">
                                    <td   align="left"> 
                                        <c:if test="${accountEntryDate != PL.accountEntryDate}">
                                            <%= sno + 1%> 
                                            <% sno++;%>
                                        </c:if>
                                    </td>
                                    <td   align="left"> 
                                        <c:if test="${accountEntryDate != PL.accountEntryDate}">
                                            <c:out value="${PL.accountEntryDate}"/>

                                        </c:if>
                                        <c:if test = "${accountEntryDate == PL.accountEntryDate}">
                                            &nbsp;
                                        </c:if>
                                    </td>
                                    <td  align="left">
                                        <c:if test="${entryStatus != PL.entryStatus}">
                                            <c:if test="${PL.entryStatus == 'CP'}"><spring:message code="finance.label.CashPayment"  text="default text"/></c:if>
                                            <c:if test="${PL.entryStatus == 'CR'}"><spring:message code="finance.label.CashReceipt"  text="default text"/></c:if>
                                            <c:if test="${PL.entryStatus == 'BP'}"><spring:message code="finance.label.BankPayment"  text="default text"/></c:if>
                                            <c:if test="${PL.entryStatus == 'BR'}"><spring:message code="finance.label.BankReceipt"  text="default text"/></c:if>
                                            <c:if test="${PL.entryStatus == 'CE'}"><spring:message code="finance.label.ContraEntry"  text="default text"/></c:if>
                                            <c:if test="${PL.entryStatus == 'JE'}"><spring:message code="finance.label.JournalEntry"  text="default text"/></c:if>

                                        </c:if>
                                        <c:if test = "${entryStatus == PL.entryStatus}">
                                            &nbsp;
                                        </c:if>

                                    </td>
                                    <td  align="left"> <c:out value="${PL.voucherCodeNo}" /></td>
                                    <c:if test="${entriesReportCreditList != null}">
                                        <c:set var="creditLedger" value=""/>
                                        <c:set var="totalCreditAmount" value="${0.00}"/>
                                        <c:forEach items="${entriesReportCreditList}" var="CL">
                                            <c:if test="${CL.creditVoucherCode == PL.voucherCode}">
                                                <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                                <c:set var="creditLedger" value="${creditLedger}${CL.creditLedgerName}"/>
                                            </c:if>
                                        </c:forEach>
                                        <td  align="left"> <c:out value="${creditLedger}" /></td>
                                    </c:if>
                                    <td   align="left"> <c:out value="${totalCreditAmount}"/> </td>
                                    <c:if test="${entriesReportDebitList != null}">
                                        <c:set var="debitLedger" value=""/>
                                        <c:set var="totalDebitAmount" value="${0.00}"/>
                                        <c:forEach items="${entriesReportDebitList}" var="DL">
                                            <c:if test="${DL.debitVoucherCode == PL.voucherCode}">
                                                <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                                <c:set var="debitLedger" value="${debitLedger}${DL.debitLedgerName}"/>

                                            </c:if>
                                        </c:forEach>
                                        <td  align="left"> <c:out value="${debitLedger}" /></td>
                                    </c:if>
                                    <td   align="left"> <c:out value="${totalDebitAmount}"/> </td>
                                    <td   align="left"> <c:out value="${PL.headerNarration}"/> </td>
                                    <c:set var="accountEntryDate" value="${PL.accountEntryDate}"/>
                                    <c:set var="entryStatus" value="${PL.entryStatus}"/>
                                </tr>
                                <c:set var="totalDebit" value="${totalDebit + totalDebitAmount}"/>
                                <c:set var="totalCredit" value="${totalCredit + totalCreditAmount}"/>


                                <% index++;%>
                            </c:forEach>
                            <tr height="30">
                                <td  colspan="5" align="right"> <b><spring:message code="finance.label.TotalCreditAmount"  text="default text"/></b></td>
                                <td  align="left"> <b><c:out value="${totalDebit}"/></b> </td>
                                <td  align="right"><b><spring:message code="finance.label.TotalDebitAmount"  text="default text"/> </b></td>
                                <td align="left"> <b><c:out value="${totalCredit}"/></b> </td>
                                <td  align="left"> </td>
                            </tr>

                        </table>
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                                    <option value="5"  selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
                            </div>
                            <div id="navigation" >
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </c:if>
                    <c:if test = "${entriesReportList != null && distcheck == 'distSummary'}" >

                        <br>
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.EntryType"  text="default text"/></th>
                            <th><spring:message code="finance.label.EntryDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.VoucherCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.CreditLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.CreditAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                            <!--</tr>-->
                            </thead>
                            <tbody>
                                <% int index = 0;%>
                                <% int sno = 0;%>
                                <c:set var="entryStatus" value=""/>
                                <c:set var="totalDebit" value="${0.00}"/>
                                <c:set var="totalCredit" value="${0.00}"/>
                                <c:forEach items="${entriesReportList}" var="PL">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text1";
                                                } else {
                                                    classText = "text2";
                                                }
                                    %>
                                    <tr height="30">
                                        <td   align="left">
                                            <c:if test="${entryStatus != PL.entryStatus}">
                                                <%= sno + 1%> 
                                                <% sno++;%>
                                            </c:if>
                                            <c:if test="${entryStatus == PL.entryStatus}">
                                                &nbsp;
                                            </c:if>
                                        </td>
                                        <td  align="left">
                                            <c:if test="${entryStatus != PL.entryStatus}">
                                                <c:if test="${PL.entryStatus == 'CP'}"><spring:message code="finance.label.CashPayment"  text="default text"/></c:if>
                                                <c:if test="${PL.entryStatus == 'CR'}"><spring:message code="finance.label.CashReceipt"  text="default text"/></c:if>
                                                <c:if test="${PL.entryStatus == 'BP'}"><spring:message code="finance.label.BankPayment"  text="default text"/></c:if>
                                                <c:if test="${PL.entryStatus == 'BR'}"><spring:message code="finance.label.BankReceipt"  text="default text"/></c:if>
                                                <c:if test="${PL.entryStatus == 'CE'}"><spring:message code="finance.label.ContraEntry"  text="default text"/></c:if>
                                                <c:if test="${PL.entryStatus == 'JE'}"><spring:message code="finance.label.JournalEntry"  text="default text"/></c:if>

                                            </c:if>
                                            <c:if test = "${entryStatus == PL.entryStatus}">
                                                &nbsp;
                                            </c:if>

                                        </td>
                                        <td   align="left"> <c:out value="${PL.accountEntryDate}"/> </td>
                                        <td  align="left"> <c:out value="${PL.voucherCodeNo}" /></td>
                                        <c:if test="${entriesReportCreditList != null}">
                                            <c:set var="creditLedger" value=""/>
                                            <c:set var="totalCreditAmount" value="${0.00}"/>
                                            <c:forEach items="${entriesReportCreditList}" var="CL">
                                                <c:if test="${CL.creditVoucherCode == PL.voucherCode}">
                                                    <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                                    <c:set var="creditLedger" value="${creditLedger}${CL.creditLedgerName}"/>
                                                </c:if>
                                            </c:forEach>
                                            <td  align="left"> <c:out value="${creditLedger}" /></td>
                                        </c:if>
                                        <td   align="left"> <c:out value="${totalCreditAmount}"/> </td>

                                        <c:if test="${entriesReportDebitList != null}">
                                            <c:set var="debitLedger" value=""/>
                                            <c:set var="totalDebitAmount" value="${0.00}"/>
                                            <c:forEach items="${entriesReportDebitList}" var="DL">
                                                <c:if test="${DL.debitVoucherCode == PL.voucherCode}">
                                                    <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                                    <c:set var="debitLedger" value="${debitLedger}${DL.debitLedgerName}"/>

                                                </c:if>
                                            </c:forEach>
                                            <td  align="left"> <c:out value="${debitLedger}" /></td>
                                        </c:if>
                                        <td   align="left"> <c:out value="${totalDebitAmount}"/> </td>
                                        <td   align="left"> <c:out value="${PL.headerNarration}"/> </td>
                                        <c:set var="entryStatus" value="${PL.entryStatus}"/>
                                    </tr>
                                    <c:set var="totalDebit" value="${totalDebit + totalDebitAmount}"/>
                                    <c:set var="totalCredit" value="${totalCredit + totalCreditAmount}"/>
                                    <% index++;%>
                                </c:forEach>
                                <tr height="30">
                                    <td  colspan="5" align="right" style="color: black"> <b><spring:message code="finance.label.TotalCreditAmount"  text="default text"/></b></td>
                                    <td  align="left" style="color: black"> <b><c:out value="${totalDebit}"/></b> </td>
                                    <td  align="right" style="color: black"><b><spring:message code="finance.label.TotalDebitAmount"  text="default text"/> </b></td>
                                    <td align="left" style="color: black"> <b><c:out value="${totalCredit}"/></b> </td>
                                    <td  align="left" style="color: black"> </td>
                                </tr>

                            </tbody>
                        </table>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                                    <option value="5"  selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
                            </div>
                            <div id="navigation" >
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>