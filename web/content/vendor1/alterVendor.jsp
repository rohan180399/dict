<%-- 
    Document   : addVendor
    Created on : Mar 8, 2009, 12:48:39 PM
    Author     : karudaiyar Subramaniam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.vendor.business.VendorTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>

    </head>
    <script>
        function submitpage()
        {
              
            if(textValidation(document.dept.vendorName,"vendorName")){
                return;
            }else if(textValidation(document.dept.tinNo,"tinNo")){
                return;
            }else if(isSelect(document.dept.vendorTypeId,"vendorType")){
                return;
            }else if(textValidation(document.dept.vendorAddress,"vendorAddress")){
                return;
       
            }else if(textValidation(document.dept.vendorPhoneNo,"vendorPhoneNo")){
                return;
       
            }else if(textValidation(document.dept.vendorMailId,"vendorMailId")){
                return;
       
            }
            else{

                var index = document.getElementsByName("selectedIndex");
                var mfrNames = document.getElementsByName("mfrNames");
                var chec=0;

                for(var i=0;(i<index.length && index.length!=0);i++){
                    if(index[i].checked){
                        chec++;
                        // alert("chk"+chec);
                    }
                }
         
                document.dept.action= "/throttle/alterVendor.do";
                document.dept.submit();
        
            }
        
        
        
        }
    
 

    </script>

    <body >
        <form name="dept"  method="post"   >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>



            <table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
                <% int index1 = 0;%>

                <c:if test="${GetVendorDetailUnique != null}">
                    <c:forEach items="${GetVendorDetailUnique}" var="vnd" >
                        <% if (index1 == 0) {%>
                        <tr height="30">
                            <Td colspan="5" class="contenthead">Alter Vendor</Td>
                        </tr>
                        <tr>
                            <td class="text1" height="30">Vendor Name</td>
                            <td class="text1" height="30">
                                <input type="hidden" name="vendorId" value=<c:out value="${vnd.vendorId}"/>  >
                                <input name="vendorName" type="text" class="form-control" value="<c:out value="${vnd.vendorName}"/>"  ></td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">Tin No</td>
                            <td class="text1" height="30">

                                <input name="tinNo" type="text" class="form-control" value="<c:out value="${vnd.tinNo}"/>"  ></td>
                        </tr>

                        <tr>


                            <td class="text2" height="30">Vendor Type </td>
                            <td class="text2"  >
                                <select class="form-control" name="vendorTypeId" style="width:125px" multiple="multiple">
                                    <option value="0">---Select---</option>
                                    <c:if test = "${VendorTypeList != null}" >
                                        <c:forEach items="${VendorTypeList}" var="Tp"> 
                                            <c:choose>
                                                <c:when test="${vnd.vendorTypeId == Tp.vendorTypeId}" >
                                                    <option selected value='<c:out value="${Tp.vendorTypeId}" />'><c:out value="${Tp.vendorTypeValue}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${Tp.vendorTypeId}" />'><c:out value="${Tp.vendorTypeValue}" /></option>
                                                </c:otherwise>
                                            </c:choose>                                            
                                        </c:forEach >
                                    </c:if>  	
                                </select></td>
                        </tr>
                        <tr>
                            <td class="text1" height="30">Vendor Address</td>
                            <td class="text2" height="30"><textarea class="form-control" name="vendorAddress"><c:out value="${vnd.vendorAddress}" /></textarea></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30">Vendor Phone No/Fax No</td>
                            <td class="text2" height="30"><input name="vendorPhoneNo" type="text" maxlength="45" class="form-control" value= <c:out value="${vnd.vendorPhoneNo}"/> ></td>
                        </tr>
                        <tr>
                            <td class="text1" height="30">Vendor Mail Id</td>
                            <td class="text1" height="30"><input name="vendorMailId" type="text" class="form-control" value=<c:out value="${vnd.vendorMailId}" />></td>

                        </tr>
                        <tr>
                            <td class="text2">Status</td>
                            <td class="text2"><select name="status" class="form-control"  style="width:125px" >
                                    <c:choose>
                                        <c:when test="${vnd.activeInd=='N' && vnd.activeInd=='n'}" >
                                            <option value="Y" >Active</option>
                                            <option value="N" selected>InActive</option>                                                                                        
                                        </c:when>
                                        <c:otherwise>
                                            <option selected value="Y" >Active</option>
                                            <option value="N">InActive</option>                                            

                                        </c:otherwise>    
                                    </c:choose>

                                </select></td>
                        </tr> 
                        <% index1++;
            }%>
                    </c:forEach>
                </c:if>
            </table>



            <br>

            <% int index = 0;%>
            <br>
            <c:if test = "${MfrList != null}" >
                <table width="400" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

                    <tr align="center">
                        <td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Manufacture Name</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Select</div></td>

                    </tr>
                    <%

                    %>

                    <c:forEach items="${MfrList}" var="list">

                        <%

                                    String classText = "";
                                    int oddEven = index % 2;
                                    int counter = 0;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <% ArrayList vendList = new ArrayList();
                                    vendList = (ArrayList) request.getAttribute("GetVendorDetailUnique");
                                    System.out.println("vendlist jsp" + vendList.size());
                        %>


                        <c:if test="${GetVendorDetailUnique != null}">
                            <c:forEach items="${GetVendorDetailUnique}" var="mfr" >
                                <c:if test="${ (mfr.mfrId == list.mfrId) && (mfr.activeInd=='Y') }" >

                                    <% counter++;%>
                                    <tr  width="208" height="40" >
                                        <td class="<%=classText%>" height="30"><%=index + 1%>-<c:out value="${list.mfrId}"/></td>
                                        <td class="<%=classText%>" height="30"><input type="hidden" name="mfrids" value='<c:out value="${list.mfrId}"/>'> <c:out value="${list.mfrName}"/></td>
                                        <td width="77" height="30" class="<%=classText%>"><input type="checkbox" checked name="selectedIndex" value='<%= index%>'></td>
                                    </tr>
                                </c:if>
                            </c:forEach >
                        </c:if>

                        <% if (counter == 0) {%>
                        <tr  width="208" height="40" >
                            <td class="<%=classText%>" height="30"><%=index + 1%>-<c:out value="${list.mfrId}"/></td>
                            <td class="<%=classText%>" height="30"><input type="hidden" name="mfrids" value='<c:out value="${list.mfrId}"/>'> <c:out value="${list.mfrName}"/></td>
                            <td width="77" height="30" class="<%=classText%>"><input type="checkbox"  name="selectedIndex" value='<%= index%>'></td>
                        </tr>
                        <%}
                                    counter = 0;
                                    index++;
                        %>

                    </c:forEach >

                </table>
            </c:if>


            <center>
                <input type="button" value="Alter" class="button" onClick="submitpage();">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

