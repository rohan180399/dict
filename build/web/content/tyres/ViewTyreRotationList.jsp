<%-- 
    Document   : ViewTyreRotationList
    Created on : 15 Mar, 2012, 5:59:24 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>ViewTyreRotationDetails</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }
            
        </script>

    </head>
    <body>

<form name="viewVehileListTyre" >

 <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Tyre Rotation List</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Vehicle No</td><td>
                <select name="vehicleId"  id="vehicleId" class="textbox" style="width:120px;">
                    <option value=""> -Select- </option>
                    <c:if test="${vehiclesList!=null}">
                    <c:forEach items="${vehiclesList}" var="veh" >                    
                    <option value='<c:out value="${veh.vehicleId}" />'><c:out value="${veh.vehicleRegNo}" /></option>
                    </c:forEach></c:if>
                </select>
            </td>
            <td>Make</td><td>
                <select class="textbox" name="vehicleModel" >
            <option value="">---Select---</option>
            <c:if test="${mfrList!=null}">
                    <c:forEach items="${mfrList}" var="mfr" >
                    <option value='<c:out value="${mfr.mfrId}" />'><c:out value="${mfr.mfrName}" /></option>
                    </c:forEach></c:if>
          </select>
            </td>
            <td>Model</td><td>
                <select class="textbox" name="vehicleModel" >
            <option value="">---Select---</option>           
          </select></td>
            </tr>
            <tr>
            <td><font color="red">*</font>From Date</td><td><input name="tripFromDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripFromDate,'dd-mm-yyyy',this)"/></td>
            <td><font color="red">*</font>To Date</td><td><input name="tripToDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripToDate,'dd-mm-yyyy',this)"/></td>
            <td colspan="2"><input type="button" name="search" value="View List" onClick="submitpage(this.name)" class="button" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
    <br>
    <table width="900" align="center" cellspacing="0" cellpadding="2">
    <tr>
        <td  class="contenthead">S.No</td>
        <td  class="contenthead">Vehicle No</td>
        <td  class="contenthead">Make</td>
        <td  class="contenthead">Model</td>
        <td  class="contenthead">Last Updated</td>
    </tr>
<%
int sino = 0;
String tdClassName = "text1";
%>

<c:if test="${vehicleDetails!=null}">

     <c:forEach items="${vehicleDetails}" var="vdl" >

<%

if((sino %2) == 0){
    tdClassName = "text2";
} else {
    tdClassName = "text1";
}
sino++;
%>
    <tr>
        <td class="<%=tdClassName%>"> <%=sino%></td>
        <td class="<%=tdClassName%>"><a href='/throttle/tyreRotation.do?vehicleId=<c:out value="${vdl.vehicleId}" />'><c:out value="${vdl.vehicleRegNo}" /></a></td>
        <td class="<%=tdClassName%>"><c:out value="${vdl.mfrName}" /></td>
        <td class="<%=tdClassName%>"><c:out value="${vdl.modelName}" /></td>
        <td class="<%=tdClassName%>">Last Updated</td>
    </tr>
</c:forEach>
    </c:if>
    </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
