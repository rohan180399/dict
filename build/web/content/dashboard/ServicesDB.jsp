<%--
    Document   : ServicesDB
    Created on : Feb 3, 2012, 12:44:06 PM
    Author     : Administrator
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<html lang="en">
<head>

        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="prettify/prettify.js"></script>
        <script type="text/javascript" src="ui/js/json2.js"></script>

        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/jquery.ui.theme.css">
    <script src="js/jquery-1.4.4.js"></script>
    <script src="js/jquery.ui.core.js"></script>
    <script src="js/jquery.ui.widget.js"></script>
    <script src="js/jquery.ui.mouse.js"></script>
    <script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
    font: normal 12px Arial;
    text-transform:uppercase;
    padding-left:10px;
    font-weight:bold;
}

.link a  {
    color:#7f8ba5;
    text-decoration:none;
}

.link a:hover {
        color:#7f8ba5;
    text-decoration:underline;

}

</style>
    <style type="text/css">
            #expand {
                width:130%;
}
    .column { width: 450px; float: left; }
    .portlet { margin: 0 1em 1em 0; }
    .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
    .portlet-header .ui-icon { float: right; }
    .portlet-content { padding: 0.4em; }
    .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
    .ui-sortable-placeholder * { visibility: hidden; }
    </style>
    <script>
    $(function() {
        $( ".column" ).sortable({
            connectWith: ".column"
        });

        $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
            .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
                .end()
            .find( ".portlet-content" );

        $( ".portlet-header .ui-icon" ).click(function() {
            $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
            $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
        });

        $( ".column" ).disableSelection();
    });
    </script>
</head>
<body>



<%


String colors[] = {"0099FF","CCFB5D","FF0000", "FFFF00"};// red,yellow
String color = "";
String currentJC = "";
String weeklyJC = "";
String monthlyJC = "";
String yearlyJC = "";



    Connection conn = null;
    int count = 0;
    try{

        String fileName = "jdbc_url.properties";
        Properties dbProps = new Properties();
   

    InputStream is = getClass().getResourceAsStream("/"+fileName);
    dbProps.load(is);//this may throw IOException
    String dbClassName = dbProps.getProperty("jdbc.driverClassName");

    String dbUrl = dbProps.getProperty("jdbc.url");
    String dbUserName = dbProps.getProperty("jdbc.username");
    String dbPassword = dbProps.getProperty("jdbc.password");
    Class.forName(dbClassName).newInstance();
    conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

    String fleetTypeQry = " SELECT vehicle_type_name,count(*) as tot FROM papl_model_master mm, "
                         + "papl_vehicle_type_master tm, papl_vehicle_master vm WHERE mm.vehicle_type_id = "
                            + "tm.vehicle_type_id AND  vm.model_id = mm.model_id AND tm.active_ind = 'Y' "
                           + "AND mm.active_ind = 'Y' AND vm.active_ind = 'Y' GROUP BY vehicle_type_name ";

    String jcStatusCurrent = "SELECT status,count(*) FROM papl_jobcard_master WHERE date_format(created_date,'%d-%m-%Y') ="
            + " date_format(now(),'%d-%m-%Y') GROUP BY status";
   
    String jcStatusWeek = "SELECT status,count(*) FROM papl_jobcard_master WHERE WEEK(created_date,2) ="
            + " WEEK(now(),2) GROUP BY status";

    String jcStatusMonth = "SELECT status,count(*) FROM papl_jobcard_master WHERE date_format(created_date,'%m-%Y') ="
            + " date_format(now(),'%m-%Y') GROUP BY status";

    String jcStatusYear = "SELECT status,count(*) FROM papl_jobcard_master WHERE date_format(created_date,'%Y') ="
            + " date_format(now(),'%Y') GROUP BY status";

    PreparedStatement pstmCurrent = conn.prepareStatement(jcStatusCurrent);
    System.out.print(pstmCurrent);
    PreparedStatement pstmWeek = conn.prepareStatement(jcStatusWeek);
    PreparedStatement pstmMonth = conn.prepareStatement(jcStatusMonth);
    PreparedStatement pstmYear = conn.prepareStatement(jcStatusYear);

    ResultSet res = pstmCurrent.executeQuery();
    while(res.next()) {        //Billing Completed; Completed  NotPlanned Planned
        if(res.getString(1).equalsIgnoreCase("Billing Completed")){
            color = colors[0];
        }
        if(res.getString(1).equalsIgnoreCase("Completed")){
            color = colors[1];
        }        
        if(res.getString(1).equalsIgnoreCase("NotPlanned")){
            color = colors[2];
        }
        if(res.getString(1).equalsIgnoreCase("Planned")){
            color = colors[3];
        }
         currentJC = currentJC+" <set value='"+res.getString(2)+"' label='"+res.getString(1)+"' color='"+color+"' alpha='60'/> ";// color='CCFB5D'
    }
    res = pstmWeek.executeQuery();
    while(res.next()) {        
        if(res.getString(1).equalsIgnoreCase("Billing Completed")){
            color = colors[0];
        }
        if(res.getString(1).equalsIgnoreCase("Completed")){
            color = colors[1];
        }        
        if(res.getString(1).equalsIgnoreCase("NotPlanned")){
            color = colors[2];
        }
        if(res.getString(1).equalsIgnoreCase("Planned")){
            color = colors[3];
        }
         weeklyJC = weeklyJC+" <set value='"+res.getString(2)+"' label='"+res.getString(1)+"' color='"+color+"' alpha='60'/> ";// color='CCFB5D'
    }
    res = pstmMonth.executeQuery();
    while(res.next()) {        
         if(res.getString(1).equalsIgnoreCase("Billing Completed")){
            color = colors[0];
        }
        if(res.getString(1).equalsIgnoreCase("Completed")){
            color = colors[1];
        }        
        if(res.getString(1).equalsIgnoreCase("NotPlanned")){
            color = colors[2];
        }
        if(res.getString(1).equalsIgnoreCase("Planned")){
            color = colors[3];
        }
         monthlyJC = monthlyJC+" <set value='"+res.getString(2)+"' label='"+res.getString(1)+"' color='"+color+"' alpha='60'/> ";// color='CCFB5D'
    }
    res = pstmYear.executeQuery();
    while(res.next()) {        
        if(res.getString(1).equalsIgnoreCase("Billing Completed")){
            color = colors[0];
        }
        if(res.getString(1).equalsIgnoreCase("Completed")){
            color = colors[1];
        }        
        if(res.getString(1).equalsIgnoreCase("NotPlanned")){
            color = colors[2];
        }
        if(res.getString(1).equalsIgnoreCase("Planned")){
            color = colors[3];
        }
         yearlyJC = yearlyJC+" <set value='"+res.getString(2)+"' label='"+res.getString(1)+"' color='"+color+"' alpha='60'/> ";// color='CCFB5D'
    }

   

    



%>

    <%
currentJC = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1'showLabels='1' showLegend='1'>"+currentJC+"</chart>";
weeklyJC = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1'showLabels='1' showLegend='1'>"+weeklyJC+"</chart>";
monthlyJC = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1'showLabels='1' showLegend='1'>"+monthlyJC+"</chart>";
yearlyJC = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1'showLabels='1' showLegend='1'>"+yearlyJC+"</chart>";
%>

<div id="expand" >
<div class="column" >
    <div class="portlet" >
            <div class="portlet-header">&nbsp;&nbsp;Current Job Card Status</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=currentJC %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="430" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
    </div>
       
    <div class="portlet">
        <div class="portlet-header">Job Card Status MTD</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=monthlyJC %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="430" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
    </div>
 
   
</div>
    <div class="column" >
    <div class="portlet" >
            <div class="portlet-header">&nbsp;&nbsp;Job Card Status WTD</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=weeklyJC %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="430" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                        </td>
                    </tr>
                    </table>
                    </div>
    </div>



  


    <div class="portlet">
        <div class="portlet-header">Job Card Status YTD</div>
        <div class="portlet-content">

                    <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/throttle/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%= yearlyJC%>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="430" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>

                        </td>
                    </tr>
                    </table>

        </div>
</div>
  


   



</div><!-- End demo -->

<%
    if(res != null) {
        res.close();
    }

}catch (FileNotFoundException fne){
    System.out.println("File Not found "+fne.getMessage());
} catch (SQLException se){
    System.out.println("SQL Exception "+se.getMessage());
}finally{
    if(conn == null) {
        conn.close();
    }
}

%>
</body>
</html>