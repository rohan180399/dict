<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" charset="utf-8">
            $(function () {
                var tabContainers = $('div.tabs > div');
                tabContainers.hide().filter(':first').show();

                $('div.tabs ul.tabNavigation a').click(function () {
                    tabContainers.hide();
                    tabContainers.filter(this.hash).show();
                    $('div.tabs ul.tabNavigation a').removeClass('selected');
                    $(this).addClass('selected');
                    return false;
                }).filter(':first').click();
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <style type="text/css" media="screen">

            UL.tabNavigation {
                list-style: none;
                margin: 0;
                padding: 0;
            }

            UL.tabNavigation LI {
                display: inline;
            }

            UL.tabNavigation LI A {
                padding: 3px 5px;
                background-color: #60a3e8;
                color: #000;
                text-decoration: none;
            }

            UL.tabNavigation LI A.selected,
            UL.tabNavigation LI A:hover {
                background-color: #76b3f1;
                color: #fff;
                padding-top: 7px;
            }

            UL.tabNavigation LI A:focus {
                outline: 0;
            }

            div.tabs > div {
                padding: 5px;
                margin-top: 3px;
            }

            div.tabs > div h2 {
                margin-top: 0;
                width:98%;
            }

            #first {
                background-color: #76b3f1;
            }

            #second {
                background-color: #76b3f1;
            }

            #third {
                background-color: #76b3f1;
            }

            #exp {
                height:55px;
                background:#e7f3ff;
            }

            .txt1 {
                color:#a0a9b8;
            }

            .txt1 a {
                color:#a0a9b8;
                text-decoration:none;
            }
            .txt1 a:hover {
                color:#000;
                text-decoration:underline;
            }


        </style>
    </head>
    <script type="text/javascript">
        var ticks=600;
        var mainwindow = null;
        var mainwindow1 = null;
        function createInterval(){
            setInterval("setcolr();", ticks);
        }
        function setcolr(){
            if( document.getElementById('blk').className == 'blink'){
                document.getElementById('blk').className ='blink1';
                document.getElementById('blk1').className ='blink1';
                document.getElementById('blk2').className ='blink1';
                document.getElementById('blk3').className ='blink1';
                document.getElementById('blk4').className ='blink1';
                document.getElementById('blk5').className ='blink1';
                document.getElementById('blk6').className ='blink1';
                document.getElementById('blk7').className ='blink1';
            }else{
                document.getElementById('blk').className ='blink';
                document.getElementById('blk1').className ='blink';
                document.getElementById('blk2').className ='blink';
                document.getElementById('blk3').className ='blink';
                document.getElementById('blk4').className ='blink';
                document.getElementById('blk5').className ='blink';
                document.getElementById('blk6').className ='blink';
                document.getElementById('blk7').className ='blink';
            }
        }
        function submitPage(value)
        {
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                var temp="";
                if(value == "GoTo"){
                    temp = document.viewTrips.GoTo.value;
                    if(temp != 'null'){
                        document.viewTrips.pageNo.value = temp;
                    }
                }
                else if(value == "First"){
                    temp ="1";
                    document.viewTrips.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewTrips.last.value;
                    document.viewTrips.pageNo.value = temp;
                    value='GoTo';
                }
            }
            //document.viewTrips.button.value=value;
            //var fromDate=document.viewTrips.fromDate.value;
            //var toDate=document.viewTrips.toDate.value;
            if(value == 'Fetch Data'){
                if(textValidation(document.viewTrips.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.viewTrips.toDate,'To Date')){
                    return;
                }
                document.viewTrips.action="/throttle/searchOverAllTripDetails.do?param=No";
                document.viewTrips.submit();            
            }
            if(value == 'Export to Excel'){
                if(textValidation(document.viewTrips.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.viewTrips.toDate,'To Date')){
                    return;
                }
                document.viewTrips.action="/throttle/searchOverAllTripDetails.do?param=ExportExcel";
                document.viewTrips.submit();
            }
        }        
        function setFocus() {
            var tripId='<%=request.getAttribute("tripId")%>';
            var regno='<%=request.getAttribute("regno")%>';            
            var driId='<%=request.getAttribute("driId")%>';
            var status='<%=request.getAttribute("status")%>';
            var locId='<%=request.getAttribute("locId")%>';
            var custId='<%=request.getAttribute("custId")%>';
            var sdate='<%=request.getAttribute("fromDate")%>';
            var edate='<%=request.getAttribute("toDate")%>';
            var ownership='<%=request.getAttribute("ownership")%>';

            if(tripId!='null'){
                document.viewTrips.tripId.value=tripId;
            }
            if(regno!='null'){
                document.viewTrips.regno.value=regno;
            }
            if(driId!='null'){
                document.viewTrips.driId.value=driId;
            }
            if(status!='null'){
                document.viewTrips.status.value=status;
            }
            if(locId!='null'){
                document.viewTrips.locId.value=locId;
            }
            if(custId!='null'){
                document.viewTrips.custId.value=custId;
            }
            if(ownership!='null'){
                document.viewTrips.ownership.value=ownership;
            }
            if(sdate!='null' && edate!='null'){
                document.viewTrips.fromDate.value=sdate;
                document.viewTrips.toDate.value=edate;
            }
        }
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }
        function newWindow(jobcardId,workorderId,jcMYFormatNo ){
            window.open('/throttle/previousJobCard.do?jobcardId='+jobcardId+"&workOrderId="+workorderId+"&jcMYFormatNo="+jcMYFormatNo, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }
        function displayCollapse(){
            if(document.getElementById("exp_table").style.display=="block"){
                document.getElementById("exp_table").style.display="none";
                document.getElementById("openClose").innerHTML="Open";
            } else{
                document.getElementById("exp_table").style.display="block";
                document.getElementById("openClose").innerHTML="Close";
            }
        }
        function callGoogleMap(rName, regNo){
            var loc=rName.split("-");           
            if(mainwindow==null || mainwindow.closed){
                mainwindow=window.open('/throttle/content/RenderService/route_map.html?start='+loc[0]+'&end='+loc[1]+'&regNo='+regNo,'mywindow','width=1200,height=700,left=0,top=10,screenX=0,screenY=100');
                //window.open('/throttle/content/RenderService/route_map.html?route='+rObj,'mywindow','width=1200,height=700,left=0,top=10,screenX=0,screenY=100');
            }else{
                    mainwindow.focus();
            }            
        }
        function callGoogleMap1(rName, regNo){
            var loc=rName.split("-");
            if(mainwindow1==null || mainwindow1.closed){
                mainwindow1=window.open('/throttle/content/RenderService/route_map.jsp?start='+loc[0]+'&end='+loc[1]+'&regNo='+regNo,'mywindow','width=1200,height=700,left=0,top=10,screenX=0,screenY=100');
                //window.open('/throttle/content/RenderService/route_map.html?route='+rObj,'mywindow','width=1200,height=700,left=0,top=10,screenX=0,screenY=100');
            }else{
                    mainwindow1.focus();
            }
        }
    </script>
    <body onload="setFocus();getVehicleNos();show_exp();">
        <form name="viewTrips" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="950" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">&nbsp;Trip Id</td>
                                    <td  height="30">
                                        <input name="tripId" id="tripId" type="text"  class="form-control" size="20" style="width:123px; " value="">
                                    </td>
                                    <td >&nbsp;Vehicle No</td>
                                    <td  ><input name="regno" id="regno" type="text"  class="form-control" size="20" style="width:123px; " value=""></td>
                                    <td  height="30">Driver Name</td>
                                    <td  height="30">
                                        <select class='form-control' style="width:123px; " id='driId'  name='driId' >
                                            <option selected  value="0">---Select---</option>
                                            <c:if test = "${driverName != null}" >
                                                <c:forEach items="${driverName}" var="dri">
                                                    <option  value='<c:out value="${dri.empId}" />'>
                                                        <c:out value="${dri.empName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>
                                    </td>
                                    <td >Status</td>
                                    <td ><select class="form-control" style="width:123px; " name="status">
                                            <option value="0">All</option>
                                            <option value="Open">Open</option>
                                            <option value="Close">Close</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td  height="30">Operation Location</td>
                                    <td  height="30">
                                        <select class='form-control' style="width:123px; " id='locId'  name='locId' >
                                            <option selected  value="0">---Select---</option>
                                            <c:if test = "${opLocation != null}" >
                                                <c:forEach items="${opLocation}" var="opl">
                                                    <option  value='<c:out value="${opl.locationId}" />'>
                                                        <c:out value="${opl.locationName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>
                                    </td>                                    
                                    <td  height="30"><font color="red">*</font>From Date</td>
                                    <td  height="30"><input type="text" name="fromDate" class="datepicker" ></td>
                                    <td ><font color="red">*</font>To Date</td>
                                    <td ><input type="text" name="toDate" class="datepicker" ></td>
                                <td >Ownership</td>
                                    <td ><select class="form-control" style="width:123px; " name="ownership">
                                            <option value="0">All</option>
                                            <option value="1">Own</option>
                                            <option value="2">Attach</option>
                                        </select>
                                    </td>
                                </tr>
                                    <tr>
                                    <td  height="30" colspan="8">
                                        <center>
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
                        <c:if test = "${tripDetails != null}" >
                            <%
                                        DecimalFormat df = new DecimalFormat("0.00##");
                                        int index = 1;
                                        int total = 0;
                                        String classText2 = "";
                                        String imageName="";
                                        String regno = "",tripDate = "",routeName = "",empName = "",outKM = "",freight = "";
                                        String inKM = "",totalTonnage = "",deliveredTonnage = "",status = "",tripId = "";
                                        double grandTotalTonnage = 0,grandTotalFreight = 0,grandDeliveredTonnage = 0;
                                        ArrayList tripDetails = (ArrayList) request.getAttribute("tripDetails");
                                        Iterator tripDet = tripDetails.iterator();
                                        OperationTO optTO = null;
                                        int flag  = 0;
                                        if (tripDetails.size() != 0) {
                            %>
                            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td class="table">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                                            <tr>
                                            <td class="bottom" align="left"><img src="images/left_status.jpg" alt=""  /></td>                                            
                                            <td class="bottom" height="35" align="right"><img src="images/icon_active.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left"><%= request.getAttribute("opened")%></span></td>
                                            <td class="bottom" align="right"><img src="images/icon_closed.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;"><%= request.getAttribute("closed")%></span></td>                                            
                                            <td align="center"><h2>Total  <%= request.getAttribute("totalTrip")%></h2></td>
                                            </tr>
                                            <tr>
                                            <td class="bottom" align="left"><img src="images/left_status.jpg" alt=""  /></td>
                                            <td class="bottom" height="35" align="right"><img src="images/icon_active.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left"><%= request.getAttribute("own")%></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td class="contentsub" height="30">&nbsp;</td>
                                    <td class="contentsub" height="30">S.No</td>
                                    <td class="contentsub" height="30">Trip Id</td>
                                    <td class="contentsub" height="30">Vehicle No</td>
                                    <td class="contentsub" height="30">Trip Date</td>
                                    <td class="contentsub" height="30">Route Name</td>                                    
                                    <td class="contentsub" height="30">Driver Name</td>
                                    <td class="contentsub" height="30">OUT KM</td>
                                    <td class="contentsub" height="30">IN KM</td>
                                    <td class="contentsub" height="30">Freight</td>
                                    <td class="contentsub" height="30">Total Tonnage</td>
                                    <td class="contentsub" height="30">Delivered Tonnage</td>                                    
                                    <td class="contentsub" height="30">Status</td>
                                    <td class="contentsub" height="30">&nbsp;</td>
                                    <td class="contentsub" height="30">&nbsp;</td>
                                    <!--<td class="contentsub" height="30">Map View</td>-->
                                </tr>
                                <%
                                while (tripDet.hasNext()) {
                                    index++;
                                    total++;
                                    int oddEven2 = index % 2;
                                    if (oddEven2 > 0) {
                                        classText2 = "text2";
                                        imageName="flag2";
                                    } else {
                                        classText2 = "text1";
                                        imageName="flag2";
                                    }

                                    
                                     optTO = new OperationTO();
                                    optTO = (OperationTO) tripDet.next();

                                    flag = optTO.getFlag();

                                    regno = optTO.getRegno();
                                    if(regno == null){
                                        regno = "";
                                        }
                                    tripId = optTO.getTripId();
                                    if(tripId == null){
                                        tripId = "";
                                        }
                                    tripDate = optTO.getTripDate();
                                    if(tripDate == null){
                                        tripDate = "";
                                        }
                                    routeName = optTO.getRouteName();
                                    if(routeName == null){
                                        routeName = "";
                                        }
                                    freight = optTO.getRevenue();
                                    if(freight == null){
                                        freight = "";
                                        }
                                    grandTotalFreight += Double.parseDouble(freight);
                                    empName = optTO.getEmpName();
                                    if(empName == null){
                                        empName = "";
                                        }
                                    outKM = optTO.getOutKM();
                                    if(outKM == null){
                                        outKM = "0";
                                        }
                                    inKM = optTO.getInKM();
                                    if(inKM == null){
                                        inKM = "0";
                                        }
                                    totalTonnage = optTO.getTotalTonnage();
                                    if(totalTonnage == null){
                                        totalTonnage = "";
                                        }
                                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                                    deliveredTonnage = optTO.getDeliveredTonnage();
                                    if(deliveredTonnage == null){
                                        deliveredTonnage = "0";
                                        }
                                    grandDeliveredTonnage += Double.parseDouble(deliveredTonnage);
                                    status = optTO.getStatus();
                                    if(status == null){
                                        status = "";
                                        }

                                    

                                        %>
                                        <tr>
                                            <%
                                            if(flag == 1){
                                                    %>
                                            <td  class="<%=classText2%>" height="30"><img width="15px" height="15px" src="/throttle/images/<%=imageName%>.gif" align="middle" border="0"></td>
                                            <%
                                                }else {
                                                    %>
                                             <td  class="<%=classText2%>" height="30">&nbsp;</td>
                                            <%
                                        }
                                        %>
                                        <td class="<%=classText2%>"  height="30" align="left"><%=index%></td>
                                        <td class="<%=classText2%>"  height="30"><a href="viewTripSheetNew.do?tripSheetIdParam=<%=tripId%>"><%=tripId%></a></td>
                                        <td class="<%=classText2%>"  height="30"><%=regno%></td>
                                        <td class="<%=classText2%>"  height="30"><%=tripDate%></td>
                                        <td class="<%=classText2%>"  height="30"><%=routeName%></td>
                                        <td class="<%=classText2%>"  height="30"><%=empName%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=outKM%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=inKM%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=freight%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=totalTonnage%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=deliveredTonnage%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=status%></td>
                                        <td class="<%=classText2%>"  height="30"><a href="viewPrintTripSheet.do?tripSheetIdParam=<%=tripId%>">print&nbsp;</a></td>
                                        <%
                                        if(status.equals("Close")){
                                            %>
                                         <td class="<%=classText2%>"  height="30"><a href="viewSettlementVoucher.do?tripId=<%=tripId%>">&nbsp;settlement</a></td>
                                        <%
                                            } else {
                                            %>
                                        <td class="<%=classText2%>"  height="30">&nbsp;</td>
                                         <%

                                        }
                                        %>
                                        </tr>

                                <%

                                   
                                    
                                    }
                                
%>
                        <tr>
                            <td class="<%=classText2%>"  height="30" colspan="7" >&nbsp;</td>
                            <td class="<%=classText2%>"  height="30" colspan="2"><b>Grand Total</b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandTotalFreight)%></b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandDeliveredTonnage)%></b></td>
                            <td class="<%=classText2%>"  height="30" colspan="3" >&nbsp;</td>
                        </tr>
                            </table>
                            <%
                                        }
                            %>

                      
                    </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>