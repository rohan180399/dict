<%-- 
    Document   : tripPlanningImport
    Created on : Nov 4, 2013, 11:17:44 PM
    Author     : Arul
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <title></title>
        <script>
            function submitPage() {
                document.tripPlanning.action = '/throttle/handleUploadTripPlanning.do';
                document.tripPlanning.submit();
            }
        </script>
    </head>
    <% String menuPath = "Operations >>  Upload Trip Planning";
        request.setAttribute("menuPath", menuPath);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String curDate = dateFormat.format(date);
    %>
    
    <body>
        <form name="tripPlanning" method="post" enctype="multipart/form-data">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <br>
            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Upload Trip Planning</td>
                </tr>
                <tr>
                    <td class="text2">Select file</td>
                    <td class="text2"><input type="file" name="importCnote" id="importCnote" class="form-control" ></td>
                    <td class="text2">Planning Date</td>
                    <td class="text2"><input type="text" name="planDate" id="planDate" class="form-control" value="<%=curDate%>" readonly></td>
                </tr>
                <tr >
                <td align="center" class="text2" colspan="4"><input type="button" value="Submit" name="Search" onclick="submitPage()">
                </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
