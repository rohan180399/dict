
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {

                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function () {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true,
                    dateFormat: 'dd-mm-yy'
                });

            });


        </script>
 <script type="text/javascript">
  
            
              function submitPage()
            {
                var errStr = "";
                var nameCheckStatus = $("#taxStatus").text();
                if (document.getElementById("taxName").value == "") {
                    errStr = "Please enter tax Name.\n";
                    alert(errStr);
                    document.getElementById("taxName").focus();                
                } else if (document.getElementById("taxCode").value == "") {
                    errStr = "Please enter tax Code.\n";
                    alert(errStr);
                    document.getElementById("taxCode").focus();                 
                } else if (document.getElementById("taxPercent").value == "") {
                    errStr = "Please enter tax Percent.\n";
                    alert(errStr);
                    document.getElementById("taxPercent").focus();                 
                } else if (document.getElementById("description").value == "") {
                    errStr = "Please enter Description.\n";
                    alert(errStr);
                    document.getElementById("description").focus();                 
                } else if (document.getElementById("effectiveDate").value == "") {
                    errStr = "Please enter Effective Date.\n";
                    alert(errStr);
                    document.getElementById("effectiveDate").focus();
                } 
                if (errStr == "") {
                    document.taxMaster.action = "/throttle/saveNewTax.do";
                    document.taxMaster.method = "post";
                    document.taxMaster.submit();
                }
            }
            
             var httpRequest;
            function checktaxName() {

                var taxName = document.getElementById('taxName').value;
                var url = '/throttle/checkTaxName.do?taxName=' + taxName;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);

            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        if (val != "" && val != 'null') {
                            $("#nameStatus").show();
                            $("#taxStatus").text('Please Check tax Name  :' + val + ' is Already Exists');
                        } else {
                            $("#nameStatus").hide();
                            $("#taxStatus").text('');
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }
            
                function setValues(sno, taxCode, taxName, taxPercent, effectiveDate, description,taxId) {
                
                var count = parseInt(document.getElementById("count").value);               
                
                for (i = 1; i <= count; i++) {
                    if (i != sno) {
                        document.getElementById("edit" + i).checked = false;
                    } else {
                        document.getElementById("edit" + i).checked = true;
                    }
                }
                document.getElementById("taxCode").value = taxCode;
                document.getElementById("taxName").value = taxName;
                document.getElementById("taxPercent").value = taxPercent;
                document.getElementById("effectiveDate").value = effectiveDate;
                document.getElementById("description").value = description;
                document.getElementById("taxId").value = taxId;

            }
        </script>
<style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>ManageTaxMaster</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Finance</a></li>
            <li class="active">ManageTaxMaster</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form method="post" name="taxMaster" class="form-horizontal form-bordered">
          <br>
                     <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
           <table class="table table-bordered">
                
                  <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b>Add TAX</b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                   <tr>
                        <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="taxStatus" style="color: red"></label></td>
                    </tr>
                 
                  <tr height="30">
                    <td ><font color="red">*</font>Tax Name</td>
                    <td ><input id="taxName" name="taxName" type="text" class="form-control" value="" style="width:260px;height:40px;" onchange="checktaxName();"></td>
                    <td ><font color="red">*</font>Tax Code</td>
                    <td ><input id="taxCode" name="taxCode" type="text" class="form-control" value="" style="width:260px;height:40px;"></td>
                  </tr>
                  <tr height="30">
                    <td ><font color="red">*</font>Tax Percentage</td>
                    <td ><input id="taxPercent" name="taxPercent" type="text" class="form-control" value="" style="width:260px;height:40px;"></td>
                    <td ><font color="red">*</font>Effective Date</td>
                    <td > 
                        <input type="textbox" id="effectiveDate" name="effectiveDate" value=""  style="width:260px;height:40px;" class="form-control datepicker">
                    </td>
                  </tr>
                  <tr height="30">
                    <td ><font color="red">*</font>Description</td>
                    <td ><textarea id="description" name="description" type="text" class="form-control" style="width:260px;height:40px;"></textarea></td>
                    <td >&nbsp;</td>
                    <td >&nbsp;
                    <input type="hidden" id="taxId" name="taxId" value=""  size="20">
                    </td>
                  </tr>
            </table>
<br>
<br>
<center>
    <input type="button" class="btn btn-success" value="Save" onclick="submitPage();" style="width:100px;height:35px;"/>
    &emsp;<input type="reset" class="btn btn-success" value="Clear" style="width:100px;height:35px;">
</center>
            
            <br>
            
            <c:if test = "${taxLists != null}" >
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <th  align="left" ><b>S.No</b></td>
                        <th  align="left" ><b>Tax code</b></td>
                        <th  align="left" ><b>Tax Name</b></td>
                        <th  align="left" ><b>Tax Percentage</b></td>
                        <th  align="left" ><b>Effective Date</b></td>
                        <th  align="left" ><b>Description</b></td>
                        <th  align="left" ><b>Status</b></td>
                        <th  align="left" ><b>Edit</b></td>
                    </thead>
                    <tbody>
                    <% 
                    int index = 0;  
                    int sno = 0;
                    %>
                    <c:forEach items="${taxLists}" var="TL">
                        <%
                            sno++;
                           
                        %>
                        <tr height="30">
                            <td   align="left"> <%= index + 1%> </td>
                            <td  align="left"> <c:out value="${TL.taxCode}" /></td>
                            <td   align="left"> <c:out value="${TL.taxName}"/> </td>
                            <td   align="left"> <c:out value="${TL.taxPercent}"/> </td>
                            <td   align="left"> <c:out value="${TL.effectiveDate}"/> </td>
                            <td   align="left"> <c:out value="${TL.description}"/> </td>                            
                           
                             <c:if test="${TL.active_ind == 'Y'}">
                                    <td   height="30"><c:out value="Active"/></td>
                                </c:if>
                                <c:if test="${TL.active_ind == 'N'}">
                                    <td   height="30"><c:out value="In Active"/></td>
                                </c:if>
                            
                            <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%=sno%>,'<c:out value="${TL.taxCode}" />','<c:out value="${TL.taxName}"/>','<c:out value="${TL.taxPercent}"/>','<c:out value="${TL.effectiveDate}"/>','<c:out value="${TL.description}"/>','<c:out value="${TL.taxId}"/>');"/></td>                            
                        </tr>
                        <% index++;%> 
                    </c:forEach>
                    </tbody>
                 <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>
            
             <br>
            <br>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
            
            <br>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
            <br>
            <br>
            <br>
            <br>
            <br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

