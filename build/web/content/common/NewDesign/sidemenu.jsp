
<%@page import="ets.domain.users.business.LoginTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<!--      <h5 class="sidebartitle">Navigation</h5>-->

<%
    Object rolId = session.getAttribute("RoleId");
    
%>
<form>

<ul class="nav nav-pills nav-stacked nav-bracket">

    <% ArrayList menuList = (ArrayList) session.getAttribute("menuList");
        if (menuList != null) {
            Iterator itr = menuList.iterator();
            LoginTO loginTO = new LoginTO();
            String paramDetails = "";
            while (itr.hasNext()) {
                loginTO = (LoginTO) itr.next();

    %>
    <li class="nav-parent"><a href=""><i class="<%=loginTO.getIconName()%>"></i> <span><spring:message code="<%=loginTO.getLabelName()%>" text="<%=loginTO.getDefaultLabelName()%>"/></span></a>
        <% ArrayList subMenuList = (ArrayList) loginTO.getUserSubMenuList();
            if (subMenuList.size() > 0) {
                Iterator itr1 = subMenuList.iterator();
                LoginTO logTO = new LoginTO();
                
                %>
                <ul class="children">
               <%
                while (itr1.hasNext()) {
                    logTO = (LoginTO) itr1.next();
                    paramDetails = logTO.getParamDetails();
                    paramDetails = logTO.getMethodName() + "?menuClick=1"+paramDetails;
        %>
            <li><a href="/throttle/<%=paramDetails%>"><i class="<%=logTO.getSubMenuIconName()%>"></i><spring:message code="<%=logTO.getSubMenuLabelName()%>" text="<%=logTO.getSubMenuDefaultLabelName()%>"/> </a></li>
        <%
                }
            }
        %>
        </ul>
    </li>
    <%                }
        }

    %>

    <!-- =============================HR Section=============================================== -->
</ul>
</form>


</div><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">

    <div class="headerbar">

        <a class="menutoggle"><i class="fa fa-bars"></i></a>



        <div class="header-right">
            <ul class="headermenu">

<!--                <li>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-globe"></i>
                            <span class="badge">1</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title"><spring:message code="sidemenu.label.YouHave5NewNotifications" text="default text"/></h5>
                            <ul class="dropdown-list gen-list">
                                <li class="new">
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                            <span class="name"><spring:message code="sidemenu.label.NewJobcard1960created" text="default text"/> <span class="badge badge-success"><spring:message code="sidemenu.label.new" text="default text"/></span></span>
                                        </span>
                                    </a>
                                </li>

                                <li class="new"><a href=""><spring:message code="sidemenu.label.SeeAllNotifications" text="default text"/></a></li>
                            </ul>
                        </div>
                    </div>
                </li>-->
                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <!--                <img src="/throttle/content/NewDesign/images/photos/loggeduser.png" alt="" />-->
                            Welcome. <%= session.getAttribute("userName")%>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <!--                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>-->
                            <!--<li><a href="#"><i class="glyphicon glyphicon-cog"></i> <spring:message code="sidemenu.label.AccountSettings" text="default text"/></a></li>-->
                            <!--<li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> <spring:message code="sidemenu.label.Help" text="default text"/></a></li>-->
                            <li><a href="/throttle/logout.do?menuClick=1"><i class="glyphicon glyphicon-log-out"></i><spring:message code="sidemenu.label.LogOut" text="default text"/></a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div><!-- header-right -->

    </div><!-- headerbar -->

    <!--sidemenu.label.LogOut-->