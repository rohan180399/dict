<%-- 
    Document   : displayTripSheetWiseExcel
    Created on : Jun 11, 2013, 10:38:22 PM
    Author     : Entitle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="tripwise" action=""  method="post">
            <%
                        DecimalFormat df = new DecimalFormat("0.00##");
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Trip_Sheet_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
            <%
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",crossing = "";
                            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "";
                            String consigneeName = "",invoiceNo = "";
                            double grandTotalFreight = 0,subTotalFreight = 0,totalSettlement = 0,grandTotalSettlementAmount = 0,subTotalSettlementAmount = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0,subTotalExpenses = 0,grandTotalExpenses = 0,totalCrossing = 0;
                            String className = "text2",settlementAmount = "";
                            int index = 0;
                            int sno = 0;
%>
            <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <c:if test = "${tripSheetDetailsList != null}" >
                                <%
                                 ArrayList tripSheetDetailsList = (ArrayList) request.getAttribute("tripSheetDetailsList");
                                Iterator detailsItr = tripSheetDetailsList.iterator();
                                ReportTO repTO = null;
                                 if (tripSheetDetailsList.size() != 0) {
                                 %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Consignee Name</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">GP No</td>
                                <td class="contentsub" height="30">Invoice No</td>
                                <td class="contentsub" height="30">Destination</td>
                                <td class="contentsub" height="30">Distance</td>
                                <td class="contentsub" height="30">Tonnage Total</td>
                                <td class="contentsub" height="30">Freight</td>
                                <td class="contentsub" height="30">Crossing</td>
                                <td class="contentsub" height="30">Settlement Amount</td>
                </tr>
                            <%
                                while (detailsItr.hasNext()) {
                                         index++;
                                        sno++;
                                            if(index%2 == 0){
                                            className = "text2";

                                            }else {
                                            className = "text1";
                                        }
                                        repTO = new ReportTO();
                                        repTO = (ReportTO) detailsItr.next();
                                        tripId  = repTO.getTripId();
                                        if(tripId == null){
                                            tripId = "";
                                            }
                                        tripDate  = repTO.getTripDate();
                                        if(tripDate == null){
                                            tripDate = "";
                                            }
                                        consigneeName  = repTO.getConsigneeName();
                                        if(consigneeName == null){
                                            consigneeName = "";
                                            }
                                        vehicleNumber  = repTO.getVehicleId();
                                        if(vehicleNumber == null){
                                            vehicleNumber = "";
                                            }
                                        gpno  = repTO.getGpno();
                                        if(gpno == null){
                                            gpno = "";
                                            }
                                        invoiceNo  = repTO.getInvoiceNo();
                                        if(invoiceNo == null){
                                            invoiceNo = "";
                                            }
                                        destination  = repTO.getDestination();
                                        if(destination == null){
                                            destination = "";
                                            }
                                        distance  = repTO.getDistance();
                                        if(distance == null){
                                            distance = "";
                                            }
                                        crossing  = repTO.getCrossing();
                                        totalCrossing += Double.parseDouble(crossing);
                                        if(crossing == null){
                                            crossing = "";
                                            }
                                        totalTonnage  = repTO.getTotalTonnage();
                                        if(totalTonnage == null){
                                            totalTonnage = "";
                                            }
                                        grandTotalTonnage += Double.parseDouble(totalTonnage);
                                        totalFreightAmount  = repTO.getTotalFreightAmount();
                                        if(totalFreightAmount == null){
                                            totalFreightAmount = "";
                                            }
                                         grandTotalFreight += Double.parseDouble(totalFreightAmount);
                                        settlementAmount  = repTO.getSettlementAmount();
                                        subTotalSettlementAmount += Double.parseDouble(settlementAmount);
                                       grandTotalSettlementAmount += Double.parseDouble(settlementAmount);
                                        if(settlementAmount == null){
                                            settlementAmount = "";
                                            }
                                       
                                        
                                        %>
                <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%></td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                          <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                          <td class="<%=className%>"  height="30"><%=gpno%></td>
                          <td class="<%=className%>"  height="30"><%=invoiceNo%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>"  height="30"><%=distance%></td>
                          <td class="<%=className%>"  height="30"><%=totalTonnage%></td>
                          <td class="<%=className%>"  height="30"><%=totalFreightAmount%></td>
                          <td class="<%=className%>"  height="30"><%=crossing%></td>
                          <td class="<%=className%>"  height="30"><%=settlementAmount%></td>
                        </tr>
                <%

                                    }
                                if(className.equalsIgnoreCase("text1")){
                                    className = "text2";
                                    }else{
                                    className = "text1";
                                    }
                                %>
                                <tr>
                                    <td class="<%=className%>"  height="30" colspan="8">&nbsp;</td>
                                    <td class="<%=className%>" align="right" height="30" ><b>Total</b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(grandTotalTonnage)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(grandTotalFreight)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(totalCrossing)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(subTotalSettlementAmount)%></b></td>
                                </tr>
                        <%
                                     }
%>

                            </c:if>
              </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
