<%-- 
    Document   : redirectPage
    Created on : Mar 19, 2014, 2:15:04 AM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
<%
    String redirectURL = "/throttle/viewTripSheets.do?statusId="+request.getAttribute("statusId")+"&tripType="+request.getAttribute("tripType");
    response.sendRedirect(redirectURL);
%>
    </body>
</html>
