<%-- 
    Document   : dischargeVessel
    Created on : Feb 16, 2016, 5:58:45 PM
    Author     : hp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">-->
<!--<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>-->

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#dischargeVesselName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVesselName.do",
                    dataType: "json",
                    data: {
                        dischargeVesselName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            alert('Add Vessel First');
                            $('#vesselId').val('');
                            $('#dischargeVesselName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                                
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vesselId').val(tmp[0]);
                $('#dischargeVesselName').val(tmp[1]);

                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    function savesubmitPage() {

        var dischargeVesselName = document.getElementById("dischargeVesselName").value;
        var vesselId = document.getElementById("vesselId").value;
        var voyageNo = document.getElementById("voyageNo").value;
        var linersName = document.getElementById("linersName").value;
        var agentName = document.getElementById("agentName").value;
        var plannedArrivalDate = document.getElementById("plannedArrivalDate").value;
        var plannedArrivalPort = document.getElementById("plannedArrivalPort").value;
        var plannedDischargeDate = document.getElementById("plannedDischargeDate").value;
        var plannedDischargePort = document.getElementById("plannedDischargePort").value;
        var status = document.getElementById("status").value;
        var description = document.getElementById("description").value;
        var updateStatus = 0;
        if (document.dischargeVessel.dischargeVesselName.value == '') {
            alert('Please Enter Vessel Name ');
            $("#dischargeVesselName").focus();
            return;
        } else if (document.dischargeVessel.voyageNo.value == '') {
            alert('Please Enter Voyage No');
            $("#voyageNo").focus();
            return;
        } else if (document.dischargeVessel.linersName.value == '') {
            alert('Please Enter Liners Name');
            $("#linersName").focus();
            return;
        } else if (document.dischargeVessel.agentName.value == '') {
            alert('Please Enter Agent Name');
            $("#agentName").focus();
            return;
        }else if (document.dischargeVessel.plannedArrivalDate.value == '') {
            alert('Please Select PlannedArrivalDate');
            $("#plannedArrivalDate").focus();
            return;
        }else if (document.dischargeVessel.plannedArrivalPort.value == '') {
            alert('Please Enter PlannedArrivalPort');
            $("#plannedArrivalPort").focus();
            return;
        }else if (document.dischargeVessel.plannedDischargeDate.value == '') {
            alert('Please Select PlannedDischargeDate ');
            $("#plannedDischargeDate").focus();
            return;
        }
        else if (document.dischargeVessel.description.value == '') {
            alert('Please Entert Description ');
            $("#description").focus();
            return;
        }
        var url = '';
        url = './savedischargeVessel.do';
        $.ajax({
            url: url,
            data: {dischargeVesselName: dischargeVesselName, vesselId: vesselId, voyageNo: voyageNo, linersName: linersName,
                agentName: agentName, plannedArrivalDate: plannedArrivalDate, plannedArrivalPort: plannedArrivalPort,
                plannedDischargeDate: plannedDischargeDate,plannedDischargePort:plannedDischargePort,status: status,description: description
            },
            type: "GET",
            success: function(response) {
                updateStatus = response.toString().trim();
                if (updateStatus == 0) {
                    $("#DischargeStatus").text("Discharge Vessel added failed ");
                } else {
                    insertStatus = updateStatus;
                    $("#DischargeStatus").text(" Discharge Vessel added sucessfully ");
                }
            },
            error: function(xhr, status, error) {
            }
        });
        document.getElementById("discharegesave").style.visibility = 'hidden';
    }

    function addSlabRate() {

        $("#slapRateHead").text();
        $('#slabRateListSet').html('');
        var url = '/throttle/manageVessel.do';
        $.ajax({
            url: url,
            type: "get",
            success: function(data)
            {
                $('#slabRateListSet').html(data);
            }
        });
    }

</script>




<body>
    <form name="dischargeVessel"  method="post" >
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <font color="green" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">

            <div align="center" id="DischargeStatus">&nbsp;&nbsp;
            </div>
        </font>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bg" class="border">
            <tr align="center">
                <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Add Vessel</div></td>
            </tr>

            <tr>
                <td class="text2" height="30"><font color="red">*</font>Vessel Name
                </td>

                <td class="text2" height="30">
                    <input name="dischargeVesselName" id="dischargeVesselName" type="text" class="form-control" value="" maxlength="50">
                    <a href="" data-toggle="modal" data-target="#myModal" onclick="addSlabRate();" style="color:#0000ff">Add Vessel</a>
                    <input name="vesselId" id="vesselId" type="hidden" class="form-control" value="" >
                </td>
                <td class="text2" height="30"><font color="red">*</font>Voyage No
                </td>
                <td class="text2" height="30"><input name="voyageNo" id="voyageNo" type="text" class="form-control" value="" maxlength="50"></td>

            </tr>
            <tr>

                <td class="text1" height="30"><font color="red">*</font>Liners Name</td>
                <td class="text2" height="30"><input name="linersName" id="linersName" type="text" class="form-control" value="" maxlength="50"></td>
                <td class="text1" height="30"><font color="red">*</font>Agent Name</td>
                <td class="text2" height="30"><input name="agentName" id="agentName" type="text" class="form-control" value="" maxlength="50"></td>
            </tr>
            <tr>

                <td class="text1" height="30"><font color="red">*</font>Planned Arrival Date</td>
                <td class="text2" height="30"><input name="plannedArrivalDate" id="plannedArrivalDate" type="text"  class="datepicker" value="" ></td>
                <td class="text1" height="30"><font color="red">*</font>planned Arrival Port</td>
                <td class="text2" height="30"><input name="plannedArrivalPort" id="plannedArrivalPort" type="text" class="form-control" value="" maxlength="50"></td>
            </tr>
            <tr>

                <td class="text1" height="30"><font color="red">*</font>Planned Discharge Date</td>
                <td class="text2" height="30"><input name="plannedDischargeDate" id="plannedDischargeDate" type="text"  class="datepicker" value="" ></td>
                <td class="text1" height="30"><font color="red">*</font>Planned Discharge Port</td>
                <td class="text2" height="30"><input name="plannedDischargePort" id="plannedDischargePort" type="text" class="form-control" value="" maxlength="50"></td>
            </tr>
            <tr>
                <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                <td class="text1">
                    <select  align="center" class="form-control" name="status" id="status" >
                        <option value='Y' selected>Active</option>
                        <option value='N'>In-Active</option>
                    </select>
                </td>
                <td class="text1" height="30"><font color="red">*</font>Description</td>
                <td class="text2" height="30"><input name="description" id="description" type="text" class="form-control" value="" maxlength="50"></td>
            </tr>



        </table>
        <br>
        <center>
            <input type="button" value="Save" id="discharegesave" class="button" onClick="savesubmitPage();">

        </center>
        <div class="modal fade" id="myModal" role="dialog" style="width: 100%;height: 100%">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&times;</button>
                        <h4 class="modal-title">Add Vessel</h4>
                    </div>
                    <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 100%">
                        <!--<div id="slabRateListSet" style="width: 100%;height: 100%">-->

                        <!--</div>-->
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>
            </div>
        </div>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

</body>
</html>


