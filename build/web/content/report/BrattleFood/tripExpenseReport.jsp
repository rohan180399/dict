<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleTripExpenseReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleTripExpenseReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
                 if('<%=request.getAttribute("vehicleId")%>' != 'null'){
                    document.getElementById('vehicleId').value='<%=request.getAttribute("vehicleId")%>';
                }
                 if('<%=request.getAttribute("vehicleNo")%>' != 'null'){
                    document.getElementById('vehicleNo').value='<%=request.getAttribute("vehicleNo")%>';
                }
            }
              function calculateDays() {
                var diff = 0;
             var one_day = 1000*60*60*24;
             var fromDate = document.getElementById("fromDate").value;
             var toDate = document.getElementById("toDate").value;
             var earDate = fromDate.split("-");
             var nexDate = toDate.split("-");
             var fD = parseFloat(earDate[0]).toFixed(0);
            var fM = parseFloat(earDate[1]).toFixed(0);
            var fY = parseFloat(earDate[2]).toFixed(0);

            var tD = parseFloat(nexDate[0]).toFixed(0);
            var tM = parseFloat(nexDate[1]).toFixed(0);
            var tY = parseFloat(nexDate[2]).toFixed(0);
             var d1 = new Date(fY,fM,fD);
            var d2 = new Date(tY,tM,tD);
            diff = (d2.getTime() - d1.getTime())/one_day;
             document.getElementById("totalDays").value = diff;
                }
                
                function viewVehicleDetails(vehicleId) {
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body onload="setValue();calculateDays();sorter.size(20);">
        <form name="accountReceivable"   method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;"  align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trip Expense Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr height="30">
                                        <td align="center">Vehicle No</td>
                                        <td height="30">
                                            <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                            <input type="text" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>" class="textbox"/>
                                        </td>
                                        <td>Fleet Center</td>
                                          <td height="30"> <select name="fleetCenterId" id="fleetCenterId" class="textbox" style="height:20px; width:122px;" >
                                               <c:if test="${companyList != null}">
                                                    <option value="" selected>--Select--</option>
                                                    <c:forEach items="${companyList}" var="companyList">
                                                        <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select></td>
                                    </tr>
                                    <tr height="30">
                                        <td align="center"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" onchange="calculateDays();">
                                            <input type="hidden" value="" name="totalDays" id="totalDays" /></td>
                                    </tr>
                                    <tr height="30">
                                        <td colspan="3" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <br>
            <c:if test="${endedTripDetails == null}">
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                </center>
            </c:if>
            <c:if test="${endedTripDetails != null}">
                  <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                    <thead>
                        <tr>
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>GR NO</h3></th>
                           <th align="center"><h3>Billing Party</h3></th>
                            <th align="center"><h3>Trip Advance</h3></th>
                            <th align="center"><h3>Print</h3></th>
                             
                        </tr> 
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${endedTripDetails}" var="eTrip">
                                  <c:set var="days" value="${totalDays}"/>
                                  
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td class="<%=classText%>"  ><%=index++%></td>
                                            <td class="<%=classText%>"  >
                                               <a href="#" onclick="viewVehicleDetails('<c:out value="${eTrip.vehicleId}"/>')"><c:out value="${eTrip.vehicleNo}"/></a></td>
                                            <td class="<%=classText%>"   ><c:out value="${eTrip.grNo}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${eTrip.billingParty}"/></td>
                                            <td class="<%=classText%>" align="right"><c:out value="${eTrip.paidExpense}"/></td>
                                            <td class="<%=classText%>" align="right"> <a href="getTripExpensePrintDetails.do?tripId=<c:out value="${eTrip.tripId}"/>&param=ExportExcel">Print</a></td>
                                             
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
          <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" >5</option>
                        <option value="10">10</option>
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>