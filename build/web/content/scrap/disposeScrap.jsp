<%-- 
    Document   : manageParts
    Created on : Mar 2, 2009, 2:49:16 PM
    Author     : karudaiyar Subramaniam
--%>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.scrap.business.ScrapTO" %>
<%@ page import="ets.domain.section.business.SectionTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<title>PAPL</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

<script language="javascript">    
 

function submitPage(){ 
    
  var index = document.getElementsByName("selectedIndex");
        var chec=0;
        for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        
        }
       }
    document.dispose.action = '/throttle/disposeScrapList.do';
    document.dispose.submit();
}
 
     
    
function setSelectbox(i)
{
 
var selected=document.getElementsByName("selectedIndex") ;

selected[i].checked = 1;
 
}

 

</script>
<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>

</head>
<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<body onload=>
<form name="dispose"  method="post" >


<!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                 
                </div>
            </div>
            <BR>
            <BR>
            <BR>
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>

<table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg" >
 <tr>
<td colspan="4" height="30" class="contenthead"><div class="contenthead"><spring:message code="stores.label.DisposeScrap"  text="default text"/>
</div></td>
<td colspan="4" height="30" class="contenthead"></td>
</tr>
 
 
</table>
<% int index = 0;  %> 
<% int unittxt = 0; %>   
            <br>
            <c:if test = "${DisposeInList != null}" >
                <table width="500" align="center" cellpadding="0" cellspacing="0" id="bg">
 	 	 	 	 	 	 	 	 	 	 	 	                    
                    <tr align="center">
                         <td height="30" class="contentsub"><spring:message code="stores.label.S.No"  text="default text"/></td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.ServicePoint"  text="default text"/></td>
                         <td height="30" class="contentsub">PAPL Code</td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.ItemName"  text="default text"/></td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.Quantity"  text="default text"/></td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.ScrapUnit"  text="default text"/></td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.UnitPrice"  text="default text"/></td>
                         <td height="30" class="contentsub"><spring:message code="stores.label.Select"  text="default text"/></td></td>
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${DisposeInList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %> 
                        <tr> 
                            
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            
                            <input type="hidden" name="scrapIds" value="<c:out value="${list.scrapId}"/>">
                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.compName}"/></td>
                            <input type="hidden" name="compIds" value="<c:out value="${list.compId}"/>">
                            <input type="hidden" name="compNames" value="<c:out value="${list.compName}"/>">
                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.paplCode}"/></td>
                            <input type="hidden" name="paplCodes" value="<c:out value="${list.paplCode}"/>">
                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.itemName}"/></td>
                            <input type="hidden" name="itemIds" value="<c:out value="${list.itemId}"/>">
                            <input type="hidden" name="itemNames" value="<c:out value="${list.itemName}"/>">
                            
                             <td class="<%=classText %>" height="30"><c:out value="${list.quandity}"/></td>
                             <input type="hidden" name="quanditys" value="<c:out value="${list.quandity}"/>">
                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${list.uomName}"/></td>
                            <input type="hidden" name="uomIds" value="<c:out value="${list.uomId}"/>">
                            <input type="hidden" name="uomNames" value="<c:out value="${list.uomName}"/>">
                                 
                                    <c:choose>
                                            <c:when test="${list.uomName != 'Kg'}">                                                                                                
                                            <td class="<%=classText %>" height="30"><input name="unitPrices" size="5" type="text" class="form-control" value="" onChange="setSelectbox(<%=index%>)"></td>
                                            
                                            </c:when>
                                        <c:otherwise>                                                                                        
                                            <td class="<%=classText%>" height="30"><input name="unitPrices" type="hidden" value="NA">NA</td>
                                        </c:otherwise>
                                    </c:choose>                                                                                            
                            
                            <td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>' onChange="setSelectbox(<%=index%>)" ></td>
                              
                      </tr> 
                      
                        <%
            index++;
             
                        %>
                    </c:forEach >
                    
                    
                </table>
                
            </c:if>
            <table align="center">
                <c:if test = "${DisposeInList != null}" > 
                      <tr>
                          <td height="30" class="text2"><strong><spring:message code="stores.label.KgUnitPrice"  text="default text"/></strong></td>
                          <td class="text1" height="30"><input name="kgunitPrice" size="5" type="text" class="form-control" value="1"   ></td>
                      </tr>
                </c:if>    
                       
            </table>
            
  <c:if test = "${DisposeInList != null}" >               
<center>
<input type="button" class="button" value="<spring:message code="stores.label.Dispose"  text="default text"/>" name="search" onClick="submitPage()">
 </c:if>  
<input type="hidden" value="" name="temp" >
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
 
</html>