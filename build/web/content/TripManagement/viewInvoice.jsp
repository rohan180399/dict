<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function selectVal(){
            var sino = parseInt(document.saveInvoice.sino.value);
            for (var i = 1; i <= sino; i++) {
                    document.getElementById("selectedRow"+i).value='N';
            }
        }
        function selectRow(){
            var sino = parseInt(document.saveInvoice.sino.value);
            for (var i = 1; i <= sino; i++) {
                if(document.getElementById("selectedId"+i).checked){
                    document.getElementById("selectedRow"+i).value='Y';
                }else{
                    document.getElementById("selectedRow"+i).value='N';}
            }
        }



        function submitPage(value)
        {
            var sino = parseInt(document.saveInvoice.sino.value);
            alert(sino);
            var ids = "";
            for (var i = 1; i <= sino; i++) {
                if(document.getElementById("selectedId"+i).checked){
                    if(ids == ""){
                        ids = document.getElementById("tripIdVal"+i).value;
                    } else {
                        ids = ids +","+document.getElementById("tripIdVal"+i).value;
                        //alert(ids);
                    }
                }
            }
            if(ids != ""){
                document.getElementById("selectedTripIds").value  = ids;
            } else {
                alert("No Trip selected");
                return false;
            }

             document.saveInvoice.action="/throttle/saveInvoice.do";
             document.saveInvoice.submit();
        }

    </script>

    <%
                String fromDate = (String) request.getAttribute("fromDate");
                String toDate = (String) request.getAttribute("toDate");
                String customer = (String) request.getAttribute("customer");
                String billType = (String) request.getAttribute("billType");
                String ownership = (String) request.getAttribute("ownership");

    %>

    <body onload="selectVal();">
        <form name="saveInvoice" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Invoice Detail</b></td>
                    <td align="right">
                        <span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">&nbsp;</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="700" cellpadding="0" cellspacing="1" border="0" align="left" class="table4">
                                <tr>
                                    <td height="30"> <b>Customer</b></td>
                                    <td  height="30">
                                        <select class="textbox" style="width:125px;" name="customer">
                                            <c:if test = "${customerList != null}" >
                                                <c:forEach items="${customerList}" var="cust">
                                                    <c:if test="${cust.customerId == customer}">
                                                        <option  value='<c:out value="${cust.customerId}" />~<c:out value="${cust.customerName}" />'><c:out value="${cust.customerName}" /> </option>
                                                    </c:if>
                                                </c:forEach >
                                            </c:if>
                                        </select>  
                                    </td>
                                    <td  height="30">Ownership</td>
                                    <td height="30">
                                        <select  style="width:125px;" name="ownership" id="ownership">
                                            <c:if test="${ownership =='1'}">
                                                <option  value='1' selected>Own</option>
                                            </c:if>
                                            <c:if test="${ownership =='2'}">
                                                <option  value='2'>Attach</option>
                                            </c:if>

                                        </select>
                                    </td>


                                </tr>
                                <tr>
                                    <td  height="30">Bill Type</td>
                                    <td height="30">
                                        <select  style="width:125px;" name="billType" id="billType">
                                            <c:if test="${billType =='Depot'}">
                                                <option  value='Depot'>Depot</option>
                                            </c:if>
                                            <c:if test="${billType =='Tancem'}">
                                                <option  value='Tancem'>Tancem</option>
                                            </c:if>
                                            <c:if test="${billType =='Party'}">
                                                <option  value='Party'>Party</option>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td  height="30">From Date : <input type="text" class="textbox" name="fromDate" id="fromDate" value="<%=fromDate%>"/>  </td>
                                    <td  height="30">To Date : <input type="text" class="textbox" name="toDate" id="toDate" value="<%=toDate%>" /></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <!--            <table cellpadding="0" cellspacing="2" align="center" border="0" width="800" id="report" bgcolor="#97caff" style="margin-top:0px;">

            <c:if test= "${totalList != null}"  >
                <c:set var="noOfTrip" value="0" />
                <c:set var="taxAmount" value="0" />
                <c:set var="TotalTonnage" value="0" />
                <c:set var="deliveredTonnage" value="0" />
                <c:set var="totalamount" value="0" />
                <c:forEach items="${totalList}" var="totalVal" >
                    <c:set var="noOfTrip" value="${noOfTrip + totalVal.noOfTrip}" />
                    <c:set var="taxAmount" value="${taxAmount + totalVal.taxAmount}" />
                    <c:set var="TotalTonnage" value="${TotalTonnage + totalVal.totalTonnage}" />
                    <c:set var="deliveredTonnage" value="${deliveredTonnage + totalVal.deliveredTonnage}" />

                    <c:set var="totalamount" value="${ totalamount + totalVal.revenue}" />



                <tr>
                    <td width="50%" height="30">
                        <input type="hidden" id="noOfTrip" name="noOfTrip" value="<c:out value="${noOfTrip}"/>"/>
                        <input type="hidden" id="taxAmount" name="taxAmount" value="<c:out value="${taxAmount}"/>"/>
                        <input type="hidden" id="totalTonnage" name="totalTonnage" value="<c:out value="${TotalTonnage}"/>"/>
                        <b>Total Tonnage : <c:out value="${totalVal.totalTonnage}"/></b>
                    </td>
                    <td>
                        <input type="hidden" id="deliveredTonnage" name="deliveredTonnage" value="<c:out value="${deliveredTonnage}"/>"/>
                        <b>Delivered Tonnage : <c:out value="${totalVal.deliveredTonnage}"/></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="45" height="30">
                        <input type="hidden" id="totalTonAmount" name="totalTonAmount" value="<c:out value="${totalamount}"/>"/>
                            <b>Total Ton Amount : <c:out value="${totalamount}"/></b><br><br>
                    </td>
                    <td>
                        <input type="hidden" id="totalamount" name="totalamount" value="<c:out value="${totalamount}"/>"/>
                       <b>Total Revenue: <c:out value="${totalVal.revenue}"/></b>
                    </td>
                </tr>
                <tr>
                    <td width="45" height="30">
                        <input type="hidden" id="totalexpenses" name="totalexpenses" value="<c:out value="${totalexpenses}"/>"/>
                            <b>Total Expenses : <c:out value="${totalVal.totalexpenses}"/></b>
                    </td>
                    <td>

                        <b>Remark :</b>
                        <input type="text" id="remark" name="remark" value="" />
                        <textarea name="remark"></textarea>

                    </td>
                </tr>
                <tr>

                    <td>


                                <input type="hidden" id="balanceamount" name="balanceamount" value="<c:out value="${balanceamount}"/>"/>
                                <b>Balance Amount : <c:out value="${totalVal.balanceamount}"/></b> <br><br>
                    </td>
                </tr>
                </c:forEach>
            </c:if>

        </table>-->
            <%
                        int sino = 0;
            %>
            <br/>
            <c:if test = "${tripList != null}" >
                <%
                            int index = 0;
                            ArrayList tripList = (ArrayList) request.getAttribute("tripList");
                            if (tripList.size() != 0) {
                %>

                <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contentsub" height="30">S No</td>
                        <td class="contentsub" height="30">Trip Id</td>
                        <td class="contentsub" height="30">GP No</td>
                        <td class="contentsub" height="30">Inv No</td>
                        <td class="contentsub" height="30">Trip Date</td>
                        <td class="contentsub" height="140">Customer Name</td>
                        <td class="contentsub" height="140">Vehicle No</td>
                        <td class="contentsub" height="30">Quantity</td>
                        <td class="contentsub" height="30">Destination</td>
                        <td class="contentsub" height="30">Bill Status</td>
                        <td class="contentsub" height="30">Freight</td>
                        <td class="contentsub" height="30">Status</td>
                        <td class="contentsub" height="30">Select</td>

                    </tr>
                    <c:forEach items="${tripList}" var="tripDetails">
                        <c:set var="total" value="${total+1}"></c:set>
                        <c:set var="totalAmo" value="${total + tripDetails.totalamount}" />
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                        %>
                            <c:set var="lpsStatus" value="${tripDetails.twoLpsStatus}"/>
                            <c:set var="tripGpNo" value="${tripDetails.gpno}"/>
                            <c:set var="tripInvoiceNo" value="${tripDetails.invoiceNo}"/>
                            <c:set var="billStatus" value="${tripDetails.selectedBillStatus}"/>
                            <c:set var="tripRevenue" value="${tripDetails.tripRevenueOne}"/>
                             <%
                            Object LpsStatus = (Object)pageContext.getAttribute("lpsStatus");
                            Object tripGpNo = (Object)pageContext.getAttribute("tripGpNo");
                            Object tripInvoiceNo = (Object)pageContext.getAttribute("tripInvoiceNo");
                            Object billStatus = (Object)pageContext.getAttribute("billStatus");
                            Object tripRevenue = (Object)pageContext.getAttribute("tripRevenue");
                            String LpsStatusValue = (String) LpsStatus;
                            if(LpsStatusValue.contains("Y")){
                            String tempValue = (String) tripGpNo;
                            String bill = (String) billStatus;
                            String invoice = (String) tripInvoiceNo;
                            String revenue = (String) tripRevenue;
                            String temp[] = null;
                            String tempInvoice[] = null;
                            String tempBillStatus[] = null;
                            String tempRevenue[] = null;
                            temp = tempValue.split(",");
                            tempInvoice = invoice.split(",");
                            tempRevenue = revenue.split(",");
                           if(!bill.equals("")){
                            tempBillStatus = bill.split(",");
                           }
                            for(int i=0;i<temp.length;i++){
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                           if(!bill.equals("") && tempBillStatus[i].contains("Paid")){
                             sino++;%>
                            <tr>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="no" name="no" value="<%=sino%>"/><%=sino%></td>
                           <td class="<%=classText%>"  height="30"><input type="hidden" id="tripId" name="tripId" value="<c:out value="${tripDetails.tripId}"/>"/><c:out value="${tripDetails.tripId}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="gpno" name="gpno" value="<%=temp[i]%>"/><%=temp[i]%></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="invoiceNo" name="invoiceNo" value="<%=tempInvoice[i]%>"/><%=tempInvoice[i]%></td>
                             <td class="<%=classText%>"  height="30"><input type="hidden" id="tripDate" name="tripDate" value="<c:out value="${tripDetails.tripDate}"/>"/><c:out value="${tripDetails.tripDate}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="custName" name="custName" value="<c:out value="${tripDetails.custName}"/>"/><c:out value="${tripDetails.custName}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="regno" name="regno" value="<c:out value="${tripDetails.regno}"/>"/><c:out value="${tripDetails.regno}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="totalTonnage" name="totalTonnage" value="<c:out value="${tripDetails.totalTonnage}"/>"/><c:out value="${tripDetails.totalTonnage}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="routeName" name="routeName" value="<c:out value="${tripDetails.routeName}"/>"/><c:out value="${tripDetails.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="billStatus" name="billStatus" value="<%=tempBillStatus[i]%>%>"/><%=tempBillStatus[i]%></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="totalamount" name="totalamount" value="<%=tempRevenue[i]%>"/><%=tempRevenue[i]%></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="status" name="status" value="<c:out value="${tripDetails.status}"/>"/><c:out value="${tripDetails.status}"/></td>
                            <td class="<%=classText%>">
                                <input type="hidden" name="tripIdVal" id="tripIdVal<%=sino%>" value="<c:out value="${tripDetails.tripId}"/>" />
                                <input type="hidden" name="selectedRow" id="selectedRow<%=sino%>" value="" />
                                <input type="checkbox" name="selectedId"  id="selectedId<%=sino%>"  onclick="selectRow();" />
                            </td>
                        </tr>
                          <% 
                             }
                            }
                           }else{
                          
                          if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                           %>
                           <c:if test="${tripDetails.billStatus != '' && tripDetails.billStatus == 'Paid'}">
                               <% sino++;%>
                          <tr>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="no" name="no" value="<%=sino%>"/><%=sino%></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="tripId" name="tripId" value="<c:out value="${tripDetails.tripId}"/>"/><c:out value="${tripDetails.tripId}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="gpno" name="gpno" value="<c:out value="${tripDetails.gpno}"/>"/><c:out value="${tripDetails.gpno}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="invoiceNo" name="invoiceNo" value="<c:out value="${tripDetails.invoiceNo}"/>"/><c:out value="${tripDetails.invoiceNo}"/></td>
                             <td class="<%=classText%>"  height="30"><input type="hidden" id="tripDate" name="tripDate" value="<c:out value="${tripDetails.tripDate}"/>"/><c:out value="${tripDetails.tripDate}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="custName" name="custName" value="<c:out value="${tripDetails.custName}"/>"/><c:out value="${tripDetails.custName}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="regno" name="regno" value="<c:out value="${tripDetails.regno}"/>"/><c:out value="${tripDetails.regno}"/></td>
                            <td class="<%=classText%>"  height="140"><input type="hidden" id="totalTonnage" name="totalTonnage" value="<c:out value="${tripDetails.totalTonnage}"/>"/><c:out value="${tripDetails.totalTonnage}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="routeName" name="routeName" value="<c:out value="${tripDetails.routeName}"/>"/><c:out value="${tripDetails.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="billStatus" name="billStatus" value="<c:out value="${tripDetails.billStatus}"/>"/><c:out value="${tripDetails.billStatus}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="totalamount" name="totalamount" value="<c:out value="${tripDetails.revenue}"/>"/><c:out value="${tripDetails.revenue}"/></td>
                            <td class="<%=classText%>"  height="30"><input type="hidden" id="status" name="status" value="<c:out value="${tripDetails.status}"/>"/><c:out value="${tripDetails.status}"/></td>
                            <td class="<%=classText%>">
                                <input type="hidden" name="tripIdVal" id="tripIdVal<%=sino%>" value="<c:out value="${tripDetails.tripId}"/>" />
                                <input type="hidden" name="selectedRow" id="selectedRow<%=sino%>" value="" />
                                <input type="checkbox" name="selectedId"  id="selectedId<%=sino%>"  onclick="selectRow();" />
                            </td>
                           
                        </tr>
                           </c:if>
                          <% } %>

                        <%index++;%>
                    </c:forEach>
                    <tr>
                        <td  height="30" colspan="14">
                            <input type="hidden" name="sino" id="sino" value="<%=sino%>" />
                            <input type="hidden" name="selectedTripIds" id="selectedTripIds" value="" />
                            <center> <input type="button" align="center"  value="Save"  class="button" name="FetchData" onClick="submitPage(this.value)"></center>
                        </td>
                    </tr>
                </table>
                <%
                            }
                %>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>

</html>