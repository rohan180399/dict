
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
        }
        else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleDailyTripPlanningDetailsExcel.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            }
            else {
                document.BPCLTransaction.action = '/throttle/handleDailyTripPlanningDetailsExcel.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Trip Plan Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="BPCLTransaction" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <!-- pointer table -->
                    <!-- message table -->
                    <%@ include file="/content/common/message.jsp"%>

                    <div id="first">
                        <table style="width:80%" align="left" border="0" class="tabouterborder" >
                            <tr height="30"   >
                                <td colspan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">
                                    Trip Plan Report
                                </td>
                            </tr>
                            <!--                                    <tr>
                                                                    <td><font color="red">*</font>Driver Name</td>
                                                                    <td height="30">
                                                                        <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                </tr>-->
                             <tr height="40">
                                <td ><font color="red">*</font> From Date</td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" style="width:180px;height:25px;color:black" ></td>
                                                                        <td><font color="red">*</font>To Date</td>
                                                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <tr>
                            <td><center>
                                <input type="button" id="buttonDesign" class="btn btn-info" name="search" onclick="submitPage(this.name);" value="Search">
                                <input type="button" id="buttonDesign" class="btn btn-info" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel">
                                <input type="button" id="buttonDesign" class="btn btn-info"  value="Print" onClick="print('printContent');" >
                                </center></td>
                            </tr>
                        </table>
<br>
<br>
                        <div id="printContent">
                            <c:if test="${tripDetails != null}">
                                <table align="center" border="0" id="table" class="sortable" style="width:100%;" >

                                    <thead height="40">
                                        <tr id="tableDesingTH" height="40">
                                            <th align="center"><h3>S.No</h3></th>
                                    <th align="center"><h3>Custmoer Name</h3></th>
                                    <th align="center" colspan="0"><h3>Address</h3></th>
                                    <th align="center"><h3>Billing Party </h3></th>
                                    <th align="center"><h3>shipping Line</h3></th>
                                    <th align="center"><h3>vehicle Type</h3></th>
                                    <th align="center"><h3>ISO/DSO</h3></th>
                                    <th align="center"><h3>FS/MT REPO</h3></th>
                                    <th align="center"><h3>PickUp Location</h3></th>
                                    <th align="center"><h3>To Location</h3></th>
                                    <th align="center" colspan="2"><h3>Total Booking</h3></th>
                                    <th align="center" colspan="2"><h3>Trip Start</h3></th>
                                    <th align="center" colspan="2"><h3>Trip End</h3></th>
                                    <th align="center" colspan="0"><h3>Consignment Order No</h3></th>
                                    <!--<th align="center" colspan="0"><h3>Delete</h3></th>-->
                                    <th align="center" colspan="0"><h3>Trailer No</h3></th>
                                    <th align="center" colspan="0"><h3>Container(s)</h3></th>
                                    <!--<th align="center" colspan="0"><h3>Date</h3></th>-->

                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td align="center" class="text1" >20'</td>
                                        <td align="center" class="text1">40'</td>
                                        <td align="center" class="text1">20'</td>
                                        <td align="center" class="text1">40'</td>
                                        <td align="center" class="text1">20'</td>
                                        <td align="center" class="text1">40'</td>
                                       
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:set var="totalUnplannedTwenty" value="0"/>
                                        <c:set var="totalUnplannedFourty" value="0"/>
                                        <c:set var="totalPlannedTwenty" value="0"/>
                                        <c:set var="totalPlannedFourty" value="0"/>
                                        <c:set var="tottalStartedTwentyFtContainer" value="0"/>
                                        <c:set var="totalStartedFourtyFtContainer" value="0"/>
                                        <c:set var="totalEndedTwentyFtContainer" value="0"/>
                                        <c:set var="totalEndedFourtyFtContainer" value="0"/>
                                        <% int index = 1;%>

                                        <c:forEach items="${tripDetails}" var="BPCLTD">
                                            <%
                                                        String classText = "";
                                                        int oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                                            %>

                                            <tr>
                                                <td align="center" width="30"class="<%=classText%>"><%=index++%></td>
                                                <td  width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.customerName}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.address}"/></td>
                                                <td  width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.billingParty}"/></td>
                                                <td width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.linerName}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.vehicleTypeName}"/></td>
                                                <td align="center">
                                                    <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '2'|| BPCLTD.id == '3' }" >
                                                        ISO
                                                    </c:if>
                                                    <c:if test="${BPCLTD.id == '4'|| BPCLTD.id == '5'}">
                                                        DSO
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <c:if test="${ BPCLTD.id == '3' }" >
                                                        MT REPO
                                                    </c:if>
                                                    <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '4'}">
                                                        FS
                                                    </c:if>
                                                    <c:if test="${BPCLTD.id == '2' || BPCLTD.id == '5'}">
                                                        Import
                                                    </c:if>
                                                </td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.origin}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.destination}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.twentyFT}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.fortyFT}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.startedTwentyFtContainer}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.startedFourtyFtContainer}"/></td>
                                                <c:set var="totalUnplannedTwenty" value="${totalUnplannedTwenty+ BPCLTD.twentyFT}"/>
                                                <c:set var="totalUnplannedFourty" value="${totalUnplannedFourty+ BPCLTD.fortyFT}"/>
                                                <c:set var="tottalStartedTwentyFtContainer" value="${tottalStartedTwentyFtContainer+ BPCLTD.startedTwentyFtContainer}"/>
                                                <c:set var="totalStartedFourtyFtContainer" value="${totalStartedFourtyFtContainer+ BPCLTD.startedFourtyFtContainer}"/>
                                                <c:set var="totalEndedTwentyFtContainer" value="${totalEndedTwentyFtContainer+ BPCLTD.endedTwentyFtContainer}"/>
                                                <c:set var="totalEndedFourtyFtContainer" value="${totalEndedFourtyFtContainer+ BPCLTD.endedFourtyFtContainer}"/>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.endedTwentyFtContainer}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.endedFourtyFtContainer}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.consignmentOrderNo}"/></td>
                                                <!--<td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.deleteOrder}"/></td>-->
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.vehicleNo}"/></td>
                                                <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.containerNo}"/></td>
                                                <!--<td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.consignmentDate}"/></td>-->
                                            </tr>
                                        </c:forEach>

                                        <tr>
                                            <td>Total</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><c:out value="${totalUnplannedTwenty}"/></td>
                                            <td><c:out value="${totalUnplannedFourty}"/></td>
                                            <td><c:out value="${tottalStartedTwentyFtContainer}"/></td>
                                            <td><c:out value="${totalStartedFourtyFtContainer}"/></td>
                                            <td><c:out value="${totalEndedTwentyFtContainer}"/></td>
                                            <td><c:out value="${totalEndedFourtyFtContainer}"/></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                    </tbody>
                                </table>


                            </c:if>
                        </div>                <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5"selected="selected" >5</option>
                                    <option value="10">10</option>
                                    <option value="20" >20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                <script type="text/javascript">
                    function print(val)
                    {
                        var DocumentContainer = document.getElementById(val);
                        var WindowObject = window.open('', "TrackHistoryData",
                                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                        WindowObject.document.writeln(DocumentContainer.innerHTML);
                        WindowObject.document.close();
                        WindowObject.focus();
                        WindowObject.print();
                        WindowObject.close();
                    }
                </script>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

