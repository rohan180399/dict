
<%-- 
    Document   : DailyCashReportExcel
    Created on : Apr 28, 2016, 7:06:41 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${dailyCashDetails != null}" >
                <table align="center" width="100%" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <td  class="contenthead">S.No</td>
                            <td  class="contenthead">Date</td>
                            <td  class="contenthead">G.R No.</td>
                            <td  class="contenthead">Vehicle No</td>
                            <td  class="contenthead">Transporter</td>
                            <td  class="contenthead">Billing Party</td>
                            <td  class="contenthead">Route</td>
                            <td  class="contenthead">Diesel ltrs</td>
                            <td  class="contenthead">Cash Dr</td>
                            <td  class="contenthead">Cash Received</td>
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${dailyCashDetails}" var="csList">
                        <tr>
                            <td align="center"><%=sno%></td>
                            <td align="center"><c:out value="${csList.paidDate}"/></td>
                            <td align="center"><c:out value="${csList.grNo}"/></td>
                            <td align="center"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center"><c:out value="${csList.transporter}"/></td>
                            <td align="left"><c:out value="${csList.customerName}"/></td>
                            <td align="center"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center"><c:out value="${csList.liters}"/></td>
                            <td align="center"><c:out value="${csList.paidCash}"/></td>
                            <td align="center"><c:out value=""/></td>

                        </tr>
                        <%  
                   index++;
                     sno++;
                        %>
                        <c:set var="cashReceived" value="${cashReceived}"></c:set>
                        <c:set var="cashDr" value="${cashDr + csList.paidCash}"></c:set>
                        <c:set var="cashInHand" value="${cashReceived - cashDr }"></c:set>
                    </c:forEach>
                </table>

                    <br>
                <br>
                <table align="center" width="500" cellpadding="5" cellspacing="0">
                    <tr>
                        <td  class="contenthead">Cash Received</td>
                        <td class="contenthead">Cash Dr</td>
                        <td class="contenthead">Cash In-Hand</td>
                    </tr>
                    <tr>
                        <td align="center"><c:out value="${cashReceived}"/></td>
                        <td align="center"><c:out value="${cashDr}"/></td>
                        <td align="center"><c:out value="${cashInHand}"/></td>
                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

           
 

</html>

