<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script>
    function submitPag(val) {
        if (validate() == '0') {
            return;
        }
        if (val == 'purchaseOrder') {
            document.mpr.purchaseType.value = "1012"
            document.mpr.action = '/throttle/generateMprForReqItems.do'
            document.mpr.submit();
        } else if (val == 'localPurchase') {
            document.mpr.purchaseType.value = "1011"
            document.mpr.action = '/throttle/generateMprForReqItems.do'
            document.mpr.submit();
        }
    }

    function validate()
    {
        var selectedMrs = document.getElementsByName("selectedIndex");
        var reqQtys = document.getElementsByName("reqQtys");
        var vendorIds = document.getElementsByName("vendorIds");
        var counter = 0;
        for (var i = 0; i < selectedMrs.length; i++) {
            if (selectedMrs[i].checked == 1) {
                counter++;
                if (parseFloat(reqQtys[i].value) == 0 || isFloat(reqQtys[i].value)) {
                    alert("Please Enter Required Quantity");
                    reqQtys[i].select();
                    reqQtys[i].focus();
                    return '0'
                }
                if (vendorIds[i].value == '0') {
                    alert("Please Select Vendor");
                    vendorIds[i].focus();
                    return '0'
                }
            }
        }
        if (counter == 0) {
            alert("Please Select Atleast one item");
            return '0';
        }
        return counter;
    }

    function selectBox(val) {
        var checkBoxs = document.getElementsByName("selectedIndex");
        checkBoxs[val].checked = 1;
    }


    function submitPage(val) {

        if (val == 'GoTo') {
            var temp = document.mpr.GoTo.value;
            if (temp != 'null') {
                document.mpr.pageNo.value = temp;
            }
        }
        document.mpr.button.value = val;
        document.mpr.action = '/throttle/handleSearchRequiredItems.do'
        document.mpr.submit();
    }

    function setValues()
    {
        if ('<%= request.getAttribute("mfrCode") %>' != 'null') {
            document.mpr.mfrCode.value = '<%= request.getAttribute("mfrCode") %>';
        }
        if ('<%= request.getAttribute("paplCode") %>' != 'null') {
            document.mpr.paplCode.value = '<%= request.getAttribute("paplCode") %>';
        }
        if ('<%= request.getAttribute("categoryId") %>' != 'null') {
            document.mpr.categoryId.value = '<%= request.getAttribute("categoryId") %>';
        }
        if ('<%= request.getAttribute("searchAll") %>' != 'null') {
            document.mpr.searchAll.value = '<%= request.getAttribute("searchAll") %>';
        }
    }





</script>



<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
          setValues();
          getVehicleNos();">
    </c:if>

    <!--  <span style="float: right">
            <a href="?paramName=en">English</a>
            |
            <a href="?paramName=ar">Arabic</a>
      </span>-->
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>  <spring:message code="stores.label.SearchParts"  text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="stores.label.Sales/Ops"  text="default text"/></a></li>
                <li class="active"><spring:message code="stores.label.SearchParts"  text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">


                <body onLoad="setValues();" >        
                    <form name="mpr" method="post">                    
                        <%--<%@ include file="/content/common/path.jsp" %>--%>            
                        <!-- pointer table -->
                        <!-- message table -->           
                        <%@ include file="/content/common/message.jsp" %>    
                        <br>
                        <%
                                        int pageIndex = (Integer)request.getAttribute("pageNo");					
                                            int index1 = ((pageIndex-1)*10)+1 ;
                        %>  


                        <table  class="table table-info mb30 table-hover" id="bg">
                            <thead>
                            <tr>
                                <th colspan="4" height="30" ><spring:message code="stores.label.SearchParts"  text="default text"/>
                                   </th>
                            </thead>
                            </tr>
                            <tr>
                                <td  height="30"><font color="red">*</font><spring:message code="stores.label.ItemCode"  text="default text"/></td>
                                <td  height="30"><input name="mfrCode" type="text" style="width:260px;height:40px;" class="form-control" value="" ></td>
                                <td  height="30"><font color="red">*</font><spring:message code="stores.label.CompanyCode"  text="default text"/></td>
                                <td  height="30"><input name="paplCode" type="text" style="width:260px;height:40px;" class="form-control" value=""></td>


                            <tr> 
                                <td  height="30"><font color="red">*</font><spring:message code="stores.label.Category"  text="default text"/></td>
                                <td   >
                                    <select style="width:260px;height:40px;" class="form-control" name="categoryId" style="width:125px;" >
                                        <option value="0">---<spring:message code="stores.label.Select"  text="default text"/>---</option>
                                        <c:if test = "${CategoryList != null}" >
                                            <c:forEach items="${CategoryList}" var="Dept"> 
                                                <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                                            </c:forEach >
                                        </c:if>   	
                                    </select>
                                </td>
                                <td  height="30"><font color="red">*</font><spring:message code="stores.label.SearchType"  text="default text"/></td>
                                <td  height="30">
                                    <select style="width:260px;height:40px;" class="form-control" name="searchAll" style="width:125px;" >
                                        <option value="">---<spring:message code="stores.label.Select"  text="default text"/>---</option> 	
                                        <option value="Y"><spring:message code="stores.label.All"  text="default text"/></option> 	
                                        <option value="N"><spring:message code="stores.label.RequiredItems"  text="default text"/></option> 	
                                    </select>                
                                </td>            
                            </tr>
                            <!--<tr> <td>&nbsp;  </td> </tr>-->
                            <tr>                
                                <td  colspan="4" align="center" height="30"><input type="button" class="btn btn-success" name="Search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPage(this.value)" >  </td>
                            </tr>    
                        </table>





                        <c:if test = "${requiredItemsList != null}" >   
                            <table class="table table-info mb30 table-hover" id="bg" >              
                                <%
                            String classText = "";
                            int oddEven = 0;
                            int index = 0;
                                %>
                                <thead>

                                <tr>
                                    <th width="25" height="30" ><spring:message code="stores.label.Sno"  text="default text"/></th>						
                                    <th width="69" height="30" ><spring:message code="stores.label.MfrCode"  text="default text"/></th>
                                    <th width="71" height="30" ><spring:message code="stores.label.CompanyCode"  text="default text"/></th>                       
                                    <th width="144" height="30"><spring:message code="stores.label.ItemName"  text="default text"/></th>
                                    <th width="31" height="30" ><spring:message code="stores.label.Uom"  text="default text"/></th>
                                    <th width="60" height="30" ><spring:message code="stores.label.ReorderLevel"  text="default text"/> </th>                                                                                                                                               
                                    <th width="58" height="30" ><spring:message code="stores.label.StockBalance"  text="default text"/> </th>                                                                                                                                                                                                                                                                                                                
                                    <th width="69" height="30" ><spring:message code="stores.label.PoRaisedQty"  text="default text"/> </th>                                                                                                                                               
                                    <th width="125" height="30" ><spring:message code="stores.label.Vendor"  text="default text"/> </th>                                                                                                                                               
                                    <th width="51" height="30" ><spring:message code="stores.label.Select"  text="default text"/></th>
                                </tr>  
                                </thead>
                                <c:forEach items="${requiredItemsList}" var="item"> 
                                    <%

                        oddEven = index1 % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr>
                                        <td  height="30"><%= index1 %></td>						
                                        <td  height="30"><c:out value="${item.mfrCode}"/></td>
                                        <td  height="30"><c:out value="${item.paplCode}"/></td>                       
                                        <td  height="30"><c:out value="${item.itemName}"/></td>
                                        <td  height="30"><c:out value="${item.uomName}"/></td>
                                        <td  height="30"><c:out value="${item.roLevel}"/></td>
                                        <td  height="30"><c:out value="${item.stockBalance}"/></td>                            
                                        <td  height="30"><c:out value="${item.poRaisedQty}"/> </td>
                                        <td  height="30"> <select name="vendorIds" class="form-control" style="width:125px;">
                                                <option value='0'> --<spring:message code="stores.label.Select"  text="default text"/> -- </option>
                                                <c:if test = "${vendorItemList != null}" >       
                                                    <c:forEach items="${vendorItemList}" var="vend">                                      
                                                        <c:if test="${vend.itemId==item.itemId}" >
                                                            <option value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>                                    
                                                        </c:if>                            
                                                    </c:forEach>      
                                                </c:if>                                
                                            </select>                    
                                        </td>                             
                                        <td  height="30"> <input type="checkbox" class="form-control" name="selectedIndex" value="<%= index %>" > <input type="hidden" name="itemIds" value='<c:out value="${item.itemId}"/>' >  </td>
                                    </tr>
                                        <!--<td></td>-->
                                    <%
                        index++;
                        index1++;
                                    %>
                                </c:forEach>

                            </table>

                            <br>           
                            <input type="hidden" name="purchaseType" value="" >          
                            <br>               
                        </c:if>     

                        <br>
                        <%@ include file="/content/common/pagination.jsp"%>      
                        <br>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
                  </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>