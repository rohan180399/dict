<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(document.getElementById('fromDate').value == ''){
                    alert("please select from date");
                    document.getElementById('fromDate').focus();
                }else if(document.getElementById('toDate').value == ''){
                    alert("please select to date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == 'ExportExcel'){
                        document.accountReceivable.action = '/throttle/handleCustomerWiseMergingProfit.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    }else{
                        document.accountReceivable.action = '/throttle/handleCustomerWiseMergingProfit.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                    var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                        submitPage('search');
                    }
                }
            }


            function viewCustomerProfitDetails(tripIds) {
                //alert(tripIds);
                window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>
                    
                     <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">العربية</a>
	  </span>-->


        <style type="text/css">





            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
            }



        </style>
   <style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.CustomerWiseProfitAfterMerging" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.CustomerWiseProfitAfterMerging" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValue();">
        <form name="accountReceivable"   method="post">
<!--     
            <table class="table table-bordered" id="report" >
                        <tr height="30" id="index" >
                            <td colspan="4"  style="background-color:#5BC0DE;">
                                <b><spring:message code="operations.reports.label.CustomerWiseProfitAfterMerging" text="default text"/></b>
                                </td>
                                </tr>
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">-->
                                <table  class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="6"><spring:message code="operations.reports.label.CustomerWiseProfitAfterMerging" text="default text"/></th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.Customer" text="default text"/></td>
                                        <td height="30">
                                            <select class="form-control" style="width:250px;height:40px;" name="customerId" id="customerId"  >
                                                <c:if test="${customerLists != null}">
                                                    <option value="">---<spring:message code="operations.reports.label.Select" text="default text"/>---</option>
                                                    <c:forEach items="${customerLists}" var="customerList">
                                                        <option value='<c:out value="${customerList.customerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            <script>
                                                document.getElementById('customerId').value = <c:out value="${customerId}"/> ;
                                            </script>
                                        </td>
<!--                                    </tr>
                                    <tr>-->
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate"  autocomplete="off" style="width:260px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate"  autocomplete="off" style="width:260px;height:40px;" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    
                                    </tr>
                                    <tr >
                                        <td></td>
                                        <td></td>
                                        <td ><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);"></td>
                                        <td ><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                </table>
<!--                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>

            <br>
            <br>
            <br>-->

            <c:if test = "${tripMergingList != null}" >

                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr>
                            <th><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.CustomerName" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.BillingType" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.Revenue" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.LoadedExpense" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.EmptyExpense" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.TotalExpense" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.Profit" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.Profit%" text="default text"/></th>
                            <th><spring:message code="operations.reports.label.NoOfTrip" text="default text"/></th>
                        </tr>
                    </thead>

                    <tbody>
                        <% int index = 0, sno = 1, sno1 = 1;%>
                        <c:set var="profitPercent" value="${0}"/>
                        <c:set var="countValue" value="${0}"/>
                        <c:set var="totalRevenue" value="${0}"/>
                        <c:set var="totalExpenses" value="${0}"/>
                        <c:set var="emptyExpenses" value="${0}"/>
                        <c:set var="loadedExpenses" value="${0}"/>
                        <c:set var="profit" value="${0}"/>
                        <c:set var="perc" value="${0}"/>
                        <c:set var="profitPercent" value="${0}"/>
                        <c:set var="mergingId" value="" scope="request"/>
                        <c:set var="totalTripNos" value="${0}" />
                        <c:set var="totalTripRevenue" value="${0}" />
                        <c:set var="toalTripProfit" value="${0}" />
                        <c:set var="totalPerc" value="${0}"/>
                        <c:set var="totalProfitPercent" value="${0}"/>
                        <c:forEach items="${tripMergingList}" var="merge">
                            <c:if test="${merge.customerId != 1040}">
                                <% sno1 = 1;%>
                                <c:set var="totalRevenue" value="${merge.earnings}"/>
                                <c:set var="emptyExpenses" value="${merge.emptyExpense}"/>
                                <c:set var="loadedExpenses" value="${merge.loadedExpense}"/>
                                <c:set var="totalExpenses" value="${emptyExpenses + loadedExpenses}"/>
                                <c:set var="profit" value="${totalRevenue - totalExpenses}"/>
                                <c:set var="perc" value="${profit/totalRevenue}"/>
                                <c:set var="profitPercent" value="${perc * 100}"/>
                                <c:set var="countValue" value="${countValue + 1}"/>
                                <tr height="30">
                                    <td align="center"><%=sno%></td>
                                    <td align="left"><c:out value="${merge.customerName}"/></td>
                                    <td align="left"><c:out value="${merge.billingType}"/></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.earnings}" /></td>
                                    <td align="left"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense}" /></td>
                                    <td align="left"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.emptyExpense}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense + merge.emptyExpense}" /></td>
                                    <td align="right">
                                        <c:if test="${profit > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profit < 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>&nbsp;
                                    </td>
                                    <td align="right">
                                        <c:if test="${profitPercent <= 0}">
                                            <font color="red">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profitPercent > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                    </td>
                                    <td align="right"><a href="#" onclick ="viewTripDetails('<c:out value="${merge.tripId}"/>')">&nbsp;<c:out value="${merge.tripCount}"/>&nbsp;</a></td>
                                    <c:set var="totalTripNos" value="${merge.tripCount+totalTripNos}" />
                                    <c:set var="totalTripRevenue" value="${merge.earnings+totalTripRevenue}" />
                                    <c:set var="totalTripExpenses" value="${totalExpenses+totalTripExpenses}" />
                                    <c:set var="toalTripProfit" value="${totalTripRevenue-totalTripExpenses}" />
                                    <c:set var="totalPerc" value="${toalTripProfit/totalTripRevenue}"/>
                                    <c:set var="totalProfitPercent" value="${totalPerc*100}"/>
                                </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:if>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
<script language="javascript" type="text/javascript">
               function viewTripDetails(tripIds) {
            //alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitMergingDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
            </script>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>

            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <br/>
            <br/>
            <c:if test = "${tripMergingList != null}" >
                <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                    <tr height="25" align="right">
                        <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalTripsCarriedOut" text="default text"/></td>
                        <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalIncome" text="default text"/></td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTripRevenue}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalFixedExpenses" text="default text"/></td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTripExpenses}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfit" text="default text"/> </td>
                        <td width="150" align="right">
                            <c:if test="${toalTripProfit > 0}">
                                <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${toalTripProfit}" /></font>
                            </c:if>
                            <c:if test="${toalTripProfit <= 0}">
                                <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${toalTripProfit}" /></font>
                            </c:if>
                        </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfit%" text="default text"/> </td>
                        <td width="150" align="right">
                            <c:if test="${totalPerc/count > 0}">
                                <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" />&nbsp;%</font>
                            </c:if>
                            <c:if test="${totalPerc/count <= 0}">
                                <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" />&nbsp;%</font>
                            </c:if>
                        </td>
                    </tr>
                </table>

            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>