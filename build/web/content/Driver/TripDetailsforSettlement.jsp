
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>

        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>

    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;    
            background-color:#CC3333;
        }            
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;    
            background-color:#F2F2F2;
        }            
    </style>
   
    <body onload="">
        <form name="proceedDriverSettlement" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            
            <br>             
            <c:if test = "${searchProDriverSettlement != null}" >
                <%
                            int index = 0;
                            ArrayList searchProDriverSettlement = (ArrayList) request.getAttribute("searchProDriverSettlement");
                            if (searchProDriverSettlement.size() != 0) {
                %>
                <table  border="0" class="border" align="center" width="900" cellpadding="0" cellspacing="0" id="bg">
                    <!--<tr>
                        <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                    </tr>-->
                    <tr>
                        <td class="contentsub"  height="30">Trip Id</td>
                        <td class="contentsub" height="30">Identity No</td>
                        <td class="contentsub" height="30">Device Id</td>
                        <td class="contentsub" height="30">Route Name</td>
                        <td class="contentsub" height="30">Out KM</td>
                        <td class="contentsub" height="30">Driver Name</td>
                        <td class="contentsub" height="30">Vehicle Out DateTime</td>
                        <td class="contentsub" height="30">Vehicle In KM</td>
                        <td class="contentsub" height="30">Location</td>

                        <td class="contentsub" height="30">Vehicle In DateTime</td>
                        <td class="contentsub" height="30">Trip Status</td>
                    </tr>
                    <c:forEach items="${searchProDriverSettlement}" var="pdriversettle">
                        <c:set var="total" value="${total+1}"></c:set>
                        <%
                                 String classText = "";
                                 int oddEven = index % 2;
                                 if (oddEven > 0) {
                                     classText = "text2";
                                 } else {
                                     classText = "text1";
                                 }
                        %>
                        <tr>                            
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.tripId}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.identityNo}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.deviceId}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.routeName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outKM}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.driverName}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outDateTime}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inKM}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.location}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inDateTime}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.status}"/></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <%
                            }
                %>
                <table  border="0" class="border" align="center" width="900" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="button" class="button" name="Proceed" onclick="submitPage();" value="Proceed to Settlement">
                        </td>
                    </tr>
                </table>
            </c:if>            
            <input name="check" type="hidden" value='0'>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>