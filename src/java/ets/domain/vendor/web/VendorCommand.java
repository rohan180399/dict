package ets.domain.vendor.web;

/**
 *
 * @author vidya
 */
public class VendorCommand {

    public VendorCommand() {
    }
    private String fuelHikePercentage = "";
    private String trailorTypeId = "";
    private String trailerTypeId = "";
    private String ratePerKm = "";
    private String startDate = "";
    private String endDate = "";
    private String VehicleType = "";
    private String vehicleTypeId = "";
    private String VehicleNo = "";
    private String VehicleId = "";
    private String contractTypeId = "";
    private String operationType = "";
    private String fixedKm = "";
    private String fixedAmount = "";
    private String rateExtraKm = "";
    private String driverReponsibility = "";
    private String fuleExpenseby = "";
    private String paymentType = "";
    private String paymentScheduleDays = "";

    private String contractId = "";
    private String[] contractId1 = null;

    private String vendorId = "";
    private String vendorName = "";
    private String vendorTypeValue = "";
    private String vendorAddress = "";
    private String vendorPhoneNo = "";
    private String vendorMailId = "";
    private String isCreditableVendor = "";
    private String activeInd = "";
    private String vendorTypeId = "";
    private String creditDays = "";
    private String priceType = "";
    private String status = "";
    private String[] manufacturerId = null;
    private String[] selectedIndex = null;
    private String[] mfrids = null;
    private String[] itemId = null;
    private String[] assignedList = null;

    private String vId = "";
    private String tinNo = "";

    //23-04-2015  createvehiclevendor start here//
    private String originIdFullTruck = "";

    private String originNameFullTruck = "";
    private String destinationIdFullTruck = "";
    private String destinationNameFullTruck = "";
    private String routeIdFullTruck = "";
    private String travelKmFullTruck = "";
    private String travelHourFullTruck = "";
    private String travelMinuteFullTruck = "";
    private String vehicleIdFullTruck = "";
    private String dateWithReeferFullTruck = "";
    private String dateWithoutReeferFullTruck = "";

    //private String vehicleTypeId="";
    private String vehicleUnits = "";
    private String trailerType = "";
    private String tType = "";
    private String spotCost = "";
    private String additionalCost = "";

//dedicate
    private String vehicleTypeIddeD = "";
    private String units = "";
    private String trailType = "";
    private String tUnits = "";
    private String fCost = "";
    private String fHrs = "";
    private String fMin = "";
    private String tFixed = "";
    private String vCost = "";
    private String mAllow = "";
    private String cCategory = "";
    private String rLimit = "";
    private String oTime = "";
    private String time = "";
    private String aCost = "";

    private String aeddeD = "0";
    private String uomdeD = "";
    private String fueldeD = "0";
    private String endDateold = "";

    private String vehicleRegNo = "";
    private String agreedDate = "";
    private String remarks = "";
    private String mfr = "";
    private String model = "";
    private String modelName = "";
    // private String trailerType = "";
    private String trailerNo = "";
    private String trailerRemarks = "";

    public String getVId() {
        return vId;
    }

    public void setVId(String vId) {
        this.vId = vId;
    }

    public String[] getAssignedList() {
        return assignedList;
    }

    public void setAssignedList(String[] assignedList) {
        this.assignedList = assignedList;
    }

    public String[] getItemId() {
        return itemId;
    }

    public void setItemId(String[] itemId) {
        this.itemId = itemId;
    }

    public String[] getMfrids() {
        return mfrids;
    }

    public void setMfrids(String[] mfrids) {
        this.mfrids = mfrids;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String[] manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getIsCreditableVendor() {
        return isCreditableVendor;
    }

    public void setIsCreditableVendor(String isCreditableVendor) {
        this.isCreditableVendor = isCreditableVendor;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorMailId() {
        return vendorMailId;
    }

    public void setVendorMailId(String vendorMailId) {
        this.vendorMailId = vendorMailId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorPhoneNo() {
        return vendorPhoneNo;
    }

    public void setVendorPhoneNo(String vendorPhoneNo) {
        this.vendorPhoneNo = vendorPhoneNo;
    }

    public String getVendorTypeValue() {
        return vendorTypeValue;
    }

    public void setVendorTypeValue(String vendorTypeValue) {
        this.vendorTypeValue = vendorTypeValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getvId() {
        return vId;
    }

    public void setvId(String vId) {
        this.vId = vId;
    }

    public String getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(String VehicleId) {
        this.VehicleId = VehicleId;
    }

    public String getVehicleNo() {
        return VehicleNo;
    }

    public void setVehicleNo(String VehicleNo) {
        this.VehicleNo = VehicleNo;
    }

    public String getVehicleType() {
        return VehicleType;
    }

    public void setVehicleType(String VehicleType) {
        this.VehicleType = VehicleType;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getDriverReponsibility() {
        return driverReponsibility;
    }

    public void setDriverReponsibility(String driverReponsibility) {
        this.driverReponsibility = driverReponsibility;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(String fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getFixedKm() {
        return fixedKm;
    }

    public void setFixedKm(String fixedKm) {
        this.fixedKm = fixedKm;
    }

    public String getFuleExpenseby() {
        return fuleExpenseby;
    }

    public void setFuleExpenseby(String fuleExpenseby) {
        this.fuleExpenseby = fuleExpenseby;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getPaymentScheduleDays() {
        return paymentScheduleDays;
    }

    public void setPaymentScheduleDays(String paymentScheduleDays) {
        this.paymentScheduleDays = paymentScheduleDays;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRateExtraKm() {
        return rateExtraKm;
    }

    public void setRateExtraKm(String rateExtraKm) {
        this.rateExtraKm = rateExtraKm;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getDateWithReeferFullTruck() {
        return dateWithReeferFullTruck;
    }

    public void setDateWithReeferFullTruck(String dateWithReeferFullTruck) {
        this.dateWithReeferFullTruck = dateWithReeferFullTruck;
    }

    public String getDateWithoutReeferFullTruck() {
        return dateWithoutReeferFullTruck;
    }

    public void setDateWithoutReeferFullTruck(String dateWithoutReeferFullTruck) {
        this.dateWithoutReeferFullTruck = dateWithoutReeferFullTruck;
    }

    public String getDestinationIdFullTruck() {
        return destinationIdFullTruck;
    }

    public void setDestinationIdFullTruck(String destinationIdFullTruck) {
        this.destinationIdFullTruck = destinationIdFullTruck;
    }

    public String getDestinationNameFullTruck() {
        return destinationNameFullTruck;
    }

    public void setDestinationNameFullTruck(String destinationNameFullTruck) {
        this.destinationNameFullTruck = destinationNameFullTruck;
    }

    public String getOriginIdFullTruck() {
        return originIdFullTruck;
    }

    public void setOriginIdFullTruck(String originIdFullTruck) {
        this.originIdFullTruck = originIdFullTruck;
    }

    public String getOriginNameFullTruck() {
        return originNameFullTruck;
    }

    public void setOriginNameFullTruck(String originNameFullTruck) {
        this.originNameFullTruck = originNameFullTruck;
    }

    public String getRouteIdFullTruck() {
        return routeIdFullTruck;
    }

    public void setRouteIdFullTruck(String routeIdFullTruck) {
        this.routeIdFullTruck = routeIdFullTruck;
    }

    public String getTravelHourFullTruck() {
        return travelHourFullTruck;
    }

    public void setTravelHourFullTruck(String travelHourFullTruck) {
        this.travelHourFullTruck = travelHourFullTruck;
    }

    public String getTravelKmFullTruck() {
        return travelKmFullTruck;
    }

    public void setTravelKmFullTruck(String travelKmFullTruck) {
        this.travelKmFullTruck = travelKmFullTruck;
    }

    public String getTravelMinuteFullTruck() {
        return travelMinuteFullTruck;
    }

    public void setTravelMinuteFullTruck(String travelMinuteFullTruck) {
        this.travelMinuteFullTruck = travelMinuteFullTruck;
    }

    public String getVehicleIdFullTruck() {
        return vehicleIdFullTruck;
    }

    public void setVehicleIdFullTruck(String vehicleIdFullTruck) {
        this.vehicleIdFullTruck = vehicleIdFullTruck;
    }

    public String getAdditionalCost() {
        return additionalCost;
    }

    public void setAdditionalCost(String additionalCost) {
        this.additionalCost = additionalCost;
    }

    public String getSpotCost() {
        return spotCost;
    }

    public void setSpotCost(String spotCost) {
        this.spotCost = spotCost;
    }

    public String gettType() {
        return tType;
    }

    public void settType(String tType) {
        this.tType = tType;
    }

    public String getTrailerType() {
        return trailerType;
    }

    public void setTrailerType(String trailerType) {
        this.trailerType = trailerType;
    }

    public String getVehicleUnits() {
        return vehicleUnits;
    }

    public void setVehicleUnits(String vehicleUnits) {
        this.vehicleUnits = vehicleUnits;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getaCost() {
        return aCost;
    }

    public void setaCost(String aCost) {
        this.aCost = aCost;
    }

    public String getfCost() {
        return fCost;
    }

    public void setfCost(String fCost) {
        this.fCost = fCost;
    }

    public String getmAllow() {
        return mAllow;
    }

    public void setmAllow(String mAllow) {
        this.mAllow = mAllow;
    }

    public String getoTime() {
        return oTime;
    }

    public void setoTime(String oTime) {
        this.oTime = oTime;
    }

    public String gettFixed() {
        return tFixed;
    }

    public void settFixed(String tFixed) {
        this.tFixed = tFixed;
    }

    public String gettUnits() {
        return tUnits;
    }

    public void settUnits(String tUnits) {
        this.tUnits = tUnits;
    }

    public String getTrailType() {
        return trailType;
    }

    public void setTrailType(String trailType) {
        this.trailType = trailType;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getvCost() {
        return vCost;
    }

    public void setvCost(String vCost) {
        this.vCost = vCost;
    }

    public String getVehicleTypeIddeD() {
        return vehicleTypeIddeD;
    }

    public void setVehicleTypeIddeD(String vehicleTypeIddeD) {
        this.vehicleTypeIddeD = vehicleTypeIddeD;
    }

    public String getAgreedDate() {
        return agreedDate;
    }

    public void setAgreedDate(String agreedDate) {
        this.agreedDate = agreedDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getAeddeD() {
        return aeddeD;
    }

    public void setAeddeD(String aeddeD) {
        this.aeddeD = aeddeD;
    }

    public String getFueldeD() {
        return fueldeD;
    }

    public void setFueldeD(String fueldeD) {
        this.fueldeD = fueldeD;
    }

    public String getUomdeD() {
        return uomdeD;
    }

    public void setUomdeD(String uomdeD) {
        this.uomdeD = uomdeD;
    }

    public String getfHrs() {
        return fHrs;
    }

    public void setfHrs(String fHrs) {
        this.fHrs = fHrs;
    }

    public String getfMin() {
        return fMin;
    }

    public void setfMin(String fMin) {
        this.fMin = fMin;
    }

    public String getEndDateold() {
        return endDateold;
    }

    public void setEndDateold(String endDateold) {
        this.endDateold = endDateold;
    }

    public String getcCategory() {
        return cCategory;
    }

    public void setcCategory(String cCategory) {
        this.cCategory = cCategory;
    }

    public String getrLimit() {
        return rLimit;
    }

    public void setrLimit(String rLimit) {
        this.rLimit = rLimit;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getTrailorTypeId() {
        return trailorTypeId;
    }

    public void setTrailorTypeId(String trailorTypeId) {
        this.trailorTypeId = trailorTypeId;
    }

    public String getFuelHikePercentage() {
        return fuelHikePercentage;
    }

    public void setFuelHikePercentage(String fuelHikePercentage) {
        this.fuelHikePercentage = fuelHikePercentage;
    }

    public String[] getContractId1() {
        return contractId1;
    }

    public void setContractId1(String[] contractId1) {
        this.contractId1 = contractId1;
    }

    public String getTrailerTypeId() {
        return trailerTypeId;
    }

    public void setTrailerTypeId(String trailerTypeId) {
        this.trailerTypeId = trailerTypeId;
    }

}
