

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ page import="ets.domain.operation.business.OperationTO" %> 
    <%@ page import="java.util.*" %> 
    <title>Contract Work Order</title>




</head>
<body>


<script>
    function submitPage(val){
        if(val=='approve'){
            document.mpr.status.value="APPROVED"
            document.mpr.action="/throttle/approveMpr.do"     
            document.mpr.submit();
        }else if(val=='reject'){
        document.mpr.status.value="REJECTED"                
        document.mpr.action="/throttle/approveMpr.do"                
        document.mpr.submit();
    }
}            


function maxlength(field, maxlen) {       
    if (field.value.length > maxlen){
    field.value = field.value.substring(0, maxlen);
    }
} 

function maxCharacters(){    
     var desc = document.mpr.report;
     maxlength(desc,72 );
}  

    function print(ind)
    {       
        if(document.mpr.report.value=='Enter Remarks Here')
        document.mpr.report.value='';
        
        var DocumentContainer = document.getElementById(ind);
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            


</script>

<form name="mpr"  method="post" >                        
    <br>
    

<%
int index=0;
ArrayList poDetail = new ArrayList();
poDetail = (ArrayList) request.getAttribute("woDetail");
OperationTO purch = new OperationTO();
int listSize=10;
int problemNameLimit = 160;
int mfrNameLimit=15;


OperationTO headContent = new OperationTO();
System.out.println("poDetail size in jsp="+poDetail.size());
headContent = (OperationTO)poDetail.get(0);
String[] address = headContent.getAddress();

String problemName = "";
String remarks = "";
String mfrName = headContent.getMfrName();


    if(headContent.getMfrName().length() > mfrNameLimit ){
        mfrName = mfrName.substring(0,mfrNameLimit-1);
    }




for(int i=0; i<poDetail.size(); i=i+listSize) {
%>



<div id="print<%= i %>" >
<style type="text/css">
.header {font-family:Arial;
font-size:15px;
color:#000000;
text-align:center;
 padding-top:10px;
 font-weight:bold;
}
.border {border:1px;
border-color:#000000;
border-style:solid;
}
.text1 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
.text2 {
font-family:Arial, Helvetica, sans-serif;
font-size:16px;
color:#000000;
}
.text3 {
font-family:Arial, Helvetica, sans-serif;
font-size:18px;
color:#000000;
}

</style>    


<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
<tr>
<td valign="top" colspan="2" height="30" align="center" class="border"><strong>WORK ORDER</strong></td>
</tr>
<tr>
<td height="125" valign="top">
<table width="350" height="131" border="0" cellpadding="0" cellspacing="0" class="border">
<tr>
<Td height="80" width="400" class="text3" align="center">
<strong> Your company Name.
</Td>
</tr>
<tr>
<Td align="center" class="text2" style="padding-left:10px; ">address 1,<br>
address 2.<br>
PHONE-xxx xxx.
</Td>
</tr>
</table>
</td>
<td height="125"  valign="top" >

<table width="350" height="131" border="0" cellpadding="0" cellspacing="0">
<tr>
<Td height="125" width="350" valign="top">
<table height="136" width="350" align="left" border="0" cellpadding="0" cellspacing="0" class="border" >
<tr>
<td colspan="2" class="text2" style="padding-left:10px;" ><strong>To</strong></td>
</tr>


<tr>
<td colspan="2" class="text2" style="line-height:20px;"  height="112" style="padding-left:10px;">
    
                                    <p><strong>M/S&nbsp;&nbsp; <%= headContent.getVendorName() %>  </strong> <br>                                
                                    <% for(int k=0;k<address.length ; k++){ %>
                                      <%= address[k] %>   <br>
                                    <% } %>         
  
                                    Phno &nbsp;:&nbsp; <%= headContent.getPhone() %> </p>  
    </td>
</tr>
<tr>
<td class="text3" width="137" style="padding-left:10px;"> WO.No.:<strong><span class="text3" >  S<%= headContent.getWoId() %></span></strong> </td>
<td class="text3" width="161"  style="padding-left:10px;"><strong>Date</strong>:<%= headContent.getCreatedDate() %> </td>
</tr>

</table>
</Td>
</tr>
</table>
</td>
</tr>
<tr>
<Td valign="top" colspan="2">
<table width="700" height="30" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
<tr>
<Td width="233" style="padding-left:10px;" align="left" class="text1"> Vehicle Make&nbsp;:&nbsp;<%= mfrName %></Td>
<Td width="233" align="center" class="text1"> Vehicle Number&nbsp;:&nbsp;<%= headContent.getRegno() %> </Td>
<Td width="233" style="padding-right:28px;" class="text1" align="right" > Vehicle KM&nbsp;:&nbsp;<%= headContent.getCurrentKM() %> </Td>
</tr>
</table>
</Td>
</tr>
<tr>
<Td valign="top" colspan="2">
       
<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">

        
        <tr>
        <Td class="text2" width="30" height="28" valign="top" class="border" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></Td>
        <Td class="text2" valign="top" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>JOB DESCRIPTION</strong></Td>
        <Td class="text2" valign="top" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>Remarks</strong></Td>
        </tr> 
            



<%	index = 0;
	for(int j=i; ( j < (listSize+i) ) && (j<poDetail.size() ) ; j++){	
		purch = new OperationTO();	
		purch = (OperationTO)poDetail.get(j);
                problemName = purch.getProblemName();
                remarks = purch.getRemarks();
                        
                if(purch.getProblemName().length() > problemNameLimit ){
                    problemName = problemName.substring(0,problemNameLimit-1);
                }
        System.out.println("j=="+j);		
%>       
    
                <tr>
                    <Td valign="top" HEIGHT="20" class="text1" width="25"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   > <%= j+1 %> </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" style="border:1px;  border-right-style:solid; border-left-style:solid;" align="left"> <%= problemName %> &nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" style="border:1px;  border-right-style:solid; border-left-style:solid;" align="left"> <%= remarks %> &nbsp; </td>
                </tr>
                
                <%  index++; }  %>

            <% while(index <= listSize ){ %>

	        <tr>
                    <Td valign="top" HEIGHT="20"  class="text1" width="25"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>
                </tr>


            <% index++; } %>
                                            
    </table>
</td>
</tr>


<Tr>
<Td valign="top" colspan="3">
<table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
<Td class="text1" colspan='3' height="20" ALIGN="left" ><strong>REPORT</strong> </Td>
<Td width="600" height="20" style="font-family:Arial, Helvetica, sans-serif;font-size:10px;" colspan='3' ALIGN="right" > <textarea style="overflow: hidden; border : none;" onKeyPress="maxCharacters()" onFocus="document.mpr.report.value='';" name="report" rows="2" cols="75" >Enter Remarks Here</textarea> </Td>
</tr>
<Tr>
<Td width="110" class="text1" >&nbsp;</Td>
<Td width="336">&nbsp;</Td>
<Td width="154" class="text1" align="right" >&nbsp;</Td>
</Tr>

</table>

</td>
</Tr>






<Tr>
<Td valign="top" colspan="3">
<table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
<Td width="110" class="text1" height="30" > &nbsp; </Td>
<Td width="336" class="text1" height="30" >&nbsp;</Td>
<Td width="154" class="text1" height="30" >&nbsp;</Td>
</Tr>

<Tr>
<Td width="110" class="text1"><strong>Indended by</strong></Td>
<Td width="336" class="text1">&nbsp;</Td>
<Td width="154" class="text1"><strong>Authorised Signatory</strong></Td>
</Tr>
</table>
</Td>
</Tr>
</table>
</div>
    <center>   
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;        
    </center>
<br>    
<br>    
<br>    
<% } %>    

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
