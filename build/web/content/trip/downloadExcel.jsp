<%-- 
    Document   : downloadExcel
    Created on : Mar 6, 2021, 12:12:13 PM
    Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.trip.business.TripTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="accountReceivable" method="post">
            <%
                            Date dNow = new Date();
                            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                            //System.out.println("Current Date: " + ft.format(dNow));
                            String curDate = ft.format(dNow);
                            String expFile = "SB_Bill_Format-" + curDate + ".xls";

                            String fileName = "attachment;filename=" + expFile;
                            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                            response.setHeader("Content-disposition", fileName);
            %>

            <br>

           
                <table style="width:auto" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                             <td >CONTAINER NO.</td>
                            <td >CONTAINER SIZE</td>
                            <td >CONTAINER TYPE</td>
                            <td >SHIPPING BILL NO.</td>
                            <td >REQUEST NO.</td>
                            <td >SHIPPING BILL DATE</td>
                            <td >GATE IN DATE</td>
                            <td >TRUCK NO.</td>
                            <td >NO. OF CONTAINER</td>
                            <!--<td >STATUS</td>-->
                            <td >COMMODITY ID</td>
                            <td >COMMODITY NAME</td>
                            
                        </tr>
                    </thead>
                   
                </table>

            

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>

