<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker1").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>
<!--    <script type="text/javascript">
$(document).ready(function () {
                var date2 = new Date();
                $(".datepicker1").datepicker({
                    dateFormat: 'dd-mm-yy'
                }).datepicker('setDate', date2)

            });
    </script>-->
<script>
    function newWindow() {
        window.open('/throttle/content/stores/PriceHistory.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }


    function tyreItemsValidate() {
        var tyreNos = document.getElementsByName("tyreNo");
        var tyreItemIds = document.getElementsByName("tyreItemId");
        var posIds = document.getElementsByName("posIds");
        var tyreCount = document.getElementsByName("tyreCount");
        var acceptedQtys = document.getElementsByName("acceptedQtys");
        var count = 0;
        if (tyreCount.length > 0) {
            for (var i = 0; i < tyreCount.length; i++) {
                count = parseInt(count) + parseInt(acceptedQtys[tyreCount[i].value].value);

            }
            if (parseInt(count) == parseInt(tyreItemIds.length)) {
                //
            } else {
                alert("Enter OEM Details");
                return 'fail';
            }

        } else {
            //
        }

        for (var i = 0; i < tyreItemIds.length; i++) {
            if (tyreItemIds[i].value != '') {
                if (tyreNos[i].value == '') {
                    alert("Please Enter Tyre Number");
                    tyreNos[i].focus();
                    return 'fail';
                }
                if (document.receiveInvoice.inventoryStatus.value == 'N') {
                    if (posIds[i].value == '0') {
                        alert("Please Select Tyre Position");
                        posIds[i].focus();
                        return 'fail';
                    }
                }
            }
        }
        return 'pass';
    }



    function submitItems() {



        document.getElementById("savebutton").style.visibility = "hidden";
        var index = document.getElementsByName("selectedIndex");
        var dat = new Date();
        var month = parseInt(dat.getMonth()) + 1;
        var today = dat.getDate() + '-' + month + '-' + dat.getFullYear();

        if (document.receiveInvoice.inVoiceNumber.value == "" && document.receiveInvoice.dcNumber.value == "") {
            alert("Please Enter Dc Number");
            document.receiveInvoice.inVoiceNumber.focus();
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }
        if (document.receiveInvoice.inVoiceDate.value == "") {
            alert("Please Enter DC Date");
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }

        if (document.receiveInvoice.transactionType.value == "0") {
            alert("Please Select Transaction Type");
            document.receiveInvoice.transactionType.focus();
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }
        if (document.receiveInvoice.transactionType.value == 'DC' && document.receiveInvoice.dcNumber.value == '') {
            document.receiveInvoice.inVoiceNumber.value = '';
            alert("Please enter DC Number");
            document.receiveInvoice.dcNumber.select();
            document.receiveInvoice.dcNumber.focus();
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }
        if (document.receiveInvoice.transactionType.value == 'DC') {
            document.receiveInvoice.inVoiceNumber.value = '';
        }
        for (var i = 0; i < index.length; i++) {

            if (calculateMRP(i) == 'fail') {
                return;
            }

        }
        if (tyreItemsValidate() == 'fail') {
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }
        if (textValidation(document.receiveInvoice.remarks, 'Remarks')) {
            document.getElementById("savebutton").style.visibility = "visible";
            return;
        }
        if (confirm("Please check whether invoice amount is tallied")) {
            document.getElementById("buttonStyle").style.visibility = "hidden";
            document.receiveInvoice.action = '/throttle/handleInsertItems.do';
            document.receiveInvoice.submit();
        }
    }


    function totalBalance(val)
    {

        var index = document.getElementsByName("selectedIndex");
        var itemAmounts = document.getElementsByName("itemAmounts");
        var tot = 0;
        var roundOff = 0;
        if (calculateMRP(val) == 'fail') {
            return 'fail';
        }
        if (calculateAmount(val) == 'fail') {
            return 'fail';
        }

        for (var i = 0; i < index.length; i++) {
            if (itemAmounts[i].value != '') {
                tot = parseFloat(tot) + parseFloat(itemAmounts[i].value);
            }
        }

        if (isFloat(document.receiveInvoice.freight.value)) {
            alert("Please Enter Valid Freight Charges");
            document.receiveInvoice.freight.select();
            document.receiveInvoice.freight.focus();
            return 'fail';
        }

        tot = parseFloat(tot) + parseFloat(document.receiveInvoice.freight.value);
        roundOff = parseFloat(tot).toFixed(0) - parseFloat(tot).toFixed(3);
        roundOff = parseFloat(roundOff).toFixed(3);

        if (parseFloat(roundOff) < 0) {
            roundOff = -(roundOff);
        }
        tot = parseFloat(tot).toFixed(0);
        document.getElementById("total").innerHTML = '<font>' + tot + '</font>';
        document.getElementById("roundOff").innerHTML = '<font>' + roundOff + '</font>';

        document.receiveInvoice.inVoiceAmount.value = tot;
    }




    function calculateAmount(val) {
        var acceptedQty = document.getElementsByName("acceptedQtys");
        var preAcceptedQty = document.getElementsByName("acceptedQty");
        var receivedQty = document.getElementsByName("receivedQtys");
        var orderedQty = document.getElementsByName("orderedQty");
        var mrp = document.getElementsByName("mrps");
        var unitPrice = document.getElementsByName("unitPrice");
        var index = document.getElementsByName("selectedIndex");
        var itemAmounts = document.getElementsByName("itemAmounts");

        if (calculateMRP(val) == 'fail') {
            return 'fail';
        }

        if (isFloat(mrp[val].value)) {
            alert("Please Enter Valid MRP");
            mrp[val].focus();
            mrp[val].select();
            return 'fail';
        }

        if (isFloat(receivedQty[val].value)) {
            alert("Please Enter Valid Received Quantity");
            receivedQty[val].focus();
            receivedQty[val].select();
            return 'fail';
        }

        if (isFloat(acceptedQty[val].value)) {
            alert("Please Enter Valid Accepted Quantity");
            acceptedQty[val].focus();
            acceptedQty[val].select();
            return 'fail';
        }

        if (parseFloat(acceptedQty[val].value) > 0) {
            if (unitPrice[val].value == '0' || unitPrice[val].value == '') {
                alert("Please Enter Valid Unit Price");
                unitPrice[val].focus();
                unitPrice[val].select();
                return 'fail';
            }
        }
        //alert(preAcceptedQty[val].value);
        var preAccepted = 0;
        if (preAcceptedQty[val].value != "") {
            preAccepted = parseFloat(preAcceptedQty[val].value);
        }
        //alert(preAccepted);
        if (parseFloat(receivedQty[val].value) >
                (parseFloat(orderedQty[val].value) - parseFloat(preAccepted))) {
            alert("Please Enter Valid Received Quantity");
            receivedQty[val].focus();
            receivedQty[val].select();
            return 'fail';
        }
        if (parseFloat(acceptedQty[val].value) > parseFloat(receivedQty[val].value)) {
            alert("Please Enter Valid Accepted Quantity");
            acceptedQty[val].focus();
            acceptedQty[val].select();
            return 'fail';
        }

        /*
         if( parseFloat( orderedQty[val].value ) <  parseFloat(acceptedQty[val].value ) ){
         alert("Accepted Quantity Should Not Exceed Ordered Quantity");
         acceptedQty[val].focus();
         acceptedQty[val].select();
         return 'fail';
         }
         */
        itemAmounts[val].value = parseFloat(acceptedQty[val].value) * parseFloat(mrp[val].value);
        itemAmounts[val].value = parseFloat(itemAmounts[val].value).toFixed(2);
        return 'pass';
    }

    function calculateMRP(val)
    {

        var mrp = document.getElementsByName("mrps");
        var unitPrice = document.getElementsByName("unitPrice");
        var tax = document.getElementsByName("tax");
        var discount = document.getElementsByName("discount");
        var priceAfterDiscount = 0;

        if (isFloat(discount[val].value)) {
            alert("Please Enter Valid Discount");
            discount[val].focus();
            discount[val].select();
            return 'fail';
        }
        if (isFloat(tax[val].value)) {
            alert("Please Enter Valid Tax");
            tax[val].focus();
            tax[val].select();
            return 'fail';
        }
        if (isFloat(unitPrice[val].value)) {
            alert("Please Enter Valid Unit Price");
            unitPrice[val].focus();
            unitPrice[val].select();
            return 'fail';
        }

        priceAfterDiscount = parseFloat(unitPrice[val].value) - (parseFloat(unitPrice[val].value) * parseFloat(discount[val].value) / 100);

        mrp[val].value = parseFloat(priceAfterDiscount) + (parseFloat(priceAfterDiscount) * parseFloat(tax[val].value) / 100);
        mrp[val].value = parseFloat(mrp[val].value).toFixed(3);
        return 'pass';
    }

    function validateDiscount(val) {
        var discount = document.getElementsByName("discount");
        var purchaseDiscount = document.getElementsByName("purchaseDiscount");
        if (isFloat(discount[val].value)) {
            alert("Please Enter Valid discount");
            //discount[val].focus();
            discount[val].select();
            return 'fail';
        }
        if (parseFloat(discount[val].value) != parseFloat(purchaseDiscount[val].value)) {
            alert("Entered Discount Value is different from PO discount");
            setTimeout(function() {
                discount[val].focus()
            }, 50);
            discount[val].select();
            return 'fail';
        }

    }
    function validateDiscountTax(val) {
        var discount = document.getElementsByName("discount");
        var purchaseDiscount = document.getElementsByName("purchaseDiscount");
        if (isFloat(discount[val].value)) {
            alert("Please Enter Valid discount");
            //discount[val].focus();
            discount[val].select();
            return 'fail';
        }
        if (parseFloat(discount[val].value) != parseFloat(purchaseDiscount[val].value)) {
            alert("Entered Discount Value is different from PO discount");
            //setTimeout(function () { discount[val].focus() }, 50);
            discount[val].select();
            return 'fail';
        }

        var tax = document.getElementsByName("tax");

        var w = tax[val].selectedIndex;
        var selected_tax = tax[val].options[w].text;


        var purchaseTax = document.getElementsByName("purchaseTax");
        //alert(selected_tax);
        if (selected_tax != purchaseTax[val].value) {
            alert("Entered Tax is different from PO Tax");
            //setTimeout(function () { tax[val].focus() }, 50);
            tax[val].focus();
            return 'fail';
        }

    }

    function validateTax(val) {
        var tax = document.getElementsByName("tax");

        var w = tax[val].selectedIndex;
        var selected_tax = tax[val].options[w].text;


        var purchaseTax = document.getElementsByName("purchaseTax");
        //alert(selected_tax);
        if (selected_tax != purchaseTax[val].value) {
            alert("Entered Tax is different from PO Tax");
            setTimeout(function() {
                tax[val].focus()
            }, 50);
            tax[val].focus();
            return 'fail';
        }

    }
    function validateUnitPrice(val) {
        var unitPrice = document.getElementsByName("unitPrice");
        var purchaseUnitPrice = document.getElementsByName("purchaseUnitPrice");
        if (isFloat(unitPrice[val].value)) {
            alert("Please Enter Valid unitPrice");
            unitPrice[val].focus();
            unitPrice[val].select();
            return 'fail';
        }
        if (parseFloat(unitPrice[val].value) != parseFloat(purchaseUnitPrice[val].value)) {
            alert("Entered Unit Price Value is different from PO Unit Price");
            setTimeout(function() {
                unitPrice[val].focus()
            }, 50);
            unitPrice[val].focus();
            unitPrice[val].select();
        }

    }
    function calculateMargin(val)
    {
        //alert("am here...");
        var itemMrp = document.getElementsByName("itemMrp");
        var unitPrice = document.getElementsByName("unitPrice");
        var margin = document.getElementsByName("margin");
        var discount = document.getElementsByName("discount");
        /*
         if( isFloat(itemMrp[val].value) ){
         alert("Please Enter Selling Price");
         itemMrp[val].focus();
         itemMrp[val].select();
         return 'fail';
         }
         */

        if (isFloat(unitPrice[val].value)) {
            alert("Please Enter Valid Unit Price");
            unitPrice[val].focus();
            unitPrice[val].select();
            return 'fail';
        }
        if (isFloat(discount[val].value)) {
            alert("Please Enter Valid discount");
            discount[val].focus();
            discount[val].select();
            return 'fail';
        }
        var buyPrice = parseFloat(unitPrice[val].value) * (1 - (parseFloat(discount[val].value) / 100));
        var marginPercent = parseFloat(itemMrp[val].value) - parseFloat(buyPrice);
        if (parseFloat(buyPrice) > 0) {
            //margin[val].value = parseFloat( marginPercent ) * 100/  parseFloat( buyPrice ) ;
            margin[val].value = parseFloat(marginPercent) * 100 / parseFloat(itemMrp[val].value);
            margin[val].value = parseFloat(margin[val].value).toFixed(2);
        } else {
            margin[val].value = 0;
        }
        /*
         if( parseFloat(margin[val].value) < 0 ){
         alert("Please Enter Valid Selling Price");
         itemMrp[val].focus();
         itemMrp[val].select();
         return 'fail';
         }
         */

        return 'pass';
    }

    function setValues() {
        document.receiveInvoice.vendorId.focus();
        if ('<%=request.getAttribute("inVoiceNumber")%>' != 'null') {
            document.receiveInvoice.inVoiceNumber.value = '<%=request.getAttribute("inVoiceNumber")%>';
        }
        if ('<%=request.getAttribute("inVoiceDate")%>' != 'null') {
            document.receiveInvoice.inVoiceDate.value = '<%=request.getAttribute("inVoiceDate")%>';
        }
        if ('<%=request.getAttribute("inVoiceAmount")%>' != 'null') {
            document.receiveInvoice.inVoiceAmount.value = '<%=  request.getAttribute("inVoiceAmount")%>';
        }
        if ('<%=request.getAttribute("poIds")%>' != 'null') {
            document.receiveInvoice.poIds.value = '<%= request.getAttribute("poIds")%>';
        }
        if ('<%=request.getAttribute("dcNumber")%>' != 'null') {
            document.receiveInvoice.dcNumber.value = '<%=  request.getAttribute("dcNumber")%>';
        }
        if ('<%=request.getAttribute("vendorId")%>' != 'null') {
            document.receiveInvoice.vendorId.value = '<%= request.getAttribute("vendorId")%>';
        }

        ajaxPoList(document.receiveInvoice.vendorId.value);
    }

    function submitPage() {
        if (document.receiveInvoice.poIds.value != '0') {
//document.receiveInvoice.inVoiceDate.value = dateFormat(document.receiveInvoice.inVoiceDate)
            document.receiveInvoice.action = '/throttle/purchaseItems.do';
            document.receiveInvoice.submit();
        }
    }


    function ajaxPoList(value) {
        var vendorId = value;
        if (vendorId != 0) {

            /*
             if(textValidation(document.receiveInvoice.inVoiceAmount,'Invoice Amount')){
             document.receiveInvoice.vendorId.value = '0';
             return;
             }
             */

            /*
             if(document.receiveInvoice.inventoryStatus.value==0){
             alert("Inventory Status Should not be Empty");
             document.receiveInvoice.vendorId.value = '0';
             return;
             }
             */
            var url = '/throttle/purchaseOrderList.do?vendorId=' + vendorId;
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }
    }

    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                var splt = temp.split('-');
                setOptions1(temp, document.receiveInvoice.poIds);
                document.receiveInvoice.poIds.value = '<%= request.getAttribute("getPO")%>';
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
    function setOptions1(text,variab) {
                                //  alert("setOptions on page"+text)
//                                alert("text = "+text+ "variab = "+variab)
                                        variab.options.length = 0;
                                        //                                alert("1")
                                        var option0 = new Option("--select--", '0');
                                        //                                alert("2")
                                        variab.options[0] = option0;
                                        //                                alert("3")


                                        if (text != "") {
                                            //                                    alert("inside the condition")
                                            var splt = text.split('~');
                                            var temp1;
                                            variab.options[0] = option0;
                                            for (var i = 0; i < splt.length; i++) {
                                                //                                    alert("splt.length ="+splt.length)
                                                //                                    alert("for loop ="+splt[i])
                                                //temp1 = splt[i].split('-');
                                                option0 = new Option(splt[i], splt[i])
                                                //alert("option0 ="+option0)
                                                variab.options[i + 1] = option0;
                                            }
                                        }
                                    }

    function setSelectbox(i)
    {
        var selected = document.getElementsByName("selectedIndex");
        selected[i];
    }
</script>

<script>

    var httpRequest1;
    function getOthers(rowIndex) {
        var url = "./getQuandity.do";
        var mfrCode = document.getElementById("mfrCode" + rowIndex).value;
        var itemCode = document.getElementById("itemCode" + rowIndex).value;
        var itemName = document.getElementById("itemName" + rowIndex).value;
        var inVoiceDate = document.getElementById("inVoiceDate").value;
        if (inVoiceDate != '') {
            var temp = inVoiceDate.split('-');
            var date = temp[0] + temp[1] + temp[2];
        }
        $.ajax({
            url: url,
            data: {mfrCode: mfrCode, itemCode: itemCode, itemName: itemName},
            type: "GET",
            success: function(response) {
                if (response.toString().trim() != "") {
                    var details = response.split('~');
                    document.getElementById("itemName" + rowIndex).value = details[5];
                    document.getElementById("itemCode" + rowIndex).value = details[4];
                    document.getElementById("mfrCode" + rowIndex).value = details[3];
                    document.getElementById("uom" + rowIndex).value = details[2];
                    document.getElementById("tyreItemId" + rowIndex).value = details[0];
                    if (details[6] == '1011' || details[6] == '1076') {
                        document.getElementById("tyreNo" + rowIndex).value = '';
                    } else {
                        //var sno= +rowIndex + +1;
                        //var oem=details[5]+date+sno;
                        //document.getElementById("tyreNo"+rowIndex).value=oem;
                        document.getElementById("tyreNo" + rowIndex).value = '';
                    }
                    //document.getElementById("tyreNo"+rowIndex).focus();
                } else {
                    alert("Invalid value");
                    document.getElementById("mfrCode" + rowIndex).focus();
                }


            },
            error: function(xhr, status, error) {
            }
        });
        document.getElementById("FreesButton").style.visibility = "hidden";

    }

    var httpRequest;
    var httpReq;
    var rowCount = 2;
    var sno = 0;
    var rowIndex = 0;

    function addRow(mfrCode)
    {
        sno++;
        //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
        //alert("am here 6..."+rowCount);
        var tab = document.getElementById("items");
        var newrow = tab.insertRow(rowCount);

        var cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' > <input type='hidden' id='tyreItemId" + rowIndex + "' name='tyreItemId' value=''  > " + sno + "</td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='25' ><input name='mfrCode' id='mfrCode" + rowIndex + "' class='form-control' value='" + mfrCode + "' type='text'></td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell0;
        cell = newrow.insertCell(2);
        var cell1 = "<td class='text1' height='25'><input name='itemId' id='itemId" + rowIndex + "' class='form-control' value='' type='hidden'><input name='itemCode' id='itemCode" + rowIndex + "' class='form-control' value='' type='text'></td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell1;

        cell = newrow.insertCell(3);
        var cell2 = "<td class='text1' height='25'><input name='itemName' id='itemName" + rowIndex + "'  value='' class='form-control'  type='text'></td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell2;

        cell = newrow.insertCell(4);
        var cell3 = " <td class='text1' height='25'><input name='uom' id='uom" + rowIndex + "' size='5' readonly class='form-control' value='' type='text'></td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell3;
        getOthers(rowIndex);

        cell = newrow.insertCell(5);
        var cell4 = " <td class='text1' height='25'><input name='tyreNo' id='tyreNo" + rowIndex + "'  class='form-control'  type='text'></td>";
        cell.setAttribute("className", "text2");
        cell.innerHTML = cell4;
        cell = newrow.insertCell(6);
        if (document.receiveInvoice.inventoryStatus.value == 'N') {
            var cell4 = "<td class='text1' height='30'> <select class='form-control' name='posIds'  style='width:150px;'><option  selected value='0'>---Select---</option>" +
                    "<c:if test = "${positionList != null}" ><c:forEach items="${positionList}" var="sec"><option  value='<c:out value="${sec.posId}" />'><c:out value="${sec.posName}" /></c:forEach ></c:if>" +
                                        "</select></td>";
                            } else {
                                var cell4 = " <td class='text1' height='25'><input name='posIds' value='0'  class='form-control'  type='hidden'></td>";
                            }
                            cell.setAttribute("className", "text2");
                            cell.innerHTML = cell4;


                            cell = newrow.insertCell(7);
                            var cell2 = "<td class='text1' height='25'><input name='warranty' id='warranty" + rowIndex + "'  value='12' class='form-control'  type='text'></td>";
                            cell.setAttribute("className", "text2");
                            cell.innerHTML = cell2;


                            cell = newrow.insertCell(8);
                            var cell4 = " <td class='text1' height='25'><input name='checkBoxIndex' onClick='clearFieldValues(" + rowIndex + ")' type='checkbox'></td>";
                            cell.setAttribute("className", "text2");
                            cell.innerHTML = cell4;

//                   getItemNames(rowIndex);
                            rowIndex++;
                            rowCount++;

                        }


//function getItemNames(val){
//    var itemTextBox = 'itemName'+ val;
//    var oTextbox = new AutoSuggestControl(document.getElementById(itemTextBox),new ListSuggestions(itemTextBox,"/throttle/handleItemSuggestions.do?"));
//    //getVehicleDetails(document.getElementById("regno"));
//}



                        function clearFieldValues(val)
                        {
                            var tyreItemId = document.getElementsByName("tyreItemId");
                            var checkBoxIndex = document.getElementsByName("checkBoxIndex");
                            var mfrCode = document.getElementsByName("mfrCode");
                            var itemCode = document.getElementsByName("itemCode");
                            var itemName = document.getElementsByName("itemName");
                            var uom = document.getElementsByName("uom");
                            var tyreNo = document.getElementsByName("tyreNo");
                            var posIds = document.getElementsByName("posIds");

                            if (checkBoxIndex[val].checked == true) {
                                getItemNames(val);
                                tyreItemId[val].value = '';
                                mfrCode[val].value = '';
                                itemCode[val].value = '';
                                itemName[val].value = '';
                                uom[val].value = '';
                                tyreNo[val].value = '';
                                posIds[val].value = '0';
                            }

                        }

                        function calculateAcceptedQty(val) {
                            if (calculateAmount(val) == 'fail') {
                                return 'fail';
                            }
                            if (totalBalance(val) == 'fail') {
                                return 'fail';
                            }
                        }




        </script>




        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ReceiveItems"  text="ReceiveItems"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.ReceiveItems"  text="ReceiveItems"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <% int indx = 0;%>

            <body onload="setValues();" >
                <form name="receiveInvoice" method="post" >


                    <%@ include file="/content/common/message.jsp" %>

                    <!--<table class="table table-bordered">-->
                    <tr align="left" >
                        <td width="80%"  align="left" >

                            <table class="table table-info mb30 table-hover">
                                <thead>

                                    <tr><th colspan="4"><spring:message code="service.label.ReceiveItems"  text="ReceiveItems"/></th>
                                </thead>
                                <input name="inVoiceNumber"    maxlength='30'  class="form-control"  type="hidden" value="0" size="20">
                                <td ><font color="red">*</font><spring:message code="stores.label.Vendor"  text="default text"/></td>

                                <td >
                                    <select class="form-control" name="vendorId" onchange="ajaxPoList(this.value);"   style="width:260px;height:40px;" >
                                        <option value="0"><spring:message code="stores.label.Na"  text="default text"/></option>
                                        <c:if test = "${vendorLists != null}" >
                                            <c:forEach items="${vendorLists}" var="vendor">
                                                <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select> </td>
                                <td ><font color="red">*</font><spring:message code="stores.label.PO/LPONo"  text="default text"/></td>

                                <td ><select class="form-control" name="poIds"    style="width:260px;height:40px;" onChange="submitPage();">

                                        <option value="0">-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                        <c:if test = "${getPo != null}" >
                                            <c:forEach items="${getPo}" var="po">
                                                <option value="<c:out value="${po.poId}"/>"><c:out value="${po.poId}"/></option>

                                            </c:forEach>
                                        </c:if>
                                    </select> </td>

                    </tr>

                    <tr>
                        <td ><font color="red">*</font><spring:message code="stores.label.InventoryStatus"  text="default text"/></td>
                        <td ><select class="form-control" name="inventoryStatus" style="width:260px;height:40px;" >
                                <c:if test = "${ItemList != null}" >
                                    <% int ind=0; %>
                                    <c:forEach items="${ItemList}" var="item">
                                        <% if(ind==0){ %>
                                        <c:if test = "${item.inventoryStatus == 'Y'}" >
                                            <option value="Y"><spring:message code="stores.label.NormalPurchase"  text="default text"/></option>
                                        </c:if>
                                        <c:if test = "${item.inventoryStatus == 'N'}" >
                                            <option value="N"><spring:message code="stores.label.DirectPurchase"  text="default text"/></option>
                                        </c:if>
                                        <% ind++; } %>
                                    </c:forEach>
                                </c:if>
                            </select></td>
                    <input name="inVoiceAmount" readonly  maxlength='15' class="form-control" type="hidden" value="" size="20">
                    <td ><font color="red">*</font><spring:message code="stores.label.DCNumber"  text="default text"/></td>
                    <td> <input name="dcNumber"  maxlength='30'  style="width:260px;height:40px;" class="form-control"  type="text" value="" size="20"> </td>
                    </tr>

                    <tr>
                        <td><font color="red">*</font><spring:message code="stores.label.TransactionType"  text="default text"/></td>
                        <td>
                            <select class="form-control" name="transactionType" style="width:260px;height:40px;" >
                                <!--<option value="0" > --Select--</option>
                                <option  value="INVOICE" > INVOICE </option>-->
                                <option selected value="DC" > <spring:message code="stores.label.DC"  text="DC"/> </option>
                            </select>
                        </td>
                        <td><font color="red">*</font><spring:message code="stores.label.DCdate"  text="ReceiveItems"/>: </td>
                        <!--<td><input name="inVoiceDate" id="inVoiceDate" readonly  class="form-control datepicker1"  type="text" value="" >-->
                        <td><input name="inVoiceDate" id="inVoiceDate" readonly style="width:260px;height:40px;" type="text"  class="datepicker form-control"  >
                            <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receiveInvoice.inVoiceDate,'dd-mm-yyyy',this)"/></td>-->
                        </td>

                    </tr>

                    </table>

                    </td>
                    <td width="20%" >

                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2" ><spring:message code="stores.label.TOTAL"  text="default text"/></th>
                                </tr>
                            </thead>
                            <tr>
                                <td width="121" height="30"  ><spring:message code="operations.label.Currency"  text="default text"/></td>
                                <td width="86" height="30" ><div id="total" ></div>  </td>
                            </tr>
                            <tr>
                                <td width="121" height="30"  ><spring:message code="stores.label.RoundOff"  text="default text"/></td>
                                <td width="86" height="30" ><div id="roundOff" ></div>  </td>
                            </tr>

                        </table>
                    </td>
                    </tr>

                    </table>


                    <c:if test = "${ItemList != null}" >
                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th><spring:message code="stores.label.SNo"  text="default text"/></th>
                                    <th><spring:message code="stores.label.MfrCode"  text="default text"/></th>
                                    <th><spring:message code="stores.label.CompanyCode"  text="default text"/> </th>
                                    <th><spring:message code="stores.label.ItemName"  text="default text"/></th>
                                    <th><spring:message code="stores.label.UOM"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Dis(%)"  text="default text"/></th>
            <!--                        <th><spring:message code="stores.label.Tax(%)"  text="default text"/></th>-->
                                    <th><spring:message code="stores.label.UNITPRICE"  text="default text"/></th>
            <!--                        <th><spring:message code="stores.label.Price(withTax)"  text="default text"/></th>-->
                                    <th><spring:message code="stores.label.PreviousPrice(usd)"  text="default text"/></th>
            <!--                        <th><spring:message code="stores.label.SellingPrice(usd)"  text="default text"/> </th>
                                    <th><spring:message code="stores.label.Margin(%)"  text="default text"/></th>-->
                                    <th><spring:message code="stores.label.OrderedQty"  text="default text"/></th>
                                    <th><spring:message code="stores.label.PreviouslyAccepted"  text="default text"/></th>
                                    <th><spring:message code="stores.label.RecdQty"  text="default text"/></th>
                                    <th><spring:message code="stores.label.AcceptedQty"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Amount(usd)"  text="default text"/></th>


                                </tr>
                            </thead>
                            <% int index = 0;%>
                            <c:forEach items="${ItemList}" var="item">
                                <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                %>

                                <tr>
                                    <td><%=index + 1%></td>
                                    <td><input type="hidden" name="mfrCodes" id="mfrCodes<%=index + 1%>" value="<c:out value="${item.mfrCode}"/>"/><c:out value="${item.mfrCode}"/></td>
                                    <td><input type="hidden" name="paplCodes" id="paplCodes<%=index + 1%>" value="<c:out value="${item.paplCode}"/>"/><c:out value="${item.paplCode}"/></td>
                                    <td><input type="hidden" name="itemIds" value="<c:out value="${item.itemId}"/>" ><c:out value="${item.itemName}"/></td>
                                    <td><c:out value="${item.uomName}"/></td>
                                    <td>
                                        <input type="text" name="discount" value="0" onblur="validateDiscountTax('<%= index %>');"  onChange="calculateMargin('<%= index %>');
                                                calculateMRP('<%= index %>');
                                                calculateAcceptedQty(<%= index %>);
                                                setSelectbox('<%= index %>');" size="4" class="form-control">

                                        <b><c:out value="${item.discount}"/></b>
                                        <input type="hidden" name="purchaseDiscount" value=<c:out value="${item.discount}"/> />
                                    </td>
                                    <%--
                                    <td>
                                    <select name="tax" class="form-control" onblur="validateDiscountTax('<%= index %>');" onChange="calculateAcceptedQty(<%= index %>);calculateMRP('<%= index %>');setSelectbox('<%= index %>');" >
        <!--                                <option value="0" selected>sel</option>-->
                                        <c:if test="${vatList!=null}" >
                                        <c:forEach items="${vatList}" var="vat" >
                                            <option value="<c:out value="${vat.vat}"/>" > <c:out value="${vat.vat}"/> </option>
                                        </c:forEach>
                                    </c:if>
                                    </select>
                                    <b><c:out value="${item.tax}"/></b>
                                    </td>
                                    --%>
                                    <td>
                                        <input type="hidden" name="tax" value="0"/> 
                                        <input type="hidden" name="purchaseTax" value=<c:out value="${item.tax}"/> />
                                        <input type="text" name="unitPrice" value="0"  onChange="validateUnitPrice('<%= index %>');
                                                calculateMRP('<%= index %>');
                                                calculateAcceptedQty(<%= index %>);
                                                calculateMargin('<%= index %>');
                                                setSelectbox('<%= index %>');" size="7" class="form-control">

                                        <b><c:out value="${item.unitPrice}"/></b>
                                        <input type="hidden" name="purchaseUnitPrice" value=<c:out value="${item.unitPrice}"/>>
                                    </td>
                                    <!--                            <td>-->
                                <input type="hidden" readonly name="mrps" value="" onchange="setSelectbox('<%= index %>');" size="7" class="form-control">
<!--                                <b><c:out value="${item.mrp}"/></b>-->
                                <input type="hidden" name="purchaseMrp" value=<c:out value="${item.mrp}"/> />
                                <!--                            </td>-->
                                <td><c:out value="${item.prePrice}"/></td>
                                <!--                            <td>-->
                                <input type="hidden" readonly name="itemMrp" value='<c:out value="${item.itemMrp}"/>' onblur="calculateMargin('<%= index %>');"  size="7" class="form-control">
                                <!--                            </td>
                                                            <td>-->
                                <input type="hidden" readonly name="margin" value="" size="7" class="form-control">
                                <!--                            </td>-->

                                <td><c:out value="${item.approvedQty}"/><input type="hidden" name="orderedQty" value=<c:out value="${item.approvedQty}"/> > </td>
                                <td><c:out value="${item.acceptedQty}"/><input type="hidden" name="acceptedQty" value=<c:out value="${item.acceptedQty}"/> ></td>
                                <td><input type="text" name="receivedQtys" value="0" onchange="setSelectbox('<%= index %>');" size="7" class="form-control"></td>

                                <td><input type="text" name="acceptedQtys" id="acceptedQtys" value="0" onchange="setSelectbox('<%= index %>');" onblur="calculateAcceptedQty(<%= index %>);" size="7" class="form-control"></td>

                                <td><input type="text"  name="itemAmounts" size="7" class="form-control" readonly value=""></td>

                                <input type="hidden" name="selectedIndex" value='<%= index %>'>
                                <input type="hidden" name="validate" value='N' >

                                <input type="hidden" name="categoryId" value='<c:out value="${item.categoryId}"/>' >
                                <c:if test="${item.categoryId==1021}" >
                                    <input type="hidden" name="tyreCount" value='<%= index %>' >
                                </c:if>
                                </tr>
                                <%
                    index++;
                                %>
                            </c:forEach>
                        </table>
                        <script>

                            function checkApproved() {
                                var acceptedQtys = document.getElementsByName("acceptedQtys");
                                var paplCode = document.getElementsByName("paplCodes");
                                var mfrCode = document.getElementsByName("mfrCodes");
                                var catIds = document.getElementsByName("categoryId");

                                var qty = 0;
                                for (var j = 0; j < paplCode.length; j++) {
                                    if (catIds[j].value == '1011') {
                                        qty = parseInt(acceptedQtys[j].value);
                                        for (var i = 0; i < parseInt(qty); i++) {
                                            addRow(mfrCode[j].value);

                                        }
                                    }
                                }

                                document.getElementById("acceptedQtys").readOnly = true
                            }
                        </script>


                        <table class="table table-info mb30 table-hover" id="items">
                            <thead>
                                <tr>
                                    <th colspan="9"><spring:message code="stores.label.OEMDETAILS"  text="default text"/></th>
                                </tr>
                                <tr>
                                    <th><spring:message code="stores.label.SNo"  text="default text"/></th>
                                    <th><spring:message code="stores.label.MFRItemCode"  text="default text"/></th>
                                    <th><spring:message code="stores.label.PAPLItemCode"  text="default text"/></th>
                                    <th><spring:message code="stores.label.ItemName"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Uom"  text="default text"/></th>
                                    <th><spring:message code="stores.label.OEM(TyreNo,BatteryNO,Others)"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Warranty(Months)"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Position"  text="default text"/></th>
                                    <th><spring:message code="stores.label.Clear"  text="default text"/></th>
                                </tr>
                        </table>
                        <%--
                                         <br>
                                         <center><div class="text2" align="center" > Remarks &nbsp;&nbsp;:&nbsp;&nbsp; <textarea class="form-control" name="remarks" rows="2" cols="15" ></textarea> </center>
                                        <br>
                                        <br>

                 <div id="buttonStyle" style="visibility:visible;" align="center" >
                <input class="button" type="button" id="FreesButton" value="Freeze Qty" onclick="checkApproved();">
                 </div>
                <br>
                <br>
                <center>
                    Freight Charges &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" class="form-control" size="5" name="freight" onblur="totalBalance('0');" value="0" >
                </center>
                <br>

                <div id="buttonStyle" style="visibility:visible;" align="center" >
                    <input class="button" type="button" id="savebutton" value="Save" onclick="submitItems();">&nbsp;
                    <!--<input value="Add Row" class="button" type="button" onClick="addRow();">-->
                </div>
                        --%>
                        <table class="table table-bordered">
                            <tr>
                                <td><spring:message code="stores.label.Remarks"  text="default text"/></td>
                                <td><textarea class="form-control" name="remarks" ></textarea></td>
                                <td><spring:message code="stores.label.FreightCharges"  text="default text"/></td>
                                <td><input type="text" class="form-control" size="5" name="freight" onblur="totalBalance('0');" value="0" ></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" >
                                    <div id="buttonStyle" style="visibility:visible;" >
                                        <input class="button" type="button" id="FreesButton" value="<spring:message code="stores.label.FreezeReceipt"  text="default text"/>" onclick="checkApproved();">
                                    </div>
                                    &nbsp;&nbsp;
                                    <input class="button" type="button" id="savebutton" value="<spring:message code="stores.label.Save"  text="default text"/>" onclick="submitItems();">
                                </td>
                            </tr>
                        </table>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
