<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>
<script language="javascript" src="/throttle/js/validate.js"></script>  
 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.PeriodicService" text="default text"/>  </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.PeriodicService" text="default text"/></li>
          <li class="active"><spring:message code="serviceplan.label.Add" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body onload="document.maintenance.serviceName.focus();">
<form name="maintenance" method="post" >



<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>


 <table class="table table-info mb30 table-hover" >
      
     <thead>
<tr >
<th colspan="6" ><div ><spring:message code="serviceplan.label.AddMaintenance" text="default text"/></div></th>
</tr>
</thead>
<tr>
<td  height="30" ><spring:message code="serviceplan.label.MaintenanceName" text="default text"/> </td>
<td   height="30" ><input type="text" maxlength="100" style="width:260px;height:40px;"  class="form-control" name="serviceName" style="width:125px; " value=""/></td>

<td  height="30" ><spring:message code="serviceplan.label.Description" text="default text"/></td>
<td  height="30" ><textarea rows="2" name="description" style="width:260px;height:40px;"  class="form-control" onkeyup="maxlength(this.form.description,200)" style="width:125px; "  cols="30"></textarea></td></tr>
 </table>

<center>
<input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.SAVE" text="default text"/>" name="Save" onClick="submitPage(this.name)">
&emsp;<input type="reset" class="btn btn-success"  value="<spring:message code="serviceplan.label.CLEAR" text="default text"/>">
</center>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
<script>
   function submitPage(value){
       if(textValidation(document.maintenance.serviceName,'Maintenance Name'));
       else if(textValidation(document.maintenance.description,'Description'));
       else{
document.maintenance.action = '/throttle/insertMaintenance.do';
document.maintenance.submit();
}
} 
</script>

</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>>
