<%--  
    Document   : VehicleAMCUpdate
    Created on : 19 Mar, 2012, 4:09:29 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle AMC Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleAMC.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleAMC.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleAMC.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleAMC.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleAMC.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleAMC.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleAMC.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {
                
                //alert("ajax");
                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                    
                }
                
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            //                            alert(detail);
                            if(detail != "null"){
                                var vehicleValues = detail.split("~");
                                document.VehicleAMC.vehicleId.value = vehicleValues[0];
                                document.VehicleAMC.chassisNo.value = vehicleValues[1];
                                document.VehicleAMC.engineNo.value = vehicleValues[2];
                                document.VehicleAMC.vehicleMake.value = vehicleValues[4];
                                document.VehicleAMC.vehicleModel.value = vehicleValues[5];
                                if(parseInt(vehicleValues[8]) > 0){
                                    document.getElementById('exMsg').innerHTML="Vehicle AMC Entry is Already Existing";
                                     document.getElementById('detail').style.display="none";
                                }
                            }else {
                                document.VehicleAMC.vehicleId.value = "";
                                document.VehicleAMC.chassisNo.value = "";
                                document.VehicleAMC.engineNo.value = "";
                                document.VehicleAMC.vehicleMake.value = "";
                                document.VehicleAMC.vehicleModel.value = "";
                                document.getElementById('exMsg').innerHTML = "";
                            }
                        }
                       
                    }

                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
               
            }


            function submitPage() {
                if(textValidation(document.VehicleAMC.regno,'Vehicle No'));
                else{
                    document.VehicleAMC.action = '/throttle/saveVehicleAMC.do';
                    document.VehicleAMC.submit();
                }
            }


            function getEvents(e,val){
                var key;
                if(window.event){
                    key = window.event.keyCode;
                }else {
                    key = e.which;
                }
                if(key == 0) {
                    getVehicleDetails(val);
                }else{
                    getVehicleDetails(val);
                }
            }

        </script>

    </head>

    <body onLoad="getVehicleNos();document.VehicleAMC.regno.focus();">
        <form name="VehicleAMC" method="post">
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle AMC Add</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <tr>
                    <td class="texttitle1">Vehicle No</td>
                    <td class="text1">
                        <input type="text" name="regNo" id="regno" class="form-control" onkeypress="getEvents(event,this.value);" onblur="getVehicleDetails(this.value);" autocomplete="off" />
                        <!--                            <input type="text" name="regNo" id="regno" class="form-control"  onchange="getVehicleDetails(this.value);" onkeypress="getVehicleDetails(this.value);" autocomplete="off" />-->
                        <input type="hidden" name="vehicleId" id="vehicleId" /></td>
                    <td class="texttitle1">Make</td>
                    <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" readonly /></td>
                </tr>
                <tr>
                    <td class="texttitle2">Model</td>
                    <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" readonly /></td>
                    <td class="texttitle2">Usage</td>
                    <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly /></td>
                </tr>
                <tr>
                    <td class="texttitle1">Engine No</td>
                    <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" readonly /></td>
                    <td class="texttitle1">Chassis No</td>
                    <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" readonly /></td>
                </tr>
                <tr>
                    <td class="texttitle2" colspan="4">
                        <center><label id="exMsg" style="color: green; text-align: center; font-weight: bold; font-size: medium;"></label></center>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <div id="detail" style="display: block;">
                            <table width="100%">
                                <tr>
                                    <td class="contenthead" colspan="4">AMC Details</td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">AMC Company</td>
                                    <td class="text1"><input type="text" name="amcCompanyName" id="amcCompanyName" class="form-control" onclick="getVehicleDetails(regno.value);" /></td>
                                    <td class="texttitle1">AMC Amount</td>
                                    <td class="text1"><input type="text" name="amcAmount" id="amcAmount" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">AMC Duration</td>
                                    <td class="text2"><input type="text" name="amcDuration" id="amcDuration" class="form-control" /></td>
                                    <td class="texttitle2">AMC Cheque Date</td>
                                    <td class="text2"><input type="text" name="amcChequeDate" id="amcChequeDate" class="datepicker" /></td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">AMC Cheque No</td>
                                    <td class="text1"><input type="text" name="amcChequeNo" id="amcChequeNo" class="form-control" /></td>
                                    <td class="texttitle1">AMC From Date</td>
                                    <td class="text1"><input type="text" name="amcFromDate" id="amcFromDate" class="datepicker" /></td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">AMC To Date</td>
                                    <td class="text2"><input type="text" name="amcToDate" id="amcToDate" class="datepicker" /></td>
                                    <td class="texttitle2">Remarks</td>
                                    <td class="text2"><input type="text" name="amcRemarks" id="amcRemarks" class="form-control" /></td>
                                </tr>

                                <tr>
                                    <td align="center" class="texttitle1" colspan="4"></td>
                                </tr>

                            </table>
                            <br>
                            <center>
                                <input type="button" value="Update" name="generate" id="generate" class="button" onclick="submitPage();">
                                <input type="reset" value="Clear" class="button">
                            </center>
                        </div>
                    </td>
                </tr>
            </table>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            //getVehicleDetails(document.getElementById("regno").value);        
        }
    </script>

</html>
