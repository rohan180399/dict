
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>





<script type="text/javascript">


$(document).ready(function() {

    $("#datepicker").datepicker({
        showOn: "button",
        format: "dd-mm-yyyy",
        autoclose: true,
        buttonImage: "calendar.gif",
        buttonImageOnly: true

    });



});

$(function() {
    $(".datepicker").datepicker({
        format: "dd-mm-yyyy",
        autoclose: true
    });

});


</script>

<script>
    function submitPage(val) {
        /*                if(val =='return'){
         document.modifyGrn.action="/throttle/handleDirectPurchaseRetQty.do"                
         document.modifyGrn.submit();                                    
         }
         else{*/
        if (validate() != 'submit') {
            return;
        }
        document.modifyGrn.action = "/throttle/handleDirectPurchaseRetQty.do"
        document.modifyGrn.submit();
    }

    function submitPage1(val) {
        var index = document.getElementsByName("selectedIndex");
        if (document.modifyGrn.inVoiceNumber.value == "") {
            alert("Please Enter Invoice Number");
            document.modifyGrn.inVoiceNumber.focus();
            return;
        }
        for (var i = 0; i < index.length; i++) {
            if (calculateMRP(i) == 'fail') {
                return;
            }
            if (calculateAmount(i) == 'fail') {
                return;
            }
            totalBalance(i);
        }
        document.getElementById("buttonStyle").style.visibility = "hidden";
        document.modifyGrn.action = "/throttle/updateGrn.do"
        document.modifyGrn.submit();
    }


    function setAmount(val) {
        var acceptedQtys = document.getElementsByName("acceptedQtys");
        var itemAmounts = document.getElementsByName("itemAmounts");
        var mrps = document.getElementsByName("mrps");
        if (floatValidation(acceptedQtys[val], "acceptedQty")) {
            acceptedQtys[val].focus();
            return;
        }
        itemAmounts[val].value = parseFloat(acceptedQtys[val].value) * parseFloat(mrps[val].value);
        itemAmounts[val].value = parseFloat(itemAmounts[val].value).toFixed(2);
    }





    function validate() {
        var selectedInd = document.getElementsByName("selectedIndex");
        var retQty = document.getElementsByName("returnedQtys");
        var acceptedQtys = document.getElementsByName("acceptedQtys");
        var counter = 0;
        if (floatValidation(document.modifyGrn.inVoiceAmount, "InVoice Amount")) {
            return;
        }
        if (textValidation(document.modifyGrn.dcNumber, "DC Number")) {
            return;
        }
        /*
         if(textValidation(document.modifyGrn.inVoiceNumber,"Invoice Number") ){
         return;
         }  
         */
        for (var i = 0; i < acceptedQtys.length; i++) {
            if (selectedInd[i].checked == true) {
                counter++;
                if (floatValidation(acceptedQtys[i], "acceptedQty")) {
                    acceptedQtys[i].focus();
                    return "notSubmit";
                }
                if (floatValidation(retQty[i], "returned Qty")) {
                    retQty[i].focus();
                    return "notSubmit";
                }
            }
        }
        if (counter == 0) {
            alert('Please Select any Item');
            return "notSubmit";
        }
        return "submit"
    }


    function calculateTotQty(val) {

        if (calculateAmount(val) == 'fail') {
            return 'fail';
        }

        if (calculateMRP(val) == 'fail') {
            return 'fail';
        }
        totalBalance(val);
    }


    function calculateAmount(val) {
        var totalQtys = document.getElementsByName("totalQtys");
        var returnedQty = document.getElementsByName("retQtys");
        var acceptedQty = document.getElementsByName("acceptedQtys");

        var mrp = document.getElementsByName("mrps");
        var index = document.getElementsByName("selectedIndex");
        var itemAmounts = document.getElementsByName("itemAmounts");

        if (isFloat(returnedQty[val].value)) {
            alert("Please Enter Valid Returned Qty");
            returnedQty[val].focus();
            returnedQty[val].select();
            return 'fail';
        }
        else if (returnedQty[val].value == '0') {
            totalQtys[val].value = acceptedQty[val].value;
        }
        else if (parseFloat(returnedQty[val].value) > parseFloat(acceptedQty[val].value)) {
            alert("Returned quantity should not exceed accepted quantity");
            returnedQty[val].focus();
            returnedQty[val].select();
            return 'fail';
        }

        totalQtys[val].value = parseFloat(acceptedQty[val].value) - parseFloat(returnedQty[val].value);

        if (calculateMRP(val) == 'fail') {
            return 'fail';
        }

        if (isFloat(mrp[val].value)) {
            alert("Please Enter Valid MRP");
            mrp[val].focus();
            mrp[val].select();
            return 'fail';
        }


        itemAmounts[val].value = parseFloat(totalQtys[val].value) * parseFloat(mrp[val].value);
        itemAmounts[val].value = parseFloat(itemAmounts[val].value).toFixed(2);
        return 'pass';
    }




    function calculateMRP(val)
    {
        var mrp = document.getElementsByName("mrps");
        var unitPrice = document.getElementsByName("unitPrice");
        var tax = document.getElementsByName("tax");
        var discount = document.getElementsByName("discount");
        var priceAfterDiscount = 0;

        if (isFloat(discount[val].value)) {
            alert("Please Enter Valid Discount");
            discount[val].focus();
            discount[val].select();
            return 'fail';
        }
        if (isFloat(tax[val].value)) {
            alert("Please Enter Valid Tax");
            tax[val].focus();
            tax[val].select();
            return 'fail';
        }
        if (isFloat(unitPrice[val].value)) {
            alert("Please Enter Valid Unit Price");
            unitPrice[val].focus();
            unitPrice[val].select();
            return 'fail';
        }

        priceAfterDiscount = parseFloat(unitPrice[val].value) - (parseFloat(unitPrice[val].value) * parseFloat(discount[val].value) / 100);
        mrp[val].value = parseFloat(priceAfterDiscount) + (parseFloat(priceAfterDiscount) * parseFloat(tax[val].value) / 100);
        mrp[val].value = parseFloat(mrp[val].value).toFixed(2);
        return 'pass';
    }







    function totalBalance(val)
    {
        var index = document.getElementsByName("selectedIndex");
        var itemAmounts = document.getElementsByName("itemAmounts");
        var tot = 0;
        var roundOff = 0;
        calculateMRP(val);
        calculateAmount(val);

        for (var i = 0; i < index.length; i++) {
            if (itemAmounts[i].value != '') {
                tot = parseFloat(tot) + parseFloat(itemAmounts[i].value);
            }
        }
        if (isFloat(document.modifyGrn.freight.value)) {
            alert("Please Enter Valid Freight Charges");
            document.modifyGrn.freight.select();
            document.modifyGrn.freight.focus();
            return 'fail';
        }

        tot = parseFloat(tot) + parseFloat(document.modifyGrn.freight.value);
        roundOff = parseFloat(tot).toFixed(0) - parseFloat(tot).toFixed(2);
        roundOff = parseFloat(roundOff).toFixed(2);

        if (parseFloat(roundOff) < 0) {
            roundOff = -(roundOff);
        }
        tot = parseFloat(tot).toFixed(0);
        document.getElementById("total").innerHTML = '<font>' + tot + '</font>';
        document.getElementById("roundOff").innerHTML = '<font>' + roundOff + '</font>';
        document.modifyGrn.inVoiceAmount.value = tot;
    }


</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ModifyGRN"  text="ModifyGRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.ModifyGRN"  text="ModifyGRN"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="modifyGrn" method="post" >           
                    <%@ include file="/content/common/message.jsp" %>    
                    <br>
                    <% int index = 0; %>
                    <c:if test = "${supplyList != null}" >   

                        <c:forEach items="${supplyList}" var="supply">  
                            <% if(index==0){ %> 
                            <!--<table class="table table-info mb30 table-hover">-->

                                <tr>
                                    <td width="90%"  align="left" >

                                        <table class="table table-info mb30 table-hover">
                                            <thead>
                                                <tr>
                                                    <th colspan="4" height="80" align="center"  ><spring:message code="stores.label.GRNInfo"  text="default text"/></th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td><spring:message code="stores.label.GRNNo"  text="default text"/></td>
                                            <input type="hidden" name="supplyId" value=<c:out value="${supply.supplyId}"/>>
                                            <input type="hidden" name="poId" value=<c:out value="${supply.poId}"/>>
                                            <td> <c:out value="${supply.supplyId}"/> </td>

                                            <td><spring:message code="stores.label.VendorName"  text="default text"/>
                                            </td>
                                            <td> 
                                                <input type="hidden" name="vendorId" id="vendorId" style="width:260px;height:40px;" class="form-control"  value=<c:out value="${supply.vendorId}"/> > 
                                                <c:out value="${supply.vendorName}"/> </td>
                                </tr>           
                                <tr>
                                    <td><spring:message code="stores.label.DCNo"  text="default text"/></td>
                                    <td>  <input type="text" readonly name="dcNumber" style="width:260px;height:40px;" class="form-control"  value=<c:out value="${supply.dcNumber}"/> >     </td>

                                    <td><spring:message code="stores.label.InvoiceNo"  text="default text"/>
                                    </td>
                                    <td> <input type="text" name="inVoiceNumber" style="width:260px;height:40px;" class="form-control"  value=<c:out value="${supply.inVoiceNumber}"/> >        </td>
                                </tr>           
                                <tr>
                                    <td><spring:message code="stores.label.Freight"  text="default text"/>
                                    </td>
                                    <td> <input type="text" name="freight" style="width:260px;height:40px;" class="form-control" onFocusOut="totalBalance('0');" value=<c:out value="${supply.freight}"/> >        </td>

                                    <td><spring:message code="stores.label.BillAmount"  text="default text"/></td>
                                    <td> <input type="text" name="inVoiceAmount"  style="width:260px;height:40px;" class="form-control" value=<c:out value="${supply.inVoiceAmount}"/>  ></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font><spring:message code="service.label.InvoiceDate"  text="default text"/> </td>
                                    <td><input name="inVoiceDate" readonly  class="form-control pull-right datepicker"  type="text" value=<c:out value="${currentDate}"/> size="20">
                                        <!--                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.modifyGrn.inVoiceDate,'dd-mm-yyyy',this)"/>-->

                                    </td>
                                    <td>&nbsp;</td>
                                    <!--<td>&nbsp;</td>-->

                                </tr>
                                <c:set var="transactionType" value="${supply.inVoiceNumber}" />
                            </table>
                            </td>    
                            <td>
                                <table class="table table-info mb30 table-hover">
                                    <thead>
                                    <tr> 
                                        <td colspan="2" height="30" class="blue" align="center" ><spring:message code="stores.label.Total"  text="default text"/></td>
                                    </tr>
                                    </thead>
                                    <tr> 
                                        <td width="121" height="30"  ><spring:message code="stores.label.Currency"  text="default text"/></td>
                                        <td width="86" height="30" ><div id="total" ><c:out value="${supply.inVoiceAmount}"/></div>  </td>
                                    </tr>
                                    <tr> 
                                        <td width="121" height="30"  ><spring:message code="stores.label.RoundOff"  text="default text"/></td>
                                        <td width="86" height="30" ><div id="roundOff" ></div>  </td>
                                    </tr>                

                                </table>
                            </td>
                            </tr>

                            </table>       
                            <% index++; } %>                
                        </c:forEach> 
                        <br>

                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <%index = 0;
                            String classText = "";
                            int oddEven = 0;
                                %>
                                <c:if test = "${supplyList != null}" >

                                    <tr>
                                        <th><spring:message code="stores.label.S.No"  text="default text"/></th>
                                        <th><spring:message code="stores.label.MfrCode"  text="default text"/></th>
                                        <th><spring:message code="stores.label.TahoosCode"  text="default text"/></th>
                                        <th><spring:message code="stores.label.ItemName"  text="default text"/></th>
                                        <th><spring:message code="stores.label.Uom"  text="default text"/></th>
                                        <th><spring:message code="stores.label.Dis(%)"  text="default text"/></th></th>
            <!--                            <th><spring:message code="stores.label.Tax(%)"  text="default text"/></th>-->
                                        <th><spring:message code="stores.label.UnitPrice"  text="default text"/></th>
            <!--                            <th><spring:message code="stores.label.Price(withTax)"  text="default text"/></th>-->
                                        <th><spring:message code="stores.label.AcceptedQty"  text="default text"/> </th>
                                        <th><spring:message code="stores.label.ReturnedQty"  text="default text"/></th>
                                        <th><spring:message code="stores.label.TotalQty"  text="default text"/></th>
                                        <th><spring:message code="stores.label.Amount"  text="default text"/> </th>
                                        <th><spring:message code="stores.label.UnusedQuantity"  text="default text"/> </th>
                                        <th><spring:message code="stores.label.Select"  text="default text"/> </th>
                                    </tr>
                                </thead>
                                <c:forEach items="${supplyList}" var="supply"> 
                                    <%

                        oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr>
                                    <input type="hidden" name="itemIds" value=<c:out value="${supply.itemId}"/> >
                                    <td> <%=index+1%></td>
                                    <td> <c:out value="${supply.mfrCode}"/></td>
                                    <td> <c:out value="${supply.paplCode}"/></td>                       
                                    <td> <c:out value="${supply.itemName}"/></td>
                                    <td> <c:out value="${supply.uomName}"/></td>
                                    <td> <input type="text"  readonly name="discount" size="5" class="form-control" onFocusOut="calculateMRP('<%= index %>');"  onChange="setAmount('<%= index %>');
                                            calculateTotQty('<%= index %>');" value=<c:out value="${supply.discount}"/> > </td>
                                    <!--                            <td> -->
                                    <input type="hidden"  name="tax" size="5" readonly class="form-control" value=<c:out value="${supply.tax}"/> >
                                    <!--                            </td>-->
                                    <td> 
                                        <input type="text"  readonly name="unitPrice" onFocusOut="calculateMRP('<%= index %>');" onChange="setAmount('<%= index %>');"  size="10" class="form-control" value=<c:out value="${supply.unitPrice}"/> >
                                    </td>
                                    <!--                            <td> -->
                                    <input type="hidden"  readonly  name="mrps" size="10" class="form-control" value=<c:out value="${supply.mrp}"/> >
                                    <!--                            </td>                                                                                                                              -->
                                    <td> <input type="text" readonly  name="acceptedQtys" size="5" onChange="setAmount('<%= index %>')" class="form-control" value=<c:out value="${supply.acceptedQty}"/> >
                                    </td>
                                    <td> <input type="text"  name="retQtys" onFocusOut="calculateTotQty('<%= index %>');"  size="5" class="form-control" value='0' > </td>
                                    <td> <input type="text" readonly   name="totalQtys" size="5" class="form-control" value='<c:out value="${supply.acceptedQty}"/>' > </td>
                                    <td> <input type="text"  readonly  name="itemAmounts" size="5" class="form-control" value=<c:out value="${supply.itemAmount}"/> > </td>                                                                                      
                                    <td> <input type="text" readonly name="returnedQtys" size="5" class="form-control" value=<c:out value="${supply.returnedQty}"/> > </td>
                                    <td> <input type="checkbox" name="selectedIndex"  value='<%= index %>' > </td> 
                                        <c:set var="inventoryStat" value="${supply.inventoryStatus}" />

                                    </tr>
                                    <%
                        index++;
                                    %>
                                </c:forEach>
                            </c:if>                  
                        </table> 
                        <input type="hidden" name="status" value="" >             

                        <center>   
                            <div id="buttonStyle" style="visibility:visible;" align="center" >
                                <input type="button" class="btn btn-success" name="approve" value="<spring:message code="stores.label.ModifyGRN"  text="default text"/>" onClick="submitPage1(this.name);" > &nbsp;
                            </div>
                            <!--<c:if test="${transactionType == ''}" >
                                <input type="button" class="button" name="approve" value="Modify GRN" onClick="submitPage1(this.name);" > &nbsp;
                            </c:if>-->

                            &nbsp;&nbsp;&nbsp;
                            <c:if test="${inventoryStat == 'N'}" >
                                <input type="button" class="btn btn-success" name="return" value="<spring:message code="stores.label.UnusedQuantity"  text="default text"/>" onClick="submitPage(this.name);" > &nbsp;            
                            </c:if>
                        </center>
                    </c:if>                  
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
