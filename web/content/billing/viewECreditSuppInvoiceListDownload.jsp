<%--
    Document   : EinvoiceDetailsExcel
    Created on : Dec 10, 2013, 12:51:26 PM
    Author     : Hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="einvoiceDetailsExcel" method="post">
        <%
                        Date dNow = new Date();
//                        String gstNo= request.getParameter("supplierGstNo");
                        String gstNo= "06AACCB8054G1ZT";
                        String billNo= request.getParameter("billNo");
                        System.out.println("billNo-" + billNo);
                        SimpleDateFormat ft = new SimpleDateFormat("ddMMyyyyhhmmss");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = gstNo+"_E-INVOICE "+billNo+" Standard_EINV" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
 
        
            <c:if test="${closedEBillList !=null}">
                <table style="width:auto" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="50">
                            <td><h3>Document Date</h3></td>
                            <td><h3>Document Number</h3></td>
                            <td><h3>Document Type Code</h3></td>
                            <td><h3>Supply Type Code</h3></td>
                            <td><h3>Recipient Legal Name</h3></td>
                            <td><h3>Recipient Trade Name</h3></td>
                            <td><h3>Recipient GSTIN</h3></td>
                            <td><h3>Place of Supply</h3></td>
                            <td><h3>Recipient Address 1</h3></td>                            
                            <td><h3>Recipient Place</h3></td>
                            <td><h3>Recipient State Code</h3></td>
                            <td><h3>Recipient PIN Code</h3></td>
                            <td><h3>Sl No</h3></td>                            
                            <td><h3>Item Description</h3></td>
                            <td><h3>Is the item a GOOD (G) or SERVICE (S) *</h3></td>
                            <td><h3>HSN or SAC code</h3></td>
                            <td><h3>Quantity</h3></td>                            
                            <td><h3>Unit of Measurement</h3></td>
                            <td><h3>Item Price </h3></td>
                            <td><h3>Gross Amount</h3></td>
                            <td><h3>Item Discount Amount</h3></td>                            
                            <td><h3>Item Taxable Value</h3></td>
                            <td><h3>GST Rate</h3></td>
                            <td><h3>IGST  Amount</h3></td>
                            <td><h3>CGST Amount</h3></td>                            
                            <td><h3>SGST/UTGST Amount</h3></td>
                            <td><h3>Comp Cess Amount Ad Valorem</h3></td>
                            <td><h3>State Cess Amount Ad Valorem</h3></td>
                            <td><h3>Other Charges(Item Level)</h3></td>                            
                            <td><h3>Item Total Amount</h3></td>
                            <td><h3>Total Taxable Value</h3></td>                            
                            <td><h3>IGST  Amount Total</h3></td>
                            <td><h3>CGST  Amount Total</h3></td>
                            <td><h3>SGST/UTGST Amount Total</h3></td>
                            <td><h3>Comp Cess Amount Total</h3></td>                            
                            <td><h3>State Cess Amount Total</h3></td>
                            <td><h3>Other Charge Invoice</h3></td>                            
                            <td><h3>Round Off Amount</h3></td>
                            <td><h3>Total Invoice Value in INR</h3></td>
                            <td><h3>Is reverse charge applicable?</h3></td>
                            <td><h3>Is Sec 7 IGST Act applicable? (IGST Applicability despite Supplier and Recipient located in same State/UT)</h3></td>                            
                            <td><h3>Preceding Document Number</h3></td>
                            <td><h3>Preceding Document Date</h3></td>                            
                            <td><h3>Supplier Legal Name</h3></td>
                            <td><h3>GSTIN of Supplier</h3></td>
                            <td><h3>Supplier Address 1</h3></td>
                            <td><h3>Supplier Place</h3></td>                            
                            <td><h3>Supplier State Code</h3></td>
                            <td><h3>Supplier PIN Code</h3></td>                            
                            <td><h3>Type of Export</h3></td>
                            <td><h3>Shipping Port Code</h3></td>
                            <td><h3>Shipping Bill Number</h3></td>
                            <td><h3>Shipping Bill Date </h3></td>                            
                            <td><h3>Payee Name</h3></td>
                             <td><h3>Payee Bank Account Number</h3></td>                            
                            <td><h3>Mode of Payment</h3></td>
                            <td><h3>Bank Branch Code</h3></td>                            
                            <td><h3>Payment Terms</h3></td>
                            <td><h3>Payment Instruction</h3></td>
                            <td><h3>Credit Transfer Terms</h3></td>
                            <td><h3>Direct Debit Terms</h3></td>                            
                            <td><h3>Credit Days</h3></td>
                             <td><h3>Ship To Legal Name</h3></td>                            
                            <td><h3>Ship To GSTIN</h3></td>
                            <td><h3>Ship To Address1</h3></td>                            
                            <td><h3>Ship To Place</h3></td>
                            <td><h3>Ship To Pincode</h3></td>
                            <td><h3>Ship To State Code</h3></td>
                            <td><h3>Dispatch From Name</h3></td>                            
                            <td><h3>Dispatch From Address1</h3></td>
                             <td><h3>Dispatch From Place</h3></td>                            
                            <td><h3>Dispatch From State Code</h3></td>
                            <td><h3>Dispatch From Pincode</h3></td>                            
                            <td><h3>Tax Scheme</h3></td>
                            <td><h3>Transporter ID</h3></td>
                            <td><h3>Trans Mode</h3></td>
                            <td><h3>Trans Distance</h3></td>                            
                            <td><h3>Transporter Name</h3></td>
                             <td><h3>Trans Doc No.</h3></td>                            
                            <td><h3>Trans Doc Date</h3></td>
                            <td><h3>Vehicle No.</h3></td>                            
                            <td><h3>Vehicle Type</h3></td>
                            <td><h3>Receipt Advice Reference</h3></td>
                            <td><h3>Receipt Advice Date</h3></td>
                            <td><h3>Tender or Lot Reference</h3></td>                            
                            <td><h3>Contract Reference</h3></td>
                             <td><h3>External Reference</h3></td>                            
                            <td><h3>Project Reference</h3></td>
                            <td><h3>PO Reference Number</h3></td>
                            <td><h3>PO Reference Date</h3></td>
                            <td><h3>Additional Supp Documents URL</h3></td>                            
                            <td><h3>Additional Supp Doc base64</h3></td>
                             <td><h3>Additional Information</h3></td>                            
                            <td><h3>Document Period Start Date/h3></td>
                            <td><h3>Document Period End Date</h3></td>
                            <td><h3>Additional Currency Code</h3></td>
                            <td><h3>Barcode</h3></td>                            
                            <td><h3>Free Quantity</h3></td>
                             <td><h3>Pre-Tax Value</h3></td>                            
                            <td><h3>Comp Cess Rate Ad Valorem</h3></td>
                            <td><h3>Comp Cess AmountNon AdValorem</h3></td>
                            <td><h3>State Cess Rate Ad Valorem</h3></td>
                            <td><h3>State Cess Amount NoN Valorem</h3></td>                            
                            <td><h3>Purchase Order Line Reference</h3></td>
                              <td><h3>Origin Country Code</h3></td>
                            <td><h3>Unique Serial Number</h3></td>                            
                            <td><h3>Batch Number</h3></td>
                              <td><h3>Batch Expiry Date</h3></td>
                            <td><h3>Warranty Date</h3></td>                            
                            <td><h3>Attribute Name</h3></td>
                              <td><h3>Attribute Value</h3></td>
                            <td><h3>Country Code of Export</h3></td>                            
                            <td><h3>Recipient Phone</h3></td>
                              <td><h3>Recipient e-mail ID</h3></td>
                            <td><h3>Recipient Address 2</h3></td>                            
                            <td><h3>Total Invoice Value in FCNR</h3></td>
                              <td><h3>Paid Amount</h3></td>
                            <td><h3>Amount Due</h3></td>                            
                            <td><h3>Discount Amount Invoice Level</h3></td>
                              <td><h3>Trade Name of Supplier</h3></td>
                            <td><h3>Supplier Address 2</h3></td>                            
                            <td><h3>Supplier Phone</h3></td>
                            <td><h3>Supplier e-mail</h3></td>                            
                            <td><h3>Ship To Trade Name</h3></td>
                              <td><h3>Ship To Address2</h3></td>
                            <td><h3>Dispatch From Address2</h3></td>                            
                            <td><h3>Remarks</h3></td>
                              <td><h3>Export Duty Amount</h3></td>
                            <td><h3>Supplier Can Opt Refund</h3></td>                            
                            <td><h3>ECOM GSTIN</h3></td>
                            <td><h3>Other Reference</h3></td>                            
                        </tr>
                    </thead>
                    <tbody>
                        <%int sno=1;
                        int index =0;
                        String classText="";
                        %>
                        <c:forEach items="${closedEBillList}" var="closedBillList">
                            <%int oddEven = index % 2;
                          if (oddEven > 0) {
                                     classText = "text2";
                                 } else {
                                     classText = "text1";
                                 }
                            %>
                            <tr>
                                <td  height="30"><c:out value="${closedBillList.documentDate}"/></td>
                                <td  height="30" align="left"><c:out value="${closedBillList.documentNumber}"/></td>
                                <td  height="30"><c:out value="${closedBillList.invType}"/></td>
                                <td  height="30"><c:out value="${closedBillList.supTyp}"/></td>
                                <td  height="30" width="auto" align="left"><c:out value="${closedBillList.billingLegalName}"/></td>
                                <td  height="30" width="auto" align="left"><c:out value="${closedBillList.billingTradeName}"/></td>
                                <td  height="30" align="left"><c:out value="${closedBillList.billingGstin}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.placeOfSupply}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.billlingAddress}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.billingLocation}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.billingState}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.billingPin}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.snos}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.itemDescription}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.gorS}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.hsnCd}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.qty}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.unit}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.itemPrice}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.grossAmount}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.itemDiscount}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.itemTaxableValue}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.gstPercent}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.igst}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.cgst}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.sgst}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.cessValue}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.statecessValue}"/></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.netamount}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.totalTaxableValue}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.totalIgst}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.totalCgst}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.totalSgst}"/></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.totalNetAmount}"/></td>                               
                                <td  height="30" align="left">Y</td>                               
                                <td  height="30" align="left">N</td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.invoiceNo}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.invoiceDate}"/></td>                               
                               <%-- <td  height="30" align="left"><c:out value="${closedBillList.compLegalName}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.compGstin}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.compAddress}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.compLocation}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.compState}"/></td>                               
                                <td  height="30" align="left"><c:out value="${closedBillList.compPin}"/></td> --%>                              
                                <td  height="30" align="left">INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.</td>                               
                                <td  height="30" align="left">06AACCB8054G1ZT</td>                               
                                <td  height="30" align="left">Panchi Gujran, Ganaur, Sonepat - 131101 Haryana. INDIA</td>                               
                                <td  height="30" align="left">Sonepat</td>                               
                                <td  height="30" align="left">6</td>                               
                                <td  height="30" align="left">131101</td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left">GST</td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                                <td  height="30" align="left"></td>                               
                      </tr>
                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>
            
            </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
