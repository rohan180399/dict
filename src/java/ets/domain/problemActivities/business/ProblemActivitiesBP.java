package ets.domain.problemActivities.business;

 
import ets.domain.racks.business.RackTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.problemActivities.data.ProblemActivitiesDAO;
import ets.domain.section.data.SectionDAO;
import java.util.ArrayList;
import java.util.Iterator;

public class ProblemActivitiesBP {

    private ProblemActivitiesDAO problemActivitiesDAO;

    public ProblemActivitiesDAO getProblemActivitiesDAO() {
        return problemActivitiesDAO;
    }

    public void setProblemActivitiesDAO(ProblemActivitiesDAO problemActivitiesDAO) {
        this.problemActivitiesDAO = problemActivitiesDAO;
    }
    
    //processGetSectionList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetpaList(ProblemActivitiesTO problemActivitiesTO) throws FPRuntimeException, FPBusinessException {
        
        ArrayList paList = new ArrayList();
        paList = problemActivitiesDAO.getpaList(problemActivitiesTO);
         if (paList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return  paList;
    }
    
    
     /**
     * This method used to get Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
     public ArrayList processProblemGetsDetail(int problemId) throws FPRuntimeException, FPBusinessException {
        ArrayList problemDetail = new ArrayList();
        problemDetail = (ArrayList) problemActivitiesDAO.getAlterProblemDetail(problemId);
       
        return problemDetail;
    }
     
       /**
     * This method used to Update vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int  processUpdateProblemDetails(ProblemActivitiesTO problemActivitiesTO,int problemId) throws FPRuntimeException, FPBusinessException {
        int mfrId = 0;
        int status=0;
        int status1=0;
        status = problemActivitiesDAO.doProblemUpdateDetails(problemActivitiesTO, problemId);
        
if(status == 0){
         throw new FPBusinessException("EM-PRB-01");
        } 
        
        return status;
    }
    //processAddProblemDetails
    
       /**
     * This method used to Update vendor Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int  processAddProblemDetails(ProblemActivitiesTO problemActivitiesTO,int userId) throws FPRuntimeException, FPBusinessException {
         
        int status=0;
         
        status = problemActivitiesDAO.doProblemAddDetails(problemActivitiesTO, userId);
        
       if(status == 0){
         throw new FPBusinessException("EM-PRB-02");
        }  
        return status;
    }
    
 }

