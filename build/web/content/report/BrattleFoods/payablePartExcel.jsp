<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripPlanned-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

                 
                       <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead height="30">
                       
                        <tr >
                            <th >Particulars</th>
                            <th >Status</th>
                            <th >Gr status</th>
                            <th >Opening GR of the Day</th>
                            <th >GR issued during the day</th>
                            <th >GR closed</th>
                            <th >Net GR Still Pending </th>
                            <th >Expense booked during the day</th>
                            <th >pending Expense to be booked</th>
                            <th >trip pending from < 7 days</th>
                            <th >Amount pending from < 7 Days</th>
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                   
                        <c:set var="totOpenGR" value="${0}"/>    
                        <c:set var="totGRissued" value="${0}"/>    
                        <c:set var="totGRClosed" value="${0}"/>    
                        <c:set var="totNetGRPending" value="${0}"/>    
                        <c:set var="totExpDuringDay" value="${0}"/>    
                        <c:set var="totPendingExp" value="${0}"/>    
                        <c:set var="totpendingGRless7" value="${0}"/>    
                        <c:set var="totAmtPendingless7" value="${0}"/>    

                            <tr  height="30">
                            <td align="center" class="text1" rowspan="15">Trip End not closed</td>
                            <td align="center" class="text1" rowspan="4">own</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grClosed']}"/></td>
                             <c:set var="netGrPending1" value="${tripMap['own-load-openGR'] + tripMap['own-load-issueGR'] - tripMap['own-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending1}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Loaded DSO</td>
                           <td align="center" class="text1"><c:out value="${tripMap['own-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grClosed']}"/></td>
                             <c:set var="netGrPending2" value="${tripMap['own-dso-openGR'] + tripMap['own-dso-issueGR'] - tripMap['own-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending2}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grClosed']}"/></td>
                             <c:set var="netGrPending3" value="${tripMap['own-empty-openGR'] + tripMap['own-empty-issueGR'] - tripMap['own-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending3}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                            <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grClosed']}"/></td>
                            <c:set var="netGrPending4" value="${tripMap['own-empty-openGR'] + tripMap['own-empty-issueGR'] - tripMap['own-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending4}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                        <c:set var="totOpenGR" value="${tripMap['own-empty-openGR'] + tripMap['own-dso-openGR'] + tripMap['own-load-openGR'] + tripMap['own-btt-openGR']}"/>   
                        <c:set var="totGRissued" value="${tripMap['own-empty-issueGR'] + tripMap['own-dso-issueGR'] + tripMap['own-load-issueGR'] + tripMap['own-btt-issueGR']}"/>   
                        <c:set var="totGRClosed" value="${tripMap['own-empty-grClosed'] + tripMap['own-dso-grClosed'] + tripMap['own-load-grClosed'] + tripMap['own-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending" value="${netGrPending1 + netGrPending2 + netGrPending3+ netGrPending4}"/>       
                        <c:set var="totExpDuringDay" value="${tripMap['own-empty-expenseDuringDay'] + tripMap['own-dso-expenseDuringDay'] + tripMap['own-load-expenseDuringDay'] + tripMap['own-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp" value="${tripMap['own-empty-pendingExpense'] + tripMap['own-dso-pendingExpense'] + tripMap['own-load-pendingExpense'] + tripMap['own-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless7" value="${tripMap['own-empty-pendingSevenDayExpense'] + tripMap['own-dso-pendingSevenDayExpense'] + tripMap['own-load-pendingSevenDayExpense'] + tripMap['own-btt-pendingSevenDayExpense']}"/>      
                                 
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless7}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Leased</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-grClosed']}"/></td>
                             <c:set var="netGrPending5" value="${tripMap['leased-load-openGR'] + tripMap['leased-load-issueGR'] - tripMap['leased-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending5}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                           <td align="center" class="text1"><c:out value="${tripMap['leased-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-grClosed']}"/></td>
                            <c:set var="netGrPending6" value="${tripMap['leased-dso-openGR'] + tripMap['leased-dso-issueGR'] - tripMap['leased-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending6}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-grClosed']}"/></td>
                            <c:set var="netGrPending7" value="${tripMap['leased-empty-openGR'] + tripMap['leased-empty-issueGR'] - tripMap['leased-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-grClosed']}"/></td>
                             <c:set var="netGrPending8" value="${tripMap['leased-btt-openGR'] + tripMap['leased-btt-issueGR'] - tripMap['leased-btt-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending8}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['leased-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                             <c:set var="totOpenGR2" value="${tripMap['leased-empty-openGR'] + tripMap['leased-dso-openGR'] + tripMap['leased-load-openGR'] + tripMap['leased-btt-openGR']}"/>   
                        <c:set var="totGRissued2" value="${tripMap['leased-empty-issueGR'] + tripMap['leased-dso-issueGR'] + tripMap['leased-load-issueGR'] + tripMap['leased-btt-issueGR']}"/>   
                        <c:set var="totGRClosed2" value="${tripMap['leased-empty-grClosed'] + tripMap['leased-dso-grClosed'] + tripMap['leased-load-grClosed'] + tripMap['leased-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending2" value="${netGrPending5 + netGrPending6 + netGrPending7+ netGrPending8}"/>       
                        <c:set var="totExpDuringDay2" value="${tripMap['leased-empty-expenseDuringDay'] + tripMap['leased-dso-expenseDuringDay'] + tripMap['leased-load-expenseDuringDay'] + tripMap['leased-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp2" value="${tripMap['leased-empty-pendingExpense'] + tripMap['leased-dso-pendingExpense'] + tripMap['leased-load-pendingExpense'] + tripMap['leased-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless72" value="${tripMap['leased-empty-grlessthn7'] + tripMap['leased-dso-grlessthn7'] + tripMap['leased-load-grlessthn7'] + tripMap['leased-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless72" value="${tripMap['leased-empty-pendingSevenDayExpense'] + tripMap['leased-dso-pendingSevenDayExpense'] + tripMap['leased-load-pendingSevenDayExpense'] + tripMap['leased-btt-pendingSevenDayExpense']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed2}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending2}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay2}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp2}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless72}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless72}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Hire</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-grClosed']}"/></td>
                            <c:set var="netGrPending9" value="${tripMap['hire-load-openGR'] + tripMap['hire-load-issueGR'] - tripMap['hire-load-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending9}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-load-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-grClosed']}"/></td>
                             <c:set var="netGrPending10" value="${tripMap['hire-dso-openGR'] + tripMap['hire-dso-issueGR'] - tripMap['hire-dso-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending10}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-dso-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                             <td align="center" class="text1"><c:out value="${tripMap['hire-empty-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-grClosed']}"/></td>
                            <c:set var="netGrPending11" value="${tripMap['hire-empty-openGR'] + tripMap['hire-empty-issueGR'] - tripMap['hire-empty-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending11}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-empty-pendingSevenDayExpense']}"/></td>
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-grClosed']}"/></td>
                            <c:set var="netGrPending12" value="${tripMap['hire-btt-openGR'] + tripMap['hire-btt-issueGR'] - tripMap['hire-btt-grClosed']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending12}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-expenseDuringDay']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-pendingExpense']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-grlessthn7']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['hire-btt-pendingSevenDayExpense']}"/></td>
                            </tr>
                        <c:set var="totOpenGR3" value="${tripMap['hire-empty-openGR'] + tripMap['hire-dso-openGR'] + tripMap['hire-load-openGR'] + tripMap['hire-btt-openGR']}"/>   
                        <c:set var="totGRissued3" value="${tripMap['hire-empty-issueGR'] + tripMap['hire-dso-issueGR'] + tripMap['hire-load-issueGR'] + tripMap['hire-btt-issueGR']}"/>   
                        <c:set var="totGRClosed3" value="${tripMap['hire-empty-grClosed'] + tripMap['hire-dso-grClosed'] + tripMap['hire-load-grClosed'] + tripMap['hire-btt-grClosed']}"/>       
                        <c:set var="totNetGRPending3" value="${netGrPending9 + netGrPending10 + netGrPending11+ netGrPending12}"/>       
                        <c:set var="totExpDuringDay3" value="${tripMap['hire-empty-expenseDuringDay'] + tripMap['hire-dso-expenseDuringDay'] + tripMap['hire-load-expenseDuringDay'] + tripMap['hire-btt-expenseDuringDay']}"/>       
                        <c:set var="totPendingExp3" value="${tripMap['hire-empty-pendingExpense'] + tripMap['hire-dso-pendingExpense'] + tripMap['hire-load-pendingExpense'] + tripMap['hire-btt-pendingExpense']}"/>      
                        <c:set var="totpendingGRless73" value="${tripMap['hire-empty-grlessthn7'] + tripMap['hire-dso-grlessthn7'] + tripMap['hire-load-grlessthn7'] + tripMap['hire-btt-grlessthn7']}"/>      
                        <c:set var="totAmtPendingless73" value="${tripMap['hire-empty-pendingSevenDayExpense'] + tripMap['hire-dso-pendingSevenDayExpense'] + tripMap['hire-load-pendingSevenDayExpense'] + tripMap['hire-btt-pendingSevenDayExpense']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR3}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued3}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRClosed3}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending3}"/></td>
                            <td align="center" class="text1"><c:out value="${totExpDuringDay3}"/></td>
                            <td align="center" class="text1"><c:out value="${totPendingExp3}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless73}"/></td>
                            <td align="center" class="text1"><c:out value="${totAmtPendingless73}"/></td>
                            </tr>
                            <c:set var="grandTotOpenGr" value="${totOpenGR + totOpenGR1 + totOpenGR2+ totOpenGR3}"/>       
                            <c:set var="grandTotGRissued" value="${totGRissued + totGRissued1 + totGRissued2+ totGRissued3}"/>       
                            <c:set var="grandTotGRClosed" value="${totGRClosed + totGRClosed1 + totGRClosed2+ totGRClosed3}"/>       
                            <c:set var="grandtotNetGRPending" value="${totNetGRPending + totNetGRPending1 + totNetGRPending2+ totNetGRPending3}"/>       
                            <c:set var="grandtotExpDuringDay" value="${totExpDuringDay + totExpDuringDay1 + totExpDuringDay2+ totExpDuringDay3}"/>       
                            <c:set var="grandtotPendingExp" value="${totPendingExp + totPendingExp1 + totPendingExp2+ totPendingExp3}"/>       
                            <c:set var="grandtottotpendingGRless7" value="${totpendingGRless7 + totpendingGRless71 + totpendingGRless72+ totpendingGRless73}"/>       
                            <c:set var="grandtottotAmtPendingless7" value="${totAmtPendingless7 + totAmtPendingless71 + totAmtPendingless72+ totAmtPendingless73}"/>       
                             <tr  height="30">
                            <td align="center" class="text1" >Grand Total</td>
                            <td align="center" class="text1" ></td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${grandTotOpenGr}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRClosed}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotNetGRPending}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotExpDuringDay}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotPendingExp}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotpendingGRless7}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotAmtPendingless7}"/></td>
                            </tr>
                       
                        <%
                            index++;
                            sno++;
                        %>
                   
                </table>
               

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>