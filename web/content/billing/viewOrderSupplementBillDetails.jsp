<%@page import="ets.domain.billing.business.BillingTO"%>
<%@page import="java.util.ArrayList"%>

<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript">

    function checkInvoiceNo() {
        var invoiceNo = document.trip.invoiceNo.value;
        if (invoiceNo == '' || invoiceNo == '0') {
            alert('please enter invoice No');
            document.trip.invoiceNo.focus();
            return false;
        } else {
            return true;
        }
    }
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    function onKeyPressBlockCharacters(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z]+$/;

        return !reg.test(keychar);

    }

</script>
<script  type="text/javascript" src="js/jq-ac-script.js">

</script>


<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });

    function submitPage(consignmentOrderId) {
//        $("#Save").hide();
//        var tripSheetIds = [];
//        var tripIds = document.getElementsByName('tripId');
//        for (var i = 1; i <= tripIds.length; i++) {
//            tripSheetIds.push(document.getElementById("tripId" + i).value);
//        }
//        var existStatus = "";
//        $.ajax({
//            url: "/throttle/getTripExistInBilling.do",
//            data: {
//                tripIdTemp: tripSheetIds
//            },
//            type: "POST",
//            dataType: 'json',
//            async: false,
//            success: function (data) {
//                if (data != '') {
//                    $.each(data, function (i, data) {
//                        existStatus = data.Id;
//
//
//                    });
//                } else {
//                    existStatus = "";
//                }
//            }
//        });
//        existStatus = 0;
//        if (existStatus == 0) {
//            document.trip.action = '/throttle/saveOrderSupplementBill.do?consignmentOrderId=' + consignmentOrderId;
//            document.trip.submit();
//        } else {
//            alert("Bill has generated for the Trip already");
//        }

 var finYear = document.getElementById("finYear").value;
        
        if(finYear==""){
            alert("Please selct the financal Year.");
            document.getElementById("finYear").focus(); 
            return false;
        }else{
             $("#Save").hide();
            document.trip.action = '/throttle/saveOrderSupplementBill.do?consignmentOrderId=' + consignmentOrderId;
            document.trip.submit();
        }

    }

    function editBillingParty() {
        document.getElementById("billingParty").readOnly = false;

        alert("Now change the Billing Party:");
//               doucment.getElementById("save").style.display='none';

        $("#save").show();

    }
    // Use the .autocomplete() method to compile the list based on input from user
    $(document).ready(function () {
        $('#billingParty').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerName.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term,
                        customerName: document.getElementById('billingParty').value
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#billingParty").val(ui.item.customerName);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.customerName;
                var tmp = value.split('-');
                $('#billingPartyId').val(tmp[0]);
                $('#customerId').val(tmp[0]);
                $('#billingParty').val(tmp[1]);
                $('#GSTNo').val(tmp[2]);
                $('#GSTSpan').text(tmp[2]);
                $('#panNo').val(tmp[3]);
                $('#panSpan').text(tmp[3]);
                $('#billingPartyAddress').val(tmp[4]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.customerName;

            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };


    });
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryBilling</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryBilling</a></li>
            <li class="active"> Billing Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="sumOfCharges('1');
                    showHide();
                  ">

                <form name="trip" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <%int sno = 1;%>
                    <div id="tabs" >
                        <ul>
                            <li><a href="#tripDetail"><span>Billing Details</span></a></li>
                        </ul>
                        <c:set var="totalRevenue" value="0" />
                        <c:set var="totalExpToBeBilled" value="0" />

                      <table>                       
                                  <tr>                       
                                                 <td class="text1" height="30" ><font color='red'>*</font>Commodity Category</td>
                                                   <input type="hidden" id="invoiceId" name="invoiceId" style="width:180px;height:40px;" value="<c:out value="${invoiceId}" />"/></td>
                                                   <input type="hidden" id="remarks11" name="remarks11" style="width:180px;height:40px;" value=""/></td>
                                                   <input type="hidden" id="commodityIds" name="commodityIds" style="width:180px;height:40px;" /></td>
                                                   <input type="hidden" id="commodityNames" name="commodityNames" style="width:180px;height:40px;" /></td>
                                                 <input type="hidden" id="commodityGstType" name="commodityGstType" style="width:180px;height:40px;" /></td>
                                                    <td><select id="commodityCategory" name="commodityCategory" style="width:180px;height:40px;" onChange="setCommaodityName(this.value);"/>
                                                            <option value="0">---select-----</option>
                                                            <c:forEach items="${comodityDetails}" var="com">
                                                                <option value='<c:out value="${com.commodityId}" />~<c:out value="${com.commodityName}" />~<c:out value="${com.gstType}" />'><c:out value="${com.commodityName}" /></option>
                                                            </c:forEach>
                                                </select></td>
                                           
                                              <script>
//                                                      document.getElementById("commodityId").value='<c:out value="${commodityId}" />~<c:out value="${articleName}" />~<c:out value="${gstType}" />'
                                                      
//                                                      $("#commodityGstType").val('<c:out value="${gstType}" />');
//                                                      $("#commodityName").val('<c:out value="${articleName}" />');
//                                                      $("#commodityCategory").val('<c:out value="${commodityId}" />');
                                                      
                                                      function setCommaodityName(commodity){
                                                          var temp =commodity.split('~');
                                                          $("#commodityIds").val(temp[0]);
                                                          $("#commodityNames").val(temp[1]);
                                                          $("#commodityGstType").val(temp[2]);
                                                      }
                                                 </script>
                                                 
                                </tr>
                            </table>
                            <br>
                            <br>
                            
                            
                            <table  border="0" class="tableborder table-bordered mb30 table-hover" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr id="tableDesingTD" height="30">
                                    <td  >Sno</td>
                                    <td  >GR No</td>
                                    <td  >container No</td>
                                    <c:if test="${movementType == 1}">
                                        <td  >S.B No</td>
                                    </c:if>
                                    <c:if test="${movementType != 1}">
                                        <td  >Bill of entry</td>
                                    </c:if>
                                    <td  >Commodity Name</td>
                                    <td  >GST Applicable</td>
                                    <td  >Trip Code</td>
                                    <td  >C Note</td>
                                    <td  >Customer</td>
                                    <td  >Start</td>
                                    <td  >End</td>
                                    <td  >Estimated Revenue</td>
                                    <td  >Detention Charge</td>
                                    <td  >Toll Tax</td>
                                    <td  >Green Tax</td>
                                    <td  >Other Expense</td>
                                    <td  >Weightment Charge</td>
                                    <td  >Weightment Expense Type</td>
                                </tr>
                                <c:set var="consignmentOrderId" value="0"></c:set>
                                <c:set var="consignmentOrderId" value="0"></c:set>
                                <c:if test = "${tripsToBeBilledDetails != null}" >

                                    <c:forEach items="${tripsToBeBilledDetails}" var="trip">
                                        <c:set var="totalRevenue" value="${totalRevenue + trip.estimatedRevenue}" />
                                        <c:set var="totalExpToBeBilled" value="${totalExpToBeBilled + trip.expenseToBeBilledToCustomer}" />
                                        <tr>
                                            <td class="text1" ><%=sno%></td>
                                            <td class="text1"><input type="hidden"id="grNo<%=sno%>" name="grNo" value="<c:out value="${trip.grNo}"/>" readonly/>
                                                <label> <c:out value="${trip.grNo}"/></label>
                                                <input type="hidden"id="consignmentOrderIds<%=sno%>" name="consignmentOrderIds" value="<c:out value="${trip.consignmentOrderId}"/>"/> </td>
                                            <td class="text1" ><input type="hidden" id="containerNoOld<%=sno%>" name="containerNoOld" value="<c:out value="${trip.containerNo}"/>"/>
                                                <input type="text" id="containerNo<%=sno%>" name="containerNo" value="<c:out value="${trip.containerNo}"/>" style="width:90px;height:20px;"/>
                                                <input type="hidden" id="tripContainerId<%=sno%>" name="tripContainerId" value="<c:out value="${trip.tripContainerId}"/>"/>
                                                <input type="hidden" id="consignmentConatinerId<%=sno%>" name="consignmentConatinerId" value="<c:out value="${trip.consignmentConatinerId}"/>"/>
                                            </td>

                                            <c:if test="${movementType == 1}">
                                                <td class="text1" ><input type="hidden" name="shippingBillNoOld" id="shippingBillNoOld<%=sno%>" value="<c:out value="${trip.shippingBillNo}"/>" />
                                                    <input type="text" name="shippingBillNo" id="shippingBillNo<%=sno%>" value="<c:out value="${trip.shippingBillNo}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
                                                </c:if>
                                                <c:if test="${movementType != 1}">

                                                <td class="text1" ><input type="hidden" name="billOfEntryOld" id="billOfEntryOld<%=sno%>" value="<c:out value="${trip.billOfEntryNo}"/>" />
                                                    <input type="text" name="billOfEntry" id="billOfEntry<%=sno%>" value="<c:out value="${trip.billOfEntryNo}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>

                                            </c:if>
                                            <td class="text1" ><input type="hidden" name="gstType" id="gstType<%=sno%>" value="<c:out value="${trip.gstType}"/>" />
                                                <input type="hidden" name="commodityName" id="commodityName<%=sno%>" value="<c:out value="${commodityName}"/>" />
                                                <input type="hidden" name="commodityId" id="commodityId<%=sno%>" value="<c:out value="${commodityId}"/>" />
                                                <c:out value="${trip.articleName}"/></td>
                                            <td class="text1" ><c:out value="${trip.gstType}"/></td>
                                            <td class="text1" ><c:out value="${trip.tripCode}"/></td>
                                            <td class="text1" ><c:out value="${trip.consignmentNoteNo}"/></td>
                                            <td class="text1" ><c:out value="${trip.customerName}"/></td>
                                            <td class="text1" ><c:out value="${trip.startDate}"/> / <c:out value="${trip.startTime}"/></td>
                                            <td class="text1"  style="color: red;"  ><b><c:out value="${trip.endDate}"/> / <c:out value="${trip.endTime}"/></b></td>
                                            <!--<td class="text1" ><c:out value="${trip.totalKM}"/> / <c:out value="${trip.totalHrs}"/></td>-->
                                            <td class="text1" ><label> <c:out value="${trip.estimatedRevenue}"/></label>  </td>
                                            <td class="text1"  align="right"><input type="hidden" id="supplementCharge<%=sno%>" name="supplementCharge" value="0" onchange="totalSuppInvAmount();" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/><input type="hidden" id="detaintionChargeOld<%=sno%>" name="detaintionChargeOld" value="<c:out value="${trip.detaintionCharge}"/>"/>
                                                <input type="text" id="detaintionCharge<%=sno%>" name="detaintionCharge" value="<c:out value="${trip.detaintionCharge}"/>" onchange="sumOfCharges('0')" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
                                            <td class="text1"  align="right"><input type="hidden" id="tollChargeOld<%=sno%>" name="tollChargeOld" value="<c:out value="${trip.tollCharge}"/>" />
                                                <input type="text" id="tollCharge<%=sno%>" name="tollCharge" value="<c:out value="${trip.tollCharge}"/>" onchange="sumOfCharges('0')" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
                                            <td class="text1"  align="right"><input type="hidden" id="greenTaxOld<%=sno%>" name="greenTaxOld" value="<c:out value="${trip.greenTax}"/>"/>
                                                <input type="text" id="greenTax<%=sno%>" name="greenTax" value="<c:out value="${trip.greenTax}"/>" onchange="sumOfCharges('0')" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
                                            <td class="text1"><input type="hidden" id="otherExpenseOld<%=sno%>" name="otherExpenseOld" value="<c:out value="${trip.otherExpense}"/>"/>
                                                <input type="text" id="otherExpense<%=sno%>" name="otherExpense" value="<c:out value="${trip.otherExpense}"/>" onchange="sumOfCharges('0')" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
                                            <td class="text1"><input type="hidden" id="weightmentChargeOld<%=sno%>" name="weightmentChargeOld" value="<c:out value="${trip.weightmentCharge}"/>"/>
                                                <input type="text" id="weightmentCharge<%=sno%>" name="weightmentCharge" value="<c:out value="${trip.weightmentCharge}"/>" onchange="sumOfCharges('0');" onKeyPress="return onKeyPressBlockCharacters(event);"  style="width:90px;height:20px;"/></td>
           <!--                                    <td class="text1"  align="left"><c:out value="${trip.totalExpenses}"/></td>
                                               <td class="text1"  align="left"><c:out value="${trip.expenseToBeBilledToCustomer}"/></td>-->
                                            <td class="text1"><select id="expenseType<%=sno%>" name="expenseType" onchange="sumOfCharges('0');"  style="width:90px;height:20px;">
                                                    <option value="0">--select--</option>
                                                    <option value="1">Billed to Customer</option>
                                                    <option value="2">Do not Billed to Customer</option>
                                                </select>
                                            </td>
                                        <script>
                                            document.getElementById("expenseType" +<%=sno%>).value = '<c:out value="${trip.weightmentExpenseType}"/>';
                                            function checkExpenseType() {
                                                if (document.getElementById("weightmentCharge" +<%=sno%>).value != '0.00') {
                                                    alert("please select expense type")
                                                    document.getElementById("expenseType" +<%=sno%>).value;
                                                }
                                            }
                                        </script>
                                        <input type="hidden" name="gstApplicable" id="gstApplicable<%=sno%>" value='<c:out value="${trip.gstType}"/>' />
                                        <input type="hidden" name="count" id="count" value='<%=sno%>' />
                                        <input type="hidden" name="tripId" id="tripId<%=sno%>"value='<c:out value="${trip.tripId}"/>' />
                                        <input type="hidden" name="vehicleId" id="vehicleId<%=sno%>"value='<c:out value="${trip.vehicleId}"/>' />
                                        <input type="hidden" name="driverId" id="driverId<%=sno%>"value='<c:out value="${trip.empId}"/>' />
                                        <input type="hidden" name="totalDays" value='<c:out value="${trip.totalDays}"/>' />
                                        <input type="hidden" name="startDate" value='<c:out value="${trip.startDate}"/>' />
                                        <input type="hidden" name="startTime" value='<c:out value="${trip.tripStartTime}"/>' />
                                        <input type="hidden" name="tripTransitHours" value='<c:out value="${trip.tripTransitHours}"/>' />
                                        <input type="hidden" name="reeferRequired" value='<c:out value="${trip.tempReeferRequired}"/>' />
                                        <input type="hidden" name="estimatedRevenue" id="estimatedRevenue<%=sno%>" value='<c:out value="${trip.estimatedRevenue}"/>' />
                                        <input type="hidden" name="articleName" id="articleName<%=sno%>" value='<c:out value="${trip.articleName}"/>' />
                                        <input type="hidden" name="tripSGSTAmount" id="tripSGSTAmount<%=sno%>" value='0' />
                                        <input type="hidden" name="tripCGSTAmount" id="tripCGSTAmount<%=sno%>" value='0' />
                                        <input type="hidden" name="tripIGSTAmount" id="tripIGSTAmount<%=sno%>" value='0' />
                                        <c:set var="tripTransitHours" value="${trip.tripTransitHours}"></c:set>
                                        <c:set var="reeferRequired" value="${trip.tempReeferRequired}"></c:set>
                                            </tr>
                                        <%sno++;%>
                                    </c:forEach>
                                </c:if>

                            </table>
                            <br>

                            <% int cntr = 1;%>
                            <br/>
                            Other Expenses to be Billed:
                            <br>

                            <table  border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr id="tableDesingTD" height="30">
                                    <td  >Sno</td>
                                    <td  >Trip Code</td>
                                    <td  >Expense Description</td>
                                    <td  >Remarks</td>
                                    <td  >Pass Through</td>
                                    <td  >Margin</td>
                                    <td  >Tax (%)</td>
                                    <td  >Expense Value</td>
                                </tr>

                                <c:if test = "${tripsOtherExpenseDetails != null}" >

                                    <c:forEach items="${tripsOtherExpenseDetails}" var="trip">
                                        <tr>
                                            <td class="text1" ><%=cntr%></td>
                                            <td class="text1" ><c:out value="${trip.tripCode}"/></td>
                                            <td class="text1" ><c:out value="${trip.expenseName}"/></td>
                                            <td class="text1" ><c:out value="${trip.expenseRemarks}"/></td>
                                            <td> <input type="hidden" name="testRemark" id="testRemark<%=cntr%>" value="<c:out value="${trip.expenseRemarks}"/>"/></td>
                                      <!--roha-->
                                        <script>
                                          var zzz=document.getElementById("testRemark"+1).value;
                                            document.getElementById("remarks11").value=zzz;
                                    
                                        </script>
                                            <td class="text1" ><c:out value="${trip.passThroughStatus}"/></td>
                                            <td class="text1"  align="left"><c:out value="${trip.marginValue}"/></td>
                                            <td class="text1"  align="left"><c:out value="${trip.taxPercentage}"/></td>
                                            <td class="text1"  align="left  "><c:out value="${trip.expenseValue}"/></td>

                                            <%cntr++;%>
                                        </c:forEach>
                                    </c:if>
                                    <% if (cntr == 1) {%>
                                <tr>
                                    <td colspan="8" >No expenses to be billed</td>
                                </tr>
                                <% }%>
                            </table>


                            <br/>
                            <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${consignmentOrderIds}"/>"/>
                            <input type="hidden" name="billingState" id="billingState" value="<c:out value="${billingState}"/>"/>
                            <input type="hidden" name="organizationId" id="organizationId" value="<c:out value="${organizationId}"/>"/>
                            <input type="hidden" name="CGST" id="CGST" value="<c:out value="${CGST}"/>"/>
                            <input type="hidden" name="SGST" id="SGST" value="<c:out value="${SGST}"/>"/>
                            <input type="hidden" name="IGST" id="IGST" value="<c:out value="${IGST}"/>"/>
                            <input type="hidden" name="GSTNo" id="GSTNo" value="<c:out value="${GSTNo}"/>"/>
                            <input type="hidden" name="panNo" id="panNo" value="<c:out value="${panNo}"/>"/>
                            <input type="hidden" name="commodity" id="commodity" value="0"/>
                            <input type="hidden" name="companyType" id="companyType" value="<c:out value="${companyType}"/>"/>

                            <table border="1" width="600">
                                <tr height="22">
                                    <td colspan="2"  style="color:black;text-align:left">Summary:</td>
                                </tr>
                                <!--                        <tr id="shippingLine">
                                                            <td align="left">Shipping Bill No</td>
                                                            <td align="right"><input type="text" value="<c:out value="${shipingBillNo}"/>" name ="shippingBillNo" id="shippingBillNo"/></td>
                                                        </tr>-->
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">PAN No</td>
                                    <td align="right"><font color='green'><c:out value="${panNo}"/></font></td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">GST No</td>
                                    <td align="right"><font color='blue'><c:out value="${GSTNo}"/> </font></td>
                                </tr>

                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Billing Party</td>
                                    <td class="text1" align="right">
                                        <input type="hidden" id="billingPartyId" name="billingPartyId" value="<c:out value="${billingPartyId}"/>"/>
                                        <input type="hidden" id="billingPartyOld"  name="billingPartyOld" value="<c:out value="${billingParty}"/>"/>
                                        <input type="text" id="billingParty"  name="billingParty" value="<c:out value="${billingParty}"/>"  style="width:auto;height:22px;width:150px;"/>
                                        <input type="hidden" id="billingPartyAddress" readonly name="billingPartyAddress" value="<c:out value="${billingPartyAddress}"/>"  style="width:auto;height:22px;width:150px;"/><c:out value="${billingParty}"/>
                                    <a href="javascript:editBillingParty();" style="color: #0090FF"></a>
                                    </td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Total Orders</td>
                                    <td align="right"><c:out value="${consignmentOrderCount}"/></td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Total Trips</td>
                                    <% sno = sno - 1;%>
                                    <td align="right"><%=sno%></td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Total Freights to be billed</td>
                                    <td align="right"><c:out value="${totalRevenue}"/></td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Total Expenses to be billed</td>
                                    <!--<td align="right"><c:out value="${totalExpToBeBilled}"/></td>-->
                                    <td align="right">
                                        <input type="hidden" id="totalExpToBeBilled" value="<c:out value="${totalExpToBeBilled}"/>">

                                        <span id="totalExpToBeBilledSpan"><c:out value="${totalExpToBeBilled}"/></span>
                                    </td>
                                </tr>
                                <%   
                                    String june="30-06-2017";
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                     Date june_17=sdf.parse(june);
                                     Date grDate=sdf.parse((String)request.getAttribute("grDate")) ;
                                if( !"1".equalsIgnoreCase((String)request.getAttribute("companyType")) && "1".equalsIgnoreCase((String)request.getAttribute("organizationId"))  && (Boolean)request.getAttribute("articleFlag") &&
                                        "".equalsIgnoreCase((String)request.getAttribute("GSTNo")) &&
                                        "9".equalsIgnoreCase((String)request.getAttribute("billingState") ) 
                                        && grDate.after(june_17) )
                                {
System.out.println(" am in if...!!!!:");   
                                %> 
                                <input type="hidden" name="text" id="checkTax" value="1"/>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">CGST (<c:out value="${CGST}"/> %)</td>
                                    <td align="right">
                                        <input type="hidden" id="cgstAmount" name="cgstAmount" value="0">
                                        <span id="cgstAmountSpan"></span>
                                    </td>
                                </tr>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">SGST (<c:out value="${SGST}"/> %)</td>
                                    <td align="right">
                                        <input type="hidden" id="sgstAmount" name="sgstAmount" value="0">
                                        <span id="sgstAmountSpan"></span>
                                    </td>
                                </tr>
                                <%} else if(!"1".equalsIgnoreCase((String)request.getAttribute("companyType")) && "1".equalsIgnoreCase((String)request.getAttribute("organizationId")) &&  (Boolean)request.getAttribute("articleFlag") 
                                    && "".equalsIgnoreCase((String)request.getAttribute("GSTNo")) 
                                    && !"9".equalsIgnoreCase((String)request.getAttribute("billingState") ) && grDate.after(june_17) )
                                { System.out.println(" am in else if...!!!!:");%>
                                <input type="hidden" name="checkTax" id="checkTax" value="2"/>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">IGST (<c:out value="${IGST}"/> %)</td>
                                    <td align="right">
                                        <input type="hidden" id="igstAmount" name="igstAmount" value="0">
                                        <span id="igstAmountSpan"></span>
                                    </td>
                                </tr>
                                <% }else{ %>
                                <input type="hidden" name="checkTax" id="checkTax" value="0"/>
                                <%}%>
                                <tr height="22" style="font-size:16px;">
                                    <td align="left">Nett Amount to be billed</td>
                                    <c:set var="netAmount" value="${totalRevenue}"></c:set>
                                        <td align="right">
                                            <input type="hidden" id="netAmount" name="netAmount" value="<c:out value="${netAmount}"/>">
                                        <input type="hidden" id="netTotalAmount" value="<c:out value="${netAmount}"/>">

                                        <span id="netAmountSpan"><c:out value="${netAmount}"/></span>
                                    </td>
                                </tr>
                            </table>

                            <input type="hidden" name="totalTax" id="totalTax" value='0' />
                            <input type="hidden" name="movementType" id="movementType" value='<c:out value="${movementType}"/>' />
                            <input type="hidden" name="billList" id="billList" value='<c:out value="${billList}"/>' />
                            <input type="hidden" name="noOfOrders" value='<c:out value="${consignmentOrderCount}"/>' />
                            <input type="hidden" name="noOfTrips" value='<%=sno%>' />
                            <input type="hidden" name="totalRevenue" value='<c:out value="${totalRevenue}"/>' />
                            <input type="hidden" name="totalExpToBeBilled" value='<c:out value="${totalExpToBeBilled}"/>' />
                            <input type="hidden" name="totOtherExpense" id="totOtherExpense" value='' />
                            <input type="hidden" name="grandTotal" value='<c:out value="${totalRevenue - totalExpToBeBilled}"/>' />

                            <input type="hidden" name="generateBill" value='1' />

                            <br/>
                            <br/>
                            <div class="text2" style="color:black;">Remarks for Detention</div>
                            <br>
                                 <input type="text" id="detaintionRemarks" name="detaintionRemarks" style="width:600px;height:80px;" value="jhghjbhjbhjbhjbhj"/></td>
                                                
                            <br>
                            <br/>
                            <!--<center>-->

                            <div id="update" style="visibility:visible">
                                <input type="button"  name="submitValue" class="btn btn-success"  value="Save Changes" onclick="submitPageForDetaiontion();" id="submitValue1" style="width:110px;"/>
                            </div>
                            <!--</center>-->
                            <br>
                            <div id="revenueChange">
                                <c:if test = "${revenueApprovalStatus == null}" >
                                    <div id="odoApprovalRequest" style="display:block;">

                                        <div class="text2" style="color:black;">Revenue Request Description</div>
                                        <br>
                                        <textarea name="odoUsageRemarks" style="width:600px;height:80px;"></textarea>
                                        <br>
                                        <br>
                                        <input type="button"  class="btn btn-info" onclick="saveOdoApprovalRequest();" value="Request approval" id="buttonDesign" style="width:130px;"/>
                                    </div>
                                    <div id="odoPendingApproval" style="display:none;" class="text1" >
                                        <br>
                                        Request has been submitted.
                                        <br>
                                        <br>


                                    </c:if>
                                    <c:if test = "${revenueApprovalStatus == 0}" >
                                        <br>
                                        <br>
                                        <div class="text2" ><strong>Request Status:</strong><font color="red"> <b>request pending for approval</b></font></div>
                                        <br>
                                    </c:if>
                                    <c:if test = "${revenueApprovalStatus == 1}" >
                                        <br>
                                        <br>
                                        <div class="text2" ><strong>Request Status:</strong> <font color="green"> <b>Your request  approved</b></font></div>
                                        <br>
                                    </c:if>
                                </div>
                            </div>
                            <c:if test = "${reeferRequired == 'Yes'}" >
                                <table width="500" cellpadding="0" cellspacing="0" align="left" border="0" id="report" style="margin-top:0px;">
                                    <tr>
                                        <td width="80" class="text1" >Total Transit Hours: </td>
                                        <td width="160"   class="text1" ><c:out value="${tripTransitHours}"/>
                                        <td width="80" class="text1" >Total Trip Days: </td>
                                        <td width="160"   class="text1" ><c:out value="${totalDays}"/>
                                        </td>
                                        <td width="80" class="text1" >Hours </td>
                                        <td width="80" class="text1" > <select name="reeferHour" id="reeferHour" class="textbox" style="height:20px; width:122px;" >
                                                <option value="4"  selected>4</option>
                                                <option value="6" >6</option>
                                                <option value="7" >7</option>
                                                <option value="8" >8</option>
                                                <option value="9" >9</option>
                                                <option value="10" >10</option>
                                                <option value="11" >11</option>
                                                <option value="12" >12</option>
                                                <option value="13" >13</option>
                                                <option value="14" >14</option>
                                                <option value="15" >15</option>
                                                <option value="16" >16</option>
                                                <option value="17" >17</option>
                                                <option value="18" >18</option>
                                                <option value="19" >19</option>
                                                <option value="20" >20</option>
                                            </select></td>


                                    </tr></table>


                                <br/>
                                <br/>
                            </c:if>

                              <br>
                             <br>
                             <div><center>
                                 Financial Year : 
                                 <select name="finYear" id="finYear" class="textbox">
                                     <!--<option value="">-Select Any One -</option>-->
                                     <!--<option value="2022">2021-22</option>-->
                                     <!--<option value="2023">2022-23</option>-->
                                     <option value="2024" selected>2023-24</option>
                                     <!--<option value="2021">2020-21</option>-->
				     <!--<option value="1920">2019-20</option>-->
                                     <!--<option value="1819">2018-19</option>-->
                                 </select>
                                 </center>
                            </div>
                            <br>
                            <br>


                            <center>
                                <br>
                                <div id="generateBills" style="visibility:visible">
                                    <input type="hidden" name="repoOrder" id="repoOrder" value="N" />
                                    <input type="button" class="btn btn-info" value="generate bill" name="Save" id="Save" onclick="submitPage('<c:out value="${consignmentOrderId}"/>');" />
                                    <input type="button" class="btn btn-info" value="Print Preview" name="Next" onclick="printPreview('<c:out value="${tripSheetId}"/>');" />
                                </div>
                            </center>
                        </div>

                        <script type="text/javascript" language="javascript">
//function checkArticleName(){
//    boolean flag=false;
//    var detaintionCharge = document.getElementsByName("detaintionCharge");
//     for (var i = 1; i < detaintionCharge.length; i++) {
//         if (document.getElementById("articleName"+i).value == 'Rice' || document.getElementById("articleName"+i).value == 'rice' || document.getElementById("articleName"+i).value == 'RICE'){
//             flag=true;
//         }
//     }
//     if(flag){
//         document.getElementById("commodity").value="1";
//     }
//}
                            function sumOfCharges(showValue) {
                             //  alert("called");
                                var weightmentCharge = "";
                                var expenseType = document.getElementsByName("expenseType");
                                var detaintionCharge = document.getElementsByName("detaintionCharge");
                                var tollCharge = document.getElementsByName("tollCharge");
                                var greenTax = document.getElementsByName("greenTax");
                                var otherExpense = document.getElementsByName("otherExpense");
                                var netTotalAmount = 0, IGSTPercentage = 0, SGSTPercentage = 0, CGSTPercentage = 0;
                                var sum = 0;
                                var totOtherExpense = 0;
                                netTotalAmount = document.getElementById("netTotalAmount").value;
                                var IGST = document.getElementById("IGST").value;
                                var SGST = document.getElementById("SGST").value;
                                var CGST = document.getElementById("CGST").value;
                                var OrgId = document.getElementById("organizationId").value;
                                var companyType = document.getElementById("companyType").value;
                                var billingStateId = document.getElementById("billingState").value;
                                var GSTNo = document.getElementById("GSTNo").value;
                                var total = 0;
                                var totalTolltax = 0;
                                var sumOtherExpense = 0;
                                var check = 0;
                                var neglectGSTSum = 0;
                                var neglectGSTax = 0;
                                for (var i = 0; i < detaintionCharge.length; i++) {
                                    if (document.getElementById("weightmentCharge" + (i + 1)).value != 0 && document.getElementById("expenseType" + (i + 1)).value == 0) {
                                        alert("please select expesne type for weightment charges.." + document.getElementById("weightmentCharge" + (i + 1)).value);
//                                        $('#update').hide();
                                        check++;
                                    }
                                    if (expenseType[i].value == 1) {
                                        weightmentCharge = document.getElementById("weightmentCharge" + (i + 1)).value;
                                    } else {
                                        weightmentCharge = "0";
                                    }
                                    total += +detaintionCharge[i].value + +tollCharge[i].value + +greenTax[i].value + +otherExpense[i].value + +weightmentCharge;
                                    totalTolltax += parseFloat(tollCharge[i].value) + parseFloat(greenTax[i].value);
                                    //checking gst applicability
                                    if (document.getElementById("gstApplicable" + (i + 1)).value != 'Y') {
                                        neglectGSTSum += document.getElementById("estimatedRevenue" + (i + 1)).value;
                                        neglectGSTax += +detaintionCharge[i].value + +tollCharge[i].value + +greenTax[i].value + +otherExpense[i].value + +weightmentCharge;

                                    }
                                    if (document.getElementById("gstApplicable" + (i + 1)).value == 'Y') {
                                        var tripGSTSum = parseFloat(document.getElementById("estimatedRevenue" + (i + 1)).value) + parseFloat(detaintionCharge[i].value) + parseFloat(otherExpense[i].value) + parseFloat(weightmentCharge);

                                        if (companyType != 1 && OrgId == "1" && billingStateId != 9 && GSTNo == "") {
                                            document.getElementById("tripIGSTAmount" + (i + 1)).value = parseFloat((parseFloat(tripGSTSum).toFixed(2) * IGST) / 100).toFixed(2);
                                        } else if (companyType != 1 && OrgId == "1" && billingStateId == 9 && GSTNo == "") {
                                            document.getElementById("tripCGSTAmount" + (i + 1)).value = parseFloat((parseFloat(tripGSTSum).toFixed(2) * CGST) / 100).toFixed(2);
                                            document.getElementById("tripSGSTAmount" + (i + 1)).value = parseFloat((parseFloat(tripGSTSum).toFixed(2) * SGST) / 100).toFixed(2);
                                        }
                                    }

                                }
                                if (check == 0) {
                                    $('#update').show();
                                }

                                totOtherExpense += +sumOtherExpense + +total; //alert("total:"+totOtherExpense);
                                document.getElementById("totOtherExpense").value = parseFloat(totOtherExpense).toFixed(2);
                                sum = +total + +netTotalAmount;
                                var baseSumForGST = parseFloat(sum) - (parseFloat(totalTolltax) + parseFloat(neglectGSTSum) + parseFloat(neglectGSTax));
                                if (companyType != 1 && OrgId == "1" && billingStateId == 9 && GSTNo == "") {
                                    SGSTPercentage = parseFloat((parseFloat(baseSumForGST).toFixed(2) * SGST) / 100).toFixed(2);
                                    //alert("SGSTPercentage:"+SGSTPercentage);
                                    CGSTPercentage = parseFloat((parseFloat(baseSumForGST).toFixed(2) * CGST) / 100).toFixed(2);
                                    document.getElementById("cgstAmount").value = parseFloat((parseFloat(baseSumForGST).toFixed(2) * CGST) / 100).toFixed(2);
                                    document.getElementById("cgstAmountSpan").innerHTML = parseFloat((parseFloat(baseSumForGST).toFixed(2) * CGST) / 100).toFixed(2);
                                    document.getElementById("sgstAmount").value = parseFloat((parseFloat(baseSumForGST).toFixed(2) * SGST) / 100).toFixed(2);
                                    document.getElementById("sgstAmountSpan").innerHTML = parseFloat((parseFloat(baseSumForGST).toFixed(2) * SGST) / 100).toFixed(2);
                                    sum = parseFloat(sum) + parseFloat(SGSTPercentage) + parseFloat(CGSTPercentage);
                                    document.getElementById("totalTax").value = parseFloat(document.getElementById("cgstAmount").value) + parseFloat(document.getElementById("sgstAmount").value);

                                    //alert("sum in if:"+sum);
                                } else if (companyType != 1 && OrgId == "1" && billingStateId != 9 && GSTNo == "") {
                                    IGSTPercentage = parseFloat((parseFloat(baseSumForGST).toFixed(2) * IGST) / 100).toFixed(2);
                                    document.getElementById("igstAmount").value = parseFloat((parseFloat(baseSumForGST).toFixed(2) * IGST) / 100).toFixed(2);
                                    document.getElementById("igstAmountSpan").innerHTML = parseFloat((parseFloat(baseSumForGST).toFixed(2) * IGST) / 100).toFixed(2);
                                    sum = parseFloat(sum) + parseFloat(IGSTPercentage);
                                    document.getElementById("totalTax").value = document.getElementById("igstAmount").value;
                                }

                                
                                document.getElementById("netAmount").value = parseFloat(sum).toFixed(2);
                                document.getElementById("netAmountSpan").innerHTML = parseFloat(sum).toFixed(2);
                                document.getElementById("totalExpToBeBilled").value = parseFloat(totOtherExpense).toFixed(2);
                                document.getElementById("totalExpToBeBilledSpan").innerHTML = parseFloat(totOtherExpense).toFixed(2);
                                if (showValue == '1') {
                                    $('#generateBills').show();
                                } else {
                                    $('#generateBills').hide();
                                }
                            }

                            function printPreview(tripSheetIds) {
                                var checkTax = document.getElementById("checkTax").value;
                                var GSTNo = document.getElementById("GSTNo").value;
                                var panNo = document.getElementById("panNo").value;
                                var billingPartyId = document.getElementById("billingPartyId").value;
                                var igstAmount = 0;
                                var cgstAmount = 0;
                                var sgstAmount = 0;
                                var SGST = 0;
                                var IGST = 0;
                                var CGST = 0;
                                if (checkTax == "2") {
                                    igstAmount = document.getElementById("igstAmount").value;
                                    IGST = document.getElementById("IGST").value;
                                }
                                if (checkTax == "1") {
                                    cgstAmount = document.getElementById("cgstAmount").value;
                                    sgstAmount = document.getElementById("sgstAmount").value;
                                    CGST = document.getElementById("CGST").value;
                                    SGST = document.getElementById("SGST").value;
                                }
                                var netAmount = document.getElementById("netAmount").value;
                                var finYear = document.getElementById("finYear").value;
        
                                if(finYear==""){
                                    alert("Please selct the financal Year.");
                                    document.getElementById("finYear").focus(); 
                                    return false;
                                }else{
                                    window.open('/throttle/printPreviewForSupplementBilling.do?tripSheetIds=' + tripSheetIds + '&taxCheck=' + checkTax + '&igstAmount=' + igstAmount + '&cgstAmount=' + cgstAmount + '&sgstAmount=' + sgstAmount + '&netAmount=' + netAmount + '&IGST=' + IGST + '&CGST=' + CGST + '&SGST=' + SGST + '&panNo=' + panNo + '&GSTNo=' + GSTNo+ '&finYear=' + finYear+ '&billingPartyId=' + billingPartyId, 'PopupPage', 'height = 600, width = 900, scrollbars = yes, resizable = yes');
                                }
                            }

                            function saveOdoApprovalRequest() {
                                //                    document.getElementById("spinner").style.display = "block";
                                //                    document.getElementById("loader").style.display = "block";
                                //                    document.getElementById('loader').className +=  'ui-loader-background';
                                var odoUsageRemarks = document.trip.odoUsageRemarks.value;
                                var requestType = 1; //odo request
                                saveApprovalRequest(odoUsageRemarks, requestType, 1)

                            }



                            var httpReq;
                            var temp = "";
                            function saveApprovalRequest(remarks, requestType, val) {
                                var tripId = document.getElementById("tripId1").value;

                                remarks = remarks.trim();
                                // alert("remarks:"+remarks);
                                var customerId = document.getElementById("customerId").value;
                                if (remarks != '') {
                                    var url = "/throttle/saveBillingRevenueChangeApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&remarks=" + remarks + "&customerId=" + customerId;
                                    //  alert(url);
                                    if (window.ActiveXObject)
                                    {
                                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                    } else if (window.XMLHttpRequest)
                                    {
                                        httpReq = new XMLHttpRequest();
                                    }
                                    httpReq.open("GET", url, true);
                                    httpReq.onreadystatechange = function () {
                                        processOdoApprovalRequest(requestType, val);
                                    };
                                    httpReq.send(null);
                                } else {
                                    alert('please enter request description');
                                }

                            }

                            function processOdoApprovalRequest(requestType, val) {
                                if (httpReq.readyState == 4) {
                                    if (httpReq.status == 200) {
                                        temp = httpReq.responseText.valueOf();
                                        //alert(temp);
                                        if (temp != '0' && temp != '' && temp != null && temp != 'null') {
                                            if (val == 1) {
                                                $('#odoApprovalRequest').hide();
                                                $('#odoPendingApproval').show();
                                            }

                                        }
                                    } else {
                                        //alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                    }
                                }
                                document.tripExpense.action = '/throttle/viewTripExpense.do';
                                document.tripExpense.submit();
                            }
                            function showHide() {
                                var movementType =<%=request.getAttribute("movementType")%>
                                //   alert(movementType);
                                if (movementType == '1') {

                                    $('#shippingLine').show();
                                    $('#billOfEntry').hide();
                                } else if (movementType == '2') {
                                    $('#shippingLine').hide();
                                    $('#billOfEntry').show();

                                }
                            }
                            
                            function submitPageForDetaiontion() {
                                var expenseType = document.getElementsByName("expenseType");
                                var weightmentCharge = document.getElementsByName("weightmentCharge");
                                var check = 0;

//                                for (var i = 0; i < weightmentCharge.length; i++) {                                    
//                                    if (document.getElementById("detaintionCharge" + (i + 1)).value == 0.00 && document.getElementById("detaintionCharge" + (i + 1)).value == 0) {
//                                        alert("please enter the detention charges.." + document.getElementById("detaintionCharge" + (i + 1)).value);
//                                        document.getElementById("detaintionCharge" + (i + 1)).focus();
//                                        check = 1;
//                                        break;
//                                    } 
//                                    if (document.getElementById("expenseType" + (i + 1)).value == 0.00 && document.getElementById("expenseType" + (i + 1)).value == 0) {
//                                        alert("please select the expenseType " + document.getElementById("expenseType" + (i + 1)).value);
//                                        document.getElementById("expenseType" + (i + 1)).focus();
//                                        check = 1;
//                                        break;
//                                    } 
//                                    
//                                    else {
//                                        check = 0;
//
//                                    }
//                                }
                                
                                if (check == 0) {
                                    document.trip.action = '/throttle/updateTollAndDetaintionForSupplemetBilling.do';
                                    document.trip.submit();
                                    $("#buttonDesign").hide();
                                    $("#submitValue1").hide();
                                }
                            }
                        </script>




                        <script>
                            $(".nexttab").click(function () {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                        </script>
                             <script>
                                                    var xxx=document.getElementById("remarks11").value;
                                                 
                                                    document.getElementById("detaintionRemarks").value=xxx;
                                                    </script>

                    </div>
                    <input type="hidden" name="customerId" id="customerId" value='<c:out value="${customerId}"/>' />
                    <input type="hidden" name="tripSheetId" id="tripSheetId" value='<c:out value="${tripSheetId}"/>' />
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
