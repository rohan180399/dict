
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            yearRange: '1900:' + new Date().getFullYear(),
            changeMonth: true, changeYear: true
        });
    });

</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>

<script type="text/javascript">

    function currentTime() {
//                alert("hellogopi");
        var grDate = document.getElementById("grDate").value;
        var firstpointId = document.getElementById("firstPointId").value;
        var grHours = document.getElementById("grHours").value;
        var grMins = document.getElementById("grMins").value;

        var date, curr_hour, curr_minute;
        date = new Date();
        curr_hour = date.getHours();
        curr_minute = date.getMinutes();
        if(parseInt(curr_hour) < 10){
            curr_hour = '0'+curr_hour;
        }
        if(parseInt(curr_minute) < 10){
            curr_minute = '0'+curr_minute;
        }
        document.getElementById("vehicleactreporthour").value = curr_hour;
        document.getElementById("vehicleactreportmin").value = curr_minute;
        document.getElementById("vehicleloadreporthour").value = curr_hour;
        document.getElementById("vehicleloadreportmin").value = curr_minute;
//        alert(firstpointId);
//        alert(curr_hour);
//        alert(curr_minute);
        if (firstpointId != '617') {

            document.getElementById("tripStartHour").value = curr_hour;
            document.getElementById("tripStartMinute").value = curr_minute;
        } else {
//            alert(grDate);
            document.getElementById("startDate").value = grDate;
            if(grHours == ''){
                grHours = '00';
            }
            if(grMins == ''){
                grMins = '00';
            }
//            if(parseInt(grHours) < 10 && grHours != ''){
//                grHours = '0'+grHours;
//            }
//            if(parseInt(grMins) < 10 && grMins != ''){
//                grMins = '0'+grMins;
//            }
//            alert(grHours);
//            alert(grMins);
            document.getElementById("tripStartHour").value = grHours;
            document.getElementById("tripStartMinute").value = grMins;

        }
       // alert(document.getElementById("tripStartMinute").value);
    }

    function calculateStartDateDifference() {
        var endDates1 = document.getElementById("tripScheduleDate").value;
        var tempDate11 = endDates1.split("-");
        var stDates1 = document.getElementById("startDate").value;
        var tempDate21 = stDates1.split("-");
        var prevTime1 = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
        var thisTime1 = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now
        var difference2 = thisTime1.getTime() - prevTime1.getTime();   // now - Feb 1

        if (prevTime1.getTime() < thisTime1.getTime()) {
            alert(" Selected Date is less than Scheduled Date");
            document.getElementById("startDate").value = document.getElementById("todayDate").value;
        }
    }


    function calculateDate() {
        var endDates = document.getElementById("vehicleactreportdate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("vehicleloadreportdate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
        var difference1 = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var endDates1 = document.getElementById("vehicleloadreportdate").value;
        var tempDate11 = endDates1.split("-");
        var stDates1 = document.getElementById("startDate").value;
        var tempDate21 = stDates1.split("-");
        var prevTime1 = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
        var thisTime1 = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now
        var difference2 = thisTime1.getTime() - prevTime1.getTime();   // now - Feb 1
        if (prevTime.getTime() < thisTime.getTime()) {
            alert(" Selected Date is greater than Reporting Time");
            document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
        }
        if (prevTime1.getTime() < thisTime1.getTime()) {
            alert(" Selected Date is less than Reporting Time");
            document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
        }
    }
    function calculateTime() {
        var endDates = document.getElementById("vehicleactreportdate").value;
        var hour1 = document.getElementById("vehicleactreporthour").value;
        var minute1 = document.getElementById("vehicleactreportmin").value;
        var endTimeIds = hour1 + ":" + minute1 + ":" + "00";
        var tempDate1 = endDates.split("-");
        var tempTime1 = endTimeIds.split(":");
        var stDates = document.getElementById("vehicleloadreportdate").value;
        var hour = document.getElementById("vehicleloadreporthour").value;
        var minute = document.getElementById("vehicleloadreportmin").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        var endDates1 = document.getElementById("startDate").value;
        var hour11 = document.getElementById("tripStartHour").value;
        var minute11 = document.getElementById("tripStartMinute").value;
        var endTimeIds1 = hour11 + ":" + minute11 + ":" + "00";
        var tempDate11 = endDates1.split("-");
        var tempTime11 = endTimeIds1.split(":");
        var stDates1 = document.getElementById("vehicleloadreportdate").value;
        var hour2 = document.getElementById("vehicleloadreporthour").value;
        var minute2 = document.getElementById("vehicleloadreportmin").value;
        var stTimeIds1 = hour2 + ":" + minute2 + ":" + "00";
        var tempDate21 = stDates1.split("-");
        var tempTime21 = stTimeIds1.split(":");
        var prevTime2 = new Date(tempDate21[2], tempDate21[1], tempDate21[0], tempTime21[0], tempTime21[1]);  // Feb 1, 2011
        var thisTime2 = new Date(tempDate11[2], tempDate11[1], tempDate11[0], tempTime11[0], tempTime11[1]);              // now
        var difference2 = thisTime2.getTime() - prevTime2.getTime();   // now - Feb 1
        var hoursDifference2 = Math.floor(difference2 / 1000 / 60 / 60);
        if (prevTime.getTime() < thisTime.getTime()) {
            alert("Selected Time is greater than Reporting Time ");
            document.getElementById("vehicleloadreporthour").focus();
        }
        if (prevTime2.getTime() > thisTime2.getTime()) {
            alert("Selected Time is   Less than Start Time");
            document.getElementById("vehicleloadreporthour").focus();
        }

    }
</script>

<script type="text/javascript" language="javascript">

    function submitPage() {

        var errorMsg1 = "";
        if (isEmpty(document.getElementById("startDate").value)) {
            alert('please enter plan start date');
            document.getElementById("startDate").focus();
        }
        if (isEmpty(document.getElementById("startOdometerReading").value)) {
            alert('please enter start odometer reading');
            document.getElementById("startOdometerReading").focus();
            return;
        }
        var customer = document.getElementById("customerType").value;
        if (customer == "2") {
            if (isEmpty(document.getElementById("vehicleactreportdate").value)) {
                alert('please enter the vehicle reporting date');
                document.getElementById("vehicleactreportdate").focus();
            }
            if (isEmpty(document.getElementById("vehicleloadtemperature").value)) {
                alert('please enter the loading temperature');
                document.getElementById("vehicleloadtemperature").focus();
            }
            if (isEmpty(document.getElementById("startHM").value)) {
                alert('please enter start HM');
                document.getElementById("startHM").focus();
            }
            if (parseFloat(document.getElementById("vehicleloadtemperature").value) == 0) {
                //errorMsg1 += "The value of vehicle temperature is 0\n";
            }
            if (parseFloat(document.getElementById("startOdometerReading").value) == 0) {
                errorMsg1 += "The value of start odometer meter reading is 0\n";
            }
            if (parseFloat(document.getElementById("startHM").value) == 0) {
                errorMsg1 += "The value of start hour meter  reading is 0";

            }

            if (errorMsg1 != "") {
                var r = confirm(errorMsg1);
                if (r == true)
                {
                    $("#StartTrip").hide();
                    document.trip.action = '/throttle/updateStartTripSheet.do';
                    document.trip.submit();
                }
            } else {
                $("#save").hide();
                document.trip.action = '/throttle/updateStartTripSheet.do';
                document.trip.submit();

            }
        }
        else {
            $("#save").hide();
            document.trip.action = '/throttle/tripDetails.do';
            document.trip.submit();
        }
    }
//           function showGPSTable(){
//               var vehOwnerShip=document.getElementById("vehOwnerShip").value;
//               if(vehOwnerShip=='3'){
//                   document.trip.elements['gpsTable'].style.display='block';
//               }else{
//
//                  document.trip.elements['gpsTable'].style.display='none';
//                   }
//               }


</script>
<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">View start Trip Sheet</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="currentTime();
            hideGps();
            addRow1();">

                <form name="trip" method="post">
                    <%
               Date today = new Date();
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
               String startDate = sdf.format(today);
                    %>
                    <%@ include file="/content/common/message.jsp" %>

                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:INR </b></font></td>
                                                        <td> <font color="white"><b><c:out value="${trip.orderRevenue}" /></b></font></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense: INR </b></font></td>
                                                        <td><font color="white"><b> <c:out value="${trip.orderExpense}" /></b></font></td>
                                                    <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                                    <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                                    <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>paid Expense:</b></font></td>
                                                        <td><font color="white"><b>  <c:out value="${trip.paidExpense}" /></b></font>
                                                            <input type="hidden" name="paidExpense" value='<c:out value="${paidExpense}" />'>
                                                        </td>

                                                    </tr>
                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="paidExpense" value="${trip.paidExpense}" />
                                                     <c:set var="profitMargin" value="${trip.orderRevenue - trip.orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td><font color="white"><b>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>

                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <table width="100%">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <input type="hidden" name="driverId" value='<c:out value="${trip.driverId}" />'>
                                <% if(loopCntr == 0) {%>
                                <tr id="index" height="30">
                                    <td>Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                    <td>Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    <td>Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                                    <td>Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                    <td>Status: <c:out value="${trip.status}"/>
                                        <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                        <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                        <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                        <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                                        <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                                        <c:set var="customerId" value="${ trip.customerId}"></c:set></td>
                                    </tr>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li  class="active" data-toggle="tab"><a href="#tripStart"><span>Trip Start </span></a></li>
                                <%--     <li><a href="#bunkDetail"><span>Fuel Details</span></a></li> --%>
                            <li data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                            <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                            <li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>
                            <li data-toggle="tab"><a href="#container"><span>Container Details</span></a></li>
                            <!--                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                            <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                            <!--
                            <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                            <!--<li><a href="#advDetail"><span>Advance</span></a></li>-->
                            <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                            <!--<li><a href="#summary"><span>Remarks</span></a></li>-->
                        </ul>
                        <input type="hidden" name="pageId" id="pageId" value="<c:out value="${uniquePageId}"/>"/>
                        <div id="container" class="tab-pane">

                            <% int index10 = 1; int containerCount = 0; %>

                            <c:if test = "${containerDetails != null}" >
                                <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30">
                                        <td  height="30" >S No</td>
                                        <td  height="30" >Container Type</td>
                                        <td  height="30" >Container No</td>
                                    </tr>
                                    <c:forEach items="${containerDetails}" var="container">
                                        <%
                                               containerCount++;
                                               String classText = "";
                                               int oddEven1 = index10 % 2;
                                               if (oddEven1 > 0) {
                                                   classText = "text1";
                                               } else {
                                                   classText = "text2";
                                               }
                                        %>
                                        <tr >
                                            <td class="<%=classText%>" height="30" ><%=index10++%></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${container.containerName}" /></td>
                                            <td class="<%=classText%>" height="30" >
                                                <input type="text" name="containerNo" value='<c:out value="${container.containerNo}" />' />
                                            </td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>
                                <center><input type="button" class="btn btn-success" name="add" value="SaveContainer" style="width:110px;height:25px;padding:2px; "/></center>
                                <br/>
                                <br/>

                            </c:if>
                            <% if (containerCount == 0) {%>
                            <table class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="containerTBL">
                                <tr id="rowId0">
                                    <td class="contenthead" align="center" height="30" >Sno</td>
                                    <td class="contenthead" height="30" >Container Type </td>
                                    <td class="contenthead" height="30" >Container No</td>
                                </tr>
                                <% int index5 = 1;%>
                                <tr>
                                    <!--                            <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>-->

                                </tr>
<!--                                <tr>
                                    <td colspan="4" align="center">
                                        <input type="button" class="button" name="add" value="add" onclick="addRowContainer();"/>
                                    </td>
                                </tr>-->



                                <script>


                                    var rowCount = 1;
                                    var sno = 1;
                                    var rowCount1 = 1;
                                    var sno1 = 0;
                                    var sno4 = 0;
                                    var httpRequest;
                                    var httpReq;
                                    var styl = "";
                                    function addRowContainer() {
                                        if (parseInt(rowCount1) % 2 == 0)
                                        {
                                            styl = "text2";
                                        } else {
                                            styl = "text1";
                                        }

                                        sno4++;
                                        var tab = document.getElementById("containerTBL");
                                        //find current no of rows
                                        var rowCountNew = document.getElementById('containerTBL').rows.length;
                                        rowCountNew--;
                                        var newrow = tab.insertRow(rowCountNew);
                                        cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' style='width:10px;'> " + sno4 + "</td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25'><select class='textbox' style='width:90px' id='containerTypeId" + sno + "'   name='trailerId'><option selected value=0>---Select---</option> <c:if test="${containerListDetails != null}" ><c:forEach items="${containerListDetails}" var="details"><option  value='<c:out value="${details.containerId}" />'><c:out value="${details.containerName}" /> </c:forEach > </c:if> </select></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' ><input type='text' value='' style='width:120px;'  name='containerNo'  id='containerNo" + sno + "'   value='' ></td>";
                                        cell.innerHTML = cell0;

                                        $(document).ready(function() {
                                            $("#datepicker").datepicker({
                                                showOn: "button",
                                                buttonImage: "calendar.gif",
                                                buttonImageOnly: true

                                            });
                                        });
                                        $(function() {
                                            $(".datepicker").datepicker({
                                                changeMonth: true, changeYear: true
                                            });
                                        });
                                        rowCount1++;
                                        sno++;
                                    }


                                        </script>


                                    </table>
                            <%}%>

                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                        </div>

                        <div id="tripStart" class="tab-pane active">
                            <script>
                                var rowCount = 1;
                                var sno = 0;
                                var rowCount1 = 1;
                                var sno1 = 0;
                                var httpRequest;
                                var httpReq;
                                var styl = "";

                                function addRow1() {

                                    if (parseInt(rowCount1) % 2 == 0)
                                    {
                                        styl = "text2";
                                    } else {
                                        styl = "text1";
                                    }
                                    sno1++;
                                    var tab = document.getElementById("addTyres1");
                                    //find current no of rows
                                    var rowCountNew = document.getElementById('addTyres1').rows.length;
                                    rowCountNew--;
                                    var newrow = tab.insertRow(rowCountNew);

                                    var cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' > " + sno1 + "</td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;

                                    // Positions
                                    cell = newrow.insertCell(1);
                                    var cell0 = "<td class='text1' height='30' ><input type='text' name='productCodes' class='textbox' value='0'>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    // TyreIds
                                    var cell = newrow.insertCell(2);
                                    var cell0 = "<td><input type='hidden' name='productNames' id='productNames' class='textbox' value='0'><input type='hidden' name='productId' id='productId' class='textbox' value='0'><select id='productName' name='productName' style='width:180px;height:40px;' onchange='setProductName();'/><option value='0'>--select--</oprtion><c:forEach items="${comodityDetails}" var="com"><option value='<c:out value="${com.commodityId}" />~<c:out value="${com.commodityName}" />'><c:out value="${com.commodityName}" /></option></c:forEach></select></td>"
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    var cell = newrow.insertCell(3);
                                    var cell0 = "<td class='text1' height='30' ><input type='text' name='productbatch' class='textbox' value='0' >";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;

                                    cell = newrow.insertCell(4);
                                    var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos'   onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='textbox' value='0' onkeyup='calcTotalPacks(this.value)'>";

                                    cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
                                    cell.setAttribute("className", "text1");
                                    cell.innerHTML = cell1;

                                    cell = newrow.insertCell(5);
                                    var cell1 = "<td class='text1' height='30' ><select name='productuom' id='uom'> <option value='1' selected >Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option></select> </td>";

                                    cell.setAttribute("className", "text1");
                                    cell.innerHTML = cell1;

                                    cell = newrow.insertCell(6);
                                    var cell0 = "<td class='text1' height='30' ><input type='text' name='weights'   onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='textbox' value='0' onkeyup='calcTotalWeights(this.value)' >";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;

                                    cell = newrow.insertCell(7);
                                    var cell0 = "<td class='text1' height='30' ><input type='text' name='loadedpackages'   id='loadedpackages' class='textbox' value='0' >";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    rowCount1++;
                                }
                            </script>
                            <c:if test = "${customerId == 1040}" >
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30">
                                        <td  colspan="4" >Trip Start Details</td>
                                    </tr>
                                    <tr>
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <td class="text1">Trip Planned Start Date</td>
                                                <td class="text1"> <c:out value="${trip.tripScheduleDate}" /></td>

                                                <td class="text1" height="25" >Trip Planned Start Time111 </td>
                                                <td class="text1"> <c:out value="${trip.tripScheduleTime}" /></td>

                                            </c:forEach>
                                        </c:if>
                                    </tr>
                                    <tr>
                                        <td class="text2"><font color="red">*</font>Trip Actual Start Date</td>
                                        <td class="text2"><input type="text" name="startDate" id="startDate" class="datepicker" value="<%=startDate%>" onchange="calculateStartDateDifference();">
                                            <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                        <td class="text2" height="25" ><font color="red">*</font>Trip Actual Start Time </td>
                                        <td class="text2" colspan="3" align="left" height="25" >HH:<select name="tripStartHour"  id="tripStartHour" class="textbox"><option value="00" selected >00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="tripStartMinute" id="tripStartMinute" class="textbox"><option value="00" selected >00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                            <c:if test="${tripSheetId != null}">
                                                <input type="hidden" name="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                                            </c:if>
                                            <c:if test="${tripSheetId == null}">
                                                <input type="hidden" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                            </c:if>
                                            <input type="hidden" name="customerType" id="customerType" value="1"  >
                                            <input type="hidden" name="vehicleactreportdate" id="vehicleactreportdate" value=""  >
                                            <input type="hidden" name="vehicleloadreportdate" id="vehicleloadreportdate"  value=""  >
                                            <input type="hidden" name="vehicleactreporthour" id="vehicleactreporthour"  value="00"  >
                                            <input type="hidden" name="vehicleloadreporthour" id="vehicleactreporthour"  value="00"  >
                                            <input type="hidden" name="vehicleactreportmin" id="vehicleactreportmin"  value="00"  >
                                            <input type="hidden" name="vehicleloadreportmin" id="vehicleactreportmin"  value="00"  >
                                            <input type="hidden" name="startHM" id="startHM"  value="0"  >
                                            <input type="hidden" name="vehicleloadtemperature" id="vehicleloadtemperature"  value=""  >
                                            <input type="hidden" name="consignmentId" value="0"/>
                                        <td><input type="hidden" name="productCodes" value="" />
                                        </td> </tr>
                                    <tr>
                                        <td class="text1"><font color="red">*</font>Trip Start Odometer Reading(KM)</td>
                                            <c:if test = "${tripPreStartDetails != null}" >
                                                <c:forEach items="${tripPreStartDetails}" var="tripPreStart">
                                                    <c:if test = "${tripPreStart.preStartLocationStatus == 1}" >
                                                    <td class="text1"><input type="text" name="startOdometerReading" value='<c:out value="${previousEndKm}" />' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" ></td>
                                                    </c:if>
                                                    <c:if test = "${tripPreStart.preStartLocationStatus != 1}" >
                                                    <td class="text1"><input type="text" name="startOdometerReading" value='0' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" ></td>
                                                    </c:if>

                                            </c:forEach>
                                        </c:if>
                                    </tr>
                                </table>
                            </c:if>
                            <c:if test = "${customerId != 1040}" >
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30">
                                        <td colspan="4" >Vehicle Reporting Details</td>
                                    </tr>
                                    <tr height="30">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <td class="text2" width="25%">Trip Planned Start Date</td>
                                                <td class="text2" width="25%"> <c:out value="${trip.tripScheduleDate}" /></td>
                                                <td class="text2" height="25" width="25%">Trip Planned Start Time </td>
                                                <td class="text2" width="25%"> <c:out value="${trip.tripScheduleTime}" /></td>
                                            <input type="hidden" name="firstPointId" id="firstPointId" value='<c:out value="${trip.firstPointId}"/>'/>
                                            <input type="hidden" name="grDate" id="grDate" value='<c:out value="${trip.grDate}"/>'/>
                                            <input type="hidden" name="grHours" id="grHours" value='<c:out value="${trip.grHours}"/>'/>
                                            <input type="hidden" name="grMins" id="grMins" value='<c:out value="${trip.grMins}"/>'/>
                                            <input type="hidden" name="vehOwnerShip" id="vehOwnerShip" value='<c:out value="${trip.ownerShip}" />'/>
                                        </c:forEach>
                                    </c:if>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" width="25%"><font color="red">*</font>Vehicle Actual Reporting Date</td>
                                        <td class="text1" width="25%"><input type="text" name="vehicleactreportdate" id="vehicleactreportdate"  class="datepicker" value="<%=startDate%>" style="width:130px;height:22px;"></td>
                                        <td class="text1" height="25" width="25%"><font color="red">*</font>Vehicle Actual Reporting Time </td>
                                        <td class="text1" colspan="3" align="left" height="25" width="25%">
                                            HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox" style="width:40px;height:22px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox" style="width:40px;height:22px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                        </td>
                                    </tr>
                                    <tr id="index" height="30">
                                        <td  colspan="4" >Vehicle Loading Details</td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text2" width="25%"><font color="red">*</font>Vehicle Actual Loading Date</td>
                                        <td class="text2" width="25%"><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" class="datepicker" value="<%=startDate%>" onchange="calculateDate();
                                    setTripActualDate(this.value)" style="width:130px;height:22px;"></td>
                                        <td class="text2" height="25" width="25%"><font color="red">*</font>Vehicle Actual Loading Time </td>
                                        <td class="text2" colspan="3" align="left" height="25"width="25%" >HH:<select name="vehicleloadreporthour"  onchange="setTimeHour(this.value)" id="vehicleloadreporthour" class="textbox" onchange="calculateTime();" style="width:40px;height:22px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleloadreportmin"  onchange="setTimeMinute(this.value)"  id="vehicleloadreportmin" class="textbox" style="width:40px;height:22px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1"><font color="red">*</font>Vehicle Loading Temperature</td>
                                        <td class="text1"><input type="text" name="vehicleloadtemperature" id="vehicleloadtemperature"  value="0" onchange="checkEmpty();" style="width:130px;height:22px;"></td>
                                        <td class="text1" style="display:none;">LR Number</td>
                                        <td class="text1" style="display:none;"><input type="text" name="lrNumber" id="lrNumber"  value="" class="textbox"></td>
                                    </tr>
                                </table>
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30">
                                        <td  colspan="6" >Trip Start Details</td>
                                    </tr>
                                    <tr height="30">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <td class="text2" width="25%">Trip Planned Start Date</td>
                                                <td class="text2" width="25%"> <c:out value="${trip.tripScheduleDate}" /></td>

                                                <td class="text2" height="25" width="25%">Trip Planned Start Time </td>
                                                <td class="text2" width="25%"> <c:out value="${trip.tripScheduleTime}" /></td>
                                            </c:forEach>
                                        </c:if>

                                    </tr>
                                    <tr height="30">
                                        <td class="text1" width="25%"><font color="red">*</font>ICD Out Date(Trip Actual Start Date)</td>
                                        <td class="text1" width="25%"><input type="text" name="startDate" id="startDate" class="datepicker" value="<%=startDate%>" onchange="calculateStartDateDifference();" style="width:130px;height:22px;">
                                            <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                        <td class="text1" height="25" width="25%"><font color="red">*</font>ICD Out Time(Trip Actual Start Time) </td>
                                        <td class="text1" colspan="3" align="left" height="25" width="25%">HH:<select name="tripStartHour"  id="tripStartHour" class="textbox" onchange="cheeckHr();" style="width:40px;height:22px;"><option value="00"  selected  >00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="tripStartMinute" id="tripStartMinute" class="textbox" style="width:40px;height:22px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select></td>

                                    </tr>
                                </table>
                                <!--<tr >-->
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="gps">

                                    <tr id="index" height="30">
                                        <td  colspan="4" >Vehicle GPS Details</td>
                                    </tr>


                                    <tr height="30">
                                        <td class="text2" width="25%"><font color="red">*</font>Vehicle Number</td>
                                        <td class="text2" width="25%"><input type="text" name="regNo" id="regNo"  class="" value="0" style="width:130px;height:22px;"> </td>
                                        <td class="text2" width="25%"><input type="hidden" name="tripId" value="<c:out value="${tripSheetId}"/>" /></td>
                                        <td class="text2" width="25%"></td>
                                    </tr>


                                    <tr height="30">
                                        <td class="text1" height="25" width="25%"><font color="red">*</font>GPS Vendor Name </td>
                                        <td class="text1" colspan="3" align="left" height="25" width="25%">
                                            <select name="gpsVendor" id="gpsVendor" class="textbox" style="width:130px;height:22px;">
                                                <option value="0">---select---</option>
                                                <option value="1">gps100</option>
                                                <option value="2">gps200</option>
                                                <option value="3">gps300</option>
                                            </select>
                                        </td>
                                        <td class="text1" width="50%" colspan="2"></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text2" width="25%"><font color="red">*</font>Gps Device No</td>
                                        <td class="text2" width="25%"><input type="text" name="gpsDeviceId" id="gpsDeviceId"  value="0"style="width:130px;height:22px;" ></td>
                                        <td class="text2" width="50%" colspan="2"></td>
                                    </tr>

                                </table>

                                <!--</tr>-->
                                <script type="text/javascript">
                                    
                                    function setProductName(){
                                        var productName = $("#productName").text();
                                        var productId = $("#productName").val();
                                        var productIdTmp = productId.split('~');
                                        $("#productId").val(productIdTmp[0]);
                                        $("#productNames").val(productIdTmp[1]);
                                    }
                                    
                                    
                                    function hideGps()
                                    {

                                        var val = document.getElementById("vehOwnerShip").value;


                                        if (val == 3) {
                                            document.getElementById("gps").style.display = "block";
                                        } else {
                                            document.getElementById("gps").style.display = "none";
                                        }

                                    }
                                    var diff;
                                    function check() {

                                        var obja = document.getElementById("startDate").value;
                                        var year = obja.match(/\d\d\d\d/)[0];
                                        var month = obja.match(/-(\d\d)-/)[1] - 1;
                                        var day = obja.match(/^\d\d/)[0];
                                        obja = new Date(year, month, day);

                                        var objb = document.getElementById("previousDate").value;
                                        var year = objb.match(/\d\d\d\d/)[0];
                                        var month = objb.match(/-(\d\d)-/)[1] - 1;
                                        var day = objb.match(/^\d\d/)[0];
                                        objb = new Date(year, month, day);

                                        var start = Date.parse(obja);

                                        var previous = Date.parse(objb);


                                        diff = start - previous;
                                        if (diff >= 0) {
                                            $("#save").show();
                                        } else {
                                            alert("start Date is less than Previous Trip!!");
                                            $("#save").hide();
                                            return false;
                                        }
                                    }

                                    function cheeckHr()
                                    {
                                //    alert("the value is" + diff);
                                        if (diff <= 0) {
                                //    alert("Nilesh");
                                            var hr = document.getElementById("previousHr").value;
                                            var starthr = document.getElementById("tripStartHour").value;
                                            if (starthr <= hr) {
                                                alert("Invalid time hr for trip start.pls check the previous trip End hour");
                                                $("#save").hide();
                                            } else {
                                                $("#save").show();
                                            }
                                        }
                                        else {
                                        }
                                    }



                                </script>

                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr height="30">
                                        <td class="text2" width="25%"><font color="red">*</font>Trip Start Odometer Reading(KM)</td>
                                            <c:if test = "${tripPreStartDetails != null}" >
                                                <c:forEach items="${tripPreStartDetails}" var="tripPreStart">
                                                    <c:if test = "${tripPreStart.preStartLocationStatus == 1}" >
                                                    <td class="text2" width="25%"><input type="text" name="startOdometerReading" value='<c:out value="${previousEndKm}" />' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" style="width:130px;height:22px;"></td>
                                                    </c:if>
                                                    <c:if test = "${tripPreStart.preStartLocationStatus != 1}" >
                                                    <td class="text2" width="25%"><input type="text" name="startOdometerReading" value='0' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" style="width:130px;height:22px;"></td>
                                                    </c:if>

                                            </c:forEach>
                                        </c:if>




                                        <td class="text2" width="25%"><font color="red">*</font> Trip Start Reefer Reading(HM)</td>
                                        <td class="text2" width="25%"><input type="text" name="startHM" id="startHM" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value="<c:out value="${previousEndHm}" />" style="width:130px;height:22px;"></td>
                                    </tr>
                                    <tr height="30">
                                        <td class="text1" >Trip Remarks</td>
                                        <td class="text1" >

                                            <textarea rows="3" cols="30" class="textbox" name="startTripRemarks" id="startTripRemarks"   style="width:130px"></textarea>
                                        <td class="text1"><input type="hidden" name="previousDate" id="previousDate" value="<c:out value="${previousTripEndDate}" />"></td>
                                        <td class="text1"><input name="previousHr" id="previousHr" type="hidden" value="<c:out value="${previousTripEndHr}"/>"></td>
                                        <td class="text1"><input name="previousMin" id="previousMin" type="hidden"  value="<c:out value="${previousTripEndMin}"/>"></td>
                                        <!--                         <td class="text1">Trip Status</td>-->
                                        <!--                         <td>
                                                                     <select name="statusId" id="status" class="textbox"  style="width:125px;">
                                                                <option value="0"> -Select- </option>
                                                                <option value="10" selected>Trip Started </option>
                                                                </select>
                                                                    </td>-->


                                        <c:if test="${tripSheetId != null}">
                                        <input type="hidden" name="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                                    </c:if>
                                    <c:if test="${tripSheetId == null}">
                                        <input type="hidden" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                    </c:if>

                                    <input type="hidden" name="customerType" id="customerType" value="2" />
                                    <td class="text1" colspan="2" ></td>
                                    </tr>

                                </table>
                                <br>
                                <br>
                               <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr id="index" height="30">
                                        <td width="20"  align="center" height="30" >Sno</td>
                                        <td  height="30" >Product/Article Code</td>
                                        <td  height="30" ><font color='red'>*</font>Commodity </td>
                                        <td  height="30" >Batch </td>
                                        <td  height="30" ><font color='red'></font>No of Packages</td>
                                        <td  height="30" ><font color='red'></font>Uom</td>
                                        <td  height="30" ><font color='red'></font>Total Weight (in Kg)</td>
                                        <td  height="30" ><font color='red'></font>Loaded Package Nos</td>
                                    </tr>
                                    <c:set var="conId" value="0" />
                                    <c:if test = "${tripPackDetails != null}" >
                                        <c:forEach items="${tripPackDetails}" var="trippack">
                                            <tr>
                                                <td></td>
                                                <td><input type="text" name="productCodes" value="<c:out value="${trippack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames1" value="<c:out value="${trippack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" value="<c:out value="${trippack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" value="<c:out value="${trippack.packageNos}"/>" readonly/></td>
                                                <td>
                                                    <input type="text" name="uomId" value="<c:out value="${trippack.uom}"/>" readonly/>
                                                    <input type="hidden" name="productuom" value="<c:out value="${trippack.uomId}"/>" readonly/>
                                                </td>
                                                <td><input type="text" name="weights" value="<c:out value="${trippack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" onKeyPress="return onKeyPressBlockCharacters(event);"   value="0"/>
                                                </td>
                                                <c:set var="conId" value="${trippack.consignmentId}" />
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    <input type="hidden" name="consignmentId" value="<c:out value="${conId}"/>"/>



                                    <tr height="50">
                                        <td colspan="8" align="center"> &nbsp;
                                            <!--                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">-->
                                            <!--<input type="button" class="btn btn-success" value="Add Row" name="savePkg" onClick="addRow1()" style="width:100px;height:25px;padding:2px; ">-->

                                        </td>
                                    </tr>
                                </table>  
                                <br/> <br/>

                                <br/>
                            </c:if>

                            <br/>
                            <center>
                                
                                <input type="button" class="btn btn-success" name="StartTrip" style="width:100px;height:35px;padding:2px; " value="StartTrip" onclick="submitPage();" />
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                                
                            </center>
                            <!--                   <center>
                                                 
                                                    <input type="button" class="btn btn-success" name="StartTrip" style="width:100px;height:25px;padding:2px; " value="StartTrip" onclick="submitPage();" />
                                                   
                                                    
                                                    <input type="button" class="button" name="Save" value="Save" id="save" onclick="submitPage();" />
                                                    <input type="button" class="button" name="StartTrip" value="StartTrip" id="StartTrip" onclick="submitPage();" />
                                                </center>-->
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>



                        <div id="tripDetail" class="tab-pane">
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr id="index" height="30">
                                    <td  colspan="6" >Trip Details</td>
                                </tr>

                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">


                                        <tr>
                                            <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                        <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                            <td class="text1">Consignment No(s)</td>
                                            <td class="text1">
                                                <c:out value="${trip.cNotes}" />
                                                <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                            </td>
                                            <td class="text1">Billing Type</td>
                                            <td class="text1">
                                                <c:out value="${trip.billingType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <!--                            <td class="text2">Customer Code</td>
                                                                        <td class="text2">BF00001</td>-->
                                            <td class="text2">Customer Name</td>
                                            <td class="text2">
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                            </td>
                                            <td class="text2">Customer Type</td>
                                            <td class="text2" colspan="3" >
                                                <c:out value="${trip.customerType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text1">Route Name</td>
                                            <td class="text1">
                                                <c:out value="${trip.routeInfo}" />
                                            </td>
                                            <!--                            <td class="text1">Route Code</td>
                                                                        <td class="text1" >DL001</td>-->
                                            <td class="text1">Reefer Required</td>
                                            <td class="text1" >
                                                <c:out value="${trip.reeferRequired}" />
                                            </td>
                                            <td class="text1">Order Est Weight (MT)</td>
                                            <td class="text1" >
                                                <c:out value="${trip.totalWeight}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Vehicle Type</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTypeName}" />
                                            </td>
                                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleNo}" />
                                                <input type="hidden" name="vehicleno" value="<c:out value="${trip.vehicleNo}" />"/>
                                                <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                                            </td>
                                            <td class="text2">Vehicle Capacity (MT)</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTonnage}" />

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text1">Veh. Cap [Util%]</td>
                                            <td class="text1">
                                                <c:out value="${trip.vehicleCapUtil}" />
                                            </td>
                                            <td class="text1">Special Instruction</td>
                                            <td class="text1">-</td>
                                            <td class="text1">Trip Schedule</td>
                                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                                                <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                                                <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                                        </tr>


                                        <tr>
                                            <td class="text2">Driver </td>
                                            <td class="text2" >
                                                <c:out value="${trip.driverName}" />
                                            </td>
                                            <td class="text2">Trailer </td>
                                            <td class="text2" colspan="3" >
                                                <c:out value="${trip.trailerNo}" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text1">Product Info </td>
                                            <td class="text1" colspan="5" >
                                                <c:out value="${trip.productInfo}" />
                                            </td>

                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>
                            <br/>

                            <br/>
                            <c:if test = "${expiryDateDetails != null}" >
                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr id="index" height="30">
                                        <td colspan="4" >Vehicle Compliance Check</td>
                                    </tr>
                                    <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                        <tr>
                                            <td class="text2">Vehicle FC Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Vehicle Permit Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Road Tax Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                <br/>
                                <br/>
                            </c:if>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                        </div>
                        <div id="routeDetail" class="tab-pane">

                            <c:if test = "${tripPointDetails != null}" >
                                <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30">
                                        <td  height="30" >S No</td>
                                        <td  height="30" >Point Name</td>
                                        <td  height="30" >Type</td>
                                        <td  height="30" >Route Order</td>
                                        <td  height="30" >Address</td>
                                        <td  height="30" >Planned Date</td>
                                        <td  height="30" >Planned Time</td>

                                    </tr>
                                    <% int index2 = 1; %>
                                    <c:forEach items="${tripPointDetails}" var="tripPoint">
                                        <%
                                                       String classText1 = "";
                                                       int oddEven = index2 % 2;
                                                       if (oddEven > 0) {
                                                           classText1 = "text1";
                                                       } else {
                                                           classText1 = "text2";
                                                       }
                                        %>
                                        <tr >
                                            <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                            <td class="<%=classText1%>" height="30" >
                                                <c:if test = "${tripPoint.pointSequence == '1'}" >
                                                    <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                                </c:if>
                                            </td>

                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>

                                <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>
                                                <td class="text1" width="150"> Estimated KM</td>
                                                <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                                <td class="text1" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text2" width="150"> Estimated Reefer Hour</td>
                                                <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                <td class="text2" colspan="4">&nbsp;</td>
                                            </tr>

                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br/>
                                <br/>
                            </c:if>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                        </div>
                        <!--                <div id="preStart">
                        
                        <c:if test = "${tripPreStartDetails != null}" >
                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                            <td class="contenthead" colspan="4" >Trip Pre Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                              <td class="text1" height="30" >Trip Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                            <tr>
                                <td class="text2" height="30" >Trip Pre Start Odometer Reading</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                <c:if test = "${tripDetails != null}" >
                                        <td class="text2">Trip Pre Start Location / Distance</td>
                                    <c:forEach items="${tripDetails}" var="trip">
                            <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:forEach>
                                </c:if>
                                </tr>
                                <tr>
                                        <td class="text1" height="30" >Trip Pre Start Remarks</td>
                                        <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                        </c:if>
                        <br>
                        <br>
                    </div>-->
                        <div id="statusDetail" class="tab-pane">
                            <% int index1 = 1; %>

                            <c:if test = "${statusDetails != null}" >
                                <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                    <tr id="index" height="30">
                                        <td  height="30" >S No</td>
                                        <td  height="30" >Status Name</td>
                                        <td  height="30" >Remarks</td>
                                        <td  height="30" >Created User Name</td>
                                        <td  height="30" >Created Date</td>
                                    </tr>
                                    <c:forEach items="${statusDetails}" var="statusDetails">
                                        <%
                                               String classText = "";
                                               int oddEven1 = index1 % 2;
                                               if (oddEven1 > 0) {
                                                   classText = "text1";
                                               } else {
                                                   classText = "text2";
                                               }
                                        %>
                                        <tr >
                                            <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                            <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>
                                <br/>
                                <br/>

                            </c:if>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                            <br>
                        </div>

                        <!--                    Fuel Change-->
                        <%-- <div id="bunkDetail">
                                                                    
                                                                        
                                                                            

                                                        <table style="width: 70%; left: 30px;" class="border" cellpadding="0"  cellspacing="0" align="center" border="0" id="fuelTBL" name="fuelTBL">
                                                            <tr >
                                                                <th  >S No</th>
                                                                <th  >Bunk Name</th>
                                                                <th >Slip No</th>
                                                                <th >Date</th>
                                                                <th >Ltrs</th>
                                                                <th >Amount</th>
                                                                <th >Person</th>
                                                                <th >Remarks</th>
                                                                <th >&nbsp;</th>
                                                            </tr>
                                                            <tr >
                                                                <td> 1</td>
                                                                <td>
                                                                    <!--<input name="bunkName" type="text" class="textbox" id="bunkName"  />-->
                                                                    <select style="width:130px;" id='bunkName1' name='bunkName' >
                                                                        <option selected  value="0">---Select---</option>
                                                                        <c:if test = "${bunkList != null}" >
                                                                            <c:forEach items="${bunkList}" var="bunk">
                                                                                <option  value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                                                </c:forEach >
                                                                            </c:if>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input name="slipNo" type="text" class="textbox" id="slipNo" value="" />
                                                                </td>

                                                                <td>
                                                                    <input name="bunkPlace" type="hidden" class="textbox" id="bunkPlace"  />
                                                                    <input name="fuelDate" id="fuelDate" type="text" class="datepicker1" value="" id="fuelDate"  style="width:120px;" />
                                                                </td>
                                                                <td>
                                                                    <input name="fuelLtrs" type="text" class="textbox" value="0" id="fuelLtrs1"  onblur="sumFuel();" onchange="fuelPrice(1);" style="width:80px;" />
                                                                </td>
                                                                <td>
                                                                    <input name="fuelAmount" readonly type="text" class="textbox" value="0" id="fuelAmount1"  onblur="sumFuel();"  style="width:80px;" readonly />
                                                                </td>

                                                                <td>
                                                                    <input name="fuelFilledName" type="text" class="textbox" id="fuelFilledName" value="<%= session.getAttribute("userName")%>" />
                                                                    <input name="fuelFilledBy" type="hidden" class="textbox" id="fuelFilledBy" value="<%= session.getAttribute("userId")%>" />
                                                                </td>
                                                                <td>
                                                                    <textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10'></textarea>
                                                                    <input type="hidden" name="HfId" id="HfId" />
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="8" align="center">
                                                                    <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="button" />
                                                                    <input type="hidden" name="totalFuelLtrs" id="totalFuelLtrs" value=""/>
                                                                    <input type="hidden" name="totalKms" id="totalKms" value=""/>
                                                                    <input type="hidden" name="totalFuelAmount" id="totalFuelAmount" value=""/>
                                                                </td>
                                                            </tr>
                                                            <script>
                                                                var poItems = 0;
                                                                var rowCount='';
                                                                var sno='';
                                                                var snumber = '';

                                                                function addFuelRow()
                                                                {
                                                                    var currentDate = new Date();
                                                                    var day = currentDate.getDate();
                                                                    var month = currentDate.getMonth() + 1;
                                                                    var year = currentDate.getFullYear();
                                                                    var myDate= day + "-" + month + "-" + year;

                                                                    if(sno < 9){
                                                                        sno++;
                                                                        var tab = document.getElementById("fuelTBL");
                                                                        var rowCount = tab.rows.length;

                                                                        snumber = parseInt(rowCount)-1;
                                                                        //                    if(snumber == 1) {
                                                                        //                        snumber = parseInt(rowCount);
                                                                        //                    }else {
                                                                        //                        snumber++;
                                                                        //                    }

                                                                        var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                                        newrow.height="30px";
                                                                        // var temp = sno1-1;
                                                                        var cell = newrow.insertCell(0);
                                                                        var cell0 = "<td><input type='hidden'  name='fuelItemId' /> "+snumber+"</td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;

                                                                        cell = newrow.insertCell(1);
                                                                        //                                        cell0 = "<td class='text2'><input name='bunkName' type='text' class='textbox'     id='bunkName' class='Textbox' /></td>";
                                                                        cell0 = "<td class='text2'><select class='textbox' style='width:100px;' id='bunkName"+snumber+"' style='width:123px'  name='bunkName'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;

                                                                        cell = newrow.insertCell(2);
                                                                        cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate"+snumber+"' type='text' class='datepicker1' style='width:120px;' value='"+myDate+"' class='Textbox' /></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;

                                                                        cell = newrow.insertCell(3);
                                                                        cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice("+snumber+");' type='text' class='textbox' id='fuelLtrs"+snumber+"' value='0' style='width:80px;' class='Textbox' /></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML     = cell0;

                                                                        cell = newrow.insertCell(4);
                                                                        cell0 = "<td class='text1'><input name='fuelAmount' readonly onBlur='sumFuel();' type='text' class='textbox' value='0' style='width:80px;' id='fuelAmount"+snumber+"' class='Textbox' /></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;



                                                                        cell = newrow.insertCell(5);
                                                                        cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden' class='textbox' id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' /><input name='fuelFilledName' type='text' style='width:55px;' class='textbox' id='fuelFilledName' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;

                                                                        cell = newrow.insertCell(6);
                                                                        cell0 = "<td class='text1'><textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10'></textarea></td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell0;

                                                                        cell = newrow.insertCell(7);
                                                                        var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'/> </td>";
                                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                                        cell.innerHTML = cell1;
                                                                        // rowCount++;


                                                                        $( ".datepicker" ).datepicker({
                                                                            /*altField: "#alternate",
                                                                            altFormat: "DD, d MM, yy"*/
                                                                            changeMonth: true,changeYear: true
                                                                        });
                                                                    }
                                                                }


                                                                function delFuelRow() {
                                                                    try {
                                                                        var table = document.getElementById("fuelTBL");
                                                                        rowCount = table.rows.length-1;
                                                                        for(var i=2; i<rowCount; i++) {
                                                                            var row = table.rows[i];
                                                                            var checkbox = row.cells[7].childNodes[0];
                                                                            if(null != checkbox && true == checkbox.checked) {
                                                                                if(rowCount <= 1) {
                                                                                    alert("Cannot delete all the rows");
                                                                                    break;
                                                                                }
                                                                                table.deleteRow(i);
                                                                                rowCount--;
                                                                                i--;
                                                                                sno--;
                                                                                // snumber--;
                                                                            }
                                                                        }sumFuel();
                                                                    }catch(e) {
                                                                        alert(e);
                                                                    }
                                                                }

                                                         function fuelPrice(rowVal)
                                                                        {
                                                                        //  alert("----->"+rowVal);
                                                                        var fuelRate = document.getElementById('bunkName'+rowVal).value;
                                                                        //alert("=====>"+fuelRate);
                                                                        var temp = fuelRate.split('~');
                                                                        fuelRate = temp[3];
                                                                      //  alert(fuelRate);

                                                                        var fuelPrice = fuelRate;
                                                                        var fuelLters = document.getElementById("fuelLtrs"+rowVal).value;
                                                                        //  alert("fuelLters==>"+fuelLters);
                                                                        var fuelAmount = (fuelPrice * fuelLters).toFixed(2);
                                                                        //  alert("fuelAmount==>"+fuelAmount);
                                                                        //    document.tripSheet.fuelAmount.value = fuelAmount;
                                                                        document.getElementById("fuelAmount"+rowVal).value=fuelAmount;
                                                                        // sumFuel();
                                                                        }
                                                                function sumFuel(){
                //                                                    alert("this s ctesting");
                                                                    var totFuel=0;
                                                                    var totltr=0;
                                                                    var totAmount=0;
                                                                    var totAmt=0;
                                                                    totFuel= document.getElementsByName('fuelLtrs');

                                                                    totAmount= document.getElementsByName('fuelAmount');
                                                                    for(i=0;i<totFuel.length;i++){
                                                                        totltr=+totltr.toFixed(2) + +totFuel[i].value ;
                                                                    }
                                                                    document.getElementById('totalFuelLtrs').value=totltr.toFixed(2);
                                                                    for(i=0;i<totAmount.length;i++){
                                                                        totAmt=+totAmt.toFixed(2) + +totAmount[i].value ;
                                                                    }
                                                                    document.getElementById('totalFuelAmount').value= totAmt.toFixed(2);
                                                                   // sumExpenses();
                                                                    //setBalance();
                                                                }


                                                                function totalKmsFunc(){
                                                                    //document.getElementById('totalKms').value=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                                                    document.getElementById('totalKms').value = 0;
                                                                }

                                                                        function sumExpenses(){
                                                                            var sumAmt=0;
                                                                            sumAmt= parseInt(document.getElementById('totalAllowance').value)+parseInt(document.getElementById('totalFuelAmount').value);
                                                                            document.getElementById('totalExpenses').value=parseInt(sumAmt);
                                                                        }
                                                                        function setBalance(){
                                                                            var sumAmt=0;
                                                                            sumAmt= parseInt(document.getElementById('totalAllowance').value)-parseInt(document.getElementById('totalFuelAmount').value);
                                                                            document.getElementById('balanceAmount').value=parseInt(sumAmt);
                                                                        }

                                                                       
                                                            </script>

                                                        </table>		                                    
                                                
                                            
                                        <br>
                                        <br>
                                   
                    <br>
                                        
                        </div>    --%>
                        <!--Fuel Change-->


                        <c:if test="${tripAdvanceDetails != null}">
                            <div id="advance" class="tab-pane">
                                <c:if test="${tripAdvanceDetails != null}">
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr id="index" height="30">
                                            <td  width="30">Sno</td>
                                            <td  width="90">Advance Date</td>
                                            <td  width="90">Trip Day</td>
                                            <td  width="120">Estimated Advance</td>
                                            <td  width="120">Requested Advance</td>
                                            <td  width="90"> Type</td>
                                            <td  width="120">Requested By</td>
                                            <td  width="120">Requested Remarks</td>
                                            <td  width="120">Approved By</td>
                                            <td  width="120">Approved Remarks</td>
                                            <td  width="120">Paid Advance</td>
                                        </tr>
                                        <%int index7=1;%>
                                        <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                            <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                            <%
                                                   String classText7 = "";
                                                   int oddEven7 = index7 % 2;
                                                   if (oddEven7 > 0) {
                                                       classText7 = "text1";
                                                   } else {
                                                       classText7 = "text2";
                                                   }
                                            %>
                                            <tr>
                                                <td class="<%=classText7%>"><%=index7++%></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                                <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                    <td class="<%=classText7%>">Adhoc</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                    <td class="<%=classText7%>">Batch</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                    <td class="<%=classText7%>">Manual</td>
                                                </c:if>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                            </tr>
                                        </c:forEach>

                                        <tr></tr>
                                        <tr>

                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1">&nbsp;</td>
                                            <td class="text1"></td>
                                            <td class="text1"></td>
                                            <td class="text1" colspan="2">Total Advance Paid</td>
                                            <td class="text1"><c:out value="${totalAdvancePaid}"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <c:if test="${tripAdvanceDetailsStatus != null}">
                                        <center style="color:black;font-size: 14px;">
                                            Advance Approval Status Details
                                        </center>
                                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <!--                           <tr id="index" height="30">
                                                                        <td  colspan="13" > Advance Approval Status Details</td>
                                                                    </tr>-->
                                            <tr id="index" height="30">
                                                <td  width="30">Sno</td>
                                                <td  width="90">Request Date</td>
                                                <td  width="90">Trip Day</td>
                                                <td  width="120">Estimated Advance</td>
                                                <td  width="120">Requested Advance</td>
                                                <td  width="90"> Type</td>
                                                <td  width="120">Requested By</td>
                                                <td  width="120">Requested Remarks</td>
                                                <td  width="120">Approval Status</td>
                                                <td  width="120">Paid Status</td>
                                            </tr>
                                            <%int index13=1;%>
                                            <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                <%
                                                       String classText13 = "";
                                                       int oddEven11 = index13 % 2;
                                                       if (oddEven11 > 0) {
                                                           classText13 = "text1";
                                                       } else {
                                                           classText13 = "text2";
                                                       }
                                                %>
                                                <tr>

                                                    <td class="<%=classText13%>"><%=index13++%></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                    <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                        <td class="<%=classText13%>">Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                        <td class="<%=classText13%>">Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                        <td class="<%=classText13%>">Manual</td>
                                                    </c:if>

                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                    <td class="<%=classText13%>">
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                            &nbsp
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                            Request Approved
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                            Request Rejected
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                            Approval in  Pending
                                                        </c:if>
                                                        &nbsp;</td>
                                                    <td class="<%=classText13%>">
                                                        <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                            Yet To Pay
                                                        </c:if>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                        <br/>
                                        <br/>

                                        <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr  height="30">
                                                    <td  colspan="13" > 
                                                <center style="color:black;font-size: 14px;">
                                                    Old Vehicle Advance Details
                                                </center></td>
                                                </tr>
                                                <tr id="index" height="30">
                                                    <td  width="30">Sno</td>
                                                    <td  width="90">Advance Date</td>
                                                    <td  width="90">Trip Day</td>
                                                    <td  width="120">Estimated Advance</td>
                                                    <td  width="120">Requested Advance</td>
                                                    <td  width="90"> Type</td>
                                                    <td  width="120">Requested By</td>
                                                    <td  width="120">Requested Remarks</td>
                                                    <td  width="120">Approved By</td>
                                                    <td  width="120">Approved Remarks</td>
                                                    <td  width="120">Paid Advance</td>
                                                </tr>
                                                <%int index17=1;%>
                                                <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                    <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                    <%
                                                           String classText17 = "";
                                                           int oddEven17 = index17 % 2;
                                                           if (oddEven17 > 0) {
                                                               classText17 = "text1";
                                                           } else {
                                                               classText17 = "text2";
                                                           }
                                                    %>
                                                    <tr>
                                                        <td class="<%=classText17%>"><%=index7++%></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                        <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                            <td class="<%=classText17%>">Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                            <td class="<%=classText17%>">Batch</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                            <td class="<%=classText17%>">Manual</td>
                                                        </c:if>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                        <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                    </tr>
                                                </c:forEach>

                                                <tr></tr>
                                                <tr>

                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1">&nbsp;</td>
                                                    <td class="text1"></td>
                                                    <td class="text1"></td>
                                                    <td class="text1" colspan="2">Total Advance Paid</td>
                                                    <td class="text1"><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <br/>

                                </c:if>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                        </c:if>






                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>
                        <script type="text/javascript">
                            function setTripActualDate(tripSatrtDate) {
                                document.getElementById("startDate").value = tripSatrtDate;
                            }
                            function setTimeHour(hours) {
                                alert(hours);
                                if(parseInt(hours) < 10 && hours != ''){
                                    hours = '0' + hours;
                                }else{
                                    hours = "00";
                                }
                                document.getElementById("tripStartHour").value = hours;

                            }
                            function setTimeMinute(minute) {
                                alert(minute);
                                if(parseInt(minute) < 10 && minute != ''){
                                    minute = '0' + minute;
                                }else{
                                    minute = "00";
                                }
                                document.getElementById("tripStartMinute").value = minute;

                            }
                            var currentDate = new Date();

                            var day = currentDate.getDate();
                            var month = currentDate.getMonth() + 1;
                            if (month == 1) {
                                month = "01"
                            }
                            if (month == 2) {
                                month = "02"
                            }
                            if (month == 3) {
                                month = "03"
                            }
                            if (month == 4) {
                                month = "04"
                            }
                            if (month == 5) {
                                month = "05"
                            }
                            if (month == 6) {
                                month = "06"
                            }
                            if (month == 7) {
                                month = "07"
                            }
                            if (month == 8) {
                                month = "08"
                            }
                            if (month == 9) {
                                month = "09"
                            }
                            var year = currentDate.getFullYear();
                            var myDate = day + "-" + month + "-" + year;

    //                                           

                            document.getElementById("fuelDate").value = myDate;
                        </script>
                    </div>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>