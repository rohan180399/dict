<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import=" java. util. * "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script>
    function calculateTransitTime(sno) {

            var averageKM = document.getElementById('averageKM').value;
            var travelKm = document.getElementById('travelKm'+ sno).value;
            var travelHour = document.getElementById('travelHour'+ sno).value;
            var travelMinute = document.getElementById('travelMinute'+ sno).value;
            var transitTime =(parseFloat(travelKm)/parseFloat(averageKM)).toFixed(2);
            var transitTimeNew = parseInt(transitTime);
            document.getElementById('travelHour'+ sno).value = transitTimeNew;
            document.getElementById('travelMinute'+ sno).value = ((transitTime - transitTimeNew)*60).toFixed(0);

            calcTravelKm();

        }
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";

    function addRow1() {
          var routeSize1 = document.getElementById('contractRouteDetailsSize').value;
            if (routeSize1 > 0) {
            sno1 = parseInt(routeSize1);
            routeSize = parseInt(routeSize1);
        }
        else {
            routeSize1++;
            sno1 = parseInt(routeSize1);
        }
        if (parseInt(rowCount1) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }
        sno1++;
        var tab = document.getElementById("addRoute");
        //find current no of rows
        var rowCountNew = document.getElementById('addRoute').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);

        cell = newrow.insertCell(0);
        var cell0 = "<td class='text1' height='25' style='width:10px;'> " + sno1 + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        if(sno1 == 1){
        cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='30' > <input type='text' id='pointType" + sno + "' name='pointType' value='PickUp'  readonly class='form-control' style='width:50px;' /></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell0 = "<td class='text1' height='30' ><input type='hidden' id='cityId" + sno + "' name='cityId' class='form-control' ><input type='text' id='cityName" + sno + "' name='cityName' class='form-control'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        callAjax(sno);

        cell = newrow.insertCell(3);
        var cell0 = "<td class='text1' height='30' ><textarea type='text' id='pointAddresss" + sno + "' name='pointAddresss'  class='form-control' ></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(4);
        var cell0 = "<td class='text1' height='30'><input type='text' id='pointSequence" + sno + "' name='pointSequence' class='form-control' value='" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(5);
        var cell0 = "<td class='text1' height='30'><input type='text' id='latitude" + sno + "' name='latitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(6);
        var cell0 = "<td class='text1' height='30'><input type='text' id='longitude" + sno + "' name='longitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(7);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelKm" + sno + "' name='travelKm' onKeyUp='calculateTransitTime(" + sno + ");' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelKm();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(8);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelHour" + sno + "' name='travelHour' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcTravelHour();calcReferHr();calcReferMin();calcReferWaitMinute();calcTotalRefer();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(9);
        var cell0 = "<td class='text1' height='30'><input typ<e='text' id='travelMinute" + sno + "' name='travelMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcTravelMinute();'>"+
            "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'>"+
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;


        cell = newrow.insertCell(10);
        var cell0 = "<td class='text1' height='30'><input type='text' id='waitMinute" + sno + "' name='waitMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcWaitMinute();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        }else{
        cell = newrow.insertCell(1);
        var cell0 = "<td class='text1' height='30' ><input type='hidden' id='updateValue" + sno + "' name='updateValue' value='1' > <input type='text' id='pointType" + sno + "' name='pointType' value='Drop'  readonly class='form-control' style='width:50px;' /> <input type='hidden' id='activeStatus" + sno + "' name='activeStatus' value='Y' style='width:80px;' > </td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell0 = "<td class='text1' height='30' ><input type='hidden' id='cityId" + sno + "' name='cityId' class='form-control' ><input type='text' id='cityName" + sno + "' name='cityName' class='form-control' style='width:90px;' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        callAjax(sno);

        cell = newrow.insertCell(3);
        var cell0 = "<td class='text1' height='30' ><textarea type='text' id='pointAddresss" + sno + "' name='pointAddresss'  class='form-control' style='width : 150px' ></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(4);
        var cell0 = "<td class='text1' height='30'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' id='pointSequence" + sno + "' name='pointSequence' class='form-control' value='" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(5);
        var cell0 = "<td class='text1' height='30'><input type='text' id='latitude" + sno + "' name='latitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell0;

        cell = newrow.insertCell(6);
        var cell0 = "<td class='text1' height='30'><input type='text' id='longitude" + sno + "' name='longitude' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(7);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelKm" + sno + "' name='travelKm' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onKeyUp='calculateTransitTime(" + sno + ");'onkeyup='calcTravelKm();' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(8);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelHour" + sno + "' name='travelHour' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelHour();calcReferHr();calcReferMin();calcReferWaitMinute();calcTotalRefer();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(9);
        var cell0 = "<td class='text1' height='30'><input type='text' id='travelMinute" + sno + "' name='travelMinute' class='form-control' value='' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcTravelMinute();'>"+
            "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'>"+
                    "<input type='hidden' id='reeferMinute" + sno + "' name='reeferMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' readonly onkeyup='calcReeferMinute();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;


        cell = newrow.insertCell(10);
        var cell0 = "<td class='text1' height='30'><input type='text' id='waitMinute" + sno + "' name='waitMinute' class='form-control' value='0' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:40px;' onkeyup='calcWaitMinute();'></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        }
        rowCount1++;
        sno++;
        document.getElementById('contractRouteDetailsSize').value++;
    }

//    function updatePointSequence(sno){
//        var pointSequence = document.getElementsByName("pointSequence");
//        alert(pointSequence.length);
//        for(var i = 0; i<pointSequence.length; i++){
//          if((document.getElementById("pointSequence"+i).value > 1) && (i < pointSequence.length)){
//             document.getElementById("pointSequence"+i).value = i+1; 
//            alert(i);
//          }else{
//          }  
//        }
//    }

    function calcTravelKm(){
        var travelKm = document.getElementsByName('travelKm');
        var distance = 0;
        for(var i = 0; i<travelKm.length; i++ ){
             if(travelKm[i].value != ''){
                distance += parseInt(travelKm[i].value);
             }
        }
        document.getElementById('distance').value = distance;
        if(distance != ''){
        calcFuleCostPerKm();
        }
    }


   var totalhrs=0;
   function calcReferHr() {
       var pointtype=document.getElementsByName('pointType');
       
        var travelHrs = document.getElementsByName('travelHour');
        var totalHrs = 0;
        
        
        for (var i = 0; i < travelHrs.length; i++) {
             if (travelHrs[i].value != '' & pointtype[i].value==='Drop' ) {
                totalHrs += parseFloat(travelHrs[i].value);
            }
    
        }
        totalHrs = parseFloat(totalHrs) ;
//        alert("totalHrs"+totalHrs);
        document.getElementById('Reeferhr').value = totalHrs ;
    
    }
    
    function calcReferMin() {

        var travelMins = document.getElementsByName('travelMinute');
         var pointtype=document.getElementsByName('pointType');
        var totalMins = 0;


        for (var i = 0; i < travelMins.length; i++) {
            if (travelMins[i].value != ''& pointtype[i].value==='Drop') {
                totalMins += parseFloat(travelMins[i].value);
            }
        }
       
        

        document.getElementById('Reefermin').value = totalMins;
        
    }
    function calcReferWaitMinute() {
        var waitMins = document.getElementsByName('waitMinute');
        var pointtype=document.getElementsByName('pointType');
        var totalMins = 0;
        for (var i = 0; i < waitMins.length; i++) {
            if (waitMins[i].value != ''& pointtype[i].value==='Drop') {
                totalMins += parseInt(waitMins[i].value);
            }
        }
        document.getElementById('totalReferWaitMinutes').value = totalMins;
       
    }
     function calcTotalRefer(){
         var totalreferhr=document.getElementById('Reeferhr').value;
         var totalrefermin=document.getElementById('Reefermin').value;
         var totalreferwait=document.getElementById('totalReferWaitMinutes').value;
         
         var total1 = totalreferhr * 60  ;
         var total2 = parseInt(totalrefermin) + parseInt(totalreferwait) ;
         
         document.getElementById('totalRefer').value =parseInt((total1 + total2)/2);
     }

    function calcTravelHour() {
        var travelHrs = document.getElementsByName('travelHour');
        var totalHrs = 0;
        for (var i = 0; i < travelHrs.length; i++) {
            if (travelHrs[i].value != '') {
                totalHrs += parseFloat(travelHrs[i].value);
            }
        }
        totalHrs = parseFloat(totalHrs) ;
        document.getElementById('totalHours').value = totalHrs + totalhrs ;
        
    }
    function calcTravelMinute() {

        var travelMins = document.getElementsByName('travelMinute');
        var totalMins = 0;


        for (var i = 0; i < travelMins.length; i++) {
            if (travelMins[i].value != '') {
                totalMins += parseFloat(travelMins[i].value);
            }
        }
        var temp=totalMins;
        if(totalMins >= 60)
        {
             totalhrs= Math.floor(temp / 60);
             totalMins=(totalMins % 60);
        }


        document.getElementById('totalMinutes').value = totalMins;

calcTravelHour();
    }
   /* function calcTravelHour(){
        var travelHrs = document.getElementsByName('travelHour');
        var totalHrs = 0;
        for(var i = 0; i<travelHrs.length; i++ ){
            if(travelHrs[i].value != ''){
            totalHrs += parseFloat(travelHrs[i].value);
            }
        }
        totalHrs = parseFloat(totalHrs) +  parseFloat(document.getElementById('preCoolingHr').value) + parseFloat(document.getElementById('loadingHr').value);
        document.getElementById('totalHours').value = totalHrs;
    }
    function calcTravelMinute(){
        var travelMins = document.getElementsByName('travelMinute');
        var totalMins = 0;
        for(var i = 0; i<travelMins.length; i++ ){
            if(travelMins[i].value != ''){
                totalMins += parseInt(travelMins[i].value);
            }
        }
        document.getElementById('totalMinutes').value = totalMins;
    }  */
    function calcReeferHour(){
        var reeferHrs = document.getElementsByName('reeferHour');
        var totalHrs = 0;
        for(var i = 0; i<reeferHrs.length; i++ ){
             if(reeferHrs[i].value != ''){
                totalHrs += parseInt(reeferHrs[i].value);
            }
        }
        document.getElementById('totalReeferHours').value = totalHrs;
        if(totalHrs != ''){
            calcReferHours();
        }
    }
    function calcReeferMinute(){
        var reeferMins = document.getElementsByName('reeferMinute');
        var totalMins = 0;
        for(var i = 0; i<reeferMins.length; i++ ){
            if(reeferMins[i].value != ''){
                totalMins += parseInt(reeferMins[i].value);
            }
        }
        document.getElementById('totalReeferMinutes').value = totalMins;
        if(totalMins != ''){
            calcReferHours();
        }
    }
    function calcWaitMinute(){
        var waitMins = document.getElementsByName('waitMinute');
        var totalMins = 0;
        for(var i = 0; i<waitMins.length; i++ ){
            if(waitMins[i].value != ''){
                totalMins += parseInt(waitMins[i].value);
            }
        }
        document.getElementById('totalWaitMinutes').value = totalMins;
    }



    function callAjax(val) {
        // Use the .autocomplete() method to compile the list based on input from user
        //alert(val);
        var pointName = 'cityName' + val;
        var pointId = 'cityId' + val;
        var address = 'pointAddresss' + val;
        var latitude = 'latitude' + val;
        var longitude = 'longitude' + val;
        //alert(prevPointId);
        $('#' + pointName).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerPoints.do",
                    dataType: "json",
                    data: {
                        cityName: request.term,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {

                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                var pointType = ui.item.pointType;
                var pointAddress = ui.item.pointAddress;
                var latitudeVal = ui.item.latitude;
                var longitudeVal = ui.item.longitude;
                //alert(latitudeVal+" : "+longitudeVal);

                $('#' + pointName).val(value);
                $('#' + pointId).val(id);
                $('#' + address).val(pointAddress);
                $('#' + latitude).val(latitudeVal);
                $('#' + longitude).val(longitudeVal);
                //validateRoute(val,value);
                 //addRowFunction();
                $('#' + address).focus();
                return false;
            }

            // Format the list menu output of the autocomplete
        }).data("autocomplete")._renderItem = function(ul, item) {
            //alert(item);
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a><table width='100%' border='0'><tr><td width='60px'>"+itemVal+"</td></tr></table></a>")
                    .appendTo(ul);
        };


    }

  function addRowFunction() {

        var rowCountNew = document.getElementById('addRoute').rows.length;
        //alert(rowCountNew);
        var rkmph = document.getElementById('averageKM').value;
        if(rkmph == 0 || rkmph == ''){
            alert("please enter average km/h");
            document.getElementById('averageKM').focus();
        }else{
            var cityName = document.getElementsByName("cityName");
            var count = 0;
            var x;
            var r=confirm("Do You Want Add One More Route!");
            //alert(cityName.length);
            if (cityName.length == 1 &&  r == true) {
            addRow1();
            } else {
                for (var i = 0; i < cityName.length; i++) {
                    //alert(cityName[i].value);
                    if (cityName[i].value != '') {
                        count++;
                    } else {
                        count = 0;
                    }
                }
                //alert(count);
                 if (count > 0 &&  r == true) {
                        addRow1();
                    }
            }
        }
    }

    //savefunction
    function submitPage(value) {
        var count1 = 0;
        var count2 = 0;
        if (document.getElementById('routeValidFrom').value == '') {
            alert("select the contract start date");
            document.getElementById('routeValidFrom').focus();
            return;
        }else if (document.getElementById('routeValidTo').value == '') {
            alert("select the contract end date");
            document.getElementById('routeValidTo').focus();
            return;
        }else if (document.getElementById('routeName').value == '') {
            alert("enter the route name");
            document.getElementById('routeName').focus();
            return;
        } else if (document.getElementById('averageKM').value == '') {
            alert("enter the average km per month");
            document.getElementById('averageKM').focus();
            return;
        } else if (document.getElementById('preCoolingHr').value == '') {
            alert("enter the pre cooling hour");
            document.getElementById('preCoolingHr').focus();
            return;
        } else if (document.getElementById('loadingHr').value == '') {
            alert("enter the loading hour");
            document.getElementById('loadingHr').focus();
            return;
        }else{
            count1 = validateRouteDetails();
            if(count1 == 0){
            count2 = validateRouteExpenseDetails();
            }
        }
        if (count1 == 0 && count2 == 0) {
//            document.secondaryRoute.action = '/throttle/saveSecondaryRoute.do';
//            document.secondaryRoute.submit();
            document.secondaryRoute.action = '/throttle/saveEditSecondaryCustomerContract.do';
            document.secondaryRoute.submit();
        }
    }


    function validateRouteDetails(){
        var pointType = document.getElementsByName("pointType");
        var cityId = document.getElementsByName("cityId");
        var cityName = document.getElementsByName("cityName");
        var pointAddresss = document.getElementsByName("pointAddresss");
        var pointSequence = document.getElementsByName("pointSequence");
        var latitude = document.getElementsByName("latitude");
        var longitude = document.getElementsByName("longitude");
        var travelKm = document.getElementsByName("travelKm");
        var travelHour = document.getElementsByName("travelHour");
        var travelMinute = document.getElementsByName("travelMinute");
        var reeferHour = document.getElementsByName("reeferHour");
        var reeferMinute = document.getElementsByName("reeferMinute");
        var waitMinute = document.getElementsByName("waitMinute");
        var a = 0;
        var count = 0;
        for (var i = 0; i < pointType.length; i++) {
            a = i + 1;
            if (cityId[i].value == '' && cityName[i].value == '') {
                alert("Please fill city name for row  " + a);
                cityName[i].focus();
                count = 1;
                return count;
            } else if (cityId[i].value == '' && cityName[i].value != '') {
                alert("Invalid city name for row  " + a);
                cityName[i].focus();
                count = 1;
                return count;    
            }else if (pointAddresss[i].value == '') {
                alert("please fill point address for row " + a);
                pointAddresss[i].focus();
                count = 1;
                return count;
            }else if (pointSequence[i].value == '') {
                alert("please fill point sequence for row " + a);
                pointSequence[i].focus();
                count = 1;
                return count;
            }else if (latitude[i].value == '') {
                alert("please fill point latitude for row " + a);
                latitude[i].focus();
                count = 1;
                return count;
            }else if (longitude[i].value == '') {
                alert("please fill point longitude for row " + a);
                longitude[i].focus();
                count = 1;
                return count;
            }else if (travelKm[i].value == '') {
                alert("please fill point travel km for row " + a);
                travelKm[i].focus();
                count = 1;
                return count;
            }else if (travelHour[i].value == '') {
                alert("please fill point travel hour for row " + a);
                travelHour[i].focus();
                count = 1;
                return count;
            }else if (travelMinute[i].value == '') {
                alert("please fill point travel minute for row " + a);
                travelMinute[i].focus();
                count = 1;
                return count;
            /*}else if (reeferHour[i].value == '') {
                alert("please fill point reefer hour for row " + a);
                reeferHour[i].focus();
                count = 1;
                return count;
            }else if (reeferMinute[i].value == '') {
                alert("please fill point reefer minute for row " + a);
                reeferMinute[i].focus();
                count = 1;
                return count; */
            }else if (waitMinute[i].value == '') {
                alert("please fill point waithing minute for row " + a);
                waitMinute[i].focus();
                count = 1;
                return count;
            }
        }
         return count;

    }
    function validateRouteExpenseDetails() {
        var vehMileage = document.getElementsByName("vehMileage");
        var vehTypeName = document.getElementsByName("vehTypeName");
        var fuelCostPerKms = document.getElementsByName("fuelCostPerKms");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var addlTollAmounts = document.getElementsByName("addlTollAmounts");
        var parkingCost = document.getElementsByName("parkingCost");
        var miscCost = document.getElementsByName("miscCost");
        var totExpense = document.getElementsByName("totExpense");

        var a = 0;
        var count = 0;
        for (var i = 0; i < vehMileage.length; i++) {
            a = i + 1;
            if (fuelCostPerKms[i].value == '') {
                alert("please fill fuel cost per km for row " + vehTypeName[i]);
                fuelCostPerKms[i].focus();
                count = 1;
                return count;
            } else if (tollAmounts[i].value == '') {
                alert("please fill toll rate per km for row " + vehTypeName[i]);
                tollAmounts[i].focus();
                count = 1;
                return count;
            } else if (addlTollAmounts[i].value == '') {
                alert("please fill add toll amount for row " + vehTypeName[i]);
                addlTollAmounts[i].focus();
                count = 1;
                return count;
            } else if (parkingCost[i].value == '') {
                alert("please fill parking toll cost for row " + vehTypeName[i]);
                parkingCost[i].focus();
                count = 1;
                return count;
            } else if (miscCost[i].value == '') {
                alert("please fill misc cost for row " + vehTypeName[i]);
                miscCost[i].focus();
                count = 1;
                return count;
            } else if (totExpense[i].value == '') {
                alert("please fill total expense for row " + vehTypeName[i]);
                totExpense[i].focus();
                count = 1;
                return count;
            }

        }
        return count;
    }



     $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getSecondaryCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    $("#customerId").val(ui.item.Id);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    $('#fixedKmPerMonth').focus();
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };


        });

</script>
<!--    calcWaitMinute();calcTravelMinute();calcTravelHour();calcTravelKm();-->
    <body onload="calcPerDayCng();setRCMValues();calcReferHr();calcReferMin();calcReferWaitMinute();calcTotalRefer();">
        <form name="secondaryRoute"  method="post">
            <br>
            <br>
            <br>



     <c:if test="${contractRouteDetails != null}">
            <c:forEach items="${contractRouteDetails}" var="crd">
            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr class="contenthead" align="left"><td colspan="4">Add Secondary Contract Route Details</td></tr>
                <tr class="text2">
                    <td>Customer Name</td>
                    <td><input type="hidden" name="customerId" id="customerId" value="<c:out value="${crd.customerId}"/>"/><input type="hidden" name="customerName" id="customerName" value="<c:out value="${crd.customerName}"/>" class="textbox"/><c:out value="${crd.customerName}"/></td>
                <!--<td>Route Name</td>-->
                       <input type="hidden" name="secondaryRouteId" id="secondaryRouteId"  value="<c:out value="${crd.secondaryRouteId}"/>" />
                       <td colspan="2">&nbsp;</td>
                  
                 </tr>
                    <input type="hidden" name="fixedKmPerMonth" id="fixedKmPerMonth" value="1000" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                    <input type="hidden" name="fixedReeferHours" id="fixedReeferHours" value="100" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                    <input type="hidden" name="fixedReeferMinutes" id="fixedReeferMinutes" value="10" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>

                <tr class="text1">
                    <td><font color='red'></font>Contract Valid From</td>
                    <td><input type="text" readonly  name="routeValidFrom" id="routeValidFrom" value='<c:out value="${crd.routeValidFrom}"/>' /></td>
                    <td><font color='red'></font>Contract Valid To</td>
                    <td><input type="text" name="routeValidTo" id="routeValidTo" value='<c:out value="${crd.routeValidTo}"/>'/></td>
                </tr>
                <tr class="text2">
                   <td>Current Diesel Cost </td>
                   <td><input type="hidden" name="dieselCost" id="dieselCost"  value="<%=request.getAttribute("currentDieselPrice")%>" readonly/><label><%=request.getAttribute("currentDieselPrice")%></label></td>
                   <td>Current CNG Cost </td>
                   <td><input type="hidden" name="fuelCost" id="fuelCost"  value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
                </tr>
                 <tr>
                    <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                    <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
                </tr>
            </table>
                 </c:forEach>
        </c:if>
            <script>
            function calcPerDayCng(){
              var fuelCost = $("#fuelCost").val();
              var perDayExp = parseFloat(fuelCost) * 1.5;
              $("#perDayAllocation").val(perDayExp);
            }
        </script>
           <table width="980px" align="center" cellpadding="0" cellspacing="0" id="addRoute" class="table2">
           <c:if test="${contractRouteDetails != null}">
            <c:forEach items="${contractRouteDetails}" var="crd">
               <tr class="text1">
                    
                    <td colspan="2" ><font color='red'>*</font>Route Name</td>
                    <td colspan="1" ><input type="text" name="routeName" id="routeName" value="<c:out value="${crd.secondaryRouteName}"/>" class="textbox"/></td>
                    <td colspan="1" ><font color='red'>*</font>Route Ave KM/H</td>
                    <td colspan="1" ><input type="text" name="averageKM" id="averageKM" class="textbox" value="<c:out value="${crd.averageKM}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                    <td colspan="2" >PreCoolingHrs</td>
                    <td colspan="1" ><input type="text" name="preCoolingHr" id="preCoolingHr" style='width:40px;' class="textbox" value="1" readonly /></td>
                    <td colspan="2" >LoadingHrs</td>
                    <td colspan="1" >
                        <input type="text" name="loadingHr" id="loadingHr" style='width:40px;' class="textbox" value="0.5" readonly />
                        <input type="hidden" name="perDayAllocation" id="perDayAllocation" style='width:40px;' class="textbox" value="" readonly />
                    </td>
                </tr>
            </c:forEach>
           </c:if>
            <tr>
                <td class="contenthead" height="30" style="width: 10px;">Sno</td>
                <td class="contenthead" height="30" style="width: 200px;">Point Type</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Point Name</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Address</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Point Sequence</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Latitude</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Longitude</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Km</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Hours</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Travel Minutes</td>
                <td class="contenthead" height="30" style="width: 10px;"><font color='red'>*</font>Wait Minutes</td>
            </tr>
            <%
                       int sno1 = 1;
                        ArrayList positionList = (ArrayList) request.getAttribute("routeContractDetails");
            %>
            <c:if test="${routeContractDetails != null}">
                <c:forEach items="${routeContractDetails}" var="rcd">
                    <tr>
                        <td align="left" style="width:10px;" ><%=sno1%><input type="hidden" id="updateValue" name="updateValue" value="0" >
                            <input type="hidden" name="secondaryRouteDetailId" id="secondaryRouteDetailId"  value="<c:out value="${rcd.secondaryRouteDetailId}"/>" />
                            <input type="hidden" name="contractRouteDetailsSize" id="contractRouteDetailsSize"  value="<c:out value="${routeContractDetailsSize}"/>" /></td>
                        <td>
                            <input type="text" name="pointType" id="pointType" value="<c:out value="${rcd.pointType}"/>"  class="textbox" style="width: 50px"/>&nbsp;
                            <% if(sno1 == 1){ %>
                            <input type="hidden" name="activeStatus" id="activeStatus<%=sno1%>" value="Y" class="textbox" style="width: 80px"/>
                             <% }%>
                            <% if(sno1 != 1 && sno1 != positionList.size()){ %>
                            <select name="activeStatus" id="activeStatus<%=sno1%>" onchange="resetPoint('<%=sno1%>')">
                                <option value="Y" selected>Active</option>
                                <option value="N">In-Active</option>
                            </select>
                            <% }%>
                            <% if(sno1 == positionList.size()){ %>
                            <input type="hidden" name="activeStatus" id="activeStatus<%=sno1%>" value="Y" class="textbox" style="width: 80px"/>
                             <% }%>
                        </td>
                        <td> <input type="hidden" name="cityId" id="cityId<%=sno1%>"  value="<c:out value="${rcd.pointId}"/>" />
                            <input type="text" name="cityName" id="cityName<%=sno1%>" onkeypress="callAjax(<%=sno1%>)" value="<c:out value="${rcd.pointName}"/>"  class="textbox" style="width: 90px"/></td>
                        <td><textarea type="text" name="pointAddresss" id="pointAddresss<%=sno1%>" style="width: 150px"><c:out value="${rcd.pointAddresss}" /></textarea></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;
                            <% if(sno1 == 1){ %>
                            <input type="text" name="pointSequence" id="pointSequence<%=sno1%>" value="<c:out value="${rcd.pointSequence}"/>"  class="textbox" style="width: 40px" readonly/>
                            <%}else{%>
                            <input type="text" name="pointSequence" id="pointSequence<%=sno1%>" value="<c:out value="${rcd.pointSequence}"/>"  class="textbox" style="width: 40px" />
                            <% }%>
                        </td>
                        <td><input type="text" name="latitude" id="latitude<%=sno1%>" value="<c:out value="${rcd.latitude}"/>"  class="textbox" style="width: 40px"/></td>
                        <td><input type="text" name="longitude" id="longitude<%=sno1%>" value="<c:out value="${rcd.longitude}"/>"  class="textbox" style="width: 40px"/></td>
                        <td><input type="text" name="travelKm" id="travelKm<%=sno1%>" value="<c:out value="${rcd.travelKm}"/>" onKeyUp="calculateTransitTime(<%=sno1%>);" class="textbox" style="width: 40px" onkeyup="calcTravelKm();"/></td>
                        <td><input type="text" name="travelHour" id="travelHour<%=sno1%>" value="<c:out value="${rcd.travelHour}"/>"  class="textbox" style="width: 40px" onkeyup="calcTravelHour();"/></td>
                        <td><input type="text" name="travelMinute" id="travelMinute<%=sno1%>" value="<c:out value="${rcd.travelMinute}"/>"  class="textbox" style="width: 40px" onkeyup="calcTravelMinute();"/></td>
                        <td><input type="text" name="waitMinute" id="waitMinute<%=sno1%>" value="<c:out value="${rcd.waitMinute}"/>"  class="textbox" style="width: 40px" onkeyup="calcWaitMinute();"/></td>

                    </tr>
                    <%sno1++;%>
                </c:forEach>
            </c:if>
            <!--                <tr>
                            </tr>-->
            <script>
                function resetPoint(sno){
                  if(document.getElementById("activeStatus"+sno).value == 'N'){
                      document.getElementById("pointSequence"+sno).value= 0;
                      document.getElementById("travelKm"+sno).value= 0;
                      document.getElementById("travelHour"+sno).value= 0;
                      document.getElementById("travelMinute"+sno).value= 0;
                      document.getElementById("waitMinute"+sno).value= 0;
                      document.getElementById("travelKm"+sno).readOnly= true;
                      document.getElementById("travelHour"+sno).readOnly= true;
                      document.getElementById("travelMinute"+sno).readOnly= true;
                      document.getElementById("waitMinute"+sno).readOnly= true;
                      calcTravelKm();  
                      var sno1 = sno;
                      sno1++;
                      while(sno1>sno){
//                          alert(document.getElementById("pointSequence"+sno1).value);
                          if(document.getElementById("pointSequence"+sno1).value != ''){
                              document.getElementById("pointSequence"+sno1).value = parseInt(sno1)-1;
                              sno1++;
                          }
                      }
                  }else{
                     document.getElementById("pointSequence"+sno).value= sno;
                       document.getElementById("travelKm"+sno).readOnly= false;
                      document.getElementById("travelHour"+sno).readOnly= false;
                      document.getElementById("travelMinute"+sno).readOnly= false;
                      document.getElementById("waitMinute"+sno).readOnly= false;
                      calcTravelKm();  
                      var sno1 = sno;
                      sno1++;
                      while(sno){
                          //alert(document.getElementById("pointSequence"+sno1).value);
                          if(document.getElementById("pointSequence"+sno).value != ''){
                              document.getElementById("pointSequence"+sno).value = parseInt(sno);
                              sno++;
                          }
                      }
                  }  
                }
            </script>    
            <center>
                <tr>
                <td colspan="6"  align="left">
                    <a href="" onclick="addRow1();" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Code</span></a>
                </td>
                <td align="right">Total</td>
                <c:if test="${contractRouteDetails != null}">
                    <c:forEach items="${contractRouteDetails}" var="crd">
                        <td><input type="text" name="distance" id="distance" value="<c:out value="${crd.totalTravelKm}"/>" readonly class="textbox" style="width: 40px"/></td>
                        <td><input type="text" name="totalHours" id="totalHours" value="<c:out value="${crd.totalTravelHour}"/>" readonly class="textbox" style="width: 40px"/></td>
                        <td><input type="text" name="totalMinutes" id="totalMinutes" value="<c:out value="${crd.totalTravelMinute}"/>" readonly class="textbox" style="width: 40px"/>
                        <input type="hidden" name="totalReeferHours" id="totalReeferHours" value="<c:out value="${crd.totalReeferHours}"/>" readonly class="textbox" style="width: 40px"/>
                        <input type="hidden" name="totalReeferMinutes" id="totalReeferMinutes" value="<c:out value="${crd.totalReeferMinutes}"/>" readonly class="textbox" style="width: 40px"/></td>
                       
                        
                <input type="hidden" name="Reeferhr" id="Reeferhr" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="Reefermin" id="Reefermin" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="totalReferWaitMinutes" id="totalReferWaitMinutes" value="0" readonly class="textbox" style="width: 40px"/>
                 <input type="hidden" name="totalRefer" id="totalRefer" value="<c:out value="${crd.totalReferMin}"/>" readonly class="textbox" style="width: 40px"/>
                        
                 
                
                        <td><input type="text" name="totalWaitMinutes" id="totalWaitMinutes" value="<c:out value="${crd.totalWaitMinutes}"/>" readonly class="textbox" style="width: 40px"/>

                        </c:forEach></c:if>
                    </td>
                    </tr>

                </center>

            </table>
<!--            <input type="button" name="updateRefer" id="updateRefer" value="updateRefer" onclick="calcReferHr();calcReferMin();calcReferWaitMinute();calcTotalRefer();setRCMValues(); "/>-->

            <br>

            <script type="text/javascript">
                //concard value get from the addrout configDetails

                  function setRCMValues(){
                    var km = document.getElementById("distance").value;
                    var hm = document.getElementById("totalRefer").value;
//                    var fuelDifferCost = document.getElementById("fuelDifferCost").value;
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var fuelCostPerMin = document.getElementsByName('fuelCostPerMin');
                    var tollAmounts = document.getElementsByName('tollAmounts');
                    var addlTollAmounts = document.getElementsByName('addlTollAmounts');
                    var parkingCosts = document.getElementsByName('parkingCost');
                    var miscCosts = document.getElementsByName('miscCost');
                    var totExpenses = document.getElementsByName('totExpense');
                    var fuelTypeId = document.getElementsByName('fuelTypeId');
                    var fuelAmnt = 0;
                    var referAmnt = 0;
                    var increaseAmount = 0;
                    var tollAmount = 0;
                    var addlTollAmount = 0;
                    var addfuleDifferAmount = 0;
                    var miscCost = 0;
                    var totExpense = 0;
                    var parkingCost = 0;
                    var totExpense1 = 0;
                    var totExpense2 = 0;
                    var j = 50;
                    var k = 0;
                    var A = 0;
                    var perDayAllocation = $("#perDayAllocation").val();
                    //alert('am here:'+fuelCostPerKms.length);
                    for (var i = 0; i < fuelCostPerKms.length; i++) {
                        if(km == ''){
                            km = 0;
                        }
                        if (hm == '') {
                        hm = 0;
                    }
                        if(tollAmounts[i].value == ''){
                            tollAmount = 0;
                        }else{
                            tollAmount = tollAmounts[i].value;
                        }
                        if(addlTollAmounts[i].value == ''){
                            addlTollAmount = 0;
                        }else{
                            addlTollAmount = addlTollAmounts[i].value;
                        }
                        if(parkingCosts[i].value == ''){
                            parkingCost = 0;
                        }else{
                            parkingCost = parkingCosts[i].value;
                        }
                        if(miscCosts[i].value == ''){
                            miscCost = 0;
                        }else{
                            miscCost = miscCosts[i].value;
                        }

                        if (fuelCostPerKms[i].value != '') {
                            fuelAmnt = fuelCostPerKms[i].value * km;
                        } else {
                            fuelAmnt = 0 * km;
                        }
                        if (fuelCostPerMin[i].value != '') {
                        referAmnt = fuelCostPerMin[i].value * hm;
                    }
//                        if(fuelDifferCost=='')
//                        {
//                           increaseAmount= 0* 925;
//                           addfuleDifferAmount=increaseAmount;
//                        }
//                        else{
//                            increaseAmount= fuelDifferCost * 925;
//                            addfuleDifferAmount=increaseAmount;
//                        }
                        if(fuelTypeId[i].value == 1002){
                        totExpense1 = parseFloat(fuelAmnt)+ parseFloat(referAmnt) + parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost) + parseFloat(miscCost);
                        }else if(fuelTypeId[i].value == 1003){
                        totExpense1 = parseFloat(fuelAmnt)+ parseFloat(referAmnt) + parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost) + parseFloat(miscCost)+ parseFloat(perDayAllocation);
                        }
//                        totExpense2 = parseFloat(totExpense1) + parseFloat(addfuleDifferAmount) ;
//                        if(totExpense2<50)
//                        {
//                            totExpense=50;
//                        }  
//                        else if(totExpense2<100 && totExpense != '50')
//                        {
//                            totExpense=100;
//                        }else
//                        {
//                            totExpense2=totExpense2;
//                        }
                          k = totExpense1 % j;
//                          alert(k);
                          if(parseInt(k)<50 && parseInt(k)!= '50'){
                               A=50-parseInt(k);
                               totExpense=totExpense1+A;
                          }else {
                            totExpense=totExpense2;
                          }
                         
                        totExpenses[i].value = parseInt(totExpense);
                        
                    }

                }




//                function setRCMValues(){
//                    var km = document.getElementById("distance").value;
//                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
//                    var tollAmounts = document.getElementsByName('tollAmounts');
//                    var addlTollAmounts = document.getElementsByName('addlTollAmounts');
//                    var miscCosts = document.getElementsByName('miscCost');
//                    var parkingCosts = document.getElementsByName('parkingCost');
//                    var totExpenses = document.getElementsByName('totExpense');
//                    var fuelAmnt = 0;
//                    var tollAmount = 0;
//                    var addlTollAmount = 0;
//                    var miscCost = 0;
//                    var parkingCost = 0;
//                    var totExpense = 0;
//                    //alert('am here:'+fuelCostPerKms.length);
//                    for (var i = 0; i < fuelCostPerKms.length; i++) {
//                        if(km == ''){
//                            km = 0;
//                        }
//                        if(tollAmounts[i].value == ''){
//                            tollAmount = 0;
//                        }else{
//                            tollAmount = tollAmounts[i].value;
//                        }
//                        if(addlTollAmounts[i].value == ''){
//                            addlTollAmount = 0;
//                        }else{
//                            addlTollAmount = addlTollAmounts[i].value;
//                        }
//                        if(miscCosts[i].value == ''){
//                            miscCost = 0;
//                        }else{
//                            miscCost = miscCosts[i].value;
//                        }
//                        if(parkingCosts[i].value == ''){
//                            parkingCost = 0;
//                        }else{
//                            miscCost = parkingCosts[i].value;
//                        }
//
//                        if (fuelCostPerKms[i].value != '') {
//                            fuelAmnt = fuelCostPerKms[i].value * km;
//                        } else {
//                            fuelAmnt = 0 * km;
//                        }
//                        totExpense = parseFloat(fuelAmnt) + parseFloat(tollAmount) + parseFloat(addlTollAmount) + parseFloat(parkingCost) + parseFloat(miscCost);
//                        totExpenses[i].value = totExpense.toFixed(2);
//                    }
//
//                }
                function setTollCost(type) {
                    if (type == 1) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var reeferExpense = document.getElementsByName('reeferExpense');
                        var factor = document.getElementsByName('factor');
                        var vehExpense = document.getElementsByName('vehExpense');
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            misCost[i].value = document.getElementById('avgMisCost').value;
                            driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                            factor[i].value = document.getElementById('avgFactor').value;
                            if(km == ''){
                                km = 0;
                            }
                            if (fuelCostPerKms[i].value != '') {
                                var fuelAmnt = fuelCostPerKms[i].value * km;
                            } else {
                                var fuelAmnt = 0 * km;
                            }
                            if (misCost[i].value != '') {
                                var misCostPerKm = misCost[i].value * km;
                            } else {
                                var misCostPerKm = 0 * km;
                            }
                            if (driverIncentive[i].value != '') {
                                var driverIncentivePerKm = driverIncentive[i].value * km;
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            if (fuelCostPerKms[i].value != '') {
                                var total = parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            driverIncentive[i].readOnly = true;
                            misCost[i].readOnly = true;
                            factor[i].readOnly = true;
                             calcVehExp();

                        }
                    }
                }

                //calculate fuelCostPerKm
                function calcFuleCostPerKm() {
                    //alert("am here");
                    var km = document.getElementById("distance").value;
                    var fuelCostPerKm = 0;
                    var fuelCost = document.getElementById('fuelCost').value;
                    var vehMileage = document.getElementsByName('vehMileage');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factor = document.getElementsByName('factor');
                    //alert(km);
                  /*  if (km != '') {
                        for (var i = 0; i < vehMileage.length; i++) {
                            var totFuelCost = parseInt(km) / parseInt(vehMileage[i].value) * parseInt(fuelCost);
                            fuelCostPerKm = parseInt(totFuelCost) / parseInt(km);
                            fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < vehMileage.length; i++) {
                            fuelCostPerKms[i].value = '';
                        }
                    }  */
                    //setTollCost(1);
                    setRCMValues();


                }
                //calculate Vehicle Expenses cost
                function calcVehExp() {
                    var km = document.getElementById('distance').value;
                    var tollAmounts = document.getElementsByName('tollAmounts');
                    var miscCostKm = document.getElementsByName('miscCostKm');
                    var driverIncenKm = document.getElementsByName('driverIncenKm');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factorPerKm = document.getElementsByName('factor');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    if (km != '') {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            var expCost = parseInt(km) * fuelCostPerKms[i].value;
                            if (miscCostKm[i].value != '') {
                                var misCost = miscCostKm[i].value * km;
                            } else {
                                var misCost = 0 * km;
                            }
                            if (driverIncenKm[i].value != '') {
                                var driverIncentive = driverIncenKm[i].value * km;
                            } else {
                                var driverIncentive = 0 * km;
                            }
                            var total = parseFloat(expCost.toFixed(2)) +  parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                            vehExpense[i].value = total.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            vehExpense[i].value = '';
                            fuelCostPerHrs[i].value = '';
                            reeferExpense[i].value = '';
                            totExpense[i].value = '';
                        }
                    }
                     calcTotExp();
                }

                //calculate reefer hours
                  function calcReferHours() {
                   var hour = 0;
                   var minute = 0;
                   hour = document.getElementById('totalReeferHours').value;
                   minute = document.getElementById('totalReeferMinutes').value;
                   var refMinute = 0;
                   if(hour > 0){
                       hour = hour * 60;
                   }
                   if(minute != ''){
                   refMinute = parseInt(hour) + parseInt(minute);
                   }else{
                   refMinute = hour;
                   }
//                   alert("refMinute==="+refMinute);
                    if (refMinute != 00) {
                        calcFuleCostPerHr(refMinute);
                    }
                }

                //calculate fuelCostPerHr
                function calcFuleCostPerHr(refMinute) {
//
                    var fuelCost = document.getElementById('fuelCost').value;
                    var reefMileage = document.getElementsByName('reefMileage');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    for (var i = 0; i < reefMileage.length; i++) {
                        var referExp = reefMileage[i].value * fuelCost;
                        fuelCostPerHrs[i].value = referExp.toFixed(2);
                        if(refMinute != '' && refMinute != 0){
                        var totalReefExp = fuelCostPerHrs[i].value * refMinute;
                        reeferExpense[i].value = (totalReefExp/60).toFixed(2);
                        }
                    }
                    calcTotExp();
                }


                function calcTotExp() {
                    var km = document.getElementById('distance').value;
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    for (var i = 0; i < vehExpense.length; i++) {
                        if (vehExpense[i].value != '') {
                            var vehExp = vehExpense[i].value;
                        } else {
                            var vehExp = 0;
                        }
                        if (reeferExpense[i].value != '') {
                            var refExp = reeferExpense[i].value;
                        } else {
                            var refExp = 0;
                        }
                        var totCost = parseFloat(vehExp) + parseFloat(refExp);
                        totExpense[i].value = totCost.toFixed(2);
                    }
                }

                function calcTollExp(index) {
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if (tollAmounts != '') {
                        var totExp = parseFloat(tollAmounts) + parseFloat(vehExp) + parseInt(reeferExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }
                }
                function calcMiscExp(index) {
                    var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if (miscCostKm != '') {
                        var totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcDriExp(index) {
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if (driverIncenKm != '') {
                        var totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcFactorExp(index) {
                    var factorPerKm = document.getElementById('factor' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if (factorPerKm != '') {
                        var totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }

            </script>
            <br>
            <br>
            <br>
            <br>
            <% int count = 0;%>
              <c:if test="${rateCostDetails != null}">
                <table width="96%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="70">
                            <th><h3>S.No</h3></th>
                            <th><h3>VehicleType </h3></th>
                            <th><h3>FuelCostPerKm</h3></th>
                             <th><h3>FuelCostPerMin</h3></th>
                            <th><h3>FixedTollCost</h3></th>
                            <th><h3>FixedAddlTollCost</h3></th>
                            <th><h3>FixedParkingCost</h3></th>
                            <th><h3>FixedMiscCost</h3></th>
                            <th><h3>Total Expense Cost</h3></th>
                        </tr>
                    </thead>
                    <% int index = 0;
                                int sno = 1;
                    %>
                    <tbody>
                         <c:forEach items="${rateCostDetails}" var="rate">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                             <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>" style="width:200px">
                                    <input type="hidden" name="vehTypeId" id="vehTypeId<%=index%>" value="<c:out value="${rate.vehTypeId}"/>" readonly/>
                                    <input type="hidden" name="fuelTypeId" id="fuelTypeId<%=index%>" value="<c:out value="${rate.fuelTypeId}"/>"/>
                                    <input type="hidden" name="vehTypeName" id="vehTypeName<%=index%>" value="<c:out value="${rate.vehicleTypeName}"/>" readonly/>
                                    <c:out value="${rate.vehicleTypeName}"/>
                                    <input type="hidden" name="secondaryRouteCostId" id="secondaryRouteCostId" value="<c:out value="${rate.secondaryRouteCostId}"/>"/>
                                </td>
                                <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerKms" id="fuelCostPerKms<%=index%>" value="<fmt:formatNumber pattern="##0.00"  value="${rate.fuelPrice / rate.vehicleMileage}"/>"  style="width: 60px" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerMin" id="fuelCostPerMin<%=index%>" value="<fmt:formatNumber pattern="##0.00"  value="${rate.fuelPrice / 60*rate.reeferMileage }"/>"  style="width: 60px" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="tollAmounts" id="tollAmounts<%=index%>" value="<c:out value="${rate.tollAmounts}"/>" style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="addlTollAmounts" id="addlTollAmounts<%=index%>" value="<c:out value="${rate.addlTollAmounts}"/>" style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="parkingCost" id="parkingCost<%=index%>" value="<c:out value="${rate.parkingCost}"/>"   style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="miscCost" id="miscCost<%=index%>" value="<c:out value="${rate.miscCostKm}"/>"   style="width: 60px" onkeyup="setRCMValues();" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="totExpense" id="totExpense<%=index%>" value="0" readonly style="width: 90px"/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>
                            <tr>
                                <td colspan="8" align="center"><input type="button" class="button" name="Save" value="Save" onclick="submitPage();"/></td>
                            </tr>
                    </tbody>
                </table>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");</script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="200">200</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
