
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2YEWWi-juKcJyjyIKB3-8lEw9GZxNiCc&libraries=places"></script>


        <script type="text/javascript">
            var source, destination;
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            google.maps.event.addDomListener(window, 'load', function () {
                new google.maps.places.SearchBox(document.getElementById('googleCityName'));

                directionsDisplay = new google.maps.DirectionsRenderer({'draggable': true});

            });
            function setCityName() {
                var tempdestination = document.getElementById("googleCityName").value;
                var tempdestination1 = tempdestination.split(",");
                document.getElementById("googleCityName").value = tempdestination1[0];
                //alert("11111"+document.getElementById("cityName").value);
                document.getElementById("cityName").value = tempdestination1[0];
                getLatLng();
            }

            function initialize(lat, lng, address) {
                var mapOptions = {
                    center: new google.maps.LatLng(lat, lng),
                    zoom: 4,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("myMap"),
                        mapOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    title: address
                });

                marker.setMap(map);

                var infotext = address + '<hr>' +
                        'Latitude: ' + lat + '<br>Longitude: ' + lng;
                var infowindow = new google.maps.InfoWindow();
                infowindow.setContent(infotext);
                infowindow.setPosition(new google.maps.LatLng(lat, lng));
                infowindow.open(map);
            }

            function getLatLng() {

                var address = document.getElementById("googleCityName").value;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var longaddress = results[0].address_components[0].long_name;
                        var latitute = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        document.getElementById("latitude").value = latitute;
                        document.getElementById("longitute").value = longitude;
                        initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
                    } else {
                    }
                });
            }
        </script>
        <!--FOR google City ends-->

        <script type="text/javascript">

$(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#countryName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCountryName.do",
                    dataType: "json",
                    data: {
                       countryName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                            $('#countryId').val('');
                            $('#countryName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#countryId').val(tmp[0]);
                $('#countryName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        });
//
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#zoneName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getZoneName.do",
                    dataType: "json",
                    data: {
                       zoneName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                            $('#zoneId').val('');
                            $('#zoneName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#zoneId').val(tmp[0]);
                $('#zoneName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        });

            function citysubmit()
            {
                var errStr = "";
                var nameCheckStatus = $("#cityNameStatus").text();
                if (document.getElementById("cityName").value == "") {
                    errStr = "Please enter cityName.\n";
                    alert(errStr);
                    document.getElementById("cityName").focus();
                } else if (nameCheckStatus != "") {
                    if (document.getElementById("editValue").value != "1") {
                        errStr = "City Name Already Exists.\n";
                        alert(errStr);
                        document.getElementById("cityName").focus();
                    }
                } else if (document.getElementById("zoneName").value == "") {
                    errStr = "Please select valid zoneName.\n";
                    alert(errStr);
                    document.getElementById("zoneName").focus();
                } else if (document.getElementById("zoneName").value != "" && document.getElementById("zoneId").value == "") {
                    errStr = "Please select valid zoneName.\n";
                    alert(errStr);
                    document.getElementById("zoneName").focus();
                } else if (document.getElementById("cityCode").value == "") {
                    errStr = "Please enter cityCode.\n";
                    alert(errStr);
                    document.getElementById("cityCode").focus();

                } else if (document.getElementById("countryName").value == "") {
                    errStr = "Please select valid countryName.\n";
                    alert(errStr);
                    document.getElementById("countryName").focus();
                } else if (document.getElementById("countryName").value != "" && document.getElementById("countryId").value == "") {
                    errStr = "Please select valid countryName.\n";
                    alert(errStr);
                    document.getElementById("countryName").focus();
                } else if (document.getElementById("latitude").value == "") {
                    errStr = "Please enter latitude.\n";
                    alert(errStr);
                    document.getElementById("latitude").focus();
                } else if (document.getElementById("longitute").value == "") {
                    errStr = "Please enter longitute.\n";
                    alert(errStr);
                    document.getElementById("longitute").focus();
                }
                if (errStr == "") {
                    document.cityMaster.action = "/throttle/saveCityMaster.do";
                    document.cityMaster.method = "post";
                    document.cityMaster.submit();
                }
            }



            function setValues(sno, cityId, cityName, zoneName, zoneId, activeInd, countryId, countryName, cityCode, latitude, longitute, icdLocation, googleCityName, borderStatus) {
                var count = parseInt(document.getElementById("count").value);
                for (i = 1; i <= count; i++) {
                    if (i != sno) {
                        document.getElementById("edit" + i).checked = false;
                    } else {
                        document.getElementById("edit" + i).checked = true;
                    }
                }
                
                document.getElementById("cityId").value = cityId;
                document.getElementById("cityName").value = cityName;
                document.getElementById("cityName").readOnly = true;
                document.getElementById("zoneName").value = zoneName;
                document.getElementById("zoneId").value = zoneId;
                document.getElementById("activeInd").value = activeInd;
                document.getElementById("countryId").value = countryId;
                document.getElementById("countryName").value = countryName;
                document.getElementById("cityCode").value = cityCode;
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitute").value = longitute;
                document.getElementById("googleCityName").value = googleCityName;
                document.getElementById('googleCityName').readOnly = true;
                $("#GcityName").hide();
                document.getElementById("editValue").value = "1";
                document.getElementById("borderStatus").value = borderStatus;
            }

            function onKeyPressBlockNumbers(e)
            {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                reg = /\d/;
                return !reg.test(keychar);
            }
</script>
<script type="text/javascript">
            var httpRequest;
            function checkCityName() {
                if(!document.getElementById('cityName').readOnly){
                var cityName = document.getElementById('cityName').value;
                var url = '/throttle/checkCityName.do?cityName=' + cityName;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function () {
                    processRequest();
                };
                httpRequest.send(null);

            }
        }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        if (val != "" && val != 'null') {
                            $("#nameStatus").show();
                            $("#saveCity").hide();
                            $("#cityNameStatus").text('Please Check City Name  :' + val + ' is Already Exists');
                        } else {
                            $("#saveCity").show();
                            $("#nameStatus").hide();
                            $("#cityNameStatus").text('');
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }


        </script>

        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <title>City Master </title>
   <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">City Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body >
        <form name="cityMaster"  method="POST">

            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="cityId" id="cityId" value=""  />
                <tr>
                <table class="table table-info mb30 table-hover" style="width:100%">
                <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">City Master</td></tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                    </tr>
                    <tr id="GcityName">
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Google City Name</td>
                        <td class="text1" colspan="3">
                            <input type="text" name="googleCityName" id="googleCityName" class="form-control" style="width:180px;height:40px;" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();setCityName();"  autocomplete="off" maxlength="50" placeholder="Enter a location" style="width:500px;height: 30px;">
                        </td>
                    </tr>

                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Throttle City Name</td>
                        <td class="text2"><input type="text" name="cityName" id="cityName" class="form-control" style="width:180px;height:40px;" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();"  autocomplete="off" maxlength="50"></td>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>City Code</td>
                        <td class="text2"><input type="text" name="cityCode" id="cityCode" class="form-control" style="width:180px;height:40px;"  maxlength="50" ></td>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>City Zone Name</td>
                        <td class="text1"><input type="hidden" name="zoneId" id="zoneId" value="<c:out value="${zoneId}"/>"  class="form-control" style="width:180px;height:40px;">
                            <input type="text" name="zoneName" id="zoneName" value="<c:out value="${zoneName}"/>" onkeypress="getEvents(event, this.value);" onchange="getCountryName(this.value);" class="form-control" style="width:180px;height:40px;"></td>
                        <td
                            class="text1">&nbsp;&nbsp;<font color="red">*</font>City Country</td>
                        <td class="text1">
                            <input type="hidden" name="countryId" id="countryId" onchange="getCountryName(zoneName.value);" class="form-control" style="width:180px;height:40px;">
                            <input type="hidden" name="editValue" id="editValue" value="0" class="textbox">
                            <input type="text" name="countryName" id="countryName" onchange="getCountryName(zoneName.value);"  class="form-control" style="width:180px;height:40px;"></td>

                    </tr>
                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Latitude</td>
                        <td class="text2">
                            <input type="text" name="latitude" id="latitude" class="form-control" style="width:180px;height:40px;"  maxlength="50" value="">
                        </td>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Longitute</td>
                        <td class="text2">
                            <input type="text" name="longitute" id="longitute" value="" class="form-control" style="width:180px;height:40px;">
                        </td>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;Status</td>
                        <td class="text1">
                            <select  align="center" class="textbox" name="activeInd" id="activeInd" class="form-control" style="width:180px;height:40px;">
                                <option value='Y'>Active</option>
                                <option value='N'>In-Active</option>
                            </select><input type="hidden" name="borderStatus" id="borderStatus" value="0" class="form-control" style="width:180px;height:40px;">
                        </td>
                        <td class="text1" colspan="2">&nbsp;&nbsp;</td>
                    </tr>


                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button"  class="btn btn-info" value="Save" id="saveCity" name="Submit" onClick="citysubmit()">


                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <h2 align="center">City List</h2>


           <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                <thead height="30">
                   <tr id="tableDesingTH" >
                        <th> S.No </th>
                        <th> Throttle City Name  </th>
                        <th> City Code  </th>
                        <th> City Zone </th>
                        <th> City Country </th>
                        <th> Latitude </th>
                        <th> Longitude </th>
                        <th> Status </th>
                        <th> Select </th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${cityMasterList != null}">
                        <c:forEach items="${cityMasterList}" var="cml">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td align="left"> <%= sno%> </td>
                                <td align="left"> <c:out value="${cml.cityName}" /></td>
                                <td align="left"> <c:out value="${cml.cityCode}" /></td>
                                <td align="left"><c:out value="${cml.zoneName}"/></td>
                                <td align="left"><c:out value="${cml.countryName}"/></td>
                                <td align="left"><c:out value="${cml.latitude}"/></td>
                                <td align="left"><c:out value="${cml.longitute}"/></td>
                                <td align="left"><c:out value="${cml.status}"/></td>
                                <td> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cml.cityId}" />', '<c:out value="${cml.cityName}" />', '<c:out value="${cml.zoneName}" />', '<c:out value="${cml.zoneId}" />', '<c:out value="${cml.status}" />', '<c:out value="${cml.countryId}"/>', '<c:out value="${cml.countryName}"/>', '<c:out value="${cml.cityCode}" />', '<c:out value="${cml.latitude}"/>', '<c:out value="${cml.longitute}"/>', '<c:out value="${cml.icdLocation}"/>', '<c:out value="${cml.googleCityName}"/>', '<c:out value="${cml.borderStatus}"/>');" /></td>
                            </tr>



                        </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>



            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
            <br>
            <br>
            <br>
        </form>
    </body>
 </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>