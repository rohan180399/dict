/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.tyres.business;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vinoth
 */
public class TyresTO {

    private String[] positionData = null;
    private String[] stepneyData = null;
    private String axleTypeId = "";
    private String vehicleId = "";
    private String vehicleRegNo = "";
    private String regno = "";
    private String mfrId = "";
    private String mfrName = "";
    private String modelId = "";
    private String modelName = "";
    private String tyreId = "";
    private String tyreNumber = "";
    private String fromDate = "";
    private String toDate = "";
    private String itemName = "";

    private String rcType = "";
    private String fromKm = "";
    private String toKm = "";
    private String totalKm = "";

    private String tyreRotationId = "";
    private String tyreRotationNo = "";
    private String tyreRotationDate = "";
    private String tyrePositionId =  "";
    private String tyrePosition =  "";
    private String tyrePositionDesc =  "";
    private String tyreRotationRemarks =  "";
    private String tyreRotationKm =  "";
    private String lastUpdate = "";
    private String active = "";

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTyreId() {
        return tyreId;
    }

    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    public String getTyreNumber() {
        return tyreNumber;
    }

    public void setTyreNumber(String tyreNumber) {
        this.tyreNumber = tyreNumber;
    }

    public String getFromKm() {
        return fromKm;
    }

    public void setFromKm(String fromKm) {
        this.fromKm = fromKm;
    }

    public String getRcType() {
        return rcType;
    }

    public void setRcType(String rcType) {
        this.rcType = rcType;
    }

    public String getToKm() {
        return toKm;
    }

    public void setToKm(String toKm) {
        this.toKm = toKm;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTyreRotationDate() {
        return tyreRotationDate;
    }

    public void setTyreRotationDate(String tyreRotationDate) {
        this.tyreRotationDate = tyreRotationDate;
    }

    public String getTyreRotationNo() {
        return tyreRotationNo;
    }

    public void setTyreRotationNo(String tyreRotationNo) {
        this.tyreRotationNo = tyreRotationNo;
    }

    public String getTyrePosition() {
        return tyrePosition;
    }

    public void setTyrePosition(String tyrePosition) {
        this.tyrePosition = tyrePosition;
    }

    public String getTyrePositionDesc() {
        return tyrePositionDesc;
    }

    public void setTyrePositionDesc(String tyrePositionDesc) {
        this.tyrePositionDesc = tyrePositionDesc;
    }

    public String getTyrePositionId() {
        return tyrePositionId;
    }

    public void setTyrePositionId(String tyrePositionId) {
        this.tyrePositionId = tyrePositionId;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getTyreRotationKm() {
        return tyreRotationKm;
    }

    public void setTyreRotationKm(String tyreRotationKm) {
        this.tyreRotationKm = tyreRotationKm;
    }

    public String getTyreRotationRemarks() {
        return tyreRotationRemarks;
    }

    public void setTyreRotationRemarks(String tyreRotationRemarks) {
        this.tyreRotationRemarks = tyreRotationRemarks;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getTyreRotationId() {
        return tyreRotationId;
    }

    public void setTyreRotationId(String tyreRotationId) {
        this.tyreRotationId = tyreRotationId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAxleTypeId() {
        return axleTypeId;
    }

    public void setAxleTypeId(String axleTypeId) {
        this.axleTypeId = axleTypeId;
    }

    public String[] getPositionData() {
        return positionData;
    }

    public void setPositionData(String[] positionData) {
        this.positionData = positionData;
    }

    public String[] getStepneyData() {
        return stepneyData;
    }

    public void setStepneyData(String[] stepneyData) {
        this.stepneyData = stepneyData;
    }

    

}
