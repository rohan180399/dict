<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "CustomerWiseMergingProfit-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>

            <c:if test = "${tripMergingList != null}" >
                <table style="width: 1100px" align="center" border="0" >
                    <thead>
                        <tr height="80">
                            <th class="contentsub"><h3>S.No</h3></th>
                            <th class="contentsub"><h3>Customer Name</h3></th>
                            <th class="contentsub"><h3>Billing Type</h3></th>
                            <th class="contentsub"><h3>Revenue</h3></th>
                            <th class="contentsub"><h3>Loaded Expense</h3></th>
                            <th class="contentsub"><h3>Empty Expense</h3></th>
                            <th class="contentsub"><h3>Total Expense</h3></th>
                            <th class="contentsub"><h3>Profit</h3></th>
                            <th class="contentsub"><h3>Profit %</h3></th>
                            <th class="contentsub"><h3>No Of Trip</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                        <c:set var="profitPercent" value="${0}"/>
                        <c:set var="countValue" value="${0}"/>
                        <c:set var="totalRevenue" value="${0}"/>
                        <c:set var="totalExpenses" value="${0}"/>
                        <c:set var="emptyExpenses" value="${0}"/>
                        <c:set var="loadedExpenses" value="${0}"/>
                        <c:set var="profit" value="${0}"/>
                        <c:set var="perc" value="${0}"/>
                        <c:set var="profitPercent" value="${0}"/>
                        <c:set var="mergingId" value="" scope="request"/>
                        <c:set var="totalTripNos" value="${0}" />
                        <c:set var="totalTripRevenue" value="${0}" />
                        <c:set var="toalTripProfit" value="${0}" />
                        <c:set var="totalPerc" value="${0}"/>
                        <c:set var="totalProfitPercent" value="${0}"/>
                        <c:forEach items="${tripMergingList}" var="merge">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <% if (oddEven > 0) {%>
                            <c:if test="${merge.customerId != 1040}">
                                <c:set var="totalRevenue" value="${merge.earnings}"/>
                                <c:set var="emptyExpenses" value="${merge.emptyExpense}"/>
                                <c:set var="loadedExpenses" value="${merge.loadedExpense}"/>
                                <c:set var="totalExpenses" value="${emptyExpenses + loadedExpenses}"/>
                                <c:set var="profit" value="${totalRevenue - totalExpenses}"/>
                                <c:set var="perc" value="${profit/totalRevenue}"/>
                                <c:set var="profitPercent" value="${perc * 100}"/>
                                <c:set var="countValue" value="${countValue + 1}"/>
                                <tr height="30" class="text1">
                                    <td align="center"><%=sno%></td>
                                    <td align="left"><c:out value="${merge.customerName}"/></td>
                                    <td align="left"><c:out value="${merge.billingType}"/></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.earnings}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.emptyExpense}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense + merge.emptyExpense}" /></td>
                                    <td align="right">
                                        <c:if test="${profit > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profit < 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>&nbsp;
                                    </td>
                                    <td align="right">
                                        <c:if test="${profitPercent <= 0}">
                                            <font color="red">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profitPercent > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                    </td>
                                    <td align="right"><c:out value="${merge.tripCount}"/>&nbsp;</td>
                                    <c:set var="totalTripNos" value="${merge.tripCount+totalTripNos}" />
                                    <c:set var="totalTripRevenue" value="${merge.earnings+totalTripRevenue}" />
                                    <c:set var="totalTripExpenses" value="${totalExpenses+totalTripExpenses}" />
                                    <c:set var="toalTripProfit" value="${totalTripRevenue-totalTripExpenses}" />
                                    <c:set var="totalPerc" value="${toalTripProfit/totalTripRevenue}"/>
                                    <c:set var="totalProfitPercent" value="${totalPerc*100}"/>

                                </tr>
                                <%
                                     index++;
                                     sno++;
                                %>
                            </c:if>
                            <%} else {%>
                            <c:if test="${merge.customerId != 1040}">
                                <c:set var="totalRevenue" value="${merge.earnings}"/>
                                <c:set var="emptyExpenses" value="${merge.emptyExpense}"/>
                                <c:set var="loadedExpenses" value="${merge.loadedExpense}"/>
                                <c:set var="totalExpenses" value="${emptyExpenses + loadedExpenses}"/>
                                <c:set var="profit" value="${totalRevenue - totalExpenses}"/>
                                <c:set var="perc" value="${profit/totalRevenue}"/>
                                <c:set var="profitPercent" value="${perc * 100}"/>
                                <c:set var="countValue" value="${countValue + 1}"/>
                                <tr height="30" class="text2">
                                    <td align="center"><%=sno%></td>
                                    <td align="left"><c:out value="${merge.customerName}"/></td>
                                    <td align="left"><c:out value="${merge.billingType}"/></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.earnings}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.emptyExpense}" /></td>
                                    <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${merge.loadedExpense + merge.emptyExpense}" /></td>
                                    <td align="right">
                                        <c:if test="${profit > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profit < 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profit}"/>
                                            </font>
                                        </c:if>&nbsp;
                                    </td>
                                    <td align="right">
                                        <c:if test="${profitPercent <= 0}">
                                            <font color="red">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                        <c:if test="${profitPercent > 0}">
                                            <font color="green">
                                                <fmt:formatNumber pattern="##0.00" value="${profitPercent}"/>
                                            </font>
                                        </c:if>
                                    </td>
                                    <td align="right"><c:out value="${merge.tripCount}"/>&nbsp;</td>
                                    <c:set var="totalTripNos" value="${merge.tripCount+totalTripNos}" />
                                    <c:set var="totalTripRevenue" value="${merge.earnings+totalTripRevenue}" />
                                    <c:set var="totalTripExpenses" value="${totalExpenses+totalTripExpenses}" />
                                    <c:set var="toalTripProfit" value="${totalTripRevenue-totalTripExpenses}" />
                                    <c:set var="totalPerc" value="${toalTripProfit/totalTripRevenue}"/>
                                    <c:set var="totalProfitPercent" value="${totalPerc*100}"/>

                                </tr>
                                <%
                                     index++;
                                     sno++;
                                %>
                            </c:if>
                            <%}%>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>


            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <br/>
            <br/>
            <c:if test = "${tripMergingList != null}" >
                <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="0" >
                    <tr height="25" align="right">
                        <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                        <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTripRevenue}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTripExpenses}" /></td>
                    </tr>
                    <tr height="25" >
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                        <td width="150" align="right">
                            <c:if test="${toalTripProfit > 0}">
                                <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${toalTripProfit}" /></font>
                            </c:if>
                            <c:if test="${toalTripProfit <= 0}">
                                <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${toalTripProfit}" /></font>
                            </c:if>
                        </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit % </td>
                        <td width="150" align="right">
                            <c:if test="${totalPerc/count > 0}">
                                <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" />&nbsp;%</font>
                            </c:if>
                            <c:if test="${totalPerc/count <= 0}">
                                <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" />&nbsp;%</font>
                            </c:if>
                        </td>
                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>