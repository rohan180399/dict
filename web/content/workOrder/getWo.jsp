
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <title>MRSList</title>
    </head>
    <body onload="document.purchase.woId.focus();">
        
        
        <script>
        function submitPage() {
            if( (document.purchase.woId.value != '') && ( !isFloat(document.purchase.woId.value) )  ) {            
            document.purchase.action="/throttle/handleAlterRCWOPage.do"
            document.purchase.submit();
            }
        }
               
            
        </script>
        
        
        
        <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
        
        <form name="purchase"  method="post" >
          
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
           
                    
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="550" id="bg" class="border">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" ><div class="contenthead">MODIFY WO </div></td>
                </tr>
                
                <tr>
                    <td class="text1" height="30"><spring:message code="stores.label.WorkOrderNo."  text="default text"/>
</td>
                    <td class="text1" height="30"> <input type="text" name="woId" value="" class="form-control"> </td>
                </tr>                                                 
            </table>
            
            <br>                                                
             <center>   
            <input type="button" class="button" name="Search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPage();" > &nbsp;            
            </center>                           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
