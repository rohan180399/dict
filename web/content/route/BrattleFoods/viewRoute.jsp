<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#routeStatus").text('Route Exists Code is :' + val);
                } else {
                    $("#routeStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    //savefunction
    function submitPage(value) {
        var count = validateRouteDetails();
        if(document.getElementById('distance').value == '' ){
            alert("enter the travel distance");
            document.getElementById('distance').focus();
        }else if(count == 0){
        document.route.action = '/throttle/updateRoute.do';
        document.route.submit();
        }
    }


function validateRouteDetails(){
        var vehMileage = document.getElementsByName("vehMileage");
        var reefMileage =  document.getElementsByName("reefMileage");
        var fuelCostPerKms =  document.getElementsByName("fuelCostPerKms");
        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var miscCostKm = document.getElementsByName("miscCostKm");
        var driverIncenKm = document.getElementsByName("driverIncenKm");
        var factor = document.getElementsByName("factor");
        var vehExpense = document.getElementsByName("vehExpense");
        var reeferExpense = document.getElementsByName("reeferExpense");
        var totExpense = document.getElementsByName("totExpense");

        var a = 0;
        var count=0;
        for(var i=0 ; i<vehMileage.length; i++ ){
            a = i+1;
            if(fuelCostPerKms[i].value == ''){
                alert("please fill fuel cost per km for row "+a);
                fuelCostPerKms[i].focus();
                count=1;
                return count;
            }else if(fuelCostPerHrs[i].value == ''){
                alert("please fill fuel cost per hrs for row "+a);
                fuelCostPerHrs[i].focus();
                count=1;
                return count;
            }else if(tollAmounts[i].value == ''){
                alert("please fill toll rate per km for row "+a);
                tollAmounts[i].focus();
                count=1;
                return count;
            }else if(miscCostKm[i].value == ''){
                alert("please fill miscellaneous cost per km for row "+a);
                miscCostKm[i].focus();
                count=1;
                return count;
            }else if(driverIncenKm[i].value == ''){
                alert("please fill driver incentive per km for row "+a);
                driverIncenKm[i].focus();
                count=1;
                return count;
            }else if(factor[i].value == ''){
                alert("please fill factor for row "+a);
                factor[i].focus();
                count=1;
                return count;
            }else if(vehExpense[i].value == ''){
                alert("please fill vehExpense for row "+a);
                vehExpense[i].focus();
                count=1;
                return count;
            }else if(reeferExpense[i].value == ''){
                alert("please fill reeferExpense for row "+a);
                reeferExpense[i].focus();
                count=1;
                return count;
            }else if(totExpense[i].value == ''){
                alert("please fill totExpense for row "+a);
                totExpense[i].focus();
                count=1;
            }
        }
        return count;
    }


//    function numbersOnly(oToCheckField, oKeyEvent) {
//    return oKeyEvent.charCode === 0 || /\d/.test(String.fromCharCode(oKeyEvent.charCode));
//  }
function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


//
//    function numeralsOnly(evt) {
//       evt = (evt) ? evt : event;
//        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
//           ((evt.which) ? evt.which : 0));
//        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//           alert("Enter numerals only in this field.");
//           return false;
//          }
//           return true;
//   }



</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Route  Edit</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="sorter.size(10);setTollCost();">
        <form name="route"  method="post">
            <c:if test="${routeList != null}">
            <table class="table table-info mb30 table-hover" style="width:70%">
                <c:forEach items="${routeList}" var="rl">
                <tr>
                    <td  colspan="4" style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Route  Edit</td>
                </tr>
                <tr>
                    <td >Route Code</td>
                    <td ><input type="hidden" name="routeId" id='routeId' class="form-control" value="<c:out value="${rl.routeId}"/>"/><input type="hidden" name="routeCode" id='routeCode' class="form-control" value="<c:out value="${rl.routeCode}"/>" readonly><c:out value="${rl.routeCode}"/></td>
                    <td >Toll Amount Type</td>
                    <td >
                        <input type="hidden" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />Average
                       <%-- <c:if test="${rl.tollAmountType == '1'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" />Fixed
                        </c:if>
                        <c:if test="${rl.tollAmountType == '2'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" checked  />Fixed
                        </c:if>  --%>
                        </td>
                </tr>
                <tr>
                    <td >From Location</td>
                    <td ><input type="hidden" name="cityFromId" id="cityFromId" class="form-control" value="<c:out value="${rl.cityFromId}"/>"/><c:out value="${rl.cityFromName}"/></td>
                    <td >To Location</td>
                    <td ><input type="hidden" name="cityToId" id="cityToId" class="form-control" value="<c:out value="${rl.cityToId}"/>"/><c:out value="${rl.cityToName}"/></td>
                </tr>
                <tr>
                    <td >KM</td>
                    <td ><input type="text" readonly name="distance" id="distance" class="form-control" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;" value="<c:out value="${rl.distance}"/>"></td>
                    <td >Travel Time(Hrs)</td>
                    <td >HR:<input type="text"  readonly   style="width:38px" name="travelHour" id="travelHour" class="textbox" value="<c:out value="${rl.travelHour}"/>" readonly/>MI:<input type="text" name="travelMinute" style="width:38px" id="travelMinute" class="textbox" value="<c:out value="${rl.travelMinute}"/>" readonly/></td>
                </tr>
                <tr>
                    <td >Reefer Running Hours</td>
                    <td >HR:<input type="text"  style="width:38px" name="reeferHour" id="reeferHour" class="textbox" value="<c:out value="${rl.reeferHour}"/>" readonly/>MI:<input type="text" name="reeferMinute" style="width:38px" id="reeferMinute" class="textbox" value="<c:out value="${rl.reeferMinute}"/>" readonly/></td>
                    <td >Road Type</td>
                    <td >
                        <select name="roadType" id="roadType">
                            <c:if test="${rl.roadType == 'National Highway'}">
                            <option value="NH" selected>National Highway</option>

                            </c:if>
                            <c:if test="${rl.roadType == 'State Highway'}">

                            <option value="SH" selected>State Highway</option>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                     <td >Current Fuel Cost/Ltr </td>
                    <td ><input type="hidden" name="fuelCost" id="fuelCost"   value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
                    <td >Current CNG Cost/Kg </td>
                    <td ><input type="hidden" name="cngCost" id="cngCost"  value="<%=request.getAttribute("currentCNGPrice")%>" readonly/><label><%=request.getAttribute("currentCNGPrice")%></label></td>
                </tr>
                <tr style="display: none">
                    <td >Status</td>
                    <td >
                        <select name="status" id="status" disabled >
                            <option value="Y">Active</option>
                            <option value="N">In Active</option>
                        </select>
                        <script>
                            document.getElementById('status').value = '<c:out value="${rl.status}"/>'
                        </script>
                    </td>

                    <td colspan="2" >
                        &nbsp;
                    </td>
                </tr>
                 <tr>
                    <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                    <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
                    <td><input type="hidden" id="avgTollAmount3118" name="avgTollAmount3118" value="<c:out value="${avgTollAmount3118}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost3118" id="avgMisCost3118"  value="<c:out value="${avgMisCost3118}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive3118" id="avgDriverIncentive3118"  value="<c:out value="${avgDriverIncentive3118}"/>"/></td>
                    <td><input type="hidden" name="avgFactor3118" id="avgFactor3118" value="<c:out value="${avgFactor3118}"/>"/></td>
                </tr>
                </c:forEach>
            </table>
            </c:if>
            <script type="text/javascript">
                //concard value get from the addrout configDetails
                function setTollCost(type) {
                    if (type == 1) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var reeferExpense = document.getElementsByName('reeferExpense');
                        var factor = document.getElementsByName('factor');
                        var vehExpense = document.getElementsByName('vehExpense');
                        for (var i = 0; i < tollAmounts.length; i++) {
                            tollAmounts[i].value = document.getElementById('avgTollAmount').value;
                            misCost[i].value = document.getElementById('avgMisCost').value;
                            driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                            factor[i].value = document.getElementById('avgFactor').value;
                            if (fuelCostPerKms[i].value != '') {
                                var fuelAmnt = fuelCostPerKms[i].value * km;
                            } else {
                                var fuelAmnt = 0 * km;
                            }
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (misCost[i].value != '') {
                                var misCostPerKm = misCost[i].value * km;
                            } else {
                                var misCostPerKm = 0 * km;
                            }
//                            if (factor[i].value != '') {
//                                var factorPerKm = factor[i].value * km;
//                            } else {
//                                var factorPerKm = 0 * km;
//                            }
                            if (driverIncentive[i].value != '') {
                                var driverIncentivePerKm = driverIncentive[i].value * km;
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            if (fuelCostPerKms[i].value != '') {
//                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt) + parseFloat(factorPerKm);
                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            driverIncentive[i].readOnly = true;
                            misCost[i].readOnly = true;
                            tollAmounts[i].readOnly = true;
                            factor[i].readOnly = true;
                            calcTotExp();
                        }
                    } else if (type == 2) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var vehExpense = document.getElementsByName('vehExpense');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var factor = document.getElementsByName("factor");
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            if (fuelCostPerKms[i].value != '') {
                                var total = fuelCostPerKms[i].value * km
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                vehExpense[i].value = 0.00;
                            }
                            tollAmounts[i].value = "";
                            misCost[i].value = '';
                            driverIncentive[i].value = '';
                            factor[i].value = '';
                            tollAmounts[i].readOnly = false;
                            driverIncentive[i].readOnly = false;
                            misCost[i].readOnly = false;
                            factor[i].readOnly = false;
                            calcTotExp();
                        }
                    }
                }

                //calculate fuelCostPerKm
                function calcFuleCostPerKm() {
                    var km = document.getElementById("distance").value;
                    var fuelCostPerKm = 0;
                    var fuelCost = document.getElementById('fuelCost').value;
                    var vehMileage = document.getElementsByName('vehMileage');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factor = document.getElementsByName('factor');
                    if (km != '') {
                        for (var i = 0; i < vehMileage.length; i++) {
                            var totFuelCost = parseInt(km) / parseInt(vehMileage[i].value) * parseInt(fuelCost);
                            fuelCostPerKm = parseInt(totFuelCost) / parseInt(km);
                            fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < vehMileage.length; i++) {
                            fuelCostPerKms[i].value = '';
                        }
                    }
                    calcVehExp();

                    //Calc Vehicle Running Hours
                    var perDayTravelKm = 450;
                    var travelKm = km / perDayTravelKm;
                    var hours = travelKm.toFixed(2) * 24;
                    var travelHrs = hours.toFixed(2);
                    alert(travelHrs);
                    var temp = travelHrs.split(".");
                    travelHrs = temp[0];
                    alert(travelHrs);
                    var travelMinute = temp[1];
                    var hour = 0;
                    var minute = 0;
                    if (temp[1] > 59) {
                        for (var travelHour = 0; travelMinute > 59; travelHrs++) {
                            hour = travelHour + 1;
                            travelMinute = parseInt(travelMinute) - 60;
                            minute = travelMinute;
                        }
                    } else {
                        hour = 0;
                        minute = travelMinute;
                    }
                    var tothour = hour + parseInt(temp[0]);
                    alert(tothour);
                     if (tothour < 10) {
                        tothour = '0' + tothour;
                    }
                    //if (minute < 10 && (minute.indexOf("0") != -1) == 'false') {
                    if (minute < 10) {
                        minute = '0' + minute;
                    }
//                    alert(minute.indexOf("0") != -1);
                    document.getElementById("travelHour").value = tothour;
                    alert(document.getElementById("travelHour").value);
                    document.getElementById("travelMinute").value = minute;
                    calcReferHours();
                }
                //calculate Vehicle Expenses cost
                function calcVehExp() {
                    var km = document.route.distance.value;
                    var tollAmounts = document.getElementsByName('tollAmounts');
                    var miscCostKm = document.getElementsByName('miscCostKm');
                    var driverIncenKm = document.getElementsByName('driverIncenKm');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factorPerKm = document.getElementsByName('factor');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    if (km != '') {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            var expCost = parseInt(km) * fuelCostPerKms[i].value;
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (miscCostKm[i].value != '') {
                                var misCost = miscCostKm[i].value * km;
                            } else {
                                var misCost = 0 * km;
                            }
                            if (driverIncenKm[i].value != '') {
                                var driverIncentive = driverIncenKm[i].value * km;
                            } else {
                                var driverIncentive = 0 * km;
                            }
//                            if (factorPerKm[i].value != '') {
//                                var factor = factorPerKm[i].value * km;
//                            } else {
//                                var factor = 0 * km;
//                            }
//                            var total = parseFloat(expCost) + parseFloat(tollAmnt) + parseFloat(misCost) + parseFloat(driverIncentive) + parseFloat(factor);
//                            alert("parseFloat(expCost)"+parseFloat(expCost));
//                            alert("parseFloat(tollAmnt)"+parseFloat(tollAmnt));
//                            alert("parseFloat(misCost)"+parseFloat(misCost));
//                            alert("parseFloat(driverIncentive)"+parseFloat(driverIncentive.toFixed(2)));
                            var total = parseFloat(expCost.toFixed(2)) + parseFloat(tollAmnt.toFixed(2)) + parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                            vehExpense[i].value = total.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            vehExpense[i].value = '';
                            fuelCostPerHrs[i].value = '';
                            reeferExpense[i].value = '';
                            totExpense[i].value = '';
                        }
                    }
                    calcTotExp();
                }

                //calculate reffer hours
                function calcReferHours() {
                    var travelHour = document.getElementById("travelHour").value;
                    var travelMinute = document.getElementById("travelMinute").value;
                    var refMinute = 0;
                    if (travelHour === '') {
                        travelHour = 0;
                    }
                    if (travelMinute == '') {
                        travelMinute = 0;
                    }
                    if (travelHour == '' && travelMinute == '') {
                        hour = 0;
                        minute = 0;
                    } else {
                        var travelHour = parseFloat(travelHour) * 60;
                        var travelHour1 = parseFloat(travelMinute) + parseFloat(travelHour);
                        var travelHour2 = travelHour1 / 100;
                        alert("travelHour2=="+travelHour2);
                        var reeferMinute = (travelHour2 * 75).toFixed(2);
                        //alert("reeferMinute=="+reeferMinute);
                        refMinute = travelHour2 * 75;
                        var hour = 0;
                        var minute = 0;
                        if (reeferMinute > 59) {
                            for (var reeferHour = 0; reeferMinute > 59; reeferHour++) {
                                hour = reeferHour + 1;
                                reeferMinute = parseInt(reeferMinute) - 60;
                                minute = reeferMinute;
                                //alert(""+minute);
                            }
                        } else {
                            hour = 0;
                            minute = reeferMinute;
                        }
                    }
                    if (hour < 10) {
                        hour = '0' + hour;
                    }

                    if (minute < 10) {
                        minute = '0' + minute;
                    }
//                    alert(minute);
                    if(minute == ''){
                        minute == '0';
                    }
                    alert("hour===="+hour);
                    //alert("minute==="+minute);
                    document.getElementById("reeferHour").value = parseFloat(hour);
                    document.getElementById("reeferMinute").value = parseFloat(minute);


                    if (refMinute != 00) {
//                    alert(refMinute);
                        calcFuleCostPerHr(refMinute);
                    }
                }

                //calculate fuelCostPerHr
                function calcFuleCostPerHr(refMinute) {
//
                    var fuelCost = document.getElementById('fuelCost').value;
                    var reefMileage = document.getElementsByName('reefMileage');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    for (var i = 0; i < reefMileage.length; i++) {
                        //alert("refMinute==="+refMinute.toFixed(2));
                   /*     var totFuleCostPerMinute = parseInt(refMinute) / parseInt(reefMileage[i].value) * parseInt(fuelCost);
                        var fuleCostPerMinute = parseInt(totFuleCostPerMinute) / parseInt(refMinute);
                        var fuelCostPerHour = fuleCostPerMinute * 60;
                        fuelCostPerHrs[i].value = fuelCostPerHour.toFixed(2);
                        var totalReeferCastPerMinute = parseInt(refMinute) * parseInt(fuelCostPerHrs[i].value);
                        var refExpCal = totalReeferCastPerMinute / 60;
                        reeferExpense[i].value = refExpCal.toFixed(2);
                    */
                        //alert(reefMileage[i].value);

                        var referExp =  reefMileage[i].value * fuelCost;
                        fuelCostPerHrs[i].value = referExp.toFixed(2);
                        //alert(fuelCostPerHrs[i].value);
                        //alert(refMinute.toFixed(2));
                        var totalReefExp = fuelCostPerHrs[i].value * refMinute.toFixed(2)/60;
                        //alert(totalReefExp.toFixed(2));
                        reeferExpense[i].value = totalReefExp.toFixed(2);


                    }
                    calcTotExp();
                }


                function calcTotExp() {
                    var km = document.route.distance.value;
//                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
//                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    for (var i = 0; i < vehExpense.length; i++) {
                        if (vehExpense[i].value != '') {
                            var vehExp = vehExpense[i].value;
                        } else {
                            var vehExp = 0;
                        }
                        if (reeferExpense[i].value != '') {
                            var refExp = reeferExpense[i].value;
                        } else {
                            var refExp = 0;
                        }
                        var totCost = parseFloat(vehExp) + parseFloat(refExp);
                        totExpense[i].value = totCost.toFixed(2);
                    }
                }

                function calcTollExp(index) {
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(tollAmounts != ''){
                    var totExp = parseFloat(tollAmounts) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }
                }
                function calcMiscExp(index) {
                    var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(miscCostKm != ''){
                        var totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcDriExp(index) {
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(driverIncenKm != ''){
                    var totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcFactorExp(index) {
                    var factorPerKm = document.getElementById('factor' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(factorPerKm !=''){
                        var totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }

            </script>
            <br>
            <br>
             <table class="table table-info mb30 table-hover" style="width:50%;"   id="bg">
                 <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">
                <td  valign="center" height="35" >Available RTO's </td>
                <td  height="35"></td>
                <td  valign="center" height="35">Applicable RTO's </td>
                </tr>
                <tr>
                <td valign="top">
                <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                <tr>
                <td width="150"><select style="width:200px;" multiple size="10" id="availableFunc" name="availableFunc" class="form-control">
                <c:if test = "${availableRTOList != null}" >
                <c:forEach items="${availableRTOList}" var="roles">
                <option value='<c:out value="${roles.rtoId}" />'><c:out value="${roles.rtoName}" /></option>
                </c:forEach >
                </c:if>
                </select></td>
                </tr>
                </table>

                </td>

                <td height="100" valign="center">
                <table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
                <tr>
                <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="form-control" value=">" onClick="copyAddress(availableFunc.value)"></td></tr>
                <tr >
                <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="form-control" value="<" onClick="copyAddress1(assignedfunc.value)"></td></tr>


                </table>
                </td>
                <td valign="top">

                <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                <tr>
                <td width="150"><select style="width:200px;" multiple size="10" id="assignedfunc" name="assignedfunc"  class="form-control">
                 <c:if test = "${routeMappedRTOList != null}" >
                <c:forEach items="${routeMappedRTOList}" var="roles">
                <option value='<c:out value="${roles.rtoId}" />'><c:out value="${roles.rtoName}" /></option>
                </c:forEach >
                </c:if>
                </select></td>
                </tr>
                </table></td>


                </tr>

                </table>


            <br>
            <br>
            <% int count = 0;%>
            <c:if test="${routeDetailsList != null}">
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Vehicle Type /th>
                            <th>Fuel Cost Per Km's</th>
                            <th>Fuel Cost Per Hr's </th>
                            <th>Toll Cost Per KM </th>
                            <th>Misc Cost Per KM</th>
                            <th>Driver Incentive Per KM</th>
                            <th>Factor</th>
                            <th>Fuel Reqd w/o Reefer (Ltrs)</th>
                            <th>Fuel Reqd with Reefer (Ltrs)</th>
                            <th>Vehicle Expense Cost</th>
                            <th>Reefer Expense Cost</th>
                            <th>Total Expense Cost</th>
                        </tr>
                    </thead>
                 <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${routeDetailsList}" var="rdl">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                            <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>" style="width: 200px"><input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${rdl.vehicleTypeId}"/>" readonly/><c:out value="${rdl.vehicleTypeName}"/>
                                    <input type="hidden" name="vehMileage" id="vehMileage" value="<c:out value="${rdl.vehicleMileage}"/>"/>
                                    <input type="hidden" name="reefMileage" id="reefMileage" value="<c:out value="${rdl.reeferMileage}"/>"/>
                                    <input type="hidden" name="fuelTypeId" id="fuelTypeId<%=index%>" value="<c:out value="${rdl.fuelTypeId}"/>"/>
                                    <input type="hidden" name="vehicleTonnage" id="vehicleTonnage<%=index%>" value="<c:out value="${rdl.vehicleTonnage}"/>"/>
                                </td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly  name="fuelCostPerKms" id="fuelCostPerKms<%=index%>"   style="width: 90px" value="<c:out value="${rdl.fuelCostKm}"/>" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="fuelCostPerHrs" id="fuelCostPerHrs<%=index%>"  style="width: 90px" value="<c:out value="${rdl.fuelCostHr}"/>"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="tollAmounts" id="tollAmounts<%=index%>"  style="width: 90px" onkeyup="calcTollExp('<%=index%>')" value="<c:out value="${rdl.tollAmountperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="miscCostKm" id="miscCostKm<%=index%>"   style="width: 90px" onkeyup="calcMiscExp('<%=index%>')" value="<c:out value="${rdl.miscCostperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="driverIncenKm" id="driverIncenKm<%=index%>"  style="width: 90px" onkeyup="calcDriExp('<%=index%>')" value="<c:out value="${rdl.driverIncentperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="factor" id="factor<%=index%>"  style="width: 90px" onkeyup="calcFacExp('<%=index%>')" value="<c:out value="${rdl.factors}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="fuelWOReefer" id="fuelWOReefer<%=index%>"style="width: 90px" value="<c:out value="${rdl.fuelWOReeferReq}"/>" />
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="fuelWithReefer" id="fuelWithReefer<%=index%>"style="width: 90px" value="<c:out value="${rdl.fuelWithReeferReq}"/>"/>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="vehExpense" id="vehExpense<%=index%>"style="width: 90px"  value="<c:out value="${rdl.vehiExpense}"/>" readonly/>
                                 <input type="hidden" name="varExpense" id="varExpense" value="<c:out value="${rdl.variExpense}"/>"/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="reeferExpense" id="reeferExpense<%=index%>"  style="width: 90px" value="<c:out value="${rdl.reefeExpense}"/>" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text"  readonly name="totExpense" id="totExpense<%=index%>"   style="width: 90px" value="<c:out value="${rdl.totaExpense}"/>" readonly/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");</script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
