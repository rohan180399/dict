<%-- 
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : gopi
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PrintTripSheet</title>
    </head>
    <body>
        <form name="enter" method="post">
            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String startDate = sdf.format(today);
            %>
            <div id="printContent">
                <table align="center" style="border-collapse: collapse">
                    <tr>
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                            <table align="left" >
                                <tr>
                                    <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                    <td style="padding-left: 60px; padding-right:50px;">
                                        <br>
                                        <font size="2">PAN NO-AACCB8054G </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">Service TaxRegnNo-AACCB8054GST001</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">CIN NO-U63040MH2006PTC159885</font><br>
                                        <font size="5"><b><u>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</u></b></font><br>
                                <center> <font size="2" > <u>Trip(Route Expanses)</u></center>

                        </td>
                        <td></td>
                    </tr>
                </table>                    
                </td>                
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; " align='center'>
                        <table width="70%" >
                            <tr>
                                <td width="40%"><font size="2">S.No..............</font><td/>
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">Lorry.No <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <td style="float: left;padding-left: 150px;" width="40%"><font size="2">Date : <%=startDate%></font></td>
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">Driver's.Name:&nbsp;<c:out value="${driverName}"/></font></td>
                                <td style="float: left;padding-left: 150px;" width="40%"><font size="2">Transport :&nbsp;<c:out value="${transpoter}"/></font></td>
                            </tr>
                            <tr>
                                 <td width="100%" colspan="2"><font size="2">Route :&nbsp;<c:out value="${routeInfo}"/></font></td>
<!--                                <td width="40%"><font size="2">From&nbsp;...................</font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <td style="float: left;padding-left: 150px;" width="40%"><font size="2">To&nbsp;...................</font></td>-->
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">GR.No:&nbsp;DICT/<c:out value="${grNo}"/></font></td>
                                <td style="float: left;padding-left: 150px;" width="70%"><font size="2">Date.Of.Issue:&nbsp;<c:out value="${grDate}"/></font></td>
                            </tr>
                            <tr>
                                <td width="60%" ><font size="2">Party:&nbsp;<c:out value="${billingParty}"/></font></td>
                                 <td style="float: left;padding-left: 150px;" width="65%"><font size="2">Diesel(ltr):&nbsp;<c:out value="${dieselUsed}"/> & Amount: <c:out value="${dieselCost}"/></font></td>
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">Trip Advance:&nbsp;Rs/-<c:out value="${paidExpense}"/></font></td>
                                <td style="float: left;padding-left: 150px;" width="40%"><font size="2">OP.M/R.&nbsp;<c:out value="${startKm}"/></font></td>
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">Vehicle Moved On&nbsp;&nbsp;<c:out value="${startDate}"/></font></td>
                                <td style="float: left;padding-left: 150px;" width="40%"><font size="2">CL.M/R&nbsp;<c:out value="${endKm}"/></font></td>
                            </tr>
                            <tr>
                                <td width="40%"><font size="2">Vehicle Returned On&nbsp;&nbsp;<c:out value="${endDate}"/></font></td>
                                <td style="float: left;padding-left: 150px;" width="60%"><font size="2">Total Kms&nbsp;<c:out value="${tripTotalKms}"/></font></td>
                            </tr>
                        </table>
                        <br>

                        <table align='center' border="1">
                            <tr align='center'> 
                                <td style="width:2px">S.No</td>
                                <td style="width:500px">Particulars</td>
                                <td style="width:100px">Amount</td>
                            </tr>
                            <tr>
                                <td align='center'>1</td>
                                <td >Diesel/Fuel Expanses</td>		
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td align='center'>2</td>
                                <td>Total Tax & Packing Expanses</td>		
                                <td>  <c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Total Tax & Packing Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>3</td>
                                <td>Tea/Food/Refreshment Expanses</td>		
                                <td> <c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Tea/Food/Refreshment Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>4</td>
                                <td>Repair & Maintence Expanses(If Any)</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Repair & Maintence Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>5</td>
                                <td> Weightment Expanses</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Weightment Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>6</td>
                                <td>Octroi Expanses</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Octroi Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>7</td>
                                <td>Bhati Expanses</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Bhati Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>8</td>
                                <td>Trip Expanses</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Trip Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>9</td>
                                <td>Misc Expanses</td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Misc Expanses' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'>10</td>
                                <td>Extra Fooding </td>		
                                <td><c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                        <c:if test="${trip.expenseName == 'Extra Fooding' }">
                                            <c:out value="${trip.totalExpenses}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align='center'>Total Amount </td>		
                                <td>&nbsp;<c:out value="${foodCost}"/></td>
                            </tr>

                        </table>            
                        <br>
                        <table width="60%">          
                            <tr>
                                <td width="40%"><font size="2">Received With Thanks Rs...........................</td>
                            </tr>
                            <tr>
                                <td><font align='left' size="2">from BLIS against the expanses incurred as per above mentioned </td>
                            </tr>    
                        </table> 
                        <br>
                </table>
            </div>
            <center><input type="button" class="button"  value="Print" onClick="print('printContent');" ></center>
            <script type="text/javascript">
                function print(val)
                {
                    var DocumentContainer = document.getElementById(val);
                    var WindowObject = window.open('', "TrackHistoryData",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body> 
</html>
