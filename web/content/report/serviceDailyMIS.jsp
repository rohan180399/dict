<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

         <script language="javascript">
             function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
         </script>
  <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsmis.label.WorkOrderReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsmis.label.FMSMIS" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsmis.label.WorkOrderReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


<body onLoad="setValues()" >

<form name="chart" method="post" >

<!--
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1"></li>
    </ul>
    <div id="first">-->
    <table class="table table-info mb30 table-hover">
        <thead><tr><th colspan="4"><spring:message code="fmsmis.label.WorkOrderReport"  text="default text"/></th></tr></thead>

  <tr>
    <td><spring:message code="fmsmis.label.FromDate"  text="default text"/></td>
    <td>
        <input type="text" class="datepicker form-control" style="width:260px;height:40px;" autocomplete="off" readonly name="fromDate" value="" >
       <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.fromDate,'dd-mm-yyyy',this)"/>-->
    </td>

    <td><spring:message code="fmsmis.label.ToDate"  text="default text"/></td>
    <td>
        <input type="text" class="datepicker form-control" style="width:260px;height:40px;"  autocomplete="off" readonly name="toDate" value="" >
        <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.toDate,'dd-mm-yyyy',this)"/>-->
    </td>
  </tr>
  <tr>
      <td align="center" colspan="4"><input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="fmsmis.label.Search"  text="default text"/>" onClick="searchSubmit();" ></td>
</tr>
    </table>
<!--    </div></div>
    </td>
    </tr>
    </table>-->


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>



<table class="table table-info mb30 table-hover">
    <thead>
    <c:if test = "${serviceSummary != null}" >
        <tr>
            <th width="2%" ><spring:message code="fmsmis.label.Sno"  text="default text"/></th>
            <th width="5%" ><spring:message code="fmsmis.label.JobCard"  text="default text"/></th>
            <th width="2%" ><spring:message code="fmsmis.label.Age"  text="default text"/></th>
            <th width="5%" ><spring:message code="fmsmis.label.RegnNo"  text="default text"/></th>
            <th width="6%" ><spring:message code="fmsmis.label.Make/Model"  text="default text"/></th>
            <th width="8%" ><spring:message code="fmsmis.label.Customer"  text="default text"/></th>
            <th width="22%" ><spring:message code="fmsmis.label.Complaint"  text="default text"/></th>
            <th width="6%" ><spring:message code="fmsmis.label.InTime"  text="default text"/></th>
            <th width="8%" ><spring:message code="fmsmis.label.PlannedDueDate"  text="default text"/></th>
            <th width="8%" ><spring:message code="fmsmis.label.ExpectedDelyDate"  text="default text"/></th>
            <th width="8%" ><spring:message code="fmsmis.label.ActualDelyDate"  text="default text"/></th>
            <th width="5%" ><spring:message code="fmsmis.label.Status"  text="Status"/></th>
            <th width="2%" ><spring:message code="fmsmis.label.Bay"  text="default text"/></th>
            <th width="13%" ><spring:message code="fmsmis.label.Remarks"  text="default text"/></th>
        </tr>
    </thead>
        <%
					int index = 1 ;
                    %>
    <c:forEach items="${serviceSummary}" var="serviceSummary">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.jobCardId}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.age}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.regNo}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.mfrName}"/>/<c:out value="${serviceSummary.modelName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.custName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.problemName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.inTime}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.scheduledDate}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.pcd}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.acd}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.status}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.bayNo}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.remarks}"/></td>






</tr>
<%
            index++;
                        %>

   </c:forEach>
</c:if>


</table>

</body>

<script>
    function searchSubmit()
    {
        document.chart.action="/throttle/handleServiceDailyMIS.do"
        document.chart.submit();
    }

function setValues(){
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.chart.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.chart.toDate.value='<%= request.getAttribute("toDate") %>';
    }
}

</script>

</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


