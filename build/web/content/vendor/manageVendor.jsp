

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script language="javascript">
    function submitPage(value)
    {
//            if (value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value == 'First' || value == 'Last') {
//                if (value == 'GoTo') {
//                    var temp = document.vendorDetail.GoTo.value;
//                    document.vendorDetail.pageNo.value = temp;
//                    document.vendorDetail.button.value = value;
//                    document.vendorDetail.action = '/throttle/manageVendorPage.do';
//                    document.vendorDetail.submit();
//                } else if (value == "First") {
//                    temp = "1";
//                    document.vendorDetail.pageNo.value = temp;
//                    value = 'GoTo';
//                } else if (value == "Last") {
//                    temp = document.vendorDetail.last.value;
//                    document.vendorDetail.pageNo.value = temp;
//                    value = 'GoTo';
//                }
//                document.vendorDetail.button.value = value;
//                document.vendorDetail.action = '/throttle/manageVendorPage.do';
//                document.vendorDetail.submit();
//            } else if (value == 'add')
//            {

        document.vendorDetail.action = '/throttle/addVendorPage.do';
//            } else if (value == 'modify') {
//
//                document.vendorDetail.action = '/throttle/';
//            }
        document.vendorDetail.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Manage Vendor</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>


        <form method="post" name="vendorDetail">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <c:if test = "${VendorList != null}" >



                
                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>

                        <!--<tr height="30">-->
                        <tr>
                            <th height="30" >S.No</th>
                            <th height="30" >Vendor Name</th>
                            <th height="30" >Vendor Type</th>
<%--                            <th height="30" >Price Type</th>--%>
                            <th height="30" >Credit Days</th>
                            <th height="30" >TIN NO</th>
                            <th height="30" >Address</th>
                            <th height="30" >PhoneNo</th>
                            <th height="30" >Mail Id</th>
                            <th height="30" >Credit Vendor?</th>
                            <th height="30" >Status</th>
                            <th height="30" >Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <% int sno = 0;
                            int index = 1;%>
                        <c:forEach items="${VendorList}" var="list"> 

                            <%

                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "form-control";
                                } else {
                                    classText = "form-control";
                                }
                            %>

                            <tr  width="208" height="40" > 
                                <td class="<%=classText%>" ><%=index%></td>
                                <td class="<%=classText%>" ><c:out value="${list.vendorName}"/></td>
                                <td class="<%=classText%>" ><c:out value="${list.vendorTypeValue}"/></td>
<%--                                <td class="<%=classText%>" ><c:out value="${list.priceType}"/></td>--%>
                                <td class="<%=classText%>" ><c:out value="${list.creditDays}"/></td>
                                <td class="<%=classText%>" ><c:out value="${list.tinNo}"/></td>
                                <td class="<%=classText%>" ><c:out value="${list.vendorAddress}"/></td>
                                <td class="<%=classText%>" ><c:out value="${list.vendorPhoneNo}"/></td>
                                <td class="<%=classText%>" ><c:out value="${list.vendorMailId}"/></td>


                                <td class="<%=classText%>" >                                
                                    <c:if test = "${list.settlementType == '1' }" >
                                        No
                                    </c:if>
                                    <c:if test = "${list.settlementType == '2'}" >
                                        Yes
                                    </c:if>
                                </td>
                                <td class="<%=classText%>" height="30">                                
                                    <c:if test = "${list.activeInd == 'Y' }" >
                                        Active 
                                    </c:if>
                                    <c:if test = "${list.activeInd == 'N'}" >
                                        InActive
                                    </c:if>
                                </td>
                                <td class="<%=classText%>" align="left"> 
                                    <a href="/throttle/alterVendorPage.do?vendorId=<c:out value='${list.vendorId}' />">
                                        <span class="label label-warning"><spring:message code="trucks.label.Edit"  text="alter"/> </span> 
                                    </a>
                                </td>
                            </tr>
                            <%
                                index++;
                            %>
                        </c:forEach>
                    </tbody>
                </table>
                <input type="hidden" name="count" id="count" value="<%=sno%>" />
            </c:if>
            <center>
                <br>
                <input type="button" name="add" value="Add" id="buttonDesign" onClick="submitPage(this.name)" class="btn btn-info">
                <c:if test = "${VendorList != null}" >
                    <!-- <input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="button">-->
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>


            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
   </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>