<%--
    Document   : customerContractMaster
    Created on : Oct 29, 2013, 10:39:28 AM
    Author     : Arul
--%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>



<c:if test="${billingTypeId == 1}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.

            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="tInput"  style="width: 60px;"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:235px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input name="ptpPickupPoint" value="" class="tInput" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="tInput" id="ptpPickupPointId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint1" value="" class="tInput" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="tInput" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="tInput" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="tInput" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="tInput" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="tInput" id="interimPoint1RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint2" value="" class="tInput" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="tInput" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="tInput" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="tInput" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="tInput" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="tInput" id="interimPoint2RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint3" value="" class="tInput" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="tInput" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="tInput" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="tInput" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="tInput" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="tInput" id="interimPoint3RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint4" value="" class="tInput" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="tInput" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="tInput" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="tInput" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="tInput" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="tInput" id="interimPoint4RouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="tInput" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="tInput" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="tInput" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="tInput" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="tInput" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="tInput" id="ptpDropPointRouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="tInput" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="tInput" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="tInput" id="ptpTotalMinutes"  style="width: 90px;"/></td>',
                '<td><input name="ptpRateWithReefer" value="" class="tInput" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="ptpRateWithoutReefer" value="" class="tInput" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                count++;
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');

                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find('#ptpPickupPoint').val('');$row.find('#ptpPickupPointId').val('');$row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint1').focus();$row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');  $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');  $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');  $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint2').focus();$row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint3').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint4').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val(''); $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');  $row.find("#ptpDropPointMinutes").val('');  $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');$row.find('#ptpDropPoint').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                            $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                            $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            if ($row.find('#interimPoint1Km').val() !== '') {
                                var interimPoint1Km = $row.find('#interimPoint1Km').val();
                            } else {
                                var interimPoint1Km = 0;
                            }
                            if ($row.find('#interimPoint2Km').val() !== '') {
                                var interimPoint2Km = $row.find('#interimPoint2Km').val();
                            } else {
                                var interimPoint2Km = 0;
                            }
                            if ($row.find('#interimPoint3Km').val() !== '') {
                                var interimPoint3Km = $row.find('#interimPoint3Km').val();
                            } else {
                                var interimPoint3Km = 0;
                            }
                            if ($row.find('#interimPoint4Km').val() !== '') {
                                var interimPoint4Km = $row.find('#interimPoint4Km').val();
                            } else {
                                var interimPoint4Km = 0;
                            }


                            if ($row.find('#interimPoint1Hrs').val() !== '') {
                                var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                            } else {
                                var interimPoint1Hrs = 0;
                            }
                            if ($row.find('#interimPoint2Hrs').val() !== '') {
                                var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                            } else {
                                var interimPoint2Hrs = 0;
                            }
                            if ($row.find('#interimPoint3Hrs').val() !== '') {
                                var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                            } else {
                                var interimPoint3Hrs = 0;
                            }
                            if ($row.find('#interimPoint4Hrs').val() !== '') {
                                var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                            } else {
                                var interimPoint4Hrs = 0;
                            }

                            if ($row.find('#interimPoint1Minutes').val() !== '') {
                                var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                            } else {
                                var interimPoint1Minutes = 0;
                            }
                            if ($row.find('#interimPoint2Minutes').val() !== '') {
                                var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                            } else {
                                var interimPoint2Minutes = 0;
                            }
                            if ($row.find('#interimPoint3Minutes').val() !== '') {
                                var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                            } else {
                                var interimPoint3Minutes = 0;
                            }
                            if ($row.find('#interimPoint4Minutes').val() !== '') {
                                var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                            } else {
                                var interimPoint4Minutes = 0;
                            }


                            var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                            $row.find('#ptpTotalKm').val(totalKm);
                            var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                            $row.find('#ptpTotalHours').val(totalHrs);
                            var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                            $row.find('#ptpTotalMinutes').val(totalMinutes);
                            $itemrow.find('#ptpRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();



                } // End if last itemCode input is empty
                return false;
            });

        });// End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });


        $(document).ready(function() {
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $('#ptpPickupPoint').val('');$('#ptpPickupPointId').val('');$("#interimPoint1").val(''); $("#interimPointId1").val('');
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#ptpPickupPoint').focus();
                                $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                    $itemrow.find('#interimPoint1').focus();
                    $("#interimPoint1").val(''); $("#interimPointId1").val('');
                    $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint1').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint1").val(''); $("#interimPointId1").val('');
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint1').focus();$("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint1").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint1').val(tmp[0]);
                    $itemrow.find('#interimPointId1').val(tmp[1]);
                    $itemrow.find('#interimPoint1Km').val(tmp[2]);
                    $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint2').focus();
                    $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');  $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');  $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');  $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint2').focus();$("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint2").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint2').val(tmp[0]);
                    $itemrow.find('#interimPointId2').val(tmp[1]);
                    $itemrow.find('#interimPoint2Km').val(tmp[2]);
                    $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint3').focus();
                    $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint3').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint3").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint3').val(tmp[0]);
                    $itemrow.find('#interimPointId3').val(tmp[1]);
                    $itemrow.find('#interimPoint3Km').val(tmp[2]);
                    $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint4').focus();
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                    $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                    $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                    $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                                $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                                $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                                $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                                $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint4').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint4").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint4').val(tmp[0]);
                    $itemrow.find('#interimPointId4').val(tmp[1]);
                    $itemrow.find('#interimPoint4Km').val(tmp[2]);
                    $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                    $itemrow.find('#ptpDropPoint').focus();
                    $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#ptpDropPointKm").val(''); $("#ptpDropPointHrs").val('');
                    $("#ptpDropPointMinutes").val(''); $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#ptpDropPointKm").val('');
                                $("#ptpDropPointHrs").val('');  $("#ptpDropPointMinutes").val('');  $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpTotalMinutes").val('');
                                $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');$('#ptpDropPoint').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                    $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                    $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                    $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                    $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    if (document.getElementById("interimPoint1Km").value != '') {
                        var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                    } else {
                        var interimPoint1Km = 0;
                    }
                    if (document.getElementById("interimPoint2Km").value != '') {
                        var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                    } else {
                        var interimPoint2Km = 0;
                    }
                    if (document.getElementById("interimPoint3Km").value != '') {
                        var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                    } else {
                        var interimPoint3Km = 0;
                    }
                    if (document.getElementById("interimPoint4Km").value != '') {
                        var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                    } else {
                        var interimPoint4Km = 0;
                    }

                    if (document.getElementById("interimPoint1Hrs").value != '') {
                        var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                    } else {
                        var interimPoint1Hrs = 0;
                    }
                    if (document.getElementById("interimPoint2Hrs").value != '') {
                        var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                    } else {
                        var interimPoint2Hrs = 0;
                    }
                    if (document.getElementById("interimPoint3Hrs").value != '') {
                        var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                    } else {
                        var interimPoint3Hrs = 0;
                    }
                    if (document.getElementById("interimPoint4Hrs").value != '') {
                        var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                    } else {
                        var interimPoint4Hrs = 0;
                    }

                    if (document.getElementById("interimPoint1Minutes").value != '') {
                        var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                    } else {
                        var interimPoint1Minutes = 0;
                    }
                    if (document.getElementById("interimPoint2Minutes").value != '') {
                        var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                    } else {
                        var interimPoint2Minutes = 0;
                    }
                    if (document.getElementById("interimPoint3Minutes").value != '') {
                        var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                    } else {
                        var interimPoint3Minutes = 0;
                    }
                    if (document.getElementById("interimPoint4Minutes").value != '') {
                        var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                    } else {
                        var interimPoint4Minutes = 0;
                    }

                    var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                    $itemrow.find('#ptpTotalKm').val(totalKm);
                    var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                    $itemrow.find('#ptpTotalHours').val(totalHrs);
                    var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                    $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                    $itemrow.find('#ptpRateWithReefer').focus();
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };

        });
    </script>


</c:if>
<c:if test="${billingTypeId == 4}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.

            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="tInput"  style="width: 60px;"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:235px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input name="ptpPickupPoint" value="" class="tInput" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="tInput" id="ptpPickupPointId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint1" value="" class="tInput" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="tInput" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="tInput" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="tInput" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="tInput" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="tInput" id="interimPoint1RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint2" value="" class="tInput" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="tInput" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="tInput" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="tInput" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="tInput" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="tInput" id="interimPoint2RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint3" value="" class="tInput" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="tInput" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="tInput" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="tInput" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="tInput" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="tInput" id="interimPoint3RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint4" value="" class="tInput" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="tInput" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="tInput" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="tInput" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="tInput" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="tInput" id="interimPoint4RouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="tInput" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="tInput" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="tInput" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="tInput" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="tInput" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="tInput" id="ptpDropPointRouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="tInput" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="tInput" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="tInput" id="ptpTotalMinutes"  style="width: 90px;"/><input type="hidden" name="ptpRateWithReefer" value="0" class="tInput" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="ptpRateWithoutReefer" value="0" class="tInput" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                count++;
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');

                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find('#ptpPickupPoint').val('');$row.find('#ptpPickupPointId').val('');$row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint1").val(''); $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val(''); $row.find("#interimPoint1Km").val(''); $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint1Hrs").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint1Minutes").val(''); $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val(''); $row.find("#interimPoint1RouteId").val(''); $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint1').focus();$row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val(''); $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');  $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint2").val(''); $row.find("#interimPointId2").val(''); $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val(''); $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');  $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');  $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint2').focus();$row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint3").val(''); $row.find("#interimPointId3").val(''); $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val(''); $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val(''); $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val(''); $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val(''); $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint3').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val(''); $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val(''); $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val(''); $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val(''); $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val(''); $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val(''); $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val(''); $row.find('#interimPoint4').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val(''); $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val(''); $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val(''); $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#ptpDropPoint").val(''); $row.find("#ptpDropPointId").val('');  $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');  $row.find("#ptpDropPointMinutes").val('');  $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val(''); $row.find("#ptpTotalHours").val(''); $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');$row.find('#ptpDropPoint').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                            $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                            $row.find("#ptpRateWithReefer").val('');$row.find("#ptpRateWithoutReefer").val('');
                            if ($row.find('#interimPoint1Km').val() !== '') {
                                var interimPoint1Km = $row.find('#interimPoint1Km').val();
                            } else {
                                var interimPoint1Km = 0;
                            }
                            if ($row.find('#interimPoint2Km').val() !== '') {
                                var interimPoint2Km = $row.find('#interimPoint2Km').val();
                            } else {
                                var interimPoint2Km = 0;
                            }
                            if ($row.find('#interimPoint3Km').val() !== '') {
                                var interimPoint3Km = $row.find('#interimPoint3Km').val();
                            } else {
                                var interimPoint3Km = 0;
                            }
                            if ($row.find('#interimPoint4Km').val() !== '') {
                                var interimPoint4Km = $row.find('#interimPoint4Km').val();
                            } else {
                                var interimPoint4Km = 0;
                            }


                            if ($row.find('#interimPoint1Hrs').val() !== '') {
                                var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                            } else {
                                var interimPoint1Hrs = 0;
                            }
                            if ($row.find('#interimPoint2Hrs').val() !== '') {
                                var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                            } else {
                                var interimPoint2Hrs = 0;
                            }
                            if ($row.find('#interimPoint3Hrs').val() !== '') {
                                var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                            } else {
                                var interimPoint3Hrs = 0;
                            }
                            if ($row.find('#interimPoint4Hrs').val() !== '') {
                                var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                            } else {
                                var interimPoint4Hrs = 0;
                            }

                            if ($row.find('#interimPoint1Minutes').val() !== '') {
                                var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                            } else {
                                var interimPoint1Minutes = 0;
                            }
                            if ($row.find('#interimPoint2Minutes').val() !== '') {
                                var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                            } else {
                                var interimPoint2Minutes = 0;
                            }
                            if ($row.find('#interimPoint3Minutes').val() !== '') {
                                var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                            } else {
                                var interimPoint3Minutes = 0;
                            }
                            if ($row.find('#interimPoint4Minutes').val() !== '') {
                                var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                            } else {
                                var interimPoint4Minutes = 0;
                            }


                            var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                            $row.find('#ptpTotalKm').val(totalKm);
                            var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                            $row.find('#ptpTotalHours').val(totalHrs);
                            var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                            $row.find('#ptpTotalMinutes').val(totalMinutes);
                            $itemrow.find('#ptpRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();



                } // End if last itemCode input is empty
                return false;
            });

        });// End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });


        $(document).ready(function() {
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $('#ptpPickupPoint').val('');$('#ptpPickupPointId').val('');$("#interimPoint1").val(''); $("#interimPointId1").val('');
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#ptpPickupPoint').focus();
                                $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                    $itemrow.find('#interimPoint1').focus();
                    $("#interimPoint1").val(''); $("#interimPointId1").val('');
                    $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint1').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint1").val(''); $("#interimPointId1").val('');
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val(''); $("#interimPoint1Km").val(''); $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint1Hrs").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint1Minutes").val(''); $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val(''); $("#interimPoint1RouteId").val(''); $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint1').focus();$("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint1").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint1').val(tmp[0]);
                    $itemrow.find('#interimPointId1').val(tmp[1]);
                    $itemrow.find('#interimPoint1Km').val(tmp[2]);
                    $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint2').focus();
                    $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val(''); $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');  $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint2").val(''); $("#interimPointId2").val(''); $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val(''); $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');  $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');  $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint2').focus();$("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint2").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint2').val(tmp[0]);
                    $itemrow.find('#interimPointId2').val(tmp[1]);
                    $itemrow.find('#interimPoint2Km').val(tmp[2]);
                    $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint3').focus();
                    $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                    $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                    $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                    $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint3").val(''); $("#interimPointId3").val(''); $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                                $("#interimPoint3Km").val(''); $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                                $("#interimPoint3Hrs").val(''); $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                                $("#interimPoint3Minutes").val(''); $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                                $("#interimPoint3RouteId").val(''); $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint3').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint3").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint3').val(tmp[0]);
                    $itemrow.find('#interimPointId3').val(tmp[1]);
                    $itemrow.find('#interimPoint3Km').val(tmp[2]);
                    $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint4').focus();
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                    $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                    $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                    $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val(''); $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val(''); $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                                $("#interimPoint4Km").val(''); $("#ptpDropPointKm").val('');
                                $("#interimPoint4Hrs").val(''); $("#ptpDropPointHrs").val('');
                                $("#interimPoint4Minutes").val(''); $("#ptpDropPointMinutes").val('');
                                $("#interimPoint4RouteId").val(''); $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                                $("#ptpTotalMinutes").val(''); $('#interimPoint4').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint4").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint4').val(tmp[0]);
                    $itemrow.find('#interimPointId4').val(tmp[1]);
                    $itemrow.find('#interimPoint4Km').val(tmp[2]);
                    $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                    $itemrow.find('#ptpDropPoint').focus();
                    $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');
                    $("#ptpDropPointKm").val(''); $("#ptpDropPointHrs").val('');
                    $("#ptpDropPointMinutes").val(''); $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val(''); $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val(''); $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#ptpDropPoint").val(''); $("#ptpDropPointId").val('');  $("#ptpDropPointKm").val('');
                                $("#ptpDropPointHrs").val('');  $("#ptpDropPointMinutes").val('');  $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val(''); $("#ptpTotalHours").val(''); $("#ptpTotalMinutes").val('');
                                $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');$('#ptpDropPoint').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                    $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                    $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                    $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                    $("#ptpRateWithReefer").val('');$("#ptpRateWithoutReefer").val('');
                    if (document.getElementById("interimPoint1Km").value != '') {
                        var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                    } else {
                        var interimPoint1Km = 0;
                    }
                    if (document.getElementById("interimPoint2Km").value != '') {
                        var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                    } else {
                        var interimPoint2Km = 0;
                    }
                    if (document.getElementById("interimPoint3Km").value != '') {
                        var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                    } else {
                        var interimPoint3Km = 0;
                    }
                    if (document.getElementById("interimPoint4Km").value != '') {
                        var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                    } else {
                        var interimPoint4Km = 0;
                    }

                    if (document.getElementById("interimPoint1Hrs").value != '') {
                        var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                    } else {
                        var interimPoint1Hrs = 0;
                    }
                    if (document.getElementById("interimPoint2Hrs").value != '') {
                        var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                    } else {
                        var interimPoint2Hrs = 0;
                    }
                    if (document.getElementById("interimPoint3Hrs").value != '') {
                        var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                    } else {
                        var interimPoint3Hrs = 0;
                    }
                    if (document.getElementById("interimPoint4Hrs").value != '') {
                        var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                    } else {
                        var interimPoint4Hrs = 0;
                    }

                    if (document.getElementById("interimPoint1Minutes").value != '') {
                        var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                    } else {
                        var interimPoint1Minutes = 0;
                    }
                    if (document.getElementById("interimPoint2Minutes").value != '') {
                        var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                    } else {
                        var interimPoint2Minutes = 0;
                    }
                    if (document.getElementById("interimPoint3Minutes").value != '') {
                        var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                    } else {
                        var interimPoint3Minutes = 0;
                    }
                    if (document.getElementById("interimPoint4Minutes").value != '') {
                        var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                    } else {
                        var interimPoint4Minutes = 0;
                    }

                    var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                    $itemrow.find('#ptpTotalKm').val(totalKm);
                    var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                    $itemrow.find('#ptpTotalHours').val(totalHrs);
                    var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                    $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                    $itemrow.find('#ptpRateWithReefer').focus();
                    return false;

                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };

        });
    </script>


</c:if>
<c:if test="${billingTypeId == 2}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable1');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.
            var count = 0;
            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="" class="tInput" style="width:120px"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpwVehicleTypeId" id="ptpwVehicleTypeId"  class="form-control" style="width:235px"><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input type="hidden" name="ptpwPickupPointId" id="ptpwPickupPointId" value="" class="tInput"  style="width: 120px;"/><input name="ptpwPickupPoint" id="ptpwPickupPoint" value="" class="tInput"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>',
                '<td><input name="ptpwDropPoint" id="ptpwDropPoint" value="" class="tInput"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpwDropPointId" id="ptpwDropPointId" value="" class="tInput"  style="width: 120px;"/><input type="hidden" name="ptpwPointRouteId" id="ptpwPointRouteId" value="" class="tInput"  style="width: 120px;"/></td>',
                '<td><input name="ptpwTotalKm" id="ptpwTotalKm" value="" class="tInput"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" readonly/><input type="hidden" name="ptpwTotalHrs" id="ptpwTotalHrs" value="" class="tInput"  style="width: 120px;"/><input type="hidden" name="ptpwTotalMinutes" id="ptpwTotalMinutes" value="" class="tInput"  style="width: 120px;"/></td>',
                '<td><input name="ptpwRateWithReefer" id="ptpwRateWithReefer" value="" class="tInput"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>',
                '<td><input name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer" value="" class="tInput"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');

            // Add row to list and allow user to use autocomplete to find items.
            $("#addRow1").bind('click', function() {
                var $row = $(rowTemp);
                count++;
                // save reference to inputs within row
                var $ptpwRouteContractCode = $row.find('#ptpwRouteContractCode');
                var $ptpwVehicleTypeId = $row.find('#ptpwVehicleTypeId');
                var $ptpwPickupPointId = $row.find('#ptpwPickupPointId');
                var $ptpwPickupPoint = $row.find('#ptpwPickupPoint');
                var $ptpwDropPoint = $row.find('#ptpwDropPoint');
                var $ptpwDropPointId = $row.find('#ptpwDropPointId');
                var $ptpwPointRouteId = $row.find('#ptpwPointRouteId');
                var $ptpwTotalKm = $row.find('#ptpwTotalKm');
                var $ptpwTotalHrs = $row.find('#ptpwTotalHrs');
                var $ptpwTotalMinutes = $row.find('#ptpwTotalMinutes');
                var $ptpwRateWithReefer = $row.find('#ptpwRateWithReefer');
                var $ptpwRateWithoutReefer = $row.find('#ptpwRateWithoutReefer');

                if ($('#ptpwRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    $row.find('#ptpwPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwDropPoint: $row.find('#ptpwPointRouteId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#ptpwPickupPoint").val('');
                                        $row.find("#ptpwPickupPointId").val('');
                                        $row.find('#ptpwDropPoint').val('');
                                        $row.find('#ptpwDropPointId').val('');
                                        $row.find('#ptpwTotalKm').val('');
                                        $row.find('#ptpwTotalHrs').val('');
                                        $row.find('#ptpwTotalMinutes').val('');
                                        $row.find('#ptpwPointRouteId').val('');
                                        $row.find('#ptpwRateWithReefer').val('');
                                        $row.find('#ptpwRateWithoutReefer').val('');
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpwPickupPointId').val(tmp[1]);

                            $itemrow.find('#ptpwDropPoint').val('');
                            $itemrow.find('#ptpwDropPointId').val('');
                            $itemrow.find('#ptpwTotalKm').val('');
                            $itemrow.find('#ptpwTotalHrs').val('');
                            $itemrow.find('#ptpwTotalMinutes').val('');
                            $itemrow.find('#ptpwPointRouteId').val('');
                            $itemrow.find('#ptpwRateWithReefer').val('');
                            $itemrow.find('#ptpwRateWithoutReefer').val('');
                            $itemrow.find('#ptpwDropPoint').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };
                    $row.find('#ptpwDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwPickupPoint: $row.find('#ptpwPickupPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if(items == ''){
                                        alert("Invalid Route Point");
                                        $row.find("#ptpwDropPoint").val('');
                                        $row.find("#ptpwDropPointId").val('');
                                        $row.find("#ptpwPointRouteId").val('');
                                        $row.find("#ptpwTotalKm").val('');
                                        $row.find("#ptpwTotalHrs").val('');
                                        $row.find("#ptpwTotalMinutes").val('');
                                        $row.find("#ptpwRateWithReefer").val('');
                                        $row.find("#ptpwRateWithoutReefer").val('');
                                        $row.find('#ptpwDropPoint').focus();
                                    }else{
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                            $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                            $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                            $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                            $itemrow.find('#ptpwRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpwRouteContractCode).focus();

                } // End if last itemCode input is empty
                return false;
            });

        });// End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });




        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#ptpwPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwDropPoint: document.getElementById("ptpwDropPoint").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            //alert(items);
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#ptpwPickupPoint").val('');
                                $("#ptpwPickupPointId").val('');
                                $('#ptpwDropPoint').val('');
                                $('#ptpwDropPointId').val('');
                                $('#ptpwTotalKm').val('');
                                $('#ptpwTotalHrs').val('');
                                $('#ptpwTotalMinutes').val('');
                                $('#ptpwPointRouteId').val('');
                                $('#ptpwRateWithReefer').val('');
                                $('#ptpwRateWithoutReefer').val('');
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },

                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpwPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpwPickupPointId').val(tmp[1]);
                    if($itemrow.find('#ptpwDropPoint').val() != ''){
                        $itemrow.find('#ptpwDropPoint').val('');
                        $itemrow.find('#ptpwDropPointId').val('');
                        $itemrow.find('#ptpwTotalKm').val('');
                        $itemrow.find('#ptpwTotalHrs').val('');
                        $itemrow.find('#ptpwTotalMinutes').val('');
                        $itemrow.find('#ptpwPointRouteId').val('');
                        $itemrow.find('#ptpwRateWithReefer').val('');
                        $itemrow.find('#ptpwRateWithoutReefer').val('');
                    }
                    $itemrow.find('#ptpwDropPoint').focus();
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };

            $('#ptpwDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwPickupPoint: document.getElementById("ptpwPickupPointId").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                alert("Invalid Route Point");
                                $("#ptpwDropPoint").val('');
                                $("#ptpwDropPointId").val('');
                                $("#ptpwPointRouteId").val('');
                                $("#ptpwTotalKm").val('');
                                $("#ptpwTotalHrs").val('');
                                $("#ptpwTotalMinutes").val('');
                                $("#ptpwRateWithReefer").val('');
                                $("#ptpwRateWithoutReefer").val('');
                                $('#ptpwDropPoint').focus();
                            }else{
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpwDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                    $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                    $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                    $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                    $itemrow.find('#ptpwRateWithReefer').focus();
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
        });

    </script>

</c:if>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };

        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term,
                        customerName: document.getElementById('customerName').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerCode").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[2] + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };

    });
</script>


<script type="text/javascript">
    function submitPage(value) {
        var validate = "";
        if (document.getElementById('contractFrom').value == "") {
            alert("Please Select Contract Start Date");
            document.getElementById('contractFrom').focus();
        } else if (document.getElementById('contractTo').value == "") {
            alert("Please Select Contract End Date");
            document.getElementById('contractTo').focus();
        } else if (document.getElementById('contractNo').value == "") {
            alert("Please Enter Contract No");
            document.getElementById('contractNo').focus();
        } else if (document.getElementById('billingTypeId').value == "1") {
            validate = validatePtpContract();
            if(validate == 'true'){
                document.customerContract.action = "/throttle/saveCustomerContract.do";
                document.customerContract.submit();
            }
        } else if (document.getElementById('billingTypeId').value == "2") {
            validate = validatePtpwContract();
            if(validate == 'true'){
                document.customerContract.action = "/throttle/saveCustomerContract.do";
                document.customerContract.submit();
            }
        } else if (document.getElementById('billingTypeId').value == "3") {
            document.customerContract.action = "/throttle/saveCustomerContract.do";
            document.customerContract.submit();
        } else if (document.getElementById('billingTypeId').value == "4") {
             validate = validateFixedContract();
             if(validate == 'true'){
                document.customerContract.action = "/throttle/saveCustomerContract.do";
                document.customerContract.submit();
             }
        } else if(validate == 'true'){
            document.customerContract.action = "/throttle/saveCustomerContract.do";
            document.customerContract.submit();
        }
    }

   
    function validatePtpContract(){
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');

        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');

        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');

        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');

        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');

        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');

        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');

        //Rate Details
        var ptpRateWithReefer = document.getElementsByName('ptpRateWithReefer');
        var ptpRateWithoutReefer = document.getElementsByName('ptpRateWithoutReefer');

        for(var i=0;i<ptpRouteContractCode.length;i++){
            var j = i+1;
            if(ptpRouteContractCode[i].value == ''){
                alert("Please Enter The Valid Route Code for row "+j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpVehicleTypeId[i].value == '0'){
                alert("Please Select The vehicle Type for row "+j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpPickupPoint[i].value == ''){
                alert("please select the pickup point for row "+j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == ''){
                alert("please select the valid point name for pickup point at row "+j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            }else if(interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select the valid point name for interim point 1 at row "+j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 2 at row "+j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 3 at row "+j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 4 at row "+j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for Drop point at row "+j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpTotalKm[i].value == ''){
                alert("please validate the route points at row "+j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpRateWithReefer[i].value == ''){
                alert("Please enter the freight rate with reefer at row "+j);
                ptpRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpRateWithoutReefer[i].value == ''){
                alert("please enter the freight rate without reefer at row "+j);
                ptpRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else{
                returnvalue = 'true';

            }
        }
        return returnvalue;
    }

    function validateFixedContract(){
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');

        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');

        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');

        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');

        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');

        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');

        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');

        //Rate Details

        for(var i=0;i<ptpRouteContractCode.length;i++){
            var j = i+1;
            if(ptpRouteContractCode[i].value == ''){
                alert("Please Enter The Valid Route Code for row "+j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpVehicleTypeId[i].value == '0'){
                alert("Please Select The vehicle Type for row "+j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpPickupPoint[i].value == ''){
                alert("please select the pickup point for row "+j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == ''){
                alert("please select the valid point name for pickup point at row "+j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            }else if(interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select the valid point name for interim point 1 at row "+j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 2 at row "+j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 3 at row "+j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for interim point 4 at row "+j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != ''){
                alert("please select valid point name for Drop point at row "+j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            }else if(ptpTotalKm[i].value == ''){
                alert("please validate the route points at row "+j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else{
                returnvalue = 'true';

            }
        }
        return returnvalue;
    }

    function validatePtpwContract(){
        var returnvalue = "";
        var ptpwRouteContractCode = document.getElementsByName('ptpwRouteContractCode');
        var ptpwVehicleTypeId = document.getElementsByName('ptpwVehicleTypeId');
        var ptpwPickupPoint = document.getElementsByName('ptpwPickupPoint');
        var ptpwPickupPointId = document.getElementsByName('ptpwPickupPointId');
        var ptpwDropPoint = document.getElementsByName('ptpwDropPoint');
        var ptpwDropPointId = document.getElementsByName('ptpwDropPointId');
        var ptpwTotalKm = document.getElementsByName('ptpwTotalKm');
        var ptpwRateWithReefer = document.getElementsByName('ptpwRateWithReefer');
        var ptpwRateWithoutReefer = document.getElementsByName('ptpwRateWithoutReefer');
        //alert(ptpwRouteContractCode.length);
        for(var i=0; i<ptpwRouteContractCode.length; i++){
            //alert("i=="+i)
            var j = i+1;
            if(ptpwRouteContractCode[i].value == ''){
                alert("Please Enter Contract Route Code For Row "+j);
                ptpwRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwVehicleTypeId[i].value == '0'){
                alert("Please Select Vehicle Type For Row " +j);
                ptpwVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwPickupPoint[i].value == ''){
                alert("Please Select The Pickup Point For Row " +j);
                ptpwPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwPickupPoint[i].value != '' && ptpwPickupPointId[i].value == ''){
                alert("Please Select The Pickup Point For Row " +j);
                ptpwPickupPoint[i].focus();
                ptpwPickupPoint[i].value='';
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwDropPointId[i].value == ''){
                alert("Please Select The Drop Point For Row " +j);
                ptpwDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwDropPoint[i].value != '' && ptpwDropPointId[i] == ''){
                alert("Please Select The Drop Point For Row " +j);
                ptpwDropPoint[i].focus();
                ptpwDropPoint[i].value='';
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwTotalKm[i].value == ''){
                alert("Validate The Route Point Details and check Total Km For Row " +j);
                ptpwTotalKm[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwRateWithReefer[i].value == ''){
                alert("Please enter the amount for rate with reefer for row " +j);
                ptpwRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if(ptpwRateWithoutReefer[i].value == ''){
                alert("Please enter the amount for rate without reefer for row " +j);
                ptpwRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            }else{
                returnvalue = 'true';
            }
        }
        return returnvalue;
    }




    function  validateFixedKmRateDetails(){
        var sts = true ;
        var fixedKmvehicleTypeId = document.getElementsByName("fixedKmvehicleTypeId");
        var vehicleNos = document.getElementsByName("vehicleNos");
        var fixedTotalKm = document.getElementsByName("fixedTotalKm");
        var fixedTotalHm = document.getElementsByName("fixedTotalHm");
        var fixedRateWithReefer = document.getElementsByName("fixedRateWithReefer");
        var extraKmRateWithReefer = document.getElementsByName("extraKmRateWithReefer");
        var extraHmRateWithReefer = document.getElementsByName("extraHmRateWithReefer");
        var fixedRateWithoutReefer = document.getElementsByName("fixedRateWithoutReefer");
        var extraKmRateWithoutReefer = document.getElementsByName("extraKmRateWithoutReefer");
        for(var i=0; i<fixedKmvehicleTypeId.length; i++){
            if(fixedKmvehicleTypeId[i].value == '0'){
                alert("Please select vehicle type");
                fixedKmvehicleTypeId[i].focus();
                sts = false;
            }else if(vehicleNos[i].value  == ''){
                alert("Please enter vehicle nos");
                vehicleNos[i].focus();
                sts = false;
            }else if(fixedTotalKm[i].value  == ''){
                alert("Please enter total km");
                fixedTotalKm[i].focus();
                sts = false;
            }else if(fixedTotalHm[i].value  == ''){
                alert("Please enter total hm");
                fixedTotalHm[i].focus();
                sts = false;
            }else if(fixedRateWithReefer[i].value  == ''){
                alert("Please enter rate with reefer");
                fixedRateWithReefer[i].focus();
                sts = false;
            }else if(extraKmRateWithReefer[i].value  == ''){
                alert("Please enter extra km rate with reefer");
                extraKmRateWithReefer[i].focus();
                sts = false;
            }else if(extraHmRateWithReefer[i].value  == ''){
                alert("Please enter extra hm rate with reefer");
                extraHmRateWithReefer[i].focus();
                sts = false;
            }else if(fixedRateWithoutReefer[i].value  == ''){
                alert("Please enter rate without reefer");
                fixedRateWithoutReefer[i].focus();
                sts = false;
            }else if(extraKmRateWithoutReefer[i].value  == ''){
                alert("Please enter extra km rate without reefer");
                extraKmRateWithoutReefer[i].focus();
                sts = false;
            }else {
                sts = true;
            }
        }
        return sts;
    }
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <c:if test="${billingTypeId != 4}">
        <body onload="">
        </c:if>
        <c:if test="${billingTypeId == 4}">
        <body onload="addRow()">
        </c:if>
        <form name="customerContract"  method="post" >
            <%@ include file="/content/common/path.jsp" %>

            <br>

            <table width="980" align="center" cellpadding="0" cellspacing="0" class="border">

                <tr>
                    <td class="contenthead" colspan="4" >Customer Contract Master</td>
                </tr>

                <tr>
                    <td class="text1">Vendor Name</td>
                    <td class="text1"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control"><input type="text" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" readonly=""></td>
                    <td class="text1">Vendor Type</td>
                    <td class="text1"><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" readonly=""></td>
                </tr>
                <tr>
                    <td class="text2">Contract From</td>
                    <td class="text2"><input type="text" name="contractFrom" id="contractFrom" valu="" class="datepicker"></td>
                    <td class="text2">Contract To</td>
                    <td class="text2"><input type="text" name="contractTo" id="contractTo" value="" class="datepicker"></td>
                </tr>
               
            </table>
            <br>

            <div id="tabs" >
<!--                <ul>
                    <c:if test="${billingTypeId == 1}">
                        <li id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                    </c:if>
                    <c:if test="${billingTypeId == 2}">
                        <li id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                    </c:if>
                    <c:if test="${billingTypeId == 3}">
                        <li id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                    </c:if>
                    <c:if test="${billingTypeId == 4}">
                        <li><a href="#revenueDetails"><span>Fixed KM - Based - Rate Details </span></a></li>
                        <li id="fkmDetails" style="display: none"><a href="#fkm"><span>Fixed KM - Based - Route Details </span></a></li>
                    </c:if>
                </ul>-->





                <c:if test="${billingTypeId == 1}">
                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table id="itemsTable" class="general">
                                <tr>
                                    <td></td>
                                    <td class="contenthead" height="80" style="width: 10px;"><font color="red">*</font>Vehicle Route Contract Code</td>
                                    <td class="contenthead" height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</td>
                                    <td class="contenthead" height="30" style="width: 10px;"><font color="red">*</font>Origin</td>
                                    <td class="contenthead" height="30" ><font color="red">*</font>Destination</td>
                                    <td class="contenthead" height="30" style="width: 90px;" ><font color="red">*</font>Total Km</td>
                                    <td class="contenthead" height="30" style="width: 90px;" ><font color="red">*</font>Rate </td>
                                    <td class="contenthead" height="30" style="width: 90px;" ><font color="red">*</font>Status</td>
                                </tr>
                                <tbody>
                                    <tr class="item-row">
                                        <td></td>
                                        <td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="tInput"  style="width: 60px;"/></td>
                                        <td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>
                                        <td><input name="ptpPickupPoint" value="" class="tInput" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.name)"/><input type="hidden" name="ptpPickupPointId" value="" class="tInput" id="ptpPickupPointId"  style="width: 90px;"/></td>
                                        <td><input name="interimPoint1" value="" class="tInput" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId1" value="" class="tInput" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="tInput" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="tInput" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="tInput" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="tInput" id="interimPoint1RouteId"  style="width: 90px;"/></td>
                                        <td><input name="interimPoint2" value="" class="tInput" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId2" value="" class="tInput" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="tInput" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="tInput" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="tInput" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="tInput" id="interimPoint2RouteId"  style="width: 90px;"/></td>
                                        <td><input name="interimPoint3" value="" class="tInput" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId3" value="" class="tInput" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="tInput" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="tInput" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="tInput" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="tInput" id="interimPoint3RouteId"  style="width: 90px;"/></td>
                                        <td><input name="interimPoint4" value="" class="tInput" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId4" value="" class="tInput" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="tInput" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="tInput" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="tInput" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="tInput" id="interimPoint4RouteId"  style="width: 90px;"/></td>
                                        <td><input name="ptpDropPoint" value="" class="tInput" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpDropPointId" value="" class="tInput" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="tInput" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="tInput" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="tInput" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="tInput" id="ptpDropPointRouteId"  style="width: 90px;"/></td>
                                        <td><input name="ptpTotalKm" value="" class="tInput" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="tInput" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="tInput" id="ptpTotalMinutes"  style="width: 90px;"/></td>
                                        <td><input name="ptpRateWithReefer" value="" class="tInput" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                        <td><input name="ptpRateWithoutReefer" value="" class="tInput" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                    </tr>
                                </tbody>
                            </table>
                            <a href="" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Code</span></a>
                            <br>
                            <br>
                            <center>
                                <input type="button" class="button" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                            </center>
                        </div>
                    </div>
                </c:if>
               

        </div>

        <script>
                    $(".nexttab").click(function() {
                        var chek = validateFixedKmRateDetails();
//                        alert(chek);
                        if(chek == true){
                        $("#fkmDetails").show();
                        $("#fkm").show();
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                        }
                    });

        </script>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
