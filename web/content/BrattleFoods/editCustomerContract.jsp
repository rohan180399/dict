<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>


<script type="text/javascript">
    function submitModelFuel() {
//            var fuelModelId = document.getElementById("fuelModelId").value;
//            alert(fuelModelId)
        var contractRateId = document.getElementById("contractRateIds").value;
        var fuelTypeIds = [];
        var fuelModelIds = [];
        var modelIds = [];
        var fuelVehicles = [];
        var fuelsDG = [];
        var totalsFuel = [];
        var fuelTypeId = document.getElementsByName("fuelTypeId");
        var fuelModelId = document.getElementsByName("fuelModelId");
//            alert(fuelModelId.length)
        var modelId = document.getElementsByName("modelId");
        var fuelVehicle = document.getElementsByName("fuelVehicles");
        var fuelDG = document.getElementsByName("fuelDGs");
        var totalFuel = document.getElementsByName("totalFuels");
        for (var i = 0; i < modelId.length; i++) {
            fuelModelIds.push(fuelModelId[i].value);
//          contractRateIds.push(contractRateId);
            modelIds.push(modelId[i].value);
            fuelVehicles.push(fuelVehicle[i].value);
            fuelsDG.push(fuelDG[i].value);
            totalsFuel.push(totalFuel[i].value);
        }
        $('#spinner').show();
        $("#loader").addClass('ui-loader-background');
        var url = "";
        url = './saveModelFuelDetails.do';
//                    alert(url);
        $.ajax({
            url: url,
            data: {fuelModelId: fuelModelIds, contractRateId: contractRateId, modelId: modelIds,
                fuelVehicle: fuelVehicles, fuelDG: fuelsDG, totalFuel: totalsFuel},
            type: "POST",
            dataType: 'json',
            success: function(data) {
//                                    alert(data)
                if (data == 1) {
                    $("#modelFuelStatus").text("Model fuel Saved Sucessfully");
                    $("#saveModel").hide();
                    $('#spinner').hide();
                    $("#loader").hide();

                }
                if (data == 2) {
                    $("#modelFuelStatus").text("Model fuel Updated Sucessfully");
                    $("#saveModel").hide();
                    $('#spinner').hide();
                    $("#loader").hide();

                }
            },
            error: function(xhr, status, error) {
                $("#modelFuelStatusError").text("error");
            }
        });
    }

    function sumFuel(sno) {
        var fuelRate = document.getElementById("fuelVehicles1" + sno).value;
        var fuelDG = document.getElementById("fuelDGs1" + sno).value;
        var totFuel;
        ptpPickupPoint
        if (fuelDG == "") {
            totFuel = parseInt(fuelRate);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
        if (fuelRate == "") {
            totFuel = parseInt(fuelDG);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
        if (fuelDG != "" && fuelRate != "") {
            totFuel = parseInt(fuelRate) + parseInt(fuelDG);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
    }
    function sumFuels() {
        var fuelRate = document.getElementById("fVehicle").value;
        var fuelDG = document.getElementById("fuelDG").value;
        var totFuel;
//        ptpPickupPoint
        if (fuelDG == "") {
            totFuel = parseInt(fuelRate);
            document.getElementById("totFuel").value = totFuel;
        }
        if (fuelRate == "") {
            totFuel = parseInt(fuelDG);
            document.getElementById("totFuel").value = totFuel;
        }
        if (fuelDG != "" && fuelRate != "") {
            totFuel = parseInt(fuelRate) + parseInt(fuelDG);
            document.getElementById("totFuel").value = totFuel;
        }
    }

    function setFuels() {
        var fuelRate = document.getElementsByName("fuelVehicles");
        var fuelDG = document.getElementsByName("fuelDGs");
        var totFuel = document.getElementsByName("totalFuels");
        for (var i = 0; i < fuelRate.length; i++) {
            fuelRate[i].value = document.getElementById("fVehicle").value;
            fuelDG[i].value = document.getElementById("fuelDG").value;
            totFuel[i].value = document.getElementById("totFuel").value;
        }
    }
</script>
<style>
    .noErrorClass{
        color: green;
    }
    .errorClass{
        color: red;
    }
</style>
<script type="text/javascript">
    var httpRequest;
    function contractRouteCheck(sno) {

        var companyId = 0;
        var contractId = document.getElementById("contractId").value;
        companyId = parseInt(document.getElementById("companyId").value);
        var customerId = parseInt(document.getElementById("customerId").value);
        if (customerId > 0) {
            //if (contractId != '') {
            var ptpVehicleTypeId = document.getElementById("ptpVehicleTypeId").value;
            var ptpPickupPointId = document.getElementById("ptpPickupPointId").value;
            var ptpInterimPoint1 = document.getElementById("interimPointId1").value;
            var ptpInterimPoint2 = document.getElementById("interimPointId2").value;
            var ptpInterimPoint3 = document.getElementById("interimPointId3").value;
            var ptpInterimPoint4 = document.getElementById("interimPointId4").value;
            var ptpDropPointId = document.getElementById("ptpDropPointId").value;
            var loadTypeId = document.getElementById("loadTypeId").value;
            var containerTypeId = document.getElementById("containerTypeId").value;
            var containerQty = document.getElementById("containerQty").value;
            var ptpRateWithReefer = document.getElementById("ptpRateWithReefer").value;
            var ptpRateWithoutReefer = document.getElementById("ptpRateWithoutReefer").value;
            if (ptpVehicleTypeId != "" && ptpVehicleTypeId != 0 && ptpPickupPointId != "" && ptpPickupPointId != 0 && ptpDropPointId != "" && ptpDropPointId != 0
                    && loadTypeId != "" && loadTypeId != 0 && containerTypeId != "" && containerTypeId != 0 && containerQty != "" && containerQty != 0 && ptpRateWithReefer != ""
                    && ptpRateWithoutReefer != "") {

                $.ajax({
                    url: "/throttle/compareContract.do",
                    dataType: "json",
                    data: {
                        ptpVehicleTypeId: ptpVehicleTypeId,
                        ptpPickupPointId: ptpPickupPointId,
                        ptpInterimPoint1: ptpInterimPoint1,
                        ptpInterimPoint2: ptpInterimPoint2,
                        ptpInterimPoint3: ptpInterimPoint3,
                        ptpInterimPoint4: ptpInterimPoint4,
                        ptpDropPointId: ptpDropPointId,
                        loadTypeId: loadTypeId,
                        containerTypeId: containerTypeId,
                        contractId: contractId,
                        containerQty: containerQty,
                        ptpRateWithReefer: ptpRateWithReefer,
                        ptpRateWithoutReefer: ptpRateWithoutReefer
                    },
                    success: function(data, textStatus, jqXHR) {
                        $.each(data, function(i, data) {
                            if (data.Id == 0) {
                                if (sno == '') {

                                    alert("Contract data for the route is not available at Organizational level. Please reenter");
                                    $('#ptpPickupPoint').val('');
                                    $('#ptpPickupPointId').val('');
                                    $("#interimPoint1").val('');
                                    $("#interimPointId1").val('');
                                    $("#interimPoint2").val('');
                                    $("#interimPointId2").val('');
                                    $("#interimPoint3").val('');
                                    $("#interimPointId3").val('');
                                    $("#interimPoint4").val('');
                                    $("#interimPointId4").val('');
                                    $("#ptpDropPoint").val('');
                                    $("#ptpDropPointId").val('');
                                    $("#interimPoint1Km").val('');
                                    $("#interimPoint2Km").val('');
                                    $("#interimPoint3Km").val('');
                                    $("#interimPoint4Km").val('');
                                    $("#ptpDropPointKm").val('');
                                    $("#interimPoint1Hrs").val('');
                                    $("#interimPoint2Hrs").val('');
                                    $("#interimPoint3Hrs").val('');
                                    $("#interimPoint4Hrs").val('');
                                    $("#ptpDropPointHrs").val('');
                                    $("#interimPoint1Minutes").val('');
                                    $("#interimPoint2Minutes").val('');
                                    $("#interimPoint3Minutes").val('');
                                    $("#interimPoint4Minutes").val('');
                                    $("#ptpDropPointMinutes").val('');
                                    $("#interimPoint1RouteId").val('');
                                    $("#interimPoint2RouteId").val('');
                                    $("#interimPoint3RouteId").val('');
                                    $("#interimPoint4RouteId").val('');
                                    $("#ptpDropPointRouteId").val('');
                                    $("#ptpTotalKm").val('');
                                    $("#ptpTotalHours").val('');
                                    $("#ptpTotalMinutes").val('');
                                    $('#ptpPickupPoint').focus();
                                    $("#ptpRateWithReefer").val('0');
                                    $("#ptpRateWithoutReefer").val('0');

                                    //saveButton
                                } else {
                                    $('#ptpRateWithReefer1' + sno).val('');
                                    $('#ptpRateWithoutReefer1' + sno).val('');
                                }
                                return false;
                            } else {

                                var valus = data.Name;
                                var temp7 = valus.split("~");
                                $("#orgRateWithReefer").text('');
                                $("#orgRateWithoutReefer").text('');
                                if (sno == "") {
                                    if (temp7[0] == 'TBA') {
                                        alert("Contract rate doesn't match with Organizational data. Contract needs approval");
                                        $("#orgValues").text('Needs Approval');
                                        $("#orgValues").removeClass('noErrorClass');
                                        $("#orgValues").addClass('errorClass');
                                        $("#orgRateWithReefer").text(temp7[1]);
                                        $("#orgRateWithoutReefer").text(temp7[2]);
                                        document.getElementById("orgRateWithReeferVal").value = temp7[1];
                                        document.getElementById("orgRateWithoutReeferVal").value = temp7[2];
                                        document.getElementById("approvalStatus").value = "2";
                                    } else {
                                        $("#orgValues").text('Auto Approval');
                                        $("#orgValues").removeClass('errorClass');
                                        $("#orgValues").addClass('noErrorClass');
                                        $("#orgRateWithReefer").text(temp7[1]);
                                        $("#orgRateWithoutReefer").text(temp7[2]);
                                        document.getElementById("approvalStatus").value = "1";
                                        document.getElementById("orgRateWithReeferVal").value = temp7[1];
                                        document.getElementById("orgRateWithoutReeferVal").value = temp7[2];
                                    }
                                }
                            }
                        });
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });

            }
        }
    }

</script>






<script type="text/javascript">
    //Getting organizational contract rate
    $(document).ready(function() {
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#ptpPickupPoint').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint2: document.getElementById("interimPointId2").value,
                        interimPoint3: document.getElementById("interimPointId3").value,
                        interimPoint4: document.getElementById("interimPointId4").value
                    },
                    success: function(data, textStatus, jqXHR) {

                        var items = data;
                        if (items == '') {
//
                            $('#ptpPickupPoint').val('');
                            $('#ptpPickupPointId').val('');
                            $("#interimPoint1").val('');
                            $("#interimPointId1").val('');
                            $("#interimPoint2").val('');
                            $("#interimPointId2").val('');
                            $("#interimPoint3").val('');
                            $("#interimPointId3").val('');
                            $("#interimPoint4").val('');
                            $("#interimPointId4").val('');
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#interimPoint1Km").val('');
                            $("#interimPoint2Km").val('');
                            $("#interimPoint3Km").val('');
                            $("#interimPoint4Km").val('');
                            $("#ptpDropPointKm").val('');
                            $("#interimPoint1Hrs").val('');
                            $("#interimPoint2Hrs").val('');
                            $("#interimPoint3Hrs").val('');
                            $("#interimPoint4Hrs").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#interimPoint1Minutes").val('');
                            $("#interimPoint2Minutes").val('');
                            $("#interimPoint3Minutes").val('');
                            $("#interimPoint4Minutes").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#interimPoint1RouteId").val('');
                            $("#interimPoint2RouteId").val('');
                            $("#interimPoint3RouteId").val('');
                            $("#interimPoint4RouteId").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpTotalMinutes").val('');
                            $('#ptpPickupPoint').focus();
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#ptpPickupPoint").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                $itemrow.find('#interimPoint1').focus();
                $("#interimPoint1").val('');
                $("#interimPointId1").val('');
                $("#interimPoint2").val('');
                $("#interimPointId2").val('');
                $("#interimPoint3").val('');
                $("#interimPointId3").val('');
                $("#interimPoint4").val('');
                $("#interimPointId4").val('');
                $("#ptpDropPoint").val('');
                $("#ptpDropPointId").val('');
                $("#interimPoint1Km").val('');
                $("#interimPoint2Km").val('');
                $("#interimPoint3Km").val('');
                $("#interimPoint4Km").val('');
                $("#ptpDropPointKm").val('');
                $("#interimPoint1Hrs").val('');
                $("#interimPoint2Hrs").val('');
                $("#interimPoint3Hrs").val('');
                $("#interimPoint4Hrs").val('');
                $("#ptpDropPointHrs").val('');
                $("#interimPoint1Minutes").val('');
                $("#interimPoint2Minutes").val('');
                $("#interimPoint3Minutes").val('');
                $("#interimPoint4Minutes").val('');
                $("#ptpDropPointMinutes").val('');
                $("#interimPoint1RouteId").val('');
                $("#interimPoint2RouteId").val('');
                $("#interimPoint3RouteId").val('');
                $("#interimPoint4RouteId").val('');
                $("#ptpDropPointRouteId").val('');
                $("#ptpTotalKm").val('');
                $("#ptpTotalHours").val('');
                $("#ptpTotalMinutes").val('');
                $("#ptpRateWithReefer").val('0');
                $("#ptpRateWithoutReefer").val('0');
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#interimPoint1').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint2: document.getElementById("interimPointId2").value,
                        interimPoint3: document.getElementById("interimPointId3").value,
                        interimPoint4: document.getElementById("interimPointId4").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {

                            $("#interimPoint1").val('');
                            $("#interimPointId1").val('');
                            $("#interimPoint2").val('');
                            $("#interimPointId2").val('');
                            $("#interimPoint3").val('');
                            $("#interimPointId3").val('');
                            $("#interimPoint4").val('');
                            $("#interimPointId4").val('');
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#interimPoint1Km").val('');
                            $("#interimPoint2Km").val('');
                            $("#interimPoint3Km").val('');
                            $("#interimPoint4Km").val('');
                            $("#ptpDropPointKm").val('');
                            $("#interimPoint1Hrs").val('');
                            $("#interimPoint2Hrs").val('');
                            $("#interimPoint3Hrs").val('');
                            $("#interimPoint4Hrs").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#interimPoint1Minutes").val('');
                            $("#interimPoint2Minutes").val('');
                            $("#interimPoint3Minutes").val('');
                            $("#interimPoint4Minutes").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#interimPoint1RouteId").val('');
                            $("#interimPoint2RouteId").val('');
                            $("#interimPoint3RouteId").val('');
                            $("#interimPoint4RouteId").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpTotalMinutes").val('');
                            $('#interimPoint1').focus();
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            minLength: 0,
            select: function(event, ui) {
                $("#interimPoint1").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#interimPoint1').val(tmp[0]);
                $itemrow.find('#interimPointId1').val(tmp[1]);
                $itemrow.find('#interimPoint1Km').val(tmp[2]);
                $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                $itemrow.find('#interimPoint2').focus();
                $("#interimPoint2").val('');
                $("#interimPointId2").val('');
                $("#interimPoint3").val('');
                $("#interimPointId3").val('');
                $("#interimPoint4").val('');
                $("#interimPointId4").val('');
                $("#ptpDropPoint").val('');
                $("#ptpDropPointId").val('');
                $("#interimPoint2Km").val('');
                $("#interimPoint3Km").val('');
                $("#interimPoint4Km").val('');
                $("#ptpDropPointKm").val('');
                $("#interimPoint2Hrs").val('');
                $("#interimPoint3Hrs").val('');
                $("#interimPoint4Hrs").val('');
                $("#ptpDropPointHrs").val('');
                $("#interimPoint2Minutes").val('');
                $("#interimPoint3Minutes").val('');
                $("#interimPoint4Minutes").val('');
                $("#ptpDropPointMinutes").val('');
                $("#interimPoint2RouteId").val('');
                $("#interimPoint3RouteId").val('');
                $("#interimPoint4RouteId").val('');
                $("#ptpDropPointRouteId").val('');
                $("#ptpTotalKm").val('');
                $("#ptpTotalHours").val('');
                $("#ptpTotalMinutes").val('');
                $("#ptpRateWithReefer").val('0');
                $("#ptpRateWithoutReefer").val('0');
                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#interimPoint2').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint3: document.getElementById("interimPointId3").value,
                        interimPoint4: document.getElementById("interimPointId4").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
//
                            $("#interimPoint2").val('');
                            $("#interimPointId2").val('');
                            $("#interimPoint3").val('');
                            $("#interimPointId3").val('');
                            $("#interimPoint4").val('');
                            $("#interimPointId4").val('');
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#interimPoint2Km").val('');
                            $("#interimPoint3Km").val('');
                            $("#interimPoint4Km").val('');
                            $("#ptpDropPointKm").val('');
                            $("#interimPoint2Hrs").val('');
                            $("#interimPoint3Hrs").val('');
                            $("#interimPoint4Hrs").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#interimPoint2Minutes").val('');
                            $("#interimPoint3Minutes").val('');
                            $("#interimPoint4Minutes").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#interimPoint2RouteId").val('');
                            $("#interimPoint3RouteId").val('');
                            $("#interimPoint4RouteId").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpTotalMinutes").val('');
                            $('#interimPoint2').focus();
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            minLength: 0,
            select: function(event, ui) {
                $("#interimPoint2").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#interimPoint2').val(tmp[0]);
                $itemrow.find('#interimPointId2').val(tmp[1]);
                $itemrow.find('#interimPoint2Km').val(tmp[2]);
                $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                $itemrow.find('#interimPoint3').focus();
                $("#interimPoint3").val('');
                $("#interimPointId3").val('');
                $("#interimPoint4").val('');
                $("#interimPointId4").val('');
                $("#ptpDropPoint").val('');
                $("#ptpDropPointId").val('');
                $("#interimPoint3Km").val('');
                $("#interimPoint4Km").val('');
                $("#ptpDropPointKm").val('');
                $("#interimPoint3Hrs").val('');
                $("#interimPoint4Hrs").val('');
                $("#ptpDropPointHrs").val('');
                $("#interimPoint3Minutes").val('');
                $("#interimPoint4Minutes").val('');
                $("#ptpDropPointMinutes").val('');
                $("#interimPoint3RouteId").val('');
                $("#interimPoint4RouteId").val('');
                $("#ptpDropPointRouteId").val('');
                $("#ptpTotalKm").val('');
                $("#ptpTotalHours").val('');
                $("#ptpTotalMinutes").val('');
                $("#ptpRateWithReefer").val('0');
                $("#ptpRateWithoutReefer").val('0');
                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#interimPoint3').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint2: document.getElementById("interimPointId2").value,
                        interimPoint4: document.getElementById("interimPointId4").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
//
                            $("#interimPoint3").val('');
                            $("#interimPointId3").val('');
                            $("#interimPoint4").val('');
                            $("#interimPointId4").val('');
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#interimPoint3Km").val('');
                            $("#interimPoint4Km").val('');
                            $("#ptpDropPointKm").val('');
                            $("#interimPoint3Hrs").val('');
                            $("#interimPoint4Hrs").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#interimPoint3Minutes").val('');
                            $("#interimPoint4Minutes").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#interimPoint3RouteId").val('');
                            $("#interimPoint4RouteId").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                            $("#ptpTotalMinutes").val('');
                            $('#interimPoint3').focus();
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            minLength: 0,
            select: function(event, ui) {
                $("#interimPoint3").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#interimPoint3').val(tmp[0]);
                $itemrow.find('#interimPointId3').val(tmp[1]);
                $itemrow.find('#interimPoint3Km').val(tmp[2]);
                $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                $itemrow.find('#interimPoint4').focus();
                $("#interimPoint4").val('');
                $("#interimPointId4").val('');
                $("#ptpDropPoint").val('');
                $("#ptpDropPointId").val('');
                $("#interimPoint4Km").val('');
                $("#ptpDropPointKm").val('');
                $("#interimPoint4Hrs").val('');
                $("#ptpDropPointHrs").val('');
                $("#interimPoint4Minutes").val('');
                $("#ptpDropPointMinutes").val('');
                $("#interimPoint4RouteId").val('');
                $("#ptpDropPointRouteId").val('');
                $("#ptpTotalKm").val('');
                $("#ptpTotalHours").val('');
                $("#ptpTotalMinutes").val('');
                $("#ptpRateWithReefer").val('0');
                $("#ptpRateWithoutReefer").val('0');
                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#interimPoint4').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint2: document.getElementById("interimPointId2").value,
                        interimPoint3: document.getElementById("interimPointId3").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
//
                            $("#interimPoint4").val('');
                            $("#interimPointId4").val('');
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#interimPoint4Km").val('');
                            $("#ptpDropPointKm").val('');
                            $("#interimPoint4Hrs").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#interimPoint4Minutes").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#interimPoint4RouteId").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                            $("#ptpTotalMinutes").val('');
                            $('#interimPoint4').focus();
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            minLength: 0,
            select: function(event, ui) {
                $("#interimPoint4").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#interimPoint4').val(tmp[0]);
                $itemrow.find('#interimPointId4').val(tmp[1]);
                $itemrow.find('#interimPoint4Km').val(tmp[2]);
                $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                $itemrow.find('#ptpDropPoint').focus();
                $("#ptpDropPoint").val('');
                $("#ptpDropPointId").val('');
                $("#ptpDropPointKm").val('');
                $("#ptpDropPointHrs").val('');
                $("#ptpDropPointMinutes").val('');
                $("#ptpDropPointRouteId").val('');
                $("#ptpTotalKm").val('');
                $("#ptpTotalHours").val('');
                $("#ptpTotalMinutes").val('');
                $("#ptpRateWithReefer").val('0');
                $("#ptpRateWithoutReefer").val('0');
                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
        $('#ptpDropPoint').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                        interimPoint1: document.getElementById("interimPointId1").value,
                        interimPoint2: document.getElementById("interimPointId2").value,
                        interimPoint3: document.getElementById("interimPointId3").value,
                        interimPoint4: document.getElementById("interimPointId4").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
//
                            $("#ptpDropPoint").val('');
                            $("#ptpDropPointId").val('');
                            $("#ptpDropPointKm").val('');
                            $("#ptpDropPointHrs").val('');
                            $("#ptpDropPointMinutes").val('');
                            $("#ptpDropPointRouteId").val('');
                            $("#ptpTotalKm").val('');
                            $("#ptpTotalHours").val('');
                            $("#ptpTotalMinutes").val('');
                            $("#ptpRateWithReefer").val('0');
                            $("#ptpRateWithoutReefer").val('0');
                            $('#ptpDropPoint').focus();
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            minLength: 0,
            select: function(event, ui) {
                //  alert("srini here....");
                $("#ptpDropPoint").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#ptpDropPoint').val(tmp[0]);
                $itemrow.find('#ptpDropPointId').val(tmp[1]);
                var vehicleTypeId = $itemrow.find('#ptpVehicleTypeId').val();
                var loadTypeId = $itemrow.find('#loadTypeId').val();
                var containerTypeId = $itemrow.find('#containerTypeId').val();
                //  alert("srini here....1"+vehicleTypeId);
                var ptpPickupPointId = $itemrow.find('#ptpPickupPointId').val();
                //  alert("srini here....2="+ptpPickupPointId);
                var interimPointId1 = $itemrow.find('#interimPointId1').val();
                //    alert("srini here....3="+interimPointId1);
                var interimPointId2 = $itemrow.find('#interimPointId2').val();
                //    alert("srini here....4="+interimPointId2);
                var interimPointId3 = $itemrow.find('#interimPointId3').val();
                var interimPointId4 = $itemrow.find('#interimPointId4').val();
                var ptpDropPointId = $itemrow.find('#ptpDropPointId').val();
                var customerId = document.getElementById('customerId').value;
                var statusRes = 0;
                //   alert("customerId="+customerId)
                if (customerId != "0") {
                    //    alert("1");
                    var ptpFromDate = $itemrow.find('#ptpFromDate').val();
                    var ptpToDate = $itemrow.find('#ptpToDate').val();
                    statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                    //statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId, ptpFromDate, ptpToDate);
                } else {
                    //     alert("2");
                    statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                }
                //alert("statusRes="+statusRes)
                if (statusRes > 0) {
                    alert("The same combination already exists in row " + statusRes + ", please check.");
                    $itemrow.find('#ptpVehicleTypeId').val(0);
                    $itemrow.find('#loadTypeId').val(0);
                    $itemrow.find('#containerTypeId').val(0);
                    $itemrow.find('#ptpPickupPoint').val('');
                    $itemrow.find('#ptpPickupPointId').val('');
                    $itemrow.find('#interimPoint1').val('');
                    $itemrow.find('#interimPointId1').val('');
                    $itemrow.find('#interimPoint2').val('');
                    $itemrow.find('#interimPointId2').val('');
                    $itemrow.find('#interimPoint3').val('');
                    $itemrow.find('#interimPointId3').val('');
                    $itemrow.find('#interimPoint4').val('');
                    $itemrow.find('#interimPointId4').val('');
                    $itemrow.find('#ptpDropPoint').val('');
                    $itemrow.find('#ptpDropPointId').val('');
                } else {
                    var sno = $itemrow.length;
                    $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                    $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                    $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                    $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    if (document.getElementById("interimPoint1Km").value != '') {
                        var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                    } else {
                        var interimPoint1Km = 0;
                    }
                    if (document.getElementById("interimPoint2Km").value != '') {
                        var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                    } else {
                        var interimPoint2Km = 0;
                    }
                    if (document.getElementById("interimPoint3Km").value != '') {
                        var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                    } else {
                        var interimPoint3Km = 0;
                    }
                    if (document.getElementById("interimPoint4Km").value != '') {
                        var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                    } else {
                        var interimPoint4Km = 0;
                    }

                    if (document.getElementById("interimPoint1Hrs").value != '') {
                        var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                    } else {
                        var interimPoint1Hrs = 0;
                    }
                    if (document.getElementById("interimPoint2Hrs").value != '') {
                        var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                    } else {
                        var interimPoint2Hrs = 0;
                    }
                    if (document.getElementById("interimPoint3Hrs").value != '') {
                        var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                    } else {
                        var interimPoint3Hrs = 0;
                    }
                    if (document.getElementById("interimPoint4Hrs").value != '') {
                        var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                    } else {
                        var interimPoint4Hrs = 0;
                    }

                    if (document.getElementById("interimPoint1Minutes").value != '') {
                        var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                    } else {
                        var interimPoint1Minutes = 0;
                    }
                    if (document.getElementById("interimPoint2Minutes").value != '') {
                        var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                    } else {
                        var interimPoint2Minutes = 0;
                    }
                    if (document.getElementById("interimPoint3Minutes").value != '') {
                        var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                    } else {
                        var interimPoint3Minutes = 0;
                    }
                    if (document.getElementById("interimPoint4Minutes").value != '') {
                        var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                    } else {
                        var interimPoint4Minutes = 0;
                    }

                    var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                    $itemrow.find('#ptpTotalKm').val(totalKm);
                    var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                    $itemrow.find('#ptpTotalHours').val(totalHrs);
                    var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                    $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                    $itemrow.find('#ptpRateWithReefer').focus();
                }
                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });

    function showAddOption() {
        $("#addNewRouteButton").hide();
        $("#addNewRoute").show();
        $("#addMoreRouteValue").val(1);
    }
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            //  setDate: new Date(),
            changeMonth: true, changeYear: true
        });

    });

    var fourMDate = new Date();
    fourMDate.setMonth(fourMDate.getMonth() + 4);
    var fourdd = fourMDate.getDate();
    var fourmm = fourMDate.getMonth() + 1;
    var fouryyyy = fourMDate.getFullYear();
    if (fourdd < 10)
    {
        fourdd = '0' + fourdd;
    }

    if (fourmm < 10)
    {
        fourmm = '0' + fourmm;
    }
    fourMDate = fourdd + '-' + fourmm + '-' + fouryyyy;
//alert("fourMDate:"+fourMDate);
    var today = new Date();

    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10)
    {
        dd = '0' + dd;
    }

    if (mm < 10)
    {
        mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;
</script>




<c:if test="${billingTypeId == 1}">
    <% int index = 0;%>
    <script>
//$(".datepicker").datepicker();
//$(".datepicker").datepicker("setDate", new Date());
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.
            var rowTemp = "";
        <c:if test="${customerId == '0'}">
            rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"><input type="hidden" name="ptpToDate" id="ptpToDate" value="02-02-2017" class="datepicker  form-control"  style="width: 120px;"/><input type="hidden" name="ptpFromDate" id="ptpFromDate" value="02-02-2017" class="datepicker  form-control"  style="width: 120px;"/><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="R100" class="form-control"  /></a></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control"  ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><select name="loadTypeId" id="loadTypeId"  class="form-control"  ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></td>',
                '<td><select name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px" ></td>',
                '<td><select name="containerQty" id="containerQty"  class="form-control"  ><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></td>',
                '<td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 90px;"/></td>',
                '<td><input name="ptpRateWithReefer" value="0" class="form-control" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="ptpRateWithoutReefer" value="0" class="form-control" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelVehicle" value="0" class="form-control" id="fuelVehicle"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelDg" value="0" class="form-control" id="fuelDg"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="totalFuel" value="0" class="form-control" id="totalFuel"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="toll" value="0" class="form-control" id="toll"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="driverBachat" value="0" class="form-control" id="driverBachat"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="dala" value="0" class="form-control" id="dala"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="misc" value="0" class="form-control" id="misc"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
        </c:if>
        <c:if test="${customerId != '0'}">
            rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><span id="orgValues" style="font-size: 12px"></span></td>',
                '<td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  style="width:150px;height:44px;"/></td>',
                '<td><input name="ptpFromDate" id="ptpFromDate" value="" readOnly class="  form-control"  style="width:150px;height:44px;"/></td>',
                '<td><input name="ptpToDate" id="ptpToDate" value="" readOnly class="  form-control"  style="width:150px;height:44px;"/><input type="hidden" name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="text" name="approvalStatus" value="0" class="form-control" id="approvalStatus"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px;height:44px;"><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></select></td>',
                '<td><select  name="containerTypeId" id="containerTypeId"   class="form-control" style="width:150px;height:44px;"></select></td>',
                '<td><select name="containerQty" id="containerQty" class="form-control"  style="width:130px;height:44px;"><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>',
                '<td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width:150px;height:44px;"  onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width:150px;height:44px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width:150px;height:44px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width:150px;height:44px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width:150px;height:44px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width:150px;height:44px;"/></td>',
                '<td><input name="ptpRateWithReefer" value="" class="form-control" id="ptpRateWithReefer" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="ptpRateWithoutReefer" value="" class="form-control" id="ptpRateWithoutReefer" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><span id="orgRateWithReefer" style="font-size: 12px; color:green"></span></td>',
                '<td><span id="orgRateWithoutReefer" style="font-size: 12px;color:green"></span></td>',
                '</tr>'
            ].join('');
        </c:if>
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                $('.datepicker').datepicker('destroy');
                count++;
                // alert("cur date:"+ new Date());
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpFromDate = $row.find('#ptpFromDate');
                var $ptpToDate = $row.find('#ptpToDate');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $loadTypeId = $row.find('#loadTypeId');
                var $containerTypeId = $row.find('#containerTypeId');
                var $containerQty = $row.find('#containerQty');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');

                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    //  $row.find('#ptpFromDate').datepicker("setDate", new Date());
                    $row.find("#ptpFromDate").val(today);
                    $row.find("#ptpToDate").val(fourMDate);

                    $row.find('#ptpVehicleTypeId').change(function() {
//                        var $itemrow = $(this).closest('tr');
                        var val = $row.find('#ptpVehicleTypeId').val();
                        if (val == '1058') {
                            // 20FT vehicle
                            $row.find('#containerTypeId').empty();
                            $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                            $row.find('#containerQty').empty();
                            $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))
                        } else if (val == '1059') {
                            // 40FT vehicle
                            $row.find('#containerTypeId').empty();
                            $row.find('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                            $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                            $row.find('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                            $row.find('#containerQty').empty();

                        }
                    });
                    $row.find('#containerTypeId').change(function() {
//                        var $itemrow = $(this).closest('tr');
                        var val = $row.find('#containerTypeId').val();
                        if (val == '1') {
                            // 20FT Container
                            $row.find('#containerQty').empty();
                            $row.find('#containerQty').append($('<option ></option>').val(2).html('2'))
                        } else if (val == '2') {
                            // 40FT Container
                            $row.find('#containerQty').empty();
                            $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))

                        }
                    });

                    $row.find('#ptpPickupPoint,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint').change(function() {
                        var id = this.id;
                        var id1 = "";
                        if (this.id == 'ptpPickupPoint') {
                            id1 = 'ptpPickupPointId';
                        }
                        if (this.id == 'interimPoint1') {
                            id1 = 'interimPointId1';
                        }
                        if (this.id == 'interimPoint2') {
                            id1 = 'interimPointId2';
                        }
                        if (this.id == 'interimPoint3') {
                            id1 = 'interimPointId3';
                        }
                        if (this.id == 'interimPoint4') {
                            id1 = 'interimPointId4';
                        }
                        if (this.id == 'ptpDropPoint') {
                            id1 = 'ptpDropPointId';
                        }
                        var ptpPickupPoint = $row.find('#' + id).val();
                        var ptpPickupPointId = $row.find('#' + id1).val();
                        if (ptpPickupPoint != '' && ptpPickupPointId == '') {
                            alert('Invalid Point Name');
                            $row.find('#' + id).focus();
                            $row.find('#' + id).val('');
                            $row.find('#' + id1).val('');
                        }
                    });
                    $row.find('#ptpPickupPoint,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint').keyup(function(event) {
                        var id = this.id;
                        var id1 = "";
                        if (this.id == 'ptpPickupPoint') {
                            id1 = 'ptpPickupPointId';
                        }
                        if (this.id == 'interimPoint1') {
                            id1 = 'interimPointId1';
                        }
                        if (this.id == 'interimPoint2') {
                            id1 = 'interimPointId2';
                        }
                        if (this.id == 'interimPoint3') {
                            id1 = 'interimPointId3';
                        }
                        if (this.id == 'interimPoint4') {
                            id1 = 'interimPointId4';
                        }
                        if (this.id == 'ptpDropPoint') {
                            id1 = 'ptpDropPointId';
                        }
                        var ptpPickupPointId = $row.find('#' + id1).val();
                        if (event.keyCode == 46 || event.keyCode == 8) {
                            $row.find('#' + id).val('');
                            $row.find('#' + id1).val('');
                        } else if (ptpPickupPointId != '' && event.keyCode != 13) {
                            $row.find('#' + id).val('');
                            $row.find('#' + id1).val('');
                        }
                    });

                    $row.find('#ptpVehicleTypeId,#loadTypeId,#containerTypeId,#containerQty,#ptpPickupPoint,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint,#ptpRateWithoutReefer,#ptpRateWithReefer').change(function() {
//                        var $itemrow = $(this).closest('tr');
                        var val = $row.find('#containerTypeId').val();
                        var ptpVehicleTypeId = $row.find("#ptpVehicleTypeId").val();
                        var ptpPickupPointId = $row.find("#ptpPickupPointId").val();
                        var ptpInterimPoint1 = $row.find("#interimPointId1").val();
                        var ptpInterimPoint2 = $row.find("#interimPoint2").val();
                        var ptpInterimPoint3 = $row.find("#interimPoint3").val();
                        var ptpInterimPoint4 = $row.find("#interimPoint4").val();
                        var ptpDropPointId = $row.find("#ptpDropPointId").val();
                        var loadTypeId = $row.find("#loadTypeId").val();
                        var containerTypeId = $row.find("#containerTypeId").val();
                        var containerQty = $row.find("#containerQty").val();
                        var ptpRateWithReefer = $row.find("#ptpRateWithReefer").val();
                        var ptpRateWithoutReefer = $row.find("#ptpRateWithoutReefer").val();
                        var customerId = $("#customerId").val();
                        if (customerId > 0) {
                            if (ptpVehicleTypeId != "" && ptpVehicleTypeId != 0 && ptpPickupPointId != "" && ptpPickupPointId != 0 && ptpDropPointId != "" && ptpDropPointId != 0
                                    && loadTypeId != "" && loadTypeId != 0 && containerTypeId != "" && containerTypeId != 0 && containerQty != "" && containerQty != 0 && ptpRateWithReefer != ""
                                    && ptpRateWithoutReefer != "") {

                                $.ajax({
                                    url: "/throttle/compareContract.do",
                                    dataType: "json",
                                    data: {
                                        ptpVehicleTypeId: ptpVehicleTypeId,
                                        ptpPickupPointId: ptpPickupPointId,
                                        ptpInterimPoint1: ptpInterimPoint1,
                                        ptpInterimPoint2: ptpInterimPoint2,
                                        ptpInterimPoint3: ptpInterimPoint3,
                                        ptpInterimPoint4: ptpInterimPoint4,
                                        ptpDropPointId: ptpDropPointId,
                                        loadTypeId: loadTypeId,
                                        containerTypeId: containerTypeId,
                                        //contractId: contractId,
                                        containerQty: containerQty,
                                        ptpRateWithReefer: ptpRateWithReefer,
                                        ptpRateWithoutReefer: ptpRateWithoutReefer
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        $.each(data, function(i, data) {
                                            if (data.Id == 0) {
                                                alert("2222");
                                                alert("Contract data for the route is not available at Organizational level. Please reenter");
                                                $row.find('#ptpPickupPoint').val('');
                                                $row.find('#ptpPickupPointId').val('');
                                                $row.find("#interimPoint1").val('');
                                                $row.find("#interimPointId1").val('');
                                                $row.find("#interimPoint2").val('');
                                                $row.find("#interimPointId2").val('');
                                                $row.find("#interimPoint3").val('');
                                                $row.find("#interimPointId3").val('');
                                                $row.find("#interimPoint4").val('');
                                                $row.find("#interimPointId4").val('');
                                                $row.find("#ptpDropPoint").val('');
                                                $row.find("#ptpDropPointId").val('');
                                                $row.find("#interimPoint1Km").val('');
                                                $row.find("#interimPoint2Km").val('');
                                                $row.find("#interimPoint3Km").val('');
                                                $row.find("#interimPoint4Km").val('');
                                                $row.find("#ptpDropPointKm").val('');
                                                $row.find("#interimPoint1Hrs").val('');
                                                $row.find("#interimPoint2Hrs").val('');
                                                $row.find("#interimPoint3Hrs").val('');
                                                $row.find("#interimPoint4Hrs").val('');
                                                $row.find("#ptpDropPointHrs").val('');
                                                $row.find("#interimPoint1Minutes").val('');
                                                $row.find("#interimPoint2Minutes").val('');
                                                $row.find("#interimPoint3Minutes").val('');
                                                $row.find("#interimPoint4Minutes").val('');
                                                $row.find("#ptpDropPointMinutes").val('');
                                                $row.find("#interimPoint1RouteId").val('');
                                                $row.find("#interimPoint2RouteId").val('');
                                                $row.find("#interimPoint3RouteId").val('');
                                                $row.find("#interimPoint4RouteId").val('');
                                                $row.find("#ptpDropPointRouteId").val('');
                                                $row.find("#ptpTotalKm").val('');
                                                $row.find("#ptpTotalHours").val('');
                                                $row.find("#ptpTotalMinutes").val('');
                                                $row.find('#ptpPickupPoint').focus();
                                                $row.find("#ptpRateWithReefer").val('');
                                                $row.find("#ptpRateWithoutReefer").val('');
                                                return false;
                                            } else {
                                                var valus = data.Name;
                                                var temp7 = valus.split("~");
                                                $row.find("#orgRateWithReefer").text('');
                                                $row.find("#orgRateWithoutReefer").text('');
                                                if (temp7[0] == 'TBA') {
                                                    alert("Contract rate doesn't match with Organizational data. Contract needs approval");
                                                    $row.find("#orgValues").text("Needs Approval");
                                                    $row.find("#orgRateWithReefer").text(temp7[1]);
                                                    $row.find("#orgRateWithoutReefer").text(temp7[2]);
                                                    $row.find("#orgValues").addClass('errorClass');
                                                    $row.find("#approvalStatus").val('2');
                                                    //   document.getElementById("approvalStatus").value = "2";
                                                } else {
                                                    $row.find("#orgValues").text("Auto Approval");
                                                    $row.find("#orgValues").addClass('noErrorClass');
                                                    $row.find("#orgRateWithReefer").text(temp7[1]);
                                                    $row.find("#orgRateWithoutReefer").text(temp7[2]);
                                                    $row.find("#approvalStatus").val('1');
                                                    //  document.getElementById("approvalStatus").value = "1";
                                                }
                                            }
                                        });
                                    },
                                    error: function(data, type) {
                                        console.log(type);
                                    }
                                });
                            }
                        }
                    });

                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find('#ptpPickupPoint').val('');
                                        $row.find('#ptpPickupPointId').val('');
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val('');
                            $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint1Km").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint1Hrs").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint1Minutes").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint1RouteId").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint1').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint2').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint3').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint4').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find('#ptpDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            var vehicleTypeId = $itemrow.find('#ptpVehicleTypeId').val();
                            var loadTypeId = $itemrow.find('#loadTypeId').val();
                            var containerTypeId = $itemrow.find('#containerTypeId').val();
                            var ptpPickupPointId = $itemrow.find('#ptpPickupPointId').val();
                            var interimPointId1 = $itemrow.find('#interimPointId1').val();
                            var interimPointId2 = $itemrow.find('#interimPointId2').val();
                            var interimPointId3 = $itemrow.find('#interimPointId3').val();
                            var interimPointId4 = $itemrow.find('#interimPointId4').val();
                            var ptpDropPointId = $itemrow.find('#ptpDropPointId').val();
                            var customerId = document.getElementById('customerId').value;
                            var statusRes = 0;
                            if (customerId != "0") {
                                var ptpFromDate = $itemrow.find('#ptpFromDate').val();
                                var ptpToDate = $itemrow.find('#ptpToDate').val();
                                statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                                //    statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId, ptpFromDate, ptpToDate);
                                // alert("statusRes ::::::::"+statusRes);
                            } else {
                                statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                            }
                            if (statusRes > 0) {
                                alert("The same combination already exists in row " + statusRes + ", please check.");
                                $itemrow.find('#ptpVehicleTypeId').val(0);
                                $itemrow.find('#loadTypeId').val(0);
                                $itemrow.find('#containerTypeId').val(0);
                                $itemrow.find('#ptpPickupPoint').val('');
                                $itemrow.find('#ptpPickupPointId').val('');
                                $itemrow.find('#interimPoint1').val('');
                                $itemrow.find('#interimPointId1').val('');
                                $itemrow.find('#interimPoint2').val('');
                                $itemrow.find('#interimPointId2').val('');
                                $itemrow.find('#interimPoint3').val('');
                                $itemrow.find('#interimPointId3').val('');
                                $itemrow.find('#interimPoint4').val('');
                                $itemrow.find('#interimPointId4').val('');
                                $itemrow.find('#ptpDropPoint').val('');
                                $itemrow.find('#ptpDropPointId').val('');
                            } else {
                                $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                                $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                                $row.find("#ptpRateWithReefer").val('');
                                $row.find("#ptpRateWithoutReefer").val('');
                                if ($row.find('#interimPoint1Km').val() !== '') {
                                    var interimPoint1Km = $row.find('#interimPoint1Km').val();
                                } else {
                                    var interimPoint1Km = 0;
                                }
                                if ($row.find('#interimPoint2Km').val() !== '') {
                                    var interimPoint2Km = $row.find('#interimPoint2Km').val();
                                } else {
                                    var interimPoint2Km = 0;
                                }
                                if ($row.find('#interimPoint3Km').val() !== '') {
                                    var interimPoint3Km = $row.find('#interimPoint3Km').val();
                                } else {
                                    var interimPoint3Km = 0;
                                }
                                if ($row.find('#interimPoint4Km').val() !== '') {
                                    var interimPoint4Km = $row.find('#interimPoint4Km').val();
                                } else {
                                    var interimPoint4Km = 0;
                                }


                                if ($row.find('#interimPoint1Hrs').val() !== '') {
                                    var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                                } else {
                                    var interimPoint1Hrs = 0;
                                }
                                if ($row.find('#interimPoint2Hrs').val() !== '') {
                                    var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                                } else {
                                    var interimPoint2Hrs = 0;
                                }
                                if ($row.find('#interimPoint3Hrs').val() !== '') {
                                    var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                                } else {
                                    var interimPoint3Hrs = 0;
                                }
                                if ($row.find('#interimPoint4Hrs').val() !== '') {
                                    var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                                } else {
                                    var interimPoint4Hrs = 0;
                                }

                                if ($row.find('#interimPoint1Minutes').val() !== '') {
                                    var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                                } else {
                                    var interimPoint1Minutes = 0;
                                }
                                if ($row.find('#interimPoint2Minutes').val() !== '') {
                                    var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                                } else {
                                    var interimPoint2Minutes = 0;
                                }
                                if ($row.find('#interimPoint3Minutes').val() !== '') {
                                    var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                                } else {
                                    var interimPoint3Minutes = 0;
                                }
                                if ($row.find('#interimPoint4Minutes').val() !== '') {
                                    var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                                } else {
                                    var interimPoint4Minutes = 0;
                                }


                                var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                                $row.find('#ptpTotalKm').val(totalKm);
                                var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                                var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);

                                if (parseInt(totalMinutes) > 59) {
                                    var hours = parseInt(totalMinutes) / 60;
                                    var minutes = parseInt(totalMinutes) % 60;
                                    totalHrs = parseInt(totalHrs) + parseInt(hours);
                                    totalMinutes = minutes;
                                }
                                $row.find('#ptpTotalHours').val(totalHrs);
                                $row.find('#ptpTotalMinutes').val(totalMinutes);
                                $itemrow.find('#ptpRateWithReefer').focus();
                            }
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();


                    $('.datepicker').datepicker();

                } else { // End if last itemCode input is empty
                    alert("please fill route code");
                }
                return false;
            });

            // Remove row when clicked
            $("#deleteRow").live('click', function() {
                $(this).parents('.item-row').remove();
                // Hide delete Icon if we only have one row in the list.
                if ($(".item-row").length < 2)
                    $("#deleteRow").hide();
            });
        });// End DOM

        function checkContractCaseExists(ptpVehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId,
                interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId) {
            //alert("Here for test");
            var status = 0;
            var checkStatus = false;
            var vehicleTypeIdEdit = document.getElementsByName("vehicleTypeIdEdit");
            var loadTypeIdEdit = document.getElementsByName("loadTypeIdEdit");
            var containerTypeIdEdit = document.getElementsByName("containerTypeIdEdit");
            var firstPointIdEdit = document.getElementsByName("firstPointIdEdit");
            var point1IdEdit = document.getElementsByName("point1IdEdit");
            var point2IdEdit = document.getElementsByName("point2IdEdit");
            var point3IdEdit = document.getElementsByName("point3IdEdit");
            var point4IdEdit = document.getElementsByName("point4IdEdit");
            var finalPointIdEdit = document.getElementsByName("finalPointIdEdit");
            var fromDate = document.getElementsByName("fromDate");
            var toDate = document.getElementsByName("toDate");
//            alert("ptpFromDate ===== "+ptpFromDate);
//            alert("ptpToDate ==== "+ptpToDate);
//            alert(ptpVehicleTypeId+" - "+vehicleTypeIdEdit[0].value);
//            alert(loadTypeId+" - "+loadTypeIdEdit[0].value);
//            alert(containerTypeId+" - "+containerTypeIdEdit[0].value);
            if (ptpPickupPointId == '') {
                ptpPickupPointId = 0;
            }
            if (interimPointId1 == '') {
                interimPointId1 = 0;
            }
            if (interimPointId2 == '') {
                interimPointId2 = 0;
            }
            if (interimPointId3 == '') {
                interimPointId3 = 0;
            }
            if (interimPointId4 == '') {
                interimPointId4 = 0;
            }
//            alert(ptpPickupPointId+" - "+firstPointIdEdit[0].value);
//            alert(interimPointId1+" - "+point1IdEdit[0].value);
//            alert(interimPointId2+" - "+point2IdEdit[0].value);
//            alert(interimPointId3+" - "+point3IdEdit[0].value);
//            alert(interimPointId4+" - "+point4IdEdit[0].value);
//            alert(ptpDropPointId+" - "+finalPointIdEdit[0].value);
            if (vehicleTypeIdEdit.length > 0) {
                for (var i = 0; i < vehicleTypeIdEdit.length; i++) {
                    //     alert(vehicleTypeIdEdit[i].value);
                    //  alert(vehicleTypeIdEdit.length);
                    if ((vehicleTypeIdEdit[i].value == ptpVehicleTypeId) && (loadTypeIdEdit[i].value == loadTypeId) && (containerTypeIdEdit[i].value == containerTypeId) && (firstPointIdEdit[i].value == ptpPickupPointId) && (point1IdEdit[i].value == interimPointId1) && (point2IdEdit[i].value == interimPointId2) && (point3IdEdit[i].value == interimPointId3) && (point4IdEdit[i].value == interimPointId4) && (finalPointIdEdit[i].value == ptpDropPointId)) {
                        checkStatus = true;
                    } else {
                        checkStatus = false;
                    }
                    //       alert("checkStatus 1 ::::: "+checkStatus);

                    if (checkStatus) {
//                        ptpFromDate = ptpFromDate.split('-');
//                        ptpFromDate = ptpFromDate[2]+"-"+ptpFromDate[1]+"-"+ptpFromDate[0];
//                        ptpToDate = ptpToDate.split('-');
//                        ptpToDate = ptpToDate[2]+"-"+ptpToDate[1]+"-"+ptpToDate[0];

                        var fromDateNew = fromDate[i].value;
                        var toDateNew = toDate[i].value;

                        //  alert("inner : "+fromDateNew+" "+toDateNew+" "+ptpFromDate+" "+ptpToDate);

                        //     checkStatus = dateCheck(fromDateNew,toDateNew,ptpFromDate,ptpToDate);

                    }

                    //     alert("checkStatus 2 : "+checkStatus);

                    if (checkStatus) {
//                        alert(checkStatus);
                        status = parseInt(i) + 1;
                        //                       alert("final : "+status);
                        return status;
                    }

                }
            }

            var ptpVehicleTypeIds = document.getElementsByName("ptpVehicleTypeId");
            var loadTypeIds = document.getElementsByName("loadTypeId");
            var containerTypeIds = document.getElementsByName("containerTypeId");
            var ptpPickupPointIds = document.getElementsByName("ptpPickupPointId");
            var interimPointId1s = document.getElementsByName("interimPointId1");
            var interimPointId2s = document.getElementsByName("interimPointId2");
            var interimPointId3s = document.getElementsByName("interimPointId3");
            var interimPointId4s = document.getElementsByName("interimPointId4");
            var ptpDropPointIds = document.getElementsByName("ptpDropPointId");

            // alert("last call : "+ptpVehicleTypeIds.length);

            if (ptpVehicleTypeIds.length > 1) {
                var rowCheck = (ptpVehicleTypeIds.length) - 1;
                for (var i = 0; i < rowCheck; i++) {
                    if ((ptpVehicleTypeIds[i].value == ptpVehicleTypeId) && (loadTypeIds[i].value == loadTypeId) && (containerTypeIds[i].value == containerTypeId) && (ptpPickupPointIds[i].value == ptpPickupPointId) && (interimPointId1s[i].value == interimPointId1) && (interimPointId2s[i].value == interimPointId2) && (interimPointId3s[i].value == interimPointId3) && (interimPointId4s[i].value == interimPointId4) && (ptpDropPointIds[i].value == ptpDropPointId)) {
                        checkStatus = true;
                    } else {
                        checkStatus = false;
                    }
//                    if(checkStatus) {
//                        ptpFromDate = ptpFromDate.split('-');
//                        ptpFromDate = ptpFromDate[2]+"-"+ptpFromDate[1]+"-"+ptpFromDate[0];
////                        alert(ptpFromDate)
//                        ptpToDate = ptpToDate.split('-');
//                        ptpToDate = ptpToDate[2]+"-"+ptpToDate[1]+"-"+ptpToDate[0];
//
//                        var fromDateNew = fromDate[i].value;
//
//                        var toDateNew = toDate[i].value;
//
//                     //   checkStatus = dateCheck(fromDateNew,toDateNew,ptpFromDate,ptpToDate);
//
//                    }
                    if (checkStatus) {
                        status = parseInt(i) + 1;
                        return status;
                    }

                }
            }
            return status;
        }

        function checkContractCaseExistsNew(ptpVehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId,
                interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId) {
            alert("in checkContractCaseExistsNew");
            $.ajax({
                url: "/throttle/getOrgContractExistCheck.do?",
                dataType: "json",
                data: {
                    ptpVehicleTypeId: ptpVehicleTypeId,
                    loadTypeId: loadTypeId,
                    containerTypeId: containerTypeId,
                    ptpPickupPointId: ptpPickupPointId,
                    interimPointId1: interimPointId1,
                    interimPointId2: interimPointId2,
                    interimPointId3: interimPointId3,
                    interimPointId4: interimPointId4,
                    ptpDropPointId: ptpDropPointId
                },
                success: function(data, textStatus, jqXHR) {
                    var routeContractId = data.routeContractId;
                    if (routeContractId == '') {
                        $("#contractStatus").text("");
                        //$("#save").show();
                    } else {
                        alert("There is already an contract existing with id =" + routeContractId);
                        $("#contractStatus").text("There is already an contract existing with id =" + routeContractId);

                    }
                },
                error: function(data, type) {
                    console.log(type);
                }
            });
        }
        function checkContractCaseExistsOLD14Mar2019(ptpVehicleTypeId, loadTypeId, containerTypeId,
                ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId) {
            // alert("in checkContractCaseExistsOLD14Mar2019");
            var status = 0;
            var checkStatus = false;
            var vehicleTypeIdEdit = document.getElementsByName("vehicleTypeIdEdit");
            var loadTypeIdEdit = document.getElementsByName("loadTypeIdEdit");
            var containerTypeIdEdit = document.getElementsByName("containerTypeIdEdit");
            var firstPointIdEdit = document.getElementsByName("firstPointIdEdit");
            var point1IdEdit = document.getElementsByName("point1IdEdit");
            var point2IdEdit = document.getElementsByName("point2IdEdit");
            var point3IdEdit = document.getElementsByName("point3IdEdit");
            var point4IdEdit = document.getElementsByName("point4IdEdit");
            var finalPointIdEdit = document.getElementsByName("finalPointIdEdit");
//            alert("ptpFromDate ===== "+ptpFromDate);
//            alert("ptpToDate ==== "+ptpToDate);
//            alert(ptpVehicleTypeId+" - "+vehicleTypeIdEdit[0].value);
//            alert(loadTypeId+" - "+loadTypeIdEdit[0].value);
//            alert(containerTypeId+" - "+containerTypeIdEdit[0].value);
            if (ptpPickupPointId == '') {
                ptpPickupPointId = 0;
            }
            if (interimPointId1 == '') {
                interimPointId1 = 0;
            }
            if (interimPointId2 == '') {
                interimPointId2 = 0;
            }
            if (interimPointId3 == '') {
                interimPointId3 = 0;
            }
            if (interimPointId4 == '') {
                interimPointId4 = 0;
            }
//            alert(ptpPickupPointId+" - "+firstPointIdEdit[0].value);
//            alert(interimPointId1+" - "+point1IdEdit[0].value);
//            alert(interimPointId2+" - "+point2IdEdit[0].value);
//            alert(interimPointId3+" - "+point3IdEdit[0].value);
//            alert(interimPointId4+" - "+point4IdEdit[0].value);
//            alert(ptpDropPointId+" - "+finalPointIdEdit[0].value);
            if (vehicleTypeIdEdit.length > 0) {
                for (var i = 0; i < vehicleTypeIdEdit.length; i++) {
                    if ((vehicleTypeIdEdit[i].value == ptpVehicleTypeId) && (loadTypeIdEdit[i].value == loadTypeId) && (containerTypeIdEdit[i].value == containerTypeId) && (firstPointIdEdit[i].value == ptpPickupPointId) && (point1IdEdit[i].value == interimPointId1) && (point2IdEdit[i].value == interimPointId2) && (point3IdEdit[i].value == interimPointId3) && (point4IdEdit[i].value == interimPointId4) && (finalPointIdEdit[i].value == ptpDropPointId)) {
                        checkStatus = true;
                    } else {
                        checkStatus = false;
                    }

                    if (checkStatus) {
//                        alert(checkStatus);
                        status = parseInt(i) + 1;
                        return status;
                    }

                }
            }

            var ptpVehicleTypeIds = document.getElementsByName("ptpVehicleTypeId");
            var loadTypeIds = document.getElementsByName("loadTypeId");
            var containerTypeIds = document.getElementsByName("containerTypeId");
            var ptpPickupPointIds = document.getElementsByName("ptpPickupPointId");
            var interimPointId1s = document.getElementsByName("interimPointId1");
            var interimPointId2s = document.getElementsByName("interimPointId2");
            var interimPointId3s = document.getElementsByName("interimPointId3");
            var interimPointId4s = document.getElementsByName("interimPointId4");
            var ptpDropPointIds = document.getElementsByName("ptpDropPointId");
            if (ptpVehicleTypeIds.length > 1) {
                var rowCheck = (ptpVehicleTypeIds.length) - 1;
                for (var i = 0; i < rowCheck; i++) {
                    if ((ptpVehicleTypeIds[i].value == ptpVehicleTypeId) && (loadTypeIds[i].value == loadTypeId) && (containerTypeIds[i].value == containerTypeId) && (ptpPickupPointIds[i].value == ptpPickupPointId) && (interimPointId1s[i].value == interimPointId1) && (interimPointId2s[i].value == interimPointId2) && (interimPointId3s[i].value == interimPointId3) && (interimPointId4s[i].value == interimPointId4) && (ptpDropPointIds[i].value == ptpDropPointId)) {
                        checkStatus = true;
                    } else {
                        checkStatus = false;
                    }
                    if (checkStatus) {
                        status = parseInt(i) + 1;
                        return status;
                    }

                }
            }
            return status;
        }

        function dateCheck(from, to, fromCheck, toCheck) {
            var retStatus = false;
            var fDate, lDate, cDate;
            var aStart = new Date(from);
            var aEnd = new Date(to);
            var bStart = new Date(fromCheck);
            var bEnd = new Date(toCheck);
            if (from == fromCheck && to == toCheck) {
                retStatus = true;
            } else if ((bStart > aStart && bStart < aEnd) && (bEnd > aStart && bEnd < aEnd)) {
                retStatus = true;
            } else if (bStart > aStart && bStart < aEnd) {
                retStatus = true;
            } else if (bEnd > aStart && bEnd < aEnd) {
                retStatus = true;
            } else {
                retStatus = false;
            }
            return retStatus;
        }
    </script>
    <%index++;%>   </c:if>



<c:if test="${billingTypeId == 4}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.

            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  /></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control"  ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width: 90px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width: 90px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width: 90px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width: 90px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width: 90px;"/></td>',
                '<td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width: 90px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width: 90px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width: 90px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width: 90px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width: 90px;"/></td>',
                '<td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width: 90px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width: 90px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width: 90px;"/><input type="hidden" name="ptpRateWithReefer" value="0" class="form-control" id="ptpRateWithReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="ptpRateWithoutReefer" value="0" class="form-control" id="ptpRateWithoutReefer"  style="width: 90px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');
            var count = 1;
            // Add row to list and allow user to use autocomplete to find items.


            $("#addRow").bind('click', function() {
                count++;
                var $row = $(rowTemp);
                // save reference to inputs within row
                var $ptpRouteContractCode = $row.find('#ptpRouteContractCode');
                var $ptpVehicleTypeId = $row.find('#ptpVehicleTypeId');
                var $ptpPickupPoint = $row.find('#ptpPickupPoint');
                var $ptpPickupPointId = $row.find('#ptpPickupPointId');
                var $interimPoint1 = $row.find('#interimPoint1');
                var $interimPointId1 = $row.find('#interimPointId1');
                var $interimPoint1Km = $row.find('#interimPoint1Km');
                var $interimPoint1Hrs = $row.find('#interimPoint1Hrs');
                var $interimPoint1Minutes = $row.find('#interimPoint1Minutes');
                var $interimPoint1RouteId = $row.find('#interimPoint1RouteId');
                var $interimPoint2 = $row.find('#interimPoint2');
                var $interimPointId2 = $row.find('#interimPointId2');
                var $interimPoint2Km = $row.find('#interimPoint2Km');
                var $interimPoint2Hrs = $row.find('#interimPoint2Hrs');
                var $interimPoint2Minutes = $row.find('#interimPoint2Minutes');
                var $interimPoint2RouteId = $row.find('#interimPoint2RouteId');
                var $interimPoint3 = $row.find('#interimPoint3');
                var $interimPointId3 = $row.find('#interimPointId3');
                var $interimPoint3Km = $row.find('#interimPoint3Km');
                var $interimPoint3Hrs = $row.find('#interimPoint3Hrs');
                var $interimPoint3Minutes = $row.find('#interimPoint3Minutes');
                var $interimPoint3RouteId = $row.find('#interimPoint3RouteId');
                var $interimPoint4 = $row.find('#interimPoint4');
                var $interimPointId4 = $row.find('#interimPointId4');
                var $interimPoint4Km = $row.find('#interimPoint4Km');
                var $interimPoint4Hrs = $row.find('#interimPoint4Hrs');
                var $interimPoint4Minutes = $row.find('#interimPoint4Minutes');
                var $interimPoint4RouteId = $row.find('#interimPoint4RouteId');
                var $ptpDropPoint = $row.find('#ptpDropPoint');
                var $ptpDropPointId = $row.find('#ptpDropPointId');
                var $ptpDropPointKm = $row.find('#ptpDropPointKm');
                var $ptpDropPointHrs = $row.find('#ptpDropPointHrs');
                var $ptpDropPointMinutes = $row.find('#ptpDropPointMinutes');
                var $ptpDropPointRouteId = $row.find('#ptpDropPointRouteId');
                var $ptpTotalKm = $row.find('#ptpTotalKm');
                var $ptpTotalHours = $row.find('#ptpTotalHours');
                var $ptpTotalMinuites = $row.find('#ptpTotalMinutes');
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');

                if ($('#ptpRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
                    $row.find('#ptpPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find('#ptpPickupPoint').val('');
                                        $row.find('#ptpPickupPointId').val('');
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#ptpPickupPoint').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                            $itemrow.find('#interimPoint1').focus();
                            $row.find("#interimPoint1").val('');
                            $row.find("#interimPointId1").val('');
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint1Km").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint1Hrs").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint1Minutes").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint1RouteId").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint1').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint1").val('');
                                        $row.find("#interimPointId1").val('');
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint1Km").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint1Hrs").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint1Minutes").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint1RouteId").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint1').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint1').val(tmp[0]);
                            $itemrow.find('#interimPointId1').val(tmp[1]);
                            $itemrow.find('#interimPoint1Km').val(tmp[2]);
                            $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint2').focus();
                            $row.find("#interimPoint2").val('');
                            $row.find("#interimPointId2").val('');
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint2Km").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint2Hrs").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint2Minutes").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint2RouteId").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint2').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint2").val('');
                                        $row.find("#interimPointId2").val('');
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint2Km").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint2Hrs").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint2Minutes").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint2RouteId").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint2').focus();
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint2').val(tmp[0]);
                            $itemrow.find('#interimPointId2').val(tmp[1]);
                            $itemrow.find('#interimPoint2Km').val(tmp[2]);
                            $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint3').focus();
                            $row.find("#interimPoint3").val('');
                            $row.find("#interimPointId3").val('');
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint3Km").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint3Hrs").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint3Minutes").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint3RouteId").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#interimPoint3').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint4: $row.find('#interimPointId4').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint3").val('');
                                        $row.find("#interimPointId3").val('');
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint3Km").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint3Hrs").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint3Minutes").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint3RouteId").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint3').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint3').val(tmp[0]);
                            $itemrow.find('#interimPointId3').val(tmp[1]);
                            $itemrow.find('#interimPoint3Km').val(tmp[2]);
                            $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                            $itemrow.find('#interimPoint4').focus();
                            $row.find("#interimPoint4").val('');
                            $row.find("#interimPointId4").val('');
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#interimPoint4Km").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#interimPoint4Hrs").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#interimPoint4Minutes").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#interimPoint4RouteId").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                    $row.find('#interimPoint4').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    ptpDropPoint: $row.find('#ptpDropPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#interimPoint4").val('');
                                        $row.find("#interimPointId4").val('');
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#interimPoint4Km").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#interimPoint4Hrs").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#interimPoint4Minutes").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#interimPoint4RouteId").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find('#interimPoint4').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#interimPoint4').val(tmp[0]);
                            $itemrow.find('#interimPointId4').val(tmp[1]);
                            $itemrow.find('#interimPoint4Km').val(tmp[2]);
                            $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                            $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                            $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                            $itemrow.find('#ptpDropPoint').focus();
                            $row.find("#ptpDropPoint").val('');
                            $row.find("#ptpDropPointId").val('');
                            $row.find("#ptpDropPointKm").val('');
                            $row.find("#ptpDropPointHrs").val('');
                            $row.find("#ptpDropPointMinutes").val('');
                            $row.find("#ptpDropPointRouteId").val('');
                            $row.find("#ptpTotalKm").val('');
                            $row.find("#ptpTotalHours").val('');
                            $row.find("#ptpTotalMinutes").val('');
                            $row.find("#ptpRateWithReefer").val('');
                            $row.find("#ptpRateWithoutReefer").val('');
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                    $row.find('#ptpDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpPickupPoint: $row.find('#ptpPickupPointId').val(),
                                    interimPoint1: $row.find('#interimPointId1').val(),
                                    interimPoint2: $row.find('#interimPointId2').val(),
                                    interimPoint3: $row.find('#interimPointId3').val(),
                                    interimPoint4: $row.find('#interimPointId4').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {
//
                                        $row.find("#ptpDropPoint").val('');
                                        $row.find("#ptpDropPointId").val('');
                                        $row.find("#ptpDropPointKm").val('');
                                        $row.find("#ptpDropPointHrs").val('');
                                        $row.find("#ptpDropPointMinutes").val('');
                                        $row.find("#ptpDropPointRouteId").val('');
                                        $row.find("#ptpTotalKm").val('');
                                        $row.find("#ptpTotalHours").val('');
                                        $row.find("#ptpTotalMinutes").val('');
                                        $row.find("#ptpRateWithReefer").val('');
                                        $row.find("#ptpRateWithoutReefer").val('');
                                        $row.find('#ptpDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $itemrow.find('#ptpDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpDropPointId').val(tmp[1]);
                            var vehicleTypeId = $itemrow.find('#ptpVehicleTypeId').val();
                            var loadTypeId = $itemrow.find('#loadTypeId').val();
                            var containerTypeId = $itemrow.find('#containerTypeId').val();
                            var containerQty = $itemrow.find('#containerQty').val();

                            var ptpPickupPointId = $itemrow.find('#ptpPickupPointId').val();
                            var interimPointId1 = $itemrow.find('#interimPointId1').val();
                            var interimPointId2 = $itemrow.find('#interimPointId2').val();
                            var interimPointId3 = $itemrow.find('#interimPointId3').val();
                            var interimPointId4 = $itemrow.find('#interimPointId4').val();
                            var ptpDropPointId = $itemrow.find('#ptpDropPointId').val();
                            var customerId = document.getElementById('customerId').value;
                            var statusRes = 0;
                            // alert("customerId:"+customerId);
                            if (customerId != "0") {
                                //    alert("1");
                                var ptpFromDate = $itemrow.find('#ptpFromDate').val();
                                var ptpToDate = $itemrow.find('#ptpToDate').val();
                                statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId,
                                        ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4,
                                        ptpDropPointId, ptpFromDate, ptpToDate);
                            } else {
                                //  alert("2");
                                statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId,
                                        ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4,
                                        ptpDropPointId);
                            }
                            //alert("statusRes="+statusRes)
                            if (statusRes > 0) {
                                alert("The same combination already exists in row " + statusRes + ", please check.");
                                $itemrow.find('#ptpVehicleTypeId').val(0);
                                $itemrow.find('#loadTypeId').val(0);
                                $itemrow.find('#containerTypeId').val(0);
                                $itemrow.find('#ptpPickupPoint').val('');
                                $itemrow.find('#ptpPickupPointId').val('');
                                $itemrow.find('#interimPoint1').val('');
                                $itemrow.find('#interimPointId1').val('');
                                $itemrow.find('#interimPoint2').val('');
                                $itemrow.find('#interimPointId2').val('');
                                $itemrow.find('#interimPoint3').val('');
                                $itemrow.find('#interimPointId3').val('');
                                $itemrow.find('#interimPoint4').val('');
                                $itemrow.find('#interimPointId4').val('');
                                $itemrow.find('#ptpDropPoint').val('');
                                $itemrow.find('#ptpDropPointId').val('');
                            } else {
                                $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                                $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                                $row.find("#ptpRateWithReefer").val('');
                                $row.find("#ptpRateWithoutReefer").val('');
                                if ($row.find('#interimPoint1Km').val() !== '') {
                                    var interimPoint1Km = $row.find('#interimPoint1Km').val();
                                } else {
                                    var interimPoint1Km = 0;
                                }
                                if ($row.find('#interimPoint2Km').val() !== '') {
                                    var interimPoint2Km = $row.find('#interimPoint2Km').val();
                                } else {
                                    var interimPoint2Km = 0;
                                }
                                if ($row.find('#interimPoint3Km').val() !== '') {
                                    var interimPoint3Km = $row.find('#interimPoint3Km').val();
                                } else {
                                    var interimPoint3Km = 0;
                                }
                                if ($row.find('#interimPoint4Km').val() !== '') {
                                    var interimPoint4Km = $row.find('#interimPoint4Km').val();
                                } else {
                                    var interimPoint4Km = 0;
                                }


                                if ($row.find('#interimPoint1Hrs').val() !== '') {
                                    var interimPoint1Hrs = $row.find('#interimPoint1Hrs').val();
                                } else {
                                    var interimPoint1Hrs = 0;
                                }
                                if ($row.find('#interimPoint2Hrs').val() !== '') {
                                    var interimPoint2Hrs = $row.find('#interimPoint2Hrs').val();
                                } else {
                                    var interimPoint2Hrs = 0;
                                }
                                if ($row.find('#interimPoint3Hrs').val() !== '') {
                                    var interimPoint3Hrs = $row.find('#interimPoint3Hrs').val();
                                } else {
                                    var interimPoint3Hrs = 0;
                                }
                                if ($row.find('#interimPoint4Hrs').val() !== '') {
                                    var interimPoint4Hrs = $row.find('#interimPoint4Hrs').val();
                                } else {
                                    var interimPoint4Hrs = 0;
                                }

                                if ($row.find('#interimPoint1Minutes').val() !== '') {
                                    var interimPoint1Minutes = $row.find('#interimPoint1Minutes').val();
                                } else {
                                    var interimPoint1Minutes = 0;
                                }
                                if ($row.find('#interimPoint2Minutes').val() !== '') {
                                    var interimPoint2Minutes = $row.find('#interimPoint2Minutes').val();
                                } else {
                                    var interimPoint2Minutes = 0;
                                }
                                if ($row.find('#interimPoint3Minutes').val() !== '') {
                                    var interimPoint3Minutes = $row.find('#interimPoint3Minutes').val();
                                } else {
                                    var interimPoint3Minutes = 0;
                                }
                                if ($row.find('#interimPoint4Minutes').val() !== '') {
                                    var interimPoint4Minutes = $row.find('#interimPoint4Minutes').val();
                                } else {
                                    var interimPoint4Minutes = 0;
                                }


                                var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                                $row.find('#ptpTotalKm').val(totalKm);
                                var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                                $row.find('#ptpTotalHours').val(totalHrs);
                                var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                                $row.find('#ptpTotalMinutes').val(totalMinutes);
                                $itemrow.find('#ptpRateWithReefer').focus();
                            }
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpRouteContractCode).focus();



                } // End if last itemCode input is empty
                return false;
            });

        });// End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });


        $(document).ready(function() {
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
//
                                $('#ptpPickupPoint').val('');
                                $('#ptpPickupPointId').val('');
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#ptpPickupPoint').focus();
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpPickupPointId').val(tmp[1]);
                    $itemrow.find('#interimPoint1').focus();
                    $("#interimPoint1").val('');
                    $("#interimPointId1").val('');
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint1Km").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint1Hrs").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint1Minutes").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint1RouteId").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint1').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
//
                                $("#interimPoint1").val('');
                                $("#interimPointId1").val('');
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint1Km").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint1Hrs").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint1Minutes").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint1RouteId").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint1').focus();
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint1").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint1').val(tmp[0]);
                    $itemrow.find('#interimPointId1').val(tmp[1]);
                    $itemrow.find('#interimPoint1Km').val(tmp[2]);
                    $itemrow.find('#interimPoint1Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint1Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint1RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint2').focus();
                    $("#interimPoint2").val('');
                    $("#interimPointId2").val('');
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint2Km").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint2Hrs").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint2Minutes").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint2RouteId").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
//
                                $("#interimPoint2").val('');
                                $("#interimPointId2").val('');
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint2Km").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint2Hrs").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint2Minutes").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint2RouteId").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint2').focus();
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint2").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint2').val(tmp[0]);
                    $itemrow.find('#interimPointId2').val(tmp[1]);
                    $itemrow.find('#interimPoint2Km').val(tmp[2]);
                    $itemrow.find('#interimPoint2Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint2Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint2RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint3').focus();
                    $("#interimPoint3").val('');
                    $("#interimPointId3").val('');
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint3Km").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint3Hrs").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint3Minutes").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint3RouteId").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
//
                                $("#interimPoint3").val('');
                                $("#interimPointId3").val('');
                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint3Km").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint3Hrs").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint3Minutes").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint3RouteId").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint3').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint3").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint3').val(tmp[0]);
                    $itemrow.find('#interimPointId3').val(tmp[1]);
                    $itemrow.find('#interimPoint3Km').val(tmp[2]);
                    $itemrow.find('#interimPoint3Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint3Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint3RouteId').val(tmp[5]);
                    $itemrow.find('#interimPoint4').focus();
                    $("#interimPoint4").val('');
                    $("#interimPointId4").val('');
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#interimPoint4Km").val('');
                    $("#ptpDropPointKm").val('');
                    $("#interimPoint4Hrs").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#interimPoint4Minutes").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#interimPoint4RouteId").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#interimPoint4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {

                                $("#interimPoint4").val('');
                                $("#interimPointId4").val('');
                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#interimPoint4Km").val('');
                                $("#ptpDropPointKm").val('');
                                $("#interimPoint4Hrs").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#interimPoint4Minutes").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#interimPoint4RouteId").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                                $("#ptpTotalMinutes").val('');
                                $('#interimPoint4').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#interimPoint4").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#interimPoint4').val(tmp[0]);
                    $itemrow.find('#interimPointId4').val(tmp[1]);
                    $itemrow.find('#interimPoint4Km').val(tmp[2]);
                    $itemrow.find('#interimPoint4Hrs').val(tmp[3]);
                    $itemrow.find('#interimPoint4Minutes').val(tmp[4]);
                    $itemrow.find('#interimPoint4RouteId').val(tmp[5]);
                    $itemrow.find('#ptpDropPoint').focus();
                    $("#ptpDropPoint").val('');
                    $("#ptpDropPointId").val('');
                    $("#ptpDropPointKm").val('');
                    $("#ptpDropPointHrs").val('');
                    $("#ptpDropPointMinutes").val('');
                    $("#ptpDropPointRouteId").val('');
                    $("#ptpTotalKm").val('');
                    $("#ptpTotalHours").val('');
                    $("#ptpTotalMinutes").val('');
                    $("#ptpRateWithReefer").val('0');
                    $("#ptpRateWithoutReefer").val('0');
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            //#ptpPickupPoint,#ptpRouteContractCode,#interimPoint1,#interimPoint2,#interimPoint3,#interimPoint4,#ptpDropPoint
            $('#ptpDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpPickupPoint: document.getElementById("ptpPickupPointId").value,
                            interimPoint1: document.getElementById("interimPointId1").value,
                            interimPoint2: document.getElementById("interimPointId2").value,
                            interimPoint3: document.getElementById("interimPointId3").value,
                            interimPoint4: document.getElementById("interimPointId4").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {

                                $("#ptpDropPoint").val('');
                                $("#ptpDropPointId").val('');
                                $("#ptpDropPointKm").val('');
                                $("#ptpDropPointHrs").val('');
                                $("#ptpDropPointMinutes").val('');
                                $("#ptpDropPointRouteId").val('');
                                $("#ptpTotalKm").val('');
                                $("#ptpTotalHours").val('');
                                $("#ptpTotalMinutes").val('');
                                $("#ptpRateWithReefer").val('0');
                                $("#ptpRateWithoutReefer").val('0');
                                $('#ptpDropPoint').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpDropPointId').val(tmp[1]);
                    var vehicleTypeId = $itemrow.find('#ptpVehicleTypeId').val();
                    var loadTypeId = $itemrow.find('#loadTypeId').val();
                    var containerTypeId = $itemrow.find('#containerTypeId').val();
                    var ptpPickupPointId = $itemrow.find('#ptpPickupPointId').val();
                    var interimPointId1 = $itemrow.find('#interimPointId1').val();
                    var interimPointId2 = $itemrow.find('#interimPointId2').val();
                    var interimPointId3 = $itemrow.find('#interimPointId3').val();
                    var interimPointId4 = $itemrow.find('#interimPointId4').val();
                    var ptpDropPointId = $itemrow.find('#ptpDropPointId').val();
                    var customerId = document.getElementById('customerId').value;
                    var statusRes = 0;

                    if (customerId != "0") {
                        var ptpFromDate = $itemrow.find('#ptpFromDate').val();
                        var ptpToDate = $itemrow.find('#ptpToDate').val();
                        statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                        //statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId, ptpFromDate, ptpToDate);
                    } else {
                        statusRes = checkContractCaseExists(vehicleTypeId, loadTypeId, containerTypeId, ptpPickupPointId, interimPointId1, interimPointId2, interimPointId3, interimPointId4, ptpDropPointId);
                    }
                    if (statusRes > 0) {
                        alert("The same combination already exists in row " + statusRes + ", please check.");
                        $itemrow.find('#ptpVehicleTypeId').val(0);
                        $itemrow.find('#loadTypeId').val(0);
                        $itemrow.find('#containerTypeId').val(0);
                        $itemrow.find('#ptpPickupPoint').val('');
                        $itemrow.find('#ptpPickupPointId').val('');
                        $itemrow.find('#interimPoint1').val('');
                        $itemrow.find('#interimPointId1').val('');
                        $itemrow.find('#interimPoint2').val('');
                        $itemrow.find('#interimPointId2').val('');
                        $itemrow.find('#interimPoint3').val('');
                        $itemrow.find('#interimPointId3').val('');
                        $itemrow.find('#interimPoint4').val('');
                        $itemrow.find('#interimPointId4').val('');
                        $itemrow.find('#ptpDropPoint').val('');
                        $itemrow.find('#ptpDropPointId').val('');
                    } else {
                        $itemrow.find('#ptpDropPointKm').val(tmp[2]);
                        $itemrow.find('#ptpDropPointHrs').val(tmp[3]);
                        $itemrow.find('#ptpDropPointMinutes').val(tmp[4]);
                        $itemrow.find('#ptpDropPointRouteId').val(tmp[5]);
                        $("#ptpRateWithReefer").val('0');
                        $("#ptpRateWithoutReefer").val('0');
                        if (document.getElementById("interimPoint1Km").value != '') {
                            var interimPoint1Km = document.getElementById("interimPoint1Km").value;
                        } else {
                            var interimPoint1Km = 0;
                        }
                        if (document.getElementById("interimPoint2Km").value != '') {
                            var interimPoint2Km = document.getElementById("interimPoint2Km").value;
                        } else {
                            var interimPoint2Km = 0;
                        }
                        if (document.getElementById("interimPoint3Km").value != '') {
                            var interimPoint3Km = document.getElementById("interimPoint3Km").value;
                        } else {
                            var interimPoint3Km = 0;
                        }
                        if (document.getElementById("interimPoint4Km").value != '') {
                            var interimPoint4Km = document.getElementById("interimPoint4Km").value;
                        } else {
                            var interimPoint4Km = 0;
                        }

                        if (document.getElementById("interimPoint1Hrs").value != '') {
                            var interimPoint1Hrs = document.getElementById("interimPoint1Hrs").value;
                        } else {
                            var interimPoint1Hrs = 0;
                        }
                        if (document.getElementById("interimPoint2Hrs").value != '') {
                            var interimPoint2Hrs = document.getElementById("interimPoint2Hrs").value;
                        } else {
                            var interimPoint2Hrs = 0;
                        }
                        if (document.getElementById("interimPoint3Hrs").value != '') {
                            var interimPoint3Hrs = document.getElementById("interimPoint3Hrs").value;
                        } else {
                            var interimPoint3Hrs = 0;
                        }
                        if (document.getElementById("interimPoint4Hrs").value != '') {
                            var interimPoint4Hrs = document.getElementById("interimPoint4Hrs").value;
                        } else {
                            var interimPoint4Hrs = 0;
                        }

                        if (document.getElementById("interimPoint1Minutes").value != '') {
                            var interimPoint1Minutes = document.getElementById("interimPoint1Minutes").value;
                        } else {
                            var interimPoint1Minutes = 0;
                        }
                        if (document.getElementById("interimPoint2Minutes").value != '') {
                            var interimPoint2Minutes = document.getElementById("interimPoint2Minutes").value;
                        } else {
                            var interimPoint2Minutes = 0;
                        }
                        if (document.getElementById("interimPoint3Minutes").value != '') {
                            var interimPoint3Minutes = document.getElementById("interimPoint3Minutes").value;
                        } else {
                            var interimPoint3Minutes = 0;
                        }
                        if (document.getElementById("interimPoint4Minutes").value != '') {
                            var interimPoint4Minutes = document.getElementById("interimPoint4Minutes").value;
                        } else {
                            var interimPoint4Minutes = 0;
                        }

                        var totalKm = parseInt(tmp[2]) + parseInt(interimPoint1Km) + parseInt(interimPoint2Km) + parseInt(interimPoint3Km) + parseInt(interimPoint4Km);
                        $itemrow.find('#ptpTotalKm').val(totalKm);
                        var totalHrs = parseInt(tmp[3]) + parseInt(interimPoint1Hrs) + parseInt(interimPoint2Hrs) + parseInt(interimPoint3Hrs) + parseInt(interimPoint4Hrs);
                        $itemrow.find('#ptpTotalHours').val(totalHrs);
                        var totalMinutes = parseInt(tmp[4]) + parseInt(interimPoint1Minutes) + parseInt(interimPoint2Minutes) + parseInt(interimPoint3Minutes) + parseInt(interimPoint4Minutes);
                        $itemrow.find('#ptpTotalMinutes').val(totalMinutes);
                        $itemrow.find('#ptpRateWithReefer').focus();
                    }
                    return false;

                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
            </script>


</c:if>
<c:if test="${billingTypeId == 2}">
    <script>
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable1');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.
            var count = 0;
            var rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                '<td><input name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="" class="form-control" style="width:120px"/></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="ptpwVehicleTypeId" id="ptpwVehicleTypeId"  class="form-control" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                '<td><input type="hidden" name="ptpwPickupPointId" id="ptpwPickupPointId" value="" class="form-control"  style="width: 120px;"/><input name="ptpwPickupPoint" id="ptpwPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>',
                '<td><input name="ptpwDropPoint" id="ptpwDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpwDropPointId" id="ptpwDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwPointRouteId" id="ptpwPointRouteId" value="" class="form-control"  style="width: 120px;"/></td>',
                '<td><input name="ptpwTotalKm" id="ptpwTotalKm" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" readonly/><input type="hidden" name="ptpwTotalHrs" id="ptpwTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwTotalMinutes" id="ptpwTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>',
                '<td><input name="ptpwRateWithReefer" id="ptpwRateWithReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>',
                '<td><input name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '</tr>'
            ].join('');

            // Add row to list and allow user to use autocomplete to find items.
            $("#addRow1").bind('click', function() {
                var $row = $(rowTemp);
                count++;
                // save reference to inputs within row
                var $ptpwRouteContractCode = $row.find('#ptpwRouteContractCode');
                var $ptpwVehicleTypeId = $row.find('#ptpwVehicleTypeId');
                var $ptpwPickupPointId = $row.find('#ptpwPickupPointId');
                var $ptpwPickupPoint = $row.find('#ptpwPickupPoint');
                var $ptpwDropPoint = $row.find('#ptpwDropPoint');
                var $ptpwDropPointId = $row.find('#ptpwDropPointId');
                var $ptpwPointRouteId = $row.find('#ptpwPointRouteId');
                var $ptpwTotalKm = $row.find('#ptpwTotalKm');
                var $ptpwTotalHrs = $row.find('#ptpwTotalHrs');
                var $ptpwTotalMinutes = $row.find('#ptpwTotalMinutes');
                var $ptpwRateWithReefer = $row.find('#ptpwRateWithReefer');
                var $ptpwRateWithoutReefer = $row.find('#ptpwRateWithoutReefer');

                if ($('#ptpwRouteContractCode:last').val() !== '') {
                    // apply autocomplete widget to newly created row
                    $row.find('#ptpwPickupPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwDropPoint: $row.find('#ptpwPointRouteId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {

                                        $row.find("#ptpwPickupPoint").val('');
                                        $row.find("#ptpwPickupPointId").val('');
                                        $row.find('#ptpwDropPoint').val('');
                                        $row.find('#ptpwDropPointId').val('');
                                        $row.find('#ptpwTotalKm').val('');
                                        $row.find('#ptpwTotalHrs').val('');
                                        $row.find('#ptpwTotalMinutes').val('');
                                        $row.find('#ptpwPointRouteId').val('');
                                        $row.find('#ptpwRateWithReefer').val('');
                                        $row.find('#ptpwRateWithoutReefer').val('');
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                            $itemrow.find('#ptpwPickupPointId').val(tmp[1]);

                            $itemrow.find('#ptpwDropPoint').val('');
                            $itemrow.find('#ptpwDropPointId').val('');
                            $itemrow.find('#ptpwTotalKm').val('');
                            $itemrow.find('#ptpwTotalHrs').val('');
                            $itemrow.find('#ptpwTotalMinutes').val('');
                            $itemrow.find('#ptpwPointRouteId').val('');
                            $itemrow.find('#ptpwRateWithReefer').val('');
                            $itemrow.find('#ptpwRateWithoutReefer').val('');
                            $itemrow.find('#ptpwDropPoint').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                    $row.find('#ptpwDropPoint').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getCCPointToPointWeightRouteName.do",
                                dataType: "json",
                                data: {
                                    elementName: $(this.element).prop("id"),
                                    elementValue: request.term,
                                    rowCount: count,
                                    ptpwPickupPoint: $row.find('#ptpwPickupPointId').val()
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //console.log( data);
                                    var items = data;
                                    if (items == '') {

                                        $row.find("#ptpwDropPoint").val('');
                                        $row.find("#ptpwDropPointId").val('');
                                        $row.find("#ptpwPointRouteId").val('');
                                        $row.find("#ptpwTotalKm").val('');
                                        $row.find("#ptpwTotalHrs").val('');
                                        $row.find("#ptpwTotalMinutes").val('');
                                        $row.find("#ptpwRateWithReefer").val('');
                                        $row.find("#ptpwRateWithoutReefer").val('');
                                        $row.find('#ptpwDropPoint').focus();
                                    } else {
                                        //alert(items);
                                        response(items);
                                    }
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 0,
                        select: function(event, ui) {
                            var $itemrow = $(this).closest('tr');
                            // Populate the input fields from the returned values
                            var value = ui.item.Name;
                            //                                alert(value);
                            var tmp = value.split('-');
                            $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                            $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                            $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                            $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                            $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                            $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                            $itemrow.find('#ptpwRateWithReefer').focus();
                            return false;
                        }
                        // Format the list menu output of the autocomplete
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        //alert(item);
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[0] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                //.append( "<a>"+ item.Name + "</a>" )
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                    // Add row after the first row in table
                    $('.item-row:last', $itemsTable).after($row);
                    $($ptpwRouteContractCode).focus();

                } // End if last itemCode input is empty
                return false;
            });

        });// End DOM

        // Remove row when clicked
        $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });




        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#ptpwPickupPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwDropPoint: document.getElementById("ptpwDropPoint").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            //alert(items);
                            if (items == '') {

                                $("#ptpwPickupPoint").val('');
                                $("#ptpwPickupPointId").val('');
                                $('#ptpwDropPoint').val('');
                                $('#ptpwDropPointId').val('');
                                $('#ptpwTotalKm').val('');
                                $('#ptpwTotalHrs').val('');
                                $('#ptpwTotalMinutes').val('');
                                $('#ptpwPointRouteId').val('');
                                $('#ptpwRateWithReefer').val('');
                                $('#ptpwRateWithoutReefer').val('');
                            } else {
                                //alert(items);
                                response(items);
                            }
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#ptpwPickupPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwPickupPoint').val(tmp[0]);
                    $itemrow.find('#ptpwPickupPointId').val(tmp[1]);
                    if ($itemrow.find('#ptpwDropPoint').val() != '') {
                        $itemrow.find('#ptpwDropPoint').val('');
                        $itemrow.find('#ptpwDropPointId').val('');
                        $itemrow.find('#ptpwTotalKm').val('');
                        $itemrow.find('#ptpwTotalHrs').val('');
                        $itemrow.find('#ptpwTotalMinutes').val('');
                        $itemrow.find('#ptpwPointRouteId').val('');
                        $itemrow.find('#ptpwRateWithReefer').val('');
                        $itemrow.find('#ptpwRateWithoutReefer').val('');
                    }
                    $itemrow.find('#ptpwDropPoint').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

            $('#ptpwDropPoint').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCCPointToPointWeightRouteName.do",
                        dataType: "json",
                        data: {
                            elementName: $(this.element).prop("id"),
                            elementValue: request.term,
                            rowCount: 1,
                            ptpwPickupPoint: document.getElementById("ptpwPickupPointId").value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {

                                $("#ptpwDropPoint").val('');
                                $("#ptpwDropPointId").val('');
                                $("#ptpwPointRouteId").val('');
                                $("#ptpwTotalKm").val('');
                                $("#ptpwTotalHrs").val('');
                                $("#ptpwTotalMinutes").val('');
                                $("#ptpwRateWithReefer").val('');
                                $("#ptpwRateWithoutReefer").val('');
                                $('#ptpwDropPoint').focus();
                            } else {
                                //alert(items);
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 0,
                select: function(event, ui) {
                    $("#ptpwDropPoint").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#ptpwDropPoint').val(tmp[0]);
                    $itemrow.find('#ptpwDropPointId').val(tmp[1]);
                    $itemrow.find('#ptpwTotalKm').val(tmp[2]);
                    $itemrow.find('#ptpwTotalHrs').val(tmp[3]);
                    $itemrow.find('#ptpwTotalMinutes').val(tmp[4]);
                    $itemrow.find('#ptpwPointRouteId').val(tmp[5]);
                    $itemrow.find('#ptpwRateWithReefer').focus();
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[0] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });

            </script>

</c:if>

<script type="text/javascript">
    function submitPage(value) {
        var validate = "";
        //        var validate = customerValidation();
        var a = 0;
        //        var  customerValidation();ptpVehicleTypeId
        var vehtypID = document.getElementsByName('ptpVehicleTypeId');
        var approvalStatus = document.getElementsByName('approvalStatus');
        if (approvalStatus == '0') {
            alert("please wait for contract approval status");
            $("#approvalStatus").focus();
            return;
        }
        //alert("ptpVehicleTypeId ------ "+vehtypID);
        var addMoreRouteValue = $("#addMoreRouteValue").val();
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpwRouteContractCode = document.getElementsByName('ptpwRouteContractCode');
        if (document.getElementById('customerId').value != '0') {
            if (document.getElementById('billingTypeId').value == "1") {
                var routeContractId = document.getElementsByName('routeContractId');
                var ptpRateWithReefer1 = document.getElementsByName('ptpRateWithReefer1');
                var ptpRateWithoutReefer1 = document.getElementsByName('ptpRateWithoutReefer1');
                for (var k = 0; k < routeContractId.length; k++) {
                    a = k + 1;
                    if (ptpRateWithReefer1[k].value == '') {
                        alert("please enter the reefer with reefer amount for row " + a);
                        ptpRateWithReefer1[k].focus();
                        return;
                    } else if (ptpRateWithoutReefer1[k].value == '') {
                        alert("please enter the reefer without reefer amount for row " + a);
                        ptpRateWithoutReefer1[k].focus();
                        return;
//                    }else if (ptpRateWithoutReefer1[k].value == '0' || ptpRateWithoutReefer1[k].value == '0.00') {
//                        alert("please enter the reefer without reefer amount for row " + a);
//                        ptpRateWithoutReefer1[k].focus();
//                        return;
//                    }else if (ptpRateWithoutReefer1[k].value == '0' || ptpRateWithoutReefer1[k].value == '0.00') {
//                        alert("please enter the reefer without reefer amount for row " + a);
//                        ptpRateWithoutReefer1[k].focus();
//                        return;
                    }
                }
                if (ptpRouteContractCode.length > 0 && addMoreRouteValue == 1) {
                    validate = validatePtpContract(a);
                } else {
                    validate = 'true';
                }
                if (validate == 'true') {
                    document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                    document.customerContract.submit();
                }
            } else if (document.getElementById('billingTypeId').value == "2") {
                var routeContractId = document.getElementsByName('routeContractId');
                var ptpwRateWithReefer1 = document.getElementsByName('ptpwRateWithReefer1');
                var ptpwRateWithoutReefer1 = document.getElementsByName('ptpwRateWithoutReefer1');
                for (var k = 0; k < routeContractId.length; k++) {
                    a = k + 1;
                    if (ptpwRateWithReefer1[k].value == '') {
                        alert("please enter the reefer with reefer amount for row " + a);
                        ptpwRateWithReefer1[k].focus();
                        return;
                    } else if (ptpwRateWithoutReefer1[k].value == '') {
                        alert("please enter the reefer without reefer amount for row " + a);
                        ptpwRateWithoutReefer1[k].focus();
                        return;
                    }
                }
                if (ptpwRouteContractCode.length > 0 && addMoreRouteValue == 1) {
                    validate = validatePtpwContract(a);
                } else {
                    validate = 'true';
                }
                if (validate == 'true') {
                    document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                    document.customerContract.submit();
                }
            } else if (document.getElementById('billingTypeId').value == "4") {
                var routeContractId = document.getElementsByName('routeContractId');
                a = routeContractId.length;
                if (addMoreRouteValue >= 1) {
                    validate = validateFixedContract(a);
                } else {
                    validate = 'true';
                }
                if (validate == 'true') {
                    document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                    document.customerContract.submit();
                }
            } else if (document.getElementById('billingTypeId').value == "3") {
                var contractRateId = document.getElementsByName('contractRateId');
                var vehicleRatePerKm = document.getElementsByName('vehicleRatePerKm');
                var reeferRatePerHour = document.getElementsByName('reeferRatePerHour');
                for (var k = 0; k < contractRateId.length; k++) {
                    a = k + 1;
                    if (vehicleRatePerKm[k].value == '') {
                        alert("please enter the reefer per km for row " + a);
                        vehicleRatePerKm[k].focus();
                        return;
                    } else if (reeferRatePerHour[k].value == '') {
                        alert("please enter the rate per reefer hour fro row " + a);
                        reeferRatePerHour[k].focus();
                        return;
                    }
                }
                document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                document.customerContract.submit();
            } else if (validate == 'true') {
                document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                document.customerContract.submit();
            }
        } else {
            if (document.getElementById('billingTypeId').value == "1") {
                var routeContractId = document.getElementsByName('routeContractId');
                var ptpRateWithReefer1 = document.getElementsByName('ptpRateWithReefer1');
                var ptpRateWithoutReefer1 = document.getElementsByName('ptpRateWithoutReefer1');
                for (var k = 0; k < routeContractId.length; k++) {
                    a = k + 1;
                    //if (ptpRateWithReefer1[k].value == '') {
                    //   alert("please enter the reefer with reefer amount for row " + a);
                    // ptpRateWithReefer1[k].focus();
                    //return;
//                    } else if (ptpRateWithoutReefer1[k].value == '') {
                    //                      alert("please enter the reefer without reefer amount for row " + a);
                    //                    ptpRateWithoutReefer1[k].focus();
                    //                  return;
                    //            }
                }
                if (ptpRouteContractCode.length > 0 && addMoreRouteValue == 1) {
                    validate = validatePtpContract(a);
                } else {
                    validate = 'true';
                }
                if (validate == 'true') {
                    contractRouteCheck('');
                    document.customerContract.action = "/throttle/saveEditCustomerContract.do";
                    document.customerContract.submit();
                }
            }
        }
    }

    function validatePtpContract(a) {
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');

        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');

        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');

        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');

        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');

        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');

        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');

        //Rate Details
        var ptpRateWithReefer = document.getElementsByName('ptpRateWithReefer');
        var ptpRateWithoutReefer = document.getElementsByName('ptpRateWithoutReefer');

        for (var i = 0; i < ptpRouteContractCode.length; i++) {
            var j = i + 1 + parseInt(a);
            if (ptpRouteContractCode[i].value == '') {
                alert("Please Enter The Valid Route Code for row " + j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpVehicleTypeId[i].value == '0') {
                alert("Please Select The vehicle Type for row " + j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value == '') {
                alert("please select the pickup point for row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == '') {
                alert("please select the valid point name for pickup point at row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            } else if (interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select the valid point name for interim point 1 at row " + j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 2 at row " + j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 3 at row " + j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 4 at row " + j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for Drop point at row " + j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpTotalKm[i].value == '') {
                alert("please validate the route points at row " + j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpRateWithReefer[i].value == '') {
                alert("Please enter the freight rate with reefer at row " + j);
                ptpRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpRateWithoutReefer[i].value == '') {
                alert("please enter the freight rate without reefer at row " + j);
                ptpRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
//            } else if (ptpRateWithReefer[i].value == 0 || ptpRateWithReefer[i].value == '0.00') {
//                alert("the freight rate with reefer cannot be 0 at row " + j);
//                ptpRateWithReefer[i].focus();
//                returnvalue = 'false';
//                return returnvalue;
//            } else if (ptpRateWithoutReefer[i].value == 0 || ptpRateWithoutReefer[i].value == '0.00') {
//                alert(" the freight rate without reefer cannot be 0 at row " + j);
//                ptpRateWithoutReefer[i].focus();
//                returnvalue = 'false';
//                return returnvalue;
            } else {
                returnvalue = 'true';

            }
        }
        return returnvalue;
    }

    function validateFixedContract(a) {
        var returnvalue = "";
        var ptpRouteContractCode = document.getElementsByName('ptpRouteContractCode');
        var ptpVehicleTypeId = document.getElementsByName('ptpVehicleTypeId');
        //Point Name
        var ptpPickupPoint = document.getElementsByName('ptpPickupPoint');
        var interimPoint1 = document.getElementsByName('interimPoint1');
        var interimPoint2 = document.getElementsByName('interimPoint2');
        var interimPoint4 = document.getElementsByName('interimPoint4');
        var interimPoint3 = document.getElementsByName('interimPoint3');
        var ptpDropPoint = document.getElementsByName('ptpDropPoint');

        //Point Id
        var ptpPickupPointId = document.getElementsByName('ptpPickupPointId');
        var interimPointId1 = document.getElementsByName('interimPointId1');
        var interimPointId2 = document.getElementsByName('interimPointId2');
        var interimPointId3 = document.getElementsByName('interimPointId3');
        var interimPointId4 = document.getElementsByName('interimPointId4');
        var ptpDropPointId = document.getElementsByName('ptpDropPointId');

        //Point Km
        var interimPoint1Km = document.getElementsByName('interimPoint1Km');
        var interimPoint2Km = document.getElementsByName('interimPoint2Km');
        var interimPoint3Km = document.getElementsByName('interimPoint3Km');
        var interimPoint4Km = document.getElementsByName('interimPoint4Km');
        var ptpDropPointKm = document.getElementsByName('ptpDropPointKm');

        //Point Hours
        var interimPoint1Hrs = document.getElementsByName('interimPoint1Hrs');
        var interimPoint2Hrs = document.getElementsByName('interimPoint2Hrs');
        var interimPoint3Hrs = document.getElementsByName('interimPoint3Hrs');
        var interimPoint4Hrs = document.getElementsByName('interimPoint4Hrs');
        var ptpDropPointHrs = document.getElementsByName('ptpDropPointHrs');

        //Point Minutes
        var interimPoint1Minutes = document.getElementsByName('interimPoint1Minutes');
        var interimPoint2Minutes = document.getElementsByName('interimPoint2Minutes');
        var interimPoint3Minutes = document.getElementsByName('interimPoint3Minutes');
        var interimPoint4Minutes = document.getElementsByName('interimPoint4Minutes');
        var ptpDropPointMinutes = document.getElementsByName('ptpDropPointMinutes');

        //Point Route Id
        var interimPoint1RouteId = document.getElementsByName('interimPoint1RouteId');
        var interimPoint2RouteId = document.getElementsByName('interimPoint2RouteId');
        var interimPoint3RouteId = document.getElementsByName('interimPoint3RouteId');
        var interimPoint4RouteId = document.getElementsByName('interimPoint4RouteId');
        var ptpDropPointRouteId = document.getElementsByName('ptpDropPointRouteId');

        //Total Km, Total Hours, Total Minutes
        var ptpTotalKm = document.getElementsByName('ptpTotalKm');
        var ptpTotalHours = document.getElementsByName('ptpTotalHours');
        var ptpTotalMinutes = document.getElementsByName('ptpTotalMinutes');

        //Rate Details

        for (var i = 0; i < ptpRouteContractCode.length; i++) {
            var j = i + 1 + parseInt(a);
            if (ptpRouteContractCode[i].value == '') {
                alert("Please Enter The Valid Route Code for row " + j);
                ptpRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpVehicleTypeId[i].value == '0') {
                alert("Please Select The vehicle Type for row " + j);
                ptpVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value == '') {
                alert("please select the pickup point for row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value == '') {
                alert("please select the valid point name for pickup point at row " + j);
                ptpPickupPoint[i].focus();
                returnvalue = 'false';
                ptpPickupPoint[i].value = '';
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                return returnvalue;
            } else if (interimPoint1[i].value != '' && interimPointId1[i].value == '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select the valid point name for interim point 1 at row " + j);
                interimPoint1[i].focus();
                interimPoint1[i].value = '';
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint2[i].value != '' && interimPointId2[i].value == '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 2 at row " + j);
                interimPoint2[i].focus();
                interimPoint2[i].value = '';
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint3[i].value != '' && interimPointId3[i].value == '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 3 at row " + j);
                interimPoint3[i].focus();
                interimPoint3[i].value = '';
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (interimPoint4[i].value != '' && interimPointId4[i].value == '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for interim point 4 at row " + j);
                interimPoint4[i].focus();
                interimPoint4[i].value = '';
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpDropPoint[i].value != '' && ptpDropPointId[i].value == '' && interimPoint4[i].value != '' && interimPointId4[i].value != '' && interimPoint3[i].value != '' && interimPointId3[i].value != '' && interimPoint2[i].value != '' && interimPointId2[i].value != '' && interimPoint1[i].value != '' && interimPointId1[i].value != '' && ptpPickupPoint[i].value != '' && ptpPickupPointId[i].value != '') {
                alert("please select valid point name for Drop point at row " + j);
                ptpDropPoint[i].focus();
                ptpDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpTotalKm[i].value == '') {
                alert("please validate the route points at row " + j);
                ptpDropPoint[i].value = '';
                ptpDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else {
                returnvalue = 'true';

            }
        }
        return returnvalue;
    }

    function validatePtpwContract(a) {
        var returnvalue = "";
        var ptpwRouteContractCode = document.getElementsByName('ptpwRouteContractCode');
        var ptpwVehicleTypeId = document.getElementsByName('ptpwVehicleTypeId');
        var ptpwPickupPoint = document.getElementsByName('ptpwPickupPoint');
        var ptpwPickupPointId = document.getElementsByName('ptpwPickupPointId');
        var ptpwDropPoint = document.getElementsByName('ptpwDropPoint');
        var ptpwDropPointId = document.getElementsByName('ptpwDropPointId');
        var ptpwTotalKm = document.getElementsByName('ptpwTotalKm');
        var ptpwRateWithReefer = document.getElementsByName('ptpwRateWithReefer');
        var ptpwRateWithoutReefer = document.getElementsByName('ptpwRateWithoutReefer');
        //alert(ptpwRouteContractCode.length);
        for (var i = 0; i < ptpwRouteContractCode.length; i++) {
            //alert("i=="+i)
            var j = i + 1 + parseInt(a);
            if (ptpwRouteContractCode[i].value == '') {
                alert("Please Enter Contract Route Code For Row " + j);
                ptpwRouteContractCode[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwVehicleTypeId[i].value == '0') {
                alert("Please Select Vehicle Type For Row " + j);
                ptpwVehicleTypeId[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwPickupPoint[i].value == '') {
                alert("Please Select The Pickup Point For Row " + j);
                ptpwPickupPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwPickupPoint[i].value != '' && ptpwPickupPointId[i].value == '') {
                alert("Please Select The Pickup Point For Row " + j);
                ptpwPickupPoint[i].focus();
                ptpwPickupPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwDropPointId[i].value == '') {
                alert("Please Select The Drop Point For Row " + j);
                ptpwDropPoint[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwDropPoint[i].value != '' && ptpwDropPointId[i] == '') {
                alert("Please Select The Drop Point For Row " + j);
                ptpwDropPoint[i].focus();
                ptpwDropPoint[i].value = '';
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwTotalKm[i].value == '') {
                alert("Validate The Route Point Details and check Total Km For Row " + j);
                ptpwTotalKm[i].focus();
                c
            } else if (ptpwRateWithReefer[i].value == '') {
                alert("Please enter the amount for rate with reefer for row " + j);
                ptpwRateWithReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else if (ptpwRateWithoutReefer[i].value == '') {
                alert("Please enter the amount for rate without reefer for row " + j);
                ptpwRateWithoutReefer[i].focus();
                returnvalue = 'false';
                return returnvalue;
            } else {
                returnvalue = 'true';
            }
        }
        return returnvalue;
    }
    //        function customerValidation(){
    //        var custContactPerson = document.getElementsByName("custContactPerson");
    //        var custPhone =  document.getElementsByName("custPhone");
    //        var custMobile =  document.getElementsByName("custMobile");
    //        var custEmail = document.getElementsByName("custEmail");
    //        var accountManagerId = document.getElementsByName("accountManagerId");
    //        var accountManager = document.getElementsByName("accountManager");
    //        var creditDays = document.getElementsByName("creditDays");
    //        var creditLimit = document.getElementsByName("creditLimit");
    //        var a = 0;
    //        var validate='true';
    //
    //            if (document.getElementById('custContactPerson').value == '') {
    //                alert("please enter the contact person");
    //                $("#custContactPerson").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('custPhone').value == '') {
    //                alert("please enter the phone no");
    //                $("#custPhone").focus();
    //                validate='false';
    //                return validate;
    //            }else if(document.getElementById('custMobile').value == ''){
    //                alert("please enter the mobile no");
    //                $("#custMobile").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('custEmail').value == '') {
    //                alert("please enter the email");
    //                $("#custEmail").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
    //                alert("please select the valid accountMcanager name");
    //                $("#accountMcanager").focus();
    //                validate=1;
    //                return validate;
    //            }else if (document.getElementById('creditLimit').value == '') {
    //                alert("please enter the credit limit");
    //                $("#creditLimit").focus();
    //                validate='false';
    //                return validate;
    //            }else if (document.getElementById('creditDays').value == '') {
    //                alert("please enter the creditDays");
    //                $("#creditDays").focus();
    //                validate='false';
    //                return validate;
    //            }
    //        return validate;
    //    }
    function customerValidation()
    {
        //    var vehMileage = document.getElementsByName("vehMileage");
        //        var reefMileage =  document.getElementsByName("reefMileage");
        //        var fuelCostPerKms =  document.getElementsByName("fuelCostPerKms");
        //        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        //        var tollAmounts = document.getElementsByName("tollAmounts");
        //        var miscCostKm = document.getElementsByName("miscCostKm");
        //        var driverIncenKm = document.getElementsByName("driverIncenKm");
        //        var factor = document.getElementsByName("factor");
        var returnvalue = "";
        if (document.getElementById('custContactPerson').value == '') {
            alert("please enter the contact person")
            $("#custContactPerson").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custPhone').value == '') {
            alert("please enter the phone no")
            $("#custPhone").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custMobile').value == '') {
            alert("please enter the mobile no")
            $("#custMobile").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('custEmail').value == '') {
            alert("please enter the email")
            $("#custEmail").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('accountManagerId').value == '' && document.getElementById('accountManager').value != '') {
            alert("please select the valid accountant name")
            $("#accountManager").focus();
            returnvalue = 'false';
            return returnvalue;
        } else if (document.getElementById('creditLimit').value == '') {
            alert("please enter the credit limit")
            $("#creditLimit").focus();
            returnvalue = 'false';
            return returnvalue;
        } else {
            returnvalue = 'true';
        }
    }

    function  validateFixedKmRateDetails() {
        var sts = true;
        var fixedKmvehicleTypeId = document.getElementsByName("fixedKmvehicleTypeId");
        var vehicleNos = document.getElementsByName("vehicleNos");
        var fixedTotalKm = document.getElementsByName("fixedTotalKm");
        var fixedTotalHm = document.getElementsByName("fixedTotalHm");
        var fixedRateWithReefer = document.getElementsByName("fixedRateWithReefer");
        var extraKmRateWithReefer = document.getElementsByName("extraKmRateWithReefer");
        var extraHmRateWithReefer = document.getElementsByName("extraHmRateWithReefer");
        var fixedRateWithoutReefer = document.getElementsByName("fixedRateWithoutReefer");
        var extraKmRateWithoutReefer = document.getElementsByName("extraKmRateWithoutReefer");
        for (var i = 0; i < fixedKmvehicleTypeId.length; i++) {
            if (fixedKmvehicleTypeId[i].value == '0') {
                alert("Please select vehicle type");
                fixedKmvehicleTypeId[i].focus();
                sts = false;
            } else if (vehicleNos[i].value == '') {
                alert("Please enter vehicle nos");
                vehicleNos[i].focus();
                sts = false;
            } else if (fixedTotalKm[i].value == '') {
                alert("Please enter total km");
                fixedTotalKm[i].focus();
                sts = false;
            } else if (fixedTotalHm[i].value == '') {
                alert("Please enter total hm");
                fixedTotalHm[i].focus();
                sts = false;
            } else if (fixedRateWithReefer[i].value == '') {
                alert("Please enter rate with reefer");
                fixedRateWithReefer[i].focus();
                sts = false;
            } else if (extraKmRateWithReefer[i].value == '') {
                alert("Please enter extra km rate with reefer");
                extraKmRateWithReefer[i].focus();
                sts = false;
            } else if (extraHmRateWithReefer[i].value == '') {
                alert("Please enter extra hm rate with reefer");
                extraHmRateWithReefer[i].focus();
                sts = false;
            } else if (fixedRateWithoutReefer[i].value == '') {
                alert("Please enter rate without reefer");
                fixedRateWithoutReefer[i].focus();
                sts = false;
            } else if (extraKmRateWithoutReefer[i].value == '') {
                alert("Please enter extra km rate without reefer");
                extraKmRateWithoutReefer[i].focus();
                sts = false;
            } else {
                sts = true;
            }
        }
        return sts;
    }


    function Savepencharge() {

        //        if (document.getElementById('penality').value == "") {
        //            alert("Please enter Penality cause");
        //            document.getElementById('penality').focus();
        //        } else if (document.getElementById('chargeamount').value == "") {
        //            alert("Please enter Penality charges");
        //            document.getElementById('chargeamount').focus();
        //        } else if (document.getElementById('pcmunit').value == "") {
        //            alert("Please enter Unit");
        //            document.getElementById('pcmunit').focus();
        //        } else if (document.getElementById('pcmremarks').value == "") {
        //            alert("Please enter Remarks");
        //            document.getElementById('pcmremarks').focus();
        //        }
        document.customerContract.action = "/throttle/updatepenalitycharges.do";
        document.customerContract.submit();

    }

    function Savedencharge() {

        //        if (document.getElementById('detention').value == "") {
        //            alert("Please enter detention reason");
        //            document.getElementById('detention').focus();
        //        } else if (document.getElementById('chargeamt').value == "") {
        //            alert("Please enter detention charges");
        //            document.getElementById('chargeamt').focus();
        //        } else if (document.getElementById('dcmunit').value == "") {
        //            alert("Please enter unit");
        //            document.getElementById('dcmunit').focus();
        //        } else if (document.getElementById('dcmremarks').value == "") {
        //            alert("Please enter Remarks");
        //            document.getElementById('dcmremarks').focus();
        //        }

        document.customerContract.action = "/throttle/updatedetentioncharges.do";
        document.customerContract.submit();

    }

</script>


<script type="text/javascript">
    function checked() {
        if (document.getElementById("editcustomer").checked == true) {
            $("#displayCustomerDetails").show();
        } else {
            $("#displayCustomerDetails").hide();
        }
    }

</script>
<div class="pageheader">
    <c:if test="${customerId != '0'}">
        <h2><i class="fa fa-edit"></i> Customer</h2>
    </c:if>
    <c:if test="${customerId == '0'}">
        <h2><i class="fa fa-edit"></i> Company</h2>
    </c:if>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><spring:message code="general.label.home"  text="Home"/></li>
                <c:if test="${customerId == '0'}">
                <li>Company</li>
                </c:if>
                <c:if test="${customerId != '0'}">
                <li>Customer</li>
                </c:if>
            <li data-toggle="tab"class="active">Edit Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setReadOnly();">
                <form name="customerContract"  method="post" onsubmit="customerValidation();">


                    <br>
                    <input type="hidden" name="custId" id="custId" value="<c:out value="${custId}"/>"/>
                    <input type="hidden" name="companyId" id="companyId" value="<c:out value="${companyId}"/>"/>
                    <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>"/>
                    <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}"/>"/>
                    <input type="hidden" name="billingTypeId" Id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>

                    <table class="table table-info mb30 table-hover"  style="width:60%;">
                        <c:if test="${customerId != '0'}">
                            <tr id="tableDesingTD">
                                <td colspan="4" >Customer Contract Details</td>
                            </tr>

                            <tr>
                                <td width="25%">Customer Name</td>
                                <td width="25%">
                                    <c:out value="${customerName}"/>
                                </td>
                                <td width="25%">Customer Code</td>
                                <td width="25%">
                                    <c:out value="${customerCode}"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Contract From</td>
                                <td><c:out value="${contractFrom}"/></td>
                                <td>Contract To</td>
                                <td>
                                    <input type="hidden" class="form-control datepicker" name="contractToOld" id="contractToOld" value="<c:out value="${contractTo}"/>"/>
                                    <input type="hidden"  id="customerName" name="customerName" value="<c:out value="${customerName}"/>"/>
                                    <input type="text" class="datepicker  form-control" name="contractTo" id="contractTo" value="<c:out value="${contractTo}"/>"  />
                                </td>
                            </tr>
                            <tr>
                                <td>Billing Type</td>
                                <td>
                                    <c:out value="${billingTypeName}"/>
                                </td>


                                <td>Contract No</td>
                                <td>
                                    <c:out value="${contractNo}"/>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>ERP ID</td>
                                <td>
                                    <c:out value="${contractNo}"/>
                                </td>
                            </tr>


                        </c:if>
                        <c:if test="${customerId == '0'}">
                            <tr id="tableDesingTD">
                                <td colspan="4" >Company Contract Details</td>
                            </tr>
                            <tr>
                                <td width="25%">Company Name</td>
                                <td width="25%">
                                    <c:out value="${companyName}"/>
                                </td>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                        </c:if>
                    </table>
                    <br>
                    <%@include file="/content/common/message.jsp" %>
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <!--                    <li><a href="#standard"><span>Standard Charges</span></a></li>-->
                            <c:if test="${billingTypeId == 1}">
                                <li class="active" data-toggle="tab" id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 1}">
                                <li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                                </c:if>
                                <c:if test="${ billingTypeId == 1}">
                                <li data-toggle="tab" id="dc" ><a href="#dcm"><span> Detentions charges </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 2}">
                                <li data-toggle="tab" id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 3}">
                                <li data-toggle="tab" id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 4}">
                                <li data-toggle="tab"><a href="#fkmRate"><span>Fixed KM - Based - Rate Details</span></a></li>
                                <li data-toggle="tab"id="fkmRouteDetails" style="display: none"><a href="#fkmRoute"><span>Fixed KM - Based - Route Details </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 5}">
                                <li data-toggle="tab"><a href="#distanceRate"><span>Distance - Based - Rate Details</span></a></li>

                            </c:if>
                        </ul>

                        <!--
                                    //Show existing contracts
                                    //user can change rates
                                    //user can make active to incactive
                                    //show only active routes
                        -->
                        <c:if test="${billingTypeId == 5}">

                            <script>
                                var count = 1;
                                $(document).ready(function() {
                                    // Get the table object to use for adding a row at the end of the table
                                    var $itemsTable = $('#distanceTable');

                                    // Create an Array to for the table row. ** Just to make things a bit easier to read.
                                    var rowTemp = "";
                                <c:if test="${display == 'none'}">
                                    rowTemp = [
                                        '<tr class="item-row">',
                                        '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                                        '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
                                        '<td><c:if test="${vehicleTypeList != null}"><select name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                                        '<td><select name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px" ></td>',
                                        '<td><select name="containerQty" id="containerQty"  class="form-control" style="width:150px" ></td>',
                                        '<td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></td>',
                                        '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;"  /></td>',
                                        '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"  /></td>',
                                        '<td><input name="rateWithReeferDistance" value="" class="form-control" id="rateWithReeferDistance"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                                        '<td><input name="rateWithoutReeferDistance" value="" class="form-control" id="rateWithoutReeferDistance"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelVehicle" value="" class="form-control" id="fuelVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                                        '</tr>'
                                    ].join('');
                                </c:if>
                                <c:if test="${display == 'block'}">
                                    rowTemp = [
                                        '<tr class="item-row">',
                                        '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                                        '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
                                        '<td><c:if test="${vehicleTypeList != null}"><select name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                                        '<td><select name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px" ></td>',
                                        '<td><select name="containerQty" id="containerQty"  class="form-control" style="width:150px" ></td>',
                                        '<td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></td>',
                                        '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;" /></td>',
                                        '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"/></td>',
                                        '<td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                                        '<td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                                        '</tr>'
                                    ].join('');
                                </c:if>

                                    // Add row to list and allow user to use autocomplete to find items.



                                    $("#addRowDistance").bind('click', function() {

                                        //  count++;
                                        alert("hiiii");

                                        if (document.getElementById("distanceTable").style.visibility == "hidden") {
                                            document.getElementById("distanceTable").style.visibility = "visible";

                                        } else {
                                            var $row = $(rowTemp);

                                            // save reference to inputs within row

                                            var $referenceName = $row.find('#referenceName');
                                            var $ptpVehicleTypeId = $row.find('#distnaceVehicleTypeId');

                                            var $containerTypeId = $row.find('#containerTypeId');
                                            var $containerQty = $row.find('#containerQty');
                                            var $ptpPickupPoint = $row.find('#fromDistance');


                                            var $ptpDropPoint = $row.find('#toDistance');

                                            var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                                            var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
                                            $row.find('#distnaceVehicleTypeId').change(function() {
                                                //                        var $itemrow = $(this).closest('tr');
                                                var val = $row.find('#distnaceVehicleTypeId').val();
                                                if (val == '1058') {
                                                    // 20FT vehicle
                                                    $row.find('#containerTypeId').empty();
                                                    $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                                    $row.find('#containerQty').empty();
                                                    $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))
                                                } else if (val == '1059') {
                                                    // 40FT vehicle
                                                    $row.find('#containerTypeId').empty();
                                                    $row.find('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                                                    $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                                    $row.find('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                                                    $row.find('#containerQty').empty();

                                                }
                                            });
                                            $row.find('#containerTypeId').change(function() {
                                                //                        var $itemrow = $(this).closest('tr');
                                                var val = $row.find('#containerTypeId').val();
                                                if (val == '1') {
                                                    // 20FT Container
                                                    $row.find('#containerQty').empty();
                                                    $row.find('#containerQty').append($('<option ></option>').val(2).html('2'))
                                                } else if (val == '2') {
                                                    // 40FT Container
                                                    $row.find('#containerQty').empty();
                                                    $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))

                                                }
                                            });
                                            $('.item-row:last', $itemsTable).after($row);
                                            //                if ($('#ptpRouteContractCode:last').val() !== '') {}else{ // End if last itemCode input is empty
                                            //                    alert("please fill route code");
                                            //                }
                                            return false;
                                        }
                                    });


                                });// End DOM

                                function setContainerTypeList(val) {
                                    if (val == '1058') {
                                        // 20FT vehicle
                                        $('#containerTypeId').empty();
                                        $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                        $('#containerQty').empty();
                                        $('#containerQty').append($('<option ></option>').val(1).html('1'))
                                    } else if (val == '1059') {
                                        // 40FT vehicle
                                        $('#containerTypeId').empty();
                                        $('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                                        $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                        $('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                                        $('#containerQty').empty();

                                    }
                                    contractRouteCheck('');
                                }
                                function setContainerQtyList(val) {
                                    if (val == '1') {
                                        // 20FT vehicle
                                        $('#containerQty').empty();
                                        $('#containerQty').append($('<option ></option>').val(2).html('2'))
                                    } else if (val == '2') {
                                        // 40FT vehicle
                                        $('#containerQty').empty();
                                        $('#containerQty').append($('<option ></option>').val(1).html('1'))

                                    }
                                    contractRouteCheck('');
                                }

                                function submitDistancePage(value) {
                                    var validate = "";
                                    //        if (document.getElementById('distnaceVehicleTypeId').value == "") {
                                    //            alert("Please Select vehicle Type");
                                    //            document.getElementById('distnaceVehicleTypeId').focus();
                                    //        } else if (document.getElementById('containerTypeId').value == "") {
                                    //            alert("Please Select container Type");
                                    //            document.getElementById('containerTypeId').focus();
                                    //        } else if (document.getElementById('fromDistance').value == "") {
                                    //            alert("Please Enter from distance");
                                    //            document.getElementById('fromDistance').focus();
                                    //        } else if (document.getElementById('toDistance').value == "") {
                                    //           alert("Please Enter from distance");
                                    //            document.getElementById('fromDistance').focus();
                                    //        } else {
                                    document.customerContract.action = "/throttle/saveCustomerDistanceContract.do";
                                    document.customerContract.submit();
                                    //   }

                                }
                            </script>
                            <script>
                                function autoCheckBoxDeten(sno) {
//                                        alert(sno)
                                    document.getElementById("editCheck" + sno).checked = true;
                                    document.getElementById("detentionEditFlag" + sno).value = '1';
                                }

                            </script>

                            </script>


                            <div id=ptp class="tab-pane active" style="padding-left:1px;overflow: auto" >
                                <div class="inpad" style="padding-left:1px;">
                                    <c:if test = "${contractList != null}" >
                                        <table class="table table-info mb30 table-hover" width="98%" >
                                            <thead>
                                                <tr>
                                                    <th>Sno</th>

                                                    <th  ><font color="red"></font>Reference name</th>


                                                    <th  ><font color="red"></font>Vehicle Type</th>
                                                    <th  ><font color="red"></font>Container Type</th>
                                                    <th  ><font color="red"></font>Container Qty</th>
                                                    <th  ><font color="red"></font>Load Type</th>
                                                    <th ><font color="red"></font>From Distance</th>

                                                    <th  ><font color="red"></font>To Distance</th>

                                                    <th  ><font color="red">*</font>Rate With Reefer</th>
                                                    <th   ><font color="red">*</font>Rate Without Reefer</th>
                                                    <th   ><font color="red">*</font>status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                       int sno = 0;
                                                %>
                                                <c:forEach items="${contractList}" var="cList">
                                                    <%sno++;%>
                                                    <tr>
                                                        <td><%=sno%><input type="hidden" id="distanceContractId<%=sno%>" name="distanceContractId" value="<c:out value="${cList.distanceContractId}"/>"/></td>
                                                        <td><c:out value="${cList.referenceName}"/></td>
                                                        <td><c:out value="${cList.vehicleType}"/></td>
                                                        <td><c:out value="${cList.containerTypeName}"/></td>
                                                        <td><c:out value="${cList.containerQty}"/></td>
                                                        <td><c:out value="${cList.loadType}"/></td>
                                                        <td><c:out value="${cList.fromDistance}"/></td>
                                                        <td><c:out value="${cList.toDistance}"/></td>
                                                        <td>  <input type="text" name="rateWithReefer1" id="rateWithReefer1<%=sno%>" class="form-control"  value="<c:out value="${cList.rateWithReefer}"/>" style="width:120px;"/>
                                                        </td>
                                                        <td>  <input type="text" name="rateWithoutReefer1" id="rateWithoutReefer1<%=sno%>" class="form-control"  value="<c:out value="${cList.rateWithoutReefer}"/>" style="width:120px;"/>
                                                        </td>

                                                        <td>   <input type="hidden" name="approvalStatus1" id="approvalStatus1<%=sno%>" value="<c:out value="${cList.activeStatus}"/>"/>
                                                            <select name="validStatus" id="validStatus<%=sno%>" class="form-control" style="width:90px;">
                                                                <option value="Y" selected >Active</option>
                                                                <option value="N"  >In Active</option>
                                                            </select></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:if>

                                    <br>
                                    <br>

                                    <table class="table table-info mb30 table-hover sortable" width="98%" id="distanceTable" style="visibility:hidden;">
                                        <thead>
                                            <tr height="40" >
                                                <th>Sno</th>

                                                <th  height="80" style="width: 10px;"><font color="red">*</font>Reference name</th>


                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Container Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Container Qty</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Load Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>From Distance</th>

                                                <th  height="30" ><font color="red">*</font>To Distance</th>

                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>


                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="item-row">
                                                <td></td>




                                                <td><input type="text" name="referenceName" id="referenceName" value="" class="form-control"  style="width:150px;height:44px;"/></td>


                                                <td><c:if test="${vehicleTypeList != null}"><select onchange='setContainerTypeList(this.value)' name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>

                                                        <td><select onchange='setContainerQtyList(this.value)' name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px;height:44px;"></select></td>
                                                        <td><select name="containerQty" id="containerQty" onblur="contractRouteCheck('');" class="form-control" style="width:150px;height:44px;"></select></td>
                                                        <td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></select></td>
                                                        <td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width:150px;height:44px;"  /></td>

                                                        <td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width:150px;height:44px;" /></td>

                                                        <td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="contractRouteCheck();"/></td>
                                                        <td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="contractRouteCheck();" /></td>

                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" id="addRowDistance" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /><b> Add Route Code</b></span></a>
                                            <br>
                                            <br>
                                            <center>
                                                <input type="button" class="btn btn-success" id="saveButton" value="Save" style="width: 120px" onclick="submitDistancePage(this.value)"/>
                                            </center>
                                            <br>
                                            <br>
                                        </div></div>
                            </c:if>

                        <c:if test="${billingTypeId == 1}">
                            <!--                        point to point-->
                            <div id="ptp" class="tab-pane active" style="padding-left:1px;overflow: auto" >
                                <div class="inpad" style="padding-left:1px;">
                                    <c:if test = "${ptpBillingList != null}" >

                                        <table class="table table-info mb30 table-hover"  id="table" style="width:98%;">
                                            <thead>
                                                <tr >
                                                    <th   >Sno</th>
                                                    <th>Update</th>
                                                        <c:if test="${customerId == '0'}">
                                                        <th   >Configure Fuel</th>
                                                        </c:if>

                                                    <c:if test="${customerId != '0'}">
                                                        <th   >Approval Required?</th>
                                                        <th   >Approval Status </th>
                                                        <th   >OrgRateWithoutRefer</th>
                                                        <th   >OrgRateWithRefer</th>

                                                        <th   >From Date</th>
                                                        <th   >To Date</th>
                                                        </c:if>
                                                    <th   >VehicleType</th>
                                                    <th   >Load Type</th>
                                                    <th   >Container Type</th>
                                                    <th   >Container Qty</th>
                                                    <th   >Origin</th>
                                                    <th   >Interim Point1 </th>
                                                    <th   >Interim Point2 </th>
                                                    <th   >Interim Point3 </th>
                                                    <th   >Interim Point4 </th>
                                                    <th   >Destination </th>
                                                    <th   >Rate With Reefer </th>
                                                    <th   >Rate WithOut Reefer </th>
                                                        <c:if test="${customerId == '0'}">
                                                        <th   >Toll</th>
                                                        <th   >Driver Bachat</th>
                                                        <th   >Dala</th>
                                                        <th  >Misc</th>
                                                        <th  >Market HireMarket Hire Without Reefer
                                                        <th  >Market HireMarket Hire With Reefer
                                                        </c:if>
                                                    <th   >Status </th>

                                                </tr>
                                            </thead>
                                            <%  int index = 0;
                                                   int sno = 0;
                                            %>
                                            <tbody>
                                                <c:forEach items="${ptpBillingList}" var="contractList">
                                                    <%sno++;%>
                                                    <tr>
                                                        <td align="left" ><%=sno%>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="editStatus" id="editStatus<%=sno%>" value="1" onclick="setActiveInd('<%=sno%>')"/>
                                                            <input type="hidden" id="activeInd<%=sno%>" name="activeInd" value="N"/>
                                                        </td>
                                                        <c:if test="${customerId == '0'}">
                                                            <td>
                                                                <input type="hidden" value="<c:out value="${contractList.fromDate}"/>" name="fromDate"/>
                                                                <input type="hidden" value="<c:out value="${contractList.toDate}"/>" name="toDate"/>
                                                                <a href="#" name="configure" id="configure" data-toggle="modal" data-target="#myModal"  onclick="configModelFuel('<c:out value="${contractList.vehicleTypeId}"/>', '<c:out value="${contractList.contractRateId}"/>', 1)">CONFIGURE</a> </td>

                                                        </c:if>


                                                        <c:if test="${customerId != '0'}">
                                                            <td><span id="orgValuesE<%=sno%>" style="font-size: 14px"></span></td>
                                                                <c:if test="${contractList.approvalStatus == '2' || contractList.approvalStatus == null}">
                                                                <td   align="left" >
                                                                    <span class="label label-warning">pending</span></td>
                                                                </c:if>
                                                                <c:if test="${contractList.approvalStatus =='1'}">
                                                                <td   align="left" > <span class="label label-success">approved</span></td>
                                                            </c:if>
                                                            <c:if test="${contractList.approvalStatus =='3'}">
                                                                <td   align="left" ><span class="label label-danger">rejected</span></td>
                                                            </c:if>
                                                            <td style="padding-right:12px;"> <input type="hidden" value="<c:out value="${contractList.orgWithoutReeferRate}"/>" id="orgWithoutReeferRate<%=sno%>" name="orgWithoutReeferRate"/><c:out value="${contractList.orgWithoutReeferRate}"/> </td>
                                                            <td style="padding-right:12px;"> <input type="hidden" value="<c:out value="${contractList.orgWithReeferRate}"/>" id="orgWithReeferRate<%=sno%>" name="orgWithReeferRate"/><c:out value="${contractList.orgWithReeferRate}"/> </td>
                                                            <td style="padding-right:12px;"> <input type="hidden" value="<c:out value="${contractList.fromDate}"/>" name="fromDate"/><c:out value="${contractList.fromDate}"/> </td>
                                                            <td style="padding-right:12px;"><input type="hidden" value="<c:out value="${contractList.toDate}"/>" name="toDate"/> <c:out value="${contractList.toDate}"/></td>
                                                            </c:if>

                                                        <td align="left" >
                                                            <input type="hidden" name="vehicleTypeName1" id="vehicleTypeName1<%=sno%>" value="<c:out value="${contractList.vehicleTypeName}"/>"/>
                                                            <input type="hidden" name="firstPickupName1" id="firstPickupName1<%=sno%>" value="<c:out value="${contractList.firstPickupName}"/>"/>
                                                            <input type="hidden" name="point1Name" id="point1Name<%=sno%>" value="<c:out value="${contractList.point1Name}"/>"/>
                                                            <input type="hidden" name="finalPointName1" id="finalPointName1<%=sno%>" value="<c:out value="${contractList.finalPointName}"/>"/>
                                                            <input type="hidden" name="validStatusOld" id="validStatusOld<%=sno%>" value="<c:out value="${contractList.activeInd}"/>"/>
                                                            <input type="hidden" name="routeContractId" id="routeContractId<%=sno%>" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                            <input type="hidden" name="contractRateId" id="contractRateId<%=sno%>" value="<c:out value="${contractList.contractRateId}"/>"/>

                                                            <input type="hidden" name="vehicleTypeId1" id="vehicleTypeId1<%=sno%>" value="<c:out value="${contractList.vehicleTypeId}"/>"/>
                                                            <input type="hidden" name="firstPickupId1" id="firstPickupId1<%=sno%>" value="<c:out value="${contractList.firstPickupId}"/>"/>
                                                            <input type="hidden" name="point1Id1" id="point1Id1<%=sno%>" value="<c:out value="${contractList.point1Id}"/>"/>
                                                            <input type="hidden" name="point2Id1" id="point2Id1<%=sno%>" value="<c:out value="${contractList.point2Id}"/>"/>
                                                            <input type="hidden" name="point3Id1" id="point3Id1<%=sno%>" value="<c:out value="${contractList.point3Id}"/>"/>
                                                            <input type="hidden" name="point4Id1" id="point4Id1<%=sno%>" value="<c:out value="${contractList.point4Id}"/>"/>
                                                            <input type="hidden" name="finalPointId1" id="finalPointId1<%=sno%>" value="<c:out value="${contractList.finalPointId}"/>"/>
                                                            <input type="hidden" name="loadTypeId1" id="loadTypeId1<%=sno%>" value="<c:out value="${contractList.loadTypeId}"/>"/>
                                                            <input type="hidden" name="containerTypeId1" id="containerTypeId1<%=sno%>" value="<c:out value="${contractList.containerTypeId}"/>"/>
                                                            <input type="hidden" name="containerQty11" id="containerQty11<%=sno%>" value="<c:out value="${contractList.containerQty1}"/>"/>
                                                            <input type="hidden" name="vehicleTypeIdEdit" value="<c:out value="${contractList.vehicleTypeId}"/>"/>
                                                            <c:out value="${contractList.vehicleTypeName}"/>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="loadTypeIdEdit" value="<c:out value="${contractList.loadTypeId}"/>"/>
                                                            <c:if test="${contractList.loadTypeId =='1'}">
                                                                Empty Trip
                                                            </c:if>
                                                            <c:if test="${contractList.loadTypeId =='2'}">
                                                                Load Trip
                                                            </c:if>
                                                            <c:if test="${contractList.loadTypeId =='3'}">
                                                                Loose Load
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="containerTypeIdEdit" value="<c:out value="${contractList.containerTypeId}"/>"/>
                                                            <c:forEach items="${containerTypeList}" var="conType">
                                                                <c:if test="${contractList.containerTypeId == conType.containerId}">
                                                        <option value="<c:out value="${conType.containerId}"/>">
                                                            <c:out value="${conType.containerName}"/>
                                                        </option>
                                                    </c:if>
                                                </c:forEach>
                                                </td>
                                                <td>
                                                    <c:if test="${contractList.containerQty1 =='1'}">
                                                        1
                                                    </c:if>
                                                    <c:if test="${contractList.containerQty1 =='2'}">
                                                        2
                                                    </c:if>
                                                </td>
                                                <td align="left"  >
                                                    <input type="hidden" name="firstPointIdEdit" value="<c:out value="${contractList.firstPickupId}"/>"/>
                                                    <c:out value="${contractList.firstPickupName}"/>
                                                </td>
                                                <td align="left"  >
                                                    <input type="hidden" name="point1IdEdit" value="<c:out value="${contractList.point1Id}"/>"/>
                                                    <c:out value="${contractList.point1Name}"/>
                                                </td>
                                                <td align="left"  >
                                                    <input type="hidden" name="point2IdEdit" value="<c:out value="${contractList.point2Id}"/>"/>
                                                    <c:out value="${contractList.point2Name}"/>
                                                </td>
                                                <td align="left"  ><input type="hidden" name="point3IdEdit" value="<c:out value="${contractList.point3Id}"/>"/><c:out value="${contractList.point3Name}"/></td>
                                                <td align="left"  ><input type="hidden" name="point4IdEdit" value="<c:out value="${contractList.point4Id}"/>"/><c:out value="${contractList.point4Name}"/></td>
                                                <td align="left"  ><input type="hidden" name="finalPointIdEdit" value="<c:out value="${contractList.finalPointId}"/>"/><c:out value="${contractList.finalPointName}"/></td>
                                                <td align="left"  >
                                                    <input type="hidden" name="ptpRateWithReefer1Old" id="ptpRateWithReefer1Old<%=sno%>"  value="<c:out value="${contractList.rateWithReefer}"/>"/>
                                                    <input type="text" name="ptpRateWithReefer1" id="ptpRateWithReefer1<%=sno%>" onchange="checkEditOrganizationRate('<%=sno%>')" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithReefer}"/>" onBlur="callCheckBox(<%=sno%>);" style="width:120px;"/>
                                                </td>
                                                <td align="left" >
                                                    <input type="hidden" name="ptpRateWithoutReefer1Old" id="ptpRateWithoutReefer1Old<%=sno%>" value="<c:out value="${contractList.rateWithoutReefer}"/>"/>
                                                    <input type="text" name="ptpRateWithoutReefer1" id="ptpRateWithoutReefer1<%=sno%>" onchange="checkEditOrganizationRate('<%=sno%>')" class="form-control" onBlur="callCheckBox(<%=sno%>)" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithoutReefer}"/>" style="width:120px;"/>
                                                </td>
                                                <c:if test="${customerId == '0'}">
                                                    <td align="left" >
                                                        <input type="hidden" name="fuelVehicleOld" id="fuelVehicleOld<%=sno%>" value="0"/>
                                                        <input type="hidden" name="fuelVehicle1" id="fuelVehicle1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="0" style="width:120px;"/>
                                                        <input type="hidden" name="fuelDgOld" id="fuelDgOld<%=sno%>" value="0"/>
                                                        <input type="hidden" name="fuelDg1" id="fuelDg1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="0" style="width:120px;"/>
                                                        <input type="hidden" name="totalFuelOld" id="totalFuelOld<%=sno%>" value="0"/>
                                                        <input type="hidden" name="totalFuel1" id="totalFuel1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="0" style="width:120px;"/>
                                                        <input type="hidden" name="tollOld" id="tollOld<%=sno%>" value="<c:out value="${contractList.toll}"/>"/>
                                                        <input type="text" name="toll1" id="toll1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.toll}"/>" style="width:120px;" onBlur="callCheckBox(<%=sno%>)"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="driverBachatOld" id="driverBachatOld<%=sno%>" value="<c:out value="${contractList.driverBachat}"/>"/>
                                                        <input type="text" name="driverBachat1" id="driverBachat1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.driverBachat}"/>" style="width:120px;" onBlur="callCheckBox(<%=sno%>)"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="dalaOld" id="dalaOld<%=sno%>" value="<c:out value="${contractList.dala}"/>"/>
                                                        <input type="text" name="dala1" id="dala1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.dala}"/>" style="width:120px;" onBlur="callCheckBox(<%=sno%>)"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="miscOld" id="miscOld<%=sno%>" value="<c:out value="${contractList.misc}"/>"/>
                                                        <input type="text" name="misc1" id="misc1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.misc}"/>" onBlur="callCheckBox(<%=sno%>)" style="width:120px;"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="marketHireOld" id="marketHireOld<%=sno%>" value="<c:out value="${contractList.marketHire}"/>"/>
                                                        <input type="text" name="marketHire1" id="marketHire1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.marketHire}"/>" style="width:120px;" onBlur="callCheckBox(<%=sno%>)"/>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="marketHireWithReeferOld" id="marketHireWithReeferOld<%=sno%>" value="<c:out value="${contractList.marketHireWithReefer}"/>"/>
                                                        <input type="text" name="marketHireWithReefer1" id="marketHireWithReefer1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.marketHireWithReefer}"/>" onBlur="callCheckBox(<%=sno%>)" style="width:120px;"/>
                                                    </td>
                                                </c:if>
                                                <td align="left"  >
                                                    <input type="hidden" name="approvalStatus1" id="approvalStatus1<%=sno%>" value="<c:out value="${contractList.approvalStatus}"/>"/>
                                                    <select name="validStatus" id="validStatus<%=sno%>" class="form-control" style="width:90px;" onBlur="callCheckBox(<%=sno%>)" />
                                                <option value="Y" selected >Active</option>
                                                <option value="N"  >In Active</option>
                                                </select>
                                                </td>

                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:if>
                                    <table border="0"><tr><td>
                                                <c:if test="${billingTypeId != 3 && billingTypeId != 4}">
                                                    <div id="addNewRouteButton" style="display:block" >
                                                        <input type="button" class="btn btn-success" name="showAdd" value="Add More" onclick="showAddOption('<c:out value="${customerId}"/>');" />

                                                    </div>
                                                </c:if>
                                            </td>
                                            </div>
                                            <c:if test="${billingTypeId != 4}">
                                                <td>
                                                    &nbsp;&nbsp;&nbsp;<input type="hidden" class="form-control" value="0"  id="addMoreRouteValue"/>
                                                    <input type="button" class="btn btn-success" value="Save"  onclick="submitPage(this.value)"/>
                                                </td>
                                            </c:if>

                                            <td>
                                                &nbsp;&nbsp;&nbsp;<a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" </a>
                                            </td>
                                            </div>
                                        </tr>
                                    </table>
                                    <br>
                                    <div id="addNewRoute" style="display:none" >
                                        <div class="inpad">
                                            <table align="left" class="table table-info mb30 table-hover sortable" width="90%" id="itemsTable">
                                                <thead>
                                                    <tr height="40" >
                                                        <th>Sno</th>
                                                            <c:if test="${customerId != 0}">
                                                            <th style="width: 10px;"><font color="red">*</font>Approval Required </th>
                                                            <th style="width: 10px;"><font color="red">*</font>Vehicle Route Contract Code</th>
                                                            <th style="width: 10px;"><font color="red">*</font>From Date </th>
                                                            <th style="width: 10px;"><font color="red">*</font>To Date</th>
                                                            </c:if>
                                                        <th style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                                        <th style="width: 10px;"><font color="red">*</font>Load Type</th>
                                                        <th style="width: 10px;"><font color="red">*</font>Container Type</th>
                                                        <th style="width: 10px;"><font color="red">*</font>Container Qty</th>

                                                        <th style="width: 10px;"><font color="red">*</font>First Pick UP</th>
                                                        <th>Interim Point 1</th>
                                                        <th>Interim Point 2</th>
                                                        <th>Interim Point 3</th>
                                                        <th>Interim Point 4</th>
                                                        <th><font color="red">*</font>Final Drop Point</th>
                                                        <th style="width: 90px;" ><font color="red">*</font>Total Km</th>

                                                        <th style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                                        <th style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>

                                                        <c:if test="${customerId == 0}">
                                                            <th style="width: 90px;" ><font color="red">*</font>Toll</th>
                                                            <th style="width: 90px;" ><font color="red">*</font>Driver Bachat</th>
                                                            <th style="width: 90px;" ><font color="red">*</font>Dala</th>
                                                            <th style="width: 90px;" ><font color="red">*</font>Misc</th>
                                                            <th style="width: 90px;" ><font color="red">*</font>MarketHireWithoutReefer</th>
                                                            <th style="width: 90px;" ><font color="red">*</font>MarketHireWithReefer</th>
                                                            </c:if>
                                                            <c:if test="${customerId != 0}">
                                                            <th style="width: 90px;" >OrgRateWithReefr</th>
                                                            <th style="width: 90px;" >OrgRateWithoutReefr</th>
                                                            </c:if>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="item-row">
                                                        <td></td>
                                                        <c:if test="${customerId != 0}">
                                                            <td><span id="orgValues" style="font-size: 14px"></span></td>
                                                            <td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="ptpFromDate" id="ptpFromDate" value="" readOnly class="  form-control"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="ptpToDate" id="ptpToDate" value="" readOnly class="  form-control"  style="width:150px;height:44px;"/>
                                                                <input type="hidden" name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                                <input type="hidden" name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                                <input type="hidden" name="approvalStatus" value="0" class="form-control" id="approvalStatus"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                            </td>
                                                    <script>
                                                        document.getElementById("ptpFromDate").value = today;
                                                        document.getElementById("ptpToDate").value = fourMDate;
                                                    </script>
                                                </c:if>
                                                <td><c:if test="${vehicleTypeList != null}"><select onchange='setContainerTypeList(this.value);' name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                        <td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px;height:44px;" onchange="contractRouteCheck('');"><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option><option value="3">Loose Load</option></select></td>
                                                        <td><select onchange='setContainerQtyList(this.value);' name="containerTypeId" id="containerTypeId"   class="form-control" style="width:150px;height:44px;"></select></td>
                                                        <td><select name="containerQty" id="containerQty" class="form-control"  style="width:130px;height:44px;" onchange="contractRouteCheck('');"><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>

                                                        <td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.id, 'ptpPickupPointId');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'ptpPickupPointId')"  /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.id, 'interimPointId1');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'interimPointId1')"/><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.id, 'interimPointId2');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'interimPointId2')"/><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"  onchange="checkPointNames(this.id, 'interimPointId3');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'interimPointId3')"/><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" onchange="checkPointNames(this.id, 'interimPointId4');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'interimPointId4')"/><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width:150px;height:44px;"  onKeyPress="return onKeyPressBlockNumbers(event);"  onchange="checkPointNames(this.id, 'ptpDropPointId');
                                                                contractRouteCheck('');" onkeyup="return checkKey(this, event, this.id, 'ptpDropPointId')"/><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width:150px;height:44px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width:150px;height:44px;"/></td>
                                                        <td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width:150px;height:44px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width:150px;height:44px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width:150px;height:44px;"/></td>

                                                        <td><input name="ptpRateWithReefer" value="" class="form-control" id="ptpRateWithReefer" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onBlur="callCheckBox(0)" onchange="contractRouteCheck('')"/></td>
                                                        <td><input name="ptpRateWithoutReefer" value="" class="form-control" id="ptpRateWithoutReefer" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onBlur="callCheckBox(0)" onchange="contractRouteCheck('')"/></td>
                                                    <c:if test="${customerId != 0}">
                                                    <td><span id="orgRateWithReefer" style="font-size: 14px;color:green;"></span><input type="hidden" name="orgRateWithReeferVal" value="" class="form-control" id="orgRateWithReeferVal" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    <td><span id="orgRateWithoutReefer" style="font-size: 14px;color:green;"></span><input type="hidden" name="orgRateWithoutReeferVal" value="" class="form-control" id="orgRateWithoutReeferVal" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    </c:if>
                                                    <c:if test="${customerId == 0}">
                                                    <input name="fuelVehicle" value="0" type="hidden" class="form-control" id="fuelVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                    <input name="fuelDg" value="0" type="hidden" class="form-control" id="fuelDg"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                    <input name="totalFuel" value="0" type="hidden" class="form-control" id="totalFuel"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                    <td><input name="toll" value="" class="form-control" id="toll"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    <td><input name="driverBachat" value="" class="form-control" id="driverBachat"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    <td><input name="dala" value="" class="form-control" id="dala"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    <td><input name="misc" value="" class="form-control" id="misc"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                        <input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="R100" class="form-control"  style="width:150px;height:44px;"/>
                                                        <input type="hidden" name="ptpFromDate" id="ptpFromDate" value="02-02-2017" class="datepicker  form-control"  style="width:150px;height:44px;"/>
                                                        <input type="hidden" name="ptpToDate" id="ptpToDate" value="02-02-2017" class="datepicker  form-control"  style="width:150px;height:44px;"/>
                                                    </td>
                                                    <td><input name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    <td><input name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                    </c:if>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div  id="dcm" class="tab-pane">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" width="90%" id="detentiontable">
                                        <thead>
                                            <tr id="rowId1" >
                                                <th >S No</th>
                                                <th  >Vehicle Type</th>
                                                <th  >Tarif Type</th>
                                                <th  >From(Days)</th>
                                                <th  >To(Days)</th>
                                                <th  >Amount</th>
                                                <th  >Remarks</th>
                                                <!--<th  >Update</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% int Sno = 0;%>
                                            <c:forEach items="${viewdetentioncharge}" var="vdc">
                                                <%
                                                            Sno++;
                                                %>
                                                <tr>
                                                    <td   align="left"> <%= Sno%>  </td>
                                                    <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                    <td>
                                                        <select class="form-control" name="TariftypeName" id="TariftypeName<%= Sno%>" onblur="autoCheckBoxDeten('<%=Sno%>')" style="width:200px;height:44px;">
                                                            <option value='0'>-Select-</option>
                                                            <option value='1'>General</option>
                                                            <option value='2'>Special</option></select>
                                                    </td>
                                            <script>
                                                document.getElementById("TariftypeName<%= Sno%>").value = <c:out value="${vdc.terifType}"/>;
                                            </script>
                                            <td   align="left"> <input type="text" class="form-control" name="detFrom" id="detFrom<%= Sno%>" onblur="autoCheckBoxDeten('<%=Sno%>')"  value='<c:out value="${vdc.timeSlot}"/>' /></td>
                                            <td   align="left">  <input type="text" class="form-control" name="detTo" id="detTo<%= Sno%>" onblur="autoCheckBoxDeten('<%=Sno%>')"  value='<c:out value="${vdc.timeSlotTo}"/>' /></td>


                                            <td align="left"   >
                                                <input type="text" name="detentioncharge" id="detentioncharge<%= Sno%>" class="form-control" onblur="autoCheckBoxDeten('<%=Sno%>')" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vdc.chargeamt}" />"  style="width:150px;height:44px;"/>
                                            </td>
                                            <td   align="left"> <c:out value="${vdc.dcmremarks}" /></td>
                                            <td align="left" style="display:none"  >
                                                <input type="checkbox" name="editCheck" id="editCheck<%= Sno%>" value="1" />
                                                <input type="hidden"  name="denid" id="denid<%= Sno%>" value="<c:out value="${vdc.id}"/>" />
                                                <input type="hidden"  name="detentionEditFlag" id="detentionEditFlag<%= Sno%>" value="0" />
                                            </td>

                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <br>

                                    <SCRIPT language="javascript">
                                        var poItems1 = 1;
                                        var rowCount1 = 2;
                                        var sno1 = 1;
                                        var snumber1 = 1;

                                        function addRowdetention()
                                        {
                                            //                                           alert(rowCount);
                                            if (sno1 < 20) {
                                                sno1++;
                                                var tab = document.getElementById("detentiontable");
                                                rowCount1 = tab.rows.length;

                                                snumber1 = (rowCount1) - 1;
                                                if (snumber1 == 1) {
                                                    snumber1 = parseInt(rowCount1);
                                                } else {
                                                    snumber1++;
                                                }
                                                //snumber1 = snumber1-1;
                                                //alert( (rowCount)-1) ;
                                                //alert(rowCount);
                                                var newrow = tab.insertRow((rowCount1));
                                                newrow.height = "30px";
                                                // var temp = sno1-1;
                                                var cell = newrow.insertCell(0);
                                                var cell0 = "<td class='text1' align='center'> " + snumber1 + "</td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(1);
                                                cell0 = "<td class='text1' ><select class='form-control' name='detention' id='detention" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><c:if test = "${vehicleTypeList!= null}" ><c:forEach  items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /></option></c:forEach ></c:if></select></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;
                                                cell = newrow.insertCell(2);
                                                cell0 = "<td class='text1' ><select class='form-control' name='Tariftype' id='Tariftype" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><option value='1'>General</option><option value='2'>Special</option></select></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(3);
                                                cell0 = "<td class='text1' ><input type='text'   name='dcmunit' id='dcmunit" + snumber1 + "' value='' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);'  style='width:120px;'/></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(4);
                                                cell0 = "<td  ><input type='text'  name='dcmunitTo' id='dcmunitTo" + snumber1 + "' class='form-control'onKeyPress='return onKeyPressBlockCharacters(event);'   style='width:120px;'  value='' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(5);
                                                cell0 = "<td class='text1' ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' onKeyPress='return onKeyPressBlockCharacters(event);'  class='form-control' style='width:120px;height:44px;'/></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(6);
                                                cell0 = "<td class='text1'><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' style='width:120px;height:44px;'/></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                rowCount1++;
                                            }
                                        }

                                            </SCRIPT>

                                        </div>
                                        <br>
                                        <center>
                                            <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowdetention()" />
                                    <c:if test="${billingTypeId != 4}"> <!--  Here-->
                                        &nbsp;&nbsp;&nbsp;<input type="hidden" class="form-control" value="0"  id="addMoreRouteValue"/>
                                        <input type="button" class="btn btn-success" value="Save"   onclick="submitPage(this.value)"/>
                                    </c:if>
                                    <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowdetention()" />-->
                                    <br>
                                    <br>
                                    <!--<input type="button" class="btn btn-success" value="Save" onclick="Savedencharge(this.value)"/>-->
                                </center>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                            </div>


                            <script type="text/javascript">


                                function setReadOnly() {

                                    if (document.getElementById("customerId").value != '0') {
                                        var ratewithReefer = document.customerContract.ptpRateWithoutReefer1;

                                        for (var i = 1; i <= ratewithReefer.length; i++) {
                                            document.getElementById("ptpRateWithoutReefer1" + i).readOnly = true;
                                            document.getElementById("ptpRateWithReefer1" + i).readOnly = true;
                                        }
                                    }
                                }

                                function checkEditOrganizationRate(sno) {
                                    var ptpRateWithoutReefer1 = document.getElementById("ptpRateWithoutReefer1" + sno).value;
                                    var ptpRateWithReefer1 = document.getElementById("ptpRateWithReefer1" + sno).value;
                                    var orgWithoutReeferRate = document.getElementById("orgWithoutReeferRate" + sno).value;
                                    var orgWithReeferRate = document.getElementById("orgWithReeferRate" + sno).value;
                                    if (parseFloat(ptpRateWithoutReefer1) >= parseFloat(orgWithoutReeferRate) && parseFloat(ptpRateWithReefer1) >= parseFloat(orgWithReeferRate)) {
                                        $("#orgValuesE" + sno).text('Auto Approval');
                                        $("#orgValuesE" + sno).addClass('noErrorClass');
                                        document.getElementById("approvalStatus1" + sno).value = "1";
                                    } else {
                                        $("#orgValuesE" + sno).text('Needs Approval');
                                        $("#orgValuesE" + sno).addClass('errorClass');
                                        document.getElementById("approvalStatus1" + sno).value = "2";

                                    }

                                }

                                function setActiveInd(sno) {
                                    if (document.getElementById("editStatus" + sno).checked == true) {
                                        document.getElementById("activeInd" + sno).value = 'Y';
                                    } else {
                                        document.getElementById("activeInd" + sno).value = 'N';
                                    }
                                }



                            </script>

                            <div id="pcm" class="tab-pane">
                                <div class="inpad">
                                    <table class="table table-info mb30 table-hover" width="90%" id="penalitytable">
                                        <thead>
                                            <tr id="rowId0" >
                                                <td    id="tableDesingTD">S No</td>
                                                <td    id="tableDesingTD">Charges names</td>
                                                <td    id="tableDesingTD">Unit</td>
                                                <td    id="tableDesingTD">Amount</td>
                                                <td    id="tableDesingTD">Remarks</td>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <% int sno = 0;%>
                                            <c:forEach items="${viewpenalitycharge}" var="vpc">
                                                <%
                                                            sno++;
                                                %>
                                                <tr>
                                                    <td   align="left" > <%= sno%>  </td>
                                                    <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                    <td   align="left"> <c:out value="${vpc.pcmunit}" /></td>
                                                    <td align="left"   >
                                                        <input type="text" name="penalitycharge" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vpc.chargeamount}" />"/>
                                                    </td>
                                                    <td   align="left"> <c:out value="${vpc.pcmremarks}" /></td>
                                                    <td align="left"   ><input type="hidden"  name="penid" id="penid" value="<c:out value="${vpc.id}"/>" /></td>
                                                </tr>
                                            </c:forEach>

                                            <tr>
                                        <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                        <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                        </tbody>
                                    </table>
                                    <br>
                                    <center>
                                        <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowPenality()" />
                                        <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowPenality()" />-->
                                        &nbsp;&nbsp;
                                        <input type="button" class="btn btn-success" value="Save" onclick="Savepencharge(this.value)"/>
                                    </center>
                                    <SCRIPT language="javascript">
                                        var poItems = 1;
                                        var rowCount = 2;
                                        var sno = 1;
                                        var snumber = 1;

                                        function addRowPenality()
                                        {
                                            //                                           alert(rowCount);
                                            if (sno < 20) {
                                                sno++;
                                                var tab = document.getElementById("penalitytable");
                                                rowCount = tab.rows.length;

                                                snumber = (rowCount) - 1;
                                                if (snumber == 1) {
                                                    snumber = parseInt(rowCount);
                                                } else {
                                                    snumber++;
                                                }
                                                snumber = snumber - 1;
                                                var newrow = tab.insertRow((rowCount));
                                                newrow.height = "30px";
                                                // var temp = sno1-1;
                                                var cell = newrow.insertCell(0);
                                                var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(1);
                                                cell0 = "<td class='text1'><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(2);
                                                cell0 = "<td class='text1'><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(3);
                                                cell0 = "<td class='text1'><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(4);
                                                cell0 = "<td class='text1'><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                rowCount++;
                                            }
                                        }

                                    </SCRIPT>
                                </div>
                                &nbsp;&nbsp;
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                            </div>

                        </c:if>

                        <c:if test="${billingTypeId == 4}">
                            <div id="fkmRate" style="overflow: auto">
                                <c:if test = "${contractFixedRateList != null}">
                                    <table class="table table-info mb30 table-hover" width="90%" id="suppExpenseTBL" >
                                        <thead>
                                            <% int sno1 = 1;%>
                                            <tr height="110" id="tableDesingTD">
                                                <th><h3>S.No</h3></th>
                                        <th><h3>Vehicle Type Name</h3></th>
                                        <th><h3>Vehicle Count</h3></th>
                                        <th><h3>Total Km</h3></th>
                                        <th><h3>Total Reefer Hm</h3></th>
                                        <th><h3>Rate with Reefer</h3></th>
                                        <th><h3>Extra Km Rate/Km with Reefer</h3></th>
                                        <th><h3>Extra Reefer Hm Rate/Hm with Reefer</h3></th>
                                        <th><h3>Rate without Reefer</h3></th>
                                        <th><h3>Extra Km Rate/Km without Reefer</h3></th>
                                        <th><h3>Status</h3></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${contractFixedRateList}" var="ptpl">
                                                <tr>
                                                    <td><%=sno1%></td>
                                                    <td><input type='text' name='editId' id="editId<%=sno1 - 1%>" value='<c:out value="${ptpl.contractFixedRateId}" />'/>
                                                        <input type="hidden" name="fixedKmvehicleTypeId" id="fixedKmvehicleTypeId<%=sno1 - 1%>" value="<c:out value="${ptpl.vehicleTypeId}" />" class="form-control"  />
                                                        <c:out value="${ptpl.vehicleTypeName}" />
                                                    </td>
                                                    <td><input type="text" name="vehicleNos" id="vehicleNos<%=sno1 - 1%>" value="<c:out value="${ptpl.contractVehicleNos}" />" class="form-control"  style="width: 90px;"/></td>
                                                    <td><input type="text" name="fixedTotalKm" id="fixedTotalKm<%=sno1 - 1%>" value="<c:out value="${ptpl.totalKm}" />" class="form-control"  style="width: 90px;"/></td>
                                                    <td><input type="text" name="fixedTotalHm" id="fixedTotalHm<%=sno1 - 1%>" value="<c:out value="${ptpl.totalHm}" />" class="form-control" style="width: 90px;"/></td>
                                                    <td><input type="text" name="fixedRateWithReefer" id="fixedRateWithReefer<%=sno1 - 1%>" value="<c:out value="${ptpl.rateWithReefer}" />" class="form-control"  /></td>
                                                    <td><input type="text" name="extraKmRateWithReefer" id="extraKmRateWithReefer<%=sno1 - 1%>" value="<c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" />" class="form-control"  /></td>
                                                    <td><input type="text" name="extraHmRateWithReefer" id="extraHmRateWithReefer<%=sno1 - 1%>" value="<c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" />" class="form-control"  /></td>
                                                    <td><input type="text" name="fixedRateWithoutReefer" id="fixedRateWithoutReefer<%=sno1 - 1%>" value="<c:out value="${ptpl.rateWithoutReefer}" />" class="form-control"  /></td>
                                                    <td><input type="text" name="extraKmRateWithoutReefer" id="extraKmRateWithoutReefer<%=sno1 - 1%>" value="<c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" />" class="form-control"  /></td>
                                                    <td><select name="status" id="status<%=sno1 - 1%>">
                                                            <c:if test="${ptpl.activeInd == 'Y'}" >
                                                                <option value="Y" selected>Active</option>
                                                                <option value="N">In-Active</option>
                                                            </c:if>
                                                            <c:if test="${ptpl.activeInd == 'N'}" >
                                                                <option value="N" selected>In-Active</option>
                                                                <option value="Y" >Active</option>
                                                            </c:if>
                                                        </select></td>
                                                </tr>
                                                <%sno1++;%>
                                            </c:forEach >
                                            <tr>
                                                <td colspan="11"  align="center">
                                                    <input class="btn btn-success" type="button" value="Add Row" id="addFixedkmRow" onClick="addRow(2);"/>&nbsp;&nbsp;&nbsp;
                                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <script>

                                var rowCount = 1;
                                var sno = 0;
                                var rowCount1 = 1;
                                var sno1 = 0;
                                var httpRequest;
                                var httpReq;
                                var styl = "";

                                function addRow(val) {
                                    if (parseInt(rowCount1) % 2 == 0)
                                    {
                                        styl = "text2";
                                    } else {
                                        styl = "text1";
                                    }

                                    var sn = sno - 1;
                                    var tab = document.getElementById("suppExpenseTBL");
                                    //find current no of rows
                                    var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                                    sno1 = 1 + parseInt(rowCountNew - 2);
                                    rowCountNew--;
                                    var newrow = tab.insertRow(rowCountNew);
                                    cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value='0'/>" + sno1 + "</td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(1);
                                    cell0 = "<td class='text1' height='25'><select class='form-control' id='fixedKmvehicleTypeId" + sno + "' style='width:125px'  name='fixedKmvehicleTypeId'  ><option selected value=0>---Select---</option> <c:if test="${vehicleTypeList != null}" ><c:forEach items="${vehicleTypeList}" var="vehType"><option  value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /> </c:forEach > </c:if> </select></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(2);
                                    cell0 = "<td class='text1' height='25'><input type='text' class='form-control' id='vehicleNos" + sno + "' style='width:125px'  name='vehicleNos' onKeyPress='return onKeyPressBlockCharacters(event);'/></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(3);
                                    cell0 = "<td class='text1' height='25' ><input type='text' name='fixedTotalKm' id='fixedTotalKm" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(4);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedTotalHm' id='fixedTotalHm" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(5);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedRateWithReefer' id='fixedRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(6);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='extraKmRateWithReefer' id='extraKmRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(7);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='extraHmRateWithReefer' id='extraHmRateWithReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(8);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='fixedRateWithoutReefer' id='fixedRateWithoutReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(9);
                                    var cell0 = "<td class='text1' height='25' ><input type='text' name='extraKmRateWithoutReefer' id='extraKmRateWithoutReefer" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' ></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    cell = newrow.insertCell(10);
                                    var cell0 = "<td class='text1' height='25' ><select name='status' id='status" + sno + "' width='120px' ><option value='Y'>Active</option></select></td>";
                                    cell.setAttribute("className", styl);
                                    cell.innerHTML = cell0;
                                    rowCount1++;
                                    sno++;
                                }
                                    </script>

                                    <div id="fkmRoute" style="overflow: auto; display: none">
                                <c:if test = "${fixedKmBillingList != null}" >
                                    <table class="table table-info mb30 table-hover" width="90%">

                                        <thead>
                                            <tr height="40" id="tableDesingTD">
                                                <td   >Sno</td>
                                                <td   >VehicleType</td>
                                                <td   >Origin</td>
                                                <td   >Interim Point1 </td>
                                                <td   >Interim Point2 </td>
                                                <td   >Interim Point3 </td>
                                                <td   >Interim Point4 </td>
                                                <td   >Destination </td>
                                                <td   >Status </td>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                                    int sno = 1;
                                        %>
                                        <tbody>
                                            <c:forEach items="${fixedKmBillingList}" var="contractList">

                                                <tr >
                                                    <td align="left" ><%=sno%>
                                                        <%sno++;%>
                                                    </td>
                                                    <td align="left" >
                                                        <input type="hidden" name="routeContractId" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                        <input type="hidden" name="contractRateId" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                        <c:out value="${contractList.vehicleTypeName}"/>
                                                    </td>
                                                    <td align="left"  ><c:out value="${contractList.firstPickupName}"/></td>
                                                    <td align="left"  ><c:out value="${contractList.point1Name}"/></td>
                                                    <td align="left"  ><c:out value="${contractList.point2Name}"/></td>
                                                    <td align="left"  ><c:out value="${contractList.point3Name}"/></td>
                                                    <td align="left"  ><c:out value="${contractList.point4Name}"/></td>
                                                    <td align="left"  ><c:out value="${contractList.finalPointName}"/></td>
                                                    <td align="left"  >
                                                        <select name="submit">
                                                            <option value="1" selected >Active</option>
                                                            <option value="0"  >In Active</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                    </table>
                                    <div id="addNewRouteButton" style="display:block" >
                                        <center>
                                            <input type="button" class="btn btn-success" name="showAdd" value="Add More" onclick="showAddOption();" />
                                            <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                        </center>

                                    </div>
                                </c:if>
                            </div>
                        </c:if>



                        <c:if test="${billingTypeId == 2}">
                            <!--                        point to point weight-->
                            <c:if test = "${ptpwBillingList != null}" >
                                <table class="table table-info mb30 table-hover" width="90%">
                                    <thead>
                                        <tr height="40" id="tableDesingTD">
                                            <td   >Sno</td>
                                            <td   >Vehicle Route Contract Code</td>
                                            <td   >VehicleType</td>
                                            <td   >Load Type</td>
                                            <td   >Container Type</td>
                                            <td   >Container Qty</td>
                                            <td   >Origin</td>
                                            <td   >Destination</td>
                                            <td   >Rate With Reefer Per Kg</td>
                                            <td   >Rate WithOut Reefer Per Kg</td>
                                            <td   >Status</td>
                                        </tr>
                                    </thead>
                                    <%
                                                int sno1 = 1;
                                    %>
                                    <tbody>
                                        <c:forEach items="${ptpwBillingList}" var="contractList">

                                            <tr >
                                                <td align="left"  ><%=sno1%></td>
                                                <%sno1++;%>
                                                <td><input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="<c:out value="${contractList.routeCode}" />" class="form-control" style="width:120px"/><c:out value="${contractList.routeCode}" /></td>
                                                <td align="left"  >

                                                    <input type="hidden" name="routeContractId" value="<c:out value="${contractList.routeContractId}"/>"/>
                                                    <input type="hidden" name="contractRateId" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                    <c:out value="${contractList.vehicleTypeName}"/>
                                                </td>
                                                <td><input type="hidden" name="loadTypeId" id="loadTypeId" value="<c:out value="${contractList.loadTypeId}" />" class="form-control"  /><input type="hidden" name="ptpVehicleTypeId" id="ptpVehicleTypeId" value="<c:out value="${contractList.ptpVehicleTypeId}" />" class="form-control"  />
                                                    <%--<c:out value="${contractList.loadTypeId}" />--%>
                                                    <c:if test="${contractList.loadTypeId == 1}" >
                                                        Empty Trip
                                                    </c:if>
                                                    <c:if test="${contractList.loadTypeId == 2}" >
                                                        Load Trip
                                                    </c:if>
                                                    <c:if test="${contractList.loadTypeId == 3}" >
                                                        Loose Load
                                                    </c:if>
                                                </td>
                                                <td><input type="hidden" name="containerTypeName" id="containerTypeName" value="<c:out value="${contractList.containerTypeName}" />" class="form-control"  /><c:out value="${contractList.containerTypeName}" /></td>
                                                <td><input type="hidden" name="containerQty1" id="containerQty1" value="<c:out value="${contractList.containerQty1}" />" class="form-control"  /><c:out value="${contractList.containerQty1}" /></td>
                                                <td align="left"  ><c:out value="${contractList.firstPickupName}"/></td>
                                                <td align="left"  ><c:out value="${contractList.finalPointName}"/></td>
                                                <td align="left"  >
                                                    <input type="text" name="ptpwRateWithReefer1" id="ptpwRateWithReefer1<%=sno1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithReeferPerKg}"/>"/>
                                                    <input type="text" name="ptpwRateWithReeferNew" id="ptpwRateWithReeferNew<%=sno1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithReeferPerKg}"/>"/>
                                                </td>
                                                <td align="left"  >
                                                    <input type="text" name="ptpwRateWithoutReefer1" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithoutReeferPerKg}"/>"/>
                                                    <input type="text" name="ptpwRateWithoutReeferNew" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.rateWithoutReeferPerKg}"/>"/>
                                                </td>
                                                <td align="left"  >
                                                    <select name="validStatusNew">
                                                        <option value="1" selected >Active</option>
                                                        <option value="0"  >In Active</option>
                                                    </select>
                                                </td>
                                            </tr>

                                        </c:forEach>
                                </table>
                                <center>
                                    <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                </center>
                            </c:if>
                        </c:if>

                        <c:if test="${billingTypeId == 3}">
                            <!--                        actual kms-->
                            <c:if test = "${actualKmBillingList != null}" >
                                <table class="table table-info mb30 table-hover" width="90%">
                                    <thead>
                                        <tr height="40" id="tableDesingTD">
                                            <th><h3>Sno</h3></th>
                                    <th><h3>VehicleType</h3></th>
                                    <th><h3>Rate Per Km</h3> </th>
                                    <th><h3>Rate Per Reefer Hr</h3> </th>
                                    <th><h3>Status</h3> </th>
                                    </tr>
                                    </thead>
                                    <%
                                                int sno2 = 1;
                                    %>
                                    <tbody>
                                        <c:forEach items="${actualKmBillingList}" var="contractList">

                                            <tr >
                                                <td align="left" ><%=sno2%></td>
                                                <%sno2++;%>
                                                <td align="left" >
                                                    <input type="hidden" name="routeContractId" value="0"/>
                                                    <input type="hidden" name="contractRateId" value="<c:out value="${contractList.contractRateId}"/>"/>
                                                    <c:out value="${contractList.vehicleTypeName}"/>
                                                </td>
                                                <td align="left" >
                                                    <input type="text" name="vehicleRatePerKm" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.vehicleRatePerKm}"/>"/>
                                                </td>
                                                <td align="left" >
                                                    <input type="text" name="reeferRatePerHour" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${contractList.reeferRatePerHour}"/>"/>
                                                </td>
                                                <td align="left" >
                                                    <select name="validStatus">
                                                        <option value="1" selected >Active</option>
                                                        <option value="0"  >In Active</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                </table>
                            </c:if>
                        </c:if>
                        <br>
                        <script>

                            function setContainerTypeList(val) {
                                if (val == '1058') {
                                    // 20FT vehicle
                                    $('#containerTypeId').empty();
                                    $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(1).html('1'))
                                } else if (val == '1059') {
                                    // 40FT vehicle
                                    $('#containerTypeId').empty();
                                    $('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                                    $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                    $('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                                    $('#containerQty').empty();

                                }
                                contractRouteCheck('');
                            }
                            function setContainerQtyList(val) {
                                if (val == '1') {
                                    // 20FT vehicle
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(2).html('2'))
                                } else if (val == '2') {
                                    // 40FT vehicle
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(1).html('1'))

                                }
                                contractRouteCheck('');
                            }

                            function checkKey(obj, e, id, id1) {
                                var idVal = $('#' + id1).val();
                                //                                alert(e.keyCode);
                                if (e.keyCode == 46 || e.keyCode == 8) {
                                    $('#' + obj.id).val('');
                                    $('#' + id).val('');
                                    $('#' + id1).val('');
                                } else if (idVal != '' && e.keyCode != 13) {
                                    $('#' + obj.id).val('');
                                    $('#' + id).val('');
                                    $('#' + id1).val('');
                                }
                            }

                            function checkPointNames(id, id1) {
                                var idValue = $('#' + id).val();
                                var idValue1 = $('#' + id1).val();
                                if (idValue != '' && idValue1 == '') {
                                    alert('Invalid Point Name');
                                    $('#' + id).focus();
                                    $('#' + id).val('');
                                    $('#' + id1).val('');
                                }

                            }

                            function getMarketHireRate(sno) {
                                var customerId = '<c:out value="${customerId}"/>';
                                var vehicleTypeId = "";
                                var loadTypeId = "";
                                var containerTypeId = "";
                                var containerQty = "";
                                var originIdFullTruck = "";
                                var pointId1 = "";
                                var pointId2 = "";
                                var pointId3 = "";
                                var pointId4 = "";
                                var destinationIdFullTruck = "";
                                vehicleTypeId = $("#ptpVehicleTypeId").val();
                                loadTypeId = $("#loadTypeId").val();
                                containerTypeId = $("#containerTypeId").val();
                                containerQty = $("#containerQty").val();
                                originIdFullTruck = $("#ptpPickupPoint").val();
                                pointId1 = $("#interimPointId1").val();
                                pointId2 = $("#interimPointId2").val();
                                pointId3 = $("#interimPointId3").val();
                                pointId4 = $("#interimPointId4").val();
                                destinationIdFullTruck = $("#ptpDropPointId").val();
                                if (vehicleTypeId != '0' && containerTypeId != '0' && containerQty != '0'
                                        && originIdFullTruck != '' && destinationIdFullTruck != '') {
                                    $.ajax({
                                        url: "/throttle/getMarketHireRate.do",
                                        dataType: "json",
                                        data: {
                                            customerId: customerId,
                                            vehicleTypeId: vehicleTypeId,
                                            loadTypeId: loadTypeId,
                                            containerTypeId: containerTypeId,
                                            containerQty: containerQty,
                                            originIdFullTruck: originIdFullTruck,
                                            pointId1: pointId1,
                                            pointId2: pointId2,
                                            pointId3: pointId3,
                                            pointId4: pointId4,
                                            destinationIdFullTruck: destinationIdFullTruck
                                        },
                                        success: function(data, textStatus, jqXHR) {
                                            if (customerId == 0) {
                                                if (data.MarketHireRate > 0) {
                                                    alert("Organisation Contract Already Exists");
                                                }
                                            } else if (customerId > 0) {
                                                if (data.MarketHireRate > 0) {
                                                    alert("Organisation Contract Already Exists");
                                                }
                                            }

                                        },
                                        error: function(data, type) {
                                        }
                                    });
                                }

                            }
                        </script>

                        <div id="addNewRoute" style="display:none" >
                            <center><span id='contractStatus' style="color: red;"></span></center>
                                <c:if test="${billingTypeId == 1}">
                                <div id="ptp" style="overflow: auto;align:left" class="tab-pane active">
                                    <div class="inpad">
                                        <!--<a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /><b> Add Route Code</b></span></a>-->
                                        <!--<br>-->

                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${billingTypeId == 4}">
                                <div id="fkmRate" style="overflow: auto">

                                </div>
                                <div id="fkmRoute" style="overflow: auto">
                                    <div class="inpad">
                                        <table class="table table-info mb30 table-hover" width="90%" id="itemsTable">
                                            <tr id="tableDesingTD">
                                                <td></td>
                                                <td  style="width: 10px;"><font color="red">*</font>Vehicle Route Contract Code</td>
                                                <td   style="width: 10px;"><font color="red">*</font>Vehicle Type</td>
                                                <td   style="width: 10px;"><font color="red">*</font>First Pick UP</td>
                                                <td   >Interim Point 1</td>
                                                <td   >Interim Point 2</td>
                                                <td   >Interim Point 3</td>
                                                <td   >Interim Point 4</td>
                                                <td   ><font color="red">*</font>Final Drop Point</td>
                                                <td   style="width:150px;height:44px;" ><font color="red">*</font>Total Km</td>
                                            </tr>
                                            <tbody>
                                                <tr class="item-row">
                                                    <td></td>
                                                    <td><input name="ptpRouteContractCode" id="ptpRouteContractCode" value="" class="form-control"  /></td>
                                                    <td><c:if test="${vehicleTypeList != null}"><select name="ptpVehicleTypeId" id="ptpVehicleTypeId"  class="form-control" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                            <td><input name="ptpPickupPoint" value="" class="form-control" id="ptpPickupPoint"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);" /><input type="hidden" name="ptpPickupPointId" value="" class="form-control" id="ptpPickupPointId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="interimPoint1" value="" class="form-control" id="interimPoint1"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId1" value="" class="form-control" id="interimPointId1"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Km" value="" class="form-control" id="interimPoint1Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Hrs" value="" class="form-control" id="interimPoint1Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1Minutes" value="" class="form-control" id="interimPoint1Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint1RouteId" value="" class="form-control" id="interimPoint1RouteId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="interimPoint2" value="" class="form-control" id="interimPoint2"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId2" value="" class="form-control" id="interimPointId2"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Km" value="" class="form-control" id="interimPoint2Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2Hrs" value="" class="form-control" id="interimPoint2Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint2Minutes" value="" class="form-control" id="interimPoint2Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint2RouteId" value="" class="form-control" id="interimPoint2RouteId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="interimPoint3" value="" class="form-control" id="interimPoint3"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId3" value="" class="form-control" id="interimPointId3"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Km" value="" class="form-control" id="interimPoint3Km"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Hrs" value="" class="form-control" id="interimPoint3Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3Minutes" value="" class="form-control" id="interimPoint3Minutes"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint3RouteId" value="" class="form-control" id="interimPoint3RouteId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="interimPoint4" value="" class="form-control" id="interimPoint4"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="interimPointId4" value="" class="form-control" id="interimPointId4"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Km" value="" class="form-control" id="interimPoint4Km"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4Hrs" value="" class="form-control" id="interimPoint4Hrs"  style="width:150px;height:44px;"/><input type="hidden" name="interimPoint4Minutes" value="" class="form-control" id="interimPoint4Minutes"  style="width:150px;height:44px;"/><input type="hidden"  name="interimPoint4RouteId" value="" class="form-control" id="interimPoint4RouteId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="ptpDropPoint" value="" class="form-control" id="ptpDropPoint"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpDropPointId" value="" class="form-control" id="ptpDropPointId"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointKm" value="" class="form-control" id="ptpDropPointKm"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointHrs" value="" class="form-control" id="ptpDropPointHrs"  style="width:150px;height:44px;"/><input type="hidden" name="ptpDropPointMinutes" value="" class="form-control" id="ptpDropPointMinutes"  style="width:150px;height:44px;"/><input type="hidden"  name="ptpDropPointRouteId" value="" class="form-control" id="ptpDropPointRouteId"  style="width:150px;height:44px;"/></td>
                                                            <td><input name="ptpTotalKm" value="" class="form-control" id="ptpTotalKm"  style="width:150px;height:44px;" readonly/><input type="hidden" name="ptpTotalHours" value="" class="form-control" id="ptpTotalHours"  style="width:150px;height:44px;"/><input type="hidden" name="ptpTotalMinutes" value="" class="form-control" id="ptpTotalMinutes"  style="width:150px;height:44px;"/><input type="hidden" name="ptpRateWithReefer" value="0" class="form-control" id="ptpRateWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="ptpRateWithoutReefer" value="0" class="form-control" id="ptpRateWithoutReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Code</span></a>
                                                <br>

                                            </div>
                                        </div>

                            </c:if>
                            <c:if test="${billingTypeId == 2}">
                                <div id="ptpw" style="overflow: auto">

                                    <div class="inpad">
                                        <table class="table table-info mb30 table-hover" width="90%" id="itemsTable1">
                                            <!--<table id="itemsTable1" class="general">-->
                                            <thead>
                                                <tr id="tableDesingTD">
                                                    <td></td>
                                                    <td   style="width:120px" ><font color="red">*</font>Vehicle Route Contract Code</td>
                                                    <td   style="width:120px"><font color="red">*</font>Vehicle Type</td>
                                                    <td   style="width:120px"><font color="red">*</font>First Pick UP</td>
                                                    <td   style="width:120px"><font color="red">*</font>Final Drop Point</td>
                                                    <td   style="width:120px"><font color="red">*</font>Total Km</td>
                                                    <td   style="width:120px"><font color="red">*</font>Rate With Reefer</td>
                                                    <td   style="width:120px"><font color="red">*</font>Rate Without Reefer</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="item-row">
                                                    <td></td>
                                                    <td><input name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="" class="form-control" style="width:120px"/></td>
                                                    <td><c:if test="${vehicleTypeList != null}"><select name="ptpwVehicleTypeId" id="ptpwVehicleTypeId"  class="form-control" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                            <td><input type="hidden" name="ptpwPickupPointId" id="ptpwPickupPointId" value="" class="form-control"  style="width: 120px;"/><input name="ptpwPickupPoint" id="ptpwPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>
                                                            <td><input name="ptpwDropPoint" id="ptpwDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="ptpwDropPointId" id="ptpwDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwPointRouteId" id="ptpwPointRouteId" value="" class="form-control"  style="width: 120px;"/></td>
                                                            <td><input name="ptpwTotalKm" id="ptpwTotalKm" value="" class="form-control"  style="width: 120px;" readonly/><input type="hidden" name="ptpwTotalHrs" id="ptpwTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="ptpwTotalMinutes" id="ptpwTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>
                                                            <td><input name="ptpwRateWithReefer" id="ptpwRateWithReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                            <td><input name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <a href="#" id="addRow1" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Code</span></a>
                                                <br>
                                                <center>
                                                    <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                </center>
                                            </div>
                                        </div>
                            </c:if>

                        </div>
                        <br>

                        <c:if test="${billingTypeId == 1}">
                            <script>
                                $('.btnNext').click(function() {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                $('.btnPrevious').click(function() {
                                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                });
                            </script>
                        </c:if>
                        <c:if test="${billingTypeId == 4}">
                            <script>
                                $(".nexttab").click(function() {
                                    var chek = validateFixedKmRateDetails();
                                    //                        alert(chek);
                                    if (chek == true) {
                                        $("#addFixedkmRow").hide();
                                        $("#fkmRouteDetails").show();
                                        $("#fkmRoute").show();
                                        var selected = $("#tabs").tabs("option", "selected");
                                        $("#tabs").tabs("option", "selected", selected + 1);
                                    }
                                });
                            </script>
                        </c:if>

                        <script>
                            function configModelFuel(typeId, contractRateId, status) {
                                //                                                                                                alert(slabId)
                                $.ajax({
                                    url: "/throttle/configModelFuel.do?",
                                    data: {
                                        typeId: typeId, contractRateId: contractRateId, status: status

                                    },
                                    type: "GET",
                                    success: function(response) {
                                        $("#modelFuelDetailsTab").html(response);
                                    }
                                });
                            }

                        </script>
                        <br>
                        <div class="modal fade" id="myModal" role="dialog" style="width: auto;height: auto;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" onclick="resetTheDetails1()">&times;</button>
                                        <h4 class="modal-title">Model Fuel Details</h4>
                                    </div>
                                    <div class="modal-body" id="modelFuelDetailsTab" style="width: auto;height:auto;">
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>

                        <script type="text/javascript">
                            function callCheckBox(val) {
                                //alert(val);
                                document.getElementById("editStatus" + val).checked = true;
                            }
                        </script>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                        <br/>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


                <%@ include file="/content/common/NewDesign/settings.jsp" %>

