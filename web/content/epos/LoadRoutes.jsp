<%
    Connection conn = null;
    try{
        String locationId = request.getParameter("LocationCode");
        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);
        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        //String locationId="";
        conn = DriverManager.getConnection(jdbcUrl, userName, password);
        //Statement stmt = conn.createStatement();
        Statement stmt1 = conn.createStatement();
        //ResultSet res = stmt.executeQuery("SELECT Location_Id FROM ra_route_master WHERE route_id = '"+routeCode+"'");
        //while(res.next()) {
            //locationId = res.getString("Location_Id");            
            ResultSet res1 = stmt1.executeQuery("SELECT route_id as Route_Code1,route_code,Location_Id,concat(from_location,' - ',to_location) as RouteName FROM ra_route_master WHERE Location_Id = "+locationId);
            while(res1.next()) {
                out.print((res1.getString("Route_Code1")+"$"+res1.getString("RouteName")+"$").trim());
            }
        //}
       if(conn != null) {
            conn.close();
        }
    }catch (Exception e) {
        out.print("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Routes --> ").append(e.getMessage()).toString());
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,ets.domain.util.FPLogUtils" %>