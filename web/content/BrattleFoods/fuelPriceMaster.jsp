<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#cityName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#cityId').val(Id);
                $itemrow.find('#cityName').val(Name);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });


    function resetCityId() {
        if (document.getElementById('cityName').value == '') {
            document.getElementById('cityId').value = '';
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Fuel</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Fuel</a></li>
            <li class="active"> Manage Fuel Price</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="fuleprice"  method="post" >
                    <br>
                    <c:if test="${effectiveDate != null}">
                        <div id="approval" style="width:300px; margin:0 auto;"><font color="green" size="4">New Fuel Price Requested To Admin </font></div>
                        </c:if>

                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bg" class="border">
                        <tr align="center" id="tableDesingTD" height="30">
                            <td colspan="2" align="center" height="30">
                                New Fuel Price
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text1" height="30"><font color="red">*</font>Effective Date/Time</td>
                            <td class="text1"> <input type="text" class="datepicker" id="effectiveDate" name="effectiveDate" value='' style="height:22px;width:150px;">
                                &nbsp;&nbsp;&nbsp;
                              <font color="red">*</font>HH:<select name="fuelEffectiveHour"  id="fuelEffectiveHour" class="textbox"><option value="00" selected >00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                               &nbsp;&nbsp;&nbsp;
                               <font color="red">*</font>MI:<select name="fuelEffectiveMinute" id="fuelEffectiveMinute" class="textbox"><option value="00" selected >00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2" height="30"><font color="red">*</font>Fuel Price INR:</td>
                            <td class="text2" height="30"><input type="text" id="fuelPrice" name="fuelPrice" class="textbox" value='' style="height:22px;width:150px;"></td>
                        </tr>
                        <tr height="30">
                            <td class="text1" height="30"><font color="red">*</font>Fuel Type:</td>
                            <td class="text1"><select name="fuelType" id="fuelType" style="height:22px;width:150px;">
                                    <option value="0">--Select--</option>
                                    <c:if test="${fuleTypeList != null}">
                                        <c:forEach items="${fuleTypeList}" var="ful">
                                            <option value='<c:out value="${ful.fuelTypeId}"/>'><c:out value="${ful.fuelTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2" height="30"><input type="hidden" id="bunkName" name="bunkName" value=""/>
                                <input type="hidden" id="bunkId" name="bunkId" value="" style="height:22px;width:150px;"/><font color="red">*</font>Bunk Name:</td>
                            <td class="text2"><select name="bunkIds" id="bunkIds" onchange="getBunkNames();" style="height:22px;width:150px;">
                                    <option value="0">--Select--</option>
                                    <c:if test="${bunkList != null}">
                                        <c:forEach items="${bunkList}" var="bunk">
                                            <option value='<c:out value="${bunk.bunkId}"/>~<c:out value="${bunk.bunkName}"/>'><c:out value="${bunk.bunkName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        <script>
                            function getBunkNames() {
                                var bunkIds = document.getElementById("bunkIds").value;
                                var bunkNames = bunkIds.split('~');
                                document.getElementById("bunkId").value = bunkNames[0];
                                document.getElementById("bunkName").value = bunkNames[1];
                            }
                        </script>

                        </tr>
                        <tr height="30">
                            <td class="text1" height="30"><font color="red">*</font>Fuel Unit:</td>
                            <td class="text1" height="30">

                                <select name="fuelUnite" id="fuelUnite" style="height:22px;width:150px;">
                                    <option value="litre">Litres</option>
                                    <option value="kg">Kg</option>
                                </select>
                            </td>
                            <!--                <script>
                                                document.getElementById("fuelUnite").value='<c:out value="${fuelUnite}"/>';
                                            </script>-->
                        </tr>
                        <!--                <tr>
                                            <td class="text1" height="30"><font color="red">*</font>City</td>
                                            <td class="text2"><input type="hidden" name="cityId" id="cityId"  value="641"/>
                                                <input type="hidden" class="textbox" name="cityName" id="cityName"  onchange="resetCityId();"/></td>
                        
                                        </tr>-->
                        <tr height="50"><td class="text2" height="30" colspan="2" align="center">
                                <input type="hidden" name="cityId" id="cityId"  value="641"/>
                                <input type="hidden" name="previousFuelStatus" id="previousFuelStatus"  value="<c:out value="${previousFuelStatus}"/>"/>
                                <input type="hidden" class="textbox" name="cityName" id="cityName"  onchange="resetCityId();"/>
                                <input align="center" type="button" class="btn btn-info" value="Save"  onClick="submitPage();" id="buttonDesign"></td>
                        </tr>
                    </table>

                    <br>
                    <!--            <table align="center">
                                    <tr class="text2">
                                        <td>Current Average Fuel Cost&nbsp;=&nbsp;</td>
                                        <td><c:out value="${avgCurrentFuelCost}"/></td>
                                    </tr>
                                </table>-->
                    <table width="20%" align="center" border="0" id="table1" class="sortable">
                        <thead>
                            <tr height="30" id="tableDesingTH">
                                <th><h3>S.No</h3></th>
                        <th><h3>Bunk Name</h3></th>
                        <th><h3>Current Fuel Price (INR)</h3></th>
                        </tr>
                        </thead>
                        <tbody>
                            <% int sno1 = 0;%>
                            <%
                                                  sno1++;
                            %>
                            <c:if test = "${bunkPriceList != null}" >
                                <c:forEach items="${bunkPriceList}" var="bunkPriceList">
                                    <tr>
                                        <td class="text2" align="left" > <%= sno1++%></td>
                                        <td class="text2" align="left" ><c:out value="${bunkPriceList.bunkName}"/></td>
                                        <td class="text2" align="left" ><c:out value="${bunkPriceList.fuelPrice}"/></td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                    <br>

                    <center style="color:black;font-size: 14px;"> View Fuel Update History</center>
                    <table width="90%" align="center" border="0" id="table" class="sortable">
                        <thead height="40">
                            <tr height="40" id="tableDesingTH">
                                <th><h3>S.No</h3></th>
                        <th><h3>Bunk Name</h3></th>
                        <th><h3>Effective Date From</h3></th>
                        <th><h3>Fuel Price (INR)</h3></th>
                        <th><h3>Change in Price (INR)</h3></th>
                        <th><h3>Status</h3></th>

                        </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>

                            <c:if test = "${viewfuelPriceMaster != null}" >
                                <%
                                                    sno++;
                                                   String className = "text1";
                                                   if ((sno % 1) == 0) {
                                                       className = "text1";
                                                   } else {
                                                       className = "text2";
                                                   }
                                %>
                                <c:forEach items="${viewfuelPriceMaster}" var="fuelPriceMaster">
                                    <c:set var="approvedStatus" value="${fuelPriceMaster.approveStatus}"/>
                                    <tr height="30">
                                        <td class="<%=className%>"  height="30"> <%= sno++%></td>
                                        <td class="<%=className%>" align="left" ><c:out value="${fuelPriceMaster.bunkName}"/></td>
                                        <td class="<%=className%>" align="left"> <input type="hidden" id="oldFuelPriceId" name="oldFuelPriceId" value="<c:out value="${fuelPriceMaster.fuelPriceId}"/>"/>
                                            <c:out value="${fuelPriceMaster.effectiveDate}"/></td>
                                        <td class="<%=className%>" align="left" ><c:out value="${fuelPriceMaster.fuelPrice}"/></td>
                                        <td class="<%=className%>" align="left" ><c:out value="${fuelPriceMaster.priceDiff}"/></td>
                                        <c:if test="${approvedStatus == '0'}">
                                            <td class="<%=className%>" align="left" >  <div id="approval" style="width:100px; margin:0 auto;">&emsp;&emsp;&emsp;&emsp;<img src="/throttle/images/rejected.jpg" align="center" width="80px" height="50px"></div></td>
                                                </c:if>
                                                <c:if test="${approvedStatus == null || approvedStatus == ''}">
                                            <td class="<%=className%>" align="left" ><div id="approval" style="width:100px; margin:0 auto;">&emsp;&emsp;&emsp;&emsp;<img src="/throttle/images/pending.jpg" align="center" width="70px" height="50px"></div></td>
                                                </c:if>
                                                <c:if test="${approvedStatus == '1'}">
                                            <td class="<%=className%>" align="left" > <div id="approval" style="width:100px; margin:0 auto;">&emsp;&emsp;&emsp;&emsp;<img src="/throttle/images/approved.jpg" align="center" width="80px" height="50px"></div></td>
                                                </c:if>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                    <script type="text/javascript">
                        function submitPage() {
                            var previousFuelStatus=document.getElementById('previousFuelStatus').value;
                            var temp=previousFuelStatus.split("~");//alert(previousFuelStatus);
                            if(temp[0] == 0){
                            if (document.getElementById('effectiveDate').value == '') {
                                alert("Please select date");
                                document.getElementById('effectiveDate').focus();
                            } else if (document.getElementById('fuelEffectiveHour').value == '') {
                                alert("Please enter Hour");
                                document.getElementById('fuelEffectiveHour').focus();
                            } else if (document.getElementById('fuelEffectiveMinute').value == '') {
                                alert("Please enter Minute");
                                document.getElementById('fuelEffectiveMinute').focus();
                            } else if (document.getElementById('fuelPrice').value == '') {
                                alert("Please enter fuel price");
                                document.getElementById('fuelPrice').focus();
                            } else if (document.getElementById('fuelType').value == '0') {
                                alert("Please select fuelType");
                                document.getElementById('fuelType').focus();
                            } else if (document.getElementById('bunkIds').value == '0') {
                                alert("Please select BunkName");
                                document.getElementById('bunkIds').focus();
                            } else {
                                document.getElementById('effectiveDate').value=document.getElementById('effectiveDate').value +"~"+document.getElementById('fuelEffectiveHour').value+":"+document.getElementById('fuelEffectiveMinute').value;
                                document.fuleprice.action = '/throttle/approveFuelPrice.do';
                                //                      document.fuleprice.action = '/throttle/saveFuelPriceMaster.do';
                                document.fuleprice.submit();
                            }
                        }else{
                            alert("sorry! you cannot raise another fuel price update unless previous day's(" +temp[1]+")fuel price gets updated.");
                        }
                        }
                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

