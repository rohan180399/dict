
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepi  cker.js"></script>-->   
<script>

    function submitPage() {

        if (document.receiveRcWo.vendorId.value == 0) {
            alert("Agent Name Should Not be Empty");
            document.receiveRcWo.vendorId.focus();
            return;
        }
        if (document.receiveRcWo.woIds.value == 0) {
            alert("Work Order Number Should Not be Empty");
            return;
        }

        /*
         if(textValidation(document.receiveRcWo.billAmount,'Bill Amount')){       
         return;
         } 
         if(textValidation(document.receiveRcWo.receivedDate,'Received Date')){       
         return;
         } 
         if(textValidation(document.receiveRcWo.remarks,'Invoice No')){       
         return;
         } 
         */
        //document.receiveRcWo.receivedDate.value = dateFormat(document.receiveRcWo.receivedDate);
        document.receiveRcWo.action = '/throttle/rcItems.do';
        document.receiveRcWo.submit();
    }


    function setValues() {
        document.receiveRcWo.vendorId.focus();
        if ('<%=request.getAttribute("vendorId")%>' != 'null') {
            document.receiveRcWo.vendorId.value = '<%= request.getAttribute("vendorId")%>';
        }

        if ('<%=request.getAttribute("receivedDate")%>' != 'null') {
            document.receiveRcWo.receivedDate.value = '<%= request.getAttribute("receivedDate")%>';
        }

        if ('<%=request.getAttribute("billAmount")%>' != 'null') {
            document.receiveRcWo.billAmount.value = '<%= request.getAttribute("billAmount")%>';
        }

        if ('<%=request.getAttribute("inVoiceNo")%>' != 'null') {
            document.receiveRcWo.remarks.value = '<%= request.getAttribute("inVoiceNo")%>';
        }

        ajaxWoList(document.receiveRcWo.vendorId.value);
    }

    function saveRcItems() {
    
        if (textValidation(document.receiveRcWo.billAmount, 'Bill Amount')) {
            return;
        }
        if (document.receiveRcWo.receivedDate.value == '') {
            alert("please enter rc received date");
            return;
        }
        if (textValidation(document.receiveRcWo.remarks, 'Invoice No')) {
            return;
        }
        var checValidate = selectedItemValidation();
    }

    function ajaxWoListOLD(value) {
        var vendorId = value;
        var url = '/throttle/rcWorkOrderList.do?vendorId=' + vendorId;
        if (window.ActiveXObject) {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest) {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processAjax();
        };
        httpReq.send(null);
    }

    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                var splt = temp.split('-');
                setOptions1(temp, document.receiveRcWo.woIds, '0', '0');
                document.receiveRcWo.woIds.value = '<%=request.getAttribute("woIds")%>';
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }

    function ajaxWoList(value) { 
        $.ajax({
            url : '/throttle/rcWorkOrderList.do',
            dataType: "json",
            data: {
                vendorId: value
            },
            success: function (temp) {
                // alert(data);
                if (temp != '') {
                    $('#woIds').empty();
                    $('#woIds').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    $.each(temp, function (i, data) {
                        $('#woIds').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Id)
                                )
                    });
                } else {
                    $('#woIds').empty();
                }
            }
        });

}


    function selectedItemValidation() {
    
        var index = document.getElementsByName("selectedIndex");
        var itemAmount = document.getElementsByName("itemAmounts");
        var rcStatus = document.getElementsByName("rcStatuses");


        if (document.receiveRcWo.vendorId.value == 0) {
            alert("Agent Name Should Not be Empty");
            document.receiveRcWo.vendorId.focus();
            return;
        }
        if (document.receiveRcWo.woIds.value == 0) {
            alert("Work Order Number Should Not be Empty");
            return;
        }
        if (textValidation(document.receiveRcWo.billAmount, 'Bill Amount')) {
            return;
        }
        if (textValidation(document.receiveRcWo.receivedDate, 'Received Date')) {
            return;
        }
        if (textValidation(document.receiveRcWo.remarks, 'Invoice No')) {
            return;
        }
        
        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (rcStatus[i].value == '0') {
                alert("Please Select Rc Status");
                rcStatus[i].focus();
                return;
            }
        }
        
        if (calculateTotal() == 'fail') {
            return 'fail';
        }
        
        document.receiveRcWo.action = '/throttle/saveRcItems.do';
        document.receiveRcWo.submit();
    }

    function checkScrap(val)
    {
        var rcStatus = document.getElementsByName("rcStatuses");
        var index = document.getElementsByName("scrap");
        if (rcStatus[val].value == 'N') {
            index[val].checked = true;
        }
    }

    function calculatSparesTax(val)
    {
        var taxPerc = document.getElementsByName("vats");
        var sparesAmnt = document.getElementsByName("sparesAmounts");
        var sparesAmntExt = document.getElementsByName("sparesAmountsExternal");
        var sparesWithTax = document.getElementsByName("sparesWithTaxs");
        var spareAmntWithOutTax = "";
    if(taxPerc[val].value != 0) {
        if (isFloat(sparesAmnt[val].value)) {
            alert("Please Enter Valid Spares Amount");
            sparesAmnt[val].focus();
            sparesAmnt[val].select();
            return "fail";
        }
        if (isFloat(sparesAmntExt[val].value)) {
            alert("Please Enter Valid Spares Amount External");
            sparesAmntExt[val].focus();
            sparesAmntExt[val].select();
            return "fail";
        }
        if (isFloat(taxPerc[val].value)) {
            alert("Please Enter Valid Tax Percentage");
            taxPerc[val].focus();
            taxPerc[val].select();
            return "fail";
        }
        spareAmntWithOutTax = parseFloat(sparesAmnt[val].value) + parseFloat(sparesAmntExt[val].value)
        sparesWithTax[val].value = parseFloat(spareAmntWithOutTax) + ((parseFloat(spareAmntWithOutTax) * parseFloat(taxPerc[val].value)) / 100);
        sparesWithTax[val].value = parseFloat(sparesWithTax[val].value).toFixed(2);
        return 'pass';
        }
    }


    function calculatePrice(val)
    {
        var sparesWithTax = document.getElementsByName("sparesWithTaxs");
        var laborCharge = document.getElementsByName("laborAmounts");
        var price = document.getElementsByName("itemAmounts");

        if (isFloat(laborCharge[val].value)) {
            alert("Please Enter Valid Labor Charge");
            laborCharge[val].focus();
            laborCharge[val].select();
            return "fail";
        }
        price[val].value = parseFloat(sparesWithTax[val].value) + parseFloat(laborCharge[val].value);
        price[val].value = parseFloat(price[val].value).toFixed(2);
        return 'pass';
    }



    function calculatePriceAmnt(val)
    {

        if (calculatSparesTax(val) == 'fail') {
            //document.receiveRcWo.billAmount.value = parseFloat(document.receiveRcWo.billAmount.value);
            return 'fail';
        }
        if (calculatePrice(val) == 'fail') {
            //document.receiveRcWo.billAmount.value = parseFloat(document.receiveRcWo.billAmount.value);
            return 'fail';
        }
        calculateTotal();
        return 'pass';
    }

    function calculateTotal()
    {
        var price = document.getElementsByName("itemAmounts");
        var sparesAmnt = document.getElementsByName("sparesAmounts");
        var temp = 0;
        for (var i = 0; i < price.length; i++) {
            if (sparesAmnt[i].value != '') {
                if (calculatSparesTax(i) == 'fail') {
                    //document.receiveRcWo.billAmount.value = parseFloat(document.receiveRcWo.billAmount.value);
                    return 'fail';
                }
                if (calculatePrice(i) == 'fail') {
                    //document.receiveRcWo.billAmount.value = parseFloat(document.receiveRcWo.billAmount.value);
                    return 'fail';
                }
                temp = parseFloat(temp) + parseFloat(price[i].value);
                document.receiveRcWo.billAmount.value = temp;
                document.getElementById("total").innerHTML = temp;
            }
        }
        document.receiveRcWo.billAmount.value = parseFloat(document.receiveRcWo.billAmount.value).toFixed(2);
        document.getElementById("total").innerHTML = document.receiveRcWo.billAmount.value;
        return 'pass';

    }


</script>
<!--<script type="text/javascript">
   $("#visaDateOfValidity").datepicker({
                                        changeMonth: true, changeYear: true, 
                                         format: 'dd-mm-yy'
   });
</script> -->


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--  <span style="float: right">
            <a href="?paramName=en">English</a>
            |
            <a href="?paramName=ar">Arabic</a>
      </span>
    -->
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>  <spring:message code="stores.label.MODIFYWO"  text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="recondition.label.Recondition"  text="default text"/></a></li>
                <li class="active"><spring:message code="stores.label.MODIFYWO"  text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onLoad="setValues();">
                    <form name="receiveRcWo" method="post">

                        <%--<%@ include file="/content/common/path.jsp" %>--%>


                        <%@ include file="/content/common/message.jsp" %>

                        <!--<table class="table table-info mb30 table-hover">-->
                            <tr>
                                <td colspan="6"  style="background-color:#5BC0DE;">
                                    <table class="table table-info mb30 table-hover" id="bg" >
                                        <thead>
                                        <tr>
                                            <th  height="30" colspan="8"><div ><spring:message code="recondition.label.ReceiveRCItems"  text="default text"/></div></th>
                                        </tr>
                                        </thead>
                                        <tr >
                                            <td width="120"  height="30" ><spring:message code="recondition.label.Agent"  text="default text"/>
                                            </td>                    
                                            <td width="149" height="30" ><select  name="vendorId" onchange="ajaxWoList(this.value);" class="form-control" style="width:260px;height:40px;" >            
                                                    <option value="0">---- <spring:message code="recondition.label.Select"  text="default text"/> ---</option>
                                                    <c:if test = "${vendorLists != null}" >
                                                        <c:forEach items="${vendorLists}" var="vendor"> 
                                                            <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>                                
                                                        </c:forEach>
                                                    </c:if>
                                                </select> </td>                    
                                            <td width="149" height="30" ><spring:message code="recondition.label.RCWorkOrderNo"  text="default text"/></td>
                                            <td><select name="woIds" id="woIds"  class="form-control" style="width:260px;height:40px;" >
                                                    <c:if test = "${getWo != null}" >
                                                        <c:forEach items="${getWo}" var="Wo"> 
                                                            <option value="<c:out value="${Wo.woId}"/>"><c:out value="${Wo.woId}"/></option>                                    
                                                        </c:forEach>
                                                    </c:if>
                                                </select></td>
                                                <td  height="30" align="left" ><spring:message code="recondition.label.BillAmount"  text="default text"/></td>
                                            <td colspan="4" height="30" align="left" ><input type="text" class="form-control" style="width:260px;height:40px;" name="billAmount"  value=""></td>
                                       
                                        </tr>
                                        <tr>
                                            
                                             <td width="149" height="30" ><spring:message code="recondition.label.ReceivedDate"  text="default text"/></td>
                                            <td  height="30" align="left" ><input name="receivedDate" type="text"   style="width:260px;height:40px;" class="datepicker"  ></td>
                                        <td  height="30" align="left" ><spring:message code="recondition.label.InvoiceNo"  text="default text"/>.</td>
                                            <td  height="30" align="left" ><input type="text" class="form-control" style="width:260px;height:40px;" name="remarks" value=""></td>
                                            <td></td>
                                            <td colspan="4" height="30" align="center" >
                                                <input name="button" align="center" type="button" class="btn btn-info" onClick="submitPage();" value="<spring:message code="recondition.label.SEARCH"  text="default text"/>"/>
                                            </td>
<!--                                            <td></td>-->
                                        </tr>  
<!--                                        <tr>
                                            
                                            <td  height="30" align="left" >&nbsp;</td>
                                            <td  height="30" align="left" >&nbsp;</td>
                                        </tr>                 -->
                                        
<!--                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                        </tr>                -->
                                    </table>
                                </td>            
                        <!--</table>-->
                                <td>
                                    <table class="table table-info mb30 table-hover">
                                        <thead>
                                        <tr> 
                                            <th colspan="2" height="30"  align="center" ><spring:message code="recondition.label.Total"  text="default text"/></th>
                                        </tr>
                                        </thead>
                                        <tr> 
                                            <td width="121" height="30"  ><spring:message code="recondition.label.SAR"  text="default text"/>.</td>
                                            <td width="86" height="30" ><div id="total" ></div>  </td>
                                        </tr>                
                                    </table>               
                                </td> 
                            </tr>            
                        <c:if test = "${ItemList != null}" >      
                            <table class="table table-info mb30 table-hover">
                                <thead>                   
                                <tr>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.Sno"  text="sno"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.MFRItemCode"  text="OEM Code"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.PAPLItemCode"  text="Internal Code"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.RCNumber"  text="RCRefNo"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.TyreNumber"  text="Tyre No"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.ItemName"  text="PartName"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.MaterialCost(Int)"  text="MaterialCost(I)"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.MaterialCost(Ext)"  text="MaterialCost(E)"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.VAT(%)"  text="VAT(%)"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.MaterialCost"  text="MaterialCost"/><br> <spring:message code="recondition.label.InclusiveofTax"  text="InclusiveofTax"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.LaborCharge"  text="LaborCharge"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.Amount"  text="Amount"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.RCDone"  text="RCDone"/></th>
                                    <th  height="30"  align="left" ><spring:message code="recondition.label.ScrapThisItem"  text="ScrapThisItem"/></th>

                                </tr>
                                </thead>
                                <% int index = 0;%>
                                <c:forEach items="${ItemList}" var="item">          
                                    <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                                    %>

                                    <tr>
                                        <td class="<%=classText %>" height="30">  <%=index + 1%></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>
                                        <td class="<%=classText %>" height="30">
                                            <input type="hidden" name="rcItemIds" value="<c:out value="${item.rcItemId}"/>">
                                            <input type="hidden" name="tyreId" value="<c:out value="${item.tyreId}"/>">
                                            <c:out value="${item.rcItemId}"/></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${item.tyreNo}"/></td>
                                        <td class="<%=classText %>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${item.itemId}"/>" ><c:out value="${item.itemName}"/></td>
                                        <td class="<%=classText %>" height="30"><input type="text" class="form-control" size="6" name="sparesAmounts" value="<c:out value="${item.sparesAmount }"/>" onBlur="calculatSparesTax('<%= index %>');" ></td>
                                        <td class="<%=classText %>" height="30"><input type="text" class="form-control" size="6" name="sparesAmountsExternal" value="" onBlur="calculatSparesTax('<%= index %>');" ></td>
                                        <td class="<%=classText %>" height="30">
                                            <select name="vats" class="form-control" onChange="calculatSparesTax('<%= index %>');" >
                                                <option value="0" > -select- </option>
                                                <c:if test="${vatList!=null}" >
                                                    <c:forEach items="${vatList}" var="vat" >
                                                        <option value="<c:out value="${vat.vat}"/>" > <c:out value="${vat.vat}"/> </option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>                                                                                    
                                        </td>
                                        <td class="<%=classText %>" height="30"><input type="text" readonly  class="form-control"  size="6" name="sparesWithTaxs" value="" ></td>
                                        <td class="<%=classText %>" height="30"><input type="text"  class="form-control" size="6"  name="laborAmounts" value="" onBlur="calculatePriceAmnt(<%= index %>)" > </td>
                                        <td class="<%=classText %>" height="30"><input type="text" readonly  class="form-control"  size="6" name="itemAmounts"   value="" class="form-control"> </td>  

                                        <td class="<%=classText %>" align="center" height="30"> 
                                            <select  class="form-control" name="rcStatuses" onchange="checkScrap(<%= index %>);" >
                                                <option value="0"><spring:message code="recondition.label.Select"  text="-select-"/></option>
                                                <option value="Y"><spring:message code="recondition.label.Yes"  text="Yes"/></option>
                                                <option value="N"><spring:message code="recondition.label.No"  text="No"/></option>
                                            </select>
                                        </td>
                                        <td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="scrap" value=""></td>
                                    <input type="hidden" name="selectedIndex" value='<%= index %>'>
                                    <input type="hidden" name="categoryIds" value='<c:out value="${item.categoryId}"/>'>
                                    </tr>
                                    <%
                        index++;
                                    %>
                                </c:forEach>                   
                            </table><br>
                            <center>
                                <tr>
                                    <td height="30" ><input type="button" class="btn btn-info" value="<spring:message code="recondition.label.Save"  text="Save"/>" onClick="saveRcItems();"></td>
                                </tr>
                            </center>                                
                        </c:if>           
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>        
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>  
